--
-- PostgreSQL database dump
--

-- Dumped from database version 11.19
-- Dumped by pg_dump version 11.19

-- Started on 2024-09-17 21:37:13

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--
-- TOC entry 857 (class 1247 OID 42953)
-- Name: atendimento; Type: TYPE; Schema: public; Owner: postgres
--

CREATE TYPE public.atendimento AS (
	codigo_aso character varying,
	usuario character varying,
	sala character varying,
	exame character varying,
	periodicidade character varying,
	empresa character varying,
	nome_funcionario character varying,
	rg character varying,
	data_nascimento date,
	funcao character varying,
	peso integer,
	altura numeric(8,2),
	situacao character varying,
	data_atendimento timestamp without time zone,
	idade character varying,
	sexo character varying,
	tiposan character varying,
	ftrh character varying
);


ALTER TYPE public.atendimento OWNER TO postgres;

--
-- TOC entry 438 (class 1255 OID 42954)
-- Name: busca_atendimentos(character varying, character varying, bigint, bigint, bigint, character varying, bigint); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION public.busca_atendimentos(datainicial character varying, datafinal character varying, idaso bigint, idsala bigint, idusuario bigint, situacao character varying, idexame bigint) RETURNS SETOF public.atendimento
    LANGUAGE plpgsql
    AS $$
	DECLARE
	R ATENDIMENTO%ROWTYPE;
	QUERY VARCHAR;

	BEGIN
		QUERY := 'SELECT 
			A.CODIGO_ASO,--NUMERO_ATENDIMENTO
			U.NOME, --USUARIO
			S.DESCRICAO,--SALA
			E.DESCRICAO,--EXAME
			P.DESCRICAO,--PERIODICIDADE
			C.RAZAO_SOCIAL,--EMPRESA
			FUNC.NOME,--NOME_FUNCIONARIO
			FUNC.RG,
			FUNC.DATA_NASCIMENTO,
			F.DESCRICAO,--FUNCAO
			FUNC.PESO,
			FUNC.ALTURA,
			BUSCA_SITUACAO_ATENDIMENTO(GFAEA.ID_GHE_FONTE_AGENTE_EXAME_ASO, ''PCMSO''), ';
			
			IF SITUACAO = 'A' THEN
				QUERY := QUERY || ' GFAEA.DATA_ATENDIDO AS DATA_ATENDIMENTO, ';
			END IF;

			IF SITUACAO = 'F' THEN
				QUERY := QUERY || ' GFAEA.DATA_FINALIZADO AS DATA_ATENDIMENTO, ';
			END IF;
			
			IF SITUACAO = 'C' THEN
				QUERY := QUERY || ' GFAEA.DATA_CANCELADO AS DATA_ATENDIMENTO, ';
			END IF;

			IF SITUACAO = 'AF' THEN
				QUERY := QUERY || ' GFAEA.DATA_ATENDIDO AS DATA_ATENDIMENTO, ';
			END IF;
			
			QUERY := QUERY || ' EXTRACT(YEAR FROM AGE(A.DATANASC_FUNCIONARIO)) AS IDADE, ';
			QUERY := QUERY || ' FUNC.SEXO AS SEXO, ';
			QUERY := QUERY || ' FUNC.TIPOSAN AS TIPOSAN, ';
			QUERY := QUERY || ' FUNC.FTRH AS FTRH ';

		QUERY := QUERY || ' FROM 
			SEG_GHE_FONTE_AGENTE_EXAME_ASO GFAEA
			LEFT JOIN SEG_SALA_EXAME SE ON GFAEA.ID_SALA_EXAME = SE.ID_SALA_EXAME
			LEFT JOIN SEG_USUARIO U ON GFAEA.ID_USUARIO = U.ID_USUARIO
			JOIN SEG_ASO A ON GFAEA.ID_ASO = A.ID_ASO
			JOIN SEG_SALA S ON SE.ID_SALA = S.ID_SALA
			JOIN SEG_PERIODICIDADE P ON A.ID_PERIODICIDADE = P.ID_PERIODICIDADE
			JOIN SEG_CLIENTE_FUNCAO_FUNCIONARIO CFF ON A.ID_CLIENTE_FUNCAO_FUNCIONARIO = CFF.ID_CLIENTE_FUNCAO_FUNCIONARIO
			JOIN SEG_CLIENTE_FUNCAO CF ON CFF.ID_SEG_CLIENTE_FUNCAO = CF.ID_SEG_CLIENTE_FUNCAO
			JOIN SEG_CLIENTE C ON C.ID_CLIENTE = CF.ID_CLIENTE
			JOIN SEG_FUNCAO F ON CF.ID_FUNCAO = F.ID_FUNCAO
			JOIN SEG_FUNCIONARIO FUNC ON CFF.ID_FUNCIONARIO = FUNC.ID_FUNCIONARIO
			JOIN SEG_GHE_FONTE_AGENTE_EXAME GFAE ON GFAEA.ID_GHE_FONTE_AGENTE_EXAME = GFAE.ID_GHE_FONTE_AGENTE_EXAME AND GFAEA.DUPLICADO = FALSE
			JOIN SEG_EXAME E ON GFAE.ID_EXAME = E.ID_EXAME';
		
			IF IDASO <> NULL OR IDASO > 0 THEN
				QUERY := QUERY || ' AND	GFAEA.ID_ASO = ' || IDASO;
			END IF;

			IF IDSALA <> NULL OR IDSALA > 0 THEN
				QUERY := QUERY || ' AND S.ID_SALA = ' || IDSALA;
			END IF;

			IF IDUSUARIO <> NULL OR IDUSUARIO > 0 THEN
				QUERY := QUERY || ' AND U.ID_USUARIO = ' || IDUSUARIO;
			END IF;
			
			IF IDEXAME <> NULL OR IDEXAME > 0 THEN
				QUERY := QUERY || ' AND E.ID_EXAME = ' || IDEXAME;
			END IF;

			IF SITUACAO = 'A' THEN
				QUERY := QUERY || ' AND GFAEA.ATENDIDO = TRUE';
				QUERY := QUERY || ' AND GFAEA.FINALIZADO = FALSE';
				QUERY := QUERY || ' AND GFAEA.CANCELADO = FALSE';
				QUERY := QUERY || ' AND GFAEA.DATA_ATENDIDO BETWEEN TO_TIMESTAMP('''||DATAINICIAL||' 00:00:00'', ''DD/MM/YYYY HH24:MI:SS'') AND TO_TIMESTAMP('''||DATAFINAL||' 23:59:59'', ''DD/MM/YYYY HH24:MI:SS'')'; 
			END IF;

			IF SITUACAO = 'F' THEN
				QUERY := QUERY || ' AND GFAEA.ATENDIDO = TRUE';
				QUERY := QUERY || ' AND GFAEA.FINALIZADO = TRUE';
				QUERY := QUERY || ' AND GFAEA.CANCELADO = FALSE';
				QUERY := QUERY || ' AND GFAEA.DATA_FINALIZADO BETWEEN TO_TIMESTAMP('''||DATAINICIAL||' 00:00:00'', ''DD/MM/YYYY HH24:MI:SS'') AND TO_TIMESTAMP('''||DATAFINAL||' 23:59:59'', ''DD/MM/YYYY HH24:MI:SS'')'; 
			END IF;
			
			IF SITUACAO = 'C' THEN
				QUERY := QUERY || ' AND GFAEA.CANCELADO = TRUE';
				QUERY := QUERY || ' AND GFAEA.DATA_CANCELADO BETWEEN TO_TIMESTAMP('''||DATAINICIAL||' 00:00:00'', ''DD/MM/YYYY HH24:MI:SS'') AND TO_TIMESTAMP('''||DATAFINAL||' 23:59:59'', ''DD/MM/YYYY HH24:MI:SS'')'; 
			END IF;

			IF SITUACAO = 'AF' THEN
				QUERY := QUERY || ' AND (GFAEA.ATENDIDO = TRUE OR GFAEA.FINALIZADO = TRUE)';
				QUERY := QUERY || ' AND GFAEA.CANCELADO = FALSE';
				QUERY := QUERY || ' AND GFAEA.DATA_ATENDIDO BETWEEN TO_TIMESTAMP('''||DATAINICIAL||' 00:00:00'', ''DD/MM/YYYY HH24:MI:SS'') AND TO_TIMESTAMP('''||DATAFINAL||' 23:59:59'', ''DD/MM/YYYY HH24:MI:SS'')'; 
			END IF;
		
		QUERY := QUERY || ' UNION ALL(SELECT 
			A.CODIGO_ASO,--NUMERO_ATENDIMENTO
			U.NOME, --USUARIO
			S.DESCRICAO,--SALA
			E.DESCRICAO,--EXAME
			P.DESCRICAO,--PERIODICIDADE
			C.RAZAO_SOCIAL,--EMPRESA
			FUNC.NOME,--NOME_FUNCIONARIO
			FUNC.RG,
			FUNC.DATA_NASCIMENTO,
			F.DESCRICAO,--FUNCAO
			FUNC.PESO,
			FUNC.ALTURA,
			BUSCA_SITUACAO_ATENDIMENTO(CFEA.ID_CLIENTE_FUNCAO_EXAME_ASO, ''EXTRA''), ';
			
			IF SITUACAO = 'A' THEN
				QUERY := QUERY || ' CFEA.DATA_ATENDIDO AS DATA_ATENDIMENTO, ';
			END IF;

			IF SITUACAO = 'F' THEN
				QUERY := QUERY || ' CFEA.DATA_FINALIZADO AS DATA_ATENDIMENTO, ';
			END IF;
			
			IF SITUACAO = 'C' THEN
				QUERY := QUERY || ' CFEA.DATA_CANCELADO AS DATA_ATENDIMENTO, ';
			END IF;
			
			IF SITUACAO = 'AF' THEN
				QUERY := QUERY || ' CFEA.DATA_ATENDIDO AS DATA_ATENDIMENTO, ';
			END IF;

			QUERY := QUERY || ' EXTRACT(YEAR FROM AGE(A.DATANASC_FUNCIONARIO)) AS IDADE, ';
			QUERY := QUERY || ' FUNC.SEXO AS SEXO, ';
			QUERY := QUERY || ' FUNC.TIPOSAN AS TIPOSAN, ';
			QUERY := QUERY || ' FUNC.FTRH AS FTRH ';
		
		QUERY := QUERY ||' FROM 
			SEG_CLIENTE_FUNCAO_EXAME_ASO CFEA
			LEFT JOIN SEG_SALA_EXAME SE ON CFEA.ID_SALA_EXAME = SE.ID_SALA_EXAME
			LEFT JOIN SEG_USUARIO U ON CFEA.ID_USUARIO = U.ID_USUARIO
			JOIN SEG_ASO A ON CFEA.ID_ASO = A.ID_ASO
			JOIN SEG_SALA S ON SE.ID_SALA = S.ID_SALA
			JOIN SEG_PERIODICIDADE P ON A.ID_PERIODICIDADE = P.ID_PERIODICIDADE
			JOIN SEG_CLIENTE_FUNCAO_FUNCIONARIO CFF ON A.ID_CLIENTE_FUNCAO_FUNCIONARIO = CFF.ID_CLIENTE_FUNCAO_FUNCIONARIO
			JOIN SEG_CLIENTE_FUNCAO CF ON CFF.ID_SEG_CLIENTE_FUNCAO = CF.ID_SEG_CLIENTE_FUNCAO
			JOIN SEG_CLIENTE C ON C.ID_CLIENTE = CF.ID_CLIENTE
			JOIN SEG_FUNCAO F ON CF.ID_FUNCAO = F.ID_FUNCAO
			JOIN SEG_FUNCIONARIO FUNC ON CFF.ID_FUNCIONARIO = FUNC.ID_FUNCIONARIO
			JOIN SEG_CLIENTE_FUNCAO_EXAME CFE ON CFEA.ID_CLIENTE_FUNCAO_EXAME = CFE.ID_CLIENTE_FUNCAO_EXAME
			JOIN SEG_EXAME E ON CFE.ID_EXAME = E.ID_EXAME';
		
		IF IDASO <> NULL OR  IDASO > 0 THEN
			QUERY := QUERY || ' AND CFEA.ID_ASO = ' || IDASO;
		END IF;
		
		IF IDSALA <> NULL OR IDSALA > 0 THEN
			QUERY := QUERY || ' AND S.ID_SALA = ' || IDSALA;
		END IF;

		IF IDUSUARIO <> NULL OR IDUSUARIO > 0 THEN
			QUERY := QUERY || ' AND U.ID_USUARIO = ' || IDUSUARIO;
		END IF;
		
		IF IDEXAME <> NULL OR IDEXAME > 0 THEN
			QUERY := QUERY || ' AND E.ID_EXAME = ' || IDEXAME;
		END IF;

		IF SITUACAO = 'A' THEN
			QUERY := QUERY || ' AND CFEA.ATENDIDO = TRUE';
			QUERY := QUERY || ' AND CFEA.FINALIZADO = FALSE';
			QUERY := QUERY || ' AND CFEA.CANCELADO = FALSE';
			QUERY := QUERY || ' AND CFEA.DATA_ATENDIDO BETWEEN TO_TIMESTAMP('''||DATAINICIAL||' 00:00:00'', ''DD/MM/YYYY HH24:MI:SS'') AND TO_TIMESTAMP('''||DATAFINAL||' 23:59:59'', ''DD/MM/YYYY HH24:MI:SS'')'; 
		END IF;

		IF SITUACAO = 'F' THEN
			QUERY := QUERY || ' AND CFEA.ATENDIDO = TRUE';
			QUERY := QUERY || ' AND CFEA.FINALIZADO = TRUE';
			QUERY := QUERY || ' AND CFEA.CANCELADO = FALSE';
			QUERY := QUERY || ' AND CFEA.DATA_FINALIZADO BETWEEN TO_TIMESTAMP('''||DATAINICIAL||' 00:00:00'', ''DD/MM/YYYY HH24:MI:SS'') AND TO_TIMESTAMP('''||DATAFINAL||' 23:59:59'', ''DD/MM/YYYY HH24:MI:SS'')'; 
		END IF;
		
		IF SITUACAO = 'C' THEN
			QUERY := QUERY || ' AND CFEA.CANCELADO = TRUE';
			QUERY := QUERY || ' AND CFEA.DATA_CANCELADO BETWEEN TO_TIMESTAMP('''||DATAINICIAL||' 00:00:00'', ''DD/MM/YYYY HH24:MI:SS'') AND TO_TIMESTAMP('''||DATAFINAL||' 23:59:59'', ''DD/MM/YYYY HH24:MI:SS'')'; 
		END IF;

		IF SITUACAO = 'AF' THEN
			QUERY := QUERY || ' AND (CFEA.ATENDIDO = TRUE OR CFEA.FINALIZADO = TRUE)';
			QUERY := QUERY || ' AND CFEA.CANCELADO = FALSE';
			QUERY := QUERY || ' AND CFEA.DATA_ATENDIDO BETWEEN TO_TIMESTAMP('''||DATAINICIAL||' 00:00:00'', ''DD/MM/YYYY HH24:MI:SS'') AND TO_TIMESTAMP('''||DATAFINAL||' 23:59:59'', ''DD/MM/YYYY HH24:MI:SS'')'; 
		END IF;
		
		QUERY := QUERY || ') ORDER BY DATA_ATENDIMENTO ASC;';
		RAISE NOTICE 'QUERY(%)', QUERY;
		
		FOR R IN EXECUTE QUERY LOOP
			RETURN NEXT R;
		END LOOP;
		RETURN;
	END
$$;


ALTER FUNCTION public.busca_atendimentos(datainicial character varying, datafinal character varying, idaso bigint, idsala bigint, idusuario bigint, situacao character varying, idexame bigint) OWNER TO postgres;

--
-- TOC entry 439 (class 1255 OID 42955)
-- Name: busca_cnae(bigint, character, character); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION public.busca_cnae(id bigint, tipocliente character, tipograurisco character) RETURNS character varying
    LANGUAGE plpgsql
    AS $$
DECLARE
	REG VARCHAR;
	OUTPUT CHARACTER VARYING = '';
BEGIN
	FOR REG IN 
		SELECT FORMATA_CNAE(CNAE_SEC.COD_CNAE) FROM SEG_CNAE_PPRA CNAE_PPRA_SEC, SEG_CNAE CNAE_SEC WHERE  CNAE_PPRA_SEC.ID_CNAE = CNAE_SEC.ID_CNAE AND CNAE_PPRA_SEC.ID_PPRA = ID AND CNAE_PPRA_SEC.FLAG_CONT = TIPOCLIENTE AND CNAE_PPRA_SEC.FLAG_PR = TIPOGRAURISCO
	LOOP
		OUTPUT := OUTPUT || REG || '  ';
	END LOOP;
	
	RETURN OUTPUT;
END;
$$;


ALTER FUNCTION public.busca_cnae(id bigint, tipocliente character, tipograurisco character) OWNER TO postgres;

--
-- TOC entry 440 (class 1255 OID 42956)
-- Name: busca_data_exame(bigint, bigint); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION public.busca_data_exame(idaso bigint, idexame bigint) RETURNS character varying
    LANGUAGE plpgsql
    AS $$
DECLARE
	REG RECORD;
	OUTPUT VARCHAR := '';
BEGIN
	FOR REG IN 
		SELECT DISTINCT GFAEA.DATA_EXAME FROM 
		SEG_GHE_FONTE_AGENTE_EXAME_ASO GFAEA, 
		SEG_GHE_FONTE_AGENTE_EXAME GFAE,
		SEG_EXAME E
		WHERE 
		GFAEA.ID_GHE_FONTE_AGENTE_EXAME = GFAE.ID_GHE_FONTE_AGENTE_EXAME
		AND E.ID_EXAME = GFAE.ID_EXAME
		AND ID_ASO = IDASO
		AND E.ID_EXAME = IDEXAME
		AND GFAEA.CANCELADO = FALSE
		AND GFAEA.DUPLICADO = FALSE

	UNION(

		SELECT CFEA.DATA_EXAME FROM
		SEG_CLIENTE_FUNCAO_EXAME_ASO CFEA,
		SEG_CLIENTE_FUNCAO_EXAME CFE,
		SEG_EXAME E
		WHERE
		CFEA.ID_CLIENTE_FUNCAO_EXAME = CFE.ID_CLIENTE_FUNCAO_EXAME
		AND CFE.ID_EXAME = E.ID_EXAME
		AND ID_ASO = IDASO
		AND E.ID_EXAME = IDEXAME
		AND CFEA.CANCELADO = FALSE)
	LOOP
		OUTPUT := TO_CHAR(REG.DATA_EXAME, 'DD/MM/YYYY');

	END LOOP;

	RETURN OUTPUT;
END;
$$;


ALTER FUNCTION public.busca_data_exame(idaso bigint, idexame bigint) OWNER TO postgres;

--
-- TOC entry 441 (class 1255 OID 42957)
-- Name: busca_data_exame_by_aso(bigint, bigint[]); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION public.busca_data_exame_by_aso(_idaso bigint, _exames bigint[]) RETURNS character varying
    LANGUAGE plpgsql
    AS $$
DECLARE
	REG RECORD;
BEGIN
	FOR REG IN 
		SELECT DISTINCT GHE_FONTE_AGENTE_EXAME_ASO.DATA_EXAME FROM 
		SEG_GHE_FONTE_AGENTE_EXAME_ASO GHE_FONTE_AGENTE_EXAME_ASO
		LEFT JOIN SEG_GHE_FONTE_AGENTE_EXAME GHE_FONTE_AGENTE_EXAME ON GHE_FONTE_AGENTE_EXAME.ID_GHE_FONTE_AGENTE_EXAME = GHE_FONTE_AGENTE_EXAME_ASO.ID_GHE_FONTE_AGENTE_EXAME
		LEFT JOIN SEG_EXAME EXAME ON EXAME.ID_EXAME = GHE_FONTE_AGENTE_EXAME.ID_EXAME
		WHERE 
		GHE_FONTE_AGENTE_EXAME_ASO.ID_ASO = _IDASO
		AND EXAME.ID_EXAME <> ALL (_EXAMES)
		AND GHE_FONTE_AGENTE_EXAME_ASO.CANCELADO = FALSE
		AND GHE_FONTE_AGENTE_EXAME_ASO.DUPLICADO = FALSE
	UNION(
		SELECT CLIENTE_FUNCAO_EXAME_ASO.DATA_EXAME FROM
		SEG_CLIENTE_FUNCAO_EXAME_ASO CLIENTE_FUNCAO_EXAME_ASO
		LEFT JOIN SEG_CLIENTE_FUNCAO_EXAME CLIENTE_FUNCAO_EXAME ON CLIENTE_FUNCAO_EXAME.ID_CLIENTE_FUNCAO_EXAME = CLIENTE_FUNCAO_EXAME_ASO.ID_CLIENTE_FUNCAO_EXAME
		LEFT JOIN SEG_EXAME EXAME ON EXAME.ID_EXAME = CLIENTE_FUNCAO_EXAME.ID_EXAME
		WHERE
		CLIENTE_FUNCAO_EXAME_ASO.ID_ASO = _IDASO
		AND EXAME.ID_EXAME <> ALL (_EXAMES)
		AND CLIENTE_FUNCAO_EXAME_ASO.CANCELADO = FALSE)
	LOOP
		RETURN TO_CHAR(REG.DATA_EXAME, 'DD/MM/YYYY');
	END LOOP;
END;
$$;


ALTER FUNCTION public.busca_data_exame_by_aso(_idaso bigint, _exames bigint[]) OWNER TO postgres;

--
-- TOC entry 442 (class 1255 OID 42958)
-- Name: busca_data_exame_list(bigint, bigint[]); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION public.busca_data_exame_list(_id_aso bigint, _id_exame bigint[]) RETURNS character varying
    LANGUAGE plpgsql
    AS $$
DECLARE
	REG RECORD;
	OUTPUT VARCHAR := '';
BEGIN
	FOR REG IN 
		SELECT DISTINCT GFAEA.DATA_EXAME FROM 
		SEG_GHE_FONTE_AGENTE_EXAME_ASO GFAEA, 
		SEG_GHE_FONTE_AGENTE_EXAME GFAE,
		SEG_EXAME E
		WHERE 
		GFAEA.ID_GHE_FONTE_AGENTE_EXAME = GFAE.ID_GHE_FONTE_AGENTE_EXAME
		AND E.ID_EXAME = GFAE.ID_EXAME
		AND ID_ASO = _ID_ASO
		AND E.ID_EXAME = ANY (_ID_EXAME)
		AND GFAEA.CANCELADO = FALSE
		AND GFAEA.DUPLICADO = FALSE

	UNION(

		SELECT CFEA.DATA_EXAME FROM
		SEG_CLIENTE_FUNCAO_EXAME_ASO CFEA,
		SEG_CLIENTE_FUNCAO_EXAME CFE,
		SEG_EXAME E
		WHERE
		CFEA.ID_CLIENTE_FUNCAO_EXAME = CFE.ID_CLIENTE_FUNCAO_EXAME
		AND CFE.ID_EXAME = E.ID_EXAME
		AND ID_ASO = _ID_ASO
		AND E.ID_EXAME = ANY (_ID_EXAME)
		AND CFEA.CANCELADO = FALSE)
	LOOP
		OUTPUT := TO_CHAR(REG.DATA_EXAME, 'DD/MM/YYYY');

	END LOOP;

	RETURN OUTPUT;
END;
$$;


ALTER FUNCTION public.busca_data_exame_list(_id_aso bigint, _id_exame bigint[]) OWNER TO postgres;

--
-- TOC entry 443 (class 1255 OID 42959)
-- Name: busca_ghe_by_atendimento(bigint); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION public.busca_ghe_by_atendimento(idaso bigint) RETURNS character varying
    LANGUAGE plpgsql
    AS $$
DECLARE
	REG VARCHAR;
	OUTPUT CHARACTER VARYING = '';
BEGIN
	FOR REG IN 
		SELECT DESCRICAO_GHE FROM SEG_ASO_GHE_SETOR WHERE ID_ASO = IDASO
	LOOP
		OUTPUT := OUTPUT || REG || '  ';
	END LOOP;

	RETURN OUTPUT;
END;
$$;


ALTER FUNCTION public.busca_ghe_by_atendimento(idaso bigint) OWNER TO postgres;

--
-- TOC entry 444 (class 1255 OID 42960)
-- Name: busca_ghe_estudo(bigint); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION public.busca_ghe_estudo(id bigint) RETURNS integer
    LANGUAGE plpgsql
    AS $$
DECLARE
	REG VARCHAR;
	NGHE INTEGER := 0;
BEGIN
	FOR REG IN 
		SELECT G.ID_GHE FROM SEG_GHE G JOIN SEG_ESTUDO P ON P.ID_ESTUDO = G.ID_ESTUDO AND G.SITUACAO = 'A'  WHERE G.ID_ESTUDO = ID
	LOOP

		NGHE := NGHE + 1;
		
	END LOOP;

	RETURN NGHE;
END;

$$;


ALTER FUNCTION public.busca_ghe_estudo(id bigint) OWNER TO postgres;

--
-- TOC entry 445 (class 1255 OID 42961)
-- Name: busca_grau_risco(bigint, character, character); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION public.busca_grau_risco(id bigint, tipocliente character, tipograurisco character) RETURNS character varying
    LANGUAGE plpgsql
    AS $$
DECLARE
	REG VARCHAR;
	OUTPUT CHARACTER VARYING = '';
BEGIN
	FOR REG IN 
		SELECT CNAE_SEC.GRAURISCO FROM SEG_CNAE_PPRA CNAE_PPRA_SEC, SEG_CNAE CNAE_SEC WHERE  CNAE_PPRA_SEC.ID_CNAE = CNAE_SEC.ID_CNAE AND CNAE_PPRA_SEC.ID_PPRA = ID AND CNAE_PPRA_SEC.FLAG_CONT = TIPOCLIENTE AND CNAE_PPRA_SEC.FLAG_PR = TIPOGRAURISCO
	LOOP
		OUTPUT := OUTPUT || REG || '               ';
	END LOOP;

	RETURN OUTPUT;
END;
$$;


ALTER FUNCTION public.busca_grau_risco(id bigint, tipocliente character, tipograurisco character) OWNER TO postgres;

--
-- TOC entry 446 (class 1255 OID 42962)
-- Name: busca_outros_exames_by_aso(bigint, integer[]); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION public.busca_outros_exames_by_aso(_idaso bigint, _exames integer[]) RETURNS character varying
    LANGUAGE plpgsql
    AS $$
DECLARE
	REG RECORD;
	OUTPUT CHARACTER VARYING = '';
BEGIN
	FOR REG IN
		SELECT EXAME.DESCRICAO AS EXAME, GHE_FONTE_AGENTE_EXAME_ASO.DATA_EXAME AS DATA_EXAME
		FROM SEG_EXAME EXAME
		LEFT JOIN SEG_GHE_FONTE_AGENTE_EXAME GHE_FONTE_AGENTE_EXAME ON EXAME.ID_EXAME = GHE_FONTE_AGENTE_EXAME.ID_EXAME
		LEFT JOIN SEG_GHE_FONTE_AGENTE_EXAME_ASO GHE_FONTE_AGENTE_EXAME_ASO ON GHE_FONTE_AGENTE_EXAME_ASO.ID_GHE_FONTE_AGENTE_EXAME = GHE_FONTE_AGENTE_EXAME.ID_GHE_FONTE_AGENTE_EXAME
		WHERE 
		GHE_FONTE_AGENTE_EXAME_ASO.ID_ASO = _IDASO
		AND EXAME.ID_EXAME <> ALL (_EXAMES)
		AND GHE_FONTE_AGENTE_EXAME_ASO.CANCELADO = FALSE
		AND GHE_FONTE_AGENTE_EXAME_ASO.DUPLICADO = FALSE
		UNION (
		SELECT EXAME.DESCRICAO AS EXAME, CLIENTE_FUNCAO_EXAME_ASO.DATA_EXAME AS DATA_EXAME
		FROM SEG_EXAME EXAME
		LEFT JOIN SEG_CLIENTE_FUNCAO_EXAME CLIENTE_FUNCAO_EXAME ON CLIENTE_FUNCAO_EXAME.ID_EXAME = EXAME.ID_EXAME
		LEFT JOIN SEG_CLIENTE_FUNCAO_EXAME_ASO CLIENTE_FUNCAO_EXAME_ASO ON CLIENTE_FUNCAO_EXAME_ASO.ID_CLIENTE_FUNCAO_EXAME = CLIENTE_FUNCAO_EXAME.ID_CLIENTE_FUNCAO_EXAME
		WHERE 
		CLIENTE_FUNCAO_EXAME_ASO.ID_ASO = _IDASO
		AND EXAME.ID_EXAME <> ALL (_EXAMES)
		AND CLIENTE_FUNCAO_EXAME_ASO.CANCELADO = FALSE	
		) ORDER BY EXAME ASC
	LOOP
		OUTPUT := OUTPUT || REG.EXAME || ' - ' || TO_CHAR(REG.DATA_EXAME, 'DD/MM/YYYY') || ', ';
	END LOOP;
	IF (CHAR_LENGTH(OUTPUT) > 0 ) THEN
		OUTPUT := SUBSTR(OUTPUT, 1, CHAR_LENGTH(OUTPUT) -2);
	END IF;
	RETURN OUTPUT;
END;
$$;


ALTER FUNCTION public.busca_outros_exames_by_aso(_idaso bigint, _exames integer[]) OWNER TO postgres;

--
-- TOC entry 447 (class 1255 OID 42963)
-- Name: busca_outros_risco_by_aso(bigint, integer[]); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION public.busca_outros_risco_by_aso(idaso bigint, idoutrosriscos integer[]) RETURNS character varying
    LANGUAGE plpgsql
    AS $$
DECLARE
	REG VARCHAR;
	OUTPUT CHARACTER VARYING = '';
	IDRISCO INTEGER = 0;
	I INTEGER = 0;
	RISCOS VARCHAR = '';
	
BEGIN
	
	FOREACH IDRISCO IN ARRAY idOutrosRiscos LOOP
		I := I + 1;
		RISCOS := RISCOS || IDRISCO;
		IF (array_length(idOutrosRiscos, 1) > I) THEN
			RISCOS := RISCOS || ', ';
		END IF;
	END LOOP;
	FOR REG IN 
		SELECT DISTINCT DESCRICAO_AGENTE
		FROM SEG_ASO_AGENTE_RISCO 
		WHERE ID_ASO = IDASO
		AND ID_AGENTE NOT IN (RISCOS)
	LOOP
		OUTPUT := OUTPUT || REG || ', ';
	END LOOP;
	RETURN OUTPUT;
END;
$$;


ALTER FUNCTION public.busca_outros_risco_by_aso(idaso bigint, idoutrosriscos integer[]) OWNER TO postgres;

--
-- TOC entry 448 (class 1255 OID 42964)
-- Name: busca_outros_riscos_by_aso(bigint, integer[]); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION public.busca_outros_riscos_by_aso(_id_aso bigint, _riscos integer[]) RETURNS character varying
    LANGUAGE plpgsql
    AS $$
DECLARE
	REG RECORD;
	OUTPUT CHARACTER VARYING = '';
	
BEGIN
	FOR REG IN 
		SELECT DISTINCT DESCRICAO_AGENTE
		FROM SEG_ASO_AGENTE_RISCO 
		WHERE ID_ASO = _ID_ASO
		AND ID_AGENTE <> ALL (_RISCOS)
	LOOP
		OUTPUT := OUTPUT || REG.DESCRICAO_AGENTE || ', ';
	END LOOP;
	IF (CHAR_LENGTH(OUTPUT) > 0 ) THEN
		OUTPUT := SUBSTR(OUTPUT, 1, CHAR_LENGTH(OUTPUT) -2);
	END IF;
	RETURN OUTPUT;
END;
$$;


ALTER FUNCTION public.busca_outros_riscos_by_aso(_id_aso bigint, _riscos integer[]) OWNER TO postgres;

--
-- TOC entry 449 (class 1255 OID 42965)
-- Name: busca_outros_riscos_by_aso_by_risco(bigint, integer[], bigint); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION public.busca_outros_riscos_by_aso_by_risco(_id_aso bigint, _riscos integer[], _id_risco bigint) RETURNS character varying
    LANGUAGE plpgsql
    AS $$
DECLARE
	REG RECORD;
	OUTPUT CHARACTER VARYING = '';
	
BEGIN
	FOR REG IN 
		SELECT DISTINCT DESCRICAO_AGENTE
		FROM SEG_ASO_AGENTE_RISCO 
		WHERE ID_ASO = _ID_ASO
		AND ID_RISCO = _ID_RISCO
		AND ID_AGENTE <> ALL (_RISCOS)
	LOOP
		OUTPUT := OUTPUT || REG.DESCRICAO_AGENTE || ', ';
	END LOOP;
	IF (CHAR_LENGTH(OUTPUT) > 0 ) THEN
		OUTPUT := SUBSTR(OUTPUT, 1, CHAR_LENGTH(OUTPUT) -2);
	END IF;
	RETURN OUTPUT;
END;
$$;


ALTER FUNCTION public.busca_outros_riscos_by_aso_by_risco(_id_aso bigint, _riscos integer[], _id_risco bigint) OWNER TO postgres;

--
-- TOC entry 423 (class 1255 OID 42966)
-- Name: busca_periodicidade(bigint, bigint); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION public.busca_periodicidade(idghe bigint, idexame bigint) RETURNS character varying
    LANGUAGE plpgsql
    AS $$
DECLARE
	REG VARCHAR;
	OUTPUT CHARACTER VARYING = '';
BEGIN
	FOR REG IN 
		SELECT P.DESCRICAO  FROM SEG_GHE G
		LEFT JOIN SEG_GHE_FONTE GF ON G.ID_GHE = GF.ID_GHE AND GF.PRINCIPAL <> 'T'
		LEFT JOIN SEG_GHE_FONTE_AGENTE GFA ON GFA.ID_GHE_FONTE = GF.ID_GHE_FONTE
		LEFT JOIN SEG_GHE_FONTE_AGENTE_EXAME GFAEX ON GFAEX.ID_GHE_FONTE_AGENTE = GFA.ID_GHE_FONTE_AGENTE
		LEFT JOIN SEG_EXAME E ON E.ID_EXAME = GFAEX.ID_EXAME
		LEFT JOIN SEG_GHE_FONTE_AGENTE_EXAME_PERIODICIDADE GFAEP ON GFAEP.ID_GHE_FONTE_AGENTE_EXAME = GFAEX.ID_GHE_FONTE_AGENTE_EXAME
		LEFT JOIN SEG_PERIODICIDADE P ON P.ID_PERIODICIDADE = GFAEP.ID_PERIODICIDADE
		WHERE G.ID_GHE = IDGHE AND E.ID_EXAME = IDEXAME ORDER BY P.DESCRICAO ASC
	
	LOOP

		OUTPUT := OUTPUT || REG || ' / ';
	END LOOP;

	RETURN OUTPUT;
END;
$$;


ALTER FUNCTION public.busca_periodicidade(idghe bigint, idexame bigint) OWNER TO postgres;

--
-- TOC entry 424 (class 1255 OID 42967)
-- Name: busca_periodicidade_aso(bigint); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION public.busca_periodicidade_aso(idaso bigint) RETURNS character varying
    LANGUAGE plpgsql
    AS $$
DECLARE
	REG VARCHAR;
	OUTPUT CHARACTER VARYING = '';
BEGIN
	FOR REG IN 
		SELECT P.DESCRICAO FROM SEG_PERIODICIDADE P
		JOIN SEG_ASO A ON P.ID_PERIODICIDADE = A.ID_PERIODICIDADE WHERE A.ID_ASO = IDASO
	LOOP

		IF REG = 'BI-ANUAL' OR REG = 'SEMESTRAL' OR REG = 'ANUAL'
		THEN
			REG := 'PERIÓDICO';
		END IF;

		OUTPUT := REG;

	END LOOP;

	RETURN OUTPUT;
END;
$$;


ALTER FUNCTION public.busca_periodicidade_aso(idaso bigint) OWNER TO postgres;

--
-- TOC entry 425 (class 1255 OID 42968)
-- Name: busca_quantidade_exame_por_coluna(bigint, bigint); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION public.busca_quantidade_exame_por_coluna(coluna bigint, id_ppra bigint) RETURNS integer
    LANGUAGE plpgsql
    AS $$
DECLARE
	QUANTIDADE_EXAMES_EXTRAS BIGINT := 0;
	QUANTIDADE_EXAMES_PCMSO BIGINT := 0;
	TOTAL_EXAMES BIGINT := 0;
	DIVISAO_EXAMES_POR_COLUNAS NUMERIC := 0.0;
	QUANTIDADE_COLUNA INTEGER := 3;
	OUTPUT BIGINT := 0;
BEGIN
	SELECT INTO QUANTIDADE_EXAMES_PCMSO COUNT(*) FROM SEG_ASO A
	JOIN SEG_GHE_FONTE_AGENTE_EXAME_ASO GFAEA ON GFAEA.ID_ASO = A.ID_ASO
	JOIN SEG_GHE_FONTE_AGENTE_EXAME GFAE ON GFAE.ID_GHE_FONTE_AGENTE_EXAME = GFAEA.ID_GHE_FONTE_AGENTE_EXAME
	JOIN SEG_EXAME EX ON EX.ID_EXAME = GFAE.ID_EXAME
	WHERE A.ID_ASO = ID_PPRA;

	SELECT INTO QUANTIDADE_EXAMES_EXTRAS COUNT(*) FROM SEG_ASO A
	JOIN SEG_CLIENTE_FUNCAO_EXAME_ASO CFEA ON CFEA.ID_ASO = A.ID_ASO
	JOIN SEG_CLIENTE_FUNCAO_EXAME CFE ON CFE.ID_CLIENTE_FUNCAO_EXAME = CFEA.ID_CLIENTE_FUNCAO_EXAME
	JOIN SEG_EXAME EX ON EX.ID_EXAME = CFE.ID_EXAME
	WHERE A.ID_ASO = ID_PPRA;

	TOTAL_EXAMES := QUANTIDADE_EXAMES_EXTRAS + QUANTIDADE_EXAMES_PCMSO;

	DIVISAO_EXAMES_POR_COLUNAS := TOTAL_EXAMES/3;

	IF (COLUNA = 1) THEN
		IF (TOTAL_EXAMES >= QUANTIDADE_COLUNA) THEN
			IF (TOTAL_EXAMES%3 >= 1) THEN
				OUTPUT := DIVISAO_EXAMES_POR_COLUNAS + 1;
			ELSE
				OUTPUT := DIVISAO_EXAMES_POR_COLUNAS;
			END IF;
		ELSE
			IF (TOTAL_EXAMES%3 >= 1) THEN
				OUTPUT := 1;
			END IF;
		END IF;
	END IF;

	IF (COLUNA = 2) THEN
		IF (TOTAL_EXAMES >= QUANTIDADE_COLUNA) THEN
			IF (TOTAL_EXAMES%3 >= 2) THEN
				OUTPUT := DIVISAO_EXAMES_POR_COLUNAS + 1;
			ELSE
				OUTPUT := DIVISAO_EXAMES_POR_COLUNAS;
			END IF;
		ELSE
			IF (TOTAL_EXAMES%3 >= 2) THEN
				OUTPUT := 1;
			END IF;
		END IF;
	END IF;

	IF (COLUNA = 3) THEN
		IF (TOTAL_EXAMES >= QUANTIDADE_COLUNA) THEN
			IF (TOTAL_EXAMES%3 < 0) THEN
				OUTPUT := DIVISAO_EXAMES_POR_COLUNAS + 1;
			ELSE
				OUTPUT := DIVISAO_EXAMES_POR_COLUNAS;
			END IF;
		ELSE
			IF (TOTAL_EXAMES%3 < 0) THEN
				OUTPUT := 1;
			END IF;
		END IF;
	END IF;

	RETURN OUTPUT;
END;

$$;


ALTER FUNCTION public.busca_quantidade_exame_por_coluna(coluna bigint, id_ppra bigint) OWNER TO postgres;

--
-- TOC entry 419 (class 1255 OID 42969)
-- Name: busca_risco_by_aso(bigint, bigint); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION public.busca_risco_by_aso(idaso bigint, tiporisco bigint) RETURNS character varying
    LANGUAGE plpgsql
    AS $$
DECLARE
	REG RECORD;
	OUTPUT CHARACTER VARYING = '';

BEGIN
	FOR REG IN 
		SELECT DISTINCT DESCRICAO_AGENTE FROM SEG_ASO_AGENTE_RISCO WHERE ID_ASO = IDASO AND ID_RISCO = TIPORISCO AND ID_AGENTE NOT IN (1,2,3,4,5)
	LOOP
		OUTPUT := OUTPUT || REG.DESCRICAO_AGENTE || ', ';

	END LOOP;
	IF (CHAR_LENGTH(OUTPUT) > 0 ) THEN
		OUTPUT := SUBSTR(OUTPUT, 1, CHAR_LENGTH(OUTPUT) -2);
	END IF;
	
	RETURN OUTPUT;
END
$$;


ALTER FUNCTION public.busca_risco_by_aso(idaso bigint, tiporisco bigint) OWNER TO postgres;

--
-- TOC entry 420 (class 1255 OID 42970)
-- Name: busca_risco_fisico_outros_by_aso_superpesa(bigint); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION public.busca_risco_fisico_outros_by_aso_superpesa(idaso bigint) RETURNS character varying
    LANGUAGE plpgsql
    AS $$
DECLARE
	REG VARCHAR;
	OUTPUT CHARACTER VARYING = '';

BEGIN
	FOR REG IN 
		SELECT DISTINCT DESCRICAO_AGENTE FROM SEG_ASO_AGENTE_RISCO WHERE ID_ASO = IDASO AND ID_RISCO = 1 AND ID_AGENTE NOT IN (1,2,3,4,5,6,7,11,8,111)
	LOOP
		OUTPUT := OUTPUT || REG || ', ';

	END LOOP;

	RETURN OUTPUT;
END;
$$;


ALTER FUNCTION public.busca_risco_fisico_outros_by_aso_superpesa(idaso bigint) OWNER TO postgres;

--
-- TOC entry 421 (class 1255 OID 42971)
-- Name: busca_risco_outros_by_aso_superpesa(bigint); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION public.busca_risco_outros_by_aso_superpesa(idaso bigint) RETURNS character varying
    LANGUAGE plpgsql
    AS $$
DECLARE
	REG VARCHAR;
	OUTPUT CHARACTER VARYING = '';

BEGIN
	FOR REG IN 
		SELECT DISTINCT DESCRICAO_AGENTE FROM SEG_ASO_AGENTE_RISCO WHERE ID_ASO = IDASO AND ID_RISCO = 1 AND ID_AGENTE NOT IN (1,2,3,4,5,6,7,11,8,111)
	LOOP
		OUTPUT := OUTPUT || REG || ', ';

	END LOOP;

	RETURN OUTPUT;
END;
$$;


ALTER FUNCTION public.busca_risco_outros_by_aso_superpesa(idaso bigint) OWNER TO postgres;

--
-- TOC entry 422 (class 1255 OID 42972)
-- Name: busca_risco_quimico_outros_by_aso_superpesa(bigint); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION public.busca_risco_quimico_outros_by_aso_superpesa(idaso bigint) RETURNS character varying
    LANGUAGE plpgsql
    AS $$
DECLARE
	REG VARCHAR;
	OUTPUT CHARACTER VARYING = '';

BEGIN
	FOR REG IN 
		SELECT DISTINCT DESCRICAO_AGENTE FROM SEG_ASO_AGENTE_RISCO WHERE ID_ASO = IDASO AND ID_RISCO = 1 AND ID_AGENTE NOT IN (1,2,3,4,5,6,7,11,8,111)
	LOOP
		OUTPUT := OUTPUT || REG || ', ';

	END LOOP;

	RETURN OUTPUT;
END;
$$;


ALTER FUNCTION public.busca_risco_quimico_outros_by_aso_superpesa(idaso bigint) OWNER TO postgres;

--
-- TOC entry 450 (class 1255 OID 42973)
-- Name: busca_setor_by_cliente_funcao(bigint, bigint); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION public.busca_setor_by_cliente_funcao(idclientefuncao bigint, idaso bigint) RETURNS character varying
    LANGUAGE plpgsql
    AS $$
DECLARE
	REG VARCHAR;
	REG_AUX VARCHAR;
	OUTPUT CHARACTER VARYING = '';
BEGIN
	FOR REG IN
		SELECT GHE_SETOR_AVULSO FROM SEG_ASO WHERE ID_ASO = idaso
	LOOP
		OUTPUT := OUTPUT || REG;
	END LOOP;
		
	IF (CHAR_LENGTH(OUTPUT) > 0 ) THEN
		RETURN OUTPUT;
	END IF;
	
	FOR REG IN 
		SELECT DESCRICAO_GHE || ' / ' || DESCRICAO_SETOR FROM SEG_ASO_GHE_SETOR WHERE ID_ASO = idaso
	LOOP
		OUTPUT := REG;
	END LOOP;
	RETURN OUTPUT;
END;
$$;


ALTER FUNCTION public.busca_setor_by_cliente_funcao(idclientefuncao bigint, idaso bigint) OWNER TO postgres;

--
-- TOC entry 451 (class 1255 OID 42974)
-- Name: busca_situacao_atendimento(bigint, character varying); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION public.busca_situacao_atendimento(iditem bigint, tipoitem character varying) RETURNS character varying
    LANGUAGE plpgsql
    AS $$
DECLARE
	REGPCMSO SEG_GHE_FONTE_AGENTE_EXAME_ASO%ROWTYPE;
	REGEXTRA SEG_CLIENTE_FUNCAO_EXAME_ASO%ROWTYPE;
	OUTPUT VARCHAR := 'NÃO IDENTIFICADO';
	QUERY VARCHAR := '';
BEGIN
	IF TIPOITEM = 'PCMSO' THEN
		QUERY := 'SELECT * FROM SEG_GHE_FONTE_AGENTE_EXAME_ASO WHERE ID_GHE_FONTE_AGENTE_EXAME_ASO = ' || IDITEM;
		FOR REGPCMSO IN EXECUTE QUERY LOOP
			IF REGPCMSO.ATENDIDO IS TRUE AND REGPCMSO.FINALIZADO IS TRUE AND REGPCMSO.CANCELADO IS FALSE THEN
				OUTPUT := 'ATENDIDO';
			END IF;
			
			IF REGPCMSO.ATENDIDO IS TRUE AND REGPCMSO.FINALIZADO IS FALSE AND REGPCMSO.CANCELADO IS FALSE THEN
				OUTPUT := 'NÃO ATENDIDO';
			END IF;

			IF REGPCMSO.CANCELADO IS TRUE THEN
				OUTPUT := 'CANCELADO';
			END IF;
		END LOOP;
	END IF;
	IF TIPOITEM = 'EXTRA' THEN
		QUERY := 'SELECT * FROM SEG_CLIENTE_FUNCAO_EXAME_ASO WHERE ID_CLIENTE_FUNCAO_EXAME_ASO = ' || IDITEM;
		FOR REGEXTRA IN EXECUTE QUERY LOOP
			IF REGEXTRA.ATENDIDO IS TRUE AND REGEXTRA.FINALIZADO IS TRUE AND REGEXTRA.CANCELADO IS FALSE THEN
				OUTPUT := 'ATENDIDO';
			END IF;
			
			IF REGEXTRA.ATENDIDO IS TRUE AND REGEXTRA.FINALIZADO IS FALSE AND REGEXTRA.CANCELADO IS FALSE THEN
				OUTPUT := 'NÃO ATENDIDO';
			END IF;

			IF REGEXTRA.CANCELADO IS TRUE THEN
				OUTPUT := 'CANCELADO';
			END IF;
		END LOOP;
	END IF;
	
	RAISE NOTICE 'QUERY(%)', QUERY;
	
	RETURN OUTPUT;
END;
$$;


ALTER FUNCTION public.busca_situacao_atendimento(iditem bigint, tipoitem character varying) OWNER TO postgres;

--
-- TOC entry 452 (class 1255 OID 42975)
-- Name: find_configuracao(bigint); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION public.find_configuracao(idconfiguracao bigint) RETURNS character varying
    LANGUAGE plpgsql
    AS $$
DECLARE
	REG VARCHAR;
	RETORNO VARCHAR = '';
BEGIN
	FOR REG IN 

		SELECT VALOR FROM SEG_CONFIGURACAO WHERE ID_CONFIGURACAO = IDCONFIGURACAO

	LOOP
		RETORNO := REG;

	END LOOP;

	RETURN RETORNO;

END;
$$;


ALTER FUNCTION public.find_configuracao(idconfiguracao bigint) OWNER TO postgres;

--
-- TOC entry 453 (class 1255 OID 42976)
-- Name: find_estudo_by_aso(bigint); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION public.find_estudo_by_aso(idaso bigint) RETURNS bigint
    LANGUAGE plpgsql
    AS $$
DECLARE
	REG BIGINT;
	VERIFICA BIGINT := -1;

BEGIN
	FOR REG IN 
		SELECT DISTINCT E.ID_ESTUDO FROM 
		SEG_GHE_FONTE_AGENTE_EXAME_ASO GFAEA, 
		SEG_GHE_FONTE_AGENTE_EXAME GFAE, 
		SEG_GHE_FONTE_AGENTE GFA, 
		SEG_GHE_FONTE GF, 
		SEG_GHE E
		WHERE GFAEA.ID_GHE_FONTE_AGENTE_EXAME = GFAE.ID_GHE_FONTE_AGENTE_EXAME
		AND GFAE.ID_GHE_FONTE_AGENTE = GFA.ID_GHE_FONTE_AGENTE
		AND GFA.ID_GHE_FONTE = GF.ID_GHE_FONTE
		AND GF.ID_GHE = E.ID_GHE
		AND GFAEA.ID_ASO = IDASO

	LOOP
		VERIFICA := REG;

	END LOOP;

	RETURN VERIFICA;

END;
$$;


ALTER FUNCTION public.find_estudo_by_aso(idaso bigint) OWNER TO postgres;

--
-- TOC entry 454 (class 1255 OID 42977)
-- Name: find_grau_risco_by_estudo(bigint); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION public.find_grau_risco_by_estudo(idestudo bigint) RETURNS character varying
    LANGUAGE plpgsql
    AS $$
DECLARE
	REG VARCHAR;
	RETORNO VARCHAR = '';

BEGIN

	FOR REG IN 

		SELECT GRAU_RISCO FROM SEG_ESTUDO WHERE ID_ESTUDO = IDESTUDO

	LOOP
		RETORNO := REG;

	END LOOP;

	RETURN RETORNO;

END;
$$;


ALTER FUNCTION public.find_grau_risco_by_estudo(idestudo bigint) OWNER TO postgres;

--
-- TOC entry 455 (class 1255 OID 42978)
-- Name: formata_cep(character varying); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION public.formata_cep(cep character varying) RETURNS character varying
    LANGUAGE plpgsql
    AS $$
BEGIN
	IF CEP = ''
	THEN
		RETURN '';
	ELSE
		RETURN SUBSTRING(CEP FROM 1 FOR 5) || '-' ||
		SUBSTRING(CEP FROM 6 FOR 3);
	END IF;
END;
$$;


ALTER FUNCTION public.formata_cep(cep character varying) OWNER TO postgres;

--
-- TOC entry 456 (class 1255 OID 42979)
-- Name: formata_cnae(character varying); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION public.formata_cnae(cod_cnae character varying) RETURNS character varying
    LANGUAGE plpgsql
    AS $$
BEGIN
	RETURN SUBSTRING(COD_CNAE FROM 1 FOR 2) || '.' || SUBSTRING(COD_CNAE FROM 3 FOR 2) || '-' || SUBSTRING(COD_CNAE FROM 5 FOR 1) || '/' || SUBSTRING(COD_CNAE FROM 6 FOR 2);
END;
$$;


ALTER FUNCTION public.formata_cnae(cod_cnae character varying) OWNER TO postgres;

--
-- TOC entry 457 (class 1255 OID 42980)
-- Name: formata_cnpj(character varying); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION public.formata_cnpj(cnpj character varying) RETURNS character varying
    LANGUAGE plpgsql
    AS $$
BEGIN
	IF CNPJ = ''
	THEN
		RETURN '';
	ELSE
		RETURN SUBSTRING(CNPJ FROM 1 FOR 2) || '.' || SUBSTRING(CNPJ FROM 3 FOR 3) || '.' || SUBSTRING(CNPJ FROM 6 FOR 3) || '/' || SUBSTRING(CNPJ FROM 9 FOR 4) || '-' || SUBSTRING(CNPJ FROM 13 FOR 2); 
	END IF;
END;
$$;


ALTER FUNCTION public.formata_cnpj(cnpj character varying) OWNER TO postgres;

--
-- TOC entry 458 (class 1255 OID 42981)
-- Name: formata_cod_estudo(character varying); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION public.formata_cod_estudo(cod_estudo character varying) RETURNS character varying
    LANGUAGE plpgsql
    AS $$
BEGIN
	RETURN SUBSTRING(COD_ESTUDO FROM	1 FOR 4) || '-' || SUBSTRING(COD_ESTUDO FROM 5 FOR 2) || '-' || SUBSTRING(COD_ESTUDO FROM 7 FOR 6) || '/' ||  SUBSTRING(COD_ESTUDO FROM 13 FOR 2); 
END;
$$;


ALTER FUNCTION public.formata_cod_estudo(cod_estudo character varying) OWNER TO postgres;

--
-- TOC entry 459 (class 1255 OID 42982)
-- Name: formata_codigo_atendimento(character varying); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION public.formata_codigo_atendimento(codigo_aso character varying) RETURNS character varying
    LANGUAGE plpgsql
    AS $$
BEGIN
	RETURN SUBSTRING(CODIGO_ASO FROM 1 FOR 4) || '.' || SUBSTRING(CODIGO_ASO FROM 5 FOR 2) || '.' || SUBSTRING(CODIGO_ASO FROM 7 FOR 6); 
END;
$$;


ALTER FUNCTION public.formata_codigo_atendimento(codigo_aso character varying) OWNER TO postgres;

--
-- TOC entry 460 (class 1255 OID 42983)
-- Name: formata_codigo_contrato(character varying); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION public.formata_codigo_contrato(codigo_contrato character varying) RETURNS character varying
    LANGUAGE plpgsql
    AS $$
BEGIN
	RETURN SUBSTRING(CODIGO_CONTRATO FROM	1 FOR 4) || '.' || 	SUBSTRING(CODIGO_CONTRATO FROM 5 FOR 2) || '.' || SUBSTRING(CODIGO_CONTRATO FROM 7 FOR 6) || '/' ||  SUBSTRING(CODIGO_CONTRATO FROM 13 FOR 2); 
END;
$$;


ALTER FUNCTION public.formata_codigo_contrato(codigo_contrato character varying) OWNER TO postgres;

--
-- TOC entry 461 (class 1255 OID 42984)
-- Name: formata_cpf(character varying); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION public.formata_cpf(cpf character varying) RETURNS character varying
    LANGUAGE plpgsql
    AS $$
BEGIN
	IF CPF = ''
	THEN
		RETURN '';
	ELSE
		RETURN SUBSTRING(CPF FROM 1 FOR 3) || '.' || SUBSTRING(CPF FROM 4 FOR 3) || '.' || SUBSTRING(CPF FROM 7 FOR 3) || '-' || SUBSTRING(CPF FROM 10 FOR 2); 
	END IF;
END;
$$;


ALTER FUNCTION public.formata_cpf(cpf character varying) OWNER TO postgres;

--
-- TOC entry 462 (class 1255 OID 42985)
-- Name: formata_pis(character varying); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION public.formata_pis(pis character varying) RETURNS character varying
    LANGUAGE plpgsql
    AS $$
BEGIN
	IF PIS = ''
	THEN
		RETURN '';
	ELSE
		RETURN SUBSTRING(PIS FROM 1 FOR 3) || '.' || SUBSTRING(PIS FROM 4 FOR 5) || '.' || SUBSTRING(PIS FROM 9 FOR 2) || '.' || SUBSTRING(PIS FROM 11 FOR 2); 
	END IF;
END;
$$;


ALTER FUNCTION public.formata_pis(pis character varying) OWNER TO postgres;

--
-- TOC entry 463 (class 1255 OID 42986)
-- Name: formata_vip(boolean); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION public.formata_vip(vip boolean) RETURNS character varying
    LANGUAGE plpgsql
    AS $$
DECLARE
	OUTPUT CHARACTER VARYING = '';

BEGIN
	IF (VIP = 'T') THEN
		RETURN 'V';
	ELSE
		RETURN '';
	END IF;
END;

$$;


ALTER FUNCTION public.formata_vip(vip boolean) OWNER TO postgres;

--
-- TOC entry 464 (class 1255 OID 42987)
-- Name: retorna_indice_mes(character varying); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION public.retorna_indice_mes(mes character varying) RETURNS integer
    LANGUAGE plpgsql
    AS $$
DECLARE	
	MESES VARCHAR[] := ARRAY['JANEIRO','FEVEREIRO', 'MARCO', 'ABRIL','MAIO','JUNHO','JULHO','AGOSTO','SETEMBRO','OUTUBRO','NOVEMBRO','DEZEMBRO', 'TODOS OS MESES'];
	I INTEGER;
BEGIN
	FOR I IN 1..13 LOOP
		IF MESES[I] = MES THEN
			RETURN I;
			EXIT;
		END IF;
	END LOOP;
END;
$$;


ALTER FUNCTION public.retorna_indice_mes(mes character varying) OWNER TO postgres;

--
-- TOC entry 465 (class 1255 OID 42988)
-- Name: verifica_aso_atende_exame(bigint, bigint); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION public.verifica_aso_atende_exame(idaso bigint, idexame bigint) RETURNS character varying
    LANGUAGE plpgsql
    AS $$
DECLARE
	REG VARCHAR;
	OUTPUT CHARACTER VARYING = '';
	VERIFICA VARCHAR := 'FALSE';
BEGIN
	FOR REG IN 
		SELECT E.DESCRICAO FROM 
		SEG_GHE_FONTE_AGENTE_EXAME_ASO GFAEA, 
		SEG_GHE_FONTE_AGENTE_EXAME GFAE,
		SEG_EXAME E
		WHERE 
		GFAEA.ID_GHE_FONTE_AGENTE_EXAME = GFAE.ID_GHE_FONTE_AGENTE_EXAME
		AND E.ID_EXAME = GFAE.ID_EXAME
		AND ID_ASO = IDASO
		AND E.ID_EXAME = IDEXAME
		AND GFAEA.CANCELADO = FALSE
		AND GFAEA.DUPLICADO = FALSE

	LOOP
		VERIFICA := 'TRUE';

	END LOOP;

	FOR REG IN 
		SELECT E.DESCRICAO FROM
		SEG_CLIENTE_FUNCAO_EXAME_ASO CFEA,
		SEG_CLIENTE_FUNCAO_EXAME CFE,
		SEG_EXAME E
		WHERE
		CFEA.ID_CLIENTE_FUNCAO_EXAME = CFE.ID_CLIENTE_FUNCAO_EXAME
		AND CFE.ID_EXAME = E.ID_EXAME
		AND ID_ASO = IDASO
		AND E.ID_EXAME = IDEXAME
		AND CFEA.CANCELADO = FALSE
	LOOP
		VERIFICA := 'TRUE';

	END LOOP;

	RETURN VERIFICA;

END;
$$;


ALTER FUNCTION public.verifica_aso_atende_exame(idaso bigint, idexame bigint) OWNER TO postgres;

--
-- TOC entry 466 (class 1255 OID 42989)
-- Name: verifica_aso_atende_exame_list(bigint, bigint[]); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION public.verifica_aso_atende_exame_list(_idaso bigint, _idexame bigint[]) RETURNS character varying
    LANGUAGE plpgsql
    AS $$
DECLARE
	REG VARCHAR;
	OUTPUT CHARACTER VARYING = '';
	VERIFICA VARCHAR := 'FALSE';
BEGIN
	FOR REG IN 
		SELECT EXAME.DESCRICAO 
		FROM SEG_GHE_FONTE_AGENTE_EXAME_ASO GHE_FONTE_AGENTE_EXAME_ASO
		JOIN SEG_GHE_FONTE_AGENTE_EXAME GHE_FONTE_AGENTE_EXAME
		ON GHE_FONTE_AGENTE_EXAME.ID_GHE_FONTE_AGENTE_EXAME = GHE_FONTE_AGENTE_EXAME_ASO.ID_GHE_FONTE_AGENTE_EXAME
		JOIN SEG_EXAME EXAME
		ON EXAME.ID_EXAME = GHE_FONTE_AGENTE_EXAME.ID_EXAME
		WHERE TRUE 
		AND GHE_FONTE_AGENTE_EXAME_ASO.ID_ASO = _IDASO
		AND EXAME.ID_EXAME = any ( _IDEXAME)
		AND GHE_FONTE_AGENTE_EXAME_ASO.CANCELADO = FALSE
		AND GHE_FONTE_AGENTE_EXAME_ASO.DUPLICADO = FALSE

	LOOP
		VERIFICA := 'TRUE';

	END LOOP;

	FOR REG IN 
		SELECT EXAME.DESCRICAO 
		FROM SEG_CLIENTE_FUNCAO_EXAME_ASO CLIENTE_FUNCAO_EXAME_ASO
		JOIN SEG_CLIENTE_FUNCAO_EXAME CLIENTE_FUNCAO_EXAME
		ON CLIENTE_FUNCAO_EXAME.ID_CLIENTE_FUNCAO_EXAME = CLIENTE_FUNCAO_EXAME_ASO.ID_CLIENTE_FUNCAO_EXAME
		JOIN SEG_EXAME EXAME
		ON EXAME.ID_EXAME = CLIENTE_FUNCAO_EXAME.ID_EXAME
		WHERE TRUE
		AND CLIENTE_FUNCAO_EXAME_ASO.ID_ASO = _IDASO
		AND EXAME.ID_EXAME = any (_IDEXAME)
		AND CLIENTE_FUNCAO_EXAME_ASO.CANCELADO = FALSE
	LOOP
		VERIFICA := 'TRUE';

	END LOOP;

	RETURN VERIFICA;

END;
$$;


ALTER FUNCTION public.verifica_aso_atende_exame_list(_idaso bigint, _idexame bigint[]) OWNER TO postgres;

--
-- TOC entry 467 (class 1255 OID 42990)
-- Name: verifica_cidade_unidade(bigint); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION public.verifica_cidade_unidade(idaso bigint) RETURNS character varying
    LANGUAGE plpgsql
    AS $$
DECLARE
	REG VARCHAR;
	OUTPUT CHARACTER VARYING = '';
BEGIN
	FOR REG IN 

		SELECT CIDADE.NOME FROM SEG_ASO ATENDIMENTO 
		JOIN SEG_ESTUDO ESTUDO ON ESTUDO.ID_ESTUDO = ATENDIMENTO.ID_ESTUDO 
		JOIN SEG_CLIENTE CLIENTE ON CLIENTE.ID_CLIENTE = ESTUDO.ID_CLIENTE_CONTRATADO AND CLIENTE.ID_CLIENTE_MATRIZ IS NOT NULL
		JOIN SEG_REP_CLI REPRESENTANTE_CLIENTE ON REPRESENTANTE_CLIENTE.ID_SEG_REP_CLI = ESTUDO.ID_SEG_REP_CLI AND CLIENTE.ID_CLIENTE_MATRIZ = REPRESENTANTE_CLIENTE.ID_CLIENTE
		LEFT JOIN SEG_CIDADE_IBGE CIDADE ON CIDADE.ID_CIDADE_IBGE = CLIENTE.ID_CIDADE_IBGE
		WHERE ID_ASO = IDASO

	LOOP
		OUTPUT := REG;

	END LOOP;

	RETURN OUTPUT;

END;
$$;


ALTER FUNCTION public.verifica_cidade_unidade(idaso bigint) OWNER TO postgres;

--
-- TOC entry 468 (class 1255 OID 42991)
-- Name: verifica_cliente_credenciavel(bigint); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION public.verifica_cliente_credenciavel(idcliente bigint) RETURNS boolean
    LANGUAGE plpgsql
    AS $$
DECLARE
	REG VARCHAR;
	VERIFICA BOOLEAN := FALSE;

BEGIN
	
	FOR REG IN 

		SELECT * FROM SEG_CLIENTE WHERE ID_CLIENTE_CREDENCIADO = IDCLIENTE

	LOOP
		VERIFICA := TRUE;

	END LOOP;

	RETURN VERIFICA;

END;
$$;


ALTER FUNCTION public.verifica_cliente_credenciavel(idcliente bigint) OWNER TO postgres;

--
-- TOC entry 469 (class 1255 OID 42992)
-- Name: verifica_cliente_matriz(bigint); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION public.verifica_cliente_matriz(idcliente bigint) RETURNS character varying
    LANGUAGE plpgsql
    AS $$
DECLARE
	REG VARCHAR;
	OUTPUT CHARACTER VARYING = '';
	VERIFICA VARCHAR := 'FALSE';

BEGIN
	FOR REG IN 

		SELECT * FROM SEG_CLIENTE WHERE ID_CLIENTE_MATRIZ = IDCLIENTE

	LOOP
		VERIFICA := 'TRUE';

	END LOOP;

	RETURN VERIFICA;

END;
$$;


ALTER FUNCTION public.verifica_cliente_matriz(idcliente bigint) OWNER TO postgres;

--
-- TOC entry 470 (class 1255 OID 42993)
-- Name: verifica_existe_risco(bigint, bigint); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION public.verifica_existe_risco(idaso bigint, idrisco bigint) RETURNS character varying
    LANGUAGE plpgsql
    AS $$
DECLARE
	REG VARCHAR;
	OUTPUT CHARACTER VARYING = '';
	VERIFICA VARCHAR := 'FALSE';
BEGIN
	FOR REG IN 

		SELECT * FROM SEG_ASO_AGENTE_RISCO WHERE ID_ASO = IDASO AND ID_RISCO = IDRISCO

	LOOP
		VERIFICA := 'TRUE';

	END LOOP;

	RETURN VERIFICA;

END;
$$;


ALTER FUNCTION public.verifica_existe_risco(idaso bigint, idrisco bigint) OWNER TO postgres;

--
-- TOC entry 471 (class 1255 OID 42994)
-- Name: verifica_existe_risco_retirando_agente_nao_identificado(bigint, bigint); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION public.verifica_existe_risco_retirando_agente_nao_identificado(idaso bigint, idrisco bigint) RETURNS character varying
    LANGUAGE plpgsql
    AS $$
DECLARE
	REG VARCHAR;
	OUTPUT CHARACTER VARYING = '';
	VERIFICA VARCHAR := 'FALSE';

BEGIN
	FOR REG IN 

		SELECT * FROM SEG_ASO_AGENTE_RISCO WHERE ID_ASO = IDASO AND ID_RISCO = IDRISCO
	LOOP
		VERIFICA := 'TRUE';

	END LOOP;

	RETURN VERIFICA;

END;
$$;


ALTER FUNCTION public.verifica_existe_risco_retirando_agente_nao_identificado(idaso bigint, idrisco bigint) OWNER TO postgres;

--
-- TOC entry 472 (class 1255 OID 42995)
-- Name: verifica_existe_unidade(bigint); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION public.verifica_existe_unidade(idaso bigint) RETURNS character varying
    LANGUAGE plpgsql
    AS $$
DECLARE
	REG VARCHAR;
	OUTPUT CHARACTER VARYING = '';
BEGIN
	FOR REG IN 

		SELECT C.RAZAO_SOCIAL FROM SEG_ASO A 
		JOIN SEG_ESTUDO E ON E.ID_ESTUDO = A.ID_ESTUDO 
		JOIN SEG_CLIENTE C ON C.ID_CLIENTE = E.ID_CLIENTE_CONTRATADO AND C.ID_CLIENTE_MATRIZ IS NOT NULL
		JOIN SEG_REP_CLI RC ON RC.ID_SEG_REP_CLI = E.ID_SEG_REP_CLI
		AND C.ID_CLIENTE_MATRIZ = RC.ID_CLIENTE
		WHERE ID_ASO = IDASO

	LOOP
		OUTPUT := REG;

	END LOOP;

	RETURN OUTPUT;

END;
$$;


ALTER FUNCTION public.verifica_existe_unidade(idaso bigint) OWNER TO postgres;

--
-- TOC entry 473 (class 1255 OID 42996)
-- Name: verifica_risco_atendimento(bigint, bigint); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION public.verifica_risco_atendimento(idatendimento bigint, idrisco bigint) RETURNS boolean
    LANGUAGE plpgsql
    AS $$
DECLARE
	REG VARCHAR;
	VERIFICA BOOLEAN := FALSE;

BEGIN
	
	FOR REG IN 

		SELECT * FROM SEG_ASO_AGENTE_RISCO WHERE ID_ASO = IDATENDIMENTO AND ID_AGENTE = IDRISCO

	LOOP
		VERIFICA := TRUE;

	END LOOP;

	RETURN VERIFICA;
END;
$$;


ALTER FUNCTION public.verifica_risco_atendimento(idatendimento bigint, idrisco bigint) OWNER TO postgres;

--
-- TOC entry 474 (class 1255 OID 42997)
-- Name: verifica_situacao_altura(bigint); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION public.verifica_situacao_altura(idaso bigint) RETURNS character varying
    LANGUAGE plpgsql
    AS $$
DECLARE
	REG VARCHAR;
	OUTPUT CHARACTER VARYING = '';
	VERIFICA VARCHAR := 'FALSE';

BEGIN
	FOR REG IN 
		SELECT ALTURA FROM SEG_GHE_FONTE_AGENTE_EXAME_ASO GFAEA
		WHERE GFAEA.ID_ASO = IDASO
		
	LOOP
		IF REG = 'T' THEN 
		VERIFICA := 'TRUE' ;
		END IF;

	END LOOP;
	
	RETURN VERIFICA;
END;
$$;


ALTER FUNCTION public.verifica_situacao_altura(idaso bigint) OWNER TO postgres;

--
-- TOC entry 475 (class 1255 OID 42998)
-- Name: verifica_situacao_confinado(bigint); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION public.verifica_situacao_confinado(idaso bigint) RETURNS character varying
    LANGUAGE plpgsql
    AS $$
DECLARE
	REG VARCHAR;
	OUTPUT CHARACTER VARYING = '';
	VERIFICA VARCHAR := 'FALSE';

BEGIN
	FOR REG IN 
		SELECT CONFINADO FROM SEG_GHE_FONTE_AGENTE_EXAME_ASO GFAEA
		WHERE GFAEA.ID_ASO = IDASO
		
	LOOP
		IF REG = 'T' THEN 
		VERIFICA := 'TRUE' ;
		END IF;

	END LOOP;
	
	RETURN VERIFICA;
END;
$$;


ALTER FUNCTION public.verifica_situacao_confinado(idaso bigint) OWNER TO postgres;

--
-- TOC entry 476 (class 1255 OID 42999)
-- Name: verifica_uf_unidade(bigint); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION public.verifica_uf_unidade(idaso bigint) RETURNS character varying
    LANGUAGE plpgsql
    AS $$
DECLARE
	REG VARCHAR;
	OUTPUT CHARACTER VARYING = '';
BEGIN
	FOR REG IN 

		SELECT CLIENTE.UF FROM SEG_ASO ATENDIMENTO 
		JOIN SEG_ESTUDO ESTUDO ON ESTUDO.ID_ESTUDO = ATENDIMENTO.ID_ESTUDO 
		JOIN SEG_CLIENTE CLIENTE ON CLIENTE.ID_CLIENTE = ESTUDO.ID_CLIENTE_CONTRATADO AND CLIENTE.ID_CLIENTE_MATRIZ IS NOT NULL
		JOIN SEG_REP_CLI REPRESENTANTE_CLIENTE ON REPRESENTANTE_CLIENTE.ID_SEG_REP_CLI = ESTUDO.ID_SEG_REP_CLI AND CLIENTE.ID_CLIENTE_MATRIZ = REPRESENTANTE_CLIENTE.ID_CLIENTE
		WHERE ID_ASO = IDASO

	LOOP
		OUTPUT := REG;

	END LOOP;

	RETURN OUTPUT;

END;
$$;


ALTER FUNCTION public.verifica_uf_unidade(idaso bigint) OWNER TO postgres;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- TOC entry 197 (class 1259 OID 43000)
-- Name: seg_acao; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.seg_acao (
    id_acao bigint NOT NULL,
    descricao character varying(50) NOT NULL,
    situacao character varying(1) NOT NULL
);


ALTER TABLE public.seg_acao OWNER TO postgres;

--
-- TOC entry 198 (class 1259 OID 43003)
-- Name: seg_acao_id_acao_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.seg_acao_id_acao_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.seg_acao_id_acao_seq OWNER TO postgres;

--
-- TOC entry 4654 (class 0 OID 0)
-- Dependencies: 198
-- Name: seg_acao_id_acao_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.seg_acao_id_acao_seq OWNED BY public.seg_acao.id_acao;


--
-- TOC entry 199 (class 1259 OID 43005)
-- Name: seg_acesso; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.seg_acesso (
    id_acesso bigint NOT NULL,
    login character varying NOT NULL,
    ip character varying NOT NULL,
    host_name character varying NOT NULL,
    ultimo_acesso timestamp without time zone NOT NULL
);


ALTER TABLE public.seg_acesso OWNER TO postgres;

--
-- TOC entry 200 (class 1259 OID 43011)
-- Name: seg_acesso_id_acesso_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.seg_acesso_id_acesso_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.seg_acesso_id_acesso_seq OWNER TO postgres;

--
-- TOC entry 4655 (class 0 OID 0)
-- Dependencies: 200
-- Name: seg_acesso_id_acesso_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.seg_acesso_id_acesso_seq OWNED BY public.seg_acesso.id_acesso;


--
-- TOC entry 201 (class 1259 OID 43013)
-- Name: seg_acompanhamento_atendimento; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.seg_acompanhamento_atendimento (
    id_acompanhamento_atendimento bigint NOT NULL,
    nome_funcionario character varying(100) NOT NULL,
    sala character varying(20) NOT NULL,
    andar integer NOT NULL,
    cliente_razao_social character varying(100) NOT NULL,
    rg character varying(15) NOT NULL,
    data_inclusao timestamp without time zone NOT NULL,
    senha_atendimento character varying(6) NOT NULL,
    flag_atendido boolean DEFAULT false NOT NULL,
    cpf_funcionario character varying(11),
    data_atendido timestamp without time zone,
    id_aso bigint NOT NULL,
    codigo_aso character varying(12) NOT NULL,
    exame character varying(100) NOT NULL,
    id_empresa bigint,
    nome_empresa character varying(100)
);


ALTER TABLE public.seg_acompanhamento_atendimento OWNER TO postgres;

--
-- TOC entry 202 (class 1259 OID 43017)
-- Name: seg_acompanhamento_atendiment_id_acompanhamento_atendimento_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.seg_acompanhamento_atendiment_id_acompanhamento_atendimento_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.seg_acompanhamento_atendiment_id_acompanhamento_atendimento_seq OWNER TO postgres;

--
-- TOC entry 4656 (class 0 OID 0)
-- Dependencies: 202
-- Name: seg_acompanhamento_atendiment_id_acompanhamento_atendimento_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.seg_acompanhamento_atendiment_id_acompanhamento_atendimento_seq OWNED BY public.seg_acompanhamento_atendimento.id_acompanhamento_atendimento;


--
-- TOC entry 203 (class 1259 OID 43019)
-- Name: seg_agente; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.seg_agente (
    id_agente bigint NOT NULL,
    id_risco bigint NOT NULL,
    descricao text NOT NULL,
    trajetoria character varying(255),
    danos character varying(255),
    limite character varying(100),
    situacao character varying(1) NOT NULL,
    privado boolean NOT NULL,
    intensidade character varying(100),
    tecnica character varying(100),
    codigo_esocial character varying(10)
);


ALTER TABLE public.seg_agente OWNER TO postgres;

--
-- TOC entry 204 (class 1259 OID 43025)
-- Name: seg_agente_id_agente_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.seg_agente_id_agente_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.seg_agente_id_agente_seq OWNER TO postgres;

--
-- TOC entry 4657 (class 0 OID 0)
-- Dependencies: 204
-- Name: seg_agente_id_agente_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.seg_agente_id_agente_seq OWNED BY public.seg_agente.id_agente;


--
-- TOC entry 205 (class 1259 OID 43027)
-- Name: seg_arquivo; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.seg_arquivo (
    id_arquivo bigint NOT NULL,
    descricao character varying NOT NULL,
    mimetype character varying NOT NULL,
    size integer NOT NULL,
    conteudo bytea NOT NULL
);


ALTER TABLE public.seg_arquivo OWNER TO postgres;

--
-- TOC entry 206 (class 1259 OID 43033)
-- Name: seg_arquivo_anexo; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.seg_arquivo_anexo (
    id_arquivo bigint NOT NULL,
    id_aso bigint NOT NULL,
    descricao character varying NOT NULL,
    mimetype character varying NOT NULL,
    size integer NOT NULL,
    conteudo bytea NOT NULL
);


ALTER TABLE public.seg_arquivo_anexo OWNER TO postgres;

--
-- TOC entry 207 (class 1259 OID 43039)
-- Name: seg_arquivo_anexo_id_arquivo_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.seg_arquivo_anexo_id_arquivo_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.seg_arquivo_anexo_id_arquivo_seq OWNER TO postgres;

--
-- TOC entry 4658 (class 0 OID 0)
-- Dependencies: 207
-- Name: seg_arquivo_anexo_id_arquivo_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.seg_arquivo_anexo_id_arquivo_seq OWNED BY public.seg_arquivo_anexo.id_arquivo;


--
-- TOC entry 208 (class 1259 OID 43041)
-- Name: seg_arquivo_id_arquivo_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.seg_arquivo_id_arquivo_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.seg_arquivo_id_arquivo_seq OWNER TO postgres;

--
-- TOC entry 4659 (class 0 OID 0)
-- Dependencies: 208
-- Name: seg_arquivo_id_arquivo_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.seg_arquivo_id_arquivo_seq OWNED BY public.seg_arquivo.id_arquivo;


--
-- TOC entry 209 (class 1259 OID 43043)
-- Name: seg_aso; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.seg_aso (
    id_aso bigint NOT NULL,
    codigo_aso character varying(12) NOT NULL,
    id_medico_examinador bigint,
    id_cliente_funcao_funcionario bigint NOT NULL,
    data_aso timestamp without time zone NOT NULL,
    nome_funcionario character varying(100),
    rg_funcionario character varying(15),
    datanasc_funcionario date,
    cpf_funcionario character varying(11),
    tiposan_funcionario character varying(2),
    ftrh_funcionario character varying(1),
    matricula_funcionario character varying(30),
    razao_empresa character varying(100),
    cnpj_empresa character varying(14),
    endereco_empresa character varying(100),
    numero_empresa character varying(10),
    complemento_empresa character varying(100),
    bairro_empresa character varying(50),
    cep_empresa character varying(8),
    cidade_empresa character varying(50),
    uf_empresa character varying(2),
    id_periodicidade bigint NOT NULL,
    situacao character varying(1) NOT NULL,
    id_medico_coordenador bigint,
    crm_coordenador character varying(15),
    telefone1_coordenador character varying(15) NOT NULL,
    telefone2_coordenador character varying(15) NOT NULL,
    nome_coordenador character varying(100),
    crm_examinador character varying(15),
    nome_examinador character varying(100),
    telefone1_examinador character varying(15),
    telefone2_examinador character varying(15),
    id_usuario_criador bigint NOT NULL,
    id_usuario_finalizador bigint,
    id_estudo bigint,
    obs_alteracao text,
    obs_aso text,
    conclusao character varying(1),
    data_gravacao timestamp without time zone NOT NULL,
    data_cancelamento timestamp without time zone,
    id_usuario_cancelamento bigint,
    id_ult_usuario_alteracao bigint,
    data_finalizacao timestamp without time zone,
    senha character varying(6),
    id_protocolo bigint,
    obs_cancelamento text,
    id_empresa bigint NOT NULL,
    id_centrocusto bigint,
    data_vencimento date,
    numero_po character varying(15),
    prioridade boolean DEFAULT false NOT NULL,
    pis character varying(11),
    ghe_setor_avulso character varying(255),
    funcao_funcionario character varying(100),
    confinado boolean DEFAULT false NOT NULL,
    altura boolean DEFAULT false NOT NULL,
    eletricidade boolean DEFAULT false NOT NULL
);


ALTER TABLE public.seg_aso OWNER TO postgres;

--
-- TOC entry 210 (class 1259 OID 43053)
-- Name: seg_aso_agente_risco; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.seg_aso_agente_risco (
    id_aso bigint NOT NULL,
    id_agente bigint NOT NULL,
    descricao_agente character varying,
    id_risco bigint,
    descricao_risco character varying
);


ALTER TABLE public.seg_aso_agente_risco OWNER TO postgres;

--
-- TOC entry 211 (class 1259 OID 43059)
-- Name: seg_aso_ghe_setor; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.seg_aso_ghe_setor (
    id_aso bigint NOT NULL,
    id_ghe bigint NOT NULL,
    descricao_ghe character varying,
    id_setor bigint,
    descricao_setor character varying,
    numero_po character varying(15)
);


ALTER TABLE public.seg_aso_ghe_setor OWNER TO postgres;

--
-- TOC entry 212 (class 1259 OID 43065)
-- Name: seg_aso_id_aso_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.seg_aso_id_aso_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.seg_aso_id_aso_seq OWNER TO postgres;

--
-- TOC entry 4660 (class 0 OID 0)
-- Dependencies: 212
-- Name: seg_aso_id_aso_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.seg_aso_id_aso_seq OWNED BY public.seg_aso.id_aso;


--
-- TOC entry 213 (class 1259 OID 43067)
-- Name: seg_atividade; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.seg_atividade (
    id_atividade bigint NOT NULL,
    descricao character varying(200) NOT NULL,
    situacao character varying(1) NOT NULL
);


ALTER TABLE public.seg_atividade OWNER TO postgres;

--
-- TOC entry 214 (class 1259 OID 43070)
-- Name: seg_atividade_cronograma; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.seg_atividade_cronograma (
    id_atividade bigint NOT NULL,
    id_cronograma bigint NOT NULL,
    mes_realizado character varying(15) NOT NULL,
    ano_realizado character varying(4) NOT NULL,
    publico character varying(250) NOT NULL,
    evidencia character varying(250) NOT NULL
);


ALTER TABLE public.seg_atividade_cronograma OWNER TO postgres;

--
-- TOC entry 215 (class 1259 OID 43076)
-- Name: seg_atividade_id_atividade_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.seg_atividade_id_atividade_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.seg_atividade_id_atividade_seq OWNER TO postgres;

--
-- TOC entry 4661 (class 0 OID 0)
-- Dependencies: 215
-- Name: seg_atividade_id_atividade_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.seg_atividade_id_atividade_seq OWNED BY public.seg_atividade.id_atividade;


--
-- TOC entry 216 (class 1259 OID 43078)
-- Name: seg_banco; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.seg_banco (
    id_banco bigint NOT NULL,
    nome character varying(100) NOT NULL,
    codigo_banco character varying(3),
    caixa boolean NOT NULL,
    situacao character varying(1) NOT NULL,
    privado boolean NOT NULL,
    boleto boolean NOT NULL
);


ALTER TABLE public.seg_banco OWNER TO postgres;

--
-- TOC entry 217 (class 1259 OID 43081)
-- Name: seg_banco_id_banco_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.seg_banco_id_banco_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.seg_banco_id_banco_seq OWNER TO postgres;

--
-- TOC entry 4662 (class 0 OID 0)
-- Dependencies: 217
-- Name: seg_banco_id_banco_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.seg_banco_id_banco_seq OWNED BY public.seg_banco.id_banco;


--
-- TOC entry 218 (class 1259 OID 43083)
-- Name: seg_centrocusto; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.seg_centrocusto (
    id bigint NOT NULL,
    id_cliente bigint NOT NULL,
    descricao character varying(50) NOT NULL,
    situacao character varying(1) DEFAULT 'A'::character varying NOT NULL
);


ALTER TABLE public.seg_centrocusto OWNER TO postgres;

--
-- TOC entry 219 (class 1259 OID 43087)
-- Name: seg_centrocusto_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.seg_centrocusto_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.seg_centrocusto_id_seq OWNER TO postgres;

--
-- TOC entry 4663 (class 0 OID 0)
-- Dependencies: 219
-- Name: seg_centrocusto_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.seg_centrocusto_id_seq OWNED BY public.seg_centrocusto.id;


--
-- TOC entry 220 (class 1259 OID 43089)
-- Name: seg_cidade_ibge; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.seg_cidade_ibge (
    id_cidade_ibge bigint NOT NULL,
    codigo character varying(6) NOT NULL,
    nome character varying(100) NOT NULL,
    uf_cidade character varying(2) NOT NULL
);


ALTER TABLE public.seg_cidade_ibge OWNER TO postgres;

--
-- TOC entry 221 (class 1259 OID 43092)
-- Name: seg_cidade_ibge_id_cidade_ibge_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.seg_cidade_ibge_id_cidade_ibge_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.seg_cidade_ibge_id_cidade_ibge_seq OWNER TO postgres;

--
-- TOC entry 4664 (class 0 OID 0)
-- Dependencies: 221
-- Name: seg_cidade_ibge_id_cidade_ibge_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.seg_cidade_ibge_id_cidade_ibge_seq OWNED BY public.seg_cidade_ibge.id_cidade_ibge;


--
-- TOC entry 222 (class 1259 OID 43094)
-- Name: seg_cli_cnae; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.seg_cli_cnae (
    id_cliente bigint NOT NULL,
    id_cnae bigint NOT NULL,
    flag_pr boolean NOT NULL
);


ALTER TABLE public.seg_cli_cnae OWNER TO postgres;

--
-- TOC entry 223 (class 1259 OID 43097)
-- Name: seg_cliente; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.seg_cliente (
    id_cliente bigint NOT NULL,
    id_ramo bigint,
    razao_social character varying(100) NOT NULL,
    fantasia character varying(100),
    cnpj character varying(14) NOT NULL,
    inscricao character varying(20) NOT NULL,
    endereco character varying(100),
    numero character varying(10),
    complemento character varying(100),
    bairro character varying(50),
    cep character varying(8),
    uf character varying(2),
    telefone1 character varying(15),
    telefone2 character varying(15),
    email character varying(50),
    site character varying(50),
    data_cadastro date NOT NULL,
    responsavel character varying(100),
    cargo character varying(50),
    documento character varying(50),
    escopo character varying(600),
    jornada character varying(300),
    situacao character varying(1),
    est_masc integer,
    est_fem integer,
    enderecocob character varying(100),
    numerocob character varying(10),
    complementocob character varying(100),
    bairrocob character varying(50),
    cepcob character varying(8),
    ufcob character varying(2),
    telefonecob character varying(15),
    id_medico bigint,
    telefone2cob character varying(15),
    vip boolean DEFAULT false NOT NULL,
    telefone_contato character varying(15),
    usa_contrato boolean DEFAULT false NOT NULL,
    id_cliente_matriz bigint,
    id_cliente_credenciado bigint,
    particular boolean DEFAULT false NOT NULL,
    unidade boolean DEFAULT false NOT NULL,
    credenciada boolean DEFAULT false NOT NULL,
    prestador boolean DEFAULT false NOT NULL,
    id_cidade_ibge bigint NOT NULL,
    id_cidade_ibge_cobranca bigint,
    destaca_iss boolean DEFAULT true NOT NULL,
    gera_cobranca_valor_liquido boolean DEFAULT false NOT NULL,
    aliquota_iss numeric(8,2) DEFAULT 0.00 NOT NULL,
    codigo_cnes character varying(7),
    usa_centro_custo boolean DEFAULT false NOT NULL,
    bloqueado boolean DEFAULT false NOT NULL,
    credenciadora boolean DEFAULT false NOT NULL,
    fisica boolean DEFAULT false NOT NULL,
    usa_po boolean DEFAULT false NOT NULL,
    simples_nacional boolean DEFAULT false NOT NULL
);


ALTER TABLE public.seg_cliente OWNER TO postgres;

--
-- TOC entry 224 (class 1259 OID 43118)
-- Name: seg_cliente_arquivo; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.seg_cliente_arquivo (
    id_cliente_arquivo bigint NOT NULL,
    id_arquivo bigint NOT NULL,
    id_cliente bigint NOT NULL,
    data_criacao timestamp without time zone NOT NULL,
    data_finalizacao timestamp without time zone,
    situacao character varying NOT NULL
);


ALTER TABLE public.seg_cliente_arquivo OWNER TO postgres;

--
-- TOC entry 225 (class 1259 OID 43124)
-- Name: seg_cliente_arquivo_id_cliente_arquivo_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.seg_cliente_arquivo_id_cliente_arquivo_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.seg_cliente_arquivo_id_cliente_arquivo_seq OWNER TO postgres;

--
-- TOC entry 4665 (class 0 OID 0)
-- Dependencies: 225
-- Name: seg_cliente_arquivo_id_cliente_arquivo_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.seg_cliente_arquivo_id_cliente_arquivo_seq OWNED BY public.seg_cliente_arquivo.id_cliente_arquivo;


--
-- TOC entry 226 (class 1259 OID 43126)
-- Name: seg_cliente_funcao; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.seg_cliente_funcao (
    id_seg_cliente_funcao bigint NOT NULL,
    id_cliente bigint NOT NULL,
    id_funcao bigint NOT NULL,
    situacao character varying(1) NOT NULL,
    data_cadastro date NOT NULL,
    data_desligamento date
);


ALTER TABLE public.seg_cliente_funcao OWNER TO postgres;

--
-- TOC entry 227 (class 1259 OID 43129)
-- Name: seg_cliente_funcao_exame; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.seg_cliente_funcao_exame (
    id_cliente_funcao_exame bigint NOT NULL,
    id_cliente_funcao bigint NOT NULL,
    id_exame bigint NOT NULL,
    situacao character varying(1) NOT NULL,
    idade_exame integer NOT NULL
);


ALTER TABLE public.seg_cliente_funcao_exame OWNER TO postgres;

--
-- TOC entry 228 (class 1259 OID 43132)
-- Name: seg_cliente_funcao_exame_aso; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.seg_cliente_funcao_exame_aso (
    id_cliente_funcao_exame_aso bigint NOT NULL,
    id_sala_exame bigint,
    id_aso bigint NOT NULL,
    id_cliente_funcao_exame bigint NOT NULL,
    data_exame date NOT NULL,
    atendido boolean DEFAULT false,
    finalizado boolean DEFAULT false,
    login character varying(15),
    devolvido boolean DEFAULT false,
    data_atendido timestamp without time zone,
    data_devolvido timestamp without time zone,
    data_finalizado timestamp without time zone,
    id_usuario bigint,
    cancelado boolean DEFAULT false,
    data_cancelado timestamp without time zone,
    resultado character varying(1),
    observacao_resultado text,
    transcrito boolean DEFAULT false NOT NULL,
    id_prestador bigint,
    id_protocolo bigint,
    id_medico bigint,
    entrega boolean DEFAULT false NOT NULL,
    data_validade date,
    imprime_aso boolean DEFAULT true NOT NULL,
    movimento_sistema boolean DEFAULT false NOT NULL
);


ALTER TABLE public.seg_cliente_funcao_exame_aso OWNER TO postgres;

--
-- TOC entry 229 (class 1259 OID 43144)
-- Name: seg_cliente_funcao_exame_aso_id_cliente_funcao_exame_aso_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.seg_cliente_funcao_exame_aso_id_cliente_funcao_exame_aso_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.seg_cliente_funcao_exame_aso_id_cliente_funcao_exame_aso_seq OWNER TO postgres;

--
-- TOC entry 4666 (class 0 OID 0)
-- Dependencies: 229
-- Name: seg_cliente_funcao_exame_aso_id_cliente_funcao_exame_aso_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.seg_cliente_funcao_exame_aso_id_cliente_funcao_exame_aso_seq OWNED BY public.seg_cliente_funcao_exame_aso.id_cliente_funcao_exame_aso;


--
-- TOC entry 230 (class 1259 OID 43146)
-- Name: seg_cliente_funcao_exame_id_cliente_funcao_exame_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.seg_cliente_funcao_exame_id_cliente_funcao_exame_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.seg_cliente_funcao_exame_id_cliente_funcao_exame_seq OWNER TO postgres;

--
-- TOC entry 4667 (class 0 OID 0)
-- Dependencies: 230
-- Name: seg_cliente_funcao_exame_id_cliente_funcao_exame_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.seg_cliente_funcao_exame_id_cliente_funcao_exame_seq OWNED BY public.seg_cliente_funcao_exame.id_cliente_funcao_exame;


--
-- TOC entry 231 (class 1259 OID 43148)
-- Name: seg_cliente_funcao_exame_periodicidade; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.seg_cliente_funcao_exame_periodicidade (
    id_periodicidade bigint NOT NULL,
    id_cliente_funcao_exame bigint NOT NULL,
    situacao character varying(1) NOT NULL,
    periodo_vencimento integer
);


ALTER TABLE public.seg_cliente_funcao_exame_periodicidade OWNER TO postgres;

--
-- TOC entry 232 (class 1259 OID 43151)
-- Name: seg_cliente_funcao_funcionario; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.seg_cliente_funcao_funcionario (
    id_cliente_funcao_funcionario bigint NOT NULL,
    id_seg_cliente_funcao bigint NOT NULL,
    id_funcionario bigint NOT NULL,
    situacao character varying(1) NOT NULL,
    data_cadastro date NOT NULL,
    data_desligamento date,
    matricula character varying(30),
    vip boolean DEFAULT false NOT NULL,
    br_pdh character varying(3),
    regime_revezamento character varying(15),
    estagiario boolean DEFAULT false NOT NULL,
    vinculo boolean DEFAULT true NOT NULL
);


ALTER TABLE public.seg_cliente_funcao_funcionario OWNER TO postgres;

--
-- TOC entry 233 (class 1259 OID 43157)
-- Name: seg_cliente_funcao_funcionari_id_cliente_funcao_funcionario_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.seg_cliente_funcao_funcionari_id_cliente_funcao_funcionario_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.seg_cliente_funcao_funcionari_id_cliente_funcao_funcionario_seq OWNER TO postgres;

--
-- TOC entry 4668 (class 0 OID 0)
-- Dependencies: 233
-- Name: seg_cliente_funcao_funcionari_id_cliente_funcao_funcionario_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.seg_cliente_funcao_funcionari_id_cliente_funcao_funcionario_seq OWNED BY public.seg_cliente_funcao_funcionario.id_cliente_funcao_funcionario;


--
-- TOC entry 234 (class 1259 OID 43159)
-- Name: seg_cliente_funcao_id_seg_cliente_funcao_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.seg_cliente_funcao_id_seg_cliente_funcao_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.seg_cliente_funcao_id_seg_cliente_funcao_seq OWNER TO postgres;

--
-- TOC entry 4669 (class 0 OID 0)
-- Dependencies: 234
-- Name: seg_cliente_funcao_id_seg_cliente_funcao_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.seg_cliente_funcao_id_seg_cliente_funcao_seq OWNED BY public.seg_cliente_funcao.id_seg_cliente_funcao;


--
-- TOC entry 235 (class 1259 OID 43161)
-- Name: seg_cliente_funcionario; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.seg_cliente_funcionario (
    id bigint NOT NULL,
    id_funcionario bigint NOT NULL,
    id_cliente bigint NOT NULL,
    br_pdh character varying(3),
    data_admissao date,
    data_demissao date,
    regime_revezamento character varying(15),
    matricula character varying(30),
    estagiario boolean DEFAULT false NOT NULL,
    vinculo boolean DEFAULT true NOT NULL
);


ALTER TABLE public.seg_cliente_funcionario OWNER TO postgres;

--
-- TOC entry 236 (class 1259 OID 43166)
-- Name: seg_cliente_funcionario_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.seg_cliente_funcionario_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.seg_cliente_funcionario_id_seq OWNER TO postgres;

--
-- TOC entry 4670 (class 0 OID 0)
-- Dependencies: 236
-- Name: seg_cliente_funcionario_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.seg_cliente_funcionario_id_seq OWNED BY public.seg_cliente_funcionario.id;


--
-- TOC entry 237 (class 1259 OID 43168)
-- Name: seg_cliente_id_cliente_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.seg_cliente_id_cliente_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.seg_cliente_id_cliente_seq OWNER TO postgres;

--
-- TOC entry 4671 (class 0 OID 0)
-- Dependencies: 237
-- Name: seg_cliente_id_cliente_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.seg_cliente_id_cliente_seq OWNED BY public.seg_cliente.id_cliente;


--
-- TOC entry 238 (class 1259 OID 43170)
-- Name: seg_cliente_proposta; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.seg_cliente_proposta (
    id_cliente_proposta bigint NOT NULL,
    id_cidade_ibge bigint,
    razao_social character varying(100) NOT NULL,
    nome_fantasia character varying(100),
    cnpj character varying(15),
    inscricao character varying(20),
    endereco character varying(100),
    numero character varying(10),
    complemento character varying(100),
    bairro character varying(50),
    cep character varying(8),
    uf character varying(2),
    telefone character varying(15) NOT NULL,
    celular character varying(15),
    data_cadastro date NOT NULL,
    contato character varying(100),
    email character varying(50)
);


ALTER TABLE public.seg_cliente_proposta OWNER TO postgres;

--
-- TOC entry 239 (class 1259 OID 43176)
-- Name: seg_cliente_proposta_id_cliente_proposta_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.seg_cliente_proposta_id_cliente_proposta_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.seg_cliente_proposta_id_cliente_proposta_seq OWNER TO postgres;

--
-- TOC entry 4672 (class 0 OID 0)
-- Dependencies: 239
-- Name: seg_cliente_proposta_id_cliente_proposta_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.seg_cliente_proposta_id_cliente_proposta_seq OWNED BY public.seg_cliente_proposta.id_cliente_proposta;


--
-- TOC entry 240 (class 1259 OID 43178)
-- Name: seg_cnae; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.seg_cnae (
    id_cnae bigint NOT NULL,
    cod_cnae character varying(7) NOT NULL,
    atividade character varying(400) NOT NULL,
    graurisco character varying(1),
    grupo character varying(5),
    situacao character varying(1) DEFAULT 'A'::character varying NOT NULL
);


ALTER TABLE public.seg_cnae OWNER TO postgres;

--
-- TOC entry 241 (class 1259 OID 43182)
-- Name: seg_cnae_estudo; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.seg_cnae_estudo (
    id_cnae_estudo bigint NOT NULL,
    id_estudo bigint NOT NULL,
    id_cnae bigint NOT NULL,
    flag_cliente boolean DEFAULT false
);


ALTER TABLE public.seg_cnae_estudo OWNER TO postgres;

--
-- TOC entry 242 (class 1259 OID 43186)
-- Name: seg_cnae_estudo_id_cnae_estudo_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.seg_cnae_estudo_id_cnae_estudo_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.seg_cnae_estudo_id_cnae_estudo_seq OWNER TO postgres;

--
-- TOC entry 4673 (class 0 OID 0)
-- Dependencies: 242
-- Name: seg_cnae_estudo_id_cnae_estudo_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.seg_cnae_estudo_id_cnae_estudo_seq OWNED BY public.seg_cnae_estudo.id_cnae_estudo;


--
-- TOC entry 243 (class 1259 OID 43188)
-- Name: seg_cnae_id_cnae_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.seg_cnae_id_cnae_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.seg_cnae_id_cnae_seq OWNER TO postgres;

--
-- TOC entry 4674 (class 0 OID 0)
-- Dependencies: 243
-- Name: seg_cnae_id_cnae_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.seg_cnae_id_cnae_seq OWNED BY public.seg_cnae.id_cnae;


--
-- TOC entry 244 (class 1259 OID 43190)
-- Name: seg_comentario_funcao; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.seg_comentario_funcao (
    id_comentario_funcao bigint NOT NULL,
    id_funcao bigint NOT NULL,
    atividade text NOT NULL,
    situacao character varying(1) NOT NULL
);


ALTER TABLE public.seg_comentario_funcao OWNER TO postgres;

--
-- TOC entry 245 (class 1259 OID 43196)
-- Name: seg_comentario_funcao_id_comentario_funcao_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.seg_comentario_funcao_id_comentario_funcao_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.seg_comentario_funcao_id_comentario_funcao_seq OWNER TO postgres;

--
-- TOC entry 4675 (class 0 OID 0)
-- Dependencies: 245
-- Name: seg_comentario_funcao_id_comentario_funcao_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.seg_comentario_funcao_id_comentario_funcao_seq OWNED BY public.seg_comentario_funcao.id_comentario_funcao;


--
-- TOC entry 246 (class 1259 OID 43198)
-- Name: seg_configuracao; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.seg_configuracao (
    id_configuracao bigint NOT NULL,
    descricao character varying NOT NULL,
    valor character varying NOT NULL
);


ALTER TABLE public.seg_configuracao OWNER TO postgres;

--
-- TOC entry 247 (class 1259 OID 43204)
-- Name: seg_configuracao_id_configuracao_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.seg_configuracao_id_configuracao_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.seg_configuracao_id_configuracao_seq OWNER TO postgres;

--
-- TOC entry 4676 (class 0 OID 0)
-- Dependencies: 247
-- Name: seg_configuracao_id_configuracao_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.seg_configuracao_id_configuracao_seq OWNED BY public.seg_configuracao.id_configuracao;


--
-- TOC entry 248 (class 1259 OID 43206)
-- Name: seg_conta; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.seg_conta (
    id_conta bigint NOT NULL,
    id_banco bigint NOT NULL,
    nome character varying(50) NOT NULL,
    agencia_numero character varying(5) NOT NULL,
    agencia_digito character varying(1),
    conta_numero character varying(12) NOT NULL,
    conta_digito character varying(2),
    proximo_numero bigint,
    numero_remessa character varying(7),
    carteira character varying(3),
    convenio character varying(16),
    variacao_carteira character varying(3),
    codigo_cedente_banco character varying(20),
    registro boolean NOT NULL,
    situacao character varying(1) NOT NULL,
    privado boolean NOT NULL
);


ALTER TABLE public.seg_conta OWNER TO postgres;

--
-- TOC entry 249 (class 1259 OID 43209)
-- Name: seg_conta_id_conta_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.seg_conta_id_conta_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.seg_conta_id_conta_seq OWNER TO postgres;

--
-- TOC entry 4677 (class 0 OID 0)
-- Dependencies: 249
-- Name: seg_conta_id_conta_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.seg_conta_id_conta_seq OWNED BY public.seg_conta.id_conta;


--
-- TOC entry 250 (class 1259 OID 43211)
-- Name: seg_contato; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.seg_contato (
    id bigint NOT NULL,
    id_cidade bigint,
    id_cliente bigint NOT NULL,
    id_tipo_contato bigint NOT NULL,
    nome character varying(100) NOT NULL,
    cpf character varying(14),
    rg character varying(15),
    data_nascimento date,
    endereco character varying(100),
    numero character varying(15),
    complemento character varying(100),
    bairro character varying(100),
    uf character varying(2),
    cep character varying(8),
    telefone character varying(15),
    celular character varying(15),
    pis_pasep character varying(11),
    email character varying(100),
    responsavel_contrato boolean DEFAULT false NOT NULL,
    responsavel_estudo boolean DEFAULT false NOT NULL
);


ALTER TABLE public.seg_contato OWNER TO postgres;

--
-- TOC entry 251 (class 1259 OID 43219)
-- Name: seg_contato_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.seg_contato_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.seg_contato_id_seq OWNER TO postgres;

--
-- TOC entry 4678 (class 0 OID 0)
-- Dependencies: 251
-- Name: seg_contato_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.seg_contato_id_seq OWNED BY public.seg_contato.id;


--
-- TOC entry 252 (class 1259 OID 43221)
-- Name: seg_contrato; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.seg_contrato (
    id_contrato bigint NOT NULL,
    codigo_contrato character varying(14),
    id_cliente bigint NOT NULL,
    data_elaboracao timestamp without time zone,
    data_fim timestamp without time zone,
    data_inicio timestamp without time zone,
    situacao character varying(1),
    observacao text,
    id_usuario_criou bigint,
    id_usuario_finalizou bigint,
    id_usuario_cancelou bigint,
    motivo_cancelamento text,
    contrato_automatico boolean DEFAULT false NOT NULL,
    data_cancelamento date,
    id_contrato_pai bigint,
    bloqueia_inadimplencia boolean DEFAULT false NOT NULL,
    gera_mensalidade boolean DEFAULT false NOT NULL,
    id_cliente_prestador bigint,
    gera_numero_vidas boolean DEFAULT false NOT NULL,
    data_cobranca integer,
    cobranca_automatica boolean DEFAULT false NOT NULL,
    valor_mensalidade numeric(8,2) DEFAULT 0.00 NOT NULL,
    valor_numero_vidas numeric(8,2) DEFAULT 0.00 NOT NULL,
    dias_bloqueio integer,
    id_contrato_raiz bigint,
    id_cliente_proposta bigint,
    proposta boolean DEFAULT false NOT NULL,
    proposta_forma_pagamento text,
    id_usuario_encerrou bigint,
    data_encerramento timestamp without time zone,
    motivo_encerramento text,
    id_empresa bigint
);


ALTER TABLE public.seg_contrato OWNER TO postgres;

--
-- TOC entry 253 (class 1259 OID 43235)
-- Name: seg_contrato_exame; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.seg_contrato_exame (
    id_contrato_exame bigint NOT NULL,
    id_exame bigint NOT NULL,
    id_contrato bigint NOT NULL,
    preco_contrato numeric(8,2) NOT NULL,
    usuario character varying(15) NOT NULL,
    custo_contrato numeric(8,2) NOT NULL,
    data_inclusao timestamp without time zone,
    situacao character varying(1) DEFAULT 'A'::character varying NOT NULL,
    altera boolean DEFAULT true NOT NULL
);


ALTER TABLE public.seg_contrato_exame OWNER TO postgres;

--
-- TOC entry 254 (class 1259 OID 43240)
-- Name: seg_contrato_exame_id_contrato_exame_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.seg_contrato_exame_id_contrato_exame_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.seg_contrato_exame_id_contrato_exame_seq OWNER TO postgres;

--
-- TOC entry 4679 (class 0 OID 0)
-- Dependencies: 254
-- Name: seg_contrato_exame_id_contrato_exame_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.seg_contrato_exame_id_contrato_exame_seq OWNED BY public.seg_contrato_exame.id_contrato_exame;


--
-- TOC entry 255 (class 1259 OID 43242)
-- Name: seg_contrato_id_contrato_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.seg_contrato_id_contrato_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.seg_contrato_id_contrato_seq OWNER TO postgres;

--
-- TOC entry 4680 (class 0 OID 0)
-- Dependencies: 255
-- Name: seg_contrato_id_contrato_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.seg_contrato_id_contrato_seq OWNED BY public.seg_contrato.id_contrato;


--
-- TOC entry 256 (class 1259 OID 43244)
-- Name: seg_correcao_estudo; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.seg_correcao_estudo (
    id_correcao_estudo bigint NOT NULL,
    id_usuario bigint NOT NULL,
    id_estudo bigint NOT NULL,
    historico text NOT NULL,
    data_correcao timestamp without time zone NOT NULL
);


ALTER TABLE public.seg_correcao_estudo OWNER TO postgres;

--
-- TOC entry 257 (class 1259 OID 43250)
-- Name: seg_correcao_estudo_id_correcao_estudo_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.seg_correcao_estudo_id_correcao_estudo_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.seg_correcao_estudo_id_correcao_estudo_seq OWNER TO postgres;

--
-- TOC entry 4681 (class 0 OID 0)
-- Dependencies: 257
-- Name: seg_correcao_estudo_id_correcao_estudo_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.seg_correcao_estudo_id_correcao_estudo_seq OWNED BY public.seg_correcao_estudo.id_correcao_estudo;


--
-- TOC entry 258 (class 1259 OID 43252)
-- Name: seg_crc; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.seg_crc (
    id_crc bigint NOT NULL,
    id_forma bigint NOT NULL,
    id_usuario_cancela bigint,
    id_usuario_estorna bigint,
    id_usuario_desdobra bigint,
    id_usuario_criador bigint NOT NULL,
    id_conta bigint,
    id_nfe bigint NOT NULL,
    valor numeric(8,2) NOT NULL,
    data_emissao date NOT NULL,
    data_vencimento date NOT NULL,
    data_pagamento date,
    situacao character varying(1) NOT NULL,
    numero_parcela integer NOT NULL,
    numero_documento character varying(20) NOT NULL,
    data_alteracao date,
    observacao_cancelamento text,
    id_crc_desdobramento bigint,
    observacao_cobranca text,
    observacao_baixa text,
    data_baixa date,
    id_usuario_baixa bigint,
    observacao_estorno_baixa text,
    data_estorno date,
    data_cancelamento date,
    id_usuario_prorrogacao bigint,
    observacao_prorrogacao text,
    valor_ajustado numeric(8,2) NOT NULL,
    valor_baixado numeric(8,2),
    nosso_numero character varying(20),
    valor_liquido numeric(8,2) DEFAULT 0.00 NOT NULL
);


ALTER TABLE public.seg_crc OWNER TO postgres;

--
-- TOC entry 259 (class 1259 OID 43259)
-- Name: seg_crc_id_crc_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.seg_crc_id_crc_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.seg_crc_id_crc_seq OWNER TO postgres;

--
-- TOC entry 4682 (class 0 OID 0)
-- Dependencies: 259
-- Name: seg_crc_id_crc_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.seg_crc_id_crc_seq OWNED BY public.seg_crc.id_crc;


--
-- TOC entry 260 (class 1259 OID 43261)
-- Name: seg_cronograma; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.seg_cronograma (
    id_cronograma bigint NOT NULL,
    descricao character varying(100)
);


ALTER TABLE public.seg_cronograma OWNER TO postgres;

--
-- TOC entry 261 (class 1259 OID 43264)
-- Name: seg_cronograma_id_cronograma_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.seg_cronograma_id_cronograma_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.seg_cronograma_id_cronograma_seq OWNER TO postgres;

--
-- TOC entry 4683 (class 0 OID 0)
-- Dependencies: 261
-- Name: seg_cronograma_id_cronograma_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.seg_cronograma_id_cronograma_seq OWNED BY public.seg_cronograma.id_cronograma;


--
-- TOC entry 262 (class 1259 OID 43266)
-- Name: seg_documento; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.seg_documento (
    id_documento bigint NOT NULL,
    descricao character varying(200) NOT NULL,
    situacao character varying(1) DEFAULT 'A'::character varying NOT NULL
);


ALTER TABLE public.seg_documento OWNER TO postgres;

--
-- TOC entry 263 (class 1259 OID 43270)
-- Name: seg_documento_estudo; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.seg_documento_estudo (
    id_documento_estudo bigint NOT NULL,
    id_documento bigint NOT NULL,
    id_estudo bigint NOT NULL
);


ALTER TABLE public.seg_documento_estudo OWNER TO postgres;

--
-- TOC entry 264 (class 1259 OID 43273)
-- Name: seg_documento_estudo_id_documento_estudo_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.seg_documento_estudo_id_documento_estudo_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.seg_documento_estudo_id_documento_estudo_seq OWNER TO postgres;

--
-- TOC entry 4684 (class 0 OID 0)
-- Dependencies: 264
-- Name: seg_documento_estudo_id_documento_estudo_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.seg_documento_estudo_id_documento_estudo_seq OWNED BY public.seg_documento_estudo.id_documento_estudo;


--
-- TOC entry 265 (class 1259 OID 43275)
-- Name: seg_documento_id_documento_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.seg_documento_id_documento_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.seg_documento_id_documento_seq OWNER TO postgres;

--
-- TOC entry 4685 (class 0 OID 0)
-- Dependencies: 265
-- Name: seg_documento_id_documento_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.seg_documento_id_documento_seq OWNED BY public.seg_documento.id_documento;


--
-- TOC entry 266 (class 1259 OID 43277)
-- Name: seg_documento_protocolo; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.seg_documento_protocolo (
    id_documento_protocolo bigint NOT NULL,
    id_aso bigint NOT NULL,
    id_documento bigint NOT NULL,
    id_protocolo bigint,
    data_gravacao date NOT NULL
);


ALTER TABLE public.seg_documento_protocolo OWNER TO postgres;

--
-- TOC entry 267 (class 1259 OID 43280)
-- Name: seg_documento_protocolo_id_documento_protocolo_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.seg_documento_protocolo_id_documento_protocolo_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.seg_documento_protocolo_id_documento_protocolo_seq OWNER TO postgres;

--
-- TOC entry 4686 (class 0 OID 0)
-- Dependencies: 267
-- Name: seg_documento_protocolo_id_documento_protocolo_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.seg_documento_protocolo_id_documento_protocolo_seq OWNED BY public.seg_documento_protocolo.id_documento_protocolo;


--
-- TOC entry 268 (class 1259 OID 43282)
-- Name: seg_dominio; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.seg_dominio (
    id_dominio bigint NOT NULL,
    descricao character varying(100) NOT NULL,
    situacao character varying(1) NOT NULL
);


ALTER TABLE public.seg_dominio OWNER TO postgres;

--
-- TOC entry 269 (class 1259 OID 43285)
-- Name: seg_dominio_acao; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.seg_dominio_acao (
    id_dominio bigint NOT NULL,
    id_acao bigint NOT NULL
);


ALTER TABLE public.seg_dominio_acao OWNER TO postgres;

--
-- TOC entry 270 (class 1259 OID 43288)
-- Name: seg_dominio_id_dominio_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.seg_dominio_id_dominio_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.seg_dominio_id_dominio_seq OWNER TO postgres;

--
-- TOC entry 4687 (class 0 OID 0)
-- Dependencies: 270
-- Name: seg_dominio_id_dominio_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.seg_dominio_id_dominio_seq OWNED BY public.seg_dominio.id_dominio;


--
-- TOC entry 271 (class 1259 OID 43290)
-- Name: seg_email; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.seg_email (
    id_email bigint NOT NULL,
    id_usuario bigint NOT NULL,
    id_prontuario bigint,
    para character varying NOT NULL,
    assunto character varying NOT NULL,
    mensagem text NOT NULL,
    anexo character varying,
    prioridade character varying NOT NULL,
    data_envio timestamp without time zone NOT NULL
);


ALTER TABLE public.seg_email OWNER TO postgres;

--
-- TOC entry 272 (class 1259 OID 43296)
-- Name: seg_email_id_email_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.seg_email_id_email_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.seg_email_id_email_seq OWNER TO postgres;

--
-- TOC entry 4688 (class 0 OID 0)
-- Dependencies: 272
-- Name: seg_email_id_email_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.seg_email_id_email_seq OWNED BY public.seg_email.id_email;


--
-- TOC entry 273 (class 1259 OID 43298)
-- Name: seg_empresa; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.seg_empresa (
    id_empresa bigint NOT NULL,
    razao_social character varying(100),
    fantasia character varying(100),
    cnpj character varying(14) NOT NULL,
    inscricao character varying(20) NOT NULL,
    endereco character varying(100),
    numero character varying(10) NOT NULL,
    complemento character varying(100),
    bairro character varying(50),
    cep character varying(8),
    uf character varying(2),
    telefone1 character varying(15),
    telefone2 character varying(15),
    email character varying(50),
    site character varying(50),
    data_cadastro date NOT NULL,
    situacao character varying(1) NOT NULL,
    logo bytea,
    mimetype character varying(30),
    id_cidade_ibge bigint NOT NULL,
    simples_nacional boolean DEFAULT false NOT NULL,
    codigo_cnes character varying(7),
    id_empresa_matriz bigint,
    valor_minimo_imposto_federal numeric(8,2) DEFAULT 0.00 NOT NULL,
    valor_minimo_ir numeric(8,2) DEFAULT 0.00 NOT NULL,
    aliquota_pis numeric(8,2) DEFAULT 0.00 NOT NULL,
    aliquota_cofins numeric(8,2) DEFAULT 0.00 NOT NULL,
    aliquota_ir numeric(8,2) DEFAULT 0.00 NOT NULL,
    aliquota_csll numeric(8,2) DEFAULT 0.00 NOT NULL,
    aliquota_iss numeric(8,2) DEFAULT 0.00 NOT NULL
);


ALTER TABLE public.seg_empresa OWNER TO postgres;

--
-- TOC entry 274 (class 1259 OID 43312)
-- Name: seg_empresa_id_empresa_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.seg_empresa_id_empresa_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.seg_empresa_id_empresa_seq OWNER TO postgres;

--
-- TOC entry 4689 (class 0 OID 0)
-- Dependencies: 274
-- Name: seg_empresa_id_empresa_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.seg_empresa_id_empresa_seq OWNED BY public.seg_empresa.id_empresa;


--
-- TOC entry 275 (class 1259 OID 43314)
-- Name: seg_endemia; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.seg_endemia (
    id_endemia bigint NOT NULL,
    doenca character varying(100),
    agente character varying(100),
    reservatorio character varying(100),
    modo_transmissao text,
    periodo_incubacao character varying(100),
    medidas_preventivas text
);


ALTER TABLE public.seg_endemia OWNER TO postgres;

--
-- TOC entry 276 (class 1259 OID 43320)
-- Name: seg_endemia_id_endemia_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.seg_endemia_id_endemia_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.seg_endemia_id_endemia_seq OWNER TO postgres;

--
-- TOC entry 4690 (class 0 OID 0)
-- Dependencies: 276
-- Name: seg_endemia_id_endemia_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.seg_endemia_id_endemia_seq OWNED BY public.seg_endemia.id_endemia;


--
-- TOC entry 277 (class 1259 OID 43322)
-- Name: seg_epi; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.seg_epi (
    id_epi bigint NOT NULL,
    descricao character varying(200) NOT NULL,
    finalidade character varying(150),
    ca character varying(50) NOT NULL,
    situacao character varying(1)
);


ALTER TABLE public.seg_epi OWNER TO postgres;

--
-- TOC entry 278 (class 1259 OID 43325)
-- Name: seg_epi_id_epi_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.seg_epi_id_epi_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.seg_epi_id_epi_seq OWNER TO postgres;

--
-- TOC entry 4691 (class 0 OID 0)
-- Dependencies: 278
-- Name: seg_epi_id_epi_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.seg_epi_id_epi_seq OWNED BY public.seg_epi.id_epi;


--
-- TOC entry 279 (class 1259 OID 43327)
-- Name: seg_epi_monitoramento; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.seg_epi_monitoramento (
    id_epi_monitoramento bigint NOT NULL,
    id_epi bigint NOT NULL,
    id_monitoramento_ghefonteagente bigint NOT NULL
);


ALTER TABLE public.seg_epi_monitoramento OWNER TO postgres;

--
-- TOC entry 280 (class 1259 OID 43330)
-- Name: seg_epi_monitoramento_id_epi_monitoramento_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.seg_epi_monitoramento_id_epi_monitoramento_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.seg_epi_monitoramento_id_epi_monitoramento_seq OWNER TO postgres;

--
-- TOC entry 4692 (class 0 OID 0)
-- Dependencies: 280
-- Name: seg_epi_monitoramento_id_epi_monitoramento_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.seg_epi_monitoramento_id_epi_monitoramento_seq OWNED BY public.seg_epi_monitoramento.id_epi_monitoramento;


--
-- TOC entry 281 (class 1259 OID 43332)
-- Name: seg_esocial_24; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.seg_esocial_24 (
    id bigint NOT NULL,
    codigo character varying(10) NOT NULL,
    descricao text NOT NULL
);


ALTER TABLE public.seg_esocial_24 OWNER TO postgres;

--
-- TOC entry 282 (class 1259 OID 43338)
-- Name: seg_esocial_24_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.seg_esocial_24_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.seg_esocial_24_id_seq OWNER TO postgres;

--
-- TOC entry 4693 (class 0 OID 0)
-- Dependencies: 282
-- Name: seg_esocial_24_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.seg_esocial_24_id_seq OWNED BY public.seg_esocial_24.id;


--
-- TOC entry 283 (class 1259 OID 43340)
-- Name: seg_esocial_lote_id_esocial_lote_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.seg_esocial_lote_id_esocial_lote_seq
    START WITH 11
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.seg_esocial_lote_id_esocial_lote_seq OWNER TO postgres;

--
-- TOC entry 284 (class 1259 OID 43342)
-- Name: seg_esocial_lote; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.seg_esocial_lote (
    id_esocial_lote bigint DEFAULT nextval('public.seg_esocial_lote_id_esocial_lote_seq'::regclass) NOT NULL,
    id_cliente bigint NOT NULL,
    tipo character varying(4) NOT NULL,
    data_criacao timestamp without time zone NOT NULL,
    codigo character varying(14),
    periodo date
);


ALTER TABLE public.seg_esocial_lote OWNER TO postgres;

--
-- TOC entry 285 (class 1259 OID 43346)
-- Name: seg_esocial_lote_aso_id_seg_esocial_lote_aso_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.seg_esocial_lote_aso_id_seg_esocial_lote_aso_seq
    START WITH 12
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.seg_esocial_lote_aso_id_seg_esocial_lote_aso_seq OWNER TO postgres;

--
-- TOC entry 286 (class 1259 OID 43348)
-- Name: seg_esocial_lote_aso; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.seg_esocial_lote_aso (
    id_seg_esocial_lote_aso bigint DEFAULT nextval('public.seg_esocial_lote_aso_id_seg_esocial_lote_aso_seq'::regclass) NOT NULL,
    id_esocial_lote bigint NOT NULL,
    id_aso bigint NOT NULL,
    situacao character varying(1) DEFAULT 'E'::character varying NOT NULL,
    nrecibo character varying(25),
    data_cancelamento timestamp(0) without time zone,
    usuario_cancelou character varying(15)
);


ALTER TABLE public.seg_esocial_lote_aso OWNER TO postgres;

--
-- TOC entry 287 (class 1259 OID 43353)
-- Name: seg_esocial_monitoramento; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.seg_esocial_monitoramento (
    id_esocial_monitoramento bigint NOT NULL,
    id_esocial_lote bigint NOT NULL,
    id_monitoramento bigint NOT NULL,
    situacao character varying(1) DEFAULT 'E'::character varying NOT NULL,
    nrecibo character varying(25),
    data_cancelamento timestamp without time zone,
    usuario_cancelou character varying(15)
);


ALTER TABLE public.seg_esocial_monitoramento OWNER TO postgres;

--
-- TOC entry 288 (class 1259 OID 43357)
-- Name: seg_esocial_monitoramento_id_esocial_monitoramento_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.seg_esocial_monitoramento_id_esocial_monitoramento_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.seg_esocial_monitoramento_id_esocial_monitoramento_seq OWNER TO postgres;

--
-- TOC entry 4694 (class 0 OID 0)
-- Dependencies: 288
-- Name: seg_esocial_monitoramento_id_esocial_monitoramento_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.seg_esocial_monitoramento_id_esocial_monitoramento_seq OWNED BY public.seg_esocial_monitoramento.id_esocial_monitoramento;


--
-- TOC entry 289 (class 1259 OID 43359)
-- Name: seg_estimativa; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.seg_estimativa (
    id_estimativa bigint NOT NULL,
    descricao character varying(100) NOT NULL,
    situacao character varying(1) NOT NULL
);


ALTER TABLE public.seg_estimativa OWNER TO postgres;

--
-- TOC entry 290 (class 1259 OID 43362)
-- Name: seg_estimativa_estudo; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.seg_estimativa_estudo (
    id_estimativa_estudo bigint NOT NULL,
    id_estudo bigint NOT NULL,
    id_estimativa bigint NOT NULL,
    quantidade integer NOT NULL
);


ALTER TABLE public.seg_estimativa_estudo OWNER TO postgres;

--
-- TOC entry 291 (class 1259 OID 43365)
-- Name: seg_estimativa_estudo_id_estimativa_estudo_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.seg_estimativa_estudo_id_estimativa_estudo_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.seg_estimativa_estudo_id_estimativa_estudo_seq OWNER TO postgres;

--
-- TOC entry 4695 (class 0 OID 0)
-- Dependencies: 291
-- Name: seg_estimativa_estudo_id_estimativa_estudo_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.seg_estimativa_estudo_id_estimativa_estudo_seq OWNED BY public.seg_estimativa_estudo.id_estimativa_estudo;


--
-- TOC entry 292 (class 1259 OID 43367)
-- Name: seg_estimativa_id_estimativa_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.seg_estimativa_id_estimativa_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.seg_estimativa_id_estimativa_seq OWNER TO postgres;

--
-- TOC entry 4696 (class 0 OID 0)
-- Dependencies: 292
-- Name: seg_estimativa_id_estimativa_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.seg_estimativa_id_estimativa_seq OWNED BY public.seg_estimativa.id_estimativa;


--
-- TOC entry 293 (class 1259 OID 43369)
-- Name: seg_estudo; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.seg_estudo (
    id_estudo bigint NOT NULL,
    id_relatorio bigint,
    cod_estudo character varying(14) NOT NULL,
    id_cronograma bigint,
    id_seg_rep_cli bigint,
    id_engenheiro bigint,
    id_tecno bigint,
    id_cliente_contratado bigint,
    data_criacao date NOT NULL,
    data_vencimento date,
    data_fechamento timestamp without time zone,
    situacao character varying(1) NOT NULL,
    estudo boolean NOT NULL,
    data_fim_contrato character varying(15),
    obra character varying(255),
    local_obra character varying(255),
    endereco character varying(100),
    numero character varying(10),
    complemento character varying(100),
    bairro character varying(50),
    cidade character varying(50),
    uf character varying(2),
    num_contrato character varying(15),
    grau_risco character varying(1),
    cli_razao_social character varying(100),
    cli_cnpj character varying(14),
    cli_inscricao character varying(20),
    cli_est_fem integer,
    cli_est_masc integer,
    cli_jornada character varying(300),
    cli_endereco character varying(100),
    cli_numero character varying(10),
    cli_complemento character varying(100),
    cli_bairro character varying(50),
    cli_cidade character varying(50),
    cli_email character varying(50),
    cli_cep character varying(8),
    cli_uf character varying(2),
    cli_telefone1 character varying(15),
    cli_telefone2 character varying(15),
    contr_razao_social character varying(100),
    contr_cnpj character varying(14),
    contr_endereco character varying(100),
    contr_numero character varying(10),
    contr_complemento character varying(100),
    contr_bairro character varying(50),
    contr_cidade character varying(50),
    contr_cep character varying(8),
    contr_uf character varying(2),
    contr_inscricao character varying(20),
    data_inicio_contrato character varying(15),
    cep character varying(8),
    cli_fantasia character varying(100),
    avulso boolean NOT NULL,
    comentario character varying(500),
    id_protocolo bigint
);


ALTER TABLE public.seg_estudo OWNER TO postgres;

--
-- TOC entry 294 (class 1259 OID 43375)
-- Name: seg_estudo_hospital; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.seg_estudo_hospital (
    id_estudo bigint NOT NULL,
    id_hospital bigint NOT NULL
);


ALTER TABLE public.seg_estudo_hospital OWNER TO postgres;

--
-- TOC entry 295 (class 1259 OID 43378)
-- Name: seg_estudo_id_estudo_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.seg_estudo_id_estudo_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.seg_estudo_id_estudo_seq OWNER TO postgres;

--
-- TOC entry 4697 (class 0 OID 0)
-- Dependencies: 295
-- Name: seg_estudo_id_estudo_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.seg_estudo_id_estudo_seq OWNED BY public.seg_estudo.id_estudo;


--
-- TOC entry 296 (class 1259 OID 43380)
-- Name: seg_estudo_medico; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.seg_estudo_medico (
    id_estudo bigint NOT NULL,
    id_medico bigint NOT NULL,
    situacao character varying(1) DEFAULT 'A'::character varying NOT NULL,
    coordenador boolean DEFAULT false NOT NULL
);


ALTER TABLE public.seg_estudo_medico OWNER TO postgres;

--
-- TOC entry 297 (class 1259 OID 43385)
-- Name: seg_exame; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.seg_exame (
    id_exame bigint NOT NULL,
    descricao character varying(100) NOT NULL,
    situacao character varying(1) NOT NULL,
    laboratorio boolean NOT NULL,
    preco numeric(8,2) NOT NULL,
    prioridade integer NOT NULL,
    custo numeric(8,2),
    externo boolean NOT NULL,
    libera_documento boolean DEFAULT false NOT NULL,
    codigo_tuss character varying(4),
    exame_complementar boolean DEFAULT false NOT NULL,
    periodo_vencimento integer,
    padrao_contrato boolean DEFAULT false NOT NULL
);


ALTER TABLE public.seg_exame OWNER TO postgres;

--
-- TOC entry 298 (class 1259 OID 43391)
-- Name: seg_exame_id_exame_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.seg_exame_id_exame_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.seg_exame_id_exame_seq OWNER TO postgres;

--
-- TOC entry 4698 (class 0 OID 0)
-- Dependencies: 298
-- Name: seg_exame_id_exame_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.seg_exame_id_exame_seq OWNED BY public.seg_exame.id_exame;


--
-- TOC entry 299 (class 1259 OID 43393)
-- Name: seg_fonte; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.seg_fonte (
    id_fonte bigint NOT NULL,
    descricao character varying(100) NOT NULL,
    situacao character varying(1) NOT NULL,
    privado boolean NOT NULL
);


ALTER TABLE public.seg_fonte OWNER TO postgres;

--
-- TOC entry 300 (class 1259 OID 43396)
-- Name: seg_fonte_id_fonte_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.seg_fonte_id_fonte_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.seg_fonte_id_fonte_seq OWNER TO postgres;

--
-- TOC entry 4699 (class 0 OID 0)
-- Dependencies: 300
-- Name: seg_fonte_id_fonte_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.seg_fonte_id_fonte_seq OWNED BY public.seg_fonte.id_fonte;


--
-- TOC entry 301 (class 1259 OID 43398)
-- Name: seg_forma; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.seg_forma (
    id_forma bigint NOT NULL,
    descricao character varying(100) NOT NULL
);


ALTER TABLE public.seg_forma OWNER TO postgres;

--
-- TOC entry 302 (class 1259 OID 43401)
-- Name: seg_forma_id_forma_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.seg_forma_id_forma_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.seg_forma_id_forma_seq OWNER TO postgres;

--
-- TOC entry 4700 (class 0 OID 0)
-- Dependencies: 302
-- Name: seg_forma_id_forma_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.seg_forma_id_forma_seq OWNED BY public.seg_forma.id_forma;


--
-- TOC entry 303 (class 1259 OID 43403)
-- Name: seg_funcao; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.seg_funcao (
    id_funcao bigint NOT NULL,
    descricao character varying(100) NOT NULL,
    cod_cbo character varying(6),
    situacao character varying(1) NOT NULL,
    comentario character varying(700) NOT NULL
);


ALTER TABLE public.seg_funcao OWNER TO postgres;

--
-- TOC entry 304 (class 1259 OID 43409)
-- Name: seg_funcao_epi; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.seg_funcao_epi (
    id_seg_funcao_epi bigint NOT NULL,
    id_estudo bigint NOT NULL,
    nome_funcao character varying(100),
    nome_epi character varying(100),
    tipo character varying(2),
    classificacao character varying(3)
);


ALTER TABLE public.seg_funcao_epi OWNER TO postgres;

--
-- TOC entry 305 (class 1259 OID 43412)
-- Name: seg_funcao_epi_id_seg_funcao_epi_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.seg_funcao_epi_id_seg_funcao_epi_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.seg_funcao_epi_id_seg_funcao_epi_seq OWNER TO postgres;

--
-- TOC entry 4701 (class 0 OID 0)
-- Dependencies: 305
-- Name: seg_funcao_epi_id_seg_funcao_epi_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.seg_funcao_epi_id_seg_funcao_epi_seq OWNED BY public.seg_funcao_epi.id_seg_funcao_epi;


--
-- TOC entry 306 (class 1259 OID 43414)
-- Name: seg_funcao_id_funcao_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.seg_funcao_id_funcao_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.seg_funcao_id_funcao_seq OWNER TO postgres;

--
-- TOC entry 4702 (class 0 OID 0)
-- Dependencies: 306
-- Name: seg_funcao_id_funcao_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.seg_funcao_id_funcao_seq OWNED BY public.seg_funcao.id_funcao;


--
-- TOC entry 307 (class 1259 OID 43416)
-- Name: seg_funcao_interna; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.seg_funcao_interna (
    id_funcao_interna bigint NOT NULL,
    descricao character varying(100) NOT NULL,
    situacao character varying(1) NOT NULL,
    privado character varying(1) NOT NULL
);


ALTER TABLE public.seg_funcao_interna OWNER TO postgres;

--
-- TOC entry 308 (class 1259 OID 43419)
-- Name: seg_funcao_interna_id_funcao_interna_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.seg_funcao_interna_id_funcao_interna_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.seg_funcao_interna_id_funcao_interna_seq OWNER TO postgres;

--
-- TOC entry 4703 (class 0 OID 0)
-- Dependencies: 308
-- Name: seg_funcao_interna_id_funcao_interna_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.seg_funcao_interna_id_funcao_interna_seq OWNED BY public.seg_funcao_interna.id_funcao_interna;


--
-- TOC entry 309 (class 1259 OID 43421)
-- Name: seg_funcionario; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.seg_funcionario (
    id_funcionario bigint NOT NULL,
    id_cidade bigint,
    nome character varying(100) NOT NULL,
    cpf character varying(11),
    rg character varying(15) NOT NULL,
    endereco character varying(100),
    numero character varying(10),
    complemento character varying(100),
    bairro character varying(50),
    cidade character varying(50),
    uf character varying(2),
    telefone1 character varying(15),
    telefone2 character varying(15),
    email character varying(50),
    tiposan character varying(2),
    ftrh character varying(1),
    data_nascimento date NOT NULL,
    peso integer,
    altura numeric(8,2),
    situacao character varying(1) NOT NULL,
    cep character varying(8),
    sexo character varying(1) NOT NULL,
    ctps character varying(15),
    pis_pasep character varying(11),
    pcd boolean DEFAULT false NOT NULL,
    orgao_emissor character varying(15),
    uf_emissor character varying(2),
    serie character varying(15),
    uf_emissor_ctps character varying(2)
);


ALTER TABLE public.seg_funcionario OWNER TO postgres;

--
-- TOC entry 310 (class 1259 OID 43428)
-- Name: seg_funcionario_id_funcionario_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.seg_funcionario_id_funcionario_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.seg_funcionario_id_funcionario_seq OWNER TO postgres;

--
-- TOC entry 4704 (class 0 OID 0)
-- Dependencies: 310
-- Name: seg_funcionario_id_funcionario_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.seg_funcionario_id_funcionario_seq OWNED BY public.seg_funcionario.id_funcionario;


--
-- TOC entry 311 (class 1259 OID 43430)
-- Name: seg_ghe; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.seg_ghe (
    id_ghe bigint NOT NULL,
    id_estudo bigint NOT NULL,
    descricao character varying(100),
    nexp integer,
    situacao character varying(1) NOT NULL,
    numero_po character varying(15)
);


ALTER TABLE public.seg_ghe OWNER TO postgres;

--
-- TOC entry 312 (class 1259 OID 43433)
-- Name: seg_ghe_fonte; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.seg_ghe_fonte (
    id_ghe_fonte bigint NOT NULL,
    id_fonte bigint NOT NULL,
    id_ghe bigint NOT NULL,
    principal boolean,
    situacao character varying(1) NOT NULL
);


ALTER TABLE public.seg_ghe_fonte OWNER TO postgres;

--
-- TOC entry 313 (class 1259 OID 43436)
-- Name: seg_ghe_fonte_agente; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.seg_ghe_fonte_agente (
    id_ghe_fonte_agente bigint NOT NULL,
    id_grad_soma bigint,
    id_grad_efeito bigint,
    id_grad_exposicao bigint,
    id_ghe_fonte bigint NOT NULL,
    id_agente bigint NOT NULL,
    tempo_exposicao character varying(30),
    situacao character varying(1) NOT NULL
);


ALTER TABLE public.seg_ghe_fonte_agente OWNER TO postgres;

--
-- TOC entry 314 (class 1259 OID 43439)
-- Name: seg_ghe_fonte_agente_epi; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.seg_ghe_fonte_agente_epi (
    id_ghe_fonte_agente_epi bigint NOT NULL,
    id_ghe_fonte_agente bigint NOT NULL,
    id_epi bigint NOT NULL,
    tipo_epi character varying(2) NOT NULL,
    classificacao character varying(3) NOT NULL
);


ALTER TABLE public.seg_ghe_fonte_agente_epi OWNER TO postgres;

--
-- TOC entry 315 (class 1259 OID 43442)
-- Name: seg_ghe_fonte_agente_epi_id_ghe_fonte_agente_epi_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.seg_ghe_fonte_agente_epi_id_ghe_fonte_agente_epi_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.seg_ghe_fonte_agente_epi_id_ghe_fonte_agente_epi_seq OWNER TO postgres;

--
-- TOC entry 4705 (class 0 OID 0)
-- Dependencies: 315
-- Name: seg_ghe_fonte_agente_epi_id_ghe_fonte_agente_epi_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.seg_ghe_fonte_agente_epi_id_ghe_fonte_agente_epi_seq OWNED BY public.seg_ghe_fonte_agente_epi.id_ghe_fonte_agente_epi;


--
-- TOC entry 316 (class 1259 OID 43444)
-- Name: seg_ghe_fonte_agente_exame; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.seg_ghe_fonte_agente_exame (
    id_ghe_fonte_agente_exame bigint NOT NULL,
    id_exame bigint NOT NULL,
    id_ghe_fonte_agente bigint NOT NULL,
    idade_exame integer NOT NULL,
    situacao character varying(1) NOT NULL,
    confinado boolean DEFAULT false NOT NULL,
    altura boolean DEFAULT false NOT NULL,
    eletricidade boolean DEFAULT false NOT NULL
);


ALTER TABLE public.seg_ghe_fonte_agente_exame OWNER TO postgres;

--
-- TOC entry 317 (class 1259 OID 43450)
-- Name: seg_ghe_fonte_agente_exame_aso; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.seg_ghe_fonte_agente_exame_aso (
    id_ghe_fonte_agente_exame_aso bigint NOT NULL,
    id_sala_exame bigint,
    id_aso bigint NOT NULL,
    id_ghe_fonte_agente_exame bigint NOT NULL,
    data_exame date NOT NULL,
    atendido boolean DEFAULT false,
    finalizado boolean DEFAULT false,
    login character varying(15),
    devolvido boolean DEFAULT false,
    data_atendido timestamp without time zone,
    data_devolvido timestamp without time zone,
    data_finalizado timestamp without time zone,
    id_usuario bigint,
    cancelado boolean DEFAULT false,
    data_cancelado timestamp without time zone,
    resultado character varying(1),
    observacao_resultado text,
    duplicado boolean NOT NULL,
    entrega boolean DEFAULT false NOT NULL,
    id_protocolo bigint,
    transcrito boolean DEFAULT false NOT NULL,
    id_prestador bigint,
    confinado boolean DEFAULT false NOT NULL,
    altura boolean DEFAULT false NOT NULL,
    id_medico bigint,
    data_validade date,
    eletricidade boolean DEFAULT false NOT NULL,
    imprime_aso boolean DEFAULT true NOT NULL
);


ALTER TABLE public.seg_ghe_fonte_agente_exame_aso OWNER TO postgres;

--
-- TOC entry 318 (class 1259 OID 43465)
-- Name: seg_ghe_fonte_agente_exame_as_id_ghe_fonte_agente_exame_aso_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.seg_ghe_fonte_agente_exame_as_id_ghe_fonte_agente_exame_aso_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.seg_ghe_fonte_agente_exame_as_id_ghe_fonte_agente_exame_aso_seq OWNER TO postgres;

--
-- TOC entry 4706 (class 0 OID 0)
-- Dependencies: 318
-- Name: seg_ghe_fonte_agente_exame_as_id_ghe_fonte_agente_exame_aso_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.seg_ghe_fonte_agente_exame_as_id_ghe_fonte_agente_exame_aso_seq OWNED BY public.seg_ghe_fonte_agente_exame_aso.id_ghe_fonte_agente_exame_aso;


--
-- TOC entry 319 (class 1259 OID 43467)
-- Name: seg_ghe_fonte_agente_exame_id_ghe_fonte_agente_exame_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.seg_ghe_fonte_agente_exame_id_ghe_fonte_agente_exame_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.seg_ghe_fonte_agente_exame_id_ghe_fonte_agente_exame_seq OWNER TO postgres;

--
-- TOC entry 4707 (class 0 OID 0)
-- Dependencies: 319
-- Name: seg_ghe_fonte_agente_exame_id_ghe_fonte_agente_exame_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.seg_ghe_fonte_agente_exame_id_ghe_fonte_agente_exame_seq OWNED BY public.seg_ghe_fonte_agente_exame.id_ghe_fonte_agente_exame;


--
-- TOC entry 320 (class 1259 OID 43469)
-- Name: seg_ghe_fonte_agente_exame_periodicidade; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.seg_ghe_fonte_agente_exame_periodicidade (
    id_periodicidade bigint NOT NULL,
    id_ghe_fonte_agente_exame bigint NOT NULL,
    situacao character varying(1) NOT NULL,
    periodo_vencimento integer
);


ALTER TABLE public.seg_ghe_fonte_agente_exame_periodicidade OWNER TO postgres;

--
-- TOC entry 321 (class 1259 OID 43472)
-- Name: seg_ghe_fonte_agente_id_ghe_fonte_agente_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.seg_ghe_fonte_agente_id_ghe_fonte_agente_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.seg_ghe_fonte_agente_id_ghe_fonte_agente_seq OWNER TO postgres;

--
-- TOC entry 4708 (class 0 OID 0)
-- Dependencies: 321
-- Name: seg_ghe_fonte_agente_id_ghe_fonte_agente_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.seg_ghe_fonte_agente_id_ghe_fonte_agente_seq OWNED BY public.seg_ghe_fonte_agente.id_ghe_fonte_agente;


--
-- TOC entry 322 (class 1259 OID 43474)
-- Name: seg_ghe_fonte_id_ghe_fonte_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.seg_ghe_fonte_id_ghe_fonte_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.seg_ghe_fonte_id_ghe_fonte_seq OWNER TO postgres;

--
-- TOC entry 4709 (class 0 OID 0)
-- Dependencies: 322
-- Name: seg_ghe_fonte_id_ghe_fonte_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.seg_ghe_fonte_id_ghe_fonte_seq OWNED BY public.seg_ghe_fonte.id_ghe_fonte;


--
-- TOC entry 323 (class 1259 OID 43476)
-- Name: seg_ghe_id_ghe_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.seg_ghe_id_ghe_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.seg_ghe_id_ghe_seq OWNER TO postgres;

--
-- TOC entry 4710 (class 0 OID 0)
-- Dependencies: 323
-- Name: seg_ghe_id_ghe_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.seg_ghe_id_ghe_seq OWNED BY public.seg_ghe.id_ghe;


--
-- TOC entry 324 (class 1259 OID 43478)
-- Name: seg_ghe_setor; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.seg_ghe_setor (
    id_ghe_setor bigint NOT NULL,
    id_setor bigint NOT NULL,
    id_ghe bigint NOT NULL,
    situacao character varying(1) NOT NULL
);


ALTER TABLE public.seg_ghe_setor OWNER TO postgres;

--
-- TOC entry 325 (class 1259 OID 43481)
-- Name: seg_ghe_setor_cliente_funcao; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.seg_ghe_setor_cliente_funcao (
    id_ghe_setor_cliente_funcao bigint NOT NULL,
    id_seg_cliente_funcao bigint NOT NULL,
    id_ghe_setor bigint NOT NULL,
    situacao character varying(1) DEFAULT 'A'::character varying NOT NULL,
    id_comentario_funcao bigint,
    confinado boolean DEFAULT false NOT NULL,
    altura boolean DEFAULT false NOT NULL,
    eletricidade boolean DEFAULT false NOT NULL
);


ALTER TABLE public.seg_ghe_setor_cliente_funcao OWNER TO postgres;

--
-- TOC entry 326 (class 1259 OID 43488)
-- Name: seg_ghe_setor_cliente_funcao_exame; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.seg_ghe_setor_cliente_funcao_exame (
    id_ghe_setor_cliente_funcao bigint NOT NULL,
    id_exame bigint NOT NULL,
    id_ghe_fonte_agente_exame bigint NOT NULL,
    idade integer NOT NULL
);


ALTER TABLE public.seg_ghe_setor_cliente_funcao_exame OWNER TO postgres;

--
-- TOC entry 327 (class 1259 OID 43491)
-- Name: seg_ghe_setor_cliente_funcao_id_ghe_setor_cliente_funcao_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.seg_ghe_setor_cliente_funcao_id_ghe_setor_cliente_funcao_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.seg_ghe_setor_cliente_funcao_id_ghe_setor_cliente_funcao_seq OWNER TO postgres;

--
-- TOC entry 4711 (class 0 OID 0)
-- Dependencies: 327
-- Name: seg_ghe_setor_cliente_funcao_id_ghe_setor_cliente_funcao_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.seg_ghe_setor_cliente_funcao_id_ghe_setor_cliente_funcao_seq OWNED BY public.seg_ghe_setor_cliente_funcao.id_ghe_setor_cliente_funcao;


--
-- TOC entry 328 (class 1259 OID 43493)
-- Name: seg_ghe_setor_id_ghe_setor_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.seg_ghe_setor_id_ghe_setor_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.seg_ghe_setor_id_ghe_setor_seq OWNER TO postgres;

--
-- TOC entry 4712 (class 0 OID 0)
-- Dependencies: 328
-- Name: seg_ghe_setor_id_ghe_setor_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.seg_ghe_setor_id_ghe_setor_seq OWNED BY public.seg_ghe_setor.id_ghe_setor;


--
-- TOC entry 329 (class 1259 OID 43495)
-- Name: seg_grad_efeito; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.seg_grad_efeito (
    id_grad_efeito bigint NOT NULL,
    descricao character varying NOT NULL,
    valor bigint NOT NULL,
    categoria character varying
);


ALTER TABLE public.seg_grad_efeito OWNER TO postgres;

--
-- TOC entry 330 (class 1259 OID 43501)
-- Name: seg_grad_efeito_id_grad_efeito_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.seg_grad_efeito_id_grad_efeito_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.seg_grad_efeito_id_grad_efeito_seq OWNER TO postgres;

--
-- TOC entry 4713 (class 0 OID 0)
-- Dependencies: 330
-- Name: seg_grad_efeito_id_grad_efeito_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.seg_grad_efeito_id_grad_efeito_seq OWNED BY public.seg_grad_efeito.id_grad_efeito;


--
-- TOC entry 331 (class 1259 OID 43503)
-- Name: seg_grad_exposicao; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.seg_grad_exposicao (
    id_grad_exposicao bigint NOT NULL,
    descricao character varying NOT NULL,
    categoria character varying NOT NULL,
    valor bigint NOT NULL
);


ALTER TABLE public.seg_grad_exposicao OWNER TO postgres;

--
-- TOC entry 332 (class 1259 OID 43509)
-- Name: seg_grad_exposicao_id_grad_exposicao_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.seg_grad_exposicao_id_grad_exposicao_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.seg_grad_exposicao_id_grad_exposicao_seq OWNER TO postgres;

--
-- TOC entry 4714 (class 0 OID 0)
-- Dependencies: 332
-- Name: seg_grad_exposicao_id_grad_exposicao_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.seg_grad_exposicao_id_grad_exposicao_seq OWNED BY public.seg_grad_exposicao.id_grad_exposicao;


--
-- TOC entry 333 (class 1259 OID 43511)
-- Name: seg_grad_soma; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.seg_grad_soma (
    id_grad_soma bigint NOT NULL,
    faixa bigint NOT NULL,
    med_controle character varying NOT NULL,
    descricao character varying NOT NULL
);


ALTER TABLE public.seg_grad_soma OWNER TO postgres;

--
-- TOC entry 334 (class 1259 OID 43517)
-- Name: seg_grad_soma_id_grad_soma_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.seg_grad_soma_id_grad_soma_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.seg_grad_soma_id_grad_soma_seq OWNER TO postgres;

--
-- TOC entry 4715 (class 0 OID 0)
-- Dependencies: 334
-- Name: seg_grad_soma_id_grad_soma_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.seg_grad_soma_id_grad_soma_seq OWNED BY public.seg_grad_soma.id_grad_soma;


--
-- TOC entry 335 (class 1259 OID 43519)
-- Name: seg_hospital; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.seg_hospital (
    id_hospital bigint NOT NULL,
    descricao character varying(100) NOT NULL,
    endereco character varying(100),
    numero character varying(10),
    complemento character varying(100),
    bairro character varying(50),
    cidade character varying(50),
    cep character varying(8),
    uf character varying(2),
    telefone1 character varying(15),
    telefone2 character varying(15),
    observacao character varying(200),
    situacao character varying(1) NOT NULL,
    id_cidade bigint
);


ALTER TABLE public.seg_hospital OWNER TO postgres;

--
-- TOC entry 336 (class 1259 OID 43525)
-- Name: seg_hospital_id_hospital_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.seg_hospital_id_hospital_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.seg_hospital_id_hospital_seq OWNER TO postgres;

--
-- TOC entry 4716 (class 0 OID 0)
-- Dependencies: 336
-- Name: seg_hospital_id_hospital_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.seg_hospital_id_hospital_seq OWNED BY public.seg_hospital.id_hospital;


--
-- TOC entry 337 (class 1259 OID 43527)
-- Name: seg_itens_cancelados_nf; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.seg_itens_cancelados_nf (
    id_itens_cancelados_nf bigint NOT NULL,
    id_ghe_fonte_agente_exame_aso bigint,
    id_cliente_funcao_exame_aso bigint,
    id_cliente bigint,
    id_produto_contrato bigint,
    id_nfe bigint,
    dt_inclusao date NOT NULL,
    dt_faturamento date,
    situacao character varying(1) NOT NULL,
    prc_unit numeric(8,2) NOT NULL,
    com_valor numeric(8,2),
    id_estudo bigint,
    id_aso bigint,
    quantidade integer NOT NULL,
    usuario character varying(15),
    id_usuario bigint,
    preco_custo numeric(8,2) DEFAULT 0
);


ALTER TABLE public.seg_itens_cancelados_nf OWNER TO postgres;

--
-- TOC entry 338 (class 1259 OID 43531)
-- Name: seg_itens_cancelados_nf_id_itens_cancelados_nf_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.seg_itens_cancelados_nf_id_itens_cancelados_nf_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.seg_itens_cancelados_nf_id_itens_cancelados_nf_seq OWNER TO postgres;

--
-- TOC entry 4717 (class 0 OID 0)
-- Dependencies: 338
-- Name: seg_itens_cancelados_nf_id_itens_cancelados_nf_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.seg_itens_cancelados_nf_id_itens_cancelados_nf_seq OWNED BY public.seg_itens_cancelados_nf.id_itens_cancelados_nf;


--
-- TOC entry 339 (class 1259 OID 43533)
-- Name: seg_log; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.seg_log (
    id_log bigint NOT NULL,
    tabela character varying(30) NOT NULL,
    id_tabela bigint NOT NULL,
    data_alteracao timestamp without time zone NOT NULL,
    atributo character varying(30) NOT NULL,
    valor_anterior character varying(600) NOT NULL,
    valor_alterado character varying(600) NOT NULL,
    usuario character varying(100) NOT NULL
);


ALTER TABLE public.seg_log OWNER TO postgres;

--
-- TOC entry 340 (class 1259 OID 43539)
-- Name: seg_log_id_log_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.seg_log_id_log_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.seg_log_id_log_seq OWNER TO postgres;

--
-- TOC entry 4718 (class 0 OID 0)
-- Dependencies: 340
-- Name: seg_log_id_log_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.seg_log_id_log_seq OWNED BY public.seg_log.id_log;


--
-- TOC entry 341 (class 1259 OID 43541)
-- Name: seg_login_auditoria; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.seg_login_auditoria (
    id_login_auditoria bigint NOT NULL,
    login character varying(30) NOT NULL,
    ip character varying(30) NOT NULL,
    data_alteracao timestamp without time zone NOT NULL,
    situacao character varying
);


ALTER TABLE public.seg_login_auditoria OWNER TO postgres;

--
-- TOC entry 342 (class 1259 OID 43547)
-- Name: seg_login_auditoria_id_login_auditoria_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.seg_login_auditoria_id_login_auditoria_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.seg_login_auditoria_id_login_auditoria_seq OWNER TO postgres;

--
-- TOC entry 4719 (class 0 OID 0)
-- Dependencies: 342
-- Name: seg_login_auditoria_id_login_auditoria_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.seg_login_auditoria_id_login_auditoria_seq OWNED BY public.seg_login_auditoria.id_login_auditoria;


--
-- TOC entry 343 (class 1259 OID 43549)
-- Name: seg_material_hospitalar; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.seg_material_hospitalar (
    id_material_hospitalar bigint NOT NULL,
    descricao character varying(100) NOT NULL,
    unidade character varying(10) NOT NULL,
    situacao character varying(1) NOT NULL
);


ALTER TABLE public.seg_material_hospitalar OWNER TO postgres;

--
-- TOC entry 344 (class 1259 OID 43552)
-- Name: seg_material_hospitalar_estudo; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.seg_material_hospitalar_estudo (
    id_estudo bigint NOT NULL,
    id_material_hospitalar bigint NOT NULL,
    quantidade integer NOT NULL
);


ALTER TABLE public.seg_material_hospitalar_estudo OWNER TO postgres;

--
-- TOC entry 345 (class 1259 OID 43555)
-- Name: seg_material_hospitalar_id_material_hospitalar_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.seg_material_hospitalar_id_material_hospitalar_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.seg_material_hospitalar_id_material_hospitalar_seq OWNER TO postgres;

--
-- TOC entry 4720 (class 0 OID 0)
-- Dependencies: 345
-- Name: seg_material_hospitalar_id_material_hospitalar_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.seg_material_hospitalar_id_material_hospitalar_seq OWNED BY public.seg_material_hospitalar.id_material_hospitalar;


--
-- TOC entry 346 (class 1259 OID 43557)
-- Name: seg_medico; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.seg_medico (
    id_medico bigint NOT NULL,
    id_cidade bigint,
    nome character varying(100) NOT NULL,
    crm character varying(12) NOT NULL,
    situacao character varying(1) NOT NULL,
    telefone1 character varying(15) NOT NULL,
    telefone2 character varying(15),
    email character varying(50),
    endereco character varying(100),
    numero character varying(10),
    complemento character varying(100),
    bairro character varying(50),
    cep character varying(8),
    cidade character varying(50),
    uf character varying(2),
    pis_pasep character varying(11),
    uf_crm character varying(2) NOT NULL,
    documento_extra character varying(15),
    assinatura bytea,
    mimetype character varying(30),
    cpf character varying(11),
    rqe character varying(15)
);


ALTER TABLE public.seg_medico OWNER TO postgres;

--
-- TOC entry 347 (class 1259 OID 43563)
-- Name: seg_medico_exame; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.seg_medico_exame (
    id bigint NOT NULL,
    id_exame bigint NOT NULL,
    id_medico bigint NOT NULL,
    data_gravacao date NOT NULL
);


ALTER TABLE public.seg_medico_exame OWNER TO postgres;

--
-- TOC entry 348 (class 1259 OID 43566)
-- Name: seg_medico_exame_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.seg_medico_exame_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.seg_medico_exame_id_seq OWNER TO postgres;

--
-- TOC entry 4721 (class 0 OID 0)
-- Dependencies: 348
-- Name: seg_medico_exame_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.seg_medico_exame_id_seq OWNED BY public.seg_medico_exame.id;


--
-- TOC entry 349 (class 1259 OID 43568)
-- Name: seg_medico_id_medico_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.seg_medico_id_medico_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.seg_medico_id_medico_seq OWNER TO postgres;

--
-- TOC entry 4722 (class 0 OID 0)
-- Dependencies: 349
-- Name: seg_medico_id_medico_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.seg_medico_id_medico_seq OWNED BY public.seg_medico.id_medico;


--
-- TOC entry 350 (class 1259 OID 43570)
-- Name: seg_monitoramento; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.seg_monitoramento (
    id_monitoramento bigint NOT NULL,
    id_monitoramento_anterior bigint,
    id_cliente_funcionario bigint,
    id_usuario bigint,
    data_inicio_condicao date,
    observacao text,
    id_cliente bigint,
    periodo date,
    id_estudo bigint,
    id_cliente_funcao_funcionario bigint,
    data_fim_condicao date
);


ALTER TABLE public.seg_monitoramento OWNER TO postgres;

--
-- TOC entry 351 (class 1259 OID 43576)
-- Name: seg_monitoramento_ghefonteagente; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.seg_monitoramento_ghefonteagente (
    id_monitoramento_ghefonteagente bigint NOT NULL,
    id_ghe_fonte_agente bigint NOT NULL,
    id_monitoramento bigint NOT NULL,
    tipo_avaliacao character varying(1) NOT NULL,
    intensidade_concentracao numeric(15,4),
    limite_tolerancia numeric(15,4),
    unidade_medicao character varying(2),
    tecnica_medicao text,
    utiliza_epc character varying(1),
    eficacia_epc character varying(1),
    utiliza_epi character varying(1),
    eficacia_epi character varying(1),
    medida_protecao character varying(1),
    condicao_funcionamento character varying(1),
    uso_init character varying(1),
    prazo_validade character varying(1),
    periodicidade_troca character varying(1),
    higienizacao character varying(1),
    numero_contrato_judicial character varying(25),
    numero_processo_judicial character varying(25)
);


ALTER TABLE public.seg_monitoramento_ghefonteagente OWNER TO postgres;

--
-- TOC entry 352 (class 1259 OID 43582)
-- Name: seg_monitoramento_ghefonteage_id_monitoramento_ghefonteagen_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.seg_monitoramento_ghefonteage_id_monitoramento_ghefonteagen_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.seg_monitoramento_ghefonteage_id_monitoramento_ghefonteagen_seq OWNER TO postgres;

--
-- TOC entry 4723 (class 0 OID 0)
-- Dependencies: 352
-- Name: seg_monitoramento_ghefonteage_id_monitoramento_ghefonteagen_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.seg_monitoramento_ghefonteage_id_monitoramento_ghefonteagen_seq OWNED BY public.seg_monitoramento_ghefonteagente.id_monitoramento_ghefonteagente;


--
-- TOC entry 353 (class 1259 OID 43584)
-- Name: seg_monitoramento_id_monitoramento_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.seg_monitoramento_id_monitoramento_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.seg_monitoramento_id_monitoramento_seq OWNER TO postgres;

--
-- TOC entry 4724 (class 0 OID 0)
-- Dependencies: 353
-- Name: seg_monitoramento_id_monitoramento_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.seg_monitoramento_id_monitoramento_seq OWNED BY public.seg_monitoramento.id_monitoramento;


--
-- TOC entry 354 (class 1259 OID 43586)
-- Name: seg_movimento; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.seg_movimento (
    id_movimento bigint NOT NULL,
    id_empresa bigint,
    id_ghe_fonte_agente_exame_aso bigint,
    id_cliente_funcao_exame_aso bigint,
    id_cliente bigint NOT NULL,
    id_produto_contrato bigint,
    id_nfe bigint,
    dt_inclusao timestamp without time zone NOT NULL,
    dt_faturamento timestamp without time zone,
    situacao character varying(1) NOT NULL,
    prc_unit numeric(8,2) NOT NULL,
    com_valor numeric(8,2),
    id_estudo bigint,
    id_aso bigint,
    quantidade integer NOT NULL,
    usuario character varying(15) NOT NULL,
    id_usuario bigint NOT NULL,
    preco_custo numeric(8,2) DEFAULT 0.00 NOT NULL,
    id_cliente_unidade bigint,
    id_protocolo bigint,
    data_gravacao timestamp without time zone NOT NULL,
    id_centrocusto bigint,
    id_contrato_exame bigint,
    id_cliente_credenciada bigint
);


ALTER TABLE public.seg_movimento OWNER TO postgres;

--
-- TOC entry 4725 (class 0 OID 0)
-- Dependencies: 354
-- Name: COLUMN seg_movimento.id_contrato_exame; Type: COMMENT; Schema: public; Owner: postgres
--

--
-- TOC entry 355 (class 1259 OID 43590)
-- Name: seg_movimento_id_movimento_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.seg_movimento_id_movimento_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.seg_movimento_id_movimento_seq OWNER TO postgres;

--
-- TOC entry 4726 (class 0 OID 0)
-- Dependencies: 355
-- Name: seg_movimento_id_movimento_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.seg_movimento_id_movimento_seq OWNED BY public.seg_movimento.id_movimento;


--
-- TOC entry 356 (class 1259 OID 43592)
-- Name: seg_nfe; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.seg_nfe (
    id_nfe bigint NOT NULL,
    id_usuario_criador bigint NOT NULL,
    id_empresa bigint NOT NULL,
    numero_nfe bigint NOT NULL,
    valor_total_nf numeric(8,2) NOT NULL,
    data_emissao timestamp without time zone NOT NULL,
    valor_pis numeric(8,2),
    valor_cofins numeric(8,2),
    base_calculo numeric(8,2),
    aliquota_iss numeric(8,2) NOT NULL,
    valor_ir numeric(8,2),
    valor_iss numeric(8,2),
    valor_csll numeric(8,2),
    razao_social character varying(100) NOT NULL,
    cnpj character varying(14) NOT NULL,
    inscricao character varying(20) NOT NULL,
    endereco character varying(100) NOT NULL,
    numero character varying(10) NOT NULL,
    complemento character varying(100) NOT NULL,
    bairro character varying(50) NOT NULL,
    cidade character varying(50) NOT NULL,
    cep character varying(8) NOT NULL,
    uf character varying(2) NOT NULL,
    telefone_1 character varying(15),
    email character varying(50),
    data_gravacao timestamp without time zone NOT NULL,
    data_cancelada timestamp without time zone,
    motivo_cancelamento text,
    id_usuario_cancela bigint,
    situacao character varying(1),
    id_plano_forma bigint NOT NULL,
    id_cliente bigint NOT NULL,
    valor_liquido numeric(8,2) DEFAULT 0.00 NOT NULL,
    id_centrocusto bigint
);


ALTER TABLE public.seg_nfe OWNER TO postgres;

--
-- TOC entry 357 (class 1259 OID 43599)
-- Name: seg_nfe_id_nfe_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.seg_nfe_id_nfe_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.seg_nfe_id_nfe_seq OWNER TO postgres;

--
-- TOC entry 4727 (class 0 OID 0)
-- Dependencies: 357
-- Name: seg_nfe_id_nfe_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.seg_nfe_id_nfe_seq OWNED BY public.seg_nfe.id_nfe;


--
-- TOC entry 358 (class 1259 OID 43601)
-- Name: seg_norma; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.seg_norma (
    id_norma bigint NOT NULL,
    descricao character varying(100) NOT NULL,
    situacao character varying(1) NOT NULL,
    conteudo character varying(400) NOT NULL
);


ALTER TABLE public.seg_norma OWNER TO postgres;

--
-- TOC entry 359 (class 1259 OID 43607)
-- Name: seg_norma_ghe; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.seg_norma_ghe (
    id_norma bigint NOT NULL,
    id_ghe bigint NOT NULL
);


ALTER TABLE public.seg_norma_ghe OWNER TO postgres;

--
-- TOC entry 360 (class 1259 OID 43610)
-- Name: seg_norma_id_norma_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.seg_norma_id_norma_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.seg_norma_id_norma_seq OWNER TO postgres;

--
-- TOC entry 4728 (class 0 OID 0)
-- Dependencies: 360
-- Name: seg_norma_id_norma_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.seg_norma_id_norma_seq OWNED BY public.seg_norma.id_norma;


--
-- TOC entry 361 (class 1259 OID 43612)
-- Name: seg_numeronfe_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.seg_numeronfe_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.seg_numeronfe_seq OWNER TO postgres;

--
-- TOC entry 362 (class 1259 OID 43614)
-- Name: seg_parcela; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.seg_parcela (
    id_parcela bigint NOT NULL,
    id_plano bigint NOT NULL,
    dias integer
);


ALTER TABLE public.seg_parcela OWNER TO postgres;

--
-- TOC entry 363 (class 1259 OID 43617)
-- Name: seg_parcela_id_parcela_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.seg_parcela_id_parcela_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.seg_parcela_id_parcela_seq OWNER TO postgres;

--
-- TOC entry 4729 (class 0 OID 0)
-- Dependencies: 363
-- Name: seg_parcela_id_parcela_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.seg_parcela_id_parcela_seq OWNED BY public.seg_parcela.id_parcela;


--
-- TOC entry 364 (class 1259 OID 43619)
-- Name: seg_perfil; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.seg_perfil (
    id_perfil bigint NOT NULL,
    descricao character varying(100) NOT NULL,
    situacao character varying(1) NOT NULL
);


ALTER TABLE public.seg_perfil OWNER TO postgres;

--
-- TOC entry 365 (class 1259 OID 43622)
-- Name: seg_perfil_dominio_acao; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.seg_perfil_dominio_acao (
    id_perfil bigint NOT NULL,
    id_dominio bigint NOT NULL,
    id_acao bigint NOT NULL
);


ALTER TABLE public.seg_perfil_dominio_acao OWNER TO postgres;

--
-- TOC entry 366 (class 1259 OID 43625)
-- Name: seg_perfil_id_perfil_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.seg_perfil_id_perfil_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.seg_perfil_id_perfil_seq OWNER TO postgres;

--
-- TOC entry 4730 (class 0 OID 0)
-- Dependencies: 366
-- Name: seg_perfil_id_perfil_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.seg_perfil_id_perfil_seq OWNED BY public.seg_perfil.id_perfil;


--
-- TOC entry 367 (class 1259 OID 43627)
-- Name: seg_periodicidade; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.seg_periodicidade (
    id_periodicidade bigint NOT NULL,
    descricao character varying(100) NOT NULL,
    ind_periodico boolean DEFAULT false NOT NULL,
    dias integer
);


ALTER TABLE public.seg_periodicidade OWNER TO postgres;

--
-- TOC entry 368 (class 1259 OID 43631)
-- Name: seg_periodicidade_id_periodicidade_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.seg_periodicidade_id_periodicidade_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.seg_periodicidade_id_periodicidade_seq OWNER TO postgres;

--
-- TOC entry 4731 (class 0 OID 0)
-- Dependencies: 368
-- Name: seg_periodicidade_id_periodicidade_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.seg_periodicidade_id_periodicidade_seq OWNED BY public.seg_periodicidade.id_periodicidade;


--
-- TOC entry 369 (class 1259 OID 43633)
-- Name: seg_plano; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.seg_plano (
    id_plano bigint NOT NULL,
    descricao character varying(100) NOT NULL,
    situacao character varying(1) NOT NULL,
    carga boolean NOT NULL
);


ALTER TABLE public.seg_plano OWNER TO postgres;

--
-- TOC entry 370 (class 1259 OID 43636)
-- Name: seg_plano_forma; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.seg_plano_forma (
    id_plano_forma bigint NOT NULL,
    id_forma bigint NOT NULL,
    id_plano bigint NOT NULL,
    situacao character varying(1) NOT NULL
);


ALTER TABLE public.seg_plano_forma OWNER TO postgres;

--
-- TOC entry 371 (class 1259 OID 43639)
-- Name: seg_plano_forma_id_plano_forma_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.seg_plano_forma_id_plano_forma_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.seg_plano_forma_id_plano_forma_seq OWNER TO postgres;

--
-- TOC entry 4732 (class 0 OID 0)
-- Dependencies: 371
-- Name: seg_plano_forma_id_plano_forma_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.seg_plano_forma_id_plano_forma_seq OWNED BY public.seg_plano_forma.id_plano_forma;


--
-- TOC entry 372 (class 1259 OID 43641)
-- Name: seg_plano_id_plano_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.seg_plano_id_plano_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.seg_plano_id_plano_seq OWNER TO postgres;

--
-- TOC entry 4733 (class 0 OID 0)
-- Dependencies: 372
-- Name: seg_plano_id_plano_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.seg_plano_id_plano_seq OWNED BY public.seg_plano.id_plano;


--
-- TOC entry 373 (class 1259 OID 43643)
-- Name: seg_procedimento_esocial; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.seg_procedimento_esocial (
    id bigint NOT NULL,
    codigo character varying(4) NOT NULL,
    procedimento text NOT NULL
);


ALTER TABLE public.seg_procedimento_esocial OWNER TO postgres;

--
-- TOC entry 374 (class 1259 OID 43649)
-- Name: seg_procedimento_esocial_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.seg_procedimento_esocial_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.seg_procedimento_esocial_id_seq OWNER TO postgres;

--
-- TOC entry 4734 (class 0 OID 0)
-- Dependencies: 374
-- Name: seg_procedimento_esocial_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.seg_procedimento_esocial_id_seq OWNED BY public.seg_procedimento_esocial.id;


--
-- TOC entry 375 (class 1259 OID 43651)
-- Name: seg_produto; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.seg_produto (
    id_produto bigint NOT NULL,
    nome character varying(100) NOT NULL,
    situacao character varying(1) NOT NULL,
    preco numeric(8,2) NOT NULL,
    tipo character varying(1) NOT NULL,
    custo numeric(8,2),
    tipo_estudo character varying(2),
    padrao_contrato boolean DEFAULT false NOT NULL,
    descricao text
);


ALTER TABLE public.seg_produto OWNER TO postgres;

--
-- TOC entry 376 (class 1259 OID 43658)
-- Name: seg_produto_contrato; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.seg_produto_contrato (
    id_produto_contrato bigint NOT NULL,
    id_produto bigint NOT NULL,
    id_contrato bigint NOT NULL,
    preco_contratado numeric(8,2) NOT NULL,
    usuario character varying(15) NOT NULL,
    custo_contrato numeric(8,2) NOT NULL,
    data_inclusao timestamp without time zone,
    situacao character varying(1) DEFAULT 'A'::character varying NOT NULL,
    altera boolean DEFAULT true NOT NULL
);


ALTER TABLE public.seg_produto_contrato OWNER TO postgres;

--
-- TOC entry 377 (class 1259 OID 43663)
-- Name: seg_produto_contrato_id_produto_contrato_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.seg_produto_contrato_id_produto_contrato_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.seg_produto_contrato_id_produto_contrato_seq OWNER TO postgres;

--
-- TOC entry 4735 (class 0 OID 0)
-- Dependencies: 377
-- Name: seg_produto_contrato_id_produto_contrato_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.seg_produto_contrato_id_produto_contrato_seq OWNED BY public.seg_produto_contrato.id_produto_contrato;


--
-- TOC entry 378 (class 1259 OID 43665)
-- Name: seg_produto_id_produto_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.seg_produto_id_produto_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.seg_produto_id_produto_seq OWNER TO postgres;

--
-- TOC entry 4736 (class 0 OID 0)
-- Dependencies: 378
-- Name: seg_produto_id_produto_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.seg_produto_id_produto_seq OWNED BY public.seg_produto.id_produto;


--
-- TOC entry 379 (class 1259 OID 43667)
-- Name: seg_prontuario; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.seg_prontuario (
    id_prontuario bigint NOT NULL,
    id_usuario bigint NOT NULL,
    id_aso bigint NOT NULL,
    pressao character varying(15),
    frequencia character varying(15),
    peso numeric(8,2),
    altura numeric(8,2),
    lassegue character varying(1),
    bebida boolean DEFAULT false,
    fumo boolean DEFAULT false,
    diarreia boolean DEFAULT false,
    tosse boolean DEFAULT false,
    afastado boolean DEFAULT false,
    convulsao boolean DEFAULT false,
    hepatite boolean DEFAULT false,
    acidente boolean DEFAULT false,
    tuberculose boolean DEFAULT false,
    antitetanica boolean DEFAULT false,
    atividade_fisica boolean DEFAULT false,
    medo_altura boolean DEFAULT false,
    dores_juntas boolean DEFAULT false,
    palpitacao boolean DEFAULT false,
    tonturas boolean DEFAULT false,
    azia boolean DEFAULT false,
    desmaio boolean DEFAULT false,
    dores_cabeca boolean DEFAULT false,
    epilepsia boolean DEFAULT false,
    insonia boolean DEFAULT false,
    problema_audicao boolean DEFAULT false,
    tendinite boolean DEFAULT false,
    fratura boolean DEFAULT false,
    alergia boolean DEFAULT false,
    asma boolean DEFAULT false,
    diabete boolean DEFAULT false,
    hernia boolean DEFAULT false,
    manchas_pele boolean DEFAULT false,
    pressao_alta boolean DEFAULT false,
    problema_visao boolean DEFAULT false,
    problema_coluna boolean DEFAULT false,
    rinite boolean DEFAULT false,
    varizes boolean DEFAULT false,
    oculos boolean DEFAULT false,
    aparelho_mucosa character varying(1) DEFAULT 'N'::character varying,
    aparelho_mucosa_obs character varying(100),
    aparelho_vascular character varying(1) DEFAULT 'N'::character varying,
    aparelho_vascular_obs character varying(100),
    aparelho_respiratorio character varying(1) DEFAULT 'N'::character varying,
    aparelho_respiratorio_obs character varying(100),
    aparelho_olho_dir character varying(1) DEFAULT 'N'::character varying,
    aparelho_olho_dir_obs character varying(100),
    aparelho_olho_esq character varying(1) DEFAULT 'N'::character varying,
    aparelho_olho_esq_obs character varying(100),
    aparelho_protese character varying(1) DEFAULT 'N'::character varying,
    aparelho_protese_obs character varying(100),
    aparelho_ganglio character varying(1) DEFAULT 'N'::character varying,
    aparelho_ganglio_obs character varying(100),
    aparelho_abdomen character varying(1) DEFAULT 'N'::character varying,
    aparelho_abdomen_obs character varying(100),
    aparelho_urina character varying(1) DEFAULT 'N'::character varying,
    aparelho_urina_obs character varying(100),
    aparelho_membro character varying(1) DEFAULT 'N'::character varying,
    aparelho_membro_obs character varying(100),
    aparelho_coluna character varying(1) DEFAULT 'N'::character varying,
    aparelho_coluna_obs character varying(100),
    aparelho_equilibrio character varying(1) DEFAULT 'N'::character varying,
    aparelho_equilibrio_obs character varying(100),
    aparelho_forca character varying(1) DEFAULT 'N'::character varying,
    aparelho_forca_obs character varying(100),
    aparelho_motor character varying(1) DEFAULT 'N'::character varying,
    aparelho_motor_obs character varying(100),
    parto integer,
    metodo character varying(20),
    data_ult_menstruacao date,
    previ_afastado boolean DEFAULT false,
    previ_recurso boolean DEFAULT false,
    data_afastamento date,
    causa text,
    laudo character varying(60),
    laudo_comentario text,
    metodo_obs text,
    observacao_aparelho text
);


ALTER TABLE public.seg_prontuario OWNER TO postgres;

--
-- TOC entry 380 (class 1259 OID 43723)
-- Name: seg_prontuario_id_prontuario_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.seg_prontuario_id_prontuario_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.seg_prontuario_id_prontuario_seq OWNER TO postgres;

--
-- TOC entry 4737 (class 0 OID 0)
-- Dependencies: 380
-- Name: seg_prontuario_id_prontuario_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.seg_prontuario_id_prontuario_seq OWNED BY public.seg_prontuario.id_prontuario;


--
-- TOC entry 381 (class 1259 OID 43725)
-- Name: seg_protocolo; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.seg_protocolo (
    id_protocolo bigint NOT NULL,
    numero character varying(12) NOT NULL,
    documento_responsavel character varying(20),
    situacao character varying(1) NOT NULL,
    data_gravacao timestamp without time zone NOT NULL,
    id_usuario_gerou bigint NOT NULL,
    data_cancelamento timestamp without time zone,
    id_usuario_cancelou bigint,
    enviado character varying(100),
    data_finalizacao timestamp without time zone,
    id_usuario_finalizou bigint,
    observacao text,
    motivo_cancelamento text,
    id_cliente bigint NOT NULL
);


ALTER TABLE public.seg_protocolo OWNER TO postgres;

--
-- TOC entry 382 (class 1259 OID 43731)
-- Name: seg_protocolo_id_protocolo_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.seg_protocolo_id_protocolo_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.seg_protocolo_id_protocolo_seq OWNER TO postgres;

--
-- TOC entry 4738 (class 0 OID 0)
-- Dependencies: 382
-- Name: seg_protocolo_id_protocolo_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.seg_protocolo_id_protocolo_seq OWNED BY public.seg_protocolo.id_protocolo;


--
-- TOC entry 383 (class 1259 OID 43733)
-- Name: seg_protocolo_item; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.seg_protocolo_item (
    id_item bigint NOT NULL,
    id_protocolo bigint NOT NULL,
    codigo_item character varying NOT NULL,
    cliente character varying NOT NULL,
    colaborador character varying,
    rg character varying,
    cpf character varying,
    periodicidade character varying NOT NULL,
    descricao_item character varying NOT NULL,
    data_item date NOT NULL,
    laudo character varying,
    tipo character varying NOT NULL
);


ALTER TABLE public.seg_protocolo_item OWNER TO postgres;

--
-- TOC entry 384 (class 1259 OID 43739)
-- Name: seg_ramo; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.seg_ramo (
    id bigint NOT NULL,
    descricao character varying(100) NOT NULL,
    situacao character varying(1) NOT NULL
);


ALTER TABLE public.seg_ramo OWNER TO postgres;

--
-- TOC entry 385 (class 1259 OID 43742)
-- Name: seg_ramo_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.seg_ramo_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.seg_ramo_id_seq OWNER TO postgres;

--
-- TOC entry 4739 (class 0 OID 0)
-- Dependencies: 385
-- Name: seg_ramo_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.seg_ramo_id_seq OWNED BY public.seg_ramo.id;


--
-- TOC entry 386 (class 1259 OID 43744)
-- Name: seg_relanual; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.seg_relanual (
    id_relanual bigint NOT NULL,
    id_usuario bigint NOT NULL,
    id_medico_coordenador bigint NOT NULL,
    id_cliente bigint NOT NULL,
    data_inicial date NOT NULL,
    data_final date NOT NULL,
    data_geracao timestamp without time zone NOT NULL
);


ALTER TABLE public.seg_relanual OWNER TO postgres;

--
-- TOC entry 387 (class 1259 OID 43747)
-- Name: seg_relanual_id_relanual_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.seg_relanual_id_relanual_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.seg_relanual_id_relanual_seq OWNER TO postgres;

--
-- TOC entry 4740 (class 0 OID 0)
-- Dependencies: 387
-- Name: seg_relanual_id_relanual_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.seg_relanual_id_relanual_seq OWNED BY public.seg_relanual.id_relanual;


--
-- TOC entry 388 (class 1259 OID 43749)
-- Name: seg_relanual_item; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.seg_relanual_item (
    id_relanual_item bigint NOT NULL,
    id_relanual bigint NOT NULL,
    id_setor bigint NOT NULL,
    descricao_setor character varying(100) NOT NULL,
    id_exame bigint NOT NULL,
    descricao_exame character varying(100) NOT NULL,
    total_exame_normal integer NOT NULL,
    total_exame_anormal integer NOT NULL,
    previsao integer NOT NULL,
    id_periodicidade bigint NOT NULL,
    descricao_periodicidade character varying(100) NOT NULL
);


ALTER TABLE public.seg_relanual_item OWNER TO postgres;

--
-- TOC entry 389 (class 1259 OID 43752)
-- Name: seg_relanual_item_id_relanual_item_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.seg_relanual_item_id_relanual_item_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.seg_relanual_item_id_relanual_item_seq OWNER TO postgres;

--
-- TOC entry 4741 (class 0 OID 0)
-- Dependencies: 389
-- Name: seg_relanual_item_id_relanual_item_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.seg_relanual_item_id_relanual_item_seq OWNED BY public.seg_relanual_item.id_relanual_item;


--
-- TOC entry 390 (class 1259 OID 43754)
-- Name: seg_relatorio; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.seg_relatorio (
    id_relatorio bigint NOT NULL,
    nome_relatorio character varying(50),
    tipo character varying(2),
    data_criacao date,
    situacao character varying(1),
    nmrelatorio character varying(50)
);


ALTER TABLE public.seg_relatorio OWNER TO postgres;

--
-- TOC entry 391 (class 1259 OID 43757)
-- Name: seg_relatorio_exame; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.seg_relatorio_exame (
    id_relatorio_exame bigint NOT NULL,
    id_exame bigint NOT NULL,
    id_relatorio bigint NOT NULL,
    situacao character varying(1) NOT NULL,
    data_gravacao date NOT NULL
);


ALTER TABLE public.seg_relatorio_exame OWNER TO postgres;

--
-- TOC entry 392 (class 1259 OID 43760)
-- Name: seg_relatorio_exame_id_relatorio_exame_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.seg_relatorio_exame_id_relatorio_exame_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.seg_relatorio_exame_id_relatorio_exame_seq OWNER TO postgres;

--
-- TOC entry 4742 (class 0 OID 0)
-- Dependencies: 392
-- Name: seg_relatorio_exame_id_relatorio_exame_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.seg_relatorio_exame_id_relatorio_exame_seq OWNED BY public.seg_relatorio_exame.id_relatorio_exame;


--
-- TOC entry 393 (class 1259 OID 43762)
-- Name: seg_relatorio_id_relatorio_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.seg_relatorio_id_relatorio_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.seg_relatorio_id_relatorio_seq OWNER TO postgres;

--
-- TOC entry 4743 (class 0 OID 0)
-- Dependencies: 393
-- Name: seg_relatorio_id_relatorio_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.seg_relatorio_id_relatorio_seq OWNED BY public.seg_relatorio.id_relatorio;


--
-- TOC entry 394 (class 1259 OID 43764)
-- Name: seg_rep_cli; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.seg_rep_cli (
    id_seg_rep_cli bigint NOT NULL,
    id_cliente bigint NOT NULL,
    id_vend bigint NOT NULL,
    situacao character varying(1) NOT NULL
);


ALTER TABLE public.seg_rep_cli OWNER TO postgres;

--
-- TOC entry 395 (class 1259 OID 43767)
-- Name: seg_rep_cli_id_seg_rep_cli_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.seg_rep_cli_id_seg_rep_cli_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.seg_rep_cli_id_seg_rep_cli_seq OWNER TO postgres;

--
-- TOC entry 4744 (class 0 OID 0)
-- Dependencies: 395
-- Name: seg_rep_cli_id_seg_rep_cli_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.seg_rep_cli_id_seg_rep_cli_seq OWNED BY public.seg_rep_cli.id_seg_rep_cli;


--
-- TOC entry 396 (class 1259 OID 43769)
-- Name: seg_revisao; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.seg_revisao (
    id_revisao bigint NOT NULL,
    id_estudo bigint NOT NULL,
    comentario character varying(255) NOT NULL,
    id_estudo_raiz bigint NOT NULL,
    id_estudo_revisao bigint,
    tipo_revisao character varying(6),
    versao_estudo character varying(3),
    data_revisao date,
    ind_correcao boolean NOT NULL
);


ALTER TABLE public.seg_revisao OWNER TO postgres;

--
-- TOC entry 397 (class 1259 OID 43772)
-- Name: seg_revisao_id_revisao_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.seg_revisao_id_revisao_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.seg_revisao_id_revisao_seq OWNER TO postgres;

--
-- TOC entry 4745 (class 0 OID 0)
-- Dependencies: 397
-- Name: seg_revisao_id_revisao_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.seg_revisao_id_revisao_seq OWNED BY public.seg_revisao.id_revisao;


--
-- TOC entry 398 (class 1259 OID 43774)
-- Name: seg_risco; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.seg_risco (
    id_risco bigint NOT NULL,
    descricao character varying(20) NOT NULL
);


ALTER TABLE public.seg_risco OWNER TO postgres;

--
-- TOC entry 399 (class 1259 OID 43777)
-- Name: seg_risco_id_risco_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.seg_risco_id_risco_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.seg_risco_id_risco_seq OWNER TO postgres;

--
-- TOC entry 4746 (class 0 OID 0)
-- Dependencies: 399
-- Name: seg_risco_id_risco_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.seg_risco_id_risco_seq OWNED BY public.seg_risco.id_risco;


--
-- TOC entry 400 (class 1259 OID 43779)
-- Name: seg_sala; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.seg_sala (
    id_sala bigint NOT NULL,
    descricao character varying(20) NOT NULL,
    situacao character varying(1) NOT NULL,
    andar integer NOT NULL,
    vip boolean NOT NULL,
    id_empresa bigint DEFAULT 1 NOT NULL,
    slug_sala character varying(20),
    qtde_chamada integer DEFAULT 1 NOT NULL
);


ALTER TABLE public.seg_sala OWNER TO postgres;

--
-- TOC entry 401 (class 1259 OID 43784)
-- Name: seg_sala_exame; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.seg_sala_exame (
    id_sala_exame bigint NOT NULL,
    id_exame bigint NOT NULL,
    id_sala bigint NOT NULL,
    tempo_medio_atendimento integer NOT NULL,
    tempo_medio_atendimento_calculado integer,
    situacao character varying(1) NOT NULL,
    liberada boolean DEFAULT true NOT NULL
);


ALTER TABLE public.seg_sala_exame OWNER TO postgres;

--
-- TOC entry 402 (class 1259 OID 43788)
-- Name: seg_sala_exame_id_sala_exame_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.seg_sala_exame_id_sala_exame_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.seg_sala_exame_id_sala_exame_seq OWNER TO postgres;

--
-- TOC entry 4747 (class 0 OID 0)
-- Dependencies: 402
-- Name: seg_sala_exame_id_sala_exame_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.seg_sala_exame_id_sala_exame_seq OWNED BY public.seg_sala_exame.id_sala_exame;


--
-- TOC entry 403 (class 1259 OID 43790)
-- Name: seg_sala_id_sala_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.seg_sala_id_sala_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.seg_sala_id_sala_seq OWNER TO postgres;

--
-- TOC entry 4748 (class 0 OID 0)
-- Dependencies: 403
-- Name: seg_sala_id_sala_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.seg_sala_id_sala_seq OWNED BY public.seg_sala.id_sala;


--
-- TOC entry 404 (class 1259 OID 43792)
-- Name: seg_setor; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.seg_setor (
    id_setor bigint NOT NULL,
    descricao character varying(100) NOT NULL,
    situacao character varying(1)
);


ALTER TABLE public.seg_setor OWNER TO postgres;

--
-- TOC entry 405 (class 1259 OID 43795)
-- Name: seg_setor_id_setor_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.seg_setor_id_setor_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.seg_setor_id_setor_seq OWNER TO postgres;

--
-- TOC entry 4749 (class 0 OID 0)
-- Dependencies: 405
-- Name: seg_setor_id_setor_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.seg_setor_id_setor_seq OWNED BY public.seg_setor.id_setor;


--
-- TOC entry 406 (class 1259 OID 43797)
-- Name: seg_tipo_contato; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.seg_tipo_contato (
    id bigint NOT NULL,
    descricao character varying(100)
);


ALTER TABLE public.seg_tipo_contato OWNER TO postgres;

--
-- TOC entry 407 (class 1259 OID 43800)
-- Name: seg_tipo_contato_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.seg_tipo_contato_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.seg_tipo_contato_id_seq OWNER TO postgres;

--
-- TOC entry 4750 (class 0 OID 0)
-- Dependencies: 407
-- Name: seg_tipo_contato_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.seg_tipo_contato_id_seq OWNED BY public.seg_tipo_contato.id;


--
-- TOC entry 408 (class 1259 OID 43802)
-- Name: seg_usuario; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.seg_usuario (
    id_usuario bigint NOT NULL,
    nome character varying(100) NOT NULL,
    cpf character varying(11) NOT NULL,
    rg character varying(15),
    doc_extra character varying(20),
    data_admissao date,
    situacao character varying(1) NOT NULL,
    login character varying(15) NOT NULL,
    senha character varying(255) NOT NULL,
    email character varying(50),
    id_medico bigint,
    id_empresa bigint,
    mte character varying(15),
    elabora_documento boolean DEFAULT false NOT NULL,
    assinatura bytea,
    mimetype character varying(30),
    externo boolean DEFAULT false NOT NULL,
    orgao_emissor character varying(10),
    uf_orgao_emissor character varying(2)
);


ALTER TABLE public.seg_usuario OWNER TO postgres;

--
-- TOC entry 409 (class 1259 OID 43811)
-- Name: seg_usuario_cliente; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.seg_usuario_cliente (
    id bigint NOT NULL,
    id_cliente bigint NOT NULL,
    id_usuario bigint NOT NULL,
    data_inclusao date NOT NULL
);


ALTER TABLE public.seg_usuario_cliente OWNER TO postgres;

--
-- TOC entry 410 (class 1259 OID 43814)
-- Name: seg_usuario_cliente_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.seg_usuario_cliente_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.seg_usuario_cliente_id_seq OWNER TO postgres;

--
-- TOC entry 4751 (class 0 OID 0)
-- Dependencies: 410
-- Name: seg_usuario_cliente_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.seg_usuario_cliente_id_seq OWNED BY public.seg_usuario_cliente.id;


--
-- TOC entry 411 (class 1259 OID 43816)
-- Name: seg_usuario_funcao_interna; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.seg_usuario_funcao_interna (
    id_usuario_funcao_interna bigint NOT NULL,
    id_funcao_interna bigint NOT NULL,
    id_usuario bigint NOT NULL,
    situacao character varying(1) NOT NULL,
    dt_inicio date NOT NULL,
    dt_termino date
);


ALTER TABLE public.seg_usuario_funcao_interna OWNER TO postgres;

--
-- TOC entry 412 (class 1259 OID 43819)
-- Name: seg_usuario_funcao_interna_id_usuario_funcao_interna_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.seg_usuario_funcao_interna_id_usuario_funcao_interna_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.seg_usuario_funcao_interna_id_usuario_funcao_interna_seq OWNER TO postgres;

--
-- TOC entry 4752 (class 0 OID 0)
-- Dependencies: 412
-- Name: seg_usuario_funcao_interna_id_usuario_funcao_interna_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.seg_usuario_funcao_interna_id_usuario_funcao_interna_seq OWNED BY public.seg_usuario_funcao_interna.id_usuario_funcao_interna;


--
-- TOC entry 413 (class 1259 OID 43821)
-- Name: seg_usuario_id_usuario_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.seg_usuario_id_usuario_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.seg_usuario_id_usuario_seq OWNER TO postgres;

--
-- TOC entry 4753 (class 0 OID 0)
-- Dependencies: 413
-- Name: seg_usuario_id_usuario_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.seg_usuario_id_usuario_seq OWNED BY public.seg_usuario.id_usuario;


--
-- TOC entry 414 (class 1259 OID 43823)
-- Name: seg_usuario_perfil; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.seg_usuario_perfil (
    id_usuario bigint NOT NULL,
    id_perfil bigint NOT NULL,
    data_inicio date
);


ALTER TABLE public.seg_usuario_perfil OWNER TO postgres;

--
-- TOC entry 415 (class 1259 OID 43826)
-- Name: seg_vendedor; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.seg_vendedor (
    id_vend bigint NOT NULL,
    nome character varying(100) NOT NULL,
    cpf character varying(11) NOT NULL,
    rg character varying(15) NOT NULL,
    endereco character varying(100),
    numero character varying(10),
    complemento character varying(100),
    bairro character varying(50),
    cidade character varying(50),
    uf character varying(2) NOT NULL,
    cep character varying(8),
    telefone1 character varying(15),
    telefone2 character varying(15),
    email character varying(50),
    comissao numeric(3,2) NOT NULL,
    data_cadastro date NOT NULL,
    situacao character varying(1),
    id_cidade_ibge bigint NOT NULL
);


ALTER TABLE public.seg_vendedor OWNER TO postgres;

--
-- TOC entry 416 (class 1259 OID 43832)
-- Name: seg_vendedor_id_vend_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.seg_vendedor_id_vend_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.seg_vendedor_id_vend_seq OWNER TO postgres;

--
-- TOC entry 4754 (class 0 OID 0)
-- Dependencies: 416
-- Name: seg_vendedor_id_vend_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.seg_vendedor_id_vend_seq OWNED BY public.seg_vendedor.id_vend;


--
-- TOC entry 417 (class 1259 OID 43834)
-- Name: seg_versao; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.seg_versao (
    id_versao bigint NOT NULL,
    id_estudo bigint NOT NULL,
    numero_revisao character varying NOT NULL,
    comentario character varying NOT NULL,
    data_cricao date NOT NULL
);


ALTER TABLE public.seg_versao OWNER TO postgres;

--
-- TOC entry 418 (class 1259 OID 43840)
-- Name: seg_versao_id_versao_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.seg_versao_id_versao_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.seg_versao_id_versao_seq OWNER TO postgres;

--
-- TOC entry 4755 (class 0 OID 0)
-- Dependencies: 418
-- Name: seg_versao_id_versao_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.seg_versao_id_versao_seq OWNED BY public.seg_versao.id_versao;


--
-- TOC entry 3454 (class 2604 OID 43842)
-- Name: seg_acao id_acao; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_acao ALTER COLUMN id_acao SET DEFAULT nextval('public.seg_acao_id_acao_seq'::regclass);


--
-- TOC entry 3455 (class 2604 OID 43843)
-- Name: seg_acesso id_acesso; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_acesso ALTER COLUMN id_acesso SET DEFAULT nextval('public.seg_acesso_id_acesso_seq'::regclass);


--
-- TOC entry 3457 (class 2604 OID 43844)
-- Name: seg_acompanhamento_atendimento id_acompanhamento_atendimento; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_acompanhamento_atendimento ALTER COLUMN id_acompanhamento_atendimento SET DEFAULT nextval('public.seg_acompanhamento_atendiment_id_acompanhamento_atendimento_seq'::regclass);


--
-- TOC entry 3458 (class 2604 OID 43845)
-- Name: seg_agente id_agente; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_agente ALTER COLUMN id_agente SET DEFAULT nextval('public.seg_agente_id_agente_seq'::regclass);


--
-- TOC entry 3459 (class 2604 OID 43846)
-- Name: seg_arquivo id_arquivo; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_arquivo ALTER COLUMN id_arquivo SET DEFAULT nextval('public.seg_arquivo_id_arquivo_seq'::regclass);


--
-- TOC entry 3460 (class 2604 OID 43847)
-- Name: seg_arquivo_anexo id_arquivo; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_arquivo_anexo ALTER COLUMN id_arquivo SET DEFAULT nextval('public.seg_arquivo_anexo_id_arquivo_seq'::regclass);


--
-- TOC entry 3465 (class 2604 OID 43848)
-- Name: seg_aso id_aso; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_aso ALTER COLUMN id_aso SET DEFAULT nextval('public.seg_aso_id_aso_seq'::regclass);


--
-- TOC entry 3466 (class 2604 OID 43849)
-- Name: seg_atividade id_atividade; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_atividade ALTER COLUMN id_atividade SET DEFAULT nextval('public.seg_atividade_id_atividade_seq'::regclass);


--
-- TOC entry 3467 (class 2604 OID 43850)
-- Name: seg_banco id_banco; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_banco ALTER COLUMN id_banco SET DEFAULT nextval('public.seg_banco_id_banco_seq'::regclass);


--
-- TOC entry 3469 (class 2604 OID 43851)
-- Name: seg_centrocusto id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_centrocusto ALTER COLUMN id SET DEFAULT nextval('public.seg_centrocusto_id_seq'::regclass);


--
-- TOC entry 3470 (class 2604 OID 43852)
-- Name: seg_cidade_ibge id_cidade_ibge; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_cidade_ibge ALTER COLUMN id_cidade_ibge SET DEFAULT nextval('public.seg_cidade_ibge_id_cidade_ibge_seq'::regclass);


--
-- TOC entry 3486 (class 2604 OID 43853)
-- Name: seg_cliente id_cliente; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_cliente ALTER COLUMN id_cliente SET DEFAULT nextval('public.seg_cliente_id_cliente_seq'::regclass);


--
-- TOC entry 3487 (class 2604 OID 43854)
-- Name: seg_cliente_arquivo id_cliente_arquivo; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_cliente_arquivo ALTER COLUMN id_cliente_arquivo SET DEFAULT nextval('public.seg_cliente_arquivo_id_cliente_arquivo_seq'::regclass);


--
-- TOC entry 3488 (class 2604 OID 43855)
-- Name: seg_cliente_funcao id_seg_cliente_funcao; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_cliente_funcao ALTER COLUMN id_seg_cliente_funcao SET DEFAULT nextval('public.seg_cliente_funcao_id_seg_cliente_funcao_seq'::regclass);


--
-- TOC entry 3489 (class 2604 OID 43856)
-- Name: seg_cliente_funcao_exame id_cliente_funcao_exame; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_cliente_funcao_exame ALTER COLUMN id_cliente_funcao_exame SET DEFAULT nextval('public.seg_cliente_funcao_exame_id_cliente_funcao_exame_seq'::regclass);


--
-- TOC entry 3496 (class 2604 OID 43857)
-- Name: seg_cliente_funcao_exame_aso id_cliente_funcao_exame_aso; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_cliente_funcao_exame_aso ALTER COLUMN id_cliente_funcao_exame_aso SET DEFAULT nextval('public.seg_cliente_funcao_exame_aso_id_cliente_funcao_exame_aso_seq'::regclass);


--
-- TOC entry 3502 (class 2604 OID 43858)
-- Name: seg_cliente_funcao_funcionario id_cliente_funcao_funcionario; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_cliente_funcao_funcionario ALTER COLUMN id_cliente_funcao_funcionario SET DEFAULT nextval('public.seg_cliente_funcao_funcionari_id_cliente_funcao_funcionario_seq'::regclass);


--
-- TOC entry 3505 (class 2604 OID 43859)
-- Name: seg_cliente_funcionario id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_cliente_funcionario ALTER COLUMN id SET DEFAULT nextval('public.seg_cliente_funcionario_id_seq'::regclass);


--
-- TOC entry 3506 (class 2604 OID 43860)
-- Name: seg_cliente_proposta id_cliente_proposta; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_cliente_proposta ALTER COLUMN id_cliente_proposta SET DEFAULT nextval('public.seg_cliente_proposta_id_cliente_proposta_seq'::regclass);


--
-- TOC entry 3508 (class 2604 OID 43861)
-- Name: seg_cnae id_cnae; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_cnae ALTER COLUMN id_cnae SET DEFAULT nextval('public.seg_cnae_id_cnae_seq'::regclass);


--
-- TOC entry 3510 (class 2604 OID 43862)
-- Name: seg_cnae_estudo id_cnae_estudo; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_cnae_estudo ALTER COLUMN id_cnae_estudo SET DEFAULT nextval('public.seg_cnae_estudo_id_cnae_estudo_seq'::regclass);


--
-- TOC entry 3511 (class 2604 OID 43863)
-- Name: seg_comentario_funcao id_comentario_funcao; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_comentario_funcao ALTER COLUMN id_comentario_funcao SET DEFAULT nextval('public.seg_comentario_funcao_id_comentario_funcao_seq'::regclass);


--
-- TOC entry 3512 (class 2604 OID 43864)
-- Name: seg_configuracao id_configuracao; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_configuracao ALTER COLUMN id_configuracao SET DEFAULT nextval('public.seg_configuracao_id_configuracao_seq'::regclass);


--
-- TOC entry 3513 (class 2604 OID 43865)
-- Name: seg_conta id_conta; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_conta ALTER COLUMN id_conta SET DEFAULT nextval('public.seg_conta_id_conta_seq'::regclass);


--
-- TOC entry 3516 (class 2604 OID 43866)
-- Name: seg_contato id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_contato ALTER COLUMN id SET DEFAULT nextval('public.seg_contato_id_seq'::regclass);


--
-- TOC entry 3525 (class 2604 OID 43867)
-- Name: seg_contrato id_contrato; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_contrato ALTER COLUMN id_contrato SET DEFAULT nextval('public.seg_contrato_id_contrato_seq'::regclass);


--
-- TOC entry 3528 (class 2604 OID 43868)
-- Name: seg_contrato_exame id_contrato_exame; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_contrato_exame ALTER COLUMN id_contrato_exame SET DEFAULT nextval('public.seg_contrato_exame_id_contrato_exame_seq'::regclass);


--
-- TOC entry 3529 (class 2604 OID 43869)
-- Name: seg_correcao_estudo id_correcao_estudo; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_correcao_estudo ALTER COLUMN id_correcao_estudo SET DEFAULT nextval('public.seg_correcao_estudo_id_correcao_estudo_seq'::regclass);


--
-- TOC entry 3531 (class 2604 OID 43870)
-- Name: seg_crc id_crc; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_crc ALTER COLUMN id_crc SET DEFAULT nextval('public.seg_crc_id_crc_seq'::regclass);


--
-- TOC entry 3532 (class 2604 OID 43871)
-- Name: seg_cronograma id_cronograma; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_cronograma ALTER COLUMN id_cronograma SET DEFAULT nextval('public.seg_cronograma_id_cronograma_seq'::regclass);


--
-- TOC entry 3534 (class 2604 OID 43872)
-- Name: seg_documento id_documento; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_documento ALTER COLUMN id_documento SET DEFAULT nextval('public.seg_documento_id_documento_seq'::regclass);


--
-- TOC entry 3535 (class 2604 OID 43873)
-- Name: seg_documento_estudo id_documento_estudo; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_documento_estudo ALTER COLUMN id_documento_estudo SET DEFAULT nextval('public.seg_documento_estudo_id_documento_estudo_seq'::regclass);


--
-- TOC entry 3536 (class 2604 OID 43874)
-- Name: seg_documento_protocolo id_documento_protocolo; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_documento_protocolo ALTER COLUMN id_documento_protocolo SET DEFAULT nextval('public.seg_documento_protocolo_id_documento_protocolo_seq'::regclass);


--
-- TOC entry 3537 (class 2604 OID 43875)
-- Name: seg_dominio id_dominio; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_dominio ALTER COLUMN id_dominio SET DEFAULT nextval('public.seg_dominio_id_dominio_seq'::regclass);


--
-- TOC entry 3538 (class 2604 OID 43876)
-- Name: seg_email id_email; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_email ALTER COLUMN id_email SET DEFAULT nextval('public.seg_email_id_email_seq'::regclass);


--
-- TOC entry 3547 (class 2604 OID 43877)
-- Name: seg_empresa id_empresa; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_empresa ALTER COLUMN id_empresa SET DEFAULT nextval('public.seg_empresa_id_empresa_seq'::regclass);


--
-- TOC entry 3548 (class 2604 OID 43878)
-- Name: seg_endemia id_endemia; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_endemia ALTER COLUMN id_endemia SET DEFAULT nextval('public.seg_endemia_id_endemia_seq'::regclass);


--
-- TOC entry 3549 (class 2604 OID 43879)
-- Name: seg_epi id_epi; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_epi ALTER COLUMN id_epi SET DEFAULT nextval('public.seg_epi_id_epi_seq'::regclass);


--
-- TOC entry 3550 (class 2604 OID 43880)
-- Name: seg_epi_monitoramento id_epi_monitoramento; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_epi_monitoramento ALTER COLUMN id_epi_monitoramento SET DEFAULT nextval('public.seg_epi_monitoramento_id_epi_monitoramento_seq'::regclass);


--
-- TOC entry 3551 (class 2604 OID 43881)
-- Name: seg_esocial_24 id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_esocial_24 ALTER COLUMN id SET DEFAULT nextval('public.seg_esocial_24_id_seq'::regclass);


--
-- TOC entry 3556 (class 2604 OID 43882)
-- Name: seg_esocial_monitoramento id_esocial_monitoramento; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_esocial_monitoramento ALTER COLUMN id_esocial_monitoramento SET DEFAULT nextval('public.seg_esocial_monitoramento_id_esocial_monitoramento_seq'::regclass);


--
-- TOC entry 3557 (class 2604 OID 43883)
-- Name: seg_estimativa id_estimativa; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_estimativa ALTER COLUMN id_estimativa SET DEFAULT nextval('public.seg_estimativa_id_estimativa_seq'::regclass);


--
-- TOC entry 3558 (class 2604 OID 43884)
-- Name: seg_estimativa_estudo id_estimativa_estudo; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_estimativa_estudo ALTER COLUMN id_estimativa_estudo SET DEFAULT nextval('public.seg_estimativa_estudo_id_estimativa_estudo_seq'::regclass);


--
-- TOC entry 3559 (class 2604 OID 43885)
-- Name: seg_estudo id_estudo; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_estudo ALTER COLUMN id_estudo SET DEFAULT nextval('public.seg_estudo_id_estudo_seq'::regclass);


--
-- TOC entry 3565 (class 2604 OID 43886)
-- Name: seg_exame id_exame; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_exame ALTER COLUMN id_exame SET DEFAULT nextval('public.seg_exame_id_exame_seq'::regclass);


--
-- TOC entry 3566 (class 2604 OID 43887)
-- Name: seg_fonte id_fonte; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_fonte ALTER COLUMN id_fonte SET DEFAULT nextval('public.seg_fonte_id_fonte_seq'::regclass);


--
-- TOC entry 3567 (class 2604 OID 43888)
-- Name: seg_forma id_forma; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_forma ALTER COLUMN id_forma SET DEFAULT nextval('public.seg_forma_id_forma_seq'::regclass);


--
-- TOC entry 3568 (class 2604 OID 43889)
-- Name: seg_funcao id_funcao; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_funcao ALTER COLUMN id_funcao SET DEFAULT nextval('public.seg_funcao_id_funcao_seq'::regclass);


--
-- TOC entry 3569 (class 2604 OID 43890)
-- Name: seg_funcao_epi id_seg_funcao_epi; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_funcao_epi ALTER COLUMN id_seg_funcao_epi SET DEFAULT nextval('public.seg_funcao_epi_id_seg_funcao_epi_seq'::regclass);


--
-- TOC entry 3570 (class 2604 OID 43891)
-- Name: seg_funcao_interna id_funcao_interna; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_funcao_interna ALTER COLUMN id_funcao_interna SET DEFAULT nextval('public.seg_funcao_interna_id_funcao_interna_seq'::regclass);


--
-- TOC entry 3572 (class 2604 OID 43892)
-- Name: seg_funcionario id_funcionario; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_funcionario ALTER COLUMN id_funcionario SET DEFAULT nextval('public.seg_funcionario_id_funcionario_seq'::regclass);


--
-- TOC entry 3573 (class 2604 OID 43893)
-- Name: seg_ghe id_ghe; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_ghe ALTER COLUMN id_ghe SET DEFAULT nextval('public.seg_ghe_id_ghe_seq'::regclass);


--
-- TOC entry 3574 (class 2604 OID 43894)
-- Name: seg_ghe_fonte id_ghe_fonte; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_ghe_fonte ALTER COLUMN id_ghe_fonte SET DEFAULT nextval('public.seg_ghe_fonte_id_ghe_fonte_seq'::regclass);


--
-- TOC entry 3575 (class 2604 OID 43895)
-- Name: seg_ghe_fonte_agente id_ghe_fonte_agente; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_ghe_fonte_agente ALTER COLUMN id_ghe_fonte_agente SET DEFAULT nextval('public.seg_ghe_fonte_agente_id_ghe_fonte_agente_seq'::regclass);


--
-- TOC entry 3576 (class 2604 OID 43896)
-- Name: seg_ghe_fonte_agente_epi id_ghe_fonte_agente_epi; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_ghe_fonte_agente_epi ALTER COLUMN id_ghe_fonte_agente_epi SET DEFAULT nextval('public.seg_ghe_fonte_agente_epi_id_ghe_fonte_agente_epi_seq'::regclass);


--
-- TOC entry 3580 (class 2604 OID 43897)
-- Name: seg_ghe_fonte_agente_exame id_ghe_fonte_agente_exame; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_ghe_fonte_agente_exame ALTER COLUMN id_ghe_fonte_agente_exame SET DEFAULT nextval('public.seg_ghe_fonte_agente_exame_id_ghe_fonte_agente_exame_seq'::regclass);


--
-- TOC entry 3590 (class 2604 OID 43898)
-- Name: seg_ghe_fonte_agente_exame_aso id_ghe_fonte_agente_exame_aso; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_ghe_fonte_agente_exame_aso ALTER COLUMN id_ghe_fonte_agente_exame_aso SET DEFAULT nextval('public.seg_ghe_fonte_agente_exame_as_id_ghe_fonte_agente_exame_aso_seq'::regclass);


--
-- TOC entry 3592 (class 2604 OID 43899)
-- Name: seg_ghe_setor id_ghe_setor; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_ghe_setor ALTER COLUMN id_ghe_setor SET DEFAULT nextval('public.seg_ghe_setor_id_ghe_setor_seq'::regclass);


--
-- TOC entry 3597 (class 2604 OID 43900)
-- Name: seg_ghe_setor_cliente_funcao id_ghe_setor_cliente_funcao; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_ghe_setor_cliente_funcao ALTER COLUMN id_ghe_setor_cliente_funcao SET DEFAULT nextval('public.seg_ghe_setor_cliente_funcao_id_ghe_setor_cliente_funcao_seq'::regclass);


--
-- TOC entry 3598 (class 2604 OID 43901)
-- Name: seg_grad_efeito id_grad_efeito; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_grad_efeito ALTER COLUMN id_grad_efeito SET DEFAULT nextval('public.seg_grad_efeito_id_grad_efeito_seq'::regclass);


--
-- TOC entry 3599 (class 2604 OID 43902)
-- Name: seg_grad_exposicao id_grad_exposicao; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_grad_exposicao ALTER COLUMN id_grad_exposicao SET DEFAULT nextval('public.seg_grad_exposicao_id_grad_exposicao_seq'::regclass);


--
-- TOC entry 3600 (class 2604 OID 43903)
-- Name: seg_grad_soma id_grad_soma; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_grad_soma ALTER COLUMN id_grad_soma SET DEFAULT nextval('public.seg_grad_soma_id_grad_soma_seq'::regclass);


--
-- TOC entry 3601 (class 2604 OID 43904)
-- Name: seg_hospital id_hospital; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_hospital ALTER COLUMN id_hospital SET DEFAULT nextval('public.seg_hospital_id_hospital_seq'::regclass);


--
-- TOC entry 3603 (class 2604 OID 43905)
-- Name: seg_itens_cancelados_nf id_itens_cancelados_nf; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_itens_cancelados_nf ALTER COLUMN id_itens_cancelados_nf SET DEFAULT nextval('public.seg_itens_cancelados_nf_id_itens_cancelados_nf_seq'::regclass);


--
-- TOC entry 3604 (class 2604 OID 43906)
-- Name: seg_log id_log; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_log ALTER COLUMN id_log SET DEFAULT nextval('public.seg_log_id_log_seq'::regclass);


--
-- TOC entry 3605 (class 2604 OID 43907)
-- Name: seg_login_auditoria id_login_auditoria; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_login_auditoria ALTER COLUMN id_login_auditoria SET DEFAULT nextval('public.seg_login_auditoria_id_login_auditoria_seq'::regclass);


--
-- TOC entry 3606 (class 2604 OID 43908)
-- Name: seg_material_hospitalar id_material_hospitalar; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_material_hospitalar ALTER COLUMN id_material_hospitalar SET DEFAULT nextval('public.seg_material_hospitalar_id_material_hospitalar_seq'::regclass);


--
-- TOC entry 3607 (class 2604 OID 43909)
-- Name: seg_medico id_medico; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_medico ALTER COLUMN id_medico SET DEFAULT nextval('public.seg_medico_id_medico_seq'::regclass);


--
-- TOC entry 3608 (class 2604 OID 43910)
-- Name: seg_medico_exame id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_medico_exame ALTER COLUMN id SET DEFAULT nextval('public.seg_medico_exame_id_seq'::regclass);


--
-- TOC entry 3609 (class 2604 OID 43911)
-- Name: seg_monitoramento id_monitoramento; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_monitoramento ALTER COLUMN id_monitoramento SET DEFAULT nextval('public.seg_monitoramento_id_monitoramento_seq'::regclass);


--
-- TOC entry 3610 (class 2604 OID 43912)
-- Name: seg_monitoramento_ghefonteagente id_monitoramento_ghefonteagente; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_monitoramento_ghefonteagente ALTER COLUMN id_monitoramento_ghefonteagente SET DEFAULT nextval('public.seg_monitoramento_ghefonteage_id_monitoramento_ghefonteagen_seq'::regclass);


--
-- TOC entry 3612 (class 2604 OID 43913)
-- Name: seg_movimento id_movimento; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_movimento ALTER COLUMN id_movimento SET DEFAULT nextval('public.seg_movimento_id_movimento_seq'::regclass);


--
-- TOC entry 3614 (class 2604 OID 43914)
-- Name: seg_nfe id_nfe; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_nfe ALTER COLUMN id_nfe SET DEFAULT nextval('public.seg_nfe_id_nfe_seq'::regclass);


--
-- TOC entry 3615 (class 2604 OID 43915)
-- Name: seg_norma id_norma; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_norma ALTER COLUMN id_norma SET DEFAULT nextval('public.seg_norma_id_norma_seq'::regclass);


--
-- TOC entry 3616 (class 2604 OID 43916)
-- Name: seg_parcela id_parcela; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_parcela ALTER COLUMN id_parcela SET DEFAULT nextval('public.seg_parcela_id_parcela_seq'::regclass);


--
-- TOC entry 3617 (class 2604 OID 43917)
-- Name: seg_perfil id_perfil; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_perfil ALTER COLUMN id_perfil SET DEFAULT nextval('public.seg_perfil_id_perfil_seq'::regclass);


--
-- TOC entry 3619 (class 2604 OID 43918)
-- Name: seg_periodicidade id_periodicidade; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_periodicidade ALTER COLUMN id_periodicidade SET DEFAULT nextval('public.seg_periodicidade_id_periodicidade_seq'::regclass);


--
-- TOC entry 3620 (class 2604 OID 43919)
-- Name: seg_plano id_plano; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_plano ALTER COLUMN id_plano SET DEFAULT nextval('public.seg_plano_id_plano_seq'::regclass);


--
-- TOC entry 3621 (class 2604 OID 43920)
-- Name: seg_plano_forma id_plano_forma; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_plano_forma ALTER COLUMN id_plano_forma SET DEFAULT nextval('public.seg_plano_forma_id_plano_forma_seq'::regclass);


--
-- TOC entry 3622 (class 2604 OID 43921)
-- Name: seg_procedimento_esocial id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_procedimento_esocial ALTER COLUMN id SET DEFAULT nextval('public.seg_procedimento_esocial_id_seq'::regclass);


--
-- TOC entry 3624 (class 2604 OID 43922)
-- Name: seg_produto id_produto; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_produto ALTER COLUMN id_produto SET DEFAULT nextval('public.seg_produto_id_produto_seq'::regclass);


--
-- TOC entry 3627 (class 2604 OID 43923)
-- Name: seg_produto_contrato id_produto_contrato; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_produto_contrato ALTER COLUMN id_produto_contrato SET DEFAULT nextval('public.seg_produto_contrato_id_produto_contrato_seq'::regclass);


--
-- TOC entry 3678 (class 2604 OID 43924)
-- Name: seg_prontuario id_prontuario; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_prontuario ALTER COLUMN id_prontuario SET DEFAULT nextval('public.seg_prontuario_id_prontuario_seq'::regclass);


--
-- TOC entry 3679 (class 2604 OID 43925)
-- Name: seg_protocolo id_protocolo; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_protocolo ALTER COLUMN id_protocolo SET DEFAULT nextval('public.seg_protocolo_id_protocolo_seq'::regclass);


--
-- TOC entry 3680 (class 2604 OID 43926)
-- Name: seg_ramo id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_ramo ALTER COLUMN id SET DEFAULT nextval('public.seg_ramo_id_seq'::regclass);


--
-- TOC entry 3681 (class 2604 OID 43927)
-- Name: seg_relanual id_relanual; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_relanual ALTER COLUMN id_relanual SET DEFAULT nextval('public.seg_relanual_id_relanual_seq'::regclass);


--
-- TOC entry 3682 (class 2604 OID 43928)
-- Name: seg_relanual_item id_relanual_item; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_relanual_item ALTER COLUMN id_relanual_item SET DEFAULT nextval('public.seg_relanual_item_id_relanual_item_seq'::regclass);


--
-- TOC entry 3683 (class 2604 OID 43929)
-- Name: seg_relatorio id_relatorio; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_relatorio ALTER COLUMN id_relatorio SET DEFAULT nextval('public.seg_relatorio_id_relatorio_seq'::regclass);


--
-- TOC entry 3684 (class 2604 OID 43930)
-- Name: seg_relatorio_exame id_relatorio_exame; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_relatorio_exame ALTER COLUMN id_relatorio_exame SET DEFAULT nextval('public.seg_relatorio_exame_id_relatorio_exame_seq'::regclass);


--
-- TOC entry 3685 (class 2604 OID 43931)
-- Name: seg_rep_cli id_seg_rep_cli; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_rep_cli ALTER COLUMN id_seg_rep_cli SET DEFAULT nextval('public.seg_rep_cli_id_seg_rep_cli_seq'::regclass);


--
-- TOC entry 3686 (class 2604 OID 43932)
-- Name: seg_revisao id_revisao; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_revisao ALTER COLUMN id_revisao SET DEFAULT nextval('public.seg_revisao_id_revisao_seq'::regclass);


--
-- TOC entry 3687 (class 2604 OID 43933)
-- Name: seg_risco id_risco; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_risco ALTER COLUMN id_risco SET DEFAULT nextval('public.seg_risco_id_risco_seq'::regclass);


--
-- TOC entry 3690 (class 2604 OID 43934)
-- Name: seg_sala id_sala; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_sala ALTER COLUMN id_sala SET DEFAULT nextval('public.seg_sala_id_sala_seq'::regclass);


--
-- TOC entry 3692 (class 2604 OID 43935)
-- Name: seg_sala_exame id_sala_exame; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_sala_exame ALTER COLUMN id_sala_exame SET DEFAULT nextval('public.seg_sala_exame_id_sala_exame_seq'::regclass);


--
-- TOC entry 3693 (class 2604 OID 43936)
-- Name: seg_setor id_setor; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_setor ALTER COLUMN id_setor SET DEFAULT nextval('public.seg_setor_id_setor_seq'::regclass);


--
-- TOC entry 3694 (class 2604 OID 43937)
-- Name: seg_tipo_contato id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_tipo_contato ALTER COLUMN id SET DEFAULT nextval('public.seg_tipo_contato_id_seq'::regclass);


--
-- TOC entry 3697 (class 2604 OID 43938)
-- Name: seg_usuario id_usuario; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_usuario ALTER COLUMN id_usuario SET DEFAULT nextval('public.seg_usuario_id_usuario_seq'::regclass);


--
-- TOC entry 3698 (class 2604 OID 43939)
-- Name: seg_usuario_cliente id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_usuario_cliente ALTER COLUMN id SET DEFAULT nextval('public.seg_usuario_cliente_id_seq'::regclass);


--
-- TOC entry 3699 (class 2604 OID 43940)
-- Name: seg_usuario_funcao_interna id_usuario_funcao_interna; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_usuario_funcao_interna ALTER COLUMN id_usuario_funcao_interna SET DEFAULT nextval('public.seg_usuario_funcao_interna_id_usuario_funcao_interna_seq'::regclass);


--
-- TOC entry 3700 (class 2604 OID 43941)
-- Name: seg_vendedor id_vend; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_vendedor ALTER COLUMN id_vend SET DEFAULT nextval('public.seg_vendedor_id_vend_seq'::regclass);


--
-- TOC entry 3701 (class 2604 OID 43942)
-- Name: seg_versao id_versao; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_versao ALTER COLUMN id_versao SET DEFAULT nextval('public.seg_versao_id_versao_seq'::regclass);


--
-- TOC entry 3703 (class 2606 OID 43995)
-- Name: seg_acao seg_acao_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_acao
    ADD CONSTRAINT seg_acao_pkey PRIMARY KEY (id_acao);


--
-- TOC entry 3706 (class 2606 OID 43997)
-- Name: seg_acesso seg_acesso_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_acesso
    ADD CONSTRAINT seg_acesso_pkey PRIMARY KEY (id_acesso);


--
-- TOC entry 3708 (class 2606 OID 43999)
-- Name: seg_acompanhamento_atendimento seg_acompanhamento_atendimento_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_acompanhamento_atendimento
    ADD CONSTRAINT seg_acompanhamento_atendimento_pkey PRIMARY KEY (id_acompanhamento_atendimento);


--
-- TOC entry 3712 (class 2606 OID 44002)
-- Name: seg_agente seg_agente_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_agente
    ADD CONSTRAINT seg_agente_pkey PRIMARY KEY (id_agente);


--
-- TOC entry 3718 (class 2606 OID 44004)
-- Name: seg_arquivo_anexo seg_arquivo_anexo_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_arquivo_anexo
    ADD CONSTRAINT seg_arquivo_anexo_pkey PRIMARY KEY (id_arquivo);


--
-- TOC entry 3714 (class 2606 OID 44006)
-- Name: seg_arquivo seg_arquivo_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_arquivo
    ADD CONSTRAINT seg_arquivo_pkey PRIMARY KEY (id_arquivo);


--
-- TOC entry 3745 (class 2606 OID 44008)
-- Name: seg_aso seg_aso_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_aso
    ADD CONSTRAINT seg_aso_pkey PRIMARY KEY (id_aso);


--
-- TOC entry 3758 (class 2606 OID 44010)
-- Name: seg_atividade_cronograma seg_atividade_cronograma_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_atividade_cronograma
    ADD CONSTRAINT seg_atividade_cronograma_pkey PRIMARY KEY (id_atividade, id_cronograma);


--
-- TOC entry 3752 (class 2606 OID 44012)
-- Name: seg_atividade seg_atividade_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_atividade
    ADD CONSTRAINT seg_atividade_pkey PRIMARY KEY (id_atividade);


--
-- TOC entry 3760 (class 2606 OID 44014)
-- Name: seg_banco seg_banco_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_banco
    ADD CONSTRAINT seg_banco_pkey PRIMARY KEY (id_banco);


--
-- TOC entry 3764 (class 2606 OID 44016)
-- Name: seg_centrocusto seg_centrocusto_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_centrocusto
    ADD CONSTRAINT seg_centrocusto_pkey PRIMARY KEY (id);


--
-- TOC entry 3767 (class 2606 OID 44018)
-- Name: seg_cidade_ibge seg_cidade_ibge_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_cidade_ibge
    ADD CONSTRAINT seg_cidade_ibge_pkey PRIMARY KEY (id_cidade_ibge);


--
-- TOC entry 3792 (class 2606 OID 44020)
-- Name: seg_cliente_arquivo seg_cliente_arquivo_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_cliente_arquivo
    ADD CONSTRAINT seg_cliente_arquivo_pkey PRIMARY KEY (id_cliente_arquivo, id_arquivo);


--
-- TOC entry 3819 (class 2606 OID 44022)
-- Name: seg_cliente_funcao_exame_aso seg_cliente_funcao_exame_aso_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_cliente_funcao_exame_aso
    ADD CONSTRAINT seg_cliente_funcao_exame_aso_pkey PRIMARY KEY (id_cliente_funcao_exame_aso);


--
-- TOC entry 3804 (class 2606 OID 44024)
-- Name: seg_cliente_funcao_exame seg_cliente_funcao_exame_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_cliente_funcao_exame
    ADD CONSTRAINT seg_cliente_funcao_exame_pkey PRIMARY KEY (id_cliente_funcao_exame);


--
-- TOC entry 3829 (class 2606 OID 44026)
-- Name: seg_cliente_funcao_funcionario seg_cliente_funcao_funcionario_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_cliente_funcao_funcionario
    ADD CONSTRAINT seg_cliente_funcao_funcionario_pkey PRIMARY KEY (id_cliente_funcao_funcionario);


--
-- TOC entry 3798 (class 2606 OID 44028)
-- Name: seg_cliente_funcao seg_cliente_funcao_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_cliente_funcao
    ADD CONSTRAINT seg_cliente_funcao_pkey PRIMARY KEY (id_seg_cliente_funcao);


--
-- TOC entry 3835 (class 2606 OID 44030)
-- Name: seg_cliente_funcionario seg_cliente_funcionario_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_cliente_funcionario
    ADD CONSTRAINT seg_cliente_funcionario_pkey PRIMARY KEY (id);


--
-- TOC entry 3785 (class 2606 OID 44032)
-- Name: seg_cliente seg_cliente_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_cliente
    ADD CONSTRAINT seg_cliente_pkey PRIMARY KEY (id_cliente);


--
-- TOC entry 3839 (class 2606 OID 44034)
-- Name: seg_cliente_proposta seg_cliente_proposta_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_cliente_proposta
    ADD CONSTRAINT seg_cliente_proposta_pkey PRIMARY KEY (id_cliente_proposta);


--
-- TOC entry 3847 (class 2606 OID 44036)
-- Name: seg_cnae_estudo seg_cnae_estudo_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_cnae_estudo
    ADD CONSTRAINT seg_cnae_estudo_pkey PRIMARY KEY (id_cnae_estudo);


--
-- TOC entry 3841 (class 2606 OID 44038)
-- Name: seg_cnae seg_cnae_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_cnae
    ADD CONSTRAINT seg_cnae_pkey PRIMARY KEY (id_cnae);


--
-- TOC entry 3851 (class 2606 OID 44040)
-- Name: seg_comentario_funcao seg_comentario_funcao_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_comentario_funcao
    ADD CONSTRAINT seg_comentario_funcao_pkey PRIMARY KEY (id_comentario_funcao);


--
-- TOC entry 3853 (class 2606 OID 44042)
-- Name: seg_configuracao seg_configuracao_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_configuracao
    ADD CONSTRAINT seg_configuracao_pkey PRIMARY KEY (id_configuracao);


--
-- TOC entry 3857 (class 2606 OID 44044)
-- Name: seg_conta seg_conta_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_conta
    ADD CONSTRAINT seg_conta_pkey PRIMARY KEY (id_conta);


--
-- TOC entry 3865 (class 2606 OID 44046)
-- Name: seg_contato seg_contato_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_contato
    ADD CONSTRAINT seg_contato_pkey PRIMARY KEY (id);


--
-- TOC entry 3885 (class 2606 OID 44048)
-- Name: seg_contrato_exame seg_contrato_exame_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_contrato_exame
    ADD CONSTRAINT seg_contrato_exame_pkey PRIMARY KEY (id_contrato_exame);


--
-- TOC entry 3879 (class 2606 OID 44050)
-- Name: seg_contrato seg_contrato_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_contrato
    ADD CONSTRAINT seg_contrato_pkey PRIMARY KEY (id_contrato);


--
-- TOC entry 3892 (class 2606 OID 44052)
-- Name: seg_correcao_estudo seg_correcao_estudo_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_correcao_estudo
    ADD CONSTRAINT seg_correcao_estudo_pkey PRIMARY KEY (id_correcao_estudo);


--
-- TOC entry 3914 (class 2606 OID 44054)
-- Name: seg_crc seg_crc_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_crc
    ADD CONSTRAINT seg_crc_pkey PRIMARY KEY (id_crc);


--
-- TOC entry 3917 (class 2606 OID 44056)
-- Name: seg_cronograma seg_cronograma_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_cronograma
    ADD CONSTRAINT seg_cronograma_pkey PRIMARY KEY (id_cronograma);


--
-- TOC entry 3925 (class 2606 OID 44058)
-- Name: seg_documento_estudo seg_documento_estudo_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_documento_estudo
    ADD CONSTRAINT seg_documento_estudo_pkey PRIMARY KEY (id_documento_estudo);


--
-- TOC entry 3919 (class 2606 OID 44060)
-- Name: seg_documento seg_documento_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_documento
    ADD CONSTRAINT seg_documento_pkey PRIMARY KEY (id_documento);


--
-- TOC entry 3933 (class 2606 OID 44062)
-- Name: seg_documento_protocolo seg_documento_protocolo_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_documento_protocolo
    ADD CONSTRAINT seg_documento_protocolo_pkey PRIMARY KEY (id_documento_protocolo);


--
-- TOC entry 3941 (class 2606 OID 44064)
-- Name: seg_dominio_acao seg_dominio_acao_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_dominio_acao
    ADD CONSTRAINT seg_dominio_acao_pkey PRIMARY KEY (id_dominio, id_acao);


--
-- TOC entry 3935 (class 2606 OID 44066)
-- Name: seg_dominio seg_dominio_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_dominio
    ADD CONSTRAINT seg_dominio_pkey PRIMARY KEY (id_dominio);


--
-- TOC entry 3946 (class 2606 OID 44068)
-- Name: seg_email seg_email_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_email
    ADD CONSTRAINT seg_email_pkey PRIMARY KEY (id_email);


--
-- TOC entry 3953 (class 2606 OID 44070)
-- Name: seg_empresa seg_empresa_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_empresa
    ADD CONSTRAINT seg_empresa_pkey PRIMARY KEY (id_empresa);


--
-- TOC entry 3955 (class 2606 OID 44072)
-- Name: seg_endemia seg_endemia_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_endemia
    ADD CONSTRAINT seg_endemia_pkey PRIMARY KEY (id_endemia);


--
-- TOC entry 3963 (class 2606 OID 44074)
-- Name: seg_epi_monitoramento seg_epi_monitoramento_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_epi_monitoramento
    ADD CONSTRAINT seg_epi_monitoramento_pkey PRIMARY KEY (id_epi_monitoramento);


--
-- TOC entry 3957 (class 2606 OID 44076)
-- Name: seg_epi seg_epi_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_epi
    ADD CONSTRAINT seg_epi_pkey PRIMARY KEY (id_epi);


--
-- TOC entry 3965 (class 2606 OID 44078)
-- Name: seg_esocial_24 seg_esocial_24_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_esocial_24
    ADD CONSTRAINT seg_esocial_24_pkey PRIMARY KEY (id);


--
-- TOC entry 3975 (class 2606 OID 44080)
-- Name: seg_esocial_lote_aso seg_esocial_lote_aso_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_esocial_lote_aso
    ADD CONSTRAINT seg_esocial_lote_aso_pkey PRIMARY KEY (id_seg_esocial_lote_aso);


--
-- TOC entry 3969 (class 2606 OID 44082)
-- Name: seg_esocial_lote seg_esocial_lote_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_esocial_lote
    ADD CONSTRAINT seg_esocial_lote_pkey PRIMARY KEY (id_esocial_lote);


--
-- TOC entry 3981 (class 2606 OID 44084)
-- Name: seg_esocial_monitoramento seg_esocial_monitoramento_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_esocial_monitoramento
    ADD CONSTRAINT seg_esocial_monitoramento_pkey PRIMARY KEY (id_esocial_monitoramento);


--
-- TOC entry 3989 (class 2606 OID 44086)
-- Name: seg_estimativa_estudo seg_estimativa_estudo_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_estimativa_estudo
    ADD CONSTRAINT seg_estimativa_estudo_pkey PRIMARY KEY (id_estimativa_estudo);


--
-- TOC entry 3983 (class 2606 OID 44088)
-- Name: seg_estimativa seg_estimativa_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_estimativa
    ADD CONSTRAINT seg_estimativa_pkey PRIMARY KEY (id_estimativa);


--
-- TOC entry 4012 (class 2606 OID 44090)
-- Name: seg_estudo_hospital seg_estudo_hospital_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_estudo_hospital
    ADD CONSTRAINT seg_estudo_hospital_pkey PRIMARY KEY (id_estudo, id_hospital);


--
-- TOC entry 4018 (class 2606 OID 44092)
-- Name: seg_estudo_medico seg_estudo_medico_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_estudo_medico
    ADD CONSTRAINT seg_estudo_medico_pkey PRIMARY KEY (id_estudo, id_medico);


--
-- TOC entry 4005 (class 2606 OID 44094)
-- Name: seg_estudo seg_estudo_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_estudo
    ADD CONSTRAINT seg_estudo_pkey PRIMARY KEY (id_estudo);


--
-- TOC entry 4020 (class 2606 OID 44096)
-- Name: seg_exame seg_exame_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_exame
    ADD CONSTRAINT seg_exame_pkey PRIMARY KEY (id_exame);


--
-- TOC entry 4022 (class 2606 OID 44098)
-- Name: seg_fonte seg_fonte_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_fonte
    ADD CONSTRAINT seg_fonte_pkey PRIMARY KEY (id_fonte);


--
-- TOC entry 4024 (class 2606 OID 44100)
-- Name: seg_forma seg_forma_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_forma
    ADD CONSTRAINT seg_forma_pkey PRIMARY KEY (id_forma);


--
-- TOC entry 4030 (class 2606 OID 44102)
-- Name: seg_funcao_epi seg_funcao_epi_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_funcao_epi
    ADD CONSTRAINT seg_funcao_epi_pkey PRIMARY KEY (id_seg_funcao_epi, id_estudo);


--
-- TOC entry 4033 (class 2606 OID 44104)
-- Name: seg_funcao_interna seg_funcao_interna_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_funcao_interna
    ADD CONSTRAINT seg_funcao_interna_pkey PRIMARY KEY (id_funcao_interna);


--
-- TOC entry 4026 (class 2606 OID 44106)
-- Name: seg_funcao seg_funcao_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_funcao
    ADD CONSTRAINT seg_funcao_pkey PRIMARY KEY (id_funcao);


--
-- TOC entry 4037 (class 2606 OID 44108)
-- Name: seg_funcionario seg_funcionario_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_funcionario
    ADD CONSTRAINT seg_funcionario_pkey PRIMARY KEY (id_funcionario);


--
-- TOC entry 4065 (class 2606 OID 44110)
-- Name: seg_ghe_fonte_agente_epi seg_ghe_fonte_agente_epi_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_ghe_fonte_agente_epi
    ADD CONSTRAINT seg_ghe_fonte_agente_epi_pkey PRIMARY KEY (id_ghe_fonte_agente_epi);


--
-- TOC entry 4088 (class 2606 OID 44112)
-- Name: seg_ghe_fonte_agente_exame_aso seg_ghe_fonte_agente_exame_aso_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_ghe_fonte_agente_exame_aso
    ADD CONSTRAINT seg_ghe_fonte_agente_exame_aso_pkey PRIMARY KEY (id_ghe_fonte_agente_exame_aso);


--
-- TOC entry 4071 (class 2606 OID 44114)
-- Name: seg_ghe_fonte_agente_exame seg_ghe_fonte_agente_exame_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_ghe_fonte_agente_exame
    ADD CONSTRAINT seg_ghe_fonte_agente_exame_pkey PRIMARY KEY (id_ghe_fonte_agente_exame);


--
-- TOC entry 4059 (class 2606 OID 44116)
-- Name: seg_ghe_fonte_agente seg_ghe_fonte_agente_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_ghe_fonte_agente
    ADD CONSTRAINT seg_ghe_fonte_agente_pkey PRIMARY KEY (id_ghe_fonte_agente);


--
-- TOC entry 4047 (class 2606 OID 44118)
-- Name: seg_ghe_fonte seg_ghe_fonte_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_ghe_fonte
    ADD CONSTRAINT seg_ghe_fonte_pkey PRIMARY KEY (id_ghe_fonte);


--
-- TOC entry 4041 (class 2606 OID 44120)
-- Name: seg_ghe seg_ghe_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_ghe
    ADD CONSTRAINT seg_ghe_pkey PRIMARY KEY (id_ghe);


--
-- TOC entry 4114 (class 2606 OID 44122)
-- Name: seg_ghe_setor_cliente_funcao_exame seg_ghe_setor_cliente_funcao_exame_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_ghe_setor_cliente_funcao_exame
    ADD CONSTRAINT seg_ghe_setor_cliente_funcao_exame_pkey PRIMARY KEY (id_ghe_setor_cliente_funcao, id_exame);


--
-- TOC entry 4106 (class 2606 OID 44124)
-- Name: seg_ghe_setor_cliente_funcao seg_ghe_setor_cliente_funcao_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_ghe_setor_cliente_funcao
    ADD CONSTRAINT seg_ghe_setor_cliente_funcao_pkey PRIMARY KEY (id_ghe_setor_cliente_funcao);


--
-- TOC entry 4098 (class 2606 OID 44126)
-- Name: seg_ghe_setor seg_ghe_setor_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_ghe_setor
    ADD CONSTRAINT seg_ghe_setor_pkey PRIMARY KEY (id_ghe_setor);


--
-- TOC entry 4116 (class 2606 OID 44128)
-- Name: seg_grad_efeito seg_grad_efeito_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_grad_efeito
    ADD CONSTRAINT seg_grad_efeito_pkey PRIMARY KEY (id_grad_efeito);


--
-- TOC entry 4118 (class 2606 OID 44130)
-- Name: seg_grad_exposicao seg_grad_exposicao_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_grad_exposicao
    ADD CONSTRAINT seg_grad_exposicao_pkey PRIMARY KEY (id_grad_exposicao);


--
-- TOC entry 4120 (class 2606 OID 44132)
-- Name: seg_grad_soma seg_grad_soma_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_grad_soma
    ADD CONSTRAINT seg_grad_soma_pkey PRIMARY KEY (id_grad_soma);


--
-- TOC entry 4124 (class 2606 OID 44134)
-- Name: seg_hospital seg_hospital_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_hospital
    ADD CONSTRAINT seg_hospital_pkey PRIMARY KEY (id_hospital);


--
-- TOC entry 4126 (class 2606 OID 44136)
-- Name: seg_itens_cancelados_nf seg_itens_cancelados_nf_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_itens_cancelados_nf
    ADD CONSTRAINT seg_itens_cancelados_nf_pkey PRIMARY KEY (id_itens_cancelados_nf);


--
-- TOC entry 4128 (class 2606 OID 44138)
-- Name: seg_log seg_log_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_log
    ADD CONSTRAINT seg_log_pkey PRIMARY KEY (id_log);


--
-- TOC entry 4130 (class 2606 OID 44140)
-- Name: seg_login_auditoria seg_login_auditoria_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_login_auditoria
    ADD CONSTRAINT seg_login_auditoria_pkey PRIMARY KEY (id_login_auditoria);


--
-- TOC entry 4136 (class 2606 OID 44142)
-- Name: seg_material_hospitalar_estudo seg_material_hospitalar_estudo_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_material_hospitalar_estudo
    ADD CONSTRAINT seg_material_hospitalar_estudo_pkey PRIMARY KEY (id_estudo, id_material_hospitalar);


--
-- TOC entry 4132 (class 2606 OID 44144)
-- Name: seg_material_hospitalar seg_material_hospitalar_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_material_hospitalar
    ADD CONSTRAINT seg_material_hospitalar_pkey PRIMARY KEY (id_material_hospitalar);


--
-- TOC entry 4148 (class 2606 OID 44146)
-- Name: seg_medico_exame seg_medico_exame_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_medico_exame
    ADD CONSTRAINT seg_medico_exame_pkey PRIMARY KEY (id);


--
-- TOC entry 4142 (class 2606 OID 44148)
-- Name: seg_medico seg_medico_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_medico
    ADD CONSTRAINT seg_medico_pkey PRIMARY KEY (id_medico);


--
-- TOC entry 4162 (class 2606 OID 44150)
-- Name: seg_monitoramento_ghefonteagente seg_monitoramento_ghefonteagente_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_monitoramento_ghefonteagente
    ADD CONSTRAINT seg_monitoramento_ghefonteagente_pkey PRIMARY KEY (id_monitoramento_ghefonteagente);


--
-- TOC entry 4156 (class 2606 OID 44152)
-- Name: seg_monitoramento seg_monitoramento_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_monitoramento
    ADD CONSTRAINT seg_monitoramento_pkey PRIMARY KEY (id_monitoramento);


--
-- TOC entry 4182 (class 2606 OID 44154)
-- Name: seg_movimento seg_movimento_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_movimento
    ADD CONSTRAINT seg_movimento_pkey PRIMARY KEY (id_movimento);


--
-- TOC entry 4194 (class 2606 OID 44156)
-- Name: seg_nfe seg_nfe_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_nfe
    ADD CONSTRAINT seg_nfe_pkey PRIMARY KEY (id_nfe);


--
-- TOC entry 4202 (class 2606 OID 44158)
-- Name: seg_norma_ghe seg_norma_ghe_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_norma_ghe
    ADD CONSTRAINT seg_norma_ghe_pkey PRIMARY KEY (id_norma, id_ghe);


--
-- TOC entry 4196 (class 2606 OID 44160)
-- Name: seg_norma seg_norma_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_norma
    ADD CONSTRAINT seg_norma_pkey PRIMARY KEY (id_norma);


--
-- TOC entry 4206 (class 2606 OID 44162)
-- Name: seg_parcela seg_parcela_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_parcela
    ADD CONSTRAINT seg_parcela_pkey PRIMARY KEY (id_parcela);


--
-- TOC entry 4214 (class 2606 OID 44164)
-- Name: seg_perfil_dominio_acao seg_perfil_dominio_acao_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_perfil_dominio_acao
    ADD CONSTRAINT seg_perfil_dominio_acao_pkey PRIMARY KEY (id_perfil, id_dominio, id_acao);


--
-- TOC entry 4208 (class 2606 OID 44166)
-- Name: seg_perfil seg_perfil_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_perfil
    ADD CONSTRAINT seg_perfil_pkey PRIMARY KEY (id_perfil);


--
-- TOC entry 4216 (class 2606 OID 44168)
-- Name: seg_periodicidade seg_periodicidade_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_periodicidade
    ADD CONSTRAINT seg_periodicidade_pkey PRIMARY KEY (id_periodicidade);


--
-- TOC entry 4224 (class 2606 OID 44170)
-- Name: seg_plano_forma seg_plano_forma_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_plano_forma
    ADD CONSTRAINT seg_plano_forma_pkey PRIMARY KEY (id_plano_forma);


--
-- TOC entry 4218 (class 2606 OID 44172)
-- Name: seg_plano seg_plano_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_plano
    ADD CONSTRAINT seg_plano_pkey PRIMARY KEY (id_plano);


--
-- TOC entry 4226 (class 2606 OID 44174)
-- Name: seg_procedimento_esocial seg_procedimento_esocial_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_procedimento_esocial
    ADD CONSTRAINT seg_procedimento_esocial_pkey PRIMARY KEY (id);


--
-- TOC entry 4234 (class 2606 OID 44176)
-- Name: seg_produto_contrato seg_produto_contrato_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_produto_contrato
    ADD CONSTRAINT seg_produto_contrato_pkey PRIMARY KEY (id_produto_contrato);


--
-- TOC entry 4228 (class 2606 OID 44178)
-- Name: seg_produto seg_produto_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_produto
    ADD CONSTRAINT seg_produto_pkey PRIMARY KEY (id_produto);


--
-- TOC entry 4241 (class 2606 OID 44180)
-- Name: seg_prontuario seg_prontuario_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_prontuario
    ADD CONSTRAINT seg_prontuario_pkey PRIMARY KEY (id_prontuario);


--
-- TOC entry 4251 (class 2606 OID 44182)
-- Name: seg_protocolo seg_protocolo_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_protocolo
    ADD CONSTRAINT seg_protocolo_pkey PRIMARY KEY (id_protocolo);


--
-- TOC entry 4253 (class 2606 OID 44184)
-- Name: seg_ramo seg_ramo_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_ramo
    ADD CONSTRAINT seg_ramo_pkey PRIMARY KEY (id);


--
-- TOC entry 4264 (class 2606 OID 44186)
-- Name: seg_relanual_item seg_relanual_item_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_relanual_item
    ADD CONSTRAINT seg_relanual_item_pkey PRIMARY KEY (id_relanual_item);


--
-- TOC entry 4261 (class 2606 OID 44188)
-- Name: seg_relanual seg_relanual_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_relanual
    ADD CONSTRAINT seg_relanual_pkey PRIMARY KEY (id_relanual);


--
-- TOC entry 4273 (class 2606 OID 44190)
-- Name: seg_relatorio_exame seg_relatorio_exame_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_relatorio_exame
    ADD CONSTRAINT seg_relatorio_exame_pkey PRIMARY KEY (id_relatorio_exame);


--
-- TOC entry 4267 (class 2606 OID 44192)
-- Name: seg_relatorio seg_relatorio_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_relatorio
    ADD CONSTRAINT seg_relatorio_pkey PRIMARY KEY (id_relatorio);


--
-- TOC entry 4279 (class 2606 OID 44194)
-- Name: seg_rep_cli seg_rep_cli_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_rep_cli
    ADD CONSTRAINT seg_rep_cli_pkey PRIMARY KEY (id_seg_rep_cli);


--
-- TOC entry 4283 (class 2606 OID 44196)
-- Name: seg_revisao seg_revisao_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_revisao
    ADD CONSTRAINT seg_revisao_pkey PRIMARY KEY (id_revisao);


--
-- TOC entry 4285 (class 2606 OID 44198)
-- Name: seg_risco seg_risco_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_risco
    ADD CONSTRAINT seg_risco_pkey PRIMARY KEY (id_risco);


--
-- TOC entry 4295 (class 2606 OID 44200)
-- Name: seg_sala_exame seg_sala_exame_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_sala_exame
    ADD CONSTRAINT seg_sala_exame_pkey PRIMARY KEY (id_sala_exame);


--
-- TOC entry 4289 (class 2606 OID 44202)
-- Name: seg_sala seg_sala_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_sala
    ADD CONSTRAINT seg_sala_pkey PRIMARY KEY (id_sala);


--
-- TOC entry 4297 (class 2606 OID 44204)
-- Name: seg_setor seg_setor_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_setor
    ADD CONSTRAINT seg_setor_pkey PRIMARY KEY (id_setor);


--
-- TOC entry 4299 (class 2606 OID 44206)
-- Name: seg_tipo_contato seg_tipo_contato_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_tipo_contato
    ADD CONSTRAINT seg_tipo_contato_pkey PRIMARY KEY (id);


--
-- TOC entry 4311 (class 2606 OID 44208)
-- Name: seg_usuario_cliente seg_usuario_cliente_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_usuario_cliente
    ADD CONSTRAINT seg_usuario_cliente_pkey PRIMARY KEY (id);


--
-- TOC entry 4317 (class 2606 OID 44210)
-- Name: seg_usuario_funcao_interna seg_usuario_funcao_interna_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_usuario_funcao_interna
    ADD CONSTRAINT seg_usuario_funcao_interna_pkey PRIMARY KEY (id_usuario_funcao_interna);


--
-- TOC entry 4323 (class 2606 OID 44212)
-- Name: seg_usuario_perfil seg_usuario_perfil_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_usuario_perfil
    ADD CONSTRAINT seg_usuario_perfil_pkey PRIMARY KEY (id_usuario, id_perfil);


--
-- TOC entry 4305 (class 2606 OID 44214)
-- Name: seg_usuario seg_usuario_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_usuario
    ADD CONSTRAINT seg_usuario_pkey PRIMARY KEY (id_usuario);


--
-- TOC entry 4327 (class 2606 OID 44216)
-- Name: seg_vendedor seg_vendedor_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_vendedor
    ADD CONSTRAINT seg_vendedor_pkey PRIMARY KEY (id_vend);


--
-- TOC entry 4331 (class 2606 OID 44218)
-- Name: seg_versao seg_versao_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_versao
    ADD CONSTRAINT seg_versao_pkey PRIMARY KEY (id_versao);


--
-- TOC entry 3836 (class 1259 OID 44219)
-- Name: cliente_proposta_id_cidade_ibge_fk; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX cliente_proposta_id_cidade_ibge_fk ON public.seg_cliente_proposta USING btree (id_cidade_ibge);


--
-- TOC entry 3920 (class 1259 OID 44220)
-- Name: documento_estudo_id_documento_fk; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX documento_estudo_id_documento_fk ON public.seg_documento_estudo USING btree (id_documento);


--
-- TOC entry 3921 (class 1259 OID 44221)
-- Name: documento_estudo_id_estudo_fk; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX documento_estudo_id_estudo_fk ON public.seg_documento_estudo USING btree (id_estudo);


--
-- TOC entry 3984 (class 1259 OID 44222)
-- Name: estimativa_estudo_id_estimativa_fk; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX estimativa_estudo_id_estimativa_fk ON public.seg_estimativa_estudo USING btree (id_estimativa);


--
-- TOC entry 3985 (class 1259 OID 44223)
-- Name: estimativa_estudo_id_estudo_fk; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX estimativa_estudo_id_estudo_fk ON public.seg_estimativa_estudo USING btree (id_estudo);


--
-- TOC entry 3936 (class 1259 OID 44224)
-- Name: ifk_acao_permite_dominio; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ifk_acao_permite_dominio ON public.seg_dominio_acao USING btree (id_acao);


--
-- TOC entry 4048 (class 1259 OID 44225)
-- Name: ifk_agente_especifica_ghe_font; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ifk_agente_especifica_ghe_font ON public.seg_ghe_fonte_agente USING btree (id_agente);


--
-- TOC entry 4157 (class 1259 OID 44226)
-- Name: ifk_agente_monitoramento; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ifk_agente_monitoramento ON public.seg_monitoramento_ghefonteagente USING btree (id_ghe_fonte_agente);


--
-- TOC entry 3786 (class 1259 OID 44227)
-- Name: ifk_arquivo_identifica_cliente; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ifk_arquivo_identifica_cliente ON public.seg_cliente_arquivo USING btree (id_cliente);


--
-- TOC entry 3715 (class 1259 OID 44228)
-- Name: ifk_aso_armazena_anexo; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ifk_aso_armazena_anexo ON public.seg_arquivo_anexo USING btree (id_aso);


--
-- TOC entry 3926 (class 1259 OID 44229)
-- Name: ifk_aso_completa_documento_pro; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ifk_aso_completa_documento_pro ON public.seg_documento_protocolo USING btree (id_aso);


--
-- TOC entry 3970 (class 1259 OID 44230)
-- Name: ifk_aso_esocial_lote_aso; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ifk_aso_esocial_lote_aso ON public.seg_esocial_lote_aso USING btree (id_aso);


--
-- TOC entry 3747 (class 1259 OID 44231)
-- Name: ifk_aso_identifica_agente_risc; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ifk_aso_identifica_agente_risc ON public.seg_aso_agente_risco USING btree (id_aso);


--
-- TOC entry 3749 (class 1259 OID 44232)
-- Name: ifk_aso_localiza_setor_ghe; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ifk_aso_localiza_setor_ghe ON public.seg_aso_ghe_setor USING btree (id_aso);


--
-- TOC entry 3719 (class 1259 OID 44235)
-- Name: ifk_aso_possui_periodicidade; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ifk_aso_possui_periodicidade ON public.seg_aso USING btree (id_periodicidade);


--
-- TOC entry 4236 (class 1259 OID 44236)
-- Name: ifk_aso_prontuaria_prontuario; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ifk_aso_prontuaria_prontuario ON public.seg_prontuario USING btree (id_aso);


--
-- TOC entry 3805 (class 1259 OID 44237)
-- Name: ifk_aso_prontuariza_cliente_fu; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ifk_aso_prontuariza_cliente_fu ON public.seg_cliente_funcao_exame_aso USING btree (id_aso);


--
-- TOC entry 4073 (class 1259 OID 44238)
-- Name: ifk_aso_prontuariza_ghe_fonte_; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ifk_aso_prontuariza_ghe_fonte_ ON public.seg_ghe_fonte_agente_exame_aso USING btree (id_aso);


--
-- TOC entry 3753 (class 1259 OID 44252)
-- Name: ifk_atividade_compoe_cronogram; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ifk_atividade_compoe_cronogram ON public.seg_atividade_cronograma USING btree (id_atividade);


--
-- TOC entry 4149 (class 1259 OID 44253)
-- Name: ifk_auto_relacionamento; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ifk_auto_relacionamento ON public.seg_monitoramento USING btree (id_monitoramento_anterior);


--
-- TOC entry 3854 (class 1259 OID 44254)
-- Name: ifk_banco_tem_conta; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ifk_banco_tem_conta ON public.seg_conta USING btree (id_banco);


--
-- TOC entry 3720 (class 1259 OID 44255)
-- Name: ifk_centro_custo_aso; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ifk_centro_custo_aso ON public.seg_aso USING btree (id_centrocusto);


--
-- TOC entry 3721 (class 1259 OID 44256)
-- Name: ifk_centrocusto_atendimento; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ifk_centrocusto_atendimento ON public.seg_aso USING btree (id_centrocusto);


--
-- TOC entry 3858 (class 1259 OID 44257)
-- Name: ifk_cidade_contato; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ifk_cidade_contato ON public.seg_contato USING btree (id_cidade);


--
-- TOC entry 3837 (class 1259 OID 44258)
-- Name: ifk_cidade_estabelece_cliente_; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ifk_cidade_estabelece_cliente_ ON public.seg_cliente_proposta USING btree (id_cidade_ibge);


--
-- TOC entry 4034 (class 1259 OID 44259)
-- Name: ifk_cidade_funcionario; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ifk_cidade_funcionario ON public.seg_funcionario USING btree (id_cidade);


--
-- TOC entry 4121 (class 1259 OID 44260)
-- Name: ifk_cidade_hospital; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ifk_cidade_hospital ON public.seg_hospital USING btree (id_cidade);


--
-- TOC entry 3772 (class 1259 OID 44261)
-- Name: ifk_cidade_ibge_identifica_cid; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ifk_cidade_ibge_identifica_cid ON public.seg_cliente USING btree (id_cidade_ibge_cobranca);


--
-- TOC entry 4139 (class 1259 OID 44262)
-- Name: ifk_cidade_medico; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ifk_cidade_medico ON public.seg_medico USING btree (id_cidade);


--
-- TOC entry 4274 (class 1259 OID 44263)
-- Name: ifk_cli_representa_rep; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ifk_cli_representa_rep ON public.seg_rep_cli USING btree (id_cliente);


--
-- TOC entry 3793 (class 1259 OID 44264)
-- Name: ifk_cliente_agrupa_funcao; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ifk_cliente_agrupa_funcao ON public.seg_cliente_funcao USING btree (id_cliente);


--
-- TOC entry 3761 (class 1259 OID 44265)
-- Name: ifk_cliente_centrocusto; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ifk_cliente_centrocusto ON public.seg_centrocusto USING btree (id_cliente);


--
-- TOC entry 3830 (class 1259 OID 44266)
-- Name: ifk_cliente_cliente_funcionari; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ifk_cliente_cliente_funcionari ON public.seg_cliente_funcionario USING btree (id_cliente);


--
-- TOC entry 4254 (class 1259 OID 44267)
-- Name: ifk_cliente_completa_relanaul; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ifk_cliente_completa_relanaul ON public.seg_relanual USING btree (id_cliente);


--
-- TOC entry 3859 (class 1259 OID 44268)
-- Name: ifk_cliente_contato; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ifk_cliente_contato ON public.seg_contato USING btree (id_cliente);


--
-- TOC entry 4183 (class 1259 OID 44269)
-- Name: ifk_cliente_emite_nota_fiscal; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ifk_cliente_emite_nota_fiscal ON public.seg_nfe USING btree (id_cliente);


--
-- TOC entry 4242 (class 1259 OID 44270)
-- Name: ifk_cliente_envia_protocolo; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ifk_cliente_envia_protocolo ON public.seg_protocolo USING btree (id_cliente);


--
-- TOC entry 3966 (class 1259 OID 44271)
-- Name: ifk_cliente_esociallote; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ifk_cliente_esociallote ON public.seg_esocial_lote USING btree (id_cliente);


--
-- TOC entry 3866 (class 1259 OID 44272)
-- Name: ifk_cliente_estabelece_contrat; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ifk_cliente_estabelece_contrat ON public.seg_contrato USING btree (id_cliente);


--
-- TOC entry 3820 (class 1259 OID 44273)
-- Name: ifk_cliente_funcao_exame_atend; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ifk_cliente_funcao_exame_atend ON public.seg_cliente_funcao_exame_periodicidade USING btree (id_cliente_funcao_exame);


--
-- TOC entry 3806 (class 1259 OID 44274)
-- Name: ifk_cliente_funcao_exame_pront; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ifk_cliente_funcao_exame_pront ON public.seg_cliente_funcao_exame_aso USING btree (id_cliente_funcao_exame);


--
-- TOC entry 4150 (class 1259 OID 44275)
-- Name: ifk_cliente_funcao_funcionario; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ifk_cliente_funcao_funcionario ON public.seg_monitoramento USING btree (id_cliente_funcao_funcionario);


--
-- TOC entry 3824 (class 1259 OID 44276)
-- Name: ifk_cliente_funcao_participa_f; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ifk_cliente_funcao_participa_f ON public.seg_cliente_funcao_funcionario USING btree (id_seg_cliente_funcao);


--
-- TOC entry 4099 (class 1259 OID 44277)
-- Name: ifk_cliente_funcao_participa_g; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ifk_cliente_funcao_participa_g ON public.seg_ghe_setor_cliente_funcao USING btree (id_seg_cliente_funcao);


--
-- TOC entry 3799 (class 1259 OID 44278)
-- Name: ifk_cliente_funcao_sugere_exam; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ifk_cliente_funcao_sugere_exam ON public.seg_cliente_funcao_exame USING btree (id_cliente_funcao);


--
-- TOC entry 3787 (class 1259 OID 44279)
-- Name: ifk_cliente_identifica_arquivo; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ifk_cliente_identifica_arquivo ON public.seg_cliente_arquivo USING btree (id_arquivo);


--
-- TOC entry 3773 (class 1259 OID 44280)
-- Name: ifk_cliente_identifica_credenc; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ifk_cliente_identifica_credenc ON public.seg_cliente USING btree (id_cliente_credenciado);


--
-- TOC entry 3774 (class 1259 OID 44281)
-- Name: ifk_cliente_identifica_matriz; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ifk_cliente_identifica_matriz ON public.seg_cliente USING btree (id_cliente_matriz);


--
-- TOC entry 3867 (class 1259 OID 44282)
-- Name: ifk_cliente_proposta_recebe_pr; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ifk_cliente_proposta_recebe_pr ON public.seg_contrato USING btree (id_cliente_proposta);


--
-- TOC entry 3768 (class 1259 OID 44283)
-- Name: ifk_cliente_qualifica_cnae; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ifk_cliente_qualifica_cnae ON public.seg_cli_cnae USING btree (id_cliente);


--
-- TOC entry 3775 (class 1259 OID 44284)
-- Name: ifk_cliente_ramo; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ifk_cliente_ramo ON public.seg_cliente USING btree (id_ramo);


--
-- TOC entry 4163 (class 1259 OID 44286)
-- Name: ifk_cliente_realiza_movimento; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ifk_cliente_realiza_movimento ON public.seg_movimento USING btree (id_cliente);


--
-- TOC entry 4164 (class 1259 OID 44289)
-- Name: ifk_cliente_unidade_gera_movim; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ifk_cliente_unidade_gera_movim ON public.seg_movimento USING btree (id_cliente_unidade);


--
-- TOC entry 4306 (class 1259 OID 44290)
-- Name: ifk_cliente_usuariocliente; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ifk_cliente_usuariocliente ON public.seg_usuario_cliente USING btree (id_cliente);


--
-- TOC entry 3769 (class 1259 OID 44291)
-- Name: ifk_cnae_qualifica_cliente; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ifk_cnae_qualifica_cliente ON public.seg_cli_cnae USING btree (id_cnae);


--
-- TOC entry 3842 (class 1259 OID 44292)
-- Name: ifk_cnae_relaciona_estudo; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ifk_cnae_relaciona_estudo ON public.seg_cnae_estudo USING btree (id_cnae);


--
-- TOC entry 3893 (class 1259 OID 44293)
-- Name: ifk_conta_participa_crc; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ifk_conta_participa_crc ON public.seg_crc USING btree (id_conta);


--
-- TOC entry 3868 (class 1259 OID 44294)
-- Name: ifk_contrato_adiciona_aditivo; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ifk_contrato_adiciona_aditivo ON public.seg_contrato USING btree (id_contrato_pai);


--
-- TOC entry 3880 (class 1259 OID 44295)
-- Name: ifk_contrato_contem_exame; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ifk_contrato_contem_exame ON public.seg_contrato_exame USING btree (id_contrato);


--
-- TOC entry 4229 (class 1259 OID 44297)
-- Name: ifk_contrato_contem_produto; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ifk_contrato_contem_produto ON public.seg_produto_contrato USING btree (id_contrato);


--
-- TOC entry 3869 (class 1259 OID 44298)
-- Name: ifk_contrato_informa_contrato_; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ifk_contrato_informa_contrato_ ON public.seg_contrato USING btree (id_contrato_raiz);


--
-- TOC entry 3894 (class 1259 OID 44299)
-- Name: ifk_crc_desdobra_crc; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ifk_crc_desdobra_crc ON public.seg_crc USING btree (id_crc_desdobramento);


--
-- TOC entry 3990 (class 1259 OID 44300)
-- Name: ifk_cronograma_completa_estudo; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ifk_cronograma_completa_estudo ON public.seg_estudo USING btree (id_cronograma);


--
-- TOC entry 3754 (class 1259 OID 44301)
-- Name: ifk_cronograma_contempla_ativi; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ifk_cronograma_contempla_ativi ON public.seg_atividade_cronograma USING btree (id_cronograma);


--
-- TOC entry 3927 (class 1259 OID 44302)
-- Name: ifk_documento_completa_documen; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ifk_documento_completa_documen ON public.seg_documento_protocolo USING btree (id_documento);


--
-- TOC entry 3922 (class 1259 OID 44303)
-- Name: ifk_documento_documento_estudo; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ifk_documento_documento_estudo ON public.seg_documento_estudo USING btree (id_documento);


--
-- TOC entry 3937 (class 1259 OID 44304)
-- Name: ifk_dominio_permite_acao; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ifk_dominio_permite_acao ON public.seg_dominio_acao USING btree (id_dominio);


--
-- TOC entry 4209 (class 1259 OID 44305)
-- Name: ifk_domino_acao_limita_perfil; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ifk_domino_acao_limita_perfil ON public.seg_perfil_dominio_acao USING btree (id_dominio, id_acao);


--
-- TOC entry 4049 (class 1259 OID 44306)
-- Name: ifk_efeito_efetivaghe_fonte_ag; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ifk_efeito_efetivaghe_fonte_ag ON public.seg_ghe_fonte_agente USING btree (id_grad_efeito);


--
-- TOC entry 3722 (class 1259 OID 44307)
-- Name: ifk_empresa_aso; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ifk_empresa_aso ON public.seg_aso USING btree (id_empresa);


--
-- TOC entry 3870 (class 1259 OID 44309)
-- Name: ifk_empresa_contrato; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ifk_empresa_contrato ON public.seg_contrato USING btree (id_empresa);


--
-- TOC entry 4184 (class 1259 OID 44310)
-- Name: ifk_empresa_fatura_notafiscal; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ifk_empresa_fatura_notafiscal ON public.seg_nfe USING btree (id_empresa);


--
-- TOC entry 3948 (class 1259 OID 44311)
-- Name: ifk_empresa_matriz; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ifk_empresa_matriz ON public.seg_empresa USING btree (id_empresa_matriz);


--
-- TOC entry 4165 (class 1259 OID 44312)
-- Name: ifk_empresa_realiza_movimento; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ifk_empresa_realiza_movimento ON public.seg_movimento USING btree (id_empresa);


--
-- TOC entry 4060 (class 1259 OID 44329)
-- Name: ifk_epi_controla_ghe_agente; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ifk_epi_controla_ghe_agente ON public.seg_ghe_fonte_agente_epi USING btree (id_epi);


--
-- TOC entry 3958 (class 1259 OID 44330)
-- Name: ifk_epi_monitoramento; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ifk_epi_monitoramento ON public.seg_epi_monitoramento USING btree (id_epi);


--
-- TOC entry 3971 (class 1259 OID 44331)
-- Name: ifk_esocial_lote_esocial_lote_; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ifk_esocial_lote_esocial_lote_ ON public.seg_esocial_lote_aso USING btree (id_esocial_lote);


--
-- TOC entry 3976 (class 1259 OID 44332)
-- Name: ifk_esocial_monitoramento; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ifk_esocial_monitoramento ON public.seg_esocial_monitoramento USING btree (id_esocial_lote);


--
-- TOC entry 3986 (class 1259 OID 44333)
-- Name: ifk_estimativa_completa_estima; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ifk_estimativa_completa_estima ON public.seg_estimativa_estudo USING btree (id_estimativa);


--
-- TOC entry 4027 (class 1259 OID 44334)
-- Name: ifk_estudo_apresenta_funcao_ep; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ifk_estudo_apresenta_funcao_ep ON public.seg_funcao_epi USING btree (id_estudo);


--
-- TOC entry 4007 (class 1259 OID 44335)
-- Name: ifk_estudo_atende_hospital; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ifk_estudo_atende_hospital ON public.seg_estudo_hospital USING btree (id_estudo);


--
-- TOC entry 3987 (class 1259 OID 44336)
-- Name: ifk_estudo_completa_estimativa; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ifk_estudo_completa_estimativa ON public.seg_estimativa_estudo USING btree (id_estudo);


--
-- TOC entry 3923 (class 1259 OID 44337)
-- Name: ifk_estudo_documento_estudo; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ifk_estudo_documento_estudo ON public.seg_documento_estudo USING btree (id_estudo);


--
-- TOC entry 4013 (class 1259 OID 44338)
-- Name: ifk_estudo_examinado_medico; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ifk_estudo_examinado_medico ON public.seg_estudo_medico USING btree (id_estudo);


--
-- TOC entry 3887 (class 1259 OID 44339)
-- Name: ifk_estudo_grava_log_correcao; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ifk_estudo_grava_log_correcao ON public.seg_correcao_estudo USING btree (id_estudo);


--
-- TOC entry 3843 (class 1259 OID 44340)
-- Name: ifk_estudo_relaciona_cnae; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ifk_estudo_relaciona_cnae ON public.seg_cnae_estudo USING btree (id_estudo);


--
-- TOC entry 3991 (class 1259 OID 44341)
-- Name: ifk_estudo_subcontrata_cli; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ifk_estudo_subcontrata_cli ON public.seg_estudo USING btree (id_cliente_contratado);


--
-- TOC entry 4133 (class 1259 OID 44342)
-- Name: ifk_estudo_utiliza_material_ho; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ifk_estudo_utiliza_material_ho ON public.seg_material_hospitalar_estudo USING btree (id_estudo);


--
-- TOC entry 3992 (class 1259 OID 44343)
-- Name: ifk_estudo_versiona_relatorio; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ifk_estudo_versiona_relatorio ON public.seg_estudo USING btree (id_relatorio);


--
-- TOC entry 4290 (class 1259 OID 44344)
-- Name: ifk_exame_atende_sala; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ifk_exame_atende_sala ON public.seg_sala_exame USING btree (id_sala);


--
-- TOC entry 3881 (class 1259 OID 44345)
-- Name: ifk_exame_contem_contrato; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ifk_exame_contem_contrato ON public.seg_contrato_exame USING btree (id_exame);


--
-- TOC entry 4143 (class 1259 OID 44346)
-- Name: ifk_exame_medicoexame; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ifk_exame_medicoexame ON public.seg_medico_exame USING btree (id_exame);


--
-- TOC entry 4067 (class 1259 OID 44347)
-- Name: ifk_exame_realiza_ghe_fonte_ag; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ifk_exame_realiza_ghe_fonte_ag ON public.seg_ghe_fonte_agente_exame USING btree (id_exame);


--
-- TOC entry 4268 (class 1259 OID 44348)
-- Name: ifk_exame_relatorio_exame; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ifk_exame_relatorio_exame ON public.seg_relatorio_exame USING btree (id_exame);


--
-- TOC entry 4107 (class 1259 OID 44349)
-- Name: ifk_exame_restrito_por_ghe_set; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ifk_exame_restrito_por_ghe_set ON public.seg_ghe_setor_cliente_funcao_exame USING btree (id_exame);


--
-- TOC entry 3800 (class 1259 OID 44350)
-- Name: ifk_exame_sugerido_cliente_fun; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ifk_exame_sugerido_cliente_fun ON public.seg_cliente_funcao_exame USING btree (id_exame);


--
-- TOC entry 4050 (class 1259 OID 44351)
-- Name: ifk_exposicao_gradativa_ghe_fo; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ifk_exposicao_gradativa_ghe_fo ON public.seg_ghe_fonte_agente USING btree (id_grad_exposicao);


--
-- TOC entry 4042 (class 1259 OID 44352)
-- Name: ifk_fonte_expoem_ghe; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ifk_fonte_expoem_ghe ON public.seg_ghe_fonte USING btree (id_fonte);


--
-- TOC entry 4219 (class 1259 OID 44353)
-- Name: ifk_forma_aceita_plano; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ifk_forma_aceita_plano ON public.seg_plano_forma USING btree (id_forma);


--
-- TOC entry 3895 (class 1259 OID 44354)
-- Name: ifk_forma_cobra_cobranca; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ifk_forma_cobra_cobranca ON public.seg_crc USING btree (id_forma);


--
-- TOC entry 3794 (class 1259 OID 44355)
-- Name: ifk_funcao_agrupa_cliente; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ifk_funcao_agrupa_cliente ON public.seg_cliente_funcao USING btree (id_funcao);


--
-- TOC entry 3848 (class 1259 OID 44356)
-- Name: ifk_funcao_descreve_comentario; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ifk_funcao_descreve_comentario ON public.seg_comentario_funcao USING btree (id_funcao);


--
-- TOC entry 4312 (class 1259 OID 44358)
-- Name: ifk_funcao_exerce_usuario; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ifk_funcao_exerce_usuario ON public.seg_usuario_funcao_interna USING btree (id_funcao_interna);


--
-- TOC entry 3831 (class 1259 OID 44359)
-- Name: ifk_funcionario_cliente_funcio; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ifk_funcionario_cliente_funcio ON public.seg_cliente_funcionario USING btree (id_funcionario);


--
-- TOC entry 3723 (class 1259 OID 44360)
-- Name: ifk_funcionario_funcao_realiza; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ifk_funcionario_funcao_realiza ON public.seg_aso USING btree (id_cliente_funcao_funcionario);


--
-- TOC entry 3825 (class 1259 OID 44361)
-- Name: ifk_funcionario_participa_clie; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ifk_funcionario_participa_clie ON public.seg_cliente_funcao_funcionario USING btree (id_funcionario);


--
-- TOC entry 4061 (class 1259 OID 44362)
-- Name: ifk_ghe_agente_controla_epi; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ifk_ghe_agente_controla_epi ON public.seg_ghe_fonte_agente_epi USING btree (id_ghe_fonte_agente);


--
-- TOC entry 4043 (class 1259 OID 44363)
-- Name: ifk_ghe_expoem_fonte; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ifk_ghe_expoem_fonte ON public.seg_ghe_fonte USING btree (id_ghe);


--
-- TOC entry 4068 (class 1259 OID 44364)
-- Name: ifk_ghe_fonte_agente_contempla; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ifk_ghe_fonte_agente_contempla ON public.seg_ghe_fonte_agente_exame USING btree (id_ghe_fonte_agente);


--
-- TOC entry 4166 (class 1259 OID 44365)
-- Name: ifk_ghe_fonte_agente_exame_aso; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ifk_ghe_fonte_agente_exame_aso ON public.seg_movimento USING btree (id_ghe_fonte_agente_exame_aso);


--
-- TOC entry 4074 (class 1259 OID 44366)
-- Name: ifk_ghe_fonte_agente_exame_con; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ifk_ghe_fonte_agente_exame_con ON public.seg_ghe_fonte_agente_exame_aso USING btree (id_ghe_fonte_agente_exame);


--
-- TOC entry 4108 (class 1259 OID 44367)
-- Name: ifk_ghe_fonte_agente_exame_ide; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ifk_ghe_fonte_agente_exame_ide ON public.seg_ghe_setor_cliente_funcao_exame USING btree (id_ghe_fonte_agente_exame);


--
-- TOC entry 4051 (class 1259 OID 44368)
-- Name: ifk_ghe_fonte_especifica_agent; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ifk_ghe_fonte_especifica_agent ON public.seg_ghe_fonte_agente USING btree (id_ghe_fonte);


--
-- TOC entry 4197 (class 1259 OID 44369)
-- Name: ifk_ghe_representa_norma; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ifk_ghe_representa_norma ON public.seg_norma_ghe USING btree (id_ghe);


--
-- TOC entry 4100 (class 1259 OID 44370)
-- Name: ifk_ghe_setor_cliente_funcao_i; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ifk_ghe_setor_cliente_funcao_i ON public.seg_ghe_setor_cliente_funcao USING btree (id_comentario_funcao);


--
-- TOC entry 4109 (class 1259 OID 44371)
-- Name: ifk_ghe_setor_cliente_funcao_r; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ifk_ghe_setor_cliente_funcao_r ON public.seg_ghe_setor_cliente_funcao_exame USING btree (id_ghe_setor_cliente_funcao);


--
-- TOC entry 4101 (class 1259 OID 44372)
-- Name: ifk_ghe_setor_participa_client; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ifk_ghe_setor_participa_client ON public.seg_ghe_setor_cliente_funcao USING btree (id_ghe_setor);


--
-- TOC entry 4093 (class 1259 OID 44373)
-- Name: ifk_ghe_tem_setor; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ifk_ghe_tem_setor ON public.seg_ghe_setor USING btree (id_ghe);


--
-- TOC entry 4089 (class 1259 OID 44374)
-- Name: ifk_ghefonteagentexame_partici; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ifk_ghefonteagentexame_partici ON public.seg_ghe_fonte_agente_exame_periodicidade USING btree (id_ghe_fonte_agente_exame);


--
-- TOC entry 4008 (class 1259 OID 44375)
-- Name: ifk_hospital_atende_estudo; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ifk_hospital_atende_estudo ON public.seg_estudo_hospital USING btree (id_hospital);


--
-- TOC entry 4134 (class 1259 OID 44376)
-- Name: ifk_material_hospitalar_utiliz; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ifk_material_hospitalar_utiliz ON public.seg_material_hospitalar_estudo USING btree (id_material_hospitalar);


--
-- TOC entry 4255 (class 1259 OID 44377)
-- Name: ifk_medico_aprova_relanual; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ifk_medico_aprova_relanual ON public.seg_relanual USING btree (id_medico_coordenador);


--
-- TOC entry 3724 (class 1259 OID 44378)
-- Name: ifk_medico_coordena_aso; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ifk_medico_coordena_aso ON public.seg_aso USING btree (id_medico_coordenador);


--
-- TOC entry 3776 (class 1259 OID 44379)
-- Name: ifk_medico_coordena_cliente; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ifk_medico_coordena_cliente ON public.seg_cliente USING btree (id_medico);


--
-- TOC entry 3725 (class 1259 OID 44380)
-- Name: ifk_medico_examina_aso; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ifk_medico_examina_aso ON public.seg_aso USING btree (id_medico_examinador);


--
-- TOC entry 4014 (class 1259 OID 44381)
-- Name: ifk_medico_examina_estudo; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ifk_medico_examina_estudo ON public.seg_estudo_medico USING btree (id_medico);


--
-- TOC entry 4075 (class 1259 OID 44382)
-- Name: ifk_medico_ghe_fonte_agente_ex; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ifk_medico_ghe_fonte_agente_ex ON public.seg_ghe_fonte_agente_exame_aso USING btree (id_medico);


--
-- TOC entry 4144 (class 1259 OID 44383)
-- Name: ifk_medico_medicoexame; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ifk_medico_medicoexame ON public.seg_medico_exame USING btree (id_medico);


--
-- TOC entry 4158 (class 1259 OID 44384)
-- Name: ifk_monitoramento_agente; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ifk_monitoramento_agente ON public.seg_monitoramento_ghefonteagente USING btree (id_monitoramento);


--
-- TOC entry 3959 (class 1259 OID 44385)
-- Name: ifk_monitoramento_epi; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ifk_monitoramento_epi ON public.seg_epi_monitoramento USING btree (id_monitoramento_ghefonteagente);


--
-- TOC entry 3977 (class 1259 OID 44386)
-- Name: ifk_monitoramento_esocial; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ifk_monitoramento_esocial ON public.seg_esocial_monitoramento USING btree (id_monitoramento);


--
-- TOC entry 4167 (class 1259 OID 44387)
-- Name: ifk_mov_pertence_nfe; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ifk_mov_pertence_nfe ON public.seg_movimento USING btree (id_nfe);


--
-- TOC entry 4185 (class 1259 OID 44388)
-- Name: ifk_nfe_contem_plano_forma; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ifk_nfe_contem_plano_forma ON public.seg_nfe USING btree (id_plano_forma);


--
-- TOC entry 3896 (class 1259 OID 44389)
-- Name: ifk_nfe_gera_crc; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ifk_nfe_gera_crc ON public.seg_crc USING btree (id_nfe);


--
-- TOC entry 4198 (class 1259 OID 44390)
-- Name: ifk_norma_representa_ghe; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ifk_norma_representa_ghe ON public.seg_norma_ghe USING btree (id_norma);


--
-- TOC entry 4318 (class 1259 OID 44391)
-- Name: ifk_perfil_participa_usuario; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ifk_perfil_participa_usuario ON public.seg_usuario_perfil USING btree (id_perfil);


--
-- TOC entry 4210 (class 1259 OID 44392)
-- Name: ifk_perfil_tem_dominio_acao; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ifk_perfil_tem_dominio_acao ON public.seg_perfil_dominio_acao USING btree (id_perfil);


--
-- TOC entry 3821 (class 1259 OID 44393)
-- Name: ifk_periodiciade_atendido_clie; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ifk_periodiciade_atendido_clie ON public.seg_cliente_funcao_exame_periodicidade USING btree (id_periodicidade);


--
-- TOC entry 4090 (class 1259 OID 44394)
-- Name: ifk_periodicidade_participa_gh; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ifk_periodicidade_participa_gh ON public.seg_ghe_fonte_agente_exame_periodicidade USING btree (id_periodicidade);


--
-- TOC entry 4220 (class 1259 OID 44395)
-- Name: ifk_plano_aceita_forma; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ifk_plano_aceita_forma ON public.seg_plano_forma USING btree (id_plano);


--
-- TOC entry 4203 (class 1259 OID 44396)
-- Name: ifk_plano_divide_parcelas; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ifk_plano_divide_parcelas ON public.seg_parcela USING btree (id_plano);


--
-- TOC entry 4038 (class 1259 OID 44397)
-- Name: ifk_ppra_contem_ghe; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ifk_ppra_contem_ghe ON public.seg_ghe USING btree (id_estudo);


--
-- TOC entry 3871 (class 1259 OID 44398)
-- Name: ifk_prestador_estabelece_contr; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ifk_prestador_estabelece_contr ON public.seg_contrato USING btree (id_cliente_prestador);


--
-- TOC entry 3807 (class 1259 OID 44399)
-- Name: ifk_prestador_presta_cliente_f; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ifk_prestador_presta_cliente_f ON public.seg_cliente_funcao_exame_aso USING btree (id_prestador);


--
-- TOC entry 4076 (class 1259 OID 44400)
-- Name: ifk_prestador_presta_ghe_fonte; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ifk_prestador_presta_ghe_fonte ON public.seg_ghe_fonte_agente_exame_aso USING btree (id_prestador);


--
-- TOC entry 4230 (class 1259 OID 44401)
-- Name: ifk_produto_contem_contrato; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ifk_produto_contem_contrato ON public.seg_produto_contrato USING btree (id_produto);


--
-- TOC entry 4168 (class 1259 OID 44402)
-- Name: ifk_produto_contrato_participa; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ifk_produto_contrato_participa ON public.seg_movimento USING btree (id_produto_contrato);


--
-- TOC entry 3942 (class 1259 OID 44403)
-- Name: ifk_prontuario_email; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ifk_prontuario_email ON public.seg_email USING btree (id_prontuario);


--
-- TOC entry 3928 (class 1259 OID 44404)
-- Name: ifk_protocolo_completa_documen; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ifk_protocolo_completa_documen ON public.seg_documento_protocolo USING btree (id_protocolo);


--
-- TOC entry 3808 (class 1259 OID 44405)
-- Name: ifk_protocolo_entrega_cliente_; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ifk_protocolo_entrega_cliente_ ON public.seg_cliente_funcao_exame_aso USING btree (id_protocolo);


--
-- TOC entry 4077 (class 1259 OID 44406)
-- Name: ifk_protocolo_entrega_ghe_font; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ifk_protocolo_entrega_ghe_font ON public.seg_ghe_fonte_agente_exame_aso USING btree (id_protocolo);


--
-- TOC entry 3726 (class 1259 OID 44407)
-- Name: ifk_protocolo_envia_atendiment; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ifk_protocolo_envia_atendiment ON public.seg_aso USING btree (id_protocolo);


--
-- TOC entry 3993 (class 1259 OID 44408)
-- Name: ifk_protocolo_envia_estudo; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ifk_protocolo_envia_estudo ON public.seg_estudo USING btree (id_protocolo);


--
-- TOC entry 4169 (class 1259 OID 44409)
-- Name: ifk_protocolo_envia_produto; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ifk_protocolo_envia_produto ON public.seg_movimento USING btree (id_protocolo);


--
-- TOC entry 4262 (class 1259 OID 44410)
-- Name: ifk_relanaul_tem_item; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ifk_relanaul_tem_item ON public.seg_relanual_item USING btree (id_relanual);


--
-- TOC entry 4269 (class 1259 OID 44411)
-- Name: ifk_relatorio_relatorio_exame; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ifk_relatorio_relatorio_exame ON public.seg_relatorio_exame USING btree (id_relatorio);


--
-- TOC entry 3994 (class 1259 OID 44412)
-- Name: ifk_rep_cli_representa_estudo; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ifk_rep_cli_representa_estudo ON public.seg_estudo USING btree (id_seg_rep_cli);


--
-- TOC entry 4275 (class 1259 OID 44413)
-- Name: ifk_rep_representa_cli; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ifk_rep_representa_cli ON public.seg_rep_cli USING btree (id_vend);


--
-- TOC entry 4280 (class 1259 OID 44414)
-- Name: ifk_rev_versiona_estudo; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ifk_rev_versiona_estudo ON public.seg_revisao USING btree (id_estudo);


--
-- TOC entry 4328 (class 1259 OID 44415)
-- Name: ifk_revisao_localiza_estudo; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ifk_revisao_localiza_estudo ON public.seg_versao USING btree (id_estudo);


--
-- TOC entry 3709 (class 1259 OID 44416)
-- Name: ifk_risco_compoe_agente; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ifk_risco_compoe_agente ON public.seg_agente USING btree (id_risco);


--
-- TOC entry 4291 (class 1259 OID 44417)
-- Name: ifk_sala_atende_exame; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ifk_sala_atende_exame ON public.seg_sala_exame USING btree (id_exame);


--
-- TOC entry 4286 (class 1259 OID 44418)
-- Name: ifk_sala_empresa; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ifk_sala_empresa ON public.seg_sala USING btree (id_empresa);


--
-- TOC entry 3809 (class 1259 OID 44419)
-- Name: ifk_sala_exame_atende_cliente_; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ifk_sala_exame_atende_cliente_ ON public.seg_cliente_funcao_exame_aso USING btree (id_sala_exame);


--
-- TOC entry 4078 (class 1259 OID 44420)
-- Name: ifk_sala_exame_atende_ghe_font; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ifk_sala_exame_atende_ghe_font ON public.seg_ghe_fonte_agente_exame_aso USING btree (id_sala_exame);


--
-- TOC entry 3777 (class 1259 OID 44421)
-- Name: ifk_seg_cidade_ibge_identifica; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ifk_seg_cidade_ibge_identifica ON public.seg_cliente USING btree (id_cidade_ibge);


--
-- TOC entry 3949 (class 1259 OID 44422)
-- Name: ifk_seg_cidade_identifica_cida; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ifk_seg_cidade_identifica_cida ON public.seg_empresa USING btree (id_cidade_ibge);


--
-- TOC entry 4324 (class 1259 OID 44423)
-- Name: ifk_seg_cidade_vendedor; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ifk_seg_cidade_vendedor ON public.seg_vendedor USING btree (id_cidade_ibge);


--
-- TOC entry 4170 (class 1259 OID 44424)
-- Name: ifk_seg_cliente_funcao_exame_a; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ifk_seg_cliente_funcao_exame_a ON public.seg_movimento USING btree (id_cliente_funcao_exame_aso);


--
-- TOC entry 4094 (class 1259 OID 44425)
-- Name: ifk_setor_tem_ghe; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ifk_setor_tem_ghe ON public.seg_ghe_setor USING btree (id_setor);


--
-- TOC entry 4052 (class 1259 OID 44426)
-- Name: ifk_soma_prioriza_ghe_fonte_ag; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ifk_soma_prioriza_ghe_fonte_ag ON public.seg_ghe_fonte_agente USING btree (id_grad_soma);


--
-- TOC entry 3860 (class 1259 OID 44427)
-- Name: ifk_tipo_contato_contato; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ifk_tipo_contato_contato ON public.seg_contato USING btree (id_tipo_contato);


--
-- TOC entry 3727 (class 1259 OID 44428)
-- Name: ifk_usuario_altera_aso; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ifk_usuario_altera_aso ON public.seg_aso USING btree (id_ult_usuario_alteracao);


--
-- TOC entry 3995 (class 1259 OID 44429)
-- Name: ifk_usuario_autoriza_estudo; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ifk_usuario_autoriza_estudo ON public.seg_estudo USING btree (id_engenheiro);


--
-- TOC entry 3897 (class 1259 OID 44430)
-- Name: ifk_usuario_baixa_crc; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ifk_usuario_baixa_crc ON public.seg_crc USING btree (id_usuario_baixa);


--
-- TOC entry 3728 (class 1259 OID 44431)
-- Name: ifk_usuario_cancela_aso; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ifk_usuario_cancela_aso ON public.seg_aso USING btree (id_usuario_cancelamento);


--
-- TOC entry 3898 (class 1259 OID 44432)
-- Name: ifk_usuario_cancela_cobranca; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ifk_usuario_cancela_cobranca ON public.seg_crc USING btree (id_usuario_cancela);


--
-- TOC entry 4186 (class 1259 OID 44433)
-- Name: ifk_usuario_cancela_nfe; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ifk_usuario_cancela_nfe ON public.seg_nfe USING btree (id_usuario_cancela);


--
-- TOC entry 4243 (class 1259 OID 44434)
-- Name: ifk_usuario_cancela_protocolo; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ifk_usuario_cancela_protocolo ON public.seg_protocolo USING btree (id_usuario_cancelou);


--
-- TOC entry 3729 (class 1259 OID 44435)
-- Name: ifk_usuario_cria_aso; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ifk_usuario_cria_aso ON public.seg_aso USING btree (id_usuario_criador);


--
-- TOC entry 3996 (class 1259 OID 44436)
-- Name: ifk_usuario_cria_estudo; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ifk_usuario_cria_estudo ON public.seg_estudo USING btree (id_tecno);


--
-- TOC entry 4187 (class 1259 OID 44437)
-- Name: ifk_usuario_cria_notafiscal; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ifk_usuario_cria_notafiscal ON public.seg_nfe USING btree (id_usuario_criador);


--
-- TOC entry 4237 (class 1259 OID 44438)
-- Name: ifk_usuario_cria_prontuario; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ifk_usuario_cria_prontuario ON public.seg_prontuario USING btree (id_usuario);


--
-- TOC entry 3899 (class 1259 OID 44439)
-- Name: ifk_usuario_desdobra_cobranca; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ifk_usuario_desdobra_cobranca ON public.seg_crc USING btree (id_usuario_desdobra);


--
-- TOC entry 4300 (class 1259 OID 44440)
-- Name: ifk_usuario_e_coordenador; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ifk_usuario_e_coordenador ON public.seg_usuario USING btree (id_medico);


--
-- TOC entry 3943 (class 1259 OID 44441)
-- Name: ifk_usuario_email; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ifk_usuario_email ON public.seg_email USING btree (id_usuario);


--
-- TOC entry 3900 (class 1259 OID 44442)
-- Name: ifk_usuario_estorna_; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ifk_usuario_estorna_ ON public.seg_crc USING btree (id_usuario_estorna);


--
-- TOC entry 4313 (class 1259 OID 44443)
-- Name: ifk_usuario_exerce_funcao; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ifk_usuario_exerce_funcao ON public.seg_usuario_funcao_interna USING btree (id_usuario);


--
-- TOC entry 3730 (class 1259 OID 44444)
-- Name: ifk_usuario_finaliza_aso; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ifk_usuario_finaliza_aso ON public.seg_aso USING btree (id_usuario_finalizador);


--
-- TOC entry 3810 (class 1259 OID 44445)
-- Name: ifk_usuario_finaliza_cliente_f; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ifk_usuario_finaliza_cliente_f ON public.seg_cliente_funcao_exame_aso USING btree (id_usuario);


--
-- TOC entry 4244 (class 1259 OID 44446)
-- Name: ifk_usuario_finaliza_protocolo; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ifk_usuario_finaliza_protocolo ON public.seg_protocolo USING btree (id_usuario_finalizou);


--
-- TOC entry 4079 (class 1259 OID 44447)
-- Name: ifk_usuario_finaliza_seg_ghe_f; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ifk_usuario_finaliza_seg_ghe_f ON public.seg_ghe_fonte_agente_exame_aso USING btree (id_usuario);


--
-- TOC entry 4171 (class 1259 OID 44448)
-- Name: ifk_usuario_gera_movimento; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ifk_usuario_gera_movimento ON public.seg_movimento USING btree (id_usuario);


--
-- TOC entry 4256 (class 1259 OID 44449)
-- Name: ifk_usuario_gera_relanual; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ifk_usuario_gera_relanual ON public.seg_relanual USING btree (id_usuario);


--
-- TOC entry 3901 (class 1259 OID 44450)
-- Name: ifk_usuario_grava_cobranca; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ifk_usuario_grava_cobranca ON public.seg_crc USING btree (id_usuario_criador);


--
-- TOC entry 4245 (class 1259 OID 44451)
-- Name: ifk_usuario_grava_protocolo; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ifk_usuario_grava_protocolo ON public.seg_protocolo USING btree (id_usuario_gerou);


--
-- TOC entry 4151 (class 1259 OID 44452)
-- Name: ifk_usuario_monitoramento; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ifk_usuario_monitoramento ON public.seg_monitoramento USING btree (id_usuario);


--
-- TOC entry 4319 (class 1259 OID 44453)
-- Name: ifk_usuario_participa_perfil; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ifk_usuario_participa_perfil ON public.seg_usuario_perfil USING btree (id_usuario);


--
-- TOC entry 3902 (class 1259 OID 44454)
-- Name: ifk_usuario_prorroga_crc; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ifk_usuario_prorroga_crc ON public.seg_crc USING btree (id_usuario_prorrogacao);


--
-- TOC entry 3888 (class 1259 OID 44455)
-- Name: ifk_usuario_realiza_correcao; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ifk_usuario_realiza_correcao ON public.seg_correcao_estudo USING btree (id_usuario);


--
-- TOC entry 4301 (class 1259 OID 44456)
-- Name: ifk_usuario_trabalha_empresa; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ifk_usuario_trabalha_empresa ON public.seg_usuario USING btree (id_empresa);


--
-- TOC entry 4307 (class 1259 OID 44457)
-- Name: ifk_usuario_usuariocliente; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ifk_usuario_usuariocliente ON public.seg_usuario_cliente USING btree (id_usuario);


--
-- TOC entry 4270 (class 1259 OID 44458)
-- Name: relatorio_exame_id_exame_fk; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX relatorio_exame_id_exame_fk ON public.seg_relatorio_exame USING btree (id_exame);


--
-- TOC entry 4271 (class 1259 OID 44459)
-- Name: relatorio_exame_id_relatorio_fk; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX relatorio_exame_id_relatorio_fk ON public.seg_relatorio_exame USING btree (id_relatorio);


--
-- TOC entry 3704 (class 1259 OID 44460)
-- Name: seg_acesso_login_ip_unq; Type: INDEX; Schema: public; Owner: postgres
--

CREATE UNIQUE INDEX seg_acesso_login_ip_unq ON public.seg_acesso USING btree (login, ip);


--
-- TOC entry 3710 (class 1259 OID 44461)
-- Name: seg_agente_id_risco_fk; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX seg_agente_id_risco_fk ON public.seg_agente USING btree (id_risco);


--
-- TOC entry 3716 (class 1259 OID 44462)
-- Name: seg_arquivo_anexo_id_aso_idx; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX seg_arquivo_anexo_id_aso_idx ON public.seg_arquivo_anexo USING btree (id_aso);


--
-- TOC entry 3748 (class 1259 OID 44463)
-- Name: seg_aso_agente_risco_id_aso_fk; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX seg_aso_agente_risco_id_aso_fk ON public.seg_aso_agente_risco USING btree (id_aso);


--
-- TOC entry 3731 (class 1259 OID 44464)
-- Name: seg_aso_data_aso_idx; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX seg_aso_data_aso_idx ON public.seg_aso USING btree (data_aso);


--
-- TOC entry 3732 (class 1259 OID 44465)
-- Name: seg_aso_fkindex12; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX seg_aso_fkindex12 ON public.seg_aso USING btree (id_centrocusto);


--
-- TOC entry 3750 (class 1259 OID 44466)
-- Name: seg_aso_ghe_setor_id_aso_fk; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX seg_aso_ghe_setor_id_aso_fk ON public.seg_aso_ghe_setor USING btree (id_aso);


--
-- TOC entry 3733 (class 1259 OID 44467)
-- Name: seg_aso_id_centrocusto_fk; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX seg_aso_id_centrocusto_fk ON public.seg_aso USING btree (id_centrocusto);


--
-- TOC entry 3734 (class 1259 OID 44468)
-- Name: seg_aso_id_cliente_funcao_funcionario_fk; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX seg_aso_id_cliente_funcao_funcionario_fk ON public.seg_aso USING btree (id_cliente_funcao_funcionario);


--
-- TOC entry 3735 (class 1259 OID 44469)
-- Name: seg_aso_id_empresa_fk; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX seg_aso_id_empresa_fk ON public.seg_aso USING btree (id_empresa);


--
-- TOC entry 3736 (class 1259 OID 44470)
-- Name: seg_aso_id_medico_coordenador_fk; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX seg_aso_id_medico_coordenador_fk ON public.seg_aso USING btree (id_medico_coordenador);


--
-- TOC entry 3737 (class 1259 OID 44471)
-- Name: seg_aso_id_medico_fk; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX seg_aso_id_medico_fk ON public.seg_aso USING btree (id_medico_examinador);


--
-- TOC entry 3738 (class 1259 OID 44472)
-- Name: seg_aso_id_periodicidade_fk; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX seg_aso_id_periodicidade_fk ON public.seg_aso USING btree (id_periodicidade);


--
-- TOC entry 3739 (class 1259 OID 44473)
-- Name: seg_aso_id_protocolo_fk; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX seg_aso_id_protocolo_fk ON public.seg_aso USING btree (id_protocolo);


--
-- TOC entry 3740 (class 1259 OID 44474)
-- Name: seg_aso_id_usuario_alteracao_fk; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX seg_aso_id_usuario_alteracao_fk ON public.seg_aso USING btree (id_ult_usuario_alteracao);


--
-- TOC entry 3741 (class 1259 OID 44475)
-- Name: seg_aso_id_usuario_cancelamento_fk; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX seg_aso_id_usuario_cancelamento_fk ON public.seg_aso USING btree (id_usuario_cancelamento);


--
-- TOC entry 3742 (class 1259 OID 44476)
-- Name: seg_aso_id_usuario_criador_fk; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX seg_aso_id_usuario_criador_fk ON public.seg_aso USING btree (id_usuario_criador);


--
-- TOC entry 3743 (class 1259 OID 44477)
-- Name: seg_aso_id_usuaro_finalizador_fk; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX seg_aso_id_usuaro_finalizador_fk ON public.seg_aso USING btree (id_usuario_finalizador);


--
-- TOC entry 3746 (class 1259 OID 44478)
-- Name: seg_aso_situacao_idx; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX seg_aso_situacao_idx ON public.seg_aso USING btree (situacao);


--
-- TOC entry 3755 (class 1259 OID 44479)
-- Name: seg_atividade_cronograma_id_atividade_id_atividade_fk; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX seg_atividade_cronograma_id_atividade_id_atividade_fk ON public.seg_atividade_cronograma USING btree (id_atividade);


--
-- TOC entry 3756 (class 1259 OID 44480)
-- Name: seg_atividade_cronograma_id_cronograma_fk; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX seg_atividade_cronograma_id_cronograma_fk ON public.seg_atividade_cronograma USING btree (id_cronograma);


--
-- TOC entry 3762 (class 1259 OID 44481)
-- Name: seg_centrocusto_id_cliente_fk; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX seg_centrocusto_id_cliente_fk ON public.seg_centrocusto USING btree (id_cliente);


--
-- TOC entry 3765 (class 1259 OID 44482)
-- Name: seg_cidade_ibge_codigo_idx; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX seg_cidade_ibge_codigo_idx ON public.seg_cidade_ibge USING btree (codigo);


--
-- TOC entry 3770 (class 1259 OID 44483)
-- Name: seg_cli_cnae_id_cliente_fk; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX seg_cli_cnae_id_cliente_fk ON public.seg_cli_cnae USING btree (id_cliente);


--
-- TOC entry 3771 (class 1259 OID 44484)
-- Name: seg_cli_cnae_id_cnae_fk; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX seg_cli_cnae_id_cnae_fk ON public.seg_cli_cnae USING btree (id_cnae);


--
-- TOC entry 3788 (class 1259 OID 44485)
-- Name: seg_cliente_arquivo_id_arquivo_fk; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX seg_cliente_arquivo_id_arquivo_fk ON public.seg_cliente_arquivo USING btree (id_arquivo);


--
-- TOC entry 3789 (class 1259 OID 44486)
-- Name: seg_cliente_arquivo_id_cliente_fk; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX seg_cliente_arquivo_id_cliente_fk ON public.seg_cliente_arquivo USING btree (id_cliente);


--
-- TOC entry 3790 (class 1259 OID 44487)
-- Name: seg_cliente_arquivo_id_cliente_id_arquivo_unique; Type: INDEX; Schema: public; Owner: postgres
--

CREATE UNIQUE INDEX seg_cliente_arquivo_id_cliente_id_arquivo_unique ON public.seg_cliente_arquivo USING btree (id_arquivo, id_cliente);


--
-- TOC entry 3811 (class 1259 OID 44488)
-- Name: seg_cliente_funcao_exame_aso_id_aso_fk; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX seg_cliente_funcao_exame_aso_id_aso_fk ON public.seg_cliente_funcao_exame_aso USING btree (id_aso);


--
-- TOC entry 3812 (class 1259 OID 44489)
-- Name: seg_cliente_funcao_exame_aso_id_cliente_funcao_exame_fk; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX seg_cliente_funcao_exame_aso_id_cliente_funcao_exame_fk ON public.seg_cliente_funcao_exame_aso USING btree (id_cliente_funcao_exame);


--
-- TOC entry 3813 (class 1259 OID 44490)
-- Name: seg_cliente_funcao_exame_aso_id_medico_fk; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX seg_cliente_funcao_exame_aso_id_medico_fk ON public.seg_cliente_funcao_exame_aso USING btree (id_medico);


--
-- TOC entry 3814 (class 1259 OID 44491)
-- Name: seg_cliente_funcao_exame_aso_id_prestador_fk; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX seg_cliente_funcao_exame_aso_id_prestador_fk ON public.seg_cliente_funcao_exame_aso USING btree (id_prestador);


--
-- TOC entry 3815 (class 1259 OID 44492)
-- Name: seg_cliente_funcao_exame_aso_id_protocolo_fk; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX seg_cliente_funcao_exame_aso_id_protocolo_fk ON public.seg_cliente_funcao_exame_aso USING btree (id_protocolo);


--
-- TOC entry 3816 (class 1259 OID 44493)
-- Name: seg_cliente_funcao_exame_aso_id_sala_exame_fk; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX seg_cliente_funcao_exame_aso_id_sala_exame_fk ON public.seg_cliente_funcao_exame_aso USING btree (id_sala_exame);


--
-- TOC entry 3817 (class 1259 OID 44494)
-- Name: seg_cliente_funcao_exame_aso_id_usuario_fk; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX seg_cliente_funcao_exame_aso_id_usuario_fk ON public.seg_cliente_funcao_exame_aso USING btree (id_usuario);


--
-- TOC entry 3801 (class 1259 OID 44495)
-- Name: seg_cliente_funcao_exame_id_cliente_funcao_fk; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX seg_cliente_funcao_exame_id_cliente_funcao_fk ON public.seg_cliente_funcao_exame USING btree (id_cliente_funcao);


--
-- TOC entry 3802 (class 1259 OID 44496)
-- Name: seg_cliente_funcao_exame_id_exame_fk; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX seg_cliente_funcao_exame_id_exame_fk ON public.seg_cliente_funcao_exame USING btree (id_exame);


--
-- TOC entry 3822 (class 1259 OID 44497)
-- Name: seg_cliente_funcao_exame_id_periodicidade_fk; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX seg_cliente_funcao_exame_id_periodicidade_fk ON public.seg_cliente_funcao_exame_periodicidade USING btree (id_periodicidade);


--
-- TOC entry 3823 (class 1259 OID 44498)
-- Name: seg_cliente_funcao_exame_periodicidade_id_cliente_funcao_exame_; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX seg_cliente_funcao_exame_periodicidade_id_cliente_funcao_exame_ ON public.seg_cliente_funcao_exame_periodicidade USING btree (id_cliente_funcao_exame);


--
-- TOC entry 3826 (class 1259 OID 44499)
-- Name: seg_cliente_funcao_funcionario_id_funcionario_fk; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX seg_cliente_funcao_funcionario_id_funcionario_fk ON public.seg_cliente_funcao_funcionario USING btree (id_funcionario);


--
-- TOC entry 3827 (class 1259 OID 44500)
-- Name: seg_cliente_funcao_funcionario_id_seg_cliente_funcao_fk; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX seg_cliente_funcao_funcionario_id_seg_cliente_funcao_fk ON public.seg_cliente_funcao_funcionario USING btree (id_seg_cliente_funcao);


--
-- TOC entry 3795 (class 1259 OID 44501)
-- Name: seg_cliente_funcao_id_cliente_fk; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX seg_cliente_funcao_id_cliente_fk ON public.seg_cliente_funcao USING btree (id_cliente);


--
-- TOC entry 3796 (class 1259 OID 44502)
-- Name: seg_cliente_funcao_id_funcao_fk; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX seg_cliente_funcao_id_funcao_fk ON public.seg_cliente_funcao USING btree (id_funcao);


--
-- TOC entry 3832 (class 1259 OID 44503)
-- Name: seg_cliente_funcionario_id_cliente_fk; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX seg_cliente_funcionario_id_cliente_fk ON public.seg_cliente_funcionario USING btree (id_cliente);


--
-- TOC entry 3833 (class 1259 OID 44504)
-- Name: seg_cliente_funcionario_id_funcionario_fk; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX seg_cliente_funcionario_id_funcionario_fk ON public.seg_cliente_funcionario USING btree (id_funcionario);


--
-- TOC entry 3778 (class 1259 OID 44505)
-- Name: seg_cliente_id_cidade_ibge_cobranca_fk; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX seg_cliente_id_cidade_ibge_cobranca_fk ON public.seg_cliente USING btree (id_cidade_ibge_cobranca);


--
-- TOC entry 3779 (class 1259 OID 44506)
-- Name: seg_cliente_id_cidade_ibge_fk; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX seg_cliente_id_cidade_ibge_fk ON public.seg_cliente USING btree (id_cidade_ibge);


--
-- TOC entry 3780 (class 1259 OID 44507)
-- Name: seg_cliente_id_cliente_credenciado_fk; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX seg_cliente_id_cliente_credenciado_fk ON public.seg_cliente USING btree (id_cliente_credenciado);


--
-- TOC entry 3781 (class 1259 OID 44508)
-- Name: seg_cliente_id_cliente_matriz_fk; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX seg_cliente_id_cliente_matriz_fk ON public.seg_cliente USING btree (id_cliente_matriz);


--
-- TOC entry 3782 (class 1259 OID 44509)
-- Name: seg_cliente_id_medico_fk; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX seg_cliente_id_medico_fk ON public.seg_cliente USING btree (id_medico);


--
-- TOC entry 3783 (class 1259 OID 44510)
-- Name: seg_cliente_id_ramo_fk; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX seg_cliente_id_ramo_fk ON public.seg_cliente USING btree (id_ramo);


--
-- TOC entry 3844 (class 1259 OID 44511)
-- Name: seg_cnae_estudo_id_cnae_fk; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX seg_cnae_estudo_id_cnae_fk ON public.seg_cnae_estudo USING btree (id_cnae);


--
-- TOC entry 3845 (class 1259 OID 44512)
-- Name: seg_cnae_estudo_id_estudo_fk; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX seg_cnae_estudo_id_estudo_fk ON public.seg_cnae_estudo USING btree (id_estudo);


--
-- TOC entry 3849 (class 1259 OID 44513)
-- Name: seg_comentario_funcao_id_funcao_fk; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX seg_comentario_funcao_id_funcao_fk ON public.seg_comentario_funcao USING btree (id_funcao);


--
-- TOC entry 3855 (class 1259 OID 44514)
-- Name: seg_conta_id_banco_fk; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX seg_conta_id_banco_fk ON public.seg_conta USING btree (id_banco);


--
-- TOC entry 3861 (class 1259 OID 44515)
-- Name: seg_contato_id_cidade; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX seg_contato_id_cidade ON public.seg_contato USING btree (id_cidade);


--
-- TOC entry 3862 (class 1259 OID 44516)
-- Name: seg_contato_id_cliente_fk; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX seg_contato_id_cliente_fk ON public.seg_contato USING btree (id_cliente);


--
-- TOC entry 3863 (class 1259 OID 44517)
-- Name: seg_contato_id_tipo_contato_fk; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX seg_contato_id_tipo_contato_fk ON public.seg_contato USING btree (id_tipo_contato);


--
-- TOC entry 3882 (class 1259 OID 44518)
-- Name: seg_contrato_exame_id_contrato_fk; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX seg_contrato_exame_id_contrato_fk ON public.seg_contrato_exame USING btree (id_contrato);


--
-- TOC entry 3883 (class 1259 OID 44519)
-- Name: seg_contrato_exame_id_exame_fk; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX seg_contrato_exame_id_exame_fk ON public.seg_contrato_exame USING btree (id_exame);


--
-- TOC entry 3886 (class 1259 OID 44520)
-- Name: seg_contrato_exame_unique; Type: INDEX; Schema: public; Owner: postgres
--

CREATE UNIQUE INDEX seg_contrato_exame_unique ON public.seg_contrato_exame USING btree (id_exame, id_contrato);


--
-- TOC entry 3872 (class 1259 OID 44521)
-- Name: seg_contrato_id_cliente_fk; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX seg_contrato_id_cliente_fk ON public.seg_contrato USING btree (id_cliente);


--
-- TOC entry 3873 (class 1259 OID 44522)
-- Name: seg_contrato_id_cliente_prestador_fk; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX seg_contrato_id_cliente_prestador_fk ON public.seg_contrato USING btree (id_cliente_prestador);


--
-- TOC entry 3874 (class 1259 OID 44523)
-- Name: seg_contrato_id_cliente_proposta_fk; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX seg_contrato_id_cliente_proposta_fk ON public.seg_contrato USING btree (id_cliente_proposta);


--
-- TOC entry 3875 (class 1259 OID 44524)
-- Name: seg_contrato_id_contrato_pai_fk; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX seg_contrato_id_contrato_pai_fk ON public.seg_contrato USING btree (id_contrato_pai);


--
-- TOC entry 3876 (class 1259 OID 44525)
-- Name: seg_contrato_id_contrato_raiz_fk; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX seg_contrato_id_contrato_raiz_fk ON public.seg_contrato USING btree (id_contrato_raiz);


--
-- TOC entry 3877 (class 1259 OID 44526)
-- Name: seg_contrato_id_empresa_fk; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX seg_contrato_id_empresa_fk ON public.seg_contrato USING btree (id_empresa);


--
-- TOC entry 3889 (class 1259 OID 44527)
-- Name: seg_correcao_estudo_id_estudo_fk; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX seg_correcao_estudo_id_estudo_fk ON public.seg_correcao_estudo USING btree (id_estudo);


--
-- TOC entry 3890 (class 1259 OID 44528)
-- Name: seg_correcao_estudo_id_usuario_fk; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX seg_correcao_estudo_id_usuario_fk ON public.seg_correcao_estudo USING btree (id_usuario);


--
-- TOC entry 3903 (class 1259 OID 44529)
-- Name: seg_crc_id_conta_fk; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX seg_crc_id_conta_fk ON public.seg_crc USING btree (id_conta);


--
-- TOC entry 3904 (class 1259 OID 44530)
-- Name: seg_crc_id_crc_desdobramento_fk; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX seg_crc_id_crc_desdobramento_fk ON public.seg_crc USING btree (id_crc_desdobramento);


--
-- TOC entry 3905 (class 1259 OID 44531)
-- Name: seg_crc_id_forma_fk; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX seg_crc_id_forma_fk ON public.seg_crc USING btree (id_forma);


--
-- TOC entry 3906 (class 1259 OID 44532)
-- Name: seg_crc_id_nfe_fk; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX seg_crc_id_nfe_fk ON public.seg_crc USING btree (id_nfe);


--
-- TOC entry 3907 (class 1259 OID 44533)
-- Name: seg_crc_id_usuario_baixa_fk; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX seg_crc_id_usuario_baixa_fk ON public.seg_crc USING btree (id_usuario_baixa);


--
-- TOC entry 3908 (class 1259 OID 44534)
-- Name: seg_crc_id_usuario_cancela_fk; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX seg_crc_id_usuario_cancela_fk ON public.seg_crc USING btree (id_usuario_cancela);


--
-- TOC entry 3909 (class 1259 OID 44535)
-- Name: seg_crc_id_usuario_criador_fk; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX seg_crc_id_usuario_criador_fk ON public.seg_crc USING btree (id_usuario_criador);


--
-- TOC entry 3910 (class 1259 OID 44536)
-- Name: seg_crc_id_usuario_desdobra_fk; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX seg_crc_id_usuario_desdobra_fk ON public.seg_crc USING btree (id_usuario_desdobra);


--
-- TOC entry 3911 (class 1259 OID 44537)
-- Name: seg_crc_id_usuario_estorna_fk; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX seg_crc_id_usuario_estorna_fk ON public.seg_crc USING btree (id_usuario_estorna);


--
-- TOC entry 3912 (class 1259 OID 44538)
-- Name: seg_crc_id_usuario_prorrogacao_fk; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX seg_crc_id_usuario_prorrogacao_fk ON public.seg_crc USING btree (id_usuario_prorrogacao);


--
-- TOC entry 3915 (class 1259 OID 44539)
-- Name: seg_crc_unique; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX seg_crc_unique ON public.seg_crc USING btree (id_conta, id_nfe);


--
-- TOC entry 3929 (class 1259 OID 44540)
-- Name: seg_documento_protocolo_id_aso_fk; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX seg_documento_protocolo_id_aso_fk ON public.seg_documento_protocolo USING btree (id_aso);


--
-- TOC entry 3930 (class 1259 OID 44541)
-- Name: seg_documento_protocolo_id_documento_fk; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX seg_documento_protocolo_id_documento_fk ON public.seg_documento_protocolo USING btree (id_documento);


--
-- TOC entry 3931 (class 1259 OID 44542)
-- Name: seg_documento_protocolo_id_protocolo_fk; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX seg_documento_protocolo_id_protocolo_fk ON public.seg_documento_protocolo USING btree (id_protocolo);


--
-- TOC entry 3938 (class 1259 OID 44543)
-- Name: seg_dominio_acao_id_acao_fk; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX seg_dominio_acao_id_acao_fk ON public.seg_dominio_acao USING btree (id_acao);


--
-- TOC entry 3939 (class 1259 OID 44544)
-- Name: seg_dominio_acao_id_dominio_fk; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX seg_dominio_acao_id_dominio_fk ON public.seg_dominio_acao USING btree (id_dominio);


--
-- TOC entry 3944 (class 1259 OID 44545)
-- Name: seg_email_id_usuario_fk; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX seg_email_id_usuario_fk ON public.seg_email USING btree (id_usuario);


--
-- TOC entry 3947 (class 1259 OID 44546)
-- Name: seg_email_prontuario_id_fk; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX seg_email_prontuario_id_fk ON public.seg_email USING btree (id_prontuario);


--
-- TOC entry 3950 (class 1259 OID 44547)
-- Name: seg_empresa_id_cidade_ibge_fk; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX seg_empresa_id_cidade_ibge_fk ON public.seg_empresa USING btree (id_cidade_ibge);


--
-- TOC entry 3951 (class 1259 OID 44548)
-- Name: seg_empresa_id_empresa_matriz_fk; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX seg_empresa_id_empresa_matriz_fk ON public.seg_empresa USING btree (id_empresa_matriz);


--
-- TOC entry 3960 (class 1259 OID 44549)
-- Name: seg_epi_monitoramento_id_epi_fk; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX seg_epi_monitoramento_id_epi_fk ON public.seg_epi_monitoramento USING btree (id_epi);


--
-- TOC entry 3961 (class 1259 OID 44550)
-- Name: seg_epi_monitoramento_id_monitoramento_ghe_fonteagente_fk; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX seg_epi_monitoramento_id_monitoramento_ghe_fonteagente_fk ON public.seg_epi_monitoramento USING btree (id_monitoramento_ghefonteagente);


--
-- TOC entry 3972 (class 1259 OID 44551)
-- Name: seg_esocial_lote_aso_id_aso_fk; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX seg_esocial_lote_aso_id_aso_fk ON public.seg_esocial_lote_aso USING btree (id_aso);


--
-- TOC entry 3973 (class 1259 OID 44552)
-- Name: seg_esocial_lote_aso_id_esocial_lote_fk; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX seg_esocial_lote_aso_id_esocial_lote_fk ON public.seg_esocial_lote_aso USING btree (id_esocial_lote);


--
-- TOC entry 3967 (class 1259 OID 44553)
-- Name: seg_esocial_lote_id_cliente_fk; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX seg_esocial_lote_id_cliente_fk ON public.seg_esocial_lote USING btree (id_cliente);


--
-- TOC entry 3978 (class 1259 OID 44554)
-- Name: seg_esocial_monitoramento_id_esocial_lote_fk; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX seg_esocial_monitoramento_id_esocial_lote_fk ON public.seg_esocial_monitoramento USING btree (id_esocial_lote);


--
-- TOC entry 3979 (class 1259 OID 44555)
-- Name: seg_esocial_monitoramento_id_monitoramento_fk; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX seg_esocial_monitoramento_id_monitoramento_fk ON public.seg_esocial_monitoramento USING btree (id_monitoramento);


--
-- TOC entry 3997 (class 1259 OID 44556)
-- Name: seg_estudo_cod_estudo_unique; Type: INDEX; Schema: public; Owner: postgres
--

CREATE UNIQUE INDEX seg_estudo_cod_estudo_unique ON public.seg_estudo USING btree (cod_estudo);


--
-- TOC entry 4009 (class 1259 OID 44557)
-- Name: seg_estudo_hospital_id_estudo_fk; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX seg_estudo_hospital_id_estudo_fk ON public.seg_estudo_hospital USING btree (id_estudo);


--
-- TOC entry 4010 (class 1259 OID 44558)
-- Name: seg_estudo_hospital_id_hospital_fk; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX seg_estudo_hospital_id_hospital_fk ON public.seg_estudo_hospital USING btree (id_hospital);


--
-- TOC entry 3998 (class 1259 OID 44559)
-- Name: seg_estudo_id_cliente_contratado_fk; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX seg_estudo_id_cliente_contratado_fk ON public.seg_estudo USING btree (id_cliente_contratado);


--
-- TOC entry 3999 (class 1259 OID 44560)
-- Name: seg_estudo_id_cronograma_unique; Type: INDEX; Schema: public; Owner: postgres
--

CREATE UNIQUE INDEX seg_estudo_id_cronograma_unique ON public.seg_estudo USING btree (id_cronograma);


--
-- TOC entry 4000 (class 1259 OID 44561)
-- Name: seg_estudo_id_engenheiro_fk; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX seg_estudo_id_engenheiro_fk ON public.seg_estudo USING btree (id_engenheiro);


--
-- TOC entry 4001 (class 1259 OID 44562)
-- Name: seg_estudo_id_protocolo_fk; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX seg_estudo_id_protocolo_fk ON public.seg_estudo USING btree (id_protocolo);


--
-- TOC entry 4002 (class 1259 OID 44563)
-- Name: seg_estudo_id_relatorio_fk; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX seg_estudo_id_relatorio_fk ON public.seg_estudo USING btree (id_relatorio);


--
-- TOC entry 4003 (class 1259 OID 44564)
-- Name: seg_estudo_id_tecno_fk; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX seg_estudo_id_tecno_fk ON public.seg_estudo USING btree (id_tecno);


--
-- TOC entry 4015 (class 1259 OID 44565)
-- Name: seg_estudo_medico_id_estudo_fk; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX seg_estudo_medico_id_estudo_fk ON public.seg_estudo_medico USING btree (id_estudo);


--
-- TOC entry 4016 (class 1259 OID 44566)
-- Name: seg_estudo_medico_id_medico_fk; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX seg_estudo_medico_id_medico_fk ON public.seg_estudo_medico USING btree (id_medico);


--
-- TOC entry 4006 (class 1259 OID 44567)
-- Name: seg_estudo_rep_cli_id_seg_rep_cli_fk; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX seg_estudo_rep_cli_id_seg_rep_cli_fk ON public.seg_estudo USING btree (id_seg_rep_cli);


--
-- TOC entry 4028 (class 1259 OID 44568)
-- Name: seg_funcao_epi_id_estudo_fk; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX seg_funcao_epi_id_estudo_fk ON public.seg_funcao_epi USING btree (id_estudo);


--
-- TOC entry 4031 (class 1259 OID 44569)
-- Name: seg_funcao_epi_unique; Type: INDEX; Schema: public; Owner: postgres
--

CREATE UNIQUE INDEX seg_funcao_epi_unique ON public.seg_funcao_epi USING btree (nome_funcao, nome_epi, id_estudo);


--
-- TOC entry 4035 (class 1259 OID 44570)
-- Name: seg_funcionario_id_cidade; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX seg_funcionario_id_cidade ON public.seg_funcionario USING btree (id_cidade);


--
-- TOC entry 4062 (class 1259 OID 44571)
-- Name: seg_ghe_fonte_agente_epi_id_epi_fk; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX seg_ghe_fonte_agente_epi_id_epi_fk ON public.seg_ghe_fonte_agente_epi USING btree (id_epi);


--
-- TOC entry 4063 (class 1259 OID 44572)
-- Name: seg_ghe_fonte_agente_epi_id_ghe_fonte_agente_fk; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX seg_ghe_fonte_agente_epi_id_ghe_fonte_agente_fk ON public.seg_ghe_fonte_agente_epi USING btree (id_ghe_fonte_agente);


--
-- TOC entry 4066 (class 1259 OID 44573)
-- Name: seg_ghe_fonte_agente_epi_unique; Type: INDEX; Schema: public; Owner: postgres
--

CREATE UNIQUE INDEX seg_ghe_fonte_agente_epi_unique ON public.seg_ghe_fonte_agente_epi USING btree (id_ghe_fonte_agente, id_epi, classificacao);


--
-- TOC entry 4080 (class 1259 OID 44574)
-- Name: seg_ghe_fonte_agente_exame_aso_id_aso_fk; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX seg_ghe_fonte_agente_exame_aso_id_aso_fk ON public.seg_ghe_fonte_agente_exame_aso USING btree (id_aso);


--
-- TOC entry 4081 (class 1259 OID 44575)
-- Name: seg_ghe_fonte_agente_exame_aso_id_ghe_fonte_agente_exame_fk; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX seg_ghe_fonte_agente_exame_aso_id_ghe_fonte_agente_exame_fk ON public.seg_ghe_fonte_agente_exame_aso USING btree (id_ghe_fonte_agente_exame);


--
-- TOC entry 4082 (class 1259 OID 44576)
-- Name: seg_ghe_fonte_agente_exame_aso_id_medico_fk; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX seg_ghe_fonte_agente_exame_aso_id_medico_fk ON public.seg_ghe_fonte_agente_exame_aso USING btree (id_medico);


--
-- TOC entry 4083 (class 1259 OID 44577)
-- Name: seg_ghe_fonte_agente_exame_aso_id_prestador_fk; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX seg_ghe_fonte_agente_exame_aso_id_prestador_fk ON public.seg_ghe_fonte_agente_exame_aso USING btree (id_prestador);


--
-- TOC entry 4084 (class 1259 OID 44578)
-- Name: seg_ghe_fonte_agente_exame_aso_id_protocolo_fk; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX seg_ghe_fonte_agente_exame_aso_id_protocolo_fk ON public.seg_ghe_fonte_agente_exame_aso USING btree (id_protocolo);


--
-- TOC entry 4085 (class 1259 OID 44579)
-- Name: seg_ghe_fonte_agente_exame_aso_id_sala_exame_fk; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX seg_ghe_fonte_agente_exame_aso_id_sala_exame_fk ON public.seg_ghe_fonte_agente_exame_aso USING btree (id_sala_exame);


--
-- TOC entry 4086 (class 1259 OID 44580)
-- Name: seg_ghe_fonte_agente_exame_aso_id_usuario_fk; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX seg_ghe_fonte_agente_exame_aso_id_usuario_fk ON public.seg_ghe_fonte_agente_exame_aso USING btree (id_usuario);


--
-- TOC entry 4069 (class 1259 OID 44581)
-- Name: seg_ghe_fonte_agente_exame_id_exame_fk; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX seg_ghe_fonte_agente_exame_id_exame_fk ON public.seg_ghe_fonte_agente_exame USING btree (id_exame);


--
-- TOC entry 4091 (class 1259 OID 44582)
-- Name: seg_ghe_fonte_agente_exame_periodicidade_id_ghe_fonte_agente_ex; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX seg_ghe_fonte_agente_exame_periodicidade_id_ghe_fonte_agente_ex ON public.seg_ghe_fonte_agente_exame_periodicidade USING btree (id_ghe_fonte_agente_exame);


--
-- TOC entry 4092 (class 1259 OID 44583)
-- Name: seg_ghe_fonte_agente_exame_periodicidade_id_periodicidade_fk; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX seg_ghe_fonte_agente_exame_periodicidade_id_periodicidade_fk ON public.seg_ghe_fonte_agente_exame_periodicidade USING btree (id_periodicidade);


--
-- TOC entry 4072 (class 1259 OID 44584)
-- Name: seg_ghe_fonte_agente_exames_id_ghe_fonte_agente_fk; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX seg_ghe_fonte_agente_exames_id_ghe_fonte_agente_fk ON public.seg_ghe_fonte_agente_exame USING btree (id_ghe_fonte_agente);


--
-- TOC entry 4053 (class 1259 OID 44585)
-- Name: seg_ghe_fonte_agente_id_agente_fk; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX seg_ghe_fonte_agente_id_agente_fk ON public.seg_ghe_fonte_agente USING btree (id_agente);


--
-- TOC entry 4054 (class 1259 OID 44586)
-- Name: seg_ghe_fonte_agente_id_ghe_fonte_fk; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX seg_ghe_fonte_agente_id_ghe_fonte_fk ON public.seg_ghe_fonte_agente USING btree (id_ghe_fonte);


--
-- TOC entry 4055 (class 1259 OID 44587)
-- Name: seg_ghe_fonte_agente_id_grad_efeito_fk; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX seg_ghe_fonte_agente_id_grad_efeito_fk ON public.seg_ghe_fonte_agente USING btree (id_grad_efeito);


--
-- TOC entry 4056 (class 1259 OID 44588)
-- Name: seg_ghe_fonte_agente_id_grad_exposicao_fk; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX seg_ghe_fonte_agente_id_grad_exposicao_fk ON public.seg_ghe_fonte_agente USING btree (id_grad_exposicao);


--
-- TOC entry 4057 (class 1259 OID 44589)
-- Name: seg_ghe_fonte_agente_id_grad_soma_fk; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX seg_ghe_fonte_agente_id_grad_soma_fk ON public.seg_ghe_fonte_agente USING btree (id_grad_soma);


--
-- TOC entry 4044 (class 1259 OID 44590)
-- Name: seg_ghe_fonte_id_fonte_fk; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX seg_ghe_fonte_id_fonte_fk ON public.seg_ghe_fonte USING btree (id_fonte);


--
-- TOC entry 4045 (class 1259 OID 44591)
-- Name: seg_ghe_fonte_id_ghe_fk; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX seg_ghe_fonte_id_ghe_fk ON public.seg_ghe_fonte USING btree (id_ghe);


--
-- TOC entry 4039 (class 1259 OID 44592)
-- Name: seg_ghe_id_estudo_fk; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX seg_ghe_id_estudo_fk ON public.seg_ghe USING btree (id_estudo);


--
-- TOC entry 4110 (class 1259 OID 44593)
-- Name: seg_ghe_setor_cliente_funcao_exame_id_exame_fk; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX seg_ghe_setor_cliente_funcao_exame_id_exame_fk ON public.seg_ghe_setor_cliente_funcao_exame USING btree (id_exame);


--
-- TOC entry 4111 (class 1259 OID 44594)
-- Name: seg_ghe_setor_cliente_funcao_exame_id_ghe_fonte_agente_exame_fk; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX seg_ghe_setor_cliente_funcao_exame_id_ghe_fonte_agente_exame_fk ON public.seg_ghe_setor_cliente_funcao_exame USING btree (id_ghe_fonte_agente_exame);


--
-- TOC entry 4112 (class 1259 OID 44595)
-- Name: seg_ghe_setor_cliente_funcao_exame_id_ghe_setor_cliente_funcao_; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX seg_ghe_setor_cliente_funcao_exame_id_ghe_setor_cliente_funcao_ ON public.seg_ghe_setor_cliente_funcao_exame USING btree (id_ghe_setor_cliente_funcao);


--
-- TOC entry 4102 (class 1259 OID 44596)
-- Name: seg_ghe_setor_cliente_funcao_id_comentario_funcao_fk; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX seg_ghe_setor_cliente_funcao_id_comentario_funcao_fk ON public.seg_ghe_setor_cliente_funcao USING btree (id_comentario_funcao);


--
-- TOC entry 4103 (class 1259 OID 44597)
-- Name: seg_ghe_setor_cliente_funcao_id_ghe_setor_fk; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX seg_ghe_setor_cliente_funcao_id_ghe_setor_fk ON public.seg_ghe_setor_cliente_funcao USING btree (id_ghe_setor);


--
-- TOC entry 4104 (class 1259 OID 44598)
-- Name: seg_ghe_setor_cliente_funcao_id_seg_cliente_funcao_fk; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX seg_ghe_setor_cliente_funcao_id_seg_cliente_funcao_fk ON public.seg_ghe_setor_cliente_funcao USING btree (id_seg_cliente_funcao);


--
-- TOC entry 4095 (class 1259 OID 44599)
-- Name: seg_ghe_setor_id_ghe_fk; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX seg_ghe_setor_id_ghe_fk ON public.seg_ghe_setor USING btree (id_ghe);


--
-- TOC entry 4096 (class 1259 OID 44600)
-- Name: seg_ghe_setor_id_setor_fk; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX seg_ghe_setor_id_setor_fk ON public.seg_ghe_setor USING btree (id_setor);


--
-- TOC entry 4122 (class 1259 OID 44601)
-- Name: seg_hospital_id_cidade_fk; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX seg_hospital_id_cidade_fk ON public.seg_hospital USING btree (id_cidade);


--
-- TOC entry 4137 (class 1259 OID 44602)
-- Name: seg_material_hospitalar_id_estudo_fk; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX seg_material_hospitalar_id_estudo_fk ON public.seg_material_hospitalar_estudo USING btree (id_estudo);


--
-- TOC entry 4138 (class 1259 OID 44603)
-- Name: seg_material_hospitalar_id_material_hospitalar_fk; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX seg_material_hospitalar_id_material_hospitalar_fk ON public.seg_material_hospitalar_estudo USING btree (id_material_hospitalar);


--
-- TOC entry 4145 (class 1259 OID 44604)
-- Name: seg_medico_exame_id_exame_fk; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX seg_medico_exame_id_exame_fk ON public.seg_medico_exame USING btree (id_exame);


--
-- TOC entry 4146 (class 1259 OID 44605)
-- Name: seg_medico_exame_id_medico_fk; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX seg_medico_exame_id_medico_fk ON public.seg_medico_exame USING btree (id_medico);


--
-- TOC entry 4140 (class 1259 OID 44606)
-- Name: seg_medico_id_cidade_fk; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX seg_medico_id_cidade_fk ON public.seg_medico USING btree (id_cidade);


--
-- TOC entry 4159 (class 1259 OID 44607)
-- Name: seg_monitoramento_ghefonteagente_id_ghe_fonte_agente_fk; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX seg_monitoramento_ghefonteagente_id_ghe_fonte_agente_fk ON public.seg_monitoramento_ghefonteagente USING btree (id_ghe_fonte_agente);


--
-- TOC entry 4160 (class 1259 OID 44608)
-- Name: seg_monitoramento_ghefonteagente_id_monitoramento_fk; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX seg_monitoramento_ghefonteagente_id_monitoramento_fk ON public.seg_monitoramento_ghefonteagente USING btree (id_monitoramento);


--
-- TOC entry 4152 (class 1259 OID 44609)
-- Name: seg_monitoramento_id_cliente_funcao_funcionario_fk; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX seg_monitoramento_id_cliente_funcao_funcionario_fk ON public.seg_monitoramento USING btree (id_cliente_funcao_funcionario);


--
-- TOC entry 4153 (class 1259 OID 44610)
-- Name: seg_monitoramento_id_usuario_fk; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX seg_monitoramento_id_usuario_fk ON public.seg_monitoramento USING btree (id_usuario);


--
-- TOC entry 4154 (class 1259 OID 44611)
-- Name: seg_monitoramento_monitoramento_fk; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX seg_monitoramento_monitoramento_fk ON public.seg_monitoramento USING btree (id_monitoramento_anterior);


--
-- TOC entry 4172 (class 1259 OID 44612)
-- Name: seg_movimento_id_cliente_fk; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX seg_movimento_id_cliente_fk ON public.seg_movimento USING btree (id_cliente);


--
-- TOC entry 4173 (class 1259 OID 44613)
-- Name: seg_movimento_id_cliente_funcao_exame_aso_fk; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX seg_movimento_id_cliente_funcao_exame_aso_fk ON public.seg_movimento USING btree (id_cliente_funcao_exame_aso);


--
-- TOC entry 4174 (class 1259 OID 44614)
-- Name: seg_movimento_id_cliente_unidade_fk; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX seg_movimento_id_cliente_unidade_fk ON public.seg_movimento USING btree (id_cliente_unidade);


--
-- TOC entry 4175 (class 1259 OID 44615)
-- Name: seg_movimento_id_empresa_fk; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX seg_movimento_id_empresa_fk ON public.seg_movimento USING btree (id_empresa);


--
-- TOC entry 4176 (class 1259 OID 44616)
-- Name: seg_movimento_id_ghe_fonte_agente_exame_aso_fk; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX seg_movimento_id_ghe_fonte_agente_exame_aso_fk ON public.seg_movimento USING btree (id_ghe_fonte_agente_exame_aso);


--
-- TOC entry 4177 (class 1259 OID 44617)
-- Name: seg_movimento_id_nfe_fk; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX seg_movimento_id_nfe_fk ON public.seg_movimento USING btree (id_nfe);


--
-- TOC entry 4178 (class 1259 OID 44618)
-- Name: seg_movimento_id_produto_contrato_fk; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX seg_movimento_id_produto_contrato_fk ON public.seg_movimento USING btree (id_produto_contrato);


--
-- TOC entry 4179 (class 1259 OID 44619)
-- Name: seg_movimento_id_protocolo_fk; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX seg_movimento_id_protocolo_fk ON public.seg_movimento USING btree (id_protocolo);


--
-- TOC entry 4180 (class 1259 OID 44620)
-- Name: seg_movimento_id_usuario_fk; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX seg_movimento_id_usuario_fk ON public.seg_movimento USING btree (id_usuario);


--
-- TOC entry 4188 (class 1259 OID 44621)
-- Name: seg_nfe_id_cliente_fk; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX seg_nfe_id_cliente_fk ON public.seg_nfe USING btree (id_cliente);


--
-- TOC entry 4189 (class 1259 OID 44622)
-- Name: seg_nfe_id_empresa_fk; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX seg_nfe_id_empresa_fk ON public.seg_nfe USING btree (id_empresa);


--
-- TOC entry 4190 (class 1259 OID 44623)
-- Name: seg_nfe_id_plano_forma_fk; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX seg_nfe_id_plano_forma_fk ON public.seg_nfe USING btree (id_plano_forma);


--
-- TOC entry 4191 (class 1259 OID 44624)
-- Name: seg_nfe_id_usuario_cancela_fk; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX seg_nfe_id_usuario_cancela_fk ON public.seg_nfe USING btree (id_usuario_cancela);


--
-- TOC entry 4192 (class 1259 OID 44625)
-- Name: seg_nfe_id_usuario_criador_fk; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX seg_nfe_id_usuario_criador_fk ON public.seg_nfe USING btree (id_usuario_criador);


--
-- TOC entry 4199 (class 1259 OID 44626)
-- Name: seg_norma_ghe_id_ghe_fk; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX seg_norma_ghe_id_ghe_fk ON public.seg_norma_ghe USING btree (id_ghe);


--
-- TOC entry 4200 (class 1259 OID 44627)
-- Name: seg_norma_ghe_id_norma_fk; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX seg_norma_ghe_id_norma_fk ON public.seg_norma_ghe USING btree (id_norma);


--
-- TOC entry 4204 (class 1259 OID 44628)
-- Name: seg_parcela_id_plano_fk; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX seg_parcela_id_plano_fk ON public.seg_parcela USING btree (id_plano);


--
-- TOC entry 4211 (class 1259 OID 44629)
-- Name: seg_perfil_dominio_acao_id_dominio_id_acao_idx; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX seg_perfil_dominio_acao_id_dominio_id_acao_idx ON public.seg_perfil_dominio_acao USING btree (id_dominio, id_acao);


--
-- TOC entry 4212 (class 1259 OID 44630)
-- Name: seg_perfil_dominio_acao_id_perfil_fk; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX seg_perfil_dominio_acao_id_perfil_fk ON public.seg_perfil_dominio_acao USING btree (id_perfil);


--
-- TOC entry 4221 (class 1259 OID 44631)
-- Name: seg_plano_forma_id_forma_fk; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX seg_plano_forma_id_forma_fk ON public.seg_plano_forma USING btree (id_forma);


--
-- TOC entry 4222 (class 1259 OID 44632)
-- Name: seg_plano_forma_id_plano_fk; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX seg_plano_forma_id_plano_fk ON public.seg_plano_forma USING btree (id_plano);


--
-- TOC entry 4231 (class 1259 OID 44633)
-- Name: seg_produto_contrato_id_contrato_fk; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX seg_produto_contrato_id_contrato_fk ON public.seg_produto_contrato USING btree (id_contrato);


--
-- TOC entry 4232 (class 1259 OID 44634)
-- Name: seg_produto_contrato_id_produto_fk; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX seg_produto_contrato_id_produto_fk ON public.seg_produto_contrato USING btree (id_produto);


--
-- TOC entry 4235 (class 1259 OID 44635)
-- Name: seg_produto_contrato_unique; Type: INDEX; Schema: public; Owner: postgres
--

CREATE UNIQUE INDEX seg_produto_contrato_unique ON public.seg_produto_contrato USING btree (id_produto, id_contrato);


--
-- TOC entry 4238 (class 1259 OID 44636)
-- Name: seg_prontuario_id_aso_fk; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX seg_prontuario_id_aso_fk ON public.seg_prontuario USING btree (id_aso);


--
-- TOC entry 4239 (class 1259 OID 44637)
-- Name: seg_prontuario_id_usuario_fk; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX seg_prontuario_id_usuario_fk ON public.seg_prontuario USING btree (id_usuario);


--
-- TOC entry 4246 (class 1259 OID 44638)
-- Name: seg_protocolo_id_cliente_fk; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX seg_protocolo_id_cliente_fk ON public.seg_protocolo USING btree (id_cliente);


--
-- TOC entry 4247 (class 1259 OID 44639)
-- Name: seg_protocolo_id_usuario_cancelou_fk; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX seg_protocolo_id_usuario_cancelou_fk ON public.seg_protocolo USING btree (id_usuario_cancelou);


--
-- TOC entry 4248 (class 1259 OID 44640)
-- Name: seg_protocolo_id_usuario_finalizou_fk; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX seg_protocolo_id_usuario_finalizou_fk ON public.seg_protocolo USING btree (id_usuario_finalizou);


--
-- TOC entry 4249 (class 1259 OID 44641)
-- Name: seg_protocolo_id_usuario_gerou_fk; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX seg_protocolo_id_usuario_gerou_fk ON public.seg_protocolo USING btree (id_usuario_gerou);


--
-- TOC entry 4257 (class 1259 OID 44642)
-- Name: seg_relanual_id_cliente_fk; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX seg_relanual_id_cliente_fk ON public.seg_relanual USING btree (id_cliente);


--
-- TOC entry 4258 (class 1259 OID 44643)
-- Name: seg_relanual_id_medico_fk; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX seg_relanual_id_medico_fk ON public.seg_relanual USING btree (id_medico_coordenador);


--
-- TOC entry 4259 (class 1259 OID 44644)
-- Name: seg_relanual_id_usuario_fk; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX seg_relanual_id_usuario_fk ON public.seg_relanual USING btree (id_usuario);


--
-- TOC entry 4265 (class 1259 OID 44645)
-- Name: seg_relanual_iten_id_relatorio_fk; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX seg_relanual_iten_id_relatorio_fk ON public.seg_relanual_item USING btree (id_relanual);


--
-- TOC entry 4276 (class 1259 OID 44646)
-- Name: seg_rep_cli_id_cliente_fk; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX seg_rep_cli_id_cliente_fk ON public.seg_rep_cli USING btree (id_cliente);


--
-- TOC entry 4277 (class 1259 OID 44647)
-- Name: seg_rep_cli_id_vend_fk; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX seg_rep_cli_id_vend_fk ON public.seg_rep_cli USING btree (id_vend);


--
-- TOC entry 4281 (class 1259 OID 44648)
-- Name: seg_revisao_id_estudo_fk; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX seg_revisao_id_estudo_fk ON public.seg_revisao USING btree (id_estudo);


--
-- TOC entry 4292 (class 1259 OID 44649)
-- Name: seg_sala_exame_id_exane_fk; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX seg_sala_exame_id_exane_fk ON public.seg_sala_exame USING btree (id_exame);


--
-- TOC entry 4293 (class 1259 OID 44650)
-- Name: seg_sala_exame_id_sala_fk; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX seg_sala_exame_id_sala_fk ON public.seg_sala_exame USING btree (id_sala);


--
-- TOC entry 4287 (class 1259 OID 44651)
-- Name: seg_sala_id_empresa; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX seg_sala_id_empresa ON public.seg_sala USING btree (id_empresa);


--
-- TOC entry 4308 (class 1259 OID 44652)
-- Name: seg_usuario_cliente_id_cliente_fk; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX seg_usuario_cliente_id_cliente_fk ON public.seg_usuario_cliente USING btree (id_cliente);


--
-- TOC entry 4309 (class 1259 OID 44653)
-- Name: seg_usuario_cliente_id_usuario_fk; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX seg_usuario_cliente_id_usuario_fk ON public.seg_usuario_cliente USING btree (id_usuario);


--
-- TOC entry 4314 (class 1259 OID 44654)
-- Name: seg_usuario_funcao_interna_id_funcao_interna_fk; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX seg_usuario_funcao_interna_id_funcao_interna_fk ON public.seg_usuario_funcao_interna USING btree (id_funcao_interna);


--
-- TOC entry 4315 (class 1259 OID 44655)
-- Name: seg_usuario_funcao_interna_id_usuario_fk; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX seg_usuario_funcao_interna_id_usuario_fk ON public.seg_usuario_funcao_interna USING btree (id_usuario);


--
-- TOC entry 4302 (class 1259 OID 44656)
-- Name: seg_usuario_id_empresa_fk; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX seg_usuario_id_empresa_fk ON public.seg_usuario USING btree (id_empresa);


--
-- TOC entry 4303 (class 1259 OID 44657)
-- Name: seg_usuario_id_medico_fk; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX seg_usuario_id_medico_fk ON public.seg_usuario USING btree (id_medico);


--
-- TOC entry 4320 (class 1259 OID 44658)
-- Name: seg_usuario_id_perfil_fk; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX seg_usuario_id_perfil_fk ON public.seg_usuario_perfil USING btree (id_perfil);


--
-- TOC entry 4321 (class 1259 OID 44659)
-- Name: seg_usuario_id_usuario_fk; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX seg_usuario_id_usuario_fk ON public.seg_usuario_perfil USING btree (id_usuario);


--
-- TOC entry 4325 (class 1259 OID 44660)
-- Name: seg_vendedor_id_cidade_ibge_fk; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX seg_vendedor_id_cidade_ibge_fk ON public.seg_vendedor USING btree (id_cidade_ibge);


--
-- TOC entry 4329 (class 1259 OID 44661)
-- Name: seg_versao_id_estudo_fk; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX seg_versao_id_estudo_fk ON public.seg_versao USING btree (id_estudo);


--
-- TOC entry 4332 (class 2606 OID 44662)
-- Name: seg_agente seg_agente_id_risco_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_agente
    ADD CONSTRAINT seg_agente_id_risco_fkey FOREIGN KEY (id_risco) REFERENCES public.seg_risco(id_risco);


--
-- TOC entry 4333 (class 2606 OID 44667)
-- Name: seg_arquivo_anexo seg_arquivo_anexo_id_aso_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_arquivo_anexo
    ADD CONSTRAINT seg_arquivo_anexo_id_aso_fkey FOREIGN KEY (id_aso) REFERENCES public.seg_aso(id_aso);


--
-- TOC entry 4345 (class 2606 OID 44672)
-- Name: seg_aso_agente_risco seg_aso_agente_risco_id_aso_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_aso_agente_risco
    ADD CONSTRAINT seg_aso_agente_risco_id_aso_fkey FOREIGN KEY (id_aso) REFERENCES public.seg_aso(id_aso);


--
-- TOC entry 4346 (class 2606 OID 44677)
-- Name: seg_aso_ghe_setor seg_aso_ghe_setor_id_aso_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_aso_ghe_setor
    ADD CONSTRAINT seg_aso_ghe_setor_id_aso_fkey FOREIGN KEY (id_aso) REFERENCES public.seg_aso(id_aso);


--
-- TOC entry 4334 (class 2606 OID 44682)
-- Name: seg_aso seg_aso_id_centrocusto_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_aso
    ADD CONSTRAINT seg_aso_id_centrocusto_fkey FOREIGN KEY (id_centrocusto) REFERENCES public.seg_centrocusto(id);


--
-- TOC entry 4335 (class 2606 OID 44687)
-- Name: seg_aso seg_aso_id_cliente_funcao_funcionario_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_aso
    ADD CONSTRAINT seg_aso_id_cliente_funcao_funcionario_fkey FOREIGN KEY (id_cliente_funcao_funcionario) REFERENCES public.seg_cliente_funcao_funcionario(id_cliente_funcao_funcionario);


--
-- TOC entry 4336 (class 2606 OID 44692)
-- Name: seg_aso seg_aso_id_empresa_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_aso
    ADD CONSTRAINT seg_aso_id_empresa_fkey FOREIGN KEY (id_empresa) REFERENCES public.seg_empresa(id_empresa);


--
-- TOC entry 4337 (class 2606 OID 44697)
-- Name: seg_aso seg_aso_id_medico_coordenador_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_aso
    ADD CONSTRAINT seg_aso_id_medico_coordenador_fkey FOREIGN KEY (id_medico_coordenador) REFERENCES public.seg_medico(id_medico);


--
-- TOC entry 4338 (class 2606 OID 44702)
-- Name: seg_aso seg_aso_id_medico_examinador_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_aso
    ADD CONSTRAINT seg_aso_id_medico_examinador_fkey FOREIGN KEY (id_medico_examinador) REFERENCES public.seg_medico(id_medico);


--
-- TOC entry 4339 (class 2606 OID 44707)
-- Name: seg_aso seg_aso_id_periodicidade_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_aso
    ADD CONSTRAINT seg_aso_id_periodicidade_fkey FOREIGN KEY (id_periodicidade) REFERENCES public.seg_periodicidade(id_periodicidade);


--
-- TOC entry 4340 (class 2606 OID 44717)
-- Name: seg_aso seg_aso_id_protocolo_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_aso
    ADD CONSTRAINT seg_aso_id_protocolo_fkey FOREIGN KEY (id_protocolo) REFERENCES public.seg_protocolo(id_protocolo);


--
-- TOC entry 4341 (class 2606 OID 44722)
-- Name: seg_aso seg_aso_id_ult_usuario_alteracao_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_aso
    ADD CONSTRAINT seg_aso_id_ult_usuario_alteracao_fkey FOREIGN KEY (id_ult_usuario_alteracao) REFERENCES public.seg_usuario(id_usuario);


--
-- TOC entry 4342 (class 2606 OID 44727)
-- Name: seg_aso seg_aso_id_usuario_cancelamento_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_aso
    ADD CONSTRAINT seg_aso_id_usuario_cancelamento_fkey FOREIGN KEY (id_usuario_cancelamento) REFERENCES public.seg_usuario(id_usuario);


--
-- TOC entry 4343 (class 2606 OID 44732)
-- Name: seg_aso seg_aso_id_usuario_criador_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_aso
    ADD CONSTRAINT seg_aso_id_usuario_criador_fkey FOREIGN KEY (id_usuario_criador) REFERENCES public.seg_usuario(id_usuario);


--
-- TOC entry 4344 (class 2606 OID 44737)
-- Name: seg_aso seg_aso_id_usuario_finalizador_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_aso
    ADD CONSTRAINT seg_aso_id_usuario_finalizador_fkey FOREIGN KEY (id_usuario_finalizador) REFERENCES public.seg_usuario(id_usuario);


--
-- TOC entry 4347 (class 2606 OID 44742)
-- Name: seg_atividade_cronograma seg_atividade_cronograma_id_atividade_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_atividade_cronograma
    ADD CONSTRAINT seg_atividade_cronograma_id_atividade_fkey FOREIGN KEY (id_atividade) REFERENCES public.seg_atividade(id_atividade);


--
-- TOC entry 4348 (class 2606 OID 44747)
-- Name: seg_atividade_cronograma seg_atividade_cronograma_id_cronograma_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_atividade_cronograma
    ADD CONSTRAINT seg_atividade_cronograma_id_cronograma_fkey FOREIGN KEY (id_cronograma) REFERENCES public.seg_cronograma(id_cronograma) ON DELETE CASCADE;


--
-- TOC entry 4349 (class 2606 OID 44752)
-- Name: seg_centrocusto seg_centrocusto_id_cliente_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_centrocusto
    ADD CONSTRAINT seg_centrocusto_id_cliente_fkey FOREIGN KEY (id_cliente) REFERENCES public.seg_cliente(id_cliente) ON DELETE CASCADE;


--
-- TOC entry 4350 (class 2606 OID 44757)
-- Name: seg_cli_cnae seg_cli_cnae_id_cliente_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_cli_cnae
    ADD CONSTRAINT seg_cli_cnae_id_cliente_fkey FOREIGN KEY (id_cliente) REFERENCES public.seg_cliente(id_cliente);


--
-- TOC entry 4351 (class 2606 OID 44762)
-- Name: seg_cli_cnae seg_cli_cnae_id_cnae_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_cli_cnae
    ADD CONSTRAINT seg_cli_cnae_id_cnae_fkey FOREIGN KEY (id_cnae) REFERENCES public.seg_cnae(id_cnae);


--
-- TOC entry 4358 (class 2606 OID 44767)
-- Name: seg_cliente_arquivo seg_cliente_arquivo_id_arquivo_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_cliente_arquivo
    ADD CONSTRAINT seg_cliente_arquivo_id_arquivo_fkey FOREIGN KEY (id_arquivo) REFERENCES public.seg_arquivo(id_arquivo);


--
-- TOC entry 4359 (class 2606 OID 44772)
-- Name: seg_cliente_arquivo seg_cliente_arquivo_id_cliente_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_cliente_arquivo
    ADD CONSTRAINT seg_cliente_arquivo_id_cliente_fkey FOREIGN KEY (id_cliente) REFERENCES public.seg_cliente(id_cliente);


--
-- TOC entry 4364 (class 2606 OID 44777)
-- Name: seg_cliente_funcao_exame_aso seg_cliente_funcao_exame_aso_id_aso_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_cliente_funcao_exame_aso
    ADD CONSTRAINT seg_cliente_funcao_exame_aso_id_aso_fkey FOREIGN KEY (id_aso) REFERENCES public.seg_aso(id_aso) ON DELETE CASCADE;


--
-- TOC entry 4365 (class 2606 OID 44782)
-- Name: seg_cliente_funcao_exame_aso seg_cliente_funcao_exame_aso_id_cliente_funcao_exame_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_cliente_funcao_exame_aso
    ADD CONSTRAINT seg_cliente_funcao_exame_aso_id_cliente_funcao_exame_fkey FOREIGN KEY (id_cliente_funcao_exame) REFERENCES public.seg_cliente_funcao_exame(id_cliente_funcao_exame) ON DELETE CASCADE;


--
-- TOC entry 4366 (class 2606 OID 44787)
-- Name: seg_cliente_funcao_exame_aso seg_cliente_funcao_exame_aso_id_prestador_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_cliente_funcao_exame_aso
    ADD CONSTRAINT seg_cliente_funcao_exame_aso_id_prestador_fkey FOREIGN KEY (id_prestador) REFERENCES public.seg_cliente(id_cliente);


--
-- TOC entry 4367 (class 2606 OID 44792)
-- Name: seg_cliente_funcao_exame_aso seg_cliente_funcao_exame_aso_id_protocolo_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_cliente_funcao_exame_aso
    ADD CONSTRAINT seg_cliente_funcao_exame_aso_id_protocolo_fkey FOREIGN KEY (id_protocolo) REFERENCES public.seg_protocolo(id_protocolo);


--
-- TOC entry 4368 (class 2606 OID 44797)
-- Name: seg_cliente_funcao_exame_aso seg_cliente_funcao_exame_aso_id_sala_exame_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_cliente_funcao_exame_aso
    ADD CONSTRAINT seg_cliente_funcao_exame_aso_id_sala_exame_fkey FOREIGN KEY (id_sala_exame) REFERENCES public.seg_sala_exame(id_sala_exame);


--
-- TOC entry 4369 (class 2606 OID 44802)
-- Name: seg_cliente_funcao_exame_aso seg_cliente_funcao_exame_aso_id_usuario_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_cliente_funcao_exame_aso
    ADD CONSTRAINT seg_cliente_funcao_exame_aso_id_usuario_fkey FOREIGN KEY (id_usuario) REFERENCES public.seg_usuario(id_usuario);


--
-- TOC entry 4362 (class 2606 OID 44807)
-- Name: seg_cliente_funcao_exame seg_cliente_funcao_exame_id_cliente_funcao_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_cliente_funcao_exame
    ADD CONSTRAINT seg_cliente_funcao_exame_id_cliente_funcao_fkey FOREIGN KEY (id_cliente_funcao) REFERENCES public.seg_cliente_funcao(id_seg_cliente_funcao) ON DELETE CASCADE;


--
-- TOC entry 4363 (class 2606 OID 44812)
-- Name: seg_cliente_funcao_exame seg_cliente_funcao_exame_id_exame_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_cliente_funcao_exame
    ADD CONSTRAINT seg_cliente_funcao_exame_id_exame_fkey FOREIGN KEY (id_exame) REFERENCES public.seg_exame(id_exame) ON DELETE CASCADE;


--
-- TOC entry 4370 (class 2606 OID 44817)
-- Name: seg_cliente_funcao_exame_periodicidade seg_cliente_funcao_exame_periodici_id_cliente_funcao_exame_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_cliente_funcao_exame_periodicidade
    ADD CONSTRAINT seg_cliente_funcao_exame_periodici_id_cliente_funcao_exame_fkey FOREIGN KEY (id_cliente_funcao_exame) REFERENCES public.seg_cliente_funcao_exame(id_cliente_funcao_exame) ON DELETE CASCADE;


--
-- TOC entry 4371 (class 2606 OID 44822)
-- Name: seg_cliente_funcao_exame_periodicidade seg_cliente_funcao_exame_periodicidade_id_periodicidade_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_cliente_funcao_exame_periodicidade
    ADD CONSTRAINT seg_cliente_funcao_exame_periodicidade_id_periodicidade_fkey FOREIGN KEY (id_periodicidade) REFERENCES public.seg_periodicidade(id_periodicidade) ON DELETE CASCADE;


--
-- TOC entry 4372 (class 2606 OID 44827)
-- Name: seg_cliente_funcao_funcionario seg_cliente_funcao_funcionario_id_funcionario_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_cliente_funcao_funcionario
    ADD CONSTRAINT seg_cliente_funcao_funcionario_id_funcionario_fkey FOREIGN KEY (id_funcionario) REFERENCES public.seg_funcionario(id_funcionario) ON DELETE CASCADE;


--
-- TOC entry 4373 (class 2606 OID 44832)
-- Name: seg_cliente_funcao_funcionario seg_cliente_funcao_funcionario_id_seg_cliente_funcao_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_cliente_funcao_funcionario
    ADD CONSTRAINT seg_cliente_funcao_funcionario_id_seg_cliente_funcao_fkey FOREIGN KEY (id_seg_cliente_funcao) REFERENCES public.seg_cliente_funcao(id_seg_cliente_funcao);


--
-- TOC entry 4360 (class 2606 OID 44837)
-- Name: seg_cliente_funcao seg_cliente_funcao_id_cliente_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_cliente_funcao
    ADD CONSTRAINT seg_cliente_funcao_id_cliente_fkey FOREIGN KEY (id_cliente) REFERENCES public.seg_cliente(id_cliente);


--
-- TOC entry 4361 (class 2606 OID 44842)
-- Name: seg_cliente_funcao seg_cliente_funcao_id_funcao_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_cliente_funcao
    ADD CONSTRAINT seg_cliente_funcao_id_funcao_fkey FOREIGN KEY (id_funcao) REFERENCES public.seg_funcao(id_funcao);


--
-- TOC entry 4374 (class 2606 OID 44847)
-- Name: seg_cliente_funcionario seg_cliente_funcionario_id_cliente_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_cliente_funcionario
    ADD CONSTRAINT seg_cliente_funcionario_id_cliente_fkey FOREIGN KEY (id_cliente) REFERENCES public.seg_cliente(id_cliente);


--
-- TOC entry 4375 (class 2606 OID 44852)
-- Name: seg_cliente_funcionario seg_cliente_funcionario_id_funcionario_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_cliente_funcionario
    ADD CONSTRAINT seg_cliente_funcionario_id_funcionario_fkey FOREIGN KEY (id_funcionario) REFERENCES public.seg_funcionario(id_funcionario);


--
-- TOC entry 4352 (class 2606 OID 44857)
-- Name: seg_cliente seg_cliente_id_cidade_ibge_cobranca_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_cliente
    ADD CONSTRAINT seg_cliente_id_cidade_ibge_cobranca_fkey FOREIGN KEY (id_cidade_ibge_cobranca) REFERENCES public.seg_cidade_ibge(id_cidade_ibge);


--
-- TOC entry 4353 (class 2606 OID 44862)
-- Name: seg_cliente seg_cliente_id_cidade_ibge_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_cliente
    ADD CONSTRAINT seg_cliente_id_cidade_ibge_fkey FOREIGN KEY (id_cidade_ibge) REFERENCES public.seg_cidade_ibge(id_cidade_ibge);


--
-- TOC entry 4354 (class 2606 OID 44867)
-- Name: seg_cliente seg_cliente_id_cliente_credenciado_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_cliente
    ADD CONSTRAINT seg_cliente_id_cliente_credenciado_fkey FOREIGN KEY (id_cliente_credenciado) REFERENCES public.seg_cliente(id_cliente);


--
-- TOC entry 4355 (class 2606 OID 44872)
-- Name: seg_cliente seg_cliente_id_cliente_matriz_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_cliente
    ADD CONSTRAINT seg_cliente_id_cliente_matriz_fkey FOREIGN KEY (id_cliente_matriz) REFERENCES public.seg_cliente(id_cliente);


--
-- TOC entry 4356 (class 2606 OID 44877)
-- Name: seg_cliente seg_cliente_id_medico_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_cliente
    ADD CONSTRAINT seg_cliente_id_medico_fkey FOREIGN KEY (id_medico) REFERENCES public.seg_medico(id_medico);


--
-- TOC entry 4357 (class 2606 OID 44882)
-- Name: seg_cliente seg_cliente_id_ramo_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_cliente
    ADD CONSTRAINT seg_cliente_id_ramo_fkey FOREIGN KEY (id_ramo) REFERENCES public.seg_ramo(id);


--
-- TOC entry 4376 (class 2606 OID 44887)
-- Name: seg_cliente_proposta seg_cliente_proposta_id_cidade_ibge_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_cliente_proposta
    ADD CONSTRAINT seg_cliente_proposta_id_cidade_ibge_fkey FOREIGN KEY (id_cidade_ibge) REFERENCES public.seg_cidade_ibge(id_cidade_ibge);


--
-- TOC entry 4377 (class 2606 OID 44892)
-- Name: seg_cnae_estudo seg_cnae_estudo_id_cnae_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_cnae_estudo
    ADD CONSTRAINT seg_cnae_estudo_id_cnae_fkey FOREIGN KEY (id_cnae) REFERENCES public.seg_cnae(id_cnae);


--
-- TOC entry 4378 (class 2606 OID 44897)
-- Name: seg_cnae_estudo seg_cnae_estudo_id_estudo_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_cnae_estudo
    ADD CONSTRAINT seg_cnae_estudo_id_estudo_fkey FOREIGN KEY (id_estudo) REFERENCES public.seg_estudo(id_estudo) ON DELETE CASCADE;


--
-- TOC entry 4379 (class 2606 OID 44902)
-- Name: seg_comentario_funcao seg_comentario_funcao_id_funcao_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_comentario_funcao
    ADD CONSTRAINT seg_comentario_funcao_id_funcao_fkey FOREIGN KEY (id_funcao) REFERENCES public.seg_funcao(id_funcao);


--
-- TOC entry 4380 (class 2606 OID 44907)
-- Name: seg_conta seg_conta_id_banco_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_conta
    ADD CONSTRAINT seg_conta_id_banco_fkey FOREIGN KEY (id_banco) REFERENCES public.seg_banco(id_banco);


--
-- TOC entry 4381 (class 2606 OID 44912)
-- Name: seg_contato seg_contato_id_cidade_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_contato
    ADD CONSTRAINT seg_contato_id_cidade_fkey FOREIGN KEY (id_cidade) REFERENCES public.seg_cidade_ibge(id_cidade_ibge);


--
-- TOC entry 4382 (class 2606 OID 44917)
-- Name: seg_contato seg_contato_id_cliente_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_contato
    ADD CONSTRAINT seg_contato_id_cliente_fkey FOREIGN KEY (id_cliente) REFERENCES public.seg_cliente(id_cliente) ON DELETE CASCADE;


--
-- TOC entry 4383 (class 2606 OID 44922)
-- Name: seg_contato seg_contato_id_tipo_contato_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_contato
    ADD CONSTRAINT seg_contato_id_tipo_contato_fkey FOREIGN KEY (id_tipo_contato) REFERENCES public.seg_tipo_contato(id);


--
-- TOC entry 4390 (class 2606 OID 44927)
-- Name: seg_contrato_exame seg_contrato_exame_id_contrato_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_contrato_exame
    ADD CONSTRAINT seg_contrato_exame_id_contrato_fkey FOREIGN KEY (id_contrato) REFERENCES public.seg_contrato(id_contrato) ON DELETE CASCADE;


--
-- TOC entry 4391 (class 2606 OID 44932)
-- Name: seg_contrato_exame seg_contrato_exame_id_exame_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_contrato_exame
    ADD CONSTRAINT seg_contrato_exame_id_exame_fkey FOREIGN KEY (id_exame) REFERENCES public.seg_exame(id_exame) ON DELETE CASCADE;


--
-- TOC entry 4384 (class 2606 OID 44937)
-- Name: seg_contrato seg_contrato_id_cliente_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_contrato
    ADD CONSTRAINT seg_contrato_id_cliente_fkey FOREIGN KEY (id_cliente) REFERENCES public.seg_cliente(id_cliente);


--
-- TOC entry 4385 (class 2606 OID 44942)
-- Name: seg_contrato seg_contrato_id_cliente_prestador_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_contrato
    ADD CONSTRAINT seg_contrato_id_cliente_prestador_fkey FOREIGN KEY (id_cliente_prestador) REFERENCES public.seg_cliente(id_cliente);


--
-- TOC entry 4386 (class 2606 OID 44947)
-- Name: seg_contrato seg_contrato_id_cliente_proposta_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_contrato
    ADD CONSTRAINT seg_contrato_id_cliente_proposta_fkey FOREIGN KEY (id_cliente_proposta) REFERENCES public.seg_cliente_proposta(id_cliente_proposta);


--
-- TOC entry 4387 (class 2606 OID 44952)
-- Name: seg_contrato seg_contrato_id_contrato_pai_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_contrato
    ADD CONSTRAINT seg_contrato_id_contrato_pai_fkey FOREIGN KEY (id_contrato_pai) REFERENCES public.seg_contrato(id_contrato);


--
-- TOC entry 4388 (class 2606 OID 44957)
-- Name: seg_contrato seg_contrato_id_contrato_raiz_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_contrato
    ADD CONSTRAINT seg_contrato_id_contrato_raiz_fkey FOREIGN KEY (id_contrato_raiz) REFERENCES public.seg_contrato(id_contrato);


--
-- TOC entry 4389 (class 2606 OID 44962)
-- Name: seg_contrato seg_contrato_id_empresa_fk; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_contrato
    ADD CONSTRAINT seg_contrato_id_empresa_fk FOREIGN KEY (id_empresa) REFERENCES public.seg_empresa(id_empresa);


--
-- TOC entry 4392 (class 2606 OID 44967)
-- Name: seg_correcao_estudo seg_correcao_estudo_id_estudo_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_correcao_estudo
    ADD CONSTRAINT seg_correcao_estudo_id_estudo_fkey FOREIGN KEY (id_estudo) REFERENCES public.seg_estudo(id_estudo);


--
-- TOC entry 4393 (class 2606 OID 44972)
-- Name: seg_correcao_estudo seg_correcao_estudo_id_usuario_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_correcao_estudo
    ADD CONSTRAINT seg_correcao_estudo_id_usuario_fkey FOREIGN KEY (id_usuario) REFERENCES public.seg_usuario(id_usuario);


--
-- TOC entry 4394 (class 2606 OID 44977)
-- Name: seg_crc seg_crc_id_conta_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_crc
    ADD CONSTRAINT seg_crc_id_conta_fkey FOREIGN KEY (id_conta) REFERENCES public.seg_conta(id_conta);


--
-- TOC entry 4395 (class 2606 OID 44982)
-- Name: seg_crc seg_crc_id_crc_desdobramento_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_crc
    ADD CONSTRAINT seg_crc_id_crc_desdobramento_fkey FOREIGN KEY (id_crc_desdobramento) REFERENCES public.seg_crc(id_crc);


--
-- TOC entry 4396 (class 2606 OID 44987)
-- Name: seg_crc seg_crc_id_forma_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_crc
    ADD CONSTRAINT seg_crc_id_forma_fkey FOREIGN KEY (id_forma) REFERENCES public.seg_forma(id_forma);


--
-- TOC entry 4397 (class 2606 OID 44992)
-- Name: seg_crc seg_crc_id_nfe_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_crc
    ADD CONSTRAINT seg_crc_id_nfe_fkey FOREIGN KEY (id_nfe) REFERENCES public.seg_nfe(id_nfe);


--
-- TOC entry 4398 (class 2606 OID 44997)
-- Name: seg_crc seg_crc_id_usuario_baixa_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_crc
    ADD CONSTRAINT seg_crc_id_usuario_baixa_fkey FOREIGN KEY (id_usuario_baixa) REFERENCES public.seg_usuario(id_usuario);


--
-- TOC entry 4399 (class 2606 OID 45002)
-- Name: seg_crc seg_crc_id_usuario_cancela_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_crc
    ADD CONSTRAINT seg_crc_id_usuario_cancela_fkey FOREIGN KEY (id_usuario_cancela) REFERENCES public.seg_usuario(id_usuario);


--
-- TOC entry 4400 (class 2606 OID 45007)
-- Name: seg_crc seg_crc_id_usuario_criador_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_crc
    ADD CONSTRAINT seg_crc_id_usuario_criador_fkey FOREIGN KEY (id_usuario_criador) REFERENCES public.seg_usuario(id_usuario);


--
-- TOC entry 4401 (class 2606 OID 45012)
-- Name: seg_crc seg_crc_id_usuario_desdobra_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_crc
    ADD CONSTRAINT seg_crc_id_usuario_desdobra_fkey FOREIGN KEY (id_usuario_desdobra) REFERENCES public.seg_usuario(id_usuario);


--
-- TOC entry 4402 (class 2606 OID 45017)
-- Name: seg_crc seg_crc_id_usuario_estorna_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_crc
    ADD CONSTRAINT seg_crc_id_usuario_estorna_fkey FOREIGN KEY (id_usuario_estorna) REFERENCES public.seg_usuario(id_usuario);


--
-- TOC entry 4403 (class 2606 OID 45022)
-- Name: seg_crc seg_crc_id_usuario_prorrogacao_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_crc
    ADD CONSTRAINT seg_crc_id_usuario_prorrogacao_fkey FOREIGN KEY (id_usuario_prorrogacao) REFERENCES public.seg_usuario(id_usuario);


--
-- TOC entry 4404 (class 2606 OID 45027)
-- Name: seg_documento_estudo seg_documento_estudo_id_documento_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_documento_estudo
    ADD CONSTRAINT seg_documento_estudo_id_documento_fkey FOREIGN KEY (id_documento) REFERENCES public.seg_documento(id_documento);


--
-- TOC entry 4405 (class 2606 OID 45032)
-- Name: seg_documento_estudo seg_documento_estudo_id_estudo_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_documento_estudo
    ADD CONSTRAINT seg_documento_estudo_id_estudo_fkey FOREIGN KEY (id_estudo) REFERENCES public.seg_estudo(id_estudo) ON DELETE CASCADE;


--
-- TOC entry 4406 (class 2606 OID 45037)
-- Name: seg_documento_protocolo seg_documento_protocolo_id_aso_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_documento_protocolo
    ADD CONSTRAINT seg_documento_protocolo_id_aso_fkey FOREIGN KEY (id_aso) REFERENCES public.seg_aso(id_aso);


--
-- TOC entry 4407 (class 2606 OID 45042)
-- Name: seg_documento_protocolo seg_documento_protocolo_id_documento_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_documento_protocolo
    ADD CONSTRAINT seg_documento_protocolo_id_documento_fkey FOREIGN KEY (id_documento) REFERENCES public.seg_documento(id_documento);


--
-- TOC entry 4408 (class 2606 OID 45047)
-- Name: seg_documento_protocolo seg_documento_protocolo_id_protocolo_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_documento_protocolo
    ADD CONSTRAINT seg_documento_protocolo_id_protocolo_fkey FOREIGN KEY (id_protocolo) REFERENCES public.seg_protocolo(id_protocolo);


--
-- TOC entry 4409 (class 2606 OID 45052)
-- Name: seg_dominio_acao seg_dominio_acao_id_acao_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_dominio_acao
    ADD CONSTRAINT seg_dominio_acao_id_acao_fkey FOREIGN KEY (id_acao) REFERENCES public.seg_acao(id_acao) ON DELETE CASCADE;


--
-- TOC entry 4410 (class 2606 OID 45057)
-- Name: seg_dominio_acao seg_dominio_acao_id_dominio_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_dominio_acao
    ADD CONSTRAINT seg_dominio_acao_id_dominio_fkey FOREIGN KEY (id_dominio) REFERENCES public.seg_dominio(id_dominio) ON DELETE CASCADE;


--
-- TOC entry 4411 (class 2606 OID 45062)
-- Name: seg_email seg_email_id_prontuario_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_email
    ADD CONSTRAINT seg_email_id_prontuario_fkey FOREIGN KEY (id_prontuario) REFERENCES public.seg_prontuario(id_prontuario);


--
-- TOC entry 4412 (class 2606 OID 45067)
-- Name: seg_email seg_email_id_usuario_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_email
    ADD CONSTRAINT seg_email_id_usuario_fkey FOREIGN KEY (id_usuario) REFERENCES public.seg_usuario(id_usuario);


--
-- TOC entry 4413 (class 2606 OID 45072)
-- Name: seg_empresa seg_empresa_id_cidade_ibge_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_empresa
    ADD CONSTRAINT seg_empresa_id_cidade_ibge_fkey FOREIGN KEY (id_cidade_ibge) REFERENCES public.seg_cidade_ibge(id_cidade_ibge);


--
-- TOC entry 4414 (class 2606 OID 45077)
-- Name: seg_empresa seg_empresa_id_empresa_matriz_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_empresa
    ADD CONSTRAINT seg_empresa_id_empresa_matriz_fkey FOREIGN KEY (id_empresa_matriz) REFERENCES public.seg_empresa(id_empresa);


--
-- TOC entry 4415 (class 2606 OID 45082)
-- Name: seg_epi_monitoramento seg_epi_monitoramento_id_epi_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_epi_monitoramento
    ADD CONSTRAINT seg_epi_monitoramento_id_epi_fkey FOREIGN KEY (id_epi) REFERENCES public.seg_epi(id_epi) ON DELETE CASCADE;


--
-- TOC entry 4416 (class 2606 OID 45087)
-- Name: seg_epi_monitoramento seg_epi_monitoramento_id_monitoramento_ghefonteagente_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_epi_monitoramento
    ADD CONSTRAINT seg_epi_monitoramento_id_monitoramento_ghefonteagente_fkey FOREIGN KEY (id_monitoramento_ghefonteagente) REFERENCES public.seg_monitoramento_ghefonteagente(id_monitoramento_ghefonteagente) ON DELETE CASCADE;


--
-- TOC entry 4418 (class 2606 OID 45092)
-- Name: seg_esocial_lote_aso seg_esocial_lote_aso_id_aso_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_esocial_lote_aso
    ADD CONSTRAINT seg_esocial_lote_aso_id_aso_fkey FOREIGN KEY (id_aso) REFERENCES public.seg_aso(id_aso);


--
-- TOC entry 4419 (class 2606 OID 45097)
-- Name: seg_esocial_lote_aso seg_esocial_lote_aso_id_esocial_lote_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_esocial_lote_aso
    ADD CONSTRAINT seg_esocial_lote_aso_id_esocial_lote_fkey FOREIGN KEY (id_esocial_lote) REFERENCES public.seg_esocial_lote(id_esocial_lote) ON DELETE CASCADE;


--
-- TOC entry 4417 (class 2606 OID 45102)
-- Name: seg_esocial_lote seg_esocial_lote_id_cliente_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_esocial_lote
    ADD CONSTRAINT seg_esocial_lote_id_cliente_fkey FOREIGN KEY (id_cliente) REFERENCES public.seg_cliente(id_cliente);


--
-- TOC entry 4420 (class 2606 OID 45107)
-- Name: seg_esocial_monitoramento seg_esocial_monitoramento_id_esocial_lote_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_esocial_monitoramento
    ADD CONSTRAINT seg_esocial_monitoramento_id_esocial_lote_fkey FOREIGN KEY (id_esocial_lote) REFERENCES public.seg_esocial_lote(id_esocial_lote) ON DELETE CASCADE;


--
-- TOC entry 4421 (class 2606 OID 45112)
-- Name: seg_esocial_monitoramento seg_esocial_monitoramento_id_monitoramento_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_esocial_monitoramento
    ADD CONSTRAINT seg_esocial_monitoramento_id_monitoramento_fkey FOREIGN KEY (id_monitoramento) REFERENCES public.seg_monitoramento(id_monitoramento) ON DELETE CASCADE;


--
-- TOC entry 4422 (class 2606 OID 45117)
-- Name: seg_estimativa_estudo seg_estimativa_estudo_id_estimativa_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_estimativa_estudo
    ADD CONSTRAINT seg_estimativa_estudo_id_estimativa_fkey FOREIGN KEY (id_estimativa) REFERENCES public.seg_estimativa(id_estimativa);


--
-- TOC entry 4423 (class 2606 OID 45122)
-- Name: seg_estimativa_estudo seg_estimativa_estudo_id_estudo_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_estimativa_estudo
    ADD CONSTRAINT seg_estimativa_estudo_id_estudo_fkey FOREIGN KEY (id_estudo) REFERENCES public.seg_estudo(id_estudo) ON DELETE CASCADE;


--
-- TOC entry 4431 (class 2606 OID 45127)
-- Name: seg_estudo_hospital seg_estudo_hospital_id_estudo_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_estudo_hospital
    ADD CONSTRAINT seg_estudo_hospital_id_estudo_fkey FOREIGN KEY (id_estudo) REFERENCES public.seg_estudo(id_estudo) ON DELETE CASCADE;


--
-- TOC entry 4432 (class 2606 OID 45132)
-- Name: seg_estudo_hospital seg_estudo_hospital_id_hospital_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_estudo_hospital
    ADD CONSTRAINT seg_estudo_hospital_id_hospital_fkey FOREIGN KEY (id_hospital) REFERENCES public.seg_hospital(id_hospital);


--
-- TOC entry 4424 (class 2606 OID 45137)
-- Name: seg_estudo seg_estudo_id_cliente_contratado_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_estudo
    ADD CONSTRAINT seg_estudo_id_cliente_contratado_fkey FOREIGN KEY (id_cliente_contratado) REFERENCES public.seg_cliente(id_cliente);


--
-- TOC entry 4425 (class 2606 OID 45142)
-- Name: seg_estudo seg_estudo_id_cronograma_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_estudo
    ADD CONSTRAINT seg_estudo_id_cronograma_fkey FOREIGN KEY (id_cronograma) REFERENCES public.seg_cronograma(id_cronograma) ON DELETE CASCADE;


--
-- TOC entry 4426 (class 2606 OID 45147)
-- Name: seg_estudo seg_estudo_id_engenheiro_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_estudo
    ADD CONSTRAINT seg_estudo_id_engenheiro_fkey FOREIGN KEY (id_engenheiro) REFERENCES public.seg_usuario(id_usuario);


--
-- TOC entry 4427 (class 2606 OID 45152)
-- Name: seg_estudo seg_estudo_id_protocolo_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_estudo
    ADD CONSTRAINT seg_estudo_id_protocolo_fkey FOREIGN KEY (id_protocolo) REFERENCES public.seg_protocolo(id_protocolo);


--
-- TOC entry 4428 (class 2606 OID 45157)
-- Name: seg_estudo seg_estudo_id_relatorio_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_estudo
    ADD CONSTRAINT seg_estudo_id_relatorio_fkey FOREIGN KEY (id_relatorio) REFERENCES public.seg_relatorio(id_relatorio);


--
-- TOC entry 4429 (class 2606 OID 45162)
-- Name: seg_estudo seg_estudo_id_seg_rep_cli_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_estudo
    ADD CONSTRAINT seg_estudo_id_seg_rep_cli_fkey FOREIGN KEY (id_seg_rep_cli) REFERENCES public.seg_rep_cli(id_seg_rep_cli);


--
-- TOC entry 4430 (class 2606 OID 45167)
-- Name: seg_estudo seg_estudo_id_tecno_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_estudo
    ADD CONSTRAINT seg_estudo_id_tecno_fkey FOREIGN KEY (id_tecno) REFERENCES public.seg_usuario(id_usuario);


--
-- TOC entry 4433 (class 2606 OID 45172)
-- Name: seg_estudo_medico seg_estudo_medico_id_estudo_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_estudo_medico
    ADD CONSTRAINT seg_estudo_medico_id_estudo_fkey FOREIGN KEY (id_estudo) REFERENCES public.seg_estudo(id_estudo) ON DELETE CASCADE;


--
-- TOC entry 4434 (class 2606 OID 45177)
-- Name: seg_estudo_medico seg_estudo_medico_id_medico_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_estudo_medico
    ADD CONSTRAINT seg_estudo_medico_id_medico_fkey FOREIGN KEY (id_medico) REFERENCES public.seg_medico(id_medico);


--
-- TOC entry 4435 (class 2606 OID 45182)
-- Name: seg_funcao_epi seg_funcao_epi_id_estudo_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_funcao_epi
    ADD CONSTRAINT seg_funcao_epi_id_estudo_fkey FOREIGN KEY (id_estudo) REFERENCES public.seg_estudo(id_estudo) ON DELETE CASCADE;


--
-- TOC entry 4436 (class 2606 OID 45187)
-- Name: seg_funcionario seg_funcionario_id_cidade_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_funcionario
    ADD CONSTRAINT seg_funcionario_id_cidade_fkey FOREIGN KEY (id_cidade) REFERENCES public.seg_cidade_ibge(id_cidade_ibge);


--
-- TOC entry 4445 (class 2606 OID 45192)
-- Name: seg_ghe_fonte_agente_epi seg_ghe_fonte_agente_epi_id_epi_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_ghe_fonte_agente_epi
    ADD CONSTRAINT seg_ghe_fonte_agente_epi_id_epi_fkey FOREIGN KEY (id_epi) REFERENCES public.seg_epi(id_epi);


--
-- TOC entry 4446 (class 2606 OID 45197)
-- Name: seg_ghe_fonte_agente_epi seg_ghe_fonte_agente_epi_id_ghe_fonte_agente_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_ghe_fonte_agente_epi
    ADD CONSTRAINT seg_ghe_fonte_agente_epi_id_ghe_fonte_agente_fkey FOREIGN KEY (id_ghe_fonte_agente) REFERENCES public.seg_ghe_fonte_agente(id_ghe_fonte_agente) ON DELETE CASCADE;


--
-- TOC entry 4449 (class 2606 OID 45202)
-- Name: seg_ghe_fonte_agente_exame_aso seg_ghe_fonte_agente_exame_aso_id_aso_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_ghe_fonte_agente_exame_aso
    ADD CONSTRAINT seg_ghe_fonte_agente_exame_aso_id_aso_fkey FOREIGN KEY (id_aso) REFERENCES public.seg_aso(id_aso);


--
-- TOC entry 4450 (class 2606 OID 45207)
-- Name: seg_ghe_fonte_agente_exame_aso seg_ghe_fonte_agente_exame_aso_id_ghe_fonte_agente_exame_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_ghe_fonte_agente_exame_aso
    ADD CONSTRAINT seg_ghe_fonte_agente_exame_aso_id_ghe_fonte_agente_exame_fkey FOREIGN KEY (id_ghe_fonte_agente_exame) REFERENCES public.seg_ghe_fonte_agente_exame(id_ghe_fonte_agente_exame);


--
-- TOC entry 4451 (class 2606 OID 45212)
-- Name: seg_ghe_fonte_agente_exame_aso seg_ghe_fonte_agente_exame_aso_id_medico_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_ghe_fonte_agente_exame_aso
    ADD CONSTRAINT seg_ghe_fonte_agente_exame_aso_id_medico_fkey FOREIGN KEY (id_medico) REFERENCES public.seg_medico(id_medico);


--
-- TOC entry 4452 (class 2606 OID 45217)
-- Name: seg_ghe_fonte_agente_exame_aso seg_ghe_fonte_agente_exame_aso_id_prestador_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_ghe_fonte_agente_exame_aso
    ADD CONSTRAINT seg_ghe_fonte_agente_exame_aso_id_prestador_fkey FOREIGN KEY (id_prestador) REFERENCES public.seg_cliente(id_cliente);


--
-- TOC entry 4453 (class 2606 OID 45222)
-- Name: seg_ghe_fonte_agente_exame_aso seg_ghe_fonte_agente_exame_aso_id_protocolo_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_ghe_fonte_agente_exame_aso
    ADD CONSTRAINT seg_ghe_fonte_agente_exame_aso_id_protocolo_fkey FOREIGN KEY (id_protocolo) REFERENCES public.seg_protocolo(id_protocolo);


--
-- TOC entry 4454 (class 2606 OID 45227)
-- Name: seg_ghe_fonte_agente_exame_aso seg_ghe_fonte_agente_exame_aso_id_sala_exame_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_ghe_fonte_agente_exame_aso
    ADD CONSTRAINT seg_ghe_fonte_agente_exame_aso_id_sala_exame_fkey FOREIGN KEY (id_sala_exame) REFERENCES public.seg_sala_exame(id_sala_exame);


--
-- TOC entry 4455 (class 2606 OID 45232)
-- Name: seg_ghe_fonte_agente_exame_aso seg_ghe_fonte_agente_exame_aso_id_usuario_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_ghe_fonte_agente_exame_aso
    ADD CONSTRAINT seg_ghe_fonte_agente_exame_aso_id_usuario_fkey FOREIGN KEY (id_usuario) REFERENCES public.seg_usuario(id_usuario);


--
-- TOC entry 4447 (class 2606 OID 45237)
-- Name: seg_ghe_fonte_agente_exame seg_ghe_fonte_agente_exame_id_exame_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_ghe_fonte_agente_exame
    ADD CONSTRAINT seg_ghe_fonte_agente_exame_id_exame_fkey FOREIGN KEY (id_exame) REFERENCES public.seg_exame(id_exame);


--
-- TOC entry 4448 (class 2606 OID 45242)
-- Name: seg_ghe_fonte_agente_exame seg_ghe_fonte_agente_exame_id_ghe_fonte_agente_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_ghe_fonte_agente_exame
    ADD CONSTRAINT seg_ghe_fonte_agente_exame_id_ghe_fonte_agente_fkey FOREIGN KEY (id_ghe_fonte_agente) REFERENCES public.seg_ghe_fonte_agente(id_ghe_fonte_agente) ON DELETE CASCADE;


--
-- TOC entry 4456 (class 2606 OID 45247)
-- Name: seg_ghe_fonte_agente_exame_periodicidade seg_ghe_fonte_agente_exame_perio_id_ghe_fonte_agente_exame_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_ghe_fonte_agente_exame_periodicidade
    ADD CONSTRAINT seg_ghe_fonte_agente_exame_perio_id_ghe_fonte_agente_exame_fkey FOREIGN KEY (id_ghe_fonte_agente_exame) REFERENCES public.seg_ghe_fonte_agente_exame(id_ghe_fonte_agente_exame) ON DELETE CASCADE;


--
-- TOC entry 4457 (class 2606 OID 45252)
-- Name: seg_ghe_fonte_agente_exame_periodicidade seg_ghe_fonte_agente_exame_periodicidade_id_periodicidade_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_ghe_fonte_agente_exame_periodicidade
    ADD CONSTRAINT seg_ghe_fonte_agente_exame_periodicidade_id_periodicidade_fkey FOREIGN KEY (id_periodicidade) REFERENCES public.seg_periodicidade(id_periodicidade);


--
-- TOC entry 4440 (class 2606 OID 45257)
-- Name: seg_ghe_fonte_agente seg_ghe_fonte_agente_id_agente_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_ghe_fonte_agente
    ADD CONSTRAINT seg_ghe_fonte_agente_id_agente_fkey FOREIGN KEY (id_agente) REFERENCES public.seg_agente(id_agente);


--
-- TOC entry 4441 (class 2606 OID 45262)
-- Name: seg_ghe_fonte_agente seg_ghe_fonte_agente_id_ghe_fonte_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_ghe_fonte_agente
    ADD CONSTRAINT seg_ghe_fonte_agente_id_ghe_fonte_fkey FOREIGN KEY (id_ghe_fonte) REFERENCES public.seg_ghe_fonte(id_ghe_fonte) ON DELETE CASCADE;


--
-- TOC entry 4442 (class 2606 OID 45267)
-- Name: seg_ghe_fonte_agente seg_ghe_fonte_agente_id_grad_efeito_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_ghe_fonte_agente
    ADD CONSTRAINT seg_ghe_fonte_agente_id_grad_efeito_fkey FOREIGN KEY (id_grad_efeito) REFERENCES public.seg_grad_efeito(id_grad_efeito);


--
-- TOC entry 4443 (class 2606 OID 45272)
-- Name: seg_ghe_fonte_agente seg_ghe_fonte_agente_id_grad_exposicao_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_ghe_fonte_agente
    ADD CONSTRAINT seg_ghe_fonte_agente_id_grad_exposicao_fkey FOREIGN KEY (id_grad_exposicao) REFERENCES public.seg_grad_exposicao(id_grad_exposicao);


--
-- TOC entry 4444 (class 2606 OID 45277)
-- Name: seg_ghe_fonte_agente seg_ghe_fonte_agente_id_grad_soma_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_ghe_fonte_agente
    ADD CONSTRAINT seg_ghe_fonte_agente_id_grad_soma_fkey FOREIGN KEY (id_grad_soma) REFERENCES public.seg_grad_soma(id_grad_soma);


--
-- TOC entry 4438 (class 2606 OID 45282)
-- Name: seg_ghe_fonte seg_ghe_fonte_id_fonte_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_ghe_fonte
    ADD CONSTRAINT seg_ghe_fonte_id_fonte_fkey FOREIGN KEY (id_fonte) REFERENCES public.seg_fonte(id_fonte);


--
-- TOC entry 4439 (class 2606 OID 45287)
-- Name: seg_ghe_fonte seg_ghe_fonte_id_ghe_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_ghe_fonte
    ADD CONSTRAINT seg_ghe_fonte_id_ghe_fkey FOREIGN KEY (id_ghe) REFERENCES public.seg_ghe(id_ghe) ON DELETE CASCADE;


--
-- TOC entry 4437 (class 2606 OID 45292)
-- Name: seg_ghe seg_ghe_id_estudo_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_ghe
    ADD CONSTRAINT seg_ghe_id_estudo_fkey FOREIGN KEY (id_estudo) REFERENCES public.seg_estudo(id_estudo) ON DELETE CASCADE;


--
-- TOC entry 4463 (class 2606 OID 45297)
-- Name: seg_ghe_setor_cliente_funcao_exame seg_ghe_setor_cliente_funcao_e_id_ghe_setor_cliente_funcao_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_ghe_setor_cliente_funcao_exame
    ADD CONSTRAINT seg_ghe_setor_cliente_funcao_e_id_ghe_setor_cliente_funcao_fkey FOREIGN KEY (id_ghe_setor_cliente_funcao) REFERENCES public.seg_ghe_setor_cliente_funcao(id_ghe_setor_cliente_funcao);


--
-- TOC entry 4464 (class 2606 OID 45302)
-- Name: seg_ghe_setor_cliente_funcao_exame seg_ghe_setor_cliente_funcao_exa_id_ghe_fonte_agente_exame_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_ghe_setor_cliente_funcao_exame
    ADD CONSTRAINT seg_ghe_setor_cliente_funcao_exa_id_ghe_fonte_agente_exame_fkey FOREIGN KEY (id_ghe_fonte_agente_exame) REFERENCES public.seg_ghe_fonte_agente_exame(id_ghe_fonte_agente_exame) ON DELETE CASCADE;


--
-- TOC entry 4465 (class 2606 OID 45307)
-- Name: seg_ghe_setor_cliente_funcao_exame seg_ghe_setor_cliente_funcao_exame_id_exame_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_ghe_setor_cliente_funcao_exame
    ADD CONSTRAINT seg_ghe_setor_cliente_funcao_exame_id_exame_fkey FOREIGN KEY (id_exame) REFERENCES public.seg_exame(id_exame);


--
-- TOC entry 4460 (class 2606 OID 45312)
-- Name: seg_ghe_setor_cliente_funcao seg_ghe_setor_cliente_funcao_id_comentario_funcao_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_ghe_setor_cliente_funcao
    ADD CONSTRAINT seg_ghe_setor_cliente_funcao_id_comentario_funcao_fkey FOREIGN KEY (id_comentario_funcao) REFERENCES public.seg_comentario_funcao(id_comentario_funcao);


--
-- TOC entry 4461 (class 2606 OID 45317)
-- Name: seg_ghe_setor_cliente_funcao seg_ghe_setor_cliente_funcao_id_ghe_setor_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_ghe_setor_cliente_funcao
    ADD CONSTRAINT seg_ghe_setor_cliente_funcao_id_ghe_setor_fkey FOREIGN KEY (id_ghe_setor) REFERENCES public.seg_ghe_setor(id_ghe_setor) ON DELETE CASCADE;


--
-- TOC entry 4462 (class 2606 OID 45322)
-- Name: seg_ghe_setor_cliente_funcao seg_ghe_setor_cliente_funcao_id_seg_cliente_funcao_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_ghe_setor_cliente_funcao
    ADD CONSTRAINT seg_ghe_setor_cliente_funcao_id_seg_cliente_funcao_fkey FOREIGN KEY (id_seg_cliente_funcao) REFERENCES public.seg_cliente_funcao(id_seg_cliente_funcao);


--
-- TOC entry 4458 (class 2606 OID 45327)
-- Name: seg_ghe_setor seg_ghe_setor_id_ghe_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_ghe_setor
    ADD CONSTRAINT seg_ghe_setor_id_ghe_fkey FOREIGN KEY (id_ghe) REFERENCES public.seg_ghe(id_ghe) ON DELETE CASCADE;


--
-- TOC entry 4459 (class 2606 OID 45332)
-- Name: seg_ghe_setor seg_ghe_setor_id_setor_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_ghe_setor
    ADD CONSTRAINT seg_ghe_setor_id_setor_fkey FOREIGN KEY (id_setor) REFERENCES public.seg_setor(id_setor);


--
-- TOC entry 4466 (class 2606 OID 45337)
-- Name: seg_hospital seg_hospital_id_cidade_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_hospital
    ADD CONSTRAINT seg_hospital_id_cidade_fkey FOREIGN KEY (id_cidade) REFERENCES public.seg_cidade_ibge(id_cidade_ibge);


--
-- TOC entry 4467 (class 2606 OID 45342)
-- Name: seg_material_hospitalar_estudo seg_material_hospitalar_estudo_id_estudo_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_material_hospitalar_estudo
    ADD CONSTRAINT seg_material_hospitalar_estudo_id_estudo_fkey FOREIGN KEY (id_estudo) REFERENCES public.seg_estudo(id_estudo) ON DELETE CASCADE;


--
-- TOC entry 4468 (class 2606 OID 45347)
-- Name: seg_material_hospitalar_estudo seg_material_hospitalar_estudo_id_material_hospitalar_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_material_hospitalar_estudo
    ADD CONSTRAINT seg_material_hospitalar_estudo_id_material_hospitalar_fkey FOREIGN KEY (id_material_hospitalar) REFERENCES public.seg_material_hospitalar(id_material_hospitalar);


--
-- TOC entry 4470 (class 2606 OID 45352)
-- Name: seg_medico_exame seg_medico_exame_id_exame_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_medico_exame
    ADD CONSTRAINT seg_medico_exame_id_exame_fkey FOREIGN KEY (id_exame) REFERENCES public.seg_exame(id_exame) ON DELETE CASCADE;


--
-- TOC entry 4471 (class 2606 OID 45357)
-- Name: seg_medico_exame seg_medico_exame_id_medico_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_medico_exame
    ADD CONSTRAINT seg_medico_exame_id_medico_fkey FOREIGN KEY (id_medico) REFERENCES public.seg_medico(id_medico) ON DELETE CASCADE;


--
-- TOC entry 4469 (class 2606 OID 45362)
-- Name: seg_medico seg_medico_id_cidade_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_medico
    ADD CONSTRAINT seg_medico_id_cidade_fkey FOREIGN KEY (id_cidade) REFERENCES public.seg_cidade_ibge(id_cidade_ibge);


--
-- TOC entry 4475 (class 2606 OID 45367)
-- Name: seg_monitoramento_ghefonteagente seg_monitoramento_ghefonteagente_id_ghe_fonte_agente_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_monitoramento_ghefonteagente
    ADD CONSTRAINT seg_monitoramento_ghefonteagente_id_ghe_fonte_agente_fkey FOREIGN KEY (id_ghe_fonte_agente) REFERENCES public.seg_ghe_fonte_agente(id_ghe_fonte_agente) ON DELETE CASCADE;


--
-- TOC entry 4476 (class 2606 OID 45372)
-- Name: seg_monitoramento_ghefonteagente seg_monitoramento_ghefonteagente_id_monitoramento_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_monitoramento_ghefonteagente
    ADD CONSTRAINT seg_monitoramento_ghefonteagente_id_monitoramento_fkey FOREIGN KEY (id_monitoramento) REFERENCES public.seg_monitoramento(id_monitoramento) ON DELETE CASCADE;


--
-- TOC entry 4472 (class 2606 OID 45377)
-- Name: seg_monitoramento seg_monitoramento_id_cliente_funcao_funcionario_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_monitoramento
    ADD CONSTRAINT seg_monitoramento_id_cliente_funcao_funcionario_fkey FOREIGN KEY (id_cliente_funcao_funcionario) REFERENCES public.seg_cliente_funcao_funcionario(id_cliente_funcao_funcionario);


--
-- TOC entry 4473 (class 2606 OID 45382)
-- Name: seg_monitoramento seg_monitoramento_id_monitoramento_anterior_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_monitoramento
    ADD CONSTRAINT seg_monitoramento_id_monitoramento_anterior_fkey FOREIGN KEY (id_monitoramento_anterior) REFERENCES public.seg_monitoramento(id_monitoramento);


--
-- TOC entry 4474 (class 2606 OID 45387)
-- Name: seg_monitoramento seg_monitoramento_id_usuario_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_monitoramento
    ADD CONSTRAINT seg_monitoramento_id_usuario_fkey FOREIGN KEY (id_usuario) REFERENCES public.seg_usuario(id_usuario);


--
-- TOC entry 4477 (class 2606 OID 45392)
-- Name: seg_movimento seg_movimento_id_cliente_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_movimento
    ADD CONSTRAINT seg_movimento_id_cliente_fkey FOREIGN KEY (id_cliente) REFERENCES public.seg_cliente(id_cliente);


--
-- TOC entry 4478 (class 2606 OID 45397)
-- Name: seg_movimento seg_movimento_id_cliente_funcao_exame_aso_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_movimento
    ADD CONSTRAINT seg_movimento_id_cliente_funcao_exame_aso_fkey FOREIGN KEY (id_cliente_funcao_exame_aso) REFERENCES public.seg_cliente_funcao_exame_aso(id_cliente_funcao_exame_aso);


--
-- TOC entry 4479 (class 2606 OID 45402)
-- Name: seg_movimento seg_movimento_id_cliente_unidade_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_movimento
    ADD CONSTRAINT seg_movimento_id_cliente_unidade_fkey FOREIGN KEY (id_cliente_unidade) REFERENCES public.seg_cliente(id_cliente);


--
-- TOC entry 4480 (class 2606 OID 45407)
-- Name: seg_movimento seg_movimento_id_empresa_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_movimento
    ADD CONSTRAINT seg_movimento_id_empresa_fkey FOREIGN KEY (id_empresa) REFERENCES public.seg_empresa(id_empresa);


--
-- TOC entry 4481 (class 2606 OID 45412)
-- Name: seg_movimento seg_movimento_id_ghe_fonte_agente_exame_aso_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_movimento
    ADD CONSTRAINT seg_movimento_id_ghe_fonte_agente_exame_aso_fkey FOREIGN KEY (id_ghe_fonte_agente_exame_aso) REFERENCES public.seg_ghe_fonte_agente_exame_aso(id_ghe_fonte_agente_exame_aso);


--
-- TOC entry 4482 (class 2606 OID 45417)
-- Name: seg_movimento seg_movimento_id_nfe_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_movimento
    ADD CONSTRAINT seg_movimento_id_nfe_fkey FOREIGN KEY (id_nfe) REFERENCES public.seg_nfe(id_nfe);


--
-- TOC entry 4483 (class 2606 OID 45422)
-- Name: seg_movimento seg_movimento_id_produto_contrato_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_movimento
    ADD CONSTRAINT seg_movimento_id_produto_contrato_fkey FOREIGN KEY (id_produto_contrato) REFERENCES public.seg_produto_contrato(id_produto_contrato);


--
-- TOC entry 4484 (class 2606 OID 45427)
-- Name: seg_movimento seg_movimento_id_protocolo_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_movimento
    ADD CONSTRAINT seg_movimento_id_protocolo_fkey FOREIGN KEY (id_protocolo) REFERENCES public.seg_protocolo(id_protocolo);


--
-- TOC entry 4485 (class 2606 OID 45432)
-- Name: seg_movimento seg_movimento_id_usuario_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_movimento
    ADD CONSTRAINT seg_movimento_id_usuario_fkey FOREIGN KEY (id_usuario) REFERENCES public.seg_usuario(id_usuario);


--
-- TOC entry 4486 (class 2606 OID 45437)
-- Name: seg_nfe seg_nfe_id_cliente_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_nfe
    ADD CONSTRAINT seg_nfe_id_cliente_fkey FOREIGN KEY (id_cliente) REFERENCES public.seg_cliente(id_cliente);


--
-- TOC entry 4487 (class 2606 OID 45442)
-- Name: seg_nfe seg_nfe_id_empresa_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_nfe
    ADD CONSTRAINT seg_nfe_id_empresa_fkey FOREIGN KEY (id_empresa) REFERENCES public.seg_empresa(id_empresa);


--
-- TOC entry 4488 (class 2606 OID 45447)
-- Name: seg_nfe seg_nfe_id_plano_forma_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_nfe
    ADD CONSTRAINT seg_nfe_id_plano_forma_fkey FOREIGN KEY (id_plano_forma) REFERENCES public.seg_plano_forma(id_plano_forma);


--
-- TOC entry 4489 (class 2606 OID 45452)
-- Name: seg_nfe seg_nfe_id_usuario_cancela_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_nfe
    ADD CONSTRAINT seg_nfe_id_usuario_cancela_fkey FOREIGN KEY (id_usuario_cancela) REFERENCES public.seg_usuario(id_usuario);


--
-- TOC entry 4490 (class 2606 OID 45457)
-- Name: seg_nfe seg_nfe_id_usuario_criador_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_nfe
    ADD CONSTRAINT seg_nfe_id_usuario_criador_fkey FOREIGN KEY (id_usuario_criador) REFERENCES public.seg_usuario(id_usuario);


--
-- TOC entry 4491 (class 2606 OID 45462)
-- Name: seg_norma_ghe seg_norma_ghe_id_ghe_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_norma_ghe
    ADD CONSTRAINT seg_norma_ghe_id_ghe_fkey FOREIGN KEY (id_ghe) REFERENCES public.seg_ghe(id_ghe) ON DELETE CASCADE;


--
-- TOC entry 4492 (class 2606 OID 45467)
-- Name: seg_norma_ghe seg_norma_ghe_id_norma_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_norma_ghe
    ADD CONSTRAINT seg_norma_ghe_id_norma_fkey FOREIGN KEY (id_norma) REFERENCES public.seg_norma(id_norma);


--
-- TOC entry 4493 (class 2606 OID 45472)
-- Name: seg_parcela seg_parcela_id_plano_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_parcela
    ADD CONSTRAINT seg_parcela_id_plano_fkey FOREIGN KEY (id_plano) REFERENCES public.seg_plano(id_plano) ON DELETE CASCADE;


--
-- TOC entry 4494 (class 2606 OID 45477)
-- Name: seg_perfil_dominio_acao seg_perfil_dominio_acao_id_dominio_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_perfil_dominio_acao
    ADD CONSTRAINT seg_perfil_dominio_acao_id_dominio_fkey FOREIGN KEY (id_dominio, id_acao) REFERENCES public.seg_dominio_acao(id_dominio, id_acao) ON DELETE CASCADE;


--
-- TOC entry 4495 (class 2606 OID 45482)
-- Name: seg_perfil_dominio_acao seg_perfil_dominio_acao_id_perfil_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_perfil_dominio_acao
    ADD CONSTRAINT seg_perfil_dominio_acao_id_perfil_fkey FOREIGN KEY (id_perfil) REFERENCES public.seg_perfil(id_perfil);


--
-- TOC entry 4496 (class 2606 OID 45487)
-- Name: seg_plano_forma seg_plano_forma_id_forma_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_plano_forma
    ADD CONSTRAINT seg_plano_forma_id_forma_fkey FOREIGN KEY (id_forma) REFERENCES public.seg_forma(id_forma);


--
-- TOC entry 4497 (class 2606 OID 45492)
-- Name: seg_plano_forma seg_plano_forma_id_plano_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_plano_forma
    ADD CONSTRAINT seg_plano_forma_id_plano_fkey FOREIGN KEY (id_plano) REFERENCES public.seg_plano(id_plano);


--
-- TOC entry 4498 (class 2606 OID 45497)
-- Name: seg_produto_contrato seg_produto_contrato_id_contrato_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_produto_contrato
    ADD CONSTRAINT seg_produto_contrato_id_contrato_fkey FOREIGN KEY (id_contrato) REFERENCES public.seg_contrato(id_contrato) ON DELETE CASCADE;


--
-- TOC entry 4499 (class 2606 OID 45502)
-- Name: seg_produto_contrato seg_produto_contrato_id_produto_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_produto_contrato
    ADD CONSTRAINT seg_produto_contrato_id_produto_fkey FOREIGN KEY (id_produto) REFERENCES public.seg_produto(id_produto) ON DELETE CASCADE;


--
-- TOC entry 4500 (class 2606 OID 45507)
-- Name: seg_prontuario seg_prontuario_id_aso_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_prontuario
    ADD CONSTRAINT seg_prontuario_id_aso_fkey FOREIGN KEY (id_aso) REFERENCES public.seg_aso(id_aso) ON DELETE CASCADE;


--
-- TOC entry 4501 (class 2606 OID 45512)
-- Name: seg_prontuario seg_prontuario_id_usuario_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_prontuario
    ADD CONSTRAINT seg_prontuario_id_usuario_fkey FOREIGN KEY (id_usuario) REFERENCES public.seg_usuario(id_usuario);


--
-- TOC entry 4502 (class 2606 OID 45517)
-- Name: seg_protocolo seg_protocolo_id_cliente_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_protocolo
    ADD CONSTRAINT seg_protocolo_id_cliente_fkey FOREIGN KEY (id_cliente) REFERENCES public.seg_cliente(id_cliente);


--
-- TOC entry 4503 (class 2606 OID 45522)
-- Name: seg_protocolo seg_protocolo_id_usuario_cancelou_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_protocolo
    ADD CONSTRAINT seg_protocolo_id_usuario_cancelou_fkey FOREIGN KEY (id_usuario_cancelou) REFERENCES public.seg_usuario(id_usuario);


--
-- TOC entry 4504 (class 2606 OID 45527)
-- Name: seg_protocolo seg_protocolo_id_usuario_finalizou_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_protocolo
    ADD CONSTRAINT seg_protocolo_id_usuario_finalizou_fkey FOREIGN KEY (id_usuario_finalizou) REFERENCES public.seg_usuario(id_usuario);


--
-- TOC entry 4505 (class 2606 OID 45532)
-- Name: seg_protocolo seg_protocolo_id_usuario_gerou_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_protocolo
    ADD CONSTRAINT seg_protocolo_id_usuario_gerou_fkey FOREIGN KEY (id_usuario_gerou) REFERENCES public.seg_usuario(id_usuario);


--
-- TOC entry 4506 (class 2606 OID 45537)
-- Name: seg_relanual seg_relanual_id_cliente_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_relanual
    ADD CONSTRAINT seg_relanual_id_cliente_fkey FOREIGN KEY (id_cliente) REFERENCES public.seg_cliente(id_cliente);


--
-- TOC entry 4507 (class 2606 OID 45542)
-- Name: seg_relanual seg_relanual_id_medico_coordenador_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_relanual
    ADD CONSTRAINT seg_relanual_id_medico_coordenador_fkey FOREIGN KEY (id_medico_coordenador) REFERENCES public.seg_medico(id_medico);


--
-- TOC entry 4508 (class 2606 OID 45547)
-- Name: seg_relanual seg_relanual_id_usuario_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_relanual
    ADD CONSTRAINT seg_relanual_id_usuario_fkey FOREIGN KEY (id_usuario) REFERENCES public.seg_usuario(id_usuario);


--
-- TOC entry 4509 (class 2606 OID 45552)
-- Name: seg_relanual_item seg_relanual_item_id_relanual_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_relanual_item
    ADD CONSTRAINT seg_relanual_item_id_relanual_fkey FOREIGN KEY (id_relanual) REFERENCES public.seg_relanual(id_relanual) ON DELETE CASCADE;


--
-- TOC entry 4510 (class 2606 OID 45557)
-- Name: seg_relatorio_exame seg_relatorio_exame_id_exame_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_relatorio_exame
    ADD CONSTRAINT seg_relatorio_exame_id_exame_fkey FOREIGN KEY (id_exame) REFERENCES public.seg_exame(id_exame);


--
-- TOC entry 4511 (class 2606 OID 45562)
-- Name: seg_relatorio_exame seg_relatorio_exame_id_relatorio_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_relatorio_exame
    ADD CONSTRAINT seg_relatorio_exame_id_relatorio_fkey FOREIGN KEY (id_relatorio) REFERENCES public.seg_relatorio(id_relatorio) ON DELETE CASCADE;


--
-- TOC entry 4512 (class 2606 OID 45567)
-- Name: seg_rep_cli seg_rep_cli_id_cliente_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_rep_cli
    ADD CONSTRAINT seg_rep_cli_id_cliente_fkey FOREIGN KEY (id_cliente) REFERENCES public.seg_cliente(id_cliente);


--
-- TOC entry 4513 (class 2606 OID 45572)
-- Name: seg_rep_cli seg_rep_cli_id_vend_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_rep_cli
    ADD CONSTRAINT seg_rep_cli_id_vend_fkey FOREIGN KEY (id_vend) REFERENCES public.seg_vendedor(id_vend);


--
-- TOC entry 4514 (class 2606 OID 45577)
-- Name: seg_revisao seg_revisao_id_estudo_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_revisao
    ADD CONSTRAINT seg_revisao_id_estudo_fkey FOREIGN KEY (id_estudo) REFERENCES public.seg_estudo(id_estudo) ON DELETE CASCADE;


--
-- TOC entry 4516 (class 2606 OID 45582)
-- Name: seg_sala_exame seg_sala_exame_id_exame_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_sala_exame
    ADD CONSTRAINT seg_sala_exame_id_exame_fkey FOREIGN KEY (id_exame) REFERENCES public.seg_exame(id_exame);


--
-- TOC entry 4517 (class 2606 OID 45587)
-- Name: seg_sala_exame seg_sala_exame_id_sala_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_sala_exame
    ADD CONSTRAINT seg_sala_exame_id_sala_fkey FOREIGN KEY (id_sala) REFERENCES public.seg_sala(id_sala) ON DELETE CASCADE;


--
-- TOC entry 4515 (class 2606 OID 45592)
-- Name: seg_sala seg_sala_id_empresa_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_sala
    ADD CONSTRAINT seg_sala_id_empresa_fkey FOREIGN KEY (id_empresa) REFERENCES public.seg_empresa(id_empresa);


--
-- TOC entry 4520 (class 2606 OID 45597)
-- Name: seg_usuario_cliente seg_usuario_cliente_id_cliente_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_usuario_cliente
    ADD CONSTRAINT seg_usuario_cliente_id_cliente_fkey FOREIGN KEY (id_cliente) REFERENCES public.seg_cliente(id_cliente) ON DELETE CASCADE;


--
-- TOC entry 4521 (class 2606 OID 45602)
-- Name: seg_usuario_cliente seg_usuario_cliente_id_usuario_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_usuario_cliente
    ADD CONSTRAINT seg_usuario_cliente_id_usuario_fkey FOREIGN KEY (id_usuario) REFERENCES public.seg_usuario(id_usuario) ON DELETE CASCADE;


--
-- TOC entry 4522 (class 2606 OID 45607)
-- Name: seg_usuario_funcao_interna seg_usuario_funcao_interna_id_funcao_interna_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_usuario_funcao_interna
    ADD CONSTRAINT seg_usuario_funcao_interna_id_funcao_interna_fkey FOREIGN KEY (id_funcao_interna) REFERENCES public.seg_funcao_interna(id_funcao_interna);


--
-- TOC entry 4523 (class 2606 OID 45612)
-- Name: seg_usuario_funcao_interna seg_usuario_funcao_interna_id_usuario_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_usuario_funcao_interna
    ADD CONSTRAINT seg_usuario_funcao_interna_id_usuario_fkey FOREIGN KEY (id_usuario) REFERENCES public.seg_usuario(id_usuario);


--
-- TOC entry 4518 (class 2606 OID 45617)
-- Name: seg_usuario seg_usuario_id_empresa_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_usuario
    ADD CONSTRAINT seg_usuario_id_empresa_fkey FOREIGN KEY (id_empresa) REFERENCES public.seg_empresa(id_empresa);


--
-- TOC entry 4519 (class 2606 OID 45622)
-- Name: seg_usuario seg_usuario_id_medico_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_usuario
    ADD CONSTRAINT seg_usuario_id_medico_fkey FOREIGN KEY (id_medico) REFERENCES public.seg_medico(id_medico);


--
-- TOC entry 4524 (class 2606 OID 45627)
-- Name: seg_usuario_perfil seg_usuario_perfil_id_perfil_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_usuario_perfil
    ADD CONSTRAINT seg_usuario_perfil_id_perfil_fkey FOREIGN KEY (id_perfil) REFERENCES public.seg_perfil(id_perfil);


--
-- TOC entry 4525 (class 2606 OID 45632)
-- Name: seg_usuario_perfil seg_usuario_perfil_id_usuario_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_usuario_perfil
    ADD CONSTRAINT seg_usuario_perfil_id_usuario_fkey FOREIGN KEY (id_usuario) REFERENCES public.seg_usuario(id_usuario);


--
-- TOC entry 4526 (class 2606 OID 45637)
-- Name: seg_vendedor seg_vendedor_id_cidade_ibge_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_vendedor
    ADD CONSTRAINT seg_vendedor_id_cidade_ibge_fkey FOREIGN KEY (id_cidade_ibge) REFERENCES public.seg_cidade_ibge(id_cidade_ibge);


--
-- TOC entry 4527 (class 2606 OID 45642)
-- Name: seg_versao seg_versao_id_estudo_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_versao
    ADD CONSTRAINT seg_versao_id_estudo_fkey FOREIGN KEY (id_estudo) REFERENCES public.seg_estudo(id_estudo) ON DELETE CASCADE;


-- Completed on 2024-09-17 21:37:14

--
-- PostgreSQL database dump complete
--

