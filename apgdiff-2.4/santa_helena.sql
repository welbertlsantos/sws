--
-- PostgreSQL database dump
--

-- Dumped from database version 9.6.24
-- Dumped by pg_dump version 11.19

-- Started on 2024-09-17 21:37:52

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--
-- TOC entry 746 (class 1247 OID 19011)
-- Name: atendimento; Type: TYPE; Schema: public; Owner: postgres
--

CREATE TYPE public.atendimento AS (
	codigo_aso character varying,
	usuario character varying,
	sala character varying,
	exame character varying,
	periodicidade character varying,
	empresa character varying,
	nome_funcionario character varying,
	rg character varying,
	data_nascimento date,
	funcao character varying,
	peso integer,
	altura numeric(8,2),
	situacao character varying,
	data_atendimento timestamp without time zone,
	idade character varying,
	sexo character varying,
	tiposan character varying,
	ftrh character varying
);


ALTER TYPE public.atendimento OWNER TO postgres;

--
-- TOC entry 428 (class 1255 OID 19012)
-- Name: busca_atendimentos(character varying, character varying, bigint, bigint, bigint, character varying, bigint); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION public.busca_atendimentos(datainicial character varying, datafinal character varying, idaso bigint, idsala bigint, idusuario bigint, situacao character varying, idexame bigint) RETURNS SETOF public.atendimento
    LANGUAGE plpgsql
    AS $$
	DECLARE
	R ATENDIMENTO%ROWTYPE;
	QUERY VARCHAR;

	BEGIN
		QUERY := 'SELECT 
			A.CODIGO_ASO,--NUMERO_ATENDIMENTO
			U.NOME, --USUARIO
			S.DESCRICAO,--SALA
			E.DESCRICAO,--EXAME
			P.DESCRICAO,--PERIODICIDADE
			C.RAZAO_SOCIAL,--EMPRESA
			FUNC.NOME,--NOME_FUNCIONARIO
			FUNC.RG,
			FUNC.DATA_NASCIMENTO,
			F.DESCRICAO,--FUNCAO
			FUNC.PESO,
			FUNC.ALTURA,
			BUSCA_SITUACAO_ATENDIMENTO(GFAEA.ID_GHE_FONTE_AGENTE_EXAME_ASO, ''PCMSO''), ';
			
			IF SITUACAO = 'A' THEN
				QUERY := QUERY || ' GFAEA.DATA_ATENDIDO AS DATA_ATENDIMENTO, ';
			END IF;

			IF SITUACAO = 'F' THEN
				QUERY := QUERY || ' GFAEA.DATA_FINALIZADO AS DATA_ATENDIMENTO, ';
			END IF;
			
			IF SITUACAO = 'C' THEN
				QUERY := QUERY || ' GFAEA.DATA_CANCELADO AS DATA_ATENDIMENTO, ';
			END IF;

			IF SITUACAO = 'AF' THEN
				QUERY := QUERY || ' GFAEA.DATA_ATENDIDO AS DATA_ATENDIMENTO, ';
			END IF;
			
			QUERY := QUERY || ' EXTRACT(YEAR FROM AGE(A.DATANASC_FUNCIONARIO)) AS IDADE, ';
			QUERY := QUERY || ' FUNC.SEXO AS SEXO, ';
			QUERY := QUERY || ' FUNC.TIPOSAN AS TIPOSAN, ';
			QUERY := QUERY || ' FUNC.FTRH AS FTRH ';

		QUERY := QUERY || ' FROM 
			SEG_GHE_FONTE_AGENTE_EXAME_ASO GFAEA
			LEFT JOIN SEG_SALA_EXAME SE ON GFAEA.ID_SALA_EXAME = SE.ID_SALA_EXAME
			LEFT JOIN SEG_USUARIO U ON GFAEA.ID_USUARIO = U.ID_USUARIO
			JOIN SEG_ASO A ON GFAEA.ID_ASO = A.ID_ASO
			JOIN SEG_SALA S ON SE.ID_SALA = S.ID_SALA
			JOIN SEG_PERIODICIDADE P ON A.ID_PERIODICIDADE = P.ID_PERIODICIDADE
			JOIN SEG_CLIENTE_FUNCAO_FUNCIONARIO CFF ON A.ID_CLIENTE_FUNCAO_FUNCIONARIO = CFF.ID_CLIENTE_FUNCAO_FUNCIONARIO
			JOIN SEG_CLIENTE_FUNCAO CF ON CFF.ID_SEG_CLIENTE_FUNCAO = CF.ID_SEG_CLIENTE_FUNCAO
			JOIN SEG_CLIENTE C ON C.ID_CLIENTE = CF.ID_CLIENTE
			JOIN SEG_FUNCAO F ON CF.ID_FUNCAO = F.ID_FUNCAO
			JOIN SEG_FUNCIONARIO FUNC ON CFF.ID_FUNCIONARIO = FUNC.ID_FUNCIONARIO
			JOIN SEG_GHE_FONTE_AGENTE_EXAME GFAE ON GFAEA.ID_GHE_FONTE_AGENTE_EXAME = GFAE.ID_GHE_FONTE_AGENTE_EXAME AND GFAEA.DUPLICADO = FALSE
			JOIN SEG_EXAME E ON GFAE.ID_EXAME = E.ID_EXAME';
		
			IF IDASO <> NULL OR IDASO > 0 THEN
				QUERY := QUERY || ' AND	GFAEA.ID_ASO = ' || IDASO;
			END IF;

			IF IDSALA <> NULL OR IDSALA > 0 THEN
				QUERY := QUERY || ' AND S.ID_SALA = ' || IDSALA;
			END IF;

			IF IDUSUARIO <> NULL OR IDUSUARIO > 0 THEN
				QUERY := QUERY || ' AND U.ID_USUARIO = ' || IDUSUARIO;
			END IF;
			
			IF IDEXAME <> NULL OR IDEXAME > 0 THEN
				QUERY := QUERY || ' AND E.ID_EXAME = ' || IDEXAME;
			END IF;

			IF SITUACAO = 'A' THEN
				QUERY := QUERY || ' AND GFAEA.ATENDIDO = TRUE';
				QUERY := QUERY || ' AND GFAEA.FINALIZADO = FALSE';
				QUERY := QUERY || ' AND GFAEA.CANCELADO = FALSE';
				QUERY := QUERY || ' AND GFAEA.DATA_ATENDIDO BETWEEN TO_TIMESTAMP('''||DATAINICIAL||' 00:00:00'', ''DD/MM/YYYY HH24:MI:SS'') AND TO_TIMESTAMP('''||DATAFINAL||' 23:59:59'', ''DD/MM/YYYY HH24:MI:SS'')'; 
			END IF;

			IF SITUACAO = 'F' THEN
				QUERY := QUERY || ' AND GFAEA.ATENDIDO = TRUE';
				QUERY := QUERY || ' AND GFAEA.FINALIZADO = TRUE';
				QUERY := QUERY || ' AND GFAEA.CANCELADO = FALSE';
				QUERY := QUERY || ' AND GFAEA.DATA_FINALIZADO BETWEEN TO_TIMESTAMP('''||DATAINICIAL||' 00:00:00'', ''DD/MM/YYYY HH24:MI:SS'') AND TO_TIMESTAMP('''||DATAFINAL||' 23:59:59'', ''DD/MM/YYYY HH24:MI:SS'')'; 
			END IF;
			
			IF SITUACAO = 'C' THEN
				QUERY := QUERY || ' AND GFAEA.CANCELADO = TRUE';
				QUERY := QUERY || ' AND GFAEA.DATA_CANCELADO BETWEEN TO_TIMESTAMP('''||DATAINICIAL||' 00:00:00'', ''DD/MM/YYYY HH24:MI:SS'') AND TO_TIMESTAMP('''||DATAFINAL||' 23:59:59'', ''DD/MM/YYYY HH24:MI:SS'')'; 
			END IF;

			IF SITUACAO = 'AF' THEN
				QUERY := QUERY || ' AND (GFAEA.ATENDIDO = TRUE OR GFAEA.FINALIZADO = TRUE)';
				QUERY := QUERY || ' AND GFAEA.CANCELADO = FALSE';
				QUERY := QUERY || ' AND GFAEA.DATA_ATENDIDO BETWEEN TO_TIMESTAMP('''||DATAINICIAL||' 00:00:00'', ''DD/MM/YYYY HH24:MI:SS'') AND TO_TIMESTAMP('''||DATAFINAL||' 23:59:59'', ''DD/MM/YYYY HH24:MI:SS'')'; 
			END IF;
		
		QUERY := QUERY || ' UNION ALL(SELECT 
			A.CODIGO_ASO,--NUMERO_ATENDIMENTO
			U.NOME, --USUARIO
			S.DESCRICAO,--SALA
			E.DESCRICAO,--EXAME
			P.DESCRICAO,--PERIODICIDADE
			C.RAZAO_SOCIAL,--EMPRESA
			FUNC.NOME,--NOME_FUNCIONARIO
			FUNC.RG,
			FUNC.DATA_NASCIMENTO,
			F.DESCRICAO,--FUNCAO
			FUNC.PESO,
			FUNC.ALTURA,
			BUSCA_SITUACAO_ATENDIMENTO(CFEA.ID_CLIENTE_FUNCAO_EXAME_ASO, ''EXTRA''), ';
			
			IF SITUACAO = 'A' THEN
				QUERY := QUERY || ' CFEA.DATA_ATENDIDO AS DATA_ATENDIMENTO, ';
			END IF;

			IF SITUACAO = 'F' THEN
				QUERY := QUERY || ' CFEA.DATA_FINALIZADO AS DATA_ATENDIMENTO, ';
			END IF;
			
			IF SITUACAO = 'C' THEN
				QUERY := QUERY || ' CFEA.DATA_CANCELADO AS DATA_ATENDIMENTO, ';
			END IF;
			
			IF SITUACAO = 'AF' THEN
				QUERY := QUERY || ' CFEA.DATA_ATENDIDO AS DATA_ATENDIMENTO, ';
			END IF;

			QUERY := QUERY || ' EXTRACT(YEAR FROM AGE(A.DATANASC_FUNCIONARIO)) AS IDADE, ';
			QUERY := QUERY || ' FUNC.SEXO AS SEXO, ';
			QUERY := QUERY || ' FUNC.TIPOSAN AS TIPOSAN, ';
			QUERY := QUERY || ' FUNC.FTRH AS FTRH ';
		
		QUERY := QUERY ||' FROM 
			SEG_CLIENTE_FUNCAO_EXAME_ASO CFEA
			LEFT JOIN SEG_SALA_EXAME SE ON CFEA.ID_SALA_EXAME = SE.ID_SALA_EXAME
			LEFT JOIN SEG_USUARIO U ON CFEA.ID_USUARIO = U.ID_USUARIO
			JOIN SEG_ASO A ON CFEA.ID_ASO = A.ID_ASO
			JOIN SEG_SALA S ON SE.ID_SALA = S.ID_SALA
			JOIN SEG_PERIODICIDADE P ON A.ID_PERIODICIDADE = P.ID_PERIODICIDADE
			JOIN SEG_CLIENTE_FUNCAO_FUNCIONARIO CFF ON A.ID_CLIENTE_FUNCAO_FUNCIONARIO = CFF.ID_CLIENTE_FUNCAO_FUNCIONARIO
			JOIN SEG_CLIENTE_FUNCAO CF ON CFF.ID_SEG_CLIENTE_FUNCAO = CF.ID_SEG_CLIENTE_FUNCAO
			JOIN SEG_CLIENTE C ON C.ID_CLIENTE = CF.ID_CLIENTE
			JOIN SEG_FUNCAO F ON CF.ID_FUNCAO = F.ID_FUNCAO
			JOIN SEG_FUNCIONARIO FUNC ON CFF.ID_FUNCIONARIO = FUNC.ID_FUNCIONARIO
			JOIN SEG_CLIENTE_FUNCAO_EXAME CFE ON CFEA.ID_CLIENTE_FUNCAO_EXAME = CFE.ID_CLIENTE_FUNCAO_EXAME
			JOIN SEG_EXAME E ON CFE.ID_EXAME = E.ID_EXAME';
		
		IF IDASO <> NULL OR  IDASO > 0 THEN
			QUERY := QUERY || ' AND CFEA.ID_ASO = ' || IDASO;
		END IF;
		
		IF IDSALA <> NULL OR IDSALA > 0 THEN
			QUERY := QUERY || ' AND S.ID_SALA = ' || IDSALA;
		END IF;

		IF IDUSUARIO <> NULL OR IDUSUARIO > 0 THEN
			QUERY := QUERY || ' AND U.ID_USUARIO = ' || IDUSUARIO;
		END IF;
		
		IF IDEXAME <> NULL OR IDEXAME > 0 THEN
			QUERY := QUERY || ' AND E.ID_EXAME = ' || IDEXAME;
		END IF;

		IF SITUACAO = 'A' THEN
			QUERY := QUERY || ' AND CFEA.ATENDIDO = TRUE';
			QUERY := QUERY || ' AND CFEA.FINALIZADO = FALSE';
			QUERY := QUERY || ' AND CFEA.CANCELADO = FALSE';
			QUERY := QUERY || ' AND CFEA.DATA_ATENDIDO BETWEEN TO_TIMESTAMP('''||DATAINICIAL||' 00:00:00'', ''DD/MM/YYYY HH24:MI:SS'') AND TO_TIMESTAMP('''||DATAFINAL||' 23:59:59'', ''DD/MM/YYYY HH24:MI:SS'')'; 
		END IF;

		IF SITUACAO = 'F' THEN
			QUERY := QUERY || ' AND CFEA.ATENDIDO = TRUE';
			QUERY := QUERY || ' AND CFEA.FINALIZADO = TRUE';
			QUERY := QUERY || ' AND CFEA.CANCELADO = FALSE';
			QUERY := QUERY || ' AND CFEA.DATA_FINALIZADO BETWEEN TO_TIMESTAMP('''||DATAINICIAL||' 00:00:00'', ''DD/MM/YYYY HH24:MI:SS'') AND TO_TIMESTAMP('''||DATAFINAL||' 23:59:59'', ''DD/MM/YYYY HH24:MI:SS'')'; 
		END IF;
		
		IF SITUACAO = 'C' THEN
			QUERY := QUERY || ' AND CFEA.CANCELADO = TRUE';
			QUERY := QUERY || ' AND CFEA.DATA_CANCELADO BETWEEN TO_TIMESTAMP('''||DATAINICIAL||' 00:00:00'', ''DD/MM/YYYY HH24:MI:SS'') AND TO_TIMESTAMP('''||DATAFINAL||' 23:59:59'', ''DD/MM/YYYY HH24:MI:SS'')'; 
		END IF;

		IF SITUACAO = 'AF' THEN
			QUERY := QUERY || ' AND (CFEA.ATENDIDO = TRUE OR CFEA.FINALIZADO = TRUE)';
			QUERY := QUERY || ' AND CFEA.CANCELADO = FALSE';
			QUERY := QUERY || ' AND CFEA.DATA_ATENDIDO BETWEEN TO_TIMESTAMP('''||DATAINICIAL||' 00:00:00'', ''DD/MM/YYYY HH24:MI:SS'') AND TO_TIMESTAMP('''||DATAFINAL||' 23:59:59'', ''DD/MM/YYYY HH24:MI:SS'')'; 
		END IF;
		
		QUERY := QUERY || ') ORDER BY DATA_ATENDIMENTO ASC;';
		RAISE NOTICE 'QUERY(%)', QUERY;
		
		FOR R IN EXECUTE QUERY LOOP
			RETURN NEXT R;
		END LOOP;
		RETURN;
	END
$$;


ALTER FUNCTION public.busca_atendimentos(datainicial character varying, datafinal character varying, idaso bigint, idsala bigint, idusuario bigint, situacao character varying, idexame bigint) OWNER TO postgres;

--
-- TOC entry 408 (class 1255 OID 19013)
-- Name: busca_cnae(bigint, character, character); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION public.busca_cnae(id bigint, tipocliente character, tipograurisco character) RETURNS character varying
    LANGUAGE plpgsql
    AS $$
DECLARE
	REG VARCHAR;
	OUTPUT CHARACTER VARYING = '';
BEGIN
	FOR REG IN 
		SELECT FORMATA_CNAE(CNAE_SEC.COD_CNAE) FROM SEG_CNAE_PPRA CNAE_PPRA_SEC, SEG_CNAE CNAE_SEC WHERE  CNAE_PPRA_SEC.ID_CNAE = CNAE_SEC.ID_CNAE AND CNAE_PPRA_SEC.ID_PPRA = ID AND CNAE_PPRA_SEC.FLAG_CONT = TIPOCLIENTE AND CNAE_PPRA_SEC.FLAG_PR = TIPOGRAURISCO
	LOOP
		OUTPUT := OUTPUT || REG || '  ';
	END LOOP;
	
	RETURN OUTPUT;
END;
$$;


ALTER FUNCTION public.busca_cnae(id bigint, tipocliente character, tipograurisco character) OWNER TO postgres;

--
-- TOC entry 456 (class 1255 OID 19014)
-- Name: busca_data_exame(bigint, bigint); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION public.busca_data_exame(idaso bigint, idexame bigint) RETURNS character varying
    LANGUAGE plpgsql
    AS $$
DECLARE
	REG RECORD;
	OUTPUT VARCHAR := '';
BEGIN
	FOR REG IN 
		SELECT DISTINCT GFAEA.DATA_EXAME FROM 
		SEG_GHE_FONTE_AGENTE_EXAME_ASO GFAEA, 
		SEG_GHE_FONTE_AGENTE_EXAME GFAE,
		SEG_EXAME E
		WHERE 
		GFAEA.ID_GHE_FONTE_AGENTE_EXAME = GFAE.ID_GHE_FONTE_AGENTE_EXAME
		AND E.ID_EXAME = GFAE.ID_EXAME
		AND ID_ASO = IDASO
		AND E.ID_EXAME = IDEXAME
		AND GFAEA.CANCELADO = FALSE
		AND GFAEA.DUPLICADO = FALSE

	UNION(

		SELECT CFEA.DATA_EXAME FROM
		SEG_CLIENTE_FUNCAO_EXAME_ASO CFEA,
		SEG_CLIENTE_FUNCAO_EXAME CFE,
		SEG_EXAME E
		WHERE
		CFEA.ID_CLIENTE_FUNCAO_EXAME = CFE.ID_CLIENTE_FUNCAO_EXAME
		AND CFE.ID_EXAME = E.ID_EXAME
		AND ID_ASO = IDASO
		AND E.ID_EXAME = IDEXAME
		AND CFEA.CANCELADO = FALSE)
	LOOP
		OUTPUT := TO_CHAR(REG.DATA_EXAME, 'DD/MM/YYYY');

	END LOOP;

	RETURN OUTPUT;
END;
$$;


ALTER FUNCTION public.busca_data_exame(idaso bigint, idexame bigint) OWNER TO postgres;

--
-- TOC entry 461 (class 1255 OID 63204)
-- Name: busca_data_exame_by_aso(bigint, bigint[]); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION public.busca_data_exame_by_aso(_idaso bigint, _exames bigint[]) RETURNS character varying
    LANGUAGE plpgsql
    AS $$
DECLARE
	REG RECORD;
BEGIN
	FOR REG IN 
		SELECT DISTINCT GHE_FONTE_AGENTE_EXAME_ASO.DATA_EXAME FROM 
		SEG_GHE_FONTE_AGENTE_EXAME_ASO GHE_FONTE_AGENTE_EXAME_ASO
		LEFT JOIN SEG_GHE_FONTE_AGENTE_EXAME GHE_FONTE_AGENTE_EXAME ON GHE_FONTE_AGENTE_EXAME.ID_GHE_FONTE_AGENTE_EXAME = GHE_FONTE_AGENTE_EXAME_ASO.ID_GHE_FONTE_AGENTE_EXAME
		LEFT JOIN SEG_EXAME EXAME ON EXAME.ID_EXAME = GHE_FONTE_AGENTE_EXAME.ID_EXAME
		WHERE 
		GHE_FONTE_AGENTE_EXAME_ASO.ID_ASO = _IDASO
		AND EXAME.ID_EXAME <> ALL (_EXAMES)
		AND GHE_FONTE_AGENTE_EXAME_ASO.CANCELADO = FALSE
		AND GHE_FONTE_AGENTE_EXAME_ASO.DUPLICADO = FALSE
	UNION(
		SELECT CLIENTE_FUNCAO_EXAME_ASO.DATA_EXAME FROM
		SEG_CLIENTE_FUNCAO_EXAME_ASO CLIENTE_FUNCAO_EXAME_ASO
		LEFT JOIN SEG_CLIENTE_FUNCAO_EXAME CLIENTE_FUNCAO_EXAME ON CLIENTE_FUNCAO_EXAME.ID_CLIENTE_FUNCAO_EXAME = CLIENTE_FUNCAO_EXAME_ASO.ID_CLIENTE_FUNCAO_EXAME
		LEFT JOIN SEG_EXAME EXAME ON EXAME.ID_EXAME = CLIENTE_FUNCAO_EXAME.ID_EXAME
		WHERE
		CLIENTE_FUNCAO_EXAME_ASO.ID_ASO = _IDASO
		AND EXAME.ID_EXAME <> ALL (_EXAMES)
		AND CLIENTE_FUNCAO_EXAME_ASO.CANCELADO = FALSE)
	LOOP
		RETURN TO_CHAR(REG.DATA_EXAME, 'DD/MM/YYYY');
	END LOOP;
END;
$$;


ALTER FUNCTION public.busca_data_exame_by_aso(_idaso bigint, _exames bigint[]) OWNER TO postgres;

--
-- TOC entry 465 (class 1255 OID 327536)
-- Name: busca_data_exame_list(bigint, bigint[]); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION public.busca_data_exame_list(_id_aso bigint, _id_exame bigint[]) RETURNS character varying
    LANGUAGE plpgsql
    AS $$
DECLARE
	REG RECORD;
	OUTPUT VARCHAR := '';
BEGIN
	FOR REG IN 
		SELECT DISTINCT GFAEA.DATA_EXAME FROM 
		SEG_GHE_FONTE_AGENTE_EXAME_ASO GFAEA, 
		SEG_GHE_FONTE_AGENTE_EXAME GFAE,
		SEG_EXAME E
		WHERE 
		GFAEA.ID_GHE_FONTE_AGENTE_EXAME = GFAE.ID_GHE_FONTE_AGENTE_EXAME
		AND E.ID_EXAME = GFAE.ID_EXAME
		AND ID_ASO = _ID_ASO
		AND E.ID_EXAME = ANY (_ID_EXAME)
		AND GFAEA.CANCELADO = FALSE
		AND GFAEA.DUPLICADO = FALSE

	UNION(

		SELECT CFEA.DATA_EXAME FROM
		SEG_CLIENTE_FUNCAO_EXAME_ASO CFEA,
		SEG_CLIENTE_FUNCAO_EXAME CFE,
		SEG_EXAME E
		WHERE
		CFEA.ID_CLIENTE_FUNCAO_EXAME = CFE.ID_CLIENTE_FUNCAO_EXAME
		AND CFE.ID_EXAME = E.ID_EXAME
		AND ID_ASO = _ID_ASO
		AND E.ID_EXAME = ANY (_ID_EXAME)
		AND CFEA.CANCELADO = FALSE)
	LOOP
		OUTPUT := TO_CHAR(REG.DATA_EXAME, 'DD/MM/YYYY');

	END LOOP;

	RETURN OUTPUT;
END;
$$;


ALTER FUNCTION public.busca_data_exame_list(_id_aso bigint, _id_exame bigint[]) OWNER TO postgres;

--
-- TOC entry 413 (class 1255 OID 46339)
-- Name: busca_ghe_by_atendimento(bigint); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION public.busca_ghe_by_atendimento(idaso bigint) RETURNS character varying
    LANGUAGE plpgsql
    AS $$
DECLARE
	REG VARCHAR;
	OUTPUT CHARACTER VARYING = '';
BEGIN
	FOR REG IN 
		SELECT DESCRICAO_GHE FROM SEG_ASO_GHE_SETOR WHERE ID_ASO = IDASO
	LOOP
		OUTPUT := OUTPUT || REG || '  ';
	END LOOP;

	RETURN OUTPUT;
END;
$$;


ALTER FUNCTION public.busca_ghe_by_atendimento(idaso bigint) OWNER TO postgres;

--
-- TOC entry 410 (class 1255 OID 19015)
-- Name: busca_ghe_estudo(bigint); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION public.busca_ghe_estudo(id bigint) RETURNS integer
    LANGUAGE plpgsql
    AS $$
DECLARE
	REG VARCHAR;
	NGHE INTEGER := 0;
BEGIN
	FOR REG IN 
		SELECT G.ID_GHE FROM SEG_GHE G JOIN SEG_ESTUDO P ON P.ID_ESTUDO = G.ID_ESTUDO AND G.SITUACAO = 'A'  WHERE G.ID_ESTUDO = ID
	LOOP

		NGHE := NGHE + 1;
		
	END LOOP;

	RETURN NGHE;
END;

$$;


ALTER FUNCTION public.busca_ghe_estudo(id bigint) OWNER TO postgres;

--
-- TOC entry 430 (class 1255 OID 19016)
-- Name: busca_grau_risco(bigint, character, character); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION public.busca_grau_risco(id bigint, tipocliente character, tipograurisco character) RETURNS character varying
    LANGUAGE plpgsql
    AS $$
DECLARE
	REG VARCHAR;
	OUTPUT CHARACTER VARYING = '';
BEGIN
	FOR REG IN 
		SELECT CNAE_SEC.GRAURISCO FROM SEG_CNAE_PPRA CNAE_PPRA_SEC, SEG_CNAE CNAE_SEC WHERE  CNAE_PPRA_SEC.ID_CNAE = CNAE_SEC.ID_CNAE AND CNAE_PPRA_SEC.ID_PPRA = ID AND CNAE_PPRA_SEC.FLAG_CONT = TIPOCLIENTE AND CNAE_PPRA_SEC.FLAG_PR = TIPOGRAURISCO
	LOOP
		OUTPUT := OUTPUT || REG || '               ';
	END LOOP;

	RETURN OUTPUT;
END;
$$;


ALTER FUNCTION public.busca_grau_risco(id bigint, tipocliente character, tipograurisco character) OWNER TO postgres;

--
-- TOC entry 460 (class 1255 OID 55131)
-- Name: busca_outros_exames_by_aso(bigint, integer[]); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION public.busca_outros_exames_by_aso(_idaso bigint, _exames integer[]) RETURNS character varying
    LANGUAGE plpgsql
    AS $$
DECLARE
	REG RECORD;
	OUTPUT CHARACTER VARYING = '';
BEGIN
	FOR REG IN
		SELECT EXAME.DESCRICAO AS EXAME, GHE_FONTE_AGENTE_EXAME_ASO.DATA_EXAME AS DATA_EXAME
		FROM SEG_EXAME EXAME
		LEFT JOIN SEG_GHE_FONTE_AGENTE_EXAME GHE_FONTE_AGENTE_EXAME ON EXAME.ID_EXAME = GHE_FONTE_AGENTE_EXAME.ID_EXAME
		LEFT JOIN SEG_GHE_FONTE_AGENTE_EXAME_ASO GHE_FONTE_AGENTE_EXAME_ASO ON GHE_FONTE_AGENTE_EXAME_ASO.ID_GHE_FONTE_AGENTE_EXAME = GHE_FONTE_AGENTE_EXAME.ID_GHE_FONTE_AGENTE_EXAME
		WHERE 
		GHE_FONTE_AGENTE_EXAME_ASO.ID_ASO = _IDASO
		AND EXAME.ID_EXAME <> ALL (_EXAMES)
		AND GHE_FONTE_AGENTE_EXAME_ASO.CANCELADO = FALSE
		AND GHE_FONTE_AGENTE_EXAME_ASO.DUPLICADO = FALSE
		UNION (
		SELECT EXAME.DESCRICAO AS EXAME, CLIENTE_FUNCAO_EXAME_ASO.DATA_EXAME AS DATA_EXAME
		FROM SEG_EXAME EXAME
		LEFT JOIN SEG_CLIENTE_FUNCAO_EXAME CLIENTE_FUNCAO_EXAME ON CLIENTE_FUNCAO_EXAME.ID_EXAME = EXAME.ID_EXAME
		LEFT JOIN SEG_CLIENTE_FUNCAO_EXAME_ASO CLIENTE_FUNCAO_EXAME_ASO ON CLIENTE_FUNCAO_EXAME_ASO.ID_CLIENTE_FUNCAO_EXAME = CLIENTE_FUNCAO_EXAME.ID_CLIENTE_FUNCAO_EXAME
		WHERE 
		CLIENTE_FUNCAO_EXAME_ASO.ID_ASO = _IDASO
		AND EXAME.ID_EXAME <> ALL (_EXAMES)
		AND CLIENTE_FUNCAO_EXAME_ASO.CANCELADO = FALSE	
		) ORDER BY EXAME ASC
	LOOP
		OUTPUT := OUTPUT || REG.EXAME || ' - ' || TO_CHAR(REG.DATA_EXAME, 'DD/MM/YYYY') || ', ';
	END LOOP;
	IF (CHAR_LENGTH(OUTPUT) > 0 ) THEN
		OUTPUT := SUBSTR(OUTPUT, 1, CHAR_LENGTH(OUTPUT) -2);
	END IF;
	RETURN OUTPUT;
END;
$$;


ALTER FUNCTION public.busca_outros_exames_by_aso(_idaso bigint, _exames integer[]) OWNER TO postgres;

--
-- TOC entry 459 (class 1255 OID 55050)
-- Name: busca_outros_risco_by_aso(bigint, integer[]); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION public.busca_outros_risco_by_aso(idaso bigint, idoutrosriscos integer[]) RETURNS character varying
    LANGUAGE plpgsql
    AS $$
DECLARE
	REG VARCHAR;
	OUTPUT CHARACTER VARYING = '';
	IDRISCO INTEGER = 0;
	I INTEGER = 0;
	RISCOS VARCHAR = '';
	
BEGIN
	
	FOREACH IDRISCO IN ARRAY idOutrosRiscos LOOP
		I := I + 1;
		RISCOS := RISCOS || IDRISCO;
		IF (array_length(idOutrosRiscos, 1) > I) THEN
			RISCOS := RISCOS || ', ';
		END IF;
	END LOOP;
	FOR REG IN 
		SELECT DISTINCT DESCRICAO_AGENTE
		FROM SEG_ASO_AGENTE_RISCO 
		WHERE ID_ASO = IDASO
		AND ID_AGENTE NOT IN (RISCOS)
	LOOP
		OUTPUT := OUTPUT || REG || ', ';
	END LOOP;
	RETURN OUTPUT;
END;
$$;


ALTER FUNCTION public.busca_outros_risco_by_aso(idaso bigint, idoutrosriscos integer[]) OWNER TO postgres;

--
-- TOC entry 457 (class 1255 OID 55124)
-- Name: busca_outros_riscos_by_aso(bigint, integer[]); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION public.busca_outros_riscos_by_aso(_id_aso bigint, _riscos integer[]) RETURNS character varying
    LANGUAGE plpgsql
    AS $$
DECLARE
	REG RECORD;
	OUTPUT CHARACTER VARYING = '';
	
BEGIN
	FOR REG IN 
		SELECT DISTINCT DESCRICAO_AGENTE
		FROM SEG_ASO_AGENTE_RISCO 
		WHERE ID_ASO = _ID_ASO
		AND ID_AGENTE <> ALL (_RISCOS)
	LOOP
		OUTPUT := OUTPUT || REG.DESCRICAO_AGENTE || ', ';
	END LOOP;
	IF (CHAR_LENGTH(OUTPUT) > 0 ) THEN
		OUTPUT := SUBSTR(OUTPUT, 1, CHAR_LENGTH(OUTPUT) -2);
	END IF;
	RETURN OUTPUT;
END;
$$;


ALTER FUNCTION public.busca_outros_riscos_by_aso(_id_aso bigint, _riscos integer[]) OWNER TO postgres;

--
-- TOC entry 458 (class 1255 OID 55130)
-- Name: busca_outros_riscos_by_aso_by_risco(bigint, integer[], bigint); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION public.busca_outros_riscos_by_aso_by_risco(_id_aso bigint, _riscos integer[], _id_risco bigint) RETURNS character varying
    LANGUAGE plpgsql
    AS $$
DECLARE
	REG RECORD;
	OUTPUT CHARACTER VARYING = '';
	
BEGIN
	FOR REG IN 
		SELECT DISTINCT DESCRICAO_AGENTE
		FROM SEG_ASO_AGENTE_RISCO 
		WHERE ID_ASO = _ID_ASO
		AND ID_RISCO = _ID_RISCO
		AND ID_AGENTE <> ALL (_RISCOS)
	LOOP
		OUTPUT := OUTPUT || REG.DESCRICAO_AGENTE || ', ';
	END LOOP;
	IF (CHAR_LENGTH(OUTPUT) > 0 ) THEN
		OUTPUT := SUBSTR(OUTPUT, 1, CHAR_LENGTH(OUTPUT) -2);
	END IF;
	RETURN OUTPUT;
END;
$$;


ALTER FUNCTION public.busca_outros_riscos_by_aso_by_risco(_id_aso bigint, _riscos integer[], _id_risco bigint) OWNER TO postgres;

--
-- TOC entry 431 (class 1255 OID 19017)
-- Name: busca_periodicidade(bigint, bigint); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION public.busca_periodicidade(idghe bigint, idexame bigint) RETURNS character varying
    LANGUAGE plpgsql
    AS $$
DECLARE
	REG VARCHAR;
	OUTPUT CHARACTER VARYING = '';
BEGIN
	FOR REG IN 
		SELECT P.DESCRICAO  FROM SEG_GHE G
		LEFT JOIN SEG_GHE_FONTE GF ON G.ID_GHE = GF.ID_GHE AND GF.PRINCIPAL <> 'T'
		LEFT JOIN SEG_GHE_FONTE_AGENTE GFA ON GFA.ID_GHE_FONTE = GF.ID_GHE_FONTE
		LEFT JOIN SEG_GHE_FONTE_AGENTE_EXAME GFAEX ON GFAEX.ID_GHE_FONTE_AGENTE = GFA.ID_GHE_FONTE_AGENTE
		LEFT JOIN SEG_EXAME E ON E.ID_EXAME = GFAEX.ID_EXAME
		LEFT JOIN SEG_GHE_FONTE_AGENTE_EXAME_PERIODICIDADE GFAEP ON GFAEP.ID_GHE_FONTE_AGENTE_EXAME = GFAEX.ID_GHE_FONTE_AGENTE_EXAME
		LEFT JOIN SEG_PERIODICIDADE P ON P.ID_PERIODICIDADE = GFAEP.ID_PERIODICIDADE
		WHERE G.ID_GHE = IDGHE AND E.ID_EXAME = IDEXAME ORDER BY P.DESCRICAO ASC
	
	LOOP

		OUTPUT := OUTPUT || REG || ' / ';
	END LOOP;

	RETURN OUTPUT;
END;
$$;


ALTER FUNCTION public.busca_periodicidade(idghe bigint, idexame bigint) OWNER TO postgres;

--
-- TOC entry 432 (class 1255 OID 19018)
-- Name: busca_periodicidade_aso(bigint); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION public.busca_periodicidade_aso(idaso bigint) RETURNS character varying
    LANGUAGE plpgsql
    AS $$
DECLARE
	REG VARCHAR;
	OUTPUT CHARACTER VARYING = '';
BEGIN
	FOR REG IN 
		SELECT P.DESCRICAO FROM SEG_PERIODICIDADE P
		JOIN SEG_ASO A ON P.ID_PERIODICIDADE = A.ID_PERIODICIDADE WHERE A.ID_ASO = IDASO
	LOOP

		IF REG = 'BI-ANUAL' OR REG = 'SEMESTRAL' OR REG = 'ANUAL'
		THEN
			REG := 'PERIÓDICO';
		END IF;

		OUTPUT := REG;

	END LOOP;

	RETURN OUTPUT;
END;
$$;


ALTER FUNCTION public.busca_periodicidade_aso(idaso bigint) OWNER TO postgres;

--
-- TOC entry 433 (class 1255 OID 19019)
-- Name: busca_quantidade_exame_por_coluna(bigint, bigint); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION public.busca_quantidade_exame_por_coluna(coluna bigint, id_ppra bigint) RETURNS integer
    LANGUAGE plpgsql
    AS $$
DECLARE
	QUANTIDADE_EXAMES_EXTRAS BIGINT := 0;
	QUANTIDADE_EXAMES_PCMSO BIGINT := 0;
	TOTAL_EXAMES BIGINT := 0;
	DIVISAO_EXAMES_POR_COLUNAS NUMERIC := 0.0;
	QUANTIDADE_COLUNA INTEGER := 3;
	OUTPUT BIGINT := 0;
BEGIN
	SELECT INTO QUANTIDADE_EXAMES_PCMSO COUNT(*) FROM SEG_ASO A
	JOIN SEG_GHE_FONTE_AGENTE_EXAME_ASO GFAEA ON GFAEA.ID_ASO = A.ID_ASO
	JOIN SEG_GHE_FONTE_AGENTE_EXAME GFAE ON GFAE.ID_GHE_FONTE_AGENTE_EXAME = GFAEA.ID_GHE_FONTE_AGENTE_EXAME
	JOIN SEG_EXAME EX ON EX.ID_EXAME = GFAE.ID_EXAME
	WHERE A.ID_ASO = ID_PPRA;

	SELECT INTO QUANTIDADE_EXAMES_EXTRAS COUNT(*) FROM SEG_ASO A
	JOIN SEG_CLIENTE_FUNCAO_EXAME_ASO CFEA ON CFEA.ID_ASO = A.ID_ASO
	JOIN SEG_CLIENTE_FUNCAO_EXAME CFE ON CFE.ID_CLIENTE_FUNCAO_EXAME = CFEA.ID_CLIENTE_FUNCAO_EXAME
	JOIN SEG_EXAME EX ON EX.ID_EXAME = CFE.ID_EXAME
	WHERE A.ID_ASO = ID_PPRA;

	TOTAL_EXAMES := QUANTIDADE_EXAMES_EXTRAS + QUANTIDADE_EXAMES_PCMSO;

	DIVISAO_EXAMES_POR_COLUNAS := TOTAL_EXAMES/3;

	IF (COLUNA = 1) THEN
		IF (TOTAL_EXAMES >= QUANTIDADE_COLUNA) THEN
			IF (TOTAL_EXAMES%3 >= 1) THEN
				OUTPUT := DIVISAO_EXAMES_POR_COLUNAS + 1;
			ELSE
				OUTPUT := DIVISAO_EXAMES_POR_COLUNAS;
			END IF;
		ELSE
			IF (TOTAL_EXAMES%3 >= 1) THEN
				OUTPUT := 1;
			END IF;
		END IF;
	END IF;

	IF (COLUNA = 2) THEN
		IF (TOTAL_EXAMES >= QUANTIDADE_COLUNA) THEN
			IF (TOTAL_EXAMES%3 >= 2) THEN
				OUTPUT := DIVISAO_EXAMES_POR_COLUNAS + 1;
			ELSE
				OUTPUT := DIVISAO_EXAMES_POR_COLUNAS;
			END IF;
		ELSE
			IF (TOTAL_EXAMES%3 >= 2) THEN
				OUTPUT := 1;
			END IF;
		END IF;
	END IF;

	IF (COLUNA = 3) THEN
		IF (TOTAL_EXAMES >= QUANTIDADE_COLUNA) THEN
			IF (TOTAL_EXAMES%3 < 0) THEN
				OUTPUT := DIVISAO_EXAMES_POR_COLUNAS + 1;
			ELSE
				OUTPUT := DIVISAO_EXAMES_POR_COLUNAS;
			END IF;
		ELSE
			IF (TOTAL_EXAMES%3 < 0) THEN
				OUTPUT := 1;
			END IF;
		END IF;
	END IF;

	RETURN OUTPUT;
END;

$$;


ALTER FUNCTION public.busca_quantidade_exame_por_coluna(coluna bigint, id_ppra bigint) OWNER TO postgres;

--
-- TOC entry 455 (class 1255 OID 19020)
-- Name: busca_risco_by_aso(bigint, bigint); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION public.busca_risco_by_aso(idaso bigint, tiporisco bigint) RETURNS character varying
    LANGUAGE plpgsql
    AS $$
DECLARE
	REG RECORD;
	OUTPUT CHARACTER VARYING = '';

BEGIN
	FOR REG IN 
		SELECT DISTINCT DESCRICAO_AGENTE FROM SEG_ASO_AGENTE_RISCO WHERE ID_ASO = IDASO AND ID_RISCO = TIPORISCO AND ID_AGENTE NOT IN (1,2,3,4,5)
	LOOP
		OUTPUT := OUTPUT || REG.DESCRICAO_AGENTE || ', ';

	END LOOP;
	IF (CHAR_LENGTH(OUTPUT) > 0 ) THEN
		OUTPUT := SUBSTR(OUTPUT, 1, CHAR_LENGTH(OUTPUT) -2);
	END IF;
	
	RETURN OUTPUT;
END
$$;


ALTER FUNCTION public.busca_risco_by_aso(idaso bigint, tiporisco bigint) OWNER TO postgres;

--
-- TOC entry 453 (class 1255 OID 21589)
-- Name: busca_risco_fisico_outros_by_aso_superpesa(bigint); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION public.busca_risco_fisico_outros_by_aso_superpesa(idaso bigint) RETURNS character varying
    LANGUAGE plpgsql
    AS $$
DECLARE
	REG VARCHAR;
	OUTPUT CHARACTER VARYING = '';

BEGIN
	FOR REG IN 
		SELECT DISTINCT DESCRICAO_AGENTE FROM SEG_ASO_AGENTE_RISCO WHERE ID_ASO = IDASO AND ID_RISCO = 1 AND ID_AGENTE NOT IN (1,2,3,4,5,6,7,11,8,111)
	LOOP
		OUTPUT := OUTPUT || REG || ', ';

	END LOOP;

	RETURN OUTPUT;
END;
$$;


ALTER FUNCTION public.busca_risco_fisico_outros_by_aso_superpesa(idaso bigint) OWNER TO postgres;

--
-- TOC entry 452 (class 1255 OID 21588)
-- Name: busca_risco_outros_by_aso_superpesa(bigint); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION public.busca_risco_outros_by_aso_superpesa(idaso bigint) RETURNS character varying
    LANGUAGE plpgsql
    AS $$
DECLARE
	REG VARCHAR;
	OUTPUT CHARACTER VARYING = '';

BEGIN
	FOR REG IN 
		SELECT DISTINCT DESCRICAO_AGENTE FROM SEG_ASO_AGENTE_RISCO WHERE ID_ASO = IDASO AND ID_RISCO = 1 AND ID_AGENTE NOT IN (1,2,3,4,5,6,7,11,8,111)
	LOOP
		OUTPUT := OUTPUT || REG || ', ';

	END LOOP;

	RETURN OUTPUT;
END;
$$;


ALTER FUNCTION public.busca_risco_outros_by_aso_superpesa(idaso bigint) OWNER TO postgres;

--
-- TOC entry 463 (class 1255 OID 21590)
-- Name: busca_risco_quimico_outros_by_aso_superpesa(bigint); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION public.busca_risco_quimico_outros_by_aso_superpesa(idaso bigint) RETURNS character varying
    LANGUAGE plpgsql
    AS $$
DECLARE
	REG VARCHAR;
	OUTPUT CHARACTER VARYING = '';

BEGIN
	FOR REG IN 
		SELECT DISTINCT DESCRICAO_AGENTE FROM SEG_ASO_AGENTE_RISCO WHERE ID_ASO = IDASO AND ID_RISCO = 1 AND ID_AGENTE NOT IN (1,2,3,4,5,6,7,11,8,111)
	LOOP
		OUTPUT := OUTPUT || REG || ', ';

	END LOOP;

	RETURN OUTPUT;
END;
$$;


ALTER FUNCTION public.busca_risco_quimico_outros_by_aso_superpesa(idaso bigint) OWNER TO postgres;

--
-- TOC entry 462 (class 1255 OID 19021)
-- Name: busca_setor_by_cliente_funcao(bigint, bigint); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION public.busca_setor_by_cliente_funcao(idclientefuncao bigint, idaso bigint) RETURNS character varying
    LANGUAGE plpgsql
    AS $$
DECLARE
	REG VARCHAR;
	REG_AUX VARCHAR;
	OUTPUT CHARACTER VARYING = '';
BEGIN
	FOR REG IN
		SELECT GHE_SETOR_AVULSO FROM SEG_ASO WHERE ID_ASO = idaso
	LOOP
		OUTPUT := OUTPUT || REG;
	END LOOP;
		
	IF (CHAR_LENGTH(OUTPUT) > 0 ) THEN
		RETURN OUTPUT;
	END IF;
	
	FOR REG IN 
		SELECT DESCRICAO_GHE || ' / ' || DESCRICAO_SETOR FROM SEG_ASO_GHE_SETOR WHERE ID_ASO = idaso
	LOOP
		OUTPUT := REG;
	END LOOP;
	RETURN OUTPUT;
END;
$$;


ALTER FUNCTION public.busca_setor_by_cliente_funcao(idclientefuncao bigint, idaso bigint) OWNER TO postgres;

--
-- TOC entry 434 (class 1255 OID 19022)
-- Name: busca_situacao_atendimento(bigint, character varying); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION public.busca_situacao_atendimento(iditem bigint, tipoitem character varying) RETURNS character varying
    LANGUAGE plpgsql
    AS $$
DECLARE
	REGPCMSO SEG_GHE_FONTE_AGENTE_EXAME_ASO%ROWTYPE;
	REGEXTRA SEG_CLIENTE_FUNCAO_EXAME_ASO%ROWTYPE;
	OUTPUT VARCHAR := 'NÃO IDENTIFICADO';
	QUERY VARCHAR := '';
BEGIN
	IF TIPOITEM = 'PCMSO' THEN
		QUERY := 'SELECT * FROM SEG_GHE_FONTE_AGENTE_EXAME_ASO WHERE ID_GHE_FONTE_AGENTE_EXAME_ASO = ' || IDITEM;
		FOR REGPCMSO IN EXECUTE QUERY LOOP
			IF REGPCMSO.ATENDIDO IS TRUE AND REGPCMSO.FINALIZADO IS TRUE AND REGPCMSO.CANCELADO IS FALSE THEN
				OUTPUT := 'ATENDIDO';
			END IF;
			
			IF REGPCMSO.ATENDIDO IS TRUE AND REGPCMSO.FINALIZADO IS FALSE AND REGPCMSO.CANCELADO IS FALSE THEN
				OUTPUT := 'NÃO ATENDIDO';
			END IF;

			IF REGPCMSO.CANCELADO IS TRUE THEN
				OUTPUT := 'CANCELADO';
			END IF;
		END LOOP;
	END IF;
	IF TIPOITEM = 'EXTRA' THEN
		QUERY := 'SELECT * FROM SEG_CLIENTE_FUNCAO_EXAME_ASO WHERE ID_CLIENTE_FUNCAO_EXAME_ASO = ' || IDITEM;
		FOR REGEXTRA IN EXECUTE QUERY LOOP
			IF REGEXTRA.ATENDIDO IS TRUE AND REGEXTRA.FINALIZADO IS TRUE AND REGEXTRA.CANCELADO IS FALSE THEN
				OUTPUT := 'ATENDIDO';
			END IF;
			
			IF REGEXTRA.ATENDIDO IS TRUE AND REGEXTRA.FINALIZADO IS FALSE AND REGEXTRA.CANCELADO IS FALSE THEN
				OUTPUT := 'NÃO ATENDIDO';
			END IF;

			IF REGEXTRA.CANCELADO IS TRUE THEN
				OUTPUT := 'CANCELADO';
			END IF;
		END LOOP;
	END IF;
	
	RAISE NOTICE 'QUERY(%)', QUERY;
	
	RETURN OUTPUT;
END;
$$;


ALTER FUNCTION public.busca_situacao_atendimento(iditem bigint, tipoitem character varying) OWNER TO postgres;

--
-- TOC entry 435 (class 1255 OID 19023)
-- Name: find_configuracao(bigint); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION public.find_configuracao(idconfiguracao bigint) RETURNS character varying
    LANGUAGE plpgsql
    AS $$
DECLARE
	REG VARCHAR;
	RETORNO VARCHAR = '';
BEGIN
	FOR REG IN 

		SELECT VALOR FROM SEG_CONFIGURACAO WHERE ID_CONFIGURACAO = IDCONFIGURACAO

	LOOP
		RETORNO := REG;

	END LOOP;

	RETURN RETORNO;

END;
$$;


ALTER FUNCTION public.find_configuracao(idconfiguracao bigint) OWNER TO postgres;

--
-- TOC entry 436 (class 1255 OID 19024)
-- Name: find_estudo_by_aso(bigint); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION public.find_estudo_by_aso(idaso bigint) RETURNS bigint
    LANGUAGE plpgsql
    AS $$
DECLARE
	REG BIGINT;
	VERIFICA BIGINT := -1;

BEGIN
	FOR REG IN 
		SELECT DISTINCT E.ID_ESTUDO FROM 
		SEG_GHE_FONTE_AGENTE_EXAME_ASO GFAEA, 
		SEG_GHE_FONTE_AGENTE_EXAME GFAE, 
		SEG_GHE_FONTE_AGENTE GFA, 
		SEG_GHE_FONTE GF, 
		SEG_GHE E
		WHERE GFAEA.ID_GHE_FONTE_AGENTE_EXAME = GFAE.ID_GHE_FONTE_AGENTE_EXAME
		AND GFAE.ID_GHE_FONTE_AGENTE = GFA.ID_GHE_FONTE_AGENTE
		AND GFA.ID_GHE_FONTE = GF.ID_GHE_FONTE
		AND GF.ID_GHE = E.ID_GHE
		AND GFAEA.ID_ASO = IDASO

	LOOP
		VERIFICA := REG;

	END LOOP;

	RETURN VERIFICA;

END;
$$;


ALTER FUNCTION public.find_estudo_by_aso(idaso bigint) OWNER TO postgres;

--
-- TOC entry 437 (class 1255 OID 19025)
-- Name: find_grau_risco_by_estudo(bigint); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION public.find_grau_risco_by_estudo(idestudo bigint) RETURNS character varying
    LANGUAGE plpgsql
    AS $$
DECLARE
	REG VARCHAR;
	RETORNO VARCHAR = '';

BEGIN

	FOR REG IN 

		SELECT GRAU_RISCO FROM SEG_ESTUDO WHERE ID_ESTUDO = IDESTUDO

	LOOP
		RETORNO := REG;

	END LOOP;

	RETURN RETORNO;

END;
$$;


ALTER FUNCTION public.find_grau_risco_by_estudo(idestudo bigint) OWNER TO postgres;

--
-- TOC entry 438 (class 1255 OID 19026)
-- Name: formata_cep(character varying); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION public.formata_cep(cep character varying) RETURNS character varying
    LANGUAGE plpgsql
    AS $$
BEGIN
	IF CEP = ''
	THEN
		RETURN '';
	ELSE
		RETURN SUBSTRING(CEP FROM 1 FOR 5) || '-' ||
		SUBSTRING(CEP FROM 6 FOR 3);
	END IF;
END;
$$;


ALTER FUNCTION public.formata_cep(cep character varying) OWNER TO postgres;

--
-- TOC entry 439 (class 1255 OID 19027)
-- Name: formata_cnae(character varying); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION public.formata_cnae(cod_cnae character varying) RETURNS character varying
    LANGUAGE plpgsql
    AS $$
BEGIN
	RETURN SUBSTRING(COD_CNAE FROM 1 FOR 2) || '.' || SUBSTRING(COD_CNAE FROM 3 FOR 2) || '-' || SUBSTRING(COD_CNAE FROM 5 FOR 1) || '/' || SUBSTRING(COD_CNAE FROM 6 FOR 2);
END;
$$;


ALTER FUNCTION public.formata_cnae(cod_cnae character varying) OWNER TO postgres;

--
-- TOC entry 411 (class 1255 OID 19028)
-- Name: formata_cnpj(character varying); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION public.formata_cnpj(cnpj character varying) RETURNS character varying
    LANGUAGE plpgsql
    AS $$
BEGIN
	IF CNPJ = ''
	THEN
		RETURN '';
	ELSE
		RETURN SUBSTRING(CNPJ FROM 1 FOR 2) || '.' || SUBSTRING(CNPJ FROM 3 FOR 3) || '.' || SUBSTRING(CNPJ FROM 6 FOR 3) || '/' || SUBSTRING(CNPJ FROM 9 FOR 4) || '-' || SUBSTRING(CNPJ FROM 13 FOR 2); 
	END IF;
END;
$$;


ALTER FUNCTION public.formata_cnpj(cnpj character varying) OWNER TO postgres;

--
-- TOC entry 412 (class 1255 OID 19029)
-- Name: formata_cod_estudo(character varying); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION public.formata_cod_estudo(cod_estudo character varying) RETURNS character varying
    LANGUAGE plpgsql
    AS $$
BEGIN
	RETURN SUBSTRING(COD_ESTUDO FROM	1 FOR 4) || '-' || SUBSTRING(COD_ESTUDO FROM 5 FOR 2) || '-' || SUBSTRING(COD_ESTUDO FROM 7 FOR 6) || '/' ||  SUBSTRING(COD_ESTUDO FROM 13 FOR 2); 
END;
$$;


ALTER FUNCTION public.formata_cod_estudo(cod_estudo character varying) OWNER TO postgres;

--
-- TOC entry 414 (class 1255 OID 19030)
-- Name: formata_codigo_atendimento(character varying); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION public.formata_codigo_atendimento(codigo_aso character varying) RETURNS character varying
    LANGUAGE plpgsql
    AS $$
BEGIN
	RETURN SUBSTRING(CODIGO_ASO FROM 1 FOR 4) || '.' || SUBSTRING(CODIGO_ASO FROM 5 FOR 2) || '.' || SUBSTRING(CODIGO_ASO FROM 7 FOR 6); 
END;
$$;


ALTER FUNCTION public.formata_codigo_atendimento(codigo_aso character varying) OWNER TO postgres;

--
-- TOC entry 415 (class 1255 OID 19031)
-- Name: formata_codigo_contrato(character varying); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION public.formata_codigo_contrato(codigo_contrato character varying) RETURNS character varying
    LANGUAGE plpgsql
    AS $$
BEGIN
	RETURN SUBSTRING(CODIGO_CONTRATO FROM	1 FOR 4) || '.' || 	SUBSTRING(CODIGO_CONTRATO FROM 5 FOR 2) || '.' || SUBSTRING(CODIGO_CONTRATO FROM 7 FOR 6) || '/' ||  SUBSTRING(CODIGO_CONTRATO FROM 13 FOR 2); 
END;
$$;


ALTER FUNCTION public.formata_codigo_contrato(codigo_contrato character varying) OWNER TO postgres;

--
-- TOC entry 429 (class 1255 OID 19032)
-- Name: formata_cpf(character varying); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION public.formata_cpf(cpf character varying) RETURNS character varying
    LANGUAGE plpgsql
    AS $$
BEGIN
	IF CPF = ''
	THEN
		RETURN '';
	ELSE
		RETURN SUBSTRING(CPF FROM 1 FOR 3) || '.' || SUBSTRING(CPF FROM 4 FOR 3) || '.' || SUBSTRING(CPF FROM 7 FOR 3) || '-' || SUBSTRING(CPF FROM 10 FOR 2); 
	END IF;
END;
$$;


ALTER FUNCTION public.formata_cpf(cpf character varying) OWNER TO postgres;

--
-- TOC entry 454 (class 1255 OID 46302)
-- Name: formata_pis(character varying); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION public.formata_pis(pis character varying) RETURNS character varying
    LANGUAGE plpgsql
    AS $$
BEGIN
	IF PIS = ''
	THEN
		RETURN '';
	ELSE
		RETURN SUBSTRING(PIS FROM 1 FOR 3) || '.' || SUBSTRING(PIS FROM 4 FOR 5) || '.' || SUBSTRING(PIS FROM 9 FOR 2) || '.' || SUBSTRING(PIS FROM 11 FOR 2); 
	END IF;
END;
$$;


ALTER FUNCTION public.formata_pis(pis character varying) OWNER TO postgres;

--
-- TOC entry 409 (class 1255 OID 19033)
-- Name: formata_vip(boolean); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION public.formata_vip(vip boolean) RETURNS character varying
    LANGUAGE plpgsql
    AS $$
DECLARE
	OUTPUT CHARACTER VARYING = '';

BEGIN
	IF (VIP = 'T') THEN
		RETURN 'V';
	ELSE
		RETURN '';
	END IF;
END;

$$;


ALTER FUNCTION public.formata_vip(vip boolean) OWNER TO postgres;

--
-- TOC entry 440 (class 1255 OID 19034)
-- Name: retorna_indice_mes(character varying); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION public.retorna_indice_mes(mes character varying) RETURNS integer
    LANGUAGE plpgsql
    AS $$
DECLARE	
	MESES VARCHAR[] := ARRAY['JANEIRO','FEVEREIRO', 'MARCO', 'ABRIL','MAIO','JUNHO','JULHO','AGOSTO','SETEMBRO','OUTUBRO','NOVEMBRO','DEZEMBRO', 'TODOS OS MESES'];
	I INTEGER;
BEGIN
	FOR I IN 1..13 LOOP
		IF MESES[I] = MES THEN
			RETURN I;
			EXIT;
		END IF;
	END LOOP;
END;
$$;


ALTER FUNCTION public.retorna_indice_mes(mes character varying) OWNER TO postgres;

--
-- TOC entry 441 (class 1255 OID 19035)
-- Name: verifica_aso_atende_exame(bigint, bigint); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION public.verifica_aso_atende_exame(idaso bigint, idexame bigint) RETURNS character varying
    LANGUAGE plpgsql
    AS $$
DECLARE
	REG VARCHAR;
	OUTPUT CHARACTER VARYING = '';
	VERIFICA VARCHAR := 'FALSE';
BEGIN
	FOR REG IN 
		SELECT E.DESCRICAO FROM 
		SEG_GHE_FONTE_AGENTE_EXAME_ASO GFAEA, 
		SEG_GHE_FONTE_AGENTE_EXAME GFAE,
		SEG_EXAME E
		WHERE 
		GFAEA.ID_GHE_FONTE_AGENTE_EXAME = GFAE.ID_GHE_FONTE_AGENTE_EXAME
		AND E.ID_EXAME = GFAE.ID_EXAME
		AND ID_ASO = IDASO
		AND E.ID_EXAME = IDEXAME
		AND GFAEA.CANCELADO = FALSE
		AND GFAEA.DUPLICADO = FALSE

	LOOP
		VERIFICA := 'TRUE';

	END LOOP;

	FOR REG IN 
		SELECT E.DESCRICAO FROM
		SEG_CLIENTE_FUNCAO_EXAME_ASO CFEA,
		SEG_CLIENTE_FUNCAO_EXAME CFE,
		SEG_EXAME E
		WHERE
		CFEA.ID_CLIENTE_FUNCAO_EXAME = CFE.ID_CLIENTE_FUNCAO_EXAME
		AND CFE.ID_EXAME = E.ID_EXAME
		AND ID_ASO = IDASO
		AND E.ID_EXAME = IDEXAME
		AND CFEA.CANCELADO = FALSE
	LOOP
		VERIFICA := 'TRUE';

	END LOOP;

	RETURN VERIFICA;

END;
$$;


ALTER FUNCTION public.verifica_aso_atende_exame(idaso bigint, idexame bigint) OWNER TO postgres;

--
-- TOC entry 464 (class 1255 OID 327535)
-- Name: verifica_aso_atende_exame_list(bigint, bigint[]); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION public.verifica_aso_atende_exame_list(_idaso bigint, _idexame bigint[]) RETURNS character varying
    LANGUAGE plpgsql
    AS $$
DECLARE
	REG VARCHAR;
	OUTPUT CHARACTER VARYING = '';
	VERIFICA VARCHAR := 'FALSE';
BEGIN
	FOR REG IN 
		SELECT EXAME.DESCRICAO 
		FROM SEG_GHE_FONTE_AGENTE_EXAME_ASO GHE_FONTE_AGENTE_EXAME_ASO
		JOIN SEG_GHE_FONTE_AGENTE_EXAME GHE_FONTE_AGENTE_EXAME
		ON GHE_FONTE_AGENTE_EXAME.ID_GHE_FONTE_AGENTE_EXAME = GHE_FONTE_AGENTE_EXAME_ASO.ID_GHE_FONTE_AGENTE_EXAME
		JOIN SEG_EXAME EXAME
		ON EXAME.ID_EXAME = GHE_FONTE_AGENTE_EXAME.ID_EXAME
		WHERE TRUE 
		AND GHE_FONTE_AGENTE_EXAME_ASO.ID_ASO = _IDASO
		AND EXAME.ID_EXAME = any ( _IDEXAME)
		AND GHE_FONTE_AGENTE_EXAME_ASO.CANCELADO = FALSE
		AND GHE_FONTE_AGENTE_EXAME_ASO.DUPLICADO = FALSE

	LOOP
		VERIFICA := 'TRUE';

	END LOOP;

	FOR REG IN 
		SELECT EXAME.DESCRICAO 
		FROM SEG_CLIENTE_FUNCAO_EXAME_ASO CLIENTE_FUNCAO_EXAME_ASO
		JOIN SEG_CLIENTE_FUNCAO_EXAME CLIENTE_FUNCAO_EXAME
		ON CLIENTE_FUNCAO_EXAME.ID_CLIENTE_FUNCAO_EXAME = CLIENTE_FUNCAO_EXAME_ASO.ID_CLIENTE_FUNCAO_EXAME
		JOIN SEG_EXAME EXAME
		ON EXAME.ID_EXAME = CLIENTE_FUNCAO_EXAME.ID_EXAME
		WHERE TRUE
		AND CLIENTE_FUNCAO_EXAME_ASO.ID_ASO = _IDASO
		AND EXAME.ID_EXAME = any (_IDEXAME)
		AND CLIENTE_FUNCAO_EXAME_ASO.CANCELADO = FALSE
	LOOP
		VERIFICA := 'TRUE';

	END LOOP;

	RETURN VERIFICA;

END;
$$;


ALTER FUNCTION public.verifica_aso_atende_exame_list(_idaso bigint, _idexame bigint[]) OWNER TO postgres;

--
-- TOC entry 450 (class 1255 OID 21586)
-- Name: verifica_cidade_unidade(bigint); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION public.verifica_cidade_unidade(idaso bigint) RETURNS character varying
    LANGUAGE plpgsql
    AS $$
DECLARE
	REG VARCHAR;
	OUTPUT CHARACTER VARYING = '';
BEGIN
	FOR REG IN 

		SELECT CIDADE.NOME FROM SEG_ASO ATENDIMENTO 
		JOIN SEG_ESTUDO ESTUDO ON ESTUDO.ID_ESTUDO = ATENDIMENTO.ID_ESTUDO 
		JOIN SEG_CLIENTE CLIENTE ON CLIENTE.ID_CLIENTE = ESTUDO.ID_CLIENTE_CONTRATADO AND CLIENTE.ID_CLIENTE_MATRIZ IS NOT NULL
		JOIN SEG_REP_CLI REPRESENTANTE_CLIENTE ON REPRESENTANTE_CLIENTE.ID_SEG_REP_CLI = ESTUDO.ID_SEG_REP_CLI AND CLIENTE.ID_CLIENTE_MATRIZ = REPRESENTANTE_CLIENTE.ID_CLIENTE
		LEFT JOIN SEG_CIDADE_IBGE CIDADE ON CIDADE.ID_CIDADE_IBGE = CLIENTE.ID_CIDADE_IBGE
		WHERE ID_ASO = IDASO

	LOOP
		OUTPUT := REG;

	END LOOP;

	RETURN OUTPUT;

END;
$$;


ALTER FUNCTION public.verifica_cidade_unidade(idaso bigint) OWNER TO postgres;

--
-- TOC entry 442 (class 1255 OID 19036)
-- Name: verifica_cliente_credenciavel(bigint); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION public.verifica_cliente_credenciavel(idcliente bigint) RETURNS boolean
    LANGUAGE plpgsql
    AS $$
DECLARE
	REG VARCHAR;
	VERIFICA BOOLEAN := FALSE;

BEGIN
	
	FOR REG IN 

		SELECT * FROM SEG_CLIENTE WHERE ID_CLIENTE_CREDENCIADO = IDCLIENTE

	LOOP
		VERIFICA := TRUE;

	END LOOP;

	RETURN VERIFICA;

END;
$$;


ALTER FUNCTION public.verifica_cliente_credenciavel(idcliente bigint) OWNER TO postgres;

--
-- TOC entry 443 (class 1255 OID 19037)
-- Name: verifica_cliente_matriz(bigint); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION public.verifica_cliente_matriz(idcliente bigint) RETURNS character varying
    LANGUAGE plpgsql
    AS $$
DECLARE
	REG VARCHAR;
	OUTPUT CHARACTER VARYING = '';
	VERIFICA VARCHAR := 'FALSE';

BEGIN
	FOR REG IN 

		SELECT * FROM SEG_CLIENTE WHERE ID_CLIENTE_MATRIZ = IDCLIENTE

	LOOP
		VERIFICA := 'TRUE';

	END LOOP;

	RETURN VERIFICA;

END;
$$;


ALTER FUNCTION public.verifica_cliente_matriz(idcliente bigint) OWNER TO postgres;

--
-- TOC entry 444 (class 1255 OID 19038)
-- Name: verifica_existe_risco(bigint, bigint); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION public.verifica_existe_risco(idaso bigint, idrisco bigint) RETURNS character varying
    LANGUAGE plpgsql
    AS $$
DECLARE
	REG VARCHAR;
	OUTPUT CHARACTER VARYING = '';
	VERIFICA VARCHAR := 'FALSE';
BEGIN
	FOR REG IN 

		SELECT * FROM SEG_ASO_AGENTE_RISCO WHERE ID_ASO = IDASO AND ID_RISCO = IDRISCO

	LOOP
		VERIFICA := 'TRUE';

	END LOOP;

	RETURN VERIFICA;

END;
$$;


ALTER FUNCTION public.verifica_existe_risco(idaso bigint, idrisco bigint) OWNER TO postgres;

--
-- TOC entry 445 (class 1255 OID 19039)
-- Name: verifica_existe_risco_retirando_agente_nao_identificado(bigint, bigint); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION public.verifica_existe_risco_retirando_agente_nao_identificado(idaso bigint, idrisco bigint) RETURNS character varying
    LANGUAGE plpgsql
    AS $$
DECLARE
	REG VARCHAR;
	OUTPUT CHARACTER VARYING = '';
	VERIFICA VARCHAR := 'FALSE';

BEGIN
	FOR REG IN 

		SELECT * FROM SEG_ASO_AGENTE_RISCO WHERE ID_ASO = IDASO AND ID_RISCO = IDRISCO
	LOOP
		VERIFICA := 'TRUE';

	END LOOP;

	RETURN VERIFICA;

END;
$$;


ALTER FUNCTION public.verifica_existe_risco_retirando_agente_nao_identificado(idaso bigint, idrisco bigint) OWNER TO postgres;

--
-- TOC entry 446 (class 1255 OID 19040)
-- Name: verifica_existe_unidade(bigint); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION public.verifica_existe_unidade(idaso bigint) RETURNS character varying
    LANGUAGE plpgsql
    AS $$
DECLARE
	REG VARCHAR;
	OUTPUT CHARACTER VARYING = '';
BEGIN
	FOR REG IN 

		SELECT C.RAZAO_SOCIAL FROM SEG_ASO A 
		JOIN SEG_ESTUDO E ON E.ID_ESTUDO = A.ID_ESTUDO 
		JOIN SEG_CLIENTE C ON C.ID_CLIENTE = E.ID_CLIENTE_CONTRATADO AND C.ID_CLIENTE_MATRIZ IS NOT NULL
		JOIN SEG_REP_CLI RC ON RC.ID_SEG_REP_CLI = E.ID_SEG_REP_CLI
		AND C.ID_CLIENTE_MATRIZ = RC.ID_CLIENTE
		WHERE ID_ASO = IDASO

	LOOP
		OUTPUT := REG;

	END LOOP;

	RETURN OUTPUT;

END;
$$;


ALTER FUNCTION public.verifica_existe_unidade(idaso bigint) OWNER TO postgres;

--
-- TOC entry 447 (class 1255 OID 19041)
-- Name: verifica_risco_atendimento(bigint, bigint); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION public.verifica_risco_atendimento(idatendimento bigint, idrisco bigint) RETURNS boolean
    LANGUAGE plpgsql
    AS $$
DECLARE
	REG VARCHAR;
	VERIFICA BOOLEAN := FALSE;

BEGIN
	
	FOR REG IN 

		SELECT * FROM SEG_ASO_AGENTE_RISCO WHERE ID_ASO = IDATENDIMENTO AND ID_AGENTE = IDRISCO

	LOOP
		VERIFICA := TRUE;

	END LOOP;

	RETURN VERIFICA;
END;
$$;


ALTER FUNCTION public.verifica_risco_atendimento(idatendimento bigint, idrisco bigint) OWNER TO postgres;

--
-- TOC entry 448 (class 1255 OID 19042)
-- Name: verifica_situacao_altura(bigint); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION public.verifica_situacao_altura(idaso bigint) RETURNS character varying
    LANGUAGE plpgsql
    AS $$
DECLARE
	REG VARCHAR;
	OUTPUT CHARACTER VARYING = '';
	VERIFICA VARCHAR := 'FALSE';

BEGIN
	FOR REG IN 
		SELECT ALTURA FROM SEG_GHE_FONTE_AGENTE_EXAME_ASO GFAEA
		WHERE GFAEA.ID_ASO = IDASO
		
	LOOP
		IF REG = 'T' THEN 
		VERIFICA := 'TRUE' ;
		END IF;

	END LOOP;
	
	RETURN VERIFICA;
END;
$$;


ALTER FUNCTION public.verifica_situacao_altura(idaso bigint) OWNER TO postgres;

--
-- TOC entry 449 (class 1255 OID 19043)
-- Name: verifica_situacao_confinado(bigint); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION public.verifica_situacao_confinado(idaso bigint) RETURNS character varying
    LANGUAGE plpgsql
    AS $$
DECLARE
	REG VARCHAR;
	OUTPUT CHARACTER VARYING = '';
	VERIFICA VARCHAR := 'FALSE';

BEGIN
	FOR REG IN 
		SELECT CONFINADO FROM SEG_GHE_FONTE_AGENTE_EXAME_ASO GFAEA
		WHERE GFAEA.ID_ASO = IDASO
		
	LOOP
		IF REG = 'T' THEN 
		VERIFICA := 'TRUE' ;
		END IF;

	END LOOP;
	
	RETURN VERIFICA;
END;
$$;


ALTER FUNCTION public.verifica_situacao_confinado(idaso bigint) OWNER TO postgres;

--
-- TOC entry 451 (class 1255 OID 21587)
-- Name: verifica_uf_unidade(bigint); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION public.verifica_uf_unidade(idaso bigint) RETURNS character varying
    LANGUAGE plpgsql
    AS $$
DECLARE
	REG VARCHAR;
	OUTPUT CHARACTER VARYING = '';
BEGIN
	FOR REG IN 

		SELECT CLIENTE.UF FROM SEG_ASO ATENDIMENTO 
		JOIN SEG_ESTUDO ESTUDO ON ESTUDO.ID_ESTUDO = ATENDIMENTO.ID_ESTUDO 
		JOIN SEG_CLIENTE CLIENTE ON CLIENTE.ID_CLIENTE = ESTUDO.ID_CLIENTE_CONTRATADO AND CLIENTE.ID_CLIENTE_MATRIZ IS NOT NULL
		JOIN SEG_REP_CLI REPRESENTANTE_CLIENTE ON REPRESENTANTE_CLIENTE.ID_SEG_REP_CLI = ESTUDO.ID_SEG_REP_CLI AND CLIENTE.ID_CLIENTE_MATRIZ = REPRESENTANTE_CLIENTE.ID_CLIENTE
		WHERE ID_ASO = IDASO

	LOOP
		OUTPUT := REG;

	END LOOP;

	RETURN OUTPUT;

END;
$$;


ALTER FUNCTION public.verifica_uf_unidade(idaso bigint) OWNER TO postgres;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- TOC entry 186 (class 1259 OID 19044)
-- Name: seg_acao; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.seg_acao (
    id_acao bigint NOT NULL,
    descricao character varying(50) NOT NULL,
    situacao character varying(1) NOT NULL
);


ALTER TABLE public.seg_acao OWNER TO postgres;

--
-- TOC entry 187 (class 1259 OID 19047)
-- Name: seg_acao_id_acao_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.seg_acao_id_acao_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.seg_acao_id_acao_seq OWNER TO postgres;

--
-- TOC entry 3969 (class 0 OID 0)
-- Dependencies: 187
-- Name: seg_acao_id_acao_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.seg_acao_id_acao_seq OWNED BY public.seg_acao.id_acao;


--
-- TOC entry 188 (class 1259 OID 19049)
-- Name: seg_acesso; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.seg_acesso (
    id_acesso bigint NOT NULL,
    login character varying NOT NULL,
    ip character varying NOT NULL,
    host_name character varying NOT NULL,
    ultimo_acesso timestamp without time zone NOT NULL
);


ALTER TABLE public.seg_acesso OWNER TO postgres;

--
-- TOC entry 189 (class 1259 OID 19055)
-- Name: seg_acesso_id_acesso_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.seg_acesso_id_acesso_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.seg_acesso_id_acesso_seq OWNER TO postgres;

--
-- TOC entry 3970 (class 0 OID 0)
-- Dependencies: 189
-- Name: seg_acesso_id_acesso_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.seg_acesso_id_acesso_seq OWNED BY public.seg_acesso.id_acesso;


--
-- TOC entry 190 (class 1259 OID 19057)
-- Name: seg_acompanhamento_atendimento; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.seg_acompanhamento_atendimento (
    id_acompanhamento_atendimento bigint NOT NULL,
    nome_funcionario character varying(100) NOT NULL,
    sala character varying(20) NOT NULL,
    andar integer NOT NULL,
    cliente_razao_social character varying(100) NOT NULL,
    rg character varying(15) NOT NULL,
    data_inclusao timestamp without time zone NOT NULL,
    senha_atendimento character varying(6) NOT NULL,
    flag_atendido boolean DEFAULT false NOT NULL,
    cpf_funcionario character varying(11),
    data_atendido timestamp without time zone,
    id_aso bigint NOT NULL,
    codigo_aso character varying(12) NOT NULL,
    exame character varying(100) NOT NULL,
    id_empresa bigint,
    nome_empresa character varying(100)
);


ALTER TABLE public.seg_acompanhamento_atendimento OWNER TO postgres;

--
-- TOC entry 191 (class 1259 OID 19060)
-- Name: seg_acompanhamento_atendiment_id_acompanhamento_atendimento_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.seg_acompanhamento_atendiment_id_acompanhamento_atendimento_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.seg_acompanhamento_atendiment_id_acompanhamento_atendimento_seq OWNER TO postgres;

--
-- TOC entry 3971 (class 0 OID 0)
-- Dependencies: 191
-- Name: seg_acompanhamento_atendiment_id_acompanhamento_atendimento_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.seg_acompanhamento_atendiment_id_acompanhamento_atendimento_seq OWNED BY public.seg_acompanhamento_atendimento.id_acompanhamento_atendimento;


--
-- TOC entry 192 (class 1259 OID 19062)
-- Name: seg_agente; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.seg_agente (
    id_agente bigint NOT NULL,
    id_risco bigint NOT NULL,
    descricao text NOT NULL,
    trajetoria character varying(255),
    danos character varying(255),
    limite character varying(100),
    situacao character varying(1) NOT NULL,
    privado boolean NOT NULL,
    intensidade character varying(100),
    tecnica character varying(100),
    codigo_esocial character varying(10)
);


ALTER TABLE public.seg_agente OWNER TO postgres;

--
-- TOC entry 193 (class 1259 OID 19068)
-- Name: seg_agente_id_agente_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.seg_agente_id_agente_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.seg_agente_id_agente_seq OWNER TO postgres;

--
-- TOC entry 3972 (class 0 OID 0)
-- Dependencies: 193
-- Name: seg_agente_id_agente_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.seg_agente_id_agente_seq OWNED BY public.seg_agente.id_agente;


--
-- TOC entry 194 (class 1259 OID 19075)
-- Name: seg_arquivo; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.seg_arquivo (
    id_arquivo bigint NOT NULL,
    descricao character varying NOT NULL,
    mimetype character varying NOT NULL,
    size integer NOT NULL,
    conteudo bytea NOT NULL
);


ALTER TABLE public.seg_arquivo OWNER TO postgres;

--
-- TOC entry 195 (class 1259 OID 19081)
-- Name: seg_arquivo_anexo; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.seg_arquivo_anexo (
    id_arquivo bigint NOT NULL,
    id_aso bigint NOT NULL,
    descricao character varying NOT NULL,
    mimetype character varying NOT NULL,
    size integer NOT NULL,
    conteudo bytea NOT NULL
);


ALTER TABLE public.seg_arquivo_anexo OWNER TO postgres;

--
-- TOC entry 196 (class 1259 OID 19087)
-- Name: seg_arquivo_anexo_id_arquivo_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.seg_arquivo_anexo_id_arquivo_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.seg_arquivo_anexo_id_arquivo_seq OWNER TO postgres;

--
-- TOC entry 3973 (class 0 OID 0)
-- Dependencies: 196
-- Name: seg_arquivo_anexo_id_arquivo_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.seg_arquivo_anexo_id_arquivo_seq OWNED BY public.seg_arquivo_anexo.id_arquivo;


--
-- TOC entry 197 (class 1259 OID 19089)
-- Name: seg_arquivo_id_arquivo_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.seg_arquivo_id_arquivo_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.seg_arquivo_id_arquivo_seq OWNER TO postgres;

--
-- TOC entry 3974 (class 0 OID 0)
-- Dependencies: 197
-- Name: seg_arquivo_id_arquivo_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.seg_arquivo_id_arquivo_seq OWNED BY public.seg_arquivo.id_arquivo;


--
-- TOC entry 198 (class 1259 OID 19091)
-- Name: seg_aso; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.seg_aso (
    id_aso bigint NOT NULL,
    codigo_aso character varying(12) NOT NULL,
    id_medico_examinador bigint,
    id_cliente_funcao_funcionario bigint NOT NULL,
    data_aso timestamp without time zone NOT NULL,
    nome_funcionario character varying(100),
    rg_funcionario character varying(15),
    datanasc_funcionario date,
    cpf_funcionario character varying(11),
    tiposan_funcionario character varying(2),
    ftrh_funcionario character varying(1),
    matricula_funcionario character varying(30),
    razao_empresa character varying(100),
    cnpj_empresa character varying(14),
    endereco_empresa character varying(100),
    numero_empresa character varying(10),
    complemento_empresa character varying(100),
    bairro_empresa character varying(50),
    cep_empresa character varying(8),
    cidade_empresa character varying(50),
    uf_empresa character varying(2),
    id_periodicidade bigint NOT NULL,
    situacao character varying(1) NOT NULL,
    id_medico_coordenador bigint,
    crm_coordenador character varying(15),
    telefone1_coordenador character varying(15) NOT NULL,
    telefone2_coordenador character varying(15) NOT NULL,
    nome_coordenador character varying(100),
    crm_examinador character varying(15),
    nome_examinador character varying(100),
    telefone1_examinador character varying(15),
    telefone2_examinador character varying(15),
    id_usuario_criador bigint NOT NULL,
    id_usuario_finalizador bigint,
    id_estudo bigint,
    obs_alteracao text,
    obs_aso text,
    conclusao character varying(1),
    data_gravacao timestamp without time zone NOT NULL,
    data_cancelamento timestamp without time zone,
    id_usuario_cancelamento bigint,
    id_ult_usuario_alteracao bigint,
    data_finalizacao timestamp without time zone,
    senha character varying(6),
    id_protocolo bigint,
    obs_cancelamento text,
    id_empresa bigint NOT NULL,
    id_centrocusto bigint,
    data_vencimento date,
    numero_po character varying(15),
    prioridade boolean DEFAULT false NOT NULL,
    pis character varying(11),
    ghe_setor_avulso character varying(255),
    funcao_funcionario character varying(100),
    confinado boolean DEFAULT false NOT NULL,
    altura boolean DEFAULT false NOT NULL,
    eletricidade boolean DEFAULT false NOT NULL
);


ALTER TABLE public.seg_aso OWNER TO postgres;

--
-- TOC entry 199 (class 1259 OID 19097)
-- Name: seg_aso_agente_risco; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.seg_aso_agente_risco (
    id_aso bigint NOT NULL,
    id_agente bigint NOT NULL,
    descricao_agente character varying,
    id_risco bigint,
    descricao_risco character varying
);


ALTER TABLE public.seg_aso_agente_risco OWNER TO postgres;

--
-- TOC entry 200 (class 1259 OID 19103)
-- Name: seg_aso_ghe_setor; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.seg_aso_ghe_setor (
    id_aso bigint NOT NULL,
    id_ghe bigint NOT NULL,
    descricao_ghe character varying,
    id_setor bigint,
    descricao_setor character varying,
    numero_po character varying(15)
);


ALTER TABLE public.seg_aso_ghe_setor OWNER TO postgres;

--
-- TOC entry 201 (class 1259 OID 19109)
-- Name: seg_aso_id_aso_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.seg_aso_id_aso_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.seg_aso_id_aso_seq OWNER TO postgres;

--
-- TOC entry 3975 (class 0 OID 0)
-- Dependencies: 201
-- Name: seg_aso_id_aso_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.seg_aso_id_aso_seq OWNED BY public.seg_aso.id_aso;


--
-- TOC entry 202 (class 1259 OID 19111)
-- Name: seg_atividade; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.seg_atividade (
    id_atividade bigint NOT NULL,
    descricao character varying(200) NOT NULL,
    situacao character varying(1) NOT NULL
);


ALTER TABLE public.seg_atividade OWNER TO postgres;

--
-- TOC entry 203 (class 1259 OID 19114)
-- Name: seg_atividade_cronograma; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.seg_atividade_cronograma (
    id_atividade bigint NOT NULL,
    id_cronograma bigint NOT NULL,
    mes_realizado character varying(15) NOT NULL,
    ano_realizado character varying(4) NOT NULL,
    publico character varying(250) NOT NULL,
    evidencia character varying(250) NOT NULL
);


ALTER TABLE public.seg_atividade_cronograma OWNER TO postgres;

--
-- TOC entry 204 (class 1259 OID 19120)
-- Name: seg_atividade_id_atividade_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.seg_atividade_id_atividade_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.seg_atividade_id_atividade_seq OWNER TO postgres;

--
-- TOC entry 3976 (class 0 OID 0)
-- Dependencies: 204
-- Name: seg_atividade_id_atividade_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.seg_atividade_id_atividade_seq OWNED BY public.seg_atividade.id_atividade;


--
-- TOC entry 205 (class 1259 OID 19122)
-- Name: seg_banco; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.seg_banco (
    id_banco bigint NOT NULL,
    nome character varying(100) NOT NULL,
    codigo_banco character varying(3),
    caixa boolean NOT NULL,
    situacao character varying(1) NOT NULL,
    privado boolean NOT NULL,
    boleto boolean NOT NULL
);


ALTER TABLE public.seg_banco OWNER TO postgres;

--
-- TOC entry 206 (class 1259 OID 19125)
-- Name: seg_banco_id_banco_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.seg_banco_id_banco_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.seg_banco_id_banco_seq OWNER TO postgres;

--
-- TOC entry 3977 (class 0 OID 0)
-- Dependencies: 206
-- Name: seg_banco_id_banco_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.seg_banco_id_banco_seq OWNED BY public.seg_banco.id_banco;


--
-- TOC entry 207 (class 1259 OID 19135)
-- Name: seg_centrocusto; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.seg_centrocusto (
    id bigint NOT NULL,
    id_cliente bigint NOT NULL,
    descricao character varying(50) NOT NULL,
    situacao character varying(1) DEFAULT 'A'::character varying NOT NULL
);


ALTER TABLE public.seg_centrocusto OWNER TO postgres;

--
-- TOC entry 208 (class 1259 OID 19139)
-- Name: seg_centrocusto_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.seg_centrocusto_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.seg_centrocusto_id_seq OWNER TO postgres;

--
-- TOC entry 3978 (class 0 OID 0)
-- Dependencies: 208
-- Name: seg_centrocusto_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.seg_centrocusto_id_seq OWNED BY public.seg_centrocusto.id;


--
-- TOC entry 209 (class 1259 OID 19141)
-- Name: seg_cidade_ibge; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.seg_cidade_ibge (
    id_cidade_ibge bigint NOT NULL,
    codigo character varying(6) NOT NULL,
    nome character varying(100) NOT NULL,
    uf_cidade character varying(2) NOT NULL
);


ALTER TABLE public.seg_cidade_ibge OWNER TO postgres;

--
-- TOC entry 210 (class 1259 OID 19144)
-- Name: seg_cidade_ibge_id_cidade_ibge_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.seg_cidade_ibge_id_cidade_ibge_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.seg_cidade_ibge_id_cidade_ibge_seq OWNER TO postgres;

--
-- TOC entry 3979 (class 0 OID 0)
-- Dependencies: 210
-- Name: seg_cidade_ibge_id_cidade_ibge_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.seg_cidade_ibge_id_cidade_ibge_seq OWNED BY public.seg_cidade_ibge.id_cidade_ibge;


--
-- TOC entry 211 (class 1259 OID 19146)
-- Name: seg_cli_cnae; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.seg_cli_cnae (
    id_cliente bigint NOT NULL,
    id_cnae bigint NOT NULL,
    flag_pr boolean NOT NULL
);


ALTER TABLE public.seg_cli_cnae OWNER TO postgres;

--
-- TOC entry 212 (class 1259 OID 19149)
-- Name: seg_cliente; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.seg_cliente (
    id_cliente bigint NOT NULL,
    id_ramo bigint,
    razao_social character varying(100) NOT NULL,
    fantasia character varying(100),
    cnpj character varying(14) NOT NULL,
    inscricao character varying(20) NOT NULL,
    endereco character varying(100),
    numero character varying(10),
    complemento character varying(100),
    bairro character varying(50),
    cep character varying(8),
    uf character varying(2),
    telefone1 character varying(15),
    telefone2 character varying(15),
    email character varying(50),
    site character varying(50),
    data_cadastro date NOT NULL,
    responsavel character varying(100),
    cargo character varying(50),
    documento character varying(50),
    escopo character varying(600),
    jornada character varying(300),
    situacao character varying(1),
    est_masc integer,
    est_fem integer,
    enderecocob character varying(100),
    numerocob character varying(10),
    complementocob character varying(100),
    bairrocob character varying(50),
    cepcob character varying(8),
    ufcob character varying(2),
    telefonecob character varying(15),
    id_medico bigint,
    telefone2cob character varying(15),
    vip boolean DEFAULT false NOT NULL,
    telefone_contato character varying(15),
    usa_contrato boolean DEFAULT false NOT NULL,
    id_cliente_matriz bigint,
    id_cliente_credenciado bigint,
    particular boolean DEFAULT false NOT NULL,
    unidade boolean DEFAULT false NOT NULL,
    credenciada boolean DEFAULT false NOT NULL,
    prestador boolean DEFAULT false NOT NULL,
    id_cidade_ibge bigint NOT NULL,
    id_cidade_ibge_cobranca bigint,
    destaca_iss boolean DEFAULT true NOT NULL,
    gera_cobranca_valor_liquido boolean DEFAULT false NOT NULL,
    aliquota_iss numeric(8,2) DEFAULT 0.00 NOT NULL,
    codigo_cnes character varying(7),
    usa_centro_custo boolean DEFAULT false NOT NULL,
    bloqueado boolean DEFAULT false NOT NULL,
    credenciadora boolean DEFAULT false NOT NULL,
    fisica boolean DEFAULT false NOT NULL,
    usa_po boolean DEFAULT false NOT NULL,
    simples_nacional boolean DEFAULT false NOT NULL
);


ALTER TABLE public.seg_cliente OWNER TO postgres;

--
-- TOC entry 213 (class 1259 OID 19167)
-- Name: seg_cliente_arquivo; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.seg_cliente_arquivo (
    id_cliente_arquivo bigint NOT NULL,
    id_arquivo bigint NOT NULL,
    id_cliente bigint NOT NULL,
    data_criacao timestamp without time zone NOT NULL,
    data_finalizacao timestamp without time zone,
    situacao character varying NOT NULL
);


ALTER TABLE public.seg_cliente_arquivo OWNER TO postgres;

--
-- TOC entry 214 (class 1259 OID 19173)
-- Name: seg_cliente_arquivo_id_cliente_arquivo_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.seg_cliente_arquivo_id_cliente_arquivo_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.seg_cliente_arquivo_id_cliente_arquivo_seq OWNER TO postgres;

--
-- TOC entry 3980 (class 0 OID 0)
-- Dependencies: 214
-- Name: seg_cliente_arquivo_id_cliente_arquivo_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.seg_cliente_arquivo_id_cliente_arquivo_seq OWNED BY public.seg_cliente_arquivo.id_cliente_arquivo;


--
-- TOC entry 215 (class 1259 OID 19175)
-- Name: seg_cliente_funcao; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.seg_cliente_funcao (
    id_seg_cliente_funcao bigint NOT NULL,
    id_cliente bigint NOT NULL,
    id_funcao bigint NOT NULL,
    situacao character varying(1) NOT NULL,
    data_cadastro date NOT NULL,
    data_desligamento date
);


ALTER TABLE public.seg_cliente_funcao OWNER TO postgres;

--
-- TOC entry 216 (class 1259 OID 19178)
-- Name: seg_cliente_funcao_exame; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.seg_cliente_funcao_exame (
    id_cliente_funcao_exame bigint NOT NULL,
    id_cliente_funcao bigint NOT NULL,
    id_exame bigint NOT NULL,
    situacao character varying(1) NOT NULL,
    idade_exame integer NOT NULL
);


ALTER TABLE public.seg_cliente_funcao_exame OWNER TO postgres;

--
-- TOC entry 217 (class 1259 OID 19181)
-- Name: seg_cliente_funcao_exame_aso; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.seg_cliente_funcao_exame_aso (
    id_cliente_funcao_exame_aso bigint NOT NULL,
    id_sala_exame bigint,
    id_aso bigint NOT NULL,
    id_cliente_funcao_exame bigint NOT NULL,
    data_exame date NOT NULL,
    atendido boolean DEFAULT false,
    finalizado boolean DEFAULT false,
    login character varying(15),
    devolvido boolean DEFAULT false,
    data_atendido timestamp without time zone,
    data_devolvido timestamp without time zone,
    data_finalizado timestamp without time zone,
    id_usuario bigint,
    cancelado boolean DEFAULT false,
    data_cancelado timestamp without time zone,
    resultado character varying(1),
    observacao_resultado text,
    transcrito boolean DEFAULT false NOT NULL,
    id_prestador bigint,
    id_protocolo bigint,
    id_medico bigint,
    entrega boolean DEFAULT false NOT NULL,
    data_validade date,
    imprime_aso boolean DEFAULT true NOT NULL
);


ALTER TABLE public.seg_cliente_funcao_exame_aso OWNER TO postgres;

--
-- TOC entry 218 (class 1259 OID 19193)
-- Name: seg_cliente_funcao_exame_aso_id_cliente_funcao_exame_aso_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.seg_cliente_funcao_exame_aso_id_cliente_funcao_exame_aso_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.seg_cliente_funcao_exame_aso_id_cliente_funcao_exame_aso_seq OWNER TO postgres;

--
-- TOC entry 3981 (class 0 OID 0)
-- Dependencies: 218
-- Name: seg_cliente_funcao_exame_aso_id_cliente_funcao_exame_aso_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.seg_cliente_funcao_exame_aso_id_cliente_funcao_exame_aso_seq OWNED BY public.seg_cliente_funcao_exame_aso.id_cliente_funcao_exame_aso;


--
-- TOC entry 219 (class 1259 OID 19195)
-- Name: seg_cliente_funcao_exame_id_cliente_funcao_exame_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.seg_cliente_funcao_exame_id_cliente_funcao_exame_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.seg_cliente_funcao_exame_id_cliente_funcao_exame_seq OWNER TO postgres;

--
-- TOC entry 3982 (class 0 OID 0)
-- Dependencies: 219
-- Name: seg_cliente_funcao_exame_id_cliente_funcao_exame_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.seg_cliente_funcao_exame_id_cliente_funcao_exame_seq OWNED BY public.seg_cliente_funcao_exame.id_cliente_funcao_exame;


--
-- TOC entry 220 (class 1259 OID 19197)
-- Name: seg_cliente_funcao_exame_periodicidade; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.seg_cliente_funcao_exame_periodicidade (
    id_periodicidade bigint NOT NULL,
    id_cliente_funcao_exame bigint NOT NULL,
    situacao character varying(1) NOT NULL,
    periodo_vencimento integer
);


ALTER TABLE public.seg_cliente_funcao_exame_periodicidade OWNER TO postgres;

--
-- TOC entry 221 (class 1259 OID 19200)
-- Name: seg_cliente_funcao_funcionario; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.seg_cliente_funcao_funcionario (
    id_cliente_funcao_funcionario bigint NOT NULL,
    id_seg_cliente_funcao bigint NOT NULL,
    id_funcionario bigint NOT NULL,
    situacao character varying(1) NOT NULL,
    data_cadastro date NOT NULL,
    data_desligamento date,
    matricula character varying(30),
    vip boolean DEFAULT false NOT NULL,
    br_pdh character varying(3),
    regime_revezamento character varying(15),
    estagiario boolean DEFAULT false NOT NULL,
    vinculo boolean DEFAULT true NOT NULL
);


ALTER TABLE public.seg_cliente_funcao_funcionario OWNER TO postgres;

--
-- TOC entry 222 (class 1259 OID 19204)
-- Name: seg_cliente_funcao_funcionari_id_cliente_funcao_funcionario_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.seg_cliente_funcao_funcionari_id_cliente_funcao_funcionario_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.seg_cliente_funcao_funcionari_id_cliente_funcao_funcionario_seq OWNER TO postgres;

--
-- TOC entry 3983 (class 0 OID 0)
-- Dependencies: 222
-- Name: seg_cliente_funcao_funcionari_id_cliente_funcao_funcionario_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.seg_cliente_funcao_funcionari_id_cliente_funcao_funcionario_seq OWNED BY public.seg_cliente_funcao_funcionario.id_cliente_funcao_funcionario;


--
-- TOC entry 223 (class 1259 OID 19206)
-- Name: seg_cliente_funcao_id_seg_cliente_funcao_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.seg_cliente_funcao_id_seg_cliente_funcao_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.seg_cliente_funcao_id_seg_cliente_funcao_seq OWNER TO postgres;

--
-- TOC entry 3984 (class 0 OID 0)
-- Dependencies: 223
-- Name: seg_cliente_funcao_id_seg_cliente_funcao_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.seg_cliente_funcao_id_seg_cliente_funcao_seq OWNED BY public.seg_cliente_funcao.id_seg_cliente_funcao;


--
-- TOC entry 224 (class 1259 OID 19208)
-- Name: seg_cliente_funcionario; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.seg_cliente_funcionario (
    id bigint NOT NULL,
    id_funcionario bigint NOT NULL,
    id_cliente bigint NOT NULL,
    br_pdh character varying(3),
    data_admissao date,
    data_demissao date,
    regime_revezamento character varying(15),
    matricula character varying(30),
    estagiario boolean DEFAULT false NOT NULL,
    vinculo boolean DEFAULT true NOT NULL
);


ALTER TABLE public.seg_cliente_funcionario OWNER TO postgres;

--
-- TOC entry 225 (class 1259 OID 19213)
-- Name: seg_cliente_funcionario_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.seg_cliente_funcionario_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.seg_cliente_funcionario_id_seq OWNER TO postgres;

--
-- TOC entry 3985 (class 0 OID 0)
-- Dependencies: 225
-- Name: seg_cliente_funcionario_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.seg_cliente_funcionario_id_seq OWNED BY public.seg_cliente_funcionario.id;


--
-- TOC entry 226 (class 1259 OID 19215)
-- Name: seg_cliente_id_cliente_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.seg_cliente_id_cliente_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.seg_cliente_id_cliente_seq OWNER TO postgres;

--
-- TOC entry 3986 (class 0 OID 0)
-- Dependencies: 226
-- Name: seg_cliente_id_cliente_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.seg_cliente_id_cliente_seq OWNED BY public.seg_cliente.id_cliente;


--
-- TOC entry 227 (class 1259 OID 19217)
-- Name: seg_cliente_proposta; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.seg_cliente_proposta (
    id_cliente_proposta bigint NOT NULL,
    id_cidade_ibge bigint,
    razao_social character varying(100) NOT NULL,
    nome_fantasia character varying(100),
    cnpj character varying(15),
    inscricao character varying(20),
    endereco character varying(100),
    numero character varying(10),
    complemento character varying(100),
    bairro character varying(50),
    cep character varying(8),
    uf character varying(2),
    telefone character varying(15) NOT NULL,
    celular character varying(15),
    data_cadastro date NOT NULL,
    contato character varying(100),
    email character varying(50)
);


ALTER TABLE public.seg_cliente_proposta OWNER TO postgres;

--
-- TOC entry 228 (class 1259 OID 19223)
-- Name: seg_cliente_proposta_id_cliente_proposta_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.seg_cliente_proposta_id_cliente_proposta_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.seg_cliente_proposta_id_cliente_proposta_seq OWNER TO postgres;

--
-- TOC entry 3987 (class 0 OID 0)
-- Dependencies: 228
-- Name: seg_cliente_proposta_id_cliente_proposta_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.seg_cliente_proposta_id_cliente_proposta_seq OWNED BY public.seg_cliente_proposta.id_cliente_proposta;


--
-- TOC entry 229 (class 1259 OID 19225)
-- Name: seg_cnae; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.seg_cnae (
    id_cnae bigint NOT NULL,
    cod_cnae character varying(7) NOT NULL,
    atividade character varying(400) NOT NULL,
    graurisco character varying(1),
    grupo character varying(5),
    situacao character varying(1) DEFAULT 'A'::character varying NOT NULL
);


ALTER TABLE public.seg_cnae OWNER TO postgres;

--
-- TOC entry 230 (class 1259 OID 19229)
-- Name: seg_cnae_estudo; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.seg_cnae_estudo (
    id_cnae_estudo bigint NOT NULL,
    id_estudo bigint NOT NULL,
    id_cnae bigint NOT NULL,
    flag_cliente boolean DEFAULT false
);


ALTER TABLE public.seg_cnae_estudo OWNER TO postgres;

--
-- TOC entry 231 (class 1259 OID 19234)
-- Name: seg_cnae_estudo_id_cnae_estudo_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.seg_cnae_estudo_id_cnae_estudo_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.seg_cnae_estudo_id_cnae_estudo_seq OWNER TO postgres;

--
-- TOC entry 3988 (class 0 OID 0)
-- Dependencies: 231
-- Name: seg_cnae_estudo_id_cnae_estudo_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.seg_cnae_estudo_id_cnae_estudo_seq OWNED BY public.seg_cnae_estudo.id_cnae_estudo;


--
-- TOC entry 232 (class 1259 OID 19236)
-- Name: seg_cnae_id_cnae_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.seg_cnae_id_cnae_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.seg_cnae_id_cnae_seq OWNER TO postgres;

--
-- TOC entry 3989 (class 0 OID 0)
-- Dependencies: 232
-- Name: seg_cnae_id_cnae_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.seg_cnae_id_cnae_seq OWNED BY public.seg_cnae.id_cnae;


--
-- TOC entry 233 (class 1259 OID 19238)
-- Name: seg_comentario_funcao; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.seg_comentario_funcao (
    id_comentario_funcao bigint NOT NULL,
    id_funcao bigint NOT NULL,
    atividade text NOT NULL,
    situacao character varying(1) NOT NULL
);


ALTER TABLE public.seg_comentario_funcao OWNER TO postgres;

--
-- TOC entry 234 (class 1259 OID 19244)
-- Name: seg_comentario_funcao_id_comentario_funcao_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.seg_comentario_funcao_id_comentario_funcao_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.seg_comentario_funcao_id_comentario_funcao_seq OWNER TO postgres;

--
-- TOC entry 3990 (class 0 OID 0)
-- Dependencies: 234
-- Name: seg_comentario_funcao_id_comentario_funcao_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.seg_comentario_funcao_id_comentario_funcao_seq OWNED BY public.seg_comentario_funcao.id_comentario_funcao;


--
-- TOC entry 235 (class 1259 OID 19246)
-- Name: seg_configuracao; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.seg_configuracao (
    id_configuracao bigint NOT NULL,
    descricao character varying NOT NULL,
    valor character varying NOT NULL
);


ALTER TABLE public.seg_configuracao OWNER TO postgres;

--
-- TOC entry 236 (class 1259 OID 19252)
-- Name: seg_configuracao_id_configuracao_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.seg_configuracao_id_configuracao_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.seg_configuracao_id_configuracao_seq OWNER TO postgres;

--
-- TOC entry 3991 (class 0 OID 0)
-- Dependencies: 236
-- Name: seg_configuracao_id_configuracao_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.seg_configuracao_id_configuracao_seq OWNED BY public.seg_configuracao.id_configuracao;


--
-- TOC entry 237 (class 1259 OID 19254)
-- Name: seg_conta; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.seg_conta (
    id_conta bigint NOT NULL,
    id_banco bigint NOT NULL,
    nome character varying(50) NOT NULL,
    agencia_numero character varying(5) NOT NULL,
    agencia_digito character varying(1),
    conta_numero character varying(12) NOT NULL,
    conta_digito character varying(2),
    proximo_numero bigint,
    numero_remessa character varying(7),
    carteira character varying(3),
    convenio character varying(16),
    variacao_carteira character varying(3),
    codigo_cedente_banco character varying(20),
    registro boolean NOT NULL,
    situacao character varying(1) NOT NULL,
    privado boolean NOT NULL
);


ALTER TABLE public.seg_conta OWNER TO postgres;

--
-- TOC entry 238 (class 1259 OID 19257)
-- Name: seg_conta_id_conta_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.seg_conta_id_conta_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.seg_conta_id_conta_seq OWNER TO postgres;

--
-- TOC entry 3992 (class 0 OID 0)
-- Dependencies: 238
-- Name: seg_conta_id_conta_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.seg_conta_id_conta_seq OWNED BY public.seg_conta.id_conta;


--
-- TOC entry 239 (class 1259 OID 19259)
-- Name: seg_contato; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.seg_contato (
    id bigint NOT NULL,
    id_cidade bigint,
    id_cliente bigint NOT NULL,
    id_tipo_contato bigint NOT NULL,
    nome character varying(100) NOT NULL,
    cpf character varying(14),
    rg character varying(15),
    data_nascimento date,
    endereco character varying(100),
    numero character varying(15),
    complemento character varying(100),
    bairro character varying(100),
    uf character varying(2),
    cep character varying(8),
    telefone character varying(15),
    celular character varying(15),
    pis_pasep character varying(11),
    email character varying(100),
    responsavel_contrato boolean DEFAULT false NOT NULL,
    responsavel_estudo boolean DEFAULT false NOT NULL
);


ALTER TABLE public.seg_contato OWNER TO postgres;

--
-- TOC entry 240 (class 1259 OID 19266)
-- Name: seg_contato_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.seg_contato_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.seg_contato_id_seq OWNER TO postgres;

--
-- TOC entry 3993 (class 0 OID 0)
-- Dependencies: 240
-- Name: seg_contato_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.seg_contato_id_seq OWNED BY public.seg_contato.id;


--
-- TOC entry 241 (class 1259 OID 19268)
-- Name: seg_contrato; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.seg_contrato (
    id_contrato bigint NOT NULL,
    codigo_contrato character varying(14),
    id_cliente bigint NOT NULL,
    data_elaboracao timestamp without time zone,
    data_fim timestamp without time zone,
    data_inicio timestamp without time zone,
    situacao character varying(1),
    observacao text,
    id_usuario_criou bigint,
    id_usuario_finalizou bigint,
    id_usuario_cancelou bigint,
    motivo_cancelamento text,
    contrato_automatico boolean DEFAULT false NOT NULL,
    data_cancelamento date,
    id_contrato_pai bigint,
    bloqueia_inadimplencia boolean DEFAULT false NOT NULL,
    gera_mensalidade boolean DEFAULT false NOT NULL,
    id_cliente_prestador bigint,
    gera_numero_vidas boolean DEFAULT false NOT NULL,
    data_cobranca integer,
    cobranca_automatica boolean DEFAULT false NOT NULL,
    valor_mensalidade numeric(8,2) DEFAULT 0.00 NOT NULL,
    valor_numero_vidas numeric(8,2) DEFAULT 0.00 NOT NULL,
    dias_bloqueio integer,
    id_contrato_raiz bigint,
    id_cliente_proposta bigint,
    proposta boolean DEFAULT false NOT NULL,
    proposta_forma_pagamento text,
    id_usuario_encerrou bigint,
    data_encerramento timestamp without time zone,
    motivo_encerramento text,
    id_empresa bigint
);


ALTER TABLE public.seg_contrato OWNER TO postgres;

--
-- TOC entry 242 (class 1259 OID 19282)
-- Name: seg_contrato_exame; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.seg_contrato_exame (
    id_contrato_exame bigint NOT NULL,
    id_exame bigint NOT NULL,
    id_contrato bigint NOT NULL,
    preco_contrato numeric(8,2) NOT NULL,
    usuario character varying(15) NOT NULL,
    custo_contrato numeric(8,2) NOT NULL,
    data_inclusao timestamp without time zone,
    situacao character varying(1) DEFAULT 'A'::character varying NOT NULL,
    altera boolean DEFAULT true NOT NULL
);


ALTER TABLE public.seg_contrato_exame OWNER TO postgres;

--
-- TOC entry 243 (class 1259 OID 19287)
-- Name: seg_contrato_exame_id_contrato_exame_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.seg_contrato_exame_id_contrato_exame_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.seg_contrato_exame_id_contrato_exame_seq OWNER TO postgres;

--
-- TOC entry 3994 (class 0 OID 0)
-- Dependencies: 243
-- Name: seg_contrato_exame_id_contrato_exame_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.seg_contrato_exame_id_contrato_exame_seq OWNED BY public.seg_contrato_exame.id_contrato_exame;


--
-- TOC entry 244 (class 1259 OID 19289)
-- Name: seg_contrato_id_contrato_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.seg_contrato_id_contrato_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.seg_contrato_id_contrato_seq OWNER TO postgres;

--
-- TOC entry 3995 (class 0 OID 0)
-- Dependencies: 244
-- Name: seg_contrato_id_contrato_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.seg_contrato_id_contrato_seq OWNED BY public.seg_contrato.id_contrato;


--
-- TOC entry 245 (class 1259 OID 19291)
-- Name: seg_correcao_estudo; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.seg_correcao_estudo (
    id_correcao_estudo bigint NOT NULL,
    id_usuario bigint NOT NULL,
    id_estudo bigint NOT NULL,
    historico text NOT NULL,
    data_correcao timestamp without time zone NOT NULL
);


ALTER TABLE public.seg_correcao_estudo OWNER TO postgres;

--
-- TOC entry 246 (class 1259 OID 19297)
-- Name: seg_correcao_estudo_id_correcao_estudo_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.seg_correcao_estudo_id_correcao_estudo_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.seg_correcao_estudo_id_correcao_estudo_seq OWNER TO postgres;

--
-- TOC entry 3996 (class 0 OID 0)
-- Dependencies: 246
-- Name: seg_correcao_estudo_id_correcao_estudo_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.seg_correcao_estudo_id_correcao_estudo_seq OWNED BY public.seg_correcao_estudo.id_correcao_estudo;


--
-- TOC entry 247 (class 1259 OID 19299)
-- Name: seg_crc; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.seg_crc (
    id_crc bigint NOT NULL,
    id_forma bigint NOT NULL,
    id_usuario_cancela bigint,
    id_usuario_estorna bigint,
    id_usuario_desdobra bigint,
    id_usuario_criador bigint NOT NULL,
    id_conta bigint,
    id_nfe bigint NOT NULL,
    valor numeric(8,2) NOT NULL,
    data_emissao date NOT NULL,
    data_vencimento date NOT NULL,
    data_pagamento date,
    situacao character varying(1) NOT NULL,
    numero_parcela integer NOT NULL,
    numero_documento character varying(20) NOT NULL,
    data_alteracao date,
    observacao_cancelamento text,
    id_crc_desdobramento bigint,
    observacao_cobranca text,
    observacao_baixa text,
    data_baixa date,
    id_usuario_baixa bigint,
    observacao_estorno_baixa text,
    data_estorno date,
    data_cancelamento date,
    id_usuario_prorrogacao bigint,
    observacao_prorrogacao text,
    valor_ajustado numeric(8,2) NOT NULL,
    valor_baixado numeric(8,2),
    nosso_numero character varying(20),
    valor_liquido numeric(8,2) DEFAULT 0.00 NOT NULL
);


ALTER TABLE public.seg_crc OWNER TO postgres;

--
-- TOC entry 248 (class 1259 OID 19306)
-- Name: seg_crc_id_crc_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.seg_crc_id_crc_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.seg_crc_id_crc_seq OWNER TO postgres;

--
-- TOC entry 3997 (class 0 OID 0)
-- Dependencies: 248
-- Name: seg_crc_id_crc_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.seg_crc_id_crc_seq OWNED BY public.seg_crc.id_crc;


--
-- TOC entry 249 (class 1259 OID 19308)
-- Name: seg_cronograma; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.seg_cronograma (
    id_cronograma bigint NOT NULL,
    descricao character varying(100)
);


ALTER TABLE public.seg_cronograma OWNER TO postgres;

--
-- TOC entry 250 (class 1259 OID 19311)
-- Name: seg_cronograma_id_cronograma_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.seg_cronograma_id_cronograma_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.seg_cronograma_id_cronograma_seq OWNER TO postgres;

--
-- TOC entry 3998 (class 0 OID 0)
-- Dependencies: 250
-- Name: seg_cronograma_id_cronograma_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.seg_cronograma_id_cronograma_seq OWNED BY public.seg_cronograma.id_cronograma;


--
-- TOC entry 251 (class 1259 OID 19313)
-- Name: seg_documento; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.seg_documento (
    id_documento bigint NOT NULL,
    descricao character varying(200) NOT NULL,
    situacao character varying(1) DEFAULT 'A'::character varying NOT NULL
);


ALTER TABLE public.seg_documento OWNER TO postgres;

--
-- TOC entry 405 (class 1259 OID 327377)
-- Name: seg_documento_estudo; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.seg_documento_estudo (
    id_documento_estudo bigint NOT NULL,
    id_documento bigint NOT NULL,
    id_estudo bigint NOT NULL
);


ALTER TABLE public.seg_documento_estudo OWNER TO postgres;

--
-- TOC entry 404 (class 1259 OID 327375)
-- Name: seg_documento_estudo_id_documento_estudo_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.seg_documento_estudo_id_documento_estudo_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.seg_documento_estudo_id_documento_estudo_seq OWNER TO postgres;

--
-- TOC entry 3999 (class 0 OID 0)
-- Dependencies: 404
-- Name: seg_documento_estudo_id_documento_estudo_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.seg_documento_estudo_id_documento_estudo_seq OWNED BY public.seg_documento_estudo.id_documento_estudo;


--
-- TOC entry 252 (class 1259 OID 19317)
-- Name: seg_documento_id_documento_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.seg_documento_id_documento_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.seg_documento_id_documento_seq OWNER TO postgres;

--
-- TOC entry 4000 (class 0 OID 0)
-- Dependencies: 252
-- Name: seg_documento_id_documento_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.seg_documento_id_documento_seq OWNED BY public.seg_documento.id_documento;


--
-- TOC entry 253 (class 1259 OID 19319)
-- Name: seg_documento_protocolo; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.seg_documento_protocolo (
    id_documento_protocolo bigint NOT NULL,
    id_aso bigint NOT NULL,
    id_documento bigint NOT NULL,
    id_protocolo bigint,
    data_gravacao date NOT NULL
);


ALTER TABLE public.seg_documento_protocolo OWNER TO postgres;

--
-- TOC entry 254 (class 1259 OID 19322)
-- Name: seg_documento_protocolo_id_documento_protocolo_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.seg_documento_protocolo_id_documento_protocolo_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.seg_documento_protocolo_id_documento_protocolo_seq OWNER TO postgres;

--
-- TOC entry 4001 (class 0 OID 0)
-- Dependencies: 254
-- Name: seg_documento_protocolo_id_documento_protocolo_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.seg_documento_protocolo_id_documento_protocolo_seq OWNED BY public.seg_documento_protocolo.id_documento_protocolo;


--
-- TOC entry 255 (class 1259 OID 19324)
-- Name: seg_dominio; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.seg_dominio (
    id_dominio bigint NOT NULL,
    descricao character varying(100) NOT NULL,
    situacao character varying(1) NOT NULL
);


ALTER TABLE public.seg_dominio OWNER TO postgres;

--
-- TOC entry 256 (class 1259 OID 19327)
-- Name: seg_dominio_acao; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.seg_dominio_acao (
    id_dominio bigint NOT NULL,
    id_acao bigint NOT NULL
);


ALTER TABLE public.seg_dominio_acao OWNER TO postgres;

--
-- TOC entry 257 (class 1259 OID 19330)
-- Name: seg_dominio_id_dominio_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.seg_dominio_id_dominio_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.seg_dominio_id_dominio_seq OWNER TO postgres;

--
-- TOC entry 4002 (class 0 OID 0)
-- Dependencies: 257
-- Name: seg_dominio_id_dominio_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.seg_dominio_id_dominio_seq OWNED BY public.seg_dominio.id_dominio;


--
-- TOC entry 258 (class 1259 OID 19332)
-- Name: seg_email; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.seg_email (
    id_email bigint NOT NULL,
    id_usuario bigint NOT NULL,
    id_prontuario bigint,
    para character varying NOT NULL,
    assunto character varying NOT NULL,
    mensagem text NOT NULL,
    anexo character varying,
    prioridade character varying NOT NULL,
    data_envio timestamp without time zone NOT NULL
);


ALTER TABLE public.seg_email OWNER TO postgres;

--
-- TOC entry 259 (class 1259 OID 19338)
-- Name: seg_email_id_email_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.seg_email_id_email_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.seg_email_id_email_seq OWNER TO postgres;

--
-- TOC entry 4003 (class 0 OID 0)
-- Dependencies: 259
-- Name: seg_email_id_email_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.seg_email_id_email_seq OWNED BY public.seg_email.id_email;


--
-- TOC entry 260 (class 1259 OID 19340)
-- Name: seg_empresa; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.seg_empresa (
    id_empresa bigint NOT NULL,
    razao_social character varying(100),
    fantasia character varying(100),
    cnpj character varying(14) NOT NULL,
    inscricao character varying(20) NOT NULL,
    endereco character varying(100),
    numero character varying(10) NOT NULL,
    complemento character varying(100),
    bairro character varying(50),
    cep character varying(8),
    uf character varying(2),
    telefone1 character varying(15),
    telefone2 character varying(15),
    email character varying(50),
    site character varying(50),
    data_cadastro date NOT NULL,
    situacao character varying(1) NOT NULL,
    logo bytea,
    mimetype character varying(30),
    id_cidade_ibge bigint NOT NULL,
    simples_nacional boolean DEFAULT false NOT NULL,
    codigo_cnes character varying(7),
    id_empresa_matriz bigint,
    valor_minimo_imposto_federal numeric(8,2) DEFAULT 0.00 NOT NULL,
    valor_minimo_ir numeric(8,2) DEFAULT 0.00 NOT NULL,
    aliquota_pis numeric(8,2) DEFAULT 0.00 NOT NULL,
    aliquota_cofins numeric(8,2) DEFAULT 0.00 NOT NULL,
    aliquota_ir numeric(8,2) DEFAULT 0.00 NOT NULL,
    aliquota_csll numeric(8,2) DEFAULT 0.00 NOT NULL,
    aliquota_iss numeric(8,2) DEFAULT 0.00 NOT NULL
);


ALTER TABLE public.seg_empresa OWNER TO postgres;

--
-- TOC entry 261 (class 1259 OID 19347)
-- Name: seg_empresa_id_empresa_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.seg_empresa_id_empresa_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.seg_empresa_id_empresa_seq OWNER TO postgres;

--
-- TOC entry 4004 (class 0 OID 0)
-- Dependencies: 261
-- Name: seg_empresa_id_empresa_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.seg_empresa_id_empresa_seq OWNED BY public.seg_empresa.id_empresa;


--
-- TOC entry 407 (class 1259 OID 327462)
-- Name: seg_endemia; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.seg_endemia (
    id_endemia bigint NOT NULL,
    doenca character varying(100),
    agente character varying(100),
    reservatorio character varying(100),
    modo_transmissao text,
    periodo_incubacao character varying(100),
    medidas_preventivas text
);


ALTER TABLE public.seg_endemia OWNER TO postgres;

--
-- TOC entry 406 (class 1259 OID 327460)
-- Name: seg_endemia_id_endemia_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.seg_endemia_id_endemia_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.seg_endemia_id_endemia_seq OWNER TO postgres;

--
-- TOC entry 4005 (class 0 OID 0)
-- Dependencies: 406
-- Name: seg_endemia_id_endemia_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.seg_endemia_id_endemia_seq OWNED BY public.seg_endemia.id_endemia;


--
-- TOC entry 262 (class 1259 OID 19357)
-- Name: seg_epi; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.seg_epi (
    id_epi bigint NOT NULL,
    descricao character varying(200) NOT NULL,
    finalidade character varying(150),
    ca character varying(50) NOT NULL,
    situacao character varying(1)
);


ALTER TABLE public.seg_epi OWNER TO postgres;

--
-- TOC entry 263 (class 1259 OID 19360)
-- Name: seg_epi_id_epi_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.seg_epi_id_epi_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.seg_epi_id_epi_seq OWNER TO postgres;

--
-- TOC entry 4006 (class 0 OID 0)
-- Dependencies: 263
-- Name: seg_epi_id_epi_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.seg_epi_id_epi_seq OWNED BY public.seg_epi.id_epi;


--
-- TOC entry 401 (class 1259 OID 245180)
-- Name: seg_epi_monitoramento; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.seg_epi_monitoramento (
    id_epi_monitoramento bigint NOT NULL,
    id_epi bigint NOT NULL,
    id_monitoramento_ghefonteagente bigint NOT NULL
);


ALTER TABLE public.seg_epi_monitoramento OWNER TO postgres;

--
-- TOC entry 400 (class 1259 OID 245178)
-- Name: seg_epi_monitoramento_id_epi_monitoramento_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.seg_epi_monitoramento_id_epi_monitoramento_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.seg_epi_monitoramento_id_epi_monitoramento_seq OWNER TO postgres;

--
-- TOC entry 4007 (class 0 OID 0)
-- Dependencies: 400
-- Name: seg_epi_monitoramento_id_epi_monitoramento_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.seg_epi_monitoramento_id_epi_monitoramento_seq OWNED BY public.seg_epi_monitoramento.id_epi_monitoramento;


--
-- TOC entry 397 (class 1259 OID 245135)
-- Name: seg_esocial_24; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.seg_esocial_24 (
    id bigint NOT NULL,
    codigo character varying(10) NOT NULL,
    descricao text NOT NULL
);


ALTER TABLE public.seg_esocial_24 OWNER TO postgres;

--
-- TOC entry 396 (class 1259 OID 245133)
-- Name: seg_esocial_24_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.seg_esocial_24_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.seg_esocial_24_id_seq OWNER TO postgres;

--
-- TOC entry 4008 (class 0 OID 0)
-- Dependencies: 396
-- Name: seg_esocial_24_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.seg_esocial_24_id_seq OWNED BY public.seg_esocial_24.id;


--
-- TOC entry 267 (class 1259 OID 19372)
-- Name: seg_esocial_lote_id_esocial_lote_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.seg_esocial_lote_id_esocial_lote_seq
    START WITH 11
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.seg_esocial_lote_id_esocial_lote_seq OWNER TO postgres;

--
-- TOC entry 264 (class 1259 OID 19362)
-- Name: seg_esocial_lote; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.seg_esocial_lote (
    id_esocial_lote bigint DEFAULT nextval('public.seg_esocial_lote_id_esocial_lote_seq'::regclass) NOT NULL,
    id_cliente bigint NOT NULL,
    tipo character varying(4) NOT NULL,
    data_criacao timestamp without time zone NOT NULL,
    codigo character varying(14),
    periodo date
);


ALTER TABLE public.seg_esocial_lote OWNER TO postgres;

--
-- TOC entry 266 (class 1259 OID 19368)
-- Name: seg_esocial_lote_aso_id_seg_esocial_lote_aso_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.seg_esocial_lote_aso_id_seg_esocial_lote_aso_seq
    START WITH 12
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.seg_esocial_lote_aso_id_seg_esocial_lote_aso_seq OWNER TO postgres;

--
-- TOC entry 265 (class 1259 OID 19365)
-- Name: seg_esocial_lote_aso; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.seg_esocial_lote_aso (
    id_seg_esocial_lote_aso bigint DEFAULT nextval('public.seg_esocial_lote_aso_id_seg_esocial_lote_aso_seq'::regclass) NOT NULL,
    id_esocial_lote bigint NOT NULL,
    id_aso bigint NOT NULL,
    situacao character varying(1) DEFAULT 'E'::character varying NOT NULL,
    nrecibo character varying(25),
    data_cancelamento timestamp(0) without time zone,
    usuario_cancelou character varying(15)
);


ALTER TABLE public.seg_esocial_lote_aso OWNER TO postgres;

--
-- TOC entry 395 (class 1259 OID 245113)
-- Name: seg_esocial_monitoramento; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.seg_esocial_monitoramento (
    id_esocial_monitoramento bigint NOT NULL,
    id_esocial_lote bigint NOT NULL,
    id_monitoramento bigint NOT NULL,
    situacao character varying(1) DEFAULT 'E'::character varying NOT NULL,
    nrecibo character varying(25),
    data_cancelamento timestamp without time zone,
    usuario_cancelou character varying(15)
);


ALTER TABLE public.seg_esocial_monitoramento OWNER TO postgres;

--
-- TOC entry 394 (class 1259 OID 245111)
-- Name: seg_esocial_monitoramento_id_esocial_monitoramento_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.seg_esocial_monitoramento_id_esocial_monitoramento_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.seg_esocial_monitoramento_id_esocial_monitoramento_seq OWNER TO postgres;

--
-- TOC entry 4009 (class 0 OID 0)
-- Dependencies: 394
-- Name: seg_esocial_monitoramento_id_esocial_monitoramento_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.seg_esocial_monitoramento_id_esocial_monitoramento_seq OWNED BY public.seg_esocial_monitoramento.id_esocial_monitoramento;


--
-- TOC entry 268 (class 1259 OID 19376)
-- Name: seg_estimativa; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.seg_estimativa (
    id_estimativa bigint NOT NULL,
    descricao character varying(100) NOT NULL,
    situacao character varying(1) NOT NULL
);


ALTER TABLE public.seg_estimativa OWNER TO postgres;

--
-- TOC entry 269 (class 1259 OID 19379)
-- Name: seg_estimativa_estudo; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.seg_estimativa_estudo (
    id_estimativa_estudo bigint NOT NULL,
    id_estudo bigint NOT NULL,
    id_estimativa bigint NOT NULL,
    quantidade integer NOT NULL
);


ALTER TABLE public.seg_estimativa_estudo OWNER TO postgres;

--
-- TOC entry 270 (class 1259 OID 19382)
-- Name: seg_estimativa_estudo_id_estimativa_estudo_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.seg_estimativa_estudo_id_estimativa_estudo_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.seg_estimativa_estudo_id_estimativa_estudo_seq OWNER TO postgres;

--
-- TOC entry 4010 (class 0 OID 0)
-- Dependencies: 270
-- Name: seg_estimativa_estudo_id_estimativa_estudo_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.seg_estimativa_estudo_id_estimativa_estudo_seq OWNED BY public.seg_estimativa_estudo.id_estimativa_estudo;


--
-- TOC entry 271 (class 1259 OID 19384)
-- Name: seg_estimativa_id_estimativa_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.seg_estimativa_id_estimativa_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.seg_estimativa_id_estimativa_seq OWNER TO postgres;

--
-- TOC entry 4011 (class 0 OID 0)
-- Dependencies: 271
-- Name: seg_estimativa_id_estimativa_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.seg_estimativa_id_estimativa_seq OWNED BY public.seg_estimativa.id_estimativa;


--
-- TOC entry 272 (class 1259 OID 19386)
-- Name: seg_estudo; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.seg_estudo (
    id_estudo bigint NOT NULL,
    id_relatorio bigint,
    cod_estudo character varying(14) NOT NULL,
    id_cronograma bigint,
    id_seg_rep_cli bigint,
    id_engenheiro bigint,
    id_tecno bigint,
    id_cliente_contratado bigint,
    data_criacao date NOT NULL,
    data_vencimento date,
    data_fechamento timestamp without time zone,
    situacao character varying(1) NOT NULL,
    estudo boolean NOT NULL,
    data_fim_contrato character varying(15),
    obra character varying(255),
    local_obra character varying(255),
    endereco character varying(100),
    numero character varying(10),
    complemento character varying(100),
    bairro character varying(50),
    cidade character varying(50),
    uf character varying(2),
    num_contrato character varying(15),
    grau_risco character varying(1),
    cli_razao_social character varying(100),
    cli_cnpj character varying(14),
    cli_inscricao character varying(20),
    cli_est_fem integer,
    cli_est_masc integer,
    cli_jornada character varying(300),
    cli_endereco character varying(100),
    cli_numero character varying(10),
    cli_complemento character varying(100),
    cli_bairro character varying(50),
    cli_cidade character varying(50),
    cli_email character varying(50),
    cli_cep character varying(8),
    cli_uf character varying(2),
    cli_telefone1 character varying(15),
    cli_telefone2 character varying(15),
    contr_razao_social character varying(100),
    contr_cnpj character varying(14),
    contr_endereco character varying(100),
    contr_numero character varying(10),
    contr_complemento character varying(100),
    contr_bairro character varying(50),
    contr_cidade character varying(50),
    contr_cep character varying(8),
    contr_uf character varying(2),
    contr_inscricao character varying(20),
    data_inicio_contrato character varying(15),
    cep character varying(8),
    cli_fantasia character varying(100),
    avulso boolean NOT NULL,
    comentario character varying(500),
    id_protocolo bigint
);


ALTER TABLE public.seg_estudo OWNER TO postgres;

--
-- TOC entry 273 (class 1259 OID 19392)
-- Name: seg_estudo_hospital; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.seg_estudo_hospital (
    id_estudo bigint NOT NULL,
    id_hospital bigint NOT NULL
);


ALTER TABLE public.seg_estudo_hospital OWNER TO postgres;

--
-- TOC entry 274 (class 1259 OID 19395)
-- Name: seg_estudo_id_estudo_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.seg_estudo_id_estudo_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.seg_estudo_id_estudo_seq OWNER TO postgres;

--
-- TOC entry 4012 (class 0 OID 0)
-- Dependencies: 274
-- Name: seg_estudo_id_estudo_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.seg_estudo_id_estudo_seq OWNED BY public.seg_estudo.id_estudo;


--
-- TOC entry 275 (class 1259 OID 19397)
-- Name: seg_estudo_medico; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.seg_estudo_medico (
    id_estudo bigint NOT NULL,
    id_medico bigint NOT NULL,
    situacao character varying(1) DEFAULT 'A'::character varying NOT NULL,
    coordenador boolean DEFAULT false NOT NULL
);


ALTER TABLE public.seg_estudo_medico OWNER TO postgres;

--
-- TOC entry 276 (class 1259 OID 19402)
-- Name: seg_exame; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.seg_exame (
    id_exame bigint NOT NULL,
    descricao character varying(100) NOT NULL,
    situacao character varying(1) NOT NULL,
    laboratorio boolean NOT NULL,
    preco numeric(8,2) NOT NULL,
    prioridade integer NOT NULL,
    custo numeric(8,2),
    externo boolean NOT NULL,
    libera_documento boolean DEFAULT false NOT NULL,
    codigo_tuss character varying(4),
    exame_complementar boolean DEFAULT false NOT NULL,
    periodo_vencimento integer,
    padrao_contrato boolean DEFAULT false NOT NULL
);


ALTER TABLE public.seg_exame OWNER TO postgres;

--
-- TOC entry 277 (class 1259 OID 19407)
-- Name: seg_exame_id_exame_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.seg_exame_id_exame_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.seg_exame_id_exame_seq OWNER TO postgres;

--
-- TOC entry 4013 (class 0 OID 0)
-- Dependencies: 277
-- Name: seg_exame_id_exame_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.seg_exame_id_exame_seq OWNED BY public.seg_exame.id_exame;


--
-- TOC entry 278 (class 1259 OID 19409)
-- Name: seg_fonte; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.seg_fonte (
    id_fonte bigint NOT NULL,
    descricao character varying(100) NOT NULL,
    situacao character varying(1) NOT NULL,
    privado boolean NOT NULL
);


ALTER TABLE public.seg_fonte OWNER TO postgres;

--
-- TOC entry 279 (class 1259 OID 19412)
-- Name: seg_fonte_id_fonte_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.seg_fonte_id_fonte_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.seg_fonte_id_fonte_seq OWNER TO postgres;

--
-- TOC entry 4014 (class 0 OID 0)
-- Dependencies: 279
-- Name: seg_fonte_id_fonte_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.seg_fonte_id_fonte_seq OWNED BY public.seg_fonte.id_fonte;


--
-- TOC entry 280 (class 1259 OID 19414)
-- Name: seg_forma; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.seg_forma (
    id_forma bigint NOT NULL,
    descricao character varying(100) NOT NULL
);


ALTER TABLE public.seg_forma OWNER TO postgres;

--
-- TOC entry 281 (class 1259 OID 19417)
-- Name: seg_forma_id_forma_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.seg_forma_id_forma_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.seg_forma_id_forma_seq OWNER TO postgres;

--
-- TOC entry 4015 (class 0 OID 0)
-- Dependencies: 281
-- Name: seg_forma_id_forma_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.seg_forma_id_forma_seq OWNED BY public.seg_forma.id_forma;


--
-- TOC entry 282 (class 1259 OID 19419)
-- Name: seg_funcao; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.seg_funcao (
    id_funcao bigint NOT NULL,
    descricao character varying(100) NOT NULL,
    cod_cbo character varying(6),
    situacao character varying(1) NOT NULL,
    comentario character varying(700) NOT NULL
);


ALTER TABLE public.seg_funcao OWNER TO postgres;

--
-- TOC entry 283 (class 1259 OID 19425)
-- Name: seg_funcao_epi; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.seg_funcao_epi (
    id_seg_funcao_epi bigint NOT NULL,
    id_estudo bigint NOT NULL,
    nome_funcao character varying(100),
    nome_epi character varying(100),
    tipo character varying(2),
    classificacao character varying(3)
);


ALTER TABLE public.seg_funcao_epi OWNER TO postgres;

--
-- TOC entry 284 (class 1259 OID 19428)
-- Name: seg_funcao_epi_id_seg_funcao_epi_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.seg_funcao_epi_id_seg_funcao_epi_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.seg_funcao_epi_id_seg_funcao_epi_seq OWNER TO postgres;

--
-- TOC entry 4016 (class 0 OID 0)
-- Dependencies: 284
-- Name: seg_funcao_epi_id_seg_funcao_epi_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.seg_funcao_epi_id_seg_funcao_epi_seq OWNED BY public.seg_funcao_epi.id_seg_funcao_epi;


--
-- TOC entry 285 (class 1259 OID 19430)
-- Name: seg_funcao_id_funcao_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.seg_funcao_id_funcao_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.seg_funcao_id_funcao_seq OWNER TO postgres;

--
-- TOC entry 4017 (class 0 OID 0)
-- Dependencies: 285
-- Name: seg_funcao_id_funcao_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.seg_funcao_id_funcao_seq OWNED BY public.seg_funcao.id_funcao;


--
-- TOC entry 286 (class 1259 OID 19432)
-- Name: seg_funcao_interna; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.seg_funcao_interna (
    id_funcao_interna bigint NOT NULL,
    descricao character varying(100) NOT NULL,
    situacao character varying(1) NOT NULL,
    privado character varying(1) NOT NULL
);


ALTER TABLE public.seg_funcao_interna OWNER TO postgres;

--
-- TOC entry 287 (class 1259 OID 19435)
-- Name: seg_funcao_interna_id_funcao_interna_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.seg_funcao_interna_id_funcao_interna_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.seg_funcao_interna_id_funcao_interna_seq OWNER TO postgres;

--
-- TOC entry 4018 (class 0 OID 0)
-- Dependencies: 287
-- Name: seg_funcao_interna_id_funcao_interna_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.seg_funcao_interna_id_funcao_interna_seq OWNED BY public.seg_funcao_interna.id_funcao_interna;


--
-- TOC entry 288 (class 1259 OID 19437)
-- Name: seg_funcionario; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.seg_funcionario (
    id_funcionario bigint NOT NULL,
    id_cidade bigint,
    nome character varying(100) NOT NULL,
    cpf character varying(11),
    rg character varying(15) NOT NULL,
    endereco character varying(100),
    numero character varying(10),
    complemento character varying(100),
    bairro character varying(50),
    cidade character varying(50),
    uf character varying(2),
    telefone1 character varying(15),
    telefone2 character varying(15),
    email character varying(50),
    tiposan character varying(2),
    ftrh character varying(1),
    data_nascimento date NOT NULL,
    peso integer,
    altura numeric(8,2),
    situacao character varying(1) NOT NULL,
    cep character varying(8),
    sexo character varying(1) NOT NULL,
    ctps character varying(15),
    pis_pasep character varying(11),
    pcd boolean DEFAULT false NOT NULL,
    orgao_emissor character varying(15),
    uf_emissor character varying(2),
    serie character varying(15),
    uf_emissor_ctps character varying(2)
);


ALTER TABLE public.seg_funcionario OWNER TO postgres;

--
-- TOC entry 289 (class 1259 OID 19443)
-- Name: seg_funcionario_id_funcionario_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.seg_funcionario_id_funcionario_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.seg_funcionario_id_funcionario_seq OWNER TO postgres;

--
-- TOC entry 4019 (class 0 OID 0)
-- Dependencies: 289
-- Name: seg_funcionario_id_funcionario_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.seg_funcionario_id_funcionario_seq OWNED BY public.seg_funcionario.id_funcionario;


--
-- TOC entry 290 (class 1259 OID 19445)
-- Name: seg_ghe; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.seg_ghe (
    id_ghe bigint NOT NULL,
    id_estudo bigint NOT NULL,
    descricao character varying(100),
    nexp integer,
    situacao character varying(1) NOT NULL,
    numero_po character varying(15)
);


ALTER TABLE public.seg_ghe OWNER TO postgres;

--
-- TOC entry 291 (class 1259 OID 19448)
-- Name: seg_ghe_fonte; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.seg_ghe_fonte (
    id_ghe_fonte bigint NOT NULL,
    id_fonte bigint NOT NULL,
    id_ghe bigint NOT NULL,
    principal boolean,
    situacao character varying(1) NOT NULL
);


ALTER TABLE public.seg_ghe_fonte OWNER TO postgres;

--
-- TOC entry 292 (class 1259 OID 19451)
-- Name: seg_ghe_fonte_agente; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.seg_ghe_fonte_agente (
    id_ghe_fonte_agente bigint NOT NULL,
    id_grad_soma bigint,
    id_grad_efeito bigint,
    id_grad_exposicao bigint,
    id_ghe_fonte bigint NOT NULL,
    id_agente bigint NOT NULL,
    tempo_exposicao character varying(30),
    situacao character varying(1) NOT NULL
);


ALTER TABLE public.seg_ghe_fonte_agente OWNER TO postgres;

--
-- TOC entry 293 (class 1259 OID 19454)
-- Name: seg_ghe_fonte_agente_epi; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.seg_ghe_fonte_agente_epi (
    id_ghe_fonte_agente_epi bigint NOT NULL,
    id_ghe_fonte_agente bigint NOT NULL,
    id_epi bigint NOT NULL,
    tipo_epi character varying(2) NOT NULL,
    classificacao character varying(3) NOT NULL
);


ALTER TABLE public.seg_ghe_fonte_agente_epi OWNER TO postgres;

--
-- TOC entry 294 (class 1259 OID 19457)
-- Name: seg_ghe_fonte_agente_epi_id_ghe_fonte_agente_epi_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.seg_ghe_fonte_agente_epi_id_ghe_fonte_agente_epi_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.seg_ghe_fonte_agente_epi_id_ghe_fonte_agente_epi_seq OWNER TO postgres;

--
-- TOC entry 4020 (class 0 OID 0)
-- Dependencies: 294
-- Name: seg_ghe_fonte_agente_epi_id_ghe_fonte_agente_epi_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.seg_ghe_fonte_agente_epi_id_ghe_fonte_agente_epi_seq OWNED BY public.seg_ghe_fonte_agente_epi.id_ghe_fonte_agente_epi;


--
-- TOC entry 295 (class 1259 OID 19459)
-- Name: seg_ghe_fonte_agente_exame; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.seg_ghe_fonte_agente_exame (
    id_ghe_fonte_agente_exame bigint NOT NULL,
    id_exame bigint NOT NULL,
    id_ghe_fonte_agente bigint NOT NULL,
    idade_exame integer NOT NULL,
    situacao character varying(1) NOT NULL,
    confinado boolean DEFAULT false NOT NULL,
    altura boolean DEFAULT false NOT NULL,
    eletricidade boolean DEFAULT false NOT NULL
);


ALTER TABLE public.seg_ghe_fonte_agente_exame OWNER TO postgres;

--
-- TOC entry 296 (class 1259 OID 19464)
-- Name: seg_ghe_fonte_agente_exame_aso; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.seg_ghe_fonte_agente_exame_aso (
    id_ghe_fonte_agente_exame_aso bigint NOT NULL,
    id_sala_exame bigint,
    id_aso bigint NOT NULL,
    id_ghe_fonte_agente_exame bigint NOT NULL,
    data_exame date NOT NULL,
    atendido boolean DEFAULT false,
    finalizado boolean DEFAULT false,
    login character varying(15),
    devolvido boolean DEFAULT false,
    data_atendido timestamp without time zone,
    data_devolvido timestamp without time zone,
    data_finalizado timestamp without time zone,
    id_usuario bigint,
    cancelado boolean DEFAULT false,
    data_cancelado timestamp without time zone,
    resultado character varying(1),
    observacao_resultado text,
    duplicado boolean NOT NULL,
    entrega boolean DEFAULT false NOT NULL,
    id_protocolo bigint,
    transcrito boolean DEFAULT false NOT NULL,
    id_prestador bigint,
    confinado boolean DEFAULT false NOT NULL,
    altura boolean DEFAULT false NOT NULL,
    id_medico bigint,
    data_validade date,
    eletricidade boolean DEFAULT false NOT NULL,
    imprime_aso boolean DEFAULT true NOT NULL
);


ALTER TABLE public.seg_ghe_fonte_agente_exame_aso OWNER TO postgres;

--
-- TOC entry 297 (class 1259 OID 19478)
-- Name: seg_ghe_fonte_agente_exame_as_id_ghe_fonte_agente_exame_aso_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.seg_ghe_fonte_agente_exame_as_id_ghe_fonte_agente_exame_aso_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.seg_ghe_fonte_agente_exame_as_id_ghe_fonte_agente_exame_aso_seq OWNER TO postgres;

--
-- TOC entry 4021 (class 0 OID 0)
-- Dependencies: 297
-- Name: seg_ghe_fonte_agente_exame_as_id_ghe_fonte_agente_exame_aso_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.seg_ghe_fonte_agente_exame_as_id_ghe_fonte_agente_exame_aso_seq OWNED BY public.seg_ghe_fonte_agente_exame_aso.id_ghe_fonte_agente_exame_aso;


--
-- TOC entry 298 (class 1259 OID 19480)
-- Name: seg_ghe_fonte_agente_exame_id_ghe_fonte_agente_exame_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.seg_ghe_fonte_agente_exame_id_ghe_fonte_agente_exame_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.seg_ghe_fonte_agente_exame_id_ghe_fonte_agente_exame_seq OWNER TO postgres;

--
-- TOC entry 4022 (class 0 OID 0)
-- Dependencies: 298
-- Name: seg_ghe_fonte_agente_exame_id_ghe_fonte_agente_exame_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.seg_ghe_fonte_agente_exame_id_ghe_fonte_agente_exame_seq OWNED BY public.seg_ghe_fonte_agente_exame.id_ghe_fonte_agente_exame;


--
-- TOC entry 299 (class 1259 OID 19482)
-- Name: seg_ghe_fonte_agente_exame_periodicidade; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.seg_ghe_fonte_agente_exame_periodicidade (
    id_periodicidade bigint NOT NULL,
    id_ghe_fonte_agente_exame bigint NOT NULL,
    situacao character varying(1) NOT NULL,
    periodo_vencimento integer
);


ALTER TABLE public.seg_ghe_fonte_agente_exame_periodicidade OWNER TO postgres;

--
-- TOC entry 300 (class 1259 OID 19485)
-- Name: seg_ghe_fonte_agente_id_ghe_fonte_agente_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.seg_ghe_fonte_agente_id_ghe_fonte_agente_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.seg_ghe_fonte_agente_id_ghe_fonte_agente_seq OWNER TO postgres;

--
-- TOC entry 4023 (class 0 OID 0)
-- Dependencies: 300
-- Name: seg_ghe_fonte_agente_id_ghe_fonte_agente_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.seg_ghe_fonte_agente_id_ghe_fonte_agente_seq OWNED BY public.seg_ghe_fonte_agente.id_ghe_fonte_agente;


--
-- TOC entry 301 (class 1259 OID 19487)
-- Name: seg_ghe_fonte_id_ghe_fonte_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.seg_ghe_fonte_id_ghe_fonte_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.seg_ghe_fonte_id_ghe_fonte_seq OWNER TO postgres;

--
-- TOC entry 4024 (class 0 OID 0)
-- Dependencies: 301
-- Name: seg_ghe_fonte_id_ghe_fonte_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.seg_ghe_fonte_id_ghe_fonte_seq OWNED BY public.seg_ghe_fonte.id_ghe_fonte;


--
-- TOC entry 302 (class 1259 OID 19489)
-- Name: seg_ghe_id_ghe_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.seg_ghe_id_ghe_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.seg_ghe_id_ghe_seq OWNER TO postgres;

--
-- TOC entry 4025 (class 0 OID 0)
-- Dependencies: 302
-- Name: seg_ghe_id_ghe_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.seg_ghe_id_ghe_seq OWNED BY public.seg_ghe.id_ghe;


--
-- TOC entry 303 (class 1259 OID 19491)
-- Name: seg_ghe_setor; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.seg_ghe_setor (
    id_ghe_setor bigint NOT NULL,
    id_setor bigint NOT NULL,
    id_ghe bigint NOT NULL,
    situacao character varying(1) NOT NULL
);


ALTER TABLE public.seg_ghe_setor OWNER TO postgres;

--
-- TOC entry 304 (class 1259 OID 19494)
-- Name: seg_ghe_setor_cliente_funcao; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.seg_ghe_setor_cliente_funcao (
    id_ghe_setor_cliente_funcao bigint NOT NULL,
    id_seg_cliente_funcao bigint NOT NULL,
    id_ghe_setor bigint NOT NULL,
    situacao character varying(1) DEFAULT 'A'::character varying NOT NULL,
    id_comentario_funcao bigint,
    confinado boolean DEFAULT false NOT NULL,
    altura boolean DEFAULT false NOT NULL,
    eletricidade boolean DEFAULT false NOT NULL
);


ALTER TABLE public.seg_ghe_setor_cliente_funcao OWNER TO postgres;

--
-- TOC entry 305 (class 1259 OID 19500)
-- Name: seg_ghe_setor_cliente_funcao_exame; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.seg_ghe_setor_cliente_funcao_exame (
    id_ghe_setor_cliente_funcao bigint NOT NULL,
    id_exame bigint NOT NULL,
    id_ghe_fonte_agente_exame bigint NOT NULL,
    idade integer NOT NULL
);


ALTER TABLE public.seg_ghe_setor_cliente_funcao_exame OWNER TO postgres;

--
-- TOC entry 306 (class 1259 OID 19503)
-- Name: seg_ghe_setor_cliente_funcao_id_ghe_setor_cliente_funcao_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.seg_ghe_setor_cliente_funcao_id_ghe_setor_cliente_funcao_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.seg_ghe_setor_cliente_funcao_id_ghe_setor_cliente_funcao_seq OWNER TO postgres;

--
-- TOC entry 4026 (class 0 OID 0)
-- Dependencies: 306
-- Name: seg_ghe_setor_cliente_funcao_id_ghe_setor_cliente_funcao_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.seg_ghe_setor_cliente_funcao_id_ghe_setor_cliente_funcao_seq OWNED BY public.seg_ghe_setor_cliente_funcao.id_ghe_setor_cliente_funcao;


--
-- TOC entry 307 (class 1259 OID 19505)
-- Name: seg_ghe_setor_id_ghe_setor_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.seg_ghe_setor_id_ghe_setor_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.seg_ghe_setor_id_ghe_setor_seq OWNER TO postgres;

--
-- TOC entry 4027 (class 0 OID 0)
-- Dependencies: 307
-- Name: seg_ghe_setor_id_ghe_setor_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.seg_ghe_setor_id_ghe_setor_seq OWNED BY public.seg_ghe_setor.id_ghe_setor;


--
-- TOC entry 308 (class 1259 OID 19507)
-- Name: seg_grad_efeito; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.seg_grad_efeito (
    id_grad_efeito bigint NOT NULL,
    descricao character varying NOT NULL,
    valor bigint NOT NULL,
    categoria character varying
);


ALTER TABLE public.seg_grad_efeito OWNER TO postgres;

--
-- TOC entry 309 (class 1259 OID 19513)
-- Name: seg_grad_efeito_id_grad_efeito_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.seg_grad_efeito_id_grad_efeito_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.seg_grad_efeito_id_grad_efeito_seq OWNER TO postgres;

--
-- TOC entry 4028 (class 0 OID 0)
-- Dependencies: 309
-- Name: seg_grad_efeito_id_grad_efeito_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.seg_grad_efeito_id_grad_efeito_seq OWNED BY public.seg_grad_efeito.id_grad_efeito;


--
-- TOC entry 310 (class 1259 OID 19515)
-- Name: seg_grad_exposicao; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.seg_grad_exposicao (
    id_grad_exposicao bigint NOT NULL,
    descricao character varying NOT NULL,
    categoria character varying NOT NULL,
    valor bigint NOT NULL
);


ALTER TABLE public.seg_grad_exposicao OWNER TO postgres;

--
-- TOC entry 311 (class 1259 OID 19521)
-- Name: seg_grad_exposicao_id_grad_exposicao_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.seg_grad_exposicao_id_grad_exposicao_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.seg_grad_exposicao_id_grad_exposicao_seq OWNER TO postgres;

--
-- TOC entry 4029 (class 0 OID 0)
-- Dependencies: 311
-- Name: seg_grad_exposicao_id_grad_exposicao_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.seg_grad_exposicao_id_grad_exposicao_seq OWNED BY public.seg_grad_exposicao.id_grad_exposicao;


--
-- TOC entry 312 (class 1259 OID 19523)
-- Name: seg_grad_soma; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.seg_grad_soma (
    id_grad_soma bigint NOT NULL,
    faixa bigint NOT NULL,
    med_controle character varying NOT NULL,
    descricao character varying NOT NULL
);


ALTER TABLE public.seg_grad_soma OWNER TO postgres;

--
-- TOC entry 313 (class 1259 OID 19529)
-- Name: seg_grad_soma_id_grad_soma_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.seg_grad_soma_id_grad_soma_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.seg_grad_soma_id_grad_soma_seq OWNER TO postgres;

--
-- TOC entry 4030 (class 0 OID 0)
-- Dependencies: 313
-- Name: seg_grad_soma_id_grad_soma_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.seg_grad_soma_id_grad_soma_seq OWNED BY public.seg_grad_soma.id_grad_soma;


--
-- TOC entry 314 (class 1259 OID 19531)
-- Name: seg_hospital; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.seg_hospital (
    id_hospital bigint NOT NULL,
    descricao character varying(100) NOT NULL,
    endereco character varying(100),
    numero character varying(10),
    complemento character varying(100),
    bairro character varying(50),
    cidade character varying(50),
    cep character varying(8),
    uf character varying(2),
    telefone1 character varying(15),
    telefone2 character varying(15),
    observacao character varying(200),
    situacao character varying(1) NOT NULL,
    id_cidade bigint
);


ALTER TABLE public.seg_hospital OWNER TO postgres;

--
-- TOC entry 315 (class 1259 OID 19537)
-- Name: seg_hospital_id_hospital_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.seg_hospital_id_hospital_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.seg_hospital_id_hospital_seq OWNER TO postgres;

--
-- TOC entry 4031 (class 0 OID 0)
-- Dependencies: 315
-- Name: seg_hospital_id_hospital_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.seg_hospital_id_hospital_seq OWNED BY public.seg_hospital.id_hospital;


--
-- TOC entry 316 (class 1259 OID 19539)
-- Name: seg_itens_cancelados_nf; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.seg_itens_cancelados_nf (
    id_itens_cancelados_nf bigint NOT NULL,
    id_ghe_fonte_agente_exame_aso bigint,
    id_cliente_funcao_exame_aso bigint,
    id_cliente bigint,
    id_produto_contrato bigint,
    id_nfe bigint,
    dt_inclusao date NOT NULL,
    dt_faturamento date,
    situacao character varying(1) NOT NULL,
    prc_unit numeric(8,2) NOT NULL,
    com_valor numeric(8,2),
    id_estudo bigint,
    id_aso bigint,
    quantidade integer NOT NULL,
    usuario character varying(15),
    id_usuario bigint,
    preco_custo numeric(8,2) DEFAULT 0
);


ALTER TABLE public.seg_itens_cancelados_nf OWNER TO postgres;

--
-- TOC entry 317 (class 1259 OID 19543)
-- Name: seg_itens_cancelados_nf_id_itens_cancelados_nf_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.seg_itens_cancelados_nf_id_itens_cancelados_nf_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.seg_itens_cancelados_nf_id_itens_cancelados_nf_seq OWNER TO postgres;

--
-- TOC entry 4032 (class 0 OID 0)
-- Dependencies: 317
-- Name: seg_itens_cancelados_nf_id_itens_cancelados_nf_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.seg_itens_cancelados_nf_id_itens_cancelados_nf_seq OWNED BY public.seg_itens_cancelados_nf.id_itens_cancelados_nf;


--
-- TOC entry 318 (class 1259 OID 19545)
-- Name: seg_log; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.seg_log (
    id_log bigint NOT NULL,
    tabela character varying(30) NOT NULL,
    id_tabela bigint NOT NULL,
    data_alteracao timestamp without time zone NOT NULL,
    atributo character varying(30) NOT NULL,
    valor_anterior character varying(600) NOT NULL,
    valor_alterado character varying(600) NOT NULL,
    usuario character varying(100) NOT NULL
);


ALTER TABLE public.seg_log OWNER TO postgres;

--
-- TOC entry 319 (class 1259 OID 19551)
-- Name: seg_log_id_log_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.seg_log_id_log_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.seg_log_id_log_seq OWNER TO postgres;

--
-- TOC entry 4033 (class 0 OID 0)
-- Dependencies: 319
-- Name: seg_log_id_log_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.seg_log_id_log_seq OWNED BY public.seg_log.id_log;


--
-- TOC entry 403 (class 1259 OID 321665)
-- Name: seg_login_auditoria; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.seg_login_auditoria (
    id_login_auditoria bigint NOT NULL,
    login character varying(30) NOT NULL,
    ip character varying(30) NOT NULL,
    data_alteracao timestamp without time zone NOT NULL,
    situacao character varying
);


ALTER TABLE public.seg_login_auditoria OWNER TO postgres;

--
-- TOC entry 402 (class 1259 OID 321663)
-- Name: seg_login_auditoria_id_login_auditoria_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.seg_login_auditoria_id_login_auditoria_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.seg_login_auditoria_id_login_auditoria_seq OWNER TO postgres;

--
-- TOC entry 4034 (class 0 OID 0)
-- Dependencies: 402
-- Name: seg_login_auditoria_id_login_auditoria_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.seg_login_auditoria_id_login_auditoria_seq OWNED BY public.seg_login_auditoria.id_login_auditoria;


--
-- TOC entry 320 (class 1259 OID 19553)
-- Name: seg_material_hospitalar; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.seg_material_hospitalar (
    id_material_hospitalar bigint NOT NULL,
    descricao character varying(100) NOT NULL,
    unidade character varying(10) NOT NULL,
    situacao character varying(1) NOT NULL
);


ALTER TABLE public.seg_material_hospitalar OWNER TO postgres;

--
-- TOC entry 321 (class 1259 OID 19556)
-- Name: seg_material_hospitalar_estudo; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.seg_material_hospitalar_estudo (
    id_estudo bigint NOT NULL,
    id_material_hospitalar bigint NOT NULL,
    quantidade integer NOT NULL
);


ALTER TABLE public.seg_material_hospitalar_estudo OWNER TO postgres;

--
-- TOC entry 322 (class 1259 OID 19559)
-- Name: seg_material_hospitalar_id_material_hospitalar_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.seg_material_hospitalar_id_material_hospitalar_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.seg_material_hospitalar_id_material_hospitalar_seq OWNER TO postgres;

--
-- TOC entry 4035 (class 0 OID 0)
-- Dependencies: 322
-- Name: seg_material_hospitalar_id_material_hospitalar_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.seg_material_hospitalar_id_material_hospitalar_seq OWNED BY public.seg_material_hospitalar.id_material_hospitalar;


--
-- TOC entry 323 (class 1259 OID 19561)
-- Name: seg_medico; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.seg_medico (
    id_medico bigint NOT NULL,
    id_cidade bigint,
    nome character varying(100) NOT NULL,
    crm character varying(12) NOT NULL,
    situacao character varying(1) NOT NULL,
    telefone1 character varying(15) NOT NULL,
    telefone2 character varying(15),
    email character varying(50),
    endereco character varying(100),
    numero character varying(10),
    complemento character varying(100),
    bairro character varying(50),
    cep character varying(8),
    cidade character varying(50),
    uf character varying(2),
    pis_pasep character varying(11),
    uf_crm character varying(2) NOT NULL,
    documento_extra character varying(15),
    assinatura bytea,
    mimetype character varying(30),
    cpf character varying(11),
    rqe character varying(15)
);


ALTER TABLE public.seg_medico OWNER TO postgres;

--
-- TOC entry 324 (class 1259 OID 19567)
-- Name: seg_medico_exame; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.seg_medico_exame (
    id bigint NOT NULL,
    id_exame bigint NOT NULL,
    id_medico bigint NOT NULL,
    data_gravacao date NOT NULL
);


ALTER TABLE public.seg_medico_exame OWNER TO postgres;

--
-- TOC entry 325 (class 1259 OID 19570)
-- Name: seg_medico_exame_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.seg_medico_exame_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.seg_medico_exame_id_seq OWNER TO postgres;

--
-- TOC entry 4036 (class 0 OID 0)
-- Dependencies: 325
-- Name: seg_medico_exame_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.seg_medico_exame_id_seq OWNED BY public.seg_medico_exame.id;


--
-- TOC entry 326 (class 1259 OID 19572)
-- Name: seg_medico_id_medico_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.seg_medico_id_medico_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.seg_medico_id_medico_seq OWNER TO postgres;

--
-- TOC entry 4037 (class 0 OID 0)
-- Dependencies: 326
-- Name: seg_medico_id_medico_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.seg_medico_id_medico_seq OWNED BY public.seg_medico.id_medico;


--
-- TOC entry 393 (class 1259 OID 245091)
-- Name: seg_monitoramento; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.seg_monitoramento (
    id_monitoramento bigint NOT NULL,
    id_monitoramento_anterior bigint,
    id_cliente_funcionario bigint,
    id_usuario bigint,
    data_inicio_condicao date,
    observacao text,
    id_cliente bigint,
    periodo date,
    id_estudo bigint,
    id_cliente_funcao_funcionario bigint,
    data_fim_condicao date
);


ALTER TABLE public.seg_monitoramento OWNER TO postgres;

--
-- TOC entry 399 (class 1259 OID 245155)
-- Name: seg_monitoramento_ghefonteagente; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.seg_monitoramento_ghefonteagente (
    id_monitoramento_ghefonteagente bigint NOT NULL,
    id_ghe_fonte_agente bigint NOT NULL,
    id_monitoramento bigint NOT NULL,
    tipo_avaliacao character varying(1) NOT NULL,
    intensidade_concentracao numeric(15,4),
    limite_tolerancia numeric(15,4),
    unidade_medicao character varying(2),
    tecnica_medicao text,
    utiliza_epc character varying(1),
    eficacia_epc character varying(1),
    utiliza_epi character varying(1),
    eficacia_epi character varying(1),
    medida_protecao character varying(1),
    condicao_funcionamento character varying(1),
    uso_init character varying(1),
    prazo_validade character varying(1),
    periodicidade_troca character varying(1),
    higienizacao character varying(1),
    numero_contrato_judicial character varying(25),
    numero_processo_judicial character varying(25)
);


ALTER TABLE public.seg_monitoramento_ghefonteagente OWNER TO postgres;

--
-- TOC entry 398 (class 1259 OID 245153)
-- Name: seg_monitoramento_ghefonteage_id_monitoramento_ghefonteagen_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.seg_monitoramento_ghefonteage_id_monitoramento_ghefonteagen_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.seg_monitoramento_ghefonteage_id_monitoramento_ghefonteagen_seq OWNER TO postgres;

--
-- TOC entry 4038 (class 0 OID 0)
-- Dependencies: 398
-- Name: seg_monitoramento_ghefonteage_id_monitoramento_ghefonteagen_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.seg_monitoramento_ghefonteage_id_monitoramento_ghefonteagen_seq OWNED BY public.seg_monitoramento_ghefonteagente.id_monitoramento_ghefonteagente;


--
-- TOC entry 392 (class 1259 OID 245089)
-- Name: seg_monitoramento_id_monitoramento_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.seg_monitoramento_id_monitoramento_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.seg_monitoramento_id_monitoramento_seq OWNER TO postgres;

--
-- TOC entry 4039 (class 0 OID 0)
-- Dependencies: 392
-- Name: seg_monitoramento_id_monitoramento_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.seg_monitoramento_id_monitoramento_seq OWNED BY public.seg_monitoramento.id_monitoramento;


--
-- TOC entry 327 (class 1259 OID 19574)
-- Name: seg_movimento; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.seg_movimento (
    id_movimento bigint NOT NULL,
    id_empresa bigint,
    id_ghe_fonte_agente_exame_aso bigint,
    id_cliente_funcao_exame_aso bigint,
    id_cliente bigint NOT NULL,
    id_produto_contrato bigint,
    id_nfe bigint,
    dt_inclusao timestamp without time zone NOT NULL,
    dt_faturamento timestamp without time zone,
    situacao character varying(1) NOT NULL,
    prc_unit numeric(8,2) NOT NULL,
    com_valor numeric(8,2),
    id_estudo bigint,
    id_aso bigint,
    quantidade integer NOT NULL,
    usuario character varying(15) NOT NULL,
    id_usuario bigint NOT NULL,
    preco_custo numeric(8,2) DEFAULT 0.00 NOT NULL,
    id_cliente_unidade bigint,
    id_protocolo bigint,
    data_gravacao timestamp without time zone NOT NULL,
    id_centrocusto bigint,
    id_contrato_exame bigint,
    id_cliente_credenciada bigint
);


ALTER TABLE public.seg_movimento OWNER TO postgres;

--
-- TOC entry 4040 (class 0 OID 0)
-- Dependencies: 327
-- Name: COLUMN seg_movimento.id_contrato_exame; Type: COMMENT; Schema: public; Owner: postgres
--

-- TOC entry 328 (class 1259 OID 19578)
-- Name: seg_movimento_id_movimento_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.seg_movimento_id_movimento_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.seg_movimento_id_movimento_seq OWNER TO postgres;

--
-- TOC entry 4041 (class 0 OID 0)
-- Dependencies: 328
-- Name: seg_movimento_id_movimento_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.seg_movimento_id_movimento_seq OWNED BY public.seg_movimento.id_movimento;


--
-- TOC entry 329 (class 1259 OID 19580)
-- Name: seg_nfe; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.seg_nfe (
    id_nfe bigint NOT NULL,
    id_usuario_criador bigint NOT NULL,
    id_empresa bigint NOT NULL,
    numero_nfe bigint NOT NULL,
    valor_total_nf numeric(8,2) NOT NULL,
    data_emissao timestamp without time zone NOT NULL,
    valor_pis numeric(8,2),
    valor_cofins numeric(8,2),
    base_calculo numeric(8,2),
    aliquota_iss numeric(8,2) NOT NULL,
    valor_ir numeric(8,2),
    valor_iss numeric(8,2),
    valor_csll numeric(8,2),
    razao_social character varying(100) NOT NULL,
    cnpj character varying(14) NOT NULL,
    inscricao character varying(20) NOT NULL,
    endereco character varying(100) NOT NULL,
    numero character varying(10) NOT NULL,
    complemento character varying(100) NOT NULL,
    bairro character varying(50) NOT NULL,
    cidade character varying(50) NOT NULL,
    cep character varying(8) NOT NULL,
    uf character varying(2) NOT NULL,
    telefone_1 character varying(15),
    email character varying(50),
    data_gravacao timestamp without time zone NOT NULL,
    data_cancelada timestamp without time zone,
    motivo_cancelamento text,
    id_usuario_cancela bigint,
    situacao character varying(1),
    id_plano_forma bigint NOT NULL,
    id_cliente bigint NOT NULL,
    valor_liquido numeric(8,2) DEFAULT 0.00 NOT NULL,
    id_centrocusto bigint
);


ALTER TABLE public.seg_nfe OWNER TO postgres;

--
-- TOC entry 330 (class 1259 OID 19587)
-- Name: seg_nfe_id_nfe_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.seg_nfe_id_nfe_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.seg_nfe_id_nfe_seq OWNER TO postgres;

--
-- TOC entry 4042 (class 0 OID 0)
-- Dependencies: 330
-- Name: seg_nfe_id_nfe_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.seg_nfe_id_nfe_seq OWNED BY public.seg_nfe.id_nfe;


--
-- TOC entry 331 (class 1259 OID 19589)
-- Name: seg_norma; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.seg_norma (
    id_norma bigint NOT NULL,
    descricao character varying(100) NOT NULL,
    situacao character varying(1) NOT NULL,
    conteudo character varying(400) NOT NULL
);


ALTER TABLE public.seg_norma OWNER TO postgres;

--
-- TOC entry 332 (class 1259 OID 19595)
-- Name: seg_norma_ghe; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.seg_norma_ghe (
    id_norma bigint NOT NULL,
    id_ghe bigint NOT NULL
);


ALTER TABLE public.seg_norma_ghe OWNER TO postgres;

--
-- TOC entry 333 (class 1259 OID 19598)
-- Name: seg_norma_id_norma_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.seg_norma_id_norma_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.seg_norma_id_norma_seq OWNER TO postgres;

--
-- TOC entry 4043 (class 0 OID 0)
-- Dependencies: 333
-- Name: seg_norma_id_norma_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.seg_norma_id_norma_seq OWNED BY public.seg_norma.id_norma;


--
-- TOC entry 334 (class 1259 OID 19600)
-- Name: seg_numeronfe_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.seg_numeronfe_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.seg_numeronfe_seq OWNER TO postgres;

--
-- TOC entry 335 (class 1259 OID 19602)
-- Name: seg_parcela; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.seg_parcela (
    id_parcela bigint NOT NULL,
    id_plano bigint NOT NULL,
    dias integer
);


ALTER TABLE public.seg_parcela OWNER TO postgres;

--
-- TOC entry 336 (class 1259 OID 19605)
-- Name: seg_parcela_id_parcela_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.seg_parcela_id_parcela_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.seg_parcela_id_parcela_seq OWNER TO postgres;

--
-- TOC entry 4044 (class 0 OID 0)
-- Dependencies: 336
-- Name: seg_parcela_id_parcela_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.seg_parcela_id_parcela_seq OWNED BY public.seg_parcela.id_parcela;


--
-- TOC entry 337 (class 1259 OID 19607)
-- Name: seg_perfil; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.seg_perfil (
    id_perfil bigint NOT NULL,
    descricao character varying(100) NOT NULL,
    situacao character varying(1) NOT NULL
);


ALTER TABLE public.seg_perfil OWNER TO postgres;

--
-- TOC entry 338 (class 1259 OID 19610)
-- Name: seg_perfil_dominio_acao; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.seg_perfil_dominio_acao (
    id_perfil bigint NOT NULL,
    id_dominio bigint NOT NULL,
    id_acao bigint NOT NULL
);


ALTER TABLE public.seg_perfil_dominio_acao OWNER TO postgres;

--
-- TOC entry 339 (class 1259 OID 19613)
-- Name: seg_perfil_id_perfil_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.seg_perfil_id_perfil_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.seg_perfil_id_perfil_seq OWNER TO postgres;

--
-- TOC entry 4045 (class 0 OID 0)
-- Dependencies: 339
-- Name: seg_perfil_id_perfil_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.seg_perfil_id_perfil_seq OWNED BY public.seg_perfil.id_perfil;


--
-- TOC entry 340 (class 1259 OID 19615)
-- Name: seg_periodicidade; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.seg_periodicidade (
    id_periodicidade bigint NOT NULL,
    descricao character varying(100) NOT NULL,
    ind_periodico boolean DEFAULT false NOT NULL,
    dias integer
);


ALTER TABLE public.seg_periodicidade OWNER TO postgres;

--
-- TOC entry 341 (class 1259 OID 19619)
-- Name: seg_periodicidade_id_periodicidade_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.seg_periodicidade_id_periodicidade_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.seg_periodicidade_id_periodicidade_seq OWNER TO postgres;

--
-- TOC entry 4046 (class 0 OID 0)
-- Dependencies: 341
-- Name: seg_periodicidade_id_periodicidade_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.seg_periodicidade_id_periodicidade_seq OWNED BY public.seg_periodicidade.id_periodicidade;


--
-- TOC entry 342 (class 1259 OID 19621)
-- Name: seg_plano; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.seg_plano (
    id_plano bigint NOT NULL,
    descricao character varying(100) NOT NULL,
    situacao character varying(1) NOT NULL,
    carga boolean NOT NULL
);


ALTER TABLE public.seg_plano OWNER TO postgres;

--
-- TOC entry 343 (class 1259 OID 19624)
-- Name: seg_plano_forma; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.seg_plano_forma (
    id_plano_forma bigint NOT NULL,
    id_forma bigint NOT NULL,
    id_plano bigint NOT NULL,
    situacao character varying(1) NOT NULL
);


ALTER TABLE public.seg_plano_forma OWNER TO postgres;

--
-- TOC entry 344 (class 1259 OID 19627)
-- Name: seg_plano_forma_id_plano_forma_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.seg_plano_forma_id_plano_forma_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.seg_plano_forma_id_plano_forma_seq OWNER TO postgres;

--
-- TOC entry 4047 (class 0 OID 0)
-- Dependencies: 344
-- Name: seg_plano_forma_id_plano_forma_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.seg_plano_forma_id_plano_forma_seq OWNED BY public.seg_plano_forma.id_plano_forma;


--
-- TOC entry 345 (class 1259 OID 19629)
-- Name: seg_plano_id_plano_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.seg_plano_id_plano_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.seg_plano_id_plano_seq OWNER TO postgres;

--
-- TOC entry 4048 (class 0 OID 0)
-- Dependencies: 345
-- Name: seg_plano_id_plano_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.seg_plano_id_plano_seq OWNED BY public.seg_plano.id_plano;


--
-- TOC entry 391 (class 1259 OID 228723)
-- Name: seg_procedimento_esocial; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.seg_procedimento_esocial (
    id bigint NOT NULL,
    codigo character varying(4) NOT NULL,
    procedimento text NOT NULL
);


ALTER TABLE public.seg_procedimento_esocial OWNER TO postgres;

--
-- TOC entry 390 (class 1259 OID 228721)
-- Name: seg_procedimento_esocial_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.seg_procedimento_esocial_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.seg_procedimento_esocial_id_seq OWNER TO postgres;

--
-- TOC entry 4049 (class 0 OID 0)
-- Dependencies: 390
-- Name: seg_procedimento_esocial_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.seg_procedimento_esocial_id_seq OWNED BY public.seg_procedimento_esocial.id;


--
-- TOC entry 346 (class 1259 OID 19673)
-- Name: seg_produto; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.seg_produto (
    id_produto bigint NOT NULL,
    nome character varying(100) NOT NULL,
    situacao character varying(1) NOT NULL,
    preco numeric(8,2) NOT NULL,
    tipo character varying(1) NOT NULL,
    custo numeric(8,2),
    tipo_estudo character varying(2),
    padrao_contrato boolean DEFAULT false NOT NULL,
    descricao text
);


ALTER TABLE public.seg_produto OWNER TO postgres;

--
-- TOC entry 347 (class 1259 OID 19676)
-- Name: seg_produto_contrato; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.seg_produto_contrato (
    id_produto_contrato bigint NOT NULL,
    id_produto bigint NOT NULL,
    id_contrato bigint NOT NULL,
    preco_contratado numeric(8,2) NOT NULL,
    usuario character varying(15) NOT NULL,
    custo_contrato numeric(8,2) NOT NULL,
    data_inclusao timestamp without time zone,
    situacao character varying(1) DEFAULT 'A'::character varying NOT NULL,
    altera boolean DEFAULT true NOT NULL
);


ALTER TABLE public.seg_produto_contrato OWNER TO postgres;

--
-- TOC entry 348 (class 1259 OID 19681)
-- Name: seg_produto_contrato_id_produto_contrato_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.seg_produto_contrato_id_produto_contrato_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.seg_produto_contrato_id_produto_contrato_seq OWNER TO postgres;

--
-- TOC entry 4050 (class 0 OID 0)
-- Dependencies: 348
-- Name: seg_produto_contrato_id_produto_contrato_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.seg_produto_contrato_id_produto_contrato_seq OWNED BY public.seg_produto_contrato.id_produto_contrato;


--
-- TOC entry 349 (class 1259 OID 19683)
-- Name: seg_produto_id_produto_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.seg_produto_id_produto_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.seg_produto_id_produto_seq OWNER TO postgres;

--
-- TOC entry 4051 (class 0 OID 0)
-- Dependencies: 349
-- Name: seg_produto_id_produto_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.seg_produto_id_produto_seq OWNED BY public.seg_produto.id_produto;


--
-- TOC entry 350 (class 1259 OID 19685)
-- Name: seg_prontuario; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.seg_prontuario (
    id_prontuario bigint NOT NULL,
    id_usuario bigint NOT NULL,
    id_aso bigint NOT NULL,
    pressao character varying(15),
    frequencia character varying(15),
    peso numeric(8,2),
    altura numeric(8,2),
    lassegue character varying(1),
    bebida boolean DEFAULT false,
    fumo boolean DEFAULT false,
    diarreia boolean DEFAULT false,
    tosse boolean DEFAULT false,
    afastado boolean DEFAULT false,
    convulsao boolean DEFAULT false,
    hepatite boolean DEFAULT false,
    acidente boolean DEFAULT false,
    tuberculose boolean DEFAULT false,
    antitetanica boolean DEFAULT false,
    atividade_fisica boolean DEFAULT false,
    medo_altura boolean DEFAULT false,
    dores_juntas boolean DEFAULT false,
    palpitacao boolean DEFAULT false,
    tonturas boolean DEFAULT false,
    azia boolean DEFAULT false,
    desmaio boolean DEFAULT false,
    dores_cabeca boolean DEFAULT false,
    epilepsia boolean DEFAULT false,
    insonia boolean DEFAULT false,
    problema_audicao boolean DEFAULT false,
    tendinite boolean DEFAULT false,
    fratura boolean DEFAULT false,
    alergia boolean DEFAULT false,
    asma boolean DEFAULT false,
    diabete boolean DEFAULT false,
    hernia boolean DEFAULT false,
    manchas_pele boolean DEFAULT false,
    pressao_alta boolean DEFAULT false,
    problema_visao boolean DEFAULT false,
    problema_coluna boolean DEFAULT false,
    rinite boolean DEFAULT false,
    varizes boolean DEFAULT false,
    oculos boolean DEFAULT false,
    aparelho_mucosa character varying(1) DEFAULT 'N'::character varying,
    aparelho_mucosa_obs character varying(100),
    aparelho_vascular character varying(1) DEFAULT 'N'::character varying,
    aparelho_vascular_obs character varying(100),
    aparelho_respiratorio character varying(1) DEFAULT 'N'::character varying,
    aparelho_respiratorio_obs character varying(100),
    aparelho_olho_dir character varying(1) DEFAULT 'N'::character varying,
    aparelho_olho_dir_obs character varying(100),
    aparelho_olho_esq character varying(1) DEFAULT 'N'::character varying,
    aparelho_olho_esq_obs character varying(100),
    aparelho_protese character varying(1) DEFAULT 'N'::character varying,
    aparelho_protese_obs character varying(100),
    aparelho_ganglio character varying(1) DEFAULT 'N'::character varying,
    aparelho_ganglio_obs character varying(100),
    aparelho_abdomen character varying(1) DEFAULT 'N'::character varying,
    aparelho_abdomen_obs character varying(100),
    aparelho_urina character varying(1) DEFAULT 'N'::character varying,
    aparelho_urina_obs character varying(100),
    aparelho_membro character varying(1) DEFAULT 'N'::character varying,
    aparelho_membro_obs character varying(100),
    aparelho_coluna character varying(1) DEFAULT 'N'::character varying,
    aparelho_coluna_obs character varying(100),
    aparelho_equilibrio character varying(1) DEFAULT 'N'::character varying,
    aparelho_equilibrio_obs character varying(100),
    aparelho_forca character varying(1) DEFAULT 'N'::character varying,
    aparelho_forca_obs character varying(100),
    aparelho_motor character varying(1) DEFAULT 'N'::character varying,
    aparelho_motor_obs character varying(100),
    parto integer,
    metodo character varying(20),
    data_ult_menstruacao date,
    previ_afastado boolean DEFAULT false,
    previ_recurso boolean DEFAULT false,
    data_afastamento date,
    causa text,
    laudo character varying(60),
    laudo_comentario text,
    metodo_obs text,
    observacao_aparelho text
);


ALTER TABLE public.seg_prontuario OWNER TO postgres;

--
-- TOC entry 351 (class 1259 OID 19741)
-- Name: seg_prontuario_id_prontuario_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.seg_prontuario_id_prontuario_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.seg_prontuario_id_prontuario_seq OWNER TO postgres;

--
-- TOC entry 4052 (class 0 OID 0)
-- Dependencies: 351
-- Name: seg_prontuario_id_prontuario_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.seg_prontuario_id_prontuario_seq OWNED BY public.seg_prontuario.id_prontuario;


--
-- TOC entry 352 (class 1259 OID 19743)
-- Name: seg_protocolo; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.seg_protocolo (
    id_protocolo bigint NOT NULL,
    numero character varying(12) NOT NULL,
    documento_responsavel character varying(20),
    situacao character varying(1) NOT NULL,
    data_gravacao timestamp without time zone NOT NULL,
    id_usuario_gerou bigint NOT NULL,
    data_cancelamento timestamp without time zone,
    id_usuario_cancelou bigint,
    enviado character varying(100),
    data_finalizacao timestamp without time zone,
    id_usuario_finalizou bigint,
    observacao text,
    motivo_cancelamento text,
    id_cliente bigint NOT NULL
);


ALTER TABLE public.seg_protocolo OWNER TO postgres;

--
-- TOC entry 353 (class 1259 OID 19749)
-- Name: seg_protocolo_id_protocolo_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.seg_protocolo_id_protocolo_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.seg_protocolo_id_protocolo_seq OWNER TO postgres;

--
-- TOC entry 4053 (class 0 OID 0)
-- Dependencies: 353
-- Name: seg_protocolo_id_protocolo_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.seg_protocolo_id_protocolo_seq OWNED BY public.seg_protocolo.id_protocolo;


--
-- TOC entry 354 (class 1259 OID 19751)
-- Name: seg_protocolo_item; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.seg_protocolo_item (
    id_item bigint NOT NULL,
    id_protocolo bigint NOT NULL,
    codigo_item character varying NOT NULL,
    cliente character varying NOT NULL,
    colaborador character varying,
    rg character varying,
    cpf character varying,
    periodicidade character varying NOT NULL,
    descricao_item character varying NOT NULL,
    data_item date NOT NULL,
    laudo character varying,
    tipo character varying NOT NULL
);


ALTER TABLE public.seg_protocolo_item OWNER TO postgres;

--
-- TOC entry 355 (class 1259 OID 19757)
-- Name: seg_ramo; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.seg_ramo (
    id bigint NOT NULL,
    descricao character varying(100) NOT NULL,
    situacao character varying(1) NOT NULL
);


ALTER TABLE public.seg_ramo OWNER TO postgres;

--
-- TOC entry 356 (class 1259 OID 19760)
-- Name: seg_ramo_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.seg_ramo_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.seg_ramo_id_seq OWNER TO postgres;

--
-- TOC entry 4054 (class 0 OID 0)
-- Dependencies: 356
-- Name: seg_ramo_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.seg_ramo_id_seq OWNED BY public.seg_ramo.id;


--
-- TOC entry 357 (class 1259 OID 19762)
-- Name: seg_relanual; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.seg_relanual (
    id_relanual bigint NOT NULL,
    id_usuario bigint NOT NULL,
    id_medico_coordenador bigint NOT NULL,
    id_cliente bigint NOT NULL,
    data_inicial date NOT NULL,
    data_final date NOT NULL,
    data_geracao timestamp without time zone NOT NULL
);


ALTER TABLE public.seg_relanual OWNER TO postgres;

--
-- TOC entry 358 (class 1259 OID 19765)
-- Name: seg_relanual_id_relanual_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.seg_relanual_id_relanual_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.seg_relanual_id_relanual_seq OWNER TO postgres;

--
-- TOC entry 4055 (class 0 OID 0)
-- Dependencies: 358
-- Name: seg_relanual_id_relanual_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.seg_relanual_id_relanual_seq OWNED BY public.seg_relanual.id_relanual;


--
-- TOC entry 359 (class 1259 OID 19767)
-- Name: seg_relanual_item; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.seg_relanual_item (
    id_relanual_item bigint NOT NULL,
    id_relanual bigint NOT NULL,
    id_setor bigint NOT NULL,
    descricao_setor character varying(100) NOT NULL,
    id_exame bigint NOT NULL,
    descricao_exame character varying(100) NOT NULL,
    total_exame_normal integer NOT NULL,
    total_exame_anormal integer NOT NULL,
    previsao integer NOT NULL,
    id_periodicidade bigint NOT NULL,
    descricao_periodicidade character varying(100) NOT NULL
);


ALTER TABLE public.seg_relanual_item OWNER TO postgres;

--
-- TOC entry 360 (class 1259 OID 19770)
-- Name: seg_relanual_item_id_relanual_item_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.seg_relanual_item_id_relanual_item_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.seg_relanual_item_id_relanual_item_seq OWNER TO postgres;

--
-- TOC entry 4056 (class 0 OID 0)
-- Dependencies: 360
-- Name: seg_relanual_item_id_relanual_item_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.seg_relanual_item_id_relanual_item_seq OWNED BY public.seg_relanual_item.id_relanual_item;


--
-- TOC entry 361 (class 1259 OID 19772)
-- Name: seg_relatorio; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.seg_relatorio (
    id_relatorio bigint NOT NULL,
    nome_relatorio character varying(50),
    tipo character varying(2),
    data_criacao date,
    situacao character varying(1),
    nmrelatorio character varying(50)
);


ALTER TABLE public.seg_relatorio OWNER TO postgres;

--
-- TOC entry 389 (class 1259 OID 21595)
-- Name: seg_relatorio_exame; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.seg_relatorio_exame (
    id_relatorio_exame bigint NOT NULL,
    id_exame bigint NOT NULL,
    id_relatorio bigint NOT NULL,
    situacao character varying(1) NOT NULL,
    data_gravacao date NOT NULL
);


ALTER TABLE public.seg_relatorio_exame OWNER TO postgres;

--
-- TOC entry 388 (class 1259 OID 21593)
-- Name: seg_relatorio_exame_id_relatorio_exame_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.seg_relatorio_exame_id_relatorio_exame_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.seg_relatorio_exame_id_relatorio_exame_seq OWNER TO postgres;

--
-- TOC entry 4057 (class 0 OID 0)
-- Dependencies: 388
-- Name: seg_relatorio_exame_id_relatorio_exame_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.seg_relatorio_exame_id_relatorio_exame_seq OWNED BY public.seg_relatorio_exame.id_relatorio_exame;


--
-- TOC entry 362 (class 1259 OID 19775)
-- Name: seg_relatorio_id_relatorio_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.seg_relatorio_id_relatorio_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.seg_relatorio_id_relatorio_seq OWNER TO postgres;

--
-- TOC entry 4058 (class 0 OID 0)
-- Dependencies: 362
-- Name: seg_relatorio_id_relatorio_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.seg_relatorio_id_relatorio_seq OWNED BY public.seg_relatorio.id_relatorio;


--
-- TOC entry 363 (class 1259 OID 19777)
-- Name: seg_rep_cli; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.seg_rep_cli (
    id_seg_rep_cli bigint NOT NULL,
    id_cliente bigint NOT NULL,
    id_vend bigint NOT NULL,
    situacao character varying(1) NOT NULL
);


ALTER TABLE public.seg_rep_cli OWNER TO postgres;

--
-- TOC entry 364 (class 1259 OID 19780)
-- Name: seg_rep_cli_id_seg_rep_cli_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.seg_rep_cli_id_seg_rep_cli_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.seg_rep_cli_id_seg_rep_cli_seq OWNER TO postgres;

--
-- TOC entry 4059 (class 0 OID 0)
-- Dependencies: 364
-- Name: seg_rep_cli_id_seg_rep_cli_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.seg_rep_cli_id_seg_rep_cli_seq OWNED BY public.seg_rep_cli.id_seg_rep_cli;


--
-- TOC entry 365 (class 1259 OID 19787)
-- Name: seg_revisao; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.seg_revisao (
    id_revisao bigint NOT NULL,
    id_estudo bigint NOT NULL,
    comentario character varying(255) NOT NULL,
    id_estudo_raiz bigint NOT NULL,
    id_estudo_revisao bigint,
    tipo_revisao character varying(6),
    versao_estudo character varying(3),
    data_revisao date,
    ind_correcao boolean NOT NULL
);


ALTER TABLE public.seg_revisao OWNER TO postgres;

--
-- TOC entry 366 (class 1259 OID 19790)
-- Name: seg_revisao_id_revisao_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.seg_revisao_id_revisao_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.seg_revisao_id_revisao_seq OWNER TO postgres;

--
-- TOC entry 4060 (class 0 OID 0)
-- Dependencies: 366
-- Name: seg_revisao_id_revisao_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.seg_revisao_id_revisao_seq OWNED BY public.seg_revisao.id_revisao;


--
-- TOC entry 367 (class 1259 OID 19792)
-- Name: seg_risco; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.seg_risco (
    id_risco bigint NOT NULL,
    descricao character varying(20) NOT NULL
);


ALTER TABLE public.seg_risco OWNER TO postgres;

--
-- TOC entry 368 (class 1259 OID 19795)
-- Name: seg_risco_id_risco_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.seg_risco_id_risco_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.seg_risco_id_risco_seq OWNER TO postgres;

--
-- TOC entry 4061 (class 0 OID 0)
-- Dependencies: 368
-- Name: seg_risco_id_risco_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.seg_risco_id_risco_seq OWNED BY public.seg_risco.id_risco;


--
-- TOC entry 369 (class 1259 OID 19797)
-- Name: seg_sala; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.seg_sala (
    id_sala bigint NOT NULL,
    descricao character varying(20) NOT NULL,
    situacao character varying(1) NOT NULL,
    andar integer NOT NULL,
    vip boolean NOT NULL,
    id_empresa bigint DEFAULT 1 NOT NULL,
    slug_sala character varying(20),
    qtde_chamada integer DEFAULT 1 NOT NULL
);


ALTER TABLE public.seg_sala OWNER TO postgres;

--
-- TOC entry 370 (class 1259 OID 19800)
-- Name: seg_sala_exame; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.seg_sala_exame (
    id_sala_exame bigint NOT NULL,
    id_exame bigint NOT NULL,
    id_sala bigint NOT NULL,
    tempo_medio_atendimento integer NOT NULL,
    tempo_medio_atendimento_calculado integer,
    situacao character varying(1) NOT NULL,
    liberada boolean DEFAULT true NOT NULL
);


ALTER TABLE public.seg_sala_exame OWNER TO postgres;

--
-- TOC entry 371 (class 1259 OID 19804)
-- Name: seg_sala_exame_id_sala_exame_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.seg_sala_exame_id_sala_exame_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.seg_sala_exame_id_sala_exame_seq OWNER TO postgres;

--
-- TOC entry 4062 (class 0 OID 0)
-- Dependencies: 371
-- Name: seg_sala_exame_id_sala_exame_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.seg_sala_exame_id_sala_exame_seq OWNED BY public.seg_sala_exame.id_sala_exame;


--
-- TOC entry 372 (class 1259 OID 19806)
-- Name: seg_sala_id_sala_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.seg_sala_id_sala_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.seg_sala_id_sala_seq OWNER TO postgres;

--
-- TOC entry 4063 (class 0 OID 0)
-- Dependencies: 372
-- Name: seg_sala_id_sala_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.seg_sala_id_sala_seq OWNED BY public.seg_sala.id_sala;


--
-- TOC entry 373 (class 1259 OID 19808)
-- Name: seg_setor; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.seg_setor (
    id_setor bigint NOT NULL,
    descricao character varying(100) NOT NULL,
    situacao character varying(1)
);


ALTER TABLE public.seg_setor OWNER TO postgres;

--
-- TOC entry 374 (class 1259 OID 19811)
-- Name: seg_setor_id_setor_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.seg_setor_id_setor_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.seg_setor_id_setor_seq OWNER TO postgres;

--
-- TOC entry 4064 (class 0 OID 0)
-- Dependencies: 374
-- Name: seg_setor_id_setor_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.seg_setor_id_setor_seq OWNED BY public.seg_setor.id_setor;


--
-- TOC entry 375 (class 1259 OID 19813)
-- Name: seg_tipo_contato; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.seg_tipo_contato (
    id bigint NOT NULL,
    descricao character varying(100)
);


ALTER TABLE public.seg_tipo_contato OWNER TO postgres;

--
-- TOC entry 376 (class 1259 OID 19816)
-- Name: seg_tipo_contato_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.seg_tipo_contato_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.seg_tipo_contato_id_seq OWNER TO postgres;

--
-- TOC entry 4065 (class 0 OID 0)
-- Dependencies: 376
-- Name: seg_tipo_contato_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.seg_tipo_contato_id_seq OWNED BY public.seg_tipo_contato.id;


--
-- TOC entry 377 (class 1259 OID 19818)
-- Name: seg_usuario; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.seg_usuario (
    id_usuario bigint NOT NULL,
    nome character varying(100) NOT NULL,
    cpf character varying(11) NOT NULL,
    rg character varying(15),
    doc_extra character varying(20),
    data_admissao date,
    situacao character varying(1) NOT NULL,
    login character varying(15) NOT NULL,
    senha character varying(255) NOT NULL,
    email character varying(50),
    id_medico bigint,
    id_empresa bigint,
    mte character varying(15),
    elabora_documento boolean DEFAULT false NOT NULL,
    assinatura bytea,
    mimetype character varying(30),
    externo boolean DEFAULT false NOT NULL,
    orgao_emissor character varying(10),
    uf_orgao_emissor character varying(2)
);


ALTER TABLE public.seg_usuario OWNER TO postgres;

--
-- TOC entry 378 (class 1259 OID 19827)
-- Name: seg_usuario_cliente; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.seg_usuario_cliente (
    id bigint NOT NULL,
    id_cliente bigint NOT NULL,
    id_usuario bigint NOT NULL,
    data_inclusao date NOT NULL
);


ALTER TABLE public.seg_usuario_cliente OWNER TO postgres;

--
-- TOC entry 379 (class 1259 OID 19830)
-- Name: seg_usuario_cliente_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.seg_usuario_cliente_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.seg_usuario_cliente_id_seq OWNER TO postgres;

--
-- TOC entry 4066 (class 0 OID 0)
-- Dependencies: 379
-- Name: seg_usuario_cliente_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.seg_usuario_cliente_id_seq OWNED BY public.seg_usuario_cliente.id;


--
-- TOC entry 380 (class 1259 OID 19832)
-- Name: seg_usuario_funcao_interna; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.seg_usuario_funcao_interna (
    id_usuario_funcao_interna bigint NOT NULL,
    id_funcao_interna bigint NOT NULL,
    id_usuario bigint NOT NULL,
    situacao character varying(1) NOT NULL,
    dt_inicio date NOT NULL,
    dt_termino date
);


ALTER TABLE public.seg_usuario_funcao_interna OWNER TO postgres;

--
-- TOC entry 381 (class 1259 OID 19835)
-- Name: seg_usuario_funcao_interna_id_usuario_funcao_interna_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.seg_usuario_funcao_interna_id_usuario_funcao_interna_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.seg_usuario_funcao_interna_id_usuario_funcao_interna_seq OWNER TO postgres;

--
-- TOC entry 4067 (class 0 OID 0)
-- Dependencies: 381
-- Name: seg_usuario_funcao_interna_id_usuario_funcao_interna_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.seg_usuario_funcao_interna_id_usuario_funcao_interna_seq OWNED BY public.seg_usuario_funcao_interna.id_usuario_funcao_interna;


--
-- TOC entry 382 (class 1259 OID 19837)
-- Name: seg_usuario_id_usuario_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.seg_usuario_id_usuario_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.seg_usuario_id_usuario_seq OWNER TO postgres;

--
-- TOC entry 4068 (class 0 OID 0)
-- Dependencies: 382
-- Name: seg_usuario_id_usuario_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.seg_usuario_id_usuario_seq OWNED BY public.seg_usuario.id_usuario;


--
-- TOC entry 383 (class 1259 OID 19839)
-- Name: seg_usuario_perfil; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.seg_usuario_perfil (
    id_usuario bigint NOT NULL,
    id_perfil bigint NOT NULL,
    data_inicio date
);


ALTER TABLE public.seg_usuario_perfil OWNER TO postgres;

--
-- TOC entry 384 (class 1259 OID 19842)
-- Name: seg_vendedor; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.seg_vendedor (
    id_vend bigint NOT NULL,
    nome character varying(100) NOT NULL,
    cpf character varying(11) NOT NULL,
    rg character varying(15) NOT NULL,
    endereco character varying(100),
    numero character varying(10),
    complemento character varying(100),
    bairro character varying(50),
    cidade character varying(50),
    uf character varying(2) NOT NULL,
    cep character varying(8),
    telefone1 character varying(15),
    telefone2 character varying(15),
    email character varying(50),
    comissao numeric(3,2) NOT NULL,
    data_cadastro date NOT NULL,
    situacao character varying(1),
    id_cidade_ibge bigint NOT NULL
);


ALTER TABLE public.seg_vendedor OWNER TO postgres;

--
-- TOC entry 385 (class 1259 OID 19848)
-- Name: seg_vendedor_id_vend_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.seg_vendedor_id_vend_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.seg_vendedor_id_vend_seq OWNER TO postgres;

--
-- TOC entry 4069 (class 0 OID 0)
-- Dependencies: 385
-- Name: seg_vendedor_id_vend_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.seg_vendedor_id_vend_seq OWNED BY public.seg_vendedor.id_vend;


--
-- TOC entry 386 (class 1259 OID 19850)
-- Name: seg_versao; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.seg_versao (
    id_versao bigint NOT NULL,
    id_estudo bigint NOT NULL,
    numero_revisao character varying NOT NULL,
    comentario character varying NOT NULL,
    data_cricao date NOT NULL
);


ALTER TABLE public.seg_versao OWNER TO postgres;

--
-- TOC entry 387 (class 1259 OID 19856)
-- Name: seg_versao_id_versao_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.seg_versao_id_versao_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.seg_versao_id_versao_seq OWNER TO postgres;

--
-- TOC entry 4070 (class 0 OID 0)
-- Dependencies: 387
-- Name: seg_versao_id_versao_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.seg_versao_id_versao_seq OWNED BY public.seg_versao.id_versao;


--
-- TOC entry 2774 (class 2604 OID 19858)
-- Name: seg_acao id_acao; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_acao ALTER COLUMN id_acao SET DEFAULT nextval('public.seg_acao_id_acao_seq'::regclass);


--
-- TOC entry 2775 (class 2604 OID 19859)
-- Name: seg_acesso id_acesso; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_acesso ALTER COLUMN id_acesso SET DEFAULT nextval('public.seg_acesso_id_acesso_seq'::regclass);


--
-- TOC entry 2776 (class 2604 OID 112434)
-- Name: seg_acompanhamento_atendimento id_acompanhamento_atendimento; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_acompanhamento_atendimento ALTER COLUMN id_acompanhamento_atendimento SET DEFAULT nextval('public.seg_acompanhamento_atendiment_id_acompanhamento_atendimento_seq'::regclass);


--
-- TOC entry 2778 (class 2604 OID 19861)
-- Name: seg_agente id_agente; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_agente ALTER COLUMN id_agente SET DEFAULT nextval('public.seg_agente_id_agente_seq'::regclass);


--
-- TOC entry 2779 (class 2604 OID 19863)
-- Name: seg_arquivo id_arquivo; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_arquivo ALTER COLUMN id_arquivo SET DEFAULT nextval('public.seg_arquivo_id_arquivo_seq'::regclass);


--
-- TOC entry 2780 (class 2604 OID 19864)
-- Name: seg_arquivo_anexo id_arquivo; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_arquivo_anexo ALTER COLUMN id_arquivo SET DEFAULT nextval('public.seg_arquivo_anexo_id_arquivo_seq'::regclass);


--
-- TOC entry 2781 (class 2604 OID 19865)
-- Name: seg_aso id_aso; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_aso ALTER COLUMN id_aso SET DEFAULT nextval('public.seg_aso_id_aso_seq'::regclass);


--
-- TOC entry 2786 (class 2604 OID 19866)
-- Name: seg_atividade id_atividade; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_atividade ALTER COLUMN id_atividade SET DEFAULT nextval('public.seg_atividade_id_atividade_seq'::regclass);


--
-- TOC entry 2787 (class 2604 OID 19867)
-- Name: seg_banco id_banco; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_banco ALTER COLUMN id_banco SET DEFAULT nextval('public.seg_banco_id_banco_seq'::regclass);


--
-- TOC entry 2788 (class 2604 OID 19869)
-- Name: seg_centrocusto id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_centrocusto ALTER COLUMN id SET DEFAULT nextval('public.seg_centrocusto_id_seq'::regclass);


--
-- TOC entry 2790 (class 2604 OID 19870)
-- Name: seg_cidade_ibge id_cidade_ibge; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_cidade_ibge ALTER COLUMN id_cidade_ibge SET DEFAULT nextval('public.seg_cidade_ibge_id_cidade_ibge_seq'::regclass);


--
-- TOC entry 2803 (class 2604 OID 19871)
-- Name: seg_cliente id_cliente; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_cliente ALTER COLUMN id_cliente SET DEFAULT nextval('public.seg_cliente_id_cliente_seq'::regclass);


--
-- TOC entry 2807 (class 2604 OID 19872)
-- Name: seg_cliente_arquivo id_cliente_arquivo; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_cliente_arquivo ALTER COLUMN id_cliente_arquivo SET DEFAULT nextval('public.seg_cliente_arquivo_id_cliente_arquivo_seq'::regclass);


--
-- TOC entry 2808 (class 2604 OID 19873)
-- Name: seg_cliente_funcao id_seg_cliente_funcao; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_cliente_funcao ALTER COLUMN id_seg_cliente_funcao SET DEFAULT nextval('public.seg_cliente_funcao_id_seg_cliente_funcao_seq'::regclass);


--
-- TOC entry 2809 (class 2604 OID 19874)
-- Name: seg_cliente_funcao_exame id_cliente_funcao_exame; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_cliente_funcao_exame ALTER COLUMN id_cliente_funcao_exame SET DEFAULT nextval('public.seg_cliente_funcao_exame_id_cliente_funcao_exame_seq'::regclass);


--
-- TOC entry 2816 (class 2604 OID 19875)
-- Name: seg_cliente_funcao_exame_aso id_cliente_funcao_exame_aso; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_cliente_funcao_exame_aso ALTER COLUMN id_cliente_funcao_exame_aso SET DEFAULT nextval('public.seg_cliente_funcao_exame_aso_id_cliente_funcao_exame_aso_seq'::regclass);


--
-- TOC entry 2819 (class 2604 OID 19876)
-- Name: seg_cliente_funcao_funcionario id_cliente_funcao_funcionario; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_cliente_funcao_funcionario ALTER COLUMN id_cliente_funcao_funcionario SET DEFAULT nextval('public.seg_cliente_funcao_funcionari_id_cliente_funcao_funcionario_seq'::regclass);


--
-- TOC entry 2824 (class 2604 OID 19877)
-- Name: seg_cliente_funcionario id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_cliente_funcionario ALTER COLUMN id SET DEFAULT nextval('public.seg_cliente_funcionario_id_seq'::regclass);


--
-- TOC entry 2825 (class 2604 OID 19878)
-- Name: seg_cliente_proposta id_cliente_proposta; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_cliente_proposta ALTER COLUMN id_cliente_proposta SET DEFAULT nextval('public.seg_cliente_proposta_id_cliente_proposta_seq'::regclass);


--
-- TOC entry 2826 (class 2604 OID 19879)
-- Name: seg_cnae id_cnae; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_cnae ALTER COLUMN id_cnae SET DEFAULT nextval('public.seg_cnae_id_cnae_seq'::regclass);


--
-- TOC entry 2828 (class 2604 OID 19880)
-- Name: seg_cnae_estudo id_cnae_estudo; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_cnae_estudo ALTER COLUMN id_cnae_estudo SET DEFAULT nextval('public.seg_cnae_estudo_id_cnae_estudo_seq'::regclass);


--
-- TOC entry 2830 (class 2604 OID 19881)
-- Name: seg_comentario_funcao id_comentario_funcao; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_comentario_funcao ALTER COLUMN id_comentario_funcao SET DEFAULT nextval('public.seg_comentario_funcao_id_comentario_funcao_seq'::regclass);


--
-- TOC entry 2831 (class 2604 OID 19882)
-- Name: seg_configuracao id_configuracao; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_configuracao ALTER COLUMN id_configuracao SET DEFAULT nextval('public.seg_configuracao_id_configuracao_seq'::regclass);


--
-- TOC entry 2832 (class 2604 OID 19883)
-- Name: seg_conta id_conta; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_conta ALTER COLUMN id_conta SET DEFAULT nextval('public.seg_conta_id_conta_seq'::regclass);


--
-- TOC entry 2834 (class 2604 OID 19884)
-- Name: seg_contato id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_contato ALTER COLUMN id SET DEFAULT nextval('public.seg_contato_id_seq'::regclass);


--
-- TOC entry 2844 (class 2604 OID 19885)
-- Name: seg_contrato id_contrato; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_contrato ALTER COLUMN id_contrato SET DEFAULT nextval('public.seg_contrato_id_contrato_seq'::regclass);


--
-- TOC entry 2847 (class 2604 OID 19886)
-- Name: seg_contrato_exame id_contrato_exame; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_contrato_exame ALTER COLUMN id_contrato_exame SET DEFAULT nextval('public.seg_contrato_exame_id_contrato_exame_seq'::regclass);


--
-- TOC entry 2848 (class 2604 OID 19887)
-- Name: seg_correcao_estudo id_correcao_estudo; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_correcao_estudo ALTER COLUMN id_correcao_estudo SET DEFAULT nextval('public.seg_correcao_estudo_id_correcao_estudo_seq'::regclass);


--
-- TOC entry 2849 (class 2604 OID 19888)
-- Name: seg_crc id_crc; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_crc ALTER COLUMN id_crc SET DEFAULT nextval('public.seg_crc_id_crc_seq'::regclass);


--
-- TOC entry 2851 (class 2604 OID 19889)
-- Name: seg_cronograma id_cronograma; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_cronograma ALTER COLUMN id_cronograma SET DEFAULT nextval('public.seg_cronograma_id_cronograma_seq'::regclass);


--
-- TOC entry 2852 (class 2604 OID 19890)
-- Name: seg_documento id_documento; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_documento ALTER COLUMN id_documento SET DEFAULT nextval('public.seg_documento_id_documento_seq'::regclass);


--
-- TOC entry 3019 (class 2604 OID 327380)
-- Name: seg_documento_estudo id_documento_estudo; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_documento_estudo ALTER COLUMN id_documento_estudo SET DEFAULT nextval('public.seg_documento_estudo_id_documento_estudo_seq'::regclass);


--
-- TOC entry 2854 (class 2604 OID 19891)
-- Name: seg_documento_protocolo id_documento_protocolo; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_documento_protocolo ALTER COLUMN id_documento_protocolo SET DEFAULT nextval('public.seg_documento_protocolo_id_documento_protocolo_seq'::regclass);


--
-- TOC entry 2855 (class 2604 OID 19892)
-- Name: seg_dominio id_dominio; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_dominio ALTER COLUMN id_dominio SET DEFAULT nextval('public.seg_dominio_id_dominio_seq'::regclass);


--
-- TOC entry 2856 (class 2604 OID 19893)
-- Name: seg_email id_email; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_email ALTER COLUMN id_email SET DEFAULT nextval('public.seg_email_id_email_seq'::regclass);


--
-- TOC entry 2858 (class 2604 OID 19894)
-- Name: seg_empresa id_empresa; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_empresa ALTER COLUMN id_empresa SET DEFAULT nextval('public.seg_empresa_id_empresa_seq'::regclass);


--
-- TOC entry 3020 (class 2604 OID 327465)
-- Name: seg_endemia id_endemia; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_endemia ALTER COLUMN id_endemia SET DEFAULT nextval('public.seg_endemia_id_endemia_seq'::regclass);


--
-- TOC entry 2866 (class 2604 OID 19896)
-- Name: seg_epi id_epi; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_epi ALTER COLUMN id_epi SET DEFAULT nextval('public.seg_epi_id_epi_seq'::regclass);


--
-- TOC entry 3017 (class 2604 OID 245183)
-- Name: seg_epi_monitoramento id_epi_monitoramento; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_epi_monitoramento ALTER COLUMN id_epi_monitoramento SET DEFAULT nextval('public.seg_epi_monitoramento_id_epi_monitoramento_seq'::regclass);


--
-- TOC entry 3015 (class 2604 OID 245138)
-- Name: seg_esocial_24 id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_esocial_24 ALTER COLUMN id SET DEFAULT nextval('public.seg_esocial_24_id_seq'::regclass);


--
-- TOC entry 3013 (class 2604 OID 245116)
-- Name: seg_esocial_monitoramento id_esocial_monitoramento; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_esocial_monitoramento ALTER COLUMN id_esocial_monitoramento SET DEFAULT nextval('public.seg_esocial_monitoramento_id_esocial_monitoramento_seq'::regclass);


--
-- TOC entry 2870 (class 2604 OID 19899)
-- Name: seg_estimativa id_estimativa; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_estimativa ALTER COLUMN id_estimativa SET DEFAULT nextval('public.seg_estimativa_id_estimativa_seq'::regclass);


--
-- TOC entry 2871 (class 2604 OID 19900)
-- Name: seg_estimativa_estudo id_estimativa_estudo; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_estimativa_estudo ALTER COLUMN id_estimativa_estudo SET DEFAULT nextval('public.seg_estimativa_estudo_id_estimativa_estudo_seq'::regclass);


--
-- TOC entry 2872 (class 2604 OID 19901)
-- Name: seg_estudo id_estudo; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_estudo ALTER COLUMN id_estudo SET DEFAULT nextval('public.seg_estudo_id_estudo_seq'::regclass);


--
-- TOC entry 2878 (class 2604 OID 19902)
-- Name: seg_exame id_exame; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_exame ALTER COLUMN id_exame SET DEFAULT nextval('public.seg_exame_id_exame_seq'::regclass);


--
-- TOC entry 2879 (class 2604 OID 19903)
-- Name: seg_fonte id_fonte; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_fonte ALTER COLUMN id_fonte SET DEFAULT nextval('public.seg_fonte_id_fonte_seq'::regclass);


--
-- TOC entry 2880 (class 2604 OID 19904)
-- Name: seg_forma id_forma; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_forma ALTER COLUMN id_forma SET DEFAULT nextval('public.seg_forma_id_forma_seq'::regclass);


--
-- TOC entry 2881 (class 2604 OID 19905)
-- Name: seg_funcao id_funcao; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_funcao ALTER COLUMN id_funcao SET DEFAULT nextval('public.seg_funcao_id_funcao_seq'::regclass);


--
-- TOC entry 2882 (class 2604 OID 19906)
-- Name: seg_funcao_epi id_seg_funcao_epi; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_funcao_epi ALTER COLUMN id_seg_funcao_epi SET DEFAULT nextval('public.seg_funcao_epi_id_seg_funcao_epi_seq'::regclass);


--
-- TOC entry 2883 (class 2604 OID 19907)
-- Name: seg_funcao_interna id_funcao_interna; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_funcao_interna ALTER COLUMN id_funcao_interna SET DEFAULT nextval('public.seg_funcao_interna_id_funcao_interna_seq'::regclass);


--
-- TOC entry 2884 (class 2604 OID 19908)
-- Name: seg_funcionario id_funcionario; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_funcionario ALTER COLUMN id_funcionario SET DEFAULT nextval('public.seg_funcionario_id_funcionario_seq'::regclass);


--
-- TOC entry 2886 (class 2604 OID 19909)
-- Name: seg_ghe id_ghe; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_ghe ALTER COLUMN id_ghe SET DEFAULT nextval('public.seg_ghe_id_ghe_seq'::regclass);


--
-- TOC entry 2887 (class 2604 OID 19910)
-- Name: seg_ghe_fonte id_ghe_fonte; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_ghe_fonte ALTER COLUMN id_ghe_fonte SET DEFAULT nextval('public.seg_ghe_fonte_id_ghe_fonte_seq'::regclass);


--
-- TOC entry 2888 (class 2604 OID 19911)
-- Name: seg_ghe_fonte_agente id_ghe_fonte_agente; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_ghe_fonte_agente ALTER COLUMN id_ghe_fonte_agente SET DEFAULT nextval('public.seg_ghe_fonte_agente_id_ghe_fonte_agente_seq'::regclass);


--
-- TOC entry 2889 (class 2604 OID 19912)
-- Name: seg_ghe_fonte_agente_epi id_ghe_fonte_agente_epi; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_ghe_fonte_agente_epi ALTER COLUMN id_ghe_fonte_agente_epi SET DEFAULT nextval('public.seg_ghe_fonte_agente_epi_id_ghe_fonte_agente_epi_seq'::regclass);


--
-- TOC entry 2892 (class 2604 OID 19913)
-- Name: seg_ghe_fonte_agente_exame id_ghe_fonte_agente_exame; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_ghe_fonte_agente_exame ALTER COLUMN id_ghe_fonte_agente_exame SET DEFAULT nextval('public.seg_ghe_fonte_agente_exame_id_ghe_fonte_agente_exame_seq'::regclass);


--
-- TOC entry 2902 (class 2604 OID 19914)
-- Name: seg_ghe_fonte_agente_exame_aso id_ghe_fonte_agente_exame_aso; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_ghe_fonte_agente_exame_aso ALTER COLUMN id_ghe_fonte_agente_exame_aso SET DEFAULT nextval('public.seg_ghe_fonte_agente_exame_as_id_ghe_fonte_agente_exame_aso_seq'::regclass);


--
-- TOC entry 2905 (class 2604 OID 19915)
-- Name: seg_ghe_setor id_ghe_setor; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_ghe_setor ALTER COLUMN id_ghe_setor SET DEFAULT nextval('public.seg_ghe_setor_id_ghe_setor_seq'::regclass);


--
-- TOC entry 2909 (class 2604 OID 19916)
-- Name: seg_ghe_setor_cliente_funcao id_ghe_setor_cliente_funcao; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_ghe_setor_cliente_funcao ALTER COLUMN id_ghe_setor_cliente_funcao SET DEFAULT nextval('public.seg_ghe_setor_cliente_funcao_id_ghe_setor_cliente_funcao_seq'::regclass);


--
-- TOC entry 2911 (class 2604 OID 19917)
-- Name: seg_grad_efeito id_grad_efeito; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_grad_efeito ALTER COLUMN id_grad_efeito SET DEFAULT nextval('public.seg_grad_efeito_id_grad_efeito_seq'::regclass);


--
-- TOC entry 2912 (class 2604 OID 19918)
-- Name: seg_grad_exposicao id_grad_exposicao; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_grad_exposicao ALTER COLUMN id_grad_exposicao SET DEFAULT nextval('public.seg_grad_exposicao_id_grad_exposicao_seq'::regclass);


--
-- TOC entry 2913 (class 2604 OID 19919)
-- Name: seg_grad_soma id_grad_soma; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_grad_soma ALTER COLUMN id_grad_soma SET DEFAULT nextval('public.seg_grad_soma_id_grad_soma_seq'::regclass);


--
-- TOC entry 2914 (class 2604 OID 19920)
-- Name: seg_hospital id_hospital; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_hospital ALTER COLUMN id_hospital SET DEFAULT nextval('public.seg_hospital_id_hospital_seq'::regclass);


--
-- TOC entry 2915 (class 2604 OID 19921)
-- Name: seg_itens_cancelados_nf id_itens_cancelados_nf; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_itens_cancelados_nf ALTER COLUMN id_itens_cancelados_nf SET DEFAULT nextval('public.seg_itens_cancelados_nf_id_itens_cancelados_nf_seq'::regclass);


--
-- TOC entry 2917 (class 2604 OID 19922)
-- Name: seg_log id_log; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_log ALTER COLUMN id_log SET DEFAULT nextval('public.seg_log_id_log_seq'::regclass);


--
-- TOC entry 3018 (class 2604 OID 321668)
-- Name: seg_login_auditoria id_login_auditoria; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_login_auditoria ALTER COLUMN id_login_auditoria SET DEFAULT nextval('public.seg_login_auditoria_id_login_auditoria_seq'::regclass);


--
-- TOC entry 2918 (class 2604 OID 19923)
-- Name: seg_material_hospitalar id_material_hospitalar; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_material_hospitalar ALTER COLUMN id_material_hospitalar SET DEFAULT nextval('public.seg_material_hospitalar_id_material_hospitalar_seq'::regclass);


--
-- TOC entry 2919 (class 2604 OID 19924)
-- Name: seg_medico id_medico; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_medico ALTER COLUMN id_medico SET DEFAULT nextval('public.seg_medico_id_medico_seq'::regclass);


--
-- TOC entry 2920 (class 2604 OID 19925)
-- Name: seg_medico_exame id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_medico_exame ALTER COLUMN id SET DEFAULT nextval('public.seg_medico_exame_id_seq'::regclass);


--
-- TOC entry 3012 (class 2604 OID 245094)
-- Name: seg_monitoramento id_monitoramento; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_monitoramento ALTER COLUMN id_monitoramento SET DEFAULT nextval('public.seg_monitoramento_id_monitoramento_seq'::regclass);


--
-- TOC entry 3016 (class 2604 OID 245158)
-- Name: seg_monitoramento_ghefonteagente id_monitoramento_ghefonteagente; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_monitoramento_ghefonteagente ALTER COLUMN id_monitoramento_ghefonteagente SET DEFAULT nextval('public.seg_monitoramento_ghefonteage_id_monitoramento_ghefonteagen_seq'::regclass);


--
-- TOC entry 2921 (class 2604 OID 19926)
-- Name: seg_movimento id_movimento; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_movimento ALTER COLUMN id_movimento SET DEFAULT nextval('public.seg_movimento_id_movimento_seq'::regclass);


--
-- TOC entry 2923 (class 2604 OID 19927)
-- Name: seg_nfe id_nfe; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_nfe ALTER COLUMN id_nfe SET DEFAULT nextval('public.seg_nfe_id_nfe_seq'::regclass);


--
-- TOC entry 2925 (class 2604 OID 19928)
-- Name: seg_norma id_norma; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_norma ALTER COLUMN id_norma SET DEFAULT nextval('public.seg_norma_id_norma_seq'::regclass);


--
-- TOC entry 2926 (class 2604 OID 19929)
-- Name: seg_parcela id_parcela; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_parcela ALTER COLUMN id_parcela SET DEFAULT nextval('public.seg_parcela_id_parcela_seq'::regclass);


--
-- TOC entry 2927 (class 2604 OID 19930)
-- Name: seg_perfil id_perfil; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_perfil ALTER COLUMN id_perfil SET DEFAULT nextval('public.seg_perfil_id_perfil_seq'::regclass);


--
-- TOC entry 2928 (class 2604 OID 19931)
-- Name: seg_periodicidade id_periodicidade; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_periodicidade ALTER COLUMN id_periodicidade SET DEFAULT nextval('public.seg_periodicidade_id_periodicidade_seq'::regclass);


--
-- TOC entry 2930 (class 2604 OID 19932)
-- Name: seg_plano id_plano; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_plano ALTER COLUMN id_plano SET DEFAULT nextval('public.seg_plano_id_plano_seq'::regclass);


--
-- TOC entry 2931 (class 2604 OID 19933)
-- Name: seg_plano_forma id_plano_forma; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_plano_forma ALTER COLUMN id_plano_forma SET DEFAULT nextval('public.seg_plano_forma_id_plano_forma_seq'::regclass);


--
-- TOC entry 3011 (class 2604 OID 228726)
-- Name: seg_procedimento_esocial id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_procedimento_esocial ALTER COLUMN id SET DEFAULT nextval('public.seg_procedimento_esocial_id_seq'::regclass);


--
-- TOC entry 2932 (class 2604 OID 19940)
-- Name: seg_produto id_produto; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_produto ALTER COLUMN id_produto SET DEFAULT nextval('public.seg_produto_id_produto_seq'::regclass);


--
-- TOC entry 2936 (class 2604 OID 19941)
-- Name: seg_produto_contrato id_produto_contrato; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_produto_contrato ALTER COLUMN id_produto_contrato SET DEFAULT nextval('public.seg_produto_contrato_id_produto_contrato_seq'::regclass);


--
-- TOC entry 2987 (class 2604 OID 19942)
-- Name: seg_prontuario id_prontuario; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_prontuario ALTER COLUMN id_prontuario SET DEFAULT nextval('public.seg_prontuario_id_prontuario_seq'::regclass);


--
-- TOC entry 2988 (class 2604 OID 19943)
-- Name: seg_protocolo id_protocolo; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_protocolo ALTER COLUMN id_protocolo SET DEFAULT nextval('public.seg_protocolo_id_protocolo_seq'::regclass);


--
-- TOC entry 2989 (class 2604 OID 19944)
-- Name: seg_ramo id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_ramo ALTER COLUMN id SET DEFAULT nextval('public.seg_ramo_id_seq'::regclass);


--
-- TOC entry 2990 (class 2604 OID 19945)
-- Name: seg_relanual id_relanual; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_relanual ALTER COLUMN id_relanual SET DEFAULT nextval('public.seg_relanual_id_relanual_seq'::regclass);


--
-- TOC entry 2991 (class 2604 OID 19946)
-- Name: seg_relanual_item id_relanual_item; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_relanual_item ALTER COLUMN id_relanual_item SET DEFAULT nextval('public.seg_relanual_item_id_relanual_item_seq'::regclass);


--
-- TOC entry 2992 (class 2604 OID 19947)
-- Name: seg_relatorio id_relatorio; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_relatorio ALTER COLUMN id_relatorio SET DEFAULT nextval('public.seg_relatorio_id_relatorio_seq'::regclass);


--
-- TOC entry 3010 (class 2604 OID 21598)
-- Name: seg_relatorio_exame id_relatorio_exame; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_relatorio_exame ALTER COLUMN id_relatorio_exame SET DEFAULT nextval('public.seg_relatorio_exame_id_relatorio_exame_seq'::regclass);


--
-- TOC entry 2993 (class 2604 OID 19948)
-- Name: seg_rep_cli id_seg_rep_cli; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_rep_cli ALTER COLUMN id_seg_rep_cli SET DEFAULT nextval('public.seg_rep_cli_id_seg_rep_cli_seq'::regclass);


--
-- TOC entry 2994 (class 2604 OID 19950)
-- Name: seg_revisao id_revisao; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_revisao ALTER COLUMN id_revisao SET DEFAULT nextval('public.seg_revisao_id_revisao_seq'::regclass);


--
-- TOC entry 2995 (class 2604 OID 19951)
-- Name: seg_risco id_risco; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_risco ALTER COLUMN id_risco SET DEFAULT nextval('public.seg_risco_id_risco_seq'::regclass);


--
-- TOC entry 2997 (class 2604 OID 19952)
-- Name: seg_sala id_sala; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_sala ALTER COLUMN id_sala SET DEFAULT nextval('public.seg_sala_id_sala_seq'::regclass);


--
-- TOC entry 2999 (class 2604 OID 19953)
-- Name: seg_sala_exame id_sala_exame; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_sala_exame ALTER COLUMN id_sala_exame SET DEFAULT nextval('public.seg_sala_exame_id_sala_exame_seq'::regclass);


--
-- TOC entry 3001 (class 2604 OID 19954)
-- Name: seg_setor id_setor; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_setor ALTER COLUMN id_setor SET DEFAULT nextval('public.seg_setor_id_setor_seq'::regclass);


--
-- TOC entry 3002 (class 2604 OID 19955)
-- Name: seg_tipo_contato id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_tipo_contato ALTER COLUMN id SET DEFAULT nextval('public.seg_tipo_contato_id_seq'::regclass);


--
-- TOC entry 3005 (class 2604 OID 19956)
-- Name: seg_usuario id_usuario; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_usuario ALTER COLUMN id_usuario SET DEFAULT nextval('public.seg_usuario_id_usuario_seq'::regclass);


--
-- TOC entry 3006 (class 2604 OID 19957)
-- Name: seg_usuario_cliente id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_usuario_cliente ALTER COLUMN id SET DEFAULT nextval('public.seg_usuario_cliente_id_seq'::regclass);


--
-- TOC entry 3007 (class 2604 OID 19958)
-- Name: seg_usuario_funcao_interna id_usuario_funcao_interna; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_usuario_funcao_interna ALTER COLUMN id_usuario_funcao_interna SET DEFAULT nextval('public.seg_usuario_funcao_interna_id_usuario_funcao_interna_seq'::regclass);


--
-- TOC entry 3008 (class 2604 OID 19959)
-- Name: seg_vendedor id_vend; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_vendedor ALTER COLUMN id_vend SET DEFAULT nextval('public.seg_vendedor_id_vend_seq'::regclass);


--
-- TOC entry 3009 (class 2604 OID 19960)
-- Name: seg_versao id_versao; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_versao ALTER COLUMN id_versao SET DEFAULT nextval('public.seg_versao_id_versao_seq'::regclass);


--
-- TOC entry 3022 (class 2606 OID 19975)
-- Name: seg_acao seg_acao_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_acao
    ADD CONSTRAINT seg_acao_pkey PRIMARY KEY (id_acao);


--
-- TOC entry 3025 (class 2606 OID 19977)
-- Name: seg_acesso seg_acesso_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_acesso
    ADD CONSTRAINT seg_acesso_pkey PRIMARY KEY (id_acesso);


--
-- TOC entry 3027 (class 2606 OID 112436)
-- Name: seg_acompanhamento_atendimento seg_acompanhamento_atendimento_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_acompanhamento_atendimento
    ADD CONSTRAINT seg_acompanhamento_atendimento_pkey PRIMARY KEY (id_acompanhamento_atendimento);


--
-- TOC entry 3031 (class 2606 OID 19981)
-- Name: seg_agente seg_agente_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_agente
    ADD CONSTRAINT seg_agente_pkey PRIMARY KEY (id_agente);


--
-- TOC entry 3037 (class 2606 OID 19985)
-- Name: seg_arquivo_anexo seg_arquivo_anexo_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_arquivo_anexo
    ADD CONSTRAINT seg_arquivo_anexo_pkey PRIMARY KEY (id_arquivo);


--
-- TOC entry 3033 (class 2606 OID 19987)
-- Name: seg_arquivo seg_arquivo_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_arquivo
    ADD CONSTRAINT seg_arquivo_pkey PRIMARY KEY (id_arquivo);


--
-- TOC entry 3064 (class 2606 OID 19989)
-- Name: seg_aso seg_aso_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_aso
    ADD CONSTRAINT seg_aso_pkey PRIMARY KEY (id_aso);


--
-- TOC entry 3077 (class 2606 OID 19991)
-- Name: seg_atividade_cronograma seg_atividade_cronograma_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_atividade_cronograma
    ADD CONSTRAINT seg_atividade_cronograma_pkey PRIMARY KEY (id_atividade, id_cronograma);


--
-- TOC entry 3071 (class 2606 OID 19993)
-- Name: seg_atividade seg_atividade_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_atividade
    ADD CONSTRAINT seg_atividade_pkey PRIMARY KEY (id_atividade);


--
-- TOC entry 3079 (class 2606 OID 19995)
-- Name: seg_banco seg_banco_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_banco
    ADD CONSTRAINT seg_banco_pkey PRIMARY KEY (id_banco);


--
-- TOC entry 3083 (class 2606 OID 19999)
-- Name: seg_centrocusto seg_centrocusto_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_centrocusto
    ADD CONSTRAINT seg_centrocusto_pkey PRIMARY KEY (id);


--
-- TOC entry 3086 (class 2606 OID 20001)
-- Name: seg_cidade_ibge seg_cidade_ibge_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_cidade_ibge
    ADD CONSTRAINT seg_cidade_ibge_pkey PRIMARY KEY (id_cidade_ibge);


--
-- TOC entry 3111 (class 2606 OID 20003)
-- Name: seg_cliente_arquivo seg_cliente_arquivo_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_cliente_arquivo
    ADD CONSTRAINT seg_cliente_arquivo_pkey PRIMARY KEY (id_cliente_arquivo, id_arquivo);


--
-- TOC entry 3138 (class 2606 OID 20005)
-- Name: seg_cliente_funcao_exame_aso seg_cliente_funcao_exame_aso_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_cliente_funcao_exame_aso
    ADD CONSTRAINT seg_cliente_funcao_exame_aso_pkey PRIMARY KEY (id_cliente_funcao_exame_aso);


--
-- TOC entry 3123 (class 2606 OID 20007)
-- Name: seg_cliente_funcao_exame seg_cliente_funcao_exame_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_cliente_funcao_exame
    ADD CONSTRAINT seg_cliente_funcao_exame_pkey PRIMARY KEY (id_cliente_funcao_exame);


--
-- TOC entry 3148 (class 2606 OID 20009)
-- Name: seg_cliente_funcao_funcionario seg_cliente_funcao_funcionario_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_cliente_funcao_funcionario
    ADD CONSTRAINT seg_cliente_funcao_funcionario_pkey PRIMARY KEY (id_cliente_funcao_funcionario);


--
-- TOC entry 3117 (class 2606 OID 20011)
-- Name: seg_cliente_funcao seg_cliente_funcao_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_cliente_funcao
    ADD CONSTRAINT seg_cliente_funcao_pkey PRIMARY KEY (id_seg_cliente_funcao);


--
-- TOC entry 3154 (class 2606 OID 20013)
-- Name: seg_cliente_funcionario seg_cliente_funcionario_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_cliente_funcionario
    ADD CONSTRAINT seg_cliente_funcionario_pkey PRIMARY KEY (id);


--
-- TOC entry 3104 (class 2606 OID 20015)
-- Name: seg_cliente seg_cliente_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_cliente
    ADD CONSTRAINT seg_cliente_pkey PRIMARY KEY (id_cliente);


--
-- TOC entry 3158 (class 2606 OID 20017)
-- Name: seg_cliente_proposta seg_cliente_proposta_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_cliente_proposta
    ADD CONSTRAINT seg_cliente_proposta_pkey PRIMARY KEY (id_cliente_proposta);


--
-- TOC entry 3166 (class 2606 OID 20019)
-- Name: seg_cnae_estudo seg_cnae_estudo_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_cnae_estudo
    ADD CONSTRAINT seg_cnae_estudo_pkey PRIMARY KEY (id_cnae_estudo);


--
-- TOC entry 3160 (class 2606 OID 20021)
-- Name: seg_cnae seg_cnae_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_cnae
    ADD CONSTRAINT seg_cnae_pkey PRIMARY KEY (id_cnae);


--
-- TOC entry 3170 (class 2606 OID 20023)
-- Name: seg_comentario_funcao seg_comentario_funcao_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_comentario_funcao
    ADD CONSTRAINT seg_comentario_funcao_pkey PRIMARY KEY (id_comentario_funcao);


--
-- TOC entry 3172 (class 2606 OID 20025)
-- Name: seg_configuracao seg_configuracao_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_configuracao
    ADD CONSTRAINT seg_configuracao_pkey PRIMARY KEY (id_configuracao);


--
-- TOC entry 3176 (class 2606 OID 20027)
-- Name: seg_conta seg_conta_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_conta
    ADD CONSTRAINT seg_conta_pkey PRIMARY KEY (id_conta);


--
-- TOC entry 3184 (class 2606 OID 20029)
-- Name: seg_contato seg_contato_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_contato
    ADD CONSTRAINT seg_contato_pkey PRIMARY KEY (id);


--
-- TOC entry 3204 (class 2606 OID 20031)
-- Name: seg_contrato_exame seg_contrato_exame_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_contrato_exame
    ADD CONSTRAINT seg_contrato_exame_pkey PRIMARY KEY (id_contrato_exame);


--
-- TOC entry 3198 (class 2606 OID 20033)
-- Name: seg_contrato seg_contrato_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_contrato
    ADD CONSTRAINT seg_contrato_pkey PRIMARY KEY (id_contrato);


--
-- TOC entry 3211 (class 2606 OID 20035)
-- Name: seg_correcao_estudo seg_correcao_estudo_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_correcao_estudo
    ADD CONSTRAINT seg_correcao_estudo_pkey PRIMARY KEY (id_correcao_estudo);


--
-- TOC entry 3233 (class 2606 OID 20037)
-- Name: seg_crc seg_crc_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_crc
    ADD CONSTRAINT seg_crc_pkey PRIMARY KEY (id_crc);


--
-- TOC entry 3236 (class 2606 OID 20039)
-- Name: seg_cronograma seg_cronograma_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_cronograma
    ADD CONSTRAINT seg_cronograma_pkey PRIMARY KEY (id_cronograma);


--
-- TOC entry 3648 (class 2606 OID 327382)
-- Name: seg_documento_estudo seg_documento_estudo_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_documento_estudo
    ADD CONSTRAINT seg_documento_estudo_pkey PRIMARY KEY (id_documento_estudo);


--
-- TOC entry 3238 (class 2606 OID 20041)
-- Name: seg_documento seg_documento_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_documento
    ADD CONSTRAINT seg_documento_pkey PRIMARY KEY (id_documento);


--
-- TOC entry 3246 (class 2606 OID 20043)
-- Name: seg_documento_protocolo seg_documento_protocolo_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_documento_protocolo
    ADD CONSTRAINT seg_documento_protocolo_pkey PRIMARY KEY (id_documento_protocolo);


--
-- TOC entry 3254 (class 2606 OID 20045)
-- Name: seg_dominio_acao seg_dominio_acao_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_dominio_acao
    ADD CONSTRAINT seg_dominio_acao_pkey PRIMARY KEY (id_dominio, id_acao);


--
-- TOC entry 3248 (class 2606 OID 20047)
-- Name: seg_dominio seg_dominio_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_dominio
    ADD CONSTRAINT seg_dominio_pkey PRIMARY KEY (id_dominio);


--
-- TOC entry 3259 (class 2606 OID 20049)
-- Name: seg_email seg_email_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_email
    ADD CONSTRAINT seg_email_pkey PRIMARY KEY (id_email);


--
-- TOC entry 3266 (class 2606 OID 20051)
-- Name: seg_empresa seg_empresa_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_empresa
    ADD CONSTRAINT seg_empresa_pkey PRIMARY KEY (id_empresa);


--
-- TOC entry 3650 (class 2606 OID 327493)
-- Name: seg_endemia seg_endemia_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_endemia
    ADD CONSTRAINT seg_endemia_pkey PRIMARY KEY (id_endemia);


--
-- TOC entry 3640 (class 2606 OID 245185)
-- Name: seg_epi_monitoramento seg_epi_monitoramento_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_epi_monitoramento
    ADD CONSTRAINT seg_epi_monitoramento_pkey PRIMARY KEY (id_epi_monitoramento);


--
-- TOC entry 3268 (class 2606 OID 20055)
-- Name: seg_epi seg_epi_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_epi
    ADD CONSTRAINT seg_epi_pkey PRIMARY KEY (id_epi);


--
-- TOC entry 3628 (class 2606 OID 245143)
-- Name: seg_esocial_24 seg_esocial_24_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_esocial_24
    ADD CONSTRAINT seg_esocial_24_pkey PRIMARY KEY (id);


--
-- TOC entry 3278 (class 2606 OID 20057)
-- Name: seg_esocial_lote_aso seg_esocial_lote_aso_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_esocial_lote_aso
    ADD CONSTRAINT seg_esocial_lote_aso_pkey PRIMARY KEY (id_seg_esocial_lote_aso);


--
-- TOC entry 3272 (class 2606 OID 20059)
-- Name: seg_esocial_lote seg_esocial_lote_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_esocial_lote
    ADD CONSTRAINT seg_esocial_lote_pkey PRIMARY KEY (id_esocial_lote);


--
-- TOC entry 3626 (class 2606 OID 245118)
-- Name: seg_esocial_monitoramento seg_esocial_monitoramento_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_esocial_monitoramento
    ADD CONSTRAINT seg_esocial_monitoramento_pkey PRIMARY KEY (id_esocial_monitoramento);


--
-- TOC entry 3286 (class 2606 OID 20061)
-- Name: seg_estimativa_estudo seg_estimativa_estudo_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_estimativa_estudo
    ADD CONSTRAINT seg_estimativa_estudo_pkey PRIMARY KEY (id_estimativa_estudo);


--
-- TOC entry 3280 (class 2606 OID 20063)
-- Name: seg_estimativa seg_estimativa_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_estimativa
    ADD CONSTRAINT seg_estimativa_pkey PRIMARY KEY (id_estimativa);


--
-- TOC entry 3309 (class 2606 OID 20065)
-- Name: seg_estudo_hospital seg_estudo_hospital_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_estudo_hospital
    ADD CONSTRAINT seg_estudo_hospital_pkey PRIMARY KEY (id_estudo, id_hospital);


--
-- TOC entry 3315 (class 2606 OID 20067)
-- Name: seg_estudo_medico seg_estudo_medico_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_estudo_medico
    ADD CONSTRAINT seg_estudo_medico_pkey PRIMARY KEY (id_estudo, id_medico);


--
-- TOC entry 3302 (class 2606 OID 20069)
-- Name: seg_estudo seg_estudo_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_estudo
    ADD CONSTRAINT seg_estudo_pkey PRIMARY KEY (id_estudo);


--
-- TOC entry 3317 (class 2606 OID 20071)
-- Name: seg_exame seg_exame_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_exame
    ADD CONSTRAINT seg_exame_pkey PRIMARY KEY (id_exame);


--
-- TOC entry 3319 (class 2606 OID 20073)
-- Name: seg_fonte seg_fonte_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_fonte
    ADD CONSTRAINT seg_fonte_pkey PRIMARY KEY (id_fonte);


--
-- TOC entry 3321 (class 2606 OID 20075)
-- Name: seg_forma seg_forma_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_forma
    ADD CONSTRAINT seg_forma_pkey PRIMARY KEY (id_forma);


--
-- TOC entry 3327 (class 2606 OID 20077)
-- Name: seg_funcao_epi seg_funcao_epi_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_funcao_epi
    ADD CONSTRAINT seg_funcao_epi_pkey PRIMARY KEY (id_seg_funcao_epi, id_estudo);


--
-- TOC entry 3330 (class 2606 OID 20079)
-- Name: seg_funcao_interna seg_funcao_interna_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_funcao_interna
    ADD CONSTRAINT seg_funcao_interna_pkey PRIMARY KEY (id_funcao_interna);


--
-- TOC entry 3323 (class 2606 OID 20081)
-- Name: seg_funcao seg_funcao_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_funcao
    ADD CONSTRAINT seg_funcao_pkey PRIMARY KEY (id_funcao);


--
-- TOC entry 3334 (class 2606 OID 20083)
-- Name: seg_funcionario seg_funcionario_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_funcionario
    ADD CONSTRAINT seg_funcionario_pkey PRIMARY KEY (id_funcionario);


--
-- TOC entry 3362 (class 2606 OID 20085)
-- Name: seg_ghe_fonte_agente_epi seg_ghe_fonte_agente_epi_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_ghe_fonte_agente_epi
    ADD CONSTRAINT seg_ghe_fonte_agente_epi_pkey PRIMARY KEY (id_ghe_fonte_agente_epi);


--
-- TOC entry 3385 (class 2606 OID 20087)
-- Name: seg_ghe_fonte_agente_exame_aso seg_ghe_fonte_agente_exame_aso_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_ghe_fonte_agente_exame_aso
    ADD CONSTRAINT seg_ghe_fonte_agente_exame_aso_pkey PRIMARY KEY (id_ghe_fonte_agente_exame_aso);


--
-- TOC entry 3368 (class 2606 OID 20089)
-- Name: seg_ghe_fonte_agente_exame seg_ghe_fonte_agente_exame_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_ghe_fonte_agente_exame
    ADD CONSTRAINT seg_ghe_fonte_agente_exame_pkey PRIMARY KEY (id_ghe_fonte_agente_exame);


--
-- TOC entry 3356 (class 2606 OID 20091)
-- Name: seg_ghe_fonte_agente seg_ghe_fonte_agente_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_ghe_fonte_agente
    ADD CONSTRAINT seg_ghe_fonte_agente_pkey PRIMARY KEY (id_ghe_fonte_agente);


--
-- TOC entry 3344 (class 2606 OID 20093)
-- Name: seg_ghe_fonte seg_ghe_fonte_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_ghe_fonte
    ADD CONSTRAINT seg_ghe_fonte_pkey PRIMARY KEY (id_ghe_fonte);


--
-- TOC entry 3338 (class 2606 OID 20095)
-- Name: seg_ghe seg_ghe_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_ghe
    ADD CONSTRAINT seg_ghe_pkey PRIMARY KEY (id_ghe);


--
-- TOC entry 3411 (class 2606 OID 20097)
-- Name: seg_ghe_setor_cliente_funcao_exame seg_ghe_setor_cliente_funcao_exame_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_ghe_setor_cliente_funcao_exame
    ADD CONSTRAINT seg_ghe_setor_cliente_funcao_exame_pkey PRIMARY KEY (id_ghe_setor_cliente_funcao, id_exame);


--
-- TOC entry 3403 (class 2606 OID 20099)
-- Name: seg_ghe_setor_cliente_funcao seg_ghe_setor_cliente_funcao_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_ghe_setor_cliente_funcao
    ADD CONSTRAINT seg_ghe_setor_cliente_funcao_pkey PRIMARY KEY (id_ghe_setor_cliente_funcao);


--
-- TOC entry 3395 (class 2606 OID 20101)
-- Name: seg_ghe_setor seg_ghe_setor_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_ghe_setor
    ADD CONSTRAINT seg_ghe_setor_pkey PRIMARY KEY (id_ghe_setor);


--
-- TOC entry 3413 (class 2606 OID 20103)
-- Name: seg_grad_efeito seg_grad_efeito_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_grad_efeito
    ADD CONSTRAINT seg_grad_efeito_pkey PRIMARY KEY (id_grad_efeito);


--
-- TOC entry 3415 (class 2606 OID 20105)
-- Name: seg_grad_exposicao seg_grad_exposicao_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_grad_exposicao
    ADD CONSTRAINT seg_grad_exposicao_pkey PRIMARY KEY (id_grad_exposicao);


--
-- TOC entry 3417 (class 2606 OID 20107)
-- Name: seg_grad_soma seg_grad_soma_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_grad_soma
    ADD CONSTRAINT seg_grad_soma_pkey PRIMARY KEY (id_grad_soma);


--
-- TOC entry 3421 (class 2606 OID 20109)
-- Name: seg_hospital seg_hospital_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_hospital
    ADD CONSTRAINT seg_hospital_pkey PRIMARY KEY (id_hospital);


--
-- TOC entry 3423 (class 2606 OID 20111)
-- Name: seg_itens_cancelados_nf seg_itens_cancelados_nf_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_itens_cancelados_nf
    ADD CONSTRAINT seg_itens_cancelados_nf_pkey PRIMARY KEY (id_itens_cancelados_nf);


--
-- TOC entry 3425 (class 2606 OID 20113)
-- Name: seg_log seg_log_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_log
    ADD CONSTRAINT seg_log_pkey PRIMARY KEY (id_log);


--
-- TOC entry 3642 (class 2606 OID 321673)
-- Name: seg_login_auditoria seg_login_auditoria_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_login_auditoria
    ADD CONSTRAINT seg_login_auditoria_pkey PRIMARY KEY (id_login_auditoria);


--
-- TOC entry 3431 (class 2606 OID 20115)
-- Name: seg_material_hospitalar_estudo seg_material_hospitalar_estudo_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_material_hospitalar_estudo
    ADD CONSTRAINT seg_material_hospitalar_estudo_pkey PRIMARY KEY (id_estudo, id_material_hospitalar);


--
-- TOC entry 3427 (class 2606 OID 20117)
-- Name: seg_material_hospitalar seg_material_hospitalar_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_material_hospitalar
    ADD CONSTRAINT seg_material_hospitalar_pkey PRIMARY KEY (id_material_hospitalar);


--
-- TOC entry 3443 (class 2606 OID 20119)
-- Name: seg_medico_exame seg_medico_exame_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_medico_exame
    ADD CONSTRAINT seg_medico_exame_pkey PRIMARY KEY (id);


--
-- TOC entry 3437 (class 2606 OID 20121)
-- Name: seg_medico seg_medico_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_medico
    ADD CONSTRAINT seg_medico_pkey PRIMARY KEY (id_medico);


--
-- TOC entry 3634 (class 2606 OID 245163)
-- Name: seg_monitoramento_ghefonteagente seg_monitoramento_ghefonteagente_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_monitoramento_ghefonteagente
    ADD CONSTRAINT seg_monitoramento_ghefonteagente_pkey PRIMARY KEY (id_monitoramento_ghefonteagente);


--
-- TOC entry 3620 (class 2606 OID 245096)
-- Name: seg_monitoramento seg_monitoramento_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_monitoramento
    ADD CONSTRAINT seg_monitoramento_pkey PRIMARY KEY (id_monitoramento);


--
-- TOC entry 3463 (class 2606 OID 20123)
-- Name: seg_movimento seg_movimento_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_movimento
    ADD CONSTRAINT seg_movimento_pkey PRIMARY KEY (id_movimento);


--
-- TOC entry 3475 (class 2606 OID 20125)
-- Name: seg_nfe seg_nfe_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_nfe
    ADD CONSTRAINT seg_nfe_pkey PRIMARY KEY (id_nfe);


--
-- TOC entry 3483 (class 2606 OID 20127)
-- Name: seg_norma_ghe seg_norma_ghe_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_norma_ghe
    ADD CONSTRAINT seg_norma_ghe_pkey PRIMARY KEY (id_norma, id_ghe);


--
-- TOC entry 3477 (class 2606 OID 20129)
-- Name: seg_norma seg_norma_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_norma
    ADD CONSTRAINT seg_norma_pkey PRIMARY KEY (id_norma);


--
-- TOC entry 3487 (class 2606 OID 20131)
-- Name: seg_parcela seg_parcela_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_parcela
    ADD CONSTRAINT seg_parcela_pkey PRIMARY KEY (id_parcela);


--
-- TOC entry 3495 (class 2606 OID 20133)
-- Name: seg_perfil_dominio_acao seg_perfil_dominio_acao_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_perfil_dominio_acao
    ADD CONSTRAINT seg_perfil_dominio_acao_pkey PRIMARY KEY (id_perfil, id_dominio, id_acao);


--
-- TOC entry 3489 (class 2606 OID 20135)
-- Name: seg_perfil seg_perfil_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_perfil
    ADD CONSTRAINT seg_perfil_pkey PRIMARY KEY (id_perfil);


--
-- TOC entry 3497 (class 2606 OID 20137)
-- Name: seg_periodicidade seg_periodicidade_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_periodicidade
    ADD CONSTRAINT seg_periodicidade_pkey PRIMARY KEY (id_periodicidade);


--
-- TOC entry 3505 (class 2606 OID 20139)
-- Name: seg_plano_forma seg_plano_forma_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_plano_forma
    ADD CONSTRAINT seg_plano_forma_pkey PRIMARY KEY (id_plano_forma);


--
-- TOC entry 3499 (class 2606 OID 20141)
-- Name: seg_plano seg_plano_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_plano
    ADD CONSTRAINT seg_plano_pkey PRIMARY KEY (id_plano);


--
-- TOC entry 3612 (class 2606 OID 228731)
-- Name: seg_procedimento_esocial seg_procedimento_esocial_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_procedimento_esocial
    ADD CONSTRAINT seg_procedimento_esocial_pkey PRIMARY KEY (id);


--
-- TOC entry 3513 (class 2606 OID 20155)
-- Name: seg_produto_contrato seg_produto_contrato_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_produto_contrato
    ADD CONSTRAINT seg_produto_contrato_pkey PRIMARY KEY (id_produto_contrato);


--
-- TOC entry 3507 (class 2606 OID 20157)
-- Name: seg_produto seg_produto_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_produto
    ADD CONSTRAINT seg_produto_pkey PRIMARY KEY (id_produto);


--
-- TOC entry 3520 (class 2606 OID 20159)
-- Name: seg_prontuario seg_prontuario_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_prontuario
    ADD CONSTRAINT seg_prontuario_pkey PRIMARY KEY (id_prontuario);


--
-- TOC entry 3530 (class 2606 OID 20161)
-- Name: seg_protocolo seg_protocolo_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_protocolo
    ADD CONSTRAINT seg_protocolo_pkey PRIMARY KEY (id_protocolo);


--
-- TOC entry 3532 (class 2606 OID 20163)
-- Name: seg_ramo seg_ramo_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_ramo
    ADD CONSTRAINT seg_ramo_pkey PRIMARY KEY (id);


--
-- TOC entry 3543 (class 2606 OID 20165)
-- Name: seg_relanual_item seg_relanual_item_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_relanual_item
    ADD CONSTRAINT seg_relanual_item_pkey PRIMARY KEY (id_relanual_item);


--
-- TOC entry 3540 (class 2606 OID 20167)
-- Name: seg_relanual seg_relanual_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_relanual
    ADD CONSTRAINT seg_relanual_pkey PRIMARY KEY (id_relanual);


--
-- TOC entry 3610 (class 2606 OID 21600)
-- Name: seg_relatorio_exame seg_relatorio_exame_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_relatorio_exame
    ADD CONSTRAINT seg_relatorio_exame_pkey PRIMARY KEY (id_relatorio_exame);


--
-- TOC entry 3546 (class 2606 OID 20169)
-- Name: seg_relatorio seg_relatorio_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_relatorio
    ADD CONSTRAINT seg_relatorio_pkey PRIMARY KEY (id_relatorio);


--
-- TOC entry 3552 (class 2606 OID 20171)
-- Name: seg_rep_cli seg_rep_cli_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_rep_cli
    ADD CONSTRAINT seg_rep_cli_pkey PRIMARY KEY (id_seg_rep_cli);


--
-- TOC entry 3556 (class 2606 OID 20175)
-- Name: seg_revisao seg_revisao_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_revisao
    ADD CONSTRAINT seg_revisao_pkey PRIMARY KEY (id_revisao);


--
-- TOC entry 3558 (class 2606 OID 20177)
-- Name: seg_risco seg_risco_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_risco
    ADD CONSTRAINT seg_risco_pkey PRIMARY KEY (id_risco);


--
-- TOC entry 3568 (class 2606 OID 20179)
-- Name: seg_sala_exame seg_sala_exame_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_sala_exame
    ADD CONSTRAINT seg_sala_exame_pkey PRIMARY KEY (id_sala_exame);


--
-- TOC entry 3562 (class 2606 OID 20181)
-- Name: seg_sala seg_sala_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_sala
    ADD CONSTRAINT seg_sala_pkey PRIMARY KEY (id_sala);


--
-- TOC entry 3570 (class 2606 OID 20183)
-- Name: seg_setor seg_setor_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_setor
    ADD CONSTRAINT seg_setor_pkey PRIMARY KEY (id_setor);


--
-- TOC entry 3572 (class 2606 OID 20185)
-- Name: seg_tipo_contato seg_tipo_contato_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_tipo_contato
    ADD CONSTRAINT seg_tipo_contato_pkey PRIMARY KEY (id);


--
-- TOC entry 3584 (class 2606 OID 20187)
-- Name: seg_usuario_cliente seg_usuario_cliente_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_usuario_cliente
    ADD CONSTRAINT seg_usuario_cliente_pkey PRIMARY KEY (id);


--
-- TOC entry 3590 (class 2606 OID 20189)
-- Name: seg_usuario_funcao_interna seg_usuario_funcao_interna_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_usuario_funcao_interna
    ADD CONSTRAINT seg_usuario_funcao_interna_pkey PRIMARY KEY (id_usuario_funcao_interna);


--
-- TOC entry 3596 (class 2606 OID 20191)
-- Name: seg_usuario_perfil seg_usuario_perfil_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_usuario_perfil
    ADD CONSTRAINT seg_usuario_perfil_pkey PRIMARY KEY (id_usuario, id_perfil);


--
-- TOC entry 3578 (class 2606 OID 20193)
-- Name: seg_usuario seg_usuario_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_usuario
    ADD CONSTRAINT seg_usuario_pkey PRIMARY KEY (id_usuario);


--
-- TOC entry 3600 (class 2606 OID 20195)
-- Name: seg_vendedor seg_vendedor_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_vendedor
    ADD CONSTRAINT seg_vendedor_pkey PRIMARY KEY (id_vend);


--
-- TOC entry 3604 (class 2606 OID 20197)
-- Name: seg_versao seg_versao_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_versao
    ADD CONSTRAINT seg_versao_pkey PRIMARY KEY (id_versao);


--
-- TOC entry 3155 (class 1259 OID 20198)
-- Name: cliente_proposta_id_cidade_ibge_fk; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX cliente_proposta_id_cidade_ibge_fk ON public.seg_cliente_proposta USING btree (id_cidade_ibge);


--
-- TOC entry 3643 (class 1259 OID 327393)
-- Name: documento_estudo_id_documento_fk; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX documento_estudo_id_documento_fk ON public.seg_documento_estudo USING btree (id_documento);


--
-- TOC entry 3644 (class 1259 OID 327394)
-- Name: documento_estudo_id_estudo_fk; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX documento_estudo_id_estudo_fk ON public.seg_documento_estudo USING btree (id_estudo);


--
-- TOC entry 3281 (class 1259 OID 20199)
-- Name: estimativa_estudo_id_estimativa_fk; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX estimativa_estudo_id_estimativa_fk ON public.seg_estimativa_estudo USING btree (id_estimativa);


--
-- TOC entry 3282 (class 1259 OID 20200)
-- Name: estimativa_estudo_id_estudo_fk; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX estimativa_estudo_id_estudo_fk ON public.seg_estimativa_estudo USING btree (id_estudo);


--
-- TOC entry 3249 (class 1259 OID 20201)
-- Name: ifk_acao_permite_dominio; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ifk_acao_permite_dominio ON public.seg_dominio_acao USING btree (id_acao);


--
-- TOC entry 3345 (class 1259 OID 20202)
-- Name: ifk_agente_especifica_ghe_font; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ifk_agente_especifica_ghe_font ON public.seg_ghe_fonte_agente USING btree (id_agente);


--
-- TOC entry 3629 (class 1259 OID 245177)
-- Name: ifk_agente_monitoramento; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ifk_agente_monitoramento ON public.seg_monitoramento_ghefonteagente USING btree (id_ghe_fonte_agente);


--
-- TOC entry 3105 (class 1259 OID 20203)
-- Name: ifk_arquivo_identifica_cliente; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ifk_arquivo_identifica_cliente ON public.seg_cliente_arquivo USING btree (id_cliente);


--
-- TOC entry 3034 (class 1259 OID 20204)
-- Name: ifk_aso_armazena_anexo; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ifk_aso_armazena_anexo ON public.seg_arquivo_anexo USING btree (id_aso);


--
-- TOC entry 3239 (class 1259 OID 20205)
-- Name: ifk_aso_completa_documento_pro; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ifk_aso_completa_documento_pro ON public.seg_documento_protocolo USING btree (id_aso);


--
-- TOC entry 3273 (class 1259 OID 294397)
-- Name: ifk_aso_esocial_lote_aso; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ifk_aso_esocial_lote_aso ON public.seg_esocial_lote_aso USING btree (id_aso);


--
-- TOC entry 3066 (class 1259 OID 20206)
-- Name: ifk_aso_identifica_agente_risc; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ifk_aso_identifica_agente_risc ON public.seg_aso_agente_risco USING btree (id_aso);


--
-- TOC entry 3068 (class 1259 OID 20207)
-- Name: ifk_aso_localiza_setor_ghe; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ifk_aso_localiza_setor_ghe ON public.seg_aso_ghe_setor USING btree (id_aso);


--
-- TOC entry 3038 (class 1259 OID 20208)
-- Name: ifk_aso_possui_periodicidade; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ifk_aso_possui_periodicidade ON public.seg_aso USING btree (id_periodicidade);


--
-- TOC entry 3515 (class 1259 OID 20209)
-- Name: ifk_aso_prontuaria_prontuario; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ifk_aso_prontuaria_prontuario ON public.seg_prontuario USING btree (id_aso);


--
-- TOC entry 3124 (class 1259 OID 20210)
-- Name: ifk_aso_prontuariza_cliente_fu; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ifk_aso_prontuariza_cliente_fu ON public.seg_cliente_funcao_exame_aso USING btree (id_aso);


--
-- TOC entry 3370 (class 1259 OID 20211)
-- Name: ifk_aso_prontuariza_ghe_fonte_; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ifk_aso_prontuariza_ghe_fonte_ ON public.seg_ghe_fonte_agente_exame_aso USING btree (id_aso);


--
-- TOC entry 3072 (class 1259 OID 20212)
-- Name: ifk_atividade_compoe_cronogram; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ifk_atividade_compoe_cronogram ON public.seg_atividade_cronograma USING btree (id_atividade);


--
-- TOC entry 3613 (class 1259 OID 245110)
-- Name: ifk_auto_relacionamento; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ifk_auto_relacionamento ON public.seg_monitoramento USING btree (id_monitoramento_anterior);


--
-- TOC entry 3173 (class 1259 OID 20213)
-- Name: ifk_banco_tem_conta; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ifk_banco_tem_conta ON public.seg_conta USING btree (id_banco);


--
-- TOC entry 3039 (class 1259 OID 294387)
-- Name: ifk_centro_custo_aso; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ifk_centro_custo_aso ON public.seg_aso USING btree (id_centrocusto);


--
-- TOC entry 3040 (class 1259 OID 294388)
-- Name: ifk_centrocusto_atendimento; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ifk_centrocusto_atendimento ON public.seg_aso USING btree (id_centrocusto);


--
-- TOC entry 3177 (class 1259 OID 20214)
-- Name: ifk_cidade_contato; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ifk_cidade_contato ON public.seg_contato USING btree (id_cidade);


--
-- TOC entry 3156 (class 1259 OID 20215)
-- Name: ifk_cidade_estabelece_cliente_; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ifk_cidade_estabelece_cliente_ ON public.seg_cliente_proposta USING btree (id_cidade_ibge);


--
-- TOC entry 3331 (class 1259 OID 20216)
-- Name: ifk_cidade_funcionario; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ifk_cidade_funcionario ON public.seg_funcionario USING btree (id_cidade);


--
-- TOC entry 3418 (class 1259 OID 294401)
-- Name: ifk_cidade_hospital; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ifk_cidade_hospital ON public.seg_hospital USING btree (id_cidade);


--
-- TOC entry 3091 (class 1259 OID 20217)
-- Name: ifk_cidade_ibge_identifica_cid; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ifk_cidade_ibge_identifica_cid ON public.seg_cliente USING btree (id_cidade_ibge_cobranca);


--
-- TOC entry 3434 (class 1259 OID 20218)
-- Name: ifk_cidade_medico; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ifk_cidade_medico ON public.seg_medico USING btree (id_cidade);


--
-- TOC entry 3547 (class 1259 OID 20219)
-- Name: ifk_cli_representa_rep; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ifk_cli_representa_rep ON public.seg_rep_cli USING btree (id_cliente);


--
-- TOC entry 3112 (class 1259 OID 20220)
-- Name: ifk_cliente_agrupa_funcao; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ifk_cliente_agrupa_funcao ON public.seg_cliente_funcao USING btree (id_cliente);


--
-- TOC entry 3080 (class 1259 OID 20221)
-- Name: ifk_cliente_centrocusto; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ifk_cliente_centrocusto ON public.seg_centrocusto USING btree (id_cliente);


--
-- TOC entry 3149 (class 1259 OID 20222)
-- Name: ifk_cliente_cliente_funcionari; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ifk_cliente_cliente_funcionari ON public.seg_cliente_funcionario USING btree (id_cliente);


--
-- TOC entry 3533 (class 1259 OID 20223)
-- Name: ifk_cliente_completa_relanaul; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ifk_cliente_completa_relanaul ON public.seg_relanual USING btree (id_cliente);


--
-- TOC entry 3178 (class 1259 OID 20224)
-- Name: ifk_cliente_contato; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ifk_cliente_contato ON public.seg_contato USING btree (id_cliente);


--
-- TOC entry 3464 (class 1259 OID 20225)
-- Name: ifk_cliente_emite_nota_fiscal; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ifk_cliente_emite_nota_fiscal ON public.seg_nfe USING btree (id_cliente);


--
-- TOC entry 3521 (class 1259 OID 20226)
-- Name: ifk_cliente_envia_protocolo; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ifk_cliente_envia_protocolo ON public.seg_protocolo USING btree (id_cliente);


--
-- TOC entry 3269 (class 1259 OID 294395)
-- Name: ifk_cliente_esociallote; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ifk_cliente_esociallote ON public.seg_esocial_lote USING btree (id_cliente);


--
-- TOC entry 3185 (class 1259 OID 20227)
-- Name: ifk_cliente_estabelece_contrat; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ifk_cliente_estabelece_contrat ON public.seg_contrato USING btree (id_cliente);


--
-- TOC entry 3139 (class 1259 OID 20228)
-- Name: ifk_cliente_funcao_exame_atend; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ifk_cliente_funcao_exame_atend ON public.seg_cliente_funcao_exame_periodicidade USING btree (id_cliente_funcao_exame);


--
-- TOC entry 3125 (class 1259 OID 20229)
-- Name: ifk_cliente_funcao_exame_pront; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ifk_cliente_funcao_exame_pront ON public.seg_cliente_funcao_exame_aso USING btree (id_cliente_funcao_exame);


--
-- TOC entry 3614 (class 1259 OID 294403)
-- Name: ifk_cliente_funcao_funcionario; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ifk_cliente_funcao_funcionario ON public.seg_monitoramento USING btree (id_cliente_funcao_funcionario);


--
-- TOC entry 3143 (class 1259 OID 20231)
-- Name: ifk_cliente_funcao_participa_f; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ifk_cliente_funcao_participa_f ON public.seg_cliente_funcao_funcionario USING btree (id_seg_cliente_funcao);


--
-- TOC entry 3396 (class 1259 OID 20232)
-- Name: ifk_cliente_funcao_participa_g; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ifk_cliente_funcao_participa_g ON public.seg_ghe_setor_cliente_funcao USING btree (id_seg_cliente_funcao);


--
-- TOC entry 3118 (class 1259 OID 20233)
-- Name: ifk_cliente_funcao_sugere_exam; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ifk_cliente_funcao_sugere_exam ON public.seg_cliente_funcao_exame USING btree (id_cliente_funcao);


--
-- TOC entry 3106 (class 1259 OID 20235)
-- Name: ifk_cliente_identifica_arquivo; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ifk_cliente_identifica_arquivo ON public.seg_cliente_arquivo USING btree (id_arquivo);


--
-- TOC entry 3092 (class 1259 OID 20236)
-- Name: ifk_cliente_identifica_credenc; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ifk_cliente_identifica_credenc ON public.seg_cliente USING btree (id_cliente_credenciado);


--
-- TOC entry 3093 (class 1259 OID 20237)
-- Name: ifk_cliente_identifica_matriz; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ifk_cliente_identifica_matriz ON public.seg_cliente USING btree (id_cliente_matriz);


--
-- TOC entry 3186 (class 1259 OID 20238)
-- Name: ifk_cliente_proposta_recebe_pr; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ifk_cliente_proposta_recebe_pr ON public.seg_contrato USING btree (id_cliente_proposta);


--
-- TOC entry 3087 (class 1259 OID 20239)
-- Name: ifk_cliente_qualifica_cnae; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ifk_cliente_qualifica_cnae ON public.seg_cli_cnae USING btree (id_cliente);


--
-- TOC entry 3094 (class 1259 OID 20240)
-- Name: ifk_cliente_ramo; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ifk_cliente_ramo ON public.seg_cliente USING btree (id_ramo);


--
-- TOC entry 3444 (class 1259 OID 20241)
-- Name: ifk_cliente_realiza_movimento; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ifk_cliente_realiza_movimento ON public.seg_movimento USING btree (id_cliente);


--
-- TOC entry 3445 (class 1259 OID 20242)
-- Name: ifk_cliente_unidade_gera_movim; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ifk_cliente_unidade_gera_movim ON public.seg_movimento USING btree (id_cliente_unidade);


--
-- TOC entry 3579 (class 1259 OID 20243)
-- Name: ifk_cliente_usuariocliente; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ifk_cliente_usuariocliente ON public.seg_usuario_cliente USING btree (id_cliente);


--
-- TOC entry 3088 (class 1259 OID 20244)
-- Name: ifk_cnae_qualifica_cliente; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ifk_cnae_qualifica_cliente ON public.seg_cli_cnae USING btree (id_cnae);


--
-- TOC entry 3161 (class 1259 OID 20245)
-- Name: ifk_cnae_relaciona_estudo; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ifk_cnae_relaciona_estudo ON public.seg_cnae_estudo USING btree (id_cnae);


--
-- TOC entry 3212 (class 1259 OID 20246)
-- Name: ifk_conta_participa_crc; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ifk_conta_participa_crc ON public.seg_crc USING btree (id_conta);


--
-- TOC entry 3187 (class 1259 OID 20247)
-- Name: ifk_contrato_adiciona_aditivo; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ifk_contrato_adiciona_aditivo ON public.seg_contrato USING btree (id_contrato_pai);


--
-- TOC entry 3199 (class 1259 OID 20248)
-- Name: ifk_contrato_contem_exame; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ifk_contrato_contem_exame ON public.seg_contrato_exame USING btree (id_contrato);


--
-- TOC entry 3508 (class 1259 OID 20249)
-- Name: ifk_contrato_contem_produto; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ifk_contrato_contem_produto ON public.seg_produto_contrato USING btree (id_contrato);


--
-- TOC entry 3188 (class 1259 OID 20250)
-- Name: ifk_contrato_informa_contrato_; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ifk_contrato_informa_contrato_ ON public.seg_contrato USING btree (id_contrato_raiz);


--
-- TOC entry 3213 (class 1259 OID 20251)
-- Name: ifk_crc_desdobra_crc; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ifk_crc_desdobra_crc ON public.seg_crc USING btree (id_crc_desdobramento);


--
-- TOC entry 3287 (class 1259 OID 20252)
-- Name: ifk_cronograma_completa_estudo; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ifk_cronograma_completa_estudo ON public.seg_estudo USING btree (id_cronograma);


--
-- TOC entry 3073 (class 1259 OID 20253)
-- Name: ifk_cronograma_contempla_ativi; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ifk_cronograma_contempla_ativi ON public.seg_atividade_cronograma USING btree (id_cronograma);


--
-- TOC entry 3240 (class 1259 OID 20254)
-- Name: ifk_documento_completa_documen; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ifk_documento_completa_documen ON public.seg_documento_protocolo USING btree (id_documento);


--
-- TOC entry 3645 (class 1259 OID 327395)
-- Name: ifk_documento_documento_estudo; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ifk_documento_documento_estudo ON public.seg_documento_estudo USING btree (id_documento);


--
-- TOC entry 3250 (class 1259 OID 20255)
-- Name: ifk_dominio_permite_acao; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ifk_dominio_permite_acao ON public.seg_dominio_acao USING btree (id_dominio);


--
-- TOC entry 3490 (class 1259 OID 20256)
-- Name: ifk_domino_acao_limita_perfil; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ifk_domino_acao_limita_perfil ON public.seg_perfil_dominio_acao USING btree (id_dominio, id_acao);


--
-- TOC entry 3346 (class 1259 OID 20257)
-- Name: ifk_efeito_efetivaghe_fonte_ag; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ifk_efeito_efetivaghe_fonte_ag ON public.seg_ghe_fonte_agente USING btree (id_grad_efeito);


--
-- TOC entry 3041 (class 1259 OID 20258)
-- Name: ifk_empresa_aso; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ifk_empresa_aso ON public.seg_aso USING btree (id_empresa);


--
-- TOC entry 3189 (class 1259 OID 294391)
-- Name: ifk_empresa_contrato; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ifk_empresa_contrato ON public.seg_contrato USING btree (id_empresa);


--
-- TOC entry 3465 (class 1259 OID 20259)
-- Name: ifk_empresa_fatura_notafiscal; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ifk_empresa_fatura_notafiscal ON public.seg_nfe USING btree (id_empresa);


--
-- TOC entry 3261 (class 1259 OID 20260)
-- Name: ifk_empresa_matriz; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ifk_empresa_matriz ON public.seg_empresa USING btree (id_empresa_matriz);


--
-- TOC entry 3446 (class 1259 OID 20261)
-- Name: ifk_empresa_realiza_movimento; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ifk_empresa_realiza_movimento ON public.seg_movimento USING btree (id_empresa);


--
-- TOC entry 3357 (class 1259 OID 20262)
-- Name: ifk_epi_controla_ghe_agente; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ifk_epi_controla_ghe_agente ON public.seg_ghe_fonte_agente_epi USING btree (id_epi);


--
-- TOC entry 3635 (class 1259 OID 245199)
-- Name: ifk_epi_monitoramento; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ifk_epi_monitoramento ON public.seg_epi_monitoramento USING btree (id_epi);


--
-- TOC entry 3274 (class 1259 OID 294398)
-- Name: ifk_esocial_lote_esocial_lote_; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ifk_esocial_lote_esocial_lote_ ON public.seg_esocial_lote_aso USING btree (id_esocial_lote);


--
-- TOC entry 3621 (class 1259 OID 245132)
-- Name: ifk_esocial_monitoramento; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ifk_esocial_monitoramento ON public.seg_esocial_monitoramento USING btree (id_esocial_lote);


--
-- TOC entry 3283 (class 1259 OID 20263)
-- Name: ifk_estimativa_completa_estima; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ifk_estimativa_completa_estima ON public.seg_estimativa_estudo USING btree (id_estimativa);


--
-- TOC entry 3324 (class 1259 OID 20264)
-- Name: ifk_estudo_apresenta_funcao_ep; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ifk_estudo_apresenta_funcao_ep ON public.seg_funcao_epi USING btree (id_estudo);


--
-- TOC entry 3304 (class 1259 OID 20265)
-- Name: ifk_estudo_atende_hospital; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ifk_estudo_atende_hospital ON public.seg_estudo_hospital USING btree (id_estudo);


--
-- TOC entry 3284 (class 1259 OID 20266)
-- Name: ifk_estudo_completa_estimativa; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ifk_estudo_completa_estimativa ON public.seg_estimativa_estudo USING btree (id_estudo);


--
-- TOC entry 3646 (class 1259 OID 327396)
-- Name: ifk_estudo_documento_estudo; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ifk_estudo_documento_estudo ON public.seg_documento_estudo USING btree (id_estudo);


--
-- TOC entry 3310 (class 1259 OID 20268)
-- Name: ifk_estudo_examinado_medico; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ifk_estudo_examinado_medico ON public.seg_estudo_medico USING btree (id_estudo);


--
-- TOC entry 3206 (class 1259 OID 20269)
-- Name: ifk_estudo_grava_log_correcao; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ifk_estudo_grava_log_correcao ON public.seg_correcao_estudo USING btree (id_estudo);


--
-- TOC entry 3162 (class 1259 OID 20270)
-- Name: ifk_estudo_relaciona_cnae; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ifk_estudo_relaciona_cnae ON public.seg_cnae_estudo USING btree (id_estudo);


--
-- TOC entry 3288 (class 1259 OID 20271)
-- Name: ifk_estudo_subcontrata_cli; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ifk_estudo_subcontrata_cli ON public.seg_estudo USING btree (id_cliente_contratado);


--
-- TOC entry 3428 (class 1259 OID 20272)
-- Name: ifk_estudo_utiliza_material_ho; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ifk_estudo_utiliza_material_ho ON public.seg_material_hospitalar_estudo USING btree (id_estudo);


--
-- TOC entry 3289 (class 1259 OID 20273)
-- Name: ifk_estudo_versiona_relatorio; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ifk_estudo_versiona_relatorio ON public.seg_estudo USING btree (id_relatorio);


--
-- TOC entry 3563 (class 1259 OID 20274)
-- Name: ifk_exame_atende_sala; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ifk_exame_atende_sala ON public.seg_sala_exame USING btree (id_sala);


--
-- TOC entry 3200 (class 1259 OID 20275)
-- Name: ifk_exame_contem_contrato; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ifk_exame_contem_contrato ON public.seg_contrato_exame USING btree (id_exame);


--
-- TOC entry 3438 (class 1259 OID 20276)
-- Name: ifk_exame_medicoexame; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ifk_exame_medicoexame ON public.seg_medico_exame USING btree (id_exame);


--
-- TOC entry 3364 (class 1259 OID 20277)
-- Name: ifk_exame_realiza_ghe_fonte_ag; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ifk_exame_realiza_ghe_fonte_ag ON public.seg_ghe_fonte_agente_exame USING btree (id_exame);


--
-- TOC entry 3605 (class 1259 OID 21614)
-- Name: ifk_exame_relatorio_exame; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ifk_exame_relatorio_exame ON public.seg_relatorio_exame USING btree (id_exame);


--
-- TOC entry 3404 (class 1259 OID 20278)
-- Name: ifk_exame_restrito_por_ghe_set; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ifk_exame_restrito_por_ghe_set ON public.seg_ghe_setor_cliente_funcao_exame USING btree (id_exame);


--
-- TOC entry 3119 (class 1259 OID 20279)
-- Name: ifk_exame_sugerido_cliente_fun; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ifk_exame_sugerido_cliente_fun ON public.seg_cliente_funcao_exame USING btree (id_exame);


--
-- TOC entry 3347 (class 1259 OID 20280)
-- Name: ifk_exposicao_gradativa_ghe_fo; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ifk_exposicao_gradativa_ghe_fo ON public.seg_ghe_fonte_agente USING btree (id_grad_exposicao);


--
-- TOC entry 3339 (class 1259 OID 20281)
-- Name: ifk_fonte_expoem_ghe; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ifk_fonte_expoem_ghe ON public.seg_ghe_fonte USING btree (id_fonte);


--
-- TOC entry 3500 (class 1259 OID 20282)
-- Name: ifk_forma_aceita_plano; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ifk_forma_aceita_plano ON public.seg_plano_forma USING btree (id_forma);


--
-- TOC entry 3214 (class 1259 OID 20283)
-- Name: ifk_forma_cobra_cobranca; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ifk_forma_cobra_cobranca ON public.seg_crc USING btree (id_forma);


--
-- TOC entry 3113 (class 1259 OID 20284)
-- Name: ifk_funcao_agrupa_cliente; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ifk_funcao_agrupa_cliente ON public.seg_cliente_funcao USING btree (id_funcao);


--
-- TOC entry 3167 (class 1259 OID 20285)
-- Name: ifk_funcao_descreve_comentario; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ifk_funcao_descreve_comentario ON public.seg_comentario_funcao USING btree (id_funcao);


--
-- TOC entry 3585 (class 1259 OID 20286)
-- Name: ifk_funcao_exerce_usuario; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ifk_funcao_exerce_usuario ON public.seg_usuario_funcao_interna USING btree (id_funcao_interna);


--
-- TOC entry 3150 (class 1259 OID 20287)
-- Name: ifk_funcionario_cliente_funcio; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ifk_funcionario_cliente_funcio ON public.seg_cliente_funcionario USING btree (id_funcionario);


--
-- TOC entry 3042 (class 1259 OID 20288)
-- Name: ifk_funcionario_funcao_realiza; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ifk_funcionario_funcao_realiza ON public.seg_aso USING btree (id_cliente_funcao_funcionario);


--
-- TOC entry 3144 (class 1259 OID 20289)
-- Name: ifk_funcionario_participa_clie; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ifk_funcionario_participa_clie ON public.seg_cliente_funcao_funcionario USING btree (id_funcionario);


--
-- TOC entry 3358 (class 1259 OID 20290)
-- Name: ifk_ghe_agente_controla_epi; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ifk_ghe_agente_controla_epi ON public.seg_ghe_fonte_agente_epi USING btree (id_ghe_fonte_agente);


--
-- TOC entry 3340 (class 1259 OID 20291)
-- Name: ifk_ghe_expoem_fonte; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ifk_ghe_expoem_fonte ON public.seg_ghe_fonte USING btree (id_ghe);


--
-- TOC entry 3365 (class 1259 OID 20292)
-- Name: ifk_ghe_fonte_agente_contempla; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ifk_ghe_fonte_agente_contempla ON public.seg_ghe_fonte_agente_exame USING btree (id_ghe_fonte_agente);


--
-- TOC entry 3447 (class 1259 OID 20293)
-- Name: ifk_ghe_fonte_agente_exame_aso; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ifk_ghe_fonte_agente_exame_aso ON public.seg_movimento USING btree (id_ghe_fonte_agente_exame_aso);


--
-- TOC entry 3371 (class 1259 OID 20294)
-- Name: ifk_ghe_fonte_agente_exame_con; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ifk_ghe_fonte_agente_exame_con ON public.seg_ghe_fonte_agente_exame_aso USING btree (id_ghe_fonte_agente_exame);


--
-- TOC entry 3405 (class 1259 OID 20295)
-- Name: ifk_ghe_fonte_agente_exame_ide; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ifk_ghe_fonte_agente_exame_ide ON public.seg_ghe_setor_cliente_funcao_exame USING btree (id_ghe_fonte_agente_exame);


--
-- TOC entry 3348 (class 1259 OID 20296)
-- Name: ifk_ghe_fonte_especifica_agent; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ifk_ghe_fonte_especifica_agent ON public.seg_ghe_fonte_agente USING btree (id_ghe_fonte);


--
-- TOC entry 3478 (class 1259 OID 20297)
-- Name: ifk_ghe_representa_norma; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ifk_ghe_representa_norma ON public.seg_norma_ghe USING btree (id_ghe);


--
-- TOC entry 3397 (class 1259 OID 20298)
-- Name: ifk_ghe_setor_cliente_funcao_i; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ifk_ghe_setor_cliente_funcao_i ON public.seg_ghe_setor_cliente_funcao USING btree (id_comentario_funcao);


--
-- TOC entry 3406 (class 1259 OID 20299)
-- Name: ifk_ghe_setor_cliente_funcao_r; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ifk_ghe_setor_cliente_funcao_r ON public.seg_ghe_setor_cliente_funcao_exame USING btree (id_ghe_setor_cliente_funcao);


--
-- TOC entry 3398 (class 1259 OID 20300)
-- Name: ifk_ghe_setor_participa_client; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ifk_ghe_setor_participa_client ON public.seg_ghe_setor_cliente_funcao USING btree (id_ghe_setor);


--
-- TOC entry 3390 (class 1259 OID 20301)
-- Name: ifk_ghe_tem_setor; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ifk_ghe_tem_setor ON public.seg_ghe_setor USING btree (id_ghe);


--
-- TOC entry 3386 (class 1259 OID 20302)
-- Name: ifk_ghefonteagentexame_partici; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ifk_ghefonteagentexame_partici ON public.seg_ghe_fonte_agente_exame_periodicidade USING btree (id_ghe_fonte_agente_exame);


--
-- TOC entry 3305 (class 1259 OID 20303)
-- Name: ifk_hospital_atende_estudo; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ifk_hospital_atende_estudo ON public.seg_estudo_hospital USING btree (id_hospital);


--
-- TOC entry 3429 (class 1259 OID 20304)
-- Name: ifk_material_hospitalar_utiliz; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ifk_material_hospitalar_utiliz ON public.seg_material_hospitalar_estudo USING btree (id_material_hospitalar);


--
-- TOC entry 3534 (class 1259 OID 20305)
-- Name: ifk_medico_aprova_relanual; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ifk_medico_aprova_relanual ON public.seg_relanual USING btree (id_medico_coordenador);


--
-- TOC entry 3043 (class 1259 OID 20306)
-- Name: ifk_medico_coordena_aso; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ifk_medico_coordena_aso ON public.seg_aso USING btree (id_medico_coordenador);


--
-- TOC entry 3095 (class 1259 OID 20307)
-- Name: ifk_medico_coordena_cliente; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ifk_medico_coordena_cliente ON public.seg_cliente USING btree (id_medico);


--
-- TOC entry 3044 (class 1259 OID 20308)
-- Name: ifk_medico_examina_aso; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ifk_medico_examina_aso ON public.seg_aso USING btree (id_medico_examinador);


--
-- TOC entry 3311 (class 1259 OID 20309)
-- Name: ifk_medico_examina_estudo; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ifk_medico_examina_estudo ON public.seg_estudo_medico USING btree (id_medico);


--
-- TOC entry 3372 (class 1259 OID 20310)
-- Name: ifk_medico_ghe_fonte_agente_ex; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ifk_medico_ghe_fonte_agente_ex ON public.seg_ghe_fonte_agente_exame_aso USING btree (id_medico);


--
-- TOC entry 3439 (class 1259 OID 20311)
-- Name: ifk_medico_medicoexame; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ifk_medico_medicoexame ON public.seg_medico_exame USING btree (id_medico);


--
-- TOC entry 3630 (class 1259 OID 245176)
-- Name: ifk_monitoramento_agente; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ifk_monitoramento_agente ON public.seg_monitoramento_ghefonteagente USING btree (id_monitoramento);


--
-- TOC entry 3636 (class 1259 OID 245198)
-- Name: ifk_monitoramento_epi; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ifk_monitoramento_epi ON public.seg_epi_monitoramento USING btree (id_monitoramento_ghefonteagente);


--
-- TOC entry 3622 (class 1259 OID 245131)
-- Name: ifk_monitoramento_esocial; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ifk_monitoramento_esocial ON public.seg_esocial_monitoramento USING btree (id_monitoramento);


--
-- TOC entry 3448 (class 1259 OID 20312)
-- Name: ifk_mov_pertence_nfe; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ifk_mov_pertence_nfe ON public.seg_movimento USING btree (id_nfe);


--
-- TOC entry 3466 (class 1259 OID 20313)
-- Name: ifk_nfe_contem_plano_forma; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ifk_nfe_contem_plano_forma ON public.seg_nfe USING btree (id_plano_forma);


--
-- TOC entry 3215 (class 1259 OID 20314)
-- Name: ifk_nfe_gera_crc; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ifk_nfe_gera_crc ON public.seg_crc USING btree (id_nfe);


--
-- TOC entry 3479 (class 1259 OID 20315)
-- Name: ifk_norma_representa_ghe; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ifk_norma_representa_ghe ON public.seg_norma_ghe USING btree (id_norma);


--
-- TOC entry 3591 (class 1259 OID 20316)
-- Name: ifk_perfil_participa_usuario; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ifk_perfil_participa_usuario ON public.seg_usuario_perfil USING btree (id_perfil);


--
-- TOC entry 3491 (class 1259 OID 20317)
-- Name: ifk_perfil_tem_dominio_acao; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ifk_perfil_tem_dominio_acao ON public.seg_perfil_dominio_acao USING btree (id_perfil);


--
-- TOC entry 3140 (class 1259 OID 20318)
-- Name: ifk_periodiciade_atendido_clie; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ifk_periodiciade_atendido_clie ON public.seg_cliente_funcao_exame_periodicidade USING btree (id_periodicidade);


--
-- TOC entry 3387 (class 1259 OID 20319)
-- Name: ifk_periodicidade_participa_gh; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ifk_periodicidade_participa_gh ON public.seg_ghe_fonte_agente_exame_periodicidade USING btree (id_periodicidade);


--
-- TOC entry 3501 (class 1259 OID 20320)
-- Name: ifk_plano_aceita_forma; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ifk_plano_aceita_forma ON public.seg_plano_forma USING btree (id_plano);


--
-- TOC entry 3484 (class 1259 OID 20321)
-- Name: ifk_plano_divide_parcelas; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ifk_plano_divide_parcelas ON public.seg_parcela USING btree (id_plano);


--
-- TOC entry 3335 (class 1259 OID 20330)
-- Name: ifk_ppra_contem_ghe; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ifk_ppra_contem_ghe ON public.seg_ghe USING btree (id_estudo);


--
-- TOC entry 3190 (class 1259 OID 20331)
-- Name: ifk_prestador_estabelece_contr; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ifk_prestador_estabelece_contr ON public.seg_contrato USING btree (id_cliente_prestador);


--
-- TOC entry 3126 (class 1259 OID 20332)
-- Name: ifk_prestador_presta_cliente_f; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ifk_prestador_presta_cliente_f ON public.seg_cliente_funcao_exame_aso USING btree (id_prestador);


--
-- TOC entry 3373 (class 1259 OID 20333)
-- Name: ifk_prestador_presta_ghe_fonte; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ifk_prestador_presta_ghe_fonte ON public.seg_ghe_fonte_agente_exame_aso USING btree (id_prestador);


--
-- TOC entry 3509 (class 1259 OID 20334)
-- Name: ifk_produto_contem_contrato; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ifk_produto_contem_contrato ON public.seg_produto_contrato USING btree (id_produto);


--
-- TOC entry 3449 (class 1259 OID 20335)
-- Name: ifk_produto_contrato_participa; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ifk_produto_contrato_participa ON public.seg_movimento USING btree (id_produto_contrato);


--
-- TOC entry 3255 (class 1259 OID 20336)
-- Name: ifk_prontuario_email; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ifk_prontuario_email ON public.seg_email USING btree (id_prontuario);


--
-- TOC entry 3241 (class 1259 OID 20337)
-- Name: ifk_protocolo_completa_documen; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ifk_protocolo_completa_documen ON public.seg_documento_protocolo USING btree (id_protocolo);


--
-- TOC entry 3127 (class 1259 OID 20338)
-- Name: ifk_protocolo_entrega_cliente_; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ifk_protocolo_entrega_cliente_ ON public.seg_cliente_funcao_exame_aso USING btree (id_protocolo);


--
-- TOC entry 3374 (class 1259 OID 20339)
-- Name: ifk_protocolo_entrega_ghe_font; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ifk_protocolo_entrega_ghe_font ON public.seg_ghe_fonte_agente_exame_aso USING btree (id_protocolo);


--
-- TOC entry 3045 (class 1259 OID 20340)
-- Name: ifk_protocolo_envia_atendiment; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ifk_protocolo_envia_atendiment ON public.seg_aso USING btree (id_protocolo);


--
-- TOC entry 3290 (class 1259 OID 20341)
-- Name: ifk_protocolo_envia_estudo; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ifk_protocolo_envia_estudo ON public.seg_estudo USING btree (id_protocolo);


--
-- TOC entry 3450 (class 1259 OID 20342)
-- Name: ifk_protocolo_envia_produto; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ifk_protocolo_envia_produto ON public.seg_movimento USING btree (id_protocolo);


--
-- TOC entry 3541 (class 1259 OID 20343)
-- Name: ifk_relanaul_tem_item; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ifk_relanaul_tem_item ON public.seg_relanual_item USING btree (id_relanual);


--
-- TOC entry 3606 (class 1259 OID 21613)
-- Name: ifk_relatorio_relatorio_exame; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ifk_relatorio_relatorio_exame ON public.seg_relatorio_exame USING btree (id_relatorio);


--
-- TOC entry 3291 (class 1259 OID 20344)
-- Name: ifk_rep_cli_representa_estudo; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ifk_rep_cli_representa_estudo ON public.seg_estudo USING btree (id_seg_rep_cli);


--
-- TOC entry 3548 (class 1259 OID 20345)
-- Name: ifk_rep_representa_cli; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ifk_rep_representa_cli ON public.seg_rep_cli USING btree (id_vend);


--
-- TOC entry 3553 (class 1259 OID 20346)
-- Name: ifk_rev_versiona_estudo; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ifk_rev_versiona_estudo ON public.seg_revisao USING btree (id_estudo);


--
-- TOC entry 3601 (class 1259 OID 20347)
-- Name: ifk_revisao_localiza_estudo; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ifk_revisao_localiza_estudo ON public.seg_versao USING btree (id_estudo);


--
-- TOC entry 3028 (class 1259 OID 20348)
-- Name: ifk_risco_compoe_agente; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ifk_risco_compoe_agente ON public.seg_agente USING btree (id_risco);


--
-- TOC entry 3564 (class 1259 OID 20349)
-- Name: ifk_sala_atende_exame; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ifk_sala_atende_exame ON public.seg_sala_exame USING btree (id_exame);


--
-- TOC entry 3559 (class 1259 OID 294410)
-- Name: ifk_sala_empresa; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ifk_sala_empresa ON public.seg_sala USING btree (id_empresa);


--
-- TOC entry 3128 (class 1259 OID 20350)
-- Name: ifk_sala_exame_atende_cliente_; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ifk_sala_exame_atende_cliente_ ON public.seg_cliente_funcao_exame_aso USING btree (id_sala_exame);


--
-- TOC entry 3375 (class 1259 OID 20351)
-- Name: ifk_sala_exame_atende_ghe_font; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ifk_sala_exame_atende_ghe_font ON public.seg_ghe_fonte_agente_exame_aso USING btree (id_sala_exame);


--
-- TOC entry 3096 (class 1259 OID 20352)
-- Name: ifk_seg_cidade_ibge_identifica; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ifk_seg_cidade_ibge_identifica ON public.seg_cliente USING btree (id_cidade_ibge);


--
-- TOC entry 3262 (class 1259 OID 20353)
-- Name: ifk_seg_cidade_identifica_cida; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ifk_seg_cidade_identifica_cida ON public.seg_empresa USING btree (id_cidade_ibge);


--
-- TOC entry 3597 (class 1259 OID 294412)
-- Name: ifk_seg_cidade_vendedor; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ifk_seg_cidade_vendedor ON public.seg_vendedor USING btree (id_cidade_ibge);


--
-- TOC entry 3451 (class 1259 OID 20354)
-- Name: ifk_seg_cliente_funcao_exame_a; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ifk_seg_cliente_funcao_exame_a ON public.seg_movimento USING btree (id_cliente_funcao_exame_aso);


--
-- TOC entry 3391 (class 1259 OID 20355)
-- Name: ifk_setor_tem_ghe; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ifk_setor_tem_ghe ON public.seg_ghe_setor USING btree (id_setor);


--
-- TOC entry 3349 (class 1259 OID 20356)
-- Name: ifk_soma_prioriza_ghe_fonte_ag; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ifk_soma_prioriza_ghe_fonte_ag ON public.seg_ghe_fonte_agente USING btree (id_grad_soma);


--
-- TOC entry 3179 (class 1259 OID 20357)
-- Name: ifk_tipo_contato_contato; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ifk_tipo_contato_contato ON public.seg_contato USING btree (id_tipo_contato);


--
-- TOC entry 3046 (class 1259 OID 20358)
-- Name: ifk_usuario_altera_aso; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ifk_usuario_altera_aso ON public.seg_aso USING btree (id_ult_usuario_alteracao);


--
-- TOC entry 3292 (class 1259 OID 20359)
-- Name: ifk_usuario_autoriza_estudo; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ifk_usuario_autoriza_estudo ON public.seg_estudo USING btree (id_engenheiro);


--
-- TOC entry 3216 (class 1259 OID 20360)
-- Name: ifk_usuario_baixa_crc; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ifk_usuario_baixa_crc ON public.seg_crc USING btree (id_usuario_baixa);


--
-- TOC entry 3047 (class 1259 OID 20361)
-- Name: ifk_usuario_cancela_aso; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ifk_usuario_cancela_aso ON public.seg_aso USING btree (id_usuario_cancelamento);


--
-- TOC entry 3217 (class 1259 OID 20362)
-- Name: ifk_usuario_cancela_cobranca; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ifk_usuario_cancela_cobranca ON public.seg_crc USING btree (id_usuario_cancela);


--
-- TOC entry 3467 (class 1259 OID 20363)
-- Name: ifk_usuario_cancela_nfe; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ifk_usuario_cancela_nfe ON public.seg_nfe USING btree (id_usuario_cancela);


--
-- TOC entry 3522 (class 1259 OID 20364)
-- Name: ifk_usuario_cancela_protocolo; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ifk_usuario_cancela_protocolo ON public.seg_protocolo USING btree (id_usuario_cancelou);


--
-- TOC entry 3048 (class 1259 OID 20365)
-- Name: ifk_usuario_cria_aso; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ifk_usuario_cria_aso ON public.seg_aso USING btree (id_usuario_criador);


--
-- TOC entry 3293 (class 1259 OID 20366)
-- Name: ifk_usuario_cria_estudo; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ifk_usuario_cria_estudo ON public.seg_estudo USING btree (id_tecno);


--
-- TOC entry 3468 (class 1259 OID 20367)
-- Name: ifk_usuario_cria_notafiscal; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ifk_usuario_cria_notafiscal ON public.seg_nfe USING btree (id_usuario_criador);


--
-- TOC entry 3516 (class 1259 OID 20368)
-- Name: ifk_usuario_cria_prontuario; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ifk_usuario_cria_prontuario ON public.seg_prontuario USING btree (id_usuario);


--
-- TOC entry 3218 (class 1259 OID 20369)
-- Name: ifk_usuario_desdobra_cobranca; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ifk_usuario_desdobra_cobranca ON public.seg_crc USING btree (id_usuario_desdobra);


--
-- TOC entry 3573 (class 1259 OID 20370)
-- Name: ifk_usuario_e_coordenador; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ifk_usuario_e_coordenador ON public.seg_usuario USING btree (id_medico);


--
-- TOC entry 3256 (class 1259 OID 20371)
-- Name: ifk_usuario_email; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ifk_usuario_email ON public.seg_email USING btree (id_usuario);


--
-- TOC entry 3219 (class 1259 OID 20372)
-- Name: ifk_usuario_estorna_; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ifk_usuario_estorna_ ON public.seg_crc USING btree (id_usuario_estorna);


--
-- TOC entry 3586 (class 1259 OID 20373)
-- Name: ifk_usuario_exerce_funcao; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ifk_usuario_exerce_funcao ON public.seg_usuario_funcao_interna USING btree (id_usuario);


--
-- TOC entry 3049 (class 1259 OID 20374)
-- Name: ifk_usuario_finaliza_aso; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ifk_usuario_finaliza_aso ON public.seg_aso USING btree (id_usuario_finalizador);


--
-- TOC entry 3129 (class 1259 OID 20375)
-- Name: ifk_usuario_finaliza_cliente_f; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ifk_usuario_finaliza_cliente_f ON public.seg_cliente_funcao_exame_aso USING btree (id_usuario);


--
-- TOC entry 3523 (class 1259 OID 20376)
-- Name: ifk_usuario_finaliza_protocolo; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ifk_usuario_finaliza_protocolo ON public.seg_protocolo USING btree (id_usuario_finalizou);


--
-- TOC entry 3376 (class 1259 OID 20377)
-- Name: ifk_usuario_finaliza_seg_ghe_f; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ifk_usuario_finaliza_seg_ghe_f ON public.seg_ghe_fonte_agente_exame_aso USING btree (id_usuario);


--
-- TOC entry 3452 (class 1259 OID 20378)
-- Name: ifk_usuario_gera_movimento; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ifk_usuario_gera_movimento ON public.seg_movimento USING btree (id_usuario);


--
-- TOC entry 3535 (class 1259 OID 20379)
-- Name: ifk_usuario_gera_relanual; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ifk_usuario_gera_relanual ON public.seg_relanual USING btree (id_usuario);


--
-- TOC entry 3220 (class 1259 OID 20380)
-- Name: ifk_usuario_grava_cobranca; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ifk_usuario_grava_cobranca ON public.seg_crc USING btree (id_usuario_criador);


--
-- TOC entry 3524 (class 1259 OID 20381)
-- Name: ifk_usuario_grava_protocolo; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ifk_usuario_grava_protocolo ON public.seg_protocolo USING btree (id_usuario_gerou);


--
-- TOC entry 3615 (class 1259 OID 294404)
-- Name: ifk_usuario_monitoramento; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ifk_usuario_monitoramento ON public.seg_monitoramento USING btree (id_usuario);


--
-- TOC entry 3592 (class 1259 OID 20382)
-- Name: ifk_usuario_participa_perfil; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ifk_usuario_participa_perfil ON public.seg_usuario_perfil USING btree (id_usuario);


--
-- TOC entry 3221 (class 1259 OID 20383)
-- Name: ifk_usuario_prorroga_crc; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ifk_usuario_prorroga_crc ON public.seg_crc USING btree (id_usuario_prorrogacao);


--
-- TOC entry 3207 (class 1259 OID 20384)
-- Name: ifk_usuario_realiza_correcao; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ifk_usuario_realiza_correcao ON public.seg_correcao_estudo USING btree (id_usuario);


--
-- TOC entry 3574 (class 1259 OID 20385)
-- Name: ifk_usuario_trabalha_empresa; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ifk_usuario_trabalha_empresa ON public.seg_usuario USING btree (id_empresa);


--
-- TOC entry 3580 (class 1259 OID 20386)
-- Name: ifk_usuario_usuariocliente; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ifk_usuario_usuariocliente ON public.seg_usuario_cliente USING btree (id_usuario);


--
-- TOC entry 3607 (class 1259 OID 21612)
-- Name: relatorio_exame_id_exame_fk; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX relatorio_exame_id_exame_fk ON public.seg_relatorio_exame USING btree (id_exame);


--
-- TOC entry 3608 (class 1259 OID 21611)
-- Name: relatorio_exame_id_relatorio_fk; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX relatorio_exame_id_relatorio_fk ON public.seg_relatorio_exame USING btree (id_relatorio);


--
-- TOC entry 3023 (class 1259 OID 20387)
-- Name: seg_acesso_login_ip_unq; Type: INDEX; Schema: public; Owner: postgres
--

CREATE UNIQUE INDEX seg_acesso_login_ip_unq ON public.seg_acesso USING btree (login, ip);


--
-- TOC entry 3029 (class 1259 OID 20388)
-- Name: seg_agente_id_risco_fk; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX seg_agente_id_risco_fk ON public.seg_agente USING btree (id_risco);


--
-- TOC entry 3035 (class 1259 OID 20390)
-- Name: seg_arquivo_anexo_id_aso_idx; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX seg_arquivo_anexo_id_aso_idx ON public.seg_arquivo_anexo USING btree (id_aso);


--
-- TOC entry 3067 (class 1259 OID 20391)
-- Name: seg_aso_agente_risco_id_aso_fk; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX seg_aso_agente_risco_id_aso_fk ON public.seg_aso_agente_risco USING btree (id_aso);


--
-- TOC entry 3050 (class 1259 OID 337216)
-- Name: seg_aso_data_aso_idx; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX seg_aso_data_aso_idx ON public.seg_aso USING btree (data_aso);


--
-- TOC entry 3051 (class 1259 OID 294389)
-- Name: seg_aso_fkindex12; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX seg_aso_fkindex12 ON public.seg_aso USING btree (id_centrocusto);


--
-- TOC entry 3069 (class 1259 OID 20392)
-- Name: seg_aso_ghe_setor_id_aso_fk; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX seg_aso_ghe_setor_id_aso_fk ON public.seg_aso_ghe_setor USING btree (id_aso);


--
-- TOC entry 3052 (class 1259 OID 294390)
-- Name: seg_aso_id_centrocusto_fk; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX seg_aso_id_centrocusto_fk ON public.seg_aso USING btree (id_centrocusto);


--
-- TOC entry 3053 (class 1259 OID 20393)
-- Name: seg_aso_id_cliente_funcao_funcionario_fk; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX seg_aso_id_cliente_funcao_funcionario_fk ON public.seg_aso USING btree (id_cliente_funcao_funcionario);


--
-- TOC entry 3054 (class 1259 OID 20394)
-- Name: seg_aso_id_empresa_fk; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX seg_aso_id_empresa_fk ON public.seg_aso USING btree (id_empresa);


--
-- TOC entry 3055 (class 1259 OID 20395)
-- Name: seg_aso_id_medico_coordenador_fk; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX seg_aso_id_medico_coordenador_fk ON public.seg_aso USING btree (id_medico_coordenador);


--
-- TOC entry 3056 (class 1259 OID 20396)
-- Name: seg_aso_id_medico_fk; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX seg_aso_id_medico_fk ON public.seg_aso USING btree (id_medico_examinador);


--
-- TOC entry 3057 (class 1259 OID 20397)
-- Name: seg_aso_id_periodicidade_fk; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX seg_aso_id_periodicidade_fk ON public.seg_aso USING btree (id_periodicidade);


--
-- TOC entry 3058 (class 1259 OID 20398)
-- Name: seg_aso_id_protocolo_fk; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX seg_aso_id_protocolo_fk ON public.seg_aso USING btree (id_protocolo);


--
-- TOC entry 3059 (class 1259 OID 20399)
-- Name: seg_aso_id_usuario_alteracao_fk; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX seg_aso_id_usuario_alteracao_fk ON public.seg_aso USING btree (id_ult_usuario_alteracao);


--
-- TOC entry 3060 (class 1259 OID 20400)
-- Name: seg_aso_id_usuario_cancelamento_fk; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX seg_aso_id_usuario_cancelamento_fk ON public.seg_aso USING btree (id_usuario_cancelamento);


--
-- TOC entry 3061 (class 1259 OID 20401)
-- Name: seg_aso_id_usuario_criador_fk; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX seg_aso_id_usuario_criador_fk ON public.seg_aso USING btree (id_usuario_criador);


--
-- TOC entry 3062 (class 1259 OID 20402)
-- Name: seg_aso_id_usuaro_finalizador_fk; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX seg_aso_id_usuaro_finalizador_fk ON public.seg_aso USING btree (id_usuario_finalizador);


--
-- TOC entry 3065 (class 1259 OID 337217)
-- Name: seg_aso_situacao_idx; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX seg_aso_situacao_idx ON public.seg_aso USING btree (situacao);


--
-- TOC entry 3074 (class 1259 OID 20403)
-- Name: seg_atividade_cronograma_id_atividade_id_atividade_fk; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX seg_atividade_cronograma_id_atividade_id_atividade_fk ON public.seg_atividade_cronograma USING btree (id_atividade);


--
-- TOC entry 3075 (class 1259 OID 20404)
-- Name: seg_atividade_cronograma_id_cronograma_fk; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX seg_atividade_cronograma_id_cronograma_fk ON public.seg_atividade_cronograma USING btree (id_cronograma);


--
-- TOC entry 3081 (class 1259 OID 20407)
-- Name: seg_centrocusto_id_cliente_fk; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX seg_centrocusto_id_cliente_fk ON public.seg_centrocusto USING btree (id_cliente);


--
-- TOC entry 3084 (class 1259 OID 20408)
-- Name: seg_cidade_ibge_codigo_idx; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX seg_cidade_ibge_codigo_idx ON public.seg_cidade_ibge USING btree (codigo);


--
-- TOC entry 3089 (class 1259 OID 20409)
-- Name: seg_cli_cnae_id_cliente_fk; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX seg_cli_cnae_id_cliente_fk ON public.seg_cli_cnae USING btree (id_cliente);


--
-- TOC entry 3090 (class 1259 OID 20410)
-- Name: seg_cli_cnae_id_cnae_fk; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX seg_cli_cnae_id_cnae_fk ON public.seg_cli_cnae USING btree (id_cnae);


--
-- TOC entry 3107 (class 1259 OID 20411)
-- Name: seg_cliente_arquivo_id_arquivo_fk; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX seg_cliente_arquivo_id_arquivo_fk ON public.seg_cliente_arquivo USING btree (id_arquivo);


--
-- TOC entry 3108 (class 1259 OID 20412)
-- Name: seg_cliente_arquivo_id_cliente_fk; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX seg_cliente_arquivo_id_cliente_fk ON public.seg_cliente_arquivo USING btree (id_cliente);


--
-- TOC entry 3109 (class 1259 OID 20413)
-- Name: seg_cliente_arquivo_id_cliente_id_arquivo_unique; Type: INDEX; Schema: public; Owner: postgres
--

CREATE UNIQUE INDEX seg_cliente_arquivo_id_cliente_id_arquivo_unique ON public.seg_cliente_arquivo USING btree (id_arquivo, id_cliente);


--
-- TOC entry 3130 (class 1259 OID 20414)
-- Name: seg_cliente_funcao_exame_aso_id_aso_fk; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX seg_cliente_funcao_exame_aso_id_aso_fk ON public.seg_cliente_funcao_exame_aso USING btree (id_aso);


--
-- TOC entry 3131 (class 1259 OID 20415)
-- Name: seg_cliente_funcao_exame_aso_id_cliente_funcao_exame_fk; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX seg_cliente_funcao_exame_aso_id_cliente_funcao_exame_fk ON public.seg_cliente_funcao_exame_aso USING btree (id_cliente_funcao_exame);


--
-- TOC entry 3132 (class 1259 OID 20416)
-- Name: seg_cliente_funcao_exame_aso_id_medico_fk; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX seg_cliente_funcao_exame_aso_id_medico_fk ON public.seg_cliente_funcao_exame_aso USING btree (id_medico);


--
-- TOC entry 3133 (class 1259 OID 20417)
-- Name: seg_cliente_funcao_exame_aso_id_prestador_fk; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX seg_cliente_funcao_exame_aso_id_prestador_fk ON public.seg_cliente_funcao_exame_aso USING btree (id_prestador);


--
-- TOC entry 3134 (class 1259 OID 20418)
-- Name: seg_cliente_funcao_exame_aso_id_protocolo_fk; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX seg_cliente_funcao_exame_aso_id_protocolo_fk ON public.seg_cliente_funcao_exame_aso USING btree (id_protocolo);


--
-- TOC entry 3135 (class 1259 OID 20419)
-- Name: seg_cliente_funcao_exame_aso_id_sala_exame_fk; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX seg_cliente_funcao_exame_aso_id_sala_exame_fk ON public.seg_cliente_funcao_exame_aso USING btree (id_sala_exame);


--
-- TOC entry 3136 (class 1259 OID 20420)
-- Name: seg_cliente_funcao_exame_aso_id_usuario_fk; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX seg_cliente_funcao_exame_aso_id_usuario_fk ON public.seg_cliente_funcao_exame_aso USING btree (id_usuario);


--
-- TOC entry 3120 (class 1259 OID 20421)
-- Name: seg_cliente_funcao_exame_id_cliente_funcao_fk; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX seg_cliente_funcao_exame_id_cliente_funcao_fk ON public.seg_cliente_funcao_exame USING btree (id_cliente_funcao);


--
-- TOC entry 3121 (class 1259 OID 20422)
-- Name: seg_cliente_funcao_exame_id_exame_fk; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX seg_cliente_funcao_exame_id_exame_fk ON public.seg_cliente_funcao_exame USING btree (id_exame);


--
-- TOC entry 3141 (class 1259 OID 20423)
-- Name: seg_cliente_funcao_exame_id_periodicidade_fk; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX seg_cliente_funcao_exame_id_periodicidade_fk ON public.seg_cliente_funcao_exame_periodicidade USING btree (id_periodicidade);


--
-- TOC entry 3142 (class 1259 OID 20424)
-- Name: seg_cliente_funcao_exame_periodicidade_id_cliente_funcao_exame_; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX seg_cliente_funcao_exame_periodicidade_id_cliente_funcao_exame_ ON public.seg_cliente_funcao_exame_periodicidade USING btree (id_cliente_funcao_exame);


--
-- TOC entry 3145 (class 1259 OID 20425)
-- Name: seg_cliente_funcao_funcionario_id_funcionario_fk; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX seg_cliente_funcao_funcionario_id_funcionario_fk ON public.seg_cliente_funcao_funcionario USING btree (id_funcionario);


--
-- TOC entry 3146 (class 1259 OID 20426)
-- Name: seg_cliente_funcao_funcionario_id_seg_cliente_funcao_fk; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX seg_cliente_funcao_funcionario_id_seg_cliente_funcao_fk ON public.seg_cliente_funcao_funcionario USING btree (id_seg_cliente_funcao);


--
-- TOC entry 3114 (class 1259 OID 20427)
-- Name: seg_cliente_funcao_id_cliente_fk; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX seg_cliente_funcao_id_cliente_fk ON public.seg_cliente_funcao USING btree (id_cliente);


--
-- TOC entry 3115 (class 1259 OID 20428)
-- Name: seg_cliente_funcao_id_funcao_fk; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX seg_cliente_funcao_id_funcao_fk ON public.seg_cliente_funcao USING btree (id_funcao);


--
-- TOC entry 3151 (class 1259 OID 20429)
-- Name: seg_cliente_funcionario_id_cliente_fk; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX seg_cliente_funcionario_id_cliente_fk ON public.seg_cliente_funcionario USING btree (id_cliente);


--
-- TOC entry 3152 (class 1259 OID 20430)
-- Name: seg_cliente_funcionario_id_funcionario_fk; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX seg_cliente_funcionario_id_funcionario_fk ON public.seg_cliente_funcionario USING btree (id_funcionario);


--
-- TOC entry 3097 (class 1259 OID 20431)
-- Name: seg_cliente_id_cidade_ibge_cobranca_fk; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX seg_cliente_id_cidade_ibge_cobranca_fk ON public.seg_cliente USING btree (id_cidade_ibge_cobranca);


--
-- TOC entry 3098 (class 1259 OID 20432)
-- Name: seg_cliente_id_cidade_ibge_fk; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX seg_cliente_id_cidade_ibge_fk ON public.seg_cliente USING btree (id_cidade_ibge);


--
-- TOC entry 3099 (class 1259 OID 20433)
-- Name: seg_cliente_id_cliente_credenciado_fk; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX seg_cliente_id_cliente_credenciado_fk ON public.seg_cliente USING btree (id_cliente_credenciado);


--
-- TOC entry 3100 (class 1259 OID 20434)
-- Name: seg_cliente_id_cliente_matriz_fk; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX seg_cliente_id_cliente_matriz_fk ON public.seg_cliente USING btree (id_cliente_matriz);


--
-- TOC entry 3101 (class 1259 OID 20435)
-- Name: seg_cliente_id_medico_fk; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX seg_cliente_id_medico_fk ON public.seg_cliente USING btree (id_medico);


--
-- TOC entry 3102 (class 1259 OID 20436)
-- Name: seg_cliente_id_ramo_fk; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX seg_cliente_id_ramo_fk ON public.seg_cliente USING btree (id_ramo);


--
-- TOC entry 3163 (class 1259 OID 20437)
-- Name: seg_cnae_estudo_id_cnae_fk; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX seg_cnae_estudo_id_cnae_fk ON public.seg_cnae_estudo USING btree (id_cnae);


--
-- TOC entry 3164 (class 1259 OID 20438)
-- Name: seg_cnae_estudo_id_estudo_fk; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX seg_cnae_estudo_id_estudo_fk ON public.seg_cnae_estudo USING btree (id_estudo);


--
-- TOC entry 3168 (class 1259 OID 20439)
-- Name: seg_comentario_funcao_id_funcao_fk; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX seg_comentario_funcao_id_funcao_fk ON public.seg_comentario_funcao USING btree (id_funcao);


--
-- TOC entry 3174 (class 1259 OID 20440)
-- Name: seg_conta_id_banco_fk; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX seg_conta_id_banco_fk ON public.seg_conta USING btree (id_banco);


--
-- TOC entry 3180 (class 1259 OID 20441)
-- Name: seg_contato_id_cidade; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX seg_contato_id_cidade ON public.seg_contato USING btree (id_cidade);


--
-- TOC entry 3181 (class 1259 OID 20442)
-- Name: seg_contato_id_cliente_fk; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX seg_contato_id_cliente_fk ON public.seg_contato USING btree (id_cliente);


--
-- TOC entry 3182 (class 1259 OID 20443)
-- Name: seg_contato_id_tipo_contato_fk; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX seg_contato_id_tipo_contato_fk ON public.seg_contato USING btree (id_tipo_contato);


--
-- TOC entry 3201 (class 1259 OID 20444)
-- Name: seg_contrato_exame_id_contrato_fk; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX seg_contrato_exame_id_contrato_fk ON public.seg_contrato_exame USING btree (id_contrato);


--
-- TOC entry 3202 (class 1259 OID 20445)
-- Name: seg_contrato_exame_id_exame_fk; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX seg_contrato_exame_id_exame_fk ON public.seg_contrato_exame USING btree (id_exame);


--
-- TOC entry 3205 (class 1259 OID 20446)
-- Name: seg_contrato_exame_unique; Type: INDEX; Schema: public; Owner: postgres
--

CREATE UNIQUE INDEX seg_contrato_exame_unique ON public.seg_contrato_exame USING btree (id_exame, id_contrato);


--
-- TOC entry 3191 (class 1259 OID 20447)
-- Name: seg_contrato_id_cliente_fk; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX seg_contrato_id_cliente_fk ON public.seg_contrato USING btree (id_cliente);


--
-- TOC entry 3192 (class 1259 OID 20448)
-- Name: seg_contrato_id_cliente_prestador_fk; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX seg_contrato_id_cliente_prestador_fk ON public.seg_contrato USING btree (id_cliente_prestador);


--
-- TOC entry 3193 (class 1259 OID 20449)
-- Name: seg_contrato_id_cliente_proposta_fk; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX seg_contrato_id_cliente_proposta_fk ON public.seg_contrato USING btree (id_cliente_proposta);


--
-- TOC entry 3194 (class 1259 OID 20450)
-- Name: seg_contrato_id_contrato_pai_fk; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX seg_contrato_id_contrato_pai_fk ON public.seg_contrato USING btree (id_contrato_pai);


--
-- TOC entry 3195 (class 1259 OID 20451)
-- Name: seg_contrato_id_contrato_raiz_fk; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX seg_contrato_id_contrato_raiz_fk ON public.seg_contrato USING btree (id_contrato_raiz);


--
-- TOC entry 3196 (class 1259 OID 294392)
-- Name: seg_contrato_id_empresa_fk; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX seg_contrato_id_empresa_fk ON public.seg_contrato USING btree (id_empresa);


--
-- TOC entry 3208 (class 1259 OID 20452)
-- Name: seg_correcao_estudo_id_estudo_fk; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX seg_correcao_estudo_id_estudo_fk ON public.seg_correcao_estudo USING btree (id_estudo);


--
-- TOC entry 3209 (class 1259 OID 20453)
-- Name: seg_correcao_estudo_id_usuario_fk; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX seg_correcao_estudo_id_usuario_fk ON public.seg_correcao_estudo USING btree (id_usuario);


--
-- TOC entry 3222 (class 1259 OID 20454)
-- Name: seg_crc_id_conta_fk; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX seg_crc_id_conta_fk ON public.seg_crc USING btree (id_conta);


--
-- TOC entry 3223 (class 1259 OID 20455)
-- Name: seg_crc_id_crc_desdobramento_fk; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX seg_crc_id_crc_desdobramento_fk ON public.seg_crc USING btree (id_crc_desdobramento);


--
-- TOC entry 3224 (class 1259 OID 20456)
-- Name: seg_crc_id_forma_fk; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX seg_crc_id_forma_fk ON public.seg_crc USING btree (id_forma);


--
-- TOC entry 3225 (class 1259 OID 20457)
-- Name: seg_crc_id_nfe_fk; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX seg_crc_id_nfe_fk ON public.seg_crc USING btree (id_nfe);


--
-- TOC entry 3226 (class 1259 OID 20458)
-- Name: seg_crc_id_usuario_baixa_fk; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX seg_crc_id_usuario_baixa_fk ON public.seg_crc USING btree (id_usuario_baixa);


--
-- TOC entry 3227 (class 1259 OID 20459)
-- Name: seg_crc_id_usuario_cancela_fk; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX seg_crc_id_usuario_cancela_fk ON public.seg_crc USING btree (id_usuario_cancela);


--
-- TOC entry 3228 (class 1259 OID 20460)
-- Name: seg_crc_id_usuario_criador_fk; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX seg_crc_id_usuario_criador_fk ON public.seg_crc USING btree (id_usuario_criador);


--
-- TOC entry 3229 (class 1259 OID 20461)
-- Name: seg_crc_id_usuario_desdobra_fk; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX seg_crc_id_usuario_desdobra_fk ON public.seg_crc USING btree (id_usuario_desdobra);


--
-- TOC entry 3230 (class 1259 OID 20462)
-- Name: seg_crc_id_usuario_estorna_fk; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX seg_crc_id_usuario_estorna_fk ON public.seg_crc USING btree (id_usuario_estorna);


--
-- TOC entry 3231 (class 1259 OID 20463)
-- Name: seg_crc_id_usuario_prorrogacao_fk; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX seg_crc_id_usuario_prorrogacao_fk ON public.seg_crc USING btree (id_usuario_prorrogacao);


--
-- TOC entry 3234 (class 1259 OID 20464)
-- Name: seg_crc_unique; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX seg_crc_unique ON public.seg_crc USING btree (id_conta, id_nfe);


--
-- TOC entry 3242 (class 1259 OID 20465)
-- Name: seg_documento_protocolo_id_aso_fk; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX seg_documento_protocolo_id_aso_fk ON public.seg_documento_protocolo USING btree (id_aso);


--
-- TOC entry 3243 (class 1259 OID 20466)
-- Name: seg_documento_protocolo_id_documento_fk; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX seg_documento_protocolo_id_documento_fk ON public.seg_documento_protocolo USING btree (id_documento);


--
-- TOC entry 3244 (class 1259 OID 20467)
-- Name: seg_documento_protocolo_id_protocolo_fk; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX seg_documento_protocolo_id_protocolo_fk ON public.seg_documento_protocolo USING btree (id_protocolo);


--
-- TOC entry 3251 (class 1259 OID 20468)
-- Name: seg_dominio_acao_id_acao_fk; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX seg_dominio_acao_id_acao_fk ON public.seg_dominio_acao USING btree (id_acao);


--
-- TOC entry 3252 (class 1259 OID 20469)
-- Name: seg_dominio_acao_id_dominio_fk; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX seg_dominio_acao_id_dominio_fk ON public.seg_dominio_acao USING btree (id_dominio);


--
-- TOC entry 3257 (class 1259 OID 20470)
-- Name: seg_email_id_usuario_fk; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX seg_email_id_usuario_fk ON public.seg_email USING btree (id_usuario);


--
-- TOC entry 3260 (class 1259 OID 20471)
-- Name: seg_email_prontuario_id_fk; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX seg_email_prontuario_id_fk ON public.seg_email USING btree (id_prontuario);


--
-- TOC entry 3263 (class 1259 OID 20472)
-- Name: seg_empresa_id_cidade_ibge_fk; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX seg_empresa_id_cidade_ibge_fk ON public.seg_empresa USING btree (id_cidade_ibge);


--
-- TOC entry 3264 (class 1259 OID 20473)
-- Name: seg_empresa_id_empresa_matriz_fk; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX seg_empresa_id_empresa_matriz_fk ON public.seg_empresa USING btree (id_empresa_matriz);


--
-- TOC entry 3637 (class 1259 OID 294393)
-- Name: seg_epi_monitoramento_id_epi_fk; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX seg_epi_monitoramento_id_epi_fk ON public.seg_epi_monitoramento USING btree (id_epi);


--
-- TOC entry 3638 (class 1259 OID 294394)
-- Name: seg_epi_monitoramento_id_monitoramento_ghe_fonteagente_fk; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX seg_epi_monitoramento_id_monitoramento_ghe_fonteagente_fk ON public.seg_epi_monitoramento USING btree (id_monitoramento_ghefonteagente);


--
-- TOC entry 3275 (class 1259 OID 294399)
-- Name: seg_esocial_lote_aso_id_aso_fk; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX seg_esocial_lote_aso_id_aso_fk ON public.seg_esocial_lote_aso USING btree (id_aso);


--
-- TOC entry 3276 (class 1259 OID 294400)
-- Name: seg_esocial_lote_aso_id_esocial_lote_fk; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX seg_esocial_lote_aso_id_esocial_lote_fk ON public.seg_esocial_lote_aso USING btree (id_esocial_lote);


--
-- TOC entry 3270 (class 1259 OID 294396)
-- Name: seg_esocial_lote_id_cliente_fk; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX seg_esocial_lote_id_cliente_fk ON public.seg_esocial_lote USING btree (id_cliente);


--
-- TOC entry 3623 (class 1259 OID 245130)
-- Name: seg_esocial_monitoramento_id_esocial_lote_fk; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX seg_esocial_monitoramento_id_esocial_lote_fk ON public.seg_esocial_monitoramento USING btree (id_esocial_lote);


--
-- TOC entry 3624 (class 1259 OID 245129)
-- Name: seg_esocial_monitoramento_id_monitoramento_fk; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX seg_esocial_monitoramento_id_monitoramento_fk ON public.seg_esocial_monitoramento USING btree (id_monitoramento);


--
-- TOC entry 3294 (class 1259 OID 20474)
-- Name: seg_estudo_cod_estudo_unique; Type: INDEX; Schema: public; Owner: postgres
--

CREATE UNIQUE INDEX seg_estudo_cod_estudo_unique ON public.seg_estudo USING btree (cod_estudo);


--
-- TOC entry 3306 (class 1259 OID 20475)
-- Name: seg_estudo_hospital_id_estudo_fk; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX seg_estudo_hospital_id_estudo_fk ON public.seg_estudo_hospital USING btree (id_estudo);


--
-- TOC entry 3307 (class 1259 OID 20476)
-- Name: seg_estudo_hospital_id_hospital_fk; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX seg_estudo_hospital_id_hospital_fk ON public.seg_estudo_hospital USING btree (id_hospital);


--
-- TOC entry 3295 (class 1259 OID 20477)
-- Name: seg_estudo_id_cliente_contratado_fk; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX seg_estudo_id_cliente_contratado_fk ON public.seg_estudo USING btree (id_cliente_contratado);


--
-- TOC entry 3296 (class 1259 OID 20478)
-- Name: seg_estudo_id_cronograma_unique; Type: INDEX; Schema: public; Owner: postgres
--

CREATE UNIQUE INDEX seg_estudo_id_cronograma_unique ON public.seg_estudo USING btree (id_cronograma);


--
-- TOC entry 3297 (class 1259 OID 20479)
-- Name: seg_estudo_id_engenheiro_fk; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX seg_estudo_id_engenheiro_fk ON public.seg_estudo USING btree (id_engenheiro);


--
-- TOC entry 3298 (class 1259 OID 20480)
-- Name: seg_estudo_id_protocolo_fk; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX seg_estudo_id_protocolo_fk ON public.seg_estudo USING btree (id_protocolo);


--
-- TOC entry 3299 (class 1259 OID 20481)
-- Name: seg_estudo_id_relatorio_fk; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX seg_estudo_id_relatorio_fk ON public.seg_estudo USING btree (id_relatorio);


--
-- TOC entry 3300 (class 1259 OID 20482)
-- Name: seg_estudo_id_tecno_fk; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX seg_estudo_id_tecno_fk ON public.seg_estudo USING btree (id_tecno);


--
-- TOC entry 3312 (class 1259 OID 20483)
-- Name: seg_estudo_medico_id_estudo_fk; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX seg_estudo_medico_id_estudo_fk ON public.seg_estudo_medico USING btree (id_estudo);


--
-- TOC entry 3313 (class 1259 OID 20484)
-- Name: seg_estudo_medico_id_medico_fk; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX seg_estudo_medico_id_medico_fk ON public.seg_estudo_medico USING btree (id_medico);


--
-- TOC entry 3303 (class 1259 OID 20485)
-- Name: seg_estudo_rep_cli_id_seg_rep_cli_fk; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX seg_estudo_rep_cli_id_seg_rep_cli_fk ON public.seg_estudo USING btree (id_seg_rep_cli);


--
-- TOC entry 3325 (class 1259 OID 20486)
-- Name: seg_funcao_epi_id_estudo_fk; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX seg_funcao_epi_id_estudo_fk ON public.seg_funcao_epi USING btree (id_estudo);


--
-- TOC entry 3328 (class 1259 OID 20487)
-- Name: seg_funcao_epi_unique; Type: INDEX; Schema: public; Owner: postgres
--

CREATE UNIQUE INDEX seg_funcao_epi_unique ON public.seg_funcao_epi USING btree (nome_funcao, nome_epi, id_estudo);


--
-- TOC entry 3332 (class 1259 OID 20488)
-- Name: seg_funcionario_id_cidade; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX seg_funcionario_id_cidade ON public.seg_funcionario USING btree (id_cidade);


--
-- TOC entry 3359 (class 1259 OID 20489)
-- Name: seg_ghe_fonte_agente_epi_id_epi_fk; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX seg_ghe_fonte_agente_epi_id_epi_fk ON public.seg_ghe_fonte_agente_epi USING btree (id_epi);


--
-- TOC entry 3360 (class 1259 OID 20490)
-- Name: seg_ghe_fonte_agente_epi_id_ghe_fonte_agente_fk; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX seg_ghe_fonte_agente_epi_id_ghe_fonte_agente_fk ON public.seg_ghe_fonte_agente_epi USING btree (id_ghe_fonte_agente);


--
-- TOC entry 3363 (class 1259 OID 20491)
-- Name: seg_ghe_fonte_agente_epi_unique; Type: INDEX; Schema: public; Owner: postgres
--

CREATE UNIQUE INDEX seg_ghe_fonte_agente_epi_unique ON public.seg_ghe_fonte_agente_epi USING btree (id_ghe_fonte_agente, id_epi, classificacao);


--
-- TOC entry 3377 (class 1259 OID 20492)
-- Name: seg_ghe_fonte_agente_exame_aso_id_aso_fk; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX seg_ghe_fonte_agente_exame_aso_id_aso_fk ON public.seg_ghe_fonte_agente_exame_aso USING btree (id_aso);


--
-- TOC entry 3378 (class 1259 OID 20493)
-- Name: seg_ghe_fonte_agente_exame_aso_id_ghe_fonte_agente_exame_fk; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX seg_ghe_fonte_agente_exame_aso_id_ghe_fonte_agente_exame_fk ON public.seg_ghe_fonte_agente_exame_aso USING btree (id_ghe_fonte_agente_exame);


--
-- TOC entry 3379 (class 1259 OID 20494)
-- Name: seg_ghe_fonte_agente_exame_aso_id_medico_fk; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX seg_ghe_fonte_agente_exame_aso_id_medico_fk ON public.seg_ghe_fonte_agente_exame_aso USING btree (id_medico);


--
-- TOC entry 3380 (class 1259 OID 20495)
-- Name: seg_ghe_fonte_agente_exame_aso_id_prestador_fk; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX seg_ghe_fonte_agente_exame_aso_id_prestador_fk ON public.seg_ghe_fonte_agente_exame_aso USING btree (id_prestador);


--
-- TOC entry 3381 (class 1259 OID 20496)
-- Name: seg_ghe_fonte_agente_exame_aso_id_protocolo_fk; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX seg_ghe_fonte_agente_exame_aso_id_protocolo_fk ON public.seg_ghe_fonte_agente_exame_aso USING btree (id_protocolo);


--
-- TOC entry 3382 (class 1259 OID 20497)
-- Name: seg_ghe_fonte_agente_exame_aso_id_sala_exame_fk; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX seg_ghe_fonte_agente_exame_aso_id_sala_exame_fk ON public.seg_ghe_fonte_agente_exame_aso USING btree (id_sala_exame);


--
-- TOC entry 3383 (class 1259 OID 20498)
-- Name: seg_ghe_fonte_agente_exame_aso_id_usuario_fk; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX seg_ghe_fonte_agente_exame_aso_id_usuario_fk ON public.seg_ghe_fonte_agente_exame_aso USING btree (id_usuario);


--
-- TOC entry 3366 (class 1259 OID 20499)
-- Name: seg_ghe_fonte_agente_exame_id_exame_fk; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX seg_ghe_fonte_agente_exame_id_exame_fk ON public.seg_ghe_fonte_agente_exame USING btree (id_exame);


--
-- TOC entry 3388 (class 1259 OID 20500)
-- Name: seg_ghe_fonte_agente_exame_periodicidade_id_ghe_fonte_agente_ex; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX seg_ghe_fonte_agente_exame_periodicidade_id_ghe_fonte_agente_ex ON public.seg_ghe_fonte_agente_exame_periodicidade USING btree (id_ghe_fonte_agente_exame);


--
-- TOC entry 3389 (class 1259 OID 20501)
-- Name: seg_ghe_fonte_agente_exame_periodicidade_id_periodicidade_fk; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX seg_ghe_fonte_agente_exame_periodicidade_id_periodicidade_fk ON public.seg_ghe_fonte_agente_exame_periodicidade USING btree (id_periodicidade);


--
-- TOC entry 3369 (class 1259 OID 20502)
-- Name: seg_ghe_fonte_agente_exames_id_ghe_fonte_agente_fk; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX seg_ghe_fonte_agente_exames_id_ghe_fonte_agente_fk ON public.seg_ghe_fonte_agente_exame USING btree (id_ghe_fonte_agente);


--
-- TOC entry 3350 (class 1259 OID 20503)
-- Name: seg_ghe_fonte_agente_id_agente_fk; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX seg_ghe_fonte_agente_id_agente_fk ON public.seg_ghe_fonte_agente USING btree (id_agente);


--
-- TOC entry 3351 (class 1259 OID 20504)
-- Name: seg_ghe_fonte_agente_id_ghe_fonte_fk; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX seg_ghe_fonte_agente_id_ghe_fonte_fk ON public.seg_ghe_fonte_agente USING btree (id_ghe_fonte);


--
-- TOC entry 3352 (class 1259 OID 20505)
-- Name: seg_ghe_fonte_agente_id_grad_efeito_fk; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX seg_ghe_fonte_agente_id_grad_efeito_fk ON public.seg_ghe_fonte_agente USING btree (id_grad_efeito);


--
-- TOC entry 3353 (class 1259 OID 20506)
-- Name: seg_ghe_fonte_agente_id_grad_exposicao_fk; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX seg_ghe_fonte_agente_id_grad_exposicao_fk ON public.seg_ghe_fonte_agente USING btree (id_grad_exposicao);


--
-- TOC entry 3354 (class 1259 OID 20507)
-- Name: seg_ghe_fonte_agente_id_grad_soma_fk; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX seg_ghe_fonte_agente_id_grad_soma_fk ON public.seg_ghe_fonte_agente USING btree (id_grad_soma);


--
-- TOC entry 3341 (class 1259 OID 20508)
-- Name: seg_ghe_fonte_id_fonte_fk; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX seg_ghe_fonte_id_fonte_fk ON public.seg_ghe_fonte USING btree (id_fonte);


--
-- TOC entry 3342 (class 1259 OID 20509)
-- Name: seg_ghe_fonte_id_ghe_fk; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX seg_ghe_fonte_id_ghe_fk ON public.seg_ghe_fonte USING btree (id_ghe);


--
-- TOC entry 3336 (class 1259 OID 20510)
-- Name: seg_ghe_id_estudo_fk; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX seg_ghe_id_estudo_fk ON public.seg_ghe USING btree (id_estudo);


--
-- TOC entry 3407 (class 1259 OID 20511)
-- Name: seg_ghe_setor_cliente_funcao_exame_id_exame_fk; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX seg_ghe_setor_cliente_funcao_exame_id_exame_fk ON public.seg_ghe_setor_cliente_funcao_exame USING btree (id_exame);


--
-- TOC entry 3408 (class 1259 OID 20512)
-- Name: seg_ghe_setor_cliente_funcao_exame_id_ghe_fonte_agente_exame_fk; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX seg_ghe_setor_cliente_funcao_exame_id_ghe_fonte_agente_exame_fk ON public.seg_ghe_setor_cliente_funcao_exame USING btree (id_ghe_fonte_agente_exame);


--
-- TOC entry 3409 (class 1259 OID 20513)
-- Name: seg_ghe_setor_cliente_funcao_exame_id_ghe_setor_cliente_funcao_; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX seg_ghe_setor_cliente_funcao_exame_id_ghe_setor_cliente_funcao_ ON public.seg_ghe_setor_cliente_funcao_exame USING btree (id_ghe_setor_cliente_funcao);


--
-- TOC entry 3399 (class 1259 OID 20514)
-- Name: seg_ghe_setor_cliente_funcao_id_comentario_funcao_fk; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX seg_ghe_setor_cliente_funcao_id_comentario_funcao_fk ON public.seg_ghe_setor_cliente_funcao USING btree (id_comentario_funcao);


--
-- TOC entry 3400 (class 1259 OID 20515)
-- Name: seg_ghe_setor_cliente_funcao_id_ghe_setor_fk; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX seg_ghe_setor_cliente_funcao_id_ghe_setor_fk ON public.seg_ghe_setor_cliente_funcao USING btree (id_ghe_setor);


--
-- TOC entry 3401 (class 1259 OID 20516)
-- Name: seg_ghe_setor_cliente_funcao_id_seg_cliente_funcao_fk; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX seg_ghe_setor_cliente_funcao_id_seg_cliente_funcao_fk ON public.seg_ghe_setor_cliente_funcao USING btree (id_seg_cliente_funcao);


--
-- TOC entry 3392 (class 1259 OID 20517)
-- Name: seg_ghe_setor_id_ghe_fk; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX seg_ghe_setor_id_ghe_fk ON public.seg_ghe_setor USING btree (id_ghe);


--
-- TOC entry 3393 (class 1259 OID 20518)
-- Name: seg_ghe_setor_id_setor_fk; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX seg_ghe_setor_id_setor_fk ON public.seg_ghe_setor USING btree (id_setor);


--
-- TOC entry 3419 (class 1259 OID 294402)
-- Name: seg_hospital_id_cidade_fk; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX seg_hospital_id_cidade_fk ON public.seg_hospital USING btree (id_cidade);


--
-- TOC entry 3432 (class 1259 OID 20519)
-- Name: seg_material_hospitalar_id_estudo_fk; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX seg_material_hospitalar_id_estudo_fk ON public.seg_material_hospitalar_estudo USING btree (id_estudo);


--
-- TOC entry 3433 (class 1259 OID 20520)
-- Name: seg_material_hospitalar_id_material_hospitalar_fk; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX seg_material_hospitalar_id_material_hospitalar_fk ON public.seg_material_hospitalar_estudo USING btree (id_material_hospitalar);


--
-- TOC entry 3440 (class 1259 OID 20521)
-- Name: seg_medico_exame_id_exame_fk; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX seg_medico_exame_id_exame_fk ON public.seg_medico_exame USING btree (id_exame);


--
-- TOC entry 3441 (class 1259 OID 20522)
-- Name: seg_medico_exame_id_medico_fk; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX seg_medico_exame_id_medico_fk ON public.seg_medico_exame USING btree (id_medico);


--
-- TOC entry 3435 (class 1259 OID 20523)
-- Name: seg_medico_id_cidade_fk; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX seg_medico_id_cidade_fk ON public.seg_medico USING btree (id_cidade);


--
-- TOC entry 3631 (class 1259 OID 294407)
-- Name: seg_monitoramento_ghefonteagente_id_ghe_fonte_agente_fk; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX seg_monitoramento_ghefonteagente_id_ghe_fonte_agente_fk ON public.seg_monitoramento_ghefonteagente USING btree (id_ghe_fonte_agente);


--
-- TOC entry 3632 (class 1259 OID 294408)
-- Name: seg_monitoramento_ghefonteagente_id_monitoramento_fk; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX seg_monitoramento_ghefonteagente_id_monitoramento_fk ON public.seg_monitoramento_ghefonteagente USING btree (id_monitoramento);


--
-- TOC entry 3616 (class 1259 OID 294405)
-- Name: seg_monitoramento_id_cliente_funcao_funcionario_fk; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX seg_monitoramento_id_cliente_funcao_funcionario_fk ON public.seg_monitoramento USING btree (id_cliente_funcao_funcionario);


--
-- TOC entry 3617 (class 1259 OID 294406)
-- Name: seg_monitoramento_id_usuario_fk; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX seg_monitoramento_id_usuario_fk ON public.seg_monitoramento USING btree (id_usuario);


--
-- TOC entry 3618 (class 1259 OID 245108)
-- Name: seg_monitoramento_monitoramento_fk; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX seg_monitoramento_monitoramento_fk ON public.seg_monitoramento USING btree (id_monitoramento_anterior);


--
-- TOC entry 3453 (class 1259 OID 20524)
-- Name: seg_movimento_id_cliente_fk; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX seg_movimento_id_cliente_fk ON public.seg_movimento USING btree (id_cliente);


--
-- TOC entry 3454 (class 1259 OID 20525)
-- Name: seg_movimento_id_cliente_funcao_exame_aso_fk; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX seg_movimento_id_cliente_funcao_exame_aso_fk ON public.seg_movimento USING btree (id_cliente_funcao_exame_aso);


--
-- TOC entry 3455 (class 1259 OID 20526)
-- Name: seg_movimento_id_cliente_unidade_fk; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX seg_movimento_id_cliente_unidade_fk ON public.seg_movimento USING btree (id_cliente_unidade);


--
-- TOC entry 3456 (class 1259 OID 20527)
-- Name: seg_movimento_id_empresa_fk; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX seg_movimento_id_empresa_fk ON public.seg_movimento USING btree (id_empresa);


--
-- TOC entry 3457 (class 1259 OID 20528)
-- Name: seg_movimento_id_ghe_fonte_agente_exame_aso_fk; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX seg_movimento_id_ghe_fonte_agente_exame_aso_fk ON public.seg_movimento USING btree (id_ghe_fonte_agente_exame_aso);


--
-- TOC entry 3458 (class 1259 OID 20529)
-- Name: seg_movimento_id_nfe_fk; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX seg_movimento_id_nfe_fk ON public.seg_movimento USING btree (id_nfe);


--
-- TOC entry 3459 (class 1259 OID 20530)
-- Name: seg_movimento_id_produto_contrato_fk; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX seg_movimento_id_produto_contrato_fk ON public.seg_movimento USING btree (id_produto_contrato);


--
-- TOC entry 3460 (class 1259 OID 20531)
-- Name: seg_movimento_id_protocolo_fk; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX seg_movimento_id_protocolo_fk ON public.seg_movimento USING btree (id_protocolo);


--
-- TOC entry 3461 (class 1259 OID 20532)
-- Name: seg_movimento_id_usuario_fk; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX seg_movimento_id_usuario_fk ON public.seg_movimento USING btree (id_usuario);


--
-- TOC entry 3469 (class 1259 OID 20533)
-- Name: seg_nfe_id_cliente_fk; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX seg_nfe_id_cliente_fk ON public.seg_nfe USING btree (id_cliente);


--
-- TOC entry 3470 (class 1259 OID 20534)
-- Name: seg_nfe_id_empresa_fk; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX seg_nfe_id_empresa_fk ON public.seg_nfe USING btree (id_empresa);


--
-- TOC entry 3471 (class 1259 OID 20535)
-- Name: seg_nfe_id_plano_forma_fk; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX seg_nfe_id_plano_forma_fk ON public.seg_nfe USING btree (id_plano_forma);


--
-- TOC entry 3472 (class 1259 OID 20536)
-- Name: seg_nfe_id_usuario_cancela_fk; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX seg_nfe_id_usuario_cancela_fk ON public.seg_nfe USING btree (id_usuario_cancela);


--
-- TOC entry 3473 (class 1259 OID 20537)
-- Name: seg_nfe_id_usuario_criador_fk; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX seg_nfe_id_usuario_criador_fk ON public.seg_nfe USING btree (id_usuario_criador);


--
-- TOC entry 3480 (class 1259 OID 20538)
-- Name: seg_norma_ghe_id_ghe_fk; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX seg_norma_ghe_id_ghe_fk ON public.seg_norma_ghe USING btree (id_ghe);


--
-- TOC entry 3481 (class 1259 OID 20539)
-- Name: seg_norma_ghe_id_norma_fk; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX seg_norma_ghe_id_norma_fk ON public.seg_norma_ghe USING btree (id_norma);


--
-- TOC entry 3485 (class 1259 OID 20540)
-- Name: seg_parcela_id_plano_fk; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX seg_parcela_id_plano_fk ON public.seg_parcela USING btree (id_plano);


--
-- TOC entry 3492 (class 1259 OID 20541)
-- Name: seg_perfil_dominio_acao_id_dominio_id_acao_idx; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX seg_perfil_dominio_acao_id_dominio_id_acao_idx ON public.seg_perfil_dominio_acao USING btree (id_dominio, id_acao);


--
-- TOC entry 3493 (class 1259 OID 20542)
-- Name: seg_perfil_dominio_acao_id_perfil_fk; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX seg_perfil_dominio_acao_id_perfil_fk ON public.seg_perfil_dominio_acao USING btree (id_perfil);


--
-- TOC entry 3502 (class 1259 OID 20543)
-- Name: seg_plano_forma_id_forma_fk; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX seg_plano_forma_id_forma_fk ON public.seg_plano_forma USING btree (id_forma);


--
-- TOC entry 3503 (class 1259 OID 20544)
-- Name: seg_plano_forma_id_plano_fk; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX seg_plano_forma_id_plano_fk ON public.seg_plano_forma USING btree (id_plano);


--
-- TOC entry 3510 (class 1259 OID 20552)
-- Name: seg_produto_contrato_id_contrato_fk; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX seg_produto_contrato_id_contrato_fk ON public.seg_produto_contrato USING btree (id_contrato);


--
-- TOC entry 3511 (class 1259 OID 20553)
-- Name: seg_produto_contrato_id_produto_fk; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX seg_produto_contrato_id_produto_fk ON public.seg_produto_contrato USING btree (id_produto);


--
-- TOC entry 3514 (class 1259 OID 20554)
-- Name: seg_produto_contrato_unique; Type: INDEX; Schema: public; Owner: postgres
--

CREATE UNIQUE INDEX seg_produto_contrato_unique ON public.seg_produto_contrato USING btree (id_produto, id_contrato);


--
-- TOC entry 3517 (class 1259 OID 20556)
-- Name: seg_prontuario_id_aso_fk; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX seg_prontuario_id_aso_fk ON public.seg_prontuario USING btree (id_aso);


--
-- TOC entry 3518 (class 1259 OID 294409)
-- Name: seg_prontuario_id_usuario_fk; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX seg_prontuario_id_usuario_fk ON public.seg_prontuario USING btree (id_usuario);


--
-- TOC entry 3525 (class 1259 OID 20557)
-- Name: seg_protocolo_id_cliente_fk; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX seg_protocolo_id_cliente_fk ON public.seg_protocolo USING btree (id_cliente);


--
-- TOC entry 3526 (class 1259 OID 20558)
-- Name: seg_protocolo_id_usuario_cancelou_fk; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX seg_protocolo_id_usuario_cancelou_fk ON public.seg_protocolo USING btree (id_usuario_cancelou);


--
-- TOC entry 3527 (class 1259 OID 20559)
-- Name: seg_protocolo_id_usuario_finalizou_fk; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX seg_protocolo_id_usuario_finalizou_fk ON public.seg_protocolo USING btree (id_usuario_finalizou);


--
-- TOC entry 3528 (class 1259 OID 20560)
-- Name: seg_protocolo_id_usuario_gerou_fk; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX seg_protocolo_id_usuario_gerou_fk ON public.seg_protocolo USING btree (id_usuario_gerou);


--
-- TOC entry 3536 (class 1259 OID 20561)
-- Name: seg_relanual_id_cliente_fk; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX seg_relanual_id_cliente_fk ON public.seg_relanual USING btree (id_cliente);


--
-- TOC entry 3537 (class 1259 OID 20562)
-- Name: seg_relanual_id_medico_fk; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX seg_relanual_id_medico_fk ON public.seg_relanual USING btree (id_medico_coordenador);


--
-- TOC entry 3538 (class 1259 OID 20563)
-- Name: seg_relanual_id_usuario_fk; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX seg_relanual_id_usuario_fk ON public.seg_relanual USING btree (id_usuario);


--
-- TOC entry 3544 (class 1259 OID 20564)
-- Name: seg_relanual_iten_id_relatorio_fk; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX seg_relanual_iten_id_relatorio_fk ON public.seg_relanual_item USING btree (id_relanual);


--
-- TOC entry 3549 (class 1259 OID 20565)
-- Name: seg_rep_cli_id_cliente_fk; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX seg_rep_cli_id_cliente_fk ON public.seg_rep_cli USING btree (id_cliente);


--
-- TOC entry 3550 (class 1259 OID 20566)
-- Name: seg_rep_cli_id_vend_fk; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX seg_rep_cli_id_vend_fk ON public.seg_rep_cli USING btree (id_vend);


--
-- TOC entry 3554 (class 1259 OID 20568)
-- Name: seg_revisao_id_estudo_fk; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX seg_revisao_id_estudo_fk ON public.seg_revisao USING btree (id_estudo);


--
-- TOC entry 3565 (class 1259 OID 20569)
-- Name: seg_sala_exame_id_exane_fk; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX seg_sala_exame_id_exane_fk ON public.seg_sala_exame USING btree (id_exame);


--
-- TOC entry 3566 (class 1259 OID 20570)
-- Name: seg_sala_exame_id_sala_fk; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX seg_sala_exame_id_sala_fk ON public.seg_sala_exame USING btree (id_sala);


--
-- TOC entry 3560 (class 1259 OID 294411)
-- Name: seg_sala_id_empresa; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX seg_sala_id_empresa ON public.seg_sala USING btree (id_empresa);


--
-- TOC entry 3581 (class 1259 OID 20571)
-- Name: seg_usuario_cliente_id_cliente_fk; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX seg_usuario_cliente_id_cliente_fk ON public.seg_usuario_cliente USING btree (id_cliente);


--
-- TOC entry 3582 (class 1259 OID 20572)
-- Name: seg_usuario_cliente_id_usuario_fk; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX seg_usuario_cliente_id_usuario_fk ON public.seg_usuario_cliente USING btree (id_usuario);


--
-- TOC entry 3587 (class 1259 OID 20573)
-- Name: seg_usuario_funcao_interna_id_funcao_interna_fk; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX seg_usuario_funcao_interna_id_funcao_interna_fk ON public.seg_usuario_funcao_interna USING btree (id_funcao_interna);


--
-- TOC entry 3588 (class 1259 OID 20574)
-- Name: seg_usuario_funcao_interna_id_usuario_fk; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX seg_usuario_funcao_interna_id_usuario_fk ON public.seg_usuario_funcao_interna USING btree (id_usuario);


--
-- TOC entry 3575 (class 1259 OID 20575)
-- Name: seg_usuario_id_empresa_fk; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX seg_usuario_id_empresa_fk ON public.seg_usuario USING btree (id_empresa);


--
-- TOC entry 3576 (class 1259 OID 20576)
-- Name: seg_usuario_id_medico_fk; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX seg_usuario_id_medico_fk ON public.seg_usuario USING btree (id_medico);


--
-- TOC entry 3593 (class 1259 OID 20577)
-- Name: seg_usuario_id_perfil_fk; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX seg_usuario_id_perfil_fk ON public.seg_usuario_perfil USING btree (id_perfil);


--
-- TOC entry 3594 (class 1259 OID 20578)
-- Name: seg_usuario_id_usuario_fk; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX seg_usuario_id_usuario_fk ON public.seg_usuario_perfil USING btree (id_usuario);


--
-- TOC entry 3598 (class 1259 OID 294413)
-- Name: seg_vendedor_id_cidade_ibge_fk; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX seg_vendedor_id_cidade_ibge_fk ON public.seg_vendedor USING btree (id_cidade_ibge);


--
-- TOC entry 3602 (class 1259 OID 20579)
-- Name: seg_versao_id_estudo_fk; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX seg_versao_id_estudo_fk ON public.seg_versao USING btree (id_estudo);


--
-- TOC entry 3651 (class 2606 OID 20590)
-- Name: seg_agente seg_agente_id_risco_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_agente
    ADD CONSTRAINT seg_agente_id_risco_fkey FOREIGN KEY (id_risco) REFERENCES public.seg_risco(id_risco);


--
-- TOC entry 3652 (class 2606 OID 20600)
-- Name: seg_arquivo_anexo seg_arquivo_anexo_id_aso_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_arquivo_anexo
    ADD CONSTRAINT seg_arquivo_anexo_id_aso_fkey FOREIGN KEY (id_aso) REFERENCES public.seg_aso(id_aso);


--
-- TOC entry 3664 (class 2606 OID 20605)
-- Name: seg_aso_agente_risco seg_aso_agente_risco_id_aso_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_aso_agente_risco
    ADD CONSTRAINT seg_aso_agente_risco_id_aso_fkey FOREIGN KEY (id_aso) REFERENCES public.seg_aso(id_aso);


--
-- TOC entry 3665 (class 2606 OID 20610)
-- Name: seg_aso_ghe_setor seg_aso_ghe_setor_id_aso_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_aso_ghe_setor
    ADD CONSTRAINT seg_aso_ghe_setor_id_aso_fkey FOREIGN KEY (id_aso) REFERENCES public.seg_aso(id_aso);


--
-- TOC entry 3663 (class 2606 OID 294342)
-- Name: seg_aso seg_aso_id_centrocusto_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_aso
    ADD CONSTRAINT seg_aso_id_centrocusto_fkey FOREIGN KEY (id_centrocusto) REFERENCES public.seg_centrocusto(id);


--
-- TOC entry 3653 (class 2606 OID 20615)
-- Name: seg_aso seg_aso_id_cliente_funcao_funcionario_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_aso
    ADD CONSTRAINT seg_aso_id_cliente_funcao_funcionario_fkey FOREIGN KEY (id_cliente_funcao_funcionario) REFERENCES public.seg_cliente_funcao_funcionario(id_cliente_funcao_funcionario);


--
-- TOC entry 3654 (class 2606 OID 20620)
-- Name: seg_aso seg_aso_id_empresa_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_aso
    ADD CONSTRAINT seg_aso_id_empresa_fkey FOREIGN KEY (id_empresa) REFERENCES public.seg_empresa(id_empresa);


--
-- TOC entry 3655 (class 2606 OID 20625)
-- Name: seg_aso seg_aso_id_medico_coordenador_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_aso
    ADD CONSTRAINT seg_aso_id_medico_coordenador_fkey FOREIGN KEY (id_medico_coordenador) REFERENCES public.seg_medico(id_medico);


--
-- TOC entry 3656 (class 2606 OID 20630)
-- Name: seg_aso seg_aso_id_medico_examinador_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_aso
    ADD CONSTRAINT seg_aso_id_medico_examinador_fkey FOREIGN KEY (id_medico_examinador) REFERENCES public.seg_medico(id_medico);


--
-- TOC entry 3657 (class 2606 OID 20635)
-- Name: seg_aso seg_aso_id_periodicidade_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_aso
    ADD CONSTRAINT seg_aso_id_periodicidade_fkey FOREIGN KEY (id_periodicidade) REFERENCES public.seg_periodicidade(id_periodicidade);


--
-- TOC entry 3658 (class 2606 OID 20640)
-- Name: seg_aso seg_aso_id_protocolo_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_aso
    ADD CONSTRAINT seg_aso_id_protocolo_fkey FOREIGN KEY (id_protocolo) REFERENCES public.seg_protocolo(id_protocolo);


--
-- TOC entry 3659 (class 2606 OID 20645)
-- Name: seg_aso seg_aso_id_ult_usuario_alteracao_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_aso
    ADD CONSTRAINT seg_aso_id_ult_usuario_alteracao_fkey FOREIGN KEY (id_ult_usuario_alteracao) REFERENCES public.seg_usuario(id_usuario);


--
-- TOC entry 3660 (class 2606 OID 20650)
-- Name: seg_aso seg_aso_id_usuario_cancelamento_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_aso
    ADD CONSTRAINT seg_aso_id_usuario_cancelamento_fkey FOREIGN KEY (id_usuario_cancelamento) REFERENCES public.seg_usuario(id_usuario);


--
-- TOC entry 3661 (class 2606 OID 20655)
-- Name: seg_aso seg_aso_id_usuario_criador_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_aso
    ADD CONSTRAINT seg_aso_id_usuario_criador_fkey FOREIGN KEY (id_usuario_criador) REFERENCES public.seg_usuario(id_usuario);


--
-- TOC entry 3662 (class 2606 OID 20660)
-- Name: seg_aso seg_aso_id_usuario_finalizador_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_aso
    ADD CONSTRAINT seg_aso_id_usuario_finalizador_fkey FOREIGN KEY (id_usuario_finalizador) REFERENCES public.seg_usuario(id_usuario);


--
-- TOC entry 3666 (class 2606 OID 20665)
-- Name: seg_atividade_cronograma seg_atividade_cronograma_id_atividade_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_atividade_cronograma
    ADD CONSTRAINT seg_atividade_cronograma_id_atividade_fkey FOREIGN KEY (id_atividade) REFERENCES public.seg_atividade(id_atividade);


--
-- TOC entry 3667 (class 2606 OID 20670)
-- Name: seg_atividade_cronograma seg_atividade_cronograma_id_cronograma_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_atividade_cronograma
    ADD CONSTRAINT seg_atividade_cronograma_id_cronograma_fkey FOREIGN KEY (id_cronograma) REFERENCES public.seg_cronograma(id_cronograma) ON DELETE CASCADE;


--
-- TOC entry 3668 (class 2606 OID 20685)
-- Name: seg_centrocusto seg_centrocusto_id_cliente_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_centrocusto
    ADD CONSTRAINT seg_centrocusto_id_cliente_fkey FOREIGN KEY (id_cliente) REFERENCES public.seg_cliente(id_cliente) ON DELETE CASCADE;


--
-- TOC entry 3669 (class 2606 OID 20690)
-- Name: seg_cli_cnae seg_cli_cnae_id_cliente_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_cli_cnae
    ADD CONSTRAINT seg_cli_cnae_id_cliente_fkey FOREIGN KEY (id_cliente) REFERENCES public.seg_cliente(id_cliente);


--
-- TOC entry 3670 (class 2606 OID 20695)
-- Name: seg_cli_cnae seg_cli_cnae_id_cnae_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_cli_cnae
    ADD CONSTRAINT seg_cli_cnae_id_cnae_fkey FOREIGN KEY (id_cnae) REFERENCES public.seg_cnae(id_cnae);


--
-- TOC entry 3677 (class 2606 OID 20700)
-- Name: seg_cliente_arquivo seg_cliente_arquivo_id_arquivo_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_cliente_arquivo
    ADD CONSTRAINT seg_cliente_arquivo_id_arquivo_fkey FOREIGN KEY (id_arquivo) REFERENCES public.seg_arquivo(id_arquivo);


--
-- TOC entry 3678 (class 2606 OID 20705)
-- Name: seg_cliente_arquivo seg_cliente_arquivo_id_cliente_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_cliente_arquivo
    ADD CONSTRAINT seg_cliente_arquivo_id_cliente_fkey FOREIGN KEY (id_cliente) REFERENCES public.seg_cliente(id_cliente);


--
-- TOC entry 3683 (class 2606 OID 20710)
-- Name: seg_cliente_funcao_exame_aso seg_cliente_funcao_exame_aso_id_aso_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_cliente_funcao_exame_aso
    ADD CONSTRAINT seg_cliente_funcao_exame_aso_id_aso_fkey FOREIGN KEY (id_aso) REFERENCES public.seg_aso(id_aso) ON DELETE CASCADE;


--
-- TOC entry 3684 (class 2606 OID 20715)
-- Name: seg_cliente_funcao_exame_aso seg_cliente_funcao_exame_aso_id_cliente_funcao_exame_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_cliente_funcao_exame_aso
    ADD CONSTRAINT seg_cliente_funcao_exame_aso_id_cliente_funcao_exame_fkey FOREIGN KEY (id_cliente_funcao_exame) REFERENCES public.seg_cliente_funcao_exame(id_cliente_funcao_exame) ON DELETE CASCADE;


--
-- TOC entry 3685 (class 2606 OID 20720)
-- Name: seg_cliente_funcao_exame_aso seg_cliente_funcao_exame_aso_id_prestador_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_cliente_funcao_exame_aso
    ADD CONSTRAINT seg_cliente_funcao_exame_aso_id_prestador_fkey FOREIGN KEY (id_prestador) REFERENCES public.seg_cliente(id_cliente);


--
-- TOC entry 3686 (class 2606 OID 20725)
-- Name: seg_cliente_funcao_exame_aso seg_cliente_funcao_exame_aso_id_protocolo_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_cliente_funcao_exame_aso
    ADD CONSTRAINT seg_cliente_funcao_exame_aso_id_protocolo_fkey FOREIGN KEY (id_protocolo) REFERENCES public.seg_protocolo(id_protocolo);


--
-- TOC entry 3687 (class 2606 OID 20730)
-- Name: seg_cliente_funcao_exame_aso seg_cliente_funcao_exame_aso_id_sala_exame_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_cliente_funcao_exame_aso
    ADD CONSTRAINT seg_cliente_funcao_exame_aso_id_sala_exame_fkey FOREIGN KEY (id_sala_exame) REFERENCES public.seg_sala_exame(id_sala_exame);


--
-- TOC entry 3688 (class 2606 OID 20735)
-- Name: seg_cliente_funcao_exame_aso seg_cliente_funcao_exame_aso_id_usuario_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_cliente_funcao_exame_aso
    ADD CONSTRAINT seg_cliente_funcao_exame_aso_id_usuario_fkey FOREIGN KEY (id_usuario) REFERENCES public.seg_usuario(id_usuario);


--
-- TOC entry 3681 (class 2606 OID 20740)
-- Name: seg_cliente_funcao_exame seg_cliente_funcao_exame_id_cliente_funcao_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_cliente_funcao_exame
    ADD CONSTRAINT seg_cliente_funcao_exame_id_cliente_funcao_fkey FOREIGN KEY (id_cliente_funcao) REFERENCES public.seg_cliente_funcao(id_seg_cliente_funcao) ON DELETE CASCADE;


--
-- TOC entry 3682 (class 2606 OID 20745)
-- Name: seg_cliente_funcao_exame seg_cliente_funcao_exame_id_exame_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_cliente_funcao_exame
    ADD CONSTRAINT seg_cliente_funcao_exame_id_exame_fkey FOREIGN KEY (id_exame) REFERENCES public.seg_exame(id_exame) ON DELETE CASCADE;


--
-- TOC entry 3689 (class 2606 OID 20750)
-- Name: seg_cliente_funcao_exame_periodicidade seg_cliente_funcao_exame_periodici_id_cliente_funcao_exame_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_cliente_funcao_exame_periodicidade
    ADD CONSTRAINT seg_cliente_funcao_exame_periodici_id_cliente_funcao_exame_fkey FOREIGN KEY (id_cliente_funcao_exame) REFERENCES public.seg_cliente_funcao_exame(id_cliente_funcao_exame) ON DELETE CASCADE;


--
-- TOC entry 3690 (class 2606 OID 20755)
-- Name: seg_cliente_funcao_exame_periodicidade seg_cliente_funcao_exame_periodicidade_id_periodicidade_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_cliente_funcao_exame_periodicidade
    ADD CONSTRAINT seg_cliente_funcao_exame_periodicidade_id_periodicidade_fkey FOREIGN KEY (id_periodicidade) REFERENCES public.seg_periodicidade(id_periodicidade) ON DELETE CASCADE;


--
-- TOC entry 3691 (class 2606 OID 20760)
-- Name: seg_cliente_funcao_funcionario seg_cliente_funcao_funcionario_id_funcionario_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_cliente_funcao_funcionario
    ADD CONSTRAINT seg_cliente_funcao_funcionario_id_funcionario_fkey FOREIGN KEY (id_funcionario) REFERENCES public.seg_funcionario(id_funcionario) ON DELETE CASCADE;


--
-- TOC entry 3692 (class 2606 OID 20765)
-- Name: seg_cliente_funcao_funcionario seg_cliente_funcao_funcionario_id_seg_cliente_funcao_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_cliente_funcao_funcionario
    ADD CONSTRAINT seg_cliente_funcao_funcionario_id_seg_cliente_funcao_fkey FOREIGN KEY (id_seg_cliente_funcao) REFERENCES public.seg_cliente_funcao(id_seg_cliente_funcao);


--
-- TOC entry 3679 (class 2606 OID 20770)
-- Name: seg_cliente_funcao seg_cliente_funcao_id_cliente_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_cliente_funcao
    ADD CONSTRAINT seg_cliente_funcao_id_cliente_fkey FOREIGN KEY (id_cliente) REFERENCES public.seg_cliente(id_cliente);


--
-- TOC entry 3680 (class 2606 OID 20775)
-- Name: seg_cliente_funcao seg_cliente_funcao_id_funcao_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_cliente_funcao
    ADD CONSTRAINT seg_cliente_funcao_id_funcao_fkey FOREIGN KEY (id_funcao) REFERENCES public.seg_funcao(id_funcao);


--
-- TOC entry 3693 (class 2606 OID 20780)
-- Name: seg_cliente_funcionario seg_cliente_funcionario_id_cliente_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_cliente_funcionario
    ADD CONSTRAINT seg_cliente_funcionario_id_cliente_fkey FOREIGN KEY (id_cliente) REFERENCES public.seg_cliente(id_cliente);


--
-- TOC entry 3694 (class 2606 OID 20785)
-- Name: seg_cliente_funcionario seg_cliente_funcionario_id_funcionario_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_cliente_funcionario
    ADD CONSTRAINT seg_cliente_funcionario_id_funcionario_fkey FOREIGN KEY (id_funcionario) REFERENCES public.seg_funcionario(id_funcionario);


--
-- TOC entry 3671 (class 2606 OID 20790)
-- Name: seg_cliente seg_cliente_id_cidade_ibge_cobranca_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_cliente
    ADD CONSTRAINT seg_cliente_id_cidade_ibge_cobranca_fkey FOREIGN KEY (id_cidade_ibge_cobranca) REFERENCES public.seg_cidade_ibge(id_cidade_ibge);


--
-- TOC entry 3672 (class 2606 OID 20795)
-- Name: seg_cliente seg_cliente_id_cidade_ibge_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_cliente
    ADD CONSTRAINT seg_cliente_id_cidade_ibge_fkey FOREIGN KEY (id_cidade_ibge) REFERENCES public.seg_cidade_ibge(id_cidade_ibge);


--
-- TOC entry 3673 (class 2606 OID 20800)
-- Name: seg_cliente seg_cliente_id_cliente_credenciado_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_cliente
    ADD CONSTRAINT seg_cliente_id_cliente_credenciado_fkey FOREIGN KEY (id_cliente_credenciado) REFERENCES public.seg_cliente(id_cliente);


--
-- TOC entry 3674 (class 2606 OID 20805)
-- Name: seg_cliente seg_cliente_id_cliente_matriz_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_cliente
    ADD CONSTRAINT seg_cliente_id_cliente_matriz_fkey FOREIGN KEY (id_cliente_matriz) REFERENCES public.seg_cliente(id_cliente);


--
-- TOC entry 3675 (class 2606 OID 20810)
-- Name: seg_cliente seg_cliente_id_medico_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_cliente
    ADD CONSTRAINT seg_cliente_id_medico_fkey FOREIGN KEY (id_medico) REFERENCES public.seg_medico(id_medico);


--
-- TOC entry 3676 (class 2606 OID 20815)
-- Name: seg_cliente seg_cliente_id_ramo_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_cliente
    ADD CONSTRAINT seg_cliente_id_ramo_fkey FOREIGN KEY (id_ramo) REFERENCES public.seg_ramo(id);


--
-- TOC entry 3695 (class 2606 OID 20820)
-- Name: seg_cliente_proposta seg_cliente_proposta_id_cidade_ibge_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_cliente_proposta
    ADD CONSTRAINT seg_cliente_proposta_id_cidade_ibge_fkey FOREIGN KEY (id_cidade_ibge) REFERENCES public.seg_cidade_ibge(id_cidade_ibge);


--
-- TOC entry 3696 (class 2606 OID 20825)
-- Name: seg_cnae_estudo seg_cnae_estudo_id_cnae_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_cnae_estudo
    ADD CONSTRAINT seg_cnae_estudo_id_cnae_fkey FOREIGN KEY (id_cnae) REFERENCES public.seg_cnae(id_cnae);


--
-- TOC entry 3697 (class 2606 OID 20830)
-- Name: seg_cnae_estudo seg_cnae_estudo_id_estudo_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_cnae_estudo
    ADD CONSTRAINT seg_cnae_estudo_id_estudo_fkey FOREIGN KEY (id_estudo) REFERENCES public.seg_estudo(id_estudo) ON DELETE CASCADE;


--
-- TOC entry 3698 (class 2606 OID 20835)
-- Name: seg_comentario_funcao seg_comentario_funcao_id_funcao_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_comentario_funcao
    ADD CONSTRAINT seg_comentario_funcao_id_funcao_fkey FOREIGN KEY (id_funcao) REFERENCES public.seg_funcao(id_funcao);


--
-- TOC entry 3699 (class 2606 OID 20840)
-- Name: seg_conta seg_conta_id_banco_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_conta
    ADD CONSTRAINT seg_conta_id_banco_fkey FOREIGN KEY (id_banco) REFERENCES public.seg_banco(id_banco);


--
-- TOC entry 3700 (class 2606 OID 20845)
-- Name: seg_contato seg_contato_id_cidade_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_contato
    ADD CONSTRAINT seg_contato_id_cidade_fkey FOREIGN KEY (id_cidade) REFERENCES public.seg_cidade_ibge(id_cidade_ibge);


--
-- TOC entry 3701 (class 2606 OID 20850)
-- Name: seg_contato seg_contato_id_cliente_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_contato
    ADD CONSTRAINT seg_contato_id_cliente_fkey FOREIGN KEY (id_cliente) REFERENCES public.seg_cliente(id_cliente) ON DELETE CASCADE;


--
-- TOC entry 3702 (class 2606 OID 20855)
-- Name: seg_contato seg_contato_id_tipo_contato_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_contato
    ADD CONSTRAINT seg_contato_id_tipo_contato_fkey FOREIGN KEY (id_tipo_contato) REFERENCES public.seg_tipo_contato(id);


--
-- TOC entry 3709 (class 2606 OID 20860)
-- Name: seg_contrato_exame seg_contrato_exame_id_contrato_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_contrato_exame
    ADD CONSTRAINT seg_contrato_exame_id_contrato_fkey FOREIGN KEY (id_contrato) REFERENCES public.seg_contrato(id_contrato) ON DELETE CASCADE;


--
-- TOC entry 3710 (class 2606 OID 20865)
-- Name: seg_contrato_exame seg_contrato_exame_id_exame_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_contrato_exame
    ADD CONSTRAINT seg_contrato_exame_id_exame_fkey FOREIGN KEY (id_exame) REFERENCES public.seg_exame(id_exame) ON DELETE CASCADE;


--
-- TOC entry 3703 (class 2606 OID 20870)
-- Name: seg_contrato seg_contrato_id_cliente_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_contrato
    ADD CONSTRAINT seg_contrato_id_cliente_fkey FOREIGN KEY (id_cliente) REFERENCES public.seg_cliente(id_cliente);


--
-- TOC entry 3704 (class 2606 OID 20875)
-- Name: seg_contrato seg_contrato_id_cliente_prestador_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_contrato
    ADD CONSTRAINT seg_contrato_id_cliente_prestador_fkey FOREIGN KEY (id_cliente_prestador) REFERENCES public.seg_cliente(id_cliente);


--
-- TOC entry 3705 (class 2606 OID 20880)
-- Name: seg_contrato seg_contrato_id_cliente_proposta_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_contrato
    ADD CONSTRAINT seg_contrato_id_cliente_proposta_fkey FOREIGN KEY (id_cliente_proposta) REFERENCES public.seg_cliente_proposta(id_cliente_proposta);


--
-- TOC entry 3706 (class 2606 OID 20885)
-- Name: seg_contrato seg_contrato_id_contrato_pai_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_contrato
    ADD CONSTRAINT seg_contrato_id_contrato_pai_fkey FOREIGN KEY (id_contrato_pai) REFERENCES public.seg_contrato(id_contrato);


--
-- TOC entry 3707 (class 2606 OID 20890)
-- Name: seg_contrato seg_contrato_id_contrato_raiz_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_contrato
    ADD CONSTRAINT seg_contrato_id_contrato_raiz_fkey FOREIGN KEY (id_contrato_raiz) REFERENCES public.seg_contrato(id_contrato);


--
-- TOC entry 3708 (class 2606 OID 20895)
-- Name: seg_contrato seg_contrato_id_empresa_fk; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_contrato
    ADD CONSTRAINT seg_contrato_id_empresa_fk FOREIGN KEY (id_empresa) REFERENCES public.seg_empresa(id_empresa);


--
-- TOC entry 3711 (class 2606 OID 20900)
-- Name: seg_correcao_estudo seg_correcao_estudo_id_estudo_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_correcao_estudo
    ADD CONSTRAINT seg_correcao_estudo_id_estudo_fkey FOREIGN KEY (id_estudo) REFERENCES public.seg_estudo(id_estudo);


--
-- TOC entry 3712 (class 2606 OID 20905)
-- Name: seg_correcao_estudo seg_correcao_estudo_id_usuario_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_correcao_estudo
    ADD CONSTRAINT seg_correcao_estudo_id_usuario_fkey FOREIGN KEY (id_usuario) REFERENCES public.seg_usuario(id_usuario);


--
-- TOC entry 3713 (class 2606 OID 20910)
-- Name: seg_crc seg_crc_id_conta_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_crc
    ADD CONSTRAINT seg_crc_id_conta_fkey FOREIGN KEY (id_conta) REFERENCES public.seg_conta(id_conta);


--
-- TOC entry 3714 (class 2606 OID 20915)
-- Name: seg_crc seg_crc_id_crc_desdobramento_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_crc
    ADD CONSTRAINT seg_crc_id_crc_desdobramento_fkey FOREIGN KEY (id_crc_desdobramento) REFERENCES public.seg_crc(id_crc);


--
-- TOC entry 3715 (class 2606 OID 20920)
-- Name: seg_crc seg_crc_id_forma_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_crc
    ADD CONSTRAINT seg_crc_id_forma_fkey FOREIGN KEY (id_forma) REFERENCES public.seg_forma(id_forma);


--
-- TOC entry 3716 (class 2606 OID 20925)
-- Name: seg_crc seg_crc_id_nfe_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_crc
    ADD CONSTRAINT seg_crc_id_nfe_fkey FOREIGN KEY (id_nfe) REFERENCES public.seg_nfe(id_nfe);


--
-- TOC entry 3717 (class 2606 OID 20930)
-- Name: seg_crc seg_crc_id_usuario_baixa_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_crc
    ADD CONSTRAINT seg_crc_id_usuario_baixa_fkey FOREIGN KEY (id_usuario_baixa) REFERENCES public.seg_usuario(id_usuario);


--
-- TOC entry 3718 (class 2606 OID 20935)
-- Name: seg_crc seg_crc_id_usuario_cancela_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_crc
    ADD CONSTRAINT seg_crc_id_usuario_cancela_fkey FOREIGN KEY (id_usuario_cancela) REFERENCES public.seg_usuario(id_usuario);


--
-- TOC entry 3719 (class 2606 OID 20940)
-- Name: seg_crc seg_crc_id_usuario_criador_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_crc
    ADD CONSTRAINT seg_crc_id_usuario_criador_fkey FOREIGN KEY (id_usuario_criador) REFERENCES public.seg_usuario(id_usuario);


--
-- TOC entry 3720 (class 2606 OID 20945)
-- Name: seg_crc seg_crc_id_usuario_desdobra_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_crc
    ADD CONSTRAINT seg_crc_id_usuario_desdobra_fkey FOREIGN KEY (id_usuario_desdobra) REFERENCES public.seg_usuario(id_usuario);


--
-- TOC entry 3721 (class 2606 OID 20950)
-- Name: seg_crc seg_crc_id_usuario_estorna_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_crc
    ADD CONSTRAINT seg_crc_id_usuario_estorna_fkey FOREIGN KEY (id_usuario_estorna) REFERENCES public.seg_usuario(id_usuario);


--
-- TOC entry 3722 (class 2606 OID 20955)
-- Name: seg_crc seg_crc_id_usuario_prorrogacao_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_crc
    ADD CONSTRAINT seg_crc_id_usuario_prorrogacao_fkey FOREIGN KEY (id_usuario_prorrogacao) REFERENCES public.seg_usuario(id_usuario);


--
-- TOC entry 3845 (class 2606 OID 327383)
-- Name: seg_documento_estudo seg_documento_estudo_id_documento_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_documento_estudo
    ADD CONSTRAINT seg_documento_estudo_id_documento_fkey FOREIGN KEY (id_documento) REFERENCES public.seg_documento(id_documento);


--
-- TOC entry 3846 (class 2606 OID 327388)
-- Name: seg_documento_estudo seg_documento_estudo_id_estudo_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_documento_estudo
    ADD CONSTRAINT seg_documento_estudo_id_estudo_fkey FOREIGN KEY (id_estudo) REFERENCES public.seg_estudo(id_estudo) ON DELETE CASCADE;


--
-- TOC entry 3723 (class 2606 OID 20960)
-- Name: seg_documento_protocolo seg_documento_protocolo_id_aso_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_documento_protocolo
    ADD CONSTRAINT seg_documento_protocolo_id_aso_fkey FOREIGN KEY (id_aso) REFERENCES public.seg_aso(id_aso);


--
-- TOC entry 3724 (class 2606 OID 20965)
-- Name: seg_documento_protocolo seg_documento_protocolo_id_documento_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_documento_protocolo
    ADD CONSTRAINT seg_documento_protocolo_id_documento_fkey FOREIGN KEY (id_documento) REFERENCES public.seg_documento(id_documento);


--
-- TOC entry 3725 (class 2606 OID 20970)
-- Name: seg_documento_protocolo seg_documento_protocolo_id_protocolo_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_documento_protocolo
    ADD CONSTRAINT seg_documento_protocolo_id_protocolo_fkey FOREIGN KEY (id_protocolo) REFERENCES public.seg_protocolo(id_protocolo);


--
-- TOC entry 3726 (class 2606 OID 20975)
-- Name: seg_dominio_acao seg_dominio_acao_id_acao_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_dominio_acao
    ADD CONSTRAINT seg_dominio_acao_id_acao_fkey FOREIGN KEY (id_acao) REFERENCES public.seg_acao(id_acao) ON DELETE CASCADE;


--
-- TOC entry 3727 (class 2606 OID 20980)
-- Name: seg_dominio_acao seg_dominio_acao_id_dominio_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_dominio_acao
    ADD CONSTRAINT seg_dominio_acao_id_dominio_fkey FOREIGN KEY (id_dominio) REFERENCES public.seg_dominio(id_dominio) ON DELETE CASCADE;


--
-- TOC entry 3728 (class 2606 OID 20985)
-- Name: seg_email seg_email_id_prontuario_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_email
    ADD CONSTRAINT seg_email_id_prontuario_fkey FOREIGN KEY (id_prontuario) REFERENCES public.seg_prontuario(id_prontuario);


--
-- TOC entry 3729 (class 2606 OID 20990)
-- Name: seg_email seg_email_id_usuario_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_email
    ADD CONSTRAINT seg_email_id_usuario_fkey FOREIGN KEY (id_usuario) REFERENCES public.seg_usuario(id_usuario);


--
-- TOC entry 3730 (class 2606 OID 20995)
-- Name: seg_empresa seg_empresa_id_cidade_ibge_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_empresa
    ADD CONSTRAINT seg_empresa_id_cidade_ibge_fkey FOREIGN KEY (id_cidade_ibge) REFERENCES public.seg_cidade_ibge(id_cidade_ibge);


--
-- TOC entry 3731 (class 2606 OID 21000)
-- Name: seg_empresa seg_empresa_id_empresa_matriz_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_empresa
    ADD CONSTRAINT seg_empresa_id_empresa_matriz_fkey FOREIGN KEY (id_empresa_matriz) REFERENCES public.seg_empresa(id_empresa);


--
-- TOC entry 3844 (class 2606 OID 245191)
-- Name: seg_epi_monitoramento seg_epi_monitoramento_id_epi_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_epi_monitoramento
    ADD CONSTRAINT seg_epi_monitoramento_id_epi_fkey FOREIGN KEY (id_epi) REFERENCES public.seg_epi(id_epi) ON DELETE CASCADE;


--
-- TOC entry 3843 (class 2606 OID 245186)
-- Name: seg_epi_monitoramento seg_epi_monitoramento_id_monitoramento_ghefonteagente_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_epi_monitoramento
    ADD CONSTRAINT seg_epi_monitoramento_id_monitoramento_ghefonteagente_fkey FOREIGN KEY (id_monitoramento_ghefonteagente) REFERENCES public.seg_monitoramento_ghefonteagente(id_monitoramento_ghefonteagente) ON DELETE CASCADE;


--
-- TOC entry 3733 (class 2606 OID 21005)
-- Name: seg_esocial_lote_aso seg_esocial_lote_aso_id_aso_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_esocial_lote_aso
    ADD CONSTRAINT seg_esocial_lote_aso_id_aso_fkey FOREIGN KEY (id_aso) REFERENCES public.seg_aso(id_aso);


--
-- TOC entry 3734 (class 2606 OID 294352)
-- Name: seg_esocial_lote_aso seg_esocial_lote_aso_id_esocial_lote_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_esocial_lote_aso
    ADD CONSTRAINT seg_esocial_lote_aso_id_esocial_lote_fkey FOREIGN KEY (id_esocial_lote) REFERENCES public.seg_esocial_lote(id_esocial_lote) ON DELETE CASCADE;


--
-- TOC entry 3732 (class 2606 OID 21015)
-- Name: seg_esocial_lote seg_esocial_lote_id_cliente_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_esocial_lote
    ADD CONSTRAINT seg_esocial_lote_id_cliente_fkey FOREIGN KEY (id_cliente) REFERENCES public.seg_cliente(id_cliente);


--
-- TOC entry 3839 (class 2606 OID 245124)
-- Name: seg_esocial_monitoramento seg_esocial_monitoramento_id_esocial_lote_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_esocial_monitoramento
    ADD CONSTRAINT seg_esocial_monitoramento_id_esocial_lote_fkey FOREIGN KEY (id_esocial_lote) REFERENCES public.seg_esocial_lote(id_esocial_lote) ON DELETE CASCADE;


--
-- TOC entry 3840 (class 2606 OID 294418)
-- Name: seg_esocial_monitoramento seg_esocial_monitoramento_id_monitoramento_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_esocial_monitoramento
    ADD CONSTRAINT seg_esocial_monitoramento_id_monitoramento_fkey FOREIGN KEY (id_monitoramento) REFERENCES public.seg_monitoramento(id_monitoramento) ON DELETE CASCADE;


--
-- TOC entry 3735 (class 2606 OID 21020)
-- Name: seg_estimativa_estudo seg_estimativa_estudo_id_estimativa_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_estimativa_estudo
    ADD CONSTRAINT seg_estimativa_estudo_id_estimativa_fkey FOREIGN KEY (id_estimativa) REFERENCES public.seg_estimativa(id_estimativa);


--
-- TOC entry 3736 (class 2606 OID 21025)
-- Name: seg_estimativa_estudo seg_estimativa_estudo_id_estudo_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_estimativa_estudo
    ADD CONSTRAINT seg_estimativa_estudo_id_estudo_fkey FOREIGN KEY (id_estudo) REFERENCES public.seg_estudo(id_estudo) ON DELETE CASCADE;


--
-- TOC entry 3744 (class 2606 OID 21030)
-- Name: seg_estudo_hospital seg_estudo_hospital_id_estudo_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_estudo_hospital
    ADD CONSTRAINT seg_estudo_hospital_id_estudo_fkey FOREIGN KEY (id_estudo) REFERENCES public.seg_estudo(id_estudo) ON DELETE CASCADE;


--
-- TOC entry 3745 (class 2606 OID 21035)
-- Name: seg_estudo_hospital seg_estudo_hospital_id_hospital_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_estudo_hospital
    ADD CONSTRAINT seg_estudo_hospital_id_hospital_fkey FOREIGN KEY (id_hospital) REFERENCES public.seg_hospital(id_hospital);


--
-- TOC entry 3737 (class 2606 OID 21040)
-- Name: seg_estudo seg_estudo_id_cliente_contratado_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_estudo
    ADD CONSTRAINT seg_estudo_id_cliente_contratado_fkey FOREIGN KEY (id_cliente_contratado) REFERENCES public.seg_cliente(id_cliente);


--
-- TOC entry 3738 (class 2606 OID 21045)
-- Name: seg_estudo seg_estudo_id_cronograma_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_estudo
    ADD CONSTRAINT seg_estudo_id_cronograma_fkey FOREIGN KEY (id_cronograma) REFERENCES public.seg_cronograma(id_cronograma) ON DELETE CASCADE;


--
-- TOC entry 3739 (class 2606 OID 21050)
-- Name: seg_estudo seg_estudo_id_engenheiro_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_estudo
    ADD CONSTRAINT seg_estudo_id_engenheiro_fkey FOREIGN KEY (id_engenheiro) REFERENCES public.seg_usuario(id_usuario);


--
-- TOC entry 3740 (class 2606 OID 21055)
-- Name: seg_estudo seg_estudo_id_protocolo_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_estudo
    ADD CONSTRAINT seg_estudo_id_protocolo_fkey FOREIGN KEY (id_protocolo) REFERENCES public.seg_protocolo(id_protocolo);


--
-- TOC entry 3741 (class 2606 OID 21060)
-- Name: seg_estudo seg_estudo_id_relatorio_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_estudo
    ADD CONSTRAINT seg_estudo_id_relatorio_fkey FOREIGN KEY (id_relatorio) REFERENCES public.seg_relatorio(id_relatorio);


--
-- TOC entry 3742 (class 2606 OID 21065)
-- Name: seg_estudo seg_estudo_id_seg_rep_cli_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_estudo
    ADD CONSTRAINT seg_estudo_id_seg_rep_cli_fkey FOREIGN KEY (id_seg_rep_cli) REFERENCES public.seg_rep_cli(id_seg_rep_cli);


--
-- TOC entry 3743 (class 2606 OID 21070)
-- Name: seg_estudo seg_estudo_id_tecno_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_estudo
    ADD CONSTRAINT seg_estudo_id_tecno_fkey FOREIGN KEY (id_tecno) REFERENCES public.seg_usuario(id_usuario);


--
-- TOC entry 3746 (class 2606 OID 21075)
-- Name: seg_estudo_medico seg_estudo_medico_id_estudo_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_estudo_medico
    ADD CONSTRAINT seg_estudo_medico_id_estudo_fkey FOREIGN KEY (id_estudo) REFERENCES public.seg_estudo(id_estudo) ON DELETE CASCADE;


--
-- TOC entry 3747 (class 2606 OID 21080)
-- Name: seg_estudo_medico seg_estudo_medico_id_medico_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_estudo_medico
    ADD CONSTRAINT seg_estudo_medico_id_medico_fkey FOREIGN KEY (id_medico) REFERENCES public.seg_medico(id_medico);


--
-- TOC entry 3748 (class 2606 OID 21085)
-- Name: seg_funcao_epi seg_funcao_epi_id_estudo_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_funcao_epi
    ADD CONSTRAINT seg_funcao_epi_id_estudo_fkey FOREIGN KEY (id_estudo) REFERENCES public.seg_estudo(id_estudo) ON DELETE CASCADE;


--
-- TOC entry 3749 (class 2606 OID 21090)
-- Name: seg_funcionario seg_funcionario_id_cidade_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_funcionario
    ADD CONSTRAINT seg_funcionario_id_cidade_fkey FOREIGN KEY (id_cidade) REFERENCES public.seg_cidade_ibge(id_cidade_ibge);


--
-- TOC entry 3758 (class 2606 OID 21095)
-- Name: seg_ghe_fonte_agente_epi seg_ghe_fonte_agente_epi_id_epi_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_ghe_fonte_agente_epi
    ADD CONSTRAINT seg_ghe_fonte_agente_epi_id_epi_fkey FOREIGN KEY (id_epi) REFERENCES public.seg_epi(id_epi);


--
-- TOC entry 3759 (class 2606 OID 21100)
-- Name: seg_ghe_fonte_agente_epi seg_ghe_fonte_agente_epi_id_ghe_fonte_agente_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_ghe_fonte_agente_epi
    ADD CONSTRAINT seg_ghe_fonte_agente_epi_id_ghe_fonte_agente_fkey FOREIGN KEY (id_ghe_fonte_agente) REFERENCES public.seg_ghe_fonte_agente(id_ghe_fonte_agente) ON DELETE CASCADE;


--
-- TOC entry 3762 (class 2606 OID 21105)
-- Name: seg_ghe_fonte_agente_exame_aso seg_ghe_fonte_agente_exame_aso_id_aso_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_ghe_fonte_agente_exame_aso
    ADD CONSTRAINT seg_ghe_fonte_agente_exame_aso_id_aso_fkey FOREIGN KEY (id_aso) REFERENCES public.seg_aso(id_aso);


--
-- TOC entry 3763 (class 2606 OID 21110)
-- Name: seg_ghe_fonte_agente_exame_aso seg_ghe_fonte_agente_exame_aso_id_ghe_fonte_agente_exame_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_ghe_fonte_agente_exame_aso
    ADD CONSTRAINT seg_ghe_fonte_agente_exame_aso_id_ghe_fonte_agente_exame_fkey FOREIGN KEY (id_ghe_fonte_agente_exame) REFERENCES public.seg_ghe_fonte_agente_exame(id_ghe_fonte_agente_exame);


--
-- TOC entry 3764 (class 2606 OID 21115)
-- Name: seg_ghe_fonte_agente_exame_aso seg_ghe_fonte_agente_exame_aso_id_medico_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_ghe_fonte_agente_exame_aso
    ADD CONSTRAINT seg_ghe_fonte_agente_exame_aso_id_medico_fkey FOREIGN KEY (id_medico) REFERENCES public.seg_medico(id_medico);


--
-- TOC entry 3765 (class 2606 OID 21120)
-- Name: seg_ghe_fonte_agente_exame_aso seg_ghe_fonte_agente_exame_aso_id_prestador_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_ghe_fonte_agente_exame_aso
    ADD CONSTRAINT seg_ghe_fonte_agente_exame_aso_id_prestador_fkey FOREIGN KEY (id_prestador) REFERENCES public.seg_cliente(id_cliente);


--
-- TOC entry 3766 (class 2606 OID 21125)
-- Name: seg_ghe_fonte_agente_exame_aso seg_ghe_fonte_agente_exame_aso_id_protocolo_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_ghe_fonte_agente_exame_aso
    ADD CONSTRAINT seg_ghe_fonte_agente_exame_aso_id_protocolo_fkey FOREIGN KEY (id_protocolo) REFERENCES public.seg_protocolo(id_protocolo);


--
-- TOC entry 3767 (class 2606 OID 21130)
-- Name: seg_ghe_fonte_agente_exame_aso seg_ghe_fonte_agente_exame_aso_id_sala_exame_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_ghe_fonte_agente_exame_aso
    ADD CONSTRAINT seg_ghe_fonte_agente_exame_aso_id_sala_exame_fkey FOREIGN KEY (id_sala_exame) REFERENCES public.seg_sala_exame(id_sala_exame);


--
-- TOC entry 3768 (class 2606 OID 21135)
-- Name: seg_ghe_fonte_agente_exame_aso seg_ghe_fonte_agente_exame_aso_id_usuario_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_ghe_fonte_agente_exame_aso
    ADD CONSTRAINT seg_ghe_fonte_agente_exame_aso_id_usuario_fkey FOREIGN KEY (id_usuario) REFERENCES public.seg_usuario(id_usuario);


--
-- TOC entry 3760 (class 2606 OID 21140)
-- Name: seg_ghe_fonte_agente_exame seg_ghe_fonte_agente_exame_id_exame_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_ghe_fonte_agente_exame
    ADD CONSTRAINT seg_ghe_fonte_agente_exame_id_exame_fkey FOREIGN KEY (id_exame) REFERENCES public.seg_exame(id_exame);


--
-- TOC entry 3761 (class 2606 OID 21145)
-- Name: seg_ghe_fonte_agente_exame seg_ghe_fonte_agente_exame_id_ghe_fonte_agente_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_ghe_fonte_agente_exame
    ADD CONSTRAINT seg_ghe_fonte_agente_exame_id_ghe_fonte_agente_fkey FOREIGN KEY (id_ghe_fonte_agente) REFERENCES public.seg_ghe_fonte_agente(id_ghe_fonte_agente) ON DELETE CASCADE;


--
-- TOC entry 3769 (class 2606 OID 21150)
-- Name: seg_ghe_fonte_agente_exame_periodicidade seg_ghe_fonte_agente_exame_perio_id_ghe_fonte_agente_exame_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_ghe_fonte_agente_exame_periodicidade
    ADD CONSTRAINT seg_ghe_fonte_agente_exame_perio_id_ghe_fonte_agente_exame_fkey FOREIGN KEY (id_ghe_fonte_agente_exame) REFERENCES public.seg_ghe_fonte_agente_exame(id_ghe_fonte_agente_exame) ON DELETE CASCADE;


--
-- TOC entry 3770 (class 2606 OID 21155)
-- Name: seg_ghe_fonte_agente_exame_periodicidade seg_ghe_fonte_agente_exame_periodicidade_id_periodicidade_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_ghe_fonte_agente_exame_periodicidade
    ADD CONSTRAINT seg_ghe_fonte_agente_exame_periodicidade_id_periodicidade_fkey FOREIGN KEY (id_periodicidade) REFERENCES public.seg_periodicidade(id_periodicidade);


--
-- TOC entry 3753 (class 2606 OID 21160)
-- Name: seg_ghe_fonte_agente seg_ghe_fonte_agente_id_agente_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_ghe_fonte_agente
    ADD CONSTRAINT seg_ghe_fonte_agente_id_agente_fkey FOREIGN KEY (id_agente) REFERENCES public.seg_agente(id_agente);


--
-- TOC entry 3754 (class 2606 OID 21165)
-- Name: seg_ghe_fonte_agente seg_ghe_fonte_agente_id_ghe_fonte_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_ghe_fonte_agente
    ADD CONSTRAINT seg_ghe_fonte_agente_id_ghe_fonte_fkey FOREIGN KEY (id_ghe_fonte) REFERENCES public.seg_ghe_fonte(id_ghe_fonte) ON DELETE CASCADE;


--
-- TOC entry 3755 (class 2606 OID 21170)
-- Name: seg_ghe_fonte_agente seg_ghe_fonte_agente_id_grad_efeito_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_ghe_fonte_agente
    ADD CONSTRAINT seg_ghe_fonte_agente_id_grad_efeito_fkey FOREIGN KEY (id_grad_efeito) REFERENCES public.seg_grad_efeito(id_grad_efeito);


--
-- TOC entry 3756 (class 2606 OID 21175)
-- Name: seg_ghe_fonte_agente seg_ghe_fonte_agente_id_grad_exposicao_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_ghe_fonte_agente
    ADD CONSTRAINT seg_ghe_fonte_agente_id_grad_exposicao_fkey FOREIGN KEY (id_grad_exposicao) REFERENCES public.seg_grad_exposicao(id_grad_exposicao);


--
-- TOC entry 3757 (class 2606 OID 21180)
-- Name: seg_ghe_fonte_agente seg_ghe_fonte_agente_id_grad_soma_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_ghe_fonte_agente
    ADD CONSTRAINT seg_ghe_fonte_agente_id_grad_soma_fkey FOREIGN KEY (id_grad_soma) REFERENCES public.seg_grad_soma(id_grad_soma);


--
-- TOC entry 3751 (class 2606 OID 21185)
-- Name: seg_ghe_fonte seg_ghe_fonte_id_fonte_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_ghe_fonte
    ADD CONSTRAINT seg_ghe_fonte_id_fonte_fkey FOREIGN KEY (id_fonte) REFERENCES public.seg_fonte(id_fonte);


--
-- TOC entry 3752 (class 2606 OID 21190)
-- Name: seg_ghe_fonte seg_ghe_fonte_id_ghe_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_ghe_fonte
    ADD CONSTRAINT seg_ghe_fonte_id_ghe_fkey FOREIGN KEY (id_ghe) REFERENCES public.seg_ghe(id_ghe) ON DELETE CASCADE;


--
-- TOC entry 3750 (class 2606 OID 21195)
-- Name: seg_ghe seg_ghe_id_estudo_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_ghe
    ADD CONSTRAINT seg_ghe_id_estudo_fkey FOREIGN KEY (id_estudo) REFERENCES public.seg_estudo(id_estudo) ON DELETE CASCADE;


--
-- TOC entry 3776 (class 2606 OID 21200)
-- Name: seg_ghe_setor_cliente_funcao_exame seg_ghe_setor_cliente_funcao_e_id_ghe_setor_cliente_funcao_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_ghe_setor_cliente_funcao_exame
    ADD CONSTRAINT seg_ghe_setor_cliente_funcao_e_id_ghe_setor_cliente_funcao_fkey FOREIGN KEY (id_ghe_setor_cliente_funcao) REFERENCES public.seg_ghe_setor_cliente_funcao(id_ghe_setor_cliente_funcao);


--
-- TOC entry 3778 (class 2606 OID 21557)
-- Name: seg_ghe_setor_cliente_funcao_exame seg_ghe_setor_cliente_funcao_exa_id_ghe_fonte_agente_exame_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_ghe_setor_cliente_funcao_exame
    ADD CONSTRAINT seg_ghe_setor_cliente_funcao_exa_id_ghe_fonte_agente_exame_fkey FOREIGN KEY (id_ghe_fonte_agente_exame) REFERENCES public.seg_ghe_fonte_agente_exame(id_ghe_fonte_agente_exame) ON DELETE CASCADE;


--
-- TOC entry 3777 (class 2606 OID 21210)
-- Name: seg_ghe_setor_cliente_funcao_exame seg_ghe_setor_cliente_funcao_exame_id_exame_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_ghe_setor_cliente_funcao_exame
    ADD CONSTRAINT seg_ghe_setor_cliente_funcao_exame_id_exame_fkey FOREIGN KEY (id_exame) REFERENCES public.seg_exame(id_exame);


--
-- TOC entry 3773 (class 2606 OID 21215)
-- Name: seg_ghe_setor_cliente_funcao seg_ghe_setor_cliente_funcao_id_comentario_funcao_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_ghe_setor_cliente_funcao
    ADD CONSTRAINT seg_ghe_setor_cliente_funcao_id_comentario_funcao_fkey FOREIGN KEY (id_comentario_funcao) REFERENCES public.seg_comentario_funcao(id_comentario_funcao);


--
-- TOC entry 3774 (class 2606 OID 21220)
-- Name: seg_ghe_setor_cliente_funcao seg_ghe_setor_cliente_funcao_id_ghe_setor_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_ghe_setor_cliente_funcao
    ADD CONSTRAINT seg_ghe_setor_cliente_funcao_id_ghe_setor_fkey FOREIGN KEY (id_ghe_setor) REFERENCES public.seg_ghe_setor(id_ghe_setor) ON DELETE CASCADE;


--
-- TOC entry 3775 (class 2606 OID 21225)
-- Name: seg_ghe_setor_cliente_funcao seg_ghe_setor_cliente_funcao_id_seg_cliente_funcao_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_ghe_setor_cliente_funcao
    ADD CONSTRAINT seg_ghe_setor_cliente_funcao_id_seg_cliente_funcao_fkey FOREIGN KEY (id_seg_cliente_funcao) REFERENCES public.seg_cliente_funcao(id_seg_cliente_funcao);


--
-- TOC entry 3771 (class 2606 OID 21230)
-- Name: seg_ghe_setor seg_ghe_setor_id_ghe_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_ghe_setor
    ADD CONSTRAINT seg_ghe_setor_id_ghe_fkey FOREIGN KEY (id_ghe) REFERENCES public.seg_ghe(id_ghe) ON DELETE CASCADE;


--
-- TOC entry 3772 (class 2606 OID 21235)
-- Name: seg_ghe_setor seg_ghe_setor_id_setor_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_ghe_setor
    ADD CONSTRAINT seg_ghe_setor_id_setor_fkey FOREIGN KEY (id_setor) REFERENCES public.seg_setor(id_setor);


--
-- TOC entry 3779 (class 2606 OID 294362)
-- Name: seg_hospital seg_hospital_id_cidade_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_hospital
    ADD CONSTRAINT seg_hospital_id_cidade_fkey FOREIGN KEY (id_cidade) REFERENCES public.seg_cidade_ibge(id_cidade_ibge);


--
-- TOC entry 3780 (class 2606 OID 21240)
-- Name: seg_material_hospitalar_estudo seg_material_hospitalar_estudo_id_estudo_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_material_hospitalar_estudo
    ADD CONSTRAINT seg_material_hospitalar_estudo_id_estudo_fkey FOREIGN KEY (id_estudo) REFERENCES public.seg_estudo(id_estudo) ON DELETE CASCADE;


--
-- TOC entry 3781 (class 2606 OID 21245)
-- Name: seg_material_hospitalar_estudo seg_material_hospitalar_estudo_id_material_hospitalar_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_material_hospitalar_estudo
    ADD CONSTRAINT seg_material_hospitalar_estudo_id_material_hospitalar_fkey FOREIGN KEY (id_material_hospitalar) REFERENCES public.seg_material_hospitalar(id_material_hospitalar);


--
-- TOC entry 3783 (class 2606 OID 21250)
-- Name: seg_medico_exame seg_medico_exame_id_exame_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_medico_exame
    ADD CONSTRAINT seg_medico_exame_id_exame_fkey FOREIGN KEY (id_exame) REFERENCES public.seg_exame(id_exame) ON DELETE CASCADE;


--
-- TOC entry 3784 (class 2606 OID 21255)
-- Name: seg_medico_exame seg_medico_exame_id_medico_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_medico_exame
    ADD CONSTRAINT seg_medico_exame_id_medico_fkey FOREIGN KEY (id_medico) REFERENCES public.seg_medico(id_medico) ON DELETE CASCADE;


--
-- TOC entry 3782 (class 2606 OID 21260)
-- Name: seg_medico seg_medico_id_cidade_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_medico
    ADD CONSTRAINT seg_medico_id_cidade_fkey FOREIGN KEY (id_cidade) REFERENCES public.seg_cidade_ibge(id_cidade_ibge);


--
-- TOC entry 3842 (class 2606 OID 245169)
-- Name: seg_monitoramento_ghefonteagente seg_monitoramento_ghefonteagente_id_ghe_fonte_agente_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_monitoramento_ghefonteagente
    ADD CONSTRAINT seg_monitoramento_ghefonteagente_id_ghe_fonte_agente_fkey FOREIGN KEY (id_ghe_fonte_agente) REFERENCES public.seg_ghe_fonte_agente(id_ghe_fonte_agente) ON DELETE CASCADE;


--
-- TOC entry 3841 (class 2606 OID 245164)
-- Name: seg_monitoramento_ghefonteagente seg_monitoramento_ghefonteagente_id_monitoramento_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_monitoramento_ghefonteagente
    ADD CONSTRAINT seg_monitoramento_ghefonteagente_id_monitoramento_fkey FOREIGN KEY (id_monitoramento) REFERENCES public.seg_monitoramento(id_monitoramento) ON DELETE CASCADE;


--
-- TOC entry 3837 (class 2606 OID 294367)
-- Name: seg_monitoramento seg_monitoramento_id_cliente_funcao_funcionario_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_monitoramento
    ADD CONSTRAINT seg_monitoramento_id_cliente_funcao_funcionario_fkey FOREIGN KEY (id_cliente_funcao_funcionario) REFERENCES public.seg_cliente_funcao_funcionario(id_cliente_funcao_funcionario);


--
-- TOC entry 3836 (class 2606 OID 245102)
-- Name: seg_monitoramento seg_monitoramento_id_monitoramento_anterior_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_monitoramento
    ADD CONSTRAINT seg_monitoramento_id_monitoramento_anterior_fkey FOREIGN KEY (id_monitoramento_anterior) REFERENCES public.seg_monitoramento(id_monitoramento);


--
-- TOC entry 3838 (class 2606 OID 294372)
-- Name: seg_monitoramento seg_monitoramento_id_usuario_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_monitoramento
    ADD CONSTRAINT seg_monitoramento_id_usuario_fkey FOREIGN KEY (id_usuario) REFERENCES public.seg_usuario(id_usuario);


--
-- TOC entry 3785 (class 2606 OID 21265)
-- Name: seg_movimento seg_movimento_id_cliente_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_movimento
    ADD CONSTRAINT seg_movimento_id_cliente_fkey FOREIGN KEY (id_cliente) REFERENCES public.seg_cliente(id_cliente);


--
-- TOC entry 3786 (class 2606 OID 21270)
-- Name: seg_movimento seg_movimento_id_cliente_funcao_exame_aso_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_movimento
    ADD CONSTRAINT seg_movimento_id_cliente_funcao_exame_aso_fkey FOREIGN KEY (id_cliente_funcao_exame_aso) REFERENCES public.seg_cliente_funcao_exame_aso(id_cliente_funcao_exame_aso);


--
-- TOC entry 3787 (class 2606 OID 21275)
-- Name: seg_movimento seg_movimento_id_cliente_unidade_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_movimento
    ADD CONSTRAINT seg_movimento_id_cliente_unidade_fkey FOREIGN KEY (id_cliente_unidade) REFERENCES public.seg_cliente(id_cliente);


--
-- TOC entry 3788 (class 2606 OID 21280)
-- Name: seg_movimento seg_movimento_id_empresa_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_movimento
    ADD CONSTRAINT seg_movimento_id_empresa_fkey FOREIGN KEY (id_empresa) REFERENCES public.seg_empresa(id_empresa);


--
-- TOC entry 3789 (class 2606 OID 21285)
-- Name: seg_movimento seg_movimento_id_ghe_fonte_agente_exame_aso_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_movimento
    ADD CONSTRAINT seg_movimento_id_ghe_fonte_agente_exame_aso_fkey FOREIGN KEY (id_ghe_fonte_agente_exame_aso) REFERENCES public.seg_ghe_fonte_agente_exame_aso(id_ghe_fonte_agente_exame_aso);


--
-- TOC entry 3790 (class 2606 OID 21290)
-- Name: seg_movimento seg_movimento_id_nfe_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_movimento
    ADD CONSTRAINT seg_movimento_id_nfe_fkey FOREIGN KEY (id_nfe) REFERENCES public.seg_nfe(id_nfe);


--
-- TOC entry 3791 (class 2606 OID 21295)
-- Name: seg_movimento seg_movimento_id_produto_contrato_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_movimento
    ADD CONSTRAINT seg_movimento_id_produto_contrato_fkey FOREIGN KEY (id_produto_contrato) REFERENCES public.seg_produto_contrato(id_produto_contrato);


--
-- TOC entry 3792 (class 2606 OID 21300)
-- Name: seg_movimento seg_movimento_id_protocolo_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_movimento
    ADD CONSTRAINT seg_movimento_id_protocolo_fkey FOREIGN KEY (id_protocolo) REFERENCES public.seg_protocolo(id_protocolo);


--
-- TOC entry 3793 (class 2606 OID 21305)
-- Name: seg_movimento seg_movimento_id_usuario_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_movimento
    ADD CONSTRAINT seg_movimento_id_usuario_fkey FOREIGN KEY (id_usuario) REFERENCES public.seg_usuario(id_usuario);


--
-- TOC entry 3794 (class 2606 OID 21310)
-- Name: seg_nfe seg_nfe_id_cliente_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_nfe
    ADD CONSTRAINT seg_nfe_id_cliente_fkey FOREIGN KEY (id_cliente) REFERENCES public.seg_cliente(id_cliente);


--
-- TOC entry 3795 (class 2606 OID 21315)
-- Name: seg_nfe seg_nfe_id_empresa_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_nfe
    ADD CONSTRAINT seg_nfe_id_empresa_fkey FOREIGN KEY (id_empresa) REFERENCES public.seg_empresa(id_empresa);


--
-- TOC entry 3796 (class 2606 OID 21320)
-- Name: seg_nfe seg_nfe_id_plano_forma_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_nfe
    ADD CONSTRAINT seg_nfe_id_plano_forma_fkey FOREIGN KEY (id_plano_forma) REFERENCES public.seg_plano_forma(id_plano_forma);


--
-- TOC entry 3797 (class 2606 OID 21325)
-- Name: seg_nfe seg_nfe_id_usuario_cancela_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_nfe
    ADD CONSTRAINT seg_nfe_id_usuario_cancela_fkey FOREIGN KEY (id_usuario_cancela) REFERENCES public.seg_usuario(id_usuario);


--
-- TOC entry 3798 (class 2606 OID 21330)
-- Name: seg_nfe seg_nfe_id_usuario_criador_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_nfe
    ADD CONSTRAINT seg_nfe_id_usuario_criador_fkey FOREIGN KEY (id_usuario_criador) REFERENCES public.seg_usuario(id_usuario);


--
-- TOC entry 3799 (class 2606 OID 21335)
-- Name: seg_norma_ghe seg_norma_ghe_id_ghe_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_norma_ghe
    ADD CONSTRAINT seg_norma_ghe_id_ghe_fkey FOREIGN KEY (id_ghe) REFERENCES public.seg_ghe(id_ghe) ON DELETE CASCADE;


--
-- TOC entry 3800 (class 2606 OID 21340)
-- Name: seg_norma_ghe seg_norma_ghe_id_norma_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_norma_ghe
    ADD CONSTRAINT seg_norma_ghe_id_norma_fkey FOREIGN KEY (id_norma) REFERENCES public.seg_norma(id_norma);


--
-- TOC entry 3801 (class 2606 OID 21345)
-- Name: seg_parcela seg_parcela_id_plano_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_parcela
    ADD CONSTRAINT seg_parcela_id_plano_fkey FOREIGN KEY (id_plano) REFERENCES public.seg_plano(id_plano) ON DELETE CASCADE;


--
-- TOC entry 3802 (class 2606 OID 21350)
-- Name: seg_perfil_dominio_acao seg_perfil_dominio_acao_id_dominio_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_perfil_dominio_acao
    ADD CONSTRAINT seg_perfil_dominio_acao_id_dominio_fkey FOREIGN KEY (id_dominio, id_acao) REFERENCES public.seg_dominio_acao(id_dominio, id_acao) ON DELETE CASCADE;


--
-- TOC entry 3803 (class 2606 OID 21355)
-- Name: seg_perfil_dominio_acao seg_perfil_dominio_acao_id_perfil_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_perfil_dominio_acao
    ADD CONSTRAINT seg_perfil_dominio_acao_id_perfil_fkey FOREIGN KEY (id_perfil) REFERENCES public.seg_perfil(id_perfil);


--
-- TOC entry 3804 (class 2606 OID 21360)
-- Name: seg_plano_forma seg_plano_forma_id_forma_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_plano_forma
    ADD CONSTRAINT seg_plano_forma_id_forma_fkey FOREIGN KEY (id_forma) REFERENCES public.seg_forma(id_forma);


--
-- TOC entry 3805 (class 2606 OID 21365)
-- Name: seg_plano_forma seg_plano_forma_id_plano_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_plano_forma
    ADD CONSTRAINT seg_plano_forma_id_plano_fkey FOREIGN KEY (id_plano) REFERENCES public.seg_plano(id_plano);


--
-- TOC entry 3806 (class 2606 OID 21405)
-- Name: seg_produto_contrato seg_produto_contrato_id_contrato_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_produto_contrato
    ADD CONSTRAINT seg_produto_contrato_id_contrato_fkey FOREIGN KEY (id_contrato) REFERENCES public.seg_contrato(id_contrato) ON DELETE CASCADE;


--
-- TOC entry 3807 (class 2606 OID 21410)
-- Name: seg_produto_contrato seg_produto_contrato_id_produto_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_produto_contrato
    ADD CONSTRAINT seg_produto_contrato_id_produto_fkey FOREIGN KEY (id_produto) REFERENCES public.seg_produto(id_produto) ON DELETE CASCADE;


--
-- TOC entry 3808 (class 2606 OID 21415)
-- Name: seg_prontuario seg_prontuario_id_aso_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_prontuario
    ADD CONSTRAINT seg_prontuario_id_aso_fkey FOREIGN KEY (id_aso) REFERENCES public.seg_aso(id_aso) ON DELETE CASCADE;


--
-- TOC entry 3809 (class 2606 OID 21420)
-- Name: seg_prontuario seg_prontuario_id_usuario_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_prontuario
    ADD CONSTRAINT seg_prontuario_id_usuario_fkey FOREIGN KEY (id_usuario) REFERENCES public.seg_usuario(id_usuario);


--
-- TOC entry 3810 (class 2606 OID 21425)
-- Name: seg_protocolo seg_protocolo_id_cliente_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_protocolo
    ADD CONSTRAINT seg_protocolo_id_cliente_fkey FOREIGN KEY (id_cliente) REFERENCES public.seg_cliente(id_cliente);


--
-- TOC entry 3811 (class 2606 OID 21430)
-- Name: seg_protocolo seg_protocolo_id_usuario_cancelou_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_protocolo
    ADD CONSTRAINT seg_protocolo_id_usuario_cancelou_fkey FOREIGN KEY (id_usuario_cancelou) REFERENCES public.seg_usuario(id_usuario);


--
-- TOC entry 3812 (class 2606 OID 21435)
-- Name: seg_protocolo seg_protocolo_id_usuario_finalizou_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_protocolo
    ADD CONSTRAINT seg_protocolo_id_usuario_finalizou_fkey FOREIGN KEY (id_usuario_finalizou) REFERENCES public.seg_usuario(id_usuario);


--
-- TOC entry 3813 (class 2606 OID 21440)
-- Name: seg_protocolo seg_protocolo_id_usuario_gerou_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_protocolo
    ADD CONSTRAINT seg_protocolo_id_usuario_gerou_fkey FOREIGN KEY (id_usuario_gerou) REFERENCES public.seg_usuario(id_usuario);


--
-- TOC entry 3814 (class 2606 OID 21445)
-- Name: seg_relanual seg_relanual_id_cliente_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_relanual
    ADD CONSTRAINT seg_relanual_id_cliente_fkey FOREIGN KEY (id_cliente) REFERENCES public.seg_cliente(id_cliente);


--
-- TOC entry 3815 (class 2606 OID 21450)
-- Name: seg_relanual seg_relanual_id_medico_coordenador_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_relanual
    ADD CONSTRAINT seg_relanual_id_medico_coordenador_fkey FOREIGN KEY (id_medico_coordenador) REFERENCES public.seg_medico(id_medico);


--
-- TOC entry 3816 (class 2606 OID 21455)
-- Name: seg_relanual seg_relanual_id_usuario_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_relanual
    ADD CONSTRAINT seg_relanual_id_usuario_fkey FOREIGN KEY (id_usuario) REFERENCES public.seg_usuario(id_usuario);


--
-- TOC entry 3817 (class 2606 OID 21460)
-- Name: seg_relanual_item seg_relanual_item_id_relanual_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_relanual_item
    ADD CONSTRAINT seg_relanual_item_id_relanual_fkey FOREIGN KEY (id_relanual) REFERENCES public.seg_relanual(id_relanual) ON DELETE CASCADE;


--
-- TOC entry 3834 (class 2606 OID 21606)
-- Name: seg_relatorio_exame seg_relatorio_exame_id_exame_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_relatorio_exame
    ADD CONSTRAINT seg_relatorio_exame_id_exame_fkey FOREIGN KEY (id_exame) REFERENCES public.seg_exame(id_exame);


--
-- TOC entry 3835 (class 2606 OID 21601)
-- Name: seg_relatorio_exame seg_relatorio_exame_id_relatorio_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_relatorio_exame
    ADD CONSTRAINT seg_relatorio_exame_id_relatorio_fkey FOREIGN KEY (id_relatorio) REFERENCES public.seg_relatorio(id_relatorio) ON DELETE CASCADE;


--
-- TOC entry 3818 (class 2606 OID 21465)
-- Name: seg_rep_cli seg_rep_cli_id_cliente_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_rep_cli
    ADD CONSTRAINT seg_rep_cli_id_cliente_fkey FOREIGN KEY (id_cliente) REFERENCES public.seg_cliente(id_cliente);


--
-- TOC entry 3819 (class 2606 OID 21470)
-- Name: seg_rep_cli seg_rep_cli_id_vend_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_rep_cli
    ADD CONSTRAINT seg_rep_cli_id_vend_fkey FOREIGN KEY (id_vend) REFERENCES public.seg_vendedor(id_vend);


--
-- TOC entry 3820 (class 2606 OID 21480)
-- Name: seg_revisao seg_revisao_id_estudo_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_revisao
    ADD CONSTRAINT seg_revisao_id_estudo_fkey FOREIGN KEY (id_estudo) REFERENCES public.seg_estudo(id_estudo) ON DELETE CASCADE;


--
-- TOC entry 3822 (class 2606 OID 21485)
-- Name: seg_sala_exame seg_sala_exame_id_exame_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_sala_exame
    ADD CONSTRAINT seg_sala_exame_id_exame_fkey FOREIGN KEY (id_exame) REFERENCES public.seg_exame(id_exame);


--
-- TOC entry 3823 (class 2606 OID 21490)
-- Name: seg_sala_exame seg_sala_exame_id_sala_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_sala_exame
    ADD CONSTRAINT seg_sala_exame_id_sala_fkey FOREIGN KEY (id_sala) REFERENCES public.seg_sala(id_sala) ON DELETE CASCADE;


--
-- TOC entry 3821 (class 2606 OID 294377)
-- Name: seg_sala seg_sala_id_empresa_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_sala
    ADD CONSTRAINT seg_sala_id_empresa_fkey FOREIGN KEY (id_empresa) REFERENCES public.seg_empresa(id_empresa);


--
-- TOC entry 3826 (class 2606 OID 21495)
-- Name: seg_usuario_cliente seg_usuario_cliente_id_cliente_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_usuario_cliente
    ADD CONSTRAINT seg_usuario_cliente_id_cliente_fkey FOREIGN KEY (id_cliente) REFERENCES public.seg_cliente(id_cliente) ON DELETE CASCADE;


--
-- TOC entry 3827 (class 2606 OID 21500)
-- Name: seg_usuario_cliente seg_usuario_cliente_id_usuario_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_usuario_cliente
    ADD CONSTRAINT seg_usuario_cliente_id_usuario_fkey FOREIGN KEY (id_usuario) REFERENCES public.seg_usuario(id_usuario) ON DELETE CASCADE;


--
-- TOC entry 3828 (class 2606 OID 21505)
-- Name: seg_usuario_funcao_interna seg_usuario_funcao_interna_id_funcao_interna_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_usuario_funcao_interna
    ADD CONSTRAINT seg_usuario_funcao_interna_id_funcao_interna_fkey FOREIGN KEY (id_funcao_interna) REFERENCES public.seg_funcao_interna(id_funcao_interna);


--
-- TOC entry 3829 (class 2606 OID 21510)
-- Name: seg_usuario_funcao_interna seg_usuario_funcao_interna_id_usuario_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_usuario_funcao_interna
    ADD CONSTRAINT seg_usuario_funcao_interna_id_usuario_fkey FOREIGN KEY (id_usuario) REFERENCES public.seg_usuario(id_usuario);


--
-- TOC entry 3824 (class 2606 OID 21515)
-- Name: seg_usuario seg_usuario_id_empresa_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_usuario
    ADD CONSTRAINT seg_usuario_id_empresa_fkey FOREIGN KEY (id_empresa) REFERENCES public.seg_empresa(id_empresa);


--
-- TOC entry 3825 (class 2606 OID 21520)
-- Name: seg_usuario seg_usuario_id_medico_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_usuario
    ADD CONSTRAINT seg_usuario_id_medico_fkey FOREIGN KEY (id_medico) REFERENCES public.seg_medico(id_medico);


--
-- TOC entry 3830 (class 2606 OID 21525)
-- Name: seg_usuario_perfil seg_usuario_perfil_id_perfil_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_usuario_perfil
    ADD CONSTRAINT seg_usuario_perfil_id_perfil_fkey FOREIGN KEY (id_perfil) REFERENCES public.seg_perfil(id_perfil);


--
-- TOC entry 3831 (class 2606 OID 21530)
-- Name: seg_usuario_perfil seg_usuario_perfil_id_usuario_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_usuario_perfil
    ADD CONSTRAINT seg_usuario_perfil_id_usuario_fkey FOREIGN KEY (id_usuario) REFERENCES public.seg_usuario(id_usuario);


--
-- TOC entry 3832 (class 2606 OID 294382)
-- Name: seg_vendedor seg_vendedor_id_cidade_ibge_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_vendedor
    ADD CONSTRAINT seg_vendedor_id_cidade_ibge_fkey FOREIGN KEY (id_cidade_ibge) REFERENCES public.seg_cidade_ibge(id_cidade_ibge);


--
-- TOC entry 3833 (class 2606 OID 21535)
-- Name: seg_versao seg_versao_id_estudo_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_versao
    ADD CONSTRAINT seg_versao_id_estudo_fkey FOREIGN KEY (id_estudo) REFERENCES public.seg_estudo(id_estudo) ON DELETE CASCADE;


-- Completed on 2024-09-17 21:38:01

--
-- PostgreSQL database dump complete
--

