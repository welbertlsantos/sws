﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SWS.Resources
{
    class ConfigurationConstans
    {
        public static String DIRETORIO = "DIRETORIO_PADRAO_APLICACAO";
        public static string SIMPLES_NACIONAL = "EMPRESA_NO_SIMPLES_NACIONAL";
        public static String ISS_PADRAO = "ALIQUOTA_SERVICO";
        public static String PIS = "PIS_PERCENTUAL";
        public static String COFINS = "COFINS_PERCENTUAL";
        public static String IR = "IR_PERCENTUAL";
        public static string INSS = "INSS_PERCENTUAL";
        public static String CSSL = "CSLL_PERCENTUAL";
        public static string NUMERO_NF = "NUMERO_INICIAL_NF";
        public static String PERCENTUAL_JUROS_MENSAL_PRORROGACAO_CRC = "PERCENTUAL_JUROS_MENSAL_PRORROGACAO_CRC";
        public static String QTD_VIAS_IMPRESSORA_TERMICA = "QTD_VIAS_IMPRESSORA_TERMICA";
        public static String EMPRESA_IMPRESSORA_TERMICA = "EMPRESA_IMPRESSORA_TERMICA";
        public static String SERVIDOR_IMPRESSORA_TERMICA = "SERVIDOR_IMPRESSORA_TERMICA";
        public static String NOME_IMPRESSORA_TERMICA = "NOME_IMPRESSORA_TERMICA";
        public static String SERIAL_CLIENTE = "SERIAL_CLIENTE";
        public static String GERA_MOVIMENTO_FINALIZACAO_EXAME = "GERA_MOVIMENTO_FINALIZACAO_EXAME";
        public static String VALOR_MINIMO_IMPOSTO_FEDERAL = "VALOR_MINIMO_IMPOSTO_FEDERAL";
        public static String VALOR_MINIO_IR = "VALOR_MINIO_IR";
        public static String GRAVA_PRODUTO_AUTOMATICO = "GRAVA_PRODUTO_AUTOMATICO";
        public static String ASO_2_VIA = "ASO_2_VIA";
        public static String ASO_TRANSCRITO = "ASO_TRANSCRITO";
        public static String ASO_INCLUSAO_EXAMES = "ASO_INCLUSAO_EXAMES";
        public static string CIDADE_EMPRESA = "CIDADE_EMPRESA";
        public static string NOME_EMPRESA = "NOME_EMPRESA";
        public static string ENDERECO_EMPRESA = "ENDERECO_EMPRESA";
        public static string NUMERO_EMPRESA = "NUMERO_EMPRESA";
        public static string COMPLEMENTO_EMPRESA = "COMPLEMENTO_EMPRESA";
        public static string BAIRRO_EMPRESA = "BAIRRO_EMPRESA";
        public static string UF_EMPRESA = "UF_EMPRESA";
        public static string CEP_EMPRESA = "CEP_EMPRESA";
        public static string CNPJ_EMPRESA = "CNPJ_EMPRESA";
        public static string INSCRICAO_ESTADUAL_EMPRESA = "INSCRICAO_ESTADUAL_EMPRESA";
        public static string EMAIL_EMPRESA = "EMAIL_EMPRESA";
        public static String SERVIDOR_SMTP = "SERVIDOR_SMTP";
        public static String NUMERO_PORTA = "NUMERO_PORTA";
        public static String SSL = "SSL";
        public static String EMAIL_ENVIO = "EMAIL_ENVIO";
        public static String SENHA_EMAIL = "SENHA_EMAIL";
        public static String USAR_FILIAL = "USA_FILIAL";
        public static string PERIODO_PESQUISA = "PERIODO_PESQUISA_ATENDIMENTO";
        public static string IMPRIME_SALA = "IMPRIME_SOMENTE_SALA";
        public static string PCMSO_VENCIDO_ASO = "IMPRIME_DADOS_PCMSO_VENCIDO";
        public static string ORDENACAO_ATENDIMENTO = "ORDENAR_ATENDIMENTO_POR_SENHA";
        public static string VERSAO_SISTEMA = "VERSAO_SISTEMA";
        public static string CANCELA_EXAME_APOS_FINALIZADO_ATENDIMENTO = "CANCELA_EXAME_APOS_FINALIZADO_ATENDIMENTO";
        public static string GRAVA_ATENDIMENTO_BLOQUEADO = "GRAVAR_ATENDIMENTO_BLOQUEADO";
        public static string MEDICO_EXAMINADOR_ATENDIMENTO = "MEDICO_EXAMINADOR_ATENDIMENTO";
        public static string CONVERT_ESOCIAL_2220_PROCEDIMENTO = "CONVERT_ESOCIAL_2220_PROCEDIMENTO";
        public static string DIRETORIO_PADRAO_EXPORTACAO_EXAMES_LABORATORIO = "DIRETORIO_PADRAO_EXPORTACAO_EXAMES_LABORATORIO";
        public static string ANALISE_CLASSIFICACAO_RISCO = "ANALISE_CLASSIFICACAO_RISCO";


    }
}
