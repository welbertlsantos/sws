﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SWS.Entidade;
using Npgsql;
using NpgsqlTypes;
using System.Data;

namespace SWS.IDao
{
	interface IRelatorioDao
	{

        DataSet findAllRelatoriosBySituacao(String tipo, NpgsqlConnection dbConnection);
        Int64 findUltimaVersaoByTipo(String tipoRelatorio, NpgsqlConnection dbConnection);
        String findNomeRelatorioById(Int64? idRelatorio, NpgsqlConnection dbConnection);
        Relatorio findById(long id, NpgsqlConnection dbConnection);
	}
}
