﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SWS.Entidade;
using Npgsql;
using System.Data;

namespace SWS.IDao
{
    interface IClienteDao
    {
        /* metodo responsavel por recuperar um cliente no banco de dados de acordo com o filtro selecionado. */
        DataSet findClienteByFilter(Cliente cliente, Boolean? prestador, Boolean? credenciada, Boolean? unidade, DateTime? dtCadastroFinal,  Boolean? aptoContrato, bool? particular, Usuario usuarioLogado, bool? credenciadora, bool? fisica, NpgsqlConnection dbConnection);

        /* metodo responsavel por recuperar um cliente no banco de dados de acordo com cnpj passado por parametro. */
        Cliente findClienteByCnpj(string cnpj, NpgsqlConnection dbConnection);

        /* metodo responsavel por incluir um cliente na tabela seg_cliente. */
        Cliente insert(Cliente cliente, NpgsqlConnection dbConnection);

        /* metodo responsavel por recuperar da tabela seg_cnae de acordo com o filtro selecionado. */
        DataSet findCnaeByFilter(Cnae cnae, NpgsqlConnection dbConnection);

        /* metodo responsavel por recuperar um cliente no banco de dados de acordo com o id */
        Cliente findById(Int64 idCliente, NpgsqlConnection dbConnection);

        /* metodo responsavel por alterar os dados do cliente na tabela seg_cli */
        Cliente updateCliente(Cliente cliente, NpgsqlConnection dbConnection);

        /* metodo responsavel por desativar um cliente na base de dados mudando o seu flag para I */
        void changeStatusCliente(Int64 idVendedor, string situacao, NpgsqlConnection dbConnection);

        /* metodo responsavel por retornar todos os clientes ativos de acordo com o filtro selecionado em tela */
        DataSet findClienteAtivoByFilter(Cliente cliente, NpgsqlConnection dbConnection);

        /* Metodo responsavel por procurar um CNAE dado um codigo Cnae <> IdCnae */
        Cnae findCnaeByCodigo(Cnae cnae, NpgsqlConnection dbConnection);

        /* Metodo responsavel por incluir um CNAE  */
        Cnae incluirCnae(Cnae cnae, NpgsqlConnection dbConnection);

        /* Metodo responsavel por retornar um Cnae dado um id(inteiro)  */
        Cnae findCnaeById(Int64 idCnae, NpgsqlConnection dbConnection);

        /* Metodo responsavel por alterar um Cnae dado um id(inteiro)  */
        Cnae updateCnae(Cnae cnae, NpgsqlConnection dbConnection);

        // Metodo responsavel por excluir um cnae no banco de dados.
        void deleteCnae(Int64 idCnae, NpgsqlConnection dbConnection);

        DataSet findClienteAtivoByFilterNotInClienteFuncaoFuncionarioSet(Cliente cliente,
            List<ClienteFuncaoFuncionario> clienteFuncaoFuncionarioSet, NpgsqlConnection dbConnection);

        DataSet findClienteAtivoAptoContrato(Cliente cliente, NpgsqlConnection dbConnection);

        Boolean verificaPodeAlterarCliente(Cliente cliente, NpgsqlConnection dbConnection);

        DataSet findAllClienteParticularAtivosByFilter(Cliente cliente, NpgsqlConnection dbConnection);

        List<Cliente> findAllPrestadorByCliente(Cliente cliente, Cliente prestador, NpgsqlConnection dbConnection);

        void changeSituacao(Cnae cnae, NpgsqlConnection dbConnection);

        Cliente findClienteByRazaoSocial(String razaoSocial, NpgsqlConnection dbConnection);

        DataSet findAllUnidadeByCliente(Cliente cliente, NpgsqlConnection dbConnection);

        Boolean findClienteHaveUnidade(Cliente cliente, NpgsqlConnection dbConnection);

        DataSet findCnaeNotInCliente(Cnae cnae, Cliente cliente, NpgsqlConnection dbConnection);

        DataSet findAllClienteByCnpjAptoContrato(String cnpj, NpgsqlConnection dbConnection);

        DataSet findAllCredenciadasByCliente(Cliente cliente, NpgsqlConnection dbConnection);
    }
}
