﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SWS.Entidade;
using Npgsql;
using System.Data;

namespace SWS.IDao
{
    interface INfeDao
    {
        Nfe insert(Nfe nfe, NpgsqlConnection dbConnection);

        DataSet findByFilter(Nfe notaFiscal, DateTime? dataEmissaoInicial,
            DateTime? dataEmissaoFinal, DateTime? dataCancelamentoInicial, DateTime? dataCancelamentoFinal, NpgsqlConnection dbConnection);

        void cancela(Nfe nfe, NpgsqlConnection dbConnection);

        Boolean findByNumero(long numeroNf, NpgsqlConnection dbConnection);

        Nfe findById(Int64 id, NpgsqlConnection dbConnection);

        long? findLastNumeroNf(NpgsqlConnection dbConnection);
        
        void restartSequence(long numeroNf, NpgsqlConnection dbConnection);

    }
}
