﻿using System;
using System.Collections.Generic;
using System.Text;
using SWS.Entidade;
using Npgsql;
using System.Data;


namespace SWS.IDao
{
    interface IExameDao
    {
        Exame insert(Exame exame, NpgsqlConnection dbConnection);
        Exame update(Exame exame, NpgsqlConnection dbConnection);
        Int64 recuperaProximoId(NpgsqlConnection dbConnection);
        Exame findByDescricao(String descricao, NpgsqlConnection dbConnection);
        Exame findById(Int64 idExame, NpgsqlConnection dbConnection);

        // Método responsável por retornar um data set com um conjunto de exames de acordo com a opção de filtro selecionado.
        DataSet findByFilter(Exame exame, NpgsqlConnection dbConnection);


        Boolean findInGheFonteAgenteExame(Exame exame, NpgsqlConnection dbConnection);

        Boolean findInExameAso(Exame exame, NpgsqlConnection dbConnection);

        void delete(Exame exame, NpgsqlConnection dbConnection);

        Int32 buscaQuantidadeExamesByGhe(Ghe ghe, NpgsqlConnection dbConnection);

        DataSet findAtivoNotInClienteFuncao(Exame exame, ClienteFuncao clienteFuncao, NpgsqlConnection dbConnection);

        DataSet findAtivoInClienteFuncao(ClienteFuncao clienteFuncao, NpgsqlConnection dbConnection);

        DataSet findAtivoNotInContrato(Contrato contrato, Exame exame, NpgsqlConnection dbConnection);

        // localiza todos os exames ativos no sistema.
        DataSet findAtivo(Exame exame, NpgsqlConnection dbConnection);

        LinkedList<Exame> findAtivoNotInAso(HashSet<Int64> idExames, Exame exame, NpgsqlConnection dbConnection);

        DataSet findAtivoNotInPropostaContrato(List<Exame> exames, Exame exame, NpgsqlConnection dbConnection);

        List<Exame> findAllAtivosPadraoContrato(NpgsqlConnection dbConnection);
    }
}
