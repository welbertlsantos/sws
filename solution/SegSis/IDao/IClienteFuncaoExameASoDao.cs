﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SWS.Entidade;
using Npgsql;
using NpgsqlTypes;

namespace SWS.IDao
{
    interface IClienteFuncaoExameASoDao
    {
        void insert(ClienteFuncaoExameASo clienteFuncaoExameAso, NpgsqlConnection dbConnection);

        Int64 recuperaProximoId(NpgsqlConnection dbConnection);

        List<ClienteFuncaoExameASo> findAllExamesByAsoInSituacao(Aso aso, Boolean? atendido, Boolean? finalizado, Boolean? cancelado, Boolean? devolvido, Boolean? transcrito, NpgsqlConnection dbConnection);

        void updateClienteFuncaoExameASoDaoInFilaAtendimento(ItemFilaAtendimento filaAtendimento, Boolean? atendido, Boolean? finalizado, Boolean? cancelado, 
            Boolean? devolvido, Int64? idSalaExame, String tipoAtendimento, Usuario usuario, NpgsqlConnection dbConnection);

        Boolean verificaClienteFuncaoExameAsoInSituacao(Int64? idClienteFuncaoExameAso, Boolean? atendido, Boolean? finalizado, Boolean? cancelado,
            Boolean? devolvido, NpgsqlConnection dbConnection);

        Boolean verificaExameAtendimento(Aso aso, NpgsqlConnection dbConnection);

        void cancelaExameAso(Aso aso, NpgsqlConnection dbConnection);

        Int32 buscaMaiorPrioridade(Aso aso, NpgsqlConnection dbConnection);

        int buscaQuantidadesExamesSendoAtendidosEmUmaSala(SalaExame salaExameProcurada, NpgsqlConnection dbConnection);

        HashSet<ClienteFuncaoExameASo> buscaExamesNaoAtendidosComMaiorprioridade(Aso aso, NpgsqlConnection dbConnection);

        void IniciarAtendimentoExame(ClienteFuncaoExameASo clienteFuncaoExameAso, SalaExame salaExame, Boolean funcionarioVip, NpgsqlConnection dbConnection);
        
        HashSet<ClienteFuncaoExameASo> buscaExamesNaoAtendidosComMesmaPrioridadeAtendidosNaMesmaSala(Aso aso, Int32 prioridade, NpgsqlConnection dbConnection);

        LinkedList<ClienteFuncaoExameASo> findAllExamesByAsoInSituacaoBySala(Aso aso, Boolean? atendido, Boolean? finalizado, 
            Boolean? cancelado, Boolean? devolvido, Sala sala, bool ordenacao, NpgsqlConnection dbConnection);

        HashSet<Int64> findAllExameInAso(Aso aso, NpgsqlConnection dbConnection);

        Boolean isPrimeiroAtendimento(Aso aso, NpgsqlConnection dbConnection);

        void alteraClienteFuncaoExameAso(ClienteFuncaoExameASo clienteFuncaoExameAso, NpgsqlConnection dbconnection);

        LinkedList<ClienteFuncaoExameASo> findAllExamesByAsoInSituacaoNotExterno(Aso aso, Boolean? atendido, Boolean? finalizado,
            Boolean? cancelado, Boolean? devolvido, NpgsqlConnection dbConnection);

        void updateInformacaoLaudo(ClienteFuncaoExameASo cfea, NpgsqlConnection dbConnection);

        void excluirPrestadorClienteFuncaoExameAso(ClienteFuncaoExameASo clienteFuncaoExameAso, NpgsqlConnection dbConnection);

        void incluirPrestadorClienteFuncaoExameAso(ClienteFuncaoExameASo clienteFuncaoExameAso, NpgsqlConnection dbConnection);

        Cliente findClienteByClienteFuncaoExameASo(ClienteFuncaoExameASo clienteFuncaoExameAso, NpgsqlConnection dbConnection);

        ClienteFuncaoExameASo findById(Int64 id, NpgsqlConnection dbConnection);

    }
}
