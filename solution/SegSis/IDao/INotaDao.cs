﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using SWS.Entidade;
using Npgsql;
using NpgsqlTypes;

namespace SWS.IDao
{
    interface INotaDao
    {

        // metodo responsavel por recuperar um objeto norma de acordo com o filtro selecionado.
        DataSet findByFilter(Nota norma, NpgsqlConnection dbConnection);

        // metodo responsavel por incluir uma norma no sistema.
        Nota insert(Nota norma, NpgsqlConnection dbConnection);

        // metodo responsavel por encontrar uma norma dado uma descricao.
        Nota findByDescricao(Nota norma, NpgsqlConnection dbConnection);

        // metodo responsavel por recuperar um objeto norma dado um id de banco.
        Nota findById(Int64 id, NpgsqlConnection dbConnection);

        // metodo responsavel por alterar um objeto norma no banco de dados.
        Nota update(Nota norma, NpgsqlConnection dbConnection);

        // metodo responsavel por retornar um variavel boolenada true caso existe uma norma que nao pode ser alterada.
        Boolean findInGheEstudo(Nota norma, NpgsqlConnection dbConnection);

        // metodo responsavel por retornar uma variaval booleana true caso exista uma norma que não pode ser excluída.
        Boolean findInByGheNota(Nota norma, NpgsqlConnection dbConnection);

        // metodo responsavel por retornar em um dataset as normas que ainda não foram incluídas no ghe.
        DataSet findByNotaGhe(Nota norma, Ghe ghe, NpgsqlConnection dbConnection);
    }
}
