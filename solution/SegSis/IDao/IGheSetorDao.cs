﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SWS.Entidade;
using Npgsql;
using NpgsqlTypes;
using System.Data;

namespace SWS.IDao
{
    interface IGheSetorDao
    {
        // metodo responsavel por retornar uma colecao de GheSetor dado um Ghe;
        HashSet<GheSetor> findAtivosByGhe(Ghe ghe, NpgsqlConnection dbConnection);

        // metodo responsavel por incluir um GheSetor no banco de dados.
        GheSetor insert(GheSetor gheSetor, NpgsqlConnection dbConnection);

        // metodo responsavel por excluir um GheSetor do banco de dados.
        void delete(GheSetor gheSetor, NpgsqlConnection dbConnection);

        // metodo responsal por retornar um data set com todos os setores dado um ghe.        
        DataSet findAllByGhe(Ghe ghe, NpgsqlConnection dbConnection);

        // método responsável por buscar o número de setores de um GHE.
        Int32 buscaQuantidadeByGhe(Ghe ghe, NpgsqlConnection dbConnection);

        void update(GheSetor gheSetor, NpgsqlConnection dbConnection);

        Boolean gheSetorIsUsedBySetor(Setor setor, NpgsqlConnection dbConnection);

        GheSetor findById(Int64 id, NpgsqlConnection dbConnection);

        LinkedList<GheSetor> findAllBySetorAndGhe(Setor setor, Ghe ghe, NpgsqlConnection dbConnection);

        bool IsUsedInService(GheSetor gheSetor, NpgsqlConnection dbConnection);

        List<GheSetor> findAllByGheList(Ghe ghe, NpgsqlConnection dbConnection);

    }
}
