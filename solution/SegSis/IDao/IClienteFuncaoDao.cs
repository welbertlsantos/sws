﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SWS.Entidade;
using Npgsql;
using NpgsqlTypes;
using System.Data;

namespace SWS.IDao
{
    interface IClienteFuncaoDao
    {
        // metodo responsavel por incluir um objeto cliente funcao no banco de dados.        
        ClienteFuncao insert(ClienteFuncao clientefuncao, NpgsqlConnection dbConnection);

        // metodo responsavel por retornar um conjunto de funcaoes dado um cliente.
        DataSet findAtivosByCliente(Cliente cliente, NpgsqlConnection dbConnection);

        // metodo responsavel por desativar uma funcao no cliente.
        void update(ClienteFuncao clientefuncao, NpgsqlConnection dbConnection);

        DataSet findAtivosByClienteFuncao(ClienteFuncao clienteFuncao, NpgsqlConnection dbConnection);

        DataSet findAtivoByClienteFuncaoAndGheSetorNotInGheSetor(ClienteFuncao clienteFuncao, GheSetor gheSetor, NpgsqlConnection dbConnection);

        Boolean funcaoIsUsedInEstudo(Funcao funcao, NpgsqlConnection dbConnection);

        ClienteFuncao findById(Int64 id, NpgsqlConnection dbConnection);

        Boolean IsPresentInPcmso(Estudo pcmso, ClienteFuncao clienteFuncao, NpgsqlConnection dbConnection);

        List<ClienteFuncao> findAtivosByClienteList(Cliente cliente, NpgsqlConnection dbConnection);

        LinkedList<ClienteFuncao> findAllByFuncaoAndCliente(Funcao funcao, Cliente cliente, NpgsqlConnection dbConnection);

    }
}
