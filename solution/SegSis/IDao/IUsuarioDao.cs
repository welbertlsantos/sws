﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SWS.Entidade;
using Npgsql;
using System.Data;

namespace SWS.IDao
{
    interface IUsuarioDao
    {
        /**
         * Método responsável por autenticar um usuário na
         * base de dados.
         */
        Usuario autentica(Usuario usuario, NpgsqlConnection dbConnection);
        
        /**
         * Método responsável por criar um incluir usuário na base de dados.
         */
        Usuario insert(Usuario usuario, NpgsqlConnection dbConnection);
        
        /**
         * Método respnsável por recuperar o próximo identificador de usuário.
         */
        Int64 recuperaProximoId(NpgsqlConnection dbConnection);

        /**
         * Método responsável por recuperar Nome lista de todos os usuários
         * da base de dados.
         */
        DataSet listaTodos(NpgsqlConnection dbConnection);

        /**
         * Método responsável por recuperar Nome lista de todos os usuários
         * ativos da base de dados.
         */
        DataSet listaTodosAtivos(NpgsqlConnection dbConnection);

        /**
         * Método responsável por recuperar Nome lista de todos os usuários
         * da acordo com filtro.
         */
        DataSet findByFilter(Usuario usuario, NpgsqlConnection dbConnection);

        /**
         * Método responsável por recuperar um usuário na
         * base de dados.
         */
        Usuario findById(Int64 idUsuario, NpgsqlConnection dbConnection);

        /**
         * Método responsável por recuperar um usuário na
         * base de dados.
         */
        Usuario update(Usuario usuario, NpgsqlConnection dbConnection);

        /**
         * Método responsável por buscar um usuário na base de dados dado cpf.
         */
        List<Usuario> findByCpf(string cpf, NpgsqlConnection dbConnection);
                
        // Método responsável por buscar um usuário na base de dados dado um login
        Usuario findUsuarioByLogin(string login, NpgsqlConnection dbConnection);

        // Método responsável por alterar a senha de um usuário.
        void updateSenha(Usuario usuario, NpgsqlConnection dbConnection);

        
        // Método responsável por listar todos os usuarios especiais de acordo com id da função passada por valor .
        DataSet findUsuarioFuncaoAtivoByFilter(Usuario usuario, Int64 idFuncaoProcurada, NpgsqlConnection dbConnection);

        LinkedList<Usuario> findUsuarioByFuncaoInterna(Int64 idFuncaoInterna, Int64? idFuncaoInternaFinal, NpgsqlConnection dbConnection);

        DataSet findUsuarioElaboraDocumento(Usuario usuario, NpgsqlConnection dbConnection);
    }
}
