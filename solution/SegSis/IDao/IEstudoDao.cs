﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SWS.Entidade;
using Npgsql;
using System.Data;


namespace SWS.IDao
{
    interface IEstudoDao
    {
        Estudo insert(Estudo ppra, NpgsqlConnection dbConnection);

        Estudo update(Estudo pcmso, NpgsqlConnection dbConnection);

        Cnae findCnaeById(Int64 idCnae, NpgsqlConnection dbConnection);

        Int64 replicaEstudo(Int64 idPpra, Int64? idCronograma, Int64 idRelatorio, NpgsqlConnection dbConnection);

        Estudo findById(Int64 id, NpgsqlConnection dbConnection);

        string findCodigoEstudoById(Int64 idPpra, NpgsqlConnection dbConnection);
        
        void delete(Int64 id, NpgsqlConnection dbConnection);

        void changeEstado(Estudo ppra, String estado, NpgsqlConnection dbConnection);

        void limpaVersao(Int64 idPpra, NpgsqlConnection dbConnection);

        void gravaVersao(Versao versao, NpgsqlConnection dbConnection);
        
        void finalizaEstudo(Estudo ppra, NpgsqlConnection dbConnection);

        Boolean verificaExistePcmsoAvusoCadastrado(Cliente cliente, Cliente clienteContratado, NpgsqlConnection dbConnection);

        Boolean pcmsoIsUnidade(Estudo pcmso, NpgsqlConnection dbConnection);

        void insertInProtocolo(Protocolo protocolo, NpgsqlConnection dbConnection);

        DataSet findByFilter(Estudo pcmso, DateTime? dataCriacaoFinal, NpgsqlConnection dbConnection);

        void gravaCronograma(Cronograma cronograma, Estudo pcmso, NpgsqlConnection dbConnection);

        LinkedList<Estudo> findLastPcmsoByCliente(Cliente Cliente, NpgsqlConnection dbConnection);

        bool isPcmsoUsedByAso(long id, NpgsqlConnection dbConnection);

        void disablePcmso(long id, NpgsqlConnection dbConnection);
    }
}
