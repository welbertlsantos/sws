﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NpgsqlTypes;
using Npgsql;
using SWS.Entidade;

namespace SWS.IDao
{
    interface IGheFonteAgenteExameAsoDao
    {
        Int64 recuperaProximoId(NpgsqlConnection dbConnection);

        void insert(GheFonteAgenteExameAso gheFonteAgenteExameAso, NpgsqlConnection dbConnection);

        List<GheFonteAgenteExameAso> findAllExamesByAsoInSituacao(Aso aso, Boolean? atendido, Boolean? finalizado, Boolean? cancelado, Boolean? devolvido, Boolean? transcrito, NpgsqlConnection dbConnection);

        void updateGheFonteAgenteExameAsoDaoInFilaAtendimento(ItemFilaAtendimento filaAtendimento, Boolean? atendido, Boolean? finalizado, Boolean? cancelado, Boolean? devolvido,
            Int64? idSalaExame, String tipoAtendimento, Usuario usuario, NpgsqlConnection dbConnection);

        Boolean verificaGheFonteAgenteExameASOInSituacao(Int64? idGheFonteAgenteExameAso, Boolean? atendido, Boolean? finalizado,
            Boolean? cancelado, Boolean? devolvido, NpgsqlConnection dbConnection);

        Boolean verificaExameAtendimento(Aso aso, NpgsqlConnection dbConnection);

        void cancelaExameAso(Aso aso, NpgsqlConnection dbConnection);

        Int32 buscaMaiorPrioridade(Aso aso, NpgsqlConnection dbConnection);

        Boolean permiteExcluirGhe(Ghe ghe, NpgsqlConnection dbConnection);

        HashSet<GheFonteAgenteExameAso> buscaExamesNaoAtendidosComMaiorprioridade(Aso aso, NpgsqlConnection dbConnection);

        Int32 buscaQuantidadesExamesSendoAtendidosEmUmaSala(SalaExame salaExame, NpgsqlConnection dbConnection);

        void IniciarAtendimentoExame(GheFonteAgenteExameAso gheFonteAgenteExameAso, SalaExame salaExame, Boolean funcionarioVip, NpgsqlConnection dbConnection);

        HashSet<GheFonteAgenteExameAso> buscaExamesNaoAtendidosComMesmaPrioridadeAtendidosNaMesmaSala(Aso aso, Int32 prioridade, NpgsqlConnection dbConnection);

        LinkedList<GheFonteAgenteExameAso> findAllExamesByAsoInSituacaoBySala(Aso aso, Boolean? atendido, Boolean? finalizado, 
            Boolean? cancelado, Boolean? devolvido, Sala sala, bool ordenacao, NpgsqlConnection dbConnection);

        HashSet<Int64> findAllExameInAso(Aso aso, NpgsqlConnection dbConnection);

        Boolean isPrimeiroAtendimento(Aso aso, NpgsqlConnection dbConnection);

        void alteraGheFonteAgenteExameAso(GheFonteAgenteExameAso gheFonteAgenteExameAso, NpgsqlConnection dbconnection);

        LinkedList<GheFonteAgenteExameAso> findAllExamesByAsoInSituacaoNotExterno(Aso aso, Boolean? atendido, Boolean? finalizado, Boolean? cancelado, Boolean? devolvido, NpgsqlConnection dbConnection);

        void updateInformacaoLaudo(GheFonteAgenteExameAso gheFonteAgenteExameAso, NpgsqlConnection dbconnection);

        void excluirPrestadorGheFonteAgenteExameAso(GheFonteAgenteExameAso gheFonteAgenteExameAso, NpgsqlConnection dbConnection);

        void incluirPrestadorGheFonteAgenteExameAso(GheFonteAgenteExameAso gheFonteAgenteExameAso, NpgsqlConnection dbConnection);

        Cliente findClienteByGheFonteAgenteExameASo(GheFonteAgenteExameAso gheFonteAgenteExameAso, NpgsqlConnection dbConnection);

        GheFonteAgenteExameAso findById(Int64 id, NpgsqlConnection dbConnection);

        Boolean verificaExisteExameNaoLaudadoNoAtendimentoPorCliente(Cliente cliente, DateTime dataInicial, DateTime dataFinal, Cliente unidade,
            Boolean matriz, CentroCusto centroCusto, NpgsqlConnection dbConnection);
        
    }
}
