﻿using Npgsql;
using SWS.Entidade;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SWS.IDao
{
    interface IMonitoramentoGheFonteAgenteDao
    {
        MonitoramentoGheFonteAgente insert(MonitoramentoGheFonteAgente monitoramentoGheFonteAgente, NpgsqlConnection dbConnection);

        MonitoramentoGheFonteAgente update(MonitoramentoGheFonteAgente monitoramentoGheFonteAgente, NpgsqlConnection dbConnection);

        MonitoramentoGheFonteAgente findById(long id, NpgsqlConnection dbConnection);

        void delete(MonitoramentoGheFonteAgente monitoramentoGheFonteAgente, NpgsqlConnection dbConnection);

        List<MonitoramentoGheFonteAgente> findAllByMonitoramento(Monitoramento monitoramento, NpgsqlConnection dbConnection);

        MonitoramentoGheFonteAgente findMonitoramentoGheFonteAgenteByGheFonteAgente(GheFonteAgente gheFonteAgente, Monitoramento monitoramento, NpgsqlConnection dbConnection);
    }
}
