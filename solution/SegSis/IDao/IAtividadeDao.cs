﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SWS.Entidade;
using Npgsql;
using System.Data;

namespace SWS.IDao
{
    interface IAtividadeDao
    {
    
        /*
         * metodo responsavel por incluir uma atividade no banco de dados
        */
        Atividade insert(Atividade atividade, NpgsqlConnection dbConnection);

        /*
         * metodo responsavel por recuperar o próximo identificado da atividade.
        */
        Int64 recuperaProximoId(NpgsqlConnection dbConnection);

        /* metodo responsavel por pesquisar uma descricao de atividade no banco de dados
        */
        Atividade findByDescricao(String atividade, NpgsqlConnection dbConnection);

        /*
         * metodo para recuparar a atividade de acordo com o filtro selecionado
        
         */
        DataSet findByFilter(Atividade atividade, NpgsqlConnection dbConnection);

        /* metodo responsavel por recuperar a atividade no banco de dados.
        */

        Atividade findById(Int64 idAtividade, NpgsqlConnection dbConnection);
        
        /* metodo responsavel por alterar uma atividade no banco de dados
        */
         Atividade update(Atividade atividadeAlterar, NpgsqlConnection dbConnection);

        // metodo responsavel por recuperar todas as atividades ativas do banco de dados.
        DataSet findByAtividade(Atividade atividade, NpgsqlConnection dbConnection);

        DataSet findByAtividadeAndCronograma(Atividade atividade, Cronograma cronograma, NpgsqlConnection dbConnection);
    }
}
