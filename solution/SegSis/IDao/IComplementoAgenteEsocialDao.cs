﻿using Npgsql;
using SWS.Entidade;
using SWS.Helper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SWS.IDao
{
    interface IComplementoAgenteEsocialDao
    {
        List<ComplementoAgenteEsocial> findAllByFilter(ComplementoAgenteEsocial complementoAgenteEsocial, NpgsqlConnection dbConnection);
        
    }
}
