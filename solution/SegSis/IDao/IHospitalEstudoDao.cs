﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SWS.Entidade;
using Npgsql;
using NpgsqlTypes;
using System.Data;

namespace SWS.IDao
{
    interface IHospitalEstudoDao
    {
        void incluirHospitalEstudo(HospitalEstudo hospitalEstudo, NpgsqlConnection dbConnection);

        DataSet findAllHospitalEstudo(Estudo pcmso, NpgsqlConnection dbConnection);

        void excluirHospitalEstudo(HospitalEstudo hospitalEstudo, NpgsqlConnection dbConnection);

        Boolean findHospitalEstudoByHospital(Hospital hospital, NpgsqlConnection dbConnection);

    }
}
