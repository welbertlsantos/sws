﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SWS.Entidade;
using Npgsql;

namespace SWS.IDao
{
    interface IParcelaDao
    {
        Parcela insert(Parcela parcela, NpgsqlConnection dbConnection);

        List<Parcela> findParcelasByPLano(Plano plano, NpgsqlConnection dbConnection);
        void deleteByPlano(Plano plano, NpgsqlConnection dbConnection);
        void delete(Parcela parcela, NpgsqlConnection dbConnection);
        Parcela update(Parcela parcela, NpgsqlConnection dbConnection);

    }
}
