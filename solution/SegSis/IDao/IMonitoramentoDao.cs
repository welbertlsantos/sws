﻿using Npgsql;
using SWS.Entidade;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SWS.IDao
{
    interface IMonitoramentoDao
    {
        Monitoramento insert(Monitoramento monitoramento, NpgsqlConnection dbConnection);

        Monitoramento update(Monitoramento monitoramento, NpgsqlConnection dbConnection);

        Monitoramento findById(long id, NpgsqlConnection dbConnection);

        void delete(Monitoramento monitoramento, NpgsqlConnection dbConnection);

        List<Monitoramento> findAllByClienteAndPeriodo(Cliente cliente, DateTime periodo, NpgsqlConnection dbConnection);

        List<Monitoramento> findAllByLote(LoteEsocial lote, NpgsqlConnection dbConnection);

        string findAtividadeDoFuncionarioByMonitoramento(Monitoramento monitoramento, NpgsqlConnection dbConnection);

        string findFuncaoLotacaoFuncionarioByMonitoramento(Monitoramento monitoramento, NpgsqlConnection dbConnection);

        string findSetorInLotacaoFuncionarioByMonitoramento(Monitoramento monitoramento, NpgsqlConnection dbConnection);

        List<Monitoramento> findAllByFilter(Monitoramento monitoramento, NpgsqlConnection dbConnection);

        Monitoramento findLastMonitoramentoByClienteFuncaoFuncionario(ClienteFuncaoFuncionario clienteFuncaoFuncionario, DateTime periodo, NpgsqlConnection dbConnection);

        bool existMonitoramentoPassadoByClienteAndPeriodo(Cliente cliente, DateTime periodo, NpgsqlConnection dbConnection);

        List<Monitoramento> findAllMonitoramentoByClienteFuncaoFuncionario(ClienteFuncaoFuncionario clienteFuncaoFuncionario, NpgsqlConnection dbConnection);

    }
}
