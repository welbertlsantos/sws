﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SWS.Entidade;
using System.Data;
using Npgsql;
using NpgsqlTypes;

namespace SWS.IDao
{
    interface IAsoDao
    {
        Boolean findAsoByClienteFuncaoFuncionario(ClienteFuncaoFuncionario clienteFuncaoFuncionario, NpgsqlConnection dbConnection);

        DataSet findAsoByFilter(Aso aso, ClienteFuncao clienteFuncao, DateTime? dataAsoFinal, DateTime? dataCriacaoFinal, Usuario usuarioLogado, NpgsqlConnection dbConnection);

        HashSet<GheFonteAgenteExame> findAllExamesByPcmsoByASOByIdadeByTipoByAlturaByConfinado(Estudo estudo, Int32 idade, Periodicidade periodicidade,
            List<Ghe> colecaoGhe, Boolean? altura, Boolean? confinado, bool? eletricidade, NpgsqlConnection dbConnection);

        Aso insert(Aso aso, NpgsqlConnection dbConnection);

        Int64 recuperaProximoId(NpgsqlConnection dbConnection);

        Aso findById(Int64 id, NpgsqlConnection dbConnection);

        Aso findAsoByCodigo(String codigo, NpgsqlConnection dbConnection);

        void updateStatusAso(Aso aso, String situacao, NpgsqlConnection dbConnection);

        LinkedList<Aso> findAsoByPeriodo(DateTime dataInicio, DateTime dataFim, NpgsqlConnection dbConnection);

        LinkedList<Aso> findAsoNaoFinalizadoByPeriodo(DateTime dataInicio, DateTime dataFim, Empresa empresa, NpgsqlConnection dbConnection);

        Aso findAsoByClienteFuncaoeExameAsoId(Int64? id, NpgsqlConnection dbConnection);

        Aso findAsoByGheFonteAgenteExameAsoId(Int64? id, NpgsqlConnection dbConnection);

        List<ClienteFuncaoExame> findAllExamesByClienteFuncaoAndIdadeAndPeriodicidade(ClienteFuncao clienteFuncao, Int32 idade, Periodicidade periodicidade, NpgsqlConnection dbConnection);

        void update(Aso aso, NpgsqlConnection dbConnection);

        void updateInformacaoLaudo(Aso atendimento, NpgsqlConnection dbConnection);

        String findSenhaByAso(Aso aso, NpgsqlConnection dbConnection);

        Boolean verificaExisteAtendimentoNoMesmoDia(Cliente cliente, Funcionario funcionario, Periodicidade periodicidade, NpgsqlConnection dbConnection);

        List<Exame> FindAllExamesByAsoBySituacao(Aso aso, Boolean? atendido,
            Boolean? finalizado, Boolean? cancelado, Boolean? devolvido, Boolean? transcrito, NpgsqlConnection dbConnection);

        Boolean verificaAsoTranscrito(Aso aso, NpgsqlConnection dbConnection);

        Boolean verificaAsoInclusaoExame(Aso aso, NpgsqlConnection dbConnection);

        void insertAsoInProtocolo(Protocolo protocolo, NpgsqlConnection dbConnection);

        Boolean findAsoInProtocolo(Aso atendimento, NpgsqlConnection dbConnection);

        Boolean verificaAtendimentoUnidade(Cliente unidade, NpgsqlConnection dbConnection);

        List<Aso> findAllByFuncionario(Funcionario funcionario, Int32 limitePesquisa, Empresa empresa, NpgsqlConnection dbConnection);

        DataSet findAsoByFilter(Cliente cliente, DateTime dataPeriodoInicial, DateTime dataPeriodoFinal, Boolean check, NpgsqlConnection dbConnection);

        DataSet findAtendimentosByLoteEsocial(LoteEsocial loteEsocial, NpgsqlConnection dbConnection);

        List<Aso> findAtendimentosByLoteEsocialList(LoteEsocial loteEsocial, NpgsqlConnection dbConnection);

        List<AtendimentoExame> FindAllExamesByAsoByBySalaInService(Aso aso, Sala sala, NpgsqlConnection dbConnection);

        void updateGheSetorAvulso(Aso aso, List<GheSetorAvulso> gheSetorAvulso, NpgsqlConnection dbConnection);

        void updateTipoAtendimento(Aso atendimento, NpgsqlConnection dbConnection);

        void updateSituacao(Aso aso, NpgsqlConnection dbConnection);

        LinkedList<Aso> findAsoNotCanceladoByPeriodo(DateTime dataInicio, DateTime dataFim, Empresa empresa, NpgsqlConnection dbConnection);

        List<Aso> findAllAtendimentoInPeriodoByFuncionarioInCliente(ClienteFuncaoFuncionario clienteFuncaoFuncionario, DateTime dataInicial, DateTime dataFinal, Empresa empresa, NpgsqlConnection dbConnection);

        List<Aso> findAllAtendimentoInSituacaoByClienteAndEmpresaAndPeriodo(Cliente cliente, string situacao, DateTime dataInicial, DateTime dataFinal, Empresa empresa, NpgsqlConnection dbConnection);
    }
}
