﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SWS.Entidade;
using Npgsql;

namespace SWS.IDao
{
    interface IAsoAgenteRiscoDao
    {
        AsoAgenteRisco insert(AsoAgenteRisco asoAgenteRisco, NpgsqlConnection dbConnection);
        List<AsoAgenteRisco> findAllByAtendimento(Aso atendimento, NpgsqlConnection dbConnection);
        void delete(AsoAgenteRisco asoAgenteRisco, NpgsqlConnection dbConnection);
    }
}
