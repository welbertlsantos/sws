﻿using Npgsql;
using SWS.Entidade;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SWS.IDao
{
    interface IEpiMonitoramentoDao
    {
        EpiMonitoramento insert(EpiMonitoramento epiMonitoramento, NpgsqlConnection dbConnection);

        EpiMonitoramento update(EpiMonitoramento epiMonitoramento, NpgsqlConnection dbConnection);

        void delete(EpiMonitoramento epiMonitoramento, NpgsqlConnection dbConnection);

        EpiMonitoramento findById(long id, NpgsqlConnection dbConnection);

        List<EpiMonitoramento> findAllByMonitoramentoGheFonteAgente(MonitoramentoGheFonteAgente monitoramentoAgente, NpgsqlConnection dbConnection);
    }
}
