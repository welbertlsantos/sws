﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SWS.Entidade;
using Npgsql;
using System.Data;

namespace SWS.IDao
{
    interface IMaterialHospitalarDao
    {
        // Método responsável por incluir um material hospitalar na base de dados.
        MaterialHospitalar insert(MaterialHospitalar materialHospitalar, NpgsqlConnection dbConnection);
        
        // Método responsável por atualizar um material hospitalar
        MaterialHospitalar update(MaterialHospitalar materialHospitalar, NpgsqlConnection dbConnection);
        
        // Método responsável por buscar o próximo identificador, baseado em uma sequence.
        Int64 recuperaProximoId(NpgsqlConnection dbConnection);
        
        // Método responsável por buscar um material hospitalar dada sua descrição.
        MaterialHospitalar findByDescricao(String descricao, NpgsqlConnection dbConnection);
        
        // Método responsável por buscar um material hospitalar dado seu identificador.
        MaterialHospitalar findById(Int64 idMaterialHospitalar, NpgsqlConnection dbConnection);
        
        // Método responsável por buscar um material hospitalar dado os campos preenchidos.
        DataSet findByFilter(MaterialHospitalar materialHospitalar, NpgsqlConnection dbConnection);
        
        // Método responsável por verificar se o material hospitalar está senndo usado por um estudo
        Boolean findInEstudo(MaterialHospitalar materialHospitalar, NpgsqlConnection dbConnection);
        
        List<MaterialHospitalar> findByPcmsoAndMateriaHospitalar(MaterialHospitalar materialHospitalar, Estudo pcmso, NpgsqlConnection dbConnection);

        
    }
}
