﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using Npgsql;
using SWS.Entidade;

namespace SWS.IDao
{
    interface IGradEfeitoDao
    {
        // metodo que recupera todos os efeitos da tabela seg_grad_efeito
        DataSet findAllGradEfeito(NpgsqlConnection dbConnection);

        // metodo responsavel por recuperar um objeto gradEfeito.
        GradEfeito findGradEfeitoByGradEfeito(GradEfeito gradEfeito, NpgsqlConnection dbConnection);

    }
}
