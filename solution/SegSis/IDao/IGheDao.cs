﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SWS.Entidade;
using Npgsql;
using System.Data;

namespace SWS.IDao
{
    interface IGheDao
    {
        // metodo responsavel por recuperar o proximo id da tabela seg_ghe
        Int64 recuperaProximoId(NpgsqlConnection dbConnection);

        // Método responsável por incluir um ghe na tabela seg_ghe
        Ghe insert(Ghe ghe, NpgsqlConnection dbConnection);

        // Método responsável por recuperar todos os Ghes de um Estudo.
        DataSet findAtivoByEstudo(Estudo ppra, NpgsqlConnection dbConnection);

        // Método responsável por recuperar todos os Ghes de um Estudo.
        HashSet<Ghe> findAllByEstudo(Int64 idPpra, NpgsqlConnection dbConnection);

        // Método responsável por recuperar um Ghe dado um Id.
        Ghe findById(Int64? idGhe, NpgsqlConnection dbConnection);

        // Método responsável por alterar a descricao de um Ghe de um Estudo.
        Ghe update(Ghe ghe, NpgsqlConnection dbConnection);

        // Método responsável por excluir um ghe da tabela seg_ghe dado um idGhe e um IdPpra.
        void deleteByEstudo(Int64 idGhe, Estudo ppra, NpgsqlConnection dbConnection);

        // Método responsável por retornar um Ghe dado um descricao.
        Ghe findByDescricao(Ghe ghe, NpgsqlConnection dbConnection);

        // Método responsável por retornar a soma do numero de expostos do ghe.
        Int64 FindNtotalExpostoByEstudo(Estudo ppra, NpgsqlConnection dbConnection);
        
        // Método responsável por buscar a quantidade de GHE's de um PPRA.
        Int32 buscaQuantidadeByEstudo(Int64 idPpra, NpgsqlConnection dbConnection);

        // Método responsável por buscar a quantidade deriscos exibidas no quadro do GHE.
        Int32 buscaQuantidadeRiscoInGhe(Ghe ghe, NpgsqlConnection dbConnection);

        // Método responsável por buscar o número de notas de um GHE.
        Int32 buscaQuantidadeNotas(Ghe ghe, NpgsqlConnection dbConnection);

        void delete(Int64 idGhe, NpgsqlConnection dbConnection);

        List<Ghe> findAtivoInEstudoByClienteFuncao(ClienteFuncao clienteFuncao, Estudo pcmso, NpgsqlConnection dbConnection);

        Boolean isGheUsed(Ghe ghe, NpgsqlConnection dbConnection);
    }
}
