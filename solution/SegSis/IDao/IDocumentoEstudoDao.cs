﻿using Npgsql;
using SWS.Entidade;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SWS.IDao
{
    interface IDocumentoEstudoDao
    {
        DocumentoEstudo insert(DocumentoEstudo documentoEstudo, NpgsqlConnection dbConnection);

        DocumentoEstudo update(DocumentoEstudo documentoEstudo, NpgsqlConnection dbConnection);

        void delete(DocumentoEstudo documentoEstudo, NpgsqlConnection dbConnection);

        DocumentoEstudo findById(long id, NpgsqlConnection dbConnection);

        DataSet findAllByEstudo(Estudo estudo, NpgsqlConnection dbConnection);

        List<DocumentoEstudo> findAllByEstudoList(Estudo pcmso, NpgsqlConnection dbConnection);

    }
}
