﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SWS.Entidade;
using System.Data;
using Npgsql;
using NpgsqlTypes;

namespace SWS.IDao
{
    interface IFuncionarioDao
    {

        DataSet findFuncionarioByFilter(Funcionario funcionario, ClienteFuncao clienteFuncao, Cliente cliente, NpgsqlConnection dbConnection);

        Funcionario findFuncionarioByCPF(String Cpf, NpgsqlConnection dbConnection);

        Funcionario findFuncionarioById(Int64 id, NpgsqlConnection dbConnection);

        Funcionario insert(Funcionario funcionario, NpgsqlConnection dbConnection);

        Funcionario update(Funcionario funcionario, NpgsqlConnection dbConnection);

        void deleteFuncionario(Funcionario funcionario, NpgsqlConnection dbConnection);

        DataSet findFuncionarioByFilterByFuncionario(Funcionario funcionario, NpgsqlConnection dbConnection);

        Funcionario findFuncionarioByRG(String rg, NpgsqlConnection dbConnection);

        Funcionario findFuncionarioByNomeRg(String nome, String rg, NpgsqlConnection dbConnection);

        Funcionario verificaPodeAlterarFuncionario(Funcionario funcionario, NpgsqlConnection dbConnection);
    }
}
