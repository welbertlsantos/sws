﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SWS.Entidade;
using Npgsql;
using System.Data;

namespace SWS.IDao
{
    interface IGheNotaDao
    {

        // metodo responsavel por incluir um objeto GheNorma na tabela seg_ghe_norma.
        void insert(GheNota gheNorma, NpgsqlConnection dbConnection);

        // metodo responsvel por deletar uma norma presente na tabela seg_norma_ghe dado um objeto norma e um objeto ghe.
        void delete(GheNota gheNorma, NpgsqlConnection dbConnection);

        HashSet<Nota> findAllByGhe(Ghe ghe, NpgsqlConnection dbconnection);

        DataSet findAllByGheDataSet(Ghe ghe, NpgsqlConnection dbconnection);
    }
}
