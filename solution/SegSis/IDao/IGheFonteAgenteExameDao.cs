﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using SWS.Facade;
using NpgsqlTypes;
using Npgsql;
using SWS.Entidade;

namespace SWS.IDao
{
    interface IGheFonteAgenteExameDao
    {
        DataSet findAtivosByGheFonteAgente(GheFonteAgente gheFonteAgente, NpgsqlConnection dbConnection);

        GheFonteAgenteExame insert(GheFonteAgenteExame gheFonteAgenteExame, NpgsqlConnection dbconnection);

        HashSet<GheFonteAgenteExame> findAllByGheFonteAgente(GheFonteAgente gheFonteAgente, NpgsqlConnection dbConnection);

        Exame findExameByGheFonteAgenteExame(GheFonteAgenteExame gheFonteAgenteExame, NpgsqlConnection dbconnection);

        void update(GheFonteAgenteExame gheFonteAgenteExame, NpgsqlConnection dbconnection);

        void delete(GheFonteAgenteExame gheFonteAgenteExame, NpgsqlConnection dbconnection);

        bool isUsedInService(GheFonteAgenteExame gheFonteAgenteExame, NpgsqlConnection dbConnection);

        Boolean isGheFonteAgenteExameUsedByExame(Exame exame, NpgsqlConnection dbConnection);

        GheFonteAgenteExame findById(Int64 id, NpgsqlConnection dbConnection);



    }
}
