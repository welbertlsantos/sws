﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SWS.Entidade;
using Npgsql;

namespace SWS.IDao
{
    interface IClienteArquivoDao
    {
        ClienteArquivo insertClienteArquivo(ClienteArquivo clienteArquivo, NpgsqlConnection dbConnection);
        ClienteArquivo findClienteArquivoById(Int64 id, NpgsqlConnection dbConnection);
        ClienteArquivo findClienteArquivoByCliente(Cliente cliente, NpgsqlConnection dbConnection);
        void finalizaClienteArquivo(ClienteArquivo clienteArquivo, NpgsqlConnection dbConnection);
    }
}
