﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SWS.Entidade;
using Npgsql;
using System.Data;

namespace SWS.IDao
{
    interface IComentarioFuncaoDao
    {
        ComentarioFuncao insert(ComentarioFuncao comentarioFuncao, NpgsqlConnection dbConnection);

        List<ComentarioFuncao> findAtivosByFuncao(Funcao funcao, NpgsqlConnection dbConnection);

        void update(ComentarioFuncao comentarioFuncao, NpgsqlConnection dbConnection);

        ComentarioFuncao findById(Int64 id, NpgsqlConnection dbConnection);
    }
}
