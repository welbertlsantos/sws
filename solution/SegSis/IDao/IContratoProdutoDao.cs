﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SWS.Entidade;
using Npgsql;
using NpgsqlTypes;
using System.Data;

namespace SWS.IDao
{
    interface IContratoProdutoDao
    {
        HashSet<ContratoProduto> findAtivosByContratoAndProduto(Contrato contrato, Produto produto, NpgsqlConnection dbConnection);

        ContratoProduto insert(ContratoProduto contratoProduto, NpgsqlConnection dbConnection);

        void delete(Int64 id, NpgsqlConnection dbConnection);

        ContratoProduto update(ContratoProduto contratoProduto, NpgsqlConnection dbConnection);

        ContratoProduto findByContratoAndProduto(Contrato contrato, Produto produto, NpgsqlConnection dbConnection);

        ContratoProduto findById(Int64 id, NpgsqlConnection dbConnection);

        DataSet findAtivosByContrato(Contrato contrato, NpgsqlConnection dbConnection);

        void reativated(ContratoProduto contratoProduto, NpgsqlConnection dbConnection);

        void desativated(ContratoProduto contratoProduto, NpgsqlConnection dbConnection);

        Boolean contratoProdutoIsUsed(ContratoProduto contratoProduto, NpgsqlConnection dbConnection);
    }
}
