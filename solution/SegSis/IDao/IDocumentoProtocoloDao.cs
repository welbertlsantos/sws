﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SWS.Entidade;
using Npgsql;

namespace SWS.IDao
{
    interface IDocumentoProtocoloDao
    {
        DocumentoProtocolo insertDocumentoProtocolo(DocumentoProtocolo documentoProtocolo, NpgsqlConnection dbConnection);

        void deleteDocumentoProtocolo(DocumentoProtocolo documentoProtocolo, NpgsqlConnection dbConnection);

    }
}
