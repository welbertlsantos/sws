﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using Npgsql;
using SWS.Entidade;

namespace SWS.IDao
{
    interface IGradExposicaoDao
    {
        // metodo responsavel por recuperar todas as exposicoes do sistema
        DataSet findAllGradExposicao(NpgsqlConnection dbConnection);

        // metodo responsavel por recuperar um objeto gradExposicao.
        GradExposicao findGradExposicaoByGradExposicao(GradExposicao gradExposicao, NpgsqlConnection dbConnection);
    
    
    }
}
