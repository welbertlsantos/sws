﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SWS.Entidade;
using Npgsql;
using System.Data;

namespace SWS.IDao
{
    interface ISalaExameDao
    {
        SalaExame insert(SalaExame salaExame, NpgsqlConnection dbConnection);
        DataSet findExameNotInSala(Exame exame, Sala sala, NpgsqlConnection dbConnection);
        void delete(SalaExame salaExame, NpgsqlConnection dbConnection);
        SalaExame update(SalaExame salaExame, NpgsqlConnection dbConnection);
        Boolean verificaPodeExcluirSalaExame(SalaExame salaExame, NpgsqlConnection dbConnection);
        HashSet<SalaExame> buscaSalasAtendemExame(Exame exame, Boolean salaVip, NpgsqlConnection dbConnection);
        HashSet<Exame> buscaExamesBySala(Sala sala, NpgsqlConnection dbConnection);
        SalaExame buscaSalaExameBySala(Sala sala, NpgsqlConnection dbConnection);
        Boolean verificaSalaAtendeExame(Sala sala, Exame exame, NpgsqlConnection dbConnection);
        SalaExame findById(Int64 id, NpgsqlConnection dbConnection);
        List<SalaExame> findAtivosBySala(Sala sala, NpgsqlConnection dbConnection);
        List<Exame> listAllNotInSala(NpgsqlConnection dbConnection);
        List<SalaExame> findAllSalaExameAtivoByExame(Exame exame, Empresa empresa, NpgsqlConnection dbConnection);
    }
}
