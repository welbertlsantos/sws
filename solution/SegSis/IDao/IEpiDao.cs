﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SWS.Entidade;
using Npgsql;
using System.Data;

namespace SWS.IDao
{
    interface IEpiDao
    {

        /* metodo responsavel por recuperar o epi no banco de dados de acordo com o filtro selecionado. */
        DataSet findByFilter(Epi epi, NpgsqlConnection dbConnection);

        /* metodo responsavel por procurar uma descrição no banco de dados. */
        Epi findByDescricao(String descricao, NpgsqlConnection dbConnection);

        /* metodo responsavel por incluir um EPI no banco de dados. */
        Epi insert(Epi epi, NpgsqlConnection dbConnection);

        /* metodo responsavel por alterar um EPI no banco de dados. */
        Epi update(Epi epi, NpgsqlConnection dbConnection);

        /* metodo responsavel por pesquisar um EPI de acordo com o ID desejado. */
        Epi findById(Int64 idEpi, NpgsqlConnection dbConnection);

        // metodo responsavel por retornar todos os epis que ainda nao foram utilizados pelo relacionamento ghe_fonte_agente_epi
        // dado um ghe_fonte_Agente
        DataSet findAtivosByEpiAndGhefonteAgente(Epi epi, GheFonteAgente gheFonteAgente, NpgsqlConnection dbConnection);
    }
}
