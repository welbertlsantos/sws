﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using Npgsql;
using SWS.Entidade;
using SWS.View.Entidade;

namespace SWS.IDao
{
    interface IFuncaoInternaDao
    {
        FuncaoInterna findById(Int64 idFuncaoInterna, NpgsqlConnection dbConnection);

        DataSet findByFilter(FuncaoInterna funcaoInterna, NpgsqlConnection dbConnection);

        FuncaoInterna insert(FuncaoInterna funcaoInterna, NpgsqlConnection dbConnection);

        FuncaoInterna findByDescricao(string descricao, NpgsqlConnection dbConnection);

        FuncaoInterna update(FuncaoInterna funcaoInternaAlterar, NpgsqlConnection dbConnection);

        FuncaoInterna findFuncaoInternaAtivoByUsuario(Usuario usuario, NpgsqlConnection dbConnection);

    }
}
