﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SWS.Entidade;
using NpgsqlTypes;
using Npgsql;
using System.Data;

namespace SWS.IDao
{
    interface IAnexoDao
    {
        void insertAnexo(Anexo anexo, NpgsqlConnection dbConnection);

        DataSet findAllAnexoByEstudo(Estudo estudo, NpgsqlConnection dbConnection);

        Anexo findAnexoByConteudo(Anexo anexo, NpgsqlConnection dbConnection);

        void deleteAnexo(Anexo anexo, NpgsqlConnection dbConnection);

        void updateAnexo(Anexo anexo, NpgsqlConnection dbConnection);
    }
}
