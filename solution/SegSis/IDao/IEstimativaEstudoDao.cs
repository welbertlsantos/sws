﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SWS.Entidade;
using Npgsql;
using System.Data;

namespace SWS.IDao
{
    interface IEstimativaEstudoDao
    {
        EstimativaEstudo insert(EstimativaEstudo estimativaEstudo, NpgsqlConnection dbConnection);

        void delete(EstimativaEstudo estimativaEstudo, NpgsqlConnection dbConnection);

        EstimativaEstudo update(EstimativaEstudo estimativaEstudo, NpgsqlConnection dbConnection);

        DataSet findAllByEstudo(Estudo estudo, NpgsqlConnection dbConnection);
        
    }
}
