﻿using Npgsql;
using SWS.Entidade;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SWS.IDao
{
    interface IProcedimentoEsocialDao
    {
        List<ProcedimentoEsocial> findAllByFilter(string textString, NpgsqlConnection dbConnection);

    }
}
