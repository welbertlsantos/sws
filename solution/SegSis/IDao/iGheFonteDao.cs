﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SWS.Entidade;
using Npgsql;
using System.Data;

namespace SWS.IDao
{
    interface IGheFonteDao
    {
        // metodo responsavel por recuperar todo o relacionamento ghe fonte dado um ghe
        DataSet findAllByGhe(Ghe ghe, NpgsqlConnection dbConnection);

        // Método responsável por recuperar todas as fontes de um ghe dado um ghe.
        HashSet<GheFonte> findAllAtivoByGhe(Ghe ghe, NpgsqlConnection dbConnection);

        // Método responsável por incluir na tabela seg_ghe_fonte;
        GheFonte insert(GheFonte gheFonte, NpgsqlConnection dbConnection);

        // Método responsável por excluir um ghe fonte dado um GheFonte
        void deleteGheFonte(GheFonte gheFonte, NpgsqlConnection dbConnection);

        // Método responsável por recurperar um objeto GheFonte
        GheFonte findByGheFonte(GheFonte gheFonte, NpgsqlConnection dbConnection);

        GheFonte findGheFonteInEpi(GheFonte gheFonte, NpgsqlConnection dbConnection);

        DataSet findAllByGheInEpi(Ghe ghe, NpgsqlConnection dbConnection);

        Boolean verificaPodeAlterarFonte(Fonte fonte, NpgsqlConnection dbConnection);

        GheFonte update(GheFonte gheFonte, NpgsqlConnection dbConnection);

        List<GheFonte> findAllByGheList(Ghe ghe, NpgsqlConnection dbConnection);

        bool validaExclusaoGheFonte(GheFonte ghefonte, NpgsqlConnection dbConnection);
    
    }
}
