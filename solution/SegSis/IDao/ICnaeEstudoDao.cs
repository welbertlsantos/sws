﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SWS.Entidade;
using Npgsql;
using NpgsqlTypes;

namespace SWS.IDao
{
    interface ICnaeEstudoDao
    {
        CnaeEstudo insert(CnaeEstudo cnaeEstudo, NpgsqlConnection dbConnection);

        void delete(CnaeEstudo cnaeEstudo, NpgsqlConnection dbConnection);

        void replicaCnaeEstudo(Int64 idPpra, Int64 idPpraNovo, NpgsqlConnection dbConnection);
        
        List<CnaeEstudo> findAllByPcmso(Estudo pcmso, NpgsqlConnection dbConnection);

        Int64 recuperaProximoId(NpgsqlConnection dbConnection);
    }
}
