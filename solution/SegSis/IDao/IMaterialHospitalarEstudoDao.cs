﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Npgsql;
using NpgsqlTypes;
using SWS.Entidade;
using System.Data;

namespace SWS.IDao
{
    interface IMaterialHospitalarEstudoDao
    {
        void insertMaterialHospitalarEstudo(MaterialHospitalarEstudo materialHospitalarEstudo, NpgsqlConnection dbConnection);

        DataSet findAllMaterialHospitalarEstudo(Estudo pcmso, NpgsqlConnection dbConnection);

        void updateMaterialHospitalarEstudo(MaterialHospitalarEstudo materialHospitalarEstudo, NpgsqlConnection dbConnection);

        void deleteMaterialHospitalarEstudo(MaterialHospitalarEstudo materialHospitalarEstudo, NpgsqlConnection dbConnection);

        HashSet<MaterialHospitalarEstudo> findAllByEstudo(Estudo pcmso, NpgsqlConnection dbConnection);

        Boolean findMaterialHospitalarEstudoByMaterialHospitalar(MaterialHospitalar materialHospitalar, NpgsqlConnection dbConnection);
    }
}
