﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SWS.Entidade;
using Npgsql;
using System.Data;


namespace SWS.IDao
{
    interface IVendedorDao
    {
        /* metodo responsavel por recuperar vendedor segundo filtros informados */
        DataSet findVendedorByFilter(Vendedor vendedor, NpgsqlConnection dbConnection);

        /* metodo responsavel por incluir um vendedor no banco de dados. */
        Vendedor insert(Vendedor vendedor, NpgsqlConnection dbConnection);

        /* metodo responsavel por encontrar um vendedor no banco por um cpf informado. */
        Vendedor findVendedorByCpf(string cpf, NpgsqlConnection dbConnection);

        /* metodo responsavel por encontrar um vendedor no banco por um id. */
        Vendedor findVendedorById(Int64 idVendedor, NpgsqlConnection dbConnection);

        /* metodo responsavel por alterar um vendedor no banco de dados. */
        Vendedor update(Vendedor vendedorAltera, NpgsqlConnection dbConnection);

        /* metodo responsavel por alterar o status de um vendedor no banco de dados. */
        void changeStatusVendedor(Vendedor vendedor, NpgsqlConnection dbConnection);

        /* metodo responsavel por localizar um vendedor pelo nome*/
        DataSet findVendedorByName(Vendedor vendedor, NpgsqlConnection dbConnection);

        DataSet findVendedorNotInCliente(Vendedor vendedor, Cliente cliente, NpgsqlConnection dbConnection);
    }
}
