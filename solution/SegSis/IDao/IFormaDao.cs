﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using Npgsql;
using SWS.Entidade;

namespace SWS.IDao
{
    interface IFormaDao
    {
        DataSet findFormaByFilter(Forma forma,Plano plano, NpgsqlConnection dbConnection);

        DataSet findFormaByPlano(Plano plano, NpgsqlConnection dbConnection);

        List<Forma> findAllFormas(NpgsqlConnection dbConnection);
    }
}
