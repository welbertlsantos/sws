﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Npgsql;
using NpgsqlTypes;
using SWS.Entidade;
using System.Data;

namespace SWS.IDao
{
    interface IMovimentoDao
    {
        Int64 recuperaProximoId(NpgsqlConnection dbConnection);

        void insert(Movimento movimento, NpgsqlConnection dbConnection);

        DataSet findMovimentoByFilter(Movimento movimento, DateTime? dataFinalLancamento,
            DateTime? dataFinalFaturamento, Boolean check, Boolean particular,
            Produto produto, Exame exame, DateTime? dataInicialAtendimento, DateTime? dataFinalAtendimento, NpgsqlConnection dbConnection);

        Movimento findMovimentoByProdutoOrExame(Movimento movimento, NpgsqlConnection dbConnection);

        void changeStatus(Movimento movimento, String status, NpgsqlConnection dbConnection);

        DataSet findMovimentoDetalhado(Cliente cliente, DateTime? dataInicialLancamento, DateTime? dataFinalLancamento,
            Boolean analitico, Boolean faturado, NpgsqlConnection dbConnection);

        Decimal findSomaMovimentoByFilter(Movimento movimento, DateTime? dataFinalLancamento,
            DateTime? dataFinalFaturamento, NpgsqlConnection dbConnection);

        void updateMovimentoFaturado(Movimento movimento, NpgsqlConnection dbConnection);

        List<Movimento> findMovimentosByNotaFiscal(Nfe notaFiscal, NpgsqlConnection dbConnection);

        DataSet findMovimentoByNotaInRelatorioNota(Nfe nfe, NpgsqlConnection dbConnection);

        void estornaFaturamentoMovimento(Movimento movimento, NpgsqlConnection dbConnection);

        DataSet findMovimentoInRecibo(Cliente cliente, DateTime dataInicial, DateTime dataFinal, NpgsqlConnection dbConnection);

        DataSet findColaboradorByMovimentoInRecibo(Cliente cliente, DateTime dataInicial, DateTime dataFinal, NpgsqlConnection dbConnection);

        DataSet somaMovimentoInternoByClienteInRelatorio(Cliente cliente, DateTime dataInicial, DateTime dataFinal, NpgsqlConnection dbConnection);

        void updateValorMovimento(Movimento movimento, Decimal valorAlterado, NpgsqlConnection dbConnection);

        Boolean findMovimentoByGheFonteAgenteExameAso(GheFonteAgenteExameAso gheFonteAgenteExameAso, NpgsqlConnection dbConnection);

        Boolean findMovimentoByClienteFuncaoExameAso(ClienteFuncaoExameASo clienteFuncaoExameAso, NpgsqlConnection dbConnection);

        LinkedList<Movimento> findByProdutoInDataMovimento(Produto produto, DateTime dataLancamentoInicial, Cliente cliente, Boolean insertedProtocolo, DateTime dataLancamentoFinal, NpgsqlConnection dbConnection);

        Movimento findById(Int64 id, NpgsqlConnection dbConnection);
        
        void insertMovimentoInProtocolo(Movimento movimento, Protocolo protocolo, NpgsqlConnection dbConnection);

        List<Movimento> findAllLancamentoByAtendimento(Aso atendimento, NpgsqlConnection dbConnection);

        FileFinanceiroXlsx createFileXlsx(Cliente cliente, DateTime dataInicialLancamento, DateTime dataFinalLancamento, DateTime? dataInicialAtendimento, DateTime? dataFinalAtendimento, string situacao, string tipoAnalise, long idCentroCusto, Cliente credenciada, NpgsqlConnection dbConnection);
    }
}
