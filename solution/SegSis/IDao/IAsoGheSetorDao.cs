﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SWS.Entidade;
using Npgsql;

namespace SWS.IDao
{
    interface IAsoGheSetorDao
    {
        AsoGheSetor insertAsoGheSetor(AsoGheSetor asoGheSetor, NpgsqlConnection dbConnection);

        List<String> findDescricaoSetoresByAso(Aso aso, NpgsqlConnection dbConnection);

        List<AsoGheSetor> findByAso(Aso aso, NpgsqlConnection dbConnection);
    }
}