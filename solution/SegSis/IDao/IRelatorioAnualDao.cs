﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SWS.Entidade;
using Npgsql;

namespace SWS.IDao
{
    interface IRelatorioAnualDao
    {
        RelatorioAnual insertRelatorioAnual(RelatorioAnual relatorioAnual, NpgsqlConnection dbConnection);

        LinkedList<Setor> verificaSetoresInRelatorio(Cliente cliente, DateTime dataInicial, DateTime dataFinal,
            Cliente unidade, Boolean matriz, CentroCusto centroCusto, NpgsqlConnection dbConnection);

        LinkedList<Exame> findExameInSetor(Setor setor, RelatorioAnual relatorioAnual, 
            Cliente unidade, Boolean matriz, CentroCusto centroCusto, NpgsqlConnection dbConnection);

        Int32 findTotalInSituacao(Setor setor, Exame exame, RelatorioAnual relatorioAnual,
            String situacao, Periodicidade tipo, Cliente unidade, Boolean matriz, CentroCusto centroCusto, NpgsqlConnection dbConnection);

        Dictionary<String, Int32> findTipoAtendimentoInSetorInExame(Setor setor, Exame exame,
            RelatorioAnual relatorioAnual, Cliente unidade, Boolean matriz, CentroCusto centroCusto, NpgsqlConnection dbConnection);

        LinkedList<Periodicidade> findTipoInExame(Setor setor, Exame exame,
            RelatorioAnual relatorioAnual, Cliente unidade, Boolean matriz, CentroCusto centroCusto, NpgsqlConnection dbConnection);
    }
}
