﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SWS.Entidade;
using Npgsql;
using System.Data;

namespace SWS.IDao
{
    interface IBancoDao
    {
        Banco insert(Banco banco, NpgsqlConnection dbConnection);
        
        Banco update(Banco banco, NpgsqlConnection dbConnection);

        Banco findById(Int64 id, NpgsqlConnection dbConnection);

        DataSet findByFilter(Banco banco, Boolean? caixa, Boolean? boleto, NpgsqlConnection dbConnection);

        Banco findByNome(String nome, NpgsqlConnection dbConnection);

        Banco findByCodigo(String codigo, NpgsqlConnection dbConnection);


    }
}
