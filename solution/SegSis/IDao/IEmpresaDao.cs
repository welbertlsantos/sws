﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using SWS.Entidade;
using Npgsql;

namespace SWS.IDao
{
    interface IEmpresaDao
    {
        DataSet findByFilter(Empresa empresa, NpgsqlConnection dbConnection);
        Empresa findByCnpj(String cnpj, NpgsqlConnection dbConnection);
        Int64 recuperaProximoId(NpgsqlConnection dbConnection);
        Empresa insert(Empresa empresa, NpgsqlConnection dbConnection);
        Empresa findById(Int64 id, NpgsqlConnection dbConnection);
        Empresa update(Empresa empresa, NpgsqlConnection dbConnection);
        List<Empresa> findAll(NpgsqlConnection dbConnection);

    }
}
