﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SWS.Entidade;
using Npgsql;

namespace SWS.IDao
{
    interface IProntuarioDao
    {
        Prontuario updateProntuario(Prontuario prontuario, NpgsqlConnection dbConnection);

        Prontuario insertProntuario(Prontuario prontuario, NpgsqlConnection dbConnection);
        
        Prontuario findProntuarioById(Int64 id, NpgsqlConnection dbConnection);
        
        Prontuario findProntuarioByAso(Aso aso, NpgsqlConnection dbConnection);

        Prontuario findLastProntuarioByFuncionario(Funcionario funcionario, NpgsqlConnection dbConnection);
    }
}
