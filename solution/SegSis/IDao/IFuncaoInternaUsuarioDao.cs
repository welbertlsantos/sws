﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SWS.Entidade;
using Npgsql;

namespace SWS.IDao
{
    interface IFuncaoInternaUsuarioDao
    {
        // Metodo responsavel por incluir um funcao interna para um usuario.
        void insert(Usuario usuario, NpgsqlConnection dbConnection);

        // metodo responsavel por desativar uma funcao interna do usuario.
        void changeStatus(Usuario usuario, NpgsqlConnection dbConnection);

    }
}
