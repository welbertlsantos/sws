﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using SWS.Entidade;
using Npgsql;
using NpgsqlTypes;

namespace SWS.IDao
{
    interface IProdutoDao
    {
        DataSet findProdutoAtivoNotInContrato(Contrato contrato, Produto produto, NpgsqlConnection dbConnection);

        DataSet findProdutoAtivoInGeneral(Produto produto, NpgsqlConnection dbConnection);

        Int64 recuperaProximoId(NpgsqlConnection dbConnection);

        Produto insert(Produto produto, NpgsqlConnection dbConnection);

        Produto findProdutoByDescricao(String descricao, NpgsqlConnection dbConnection);

        DataSet findProdutoByFilter(Produto produto, NpgsqlConnection dbConnection);

        Produto updateProduto(Produto produto, NpgsqlConnection dbConnection);

        Produto findProdutoById(Int64 id, NpgsqlConnection dbConnection);

        Boolean verificaPodeExcluirProduto(Produto produto, NpgsqlConnection dbConnection);

        DataSet findProdutoByTipoEstudo(String tipo_estudo, NpgsqlConnection dbConnection);

        DataSet findProdutoAtivoNotInPropostaContrato(List<Produto> produtos, Produto produto, NpgsqlConnection dbConnection);

        List<Produto> findAllAtivosPadraoContrato(NpgsqlConnection dbConnection);
    }
}
