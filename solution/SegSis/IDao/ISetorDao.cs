﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SWS.Entidade;
using Npgsql;
using System.Data;

namespace SWS.IDao
{
    interface ISetorDao
    {

        /* metodo responsavel por incluir um setor no banco de dados */
        Setor insert(Setor setor, NpgsqlConnection dbConnection);

        /* metodo responsavel por recuperar o próximo identificador do setor.  */
        Int64 recuperaProximoId(NpgsqlConnection dbConnection);

        /* metodo responsavel por pesquisar um setor no banco de dados */
        Setor findSetorByDescricao(String setor, NpgsqlConnection dbConnection);

        /* metodo para recuperar o setor de acordo com o filtro selecionado */
        DataSet findSetorByFilter(Setor setor, NpgsqlConnection dbConnection);

        /* metodo responsavel por recuperar o setor no banco de dados. */
        Setor findSetorById(Int64 idsetor, NpgsqlConnection dbConnection);

        /* metodo responsavel por alterar um setor no banco de dados */
        Setor update(Setor SetorAlterar, NpgsqlConnection dbConnection);

        // metodo responsavel por retornar um conjunto de setores ativos.
        DataSet findAllSetorAtivo(Setor setor, Ghe ghe, ClienteFuncao ClienteFuncao, NpgsqlConnection dbConnection);
    }
}
