﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SWS.IDao;
using System.Data;
using SWS.Entidade;
using Npgsql;
using SWS.Excecao;
using NpgsqlTypes;
using SWS.Facade;

namespace SWS.IDao
{
    interface IGheFonteAgenteEpiDao
    {
        // metodo responsavel por incluir um objeto gheFonteAgenteEpi no banco de dados.
        GheFonteAgenteEpi insertGheFonteAgenteEpi(GheFonteAgenteEpi gheFonteAgenteEpi, NpgsqlConnection dbConnection);
                
        // metodo responsavel por retornar um objeto Completo.
        GheFonteAgenteEpi findGheFonteAgenteEpiByGheFonteAgenteEpi(GheFonteAgenteEpi gheFonteAgenteEpi, NpgsqlConnection dbConnection);

        // método responsavel por retornar uma colecao de ghefonteAgenteEpi dado um gheFonteAgenteSelecionado
        HashSet<GheFonteAgenteEpi> findAllGheFonteAgenteEpi(GheFonteAgente gheFonteAgente, NpgsqlConnection dbConnection);

        // método responsável por excluir um objeto ghe_fonte_agente_epi dado um id da tabela.
        void excluiGheFonteAgenteEpi(Int64 id, NpgsqlConnection dbConnection);

        // método que procura se existe um mesmo epi, mesmo ghe_agente com a mesma classificação.
        Boolean findGheFonteAgenteEpi(GheFonteAgenteEpi gheFonteAgenteEpi, NpgsqlConnection dbConnection);

        Boolean findGheFonteAgenteEpiByEpi(Epi epi, NpgsqlConnection dbConnection);

        DataSet findAllGheFonteAgenteEpiByGHeFonteAgente(GheFonteAgente gheFonteAgente, NpgsqlConnection dbConnection);
        

    }
}
