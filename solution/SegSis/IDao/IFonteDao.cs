﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SWS.Entidade;
using Npgsql;
using System.Data;

namespace SWS.IDao
{
    interface IFonteDao
    {

        /*
         * metodo responsavel por incluir uma fonte no banco de dados
        */
        Fonte insert(Fonte fonte, NpgsqlConnection dbConnection);

        /*
         * metodo responsavel por recuperar o próximo identificador da fonte.
        */
        Int64 recuperaProximoId(NpgsqlConnection dbConnection);

        /* metodo responsavel por pesquisar uma descricao de fonte no banco de dados
        */
        Fonte findByDescricao(String fonte, NpgsqlConnection dbConnection);

        /*
         * metodo para recuparar a fonte de acordo com o filtro selecionado
        
         */
        DataSet findByFilter(Fonte fonte, NpgsqlConnection dbConnection);

        /* metodo responsavel por recuperar a fonte no banco de dados.
        */

        Fonte findById(Int64 idFonte, NpgsqlConnection dbConnection);

        /* metodo responsavel por alterar uma fonte no banco de dados
        */
        Fonte update(Fonte FonteAlterar, NpgsqlConnection dbConnection);

        // Método responsável por recurar todas as fontes ativas do sistema de um GHE.
        DataSet findAllAtivaByGhe(Fonte fonte, Ghe ghe, NpgsqlConnection dbConnection);

    }
}
