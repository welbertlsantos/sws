﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SWS.Entidade;
using Npgsql;

namespace SWS.IDao
{
    interface IGradSomaDao
    {
        // metodo responsavel por recuperar um GradSoma, dado uma faixa.
        GradSoma findGradSomaByGradSoma(GradSoma gradSoma, NpgsqlConnection dbConnection);

        // metodo responsável por recuperar um gradSoma dado o id.
        GradSoma findGradSomaById(GradSoma gradSoma, NpgsqlConnection dbConnection);
                
    }
}
