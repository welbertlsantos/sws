﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SWS.Entidade;
using Npgsql;

namespace SWS.IDao
{
    interface IConfiguracaoDao
    {
        Dictionary<String, String> findAllConfiguracao(NpgsqlConnection dbConnection);

        Configuracao update(Configuracao configuracao, NpgsqlConnection dbConnection);

        List<Configuracao> findAll(NpgsqlConnection dbConnection);
    }
}
