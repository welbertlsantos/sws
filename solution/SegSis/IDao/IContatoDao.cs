﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SWS.Entidade;
using Npgsql;
using System.Data;

namespace SWS.IDao
{
    interface IContatoDao
    {
        Contato insert(Contato contato, NpgsqlConnection dbConnection);

        Contato update(Contato contato, NpgsqlConnection dbConnection);

        Contato findById(Int64 id, NpgsqlConnection dbConnection);

        void delete(Contato contato, NpgsqlConnection dbConnection);
        
        List<Contato> findByCliente(Cliente cliente, NpgsqlConnection dbConnection);

        DataSet findTipoContato(NpgsqlConnection dbConnection);
    }
}
