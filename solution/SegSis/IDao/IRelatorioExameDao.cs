﻿using Npgsql;
using SWS.Entidade;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SWS.IDao
{
    interface IRelatorioExameDao
    {
        RelatorioExame insert(RelatorioExame relatorioExame, NpgsqlConnection dbConnection);
        RelatorioExame findById(long id, NpgsqlConnection dbConnection);
        List<RelatorioExame> findAllByExame(Exame exame, NpgsqlConnection dbConnection);
        RelatorioExame update(RelatorioExame relatorioExame, NpgsqlConnection dbConnection);
        void delete(RelatorioExame relatorioExame, NpgsqlConnection dbConnection);
    }
}
