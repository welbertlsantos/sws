﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using Npgsql;
using SWS.Entidade;

namespace SWS.IDao
{
    interface IAcaoDao
    {
        /*
         * Método responsável por listar todas as ações.
         */
        DataSet listaTodos(NpgsqlConnection dbConnection);

        /*
         * Método responsável por verificar existe relação entre perfil, dominio e ação.
         */
        Boolean verificaAcaoSelecionadaParaPerfilDominio(Int64 idPerfil, Int64 idDominio, Int64 idAcao, NpgsqlConnection dbConnection);

        /*
         * Método responsável por remover a relação entre perfil, dominio e ação.
         */
        void removeAcaoDominioFromPerfil(Int64 idPerfil, Int64 idDominio, Int64 idAcao, NpgsqlConnection dbConnection);

        /*
         * Método responsável por criar a relação entre perfil, dominio e ação.
         */
        void criaAcaoDominioFromPerfil(Int64 idPerfil, Int64 idDominio, Int64 idAcao, NpgsqlConnection dbConnection);
        
        /*
         * Método responsável por listar todas as ações de um dado perfil e domínio
         */
        HashSet<Acao> findByPerfilAndDominio(Int64? idPerfil, Int64 idDominio, NpgsqlConnection dbConnection);

        DataSet listaTodosByDominio(Int64 idDominio, NpgsqlConnection dbConnection);
    }
}
