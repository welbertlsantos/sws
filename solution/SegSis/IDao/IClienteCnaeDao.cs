﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SWS.Entidade;
using Npgsql;
using System.Data;

namespace SWS.IDao
{
    interface IClienteCnaeDao
    {
        /* metodo responsavel por incluir na tabela seg_cli_cnae o relacionamento cliente cnae. */
        ClienteCnae incluirClienteCnae(ClienteCnae clienteCnae, NpgsqlConnection dbConnection);

        /* metodo responsavel por recuperar a colecao de cnaes de um cliente. */
        List<ClienteCnae> findClienteCnaeByCliente(Cliente cliente, NpgsqlConnection dbConnection);

        // método responsável por excluir um relacionamento cliente cnae dado um idCliente.
        void deleteClienteCnae(Int64 idCliente, NpgsqlConnection dbConnection);

        // método responsável por retornar para um dataset todos os cnaes do cliente.
        DataSet findAllCnaeByCliente(Cliente cliente, NpgsqlConnection dbConnection);

        // método responsável por retornar um Objeto Cnae dado um idCnae
        Cnae findCnaeById(Int64 idCnae, NpgsqlConnection dbConnection);
        
        // metodo responsavel por incluir a flag 'S' no relacionamento cnae x cliente marcando o CNAE principal.
        void updateClienteCnae(ClienteCnae clienteCnae, NpgsqlConnection dbConnection);

        // metodo responsavel por retornar um objeto cnae que sera o cnae principal do cliente.
        Cnae findCnaePrincipalCliente(Int64 idCliente,  NpgsqlConnection dbConnection);

        // metodo responsavel por retornar uma lista contendo todos os cnaes relacionados ao cliente da tabela seg_cli_cnae;
        HashSet<ClienteCnae> findAllCnaeClienteByClienteCnae(Cliente cliente, NpgsqlConnection dbConnection);

        // metodo responsavel por recuperar o cnae principal do cliente dado um cliente.
        Cnae findCnaePrincipalByCliente(Cliente cliente, NpgsqlConnection dbConnection);

        void deleteClienteCnaeInCliente(ClienteCnae clienteCnae, NpgsqlConnection dbConnection);



    }
}
