﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SWS.Entidade;
using Npgsql;

namespace SWS.IDao
{
    interface IPlanoFormaDao
    {
        PlanoForma insert(PlanoForma planoForma, NpgsqlConnection dbConnection);

        PlanoForma update(PlanoForma planoForma, NpgsqlConnection dbConnection);

        PlanoForma findById(Int64 id, NpgsqlConnection dbConnection);

        List<PlanoForma> findAtivosByPlano(Plano plano, NpgsqlConnection dbConnection);

        PlanoForma findByFormaByPlano(Forma forma, Plano plano, NpgsqlConnection dbConnection);
    }
}
