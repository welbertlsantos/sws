﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SWS.Entidade;
using Npgsql;
using NpgsqlTypes;
using System.Data;

namespace SWS.IDao
{
    interface IGheSetorClienteFuncaoDao
    {
        // metodo responsavel por retornar uma lista com o objeto GheSetorClienteFuncao dado um GheSetor
        HashSet<GheSetorClienteFuncao> findAllByGheSetorClienteFuncao(GheSetorClienteFuncao gheSetorClienteFuncao, NpgsqlConnection dbConnection);

        // metodo responsavel por retornar uma lista de funcoes ativas por cliente.
        DataSet findAllByGheSetorClienteFuncaoAndCliente(GheSetorClienteFuncao gheSetorClienteFuncao, Cliente cliente, NpgsqlConnection dbConnection);

        // metodo responsavel por incluir um objeto gheSetorClienteFuncao
        GheSetorClienteFuncao insert(GheSetorClienteFuncao gheSetorClienteFuncao, NpgsqlConnection dbConnection);

        // metodo responsavel por excluir um objeto gheSetorClienteFuncao
        void delete(GheSetorClienteFuncao gheSetorClienteFuncao, NpgsqlConnection dbConnection);

        // Método responsável por retonar uma colecao de gheSetorClienteFuncao dado um GheSetor.
        HashSet<GheSetorClienteFuncao> findAtivosByGheSetor(GheSetor gheSetor, NpgsqlConnection dbConnection);

        // Método responsável por desativar um gheSetorClienteFuncao.
        void update(GheSetorClienteFuncao gheSetorClienteFuncao, NpgsqlConnection dbConnection);

        // Método responsável por listar gheSetorClienteFuncao por ghe e função.
        HashSet<GheSetorClienteFuncao> findAtivosByGheAndFuncao(Ghe ghe, Funcao funcao, NpgsqlConnection dbConnection);

        DataSet findAllByGheSetor(GheSetor gheSetor, NpgsqlConnection dbConnection);

        Boolean IsNotUsedComentarioByGheSetorClienteFuncao(GheSetorClienteFuncao gheSetorClienteFuncao, NpgsqlConnection dbConnection);

        GheSetorClienteFuncao findById(Int64 id, NpgsqlConnection dbConnection);

        List<GheSetorClienteFuncao> findAtivosByGheSetorAndClienteFuncao(GheSetor gheSetor, ClienteFuncao clieteFuncao, NpgsqlConnection dbConnection);

        bool isUsedInService(GheSetorClienteFuncao gheSetorClienteFuncao, NpgsqlConnection dbConnection);

        List<GheSetorClienteFuncao> findAllByGheSetorList(GheSetor gheSetor, NpgsqlConnection dbConnection);
    }
}
