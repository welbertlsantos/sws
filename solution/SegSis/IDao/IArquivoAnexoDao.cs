﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SWS.Entidade;
using Npgsql;
using System.Data;

namespace SWS.IDao
{
    interface IArquivoAnexoDao
    {
        ArquivoAnexo insertArquivoAnexo(ArquivoAnexo arquivo, NpgsqlConnection dbConnection);
        ArquivoAnexo findArquivoAnexoById(Int64? id, NpgsqlConnection dbConnection);
        DataSet findAllArquivoAnexoByAso(Aso aso, NpgsqlConnection dbConnection);
        void delete(ArquivoAnexo arquivoAnexo, NpgsqlConnection dbConnection);
    }
}
