﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using Npgsql;
using NpgsqlTypes;
using SWS.Entidade;

namespace SWS.IDao
{
    interface IGheFonteAgenteExamePeriodicidadeDao
    {
        void insert(GheFonteAgenteExamePeriodicidade gheFonteAgenteExamePeriodicidade, NpgsqlConnection dbconnection);

        HashSet<GheFonteAgenteExamePeriodicidade> findAll(GheFonteAgenteExame gheFonteAgenteExame, NpgsqlConnection dbConnection);

        void update(GheFonteAgenteExamePeriodicidade gheFonteAgenteExamePeriodicidade, NpgsqlConnection dbConnection);

        void delete(GheFonteAgenteExamePeriodicidade gheFonteAgenteExamePeriodicidade, NpgsqlConnection dbConnection);

        bool isUsedInService(GheFonteAgenteExamePeriodicidade gheFonteAgenteExamePeriodicidade, NpgsqlConnection dbConnection);

        List<GheFonteAgenteExamePeriodicidade> listAllByGheByPeriodicidade(List<Ghe> ghe, Periodicidade periodicidade, NpgsqlConnection dbConnection);

        List<GheFonteAgenteExamePeriodicidade> findAllAtivosByGheFonteAgenteExame(GheFonteAgenteExame gheFonteAgenteExame, NpgsqlConnection dbConnection);
    }
}
