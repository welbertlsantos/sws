﻿using Npgsql;
using SWS.Entidade;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;

namespace SWS.IDao
{
    interface ILoteEsocialDao
    {
        LoteEsocial incluirLoteEsocial(LoteEsocial loteEsocial, NpgsqlConnection dbConnection);

        DataSet findByFilter(LoteEsocial loteEsocial, DateTime? dataCriacaoInicial, DateTime? dataCriacaoFinal, NpgsqlConnection dbConnection);

        LoteEsocialAso incluirLoteEsocialAso(LoteEsocialAso loteEsocialAso, NpgsqlConnection dbConnection);
        
        List<string> cpfDuplicadosNoLote(LoteEsocial loteEsocial, NpgsqlConnection dbConnection);

        LoteEsocial findById(long id, NpgsqlConnection dbConnection);

        void delete(LoteEsocial loteEsocial, NpgsqlConnection dbConnection);

        LoteEsocialMonitoramento insertLoteEsocialMonitoramento(LoteEsocialMonitoramento loteMonitoramento, NpgsqlConnection dbConnection);

        bool verificaExisteLoteJaGeradoByClienteAndPeriodo(Cliente cliente, DateTime periodo, NpgsqlConnection dbConnection);

        List<LoteEsocialAso> findAllCancelados2220ByFilter(Cliente cliente, DateTime periodoInicial, DateTime periodoFinal, NpgsqlConnection dbConnection);

        List<LoteEsocialMonitoramento> findAllCancelados2240ByFilter(Cliente cliente, DateTime periodoInicial, DateTime periodoFinal, NpgsqlConnection dbConnection);

        List<LoteEsocialAso> findAll2220ForCancelByFilter(Cliente cliente, ClienteFuncaoFuncionario clienteFuncaoFuncionario, String codigoLote, DateTime periodoInicial, DateTime periodoFinal, NpgsqlConnection dbConnection);

        List<LoteEsocialMonitoramento> findAll2240ForCancelByFilter(Cliente cliente, ClienteFuncaoFuncionario clienteFuncaoFuncionario, string codigoLote, DateTime periodoInicial, DateTime periodoFinal, NpgsqlConnection dbConnection);

        LoteEsocialAso findEsocialLoteAsoById(long id, NpgsqlConnection dbConnection);
        
        LoteEsocialAso updateLoteEsocialAso(LoteEsocialAso loteEsocialAso, NpgsqlConnection dbConnection);

        LoteEsocialMonitoramento findLoteEsocialMonitoramentoById(long id, NpgsqlConnection dbConnection);

        LoteEsocialMonitoramento updateLoteEsocialMonitoramento(LoteEsocialMonitoramento loteEsocialMonitoramento, NpgsqlConnection dbConnection);


    }
}
