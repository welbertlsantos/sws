﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SWS.Entidade;
using Npgsql;
using System.Data;

namespace SWS.IDao
{
    interface IContaDao
    {
        Conta insert(Conta conta, NpgsqlConnection dbConnection);

        Conta update(Conta conta, NpgsqlConnection dbConnection);

        Conta findById(Int64 id, NpgsqlConnection dbConnection);

        void mudaSituacaoConta(Conta conta, NpgsqlConnection dbConnection);
        
        DataSet findByFilter(Conta conta, NpgsqlConnection dbConnection);

        Conta findByNome(String nome, NpgsqlConnection dbConnection);

        Boolean verificaPodeAlterarConta(Int64 id, NpgsqlConnection dbConnection);

        void incrementaProximoNumero(Conta conta, NpgsqlConnection dbConnection);
    }
}
