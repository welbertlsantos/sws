﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SWS.Entidade;
using Npgsql;
using System.Data;

namespace SWS.IDao
{
    interface IAgenteDao
    {
        DataSet findByFilter(Agente agente, NpgsqlConnection dbConnection);

        Agente insert(Agente agente, NpgsqlConnection dbConnection);

        Agente findByDescricao(string descricao, NpgsqlConnection dbConnection);

        Agente findById(Int64 idAgente, NpgsqlConnection dbConnection);

        Agente update(Agente agenteAlterar, NpgsqlConnection dbConnection);

        DataSet findAllNotInGheFonte(Agente agente, GheFonte gheFonte, NpgsqlConnection dbConnection);

    }
}
