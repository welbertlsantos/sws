﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SWS.Entidade;
using Npgsql;
using System.Data;

namespace SWS.IDao
{
    interface IClienteFuncaoExamePeriodicidadeDao
    {
        ClienteFuncaoExamePeriodicidade insert(ClienteFuncaoExamePeriodicidade clienteFuncaoExamePeriodicidade, NpgsqlConnection dbConnection);
        DataSet listaPeriodicidadesByClienteFuncaoExame(ClienteFuncaoExame clienteFuncaoExame, NpgsqlConnection dbConnection);
        void delete(ClienteFuncaoExamePeriodicidade clienteFuncaoExamePeriodicidade, NpgsqlConnection dbConnection);
        void update(ClienteFuncaoExamePeriodicidade clienteFuncaoExamePeriodicidade, NpgsqlConnection dbConnection);
        List<ClienteFuncaoExamePeriodicidade> listAllByClienteFuncaoByPeriodicidade(ClienteFuncao clienteFuncao, Periodicidade periodicidade, NpgsqlConnection dbConnection);
    
    }
}
