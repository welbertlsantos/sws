﻿using Npgsql;
using SWS.Entidade;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SWS.IDao
{
    interface IClienteFuncionarioDao
    {
        ClienteFuncionario findById(long id, NpgsqlConnection dbConnection);
        List<ClienteFuncionario> findAtivosByCliente(Cliente cliente, DateTime periodo, NpgsqlConnection dbConnection);
        List<ClienteFuncionario> findAllByFuncionario(Funcionario funcionario, NpgsqlConnection dbConnection);
        ClienteFuncionario update(ClienteFuncionario clienteFuncionario, NpgsqlConnection dbConnection);
        void delete(ClienteFuncionario clienteFuncionario, NpgsqlConnection dbConnection);
    }
}
