﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using SWS.Entidade;
using Npgsql;

namespace SWS.IDao
{
    interface IHospitalDao
    {
        // Método responsável por incluir um hospital na base de dados.
        Hospital insert(Hospital hospital, NpgsqlConnection dbConnection);

        // Método responsável por atualizar um hospital
        Hospital update(Hospital hospital, NpgsqlConnection dbConnection);

        // Método responsável por buscar o próximo identificador, baseado em uma sequence.
        Int64 recuperaProximoId(NpgsqlConnection dbConnection);

        // Método responsável por buscar um hospital dada sua descrição.
        Hospital findByNome(String descricao, NpgsqlConnection dbConnection);

        // Método responsável por buscar um hospital dado seu identificador.
        Hospital findHospitalById(Int64 idHospital, NpgsqlConnection dbConnection);

        // Método responsável por buscar um hospital dado os campos preenchidos.
        DataSet findHospitalByFilter(Hospital hospital, NpgsqlConnection dbConnection);

        // Método responsável por verificar se o hospital está senndo usado por um estudo
        Boolean findHospitalInEstudo(Hospital hospital, NpgsqlConnection dbConnection);

        List<Hospital> findAll(NpgsqlConnection dbConnection);

        DataSet findAllHospitalEstudoByPcmso(HospitalEstudo hospitalEstudo, NpgsqlConnection dbConnection);

        List<Hospital> findAllByEstudo(Estudo pcmso, NpgsqlConnection dbConnection);
    }
}
