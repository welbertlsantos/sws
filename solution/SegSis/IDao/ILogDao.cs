﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SWS.Entidade;
using Npgsql;

namespace SWS.IDao
{
    interface ILogDao
    {
        void insertLog(Log log, NpgsqlConnection dbConnection);
    }
}
