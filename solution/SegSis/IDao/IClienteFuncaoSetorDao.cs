﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Npgsql;
using SWS.Entidade;
using System.Data;

namespace SWS.IDao
{
    interface IClienteFuncaoSetorDao
    {
        void deleteClienteFuncaoSetor(Int64 id, NpgsqlConnection dbConnection);

        void insertClienteFuncaoSetor(ClienteFuncaoSetor clienteFuncaoSetor, NpgsqlConnection dbConnection);

        DataSet findAllSetorByClienteFuncao(ClienteFuncao clienteFuncao, NpgsqlConnection dbConnection);

        Boolean findClienteFuncaoSetorByClienteFuncaoExameAso(ClienteFuncaoSetor clienteFuncaoSetor, NpgsqlConnection dbConnection);

        void desativaClienteFuncaoSetor(ClienteFuncaoSetor clienteFuncaoSetor, NpgsqlConnection dbConnection);

    }
}
