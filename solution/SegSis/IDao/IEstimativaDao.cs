﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using Npgsql;
using SWS.Entidade;

namespace SWS.IDao
{
    interface IEstimativaDao
    {
        DataSet findAll(NpgsqlConnection dbConnection);

        DataSet findAllNotInEstudo(Estudo estudo, Estimativa estimativa, NpgsqlConnection dbConnection);
    }
}
