﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SWS.Entidade;
using Npgsql;
using System.Data;

namespace SWS.IDao
{
    interface IClienteVendedorDao
    {
        /* metodo responsavel por incluir cliente vendedor no relacionamento seg_rep_cli  */
        VendedorCliente insert(VendedorCliente vendedorCliente, NpgsqlConnection dbConnection);

        /* metodo responsavel por retornar uma lista com vendedorCliente */
        List<VendedorCliente> findByCliente(Cliente cliente, NpgsqlConnection dbConnection);
        
        /* metodo responsavel por retornar um objeto da classe VendedorCliente passando o id como referencia */
        VendedorCliente findById(Int64 idVendedorCliente, NpgsqlConnection dbConnection);

        /* metodo responsavel por alterar o status do relacionamento entre cliente e vendedor */
        void update(VendedorCliente vendedorCliente, NpgsqlConnection dbConnection);
        
    }
}
