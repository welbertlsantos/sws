﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Npgsql;
using SWS.Entidade;

namespace SWS.IDao
{
    interface IAcompanhamentoAtendimentoDao
    {
        AcompanhamentoAtendimento insert(AcompanhamentoAtendimento acompanhamentoAtendimento, NpgsqlConnection dbConnection);

    }
}
