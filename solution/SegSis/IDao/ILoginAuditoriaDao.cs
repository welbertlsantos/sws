﻿using Npgsql;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SWS.IDao
{
    interface ILoginAuditoriaDao
    {
        /*
         * Método responsável por registrar login auditoria.
         */
        void registrarLoginAuditoria(String login, String ip, NpgsqlConnection dbConnection);

        /*
        * Método responsável por desativar login auditoria.
        */
        void desativarLoginAuditoria(String login, String ip, NpgsqlConnection dbConnection);

        /*
        * Método responsável por verificar se o login está ativo.
        */
        Boolean isLoginAtivo(String login, String ip, NpgsqlConnection dbConnection);
    }
}
