﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SWS.Entidade;
using Npgsql;
using System.Data;

namespace SWS.IDao
{
    interface IPlanoDao
    {
        Plano insert(Plano plano, NpgsqlConnection dbConnection);

        Plano update(Plano plano, NpgsqlConnection dbConnection);

        Plano findById(Int64 id, NpgsqlConnection dbConnection);

        DataSet findByFilter(Plano plano, NpgsqlConnection dbConnection);

        Plano findByDescricao(string descricao, NpgsqlConnection dbConnection);
    }
}
