﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using Npgsql;
using SWS.Entidade;

namespace SWS.IDao
{
    interface IPerfilDao
    {
        /**
         * Método responsável por listar todos os perfis.
         */
        DataSet findAll(NpgsqlConnection dbConnection);

        /*
         * Método responsável por listar os perfis de um determinado usuário.
         */
        HashSet<Perfil> findByUsuario(Int64? idUsuario, NpgsqlConnection dbConnection);

        /*
         * Método responsável por listar os perfis de acordo com os parâmetros passados.
         */
        DataSet findByFilter(Perfil perfil, NpgsqlConnection dbConnection);

        /* Método responsável por procurar um perfil de acordo com os Id Passado. */
        Perfil findById(Int64 idPerfil, NpgsqlConnection dbConnection);

        /* Método responsável por desativar um perfil . */
        void changeStatusPerfil(Int64 idPerfil, string situacao, NpgsqlConnection dbConnection);

        /* Método responsável por incluir um perfil . */
        Perfil insert(Perfil perfil, NpgsqlConnection dbConnection);

        /* Método responsável por procurar uma descricao no banco de dados. */
        Perfil findByDescricao(string descricao, NpgsqlConnection dbConnection);

        /* Método responsável por alterar um perfil de usuário no banco de dados. */
        Perfil update(Perfil perfilAlterar, NpgsqlConnection dbConnection);
    }
}
