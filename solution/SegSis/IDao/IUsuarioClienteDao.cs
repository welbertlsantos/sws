﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SWS.Entidade;
using Npgsql;

namespace SWS.IDao
{
    interface IUsuarioClienteDao
    {
        UsuarioCliente insert(UsuarioCliente usuarioCliente, NpgsqlConnection dbConnection);
        UsuarioCliente update(UsuarioCliente usuarioCliente, NpgsqlConnection dbConnection);
        List<UsuarioCliente> findAllByUsuario(Usuario usuario, NpgsqlConnection dbConnection);
        void delete(UsuarioCliente usuarioCliente, NpgsqlConnection dbConnection);
        UsuarioCliente findById(long id, NpgsqlConnection dbConnection);
 
    }
}
