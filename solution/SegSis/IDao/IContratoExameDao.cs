﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Npgsql;
using NpgsqlTypes;
using SWS.Entidade;
using System.Data;


namespace SWS.IDao
{
    interface IContratoExameDao
    {
        Int64 recuperaProximoId(NpgsqlConnection dbConnection);

        void insert(ContratoExame contratoExame, NpgsqlConnection dbConnection);

        HashSet<ContratoExame> findAtivosByContrato(Contrato contrato, NpgsqlConnection dbConnection);

        void delete(Int64 id, NpgsqlConnection dbConnection);

        void update(ContratoExame contratoExame, NpgsqlConnection dbConnection);

        ContratoExame findAllByContratoAndExameAndSituacao(Contrato contrato, Exame exame, String str,  NpgsqlConnection dbConnection);

        DataSet findAtivosByContratoDataSet(Contrato contrato, NpgsqlConnection dbConnection);

        void reativated(ContratoExame contratoExame, NpgsqlConnection dbConnection);

        void desativated(ContratoExame contratoExame, NpgsqlConnection dbConnection);

        ContratoExame findById(long id, NpgsqlConnection dbConnection);
        
    }
}
