﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SWS.Entidade;
using Npgsql;

namespace SWS.IDao
{
    interface IClientePropostaDao
    {
        ClienteProposta insertClienteProposta(ClienteProposta clienteProposta, NpgsqlConnection dbConnection);

        ClienteProposta findClientePropostaByContrato(Contrato contrato, NpgsqlConnection dbConnection);

    }
}
