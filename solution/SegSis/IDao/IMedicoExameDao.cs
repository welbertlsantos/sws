﻿using Npgsql;
using SWS.Entidade;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SWS.IDao
{
    interface IMedicoExameDao
    {
        long recuperaProximoId(NpgsqlConnection dbConnection);
        MedicoExame insert(MedicoExame medicoExame, NpgsqlConnection dbConnection);
        MedicoExame update(MedicoExame medicoExame, NpgsqlConnection dbConnection);
        MedicoExame findById(long id, NpgsqlConnection dbConnection);
        List<MedicoExame> findAllByExame(Exame exame, NpgsqlConnection dbConnection);
        void delete(MedicoExame medicoExame, NpgsqlConnection dbConnection);
    }
}
