﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using Npgsql;
using NpgsqlTypes;
using SWS.Entidade;

namespace SWS.IDao
{
    interface IPeriodicidadeDao
    {
        DataSet findAll(NpgsqlConnection dbConnection);

        DataSet findByGheFonteAgenteExame(Int64 idGheFonteAgenteExame, NpgsqlConnection dbConnection);

        Periodicidade findById(Int64 idPeriodicidade, NpgsqlConnection dbConnection);

        DataSet findByFilter(Periodicidade periodicidade, NpgsqlConnection dbConnection);

        DataSet findAllNotInClienteFuncaoExame(ClienteFuncaoExame clienteFuncaoExame, NpgsqlConnection dbConnection);

        DataSet findAllNotInGheFonteAgenteExame(Periodicidade periodicidade, GheFonteAgenteExame gheFonteAgenteExame, NpgsqlConnection dbConnection);
    }
}
