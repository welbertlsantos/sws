﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using SWS.Entidade;
using Npgsql;

namespace SWS.IDao
{
    interface IRamoDao
    {
        DataSet findRamoByFilter(Ramo ramo, NpgsqlConnection dbConnection);

        Ramo insert(Ramo ramo, NpgsqlConnection dbConnection);

        Ramo findById(Int64 id, NpgsqlConnection dbConnection);

        Ramo update(Ramo ramo, NpgsqlConnection dbConnection);

        Ramo findByDescricao(String value, NpgsqlConnection dbConnection);
    }
}
