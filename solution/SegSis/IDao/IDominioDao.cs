﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Npgsql;
using System.Data;
using SWS.Entidade;

namespace SWS.IDao
{
    interface IDominioDao
    {
        /*
         * Método responsável por listar todos os domínios.
         */
        DataSet listaTodos(NpgsqlConnection dbConnection);

        /**
         * Método responsável por listar todos os domínios associados a um perfil
         */
        HashSet<Dominio> findByPerfil(Int64? idDominio, NpgsqlConnection dbConnection);
    }
}
