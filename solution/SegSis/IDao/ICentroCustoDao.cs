﻿using Npgsql;
using SWS.Entidade;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SWS.IDao
{
    public interface ICentroCustoDao
    {
        CentroCusto insert(CentroCusto centroCusto, NpgsqlConnection dbConnection);
        CentroCusto update(CentroCusto centroCusto, NpgsqlConnection dbConnection);
        List<CentroCusto> findAtivosByCliente(Cliente cliente, NpgsqlConnection dbConnection);
        void delete(CentroCusto centroCusto, NpgsqlConnection dbConnection);
        bool isUsed(CentroCusto centroCusto, NpgsqlConnection dbConnection);
        CentroCusto findById(long id, NpgsqlConnection dbConnection);
    }
}
