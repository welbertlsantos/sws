﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Npgsql;
using SWS.Entidade;
using System.Data;


namespace SWS.IDao
{
    interface IProtocoloDao
    {
        Int64 recuperaProximoId(NpgsqlConnection dbConnection);

        Protocolo insertProtocolo(Protocolo protocolo, NpgsqlConnection dbConnection);

        Protocolo updateProtocolo(Protocolo protocolo, String acao, NpgsqlConnection dbConnection);

        void changeSituacao(Protocolo protocolo, String situacao, NpgsqlConnection dbConnection);

        DataSet findByFilter(Protocolo protocolo, DateTime? dataGravacaoFinal,
            DateTime? dataCancelamentoFinal, String tipoItem, Int64? idItem, NpgsqlConnection dbConnection);

        DataSet findAllItensByProtocolo(Protocolo protocolo, NpgsqlConnection dbConnection);

        Protocolo findById(Int64 id, NpgsqlConnection dbConnection);

        void insertExameInProtocolo(Protocolo protocolo, NpgsqlConnection dbConnection);

        void deleteItemProtocolo(Int64 id, String tipo, NpgsqlConnection dbConnection);

        void InsertItemProtocolo(ProtocoloItem protocoloItem, NpgsqlConnection dbConnection);
    }
}
