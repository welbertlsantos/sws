﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SWS.Entidade;
using Npgsql;
using System.Data;

namespace SWS.IDao
{
    interface ICronogramaDao
    {
        // Método responsavel por incluir um cronograma na tabela seg_cronograma.
        Cronograma insert(Cronograma cronograma, NpgsqlConnection dbConnection);

        // Método responsável por alterar um cronograma na tabela seg_cronograma.
        Cronograma update(Cronograma cronograma, NpgsqlConnection dbConnection);

        // Método responsável por buscar um cronograma pelo id.
        Cronograma findById(Int64 idCronograma, NpgsqlConnection dbConnection);

        Cronograma findByEstudo(Estudo estudo, NpgsqlConnection dbConnection);
    }
}
