﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SWS.Entidade;
using Npgsql;
using NpgsqlTypes;
using System.Data;

namespace SWS.IDao
{
    interface IContratoDao
    {
        Contrato findContratoSistemaByCliente(Cliente cliente, NpgsqlConnection dbConnection);

        Contrato insert(Contrato contrato, NpgsqlConnection dbConnection);

        Int64 recuperaProximoId(NpgsqlConnection dbConnection);

        Contrato insertContratoPrestador(Contrato contrato, NpgsqlConnection dbConnection);

        List<Contrato> findAllByCliente(Cliente cliente, ClienteProposta clienteProposta, NpgsqlConnection dbConnection);

        Contrato findById(Int64 id, NpgsqlConnection dbConnection);

        DataSet findContratoByFilter(Contrato contrato, DateTime? dataElaboracao, DateTime? dataVencimento,
            DateTime? dataFechamento, DateTime? dataCancelamento, NpgsqlConnection dbConnection);

        DataSet findAditivoByContrato(Contrato contrato, NpgsqlConnection dbConnection);

        Contrato update(Contrato contrato, NpgsqlConnection dbConnection);

        void cancela(Contrato contrato, NpgsqlConnection dbConnection);

        void validaContrato(Contrato contrato, NpgsqlConnection dbConnection);
        
        void validaProposta(Contrato contrato, NpgsqlConnection dbConnection);

        Contrato insertAditivo(Contrato contrato, NpgsqlConnection dbConnection);

        void changeStatus(Contrato contrato, NpgsqlConnection dbConnection);
        
        Boolean verificaUltimoContrato(Contrato contrato, NpgsqlConnection dbConnection);

        Boolean verificaIsAditivo(Contrato contrato, NpgsqlConnection dbConnection);

        void delete(Contrato contrato, NpgsqlConnection dbConnection);

        Contrato findContratoVigenteByData(DateTime dtInicio, Cliente cliente, Cliente prestador, NpgsqlConnection dbConnection);

        void encerra(Contrato contrato, NpgsqlConnection dbConnection);

        Contrato verificaContratoVigente(DateTime dataInicio, DateTime? dataFim, Cliente cliente, Cliente prestador, NpgsqlConnection dbConnection);

        Contrato findLastContratoByCliente(Cliente cliente, NpgsqlConnection dbConnection);


    }
}
