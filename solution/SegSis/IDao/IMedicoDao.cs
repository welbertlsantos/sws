﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SWS.Entidade;
using Npgsql;
using System.Data;

namespace SWS.IDao
{
    interface IMedicoDao
    {
        // Método responsável por incluir um médico.
        Medico insert(Medico medico, NpgsqlConnection dbConnection);

        // Método responsável por atualizar um medico.
        Medico update(Medico medico, NpgsqlConnection dbConnection);

        // Método responsável por recuperar o próximo identificador disponível para médico.
        Int64 recuperaProximoId(NpgsqlConnection dbConnection);

        // Método reponsável por buscar um médico pelo seu identificador.
        Medico findById(Int64 idMedico, NpgsqlConnection dbConnection);

        // Método responsável por buscar um médico pelos campos preenchidos.
        DataSet findByFilter(Medico medico, NpgsqlConnection dbConnection);

        // Método responsável por listar os médicos examinadores de um estudo.
        Boolean isMedicoUsed(Medico medico, NpgsqlConnection dbConnection);

        // Método responsável por listar todos os médicos ativos.
        DataSet findMedicosAtivoByMedico(Medico medico, NpgsqlConnection dbConnection);

        DataSet findAtivosByMedicoEstudo(MedicoEstudo medicoEstudo, NpgsqlConnection dbConnection);

        HashSet<Medico> findAllByPcmso(Estudo pcmso, NpgsqlConnection dbConnection);

        Medico findCoordenadorByEstudo(Estudo pcmso, NpgsqlConnection dbConnection);

        Medico findMedicoCoordenadorByCliente(Cliente cliente, NpgsqlConnection dbConnection);

        Boolean findMedicoByCRM(String crm, NpgsqlConnection dbConnection);

        Medico findMedicoByCrm(String crm, NpgsqlConnection dbConnection);

        DataSet findMedicoCoordenadorNotInCliente(Cliente cliente, Medico medico, NpgsqlConnection dbConnection);
    }
}
