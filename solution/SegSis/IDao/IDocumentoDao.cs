﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Npgsql;
using System.Data;
using SWS.Entidade;

namespace SWS.IDao
{
    interface IDocumentoDao
    {
        Int64 recuperaProximoId(NpgsqlConnection dbConnection);

        DataSet findByFilter(Documento documento, NpgsqlConnection dbConnection);

        Documento insert(Documento documento, NpgsqlConnection dbConnection);

        Documento findByDescricao(String descricao, NpgsqlConnection dbConnection);

        Documento findById(Int64 id, NpgsqlConnection dbConnection);

        Documento update(Documento documento, NpgsqlConnection dbConnection);

        List<Documento> findAll(NpgsqlConnection dbConnection);

        Boolean checkCanChanges(Documento documento, NpgsqlConnection dbConnection);

    }
}
