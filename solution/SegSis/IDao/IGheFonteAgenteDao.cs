﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SWS.Entidade;
using System.Data;
using Npgsql;

namespace SWS.IDao
{
    interface IGheFonteAgenteDao
    {
        // metodo responsavel por incluir um objeto gheFonteAgente.
        GheFonteAgente insert(GheFonteAgente gheFonteAgente, NpgsqlConnection dbConnection);

        // método responsável por excluir um GheFonteAgente dado um id.
        void delete(Int64 id, NpgsqlConnection dbConnection);

        // metodo responsavel por recuperar um objeto GheFonteAgente dado um id.
        GheFonteAgente findById(Int64 id, NpgsqlConnection dbConnection);

        // Método responsável por listar todos os GheFonteAgente dado GheFonte.
        HashSet<GheFonteAgente> findAtivosByGheFonteHash(GheFonte gheFonteBusca, NpgsqlConnection dbConnection);

        // Método responsável por listas todos os agentes dado um GheFonte
        DataSet findAllByGheFonte(GheFonte gheFonte, NpgsqlConnection dbConnection);

        // Método responsavel por retornar uma coleção de Riscos, para montar a grid de inclusão e alteração do pcmso.
        DataSet findAllRiscosByGhe(Ghe ghe, NpgsqlConnection dbConnection);

        // Método responsável por verificar se um gheFonteAgente já foi utilizado
        Boolean isGheFonteAgenteUsed(GheFonteAgente gheFonteAgente, NpgsqlConnection dbConnection);

        GheFonteAgente update(GheFonteAgente gheFonteAgente, NpgsqlConnection dbConnection);

        Boolean isUsedByAgente(Agente agente, NpgsqlConnection dbConnection);

        GheFonteAgente findByGheFonteAndAgente(GheFonte gheFonte, Agente agente, NpgsqlConnection dbConnection);

        bool isUsedInService(GheFonteAgente gheFonteAgente, NpgsqlConnection dbConnection);

        List<GheFonteAgente> findAllByGheFonteList(GheFonte gheFonte, NpgsqlConnection dbConnection);
    }
}
