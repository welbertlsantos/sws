﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Npgsql;
using SWS.Entidade;

namespace SWS.Dao
{
    interface IRevisaoDao
    {
        // Método responsável por cadastrar uma revisão.
        void incluir(Revisao revisao, NpgsqlConnection dbConnection);

        // Método resposnável por verificar se a revisão é a última.
        Boolean verificaPodeReplicar(Int64? revisao, NpgsqlConnection dbConnection);

        // Método responsável por buscar a raiz de uma determinada revisão.
        Int64? findRaiz(Int64? idPpra, NpgsqlConnection dbConnection);

        // Método responsável por buscar uma revisao.
        Revisao findRevisao(Int64? idPpra, NpgsqlConnection dbConnection);

        Boolean isCorrecao(Int64? idEstudo, NpgsqlConnection dbConnection);
    }
}
