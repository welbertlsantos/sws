﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SWS.Entidade;
using Npgsql;
using System.Data;

namespace SWS.IDao
{
    interface ICobrancaDao
    {
        Cobranca insert(Cobranca cobranca, NpgsqlConnection dbConnection);
        
        DataSet findByFilter(Cobranca cobranca, Cliente cliente, Nfe nfe, 
            DateTime? dataInicioEmissao, DateTime? dataFimEmissao, DateTime? dataInicioVencimento, 
            DateTime? dataFimVencimento, DateTime? dataInicioBaixa, DateTime? dataFimBaixa, 
            DateTime? dataInicioCancelamento, DateTime? dataFimCancelamento, NpgsqlConnection dbConnection);
        
        void baixarCobranca(Cobranca cobranca, NpgsqlConnection dbConnection);
        
        void estornarBaixaCobranca(Cobranca cobranca, NpgsqlConnection dbConnection);
        
        void cancelarCobranca(Cobranca cobranca, NpgsqlConnection dbConnection);
        
        void reativarCobranca(Cobranca cobranca, NpgsqlConnection dbConnection);

        LinkedList<Cobranca> findCobrancasByNotafiscal(Nfe notaFiscal, NpgsqlConnection dbConnection);
        
        void alteraFormaPagamento(Cobranca cobranca, NpgsqlConnection dbConnection);

        void prorrogar(Cobranca cobranca, NpgsqlConnection dbConnection);

        Cobranca findById(Int64 id, NpgsqlConnection dbConnection);

        void alteraSituacaoCobranca(Cobranca cobranca, String situacao, NpgsqlConnection dbConnection);

    }
}
