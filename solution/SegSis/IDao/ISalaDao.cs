﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SWS.Entidade;
using Npgsql;
using NpgsqlTypes;
using System.Data;

namespace SWS.IDao
{
    interface ISalaDao
    {
        Sala findByDescricao(String descricao, long idEmpresa, NpgsqlConnection dbConnection);
        Sala insert(Sala sala, NpgsqlConnection dbConnection);
        DataSet findByFilter(Sala sala, NpgsqlConnection dbConnection);
        Sala findById(Int64 id, NpgsqlConnection dbConnection);
        Sala update(Sala sala, NpgsqlConnection dbConnection);
        List<Sala> findallSalaByEmpresa(Empresa empresa, NpgsqlConnection dbConnection);
        
    }
}
