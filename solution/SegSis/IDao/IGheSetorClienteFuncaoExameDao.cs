﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SWS.Entidade;
using Npgsql;
using System.Data;

namespace SWS.IDao
{
    interface IGheSetorClienteFuncaoExameDao
    {
        DataSet findAllFuncaoNotInGheSetorClienteFuncaoExame(Exame exame, Ghe ghe, Funcao funcao, NpgsqlConnection dbConnection);

        GheSetorClienteFuncaoExame insert(GheSetorClienteFuncaoExame gheSetorClienteFuncaoExame, NpgsqlConnection dbConnection);

        Dictionary<Int64, Dictionary<Funcao, Int32>> findAllByGheAndExame(Ghe ghe, Exame exame, NpgsqlConnection dbConnection);

        void deleteByGheAndExame(Ghe ghe, Exame exame, NpgsqlConnection dbConnection);

        void deleteByGheAndExameAndFuncao(Ghe ghe, Exame exame, Funcao funcao, NpgsqlConnection dbConnection);

        List<GheSetorClienteFuncaoExame> findByGheAndFuncao(Ghe ghe, Funcao funcao, NpgsqlConnection dbConnection);

        GheSetorClienteFuncaoExame update(GheSetorClienteFuncaoExame gheSetorClienteFuncaoExame, NpgsqlConnection dbConnection);

        List<GheSetorClienteFuncaoExame> findAllByGheSetorClienteFuncao(GheSetorClienteFuncao gheSetorClienteFuncao, NpgsqlConnection dbConnection);
    }
}
