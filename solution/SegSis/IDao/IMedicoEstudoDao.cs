﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SWS.Entidade;
using Npgsql;
using NpgsqlTypes;
using System.Data;


namespace SWS.IDao
{
    interface IMedicoEstudoDao
    {
        DataSet findAllByEstudo(Estudo pcmso, NpgsqlConnection dbConnection);

        MedicoEstudo insert(MedicoEstudo medicoEstudo, NpgsqlConnection dbConnection);

        void delete(MedicoEstudo medicoEstudo, NpgsqlConnection dbConnection);

        Boolean medicoIsUsed(Medico medico, NpgsqlConnection dbConnection);

        List<MedicoEstudo> findAllByPcmsoList(Estudo pcmso, NpgsqlConnection dbConnection);

        MedicoEstudo update(MedicoEstudo medicoEstudo, NpgsqlConnection dbConnection);
        

    }
}
