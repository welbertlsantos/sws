﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SWS.Entidade;
using Npgsql;
using System.Data;

namespace SWS.IDao
{
    interface ICorrecaoEstudoDao
    {
        CorrecaoEstudo insertCorrecaoEstudo(CorrecaoEstudo correcaoEstudo, NpgsqlConnection dbConnection);

        DataSet findAllCorrecaoByEstudo(Estudo estudo, NpgsqlConnection dbConnection);

    }
}
