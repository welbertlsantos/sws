﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SWS.Entidade;
using Npgsql;

namespace SWS.IDao
{
    interface IEmailDao
    {
        /// <summary>
        /// Método que insere um e-mail na tabela
        /// </summary>
        /// <param name="email">Atributo email para gravar na tabela SEG_EMAIL.</param>
        /// <param name="dbConnection">parâmetros de conexão.</param>
        /// <returns>E-mail gravado no banco com o seu Id.</returns>
        Email insert(Email email, NpgsqlConnection dbConnection);

        /// <summary>
        /// Método qeu retorna uma colecao de email dado um prontuário.
        /// </summary>
        /// <param name="prontuario">Prontuário enviado para consulta</param>
        /// <param name="dbConnection">conexão com o banco de dados.</param>
        /// <returns>Lista encadeadade de emails do prontuário.</returns>
        List<Email> findEmailByProntuario(Prontuario prontuario, NpgsqlConnection dbConnection);

        List<Email> findEmailByUsuario(Usuario usuario, NpgsqlConnection dbConnection);

    }
}
