﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SWS.Entidade;
using Npgsql;

namespace SWS.IDao
{
    interface IClienteFuncaoExameDao
    {
        ClienteFuncaoExame insert(ClienteFuncaoExame clienteFuncaoExame, NpgsqlConnection dbConnection);

        ClienteFuncaoExame findByClienteFuncaoAndExame(ClienteFuncao clienteFuncao, Exame exame, NpgsqlConnection dbConnection);

        Boolean isUsedByClienteFuncaoExame(ClienteFuncaoExame clienteFuncaoExame, NpgsqlConnection dbConnection);

        void delete(ClienteFuncaoExame clienteFuncaoExame, NpgsqlConnection dbConnection);

        void disable(ClienteFuncaoExame clienteFuncaoExame, NpgsqlConnection dbConnection);

        Exame findExameByClienteFuncaoExame(ClienteFuncaoExame clienteFuncaoExame, NpgsqlConnection dbconnection);

        void update(ClienteFuncaoExame clienteFuncaoExame, NpgsqlConnection dbConnection);

        Boolean IsUsedByExame(Exame exame, NpgsqlConnection dbConnection);

        ClienteFuncaoExame findById(Int64 id, NpgsqlConnection dbConnection);
    }
}
