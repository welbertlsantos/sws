﻿using System;
using System.Collections.Generic;
using System.Linq;
using Npgsql;
using NpgsqlTypes;
using SWS.Entidade;

namespace SWS.IDao
{
    interface IClienteFuncaoFuncionarioDao
    {
        ClienteFuncaoFuncionario insert(ClienteFuncaoFuncionario clienteFuncaoFuncionario, NpgsqlConnection dbConnection);

        Int64 recuperaProximoId(NpgsqlConnection dbConnection);

        ClienteFuncaoFuncionario findAtivoByFuncionario(Funcionario funcionario, NpgsqlConnection dbConnection);

        void disable(ClienteFuncaoFuncionario clienteFuncaoFuncionario, DateTime dataDemissao, NpgsqlConnection dbConnection);

        ClienteFuncaoFuncionario findById(Int64? id, NpgsqlConnection dbConnection);

        ClienteFuncaoFuncionario findByFuncionarioAndSituacao(Funcionario funcionario, String situacao, NpgsqlConnection dbConnection);

        List<ClienteFuncaoFuncionario> findByFuncionarioAndSituacaoList(Funcionario funcionario, bool ativos, NpgsqlConnection dbConnection);

        void delete(ClienteFuncaoFuncionario clienteFuncaoFuncionario, NpgsqlConnection dbConnection);

        void update(ClienteFuncaoFuncionario clienteFuncaoFuncionario, NpgsqlConnection dbConnection);

        List<ClienteFuncaoFuncionario> findAllFuncionarioByClienteAndFuncionario(Cliente cliente, Funcionario funcionario, bool ativos, NpgsqlConnection dbConnection);

        List<ClienteFuncaoFuncionario> findAtivosByCliente(Cliente cliente, DateTime periodo, NpgsqlConnection dbConnection);

        List<ClienteFuncaoFuncionario> findAllByClienteFuncao(ClienteFuncao clienteFuncao, NpgsqlConnection dbConnection);


    }
}
