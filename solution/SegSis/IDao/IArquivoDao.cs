﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SWS.Entidade;
using Npgsql;

namespace SWS.IDao
{
    interface IArquivoDao
    {
        Arquivo insertArquivo(Arquivo arquivo, NpgsqlConnection dbConnection);
        Arquivo findArquivoById(Int64? id, NpgsqlConnection dbConnection);
    }
}
