﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SWS.Entidade;
using Npgsql;
using System.Data;

namespace SWS.IDao
{
    interface IFuncaoDao
    {

        /* Método responsavel por recuperar uma funcao atráves do filtro fornecido */
        DataSet findByFilter(Funcao funcao, NpgsqlConnection dbConnection);

        /* Método responsavel por incluir uma funcao no banco de dados */
        Funcao insert(Funcao funcao, NpgsqlConnection dbConnection);

        /* Método responsavel por procurar uma descricao no banco de dados. */
        Funcao findByDescricao(string descricao, NpgsqlConnection dbConnection);

        /* Método responsavel por procurar uma funcao no banco de dados de acordo com o id informado. */
        Funcao findById(Int64 idFuncao, NpgsqlConnection dbConnection);

        /* Método responsavel por alterar uma funcao no banco de dados. */
        Funcao update(Funcao funcaoAlterar, NpgsqlConnection dbConnection);

        // metodo responsavel por retornar um conjunto de funcao ativas.
        DataSet findAtivaNotInClient(Funcao funcao, Cliente cliente, NpgsqlConnection dbConnection);

        // Método responsável por listar todas as funções de um GHE.
        HashSet<Funcao> findByGhe(Ghe ghe, NpgsqlConnection dbConnection);

        // Método responsável por listas todas as funções distintas de um ghe dado um ghe.
        DataSet findAllByGhe(Ghe ghe, NpgsqlConnection dbConnection);

        DataSet findAtivaNotInClient(Funcao funcao, Boolean select, List<ClienteFuncao> funcoes, NpgsqlConnection dbConnection);

        Funcao verificaPodeAlterarFuncao(Funcao funcao, NpgsqlConnection dbConnection);
    }
}
