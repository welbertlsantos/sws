﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SWS.ViewHelper;
using Npgsql;
using SWS.Entidade;
using System.Data;

namespace SWS.IDao
{
    interface ICidadeIbgeDao
    {

        IList<SelectItem> findAllCidadeByUf(String uf, NpgsqlConnection dbConnection);

        CidadeIbge findCidadeById(Int64 id, NpgsqlConnection dbConnection);

        IList<SelectItem> findAllCidade(NpgsqlConnection dbConnection);

        SelectItem findItemCidadeById(Int64 id, NpgsqlConnection dbConnection);

        DataSet findAllCidadeByUfByCidade(String uf, CidadeIbge cidade, NpgsqlConnection dbConnection);


    }
}
