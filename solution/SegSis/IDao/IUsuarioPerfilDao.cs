﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SWS.Entidade;
using Npgsql;
using System.Data;

namespace SWS.IDao
{
    interface IUsuarioPerfilDao
    {
        /*
         * Método responsável por vincular um usuário Nome um perfil.
         */
        UsuarioPerfil incluir(UsuarioPerfil usuarioPerfil, NpgsqlConnection dbConnection);

        /*
         * Método responsável por excluir o vínculo entre um usuário e um perfil.
         */
        void excluir(UsuarioPerfil usuarioPerfil, NpgsqlConnection dbConnection);

        /*
         * Método responsável por excluir todos os perfis de um usuário.
         */
        void excluirPerfisDeUmUsuario(Int64? idUsuario, NpgsqlConnection dbConnection);

        DataSet findAllPerfilByUsuario(Usuario usuario, NpgsqlConnection dbConnection);
    }
}
