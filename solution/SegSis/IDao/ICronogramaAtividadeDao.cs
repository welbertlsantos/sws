﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SWS.Entidade;
using Npgsql;
using System.Data;

namespace SWS.IDao
{
    interface ICronogramaAtividadeDao
    {
        // Método responsável por incluri todo o relacionamento entre atividade e cronograma na tabela seg_atividade_cronograma.
        CronogramaAtividade insert(CronogramaAtividade cronogramaAtividade, NpgsqlConnection dbConnection);

        // Método responsável por buscar o cronograma atividade através do cronograma
        HashSet<CronogramaAtividade> findByCronograma(Cronograma cronograma, NpgsqlConnection dbConnection);

        void update(CronogramaAtividade cronogramaAtividade, NpgsqlConnection dbConnection);

        void delete(CronogramaAtividade cronogramaAtividade, NpgsqlConnection dbConnection);

        Dictionary<Int64, CronogramaAtividade> findByCronogramaDic(Cronograma cronograma, NpgsqlConnection dbConnection);

        Boolean isAtividadeCronogramaIsUsedByAtividade(Atividade atividade, NpgsqlConnection dbConnection);
    }
}
