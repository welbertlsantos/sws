﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SWS.Excecao
{
    class SetorException : System.Exception
    {
        public static String msg1 = "O Setor não pode ser alterado." + System.Environment.NewLine + "Já foi usado por um estudo.";
        
        public SetorException() : base () {}
        public SetorException(String message) : base(message) {}
        public SetorException(string message, System.Exception inner) : base(message, inner) { }

        // A constructor é necessário para serializacao quando uma
        // exceção é propagada de um servidor remoto para um cliente. 

        protected SetorException(System.Runtime.Serialization.SerializationInfo info,
            System.Runtime.Serialization.StreamingContext context) { }
    }
}
