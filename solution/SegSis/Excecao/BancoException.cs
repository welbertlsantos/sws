﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SWS.Excecao
{
    class BancoException :System.Exception
    {
        public static String msg1 = "Não é possível gravar o Banco. Já existe um banco com esse nome.";
        public static String msg2 = "Não é possível gravar o Banco. Já existe um banco com esse código.";

        public BancoException() : base () {}
        public BancoException(String message) : base(message) {}
        public BancoException(string message, System.Exception inner) : base(message, inner) { }

        // A constructor é necessário para serializacao quando uma
        // exceção é propagada de um servidor remoto para um cliente. 

        protected BancoException(System.Runtime.Serialization.SerializationInfo info,
            System.Runtime.Serialization.StreamingContext context) { }

    }
}
