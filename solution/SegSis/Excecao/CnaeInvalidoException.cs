﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SWS.Excecao
{
    class CnaeInvalidoException :  System.Exception
    {
        public static String msg = "CNAE inválido (99.99-9/99)";
        public static String msg1 = " Só pode incluir no máximo 5 cnaes!";
        
        public CnaeInvalidoException() : base() { }
        public CnaeInvalidoException(string message) : base(message) { }
        public CnaeInvalidoException(string message, System.Exception inner) : base(message, inner) { }

        // A constructor é necessário para serializacao quando uma
        // exceção é propagada de um servidor remoto para um cliente. 
        protected CnaeInvalidoException(System.Runtime.Serialization.SerializationInfo info,
            System.Runtime.Serialization.StreamingContext context) { }
    }
}
