﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SWS.Excecao
{
    class SalaException : System.Exception
    {
        public static String msg1 = "Sala já existente.";
        public static String msg2 = "Sala desativada com sucesso";
                
        public SalaException() : base () {}
        public SalaException(String message) : base(message) {}
        public SalaException(string message, System.Exception inner) : base(message, inner) { }

        // A constructor é necessário para serializacao quando uma
        // exceção é propagada de um servidor remoto para um cliente. 

        protected SalaException(System.Runtime.Serialization.SerializationInfo info,
            System.Runtime.Serialization.StreamingContext context) { }
    }
}
