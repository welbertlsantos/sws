﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SWS.Excecao
{
    class DescricaoGheJaCadastradaException : System.Exception
    {
        public static String msg = "Descricao do GHE já utilizada";
        
        public DescricaoGheJaCadastradaException() : base() { }
        public DescricaoGheJaCadastradaException(string message) : base(message) { }
        public DescricaoGheJaCadastradaException(string message, System.Exception inner) : base(message, inner) { }

        // A constructor é necessário para serializacao quando uma
        // exceção é propagada de um servidor remoto para um cliente. 
        protected DescricaoGheJaCadastradaException(System.Runtime.Serialization.SerializationInfo info,
            System.Runtime.Serialization.StreamingContext context) { }
    }
}
