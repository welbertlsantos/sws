﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SWS.Excecao
{
    class ProdutoExecption : System.Exception
    {
        public static String msg = "Não é possível incluir o produto. Descrição já utilizada";
        public static String msg1 = "Não é possível alterar o produto. Descrição já utilizada";
        public static String msg2 = "Produto exclusivo do sistema e não pode ser alterado";
        public static String msg3 = "Não é possível alterar o produto. Exclusivo do Sistema" + System.Environment.NewLine +
            "Para alterar o preço use a tela de alteração de precos";

        public static String msg4 = "Não existe o produto no Contrato desse cliente." + System.Environment.NewLine +
            "Solicite ao Financeiro a inclusão desse produto no contrato";
                

        public ProdutoExecption() : base () {}
        public ProdutoExecption(String message) : base(message) {}
        public ProdutoExecption(string message, System.Exception inner) : base(message, inner) { }

        // A constructor é necessário para serializacao quando uma
        // exceção é propagada de um servidor remoto para um cliente. 

        protected ProdutoExecption(System.Runtime.Serialization.SerializationInfo info,
            System.Runtime.Serialization.StreamingContext context) { }
    }
}
