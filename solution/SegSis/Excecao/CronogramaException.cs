﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SWS.Excecao
{
    class CronogramaException :System.Exception
    {
        public static String msg = "Descrição é obrigatoria.";
                
        public CronogramaException() : base () {}
        public CronogramaException(String message) : base(message) {}
        public CronogramaException(string message, System.Exception inner) : base(message, inner) { }

        // A constructor é necessário para serializacao quando uma
        // exceção é propagada de um servidor remoto para um cliente. 

        protected CronogramaException(System.Runtime.Serialization.SerializationInfo info,
            System.Runtime.Serialization.StreamingContext context) { }
    }
}
