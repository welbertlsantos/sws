﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SWS.Excecao
{
    class MedicoException: System.Exception
    {
        public static String msg2 = "Médico já utilizado no sistema. " + System.Environment.NewLine + "Alteração não será possível. ";
        public static String msg3 = "Usuário é médico. É obrigatório vincular o médico coordenador ";
        public static String msg1 = "Não é possível cadastrar o médico." + System.Environment.NewLine + "O CRM já foi cadastrado em outro médico";
        public static String msg4 = "Não é possível alterar o médico." + System.Environment.NewLine + "O CRM já foi cadastrado em outro médico";


        public MedicoException() : base() { }
        public MedicoException(String message) : base(message) { }
        public MedicoException(string message, System.Exception inner) : base(message, inner) { }

        // A constructor é necessário para serializacao quando uma
        // exceção é propagada de um servidor remoto para um cliente. 

        protected MedicoException(System.Runtime.Serialization.SerializationInfo info,
            System.Runtime.Serialization.StreamingContext context) { }
    }
}
