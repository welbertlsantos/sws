﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SWS.Excecao
{
    class UsuarioJaCadastradoException : System.Exception
    {
        public static String msg2 = "Não é possível gravar usuário. Login já utilizado.";
        
        public UsuarioJaCadastradoException() : base() { }
        public UsuarioJaCadastradoException(string message) : base(message) { }
        public UsuarioJaCadastradoException(string message, System.Exception inner) : base(message, inner) { }

        // A constructor é necessário para serializacao quando uma
        // exceção é propagada de um servidor remoto para um cliente. 
        protected UsuarioJaCadastradoException(System.Runtime.Serialization.SerializationInfo info,
            System.Runtime.Serialization.StreamingContext context) { }
    }
}
