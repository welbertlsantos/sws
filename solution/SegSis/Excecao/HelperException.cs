﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SWS.Excecao
{
    class HelperException : System.Exception
    {
        public static String msg = "Valor máximo permitido 99.999.999,99!";
        
        public HelperException() : base () {}
        public HelperException(String message) : base(message) {}
        public HelperException(string message, System.Exception inner) : base(message, inner) { }

        // A constructor é necessário para serializacao quando uma
        // exceção é propagada de um servidor remoto para um cliente. 

        protected HelperException(System.Runtime.Serialization.SerializationInfo info,
            System.Runtime.Serialization.StreamingContext context) { }
    }
}
