﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SWS.Excecao
{
    class ContratoException :System.Exception
    {
        public static String msg = "Exclusão não permitida. Esse exame já foi usado em um movimento.";
        public static String msg1 = "Exclusão não permitida. Esse produto já foi usado em um movimento.";
        public static String msg2 = "Contrato do cliente não cadastrado ou fora de vigência." + System.Environment.NewLine + 
            "Solicite ao Financeiro a inclusão do contrato.";

        public static String msg3 = "Produto não cadastrado no contrato do cliente." + System.Environment.NewLine +
            "Solicite ao Financeiro a inclusão desse produto.";

        public static String msg4 = " não foi cadastrado no contrato do cliente. Solicite ao Financeiro a sua inclusão";

        public static String msg5 = "Exame não cadastrado no contrato do cliente." + System.Environment.NewLine +
            "Solicite ao Financeiro a inclusão do Exame: ";

        public static String msg6 = "O contrato Selecionado não é o último contrato vigente para o cliente. ";
        
        public static String msg7 = "Contrato não permitido para impressão. ";

        public static String msg8 = "Contrato do cliente com o prestador não cadastrado ou fora de vigência." + System.Environment.NewLine +
            "Solicite ao Financeiro a inclusão do contrato.";

        public static String msg9 = "Já existe um contrato ou proposta elaborado hoje para o cliente.";

        public ContratoException() : base () {}
        public ContratoException(String message) : base(message) {}
        public ContratoException(string message, System.Exception inner) : base(message, inner) { }

        // A constructor é necessário para serializacao quando uma
        // exceção é propagada de um servidor remoto para um cliente. 

        protected ContratoException(System.Runtime.Serialization.SerializationInfo info,
            System.Runtime.Serialization.StreamingContext context) { }
    }
}
