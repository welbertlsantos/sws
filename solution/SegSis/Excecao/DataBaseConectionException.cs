﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SWS.Excecao
{
    [Serializable()]
    class DataBaseConectionException : System.Exception
    {
        public static String msg = "Problemas de conexão com Banco de dados. Por favor, entre em contato com o suporte do sistema e informe o erro: "  ;

        public DataBaseConectionException() : base() { }
        public DataBaseConectionException(string message) : base(message) { }
        public DataBaseConectionException(string message, System.Exception inner) : base(message, inner) { }

        // A constructor é necessário para serializacao quando uma
        // exceção é propagada de um servidor remoto para um cliente.
        protected DataBaseConectionException(System.Runtime.Serialization.SerializationInfo info,
            System.Runtime.Serialization.StreamingContext context) { }
    }
}
