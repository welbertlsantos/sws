﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SWS.Excecao
{
    class PlanoException: System.Exception
    {
        public static String msg1 = "A parcela 1 deverá ser obrigatóriamente preenchida." +
                        System.Environment.NewLine + "Em caso de prazo a vista, preencher com 0." ;

        public static String msg2 = "Reveja os valores digitados nas parcelas." +
            System.Environment.NewLine + "O valor anterior não pode ser maior ou igual ou posterior. ";
        
        public static String msg3 = "Reveja os valores digitados";
        
        public PlanoException() : base () {}
        public PlanoException(String message) : base(message) {}
        public PlanoException(string message, System.Exception inner) : base(message, inner) { }

        // A constructor é necessário para serializacao quando uma
        // exceção é propagada de um servidor remoto para um cliente. 

        protected PlanoException(System.Runtime.Serialization.SerializationInfo info,
            System.Runtime.Serialization.StreamingContext context) { }
    
    }
}
