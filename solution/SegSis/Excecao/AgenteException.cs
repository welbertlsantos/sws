﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SWS.Excecao
{
    class AgenteException : System.Exception
    {
        public static String msg1 = "Selecione um agente!";
        public static String msg2 = "Não é possivel incluir Agente. Essa descrição já está cadastrada.";
        
        public AgenteException() : base () {}
        public AgenteException(String message) : base(message) {}
        public AgenteException(string message, System.Exception inner) : base(message, inner) { }

        // A constructor é necessário para serializacao quando uma
        // exceção é propagada de um servidor remoto para um cliente. 

        protected AgenteException(System.Runtime.Serialization.SerializationInfo info,
            System.Runtime.Serialization.StreamingContext context) { }
    }
}
