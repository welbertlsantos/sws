﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SWS.Excecao
{
    class cnaeJaUtilizadoException : System.Exception
    {
        public static String msg = "Exclusão não permitida: CNAE Já Utilizado";
        
        public cnaeJaUtilizadoException() : base() { }
        public cnaeJaUtilizadoException(string message) : base(message) { }
        public cnaeJaUtilizadoException(string message, System.Exception inner) : base(message, inner) { }

        // A constructor é necessário para serializacao quando uma
        // exceção é propagada de um servidor remoto para um cliente. 
        protected cnaeJaUtilizadoException(System.Runtime.Serialization.SerializationInfo info,
            System.Runtime.Serialization.StreamingContext context) { }
    }
}
