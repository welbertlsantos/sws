﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SWS.Excecao
{
    class EmpresaException :System.Exception
    {
        public static String msg1 = "Não é possível gravar a empresa. O CNPJ já foi utilizado.";
        public static String msg2 = "Não é possível alterar a empresa. Empresa já excluída do sistema.";
        public static String msg3 = "Empresá já excluída do sistema.";
        public static String msg4 = "É obrigatório o cadastro da empresa do usuário.";

        
        public EmpresaException() : base () {}
        public EmpresaException(String message) : base(message) {}
        public EmpresaException(string message, System.Exception inner) : base(message, inner) { }

        // A constructor é necessário para serializacao quando uma
        // exceção é propagada de um servidor remoto para um cliente. 

        protected EmpresaException(System.Runtime.Serialization.SerializationInfo info,
            System.Runtime.Serialization.StreamingContext context) { }
    }
}
