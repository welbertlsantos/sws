﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SWS.Excecao
{
    class AtividadeJaCadastradaException : System.Exception
    {
        public AtividadeJaCadastradaException() : base() { }
        public AtividadeJaCadastradaException(string message) : base(message) { }
        public AtividadeJaCadastradaException(string message, System.Exception inner) : base(message, inner) { }

        // A constructor é necessário para serializacao quando uma
        // exceção é propagada de um servidor remoto para um cliente. 
        protected AtividadeJaCadastradaException(System.Runtime.Serialization.SerializationInfo info,
            System.Runtime.Serialization.StreamingContext context) { }
    }
}