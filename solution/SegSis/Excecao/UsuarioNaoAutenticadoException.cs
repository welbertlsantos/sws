﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SWS.Excecao
{
    [Serializable()]
    public class UsuarioNaoAutenticadoException : System.Exception
    {
        public UsuarioNaoAutenticadoException() : base() { }
        public UsuarioNaoAutenticadoException(string message) : base(message) { }
        public UsuarioNaoAutenticadoException(string message, System.Exception inner) : base(message, inner) { }

        // A constructor é necessário para serializacao quando uma
        // exceção é propagada de um servidor remoto para um cliente. 
        protected UsuarioNaoAutenticadoException(System.Runtime.Serialization.SerializationInfo info,
            System.Runtime.Serialization.StreamingContext context) { }
    }
}
