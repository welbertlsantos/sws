﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SWS.Excecao
{
    class GheFonteAgenteEpiException : System.Exception
    {
            public static String msg = "Não pode existir um EPI com a mesma classificação!";
            
            public GheFonteAgenteEpiException() : base () {}
            public GheFonteAgenteEpiException(String message) : base(message) {}
            public GheFonteAgenteEpiException(string message, System.Exception inner) : base(message, inner) { }

         // A constructor é necessário para serializacao quando uma
        // exceção é propagada de um servidor remoto para um cliente. 

         protected GheFonteAgenteEpiException(System.Runtime.Serialization.SerializationInfo info,
            System.Runtime.Serialization.StreamingContext context) { }
    }
   
}
