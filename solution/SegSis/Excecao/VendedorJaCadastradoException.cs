﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SWS.Excecao
{
    class VendedorJaCadastradoException : System.Exception
    {
        public VendedorJaCadastradoException() : base() { }
        public VendedorJaCadastradoException(string message) : base(message) { }
        public VendedorJaCadastradoException(string message, System.Exception inner) : base(message, inner) { }

        // A constructor é necessário para serializacao quando uma
        // exceção é propagada de um servidor remoto para um cliente. 
        
        protected VendedorJaCadastradoException(System.Runtime.Serialization.SerializationInfo info,
            System.Runtime.Serialization.StreamingContext context) { }
    
    }
}
