﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SWS.Excecao
{
    class ExameException : System.Exception
    {
        public static String msg = "Não é possível cadastrar exame. Descrição já utilizada!";
        public static String msg1 = "Não é possível alterar exame. Descrição já utilizada!";
        public static String msg2 = " Exame já utilizado no sistema. Alteração não será possível "; 
        


        public ExameException() : base() { }
        public ExameException(String message) : base(message) { }
        public ExameException(string message, System.Exception inner) : base(message, inner) { }

        // A constructor é necessário para serializacao quando uma
        // exceção é propagada de um servidor remoto para um cliente. 

        protected ExameException(System.Runtime.Serialization.SerializationInfo info,
            System.Runtime.Serialization.StreamingContext context) { }
    }
}

