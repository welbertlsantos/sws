﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SWS.Excecao
{
    class FuncaoException : System.Exception
    {
        public static String msg = "A função não pode ser alterada.";
        public static String msg1 = "Ela já foi usada em um cliente";
        
        public FuncaoException() : base () {}
        public FuncaoException(String message) : base(message) {}
        public FuncaoException(string message, System.Exception inner) : base(message, inner) { }

        // A constructor é necessário para serializacao quando uma
        // exceção é propagada de um servidor remoto para um cliente. 

        protected FuncaoException(System.Runtime.Serialization.SerializationInfo info,
            System.Runtime.Serialization.StreamingContext context) { }
    }
    
}
