﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SWS.Excecao
{
    class ArquivoException :System.Exception
    {
        public static String msg1 = "Arquivo excede o tamanho máximo de 5 MegaBytes!";

        public ArquivoException() : base () {}
        public ArquivoException(String message) : base(message) {}
        public ArquivoException(string message, System.Exception inner) : base(message, inner) { }

        // A constructor é necessário para serializacao quando uma
        // exceção é propagada de um servidor remoto para um cliente. 

        protected ArquivoException(System.Runtime.Serialization.SerializationInfo info,
            System.Runtime.Serialization.StreamingContext context) { }
    }
}
