﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SWS.Excecao
{
    class GradacaoException : System.Exception
    {
        public static String msg = "Selecione uma exposição!";
        public static String msg2 = "Selecione um efeito!";
        
        public GradacaoException() : base () {}
        public GradacaoException(String message) : base(message) {}
        public GradacaoException(string message, System.Exception inner) : base(message, inner) { }

        // A constructor é necessário para serializacao quando uma
        // exceção é propagada de um servidor remoto para um cliente. 

        protected GradacaoException(System.Runtime.Serialization.SerializationInfo info,
            System.Runtime.Serialization.StreamingContext context) { }
    }
}
