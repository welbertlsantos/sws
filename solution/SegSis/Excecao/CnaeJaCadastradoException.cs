﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SWS.Excecao
{
    class CnaeJaCadastradoException : System.Exception
    {
        public static String msg = "CNAE já cadastrado.";
        
        public CnaeJaCadastradoException() : base() { }
        public CnaeJaCadastradoException(string message) : base(message) { }
        public CnaeJaCadastradoException(string message, System.Exception inner) : base(message, inner) { }

        // A constructor é necessário para serializacao quando uma
        // exceção é propagada de um servidor remoto para um cliente. 
        protected CnaeJaCadastradoException(System.Runtime.Serialization.SerializationInfo info,
            System.Runtime.Serialization.StreamingContext context) { }
    
    }
}
