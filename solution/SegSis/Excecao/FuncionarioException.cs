﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SWS.Excecao
{
    class FuncionarioException : System.Exception
    {

        public static String msg1 = "CPF Já existente";
        public static String msg2 = "O Nome do Funcionário é obrigatório";
        public static String msg3 = "Nenhum dado foi informado para realizar a pesquisa.";
        public static String msg4 = "É obrigatório o funcionário estar relacionado a uma empresa.";
        public static String msg5 = "RG já existente no cadastro";
        public static String msg6 = "Verificar o CEP digitado";
        public static String msg7 = "Altura deve ser no formato X,XX";
        public static String msg8 = "Já existe um funcionário cadastrado com o mesmo nome e RG.";
        
        public FuncionarioException() : base() { }
        public FuncionarioException(String message) : base(message) { }
        public FuncionarioException(string message, System.Exception inner) : base(message, inner) { }

        // A constructor é necessário para serializacao quando uma
        // exceção é propagada de um servidor remoto para um cliente. 

        protected FuncionarioException(System.Runtime.Serialization.SerializationInfo info,
            System.Runtime.Serialization.StreamingContext context) { }

    }
}


