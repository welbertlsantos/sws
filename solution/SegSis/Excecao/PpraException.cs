﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SWS.Excecao
{
    class PpraException : System.Exception
    {
        public static String msg = "  Não é possivel criar PPRA sem cliente!";
        public static String msg1 = " Somente é possível alterar um PPRA em construção!";
        public static String msg2 = " Somente é possível criar revisão de um PPRA finalizado!";
        public static String msg3 = " Este PPRA não é a última versão. Só é possível criar revisão da última versão!";
        public static String msg4 = " Não é possével excluir um estudo que já esteja fechado ";
        public static String msg5 = " Não é possível finalizar um PPRA que não esteja Aguardando finalização ";
        public static String msg6 = " Numero total de expostos do ghe difere do numero de funcionários do cliente";
        public static String msg7 = " Não pode existir um PPRA sem um GHE !";
        public static String msg8 = " Não pode existir um GHE sem pelo menos um setor! ";
        public static String msg9 = " Não pode existir Setor sem pelo menos uma função !";
        public static String msg10 = "Não pode existir cronograma sem pelo menos uma atividade ";
        public static String msg11 = "Não pode existir PPRA sem cronograma ";
        public static String msg12 = "É obrigatório ter um elaboradora para o PPRA ";
        public static String msg13 = "Não pode existir função sem atividade.";
        

        
        public PpraException() : base () {}
        public PpraException(String message) : base(message) {}
        public PpraException(string message, System.Exception inner) : base(message, inner) { }

        // A constructor é necessário para serializacao quando uma
        // exceção é propagada de um servidor remoto para um cliente. 

        protected PpraException(System.Runtime.Serialization.SerializationInfo info,
            System.Runtime.Serialization.StreamingContext context) { }
    }
}
