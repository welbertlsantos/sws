﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SWS.Excecao
{
    class AsoException : System.Exception
    {
        public static String msg1 = "É obrigatório selecionar um período.";
        //public static String msg2 = "Não pode haver um aso sem um médico examinador.";
        public static String msg3 = "É obrigatorio definir a periodicidade do Atendimento.";
        public static String msg4 = "É obrigatório selecionar um funcionário.";
        public static String msg5 = "Não é possível cancelar o Atendimento. O Atendimento já está cancelado ou finalizado.";
        public static String msg6 = "Não é possível imprimir o Atendimento. O Atendimento já está cancelado.";
        public static String msg7 = "Não existe coordenador ativo no momento no PCMSO informado. Gostaria de continar com o aso mesmo assim?";
        public static String msg8 = "É necessário alterar o cadastro do cliente.";
        public static String msg9 = "Não é possível finalizar um Atendimento que não esteja em construçao.";
        public static String msg10 = "Não é possível finalizar o Atendimento. Existe(m) exame(s) pendente(s).";

        public static String msg11 = "Não é possível cancelar o Atendimento. O Atendimento já está faturado." + System.Environment.NewLine +
            "Solicite ao financeiro para estornar o movimento";

        public static String msg12 = "Não é possível cancelar o Atendimento. " + System.Environment.NewLine +
            "Existe(m) exame(s) em atendimento.";

        public static String msg13 = "Não é possível incluir exame. Atendimento já finalizado ou cancelado.";
        public static String msg14 = "Não é possível iniciar o atendimento. Atendimento já finalizado ou cancelado.";
        public static String msg15 = "Não é possível alterar o atendimento. Atendimento já cancelado ou finalizado.";
        public static String msg16 = "Já existe um atendimento realizado hoje para esse mesmo funcionário, para mesma empresa na mesma periodicidade.";
        
        public AsoException() : base () {}
        public AsoException(String message) : base(message) {}
        public AsoException(string message, System.Exception inner) : base(message, inner) { }

        // A constructor é necessário para serializacao quando uma
        // exceção é propagada de um servidor remoto para um cliente. 

        protected AsoException(System.Runtime.Serialization.SerializationInfo info,
            System.Runtime.Serialization.StreamingContext context) { }

    }
}
