﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SWS.Excecao
{
    class AnexoException :System.Exception
    {

        public static String msg = "É obrigatório um conteúdo para o anexo!";
        public static String msg1 = "Conteúdo já cadastrado";

        public AnexoException() : base () {}
        public AnexoException(String message) : base(message) {}
        public AnexoException(string message, System.Exception inner) : base(message, inner) { }

        // A constructor é necessário para serializacao quando uma
        // exceção é propagada de um servidor remoto para um cliente. 

        protected AnexoException(System.Runtime.Serialization.SerializationInfo info,
            System.Runtime.Serialization.StreamingContext context) { }
    }
}
