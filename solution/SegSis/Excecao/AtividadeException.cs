﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SWS.Excecao
{
    class AtividadeException : System.Exception
    {
        public static String msg1 = "O ano deve ser no formato AAAA!";
        public static String msg2 = "Atividade é obrigatória!";

        public AtividadeException() : base () {}
        public AtividadeException(String message) : base(message) {}
        public AtividadeException(string message, System.Exception inner) : base(message, inner) { }

         // A constructor é necessário para serializacao quando uma
        // exceção é propagada de um servidor remoto para um cliente. 

        protected AtividadeException(System.Runtime.Serialization.SerializationInfo info,
            System.Runtime.Serialization.StreamingContext context) { }
    }
}
