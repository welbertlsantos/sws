﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SWS.Excecao
{
    class FonteException : System.Exception
    {
        public static String msg = "Selecione uma fonte!";
        public static String msg1 = "Não é possível alterar a fonte.";
        public static String msg2 = "A fonte já foi usada em um estudo.";

        public FonteException() : base() { }
        public FonteException(String message) : base(message) { }
        public FonteException(string message, System.Exception inner) : base(message, inner) { }

        // A constructor é necessário para serializacao quando uma
        // exceção é propagada de um servidor remoto para um cliente. 

        protected FonteException(System.Runtime.Serialization.SerializationInfo info,
            System.Runtime.Serialization.StreamingContext context) { }
    }
}

