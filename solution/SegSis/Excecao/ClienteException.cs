﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SWS.Excecao
{
    class ClienteException : System.Exception
    {
        public static String msg1 = "Selecione um cliente para o PPRA!";
        public static String msg2 = "Selecione um cliente";
        public static String msg3 = "O cliente deve ter pelo menos uma função cadastrada";
        public static String msg4 = "O cliente deve ter pelo menos um CNAE cadastrado. Favor alterar o cadastro do cliente.";
        public static String msg5 = "O cliente contratante deve ter pelo menos um CNAE cadastrado. Favor alterar o cadastro do cliente.";
        public static String msg6 = "Não é possível cadastrar o prestador. Já existe um prestador cadastrado com esse CNPJ";
        public static String msg7 = "Não é possível alterar o prestador. Já existe um prestador cadastrado com esse CNPJ";
        public static String msg8 = "Nao e possivel incluir o cliente. Ja existe um cliente com esse CNPJ";

        public ClienteException() : base () {}
        public ClienteException(String message) : base(message) {}
        public ClienteException(string message, System.Exception inner) : base(message, inner) { }

         // A constructor é necessário para serializacao quando uma
        // exceção é propagada de um servidor remoto para um cliente. 

        protected ClienteException(System.Runtime.Serialization.SerializationInfo info,
            System.Runtime.Serialization.StreamingContext context) { }

    }
}




