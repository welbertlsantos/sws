﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SWS.Excecao
{
    class NotaFiscalException : System.Exception
    {
        public static String msg1 = "A nota não pode ser cancelada." + System.Environment.NewLine +
            "Existem cobranças baixadas. Efetue o estorno e tente novamente.";
        
        public NotaFiscalException() : base () {}
        public NotaFiscalException(String message) : base(message) {}
        public NotaFiscalException(string message, System.Exception inner) : base(message, inner) { }

        // A constructor é necessário para serializacao quando uma
        // exceção é propagada de um servidor remoto para um cliente. 

        protected NotaFiscalException(System.Runtime.Serialization.SerializationInfo info,
            System.Runtime.Serialization.StreamingContext context) { }
    }
    
}
