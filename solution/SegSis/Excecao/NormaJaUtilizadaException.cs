﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SWS.Excecao
{
    class NormaJaUtilizadaException : System.Exception
    {
        public static String msg = "Norma não pode ser alterada. Já foi utilizada em um PPRA finalizado.";
        public static String msg1 = "Norma não pode ser excluída. Já foi utilizada em um GHE.";
        
        public NormaJaUtilizadaException() : base() { }
        public NormaJaUtilizadaException(string message) : base(message) { }
        public NormaJaUtilizadaException(string message, System.Exception inner) : base(message, inner) { }

        // A constructor é necessário para serializacao quando uma
        // exceção é propagada de um servidor remoto para um cliente. 
        
        protected NormaJaUtilizadaException(System.Runtime.Serialization.SerializationInfo info,
            System.Runtime.Serialization.StreamingContext context) { }
    }
}
