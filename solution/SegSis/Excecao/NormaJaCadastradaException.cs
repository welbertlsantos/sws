﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SWS.Excecao
{
    class NormaJaCadastradaException : System.Exception
    {
        public static String msg = "Norma já cadastrada";
        public static String msg1 = "Só é permitido no máximo 4 notas em cada Ghe";
        public static String msg2 = "Selecione uma linha!";
        
        public NormaJaCadastradaException() : base() { }
        public NormaJaCadastradaException(string message) : base(message) { }
        public NormaJaCadastradaException(string message, System.Exception inner) : base(message, inner) { }

        // A constructor é necessário para serializacao quando uma
        // exceção é propagada de um servidor remoto para um cliente. 
        protected NormaJaCadastradaException(System.Runtime.Serialization.SerializationInfo info,
            System.Runtime.Serialization.StreamingContext context) { }
    }
}

