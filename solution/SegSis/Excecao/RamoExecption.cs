﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SWS.Excecao
{
    class RamoExecption : System.Exception
    {
        public static String msg1 = "Descrição já utilizada para o Ramo de Atividade.";
        
        public RamoExecption() : base () {}
        public RamoExecption(String message) : base(message) {}
        public RamoExecption(string message, System.Exception inner) : base(message, inner) { }

        // A constructor é necessário para serializacao quando uma
        // exceção é propagada de um servidor remoto para um cliente. 

        protected RamoExecption(System.Runtime.Serialization.SerializationInfo info,
            System.Runtime.Serialization.StreamingContext context) { }
    }
}
