﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SWS.Excecao
{
    class HospitalException : System.Exception
    {
        public static String msg = "Não é possível cadastrar Hospital. Descrição já utilizada!";
        public static String msg1 = "Não é possível alterar Hospital. Descrição já utilizada!";
        public static String msg2 = "Hospital já utilizado no sistema." + System.Environment.NewLine + "Alteração não será possível. ";


        public HospitalException() : base() { }
        public HospitalException(String message) : base(message) { }
        public HospitalException(string message, System.Exception inner) : base(message, inner) { }

        // A constructor é necessário para serializacao quando uma
        // exceção é propagada de um servidor remoto para um cliente. 

        protected HospitalException(System.Runtime.Serialization.SerializationInfo info,
            System.Runtime.Serialization.StreamingContext context) { }
    }
}
