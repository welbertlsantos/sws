﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SWS.Excecao
{
    class EncerrarProcessamentoSemErroException : System.Exception
    {
        public EncerrarProcessamentoSemErroException() : base () {}
        public EncerrarProcessamentoSemErroException(String message) : base(message) {}
        public EncerrarProcessamentoSemErroException(string message, System.Exception inner) : base(message, inner) { }

        // A constructor é necessário para serializacao quando uma
        // exceção é propagada de um servidor remoto para um cliente. 

        protected EncerrarProcessamentoSemErroException(System.Runtime.Serialization.SerializationInfo info,
            System.Runtime.Serialization.StreamingContext context) { }
    }
}
