﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SWS.Excecao
{
    class EsocialException : Exception
    {
        public static String msg1 = "É obrigatório selecionar um período.";
        public static String msg2 = "É obrigatório selecionar um cliente.";
        public static String msg3 = "É obrigatório selecionar um atendimentos.";

        public EsocialException() : base() { }
        public EsocialException(String message) : base(message) { }
        public EsocialException(string message, System.Exception inner) : base(message, inner) { }

        protected EsocialException(System.Runtime.Serialization.SerializationInfo info,
            System.Runtime.Serialization.StreamingContext context) { }

    }
}
