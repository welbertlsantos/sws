﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SWS.Excecao
{
    class PcmsoException : System.Exception
    {
        public static String msg1 = "A data final não pode ser MENOR que a data inicial!";
        public static String msg2 = "A data de criação não pode ser maior que a data de vencimento";
        public static String msg3 = "Somente é possível alterar um PCMSO que esteja em construção!";
        public static String msg4 = "É obrigatório ter um elaborador para o pcmso";
        public static String msg5 = "Não pode existir um PCMSO sem cronograma";
        public static String msg6 = "Não pode existir cronograma sem atividade";
        public static String msg7 = "Não é possível finalizar um PCMSO que não esteja aguardando Finalização!";
        public static String msg8 = "Não é possível excluir um PCMSO que esteja finalzado!";
        public static String msg9 = " Somente é possível criar revisão de um PCMSO finalizado!";
        public static String msg10 = " Este PCMSO não é a última versão. Só é possível criar revisão da última versão!";
        public static String msg11 = " Não é possivel criar PCMSO sem cliente!";



        public PcmsoException() : base () {}
        public PcmsoException(String message) : base(message) {}
        public PcmsoException(string message, System.Exception inner) : base(message, inner) { }

        // A constructor é necessário para serializacao quando uma
        // exceção é propagada de um servidor remoto para um cliente. 

        protected PcmsoException(System.Runtime.Serialization.SerializationInfo info,
            System.Runtime.Serialization.StreamingContext context) { }
        
    }
}
