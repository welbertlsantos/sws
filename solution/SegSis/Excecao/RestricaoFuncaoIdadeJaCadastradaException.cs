﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SWS.Excecao
{
    class RestricaoFuncaoIdadeJaCadastradaException : System.Exception
    {
        public static String msg = "A Função já se encontra na lista de exceções por idade.";

        public RestricaoFuncaoIdadeJaCadastradaException() : base() { }
        public RestricaoFuncaoIdadeJaCadastradaException(String message) : base(message) { }
        public RestricaoFuncaoIdadeJaCadastradaException(string message, System.Exception inner) : base(message, inner) { }

        // A constructor é necessário para serializacao quando uma
        // exceção é propagada de um servidor remoto para um cliente. 

        protected RestricaoFuncaoIdadeJaCadastradaException(System.Runtime.Serialization.SerializationInfo info,
            System.Runtime.Serialization.StreamingContext context) { }
    }
}