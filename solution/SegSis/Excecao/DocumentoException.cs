﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SWS.Excecao
{
    class DocumentoException : System.Exception
    {
        public static String msg1 = "Documento já cadastrado";
        public static String msg2 = "Já existe um documento cadastrado com esse nome.";
        public static String msg3 = "Documento já utilizado em um protocolo.";

        public DocumentoException() : base () {}
        public DocumentoException(String message) : base(message) {}
        public DocumentoException(string message, System.Exception inner) : base(message, inner) { }

        protected DocumentoException(System.Runtime.Serialization.SerializationInfo info,
            System.Runtime.Serialization.StreamingContext context) { }
    }
}
