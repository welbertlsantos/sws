﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SWS.Excecao
{
    class GheFonteAgenteException : System.Exception
    {
        public static String msg = "Você deve selecionar um Risco";

        public GheFonteAgenteException() : base() { }
        public GheFonteAgenteException(String message) : base(message) { }
        public GheFonteAgenteException(string message, System.Exception inner) : base(message, inner) { }

        // A constructor é necessário para serializacao quando uma
        // exceção é propagada de um servidor remoto para um cliente. 

        protected GheFonteAgenteException(System.Runtime.Serialization.SerializationInfo info,
            System.Runtime.Serialization.StreamingContext context) { }

    }
}

