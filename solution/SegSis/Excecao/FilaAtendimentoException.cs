﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SWS.Excecao
{
    class FilaAtendimentoException: System.Exception
    {
        public static String msg1 = "O item está em atendimento no momento. Atualize sua tela";
        public static String msg2 = "O item já foi finalizado. Atualize sua tela";
        public static String msg3 = "O item já foi cancelado. Atualize sua tela";
        public static String msg4 = "Não é possível cancelar o item porque o Atendimento já foi finalizado.";
        public static string msg5 = "Você deverá marcar algum exame para confirmar.";

        
        public FilaAtendimentoException() : base() { }
        public FilaAtendimentoException(String message) : base(message) { }
        public FilaAtendimentoException(string message, System.Exception inner) : base(message, inner) { }

        // A constructor é necessário para serializacao quando uma
        // exceção é propagada de um servidor remoto para um cliente. 

        protected FilaAtendimentoException(System.Runtime.Serialization.SerializationInfo info,
            System.Runtime.Serialization.StreamingContext context) { }
    }
}
