﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SWS.Excecao
{
    class MaterialHospitalarException: System.Exception
    {
        public static String msg = "Não é possível cadastrar Material Hospitalar. Descrição já utilizada.";
        public static String msg1 = "Não é possível alterar Material Hospitalar. Descrição já utilizada.";
        public static String msg2 = "Material Hospitalar já utilizado no sistema." + System.Environment.NewLine + "Alteração não será possível. "; 
        public static String msg3 = "A Quantidade no material hospitalar não pode ser igual a zero. REVEJA AS QUANTIDADES!";


        public MaterialHospitalarException() : base() { }
        public MaterialHospitalarException(String message) : base(message) { }
        public MaterialHospitalarException(string message, System.Exception inner) : base(message, inner) { }

        // A constructor é necessário para serializacao quando uma
        // exceção é propagada de um servidor remoto para um cliente. 

        protected MaterialHospitalarException(System.Runtime.Serialization.SerializationInfo info,
            System.Runtime.Serialization.StreamingContext context) { }
    }
}
