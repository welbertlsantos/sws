﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SWS.Entidade;
using Npgsql;
using NpgsqlTypes;
using SWS.Helper;
using SWS.Excecao;
using SWS.Dao;
using SWS.IDao;
using System.Data;
using System.Windows.Forms;
using System.Runtime.CompilerServices;
using SWS.View.Resources;

namespace SWS.Facade
{
    class FinanceiroFacade
    {
        public static FinanceiroFacade financeiroFacade = null;

        private FinanceiroFacade() { }

        public static FinanceiroFacade getInstance()
        {
            if (financeiroFacade == null)
            {
                financeiroFacade = new FinanceiroFacade();
            }

            return financeiroFacade;
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public Contrato findContratoSistemaByCliente(Cliente cliente)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findContratoByCliente");

            NpgsqlConnection dbConnection = FabricaConexao.getConexao();
            IContratoDao contradoDao = FabricaDao.getContratoDao();
            IEmpresaDao empresaDao = FabricaDao.getEmpresaDao();
            Contrato contrato = null;
            try
            {
                contrato = contradoDao.findContratoSistemaByCliente(cliente, dbConnection);
                if (contrato != null)
                    contrato.Empresa = contrato.Empresa != null ? empresaDao.findById((long)contrato.Empresa.Id, dbConnection) : null;

            }
            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findContratoByCliente", ex);
                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }
            finally
            {
                FabricaConexao.fecharConexao();
            }
            return contrato;
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public void insertContratoExame(ContratoExame contratoExame)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método insertContratoExame");

            NpgsqlConnection dbConnection = FabricaConexao.getConexao();

            IContratoExameDao contratoExameDao = FabricaDao.getContratoExameDao();

            NpgsqlTransaction transaction = dbConnection.BeginTransaction();

            try
            {
                /* trabalhar método para verificar se já existe o exame nesse contrato. 
                 * caso exista então ele será reativado */

                ContratoExame contratoExameFind = contratoExameDao.findAllByContratoAndExameAndSituacao(contratoExame.Contrato,
                    contratoExame.Exame, string.Empty, dbConnection);

                if (contratoExameFind != null)
                {
                    contratoExameDao.reativated(contratoExameFind, dbConnection);
                }
                else
                {
                    contratoExameDao.insert(contratoExame, dbConnection);
                }
                
                transaction.Commit();
                
            }
            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - insertContratoExame", ex);
                transaction.Rollback();
                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }
            finally
            {
                FabricaConexao.fecharConexao();
            }

        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public Contrato insertContrato(Contrato contrato, List<ContratoExame> exames, 
            List<ContratoProduto> produtos)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método insertContrato");

            NpgsqlConnection dbConnection = FabricaConexao.getConexao();

            IContratoDao contratoDao = FabricaDao.getContratoDao();
            IContratoExameDao contratoExameDao = FabricaDao.getContratoExameDao();
            IContratoProdutoDao contratoProdutoDao = FabricaDao.getContratoProdutoDao();
            IClientePropostaDao clienteProposta = FabricaDao.getClientePropostaDao();

            Contrato contratoNovo = null;
            ClienteProposta clientePropostaNovo = null;

            NpgsqlTransaction transaction = dbConnection.BeginTransaction();

            try
            {
                /* verificando que tipo de contrato será inserido. Proposta ou Contrato . 
                 Caso seja proposta verificar se será incluído um cliente do sistema
                 ou um cliente proposta.*/

                if (contrato.Proposta)
                {
                    if (contrato.ClienteProposta != null)
                    { 
                        clientePropostaNovo = clienteProposta.insertClienteProposta(contrato.ClienteProposta, dbConnection);
                        /* setando o novo clienteProposta no contrato */
                        contrato.ClienteProposta = clientePropostaNovo;
                        
                    }
                }

                contratoNovo = contratoDao.insert(contrato, dbConnection);
                
                /* incluindo o contratoExame */

                if (exames != null)
                {
                    foreach (ContratoExame ce in exames)
                    {
                        ce.Contrato = contratoNovo;
                        contratoExameDao.insert(ce, dbConnection);
                    }
                }

                if (produtos != null)
                {
                    foreach (ContratoProduto cp in produtos)
                    {
                        cp.Contrato = contratoNovo;
                        contratoProdutoDao.insert(cp, dbConnection);
                    }
                }
                
                transaction.Commit();
            }
            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - insertContrato", ex);
                transaction.Rollback();
                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }
            finally
            {
                FabricaConexao.fecharConexao();
            }

            return contratoNovo;

        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public HashSet<ContratoProduto> findAllProdutosByClienteInContrato(Contrato contrato, Produto produto)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findAllProdutosByClienteInContrato");

            NpgsqlConnection dbConnection = FabricaConexao.getConexao();

            IContratoProdutoDao contradoProdutoDao = FabricaDao.getContratoProdutoDao();
            
            try
            {
                return contradoProdutoDao.findAtivosByContratoAndProduto(contrato, produto, dbConnection);

            }
            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findAllProdutosByClienteInContrato", ex);
                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }
            finally
            {
                FabricaConexao.fecharConexao();
            }
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public HashSet<ContratoExame> findAllExamesByClienteInContrato(Contrato contrato)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findAllExamesByClienteInContrato");

            NpgsqlConnection dbConnection = FabricaConexao.getConexao();

            IContratoExameDao contratoExameDao = FabricaDao.getContratoExameDao();

            try
            {
                return contratoExameDao.findAtivosByContrato(contrato, dbConnection);

            }
            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findAllExamesByClienteInContrato", ex);
                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }
            finally
            {
                FabricaConexao.fecharConexao();
            }
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public void deleteContratoExame(ContratoExame contratoExame)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método deleteContratoExame");

            NpgsqlConnection dbConnection = FabricaConexao.getConexao();

            IContratoExameDao contratoExameDao = FabricaDao.getContratoExameDao();

            NpgsqlTransaction transaction = dbConnection.BeginTransaction();

            try
            {
                contratoExameDao.desativated(contratoExame, dbConnection);
                transaction.Commit();
                
            }
            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - deleteContratoExame", ex);
                transaction.Rollback();
                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }
            finally
            {
                FabricaConexao.fecharConexao();
            }

        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public DataSet findProdutoAtivoNotInContrato(Contrato contrato, Produto produto)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findProdutoAtivoNotInContrato");

            NpgsqlConnection dbConnection = FabricaConexao.getConexao();

            IProdutoDao produtoDao = FabricaDao.getProdutoDao();

            try
            {
                if (contrato != null)
                {
                    return produtoDao.findProdutoAtivoNotInContrato(contrato, produto, dbConnection);
                }
                else
                {
                    return produtoDao.findProdutoAtivoInGeneral(produto, dbConnection);
                }
            }
            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findProdutoAtivoNotInContrato", ex);
                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }
            finally
            {
                FabricaConexao.fecharConexao();
            }
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public void insertContratoProduto(ContratoProduto contratoProduto)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método insertContratoProduto");

            NpgsqlConnection dbConnection = FabricaConexao.getConexao();

            IContratoProdutoDao contratoProdutoDao = FabricaDao.getContratoProdutoDao();

            NpgsqlTransaction transaction = dbConnection.BeginTransaction();

            try
            {
                /* trabalhar método para verificar se já existe o produto nesse contrato. 
                 * caso exista então ele será reativado */

                ContratoProduto contratoProdutoFind = contratoProdutoDao.findByContratoAndProduto(contratoProduto.Contrato,
                    contratoProduto.Produto, dbConnection);

                if (contratoProdutoFind != null)
                {
                    contratoProdutoDao.reativated(contratoProdutoFind, dbConnection);
                }
                else
                {
                    contratoProdutoDao.insert(contratoProduto, dbConnection);
                }

                transaction.Commit();

            }
            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - insertContratoProduto", ex);
                transaction.Rollback();
                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }
            finally
            {
                FabricaConexao.fecharConexao();
            }

        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public void deleteContratoProduto(ContratoProduto contratoProduto)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método deleteContratoProduto");

            NpgsqlConnection dbConnection = FabricaConexao.getConexao();

            IContratoProdutoDao contratoProdutoDao = FabricaDao.getContratoProdutoDao();

            NpgsqlTransaction transaction = dbConnection.BeginTransaction();

            try
            {

                contratoProdutoDao.desativated(contratoProduto, dbConnection);
                transaction.Commit();
                
            }
            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - deleteProduto", ex);
                transaction.Rollback();
                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }
            finally
            {
                FabricaConexao.fecharConexao();
            }

        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public Contrato updateContrato(Contrato contrato)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método updateContrato");

            NpgsqlConnection dbConnection = FabricaConexao.getConexao();

            IContratoDao contratoDao = FabricaDao.getContratoDao();
            
            NpgsqlTransaction transaction = dbConnection.BeginTransaction();

            try
            {
                contrato = contratoDao.update(contrato, dbConnection);
                transaction.Commit();
                return contrato;

            }
            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - updateContrato", ex);
                transaction.Rollback();
                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }
            finally
            {
                FabricaConexao.fecharConexao();
            }

        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public Produto insertProduto(Produto produto)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método insertProduto");

            NpgsqlConnection dbConnection = FabricaConexao.getConexao();

            IProdutoDao produtoDao = FabricaDao.getProdutoDao();

            NpgsqlTransaction transaction = dbConnection.BeginTransaction();

            Produto produtoNovo = null;

            try
            {
                // validando a descricao do produto

                Produto produtoProcurado = produtoDao.findProdutoByDescricao(produto.Nome, dbConnection);

                if (produtoProcurado == null)
                {
                    produtoNovo = produtoDao.insert(produto, dbConnection);
                    transaction.Commit();
                    
                }
                else
                {
                    throw new ProdutoExecption(ProdutoExecption.msg);
                }
                              

            }
            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - insertProduto", ex);
                transaction.Rollback();
                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }
            finally
            {
                FabricaConexao.fecharConexao();
            }

            return produtoNovo;

        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public DataSet findProdutoByFilter(Produto produto)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findProdutoByFilter");

            NpgsqlConnection dbConnection = FabricaConexao.getConexao();

            IProdutoDao produtoDao = FabricaDao.getProdutoDao();
            try
            {
                return produtoDao.findProdutoByFilter(produto, dbConnection);
            
            }
            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findProdutoByFilter", ex);
                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }
            finally
            {
                FabricaConexao.fecharConexao();
            }

        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public Produto updateProduto(Produto produto)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método updateProduto");

            NpgsqlConnection dbConnection = FabricaConexao.getConexao();

            IProdutoDao produtoDao = FabricaDao.getProdutoDao();

            NpgsqlTransaction transaction = dbConnection.BeginTransaction();

            Produto produtoAlterado = null;

            try
            {
                // validando a descricao do produto se pode ser gravado.

                Produto produtoProcurado = produtoDao.findProdutoByDescricao(produto.Nome, dbConnection);

                if (produtoProcurado == null || produtoProcurado.Id == produto.Id)
                {
                    produtoAlterado = produtoDao.updateProduto(produto, dbConnection);
                    transaction.Commit();

                }
                else
                {
                    throw new ProdutoExecption(ProdutoExecption.msg);
                }


            }
            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - updateProduto", ex);
                transaction.Rollback();
                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }
            finally
            {
                FabricaConexao.fecharConexao();
            }

            return produtoAlterado;

        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public Produto findProdutoById(Int64 id)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findProdutoById");

            NpgsqlConnection dbConnection = FabricaConexao.getConexao();

            IProdutoDao produtoDao = FabricaDao.getProdutoDao();

            Produto produto = null;

            try
            {
                produto = produtoDao.findProdutoById(id, dbConnection);

            }
            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findProdutoById", ex);
                
                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }
            finally
            {
                FabricaConexao.fecharConexao();
            }

            return produto;

        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public void insertMovimento(Movimento movimento)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método insertMovimento");

            NpgsqlConnection dbConnection = FabricaConexao.getConexao();

            IMovimentoDao movimentoDao = FabricaDao.getMovimentoDao();

            NpgsqlTransaction transaction = dbConnection.BeginTransaction();

            try
            {
                movimentoDao.insert(movimento, dbConnection);
                transaction.Commit();
                
            }
            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - insertMovimento", ex);
                transaction.Rollback();
                throw new DataBaseConectionException(DataBaseConectionException.msg + ex);
            }
            finally
            {
                FabricaConexao.fecharConexao();
            }
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public ContratoProduto findContratoProdutoByProdutoInContrato(Contrato contrato, Produto produto)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findContratoProdutoByContrato");

            NpgsqlConnection dbConnection = FabricaConexao.getConexao();

            IContratoProdutoDao contratoProdutoDao = FabricaDao.getContratoProdutoDao();
            IUsuarioDao usuarioDao = FabricaDao.getUsuarioDao();

            ContratoProduto contratoProduto = null;

            try
            {
                contratoProduto = contratoProdutoDao.findByContratoAndProduto(contrato, produto, dbConnection);

                if (contratoProduto != null)
                {
                    Usuario usuario = usuarioDao.findById((Int64)contrato.UsuarioCriador.Id, dbConnection);
                    contratoProduto.Usuario = usuario;
                }
            }
            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findContratoProdutoByProdutoInContrato", ex);

                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }
            finally
            {
                FabricaConexao.fecharConexao();
            }

            return contratoProduto;

        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public DataSet findMovimentoByFilter(Movimento movimento, DateTime? dataFinalLancamento, DateTime? dataFinalFaturamento, Boolean check, Boolean particular, Produto produto, Exame exame, DateTime? dataInicialAtendimento, DateTime? dataFinalAtendimento)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findMovimentoByFilter");

            NpgsqlConnection dbConnection = FabricaConexao.getConexao();

            IMovimentoDao movimentoDao = FabricaDao.getMovimentoDao();

            try
            {
                return movimentoDao.findMovimentoByFilter(movimento, dataFinalLancamento, dataFinalFaturamento, check, particular, produto, exame, dataInicialAtendimento, dataFinalAtendimento,  dbConnection);
            
            }
            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findMovimentoByFilter", ex);

                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }
            finally
            {
                FabricaConexao.fecharConexao();
            }
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public DataSet findMovimentoDetalhado(Cliente cliente, DateTime? dataInicialLancamento, DateTime? dataFinalLancamento, Boolean analitico, Boolean faturado)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findMovimentoDetalhado");

            NpgsqlConnection dbConnection = FabricaConexao.getConexao();

            IMovimentoDao movimentoDao = FabricaDao.getMovimentoDao();

            try
            {
                return movimentoDao.findMovimentoDetalhado(cliente, dataInicialLancamento, dataFinalLancamento, analitico, faturado, dbConnection);

            }
            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findMovimentoDetalhado", ex);

                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }
            finally
            {
                FabricaConexao.fecharConexao();
            }
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public DataSet findPlanoByFilter(Plano plano)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findPlanoByFilter");

            NpgsqlConnection dbConnection = FabricaConexao.getConexao();

            IPlanoDao planoDao = FabricaDao.getPlanoDao();
            IParcelaDao parcelaDao = FabricaDao.getParcelaDao();

            try
            {
                return planoDao.findByFilter(plano , dbConnection);

            }
            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findPlanoByFilter", ex);

                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }
            finally
            {
                FabricaConexao.fecharConexao();
            }
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public Plano incluirPlano(Plano plano)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método incluirPlano");

            NpgsqlConnection dbConnection = FabricaConexao.getConexao();

            IPlanoDao planoDao = FabricaDao.getPlanoDao();
            IPlanoFormaDao planoFormaDao = FabricaDao.getPlanoFormaDao();
            IParcelaDao parcelaDao = FabricaDao.getParcelaDao();
            NpgsqlTransaction transaction = dbConnection.BeginTransaction();

            try
            {
                if (planoDao.findByDescricao(plano.Descricao, dbConnection) != null)
                    throw new Exception("Já existe um plano cadastrado com essa descrição.");
                
                plano = planoDao.insert(plano, dbConnection);
                
                plano.Formas.ForEach(delegate(PlanoForma forma)
                {
                    forma.Plano = plano;
                    planoFormaDao.insert(forma, dbConnection);
                });

                plano.Parcelas.ForEach(delegate(Parcela parcela)
                {
                    parcela.Plano = plano;
                    parcelaDao.insert(parcela, dbConnection);
                });

                transaction.Commit();

            }
            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - incluirPlano", ex);
                transaction.Rollback();
                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }
            finally
            {
                FabricaConexao.fecharConexao();
            }

            return plano;
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public Plano updatePlano(Plano plano)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método alterarPlano");

            NpgsqlConnection dbConnection = FabricaConexao.getConexao();

            IPlanoDao planoDao = FabricaDao.getPlanoDao();
            IParcelaDao parcelaDao = FabricaDao.getParcelaDao();

            NpgsqlTransaction transaction = dbConnection.BeginTransaction();

            try
            {
                Plano planoProcura = planoDao.findByDescricao(plano.Descricao, dbConnection);

                if (planoProcura.Id != plano.Id)
                    throw new Exception("Já existe um plano cadastrado com essa descrição.");
                
                plano = planoDao.update(plano, dbConnection);
                transaction.Commit();

            }
            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - alterarPlano", ex);
                transaction.Rollback();
                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }
            finally
            {
                FabricaConexao.fecharConexao();
            }

            return plano;
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public Plano findPlanoById(Int64 id)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findPlanoById");

            NpgsqlConnection dbConnection = FabricaConexao.getConexao();

            IPlanoDao planoDao = FabricaDao.getPlanoDao();
            IParcelaDao parcelaDao = FabricaDao.getParcelaDao();
            IPlanoFormaDao planoFormaDao = FabricaDao.getPlanoFormaDao();
            
            try
            {
                Plano plano = planoDao.findById(id, dbConnection);
                
                /* incluindo as parcelas presentes no plano */
                plano.Parcelas = parcelaDao.findParcelasByPLano(plano, dbConnection);

                /* incluindo as formas presente no plano */
                plano.Formas = planoFormaDao.findAtivosByPlano(plano, dbConnection);

                return plano;

            }
            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findPlanoById", ex);
                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }
            finally
            {
                FabricaConexao.fecharConexao();
            }
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public DataSet findBancoByFilter(Banco banco, Boolean? caixa, Boolean? boleto)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findBancoByFilter");

            NpgsqlConnection dbConnection = FabricaConexao.getConexao();

            IBancoDao bancoDao = FabricaDao.getBancoDao();

            try
            {
                return bancoDao.findByFilter(banco, caixa, boleto, dbConnection);

            }
            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findBancoByFilter", ex);
                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }
            finally
            {
                FabricaConexao.fecharConexao();
            }
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public Banco incluirBanco(Banco banco)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método incluirBanco");

            NpgsqlConnection dbConnection = FabricaConexao.getConexao();

            IBancoDao bancoDao = FabricaDao.getBancoDao();

            NpgsqlTransaction transaction = dbConnection.BeginTransaction();

            try
            {
                /* verificando se já existe um banco cadastrado com esse nome. */

                Banco bancoProcurado = bancoDao.findByNome(banco.Nome, dbConnection);

                if (bancoProcurado == null)
                {
                    /* verificando se já existe um banco cadastrado com esse código, 
                     * não esquecendo que os caixas administrativos não precisam de código*/

                    if ((Boolean)banco.Caixa)
                    {
                        banco = bancoDao.insert(banco, dbConnection);
                        transaction.Commit();

                    }
                    else
                    {
                        bancoProcurado = bancoDao.findByCodigo(banco.CodigoBanco, dbConnection);

                        if (bancoProcurado == null)
                        {
                            banco = bancoDao.insert(banco, dbConnection);
                            transaction.Commit();
                        }
                        else
                        {
                            throw new BancoException(BancoException.msg2);
                        }
                    }

                }
                else
                {
                    throw new BancoException(BancoException.msg1);
                }
            }
            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - incluirBanco", ex);
                transaction.Rollback();
                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }
            finally
            {
                FabricaConexao.fecharConexao();
            }

            return banco;
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public Banco alteraBanco(Banco banco)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método alteraBanco");

            NpgsqlConnection dbConnection = FabricaConexao.getConexao();

            IBancoDao bancoDao = FabricaDao.getBancoDao();

            NpgsqlTransaction transaction = dbConnection.BeginTransaction();

            try
            {
                /* verificando se existe um banco cadastrado no sistema com o mesmo nome 
                 * escolhido. Caso seja o mesmo banco, não haverá problemas */

                Banco bancoProcura = bancoDao.findByNome(banco.Nome.ToUpper(), dbConnection);

                if (bancoProcura == null || bancoProcura.Id == banco.Id)
                {

                    if ((Boolean)banco.Caixa)
                    {
                        banco = bancoDao.update(banco, dbConnection);
                        transaction.Commit();
                    }
                    
                    else
                    {

                        /* verificando se existe um banco cadastrado no sistema com o mesmo código
                         * bancário escolhido. Caso seja o mesmo  banco então não haverá problemas.
                         * Lembrando que existe caixas administrativos que
                         * não possuem códigos.
                         */

                        bancoProcura = bancoDao.findByCodigo(banco.CodigoBanco, dbConnection);

                        if (bancoProcura != null || bancoProcura.Id == banco.Id)
                        {
                            banco = bancoDao.update(banco, dbConnection);
                            transaction.Commit();
                        }
                        else
                        {
                            throw new BancoException(BancoException.msg2);
                        }

                    }
                }
                else
                {
                    throw new BancoException(BancoException.msg1);
                }
            }
            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - alteraBanco", ex);
                transaction.Rollback();
                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }
            finally
            {
                FabricaConexao.fecharConexao();
            }

            return banco;
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public Banco findBancoById(Int64 id)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findBancoById");

            NpgsqlConnection dbConnection = FabricaConexao.getConexao();

            IBancoDao bancoDao = FabricaDao.getBancoDao();

            try
            {
                return bancoDao.findById(id, dbConnection);

            }
            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findBancoById", ex);
                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }
            finally
            {
                FabricaConexao.fecharConexao();
            }
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public Banco findBancoByNome(String nome)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findBancoByNome");

            NpgsqlConnection dbConnection = FabricaConexao.getConexao();

            IBancoDao bancoDao = FabricaDao.getBancoDao();

            try
            {
                return bancoDao.findByNome(nome, dbConnection);

            }
            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findBancoByNome", ex);
                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }
            finally
            {
                FabricaConexao.fecharConexao();
            }
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public Conta insertConta(Conta conta)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método insertConta");

            NpgsqlConnection dbConnection = FabricaConexao.getConexao();

            IContaDao contaDao = FabricaDao.getContaDao();

            Conta contaProcura = null;

            NpgsqlTransaction transaction = dbConnection.BeginTransaction();

            try
            {
                /* verificando se tem uma conta cobrança já cadastrada com esse nome */

                contaProcura = contaDao.findByNome(conta.Nome, dbConnection);

                if (contaProcura == null)
                {
                    conta = contaDao.insert(conta, dbConnection);
                    transaction.Commit();
                }
                else
                {
                    throw new Exception("Já existe uma conta cadastrada com esse nome.");
                }

            }
            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - insertConta", ex);
                transaction.Rollback();
                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }
            finally
            {
                FabricaConexao.fecharConexao();
            }

            return conta;

        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public Conta updateConta(Conta conta)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método updateConta");

            NpgsqlConnection dbConnection = FabricaConexao.getConexao();

            IContaDao contaDao = FabricaDao.getContaDao();

            NpgsqlTransaction transaction = dbConnection.BeginTransaction();

            Conta contaProcura = null;

            try
            {
                // verificando se existe outra conta com esse nome.
                contaProcura = contaDao.findByNome(conta.Nome, dbConnection);

                if (contaProcura != null && contaProcura.Id == conta.Id)
                {
                    conta = contaDao.update(conta, dbConnection);
                    transaction.Commit();
                }
                else
                {
                    throw new Exception("Já existe uma conta cadastrada com esse nome.");
                }

            }
            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - updateConta", ex);
                transaction.Rollback();
                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }
            finally
            {
                FabricaConexao.fecharConexao();
            }

            return conta;
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public Conta findContaById(Int64 id)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findContaById");

            NpgsqlConnection dbConnection = FabricaConexao.getConexao();

            IContaDao contaDao = FabricaDao.getContaDao();

            try
            {
                return contaDao.findById(id, dbConnection);

            }
            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findContaById", ex);
                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }
            finally
            {
                FabricaConexao.fecharConexao();
            }
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public void mudaSituacaoConta(Conta conta)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método mudaSituacaoConta");

            NpgsqlConnection dbConnection = FabricaConexao.getConexao();

            IContaDao contaDao = FabricaDao.getContaDao();

            NpgsqlTransaction transaction = dbConnection.BeginTransaction();

            try
            {
                contaDao.mudaSituacaoConta(conta, dbConnection);
                transaction.Commit();

            }
            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - mudaSituacaoConta", ex);
                transaction.Rollback();
                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }
            finally
            {
                FabricaConexao.fecharConexao();
            }
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public DataSet findContaByFilter(Conta conta)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findContaByFilter");

            NpgsqlConnection dbConnection = FabricaConexao.getConexao();

            IContaDao contaDao = FabricaDao.getContaDao();

            try
            {
                return contaDao.findByFilter(conta, dbConnection);

            }
            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findContaByFilter", ex);
                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }
            finally
            {
                FabricaConexao.fecharConexao();
            }
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public Conta findContaByName(String nome)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findContaByName");

            NpgsqlConnection dbConnection = FabricaConexao.getConexao();

            IContaDao contaDao = FabricaDao.getContaDao();

            try
            {
                return contaDao.findByNome(nome, dbConnection);

            }
            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findContaByName", ex);
                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }
            finally
            {
                FabricaConexao.fecharConexao();
            }
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public Decimal findSomaMovimentoByFilter(Movimento movimento, DateTime? dataFinalLancamento, DateTime? dataFinalFaturamento)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findMovimentoByFilter");

            NpgsqlConnection dbConnection = FabricaConexao.getConexao();

            IMovimentoDao movimentoDao = FabricaDao.getMovimentoDao();

            try
            {
                return movimentoDao.findSomaMovimentoByFilter(movimento, dataFinalLancamento, dataFinalFaturamento, dbConnection);

            }
            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findSomaMovimentoByFilter", ex);

                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }
            finally
            {
                FabricaConexao.fecharConexao();
            }
        }
        
        [MethodImpl(MethodImplOptions.Synchronized)]
        internal Nfe gravaNotaFiscal(Nfe nfe, Conta conta, Usuario usuarioCriador)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método gravaNotaFiscal");

            NpgsqlConnection dbConnection = FabricaConexao.getConexao();
            
            NpgsqlTransaction transaction = dbConnection.BeginTransaction();
            
            INfeDao nfeDao = FabricaDao.getNfeDao();
            IMovimentoDao movimentoDao = FabricaDao.getMovimentoDao();

            try
            {
                // gravando nota fiscal no banco
                nfe = nfeDao.insert(nfe, dbConnection);

                // fazendo update na tabela de movimento para informar que o movimento foi faturado.
                foreach (Movimento movimento in nfe.ListaMovimento)
                {
                    movimento.Nfe = nfe;
                    movimento.Empresa = nfe.Empresa;
                    movimentoDao.updateMovimentoFaturado(movimento, dbConnection);
                }

                // Gravando parcelas do contas a receber.
                coreGeraCobranca(nfe, conta, usuarioCriador, dbConnection);

                transaction.Commit();
                
            }
            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - gravaNotaFiscal", ex);
                transaction.Rollback();
                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }
            finally
            {
                FabricaConexao.fecharConexao();
            }

            return nfe;

        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public DataSet findFormaByFilter(Forma forma, Plano plano)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findFormaByFilter");

            NpgsqlConnection dbConnection = FabricaConexao.getConexao();

            IFormaDao formaDao = FabricaDao.getFormaDao();

            try
            {
                return formaDao.findFormaByFilter(forma, plano, dbConnection);

            }
            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findFormaByFilter", ex);
                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }
            finally
            {
                FabricaConexao.fecharConexao();
            }


        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public PlanoForma insertPlanoForma(PlanoForma planoForma)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método incluirPlanoForma");

            NpgsqlConnection dbConnection = FabricaConexao.getConexao();

            IPlanoFormaDao planoFormaDao = FabricaDao.getPlanoFormaDao();

            NpgsqlTransaction transaction = dbConnection.BeginTransaction();

            try
            {
                /* verificando se já não existe a forma selecionada incluida como desativada */

                PlanoForma planoFormaProcurado = planoFormaDao.findByFormaByPlano(planoForma.Forma, planoForma.Plano,  dbConnection);

                if (planoFormaProcurado != null)
                {
                    if (string.Equals(planoFormaProcurado.Situacao, ApplicationConstants.DESATIVADO))
                    {
                        planoFormaProcurado.Situacao = ApplicationConstants.ATIVO;
                        planoForma = planoFormaDao.update(planoFormaProcurado, dbConnection);
                    }
                }
                else
                    planoForma = planoFormaDao.insert(planoForma, dbConnection);
                
                transaction.Commit();

            }
            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - incluirPlanoForma", ex);
                transaction.Rollback();
                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }
            finally
            {
                FabricaConexao.fecharConexao();
            }

            return planoForma;
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public PlanoForma updatePlanoForma(PlanoForma planoForma)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método desativaPlanoForma");

            NpgsqlConnection dbConnection = FabricaConexao.getConexao();

            IPlanoFormaDao planoFormaDao = FabricaDao.getPlanoFormaDao();

            NpgsqlTransaction transaction = dbConnection.BeginTransaction();

            try
            {
                planoForma = planoFormaDao.update(planoForma, dbConnection);
                transaction.Commit();

            }
            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - desativaPlanoForma", ex);
                transaction.Rollback();
                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }
            finally
            {
                FabricaConexao.fecharConexao();
            }

            return planoForma;
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public DataSet findFormaByPlano(Plano plano)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findFormaByPlano");

            NpgsqlConnection dbConnection = FabricaConexao.getConexao();

            IFormaDao formaDao = FabricaDao.getFormaDao();

            try
            {
                return formaDao.findFormaByPlano(plano, dbConnection);

            }
            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findFormaByPlano", ex);
                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }
            finally
            {
                FabricaConexao.fecharConexao();
            }
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public PlanoForma findPlanoFormaById(Int64 id)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findPlanoFormaById");

            NpgsqlConnection dbConnection = FabricaConexao.getConexao();

            IPlanoFormaDao planoFormaDao = FabricaDao.getPlanoFormaDao();
            IParcelaDao parcelaDao = FabricaDao.getParcelaDao();

            try
            {
                PlanoForma planoForma = null;
                
                planoForma = planoFormaDao.findById(id, dbConnection);                
                
                planoForma.Plano.Parcelas = parcelaDao.findParcelasByPLano(planoForma.Plano, dbConnection);

                return planoForma;
                    

            }
            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findPlanoFormaById", ex);
                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }
            finally
            {
                FabricaConexao.fecharConexao();
            }
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        private void coreGeraCobranca(Nfe nfe, Conta conta, Usuario usuarioCriador, NpgsqlConnection dbConnection)
        {
            try
            {
                LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método coreGeraCobranca");

                ICobrancaDao cobrancaDao = FabricaDao.getCobrancaDao();
                IContaDao contaDao = FabricaDao.getContaDao();
                
                Cobranca cobranca = null;
                
                Decimal valorTotalCobranca = 0;

                String nossoNumero = null;

                /* verificando se o valor da cobranca será gerado em relaçao ao valor
                 * cheio de nota fiscal ou se será gerado em relação ao valor líquido. 
                 * verificar parametro do cliente gera_cobranca_valor_liquido */

                if (nfe.Cliente.GeraCobrancaValorLiquido)
                {
                    nfe.ValorTotalNf = Convert.ToDecimal(nfe.ValorLiquido);
                }

                Decimal valorCobranca = System.Math.Round((nfe.ValorTotalNf / 
                    nfe.PlanoForma.Plano.Parcelas.Count), 2);


                for (int i = 0; i < nfe.PlanoForma.Plano.Parcelas.Count; i++)
                {

                    nossoNumero = geraNossoNumero(nfe, conta, cobranca, dbConnection);


                    valorTotalCobranca += valorCobranca;

                    if (i == nfe.PlanoForma.Plano.Parcelas.Count)
                    {
                        valorCobranca = (nfe.ValorTotalNf - valorTotalCobranca);
                    }

                    cobranca = new Cobranca(null, nfe.PlanoForma.Forma,null, null, null, usuarioCriador, null, conta, nfe, valorCobranca, nfe.DataEmissao, nfe.DataEmissao.AddDays((double)nfe.PlanoForma.Plano.Parcelas.ElementAt(i).Dias), null, ApplicationConstants.ATIVO, i + 1, Convert.ToString(nfe.NumeroNfe), null, null,null,null,null, null, null,null, null, null, null, null,nossoNumero);

                    cobranca = cobrancaDao.insert(cobranca, dbConnection);

                    nfe.ListaCobranca.AddLast(cobranca);

                }

            }
            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findPlanoFormaById", ex);
                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }
            
        }

        private String geraNossoNumero(Nfe nfe, Conta conta, Cobranca cobranca, NpgsqlConnection dbConnection)
        {
            String nossoNumero = string.Empty;

            IContaDao contaDao = FabricaDao.getContaDao();
            IClienteDao clienteDao = FabricaDao.getClienteDao();

            /* descobrindo se a cobranca será em boleto bancário */

            if (nfe != null && conta != null)
            {
                if (String.Equals(nfe.PlanoForma.Forma.Descricao, ApplicationConstants.BOLETO) || (cobranca != null && string.Equals(cobranca.Forma.Descricao, ApplicationConstants.BOLETO)))
                {
                    /* verificando se a conta que foi selecionada tem uma 
                     * carteira registrada ou não */

                    if (conta.Registro)
                    {
                        /* banco HSBC */

                        /* o cálculo do nosso número para carteira registrada no banco HSBC será feito da
                         * seguinte maneira:
                         * 
                         * EEEEE + NNNNN onde: 
                         * EEEEE será o numero fornecido pelo banco presente 
                         * no campo codigo_cedente na conta utilizada e
                         * NNNNN será o sequencial obtido no campo proximo_numero */


                        if (conta.Banco.CodigoBanco == "399")
                        {
                            nossoNumero = conta.Convenio.Substring(0, 5) +
                                Convert.ToString(conta.ProximoNumero.ToString()).PadLeft(5, '0');
                        }

                        /* banco do Brasil */
                        if (conta.Banco.CodigoBanco == "001")
                        {
                            /* o cálculo para o nosso numero no banco do brasil deverá ser
                             * feito de acordo com o esquema abaixo:
                               
                             * para convênios de 4 posicoes o nosso numero deverá ter 11 posicoes
                             * e formatado da seguinte maneira:
                             * CCCCNNNNNNN-X 
                             * CCCC - CONVÊNIO DE 4 POSICOES
                             * NNNNNNN - SEQUENCIAL DO BOLETO
                             * X - DIGITO VERIFICADOR
                             * será informado apenas CONVÊNIO + SEQUENCIAL
                             */

                            if (conta.Convenio.Length == 4)
                            {
                                nossoNumero = Convert.ToString(conta.ProximoNumero.ToString());
                            }

                            /* para convênios de 6 posicoes o nosso numero deverá ter 11 posicoes
                             * e formatado da seguinte maneira:
                             * CCCCCCNNNNN-X 
                             * CCCC - CONVÊNIO DE 4 POSICOES
                             * NNNNN - SEQUENCIAL DO BOLETO
                             * X - DIGITO VERIFICADOR
                             * será informado apenas CONVÊNIO + SEQUENCIAL
                             */

                            if (conta.Convenio.Length == 6)
                            {
                                nossoNumero = Convert.ToString(conta.ProximoNumero.ToString());
                            }

                            /* para convênios de 7 posicoes o nosso numero deverá ter 17 posicoes
                             * e formatado da seguinte maneira:
                             * CCCCCCCNNNNNNNNNN 
                             * CCCCCCC - CONVÊNIO DE 7 POSICOES
                             * NNNNNNNNNn - SEQUENCIAL DO BOLETO
                             */

                            if (conta.Convenio.Length == 7)
                            {
                                nossoNumero = Convert.ToString(conta.ProximoNumero.ToString());
                            }
                        }
                    }
                    else
                    {
                        /* cobrancas não registradas
                         * Será implementado somente a cobrança não registrada
                         * CNR do banco HSBC.
                         * 
                         * Nosso número é composto por :
                         * 9 bytes do código do cnpj do cliente +
                         * cálculo do código do cedente com o fator de vencimento para gerar o 
                         * antepenúltimo e o último dígito. 
                         * o penúltimo dígito será igual a 4. */

                        Cliente cliente = clienteDao.findById((Int64)nfe.Cliente.Id, dbConnection);
                        nossoNumero = cliente.Cnpj.Substring(0, 4) +
                            Convert.ToString(conta.ProximoNumero.ToString()).PadLeft(4, '0');

                    }

                    /* atualizando o nosso numero */
                    contaDao.incrementaProximoNumero(conta, dbConnection);
                    conta.ProximoNumero = conta.ProximoNumero + 1;
                }
            }

            return nossoNumero;
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public DataSet findCobrancaByFilter(Cobranca cobranca, Cliente cliente, Nfe nfe, DateTime? dataInicioEmissao, DateTime? dataFimEmissao, DateTime? dataInicioVencimento, DateTime? dataFimVencimento, DateTime? dataInicioBaixa, DateTime? dataFimBaixa,  DateTime? dataInicioCancelamento, DateTime? dataFimCancelamento) 
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findCobrancaByFilter");

            NpgsqlConnection dbConnection = FabricaConexao.getConexao();

            ICobrancaDao cobrancaDao = FabricaDao.getCobrancaDao();

            try
            {
                return cobrancaDao.findByFilter(cobranca, cliente, nfe, dataInicioEmissao, dataFimEmissao, dataInicioVencimento, dataFimVencimento, dataInicioBaixa, dataFimBaixa, dataInicioCancelamento, dataFimCancelamento, dbConnection);

            }
            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findCobrancaByFilter", ex);
                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }
            finally
            {
                FabricaConexao.fecharConexao();
            }
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public void baixarCobranca(Cobranca cobranca)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método baixarCobranca");
            NpgsqlConnection dbConnection = FabricaConexao.getConexao();
            ICobrancaDao cobrancaDao = FabricaDao.getCobrancaDao();
            NpgsqlTransaction transaction = dbConnection.BeginTransaction();

            try
            {
                cobrancaDao.baixarCobranca(cobranca, dbConnection);
                transaction.Commit();

            }
            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - baixarCobranca", ex);
                transaction.Rollback();
                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }
            finally
            {
                FabricaConexao.fecharConexao();
            }
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public void estornarBaixaCobranca(Cobranca cobranca)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método estornarBaixaCobranca");
            NpgsqlConnection dbConnection = FabricaConexao.getConexao();
            ICobrancaDao cobrancaDao = FabricaDao.getCobrancaDao();
            NpgsqlTransaction transaction = dbConnection.BeginTransaction();

            try
            {
                cobrancaDao.estornarBaixaCobranca(cobranca, dbConnection);
                transaction.Commit();

            }
            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - estornarBaixaCobranca", ex);
                transaction.Rollback();
                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }
            finally
            {
                FabricaConexao.fecharConexao();
            }
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public void cancelarCobranca(Cobranca cobranca)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método cancelarCobranca");
            NpgsqlConnection dbConnection = FabricaConexao.getConexao();
            ICobrancaDao cobrancaDao = FabricaDao.getCobrancaDao();
            NpgsqlTransaction transaction = dbConnection.BeginTransaction();

            try
            {
                cobrancaDao.cancelarCobranca(cobranca, dbConnection);
                transaction.Commit();

            }
            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - cancelarCobranca", ex);
                transaction.Rollback();
                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }
            finally
            {
                FabricaConexao.fecharConexao();
            }
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public void reativarCobranca(Cobranca cobranca)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método reativarCobranca");
            NpgsqlConnection dbConnection = FabricaConexao.getConexao();
            ICobrancaDao cobrancaDao = FabricaDao.getCobrancaDao();
            NpgsqlTransaction transaction = dbConnection.BeginTransaction();

            try
            {
                cobrancaDao.reativarCobranca(cobranca, dbConnection);
                transaction.Commit();

            }
            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - reativarCobranca", ex);
                transaction.Rollback();
                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }
            finally
            {
                FabricaConexao.fecharConexao();
            }
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public DataSet findNotaFiscalByFilter(Nfe notaFiscal, DateTime? dataEmissaoInicial, DateTime? dataEmissaoFinal,
            DateTime? dataCancelamentoInicial, DateTime? dataCancelamentoFinal)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findNotaFiscalByFilter");
            
            NpgsqlConnection dbConnection = FabricaConexao.getConexao();
            
            INfeDao NfeDao = FabricaDao.getNfeDao();
            
            try
            {
                return NfeDao.findByFilter(notaFiscal, dataEmissaoInicial, dataEmissaoFinal,
                    dataCancelamentoInicial, dataCancelamentoFinal, dbConnection);

            }
            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findNotaFiscalByFilter", ex);
                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }
            finally
            {
                FabricaConexao.fecharConexao();
            }
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public Nfe findNotaFiscalById(Int64 id)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findNotaFiscalById");

            NpgsqlConnection dbConnection = FabricaConexao.getConexao();

            INfeDao NfeDao = FabricaDao.getNfeDao();
            IEmpresaDao empresaDao = FabricaDao.getEmpresaDao();
            IUsuarioDao usuarioDao = FabricaDao.getUsuarioDao();
            IClienteDao clienteDao = FabricaDao.getClienteDao();
            IPlanoFormaDao planoFormaDao = FabricaDao.getPlanoFormaDao();
            IMovimentoDao movimentoDao = FabricaDao.getMovimentoDao();
            ICobrancaDao cobrancaDao = FabricaDao.getCobrancaDao();
            IContaDao contaDao = FabricaDao.getContaDao();
            ICentroCustoDao centroCustoDao = FabricaDao.getCentroCustoDao();

            Nfe notaFiscalRetorno = null;

            try
            {
                notaFiscalRetorno = NfeDao.findById(id, dbConnection);

                //preeenchendo as informações da nota fiscal.

                //informações da empresa emitente
                notaFiscalRetorno.Empresa = (Empresa)empresaDao.findById((Int64)notaFiscalRetorno.Empresa.Id, dbConnection);

                //informações do usuário que emitiu a nota fiscal.
                notaFiscalRetorno.UsuarioCriador = (Usuario)usuarioDao.findById((Int64)notaFiscalRetorno.UsuarioCriador.Id, dbConnection);


                //informações do cliente.
                notaFiscalRetorno.Cliente = (Cliente)clienteDao.findById((Int64)notaFiscalRetorno.Cliente.Id, dbConnection);


                //informações do plano de pagamento e forma de pagamento
                notaFiscalRetorno.PlanoForma = (PlanoForma)planoFormaDao.findById((Int64) notaFiscalRetorno.PlanoForma.Id, dbConnection);

                //informações sobre os movimentosLancados que pertencem a nota fiscal.
                notaFiscalRetorno.ListaMovimento = movimentoDao.findMovimentosByNotaFiscal(notaFiscalRetorno, dbConnection);
                notaFiscalRetorno.ListaMovimento.ForEach(delegate(Movimento movimento)
                {
                    if (movimento.CentroCusto != null)
                        movimento.CentroCusto = centroCustoDao.findById((long)movimento.CentroCusto.Id, dbConnection);
                });

                /* informação do centro de custo na nota fiscal */
                notaFiscalRetorno.CentroCusto = notaFiscalRetorno.CentroCusto != null ? centroCustoDao.findById((long)notaFiscalRetorno.CentroCusto.Id, dbConnection) : null;

                // informações sobre as cobranças que pertencem a nota fiscal.
                notaFiscalRetorno.ListaCobranca = cobrancaDao.findCobrancasByNotafiscal(notaFiscalRetorno, dbConnection);

                // fazendo update dos dados da lista de cobranças.

                foreach (Cobranca cobranca in notaFiscalRetorno.ListaCobranca)
                {
                    // preenchendo informações do usuários que cancelou.
                    if (cobranca.UsuarioCancela != null)
                    {
                        cobranca.UsuarioCancela = (Usuario)usuarioDao.findById((Int64)cobranca.UsuarioCancela.Id, dbConnection);
                    }
                    //preenchendo informações do usuário que criou
                    cobranca.UsuarioCriador = (Usuario)usuarioDao.findById((Int64)cobranca.UsuarioCriador.Id, dbConnection);

                    //prenchendo informações dos usuario que estornou

                    if (cobranca.UsuarioEstorna != null)
                    {
                        cobranca.UsuarioEstorna = (Usuario)usuarioDao.findById((Int64)cobranca.UsuarioEstorna.Id, dbConnection);
                    }

                    //preenchendo informações do usuario que baixou a cobranca.

                    if (cobranca.UsuarioBaixa != null)
                    {
                        cobranca.UsuarioBaixa = (Usuario)usuarioDao.findById((Int64)cobranca.UsuarioBaixa.Id, dbConnection);
                    }

                    //preechendo informação do usuario que desdobrou a cobrança.

                    if (cobranca.UsuarioDesdobra != null)
                    {
                        cobranca.UsuarioDesdobra = (Usuario)usuarioDao.findById((Int64)cobranca.UsuarioDesdobra.Id, dbConnection);
                    }

                    //preenchendo informação da conta.
                    if (cobranca.Conta != null)
                    {
                        cobranca.Conta = (Conta)contaDao.findById((Int64)cobranca.Conta.Id, dbConnection);
                    }
                }

            }
            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findNotaFiscalById", ex);
                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }
            finally
            {
                FabricaConexao.fecharConexao();
            }

            return notaFiscalRetorno;
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public List<Forma> findAllFormas()
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findAllFormas");

            NpgsqlConnection dbConnection = FabricaConexao.getConexao();

            IFormaDao formaDao = FabricaDao.getFormaDao();

            try
            {
                return formaDao.findAllFormas(dbConnection);

            }
            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findAllFormas", ex);
                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }
            finally
            {
                FabricaConexao.fecharConexao();
            }
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public void prorrogarCobranca(Cobranca cobranca)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método prorrogarCobranca");
            NpgsqlConnection dbConnection = FabricaConexao.getConexao();
            ICobrancaDao cobrancaDao = FabricaDao.getCobrancaDao();
            NpgsqlTransaction transaction = dbConnection.BeginTransaction();

            try
            {
                cobrancaDao.prorrogar(cobranca, dbConnection);
                transaction.Commit();

            }
            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - prorrogarCobranca", ex);
                transaction.Rollback();
                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }
            finally
            {
                FabricaConexao.fecharConexao();
            }
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public void alterarFormaPagamentoCobranca(Cobranca cobranca)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método alterarFormaPagamentoCobranca");
            NpgsqlConnection dbConnection = FabricaConexao.getConexao();
            ICobrancaDao cobrancaDao = FabricaDao.getCobrancaDao();
            INfeDao nfeDao = FabricaDao.getNfeDao();
            IContaDao contaDao = FabricaDao.getContaDao();
            IPlanoFormaDao planoFormaDao = FabricaDao.getPlanoFormaDao();
            NpgsqlTransaction transaction = dbConnection.BeginTransaction();

            try
            {
                if (cobranca.Forma!= null && cobranca.Forma.Descricao.Equals(ApplicationConstants.BOLETO))
                {
                    Nfe nfe = nfeDao.findById((Int64)cobrancaDao.findById((Int64)cobranca.Id, dbConnection).Nota.Id, dbConnection);
                    nfe.PlanoForma = nfe.PlanoForma != null ? planoFormaDao.findById((long)nfe.PlanoForma.Id, dbConnection) : null;
                    cobranca.NossoNumero = geraNossoNumero(nfe, cobranca.Conta, cobranca, dbConnection);
                    Conta conta = contaDao.findById((Int64)cobranca.Conta.Id, dbConnection);
                    cobranca.NossoNumero = geraNossoNumero(nfe, cobranca.Conta, cobranca, dbConnection);
                }
                
                cobrancaDao.alteraFormaPagamento(cobranca, dbConnection);
                transaction.Commit();
            }
            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - alterarFormaPagamentoCobranca", ex);
                transaction.Rollback();
                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }
            finally
            {
                FabricaConexao.fecharConexao();
            }
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public void cancelaNotaFiscal(Nfe notaFiscal)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método cancelaNotaFiscal");
            NpgsqlConnection dbConnection = FabricaConexao.getConexao();
            INfeDao nfeDao = FabricaDao.getNfeDao();
            ICobrancaDao cobrancaDao = FabricaDao.getCobrancaDao();
            IMovimentoDao movimentoDao = FabricaDao.getMovimentoDao();
            NpgsqlTransaction transaction = dbConnection.BeginTransaction();
            IItensCanceladosNfDao itensCanceladosNfDao = FabricaDao.getItensCanceladosNfDao();

            try
            {

                /* Verificando se a nota pode ser cancelada. 
                 * A verificação inicial é que ela não tenha
                 * nenhuma cobrança com status baixada no sistema.
                 * caso a cobrança não tenha sido cancelada, ela será baixada*/

                foreach (Cobranca cobranca in notaFiscal.ListaCobranca)
                {
                    if (!String.Equals(ApplicationConstants.BAIXADO, cobranca.Situacao))
                    {
                        /* setando dados do cancelamento da cobranca */

                        cobranca.UsuarioCancela = PermissionamentoFacade.usuarioAutenticado;
                        cobranca.Situacao = ApplicationConstants.CANCELADA;
                        cobranca.ObservacaoCancelamento = "NOTA FISCAL CANCELADA";
                        cobrancaDao.cancelarCobranca(cobranca, dbConnection);
                    }
                    else
                    {
                        throw new NotaFiscalException(NotaFiscalException.msg1);
                    }
                }

                /* setando usuario que cancelou a nota fiscal para a entidade */

                notaFiscal.UsuarioCancelamento = PermissionamentoFacade.usuarioAutenticado;
                
                nfeDao.cancela(notaFiscal, dbConnection);

                /* deixando o movimento disponível para um novo faturamento */

                foreach (Movimento movimento in notaFiscal.ListaMovimento)
                {
                    movimentoDao.estornaFaturamentoMovimento(movimento, dbConnection);

                    /* gravando itens cancelados na tabela de itens de movimento 
                     * cancelado. Histórico de itens cancelados. */

                    movimento.Situacao = ApplicationConstants.CANCELADA;
                    movimento.Usuario = PermissionamentoFacade.usuarioAutenticado;
                    itensCanceladosNfDao.insertMovimentoCancelado(movimento, dbConnection);
                
                }
                
                transaction.Commit();

            }
            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - cancelaNotaFiscal", ex);
                transaction.Rollback();
                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }
            finally
            {
                FabricaConexao.fecharConexao();
            }
        }
   

        [MethodImpl(MethodImplOptions.Synchronized)]
        public Cobranca findCobrancaById(Int64 idCobranca)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findCobrancaById");
            
            NpgsqlConnection dbConnection = FabricaConexao.getConexao();
            
            ICobrancaDao cobrancaDao = FabricaDao.getCobrancaDao();
            IContaDao contaDao = FabricaDao.getContaDao();
            INfeDao notaDao = FabricaDao.getNfeDao();
            IClienteDao clienteDao = FabricaDao.getClienteDao();
            IPlanoFormaDao planoFormaDao = FabricaDao.getPlanoFormaDao();
            Cobranca cobranca = null;
            
            try
            {
                cobranca = cobrancaDao.findById(idCobranca, dbConnection);

                /* preenchendo demais informações da cobranca */
                cobranca.Nota = cobranca.Nota != null ? notaDao.findById((long)cobranca.Nota.Id, dbConnection) : null;
                cobranca.Nota.Cliente = cobranca.Nota.Cliente != null ? clienteDao.findById((long)cobranca.Nota.Cliente.Id, dbConnection) : null;
                cobranca.Nota.PlanoForma = cobranca.Nota.PlanoForma != null ? planoFormaDao.findById((long)cobranca.Nota.PlanoForma.Id, dbConnection) : null;
                cobranca.Conta = cobranca.Conta != null ? contaDao.findById((long)cobranca.Conta.Id, dbConnection) : null;

                /* preenchendo demais informações da cobranca */
                cobranca.Nota = cobranca.Nota != null ? notaDao.findById((long)cobranca.Nota.Id, dbConnection) : null;
                cobranca.Nota.Cliente = cobranca.Nota.Cliente != null ? clienteDao.findById((long)cobranca.Nota.Cliente.Id, dbConnection) : null;
                cobranca.Conta = cobranca.Conta != null ? contaDao.findById((long)cobranca.Conta.Id, dbConnection) : null;

            }
            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findCobrancaById", ex);
                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }
            finally
            {
                FabricaConexao.fecharConexao();
            }
            return cobranca;
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public Boolean verificaPodeAlterarConta(Int64 idConta)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método verificaPodeAlterarConta");

            NpgsqlConnection dbConnection = FabricaConexao.getConexao();

            IContaDao contaDao = FabricaDao.getContaDao();

            try
            {
                return contaDao.verificaPodeAlterarConta(idConta, dbConnection);

            }
            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - verificaPodeAlterarConta", ex);
                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }
            finally
            {
                FabricaConexao.fecharConexao();
            }
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public DataSet findMovimentoByNotaInRelatorioNota(Nfe nota)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findMovimentoByNotaInRelatorioNota");

            NpgsqlConnection dbConnection = FabricaConexao.getConexao();

            IMovimentoDao movimentoDao = FabricaDao.getMovimentoDao();

            try
            {
                return movimentoDao.findMovimentoByNotaInRelatorioNota(nota, dbConnection);

            }
            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findMovimentoByNotaInRelatorioNota", ex);
                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }
            finally
            {
                FabricaConexao.fecharConexao();
            }
        }

        public void desdobrarCobranca(Cobranca cobrancaOriginal, List<Cobranca> listaCobranca)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método desdobraCobranca");
            NpgsqlConnection dbConnection = FabricaConexao.getConexao();
            ICobrancaDao cobrancaDao = FabricaDao.getCobrancaDao();
            NpgsqlTransaction transaction = dbConnection.BeginTransaction();

            try
            {
                //Salvando as novas cobranças
                foreach (Cobranca cobranca in listaCobranca)
                {
                    cobranca.NossoNumero = geraNossoNumero(cobranca.Nota, cobranca.Conta, cobranca, dbConnection);
                    cobranca.NossoNumero = geraNossoNumero(cobranca.Nota, cobranca.Conta, cobranca, dbConnection);

                    cobranca.NumeroParcela = cobrancaDao.findCobrancasByNotafiscal(cobranca.Nota, dbConnection).Count + 1;

                    cobrancaDao.insert(cobranca, dbConnection);
                }

                //Alterando a situação da cobrança original para desdobrada
                cobrancaDao.alteraSituacaoCobranca(cobrancaOriginal, ApplicationConstants.DESDOBRADA, dbConnection);

                transaction.Commit();

            }
            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - desdobraCobranca", ex);
                transaction.Rollback();
                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }
            finally
            {
                FabricaConexao.fecharConexao();
            }
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public void cancelaMovimento(Movimento movimento)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método cancelaMovimento");
            NpgsqlConnection dbConnection = FabricaConexao.getConexao();
            IMovimentoDao movimentoDao = FabricaDao.getMovimentoDao();
            IEstudoDao estudoDao = FabricaDao.getEstudoDao();
            NpgsqlTransaction transaction = dbConnection.BeginTransaction();

            try
            {

                /* cancelado movimento inicial */
                movimentoDao.changeStatus(movimento, ApplicationConstants.CANCELADA, dbConnection);
                
                /* inserindo movimento com valor contrário com situação cancelada */

                /* verificando se  existe estudo e aso e verificando se existe a unidade cadastrada.*/
                Cliente unidade = null;
                if (movimento.Estudo != null)
                    if (movimento.Aso != null)
                        unidade = estudoDao.pcmsoIsUnidade((Estudo)movimento.Aso.Estudo, dbConnection) ? movimento.Aso.Estudo.ClienteContratante : null;

                movimentoDao.insert(new Movimento(null, movimento.GheFonteAgenteExameAso, movimento.ClienteFuncaoExameAso, movimento.Cliente, movimento.ContratoProduto, movimento.Nfe, DateTime.Now, movimento.DataFaturamento, ApplicationConstants.CANCELADA, (Decimal?)movimento.PrecoUnit * -1, movimento.ValorComissao, movimento.Estudo, movimento.Aso, movimento.Quantidade, PermissionamentoFacade.usuarioAutenticado, PermissionamentoFacade.usuarioAutenticado.Empresa, movimento.CustoUnitario, unidade , movimento.Protocolo, DateTime.Now, movimento.CentroCusto, movimento.ContratoExame, null), dbConnection);
                transaction.Commit();

            }
            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - cancelaNotaFiscal", ex);
                transaction.Rollback();
                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }
            finally
            {
                FabricaConexao.fecharConexao();
            }
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public void faturaMovimentoInterno(List<Movimento> movimentos, Empresa empresa)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método faturaMovimentoInterno");
            NpgsqlConnection dbConnection = FabricaConexao.getConexao();
            IMovimentoDao movimentoDao = FabricaDao.getMovimentoDao();
            NpgsqlTransaction transaction = dbConnection.BeginTransaction();

            try
            {

                /* preenchendo itens da coleção para o faturamento */

                foreach (Movimento movimento in movimentos)
                {
                    /* setando empresa */
                    movimento.Empresa = empresa;
                    /* setando a data de faturamento para a data do sistema
                     * não será permitido faturamento retroativo. */
                    movimento.DataFaturamento = Convert.ToDateTime(DateTime.Now);
                    /* gravando o movimento como faturado */
                    movimentoDao.updateMovimentoFaturado(movimento, dbConnection);
                }
                
                transaction.Commit();

            }
            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - faturaMovimentoInterno", ex);
                transaction.Rollback();
                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }
            finally
            {
                FabricaConexao.fecharConexao();
            }
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public DataSet somaMovimentoInternoByClienteInRelatorio(Cliente cliente, DateTime dataInicial, DateTime dataFinal)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método somaMovimentoInternoByClienteInRelatorio");

            NpgsqlConnection dbConnection = FabricaConexao.getConexao();

            IMovimentoDao movimentoDao = FabricaDao.getMovimentoDao();

            try
            {
                return movimentoDao.somaMovimentoInternoByClienteInRelatorio(cliente, dataInicial, dataFinal, dbConnection);

            }
            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - somaMovimentoInternoByClienteInRelatorio", ex);
                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }
            finally
            {
                FabricaConexao.fecharConexao();
            }


        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public DataSet findColaboradorByMovimentoInRecibo(Cliente cliente, DateTime dataInicial, DateTime dataFinal)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findColaboradorByMovimentoInRecibo");

            NpgsqlConnection dbConnection = FabricaConexao.getConexao();

            IMovimentoDao movimentoDao = FabricaDao.getMovimentoDao();

            try
            {
                return movimentoDao.findColaboradorByMovimentoInRecibo(cliente, dataInicial, dataFinal, dbConnection);

            }
            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findColaboradorByMovimentoInRecibo", ex);
                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }
            finally
            {
                FabricaConexao.fecharConexao();
            }

        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public DataSet findMovimentoInRecibo(Cliente cliente, DateTime dataInicial, DateTime dataFinal)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findMovimentoInRecibo");

            NpgsqlConnection dbConnection = FabricaConexao.getConexao();

            IMovimentoDao movimentoDao = FabricaDao.getMovimentoDao();

            try
            {
                return movimentoDao.findMovimentoInRecibo(cliente, dataInicial, dataFinal, dbConnection);

            }
            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findMovimentoInRecibo", ex);
                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }
            finally
            {
                FabricaConexao.fecharConexao();
            }
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public LinkedList<Cobranca> findCobrancasByNotafiscal(Nfe notaFiscal)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findCobrancasByNotafiscal");

            NpgsqlConnection dbConnection = FabricaConexao.getConexao();

            ICobrancaDao cobrancaDao = FabricaDao.getCobrancaDao();

            try
            {
                return cobrancaDao.findCobrancasByNotafiscal(notaFiscal, dbConnection);

            }
            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findCobrancasByNotafiscal", ex);
                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }
            finally
            {
                FabricaConexao.fecharConexao();
            }
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public DataSet findProdutoByTipoEstudo(String tipoEstudo)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findProdutoByTipoEstudo");

            NpgsqlConnection dbConnection = FabricaConexao.getConexao();

            IProdutoDao ProdutoDao = FabricaDao.getProdutoDao();

            try
            {
                return ProdutoDao.findProdutoByTipoEstudo(tipoEstudo, dbConnection);

            }
            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findProdutoByTipoEstudo", ex);
                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }
            finally
            {
                FabricaConexao.fecharConexao();
            }
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public ContratoProduto updateContratoProduto(ContratoProduto contratoProduto)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método updateContratoProduto");

            NpgsqlConnection dbConnection = FabricaConexao.getConexao();

            IContratoProdutoDao contratoProdutoDao = FabricaDao.getContratoProdutoDao();

            NpgsqlTransaction transaction = dbConnection.BeginTransaction();

            try
            {
                contratoProdutoDao.update(contratoProduto, dbConnection);

                transaction.Commit();
                

            }
            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - updateContratoProduto", ex);
                transaction.Rollback();
                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }
            finally
            {
                FabricaConexao.fecharConexao();
            }

            return contratoProduto;

        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public Contrato insertContratoPrestador(Contrato contrato, List<ContratoExame> exames, List<ContratoProduto> produtos)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método insertContratoPrestador");

            NpgsqlConnection dbConnection = FabricaConexao.getConexao();

            IContratoDao contratoDao = FabricaDao.getContratoDao();
            IContratoExameDao contratoExameDao = FabricaDao.getContratoExameDao();
            IContratoProdutoDao contratoProdutoDao = FabricaDao.getContratoProdutoDao();

            NpgsqlTransaction transaction = dbConnection.BeginTransaction();

            Contrato contratoPrestador; 

            try
            {
                /* inserindo informações do novo contrato de acordo com as informações do contrato 
                 * passado como parâmetro.*/
                contratoPrestador = contratoDao.insertContratoPrestador(contrato, dbConnection);

                /* inserindo novos exames e produtos no novo contrato. */

                foreach (ContratoExame contratoExame in exames)
                {
                    contratoExame.Contrato = contratoPrestador;
                    contratoExameDao.insert(contratoExame, dbConnection);
                }

                foreach (ContratoProduto contratoProduto in produtos)
                {
                    contratoProduto.Contrato = contratoPrestador;
                    contratoProdutoDao.insert(contratoProduto, dbConnection);
                }
                
                
                transaction.Commit();
            }
            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - insertContratoPrestador", ex);
                transaction.Rollback();
                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }
            finally
            {
                FabricaConexao.fecharConexao();
            }

            return contratoPrestador;

        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public List<Contrato> findAllContratoByCliente(Cliente cliente, ClienteProposta clienteProposta)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findAllContratoByCliente");

            NpgsqlConnection dbConnection = FabricaConexao.getConexao();

            IContratoDao contratoDao = FabricaDao.getContratoDao();
            IClienteDao clienteDao = FabricaDao.getClienteDao();
            IEmpresaDao empresaDao = FabricaDao.getEmpresaDao();

            List<Contrato> contratos = contratoDao.findAllByCliente(cliente, clienteProposta, dbConnection);
            
            try
            {
                //preenchendo os objetos dentro da list

                foreach (Contrato contrato in contratos)
                {
                    contrato.Cliente = clienteDao.findById(Convert.ToInt64(contrato.Cliente.Id), dbConnection);

                    if (contrato.Prestador != null)
                    {
                        contrato.Prestador = clienteDao.findById(Convert.ToInt64(contrato.Prestador.Id), dbConnection);
                    }
                }

                contratos.ForEach(delegate(Contrato contrato)
                {
                    contrato.Empresa = contrato.Empresa != null ? empresaDao.findById((long)contrato.Empresa.Id, dbConnection) : null;
                });
                
            }
            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findAllContratoByCliente", ex);
                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }
            finally
            {
                FabricaConexao.fecharConexao();
            }

            return contratos;

        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public Contrato findContratoById(Int64 id)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findContratoById");

            NpgsqlConnection dbConnection = FabricaConexao.getConexao();

            IContratoDao contratoDao = FabricaDao.getContratoDao();
            IClienteDao clienteDao = FabricaDao.getClienteDao();
            IPlanoFormaDao planoFormaDao = FabricaDao.getPlanoFormaDao();
            IEmpresaDao empresaDao = FabricaDao.getEmpresaDao();
            IContratoExameDao contratoexameDao = FabricaDao.getContratoExameDao();
            
            String status = String.Empty;
            Contrato contrato;

            try
            {
                
                contrato = contratoDao.findById(id, dbConnection);
                
                /* verificando se o cliente foi referenciado ao contrato */

                if (contrato.Cliente != null)
                {
                    contrato.Cliente = clienteDao.findById(Convert.ToInt64(contrato.Cliente.Id), dbConnection);
                }

                if (contrato.Prestador != null)
                {
                    contrato.Prestador = clienteDao.findById(Convert.ToInt64(contrato.Prestador.Id), dbConnection);
                }

                /* preenchendo informacao de contrato pai */

                if (contrato.ContratoPai != null)
                {
                    contrato.ContratoPai = contratoDao.findById(Convert.ToInt64(contrato.ContratoPai.Id), dbConnection);
                }

                /* preenchendo informacao do contrato raiz */

                if (contrato.ContratoRaiz != null)
                {
                    contrato.ContratoRaiz = contratoDao.findById(Convert.ToInt64(contrato.ContratoRaiz.Id), dbConnection);
                }

                /* preenchendo informação da empresa no contrato */
                contrato.Empresa = contrato.Empresa != null ? empresaDao.findById((long)contrato.Empresa.Id, dbConnection) : null;

            }
            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findContratoById", ex);
                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }
            finally
            {
                FabricaConexao.fecharConexao();
            }

            return contrato;

        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public void updateupdateValorMovimento(Movimento movimento, Decimal valorAlterado)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método updateupdateValorMovimento");

            NpgsqlConnection dbConnection = FabricaConexao.getConexao();

            IMovimentoDao movimentoDao = FabricaDao.getMovimentoDao();
            ILogDao logDao = FabricaDao.getLogDao();

            NpgsqlTransaction transaction = dbConnection.BeginTransaction();

            try
            {
                movimentoDao.updateValorMovimento(movimento, valorAlterado, dbConnection);
                
                /* gravando histórico de alteração do movimento */

                Usuario usuario = PermissionamentoFacade.usuarioAutenticado;

                Log log = new Log(null, "SEG_MOVIMENTO", movimento.Id, DateTime.Now, "PRC_UNIT", movimento.PrecoUnit.ToString(), valorAlterado.ToString(), usuario.Login);

                logDao.insertLog(log, dbConnection);

                transaction.Commit();


            }
            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - updateupdateValorMovimento", ex);
                transaction.Rollback();
                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }
            finally
            {
                FabricaConexao.fecharConexao();
            }

        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public ContratoExame findContratoExameByContratoandExame(Contrato contrato, Exame exame)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findContratoExameByContratoandExame");
            NpgsqlConnection dbConnection = FabricaConexao.getConexao();
            IContratoExameDao contratoExameDao = FabricaDao.getContratoExameDao();

            try
            {
                return contratoExameDao.findAllByContratoAndExameAndSituacao(contrato, exame, ApplicationConstants.ATIVO, dbConnection);
            }
            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findContratoExameByContratoandExame", ex);
                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }
            finally
            {
                FabricaConexao.fecharConexao();
            }
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public void incluirMovimentoPorItem(GheFonteAgenteExameAso gheFonteAgenteExameAso,
            ClienteFuncaoExameASo clienteFuncaoExameAso)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método incluirMovimentoPorItem");

            NpgsqlConnection dbConnection = FabricaConexao.getConexao();

            IMovimentoDao movimentoDao = FabricaDao.getMovimentoDao();
            ILogDao logDao = FabricaDao.getLogDao();
            IGheFonteAgenteExameAsoDao gheFonteAgenteExameAsoDao = FabricaDao.getGheFonteAgenteExameAsoDao();
            IClienteFuncaoExameASoDao clienteFuncaoExameAsoDao = FabricaDao.getClienteFuncaoExameASoDao();
            IContratoDao contratoDao = FabricaDao.getContratoDao();
            IContratoExameDao contratoExameDao = FabricaDao.getContratoExameDao();
            IEstudoDao estudoDao = FabricaDao.getEstudoDao();
            IClienteDao clienteDao = FabricaDao.getClienteDao();

            NpgsqlTransaction transaction = dbConnection.BeginTransaction();

            try
            {
                Contrato contratoVigente = null;
                Contrato contratoVigentePrestador = null;
                ContratoExame contratoExame = null;
                Cliente clienteCobrado = null;
                Cliente clienteCredenciada = null;

                /* VERIFICANDO PARA SABER SE O CLIENTE É UMA CREDENCIADA DE ALGUEM.
                 * CASO SEJA, ENTÃO O LANCAMENTO FINANCEIRO SERÁ FEITO NA CREDENCIADA. */

                if (gheFonteAgenteExameAso != null)
                {

                    if (gheFonteAgenteExameAso.Aso.ClienteFuncaoFuncionario.ClienteFuncao.Cliente.CredenciadaCliente != null)
                    {
                        clienteCobrado = gheFonteAgenteExameAso.Aso.ClienteFuncaoFuncionario.ClienteFuncao.Cliente.CredenciadaCliente;
                        clienteCobrado = clienteDao.findById((Int64)clienteCobrado.Id, dbConnection);
                        clienteCredenciada = gheFonteAgenteExameAso.Aso.ClienteFuncaoFuncionario.ClienteFuncao.Cliente;
                        clienteCredenciada = clienteDao.findById((Int64)clienteCredenciada.Id, dbConnection);
                    }
                    else
                    {
                        clienteCobrado = gheFonteAgenteExameAso.Aso.ClienteFuncaoFuncionario.ClienteFuncao.Cliente;
                        clienteCobrado = clienteDao.findById((Int64)clienteCobrado.Id, dbConnection);
                    }
                }
                else
                {
                    if (clienteFuncaoExameAso.Aso.ClienteFuncaoFuncionario.ClienteFuncao.Cliente.CredenciadaCliente != null)
                    {
                        clienteCobrado = clienteFuncaoExameAso.Aso.ClienteFuncaoFuncionario.ClienteFuncao.Cliente.CredenciadaCliente;
                        clienteCobrado = clienteDao.findById((long)clienteCobrado.Id, dbConnection);
                        clienteCredenciada = clienteFuncaoExameAso.Aso.ClienteFuncaoFuncionario.ClienteFuncao.Cliente;
                        clienteCredenciada = clienteDao.findById((long)clienteCredenciada.Id, dbConnection);
                    }
                    else
                    {
                        clienteCobrado = clienteFuncaoExameAso.Aso.ClienteFuncaoFuncionario.ClienteFuncao.Cliente;
                        clienteCobrado = clienteDao.findById((long)clienteCobrado.Id, dbConnection);

                    }
                }
                /* VERIFICANDO SE É UM MOVIMENTO DE CLIENTEFUNCAOEXAME OU DE GHEFONTEAGENTEEXAME */

                if (gheFonteAgenteExameAso != null)
                {
                    #region EXAMES NO PCMSO
                                        
                    /* VERIFICANDO PARA SABER SE O CLIENTE ESTÁ MARCADO PARA USAR CONTRATO. 
                     * CASO O CLIENTE NÃO UTILIZE CONTRATO, ENTÃO SERÁ UTILIZADO O CONTRATO DO SISTEMA. */

                    if (clienteCobrado.UsaContrato)
                    {
                        /* verificando o contrato vigente do cliente */
                        contratoVigente = contratoDao.findContratoVigenteByData((DateTime)gheFonteAgenteExameAso.Aso.DataElaboracao,
                            clienteCobrado, null, dbConnection);

                        /* VERIFICANDO PARA SABER SE O CLIENTE NÃO TEM CONTRATO CADASTRADO. 
                         * CASO SEJA POSITIVO SERÁ AVISADO AO USUARIO */
                        if (contratoVigente == null)
                        {
                            throw new ContratoException(ContratoException.msg3);
                        }

                        /* VERIFICANDO SE O EXAME FOI REALIZADO POR UM PRESTADOR EXTERNO */
                        if (gheFonteAgenteExameAso.Prestador != null)
                        {
                            contratoVigentePrestador = contratoDao.findContratoVigenteByData((DateTime)gheFonteAgenteExameAso.Aso.DataElaboracao, clienteCobrado, gheFonteAgenteExameAso.Prestador, dbConnection);

                            if (contratoVigentePrestador == null)
                            {
                                throw new ContratoException (ContratoException.msg2);
                            }
                        }
                            
                        /* RECUPERANDO O CONTRATO DO CLIENTE */
                        contratoExame = contratoExameDao.findAllByContratoAndExameAndSituacao(contratoVigentePrestador == null ? contratoVigente : contratoVigentePrestador,
                                gheFonteAgenteExameAso.GheFonteAgenteExame.Exame, ApplicationConstants.ATIVO, dbConnection);

                        /* VERIFICANDO PARA SABER SE EXISTE O EXAME SELECIONADO NO CONTRATO */
                        if (contratoExame == null)
                        {
                            throw new ContratoException(ContratoException.msg5 + gheFonteAgenteExameAso.GheFonteAgenteExame.Exame.Descricao);
                        }
                                                            
                        /* GERANDO O MOVIMENTO FINANCEIRO PARA GRAVAÇÃO */
                        Movimento movimento = new Movimento(null, gheFonteAgenteExameAso, null, clienteCobrado, null, null, DateTime.Now, null, ApplicationConstants.PENDENTE, contratoExame.PrecoContratado, null, null, gheFonteAgenteExameAso.Aso, 1, PermissionamentoFacade.usuarioAutenticado, PermissionamentoFacade.usuarioAutenticado.Empresa, contratoExame.CustoContrato, estudoDao.pcmsoIsUnidade(gheFonteAgenteExameAso.Aso.Estudo, dbConnection) ? gheFonteAgenteExameAso.Aso.Estudo.ClienteContratante : null, null, DateTime.Now, gheFonteAgenteExameAso.Aso.CentroCusto, contratoExame, clienteCredenciada);
                        movimentoDao.insert(movimento, dbConnection);
                        
                    }
                    else
                    {
                        /* CLIENTE NÃO UTILIZA CONTRATO, ENTÃO SERÁ UTILIZADO O CONTRATO DO SISTEMA. */

                        Contrato contratoSistema = contratoDao.findContratoSistemaByCliente(clienteCobrado, dbConnection);

                        if (contratoSistema != null)
                        {
                            criarMovimentoItemContratoSistema(gheFonteAgenteExameAso.GheFonteAgenteExame.Exame,
                                contratoSistema, gheFonteAgenteExameAso, null, gheFonteAgenteExameAso.Aso,
                                clienteCobrado, clienteCredenciada, dbConnection);
                        }
                        else
                        {
                            /*CRIANDO CONTRATO PARA O CLIENTE.*/
                            contratoSistema = contratoDao.insert(new Contrato(null, String.Empty, clienteCobrado,
                                PermissionamentoFacade.usuarioAutenticado, null, null, null, ApplicationConstants.CONTRATO_FECHADO, String.Empty, null, null, String.Empty, null, true, null, null, false, false, false, null, false, null, null, null, null, null, false, null, null, null, string.Empty, PermissionamentoFacade.usuarioAutenticado.Empresa), dbConnection);

                            criarMovimentoItemContratoSistema(gheFonteAgenteExameAso.GheFonteAgenteExame.Exame,
                                contratoSistema, gheFonteAgenteExameAso, null, gheFonteAgenteExameAso.Aso,
                                clienteCobrado, clienteCredenciada, dbConnection);
                        }

                    }

                    #endregion
                }
                else
                {
                    #region EXAMES EXTRAS

                    /* O ITEM PERTENCE A UM CLIENTEFUNCAOEXAMEASO */


                    /* VERIFICANDO PARA SABER SE O CLIENTE ESTÁ MARCADO PARA UTILIZAR CONTRATO. */
                    if (clienteCobrado.UsaContrato)
                    {
                        contratoVigente = contratoDao.findContratoVigenteByData((DateTime)clienteFuncaoExameAso.Aso.DataElaboracao, clienteCobrado, null, dbConnection);

                        if (contratoVigente == null)
                        {
                            throw new ContratoException(ContratoException.msg2);
                        }

                        if (clienteFuncaoExameAso.Prestador != null)
                        {
                            contratoVigentePrestador = contratoDao.findContratoVigenteByData((DateTime)clienteFuncaoExameAso.Aso.DataElaboracao, clienteCobrado, clienteFuncaoExameAso.Prestador, dbConnection);

                            if (contratoVigentePrestador == null)
                            {
                                throw new ContratoException (ContratoException.msg2);
                            }
                        }

                        contratoExame = contratoExameDao.findAllByContratoAndExameAndSituacao(contratoVigentePrestador == null? contratoVigente : contratoVigentePrestador, clienteFuncaoExameAso.ClienteFuncaoExame.Exame, ApplicationConstants.ATIVO, dbConnection);

                        if (contratoExame == null)
                        {
                            throw new ContratoException(ContratoException.msg5 + clienteFuncaoExameAso.ClienteFuncaoExame.Exame.Descricao);
                        }
                            
                        /* GERANDO O MOVIMENTO FINANCEIRO PARA GRAVAÇÃO */

                                
                        Movimento movimento = new Movimento(null, null, clienteFuncaoExameAso, clienteCobrado, null, null, DateTime.Now, null, ApplicationConstants.PENDENTE, contratoExame.PrecoContratado, null, null, clienteFuncaoExameAso.Aso, 1, PermissionamentoFacade.usuarioAutenticado, PermissionamentoFacade.usuarioAutenticado.Empresa, contratoExame.CustoContrato, estudoDao.pcmsoIsUnidade(clienteFuncaoExameAso.Aso.Estudo, dbConnection) ? clienteFuncaoExameAso.Aso.Estudo.ClienteContratante : null, null, DateTime.Now, clienteFuncaoExameAso.Aso.CentroCusto, contratoExame, clienteCredenciada);
                        movimentoDao.insert(movimento, dbConnection);

                    }
                    else
                    {
                        /*CLIENTE NÃO UTILIZA CONTRATO. SERÁ UTILIZADO O CONTRATO DO CLIENTE.  */
                        
                        Contrato contratoSistema = contratoDao.findContratoSistemaByCliente(clienteCobrado, dbConnection);

                        if (contratoSistema != null)
                        {
                            criarMovimentoItemContratoSistema(clienteFuncaoExameAso.ClienteFuncaoExame.Exame, contratoSistema, null, clienteFuncaoExameAso, clienteFuncaoExameAso.Aso, clienteCobrado, clienteCredenciada, dbConnection);
                        }
                        else
                        {
                            /*CRIANDO CONTRATO PARA O CLIENTE.*/
                            contratoSistema = contratoDao.insert(new Contrato(null, String.Empty, clienteCobrado, PermissionamentoFacade.usuarioAutenticado, null, null, null, ApplicationConstants.CONTRATO_FECHADO, String.Empty, null, null, String.Empty, null, true, null, null, false, false, false, null, false, null, null, null, null, null, false, null, null, null, String.Empty, PermissionamentoFacade.usuarioAutenticado.Empresa), dbConnection);
                            criarMovimentoItemContratoSistema(clienteFuncaoExameAso.ClienteFuncaoExame.Exame, contratoSistema, null, clienteFuncaoExameAso, clienteFuncaoExameAso.Aso, clienteCobrado, clienteCredenciada, dbConnection);
                        }
                    }

                    #endregion
                }

                transaction.Commit();
            }
            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - incluirMovimentoPorItem", ex);
                transaction.Rollback();
                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }
            finally
            {
                FabricaConexao.fecharConexao();
            }
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public Boolean findNotaFiscalByNumero(long numero)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findNotaFiscalByNumero");
            NpgsqlConnection dbConnection = FabricaConexao.getConexao();
            INfeDao nfeDao = FabricaDao.getNfeDao();

            try
            {
                return nfeDao.findByNumero(numero, dbConnection);
            }
            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findNotaFiscalByNumero", ex);
                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }
            finally
            {
                FabricaConexao.fecharConexao();
            }
        }
        
        [MethodImpl(MethodImplOptions.Synchronized)]
        public LinkedList<Movimento> findByProdutoInDataMovimento(Produto produto, DateTime dataLancamentoInicial, 
            Cliente cliente, Boolean insertedInProtocolo, DateTime dataLancamentoFinal)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findByProdutoInDataMovimento");
            
            NpgsqlConnection dbConnection = FabricaConexao.getConexao();
            
            IMovimentoDao movimentoDao = FabricaDao.getMovimentoDao();
            IGheFonteAgenteExameAsoDao gheFonteAgenteExameAsoDao = FabricaDao.getGheFonteAgenteExameAsoDao();
            IClienteFuncaoExameASoDao clienteFuncaoExameAsoDao = FabricaDao.getClienteFuncaoExameASoDao();
            IAsoDao asoDao = FabricaDao.getAsoDao();
            ISalaExameDao salaExameDao = FabricaDao.getSalaExameDao();
            IGheFonteAgenteExameDao gheFonteAgenteExameDao = FabricaDao.getGheFonteAgenteExameDao();
            IClienteDao clienteDao = FabricaDao.getClienteDao();
            IProtocoloDao protocoloDao = FabricaDao.getProtocoloDao();
            IUsuarioDao usuarioDao = FabricaDao.getUsuarioDao();
            IClienteFuncaoExameDao clienteFuncaoExameDao = FabricaDao.getClienteFuncaoExameDao();
            IContratoProdutoDao contratoProdutoDao = FabricaDao.getContratoProdutoDao();
            INfeDao nfeDao = FabricaDao.getNfeDao();
            IEmpresaDao empresaDao = FabricaDao.getEmpresaDao();
            IPlanoFormaDao planoFormaDao = FabricaDao.getPlanoFormaDao();
            IEstudoDao estudoDao = FabricaDao.getEstudoDao();
            ICentroCustoDao centroCustoDao = FabricaDao.getCentroCustoDao();
            IContratoExameDao contratoExameDao = FabricaDao.getContratoExameDao();

            LinkedList<Movimento> movimentos = new LinkedList<Movimento>();

            try
            {
                movimentos = movimentoDao.findByProdutoInDataMovimento(produto, dataLancamentoInicial, cliente, 
                    insertedInProtocolo, dataLancamentoFinal, dbConnection);

                foreach (Movimento movimento in movimentos)
                {

                    if (movimento.Cliente != null)
                    {
                        movimento.Cliente = clienteDao.findById((Int64)movimento.Cliente.Id, dbConnection);
                    }

                    if (movimento.ContratoProduto != null)
                    {
                        movimento.ContratoProduto = contratoProdutoDao.findById((Int64)movimento.ContratoProduto.Id, dbConnection);
                    }

                    #region nfe

                    if (movimento.Nfe != null)
                    {
                        movimento.Nfe = nfeDao.findById((Int64)movimento.Nfe.Id, dbConnection);

                        /* preenchendo objeto nfe */

                        Nfe nfe = movimento.Nfe;

                        if (nfe.UsuarioCriador != null)
                        {
                            nfe.UsuarioCriador = usuarioDao.findById((Int64)nfe.UsuarioCriador.Id, dbConnection);
                        }

                        if (nfe.UsuarioCancelamento != null)
                        {
                            nfe.UsuarioCancelamento = usuarioDao.findById((Int64)nfe.UsuarioCancelamento.Id, dbConnection);
                        }

                        if (nfe.Empresa != null)
                        {
                            nfe.Empresa = empresaDao.findById((Int64)nfe.Empresa.Id, dbConnection);
                        }

                        if (nfe.Cliente != null)
                        {
                            nfe.Cliente = clienteDao.findById((Int64)nfe.Cliente.Id, dbConnection);
                        }

                        if (nfe.PlanoForma != null)
                        {
                            nfe.PlanoForma = planoFormaDao.findById((Int64)nfe.PlanoForma.Id, dbConnection);
                        }

                        movimento.Nfe = nfe;
                            
                    }

                    #endregion

                    #region usuario

                    if (movimento.Usuario != null)
                    {
                        movimento.Usuario = usuarioDao.findById((Int64)movimento.Usuario.Id, dbConnection);
                    }
                    #endregion

                    #region empresa

                    if (movimento.Empresa != null)
                    {
                        movimento.Empresa = empresaDao.findById((Int64)movimento.Empresa.Id, dbConnection);
                    }
                        

                    #endregion

                    #region unidade

                    if (movimento.Unidade != null)
                    {
                        movimento.Unidade = clienteDao.findById((Int64)movimento.Unidade.Id, dbConnection);
                    }


                    #endregion

                    #region protocolo

                    if (movimento.Protocolo != null)
                    {
                        movimento.Protocolo = protocoloDao.findById((Int64)movimento.Protocolo.Id, dbConnection);
                    }

                    #endregion

                    /* preenchendo centro de custo */
                    if (movimento.CentroCusto != null)
                        movimento.CentroCusto = centroCustoDao.findById((long)movimento.CentroCusto.Id, dbConnection);

                    if (movimento.ContratoExame != null)
                        movimento.ContratoExame = contratoExameDao.findById((long)movimento.ContratoExame.Id, dbConnection);
                        
                }
                return movimentos;
            }
            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findByProdutoInDataMovimento", ex);
                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }
            finally
            {
                FabricaConexao.fecharConexao();
            }
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public Movimento findMovimentoById(Int64 id)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findMovimentoById");

            NpgsqlConnection dbConnection = FabricaConexao.getConexao();

            IMovimentoDao movimentoDao = FabricaDao.getMovimentoDao();
            IClienteDao clienteDao = FabricaDao.getClienteDao();
            ICentroCustoDao centroCustoDao = FabricaDao.getCentroCustoDao();
            IEmpresaDao empresaDao = FabricaDao.getEmpresaDao();

            Movimento movimentoRetorno = null;

            try
            {
                movimentoRetorno = movimentoDao.findById(id, dbConnection);
                /* informação do centro de custo */
                if (movimentoRetorno.CentroCusto != null)
                    movimentoRetorno.CentroCusto = centroCustoDao.findById((long)movimentoRetorno.CentroCusto.Id, dbConnection);

                if (movimentoRetorno.ContratoProduto != null)
                    movimentoRetorno.ContratoProduto.Contrato.Empresa = movimentoRetorno.ContratoProduto.Contrato.Empresa != null ? empresaDao.findById((long)movimentoRetorno.ContratoProduto.Contrato.Empresa.Id, dbConnection) : null;
                else
                    movimentoRetorno.Empresa = empresaDao.findById((long)movimentoRetorno.Empresa.Id, dbConnection);

                movimentoRetorno.Cliente = clienteDao.findById((long)movimentoRetorno.Cliente.Id, dbConnection);

                return movimentoRetorno;
            }
            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findMovimentoById", ex);
                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }
            finally
            {
                FabricaConexao.fecharConexao();
            }
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        private void criarMovimentoItemContratoSistema(Exame exame, Contrato contrato, GheFonteAgenteExameAso gheFonteAgenteExameAso, 
            ClienteFuncaoExameASo clienteFuncaoExameAso , Aso aso, Cliente clienteCobrado, Cliente clienteCredenciada, NpgsqlConnection dbConnection)
        {
            IAsoDao asoDao = FabricaDao.getAsoDao();
            IMovimentoDao movimentoDao = FabricaDao.getMovimentoDao();
            IContratoExameDao contratoExameDao = FabricaDao.getContratoExameDao();
            IEstudoDao estudoDao = FabricaDao.getEstudoDao();

            ContratoExame contratoExame = contratoExameDao.findAllByContratoAndExameAndSituacao(contrato, exame, string.Empty, dbConnection);

            if (contratoExame == null)
            {
                // CRIANDO CONTRATO EXAME.
                contratoExameDao.insert(new ContratoExame(null, exame, contrato, 
                    exame.Preco, PermissionamentoFacade.usuarioAutenticado, exame.Custo, DateTime.Now, ApplicationConstants.ATIVO, true), dbConnection);
            }
            else
            {
                /*ATUALIZANDO VALORES DO CONTRATO COM PREÇO DE TABELA */
                contratoExame.CustoContrato = exame.Custo;
                contratoExame.PrecoContratado = exame.Preco;
                contratoExame.Situacao = ApplicationConstants.ATIVO;
                contratoExameDao.update(contratoExame, dbConnection);
            }

            contratoExame = contratoExameDao.findAllByContratoAndExameAndSituacao(contrato, exame, string.Empty, dbConnection);

            if (gheFonteAgenteExameAso != null)
            {
                Movimento movimento = new Movimento(null, gheFonteAgenteExameAso, null, clienteCobrado, null, null, DateTime.Now, null, ApplicationConstants.PENDENTE, contratoExame.PrecoContratado, null, null, aso, Convert.ToInt32(1), PermissionamentoFacade.usuarioAutenticado, PermissionamentoFacade.usuarioAutenticado.Empresa, contratoExame.CustoContrato, estudoDao.pcmsoIsUnidade((Estudo)aso.Estudo, dbConnection) ? aso.Estudo.ClienteContratante : null,  null, DateTime.Now, aso.CentroCusto, contratoExame, clienteCredenciada);
                movimentoDao.insert(movimento, dbConnection);
            }
            else
            {
                Movimento movimento = new Movimento(null, null, clienteFuncaoExameAso, clienteCobrado, null, null, DateTime.Now, null, ApplicationConstants.PENDENTE, contratoExame.PrecoContratado,  null, null,  aso, Convert.ToInt32(1), PermissionamentoFacade.usuarioAutenticado, PermissionamentoFacade.usuarioAutenticado.Empresa, contratoExame.CustoContrato, estudoDao.pcmsoIsUnidade((Estudo)aso.Estudo, dbConnection) ? aso.Estudo.ClienteContratante : null, null, DateTime.Now, aso.CentroCusto, contratoExame, clienteCredenciada);
                movimentoDao.insert(movimento, dbConnection);
            }
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public DataSet findContratoByFilter(Contrato contrato, DateTime? dataElaboracao, DateTime? dataVencimento, 
            DateTime? dataFechamento, DateTime? dataCancelamento)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findContratoByFilter");

            NpgsqlConnection dbConnection = FabricaConexao.getConexao();

            IContratoDao contratoDao = FabricaDao.getContratoDao();

            try
            {
                return contratoDao.findContratoByFilter(contrato, dataElaboracao, dataVencimento, dataFechamento,
                     dataCancelamento, dbConnection);

            }
            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findContratoByFilter", ex);
                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }
            finally
            {
                FabricaConexao.fecharConexao();
            }
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public DataSet findAditivoByContrato(Contrato contrato)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findAditivoByContrato");

            NpgsqlConnection dbConnection = FabricaConexao.getConexao();

            IContratoDao contratoDao = FabricaDao.getContratoDao();

            try
            {
                return contratoDao.findAditivoByContrato(contrato, dbConnection);

            }
            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findAditivoByContrato", ex);
                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }
            finally
            {
                FabricaConexao.fecharConexao();
            }
        }
        
        [MethodImpl(MethodImplOptions.Synchronized)]
        public DataSet findAllContratoExameByContrato(Contrato contrato)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findAllContratoExameByContrato");

            NpgsqlConnection dbConnection = FabricaConexao.getConexao();

            IContratoExameDao contratoExameDao = FabricaDao.getContratoExameDao();

            try
            {
                return contratoExameDao.findAtivosByContratoDataSet(contrato, dbConnection);

            }
            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findAllContratoExameByContrato", ex);
                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }
            finally
            {
                FabricaConexao.fecharConexao();
            }
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public DataSet findAllContratoProdutoByContrato(Contrato contrato)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findAllContratoProdutoByContrato");

            NpgsqlConnection dbConnection = FabricaConexao.getConexao();

            IContratoProdutoDao contratoProdutoDao = FabricaDao.getContratoProdutoDao();

            try
            {
                return contratoProdutoDao.findAtivosByContrato(contrato, dbConnection);

            }
            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findAllContratoProdutoByContrato", ex);
                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }
            finally
            {
                FabricaConexao.fecharConexao();
            }
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public DataSet findProdutoAtivoNotInPropostaContrato(List<Produto> produtos, Produto produto)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findProdutoAtivoNotInPropostaContrato");

            NpgsqlConnection dbConnection = FabricaConexao.getConexao();

            IProdutoDao produtoDao = FabricaDao.getProdutoDao();

            try
            {
                return produtoDao.findProdutoAtivoNotInPropostaContrato(produtos, produto, dbConnection);

            }
            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findProdutoAtivoNotInPropostaContrato", ex);
                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }
            finally
            {
                FabricaConexao.fecharConexao();
            }
        }
        
        [MethodImpl(MethodImplOptions.Synchronized)]
        public ClienteProposta findClientePropostaByContrato(Contrato contrato)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findClientePropostaByContrato");

            NpgsqlConnection dbConnection = FabricaConexao.getConexao();

            IClientePropostaDao clientePropostaDao = FabricaDao.getClientePropostaDao();

            try
            {
                return clientePropostaDao.findClientePropostaByContrato(contrato, dbConnection);

            }
            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findClientePropostaByContrato", ex);
                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }
            finally
            {
                FabricaConexao.fecharConexao();
            }
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public void updateContratoExame(ContratoExame contratoExame)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método updateContratoExame");

            NpgsqlConnection dbConnection = FabricaConexao.getConexao();
            NpgsqlTransaction transaction = dbConnection.BeginTransaction();

            IContratoExameDao contratoExameDao = FabricaDao.getContratoExameDao();

            try
            {
                contratoExameDao.update(contratoExame, dbConnection);
                transaction.Commit();

            }
            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - updateContratoExame", ex);
                transaction.Rollback();
                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }
            finally
            {
                FabricaConexao.fecharConexao();
            }
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public void cancelaContrato(Contrato contrato)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método cancelaContrato");
            NpgsqlConnection dbConnection = FabricaConexao.getConexao();
            NpgsqlTransaction transaction = dbConnection.BeginTransaction();

            IContratoDao contratoDao = FabricaDao.getContratoDao();

            try
            {

                contratoDao.cancela(contrato, dbConnection);
                transaction.Commit();

            }
            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - cancelaContrato", ex);
                transaction.Rollback();
                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }
            finally
            {
                FabricaConexao.fecharConexao();
            }
        }
        
        [MethodImpl(MethodImplOptions.Synchronized)]
        public void validarContrato(Contrato contrato)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método validarContrato");
            NpgsqlConnection dbConnection = FabricaConexao.getConexao();
            NpgsqlTransaction transaction = dbConnection.BeginTransaction();

            IContratoDao contratoDao = FabricaDao.getContratoDao();
            IContratoProdutoDao contratoProdutoDao = FabricaDao.getContratoProdutoDao();
            IContratoExameDao contratoExameDao = FabricaDao.getContratoExameDao();

            try
            {

                /* verificando se a data inicio do contrato é maior do que a data atual. Não será 
                 * permitido criar contrato com data retroativa */
                /* a menos que não exista nenhum contrato vigente no período informado. Somente nesse caso é possível criar um contrato com data retroativa.
                 * 05/12/2016 as 15:59h */ 

                /* verificando se existe algum contrato ativo para o período informado */

                Contrato contratoValidar = contratoDao.verificaContratoVigente(Convert.ToDateTime(contrato.DataInicio), contrato.DataFim == null ? null : (DateTime?)contrato.DataFim, contrato.Cliente, contrato.Prestador, dbConnection);

                if (contratoValidar != null)
                    if (contrato.DataInicio < DateTime.Now)
                        contrato.DataInicio = (DateTime?)DateTime.Now;
                    
                contratoDao.validaContrato(contrato, dbConnection);

                /* mudando o flag para false para não permitir mais alteração do contrato e mantendo a 
                 * consistencia nos dados */

                HashSet<ContratoExame> exames = contratoExameDao.findAtivosByContrato(contrato, dbConnection);
                HashSet<ContratoProduto> produtos = contratoProdutoDao.findAtivosByContratoAndProduto(contrato, null, dbConnection);

                foreach (ContratoExame ce in exames)
                {
                    ce.Altera = false;
                    contratoExameDao.update(ce, dbConnection);
                }

                foreach (ContratoProduto cp in produtos)
                {
                    cp.Altera =  false;
                    contratoProdutoDao.update(cp, dbConnection);
                }
                
                /* verificando se o contrato é um aditivo. Caso seja um aditivo, então o contrato anterior
                 * será encerrado porque não pode existir mais de um contrato fechado para o cliente */

                if (contratoDao.verificaIsAditivo(contrato, dbConnection))
                {
                    /* encerrando o contrato pai do aditivo */

                    Contrato contratoPai = contratoDao.findById((Int64)contrato.ContratoPai.Id, dbConnection);
                    contratoPai.Situacao = ApplicationConstants.CONTRATO_ENCERRADO;
                    contratoDao.changeStatus(contratoPai, dbConnection);

                    /* mudando o status do flagAltera para false. 
                     * Recuperando todos os exames cadastrados no contrato que será finalizado. */

                    HashSet<ContratoExame> examesAditivo = contratoExameDao.findAtivosByContrato(contrato, dbConnection);
                    HashSet<ContratoProduto> produtosAditivo = contratoProdutoDao.findAtivosByContratoAndProduto(contrato, null, dbConnection);

                    foreach (ContratoExame ce in examesAditivo)
                    {
                        ce.Altera = false;
                        contratoExameDao.update(ce, dbConnection);
                    }

                    foreach (ContratoProduto cp in produtosAditivo)
                    {
                        cp.Altera = false;
                        contratoProdutoDao.update(cp, dbConnection);
                    }
                    
                }

                transaction.Commit();

            }
            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - validarContrato", ex);
                transaction.Rollback();
                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }
            finally
            {
                FabricaConexao.fecharConexao();
            }
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public void validaProposta(Contrato contrato)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método validaProposta");
            NpgsqlConnection dbConnection = FabricaConexao.getConexao();
            NpgsqlTransaction transaction = dbConnection.BeginTransaction();

            IContratoDao contratoDao = FabricaDao.getContratoDao();

            try
            {
                contratoDao.validaProposta(contrato, dbConnection);
                transaction.Commit();

            }
            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - validaProposta", ex);
                transaction.Rollback();
                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }
            finally
            {
                FabricaConexao.fecharConexao();
            }
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public Movimento findMovimentoByProdutoOrExame(Movimento movimento)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findMovimentoByProdutoOrExame");
            NpgsqlConnection dbConnection = FabricaConexao.getConexao();
            
            IMovimentoDao movimentoDao = FabricaDao.getMovimentoDao();
            ICentroCustoDao centroCustoDao = FabricaDao.getCentroCustoDao();

            Movimento movimentoRetorno = null;
            try
            {
                movimentoRetorno = movimentoDao.findMovimentoByProdutoOrExame(movimento, dbConnection);
                /* preenchendo informação do centro de custo */
                if (movimentoRetorno != null && movimentoRetorno.CentroCusto != null)
                    movimentoRetorno.CentroCusto = centroCustoDao.findById((long)movimentoRetorno.CentroCusto.Id, dbConnection);
            }
            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findMovimentoByProdutoOrExame", ex);
                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }
            finally
            {
                FabricaConexao.fecharConexao();
            }

            return movimentoRetorno;
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public Contrato insertAditivoInContrato(Contrato contratoPai)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método insertAditivoInContrato");
            NpgsqlConnection dbConnection = FabricaConexao.getConexao();
            NpgsqlTransaction transaction = dbConnection.BeginTransaction();

            IContratoDao contratoDao = FabricaDao.getContratoDao();
            IContratoExameDao contratoExameDao = FabricaDao.getContratoExameDao();
            IContratoProdutoDao contratoProdutoDao = FabricaDao.getContratoProdutoDao();
            IClienteDao clienteDao = FabricaDao.getClienteDao();
            IEmpresaDao empresaDao = FabricaDao.getEmpresaDao();

            try
            {
                
                /* verificando se o contrato Selecionado é a ultimo contratoVigente */

                if (!contratoDao.verificaUltimoContrato(contratoPai, dbConnection))
                    throw new ContratoException(ContratoException.msg6);
                                
                /* criando um novo contrato baseado no contratoRaiz */

                Contrato aditivo = contratoDao.insertAditivo(contratoPai, dbConnection);
                
                /* preenchendo informação do aditivo */
                aditivo = contratoDao.findById((Int64)aditivo.Id, dbConnection);
                aditivo.Empresa = empresaDao.findById((long)aditivo.Empresa.Id, dbConnection);

                /* preenchendo informação do cliente */

                if (aditivo.Cliente != null)
                    aditivo.Cliente = clienteDao.findById((Int64)aditivo.Cliente.Id, dbConnection);

                /* criando relacionamento com exames do aditivo */
                /* recuperando todos os contratoExames ativos do contrato raiz */

                HashSet<ContratoExame> contratoExames = contratoExameDao.findAtivosByContrato(contratoPai, dbConnection);

                /* criando novos contratoExames para os aditivos */

                foreach (ContratoExame ce in contratoExames)
                    contratoExameDao.insert(new ContratoExame(null, ce.Exame, aditivo, ce.PrecoContratado, PermissionamentoFacade.usuarioAutenticado, ce.CustoContrato, DateTime.Now, ApplicationConstants.ATIVO, false), dbConnection);
                
                /* criando relacionamento com produtos do aditivo
                 * Recuperando todos os contratoProdutos ativos do contrato Raiz */

                HashSet<ContratoProduto> contratoProdutos = contratoProdutoDao.findAtivosByContratoAndProduto(contratoPai, null, dbConnection);

                foreach (ContratoProduto cp in contratoProdutos)
                    contratoProdutoDao.insert(new ContratoProduto(null, cp.Produto, aditivo, cp.PrecoContratado, PermissionamentoFacade.usuarioAutenticado, cp.CustoContrato, DateTime.Now, ApplicationConstants.ATIVO, false), dbConnection);

                transaction.Commit();
                return aditivo;

            }
            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - insertAditivoInContrato", ex);
                transaction.Rollback();
                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }
            finally
            {
                FabricaConexao.fecharConexao();
            }
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public void deleteContrato(Contrato contrato)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método deleteContrato");
            NpgsqlConnection dbConnection = FabricaConexao.getConexao();
            NpgsqlTransaction transaction = dbConnection.BeginTransaction();

            IContratoDao contratoDao = FabricaDao.getContratoDao();

            try
            {
                contratoDao.delete(contrato, dbConnection);
                transaction.Commit();

            }
            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - deleteContrato", ex);
                transaction.Rollback();
                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }
            finally
            {
                FabricaConexao.fecharConexao();
            }
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public Boolean contratoProdutoIsUsed(ContratoProduto contratoProduto)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método contratoProdutoIsUsed");
            NpgsqlConnection dbConnection = FabricaConexao.getConexao();

            IContratoProdutoDao contratoProdutoDao = FabricaDao.getContratoProdutoDao();

            try
            {
                return contratoProdutoDao.contratoProdutoIsUsed(contratoProduto, dbConnection);

            }
            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - contratoProdutoIsUsed", ex);
                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }
            finally
            {
                FabricaConexao.fecharConexao();
            }
        }


        [MethodImpl(MethodImplOptions.Synchronized)]
        public Boolean verificaUltimoContrato(Contrato contrato)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método verificaUltimoContrato");
            NpgsqlConnection dbConnection = FabricaConexao.getConexao();

            IContratoDao contratoDao = FabricaDao.getContratoDao();

            try
            {
                return contratoDao.verificaUltimoContrato(contrato, dbConnection);

            }
            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - verificaUltimoContrato", ex);
                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }
            finally
            {
                FabricaConexao.fecharConexao();
            }
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public void changeStatusContrato(Contrato contrato)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método changeStatusContrato");
            NpgsqlConnection dbConnection = FabricaConexao.getConexao();
            NpgsqlTransaction transaction = dbConnection.BeginTransaction();
            IContratoDao contratoDao = FabricaDao.getContratoDao();

            try
            {
                contratoDao.changeStatus(contrato, dbConnection);
                transaction.Commit();


            }
            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - changeStatusContrato", ex);
                transaction.Rollback();
                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }
            finally
            {
                FabricaConexao.fecharConexao();
            }
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public Contrato findContratoVigenteByData(DateTime dt, Cliente cliente, Cliente prestador)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findContratoVigenteByData");
            NpgsqlConnection dbConnection = FabricaConexao.getConexao();
            IContratoDao contratoDao = FabricaDao.getContratoDao();
            IEmpresaDao empresaDao = FabricaDao.getEmpresaDao();

            Contrato contrato = null;
            try
            {
                contrato = contratoDao.findContratoVigenteByData(dt, cliente, prestador, dbConnection);
                if (contrato != null)
                    contrato.Empresa = contrato.Empresa != null ? empresaDao.findById((long)contrato.Empresa.Id, dbConnection) : null;

            }
            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findContratoVigenteByData", ex);
                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }
            finally
            {
                FabricaConexao.fecharConexao();
            }
            return contrato;
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public void encerraContrato(Contrato contrato)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método encerraContrato");
            NpgsqlConnection dbConnection = FabricaConexao.getConexao();
            NpgsqlTransaction transaction = dbConnection.BeginTransaction();
            IContratoDao contratoDao = FabricaDao.getContratoDao();

            try
            {
                /* verificando se a data de elaboração e inicio do contrato é menor do que a data
                 * atual do sistema. Caso seja igual então o contrato terá que ser encerrado com a data
                 * de hoje. Caso seja menor então ele será encerrado com a data de ontem. */

                contratoDao.encerra(contrato, dbConnection);
                transaction.Commit();


            }
            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - encerraContrato", ex);
                transaction.Rollback();
                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }
            finally
            {
                FabricaConexao.fecharConexao();
            }
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public Contrato verificaContratoVigente(DateTime dataInicio, DateTime? dataFim, Cliente cliente, Cliente prestador)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método verificaContratoVigente");
            NpgsqlConnection dbConnection = FabricaConexao.getConexao();
            IContratoDao contratoDao = FabricaDao.getContratoDao();
            IEmpresaDao empresaDao = FabricaDao.getEmpresaDao();
            Contrato contrato = null;
            try
            {
                contrato = contratoDao.verificaContratoVigente(dataInicio, dataFim, cliente, prestador, dbConnection);
                if (contrato != null)
                    contrato.Empresa = contrato.Empresa != null ? empresaDao.findById((long)contrato.Empresa.Id, dbConnection) : null;
            }
            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - verificaContratoVigente", ex);
                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }
            finally
            {
                FabricaConexao.fecharConexao();
            }
            return contrato;
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public Contrato findLastContratoByCliente(Cliente cliente)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findLastContratoByCliente");
            NpgsqlConnection dbConnection = FabricaConexao.getConexao();
            IContratoDao contratoDao = FabricaDao.getContratoDao();
            IEmpresaDao empresaDao = FabricaDao.getEmpresaDao();
            Contrato contrato = null;
            try
            {
                contrato = contratoDao.findLastContratoByCliente(cliente, dbConnection);
                if (contrato != null)
                    contrato.Empresa = contrato.Empresa != null ? empresaDao.findById((long)contrato.Empresa.Id, dbConnection) : null;
            }
            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findLastContratoByCliente", ex);
                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }
            finally
            {
                FabricaConexao.fecharConexao();
            }
            return contrato;
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public long? findLastNumeroNfe()
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findLastNumeroNfef");
            NpgsqlConnection dbConnection = FabricaConexao.getConexao();
            INfeDao nfeDao = FabricaDao.getNfeDao();

            try
            {
                return nfeDao.findLastNumeroNf(dbConnection);

            }
            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findLastNumeroNfef", ex);
                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }
            finally
            {
                FabricaConexao.fecharConexao();
            }
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public void restartSequenceNumeroNfe(long numeroNfe)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método restartSequenceNumeroNfe");
            NpgsqlConnection dbConnection = FabricaConexao.getConexao();
            INfeDao nfeDao = FabricaDao.getNfeDao();

            try
            {
                nfeDao.restartSequence(numeroNfe, dbConnection);

            }
            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - restartSequenceNumeroNfe", ex);
                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }
            finally
            {
                FabricaConexao.fecharConexao();
            }
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public Parcela insertParcela(Parcela parcela)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método insertParcela");

            NpgsqlConnection dbConnection = FabricaConexao.getConexao();
            IParcelaDao parcelaDao = FabricaDao.getParcelaDao();

            NpgsqlTransaction transaction = dbConnection.BeginTransaction();

            try
            {
                parcela = parcelaDao.insert(parcela, dbConnection);
                transaction.Commit();

            }
            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - mudaSituacaoPlano", ex);
                transaction.Rollback();
                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }
            finally
            {
                FabricaConexao.fecharConexao();
            }
            return parcela;
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public void deleteParcela(Parcela parcela)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método deleteParcela");

            NpgsqlConnection dbConnection = FabricaConexao.getConexao();
            IParcelaDao parcelaDao = FabricaDao.getParcelaDao();

            NpgsqlTransaction transaction = dbConnection.BeginTransaction();

            try
            {
                parcelaDao.delete(parcela, dbConnection);
                transaction.Commit();

            }
            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - deleteParcela", ex);
                transaction.Rollback();
                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }
            finally
            {
                FabricaConexao.fecharConexao();
            }
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public Parcela updateParcela(Parcela parcela)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método updateParcela");

            NpgsqlConnection dbConnection = FabricaConexao.getConexao();
            IParcelaDao parcelaDao = FabricaDao.getParcelaDao();

            NpgsqlTransaction transaction = dbConnection.BeginTransaction();

            try
            {
                parcela = parcelaDao.update(parcela, dbConnection);
                transaction.Commit();

            }
            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - updateParcela", ex);
                transaction.Rollback();
                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }
            finally
            {
                FabricaConexao.fecharConexao();
            }
            return parcela;
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public FileFinanceiroXlsx createFileXlsxMovimento(Cliente cliente, DateTime dataInicialLancamento, DateTime dataFinalLancamento, DateTime? dataInicialAtendimento, DateTime? dataFinalAtendimento, string situacao, string tipoAnalise,  long idCentroCusto, Cliente credenciada)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método createFileXlsxMovimento");

            NpgsqlConnection dbConnection = FabricaConexao.getConexao();
            IMovimentoDao movimentoDao = FabricaDao.getMovimentoDao();

            try
            {
                return movimentoDao.createFileXlsx(cliente, dataInicialLancamento, dataFinalLancamento, dataInicialAtendimento, dataFinalAtendimento, situacao, tipoAnalise, idCentroCusto, credenciada, dbConnection);

            }
            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - createFileXlsxMovimento", ex);
                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }
            finally
            {
                FabricaConexao.fecharConexao();
            }
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public List<Produto> findAllAtivosPadraoContrato()
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findAllAtivosPadraoContrato");

            NpgsqlConnection dbConnection = FabricaConexao.getConexao();

            IProdutoDao produtoDao = FabricaDao.getProdutoDao();

            try
            {
                return produtoDao.findAllAtivosPadraoContrato(dbConnection);

            }
            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findAllAtivosPadraoContrato", ex);

                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }
            finally
            {
                FabricaConexao.fecharConexao();
            }

        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public Produto findProdutoByDescricao(string descricao)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findProdutoByDescricao");

            NpgsqlConnection dbConnection = FabricaConexao.getConexao();

            IProdutoDao produtoDao = FabricaDao.getProdutoDao();

            try
            {
                return produtoDao.findProdutoByDescricao(descricao, dbConnection);

            }
            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findProdutoByDescricao", ex);
                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }
            finally
            {
                FabricaConexao.fecharConexao();
            }

        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public ContratoExame findContratoExameById(long id)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findContratoExameById");

            NpgsqlConnection dbConnection = FabricaConexao.getConexao();
            IContratoExameDao contratoExameDao = FabricaDao.getContratoExameDao();

            ContratoExame contratoExame = null;

            try
            {
                contratoExame = contratoExameDao.findById(id, dbConnection);
            }
            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findContratoExameById", ex);
                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }
            finally
            {
                FabricaConexao.fecharConexao();
                
            }
            return contratoExame;
        }


        [MethodImpl(MethodImplOptions.Synchronized)]
        public List<Movimento> findAllLancamentoByAtendimento(Aso atendimento)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findAllLancamentoByAtendimento");

            NpgsqlConnection dbConnection = FabricaConexao.getConexao();
            IMovimentoDao movimentoDao = FabricaDao.getMovimentoDao();

            List<Movimento> movimentos = new List<Movimento>();

            try
            {
                movimentos = movimentoDao.findAllLancamentoByAtendimento(atendimento, dbConnection);
            }
            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findAllLancamentoByAtendimento", ex);
                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }
            finally
            {
                FabricaConexao.fecharConexao();

            }
            return movimentos;
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public Contrato copiarContrato(Contrato contratoNovo, Contrato contratoOrigem)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método copiarContrato");
            NpgsqlConnection dbConnection = FabricaConexao.getConexao();
            NpgsqlTransaction transaction = dbConnection.BeginTransaction();
            IContratoDao contratoDao = FabricaDao.getContratoDao();
            IContratoExameDao contratoExameDao = FabricaDao.getContratoExameDao();
            IContratoProdutoDao contratoProdutoDao = FabricaDao.getContratoProdutoDao();
            try
            {
                /* verificando se o contrato podera ser criado. Não pode haver dois
                 * contratos no mesmo período */

                Contrato contratoVerificado = contratoDao.findContratoVigenteByData((DateTime)contratoNovo.DataInicio, contratoNovo.Cliente, contratoOrigem.Prestador, dbConnection);

                if (contratoVerificado != null)
                    throw new Exception("O Contrato não poderá ser copiado. Já existe um contrato vigente para essa data.");

                /* copiando informações do contratoOrigem */
                contratoNovo.Prestador = contratoOrigem.Prestador;
                contratoNovo.ValorMensalidade = contratoOrigem.ValorMensalidade;
                contratoNovo.ValorNumeroVidas = contratoOrigem.ValorNumeroVidas;
                contratoNovo.UsuarioCriador = PermissionamentoFacade.usuarioAutenticado;
                contratoNovo.Empresa = PermissionamentoFacade.usuarioAutenticado.Empresa;
                contratoNovo.BloqueiaInadimplencia = contratoOrigem.BloqueiaInadimplencia;
                contratoNovo.CobrancaAutomatica = contratoOrigem.CobrancaAutomatica;
                contratoNovo.DataCobranca = contratoOrigem.DataCobranca;
                contratoNovo.DiasInadimplenciaBloqueio = contratoOrigem.DiasInadimplenciaBloqueio;
                contratoNovo.GeraMensalidade = contratoOrigem.GeraMensalidade;
                contratoNovo.GeraNumeroVidas = contratoOrigem.GeraNumeroVidas;

                contratoNovo = contratoDao.insert(contratoNovo, dbConnection);

                /* gravando informações de exames e produtos */

                List<ContratoExame> examesNoContrato = contratoExameDao.findAtivosByContrato(contratoOrigem, dbConnection).ToList();
                List<ContratoProduto> produtosNoContrato = contratoProdutoDao.findAtivosByContratoAndProduto(contratoOrigem, null, dbConnection).ToList();

                foreach (ContratoExame contratoExame in examesNoContrato)
                {
                    ContratoExame contratoExameNovo = new ContratoExame();
                    contratoExameNovo.Contrato = contratoNovo;
                    contratoExameNovo.Altera = contratoExame.Altera;
                    contratoExameNovo.CustoContrato = contratoExameNovo.CustoContrato;
                    contratoExameNovo.DataInclusao = DateTime.Now;
                    contratoExameNovo.Exame = contratoExame.Exame;
                    contratoExameNovo.PrecoContratado = contratoExame.PrecoContratado;
                    contratoExameNovo.Situacao = ApplicationConstants.ATIVO;
                    contratoExameNovo.Usuario = PermissionamentoFacade.usuarioAutenticado;
                    contratoExameDao.insert(contratoExameNovo, dbConnection);
                }

                foreach (ContratoProduto contratoProduto in produtosNoContrato)
                {
                    ContratoProduto contratoProdutoNovo = new ContratoProduto();
                    contratoProdutoNovo.Contrato = contratoNovo;
                    contratoProdutoNovo.Altera = contratoProduto.Altera;
                    contratoProdutoNovo.CustoContrato = contratoProduto.CustoContrato;
                    contratoProdutoNovo.DataInclusao = DateTime.Now;
                    contratoProdutoNovo.Produto = contratoProduto.Produto;
                    contratoProdutoNovo.PrecoContratado = contratoProduto.PrecoContratado;
                    contratoProdutoNovo.Situacao = ApplicationConstants.ATIVO;
                    contratoProdutoNovo.Usuario = PermissionamentoFacade.usuarioAutenticado;
                    contratoProdutoDao.insert(contratoProdutoNovo, dbConnection);
                }

                transaction.Commit();
                
            }
            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - copiarContrato", ex);
                transaction.Rollback();
                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }
            finally
            {
                FabricaConexao.fecharConexao();

            }
           return contratoNovo;
        }

    }

}
