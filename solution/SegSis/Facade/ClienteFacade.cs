﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SWS.IDao;
using SWS.Entidade;
using Npgsql;
using SWS.Excecao;
using System.Data;
using SWS.Dao;
using SWS.View;
using SWS.View.Resources;
using SWS.Helper;
using System.Runtime.CompilerServices;
using SWS.ViewHelper;


namespace SWS.Facade
{
    class ClienteFacade
    {

        #region cliente
        public static ClienteFacade clienteFacade = null;
        
        private ClienteFacade() { }

        public static ClienteFacade getInstance()
        {
            if (clienteFacade == null)
            {
                clienteFacade = new ClienteFacade();
            }

            return clienteFacade;
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public DataSet findClienteByFilter(Cliente cliente, Boolean? prestador, Boolean? credenciada, Boolean? unidade, DateTime? dtCadastroFinal,  Boolean? aptoContrato, bool? particular, bool? credenciadora, bool? fisica)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findClienteByFilter");

            NpgsqlConnection dbConnection = FabricaConexao.getConexao();
            IClienteDao clienteDao = FabricaDao.getClienteDao();

            try
            {
                return clienteDao.findClienteByFilter(cliente, prestador, credenciada, unidade, dtCadastroFinal, aptoContrato, particular, PermissionamentoFacade.usuarioAutenticado, credenciadora, fisica, dbConnection);

            }
            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findClienteByFilter", ex);
                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }
            finally
            {
                FabricaConexao.fecharConexao();
            }
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public Cliente findClienteByCnpj(String cnpj)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findClienteByCnpj");

            NpgsqlConnection dbConnection = FabricaConexao.getConexao();
            
            IClienteDao clienteDao = FabricaDao.getClienteDao();
            IMedicoDao medicoDao = FabricaDao.getMedicoDao();
            IClienteVendedorDao clienteVendedorDao = FabricaDao.getClienteVendedorDao();


            try
            {
                Cliente cliente = clienteDao.findClienteByCnpj(cnpj, dbConnection);

                if (cliente != null)
                {
                    // setando dados da matriz e credenciada e coordenador do cliente.
                    if (cliente.Matriz != null)
                    {
                        cliente.Matriz = clienteDao.findById((Int64)cliente.Matriz.Id, dbConnection);
                    }

                    if (cliente.CredenciadaCliente != null)
                    {
                        cliente.CredenciadaCliente = clienteDao.findById((Int64)cliente.CredenciadaCliente.Id, dbConnection);
                    }

                    if (cliente.Coordenador != null)
                    {
                        cliente.Coordenador = medicoDao.findById((Int64)cliente.Coordenador.Id, dbConnection);
                    }

                    cliente.VendedorCliente = clienteVendedorDao.findByCliente(cliente, dbConnection);

                }

                return cliente;
            }
            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findClienteByCnpj", ex);
                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }
            finally
            {
                FabricaConexao.fecharConexao();
            }
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public Cliente insertCliente(Cliente cliente)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método insertCliente");

            Cliente clienteNovo = null;
            
            NpgsqlConnection dbConnection = FabricaConexao.getConexao();
            IClienteDao clienteDao = FabricaDao.getClienteDao();
            IClienteVendedorDao clienteVendedorDao = FabricaDao.getClienteVendedorDao();
            IClienteCnaeDao clienteCnaeDao = FabricaDao.getClienteCnaeDao();
            IClienteFuncaoDao clienteFuncaoDao = FabricaDao.getClienteFuncaoDao();
            IRamoDao ramoDao = FabricaDao.getRamoDao();
            IContatoDao contatoDao = FabricaDao.getContatoDao();
            ICentroCustoDao centroCustoDao = FabricaDao.getCentroCustoDao();
            NpgsqlTransaction transaction = dbConnection.BeginTransaction();

            try
            {

                /* verificando se ja existe um cliente cadastrado com o mesmo cnpj digitado */
                if (cliente.Matriz == null)
                {
                    Cliente clienteBusca = clienteDao.findClienteByCnpj(Convert.ToString(cliente.Cnpj), dbConnection);
                    if (clienteBusca != null)
                        throw new ClienteException(ClienteException.msg8);
                }

                clienteNovo = clienteDao.insert(cliente, dbConnection);

                foreach (VendedorCliente vendedorCliente in cliente.VendedorCliente)
                {
                    vendedorCliente.Cliente = clienteNovo;
                    clienteVendedorDao.insert(vendedorCliente, dbConnection);
                }
                
                foreach (ClienteCnae clienteCnae in cliente.ClienteCnae)
                {
                   clienteCnae.Cliente = clienteNovo;
                   clienteCnaeDao.incluirClienteCnae(clienteCnae, dbConnection);
                    
                }

                foreach (ClienteFuncao clienteFuncao in clienteNovo.Funcoes)
                {
                    clienteFuncao.Cliente = clienteNovo;
                    clienteFuncaoDao.insert(clienteFuncao, dbConnection);
                }

                foreach (Contato contato in cliente.Contatos)
                {
                    contato.Cliente = clienteNovo;
                    contatoDao.insert(contato, dbConnection);
                }

                /* incluindo centro de custo do cliente */
                clienteNovo.CentroCusto.ForEach(delegate(CentroCusto centroCusto)
                {
                    centroCusto.Cliente = clienteNovo;
                    centroCustoDao.insert(centroCusto, dbConnection);
                });

                transaction.Commit();
            
            }
            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - insertCliente", ex);
                transaction.Rollback();
                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }
            finally
            {
                FabricaConexao.fecharConexao();
            }

            return cliente;
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public Cliente findClienteById(Int64 idCliente)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findClienteById");

            NpgsqlConnection dbConnection = FabricaConexao.getConexao();
            
            // relacionamento cliente
            IClienteDao clienteDao = FabricaDao.getClienteDao();
            IClienteVendedorDao clienteVendedorDao = FabricaDao.getClienteVendedorDao();
            IClienteCnaeDao clienteCnaeDao = FabricaDao.getClienteCnaeDao();
            IMedicoDao medicoDao = FabricaDao.getMedicoDao();
            IClienteFuncaoDao clienteFuncaoDao = FabricaDao.getClienteFuncaoDao();
            ICentroCustoDao centroCustoDao = FabricaDao.getCentroCustoDao();
            try
            {
                // preenchendo objeto cliente com list e hash
                Cliente cliente = clienteDao.findById(idCliente, dbConnection);
                List<ClienteFuncao> clienteFuncao = clienteFuncaoDao.findAtivosByClienteList(cliente, dbConnection);
                
                List<ClienteCnae> clienteCnae = clienteCnaeDao.findClienteCnaeByCliente(cliente, dbConnection);

                if (clienteCnae.Count > 0)
                    cliente.ClienteCnae = clienteCnae;
                
                // recuperando dados do médico cooordenador.
                
                if (cliente.Coordenador != null)
                    cliente.Coordenador = medicoDao.findById((Int64)cliente.Coordenador.Id, dbConnection);

                cliente.VendedorCliente = clienteVendedorDao.findByCliente(cliente, dbConnection);

                if (cliente.CredenciadaCliente != null)
                    cliente.CredenciadaCliente = clienteDao.findById((Int64)cliente.CredenciadaCliente.Id, dbConnection);

                if (cliente.Matriz != null)
                        cliente.Matriz = clienteDao.findById((Int64)cliente.Matriz.Id, dbConnection);

                if (clienteFuncao.Count > 0)
                    cliente.Funcoes = clienteFuncaoDao.findAtivosByClienteList(cliente, dbConnection);

                /* verificando se o cliente utiliza centro de custo e preenchendo a coleação */

                if (cliente.UsaCentroCusto)
                    cliente.CentroCusto = centroCustoDao.findAtivosByCliente(cliente, dbConnection);


                return cliente;
            }
            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findClienteById", ex);
                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }
            finally
            {
                FabricaConexao.fecharConexao();
            }
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public Cliente findClientePpraById(Int64 idCliente)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findClientePpraById");

            NpgsqlConnection dbConnection = FabricaConexao.getConexao();

            IClienteDao clienteDao = FabricaDao.getClienteDao();
            try
            {
            
                Cliente cliente = clienteDao.findById(idCliente, dbConnection);
               return cliente;
            }
            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findClientePpraById", ex);
                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }
            finally
            {
                FabricaConexao.fecharConexao();
            }
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public Cliente clienteAlterar(Cliente cliente)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método clienteAlterar");

            NpgsqlConnection dbConnection = FabricaConexao.getConexao();
            NpgsqlTransaction transaction = dbConnection.BeginTransaction();
                        
            IClienteDao clienteDao = FabricaDao.getClienteDao();

            try
            {
                cliente = clienteDao.updateCliente(cliente, dbConnection);
                transaction.Commit();

            }
            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - ClienteAlterar", ex);
                transaction.Rollback();
                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }
            finally
            {
                FabricaConexao.fecharConexao();
            }

            return cliente;
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public void excluiCliente(Int64 idVendedor, String situacao)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método excluiCliente");

            NpgsqlConnection dbConnection = FabricaConexao.getConexao();

            IClienteDao clienteDao = FabricaDao.getClienteDao();

            NpgsqlTransaction transaction = dbConnection.BeginTransaction();

            try
            {
                clienteDao.changeStatusCliente(idVendedor, situacao, dbConnection);

                transaction.Commit();
            }
            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - changeStatusCliente", ex);
                transaction.Rollback();
                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }
            finally
            {
                FabricaConexao.fecharConexao();
            }

        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public DataSet findClienteAtivoByFilter(Cliente cliente)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findClienteAtivoByFilter");

            NpgsqlConnection dbConnection = FabricaConexao.getConexao();
            IClienteDao clienteDao = FabricaDao.getClienteDao();

            try
            {
                return clienteDao.findClienteAtivoByFilter(cliente, dbConnection);

            }
            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findClienteAtivoByFilter", ex);
                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }
            finally
            {
                FabricaConexao.fecharConexao();
            }
                    
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public DataSet findClienteAtivoAptoContrato(Cliente cliente)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findClienteAtivoAptoContrato");

            NpgsqlConnection dbConnection = FabricaConexao.getConexao();
            IClienteDao clienteDao = FabricaDao.getClienteDao();

            try
            {
                return clienteDao.findClienteAtivoAptoContrato(cliente, dbConnection);

            }
            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findClienteAtivoAptoContrato", ex);
                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }
            finally
            {
                FabricaConexao.fecharConexao();
            }

        }

        #endregion
        
        #region Cnae

        [MethodImpl(MethodImplOptions.Synchronized)]
        public DataSet findCnaeByFilter(Cnae cnae)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findClienteVendedorByIdCliente");

            NpgsqlConnection dbConnection = FabricaConexao.getConexao();
            IClienteDao clienteDao = FabricaDao.getClienteDao();

            try
            {
                return clienteDao.findCnaeByFilter(cnae, dbConnection);

            }
            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findCnaeByFilter", ex);
                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }
            finally
            {
                FabricaConexao.fecharConexao();
            }
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public DataSet findAllCnaeByCliente(Cliente cliente)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findAllCnaeByCliente");

            NpgsqlConnection dbConnection = FabricaConexao.getConexao();
            IClienteCnaeDao clienteCnaeDao = FabricaDao.getClienteCnaeDao();

            try
            {
                return clienteCnaeDao.findAllCnaeByCliente(cliente, dbConnection);

            }
            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findAllCnaeByCliente", ex);
                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }
            finally
            {
                FabricaConexao.fecharConexao();
            }
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public Cnae incluirCnae(Cnae cnae)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método incluirCnae");

            Cnae cnaeNovo = null;
            Cnae cnaeProcurado = null;

            NpgsqlConnection dbConnection = FabricaConexao.getConexao();
            
            IClienteDao clienteDao = FabricaDao.getClienteDao();
            
            NpgsqlTransaction transaction = dbConnection.BeginTransaction();
            try
            {

                if ((cnaeProcurado = clienteDao.findCnaeByCodigo(cnae, dbConnection)) == null)
                {
                    cnaeNovo = clienteDao.incluirCnae(cnae, dbConnection);
                    transaction.Commit();
                }
                else
                {
                        throw new CnaeJaCadastradoException(CnaeJaCadastradoException.msg);
                }
            
            }
            
            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - incluirCnae", ex);
                transaction.Rollback();
                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }
          
            finally
            {
                FabricaConexao.fecharConexao();
            }

            return cnae;
        
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public Cnae findCnaeById(Int64 id)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findCnaeById");

            NpgsqlConnection dbConnection = FabricaConexao.getConexao();
            IClienteDao clienteDao = FabricaDao.getClienteDao();

            try
            {
                return clienteDao.findCnaeById(id, dbConnection);

            }
            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findCnaeById", ex);
                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }
            finally
            {
                FabricaConexao.fecharConexao();
            }
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public Cnae alteraCnae(Cnae cnae)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método alteraCnae");

            Cnae cnaeAlterado = null;
            Cnae cnaeProcurado = null;

            NpgsqlConnection dbConnection = FabricaConexao.getConexao();

            IClienteDao clienteDao = FabricaDao.getClienteDao();

            NpgsqlTransaction transaction = dbConnection.BeginTransaction();
            try
            {
                // procurando se existe outro cnae com o mesmo codigo que nao seja o que foi selecionado para alteracao.
                // assim impedimos que possa ser alterado para um cnae com código já existe
                if ((cnaeProcurado = clienteDao.findCnaeByCodigo(cnae, dbConnection)) != null)
                {
                    if (cnaeProcurado.Id == cnae.Id)
                    {
                        cnaeAlterado = clienteDao.updateCnae(cnae, dbConnection);
                        transaction.Commit();
                    }
                    else
                    {
                        throw new CnaeJaCadastradoException(CnaeJaCadastradoException.msg);

                    }
                }
                else
                {
                    cnaeAlterado = clienteDao.updateCnae(cnae, dbConnection);
                    transaction.Commit();
                }

            }

            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - alteraCnae", ex);
                transaction.Rollback();
                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }

            finally
            {
                FabricaConexao.fecharConexao();
            }

            return cnae;

        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public void excluiCnae(Int64 idCnae)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método excluiCnae");

            Cnae cnaeCliente = null;
            Cnae cnaePpra = null;

            NpgsqlConnection dbConnection = FabricaConexao.getConexao();

            IClienteDao clienteDao = FabricaDao.getClienteDao();
            IEstudoDao ppraDao = FabricaDao.getEstudoDao();

            IClienteCnaeDao clienteCnaeDao = FabricaDao.getClienteCnaeDao();

            NpgsqlTransaction transaction = dbConnection.BeginTransaction();

            try
            {

                // verificando relacionamento em cliente x cnae
                cnaeCliente = clienteCnaeDao.findCnaeById(idCnae, dbConnection);

                // verificando relacionamento com o estudo-cnae
                cnaePpra = ppraDao.findCnaeById(idCnae, dbConnection);

                if (cnaeCliente == null && cnaePpra == null)
                {
                    clienteDao.deleteCnae(idCnae, dbConnection);
                    transaction.Commit();
                }
                else
                {
                    throw new cnaeJaUtilizadoException(cnaeJaUtilizadoException.msg);
                }
            }
            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - excluiCnae", ex);
                transaction.Rollback();
                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }
            finally
            {
                FabricaConexao.fecharConexao();
            }

        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public Cnae findCnaePrincipalCliente(Int64 idCliente)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findCnaePrincipalCliente");

            NpgsqlConnection dbConnection = FabricaConexao.getConexao();
            IClienteCnaeDao clienteCnaeDao = FabricaDao.getClienteCnaeDao();

            try
            {
                return clienteCnaeDao.findCnaePrincipalCliente(idCliente, dbConnection);

            }
            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findCnaePrincipalCliente", ex);
                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }
            finally
            {
                FabricaConexao.fecharConexao();
            }
        }


        #endregion

        #region clienteFuncao

        [MethodImpl(MethodImplOptions.Synchronized)]
        public DataSet findFuncaoByClienteFuncao(ClienteFuncao clienteFuncao)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findFuncaoByClienteFuncao");
            
            NpgsqlConnection dbConnection = FabricaConexao.getConexao();

            IClienteFuncaoDao clienteFuncaoDao = FabricaDao.getClienteFuncaoDao();

            try
            {
                return clienteFuncaoDao.findAtivosByClienteFuncao(clienteFuncao, dbConnection);
                
            }
            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findFuncaoByClienteFuncao", ex);
                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }
            finally
            {
                FabricaConexao.fecharConexao();
            }

        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public DataSet findFuncaoByClienteFuncaoNotInGheSetor(ClienteFuncao clienteFuncao, GheSetor gheSetor)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findFuncaoByClienteFuncaoNotInGheSetor");

            NpgsqlConnection dbConnection = FabricaConexao.getConexao();

            IClienteFuncaoDao clienteFuncaoDao = FabricaDao.getClienteFuncaoDao();

            try
            {
                return clienteFuncaoDao.findAtivoByClienteFuncaoAndGheSetorNotInGheSetor(clienteFuncao, gheSetor, dbConnection);

            }
            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findFuncaoByClienteFuncaoNotInGheSetor", ex);
                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }
            finally
            {
                FabricaConexao.fecharConexao();
            }

        }
        
        #endregion

        #region Logo

        [MethodImpl(MethodImplOptions.Synchronized)]
        public Arquivo incluirArquivo(Arquivo arquivo)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método incluirArquivo");

            Arquivo arquivoNovo = null;
            
            NpgsqlConnection dbConnection = FabricaConexao.getConexao();
            IArquivoDao arquivoDao = FabricaDao.getArquivoDao();

            NpgsqlTransaction transaction = dbConnection.BeginTransaction();
            try
            {
                arquivoNovo = arquivoDao.insertArquivo(arquivo, dbConnection);
                
                transaction.Commit();
            }
            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - incluirArquivo", ex);
                transaction.Rollback();
                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }
            finally
            {
                FabricaConexao.fecharConexao();
            }

            return arquivoNovo;
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public Arquivo findArquivoById(Int64? idArquivo)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findArquivoById");

            NpgsqlConnection dbConnection = FabricaConexao.getConexao();
            IArquivoDao arquivoDao = FabricaDao.getArquivoDao();

            try
            {
                return arquivoDao.findArquivoById(idArquivo, dbConnection);
            }
            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findArquivoById", ex);
                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }
            finally
            {
                FabricaConexao.fecharConexao();
            }
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public ClienteArquivo findClienteArquivoById(Int64 idClienteArquivo)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findClienteArquivoById");

            NpgsqlConnection dbConnection = FabricaConexao.getConexao();
            IClienteArquivoDao clienteArquivoDao = FabricaDao.getClienteArquivoDao();

            try
            {
                return clienteArquivoDao.findClienteArquivoById(idClienteArquivo, dbConnection);
            }
            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findClienteArquivoById", ex);
                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }
            finally
            {
                FabricaConexao.fecharConexao();
            }
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public ClienteArquivo findClienteArquivoByCliente(Cliente cliente)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findClienteArquivoByCliente");

            NpgsqlConnection dbConnection = FabricaConexao.getConexao();
            IClienteArquivoDao clienteArquivoDao = FabricaDao.getClienteArquivoDao();
            IArquivoDao arquivoDao = FabricaDao.getArquivoDao();

            try
            {
                ClienteArquivo clienteArquivoFind = clienteArquivoDao.findClienteArquivoByCliente(cliente, dbConnection);

                /* recuperando informação do arquivo */

                if (clienteArquivoFind != null)
                    clienteArquivoFind.Arquivo = arquivoDao.findArquivoById((Int64)clienteArquivoFind.Arquivo.Id, dbConnection);

                return clienteArquivoFind;
                
            }

            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findClienteArquivoByCliente", ex);
                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }
            finally
            {
                FabricaConexao.fecharConexao();
            }
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public ClienteArquivo incluirClienteArquivo(ClienteArquivo clienteArquivo)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método incluirClienteArquivo");

            ClienteArquivo clienteArquivoNovo = null;

            NpgsqlConnection dbConnection = FabricaConexao.getConexao();
            IClienteArquivoDao clienteArquivoDao = FabricaDao.getClienteArquivoDao();

            NpgsqlTransaction transaction = dbConnection.BeginTransaction();
            try
            {
                clienteArquivoNovo = clienteArquivoDao.insertClienteArquivo(clienteArquivo, dbConnection);

                transaction.Commit();
            }
            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - incluirClienteArquivo", ex);
                transaction.Rollback();
                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }
            finally
            {
                FabricaConexao.fecharConexao();
            }

            return clienteArquivoNovo;
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public void finalizaClienteArquivo(ClienteArquivo clienteArquivo)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método finalizaClienteArquivo");

            NpgsqlConnection dbConnection = FabricaConexao.getConexao();
            IClienteArquivoDao clienteArquivoDao = FabricaDao.getClienteArquivoDao();

            NpgsqlTransaction transaction = dbConnection.BeginTransaction();
            try
            {
                clienteArquivoDao.finalizaClienteArquivo(clienteArquivo, dbConnection);

                transaction.Commit();
            }
            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - finalizaClienteArquivo", ex);
                transaction.Rollback();
                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }
            finally
            {
                FabricaConexao.fecharConexao();
            }
        }

        #endregion

        [MethodImpl(MethodImplOptions.Synchronized)]
        public DataSet findClienteAtivoByFilterNotInClienteFuncaoFuncionarioSet(Cliente cliente, 
            List<ClienteFuncaoFuncionario> clienteFuncaoFuncionarioSet)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findClienteAtivoByFilterNotInClienteFuncaoFuncionarioSet");

            NpgsqlConnection dbConnection = FabricaConexao.getConexao();
            IClienteDao clienteDao = FabricaDao.getClienteDao();

            try
            {
                return clienteDao.findClienteAtivoByFilterNotInClienteFuncaoFuncionarioSet(cliente, clienteFuncaoFuncionarioSet, dbConnection);

            }
            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findClienteAtivoByFilterNotInClienteFuncaoFuncionarioSet", ex);
                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }
            finally
            {
                FabricaConexao.fecharConexao();
            }
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public Boolean verificaPodeAlterarCliente(Cliente cliente)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método verificaPodeAlterarCliente");

            NpgsqlConnection dbConnection = FabricaConexao.getConexao();
            IClienteDao clienteDao = FabricaDao.getClienteDao();

            try
            {
                return clienteDao.verificaPodeAlterarCliente(cliente, dbConnection);

            }
            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - verificaPodeAlterarCliente", ex);
                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }
            finally
            {
                FabricaConexao.fecharConexao();
            }
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public DataSet findAllClienteAtivoParticularByFilter(Cliente cliente)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findAllClienteAtivoParticularByFilter");

            NpgsqlConnection dbConnection = FabricaConexao.getConexao();
            IClienteDao clienteDao = FabricaDao.getClienteDao();

            try
            {
                return clienteDao.findAllClienteParticularAtivosByFilter(cliente, dbConnection);

            }
            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findAllClienteAtivoParticularByFilter", ex);
                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }
            finally
            {
                FabricaConexao.fecharConexao();
            }

        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public IList<SelectItem> findAllCidadeByUf(String uf)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findAllCidadeByUf");

            NpgsqlConnection dbConnection = FabricaConexao.getConexao();
            ICidadeIbgeDao cidadeIbgeDao = FabricaDao.getCidadeIbgeDao();

            try
            {
                return cidadeIbgeDao.findAllCidadeByUf(uf, dbConnection);

            }
            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findAllCidadeByUf", ex);
                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }
            finally
            {
                FabricaConexao.fecharConexao();
            }
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public IList<SelectItem> findAllCidadeIbge()
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findAllCidadeIbge");

            NpgsqlConnection dbConnection = FabricaConexao.getConexao();
            ICidadeIbgeDao cidadeIbgeDao = FabricaDao.getCidadeIbgeDao();

            try
            {
                return cidadeIbgeDao.findAllCidade(dbConnection);

            }
            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findAllCidadeIbge", ex);
                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }
            finally
            {
                FabricaConexao.fecharConexao();
            }
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public CidadeIbge findCidadeIbgeById(Int64 id)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findCidadeIbgeById");

            NpgsqlConnection dbConnection = FabricaConexao.getConexao();
            ICidadeIbgeDao cidadeIbgeDao = FabricaDao.getCidadeIbgeDao();

            try
            {
                return cidadeIbgeDao.findCidadeById(id, dbConnection);

            }
            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findCidadeIbgeById", ex);
                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }
            finally
            {
                FabricaConexao.fecharConexao();
            }
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public SelectItem findItemCidadeById(Int64 id)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findItemCidadeById");

            NpgsqlConnection dbConnection = FabricaConexao.getConexao();
            ICidadeIbgeDao cidadeIbgeDao = FabricaDao.getCidadeIbgeDao();

            try
            {
                return cidadeIbgeDao.findItemCidadeById(id, dbConnection);

            }
            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findItemCidadeById", ex);
                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }
            finally
            {
                FabricaConexao.fecharConexao();
            }
        }
        [MethodImpl(MethodImplOptions.Synchronized)]
        
        public List<Cliente> findAllPrestadorByCliente(Cliente cliente, Cliente prestador)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findAllPrestadorByCliente");

            NpgsqlConnection dbConnection = FabricaConexao.getConexao();
            
            IClienteDao clienteDao = FabricaDao.getClienteDao();

            try
            {
                return clienteDao.findAllPrestadorByCliente(cliente, prestador, dbConnection);

            }
            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findAllPrestadorByCliente", ex);
                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }
            finally
            {
                FabricaConexao.fecharConexao();
            }
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public void changeSituacao(Cnae cnae)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método changeSituacao");

            NpgsqlConnection dbConnection = FabricaConexao.getConexao();

            IClienteDao clienteDao = FabricaDao.getClienteDao();

            NpgsqlTransaction transaction = dbConnection.BeginTransaction();
            try
            {

                clienteDao.changeSituacao(cnae, dbConnection);
            
                transaction.Commit();
            
            }

            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - changeSituacao", ex);
                transaction.Rollback();
                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }

            finally
            {
                FabricaConexao.fecharConexao();
            }

        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public Cliente findClienteByRazaoSocical(String razaoSocial)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findClienteByRazaoSocical");

            NpgsqlConnection dbConnection = FabricaConexao.getConexao();

            IClienteDao clienteDao = FabricaDao.getClienteDao();
            IMedicoDao medicoDao = FabricaDao.getMedicoDao();

            try
            {
                Cliente cliente = clienteDao.findClienteByRazaoSocial(razaoSocial, dbConnection);

                if (cliente != null)
                {
                    // setando dados da matriz e credenciada e coordenador do cliente.
                    if (cliente.Matriz != null)
                    {
                        cliente.Matriz = clienteDao.findById((Int64)cliente.Matriz.Id, dbConnection);
                    }

                    if (cliente.CredenciadaCliente != null)
                    {
                        cliente.CredenciadaCliente = clienteDao.findById((Int64)cliente.CredenciadaCliente.Id, dbConnection);
                    }

                    if (cliente.Coordenador != null)
                    {
                        cliente.Coordenador = medicoDao.findById((Int64)cliente.Coordenador.Id, dbConnection);
                    }
                }

                return cliente;
            }
            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findClienteByRazaoSocical", ex);
                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }
            finally
            {
                FabricaConexao.fecharConexao();
            }
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public DataSet findAllUnidadeByCliente(Cliente cliente)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findAllUnidadeByCliente");

            NpgsqlConnection dbConnection = FabricaConexao.getConexao();
            IClienteDao clienteDao = FabricaDao.getClienteDao();

            try
            {
                return clienteDao.findAllUnidadeByCliente(cliente, dbConnection);

            }
            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findAllUnidadeByCliente", ex);
                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }
            finally
            {
                FabricaConexao.fecharConexao();
            }
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public Boolean findClienteHaveUnidade(Cliente cliente)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findClienteHaveUnidade");

            NpgsqlConnection dbConnection = FabricaConexao.getConexao();
            IClienteDao clienteDao = FabricaDao.getClienteDao();

            try
            {
                return clienteDao.findClienteHaveUnidade(cliente, dbConnection);

            }
            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findAllUnidadeByCliente", ex);
                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }
            finally
            {
                FabricaConexao.fecharConexao();
            }
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public List<ClienteFuncao> findAllFuncaoByCliente(Cliente cliente)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findAllFuncaoByCliente");

            NpgsqlConnection dbConnection = FabricaConexao.getConexao();
            IClienteFuncaoDao clienteFuncaoDao = FabricaDao.getClienteFuncaoDao();

            try
            {
                return clienteFuncaoDao.findAtivosByClienteList(cliente, dbConnection);

            }
            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findAllUnidadeByCliente", ex);
                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }
            finally
            {
                FabricaConexao.fecharConexao();
            }
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public DataSet findCnaeNotInCliente(Cnae cnae, Cliente cliente)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findCnaeNotInCliente");

            NpgsqlConnection dbConnection = FabricaConexao.getConexao();
            IClienteDao clienteDao = FabricaDao.getClienteDao();

            try
            {
                return clienteDao.findCnaeNotInCliente(cnae, cliente, dbConnection);

            }
            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findCnaeNotInCliente", ex);
                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }
            finally
            {
                FabricaConexao.fecharConexao();
            }
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public ClienteCnae incluirCnaeCliente(ClienteCnae clienteCnae)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método incluirCnaeCliente");

            NpgsqlConnection dbConnection = FabricaConexao.getConexao();
            
            IClienteCnaeDao clienteCnaeDao = FabricaDao.getClienteCnaeDao();

            NpgsqlTransaction transaction = dbConnection.BeginTransaction();

            try
            {
                clienteCnae = clienteCnaeDao.incluirClienteCnae(clienteCnae, dbConnection);
                transaction.Commit();

                return clienteCnae;
            }
            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - incluirCnaeCliente", ex);
                transaction.Rollback();
                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }
            finally
            {
                FabricaConexao.fecharConexao();
            }

        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public List<ClienteCnae> findClienteCnaeByCliente(Cliente cliente)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findAllCnaeByCliente");

            NpgsqlConnection dbConnection = FabricaConexao.getConexao();

            IClienteCnaeDao clienteCnaeDao = FabricaDao.getClienteCnaeDao();

            try
            {
                return clienteCnaeDao.findClienteCnaeByCliente(cliente, dbConnection);
            }
            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findAllCnaeByCliente", ex);
                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }
            finally
            {
                FabricaConexao.fecharConexao();
            }

        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public void deleteClienteCnaeInCliente(ClienteCnae clienteCnae)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método deleteClienteCnae");

            NpgsqlConnection dbConnection = FabricaConexao.getConexao();

            IClienteCnaeDao clienteCnaeDao = FabricaDao.getClienteCnaeDao();

            NpgsqlTransaction transaction = dbConnection.BeginTransaction();

            try
            {
                clienteCnaeDao.deleteClienteCnaeInCliente(clienteCnae, dbConnection);
                transaction.Commit();

            }
            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - deleteClienteCnae", ex);
                transaction.Rollback();
                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }
            finally
            {
                FabricaConexao.fecharConexao();
            }

        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public void updateClienteCnae(ClienteCnae clienteCnae, String situacao)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método updateClienteCnae");

            NpgsqlConnection dbConnection = FabricaConexao.getConexao();

            IClienteCnaeDao clienteCnaeDao = FabricaDao.getClienteCnaeDao();

            NpgsqlTransaction transaction = dbConnection.BeginTransaction();

            try
            {
                clienteCnaeDao.updateClienteCnae(clienteCnae, dbConnection);
                transaction.Commit();

            }
            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - updateClienteCnae", ex);
                transaction.Rollback();
                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }
            finally
            {
                FabricaConexao.fecharConexao();
            }

        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public DataSet findCidadesByUf(String uf, CidadeIbge cidade)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findCidadesByUf");

            NpgsqlConnection dbConnection = FabricaConexao.getConexao();

            ICidadeIbgeDao cidadeDao = FabricaDao.getCidadeIbgeDao();

            try
            {
                return cidadeDao.findAllCidadeByUfByCidade(uf, cidade, dbConnection);

            }
            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findCidadesByUf", ex);
                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }
            finally
            {
                FabricaConexao.fecharConexao();
            }

        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public DataSet findAllClienteByCNPJAptoContrato(String cnpj)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findAllClienteByCNPJAptoContrato");

            NpgsqlConnection dbConnection = FabricaConexao.getConexao();
            
            IClienteDao clienteDao = FabricaDao.getClienteDao();

            try
            {
                return clienteDao.findAllClienteByCnpjAptoContrato(cnpj, dbConnection);

            }
            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findAllClienteByCNPJAptoContrato", ex);
                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }
            finally
            {
                FabricaConexao.fecharConexao();
            }
        }


        [MethodImpl(MethodImplOptions.Synchronized)]
        public DataSet findAllCredenciadaByCliente(Cliente cliente)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findAllCredenciadaByCliente");

            NpgsqlConnection dbConnection = FabricaConexao.getConexao();

            IClienteDao clienteDao = FabricaDao.getClienteDao();

            try
            {
                return clienteDao.findAllCredenciadasByCliente(cliente, dbConnection);

            }
            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findAllCredenciadaByCliente", ex);
                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }
            finally
            {
                FabricaConexao.fecharConexao();
            }
        }


        #region Ramo

        [MethodImpl(MethodImplOptions.Synchronized)]
        public DataSet findRamoByFilter(Ramo ramo)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findRamoByFilter");

            NpgsqlConnection dbConnection = FabricaConexao.getConexao();
            IRamoDao ramoDao = FabricaDao.getRamoDao();

            try
            {
                return ramoDao.findRamoByFilter(ramo, dbConnection);

            }
            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findRamoByFilter", ex);
                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }
            finally
            {
                FabricaConexao.fecharConexao();
            }
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public Ramo insertRamo(Ramo ramo)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método insertRamo");

            Ramo newRamo = null;
            
            NpgsqlConnection dbConnection = FabricaConexao.getConexao();
            IRamoDao ramoDao = FabricaDao.getRamoDao();
            NpgsqlTransaction transaction = dbConnection.BeginTransaction();

            try
            {
                /* verificando se poderá ser inserido um ramo de atividade */

                if (ramoDao.findByDescricao(ramo.Descricao, dbConnection) != null)
                    throw new RamoExecption(RamoExecption.msg1);

                newRamo = ramoDao.insert(ramo, dbConnection);

                transaction.Commit();

            }
            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - insertRamo", ex);
                transaction.Rollback();
                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }
            finally
            {
                FabricaConexao.fecharConexao();
            }

            return ramo;
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public Ramo findRamoById(Int64 id)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findRamoById");

            NpgsqlConnection dbConnection = FabricaConexao.getConexao();

            IRamoDao ramoDao = FabricaDao.getRamoDao();

            try
            {
                return ramoDao.findById(id, dbConnection);
            }
            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findRamoById", ex);
                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }
            finally
            {
                FabricaConexao.fecharConexao();
            }
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public Ramo updateRamo(Ramo ramo)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método updateRamo");

            NpgsqlConnection dbConnection = FabricaConexao.getConexao();
            IRamoDao ramoDao = FabricaDao.getRamoDao();
            NpgsqlTransaction transaction = dbConnection.BeginTransaction();

            try
            {
                ramoDao.update(ramo, dbConnection);
                transaction.Commit();

            }
            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - updateRamo", ex);
                transaction.Rollback();
                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }
            finally
            {
                FabricaConexao.fecharConexao();
            }

            return ramo;
        }

        #endregion

        [MethodImpl(MethodImplOptions.Synchronized)]
        public Contato insertContato(Contato contato)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método insertContato");

            NpgsqlConnection dbConnection = FabricaConexao.getConexao();
            IContatoDao contatoDao = FabricaDao.getContatoDao();
            NpgsqlTransaction transaction = dbConnection.BeginTransaction();

            Contato contatoNew = null;

            try
            {
                contatoNew = contatoDao.insert(contato, dbConnection);
                transaction.Commit();

            }
            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - insertContato", ex);
                transaction.Rollback();
                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }
            finally
            {
                FabricaConexao.fecharConexao();
            }

            return contatoNew;

        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public Contato updateContato(Contato contato)
        {
            
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método updateContato");
            
            NpgsqlConnection dbConnection = FabricaConexao.getConexao();
            NpgsqlTransaction transaction = dbConnection.BeginTransaction();
            IContatoDao contatoDao = FabricaDao.getContatoDao();

            Contato contatoUpdate;

            try
            {
                contatoUpdate = contatoDao.update(contato, dbConnection);
                transaction.Commit();


            }
            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - updateContato", ex);
                transaction.Rollback();
                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }
            finally
            {
                FabricaConexao.fecharConexao();
            }
            return contatoUpdate;
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public void deleteContato(Contato contato)
        {

            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método deleteContato");

            NpgsqlConnection dbConnection = FabricaConexao.getConexao();
            NpgsqlTransaction transaction = dbConnection.BeginTransaction();
            IContatoDao contatoDao = FabricaDao.getContatoDao();

            try
            {
                contatoDao.delete(contato, dbConnection);
                transaction.Commit();

            }
            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - deleteContato", ex);
                transaction.Rollback();
                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }
            finally
            {
                FabricaConexao.fecharConexao();
            }
        }


        [MethodImpl(MethodImplOptions.Synchronized)]
        public List<Contato> findByCliente(Cliente cliente)
        {

            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findByCliente");

            NpgsqlConnection dbConnection = FabricaConexao.getConexao();
            IContatoDao contatoDao = FabricaDao.getContatoDao();

            try
            {
                return contatoDao.findByCliente(cliente , dbConnection);

            }
            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findByCliente", ex);
                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }
            finally
            {
                FabricaConexao.fecharConexao();
            }
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public DataSet findTipoContato()
        {

            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findTipoContato");

            NpgsqlConnection dbConnection = FabricaConexao.getConexao();
            IContatoDao contatoDao = FabricaDao.getContatoDao();

            try
            {
                return contatoDao.findTipoContato(dbConnection);

            }
            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findTipoContato", ex);
                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }
            finally
            {
                FabricaConexao.fecharConexao();
            }
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public List<VendedorCliente> findClienteVendedorByCliente(Cliente cliente)
        {

            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findClienteVendedorByCliente");
            
            NpgsqlConnection dbConnection = FabricaConexao.getConexao();
            IClienteVendedorDao clienteVendedorDao = FabricaDao.getClienteVendedorDao();

            try
            {
                return clienteVendedorDao.findByCliente(cliente, dbConnection);

            }
            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findClienteVendedorByCliente", ex);
                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }
            finally
            {
                FabricaConexao.fecharConexao();
            }
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public CentroCusto insertCentroCusto(CentroCusto centroCusto)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método insertCentroCusto");
            
            NpgsqlConnection dbConnection = FabricaConexao.getConexao();
            NpgsqlTransaction transaction = dbConnection.BeginTransaction();
            
            ICentroCustoDao centroCustoDao = FabricaDao.getCentroCustoDao();

            try
            {
                centroCusto = centroCustoDao.insert(centroCusto, dbConnection);
                transaction.Commit();

            }
            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - insertCentroCusto", ex);
                transaction.Rollback();
                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }
            finally
            {
                FabricaConexao.fecharConexao();
            }
            return centroCusto;
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public CentroCusto updateCentroCusto(CentroCusto centroCusto)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método updateCentroCusto");
            
            NpgsqlConnection dbConnection = FabricaConexao.getConexao();
            NpgsqlTransaction transaction = dbConnection.BeginTransaction();
            
            ICentroCustoDao centroCustoDao = FabricaDao.getCentroCustoDao();

            try
            {
                centroCusto = centroCustoDao.update(centroCusto, dbConnection);
                transaction.Commit();

            }
            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - updateCentroCusto", ex);
                transaction.Rollback();
                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }
            finally
            {
                FabricaConexao.fecharConexao();
            }
            return centroCusto;
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public List<CentroCusto> findCentroCustoAtivoByCliente(Cliente cliente)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findCentroCustoAtivoByCliente");

            NpgsqlConnection dbConnection = FabricaConexao.getConexao();

            ICentroCustoDao centroCustoDao = FabricaDao.getCentroCustoDao();
            IClienteDao clienteDao = FabricaDao.getClienteDao();

            List<CentroCusto> centroCustoCliente = new List<CentroCusto>();

            try
            {
                centroCustoCliente = centroCustoDao.findAtivosByCliente(cliente, dbConnection);
                centroCustoCliente.ForEach(delegate (CentroCusto centroCusto) {
                    centroCusto.Cliente = clienteDao.findById((long)centroCusto.Cliente.Id, dbConnection);
                });

            }
            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findCentroCustoAtivoByCliente", ex);
                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }
            finally
            {
                FabricaConexao.fecharConexao();
            }
            return centroCustoCliente;
        }


        [MethodImpl(MethodImplOptions.Synchronized)]
        public void deleteCentroCusto(CentroCusto centroCusto)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método deleteCentroCusto");

            NpgsqlConnection dbConnection = FabricaConexao.getConexao();
            NpgsqlTransaction transaction = dbConnection.BeginTransaction();

            ICentroCustoDao centroCustoDao = FabricaDao.getCentroCustoDao();

            try
            {
                /* testando para saber se o centro de custo pode ser 
                 * excluido ou desativado */

                if (centroCustoDao.isUsed(centroCusto, dbConnection))
                {
                    centroCusto.Situacao = ApplicationConstants.DESATIVADO;
                    centroCustoDao.update(centroCusto, dbConnection);
                }
                else
                    centroCustoDao.delete(centroCusto, dbConnection);

                transaction.Commit();

            }
            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - deleteCentroCusto", ex);
                transaction.Rollback();
                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }
            finally
            {
                FabricaConexao.fecharConexao();
            }
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public bool centroCustoIdUsed(CentroCusto centroCusto)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método centroCustoIdUsed");

            NpgsqlConnection dbConnection = FabricaConexao.getConexao();

            ICentroCustoDao centroCustoDao = FabricaDao.getCentroCustoDao();

            try
            {
                return centroCustoDao.isUsed(centroCusto, dbConnection);

            }
            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - centroCustoIdUsed", ex);
                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }
            finally
            {
                FabricaConexao.fecharConexao();
            }
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public CentroCusto findCentroCustoById(long id)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findCentroCustoById");

            NpgsqlConnection dbConnection = FabricaConexao.getConexao();

            ICentroCustoDao centroCustoDao = FabricaDao.getCentroCustoDao();
            IClienteDao clienteDao = FabricaDao.getClienteDao();
            CentroCusto centroCusto = null;

            try
            {
                centroCusto = centroCustoDao.findById(id, dbConnection);
                centroCusto.Cliente = clienteDao.findById((long)centroCusto.Cliente.Id, dbConnection);
            }
            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findCentroCustoById", ex);
                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }
            finally
            {
                FabricaConexao.fecharConexao();
            }
            return centroCusto;
        }

        

    }
}
