﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SWS.IDao;
using SWS.Entidade;
using Npgsql;
using SWS.Excecao;
using System.Data;
using SWS.Dao;
using SWS.View.Resources;
using SWS.Helper;
using SWS.View.ViewHelper;
using System.Runtime.CompilerServices;

namespace SWS.Facade
{
    class PpraFacade
    {
        public static PpraFacade ppraFacade = null;

        private PpraFacade() { }

        public static PpraFacade getInstance()
        {
            if (ppraFacade == null)
            {
                ppraFacade = new PpraFacade();
            }

            return ppraFacade;
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public Estudo findPpraById(Int64 id)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findPpraById");
            
            NpgsqlConnection dbConnection = FabricaConexao.getConexao();

            Estudo ppraRetorno = new Estudo();
            Cronograma cronograma = new Cronograma();
            VendedorCliente vendedorCliente = new VendedorCliente();
            Usuario engenheiro;
            Usuario tecnico;
            Cliente clienteContratante = new Cliente();
           
            
            IEstudoDao ppraDao = FabricaDao.getEstudoDao();
            ICronogramaDao cronogramaDao = FabricaDao.getCronogramaDao();
            ICronogramaAtividadeDao cronogramaAtividadeDao = FabricaDao.getCronogramaAtividadeDao();
            IClienteVendedorDao vendedorClienteDao = FabricaDao.getClienteVendedorDao();
            IUsuarioDao usuarioDao = FabricaDao.getUsuarioDao();
            IClienteDao clienteDao = FabricaDao.getClienteDao();

            try
            {
                // buscar estudo procurado
                ppraRetorno = ppraDao.findById(id, dbConnection);
                
                // buscar objeto cronograma com as listas de atividades

                if (ppraRetorno.Cronograma != null)
                {
                    cronograma = cronogramaDao.findById((Int64)ppraRetorno.Cronograma.Id, dbConnection);
                    cronograma.CronogramaAtividade = cronogramaAtividadeDao.findByCronograma(ppraRetorno.Cronograma, dbConnection);
                    ppraRetorno.Cronograma = cronograma;
                }

                //buscar objeto vendedorCliente
                vendedorCliente = vendedorClienteDao.findById((Int64)ppraRetorno.VendedorCliente.getId(), dbConnection);
                ppraRetorno.VendedorCliente = vendedorCliente;

                // buscando dados do engenheiro
                if (ppraRetorno.Engenheiro !=null)
                {
                   engenheiro = usuarioDao.findById((Int64)ppraRetorno.Engenheiro.Id, dbConnection);
                   ppraRetorno.Engenheiro = engenheiro;
                }
                
                // buscando dados do tecnico

                if (ppraRetorno.Tecno != null)
                {
                    tecnico = usuarioDao.findById((Int64)ppraRetorno.Tecno.Id, dbConnection);
                    ppraRetorno.Tecno = tecnico;
                }

                // buscando cliente contratante

                if (ppraRetorno.ClienteContratante != null)
                {
                    clienteContratante = clienteDao.findById((Int64)ppraRetorno.ClienteContratante.Id, dbConnection);
                    ppraRetorno.ClienteContratante = clienteContratante;
                }


                return ppraRetorno;

            }
            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findPpraById", ex);
                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }
            finally
            {
                FabricaConexao.fecharConexao();
            }
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public DataSet findPpraByFilter(Estudo ppra, DateTime? dataCriacaoFinal)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findEstudoByFilter");

            NpgsqlConnection dbConnection = FabricaConexao.getConexao();
            IEstudoDao ppraDao = FabricaDao.getEstudoDao();

            try
            {
                return ppraDao.findByFilter(ppra, dataCriacaoFinal, dbConnection);

            }
            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findEstudoByFilter", ex);
                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }
            finally
            {
                FabricaConexao.fecharConexao();
            }
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public Estudo incluirPpra(Estudo ppra)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método incluirEstudo");

            NpgsqlConnection dbConnection = FabricaConexao.getConexao(); // criando conexao com o banco de dados.
            
            HashSet<ClienteCnae> listaCnaeCliente = new HashSet<ClienteCnae>();
            HashSet<CnaeEstudo> ListaCnaePpra = new HashSet<CnaeEstudo>();

            IEstudoDao ppraDao = FabricaDao.getEstudoDao(); // pegando assinatura do metodo.
            ICnaeEstudoDao cnaePpraDao = FabricaDao.getCnaeEstudoDao();
            IClienteCnaeDao cnaeClienteDao = FabricaDao.getClienteCnaeDao();
            IRevisaoDao revisaoDao = FabricaDao.getRevisaoDao();
            ICronogramaDao cronogramaDao = FabricaDao.getCronogramaDao();
            Cnae cnaePrincipalContratante = new Cnae();
            CnaeEstudo cnaePpraContratante = new CnaeEstudo();


            NpgsqlTransaction transaction = dbConnection.BeginTransaction();
            try
            {
                /* incluindo o cronograma inicial do estudo */
                ppra.Cronograma = cronogramaDao.insert(ppra.Cronograma, dbConnection);

                ppra = ppraDao.insert(ppra, dbConnection); 
               
                
                // Incluindo cnae do cliente no estudo
                listaCnaeCliente = cnaeClienteDao.findAllCnaeClienteByClienteCnae(ppra.VendedorCliente.getCliente(), dbConnection);

                
                //Validar necessidade de pelo menos 1 cnae para o cliente
                if (listaCnaeCliente == null || listaCnaeCliente.Count == 0)
                {
                    throw new ClienteException(ClienteException.msg4);
                }

                foreach (ClienteCnae clienteCnae in listaCnaeCliente)
                {
                    CnaeEstudo cnaePpra = new CnaeEstudo(null, clienteCnae.Cnae, ppra, false, false);
                    String FlagCnaePrincipal = null;

                    FlagCnaePrincipal = Convert.ToString(clienteCnae.Flag_pr);

                    /*
                    if (String.Equals(FlagCnaePrincipal, ApplicationConstants.PRINCIPAL))
                    {
                        cnaePpra.FlagPrincipal = FlagCnaePrincipal;
                    }
                     */

                    // incluindo o cnae na tabela de cnae_ppra;

                    cnaePpraDao.insertCnaeEstudo(cnaePpra,dbConnection);
                }

                // preenchimento do cnaeEstudo caso o estudo tenha um contratado

                // excluir o relacionamento do cnae do cliente contratante.
                cnaePpraDao.deleteCnaeEstudoDaoContratante(ppra, dbConnection);


                if (ppra.ClienteContratante != null)
                {
                    
                    /*verificando se o cliente contratante é uma unidade. 
                     * caso seja unidade então não há a necessidade de incluir 
                    * o relacionamento com o cnae porque a unidade não possui cnae 
                    * alteracao em 12/05/2015 welbert santos
                    */

                    if (!ppra.ClienteContratante.Unidade)
                    {
                        // metodo para buscar o cnae principal do cliente contratante.
                        cnaePrincipalContratante = cnaeClienteDao.findCnaePrincipalByCliente(ppra.ClienteContratante, dbConnection);

                        //Validar necessidade de pelo menos 1 cnae para o cliente contratante
                        if (cnaePrincipalContratante == null || cnaePrincipalContratante.Id == null)
                        {
                            throw new ClienteException(ClienteException.msg5);
                        }

                        cnaePpraContratante.Cnae = cnaePrincipalContratante;
                        cnaePpraContratante.Estudo = ppra;
                        cnaePpraContratante.FlagPrincipalContratante = true;

                        // metodo para incluir no estudo o cnae do contratante
                        cnaePpraDao.insertCnaeEstudoContratante(cnaePpraContratante, dbConnection);
                    }
                }

                revisaoDao.incluir(new Revisao(null, ppra, "Emissão Original", ppra, ppra, "PPRA", ppra.CodigoEstudo.Substring(ppra.CodigoEstudo.Length - 2, 2), null, false), dbConnection);
                
                transaction.Commit();
            }
            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - novoPcmso", ex);
                transaction.Rollback();
                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }
            finally
            {
                FabricaConexao.fecharConexao();
            }

            return ppra;
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public Ghe findGheById(Int64? idGhe)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findGheById");

            NpgsqlConnection dbConnection = FabricaConexao.getConexao();
            IGheDao gheDao = FabricaDao.getGheDao();
            try
            {
                Ghe ghe = gheDao.findById(idGhe, dbConnection);
                return ghe;

            }
            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findGheById", ex);
                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }
            finally
            {
                FabricaConexao.fecharConexao();
            }
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public Estudo alteraPpra(Estudo ppra)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método alteraPpra");

            NpgsqlConnection dbConnection = FabricaConexao.getConexao();
            IEstudoDao ppraDao = FabricaDao.getEstudoDao();
            ICnaeEstudoDao cnaePpraDao = FabricaDao.getCnaeEstudoDao();
            IClienteCnaeDao cnaeClienteDao = FabricaDao.getClienteCnaeDao();
            Cnae cnaePrincipalContratante = new Cnae();
            CnaeEstudo cnaePpraContratante = new CnaeEstudo();
           

            NpgsqlTransaction transaction = dbConnection.BeginTransaction();
            try
            {
                ppra = ppraDao.update(ppra, dbConnection);

                // excluir o relacionamento do cnae do cliente contratante.
                cnaePpraDao.deleteCnaeEstudoDaoContratante(ppra, dbConnection);

                if (ppra.ClienteContratante != null)
                {
                    
                    // metodo para buscar o cnae principal do cliente contratante.
                    cnaePrincipalContratante = cnaeClienteDao.findCnaePrincipalByCliente(ppra.ClienteContratante, dbConnection);

                    cnaePpraContratante.Cnae = cnaePrincipalContratante;
                    cnaePpraContratante.Estudo = ppra;
                    cnaePpraContratante.FlagPrincipalContratante = true; 

                    // metodo para incluir no estudo o cnae do contratante
                    cnaePpraDao.insertCnaeEstudoContratante(cnaePpraContratante, dbConnection);

                }

                transaction.Commit();
            }
            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - alterarPpra", ex);
                transaction.Rollback();
                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }
            finally
            {
                FabricaConexao.fecharConexao();
            }

            return ppra;
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public Estudo incluiCronograma(Estudo ppra)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método incluiCronograma");

            NpgsqlConnection dbConnection = FabricaConexao.getConexao();
            ICronogramaDao cronogramaDao = FabricaDao.getCronogramaDao();
            IEstudoDao ppraDao = FabricaDao.getEstudoDao();
            ICronogramaAtividadeDao cronogramaAtividadeDao = FabricaDao.getCronogramaAtividadeDao();
            NpgsqlTransaction transaction = dbConnection.BeginTransaction();
            try
            {
                ppra.Cronograma = cronogramaDao.insert(ppra.Cronograma, dbConnection);
                
                // criando relacionamento entre cronograma atividade
                foreach (CronogramaAtividade cronogramaAtividade in ppra.Cronograma.CronogramaAtividade)
                {
                    cronogramaAtividade.Cronograma = ppra.Cronograma;

                    cronogramaAtividadeDao.insert(cronogramaAtividade, dbConnection);
                }

                // setando id cronograma no estudo.

                ppra = ppraDao.updateCronogramaEstudo(ppra, dbConnection);

                transaction.Commit();
            }
            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - IncluiCronograma", ex);
                transaction.Rollback();
                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }
            finally
            {
                FabricaConexao.fecharConexao();
            }

            return ppra;
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public Ghe incluirGhePpra(Ghe ghe)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método incluirGheEstudo");

            Ghe gheNovo = null; // iniciando objeto do tipo Ghe como nulo.
            Ghe gheProcurado = null;
            
            NpgsqlConnection dbConnection = FabricaConexao.getConexao(); // criando conexao com o banco de dados.
            
            IGheDao gheDao = FabricaDao.getGheDao(); // pegando assinatura do metodo.
            IGheNotaDao gheNormaDao = FabricaDao.getGheNormaDao();
            
            NpgsqlTransaction transaction = dbConnection.BeginTransaction();
            try
            {
                gheProcurado = gheDao.findByDescricao(ghe, dbConnection);

                if (gheProcurado == null)
                {
                    //Incluindo ghe
                    gheNovo = gheDao.insert(ghe, dbConnection); // passando para camada Dao para transacao com o banco.
                    
                    //Incluindo normas no GHE

                    if (ghe.Norma != null) // testando se existe algum elemento norma incluido no GHE
                    {
                        
                        GheNota ghenorma = new GheNota();
                        
                        foreach (Nota norma in ghe.Norma)
                        {
                            ghenorma.Ghe = ghe;
                            ghenorma.Nota = norma;
                        
                            gheNormaDao.insert(ghenorma, dbConnection);
                            
                        }
                    }
                }
                else
                {
                    throw new DescricaoGheJaCadastradaException(DescricaoGheJaCadastradaException.msg);
                }
                
                transaction.Commit();
            }
            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - novoGhe", ex);
                transaction.Rollback();
                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }
            finally
            {
                FabricaConexao.fecharConexao();
            }

            return gheNovo;
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public DataSet findGhePpra(Estudo ppra)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findGhePpra");

            NpgsqlConnection dbConnection = FabricaConexao.getConexao();

            IGheDao gheDao = FabricaDao.getGheDao();

            try
            {
                return gheDao.findAtivoByEstudo(ppra, dbConnection);

            }
            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findAllGhePpra", ex);
                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }
            finally
            {
                FabricaConexao.fecharConexao();
            }
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public Ghe updateGhe(Ghe ghe)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método alterarGhe");

            NpgsqlConnection dbConnection = FabricaConexao.getConexao();
            IGheDao gheDao = FabricaDao.getGheDao();
            NpgsqlTransaction transaction = dbConnection.BeginTransaction();

            try
            {
                ghe = gheDao.update(ghe, dbConnection);

                transaction.Commit();
            }
            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - alterarGhe", ex);
                transaction.Rollback();
                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }
            finally
            {
                FabricaConexao.fecharConexao();
            }

            return ghe;
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public void deleteGheById(Int64 idGhe, Estudo ppra)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método deleteGheById");

            NpgsqlConnection dbConnection = FabricaConexao.getConexao();
            IGheDao gheDao = FabricaDao.getGheDao();
            NpgsqlTransaction transaction = dbConnection.BeginTransaction();
            try
            {
                gheDao.deleteByEstudo(idGhe, ppra, dbConnection);

                transaction.Commit();
            }
            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - excluiGhe", ex);
                transaction.Rollback();
                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }
            finally
            {
                FabricaConexao.fecharConexao();
            }
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public Ghe alteraGhePpra(Ghe ghe)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método alteraGhePpra");

            Ghe gheAlterado = null; // iniciando objeto do tipo Ghe como nulo.
            Ghe gheProcurado = null;

            NpgsqlConnection dbConnection = FabricaConexao.getConexao(); // criando conexao com o banco de dados.

            IGheDao gheDao = FabricaDao.getGheDao(); // pegando assinatura do metodo.
            IGheNotaDao gheNormaDao = FabricaDao.getGheNormaDao();

            NpgsqlTransaction transaction = dbConnection.BeginTransaction();
            try
            {
                gheProcurado = gheDao.findByDescricao(ghe, dbConnection);

                if (gheProcurado == null || gheProcurado.Id == ghe.Id)
                {
                    //Alterando GHE
                    gheAlterado = gheDao.update(ghe, dbConnection); // passando para camada Dao para transacao com o banco.
                }
                else
                {
                    throw new DescricaoGheJaCadastradaException(DescricaoGheJaCadastradaException.msg);
                }
                transaction.Commit();
            }
            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - novoGhe", ex);
                transaction.Rollback();
                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }
            finally
            {
                FabricaConexao.fecharConexao();
            }

            return gheAlterado;
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public void deleteNormaGhe(GheNota gheNorma)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método deleteNormaGhe");

            NpgsqlConnection dbConnection = FabricaConexao.getConexao();

            IGheNotaDao gheNormaDao = FabricaDao.getGheNormaDao();

            NpgsqlTransaction transaction = dbConnection.BeginTransaction();
            
            try
            {
                gheNormaDao.delete(gheNorma, dbConnection);

                transaction.Commit();
            }
            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - deleteNormaGhe", ex);
                transaction.Rollback();
                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }
            finally
            {
                FabricaConexao.fecharConexao();
            }
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public DataSet findAllFonteAtivaByGhe(Fonte fonte, Ghe ghe)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findAllFonteAtivaByGhe");

            NpgsqlConnection dbConnection = FabricaConexao.getConexao();

            IFonteDao fonteDao = FabricaDao.getFonteDao();

            try
            {
                return fonteDao.findAllAtivaByGhe(fonte, ghe, dbConnection);

            }
            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findAllFonteAtiva", ex);
                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }
            finally
            {
                FabricaConexao.fecharConexao();
            }
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public DataSet findAllFonteByGheDataSet(Ghe ghe)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findAllFonteByGheDataSet");

            NpgsqlConnection dbConnection = FabricaConexao.getConexao();
            IGheFonteDao gheFonteDao = FabricaDao.getGheFonteDao();
            try
            {
                return gheFonteDao.findAllByGhe(ghe, dbConnection);

            }
            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findAllFonteByGhe", ex);
                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }
            finally
            {
                FabricaConexao.fecharConexao();
            }
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public HashSet<GheFonteAgente> findAllGheFonteAgenteByGheFonte(GheFonte gheFonte)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findAllGheFonteAgenteByGheFonte");

            NpgsqlConnection dbConnection = FabricaConexao.getConexao();
            IGheFonteAgenteDao gheFonteAgenteDao = FabricaDao.getGheFonteAgenteDao();
            try
            {
                return gheFonteAgenteDao.findAtivosByGheFonteHash(gheFonte, dbConnection);

            }
            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findAllGheFonteAgenteByGheFonte", ex);
                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }
            finally
            {
                FabricaConexao.fecharConexao();
            }
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public HashSet<GheFonte> findAllFonteByGhe(Ghe ghe)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findAllFonteByGhe");

            NpgsqlConnection dbConnection = FabricaConexao.getConexao();
            HashSet<GheFonte> setGheFonte = new HashSet<GheFonte>();

            IGheFonteDao gheFonteDao = FabricaDao.getGheFonteDao();
            try
            {
                
                foreach (GheFonte gheFonte in gheFonteDao.findAllAtivoByGhe(ghe, dbConnection))
                {
                    setGheFonte.Add(new GheFonte(gheFonte.getId(), gheFonte.getFonte(), gheFonte.getGhe(),gheFonte.getPrincipal(), gheFonte.Situacao));
                }
                
                return setGheFonte;

            }
            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findAllFonteByGhe", ex);
                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }

            finally
            {
                FabricaConexao.fecharConexao();
            }
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public GheFonte incluiGheFonte(GheFonte gheFonte)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método incluiGheFonte");

            NpgsqlConnection dbConnection = FabricaConexao.getConexao();

            IGheFonteDao gheFonteDao = FabricaDao.getGheFonteDao();

            try
            {
                gheFonte = gheFonteDao.insert(gheFonte, dbConnection);

                return gheFonte;
            }
            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - incluiGheFonte", ex);
                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }

            finally
            {
                FabricaConexao.fecharConexao();
            }
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public void deleteGheFonteById(GheFonte gheFonte)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método deleteGheFonteById");

            NpgsqlConnection dbConnection = FabricaConexao.getConexao();
            IGheFonteDao gheFonteDao = FabricaDao.getGheFonteDao();

            NpgsqlTransaction transaction = dbConnection.BeginTransaction();

            try
            {
                gheFonteDao.deleteGheFonte(gheFonte, dbConnection);

                transaction.Commit();
            }
            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - deleteGheFonteById", ex);
                transaction.Rollback();
                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }
            finally
            {
                FabricaConexao.fecharConexao();
            }
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public DataSet findAllGradExposicao()
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findAllGradExposicao");

            NpgsqlConnection dbConnection = FabricaConexao.getConexao();

            IGradExposicaoDao gradExposicaoDao = FabricaDao.getGradExposicaoDao();

            try
            {
                return gradExposicaoDao.findAllGradExposicao(dbConnection);

            }
            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findAllGradExposicao", ex);
                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }
            finally
            {
                FabricaConexao.fecharConexao();
            }
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public DataSet findAllGradEfeito()
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findAllGradEfeito");

            NpgsqlConnection dbConnection = FabricaConexao.getConexao();

            IGradEfeitoDao gradEfeitoDao = FabricaDao.getGradEfeitoDao();

            try
            {
                return gradEfeitoDao.findAllGradEfeito(dbConnection);

            }
            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findAllGradEfeito", ex);
                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }
            finally
            {
                FabricaConexao.fecharConexao();
            }
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public GradExposicao findGradExposicaoByGradExposicao(GradExposicao gradExposicao)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findGradExposicaoByGradExposicao");

            NpgsqlConnection dbConnection = FabricaConexao.getConexao();

            IGradExposicaoDao gradExposicaoDao = FabricaDao.getGradExposicaoDao();

            try
            {
                return gradExposicaoDao.findGradExposicaoByGradExposicao(gradExposicao, dbConnection);

            }
            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findGradExposicaoByGradExposicao", ex);
                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }
            finally
            {
                FabricaConexao.fecharConexao();
            }
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public GradEfeito findGradEfeitoByGradEfeito(GradEfeito gradEfeito)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findGradEfeitoByGradEfeito");

            NpgsqlConnection dbConnection = FabricaConexao.getConexao();

            IGradEfeitoDao gradEfeitoDao = FabricaDao.getGradEfeitoDao();

            try
            {
                return gradEfeitoDao.findGradEfeitoByGradEfeito(gradEfeito, dbConnection);

            }
            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findGradEfeitoByGradEfeito", ex);
                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }
            finally
            {
                FabricaConexao.fecharConexao();
            }
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public GradSoma findGradSomaByGradSoma(GradSoma gradSoma)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findGradSomaByGradSoma");

            NpgsqlConnection dbConnection = FabricaConexao.getConexao();

            IGradSomaDao gradSomaDao = FabricaDao.getGradSomaDao();

            try
            {
                return gradSomaDao.findGradSomaByGradSoma(gradSoma, dbConnection);

            }
            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findGradSomaByGradSoma", ex);
                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }
            finally
            {
                FabricaConexao.fecharConexao();
            }
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public GheFonte findGheFonteByGheFonte(GheFonte gheFonte)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findGheFonteByGheFonte");

            NpgsqlConnection dbConnection = FabricaConexao.getConexao();

            IGheFonteDao gheFonteDao = FabricaDao.getGheFonteDao();

            try
            {
                return gheFonteDao.findByGheFonte(gheFonte, dbConnection);

            }
            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findGheFonteByGheFonte", ex);
                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }
            finally
            {
                FabricaConexao.fecharConexao();
            }
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public GheFonteAgente incluiGheFonteAgente(GheFonteAgente gheFonteAgente)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método incluiGheFonteAgente");

            NpgsqlConnection dbConnection = FabricaConexao.getConexao();
            IGheFonteAgenteDao gheFonteAgenteDao = FabricaDao.getGheFonteAgenteDao();
            try
            {
                gheFonteAgente = gheFonteAgenteDao.insert(gheFonteAgente, dbConnection);
                
                return gheFonteAgente;
            }
            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - incluiGheFonteAgente", ex);
                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }
            finally
            {
                FabricaConexao.fecharConexao();
            }
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public void deleteGheFonteAgenteById(Int64 id)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método deleteGheFonteAgenteById");

            NpgsqlConnection dbConnection = FabricaConexao.getConexao();
            IGheFonteAgenteDao gheFonteAgenteDao = FabricaDao.getGheFonteAgenteDao();

            NpgsqlTransaction transaction = dbConnection.BeginTransaction();

            try
            {
                gheFonteAgenteDao.delete(id, dbConnection);

                transaction.Commit();
            }
            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - deleteGheFonteAgenteById", ex);
                transaction.Rollback();
                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }
            finally
            {
                FabricaConexao.fecharConexao();
            }
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public GheFonteAgente findGheFonteAgenteById(Int64 id)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findGheFonteAgenteById");

            NpgsqlConnection dbConnection = FabricaConexao.getConexao();

            IGheFonteAgenteDao gheFonteAgenteDao = FabricaDao.getGheFonteAgenteDao();
            IGheFonteDao gheFonteDao = FabricaDao.getGheFonteDao();
            IAgenteDao agenteDao = FabricaDao.getAgenteDao();
            IGradEfeitoDao gradEfeitoDao = FabricaDao.getGradEfeitoDao();
            IGradSomaDao gradSomaDao = FabricaDao.getGradSomaDao();
            IGradExposicaoDao gradExposicaoDao = FabricaDao.getGradExposicaoDao();

            try
            {
                GheFonteAgente gheFonteAgente = gheFonteAgenteDao.findById(id, dbConnection);

                if (gheFonteAgente != null)
                {
                    /* preenchendo demais objetos */
                    if (gheFonteAgente.GheFonte != null)
                        gheFonteAgente.GheFonte = gheFonteDao.findByGheFonte(gheFonteAgente.GheFonte, dbConnection);
                    
                    if (gheFonteAgente.Agente != null)
                        gheFonteAgente.Agente = agenteDao.findById((Int64)gheFonteAgente.Agente.Id, dbConnection);

                    gheFonteAgente.GradSoma = gheFonteAgente.GradSoma == null ? null : gradSomaDao.findGradSomaById(gheFonteAgente.GradSoma, dbConnection);
                    
                    gheFonteAgente.GradEfeito = gheFonteAgente.GradEfeito == null ? null : gradEfeitoDao.findGradEfeitoByGradEfeito(gheFonteAgente.GradEfeito, dbConnection);
                    
                    gheFonteAgente.GradExposicao = gheFonteAgente.GradExposicao == null ? null : gradExposicaoDao.findGradExposicaoByGradExposicao(gheFonteAgente.GradExposicao, dbConnection);
                }

                return gheFonteAgente;

            }
            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findGheFonteAgenteById", ex);
                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }
            finally
            {
                FabricaConexao.fecharConexao();
            }
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public GheFonteAgente findGheFonteAgenteByGheFonteAgente(GheFonteAgente gheFonteAgente)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findGheFonteAgenteByGheFonteAgente");

            NpgsqlConnection dbConnection = FabricaConexao.getConexao();
            IGheFonteAgenteDao gheFonteAgenteDao = FabricaDao.getGheFonteAgenteDao();
            IGradSomaDao gradSomaDao = FabricaDao.getGradSomaDao();
            IGradEfeitoDao gradEfeitoDao = FabricaDao.getGradEfeitoDao();
            IGradExposicaoDao gradExposicaoDao = FabricaDao.getGradExposicaoDao();
            IGheFonteDao gheFonteDao = FabricaDao.getGheFonteDao();
            IAgenteDao agenteDao = FabricaDao.getAgenteDao();
            
            GheFonteAgente gheFonteAgenteRetorno = new GheFonteAgente();
            GradSoma gradSoma = new GradSoma();
            GradEfeito gradEfeito = new GradEfeito();
            GradExposicao gradExposicao = new GradExposicao();
            GheFonte gheFonte = new GheFonte();
            Agente agente = new Agente();
            
            try
            {
                gheFonteAgenteRetorno = gheFonteAgenteDao.findById((Int64)gheFonteAgente.Id, dbConnection);
                
                // preenchendo demais objetos da classe GheFonteAgente

                gheFonteAgenteRetorno.GradSoma = gradSomaDao.findGradSomaById(gheFonteAgenteRetorno.GradSoma, dbConnection);
                gheFonteAgenteRetorno.GradEfeito = gradEfeitoDao.findGradEfeitoByGradEfeito(gheFonteAgenteRetorno.GradEfeito, dbConnection);
                gheFonteAgenteRetorno.GradExposicao = gradExposicaoDao.findGradExposicaoByGradExposicao(gheFonteAgenteRetorno.GradExposicao, dbConnection);
                
                gheFonteAgenteRetorno.GheFonte = gheFonteDao.findByGheFonte(gheFonteAgenteRetorno.GheFonte, dbConnection);
                gheFonteAgenteRetorno.Agente = agenteDao.findById((Int64)gheFonteAgenteRetorno.Agente.Id, dbConnection);
                
                return gheFonteAgenteRetorno;

            }
            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findGheFonteAgenteByGheFonteAgente", ex);
                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }
            finally
            {
                
                FabricaConexao.fecharConexao();
            }
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public DataSet findAllAgenteByGheFonte(GheFonte gheFonte)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findAllAgenteByGheFonte");

            NpgsqlConnection dbConnection = FabricaConexao.getConexao();
            
            IGheFonteAgenteDao gheFonteAgenteDao = FabricaDao.getGheFonteAgenteDao();
            try
            {
                return gheFonteAgenteDao.findAllByGheFonte(gheFonte, dbConnection);

            }
            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findAllAgenteByGheFonte", ex);
                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }
            finally
            {
                FabricaConexao.fecharConexao();
            }
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public GheFonteAgenteEpi incluirGheFonteAgenteEpi(GheFonteAgenteEpi gheFonteAgenteEpi)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método incluirGheFonteAgenteEpi");

            GheFonteAgenteEpi GheFonteAgenteEpiNovo = null; 
            NpgsqlConnection dbConnection = FabricaConexao.getConexao(); 
            IGheFonteAgenteEpiDao gheFonteAgenteEpiDao = FabricaDao.getGheFonteAgenteEpiDao(); 
            
            NpgsqlTransaction transaction = dbConnection.BeginTransaction();
            
            try
            {

                GheFonteAgenteEpiNovo = gheFonteAgenteEpiDao.insertGheFonteAgenteEpi(gheFonteAgenteEpi, dbConnection); 
                transaction.Commit();
            }
            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - incluirGheFonteAgenteEpi", ex);
                transaction.Rollback();
                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }
            finally
            {
                FabricaConexao.fecharConexao();
            }

            return GheFonteAgenteEpiNovo;
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public void deleteGheFonteAgenteEpi(Int64 id)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método deleteGheFonteAgente");

            NpgsqlConnection dbConnection = FabricaConexao.getConexao();

            IGheFonteAgenteEpiDao gheFonteAgenteEpiDao = FabricaDao.getGheFonteAgenteEpiDao();

            NpgsqlTransaction transaction = dbConnection.BeginTransaction();

            try
            {
                gheFonteAgenteEpiDao.excluiGheFonteAgenteEpi(id,dbConnection);

                transaction.Commit();
            }
            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - deleteGheFonteAgente", ex);
                transaction.Rollback();
                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }
            finally
            {
                FabricaConexao.fecharConexao();
            }
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public GheFonteAgenteEpi findGheFonteAgenteEpiByGheFonteAgenteEpi(GheFonteAgenteEpi gheFonteAgenteEpi)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findGheFonteAgenteEpiByGheFonteAgenteEpi");

            NpgsqlConnection dbConnection = FabricaConexao.getConexao();

            IGheFonteAgenteEpiDao gheFonteAgenteEpiDao = FabricaDao.getGheFonteAgenteEpiDao();

            GheFonteAgenteEpi gheFonteAgenteEpiRetorno = new GheFonteAgenteEpi();
            
            try
            {
                gheFonteAgenteEpiRetorno = gheFonteAgenteEpiDao.findGheFonteAgenteEpiByGheFonteAgenteEpi(gheFonteAgenteEpi, dbConnection);
                
                return gheFonteAgenteEpiRetorno;

            }
            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findGheFonteAgenteEpiByGheFonteAgenteEpi", ex);
                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }
            finally
            {

                FabricaConexao.fecharConexao();
            }
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public Boolean findGheFonteAgenteEpi(GheFonteAgenteEpi gheFonteAgenteEpi)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findGheFonteAgenteEpi");

            Boolean retorno;

            NpgsqlConnection dbConnection = FabricaConexao.getConexao();

            IGheFonteAgenteEpiDao gheFonteAgenteEpiDao = FabricaDao.getGheFonteAgenteEpiDao();

            try
            {
                retorno = gheFonteAgenteEpiDao.findGheFonteAgenteEpi(gheFonteAgenteEpi, dbConnection);
                return retorno;

            }
            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findGheFonteAgenteEpi", ex);
                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }
            finally
            {
                FabricaConexao.fecharConexao();
            }
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public HashSet<GheFonteAgenteEpi> findAllGheFonteAgenteEpi(GheFonteAgente gheFonteAgente)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findAllGheFonteAgenteEpi");

            NpgsqlConnection dbConnection = FabricaConexao.getConexao();

            IGheFonteAgenteEpiDao gheFonteAgenteEpiDao = FabricaDao.getGheFonteAgenteEpiDao();
            HashSet<GheFonteAgenteEpi> gheFonteAgenteEpiSetRetorno = new HashSet<GheFonteAgenteEpi>();

            try
            {
                gheFonteAgenteEpiSetRetorno = gheFonteAgenteEpiDao.findAllGheFonteAgenteEpi(gheFonteAgente,dbConnection);

                return gheFonteAgenteEpiSetRetorno;

            }
            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findAllGheFonteAgenteEpi", ex);
                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }
            finally
            {
                FabricaConexao.fecharConexao();
            }
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public HashSet<GheSetor> findAllGheSetor(Ghe ghe)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findAllGheSetor");

            NpgsqlConnection dbConnection = FabricaConexao.getConexao();

            IGheSetorDao gheSetorDao = FabricaDao.getGheSetorDao();
            
            HashSet<GheSetor> gheSetorRetorno = new HashSet<GheSetor>();

            try
            {
                gheSetorRetorno = gheSetorDao.findAtivosByGhe(ghe, dbConnection);

                return gheSetorRetorno;

            }
            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findAllGheSetor", ex);
                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }
            finally
            {
                FabricaConexao.fecharConexao();
            }
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public void deleteGheSetor(GheSetor gheSetor)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método deleteGheSetor");

            NpgsqlConnection dbConnection = FabricaConexao.getConexao();

            IGheSetorDao gheSetorDao = FabricaDao.getGheSetorDao();

            NpgsqlTransaction transaction = dbConnection.BeginTransaction();

            try
            {
                gheSetorDao.delete(gheSetor, dbConnection);
                transaction.Commit();

            }
            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - deleteGheSetor", ex);
                transaction.Rollback();
                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }
            finally
            {
                FabricaConexao.fecharConexao();
            }
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public DataSet findAllFuncaoAtivaNotInClient(Funcao funcao, Cliente cliente)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findAllFuncaoAtiva");

            NpgsqlConnection dbConnection = FabricaConexao.getConexao();

            IFuncaoDao funcaoDao = FabricaDao.getFuncaoDao();

            try
            {
                return funcaoDao.findAtivaNotInClient(funcao, cliente, dbConnection);

            }
            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findAllFuncaoAtiva", ex);
                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }
            finally
            {
                FabricaConexao.fecharConexao();
            }
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public ClienteFuncao insertClienteFuncao(ClienteFuncao clienteFuncao)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método incluirClienteFuncao");

            NpgsqlConnection dbConnection = FabricaConexao.getConexao();
            IClienteFuncaoDao clienteFuncaoDao = FabricaDao.getClienteFuncaoDao();
            NpgsqlTransaction transaction = dbConnection.BeginTransaction();

            try
            {
                
                /* verificando se existe um cliente funcao cadastrado para o cliente e se está desativado.
                 * caso exista, então esse cliente funcao deverá ser reativado, 
                 * evitando problemas nos relacionamentos do pcmso.
                 * 18/10/2015 - welbert santos */
               
                LinkedList<ClienteFuncao> funcaoInCliente = new LinkedList<ClienteFuncao>();
                funcaoInCliente = clienteFuncaoDao.findAllByFuncaoAndCliente(clienteFuncao.Funcao, clienteFuncao.Cliente, dbConnection);

                if (funcaoInCliente.Count > 0)
                {

                    funcaoInCliente.ToList().ForEach(delegate(ClienteFuncao clienteFuncaoPesquisar)
                    {
                        if (string.Equals(clienteFuncaoPesquisar.Situacao, ApplicationConstants.DESATIVADO))
                        {
                            clienteFuncaoPesquisar.Situacao = ApplicationConstants.ATIVO;
                            clienteFuncaoPesquisar.DataExclusao = null;
                            clienteFuncaoPesquisar.DataCadastro = DateTime.Now;
                            clienteFuncaoDao.update(clienteFuncaoPesquisar, dbConnection);
                        }
                    });
                }
                else
                    clienteFuncaoDao.insert(clienteFuncao, dbConnection);

                transaction.Commit();

            }
            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - incluirClienteFuncao", ex);
                transaction.Rollback();
                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }
            finally
            {
                FabricaConexao.fecharConexao();
            }

            return clienteFuncao;
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public DataSet findAllClienteFuncao(Cliente cliente)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findAllClienteFuncao");

            NpgsqlConnection dbConnection = FabricaConexao.getConexao();

            IClienteFuncaoDao clienteFuncaoDao = FabricaDao.getClienteFuncaoDao();

            try
            {
                return clienteFuncaoDao.findAtivosByCliente(cliente, dbConnection);

            }
            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findAllClienteFuncao", ex);
                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }
            finally
            {
                FabricaConexao.fecharConexao();
            }
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public void updateClienteFuncao(ClienteFuncao clienteFuncao)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método deleteClienteFuncao");

            NpgsqlConnection dbConnection = FabricaConexao.getConexao();

            IClienteFuncaoDao clienteFuncaoDao = FabricaDao.getClienteFuncaoDao();

            NpgsqlTransaction transaction = dbConnection.BeginTransaction();

            try
            {
                clienteFuncaoDao.update(clienteFuncao, dbConnection);
                transaction.Commit();

            }
            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - deleteClienteFuncao", ex);
                transaction.Rollback();
                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }
            finally
            {
                FabricaConexao.fecharConexao();
            }
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public DataSet findAllSetorByGhe(Ghe ghe)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findAllSetorByGhe");

            NpgsqlConnection dbConnection = FabricaConexao.getConexao();
            IGheSetorDao gheSetorDao = FabricaDao.getGheSetorDao();

            try
            {
                return gheSetorDao.findAllByGhe(ghe, dbConnection);

            }
            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findAllSetorByGhe", ex);
                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }
            finally
            {
                FabricaConexao.fecharConexao();
            }
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public GheSetor findGheSetorById(Int64 id)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findGheSetorById");

            NpgsqlConnection dbConnection = FabricaConexao.getConexao();

            IGheSetorDao gheSetorDao = FabricaDao.getGheSetorDao();

            try
            {
                return gheSetorDao.findById(id, dbConnection);

            }
            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findGheSetorById", ex);
                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }
            finally
            {
                FabricaConexao.fecharConexao();
            }
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public HashSet<GheSetorClienteFuncao> findAllClienteFuncaoByClienteFuncao(GheSetorClienteFuncao gheSetorClienteFuncao)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findAllClienteFuncaoByClienteFuncao");

            NpgsqlConnection dbConnection = FabricaConexao.getConexao();

            IGheSetorClienteFuncaoDao gheSetorClienteFuncaoDao = FabricaDao.getGheSetorClienteFuncaoDao();

            try
            {
                return gheSetorClienteFuncaoDao.findAllByGheSetorClienteFuncao(gheSetorClienteFuncao, dbConnection);

            }
            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findAllClienteFuncaoByClienteFuncao", ex);
                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }
            finally
            {
                FabricaConexao.fecharConexao();
            }
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public DataSet findAllFuncaoAtivaByGheSetorClienteFuncao(GheSetorClienteFuncao gheSetorClienteFuncao, Cliente cliente)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findAllFuncaoAtivaByGheSetorClienteFuncao");

            NpgsqlConnection dbConnection = FabricaConexao.getConexao();

            IGheSetorClienteFuncaoDao gheSetorClienteFuncaoDao = FabricaDao.getGheSetorClienteFuncaoDao();

            try
            {
                return gheSetorClienteFuncaoDao.findAllByGheSetorClienteFuncaoAndCliente(gheSetorClienteFuncao, cliente,dbConnection);

            }
            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findAllFuncaoAtivaByGheSetorClienteFuncao", ex);
                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }
            finally
            {
                FabricaConexao.fecharConexao();
            }
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public void inserirGheSetorClienteFuncao(GheSetorClienteFuncao gheSetorClienteFuncao)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método inserirGheSetorClienteFuncao");

            NpgsqlConnection dbConnection = FabricaConexao.getConexao();

            IGheSetorClienteFuncaoDao gheSetorClienteFuncaoDao = FabricaDao.getGheSetorClienteFuncaoDao();

            NpgsqlTransaction transaction = dbConnection.BeginTransaction();

            try
            {
                gheSetorClienteFuncaoDao.insert(gheSetorClienteFuncao, dbConnection);
                transaction.Commit();

            }
            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - inserirGheSetorClienteFuncao", ex);
                transaction.Rollback();
                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }
            finally
            {
                FabricaConexao.fecharConexao();
            }
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public void excluirGheSetorClienteFuncao(GheSetorClienteFuncao gheSetorClienteFuncao)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método excluirGheSetorClienteFuncao");

            NpgsqlConnection dbConnection = FabricaConexao.getConexao();

            IGheSetorClienteFuncaoDao gheSetorClienteFuncaoDao = FabricaDao.getGheSetorClienteFuncaoDao();

            NpgsqlTransaction transaction = dbConnection.BeginTransaction();

            try
            {
                gheSetorClienteFuncaoDao.delete(gheSetorClienteFuncao, dbConnection);
                transaction.Commit();

            }
            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - excluirGheSetorClienteFuncao", ex);
                transaction.Rollback();
                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }
            finally
            {
                FabricaConexao.fecharConexao();
            }
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public void excluiPpra(Int64 id)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método excluiPpra");

            NpgsqlConnection dbConnection = FabricaConexao.getConexao();

            IEstudoDao ppraDao = FabricaDao.getEstudoDao();

            NpgsqlTransaction transaction = dbConnection.BeginTransaction();

            try
            {
                ppraDao.delete(id, dbConnection);
                transaction.Commit();

            }
            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - excluiPpra", ex);
                transaction.Rollback();
                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }
            finally
            {
                FabricaConexao.fecharConexao();
            }
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public DataSet findPpraByPcmso(Estudo ppra)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findPpraByPcmso");

            NpgsqlConnection dbConnection = FabricaConexao.getConexao();
            
            IEstudoDao ppraDao = FabricaDao.getEstudoDao();

            try
            {
                return ppraDao.findEstudoFechado(ppra, dbConnection);

            }
            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findPpraByPcmso", ex);
                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }
            finally
            {
                FabricaConexao.fecharConexao();
            }
        }
                                
        [MethodImpl(MethodImplOptions.Synchronized)]
        public Atividade incluirAtividade(Atividade atividade)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método incluirAtividade");

            Atividade atividadeNova = null; 
            NpgsqlConnection dbConnection = FabricaConexao.getConexao(); 
            IAtividadeDao atividadeDao = FabricaDao.getAtividadeDao(); 
            NpgsqlTransaction transaction = dbConnection.BeginTransaction();
            try
            {
                if (atividadeDao.findByDescricao(atividade.Descricao, dbConnection) != null)
                    throw new Exception("Já existe uma atividade com esse nome.");

                atividadeNova = atividadeDao.insert(atividade, dbConnection); 

                transaction.Commit();
            }
            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - novaAtividade", ex);
                transaction.Rollback();
                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }
            finally
            {
                FabricaConexao.fecharConexao();
            }

            return atividade;
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public Atividade findAtividadeByDescricao(String descricao)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findAtividadeByDescricao");

            NpgsqlConnection dbConnection = FabricaConexao.getConexao();
            IAtividadeDao atividadeDao = FabricaDao.getAtividadeDao();

            try
            {
                Atividade atividade = atividadeDao.findByDescricao(descricao, dbConnection);

                return atividade;
            }
            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findAtividadeByDescricao", ex);
                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }
            finally
            {
                FabricaConexao.fecharConexao();
            }
        }
        
        [MethodImpl(MethodImplOptions.Synchronized)]
        public DataSet findAtividadeAtivosByDescricao(Atividade atividade)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findAtividadeAtivosByDescricao");

            NpgsqlConnection dbConnection = FabricaConexao.getConexao();
            IAtividadeDao atividadeDao = FabricaDao.getAtividadeDao();

            try
            {
                return atividadeDao.findByAtividade(atividade, dbConnection);

            }
            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findAtividadeAtivosByDescricao", ex);
                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }
            finally
            {
                FabricaConexao.fecharConexao();
            }
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public DataSet FindAtividadeByFilter(Atividade atividade)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método FindAtividadeByFilter");

            NpgsqlConnection dbConnection = FabricaConexao.getConexao();
            IAtividadeDao atividadeDao = FabricaDao.getAtividadeDao();

            try
            {
                return atividadeDao.findByFilter(atividade, dbConnection);

            }
            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findAtividadeByFilter", ex);
                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }
            finally
            {
                FabricaConexao.fecharConexao();
            }
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public Atividade findAtividadeById(Int64 idAtividade)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findAtividadeById");

            NpgsqlConnection dbConnection = FabricaConexao.getConexao();
            IAtividadeDao atividadedao = FabricaDao.getAtividadeDao();
            try
            {
                Atividade atividade = atividadedao.findById(idAtividade, dbConnection);
                return atividade;

            }
            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findAtividadeById", ex);
                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }
            finally
            {
                FabricaConexao.fecharConexao();
            }
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public Atividade updateAtividade(Atividade atividade)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método alteraAtividade");

            NpgsqlConnection dbConnection = FabricaConexao.getConexao(); 
            IAtividadeDao atividadeDao = FabricaDao.getAtividadeDao(); 
            NpgsqlTransaction transaction = dbConnection.BeginTransaction();
            try
            {
                Atividade atividadeProcura = atividadeDao.findByDescricao(atividade.Descricao, dbConnection);

                if (atividadeProcura != null && atividadeProcura.Id != atividade.Id)
                    throw new Exception("Já existe uma atividade cadastrada com esse nome.");

                atividade = atividadeDao.update(atividade, dbConnection); 

                transaction.Commit();
            }
            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - alter arAtividade", ex);
                transaction.Rollback();
                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }
            finally
            {
                FabricaConexao.fecharConexao();
            }

            return atividade;
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public Fonte incluirFonte(Fonte fonte)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método incluirFonte");

            Fonte fonteNova = null; 
            NpgsqlConnection dbConnection = FabricaConexao.getConexao(); 
            IFonteDao FonteDao = FabricaDao.getFonteDao(); 
            NpgsqlTransaction transaction = dbConnection.BeginTransaction();
            
            try
            {
                if (FonteDao.findByDescricao(fonte.Descricao, dbConnection) != null)
                    throw new Exception("Fonte com essa descrição já está cadastrada.");

                fonteNova = FonteDao.insert(fonte, dbConnection); 

                transaction.Commit();
            }
            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - novaFonte", ex);
                transaction.Rollback();
                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }
            finally
            {
                FabricaConexao.fecharConexao();
            }

            return fonte;
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public Fonte findFonteByDescricao(String descricao)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findFonteByDescricao");

            NpgsqlConnection dbConnection = FabricaConexao.getConexao();
            IFonteDao fonteDao = FabricaDao.getFonteDao();

            try
            {
                Fonte fonte = fonteDao.findByDescricao(descricao, dbConnection);

                return fonte;
            }
            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findFonteByDescricao", ex);
                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }
            finally
            {
                FabricaConexao.fecharConexao();
            }
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public DataSet FindFonteByFilter(Fonte fonte)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método FindFonteByFilter");

            NpgsqlConnection dbConnection = FabricaConexao.getConexao();
            IFonteDao fontedao = FabricaDao.getFonteDao();

            try
            {
                return fontedao.findByFilter(fonte, dbConnection);

            }
            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findFonteByFilter", ex);
                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }
            finally
            {
                FabricaConexao.fecharConexao();
            }
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public Fonte findFonteById(Int64 idFonte)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findFonteById");

            NpgsqlConnection dbConnection = FabricaConexao.getConexao();
            IFonteDao fontedao = FabricaDao.getFonteDao();
            try
            {
                Fonte fonte = fontedao.findById(idFonte, dbConnection);
                return fonte;

            }
            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findFonteById", ex);
                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }
            finally
            {
                FabricaConexao.fecharConexao();
            }
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public Fonte updateFonte(Fonte fonte)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método alteraFonte");

            NpgsqlConnection dbConnection = FabricaConexao.getConexao(); 
            IFonteDao fonteDao = FabricaDao.getFonteDao(); 
            NpgsqlTransaction transaction = dbConnection.BeginTransaction();
            try
            {
                Fonte fonteProcura = fonteDao.findByDescricao(fonte.Descricao, dbConnection);

                if (fonteProcura != null && fonteProcura.Id != fonte.Id)
                    throw new Exception("Já existe uma fonte com essa descrição.");
                
                fonte = fonteDao.update(fonte, dbConnection); 
                transaction.Commit();
            
            }
            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - alterarFonte", ex);
                transaction.Rollback();
                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }
            finally
            {
                FabricaConexao.fecharConexao();
            }

            return fonte;
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public Setor incluirSetor(Setor setor)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método incluirSetor");

            Setor setorNovo = null;
            NpgsqlConnection dbConnection = FabricaConexao.getConexao();
            ISetorDao setorDao = FabricaDao.getSetorDao();
            NpgsqlTransaction transaction = dbConnection.BeginTransaction();
            try
            {
                setorNovo = setorDao.insert(setor, dbConnection);

                transaction.Commit();
            }
            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - novoSetor", ex);
                transaction.Rollback();
                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }
            finally
            {
                FabricaConexao.fecharConexao();
            }

            return setor;
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public Setor findSetorByDescricao(String descricao)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findSetorByDescricao");

            NpgsqlConnection dbConnection = FabricaConexao.getConexao();
            ISetorDao setorDao = FabricaDao.getSetorDao();

            try
            {
                Setor setor = setorDao.findSetorByDescricao(descricao, dbConnection);

                return setor;
            }
            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findSetorByDescricao", ex);
                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }
            finally
            {
                FabricaConexao.fecharConexao();
            }
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public DataSet FindSetorByFilter(Setor setor)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método FindSetorByFilter");

            NpgsqlConnection dbConnection = FabricaConexao.getConexao();
            ISetorDao setordao = FabricaDao.getSetorDao();

            try
            {
                return setordao.findSetorByFilter(setor, dbConnection);

            }
            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findSetorByFilter", ex);
                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }
            finally
            {
                FabricaConexao.fecharConexao();
            }
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public Setor findSetorById(Int64 idSetor)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findSetorById");

            NpgsqlConnection dbConnection = FabricaConexao.getConexao();
            ISetorDao setordao = FabricaDao.getSetorDao();
            try
            {
                Setor setor = setordao.findSetorById(idSetor, dbConnection);
                return setor;

            }
            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findSetorById", ex);
                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }
            finally
            {
                FabricaConexao.fecharConexao();
            }
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public Setor updateSetor(Setor setorAlterar)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método updateSetor");

            Setor setorNovo = null;
            NpgsqlConnection dbConnection = FabricaConexao.getConexao();
            ISetorDao setorDao = FabricaDao.getSetorDao();
            NpgsqlTransaction transaction = dbConnection.BeginTransaction();
            try
            {
                setorNovo = setorDao.update(setorAlterar, dbConnection);
                transaction.Commit();
            }
            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - updateSetor", ex);
                transaction.Rollback();
                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }
            finally
            {
                FabricaConexao.fecharConexao();
            }

            return setorNovo;
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public DataSet findAllSetorAtivo(Setor setor, Ghe ghe, ClienteFuncao ClienteFuncao)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findAllSetorAtivo");

            NpgsqlConnection dbConnection = FabricaConexao.getConexao();
            ISetorDao setordao = FabricaDao.getSetorDao();

            try
            {
                return setordao.findAllSetorAtivo(setor, ghe, ClienteFuncao, dbConnection);

            }
            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findAllSetorAtivo", ex);
                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }
            finally
            {
                FabricaConexao.fecharConexao();
            }
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public void incluirGheSetor(GheSetor gheSetor)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método incluirGheSetor");

            NpgsqlConnection dbConnection = FabricaConexao.getConexao();
            IGheSetorDao gheSetordao = FabricaDao.getGheSetorDao();
            NpgsqlTransaction transaction = dbConnection.BeginTransaction();

            try
            {
                /* verificar se ná inclusao do GheSetor se já existe um gheSetor desativado. 
                 * Caso esteja desativado, o mesmo deverá ser reativado para evitar
                 * problemas de relacionamento com o pcmso.
                 * 18/10-2015 welbert santos */

                LinkedList<GheSetor> gheSetorList = gheSetordao.findAllBySetorAndGhe(gheSetor.Setor,
                    gheSetor.Ghe, dbConnection);


                if (gheSetorList.Count > 0)
                {
                    foreach (GheSetor gs in gheSetorList)
                    {
                        if (String.Equals(ApplicationConstants.DESATIVADO, gs.Situacao))
                        {
                            gheSetor.Situacao = ApplicationConstants.ATIVO;
                            gheSetordao.update(gheSetor, dbConnection);
                            transaction.Commit();
                            break;
                        }
                    }
                }
                else
                {
                    gheSetordao.insert(gheSetor, dbConnection);
                    transaction.Commit();
                }

            }
            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - incluirGheSetor", ex);
                transaction.Rollback();
                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }
            finally
            {
                FabricaConexao.fecharConexao();
            }
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public DataSet findEpiByFilter(Epi epi)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findEpiByFilter");

            NpgsqlConnection dbConnection = FabricaConexao.getConexao();
            IEpiDao epidao = FabricaDao.getEpiDao();

            try
            {
                return epidao.findByFilter(epi, dbConnection);

            }
            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findEpiByFilter", ex);
                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }
            finally
            {
                FabricaConexao.fecharConexao();
            }
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public Epi findEpiByDescricao(String descricao)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findEpiByDescricao");

            NpgsqlConnection dbConnection = FabricaConexao.getConexao();
            IEpiDao epiDao = FabricaDao.getEpiDao();
            try
            {
                Epi epi = epiDao.findByDescricao(descricao, dbConnection);
                return epi;
            }
            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findEpiByDescricao", ex);
                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }
            finally
            {
                FabricaConexao.fecharConexao();
            }
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public Epi incluirEpi(Epi epi)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método incluirEpi");

            Epi epiNovo = null;
            NpgsqlConnection dbConnection = FabricaConexao.getConexao();
            IEpiDao epiDao = FabricaDao.getEpiDao();
            NpgsqlTransaction transaction = dbConnection.BeginTransaction();
            try
            {
                /* verificando se já existe um EPI cadastrado com essa descrição */

                if (epiDao.findByDescricao(epi.Descricao, dbConnection) != null)
                    throw new Exception("Já existe um EPI cadastrado com essa descriçao");

                epiNovo = epiDao.insert(epi, dbConnection);

                transaction.Commit();
            }
            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - novoEpi", ex);
                transaction.Rollback();
                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }
            finally
            {
                FabricaConexao.fecharConexao();
            }
            return epi;
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public Epi alteraEpi(Epi epiAlterar)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método alteraEpi");

            Epi epiNovo = null;
            NpgsqlConnection dbConnection = FabricaConexao.getConexao();
            IEpiDao epiDao = FabricaDao.getEpiDao();
            NpgsqlTransaction transaction = dbConnection.BeginTransaction();
            try
            {
                /* verificando se não existe nenhum EPI já cadastrado com esse nome. */

                Epi epiProcurado = epiDao.findByDescricao(epiAlterar.Descricao, dbConnection);

                if (epiProcurado != null && epiProcurado.Id != epiAlterar.Id)
                    throw new Exception("Já existe um EPI cadastrado com essa descrição.");
                 
                epiNovo = epiDao.update(epiAlterar, dbConnection);
                transaction.Commit();
            }
            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - alterarEpi", ex);
                transaction.Rollback();
                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }
            finally
            {
                FabricaConexao.fecharConexao();
            }

            return epiNovo;
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public Epi findEpiById(Int64 idEpi)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findEpiById");

            NpgsqlConnection dbConnection = FabricaConexao.getConexao();
            IEpiDao epiDao = FabricaDao.getEpiDao();
            try
            {
                Epi epi = epiDao.findById(idEpi, dbConnection);
                return epi;

            }
            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findEpiById", ex);
                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }
            finally
            {
                FabricaConexao.fecharConexao();
            }
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public DataSet findAgenteByFilter(Agente agente)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findAgenteByFilter");

            NpgsqlConnection dbConnection = FabricaConexao.getConexao();
            IAgenteDao agenteDao = FabricaDao.getAgenteDao();

            try
            {
                return agenteDao.findByFilter(agente, dbConnection);

            }
            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findAgenteByFilter", ex);
                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }
            finally
            {
                FabricaConexao.fecharConexao();
            }
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public DataSet findAgenteAtivoByGheFonteAgenteByFilter(GheFonteAgente gheFonteAgente)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findAgenteAtivoByGheFonteAgenteByFilter");

            NpgsqlConnection dbConnection = FabricaConexao.getConexao();
            IAgenteDao agenteDao = FabricaDao.getAgenteDao();

            try
            {
                return agenteDao.findByGheFonteAgente(gheFonteAgente, dbConnection);

            }
            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findAgenteAtivoByGheFonteAgenteByFilter", ex);
                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }
            finally
            {
                FabricaConexao.fecharConexao();
            }
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public Agente insertAgente(Agente agente)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método incluirAgente");

            Agente agenteNovo = null;
            NpgsqlConnection dbConnection = FabricaConexao.getConexao();
            IAgenteDao agenteDao = FabricaDao.getAgenteDao();
            NpgsqlTransaction transaction = dbConnection.BeginTransaction();
            try
            {
                /* verificando se existe um agente com a descrição utilizada */

                Agente agenteProcura = agenteDao.findByDescricao(agente.Descricao, dbConnection);

                if (agenteProcura != null)
                    throw new AgenteException(AgenteException.msg2);
                
                agenteNovo = agenteDao.insert(agente, dbConnection);

                transaction.Commit();
            }
            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - novoAgente", ex);
                transaction.Rollback();
                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }
            finally
            {
                FabricaConexao.fecharConexao();
            }
            return agente;
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public Agente findAgenteByDescricao(String descricao)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findAgenteByDescricao");

            NpgsqlConnection dbConnection = FabricaConexao.getConexao();
            IAgenteDao agenteDao = FabricaDao.getAgenteDao();
            try
            {
                Agente agente = agenteDao.findByDescricao(descricao, dbConnection);
                return agente;
            }
            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findAgenteByDescricao", ex);
                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }
            finally
            {
                FabricaConexao.fecharConexao();
            }
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public Agente findAgenteById(Int64 idAgente)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findAgenteById");

            NpgsqlConnection dbConnection = FabricaConexao.getConexao();
            IAgenteDao agenteDao = FabricaDao.getAgenteDao();
            try
            {
                Agente agente = agenteDao.findById(idAgente, dbConnection);
                return agente;

            }
            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findAgenteById", ex);
                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }
            finally
            {
                FabricaConexao.fecharConexao();
            }
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public Agente updateAgente(Agente agente)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método alteraAgente");

            NpgsqlConnection dbConnection = FabricaConexao.getConexao();
            IAgenteDao agenteDao = FabricaDao.getAgenteDao();
            NpgsqlTransaction transaction = dbConnection.BeginTransaction();

            try
            {
                /* verificando para saber se existe outro agente com essa descrição */

                Agente agenteProcura = agenteDao.findByDescricao(agente.Descricao, dbConnection);
                
                if (agenteProcura != null && (String.Equals(agenteProcura.Descricao.Trim(), agente.Descricao.Trim()) && agente.Id != agenteProcura.Id))
                    throw new AgenteException(AgenteException.msg2);
                
                agente = agenteDao.update(agente, dbConnection);
                transaction.Commit();
            }
            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - alterarAgente", ex);
                transaction.Rollback();
                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }
            finally
            {
                FabricaConexao.fecharConexao();
            }

            return agente;
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public DataSet FindFuncaoByFilter(Funcao funcao)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método FindFuncaoByFilter");

            NpgsqlConnection dbConnection = FabricaConexao.getConexao();
            IFuncaoDao funcaoDao = FabricaDao.getFuncaoDao();

            try
            {
                return funcaoDao.findByFilter(funcao, dbConnection);

            }
            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findFuncaoByFilter", ex);
                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }
            finally
            {
                FabricaConexao.fecharConexao();
            }
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public Funcao insertFuncao(Funcao funcao)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método incluirFuncao");

            Funcao funcaoNova = null;
            NpgsqlConnection dbConnection = FabricaConexao.getConexao();
            IFuncaoDao funcaoDao = FabricaDao.getFuncaoDao();
            IComentarioFuncaoDao comentarioFuncaoDao = FabricaDao.getComentarioFuncaoDao();
            NpgsqlTransaction transaction = dbConnection.BeginTransaction();
            
            try
            {

                /* verificando se nao existe uma função cadastrada com esse nome */

                if (funcaoDao.findByDescricao(funcao.Descricao, dbConnection) != null)
                    throw new Exception("Já existe uma função cadastrada com esse nome.");

                funcaoNova = funcaoDao.insert(funcao, dbConnection);

                /* incluindo as atividades da função */

                funcao.Atividades.ForEach(delegate(ComentarioFuncao comentarioFuncao)
                {
                    comentarioFuncao.Funcao = funcaoNova;
                    comentarioFuncaoDao.insert(comentarioFuncao, dbConnection);
                });

                transaction.Commit();
            }
            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - IncluirFuncao", ex);
                transaction.Rollback();
                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }
            finally
            {
                FabricaConexao.fecharConexao();
            }

            return funcao;
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public Funcao findFuncaoByDescricao(String descricao)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findFuncaoByDescricao");

            NpgsqlConnection dbConnection = FabricaConexao.getConexao();
            IFuncaoDao funcaoDao = FabricaDao.getFuncaoDao();

            try
            {
                Funcao funcao = funcaoDao.findByDescricao(descricao, dbConnection);

                return funcao;
            }
            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findFuncaoByDescricao", ex);
                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }
            finally
            {
                FabricaConexao.fecharConexao();
            }
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public Funcao findFuncaoById(Int64 idFuncao)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findFuncaoById");

            NpgsqlConnection dbConnection = FabricaConexao.getConexao();
            IFuncaoDao funcaoDao = FabricaDao.getFuncaoDao();
            IComentarioFuncaoDao comentarioFuncaoDao = FabricaDao.getComentarioFuncaoDao();
            try
            {
                Funcao funcao = funcaoDao.findById(idFuncao, dbConnection);
                
                /* preenchendo atividades da função. */

                funcao.Atividades = comentarioFuncaoDao.findAtivosByFuncao(funcao, dbConnection);
                
                return funcao;

            }
            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findFuncaoById", ex);
                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }
            finally
            {
                FabricaConexao.fecharConexao();
            }
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public Funcao updateFuncao(Funcao funcao)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método alteraFuncao");

            Funcao funcaoNova = null;
            NpgsqlConnection dbConnection = FabricaConexao.getConexao();
            IFuncaoDao funcaoDao = FabricaDao.getFuncaoDao();
            NpgsqlTransaction transaction = dbConnection.BeginTransaction();
            try
            {
                /* verificando se já existe uma função cadastrada com esse nome */

                Funcao funcaoProcura = funcaoDao.findByDescricao(funcao.Descricao, dbConnection);

                if (funcaoProcura != null && funcaoProcura.Id != funcao.Id)
                    throw new Exception("Já existe uma função cadastrada com esse nome.");
                
                funcaoNova = funcaoDao.update(funcao, dbConnection);
                transaction.Commit();
            }
            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - alterarFuncao", ex);
                transaction.Rollback();
                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }
            finally
            {
                FabricaConexao.fecharConexao();
            }

            return funcaoNova;
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public DataSet findNormaByFilter(Nota norma)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findNormaByFilter");

            NpgsqlConnection dbConnection = FabricaConexao.getConexao();
            
            INotaDao normaDao = FabricaDao.getNormaDao();

            try
            {
                return normaDao.findByFilter(norma, dbConnection);

            }
            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findNormaByFilter", ex);
                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }
            finally
            {
                FabricaConexao.fecharConexao();
            }
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public DataSet findNormaGheByFilter(Nota norma, Ghe ghe)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findNormaGheByFilter");

            NpgsqlConnection dbConnection = FabricaConexao.getConexao();

            INotaDao normaDao = FabricaDao.getNormaDao();

            try
            {
                return normaDao.findByNotaGhe(norma, ghe, dbConnection);

            }
            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findNormaGheByFilter", ex);
                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }
            finally
            {
                FabricaConexao.fecharConexao();
            }
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public Nota insertNota(Nota nota)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método incluirNorma");

            Nota novaNorma = null;
            NpgsqlConnection dbConnection = FabricaConexao.getConexao();
            INotaDao normaDao = FabricaDao.getNormaDao();
            NpgsqlTransaction transaction = dbConnection.BeginTransaction();

            try
            {

                if (normaDao.findByDescricao(nota, dbConnection) != null)
                    throw new AtividadeJaCadastradaException(NormaJaCadastradaException.msg);
                
                novaNorma = normaDao.insert(nota, dbConnection);
                transaction.Commit();

            }
            catch (DataBaseConectionException ex)
            {
                
                transaction.Rollback();
                LogHelper.logger.Error(this.GetType().Name + " - incluirNorma", ex);
                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
                
            }
            finally
            {
                FabricaConexao.fecharConexao();
            }

            return nota;
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public Nota findNotaById(Int64 id)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findNormaById");

            NpgsqlConnection dbConnection = FabricaConexao.getConexao();

            INotaDao normaDao = FabricaDao.getNormaDao();

            try
            {
                return normaDao.findById(id, dbConnection);

            }
            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findNormaById", ex);
                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }
            finally
            {
                FabricaConexao.fecharConexao();
            }
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public Nota updateNota(Nota nota)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método alterarNorma");

            NpgsqlConnection dbConnection = FabricaConexao.getConexao();
            INotaDao normaDao = FabricaDao.getNormaDao();
            NpgsqlTransaction transaction = dbConnection.BeginTransaction();

            try
            {

                Nota buscaNota = normaDao.findByDescricao(nota, dbConnection);

                if (buscaNota != null && buscaNota.Id != nota.Id)
                    throw new NormaJaCadastradaException(NormaJaCadastradaException.msg);

                if (normaDao.findInGheEstudo(nota, dbConnection))
                    throw new NormaJaUtilizadaException(NormaJaUtilizadaException.msg);

                nota = normaDao.update(nota, dbConnection);
                transaction.Commit();

            }
            catch (DataBaseConectionException ex)
            {

                transaction.Rollback();
                LogHelper.logger.Error(this.GetType().Name + " - incluirNorma", ex);
                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);

            }
            finally
            {
                FabricaConexao.fecharConexao();
            }

            return nota;
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public GradSoma findGradSomaById(GradSoma gradSoma)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findGradSomaById");

            NpgsqlConnection dbConnection = FabricaConexao.getConexao();
            IGradSomaDao gradSomaDao = FabricaDao.getGradSomaDao();
            try
            {
                gradSoma = gradSomaDao.findGradSomaById(gradSoma, dbConnection);
                return gradSoma;

            }
            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findGradSomaById", ex);
                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }
            finally
            {
                FabricaConexao.fecharConexao();
            }
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public GradEfeito findGradEfeitoById(GradEfeito gradEfeito)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findGradEfeitoById");

            NpgsqlConnection dbConnection = FabricaConexao.getConexao();
            IGradEfeitoDao gradEfeitoDao = FabricaDao.getGradEfeitoDao();
            try
            {
                gradEfeito = gradEfeitoDao.findGradEfeitoByGradEfeito(gradEfeito, dbConnection);
                return gradEfeito;

            }
            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findGradEfeitoById", ex);
                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }
            finally
            {
                FabricaConexao.fecharConexao();
            }
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public GradExposicao findGradExposicaoById(GradExposicao gradExposicao)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findGradExposicaoById");

            NpgsqlConnection dbConnection = FabricaConexao.getConexao();
            IGradExposicaoDao gradExposicaoDao = FabricaDao.getGradExposicaoDao();
            try
            {
                gradExposicao = gradExposicaoDao.findGradExposicaoByGradExposicao(gradExposicao, dbConnection);
                return gradExposicao;

            }
            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findGradExposicaoById", ex);
                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }
            finally
            {
                FabricaConexao.fecharConexao();
            }
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public void replicarPpra(Int64 idPpra, string comentario, Boolean indCorrecao)
        {
            NpgsqlConnection dbConnection = FabricaConexao.getConexao();
            IEstudoDao ppraDao = FabricaDao.getEstudoDao();
            ICnaeEstudoDao cnaePpraDao = FabricaDao.getCnaeEstudoDao();
            IGheDao gheDao = FabricaDao.getGheDao();
            IGheNotaDao gheNormaDao = FabricaDao.getGheNormaDao();
            IGheSetorDao gheSetorDao = FabricaDao.getGheSetorDao();
            IGheFonteDao gheFonteDao = FabricaDao.getGheFonteDao();
            IGheSetorClienteFuncaoDao gheSetorClienteFuncaoDao = FabricaDao.getGheSetorClienteFuncaoDao();
            IGheFonteAgenteDao gheFonteAgenteDao = FabricaDao.getGheFonteAgenteDao();
            IGheFonteAgenteEpiDao gheFonteAgenteEpiDao = FabricaDao.getGheFonteAgenteEpiDao();
            ICronogramaDao cronogramaDao = FabricaDao.getCronogramaDao();
            ICronogramaAtividadeDao cronogramaAtividadeDao = FabricaDao.getCronogramaAtividadeDao();
            IRevisaoDao revisaoDao = FabricaDao.getRevisaoDao();
            IRelatorioDao relatorioDao = FabricaDao.getRelatorioDao();
            IEstimativaEstudoDao estimativaEstudoDao = FabricaDao.getEstimativaEstudoDao();
            Int64 idPpraNovo;
            Estudo novoPpra = null;
            Ghe novoGhe;
            Ghe gheAntigo;
            Cronograma cronogramaAntigo;
            Cronograma cronogramaNovo;

            NpgsqlTransaction transaction = dbConnection.BeginTransaction();
            try
            {
                //Replicando cronograma
                cronogramaAntigo = cronogramaDao.findByEstudo(new Estudo(idPpra), dbConnection);
                cronogramaAntigo.CronogramaAtividade = cronogramaAtividadeDao.findByCronograma(cronogramaAntigo, dbConnection);

                cronogramaNovo = cronogramaDao.insert(cronogramaAntigo, dbConnection);
                foreach (CronogramaAtividade cronogramaAtividade in cronogramaAntigo.CronogramaAtividade)
                {
                    cronogramaAtividade.Cronograma = cronogramaNovo;
                    cronogramaAtividadeDao.insert(cronogramaAtividade, dbConnection);
                }

                //Copiando os dados brutos do estudo
                idPpraNovo = ppraDao.replicaEstudo(idPpra, cronogramaNovo.Id, relatorioDao.findUltimaVersaoByTipo(ApplicationConstants.RELATORIO_PPRA, dbConnection), dbConnection);

                //Replicando CNAE_PPRA
                cnaePpraDao.replicaCanaeEstudo(idPpra, idPpraNovo, dbConnection);

                //Replica FUNCAO_EPI
                //estudoDao.replicaFuncaoEpi(idEstudo, idEstudoNovo, dbConnection);

                /* replicando estimativa do estudo */
                foreach (DataRow row in estimativaEstudoDao.findAllByEstudo(new Estudo(idPpra), dbConnection).Tables[0].Rows)
                {
                    estimativaEstudoDao.insert(new EstimativaEstudo(null, new Estudo(idPpraNovo), new Estimativa((Int64)row["id_estimativa"], row["descricao"].ToString(), "A"), (Int32)row["quantidade"]), dbConnection);
                }

                foreach (Ghe ghe in gheDao.findAllByEstudo(idPpra, dbConnection))
                {
                    gheAntigo = gheDao.findById(ghe.Id, dbConnection);
                    gheAntigo.GheSetor = gheSetorDao.findAtivosByGhe(gheAntigo, dbConnection);
                    gheAntigo.GheFonte = gheFonteDao.findAllAtivoByGhe(gheAntigo, dbConnection);

                    //Criando GHE
                    novoPpra = new Estudo(idPpraNovo);
                    ghe.Estudo = novoPpra;
                    novoGhe = gheDao.insert(ghe, dbConnection);

                    //Replicando Normas
                    GheNota gheNorma;
                    foreach (Nota norma in gheAntigo.Norma)
                    {
                        gheNorma = new GheNota(novoGhe, norma);
                        gheNormaDao.insert(gheNorma, dbConnection);
                    }

                    //Replicando setores dos GHE's
                    GheSetor gheSetorNovo;
                    GheSetor gheSetorAntigo;
                    foreach (GheSetor gheSetor in gheAntigo.GheSetor)
                    {
                        gheSetorAntigo = new GheSetor(gheSetor.Id, gheSetor.Setor, gheSetor.Ghe, gheSetor.Situacao);
                        gheSetor.Ghe = novoGhe;
                        gheSetorNovo = gheSetorDao.insert(gheSetor, dbConnection);

                        //Replicando ghe_setor_cliente_funcao
                        foreach (GheSetorClienteFuncao gheSetorClienteFuncao in gheSetorClienteFuncaoDao.findAllByGheSetorClienteFuncao(new GheSetorClienteFuncao(null, null, gheSetorAntigo, false, false, ApplicationConstants.ATIVO, null, false), dbConnection))
                        {
                            gheSetorClienteFuncao.GheSetor = gheSetorNovo;
                            gheSetorClienteFuncaoDao.insert(gheSetorClienteFuncao, dbConnection);
                        }
                    }

                    //Replicando fontes dos GHE's
                    GheFonte gheFonteNovo;
                    GheFonte gheFonteAntigo;
                    foreach (GheFonte gheFonte in gheAntigo.GheFonte)
                    {
                        gheFonteAntigo = new GheFonte(gheFonte.getId(), gheFonte.getFonte(), gheFonte.getGhe(), gheFonte.getPrincipal(), gheFonte.Situacao);
                        gheFonte.setGhe(novoGhe);
                        gheFonteNovo = gheFonteDao.insert(gheFonte, dbConnection);

                        //Replicando ghe_fonte_agente
                        GheFonteAgente gheFonteAgenteNovo;
                        GheFonteAgente gheFonteAgenteAntigo;
                        foreach (GheFonteAgente gheFonteAgente in gheFonteAgenteDao.findAtivosByGheFonteHash(gheFonteAntigo, dbConnection))
                        {
                            gheFonteAgenteAntigo = new GheFonteAgente(gheFonteAgente.Id, gheFonteAgente.GradSoma, gheFonteAgente.GradEfeito, 
                                gheFonteAgente.GradExposicao, gheFonteAgente.GheFonte, gheFonteAgente.Agente, gheFonteAgente.TempoExposicao, gheFonteAgente.Situacao);
                            gheFonteAgente.GheFonte = gheFonteNovo;
                            gheFonteAgenteNovo = gheFonteAgenteDao.insert(gheFonteAgente, dbConnection);

                            //Replicando ghe_fonte_agente_epi
                            foreach (GheFonteAgenteEpi gheFonteAgenteEpi in gheFonteAgenteEpiDao.findAllGheFonteAgenteEpi(gheFonteAgenteAntigo, dbConnection))
                            {
                                gheFonteAgenteEpi.setGheFonteAgente(gheFonteAgenteNovo);
                                gheFonteAgenteEpiDao.insertGheFonteAgenteEpi(gheFonteAgenteEpi, dbConnection);
                            }
                        }
                    }
                }

                Int64? idPpraRaiz = revisaoDao.findRaiz(idPpra, dbConnection);
                string codigoPpra = ppraDao.findCodigoEstudoById(idPpra, dbConnection);
                if (idPpraRaiz == null)
                {
                    idPpraRaiz = idPpra;
                }

                //Alimenta Tabela de Revisão
                Int32 versao = Int32.Parse(codigoPpra.Substring(codigoPpra.Length - 2, 2)) + 1;
                revisaoDao.incluir(new Revisao(null, novoPpra, comentario, new Estudo(idPpraRaiz), new Estudo(idPpra), "PPRA", versao.ToString().PadLeft(2, '0'), null, indCorrecao), dbConnection);

                transaction.Commit();
            }
            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - replicarPpra", ex);
                transaction.Rollback();
                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }
            finally
            {
                FabricaConexao.fecharConexao();
            }
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public Boolean verificaPodeReplicarByRevisao(Int64? idPpra)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método verificaPodeReplicarByRevisao");

            NpgsqlConnection dbConnection = FabricaConexao.getConexao();
            IRevisaoDao revisaoDao = FabricaDao.getRevisaoDao();

            try
            {
                return revisaoDao.verificaPodeReplicar(idPpra, dbConnection);

            }
            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - verificaPodeReplicarByRevisao", ex);
                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }
            finally
            {
                FabricaConexao.fecharConexao();
            }
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public Boolean verificaPodeReplicarByPpra(Int64? idPpra)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método verificaPodeReplicarByPpra");

            NpgsqlConnection dbConnection = FabricaConexao.getConexao();
            IEstudoDao ppraDao = FabricaDao.getEstudoDao();

            try
            {
                return ppraDao.verificaPodeReplicar(idPpra, dbConnection);

            }
            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - verificaPodeReplicarByPpra", ex);
                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }
            finally
            {
                FabricaConexao.fecharConexao();
            }
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public void geraBaseFuncaoEpi(Int64 idPpra)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método geraBaseFuncaoEpi");

            NpgsqlConnection dbConnection = FabricaConexao.getConexao();
            IEstudoDao ppraDao = FabricaDao.getEstudoDao();
            IGheDao gheDao = FabricaDao.getGheDao();
            IGheFonteDao gheFonteDao = FabricaDao.getGheFonteDao();
            IGheFonteAgenteDao gheFonteAgenteDao = FabricaDao.getGheFonteAgenteDao();
            IGheFonteAgenteEpiDao gheFonteAgenteEpiDao = FabricaDao.getGheFonteAgenteEpiDao();
            IFuncaoDao funcaoDao = FabricaDao.getFuncaoDao();

            HashSet<Ghe> gheList = null;
            HashSet<GheFonte> gheFonteList = null;
            HashSet<GheFonteAgente> gheFonteAgenteList = null;
            HashSet<GheFonteAgenteEpi> gheFonteAgenteEpiList = null;
            HashSet<Funcao> funcoes = null;
            FuncaoEpi funcaoEpi = null;

            HashSet<Funcao> funcoesDoPpra = new HashSet<Funcao>();
            HashSet<GheFonteAgenteEpi> episDoPpra = new HashSet<GheFonteAgenteEpi>();

            NpgsqlTransaction transaction = dbConnection.BeginTransaction();

            try
            {
                //Limpando a tabela temporária
                ppraDao.limpaFuncaoEpi(new FuncaoEpi(null, new Estudo(idPpra), null, null, null, null), dbConnection);
                
                gheList = gheDao.findAllByEstudo(idPpra, dbConnection);
                foreach (Ghe ghe in gheList)
                {
                    funcoes = funcaoDao.findByGhe(ghe, dbConnection);
                    foreach (Funcao funcao in funcoes)
                    {
                        funcoesDoPpra.Add(funcao);
                        gheFonteList = gheFonteDao.findAllAtivoByGhe(ghe, dbConnection);
                        foreach (GheFonte gheFonte in gheFonteList)
                        {
                            gheFonteAgenteList = gheFonteAgenteDao.findAtivosByGheFonteHash(gheFonte, dbConnection);
                            foreach (GheFonteAgente gheFonteAgente in gheFonteAgenteList)
                            {
                                gheFonteAgenteEpiList = gheFonteAgenteEpiDao.findAllGheFonteAgenteEpi(gheFonteAgente, dbConnection);
                                String tipoObrigatoriedade;
                                FuncaoEpi funcaoEpiAlteraObrigatoriedade;
                                foreach (GheFonteAgenteEpi gheFonteAgenteEpi in gheFonteAgenteEpiList)
                                {
                                    if (gheFonteAgenteEpi.getClassificacao().Equals(ApplicationConstants.EPI))
                                    {
                                        if (!ppraDao.verificaExisteFuncaoEpi(funcao, gheFonteAgenteEpi.getEpi(), idPpra, dbConnection))
                                        {
                                            episDoPpra.Add(gheFonteAgenteEpi);
                                            funcaoEpi = new FuncaoEpi(null, new Estudo(idPpra), funcao.Descricao, gheFonteAgenteEpi.getEpi().Descricao, gheFonteAgenteEpi.getTipo(), ApplicationConstants.EPI);
                                            ppraDao.preencheFuncaoEpi(funcaoEpi, dbConnection);
                                        }
                                        else
                                        {
                                            tipoObrigatoriedade = ppraDao.buscaTipoObrigatoriedadeFuncaoEpi(funcao, gheFonteAgenteEpi.getEpi(), idPpra, dbConnection);
                                            if (tipoObrigatoriedade.Equals(ApplicationConstants.EVENTUAL) && gheFonteAgenteEpi.getTipo().Equals(ApplicationConstants.OBRIGATORIO))
                                            {
                                                //Atualizar para Obrigatório
                                                funcaoEpiAlteraObrigatoriedade = ppraDao.buscaFuncaoEpiByEstudoFuncaoEpi(funcao, gheFonteAgenteEpi.getEpi(), idPpra, dbConnection);
                                                funcaoEpiAlteraObrigatoriedade.setTipo(ApplicationConstants.OBRIGATORIO);
                                                ppraDao.alteraTipoObrigatoriedadeFuncaoEpi(funcaoEpiAlteraObrigatoriedade, dbConnection);
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }

                //Gravando as situações não aplicáveis
                foreach (Funcao funcao in funcoesDoPpra)
                {
                    foreach (GheFonteAgenteEpi gheFonteAgenteEpi in episDoPpra)
                    {
                        if (!ppraDao.verificaExisteFuncaoEpi(funcao, gheFonteAgenteEpi.getEpi(), idPpra, dbConnection))
                        {
                            if (gheFonteAgenteEpi.getClassificacao().Equals(ApplicationConstants.EPI))
                            {
                                ppraDao.preencheFuncaoEpi(new FuncaoEpi(null, new Estudo(idPpra), funcao.Descricao, gheFonteAgenteEpi.getEpi().Descricao, ApplicationConstants.NAO_APLICA, ApplicationConstants.EPI), dbConnection);
                            }
                        }
                    }
                }

                transaction.Commit();
            }
            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - geraBaseFuncaoEpi", ex);
                transaction.Rollback();
                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }
            finally
            {
                FabricaConexao.fecharConexao();
            }
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public void mudaEstadoPpra(Estudo ppra, String estado, Produto produtoSelecionado)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método mudaEstadoPpra"); ;
            
            NpgsqlConnection dbConnection = FabricaConexao.getConexao();

            NpgsqlTransaction transaction = dbConnection.BeginTransaction();

            IEstudoDao ppraDao = FabricaDao.getEstudoDao();
            IGheDao gheDao = FabricaDao.getGheDao();
            IGheSetorDao gheSetorDao = FabricaDao.getGheSetorDao();
            IGheSetorClienteFuncaoDao gheSetorClienteFuncaoDao = FabricaDao.getGheSetorClienteFuncaoDao();
            IMovimentoDao movimentoDao = FabricaDao.getMovimentoDao();
            IContratoDao contratoDao = FabricaDao.getContratoDao();
            IProdutoDao produtoDao = FabricaDao.getProdutoDao();
            IContratoProdutoDao contratoProdutoDao = FabricaDao.getContratoProdutoDao();
            IClienteDao clienteDao = FabricaDao.getClienteDao();

            // Int64 nTotalExposto;
            Int64 idPpra;

            HashSet<Ghe> listaTodosGhe = new HashSet<Ghe>();
            HashSet<GheSetor> listaTodosGheSetor = new HashSet<GheSetor>();
            HashSet<GheSetorClienteFuncao> listaTodosGheSetorFuncao = new HashSet<GheSetorClienteFuncao>();
            IRevisaoDao revisaoDao = FabricaDao.getRevisaoDao();

            try
            {

                #region validação

                // Regra de Validação 1 - Não pode existir estudo sem técnico.
                // Regra não se aplica mais devido a uma mudança de regra de negócio da empresa
                // Pode existir um PPRA sem técnico
                // 26/04/2013
                // WElbert Santos

                /*
                if (pcmso.Tecno == null)
                {
                    throw new PpraException(PpraException.msg12);
                }
                */

                // Regra de Validação 2 - Não pode existir PPRA sem um GHE.

                idPpra = (Int64)ppra.Id;

                listaTodosGhe = gheDao.findAllByEstudo(idPpra, dbConnection);

                if (listaTodosGhe.Count == 0)
                {
                    throw new PpraException(PpraException.msg7);
                }

                // Regra de Validação 3 - Não pode existir um GHE sem um Setor.

                foreach (Ghe ghe in listaTodosGhe)
                {
                    listaTodosGheSetor = gheSetorDao.findAtivosByGhe(ghe, dbConnection);

                    if (listaTodosGheSetor.Count == 0)
                    {
                        throw new PpraException(PpraException.msg8);
                    }
                    else
                    {
                        // Regra de validação 4 - Não pode existir um Setor sem função

                        foreach (GheSetor gheSetor in listaTodosGheSetor)
                        {
                            listaTodosGheSetorFuncao = gheSetorClienteFuncaoDao.findAtivosByGheSetor(gheSetor, dbConnection);

                            if (listaTodosGheSetorFuncao.Count == 0)
                            {
                                throw new PpraException(PpraException.msg9);
                            }

                            /* regra 7 - 17/04/2015
                             * Não pode existir um gheSetorClienteFuncao sem comentarioFuncao */

                            foreach (GheSetorClienteFuncao gheSetorClienteFuncao in listaTodosGheSetorFuncao)
                            {
                                if (!gheSetorClienteFuncaoDao.IsNotUsedComentarioByGheSetorClienteFuncao(gheSetorClienteFuncao, dbConnection))
                                {
                                    throw new PpraException(PpraException.msg13);
                                }
                            }

                        }
                    }
                }

                /*
                Regra de validacao 5 - o numero total de exposto tem que corresponder ao numero total de
                funcionário do cliente
                2014-07-26 - welbert.
                 * Essa regra será desativada pois o número de expostos no ghe deve ser do estudo e não do cliente.
                 */

                /*
                nTotalExposto = gheDao.FindNtotalExpostoByGhe(ppra, dbConnection);

                if (nTotalExposto != ((Int64)ppra.getVendedorcliente().getCliente().getEstFem() + (Int64)ppra.getVendedorcliente().getCliente().getEstMasc()))
                {
                    throw new PpraException(PpraException.msg6);
                }

                */
                // Regra numero 6 - Não pode existir um cronograma sem atividade.

                if (ppra.Cronograma != null)
                {
                    if (ppra.Cronograma.CronogramaAtividade.Count == 0)
                    {
                        throw new PpraException(PpraException.msg10);
                    }

                }
                else
                {
                    throw new PpraException(PpraException.msg11);
                }

                #endregion


                Cliente clienteCobrado = ppra.VendedorCliente.getCliente().CredenciadaCliente != null ? ppra.VendedorCliente.getCliente().CredenciadaCliente : ppra.VendedorCliente.getCliente();
                
                clienteCobrado = clienteDao.findById((Int64)clienteCobrado.Id, dbConnection);


                if (String.Equals(estado, ApplicationConstants.ATIVO))
                {
                    ppraDao.changeEstado(ppra, estado, dbConnection);

                }
                else if (String.Equals(estado, ApplicationConstants.CONSTRUCAO))
                {
                    // retirar elaborador do banco de dados.
                    ppra.Engenheiro = null;
                    ppraDao.changeEstado(ppra, estado, dbConnection);
                }
                
                if (estado.Equals("F"))
                {
                    // o engenheiro será gravado através de uma escolha na tela principal do aso.
                    // 26/04/2013 - welbert santos

                    ppraDao.finalizaEstudo(ppra, dbConnection);

                    // incluindo no movimento o estudo.
                    // buscando informações do contrato do cliente.

                    // verificando se o cliente será cobrado por tabela ou contrato.

                    if (!revisaoDao.isCorrecao(ppra.Id, dbConnection))
                    {
                        if (ppra.VendedorCliente.getCliente().UsaContrato)
                        {

                            Contrato contratoVigente = contratoDao.findContratoVigenteByData((DateTime)ppra.DataCriacao, ppra.VendedorCliente.getCliente(), null, dbConnection);

                            if (contratoVigente == null)
                            {
                                throw new ContratoException(ContratoException.msg2);
                            }
                            
                            ContratoProduto contratoProduto = contratoProdutoDao.findByContratoAndProduto(contratoVigente, produtoSelecionado, dbConnection);
                            
                            if (contratoProduto == null)
                            {
                                throw new ContratoException(ContratoException.msg3);
                            }
                            
                            Movimento movimento = new Movimento(null, null, null, clienteCobrado, contratoProduto, null, DateTime.Now, null, ApplicationConstants.PENDENTE, Convert.ToDecimal(contratoProduto.getPrecoContratado()), Convert.ToDecimal(ValidaCampoHelper.CalculaValorComissao(contratoProduto, ppra.VendedorCliente.getVendedor())),  ppra, null, Convert.ToInt32(1), PermissionamentoFacade.usuarioAutenticado, PermissionamentoFacade.usuarioAutenticado.Empresa, contratoProduto.getCustoContrato(), null, null, DateTime.Now, null);
                            movimentoDao.insert(movimento, dbConnection);

                        }
                        else
                        {
                            // inserindo no movimento sem contrato

                            Contrato contratoSistema = contratoDao.findContratoSistemaByCliente(clienteCobrado, dbConnection);

                            if (contratoSistema == null)
                            {
                                // criando contrato para o cliente.
                                contratoSistema = contratoDao.insert(new Contrato(null, String.Empty, clienteCobrado,
                                    PermissionamentoFacade.usuarioAutenticado, null, null, null, ApplicationConstants.CONTRATO_FECHADO, String.Empty, null, null, String.Empty, null, true, null, null, false, false, false, null, false, null, null, null, null, null, false, null, null, null, String.Empty, PermissionamentoFacade.usuarioAutenticado.Empresa), dbConnection);

                                // criando contrato produto.
                                contratoProdutoDao.insert(new ContratoProduto(null, produtoSelecionado, contratoSistema,
                                    produtoSelecionado.Preco, PermissionamentoFacade.usuarioAutenticado, produtoSelecionado.Custo, null, String.Empty, true), dbConnection);


                                // buscando contrato produto

                                ContratoProduto contratoProdutoSelecinado = contratoProdutoDao.findByContratoAndProduto(contratoSistema, produtoSelecionado, dbConnection);

                                Movimento movimento = new Movimento(null, null, null, clienteCobrado, contratoProdutoSelecinado, null, DateTime.Now, null, ApplicationConstants.PENDENTE, Convert.ToDecimal(contratoProdutoSelecinado.getPrecoContratado()), Convert.ToDecimal(ValidaCampoHelper.CalculaValorComissao(contratoProdutoSelecinado, ppra.VendedorCliente.getVendedor())), ppra, null, Convert.ToInt32(1), PermissionamentoFacade.usuarioAutenticado, PermissionamentoFacade.usuarioAutenticado.Empresa, contratoProdutoSelecinado.getCustoContrato(), null, null, DateTime.Now, null);
                                movimentoDao.insert(movimento, dbConnection);

                            }
                            else
                            {
                                
                                ContratoProduto contratoProdutoSelecinado = contratoProdutoDao.findByContratoAndProduto(contratoSistema, produtoSelecionado, dbConnection);

                                if (contratoProdutoSelecinado != null)
                                {
                                    // atualizar o preço do produto
                                    contratoProdutoSelecinado.setPrecoContratado(produtoSelecionado.Preco);
                                    contratoProdutoSelecinado.setCustoContrato(produtoSelecionado.Custo);
                                    contratoProdutoDao.update(contratoProdutoSelecinado, dbConnection);
                                }
                                else
                                {
                                    contratoProdutoDao.insert(new ContratoProduto(null, produtoSelecionado, contratoSistema,
                                        produtoSelecionado.Preco, PermissionamentoFacade.usuarioAutenticado, null, null, String.Empty, true), dbConnection);
                                    
                                }

                                contratoProdutoSelecinado = contratoProdutoDao.findByContratoAndProduto(contratoSistema, produtoSelecionado, dbConnection);

                                Movimento movimento = new Movimento(null, null, null, clienteCobrado, contratoProdutoSelecinado, null, DateTime.Now, null, ApplicationConstants.PENDENTE, Convert.ToDecimal(contratoProdutoSelecinado.getPrecoContratado()), Convert.ToDecimal(ValidaCampoHelper.CalculaValorComissao(contratoProdutoSelecinado, ppra.VendedorCliente.getVendedor())), ppra, null, Convert.ToInt32(1), PermissionamentoFacade.usuarioAutenticado, PermissionamentoFacade.usuarioAutenticado.Empresa, contratoProdutoSelecinado.getCustoContrato(), null, null, DateTime.Now, null);
                                movimentoDao.insert(movimento, dbConnection);

                            }

                        }
                    }
                    
                }

                transaction.Commit();

            }
            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - mudaEstadoPpra", ex);
                transaction.Rollback();
                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }
            finally
            {
                FabricaConexao.fecharConexao();
            }
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public void montaQuadroVersao(Int64 idPpra)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método montaQuadroRevisao"); ;

            NpgsqlConnection dbConnection = FabricaConexao.getConexao();
            NpgsqlTransaction transaction = dbConnection.BeginTransaction();
            IEstudoDao ppraDao = FabricaDao.getEstudoDao();
            IRevisaoDao revisaoDao = FabricaDao.getRevisaoDao();
            LinkedList<Revisao> revisoes = new LinkedList<Revisao>();
            Revisao revisao = null;
            
            try
            {
                ppraDao.limpaVersao(idPpra, dbConnection);

                revisao = revisaoDao.findRevisao(idPpra, dbConnection);

                while (true)
                {
                    if (revisao.Estudo.Id == revisao.EstudoRevisao.Id)
                    {
                        revisoes.AddLast(revisao);
                        break;
                    }
                    else
                    {
                        revisoes.AddLast(revisao);
                        revisao = revisaoDao.findRevisao(revisao.EstudoRevisao.Id, dbConnection);
                    }
                }

                // Gravando versões no quadro.
                foreach (Revisao revisaoEncontrada in revisoes)
                {
                    revisaoEncontrada.Estudo.Id = idPpra;

                    if (!revisaoEncontrada.IndCorrecao)
                    {
                        ppraDao.gravaVersao(new Versao(null, revisaoEncontrada.Estudo, revisaoEncontrada.VersaoEstudo, revisaoEncontrada.Comentario, revisaoEncontrada.DataRevisao), dbConnection);
                    }
                }

                transaction.Commit();
            }
            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - montaQuadroRevisao", ex);
                transaction.Rollback();
                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }
            finally
            {
                FabricaConexao.fecharConexao();
            }
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public void incluiAnexo(Anexo anexo)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método incluiAnexo");

            NpgsqlConnection dbConnection = FabricaConexao.getConexao();

            IAnexoDao anexoDao = FabricaDao.getAnexoDao();

            NpgsqlTransaction transaction = dbConnection.BeginTransaction();

            try
            {

                //validando descricao digitada se já existe no estudo.

                if (anexoDao.findAnexoByConteudo(anexo, dbConnection) == null)
                {
                    anexoDao.insertAnexo(anexo, dbConnection);
                    transaction.Commit();

                }
                else
                {
                    throw new AnexoException(AnexoException.msg1);
                }


            }
            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - incluiAnexo", ex);
                transaction.Rollback();
                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }
            finally
            {
                FabricaConexao.fecharConexao();
            }

        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public DataSet findAllAnexoByEstudo(Estudo ppra)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findAnexoByEstudo");

            NpgsqlConnection dbConnection = FabricaConexao.getConexao();

            IAnexoDao anexoDao = FabricaDao.getAnexoDao();

            try
            {
                return anexoDao.findAllAnexoByEstudo(ppra, dbConnection);

            }
            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findAnexoByEstudo", ex);

                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }
            finally
            {
                FabricaConexao.fecharConexao();
            }
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public void excluiAnexo(Anexo anexo)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método excluiAnexo");

            NpgsqlConnection dbConnection = FabricaConexao.getConexao();

            IAnexoDao anexoDao = FabricaDao.getAnexoDao();

            NpgsqlTransaction transaction = dbConnection.BeginTransaction();

            try
            {
                anexoDao.deleteAnexo(anexo, dbConnection);
                transaction.Commit();

            }
            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - excluiAnexo", ex);
                transaction.Rollback();
                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }
            finally
            {
                FabricaConexao.fecharConexao();
            }
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public void alteraAnexo(Anexo anexo)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método alteraAnexo");

            NpgsqlConnection dbConnection = FabricaConexao.getConexao();

            IAnexoDao anexoDao = FabricaDao.getAnexoDao();

            Anexo anexoProcurado = null;

            NpgsqlTransaction transaction = dbConnection.BeginTransaction();

            try
            {

                //validando descricao digitada se já existe no estudo.

                anexoProcurado = anexoDao.findAnexoByConteudo(anexo, dbConnection);

                if (anexoProcurado != null && anexo.getId() != anexoProcurado.getId())
                {
                    throw new AnexoException(AnexoException.msg1);


                }
                else
                {
                    anexoDao.updateAnexo(anexo, dbConnection);
                    transaction.Commit();
                }


            }
            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - incluiAnexo", ex);
                transaction.Rollback();
                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }
            finally
            {
                FabricaConexao.fecharConexao();
            }

        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public DataSet findRisco()
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findRisco");

            NpgsqlConnection dbConnection = FabricaConexao.getConexao();

            IRiscoDao riscoDao = FabricaDao.getRiscoDao();

            try
            {
                return riscoDao.findRisco(dbConnection);

            }
            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findRisco", ex);
                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }
            finally
            {
                FabricaConexao.fecharConexao();
            }
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public GheFonte findGheFonteByGheFonteInEpi(GheFonte gheFonte)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findGheFonteByGheFonteInEpi");

            NpgsqlConnection dbConnection = FabricaConexao.getConexao();

            IGheFonteDao gheFonteDao = FabricaDao.getGheFonteDao();

            try
            {
                return gheFonteDao.findGheFonteInEpi(gheFonte, dbConnection);

            }
            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findGheFonteByGheFonteInEpi", ex);
                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }
            finally
            {
                FabricaConexao.fecharConexao();
            }
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public DataSet findAllFonteByGheInEpi(Ghe ghe)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findAllFonteByGheDataSet");

            NpgsqlConnection dbConnection = FabricaConexao.getConexao();
            IGheFonteDao gheFonteDao = FabricaDao.getGheFonteDao();
            try
            {
                return gheFonteDao.findAllByGheInEpi(ghe, dbConnection);

            }
            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findAllFonteByGhe", ex);
                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }
            finally
            {
                FabricaConexao.fecharConexao();
            }
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public Boolean findGheFonteAgenteByAgente(Agente agente)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findGheFonteAgenteByAgente");

            NpgsqlConnection dbConnection = FabricaConexao.getConexao();

            IGheFonteAgenteDao gheFonteAgenteDao = FabricaDao.getGheFonteAgenteDao();

            try
            {
                return gheFonteAgenteDao.isUsedByAgente(agente, dbConnection);

            }
            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findGheFonteAgenteByAgente", ex);
                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }
            finally
            {
                FabricaConexao.fecharConexao();
            }
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public Boolean findCronogramaAtividadeByAtividade(Atividade atividade)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findCronogramaAtividadeByAtividade");

            NpgsqlConnection dbConnection = FabricaConexao.getConexao();

            ICronogramaAtividadeDao cronogramaAtividadeDao = FabricaDao.getCronogramaAtividadeDao();
            
            try
            {
                return cronogramaAtividadeDao.isAtividadeCronogramaIsUsedByAtividade(atividade, dbConnection);

            }
            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findCronogramaAtividadeByAtividade", ex);
                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }
            finally
            {
                FabricaConexao.fecharConexao();
            }
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public Boolean findGheFonteAgenteEpiByEpi(Epi epi)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findGheFonteAgenteEpiByEpi");

            NpgsqlConnection dbConnection = FabricaConexao.getConexao();

            IGheFonteAgenteEpiDao gheFonteAgenteEpiDao = FabricaDao.getGheFonteAgenteEpiDao();

            try
            {
                return gheFonteAgenteEpiDao.findGheFonteAgenteEpiByEpi(epi, dbConnection);

            }
            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findGheFonteAgenteEpiByEpi", ex);
                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }
            finally
            {
                FabricaConexao.fecharConexao();
            }
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public Boolean verificaPodeAlterarFonte(Fonte fonte)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findGheFonteByFonte");

            NpgsqlConnection dbConnection = FabricaConexao.getConexao();

            IGheFonteDao gheFonteDao = FabricaDao.getGheFonteDao();

            try
            {
                return gheFonteDao.verificaPodeAlterarFonte(fonte, dbConnection);

            }
            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findGheFonteByFonte", ex);
                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }
            finally
            {
                FabricaConexao.fecharConexao();
            }
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public Boolean findClienteFuncaoByFuncao(Funcao funcao)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findClienteFuncaoByFuncao");

            NpgsqlConnection dbConnection = FabricaConexao.getConexao();

            IClienteFuncaoDao clienteFuncaoDao = FabricaDao.getClienteFuncaoDao();

            try
            {
                return clienteFuncaoDao.funcaoIsUsedInEstudo(funcao, dbConnection);

            }
            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findClienteFuncaoByFuncao", ex);
                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }
            finally
            {
                FabricaConexao.fecharConexao();
            }
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public Boolean findGheSetorBySetor(Setor setor)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findGheSetorBySetor");

            NpgsqlConnection dbConnection = FabricaConexao.getConexao();

            IGheSetorDao gheSetorDao = FabricaDao.getGheSetorDao();

            try
            {
                return gheSetorDao.gheSetorIsUsedBySetor(setor, dbConnection);

            }
            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findGheSetorBySetor", ex);
                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }
            finally
            {
                FabricaConexao.fecharConexao();
            }
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public ClienteFuncao findClienteFuncaoById(Int64 id)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findClienteFuncaoById");

            NpgsqlConnection dbConnection = FabricaConexao.getConexao();

            IClienteFuncaoDao clienteFuncaoDao = FabricaDao.getClienteFuncaoDao();
            IClienteDao clienteDao = FabricaDao.getClienteDao();
            IFuncaoDao funcaoDao = FabricaDao.getFuncaoDao();

            ClienteFuncao clienteFuncaoRetorno = null;

            try
            {
                // buscando o cliente Funcao
                clienteFuncaoRetorno = clienteFuncaoDao.findById(id, dbConnection);

                // preenchendo o clienteFuncao já com os métodos prontos.
                clienteFuncaoRetorno.Cliente = clienteDao.findById((Int64)clienteFuncaoRetorno.Cliente.Id, dbConnection);

                // preenchendo o Funcao já com os métodos prontos.
                clienteFuncaoRetorno.Funcao = funcaoDao.findById((Int64)clienteFuncaoRetorno.Funcao.Id, dbConnection);

                return clienteFuncaoRetorno;

            }
            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findClienteFuncaoById", ex);
                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }
            finally
            {
                FabricaConexao.fecharConexao();
            }
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public DataSet findAllFuncaoAtivaNotInCliente(Funcao funcao, Boolean select, List<ClienteFuncao> funcoes)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findAllFuncaoAtiva");

            NpgsqlConnection dbConnection = FabricaConexao.getConexao();

            IFuncaoDao funcaoDao = FabricaDao.getFuncaoDao();

            try
            {
                return funcaoDao.findAtivaNotInClient(funcao, select, funcoes, dbConnection);

            }
            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findAllFuncaoAtiva", ex);
                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }
            finally
            {
                FabricaConexao.fecharConexao();
            }
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public Estudo findByCodigo(String codigo, String tipoEstudo)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findByCodigo");

            NpgsqlConnection dbConnection = FabricaConexao.getConexao();

            Estudo ppraProcura = null;

            IEstudoDao estudoDao = FabricaDao.getEstudoDao();

            IClienteVendedorDao clienteVendedorDao = FabricaDao.getClienteVendedorDao();
            ICronogramaAtividadeDao cronogramaAtividadeDao = FabricaDao.getCronogramaAtividadeDao();
            ICronogramaDao cronogramaDao = FabricaDao.getCronogramaDao();
            IUsuarioDao usuarioDao = FabricaDao.getUsuarioDao();
            IClienteDao clienteDao = FabricaDao.getClienteDao();

            try
            {

                ppraProcura = estudoDao.findByCodigoAndTipo(codigo, tipoEstudo, dbConnection);

                if (ppraProcura != null)
                {

                    /* setando o clienteVendedor para o estudo */
                    if (ppraProcura.VendedorCliente != null)
                    {
                        ppraProcura.VendedorCliente = (VendedorCliente)clienteVendedorDao.findById((Int64)ppraProcura.VendedorCliente.getId(), dbConnection);
                    }

                    /* setando o técnico que elaborou o estudo */
                    if (ppraProcura.Tecno != null)
                    {
                        ppraProcura.Tecno = usuarioDao.findById((Int64)ppraProcura.Tecno.Id, dbConnection);
                    }

                    /* setando o engenheiro responsável pelo estudo */
                    if (ppraProcura.Engenheiro != null)
                    {
                        ppraProcura.Engenheiro = usuarioDao.findById((Int64)ppraProcura.Engenheiro.Id, dbConnection);
                    }

                    /* setando informações do contratante para o estudo */
                    if (ppraProcura.ClienteContratante != null)
                    {
                        ppraProcura.ClienteContratante = clienteDao.findById((Int64)ppraProcura.ClienteContratante.Id, dbConnection);
                    }

                    /* setando informacoes do cronograma para o estudo */
                    if (ppraProcura.Cronograma != null && ppraProcura.Cronograma.Id != null)
                    {
                        ppraProcura.Cronograma = cronogramaDao.findById((Int64)ppraProcura.Cronograma.Id, dbConnection);
                        ppraProcura.Cronograma.CronogramaAtividade = cronogramaAtividadeDao.findByCronograma(ppraProcura.Cronograma, dbConnection);
                    }
                }
            }
            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findByCodigo", ex);

                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }
            finally
            {
                FabricaConexao.fecharConexao();
            }

            return ppraProcura;
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public Boolean findPpraInProtocolo(Estudo ppra)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findPpraInProtocolo");

            NpgsqlConnection dbConnection = FabricaConexao.getConexao();

            IEstudoDao estudoDao = FabricaDao.getEstudoDao();

            try
            {
                return estudoDao.findEstudoInProtocolo(ppra, ApplicationConstants.PPRA, dbConnection);
            }
            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findPpraInProtocolo", ex);
                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }
            finally
            {
                FabricaConexao.fecharConexao();
            }

        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public void incluiGheNorma(GheNota gheNorma)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método incluiGheNorma");

            NpgsqlConnection dbConnection = FabricaConexao.getConexao(); // criando conexao com o banco de dados.

            IGheNotaDao gheNormaDao = FabricaDao.getGheNormaDao();

            NpgsqlTransaction transaction = dbConnection.BeginTransaction();
            
            try
            {
                gheNormaDao.insert(gheNorma, dbConnection);

                transaction.Commit();
            }
            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - incluiGheNorma", ex);
                transaction.Rollback();
                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }
            finally
            {
                FabricaConexao.fecharConexao();
            }

        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public DataSet findAllNotasByGhe(Ghe ghe)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findAllNotasByGhe");

            NpgsqlConnection dbConnection = FabricaConexao.getConexao(); // criando conexao com o banco de dados.

            IGheNotaDao gheNormaDao = FabricaDao.getGheNormaDao();

            try
            {
                return gheNormaDao.findAllByGheDataSet(ghe, dbConnection);

            }
            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findAllNotasByGhe", ex);
                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }
            finally
            {
                FabricaConexao.fecharConexao();
            }

        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public DataSet findAllGheFonteAgenteEpiByGHeFonteAgente(GheFonteAgente gheFonteAgente)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findAllGheFonteAgenteEpiByGHeFonteAgente");

            NpgsqlConnection dbConnection = FabricaConexao.getConexao(); // criando conexao com o banco de dados.

            IGheFonteAgenteEpiDao gheFonteAgenteEpiDao = FabricaDao.getGheFonteAgenteEpiDao();

            try
            {
                return gheFonteAgenteEpiDao.findAllGheFonteAgenteEpiByGHeFonteAgente(gheFonteAgente, dbConnection);

            }
            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findAllGheFonteAgenteEpiByGHeFonteAgente", ex);
                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }
            finally
            {
                FabricaConexao.fecharConexao();
            }

        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public DataSet findAllGheSetorClienteFuncaoByGheSetor(GheSetor gheSetor)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findAllGheSetorClienteFuncaoByGheSetor");

            NpgsqlConnection dbConnection = FabricaConexao.getConexao();

            IGheSetorClienteFuncaoDao gheSetorClienteFuncaoDao = FabricaDao.getGheSetorClienteFuncaoDao();

            try
            {
                return gheSetorClienteFuncaoDao.findAllByGheSetor(gheSetor, dbConnection);

            }
            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findAllGheSetorClienteFuncaoByGheSetor", ex);
                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }
            finally
            {
                FabricaConexao.fecharConexao();
            }
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public List<ComentarioFuncao> findAllComentarioFuncaoAtivoByFuncao(Funcao funcao)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findAllComentarioFuncaoAtivoByFuncao");

            NpgsqlConnection dbConnection = FabricaConexao.getConexao();

            IComentarioFuncaoDao comentarioFuncaoDao = FabricaDao.getComentarioFuncaoDao();

            try
            {
                return comentarioFuncaoDao.findAtivosByFuncao(funcao, dbConnection);

            }
            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findAllComentarioFuncaoAtivoByFuncao", ex);
                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }
            finally
            {
                FabricaConexao.fecharConexao();
            }
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public ComentarioFuncao insertComentarioFuncao(ComentarioFuncao comentarioFuncao)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método incluirComentarioFuncao");

            NpgsqlConnection dbConnection = FabricaConexao.getConexao();
            
            IComentarioFuncaoDao comentarioFuncaoDao = FabricaDao.getComentarioFuncaoDao();
            
            NpgsqlTransaction transaction = dbConnection.BeginTransaction();

            ComentarioFuncao comentarioFuncaoNovo = null;

            try
            {
                comentarioFuncaoNovo = comentarioFuncaoDao.insert(comentarioFuncao, dbConnection);
                transaction.Commit();
            }
            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - incluirComentarioFuncao", ex);
                transaction.Rollback();
                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }
            finally
            {
                FabricaConexao.fecharConexao();
            }

            return comentarioFuncaoNovo;
            
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public void updateComentarioFuncao(ComentarioFuncao comentarioFuncao)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método updateComentarioFuncao");

            NpgsqlConnection dbConnection = FabricaConexao.getConexao();

            IComentarioFuncaoDao comentarioFuncaoDao = FabricaDao.getComentarioFuncaoDao();

            NpgsqlTransaction transaction = dbConnection.BeginTransaction();

            try
            {
                comentarioFuncaoDao.update(comentarioFuncao, dbConnection);
                transaction.Commit();
            }
            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - updateComentarioFuncao", ex);
                transaction.Rollback();
                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }
            finally
            {
                FabricaConexao.fecharConexao();
            }

        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public DataSet findAllEstimativaNotInEstudo(Estudo estudo, Estimativa estimativa)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findAllEstimativaNotInEstudo");

            NpgsqlConnection dbConnection = FabricaConexao.getConexao();

            IEstimativaDao estimativaDao = FabricaDao.getEstimativaDao();
            
            try
            {
                return estimativaDao.findAllNotInEstudo(estudo, estimativa, dbConnection);
            }
            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findAllEstimativaNotInEstudo", ex);
                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }
            finally
            {
                FabricaConexao.fecharConexao();
            }

        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public EstimativaEstudo insertEstimativaEstudo(EstimativaEstudo estimativaEstudo)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método insertEstimativaEstudo");

            NpgsqlConnection dbConnection = FabricaConexao.getConexao();

            IEstimativaEstudoDao estimativaEstudoDao = FabricaDao.getEstimativaEstudoDao();

            NpgsqlTransaction transaction = dbConnection.BeginTransaction();

            try
            {
                EstimativaEstudo estimativaEstudoNew = estimativaEstudoDao.insert(estimativaEstudo, dbConnection);
                transaction.Commit();
                return estimativaEstudoNew;

            }
            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - insertEstimativaEstudo", ex);
                transaction.Rollback();
                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }
            finally
            {
                FabricaConexao.fecharConexao();
            }

        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public DataSet findAllEstimativaInEstudo(Estudo estudo)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findAllEstimativaInEstudo");

            NpgsqlConnection dbConnection = FabricaConexao.getConexao();

            IEstimativaEstudoDao estimativaEstudoDao = FabricaDao.getEstimativaEstudoDao();

            try
            {
                return estimativaEstudoDao.findAllByEstudo(estudo, dbConnection);
            }
            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findAllEstimativaInEstudo", ex);
                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }
            finally
            {
                FabricaConexao.fecharConexao();
            }

        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public EstimativaEstudo updateEstimaEstudo(EstimativaEstudo estimativaEstudo)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método updateEstimaEstudo");

            NpgsqlConnection dbConnection = FabricaConexao.getConexao();

            IEstimativaEstudoDao estimativaEstudoDao = FabricaDao.getEstimativaEstudoDao();

            NpgsqlTransaction transaction = dbConnection.BeginTransaction();

            try
            {
                EstimativaEstudo estimativaEstudoUpdate = estimativaEstudoDao.update(estimativaEstudo, dbConnection);
                transaction.Commit();
                return estimativaEstudoUpdate;

            }
            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - updateEstimaEstudo", ex);
                transaction.Rollback();
                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }
            finally
            {
                FabricaConexao.fecharConexao();
            }

        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public void deleteEstimativaEstudo(EstimativaEstudo estimativaEstudo)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método deleteEstimativaEstudo");

            NpgsqlConnection dbConnection = FabricaConexao.getConexao();

            IEstimativaEstudoDao estimativaEstudoDao = FabricaDao.getEstimativaEstudoDao();

            NpgsqlTransaction transaction = dbConnection.BeginTransaction();

            try
            {
                estimativaEstudoDao.delete(estimativaEstudo, dbConnection);
                transaction.Commit();

            }
            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - updateEstimaEstudo", ex);
                transaction.Rollback();
                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }
            finally
            {
                FabricaConexao.fecharConexao();
            }

        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public HashSet<GheFonte> findAllGheFonteByGhe(Ghe ghe)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findAllGheFonteByGhe");

            NpgsqlConnection dbConnection = FabricaConexao.getConexao();

            IGheFonteDao gheFonteDao = FabricaDao.getGheFonteDao();

            try
            {
                return gheFonteDao.findAllAtivoByGhe(ghe, dbConnection);

            }
            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findAllGheFonteByGhe", ex);
                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }
            finally
            {
                FabricaConexao.fecharConexao();
            }
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public GheSetorClienteFuncao findGheSetorClienteFuncaoById(Int64 id)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findGheSetorClienteFuncaoById");

            NpgsqlConnection dbConnection = FabricaConexao.getConexao();
            IGheSetorClienteFuncaoDao gheSetorClienteFuncaoDao = FabricaDao.getGheSetorClienteFuncaoDao();
            IClienteFuncaoDao clienteFuncaoDao = FabricaDao.getClienteFuncaoDao();
            IGheSetorDao gheSetorDao = FabricaDao.getGheSetorDao();
            IComentarioFuncaoDao comentarioFuncaoDao = FabricaDao.getComentarioFuncaoDao();

            GheSetorClienteFuncao gheSetorClienteFuncao = null;

            try
            {
                gheSetorClienteFuncao = gheSetorClienteFuncaoDao.findById(id, dbConnection);

                /* preencheendo demais objetos */

                if (gheSetorClienteFuncao.ClienteFuncao != null)
                    gheSetorClienteFuncao.ClienteFuncao = clienteFuncaoDao.findById((Int64)gheSetorClienteFuncao.ClienteFuncao.Id, dbConnection);

                if (gheSetorClienteFuncao.GheSetor != null)
                    gheSetorClienteFuncao.GheSetor = gheSetorDao.findById((Int64)gheSetorClienteFuncao.GheSetor.Id, dbConnection);

                if (gheSetorClienteFuncao.ComentarioFuncao != null)
                    gheSetorClienteFuncao.ComentarioFuncao = comentarioFuncaoDao.findById((Int64)gheSetorClienteFuncao.ComentarioFuncao.Id, dbConnection);

                return gheSetorClienteFuncao;

            }
            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findGheSetorClienteFuncaoById", ex);
                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }
            finally
            {
                FabricaConexao.fecharConexao();
            }
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public ComentarioFuncao findComentarioFuncaoById(Int64 id)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findComentarioFuncaoById");

            NpgsqlConnection dbConnection = FabricaConexao.getConexao();
            IComentarioFuncaoDao comentarioFuncaoDao = FabricaDao.getComentarioFuncaoDao();

            ComentarioFuncao comentarioFuncao = null;

            try
            {
                comentarioFuncao = comentarioFuncaoDao.findById(id, dbConnection);
                return comentarioFuncao;

            }
            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findComentarioFuncaoById", ex);
                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }
            finally
            {
                FabricaConexao.fecharConexao();
            }
        }


        [MethodImpl(MethodImplOptions.Synchronized)]
        public GheFonteAgente findGheFonteAgenteByGheFonteAndAgente(GheFonte gheFonte, Agente agente)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findGheFonteAgenteByGheFonteAndAgente");

            NpgsqlConnection dbConnection = FabricaConexao.getConexao();
            IGheFonteAgenteDao gheFonteAgenteDao = FabricaDao.getGheFonteAgenteDao();

            GheFonteAgente gheFonteAgente = null;

            try
            {
                gheFonteAgente = gheFonteAgenteDao.findByGheFonteAndAgente(gheFonte, agente, dbConnection);
                return gheFonteAgente;

            }
            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findGheFonteAgenteByGheFonteAndAgente", ex);
                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }
            finally
            {
                FabricaConexao.fecharConexao();
            }
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public LinkedList<GheSetor> findAllGheSetorByGheAndSetor(Ghe ghe, Setor setor)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findAllGheSetorByGheAndSetor");

            NpgsqlConnection dbConnection = FabricaConexao.getConexao();
            IGheSetorDao gheSetorDao = FabricaDao.getGheSetorDao();

            try
            {
                return gheSetorDao.findAllBySetorAndGhe(setor, ghe, dbConnection);

            }
            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findAllGheSetorByGheAndSetor", ex);
                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }
            finally
            {
                FabricaConexao.fecharConexao();
            }
        }

    }

}