﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using SWS.Entidade;
using SWS.Excecao;
using SWS.Helper;
using Npgsql;
using NpgsqlTypes;
using SWS.Dao;
using SWS.IDao;
using System.Runtime.CompilerServices;
using SWS.View.Resources;

namespace SWS.Facade
{
    class RelatorioFacade
    {
        public static RelatorioFacade relatorioFacade = null;

        private RelatorioFacade() { }

        public static RelatorioFacade getInstance()
        {
            if (relatorioFacade == null)
            {
                relatorioFacade = new RelatorioFacade();
            }

            return relatorioFacade;
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public DataSet findAllRelatoriosBySituacao(String tipo)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findAllRelatoriosBySituacao");

            NpgsqlConnection dbConnection = FabricaConexao.getConexao();

            IRelatorioDao relatorioDao = FabricaDao.getRelatorioDao();

            try
            {
                return relatorioDao.findAllRelatoriosBySituacao(tipo, dbConnection);

            }
            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findAllRelatoriosBySituacao", ex);
                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }
            finally
            {
                FabricaConexao.fecharConexao();
            }
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public String findNomeRelatorioById(Int64? idRelatorio)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findNomeRelatorioById");

            NpgsqlConnection dbConnection = FabricaConexao.getConexao();

            IRelatorioDao relatorioDao = FabricaDao.getRelatorioDao();

            try
            {
                return relatorioDao.findNomeRelatorioById(idRelatorio, dbConnection);

            }
            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findNomeRelatorioById", ex);
                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }
            finally
            {
                FabricaConexao.fecharConexao();
            }
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public Int64 findUltimaVersaoByTipo(String tipoRelatorio)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findUltimaVersaoByTipo");

            NpgsqlConnection dbConnection = FabricaConexao.getConexao();

            IRelatorioDao relatorioDao = FabricaDao.getRelatorioDao();

            try
            {
                return relatorioDao.findUltimaVersaoByTipo(tipoRelatorio, dbConnection);

            }
            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findUltimaVersaoByTipo", ex);
                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }
            finally
            {
                FabricaConexao.fecharConexao();
            }
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public Relatorio findRelatorioById(long id)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findRelatorioById");

            NpgsqlConnection dbConnection = FabricaConexao.getConexao();

            IRelatorioDao relatorioDao = FabricaDao.getRelatorioDao();

            try
            {
                return relatorioDao.findById(id, dbConnection);

            }
            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findRelatorioById", ex);
                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }
            finally
            {
                FabricaConexao.fecharConexao();
            }
        }


        [MethodImpl(MethodImplOptions.Synchronized)]
        public RelatorioExame insertRelatorioExame(RelatorioExame relatorioExame)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método insertRelatorioExame");

            NpgsqlConnection dbConnection = FabricaConexao.getConexao();

            IRelatorioExameDao relatorioExameDao = FabricaDao.getRelatorioExameDao();
            
            NpgsqlTransaction transaction = dbConnection.BeginTransaction();

            RelatorioExame relatorioExameNew = null;

            try
            {
                /* verificando se já existe esse relatório gravado com status "D" */
                if (relatorioExameDao.findAllByExame(relatorioExame.Exame, dbConnection).Exists(x => x.Relatorio.Id == relatorioExame.Relatorio.Id && string.Equals(x.Situacao, ApplicationConstants.DESATIVADO)))
                {
                    relatorioExameNew = relatorioExameDao.findAllByExame(relatorioExame.Exame, dbConnection).Find(x => x.IdRelatorioExame == relatorioExame.Relatorio.Id);
                    relatorioExameNew.Situacao = ApplicationConstants.ATIVO;
                    relatorioExameNew = relatorioExameDao.update(relatorioExameNew, dbConnection);
                }
                else 
                    relatorioExameNew = relatorioExameDao.insert(relatorioExame, dbConnection);
                
                transaction.Commit();

            }
            catch (DataBaseConectionException ex)
            {
                transaction.Rollback();
                LogHelper.logger.Error(this.GetType().Name + " - insertRelatorioExame", ex);
                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }
            finally
            {
                FabricaConexao.fecharConexao();
            }

            return relatorioExameNew;
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public RelatorioExame updateRelatorioExame(RelatorioExame relatorioExame)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método updateRelatorioExame");

            NpgsqlConnection dbConnection = FabricaConexao.getConexao();

            IRelatorioExameDao relatorioExameDao = FabricaDao.getRelatorioExameDao();

            NpgsqlTransaction transaction = dbConnection.BeginTransaction();

            try
            {
                relatorioExame = relatorioExameDao.update(relatorioExame, dbConnection);
                transaction.Commit();

            }
            catch (DataBaseConectionException ex)
            {
                transaction.Rollback();
                LogHelper.logger.Error(this.GetType().Name + " - updateRelatorioExame", ex);
                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }
            finally
            {
                FabricaConexao.fecharConexao();
            }

            return relatorioExame;
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public RelatorioExame findRelatorioExameById(long id)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findRelatorioExameById");

            NpgsqlConnection dbConnection = FabricaConexao.getConexao();

            IRelatorioExameDao relatorioExameDao = FabricaDao.getRelatorioExameDao();

            RelatorioExame relatorioExameFound = null;

            try
            {
                return relatorioExameFound = relatorioExameDao.findById(id, dbConnection);

            }
            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findRelatorioExameById", ex);
                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }
            finally
            {
                FabricaConexao.fecharConexao();
            }

        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public List<RelatorioExame> findAllRelatorioExameByExame(Exame exame)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findAllRelatorioExameByExame");

            NpgsqlConnection dbConnection = FabricaConexao.getConexao();

            IRelatorioExameDao relatorioExameDao = FabricaDao.getRelatorioExameDao();

            try
            {
                return relatorioExameDao.findAllByExame(exame, dbConnection);

            }
            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findAllRelatorioExameByExame", ex);
                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }
            finally
            {
                FabricaConexao.fecharConexao();
            }

        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public void deleteRelatorioExame(RelatorioExame relatorioExame)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método deleteRelatorioExame");

            NpgsqlConnection dbConnection = FabricaConexao.getConexao();

            IRelatorioExameDao relatorioExameDao = FabricaDao.getRelatorioExameDao();

            NpgsqlTransaction transaction = dbConnection.BeginTransaction();

            try
            {
                relatorioExameDao.delete(relatorioExame, dbConnection);
                transaction.Commit();

            }
            catch (DataBaseConectionException ex)
            {
                transaction.Rollback();
                LogHelper.logger.Error(this.GetType().Name + " - deleteRelatorioExame", ex);
                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }
            finally
            {
                FabricaConexao.fecharConexao();
            }

        }


    }
}
