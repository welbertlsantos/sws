﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.CompilerServices;
using SWS.Entidade;
using SWS.Helper;
using Npgsql;
using SWS.Dao;
using SWS.IDao;
using SWS.Excecao;
using System.Data;

namespace SWS.Facade
{
    class LogFacade
    {
        public static LogFacade logFacade = null;

        private LogFacade() { }

        public static LogFacade getInstance()
        {
            if (logFacade == null)
            {
                logFacade = new LogFacade();
            }

            return logFacade;
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public CorrecaoEstudo insertCorrecaoEstudo(CorrecaoEstudo correcaoEstudo)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método insertCorrecaoEstudo");

            NpgsqlConnection dbConnection = FabricaConexao.getConexao(); 

            ICorrecaoEstudoDao correcaoEstudoDao = FabricaDao.getCorrecaoEstudoDao(); 
            NpgsqlTransaction transaction = dbConnection.BeginTransaction();
            
            try
            {
                correcaoEstudo = correcaoEstudoDao.insertCorrecaoEstudo(correcaoEstudo, dbConnection);
                transaction.Commit();
            }
            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - insertCorrecaoEstudo", ex);
                transaction.Rollback();
                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }
            finally
            {
                FabricaConexao.fecharConexao();
            }

            return correcaoEstudo;
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public DataSet findAllCorrecaoEstudoByEstudo(Estudo estudo)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findAllCorrecaoEstudoByEstudo");

            NpgsqlConnection dbConnection = FabricaConexao.getConexao();
            ICorrecaoEstudoDao correcaoEstudoDao = FabricaDao.getCorrecaoEstudoDao(); 

            try
            {
                return correcaoEstudoDao.findAllCorrecaoByEstudo(estudo, dbConnection);

            }
            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findAllCorrecaoEstudoByEstudo", ex);
                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }
            finally
            {
                FabricaConexao.fecharConexao();
            }
        }

    }
}
