﻿using SWS.IDao;
using SWS.Entidade;
using Npgsql;
using SWS.Excecao;
using System;
using System.Data;
using SWS.Dao;
using System.Collections.Generic;
using SWS.Helper;
using System.Runtime.CompilerServices;

namespace SWS.Facade
{
    class UsuarioFacade
    {
        public static UsuarioFacade usuarioFacade = null;

        private UsuarioFacade() { }

        public static UsuarioFacade getInstance()
        {
            if (usuarioFacade == null)
            {
                usuarioFacade = new UsuarioFacade();
            }

            return usuarioFacade;
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public Usuario novoUsuario(Usuario usuario)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método novoUsuario");

            Usuario usuarioNovo = null;
            NpgsqlConnection dbConnection = FabricaConexao.getConexao();
            IUsuarioDao usuarioDao = FabricaDao.getUsuarioDao();
            IUsuarioPerfilDao usuarioPerfilDao = FabricaDao.getUsuarioPerfilDao();
            IFuncaoInternaUsuarioDao funcaoInternaUsuarioDado = FabricaDao.getFuncaoInternaUsuarioDao();
            IUsuarioClienteDao usuarioClienteDao = FabricaDao.getUsuarioClienteDao();
            
            NpgsqlTransaction transaction = dbConnection.BeginTransaction();

            try
            {
                /*Verificando o cpf do usuário */
                List<Usuario> usuarios = usuarioDao.findByCpf(usuario.Cpf, dbConnection);
                
                if (!usuario.Externo)
                    if (usuarios.Find(x => x.Cpf == usuario.Cpf && x.Empresa.Id == usuario.Empresa.Id ) != null)
                        throw new Exception("Não é possível cadastrar o usuário. CPF já existente");

                if (usuarioDao.findUsuarioByLogin(usuario.Login, dbConnection) != null)
                    throw new UsuarioJaCadastradoException(UsuarioJaCadastradoException.msg2);
                                
                //Incluindo Usuário
                usuarioNovo = usuarioDao.insert(usuario, dbConnection);

                //Incluindo Perfis
                foreach (Perfil perfil in usuario.Perfil)
                    usuarioPerfilDao.incluir(new UsuarioPerfil(usuarioNovo, perfil, DateTime.Now), dbConnection);

                /*Incluindo funcao Interna no usuario*/
                if (!usuarioNovo.Externo)
                    funcaoInternaUsuarioDado.insert(usuarioNovo, dbConnection);

                /* incluindo restrinção de clientes para o usuario */
                usuario.UsuarioCliente.ForEach(delegate(UsuarioCliente usuarioCliente)
                {
                    usuarioCliente.Usuario = usuarioNovo;
                    usuarioCliente = usuarioClienteDao.insert(usuarioCliente, dbConnection);
                });


                transaction.Commit();
            }
            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - novoUsuario", ex);
                transaction.Rollback();
                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }
            finally
            {
                FabricaConexao.fecharConexao();
            }

            return usuarioNovo;
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public Int64 recuperaProximoId()
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método recuperaProximoId");

            Int64 proximoId;
            NpgsqlConnection dbConnection = FabricaConexao.getConexao();
            IUsuarioDao usuarioDao = FabricaDao.getUsuarioDao();

            try
            {
                proximoId = usuarioDao.recuperaProximoId(dbConnection);
            }
            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - recuperaProximoId", ex);
                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }
            finally
            {
                FabricaConexao.fecharConexao();
            }

            return proximoId;
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public DataSet listaUsuarios()
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método listaUsuarios");

            NpgsqlConnection dbConnection = FabricaConexao.getConexao();
            IUsuarioDao usuarioDao = FabricaDao.getUsuarioDao();

            try
            {
                return usuarioDao.listaTodos(dbConnection);
            }
            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - listaUsuarios", ex);
                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }
            finally
            {
                FabricaConexao.fecharConexao();
            }
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public DataSet listaUsuariosAtivos()
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método listaUsuariosAtivos");

            NpgsqlConnection dbConnection = FabricaConexao.getConexao();
            IUsuarioDao usuarioDao = FabricaDao.getUsuarioDao();

            try
            {
                return usuarioDao.listaTodosAtivos(dbConnection);
            }
            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - listaUsuariosAtivos", ex);
                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }
            finally
            {
                FabricaConexao.fecharConexao();
            }
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public DataSet findUsuarioByFilter(Usuario usuario)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findUsuarioByFilter");

            NpgsqlConnection dbConnection = FabricaConexao.getConexao();
            IUsuarioDao usuarioDao = FabricaDao.getUsuarioDao();

            try
            {
                return usuarioDao.findByFilter(usuario, dbConnection);
            }
            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findUsuarioByFilter", ex);
                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }
            finally
            {
                FabricaConexao.fecharConexao();
            }
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public Usuario findUsuarioById(Int64 idUsuario)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findUsuarioById");

            NpgsqlConnection dbConnection = FabricaConexao.getConexao();
            IUsuarioDao usuarioDao = FabricaDao.getUsuarioDao();
            IFuncaoInternaDao funcaoInternaDao = FabricaDao.getFuncaoInternaDao();
            IEmpresaDao empresaDao = FabricaDao.getEmpresaDao();
            IMedicoDao medicoDao = FabricaDao.getMedicoDao();
            IUsuarioClienteDao usuarioClienteDao = FabricaDao.getUsuarioClienteDao();
            IClienteDao clienteDao = FabricaDao.getClienteDao();

            try
            {
                Usuario usuario = usuarioDao.findById(idUsuario, dbConnection);

                /* populando demais informações do usuário */
                if (usuario.Empresa != null)
                    usuario.Empresa = empresaDao.findById((Int64)usuario.Empresa.Id, dbConnection);

                if (usuario.Medico != null)
                    usuario.Medico = medicoDao.findById((Int64)usuario.Medico.Id, dbConnection);

                if (usuario.FuncaoInterna != null)
                    usuario.FuncaoInterna = funcaoInternaDao.findFuncaoInternaAtivoByUsuario(usuario, dbConnection);

                /* localizando os clientes relacionados ao usuario */

                usuario.UsuarioCliente = usuarioClienteDao.findAllByUsuario(usuario, dbConnection);
                usuario.UsuarioCliente.ForEach(x => x.Cliente = clienteDao.findById((long)x.Cliente.Id, dbConnection));

                return usuario;
            }
            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findUsuarioById", ex);
                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }
            finally
            {
                FabricaConexao.fecharConexao();
            }
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public Usuario alterarUsuario(Usuario usuario, Boolean chaveFuncaoInterna)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método alterarUsuario");

            Usuario usuarioAlterado = null;
            NpgsqlConnection dbConnection = FabricaConexao.getConexao();
            
            IUsuarioDao usuarioDao = FabricaDao.getUsuarioDao();
            IUsuarioPerfilDao usuarioPerfilDao = FabricaDao.getUsuarioPerfilDao();
            IFuncaoInternaUsuarioDao funcaoInternaUsuarioDao = FabricaDao.getFuncaoInternaUsuarioDao();
            
            NpgsqlTransaction transaction = dbConnection.BeginTransaction();

            try
            {
                /* verificando se o cpf já existe no cadastro */
                List<Usuario> usuarios = usuarioDao.findByCpf(usuario.Cpf, dbConnection);

                if (!usuario.Externo)
                    if (usuarios.Find(x => (x.Cpf == usuario.Cpf && x.Id != usuario.Id) && x.Empresa.Id == usuario.Empresa.Id) != null)
                        throw new Exception ("Não é possível alterar o Usuário. CPF já existente nessa empresa.");

                usuarioAlterado = usuarioDao.update(usuario, dbConnection);

                transaction.Commit();
            }
            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - alterarUsuario", ex);
                transaction.Rollback();
                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }
            finally
            {
                FabricaConexao.fecharConexao();
            }

            return usuarioAlterado;
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public void alterarSenhaUsuario(Usuario usuario)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método alterarSenhaUsuario");

            NpgsqlConnection dbConnection = FabricaConexao.getConexao();
            IUsuarioDao usuarioDao = FabricaDao.getUsuarioDao();
            NpgsqlTransaction transaction = dbConnection.BeginTransaction();
            try
            {
                //Alterando Usuário
                usuarioDao.updateSenha(usuario, dbConnection);
                transaction.Commit();
            }
            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - alterarSenhaUsuario", ex);
                transaction.Rollback();
                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }
            finally
            {
                FabricaConexao.fecharConexao();
            }

        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public DataSet listaTodosDominios()
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método listaTodosDominios");

            NpgsqlConnection dbConnection = FabricaConexao.getConexao();
            IDominioDao dominioDao = FabricaDao.getDominioDao();

            try
            {
                return dominioDao.listaTodos(dbConnection);
            }
            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - listaTodosDominios", ex);
                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }
            finally
            {
                FabricaConexao.fecharConexao();
            }
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public DataSet findUsuarioFuncaoAtivoByFilter(Usuario usuario, Int64 idFuncaoProcurada)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findUsuarioFuncaoAtivoByFilter");

            NpgsqlConnection dbConnection = FabricaConexao.getConexao();
            IUsuarioDao usuarioDao = FabricaDao.getUsuarioDao();

            try
            {
                return usuarioDao.findUsuarioFuncaoAtivoByFilter(usuario, idFuncaoProcurada, dbConnection);

            }
            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findUsuarioTecnicoAtivoByFilter", ex);
                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }
            finally
            {
                FabricaConexao.fecharConexao();
            }
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public Usuario findUsuarioPpraById(Int64 id)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findUsuarioPpraById");

            NpgsqlConnection dbConnection = FabricaConexao.getConexao();
            IUsuarioDao usuarioDao = FabricaDao.getUsuarioDao();
            IMedicoDao medicoDao = FabricaDao.getMedicoDao();
            IEmpresaDao empresaDao = FabricaDao.getEmpresaDao();

            try
            {
                Usuario usuario = usuarioDao.findById(id, dbConnection);

                /* preenchendo informações do usuário */
                
                
                if (usuario.Medico != null)   
                    usuario.Medico= medicoDao.findById((Int64)usuario.Medico.Id, dbConnection);

                if (usuario.Empresa != null)
                    usuario.Empresa = empresaDao.findById((Int64)usuario.Empresa.Id, dbConnection);

                return usuario;
            }
            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findUsuarioPpraById", ex);
                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }
            finally
            {
                FabricaConexao.fecharConexao();
            }
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public DataSet findUsuarioElaboraDocumento(Usuario usuario)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findUsuarioElaboraDocumento");

            NpgsqlConnection dbConnection = FabricaConexao.getConexao();
            
            IUsuarioDao usuarioDao = FabricaDao.getUsuarioDao();

            try
            {
                return usuarioDao.findUsuarioElaboraDocumento(usuario,  dbConnection);

            }
            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findUsuarioElaboraDocumento", ex);
                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }
            finally
            {
                FabricaConexao.fecharConexao();
            }
        }

        #region perfil

        [MethodImpl(MethodImplOptions.Synchronized)]
        public Perfil findPerfilById(Int64 idPerfil)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findPerfilById");

            NpgsqlConnection dbConnection = FabricaConexao.getConexao();
            IPerfilDao perfilDao = FabricaDao.getPerfilDao();
            try
            {
                Perfil perfil = perfilDao.findById(idPerfil, dbConnection);
                return perfil;

            }
            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findPerfilById", ex);
                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }
            finally
            {
                FabricaConexao.fecharConexao();
            }
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public void desativaPerfil(Int64 idPerfil, String situacao)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método desativaPerfil");

            NpgsqlConnection dbConnection = FabricaConexao.getConexao();
            IPerfilDao perfilDao = FabricaDao.getPerfilDao();
            NpgsqlTransaction transaction = dbConnection.BeginTransaction();
            try
            {
                perfilDao.changeStatusPerfil(idPerfil, situacao, dbConnection);

                transaction.Commit();
            }
            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - alterarStatusPerfil", ex);
                transaction.Rollback();
                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }
            finally
            {
                FabricaConexao.fecharConexao();
            }

        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public DataSet FindPerfilByFilter(Perfil perfil)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método FindPerfilByFilter");

            NpgsqlConnection dbConnection = FabricaConexao.getConexao();
            IPerfilDao perfilDao = FabricaDao.getPerfilDao();

            try
            {
                return perfilDao.findByFilter(perfil, dbConnection);

            }
            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findPerfilByFilter", ex);
                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }
            finally
            {
                FabricaConexao.fecharConexao();
            }
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public Perfil incluirPerfil(Perfil perfil)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método incluirPerfil");

            Perfil perfilNovo = null;
            NpgsqlConnection dbConnection = FabricaConexao.getConexao();
            IPerfilDao perfilDao = FabricaDao.getPerfilDao();
            NpgsqlTransaction transaction = dbConnection.BeginTransaction();
            try
            {
                /* verificando se existe um perfil com o mesmo nome */

                if (perfilDao.findByDescricao(perfil.Descricao, dbConnection) != null)
                    throw new Exception("Já existe um perfil cadastrado com esse nome.");
                
                perfilNovo = perfilDao.insert(perfil, dbConnection);
                transaction.Commit();
            }
            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - IncluirPerfil", ex);
                transaction.Rollback();
                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }
            finally
            {
                FabricaConexao.fecharConexao();
            }

            return perfil;
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public Perfil findPerfilByDescricao(String descricao)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findPerfilByDescricao");

            NpgsqlConnection dbConnection = FabricaConexao.getConexao();
            IPerfilDao perfilDao = FabricaDao.getPerfilDao();

            try
            {
                Perfil perfil = perfilDao.findByDescricao(descricao, dbConnection);

                return perfil;
            }
            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findPerfilByDescricao", ex);
                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }
            finally
            {
                FabricaConexao.fecharConexao();
            }
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public Perfil updatePerfil(Perfil perfilAlterar)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método alteraPerfil");

            Perfil perfilNovo = null;
            NpgsqlConnection dbConnection = FabricaConexao.getConexao();
            IPerfilDao perfilDao = FabricaDao.getPerfilDao();
            NpgsqlTransaction transaction = dbConnection.BeginTransaction();

            try
            {
                /* verificando se não existe outro perfil com esse mesmo nome */

                Perfil perfilProcurado = perfilDao.findByDescricao(perfilAlterar.Descricao, dbConnection);

                if (perfilProcurado != null && perfilProcurado.Id != perfilAlterar.Id)
                    throw new Exception("Já existe um perfil cadastrado com esse nome.");

                perfilNovo = perfilDao.update(perfilAlterar, dbConnection);
                transaction.Commit();
            }
            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - alterarPerfil", ex);
                transaction.Rollback();
                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }
            finally
            {
                FabricaConexao.fecharConexao();
            }

            return perfilNovo;
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public Usuario findUsuarioByLogin(String login)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findUsuarioByLogin");

            NpgsqlConnection dbConnection = FabricaConexao.getConexao();
            IUsuarioDao usuarioDao = FabricaDao.getUsuarioDao();
            IMedicoDao medicoDao = FabricaDao.getMedicoDao();
            IEmpresaDao empresaDao = FabricaDao.getEmpresaDao();
            try
            {
                Usuario usuario = usuarioDao.findUsuarioByLogin(login, dbConnection);

                if (usuario.Medico != null)
                    usuario.Medico = medicoDao.findById((Int64)usuario.Medico.Id, dbConnection);

                if (usuario.Empresa != null)
                    usuario.Empresa = empresaDao.findById((Int64)usuario.Empresa.Id, dbConnection);

                return usuario;
            
            }
            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findUsuarioByLogin", ex);
                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }
            finally
            {
                FabricaConexao.fecharConexao();
            }
        
        
        }
        
        #endregion

        #region funcaoIntena

        [MethodImpl(MethodImplOptions.Synchronized)]
        public DataSet FindFuncaoInternaByFilter(FuncaoInterna funcaoInterna)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método FindFuncaoInternaByFilter");

            NpgsqlConnection dbConnection = FabricaConexao.getConexao();
            IFuncaoInternaDao funcaoInternaDao = FabricaDao.getFuncaoInternaDao();

            try
            {
                return funcaoInternaDao.findByFilter(funcaoInterna, dbConnection);

            }
            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findFuncaoInternaByFilter", ex);
                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }
            finally
            {
                FabricaConexao.fecharConexao();
            }
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public FuncaoInterna incluirFuncaoInterna(FuncaoInterna funcaoInterna)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método incluirFuncaoInterna");

            FuncaoInterna funcaoInternaNovo = null;
            NpgsqlConnection dbConnection = FabricaConexao.getConexao();
            IFuncaoInternaDao funcaoInternaDao = FabricaDao.getFuncaoInternaDao();
            NpgsqlTransaction transaction = dbConnection.BeginTransaction();
            try
            {
                /* verificando se não existe uma função com essa descrição*/

                if (funcaoInternaDao.findByDescricao(funcaoInterna.Descricao, dbConnection) != null)
                    throw new Exception("Já existe uma função cadastrada com essa descrição.");

                funcaoInternaNovo = funcaoInternaDao.insert(funcaoInterna, dbConnection);
                transaction.Commit();
            }
            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - IncluirFuncaoInterna", ex);
                transaction.Rollback();
                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }
            finally
            {
                FabricaConexao.fecharConexao();
            }

            return funcaoInterna;
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public FuncaoInterna findFuncaoInternaByDescricao(String descricao)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findFuncaoInternaByDescricao");

            NpgsqlConnection dbConnection = FabricaConexao.getConexao();
            IFuncaoInternaDao funcaoInternaDao = FabricaDao.getFuncaoInternaDao();

            try
            {
                FuncaoInterna funcaoInterna = funcaoInternaDao.findByDescricao(descricao, dbConnection);

                return funcaoInterna;
            }
            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findFuncaoInternaByDescricao", ex);
                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }
            finally
            {
                FabricaConexao.fecharConexao();
            }
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public FuncaoInterna findFuncaoInternaById(Int64 idFuncaoInterna)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findFuncaoInternaById");

            NpgsqlConnection dbConnection = FabricaConexao.getConexao();
            IFuncaoInternaDao funcaoInternaDao = FabricaDao.getFuncaoInternaDao();
            try
            {
                FuncaoInterna funcaoInterna = funcaoInternaDao.findById(idFuncaoInterna, dbConnection);
                return funcaoInterna;

            }
            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findFuncaoInternaById", ex);
                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }
            finally
            {
                FabricaConexao.fecharConexao();
            }
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public FuncaoInterna alteraFuncaoInterna(FuncaoInterna funcaoInternaAlterar)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método alteraFuncaoInterna");

            NpgsqlConnection dbConnection = FabricaConexao.getConexao();
            IFuncaoInternaDao funcaoInternaDao = FabricaDao.getFuncaoInternaDao();
            NpgsqlTransaction transaction = dbConnection.BeginTransaction();
            try
            {
                /* verificando se não existe uma função cadastrada com essa descrição */

                FuncaoInterna funcaoInterna = funcaoInternaDao.findByDescricao(funcaoInternaAlterar.Descricao, dbConnection) ;

                if (funcaoInterna != null && funcaoInternaAlterar.Id != funcaoInterna.Id)
                    throw new Exception("Já existe uma função interna cadastrada com essa descrição.");

                funcaoInternaAlterar = funcaoInternaDao.update(funcaoInternaAlterar, dbConnection);
                
                transaction.Commit();
            }
            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - alterarFuncaoInterna", ex);
                transaction.Rollback();
                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }
            finally
            {
                FabricaConexao.fecharConexao();
            }

            return funcaoInternaAlterar;
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public void desativaFuncaoInternaUsuario(Usuario usuario)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método desativaFuncaoInternaUsuario");

            NpgsqlConnection dbConnection = FabricaConexao.getConexao();
            IFuncaoInternaUsuarioDao funcaoInternaUsuarioDao = FabricaDao.getFuncaoInternaUsuarioDao();

            try
            {
                funcaoInternaUsuarioDao.changeStatus (usuario, dbConnection);

            }
            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - desativaFuncaoInternaUsuario", ex);
                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }
            finally
            {
                FabricaConexao.fecharConexao();
            }
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public LinkedList<Usuario> findUsuarioByFuncaoInterna(Int64 idFuncaoInterna, Int64? idFuncaoInternaFinal)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findUsuarioByFuncaoInterna");

            NpgsqlConnection dbConnection = FabricaConexao.getConexao();
            
            IUsuarioDao usuarioDao = FabricaDao.getUsuarioDao();
            IEmpresaDao empresaDao = FabricaDao.getEmpresaDao();

            LinkedList<Usuario> usuarios = new LinkedList<Usuario>();

            try
            {
                usuarios = usuarioDao.findUsuarioByFuncaoInterna(idFuncaoInterna, idFuncaoInternaFinal, dbConnection);
               
                foreach (Usuario usuario in usuarios)
                    usuario.Empresa = empresaDao.findById((long)usuario.Empresa.Id, dbConnection);

                
            }
            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findUsuarioByFuncaoInterna", ex);
                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }
            finally
            {
                FabricaConexao.fecharConexao();
            }

            return usuarios;
        }

        #endregion

        [MethodImpl(MethodImplOptions.Synchronized)]
        public UsuarioCliente insertUsuarioCliente(UsuarioCliente usuarioCliente)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método insertUsuarioCliente");

            NpgsqlConnection dbConnection = FabricaConexao.getConexao();

            IUsuarioClienteDao usuarioClienteDao = FabricaDao.getUsuarioClienteDao();

            try
            {
                usuarioCliente = usuarioClienteDao.insert(usuarioCliente, dbConnection);

            }
            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - insertUsuarioCliente", ex);
                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }
            finally
            {
                FabricaConexao.fecharConexao();
            }

            return usuarioCliente;
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public UsuarioCliente updateUsuarioCliente(UsuarioCliente usuarioCliente)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método updateUsuarioCliente");

            NpgsqlConnection dbConnection = FabricaConexao.getConexao();

            IUsuarioClienteDao usuarioClienteDao = FabricaDao.getUsuarioClienteDao();

            try
            {
                usuarioCliente = usuarioClienteDao.update(usuarioCliente, dbConnection);

            }
            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - updateUsuarioCliente", ex);
                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }
            finally
            {
                FabricaConexao.fecharConexao();
            }

            return usuarioCliente;
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public List<UsuarioCliente> findAllUsuarioClienteByUsuario(Usuario usuario)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findAllUsuarioClienteByUsuario");

            NpgsqlConnection dbConnection = FabricaConexao.getConexao();

            IUsuarioClienteDao usuarioClienteDao = FabricaDao.getUsuarioClienteDao();
            IClienteDao clienteDao = FabricaDao.getClienteDao();

            List<UsuarioCliente> clientes;

            try
            {
                clientes = usuarioClienteDao.findAllByUsuario(usuario, dbConnection);
                clientes.ForEach(x => x.Cliente = clienteDao.findById((long)x.Cliente.Id, dbConnection));
            }
            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findAllUsuarioClienteByUsuario", ex);
                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }
            finally
            {
                FabricaConexao.fecharConexao();
            }

            return clientes;
        }


        [MethodImpl(MethodImplOptions.Synchronized)]
        public void deleteUsuarioCliente(UsuarioCliente usuarioCliente)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método deleteUsuarioCliente");

            NpgsqlConnection dbConnection = FabricaConexao.getConexao();

            IUsuarioClienteDao usuarioClienteDao = FabricaDao.getUsuarioClienteDao();

            try
            {
                usuarioClienteDao.delete(usuarioCliente, dbConnection);

            }
            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - deleteUsuarioCliente", ex);
                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }
            finally
            {
                FabricaConexao.fecharConexao();
            }

        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public UsuarioCliente findUsuarioClienteById(long id)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findUsuarioClienteById");

            NpgsqlConnection dbConnection = FabricaConexao.getConexao();

            IUsuarioClienteDao usuarioClienteDao = FabricaDao.getUsuarioClienteDao();

            UsuarioCliente usuarioCliente;

            try
            {
                usuarioCliente = usuarioClienteDao.findById(id, dbConnection);

            }
            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findUsuarioClienteById", ex);
                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }
            finally
            {
                FabricaConexao.fecharConexao();
            }

            return usuarioCliente;

        }
    }
}
