﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using SWS.Entidade;
using Npgsql;
using NpgsqlTypes;
using SWS.Dao;
using SWS.IDao;
using SWS.Excecao;
using SWS.Helper;
using SWS.View.Resources;
using SWS.View.ViewHelper;
using System.Runtime.CompilerServices;
using SWS.ViewHelper;
using SWS.Resources;
using System.IO;
using System.Xml.Serialization;
using System.Xml;
using SWS.Integration.Model;

namespace SWS.Facade
{
    class AsoFacade
    {
        public static AsoFacade asoFacade = null;

        private AsoFacade() { }

        public static AsoFacade getInstance()
        {
            if (asoFacade == null)
            {
                asoFacade = new AsoFacade();
            }

            return asoFacade;
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public DataSet findAsoByFilter(Aso aso, ClienteFuncao clienteFuncao, DateTime? dataAsoFinal, DateTime? dataCriacaoFinal)
        {

            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findAsoByFilter");

            NpgsqlConnection dbConnection = FabricaConexao.getConexao();

            IAsoDao asoDao = FabricaDao.getAsoDao();

            try
            {
                return asoDao.findAsoByFilter(aso, clienteFuncao, dataAsoFinal, dataCriacaoFinal, PermissionamentoFacade.usuarioAutenticado, dbConnection);

            }
            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findAsoByFilter", ex);
                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }
            finally
            {
                FabricaConexao.fecharConexao();
            }
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public DataSet findAsoByFilter(Cliente cliente, DateTime dataPeriodoInicial, DateTime dataPeriodoFinal, Boolean check)
        {

            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findAsoByFilter");

            NpgsqlConnection dbConnection = FabricaConexao.getConexao();

            IAsoDao asoDao = FabricaDao.getAsoDao();

            try
            {
                return asoDao.findAsoByFilter(cliente, dataPeriodoInicial, dataPeriodoFinal, check, dbConnection);

            }
            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findAsoByFilter", ex);
                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }
            finally
            {
                FabricaConexao.fecharConexao();
            }
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public HashSet<GheFonteAgenteExame> findAllExamesByPcmsoByASO(Estudo estudo, Int32 idade, Periodicidade periodicidade, 
            List<Ghe> colecaoGhe, Boolean? altura, Boolean? confinado, bool? eletricidade)
        {

            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findAllExamesByPcmsoByASO");

            NpgsqlConnection dbConnection = FabricaConexao.getConexao();

            IAsoDao asoDao = FabricaDao.getAsoDao();

            try
            {
                return asoDao.findAllExamesByPcmsoByASOByIdadeByTipoByAlturaByConfinado(estudo, idade, periodicidade, colecaoGhe, altura, confinado, eletricidade, dbConnection);

            }
            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findAllExamesByPcmsoByASO", ex);
                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }
            finally
            {
                FabricaConexao.fecharConexao();
            }
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public Aso incluirAso(Aso aso, List<GheFonteAgenteExameAso> examePCMSO, List<ClienteFuncaoExameASo> exameExtra, 
            List<GheFonteAgenteExameAso> exameRepetido, List<GheSetor> gheSetorInPcmso, 
            HashSet<Agente> riscoInPcmso)
        {
            
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método incluirAso");

            Aso asoNovo = null;
            
            NpgsqlConnection dbConnection = FabricaConexao.getConexao();

            IAsoDao asoDao = FabricaDao.getAsoDao();
            IGheFonteAgenteExameAsoDao gheFonteAgenteExameAsoDao = FabricaDao.getGheFonteAgenteExameAsoDao();
            IClienteFuncaoExameASoDao clienteFuncaoExameAsoDao = FabricaDao.getClienteFuncaoExameASoDao();
            IPeriodicidadeDao periodicidadeDao = FabricaDao.getPeriodicidadeDao();
            IClienteFuncaoFuncionarioDao clienteFuncaoFuncionarioDao = FabricaDao.getClienteFuncaoFuncionarioDao();
            IProntuarioDao prontuarioDao = FabricaDao.getProntuarioDao();
            IAsoGheSetorDao asoGheSetorDao = FabricaDao.getAsoGheSetorDao();
            IAsoAgenteRiscoDao asoAgenteRiscoDao = FabricaDao.getAsoAgenteRisco();
            IMedicoExameDao medicoExameDao = FabricaDao.getMedicoExameDao();


            NpgsqlTransaction transaction = dbConnection.BeginTransaction();

            HashSet<Int64> ColecaoIdExamesInseridos = new HashSet<Int64>();
            
            try
            {
                asoNovo = asoDao.insert(aso, dbConnection);

                /* gravando dados no relacionamento com o pcmso caso exista. */

                examePCMSO.ForEach(x => x.Aso = asoNovo);
                   
                foreach (GheFonteAgenteExameAso gheFonteAgenteExameAso in examePCMSO)
                {
                    if (gheFonteAgenteExameAso.Transcrito)
                        gheFonteAgenteExameAso.Prestador = null;

                    /* verificando se tem médico já cadastrado na tabela seg_medico_exame para preencher o ghefonteagenteexameaso */

                    List<MedicoExame> medicos = medicoExameDao.findAllByExame(gheFonteAgenteExameAso.GheFonteAgenteExame.Exame, dbConnection);
                    if (medicos.Count == 1)
                    {
                        MedicoExame medicoExame = medicos.ElementAt(0);
                        gheFonteAgenteExameAso.Medico = medicoExame.Medico;
                    }

                    gheFonteAgenteExameAsoDao.insert(gheFonteAgenteExameAso, dbConnection);
                    ColecaoIdExamesInseridos.Add((Int64)gheFonteAgenteExameAso.GheFonteAgenteExame.Exame.Id);
                }

                /*gravando dados no relacionamento para exames repetidos. */

                exameRepetido.ForEach(x => x.Aso = asoNovo);

                foreach (GheFonteAgenteExameAso gheFonteAgenteExameAso in exameRepetido)
                {
                    List<MedicoExame> medicos = medicoExameDao.findAllByExame(gheFonteAgenteExameAso.GheFonteAgenteExame.Exame, dbConnection);
                    if (medicos.Count == 1)
                    {
                        MedicoExame medicoExame = medicos.ElementAt(0);
                        gheFonteAgenteExameAso.Medico = medicoExame.Medico;
                    }

                    gheFonteAgenteExameAsoDao.insert(gheFonteAgenteExameAso, dbConnection);
                }

                /*gravando dados do relacionamento com o exame Extra */
                exameExtra.ForEach(x => x.Aso = asoNovo);
                
                foreach (ClienteFuncaoExameASo clienteFuncaoExameAso in exameExtra)
                {

                    if (!ColecaoIdExamesInseridos.Contains((Int64)clienteFuncaoExameAso.ClienteFuncaoExame.Exame.Id))
                    {
                        /* não permitindo que exames transcritos sejam encaminhados para o prestador */
                        if (clienteFuncaoExameAso.Transcrito)
                            clienteFuncaoExameAso.Prestador = null;

                        List<MedicoExame> medicos = medicoExameDao.findAllByExame(clienteFuncaoExameAso.ClienteFuncaoExame.Exame, dbConnection);
                        if (medicos.Count == 1)
                        {
                            MedicoExame medicoExame = medicos.ElementAt(0);
                            clienteFuncaoExameAso.Medico = medicoExame.Medico;
                        }

                        clienteFuncaoExameAsoDao.insert(clienteFuncaoExameAso, dbConnection);
                    }
                }

                /* iniciando gravacao do aso ghe setor avulso */

                if (aso.GheSetorAvulso.Count > 0)
                    asoDao.updateGheSetorAvulso(asoNovo, aso.GheSetorAvulso, dbConnection);
                

                /* gravando aso ghe setor no atendimento */
              
                foreach (GheSetor gheSetor in gheSetorInPcmso)
                {
                    AsoGheSetor asoGheSetor = new AsoGheSetor((Int64)asoNovo.Id, (Int64)gheSetor.Ghe.Id,
                            (String)gheSetor.Ghe.Descricao, (Int64)gheSetor.Setor.Id,
                            (String)gheSetor.Setor.Descricao, gheSetor.Ghe.NumeroPo);

                    asoGheSetorDao.insertAsoGheSetor(asoGheSetor, dbConnection);
                }
             
                /* iniciando gravacao na tabela seg_aso_agente_risco */

                if (asoNovo.AsoAgenteRisco.Count > 0)
                {
                    asoNovo.AsoAgenteRisco.ForEach(delegate(AsoAgenteRisco asoAgenteRisco)
                    {
                        asoAgenteRiscoDao.insert(new AsoAgenteRisco((long?)asoNovo.Id, (long?)asoAgenteRisco.IdAgente, asoAgenteRisco.DescricaoAgente, (long?)asoAgenteRisco.IdRisco, asoAgenteRisco.DescricaoRisco), dbConnection);
                    });
                }
                else
                {
                    foreach (Agente agente in riscoInPcmso)
                    {
                        AsoAgenteRisco asoAgenteRisco = new AsoAgenteRisco((Int64)asoNovo.Id,
                            (Int64)agente.Id, (String)agente.Descricao, (Int64)agente.Risco.Id,
                            (String)agente.Risco.Descricao);

                        asoAgenteRiscoDao.insert(asoAgenteRisco, dbConnection);
                    }
                }

                string diretorioPadraoExportacaoExamesLaboratorio;

                PermissionamentoFacade.mapaConfiguracoes.TryGetValue(ConfigurationConstans.DIRETORIO_PADRAO_EXPORTACAO_EXAMES_LABORATORIO, out diretorioPadraoExportacaoExamesLaboratorio);

                if (!string.IsNullOrEmpty(diretorioPadraoExportacaoExamesLaboratorio))
                {
                    generateExamesXML(aso, examePCMSO, exameExtra, diretorioPadraoExportacaoExamesLaboratorio, dbConnection);
                }
                
                transaction.Commit();
            }
            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - incluirAso", ex);
                transaction.Rollback();
                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }
            catch (Exception exception)
            {
                LogHelper.logger.Error(this.GetType().Name + " - incluirAso", exception);
                transaction.Rollback();
                throw new DataBaseConectionException(DataBaseConectionException.msg + exception.Message);
            }
            finally
            {
                FabricaConexao.fecharConexao();
            }

            return asoNovo;

        }

        private void generateExamesXML(Aso aso, List<GheFonteAgenteExameAso> examePCMSO, List<ClienteFuncaoExameASo> exameExtra, string localGravacaoArquivo, NpgsqlConnection dbConnection)
        {

            HashSet<Exame> exames = new HashSet<Exame>();
            IExameDao exameDAO = FabricaDao.getExameDao();
            Exame exameEncontrado = null;

            foreach (GheFonteAgenteExameAso gheFonteAgenteExameAso in examePCMSO)
            {
                if (!gheFonteAgenteExameAso.Transcrito)
                {
                    exameEncontrado = exameDAO.findById((Int64)gheFonteAgenteExameAso.GheFonteAgenteExame.Exame.Id, dbConnection);
                    if (exameEncontrado != null && exameEncontrado.Laboratorio.Value)
                    {
                        exames.Add(exameEncontrado);
                    }
                }
            }

            foreach (ClienteFuncaoExameASo clienteFuncaoExameAso in exameExtra)
            {
                if (!clienteFuncaoExameAso.Transcrito)
                {
                    exameEncontrado = exameDAO.findById((Int64)clienteFuncaoExameAso.ClienteFuncaoExame.Exame.Id, dbConnection);
                    if (exameEncontrado != null && exameEncontrado.Laboratorio.Value)
                    {
                        exames.Add(exameEncontrado);
                    }
                }
            }


            SWS.Integration.Model.ContaMedicaField contaMedica = new ContaMedicaField();
            contaMedica.codigoExternoContaMedica = "0";
            contaMedica.codigoExternoPaciente = Convert.ToString(aso.ClienteFuncaoFuncionario.Funcionario.Id);
            contaMedica.origemPaciente = aso.Funcionario.Rg;
            contaMedica.tipoAtendimento = "Ambulatorio";
            contaMedica.nome = aso.Funcionario.Nome;
            contaMedica.cpf = aso.Funcionario.Cpf;
            contaMedica.dataNascimento = aso.Funcionario.DataNascimento.ToString("yyyyMMdd");
            contaMedica.sexo = aso.Funcionario.Sexo;
            contaMedica.idade = Convert.ToString(ValidaCampoHelper.CalculaIdade(aso.Funcionario.DataNascimento));
            contaMedica.nomeMedico = aso.Coordenador != null ? aso.Coordenador.Nome : aso.Examinador != null ? aso.Examinador.Nome : " ";
            contaMedica.crm = aso.Coordenador != null ? aso.Coordenador.Crm : aso.Examinador != null ? aso.Examinador.Crm : " ";
            contaMedica.registroANS = aso.Cliente.Cnpj;
            contaMedica.convenio = aso.Cliente.RazaoSocial;
            contaMedica.numeroConvenio = Convert.ToString(aso.Cliente.Id);
            contaMedica.numeroPlano = Convert.ToString(aso.Cliente.Id);
            contaMedica.nomePlano = Convert.ToString(aso.Cliente.RazaoSocial);
            contaMedica.guia = aso.Codigo;
            contaMedica.matricula = aso.ClienteFuncaoFuncionario.Matricula;
            
            
            List<SWS.Integration.Model.Exames> examesLaboratorio = new List<SWS.Integration.Model.Exames>();
            
            foreach (Exame exame in exames ) {
                SWS.Integration.Model.Exames exameLaboratorio = new Exames();
                exameLaboratorio.codigoExternoMovimento = "0";
                exameLaboratorio.codigoExternoExame = Convert.ToString(exame.Id);
                exameLaboratorio.codigoTUSS = string.IsNullOrEmpty(exame.CodigoTuss) ? "0" : exame.CodigoTuss;
                exameLaboratorio.nomeExame = exame.Descricao;
                examesLaboratorio.Add(exameLaboratorio);
            }

            contaMedica.exames = examesLaboratorio;
                
            // Caminho para a pasta onde você deseja salvar o arquivo XML
            string pastaDestino = localGravacaoArquivo;

            XmlSerializer serializer = new XmlSerializer(typeof(SWS.Integration.Model.ContaMedicaField));

            // Nome do arquivo XML (pode ser ajustado conforme necessário)
            string nomeArquivo = aso.Codigo + ".xml";

            // Caminho completo para o arquivo XML
            string caminhoCompleto = Path.Combine(pastaDestino, nomeArquivo);

            // Certifique-se de que o diretório existe
            Directory.CreateDirectory(pastaDestino);

            XmlWriterSettings settings = new XmlWriterSettings
            {
                Indent = true,
                Encoding = Encoding.UTF8 // Especifica a codificação UTF-8
            };

            using (FileStream fileStream = new FileStream(caminhoCompleto, FileMode.Create))
            {
                using (XmlWriter xmlWriter = XmlWriter.Create(fileStream, settings))
                {
                    serializer.Serialize(xmlWriter, contaMedica);
                }
            }
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public Aso findAtendimentoById(Int64 id)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findAsoById");

            Aso atendimentoProcurado = new Aso();

            NpgsqlConnection dbConnection = FabricaConexao.getConexao();

            IAsoDao asoDao = FabricaDao.getAsoDao();
            IMedicoDao medicoDao = FabricaDao.getMedicoDao();
            IClienteFuncaoFuncionarioDao clienteFuncaoFuncionarioDao = FabricaDao.getClienteFuncaoFuncionarioDao();
            IFuncionarioDao funcionarioDao = FabricaDao.getFuncionarioDao();
            IClienteDao clienteDao = FabricaDao.getClienteDao();
            IPeriodicidadeDao periodicidadeDao = FabricaDao.getPeriodicidadeDao();
            IGheFonteAgenteExameAsoDao gheFonteAgenteExameAsoDao = FabricaDao.getGheFonteAgenteExameAsoDao();
            IClienteFuncaoExameASoDao clienteFuncaoExameAsoDao = FabricaDao.getClienteFuncaoExameASoDao();
            IUsuarioDao usuarioDao = FabricaDao.getUsuarioDao();
            IEstudoDao estudoDao = FabricaDao.getEstudoDao();
            ICentroCustoDao centroCustoDao = FabricaDao.getCentroCustoDao();
            IAsoAgenteRiscoDao asoAgenteRiscoDao = FabricaDao.getAsoAgenteRisco();

            try
            {
                atendimentoProcurado = asoDao.findById(id, dbConnection);

                /** incluindo dados de cliente */
                atendimentoProcurado.Cliente = clienteDao.findById((long)atendimentoProcurado.Cliente.Id, dbConnection);
                
                // montando outros objetos do aso. 

                atendimentoProcurado.Examinador = atendimentoProcurado.Examinador != null ? medicoDao.findById((long)atendimentoProcurado.Examinador.Id, dbConnection) : null;

                /* armazendando a matricula do colaborador */
                string matricula = atendimentoProcurado.ClienteFuncaoFuncionario.Matricula;
                atendimentoProcurado.ClienteFuncaoFuncionario = clienteFuncaoFuncionarioDao.findById((Int64)atendimentoProcurado.ClienteFuncaoFuncionario.Id, dbConnection);
                atendimentoProcurado.ClienteFuncaoFuncionario.Matricula = matricula;

                atendimentoProcurado.Periodicidade = periodicidadeDao.findById((Int64)atendimentoProcurado.Periodicidade.Id, dbConnection);

                atendimentoProcurado.Coordenador = atendimentoProcurado.Coordenador != null ? medicoDao.findById((long)atendimentoProcurado.Coordenador.Id, dbConnection) : null;

                //asoProcurado.Cliente = asoProcurado.Cliente != null ? clienteDao.findClienteByCnpj((String)asoProcurado.Cliente.Cnpj, dbConnection) : null;

                if (atendimentoProcurado.Finalizador != null)
                    atendimentoProcurado.Finalizador = usuarioDao.findById((Int64)atendimentoProcurado.Finalizador.Id, dbConnection);

                atendimentoProcurado.Criador = usuarioDao.findById((Int64)atendimentoProcurado.Criador.Id, dbConnection);

                // montando as coleções de exames do pcmso e do exame extra.

                // montando as coleções de exames do pcmso.
                atendimentoProcurado.ExamePcmso = gheFonteAgenteExameAsoDao.findAllExamesByAsoInSituacao(atendimentoProcurado, null, null, null, null, null, dbConnection);

                // montando as coleções de exames do exame extra.
                atendimentoProcurado.ExameExtra = clienteFuncaoExameAsoDao.findAllExamesByAsoInSituacao(atendimentoProcurado, null, null, null, null, null, dbConnection);

                /* incluindo dados do pcmso no aso caso exista um pcmso */

                atendimentoProcurado.Estudo = atendimentoProcurado.Estudo != null ? (Estudo)estudoDao.findById((Int64)atendimentoProcurado.Estudo.Id, dbConnection) : null;

                if (atendimentoProcurado.CentroCusto != null)
                {
                    atendimentoProcurado.CentroCusto = centroCustoDao.findById((long)atendimentoProcurado.CentroCusto.Id, dbConnection);
                    atendimentoProcurado.CentroCusto.Cliente = clienteDao.findById((long)atendimentoProcurado.CentroCusto.Cliente.Id, dbConnection);
                }

                /* incluíndo informação do aso agente risco no atendimento */
                atendimentoProcurado.AsoAgenteRisco = asoAgenteRiscoDao.findAllByAtendimento(atendimentoProcurado, dbConnection);

                return atendimentoProcurado;

            }
            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findAsoById", ex);
                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }
            finally
            {
                FabricaConexao.fecharConexao();
            }
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public Aso findAsoByCodigo(String codigo)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findAsoById");

            Aso asoProcurado = new Aso();

            NpgsqlConnection dbConnection = FabricaConexao.getConexao();

            IAsoDao asoDao = FabricaDao.getAsoDao();
            IMedicoDao medicoDao = FabricaDao.getMedicoDao();
            IClienteFuncaoFuncionarioDao clienteFuncaoFuncionarioDao = FabricaDao.getClienteFuncaoFuncionarioDao();
            IFuncionarioDao funcionarioDao = FabricaDao.getFuncionarioDao();
            IClienteDao clienteDao = FabricaDao.getClienteDao();
            IPeriodicidadeDao periodicidadeDao = FabricaDao.getPeriodicidadeDao();
            IGheFonteAgenteExameAsoDao gheFonteAgenteExameAsoDao = FabricaDao.getGheFonteAgenteExameAsoDao();
            IClienteFuncaoExameASoDao clienteFuncaoExameAsoDao = FabricaDao.getClienteFuncaoExameASoDao();
            IUsuarioDao usuarioDao = FabricaDao.getUsuarioDao();
            IEstudoDao estudoDao = FabricaDao.getEstudoDao();
            ICentroCustoDao centroCustoDao = FabricaDao.getCentroCustoDao();
            try
            {
                asoProcurado = asoDao.findAsoByCodigo(codigo, dbConnection);

                if (asoProcurado != null)
                {

                    asoProcurado.ClienteFuncaoFuncionario = clienteFuncaoFuncionarioDao.findById((Int64)asoProcurado.ClienteFuncaoFuncionario.Id, dbConnection);

                    asoProcurado.Periodicidade = periodicidadeDao.findById((Int64)asoProcurado.Periodicidade.Id, dbConnection);

                    asoProcurado.Coordenador = asoProcurado.Coordenador != null ? medicoDao.findById((Int64)asoProcurado.Coordenador.Id, dbConnection) : null;

                    //asoProcurado.setEmpresa(clienteDao.findClienteByCnpj((String)asoProcurado.getEmpresa().getCnpj(), dbConnection));

                    if (asoProcurado.Finalizador != null)
                        asoProcurado.Finalizador = usuarioDao.findById((Int64)asoProcurado.Finalizador.Id, dbConnection);

                    asoProcurado.Criador = usuarioDao.findById((Int64)asoProcurado.Criador.Id, dbConnection);

                    // montando as coleções de exames do pcmso.
                    asoProcurado.ExamePcmso = gheFonteAgenteExameAsoDao.findAllExamesByAsoInSituacao(asoProcurado, null, null, null, null, false, dbConnection);

                    // montando as coleções de exames do exame extra.
                    asoProcurado.ExameExtra = clienteFuncaoExameAsoDao.findAllExamesByAsoInSituacao(asoProcurado, null, null, null, null, false, dbConnection);

                    /* incluindo dados do pcmso no aso caso exista um pcmso */
                    asoProcurado.Estudo = asoProcurado.Estudo != null ? (Estudo)estudoDao.findById((Int64)asoProcurado.Estudo.Id, dbConnection) : null;

                    /* incluindo dados do centro de custo */
                    if (asoProcurado.CentroCusto != null)
                    {
                        asoProcurado.CentroCusto = centroCustoDao.findById((long)asoProcurado.CentroCusto.Id, dbConnection);
                        asoProcurado.CentroCusto.Cliente = clienteDao.findById((long)asoProcurado.CentroCusto.Cliente.Id, dbConnection);
                    }
                }

                return asoProcurado;
            }
            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findAsoByCodigo", ex);
                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }
            finally
            {
                FabricaConexao.fecharConexao();
            }
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public Aso findAsoByIdComExamesTranscritos(Int64 id)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findAsoByIdComExamesTranscritos");

            Aso asoProcurado = new Aso();

            NpgsqlConnection dbConnection = FabricaConexao.getConexao();

            IAsoDao asoDao = FabricaDao.getAsoDao();
            IMedicoDao medicoDao = FabricaDao.getMedicoDao();
            IClienteFuncaoFuncionarioDao clienteFuncaoFuncionarioDao = FabricaDao.getClienteFuncaoFuncionarioDao();
            IFuncionarioDao funcionarioDao = FabricaDao.getFuncionarioDao();
            IClienteDao clienteDao = FabricaDao.getClienteDao();
            IPeriodicidadeDao periodicidadeDao = FabricaDao.getPeriodicidadeDao();
            IGheFonteAgenteExameAsoDao gheFonteAgenteExameAsoDao = FabricaDao.getGheFonteAgenteExameAsoDao();
            IClienteFuncaoExameASoDao clienteFuncaoExameAsoDao = FabricaDao.getClienteFuncaoExameASoDao();
            IUsuarioDao usuarioDao = FabricaDao.getUsuarioDao();

            try
            {
                asoProcurado = asoDao.findById(id, dbConnection);

                // montando outros objetos do aso. 
                /*
                if (asoProcurado.getExaminador() != null)
                {
                    asoProcurado.setExaminador(medicoDao.findMedicoById((Int64)asoProcurado.getExaminador().getId(), dbConnection));
                }
                */

                asoProcurado.ClienteFuncaoFuncionario = clienteFuncaoFuncionarioDao.findById((Int64)asoProcurado.ClienteFuncaoFuncionario.Id, dbConnection);

                asoProcurado.Periodicidade = periodicidadeDao.findById((Int64)asoProcurado.Periodicidade.Id, dbConnection);

                //asoProcurado.setCoordenador(medicoDao.findMedicoById((Int64)asoProcurado.getCoordenador().getId(), dbConnection));

                //asoProcurado.setEmpresa(clienteDao.findClienteByCnpj((String)asoProcurado.getEmpresa().getCnpj(), dbConnection));

                if (asoProcurado.Finalizador != null)
                    asoProcurado.Finalizador = usuarioDao.findById((Int64)asoProcurado.Finalizador.Id, dbConnection);

                asoProcurado.Criador = usuarioDao.findById((Int64)asoProcurado.Criador.Id, dbConnection);

                // montando as coleções de exames do pcmso e do exame extra.

                // montando as coleções de exames do pcmso.
                asoProcurado.ExamePcmso = gheFonteAgenteExameAsoDao.findAllExamesByAsoInSituacao(asoProcurado, null, null, null, null, null, dbConnection);

                // montando as coleções de exames do exame extra.
                asoProcurado.ExameExtra = clienteFuncaoExameAsoDao.findAllExamesByAsoInSituacao(asoProcurado, null, null, null, null, null, dbConnection);

                return asoProcurado;

            }
            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findAsoByIdComExamesTranscritos", ex);
                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }
            finally
            {
                FabricaConexao.fecharConexao();
            }
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public ClienteFuncaoFuncionario findClienteFuncaoFuncionarioById(Int64 id)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findClienteFuncaoFuncionarioById");

            Aso asoProcurado = new Aso();

            NpgsqlConnection dbConnection = FabricaConexao.getConexao();

            IClienteFuncaoFuncionarioDao clienteFuncaoFuncionarioDao = FabricaDao.getClienteFuncaoFuncionarioDao();
            
            try
            {
                return clienteFuncaoFuncionarioDao.findById(id, dbConnection);

            }
            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findClienteFuncaoFuncionarioById", ex);
                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }
            finally
            {
                FabricaConexao.fecharConexao();
            }
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public void cancelaAso(Aso aso)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método cancelaAso");

            NpgsqlConnection dbConnection = FabricaConexao.getConexao();
            NpgsqlTransaction transaction = dbConnection.BeginTransaction();

            IAsoDao asoDao = FabricaDao.getAsoDao();
            IMovimentoDao movimentoDao = FabricaDao.getMovimentoDao();
            IClienteFuncaoExameASoDao clienteFuncaoExameAsoDao = FabricaDao.getClienteFuncaoExameASoDao();
            IGheFonteAgenteExameAsoDao gheFonteAgenteExameAsoDao = FabricaDao.getGheFonteAgenteExameAsoDao();
            IEstudoDao estudoDao = FabricaDao.getEstudoDao();
            ICentroCustoDao centroCustoDao = FabricaDao.getCentroCustoDao();
            
            try
            {
                /* verificando se o aso já está cancelado */
                if (aso.Situacao.Equals(ApplicationConstants.DESATIVADO))
                    throw new AsoException(AsoException.msg5);
                
                /* verificando se existe exame em atendimento para o aso. */
                if (clienteFuncaoExameAsoDao.verificaExameAtendimento(aso, dbConnection) && gheFonteAgenteExameAsoDao.verificaExameAtendimento(aso, dbConnection))
                    throw new AsoException(AsoException.msg12);
                
                /* Realizando update no aso e alterando o status dele para cancelado. */
                asoDao.updateStatusAso(aso, ApplicationConstants.DESATIVADO, dbConnection);
                        
                /* cancelando os exames do aso, informando o flag cancelado e a data que foi cancelado. Verificando se existe exames no pcmso para ser cancelado. */
                if (aso.ExamePcmso.Count > 0)
                    gheFonteAgenteExameAsoDao.cancelaExameAso(aso, dbConnection);

                /* verificando se existe exames extra para ser cancelado. */
                if (aso.ExameExtra.Count > 0)
                    clienteFuncaoExameAsoDao.cancelaExameAso(aso, dbConnection);


                /* buscando todos os lançamentos referente ao atendimento no movimento */

                List<Movimento> movimentos = movimentoDao.findAllLancamentoByAtendimento(aso, dbConnection);
                     
                /* cancelando todos os movimentosLancados que estão pendentes no atendimento */

                movimentos.ForEach(delegate(Movimento movimento)
                {
                    /* verificando se existe o movimento já foi faturado */
                    if (string.Equals(movimento.Situacao, ApplicationConstants.FECHADO))
                        throw new Exception("Esse atendimento já foi faturado. O cancelamento não será permitido.");

                    /* preenchendo informação do centro de custo */
                    if (movimento.CentroCusto != null)
                        movimento.CentroCusto = centroCustoDao.findById((long)movimento.CentroCusto.Id, dbConnection);

                    if (string.Equals(movimento.Situacao, ApplicationConstants.PENDENTE))
                    {
                        /* alterando a situação do movimento para cancelado */
                        movimentoDao.changeStatus(movimento, ApplicationConstants.CANCELADA, dbConnection);

                        /* criando um novo lançamento no movimento para contabilidade */
                        movimentoDao.insert(new Movimento(null, movimento.GheFonteAgenteExameAso, movimento.ClienteFuncaoExameAso, movimento.Cliente, movimento.ContratoProduto, movimento.Nfe, DateTime.Now, null, ApplicationConstants.CANCELADA, movimento.PrecoUnit * -1, movimento.ValorComissao, movimento.Estudo, movimento.Aso, movimento.Quantidade, movimento.Usuario, movimento.Empresa, movimento.CustoUnitario, movimento.Unidade, movimento.Protocolo, DateTime.Now, aso.CentroCusto, movimento.ContratoExame, movimento.ClienteCredenciada), dbConnection);

                    }
                });


                transaction.Commit();
                
            }
            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - cancelaAso", ex);
                transaction.Rollback();
                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }
            finally
            {
                FabricaConexao.fecharConexao();
            }
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public void finalizaAso(Aso atendimento)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método finalizaAso");

            NpgsqlConnection dbConnection = FabricaConexao.getConexao();
            NpgsqlTransaction transaction = dbConnection.BeginTransaction();

            IAsoDao asoDao = FabricaDao.getAsoDao();
            IMovimentoDao movimentoDao = FabricaDao.getMovimentoDao();
            IContratoDao contratoDao = FabricaDao.getContratoDao();
            IProdutoDao produtoDao = FabricaDao.getProdutoDao();
            IContratoProdutoDao contratoProdutoDao = FabricaDao.getContratoProdutoDao();
            IContratoExameDao contratoExameDao = FabricaDao.getContratoExameDao();
            IGheFonteAgenteExameDao gheFonteAgenteExameDao = FabricaDao.getGheFonteAgenteExameDao();
            IClienteFuncaoExameDao clienteFuncaoExameDao = FabricaDao.getClienteFuncaoExameDao();
            IClienteFuncaoExameASoDao clienteFuncaoExameAsoDao = FabricaDao.getClienteFuncaoExameASoDao();
            IGheFonteAgenteExameAsoDao gheFonteAgenteExameAsoDao = FabricaDao.getGheFonteAgenteExameAsoDao();
            IClienteDao clienteDao = FabricaDao.getClienteDao();
            IEstudoDao pcmsoDao = FabricaDao.getEstudoDao();
            
            try
            {
                
                Contrato contratoVigente = null;
                Contrato contratoVigentePrestador = null;
                ContratoProduto contratoProduto = null;
                ContratoExame contratoExame;
                Exame exame = null;
                Produto produtolancamentoAutomatico = null;

                bool transcrito = false;
                bool inclusaoExame = false;

                String geraMovimento = String.Empty;
                PermissionamentoFacade.mapaConfiguracoes.TryGetValue(ConfigurationConstans.GERA_MOVIMENTO_FINALIZACAO_EXAME, out geraMovimento);
                
                String geraProdutoAutomatico = String.Empty;
                PermissionamentoFacade.mapaConfiguracoes.TryGetValue(ConfigurationConstans.GRAVA_PRODUTO_AUTOMATICO, out geraProdutoAutomatico);

                String produtoASoTranscrito = String.Empty;
                PermissionamentoFacade.mapaConfiguracoes.TryGetValue(ConfigurationConstans.ASO_TRANSCRITO, out produtoASoTranscrito);
                
                String produtoInclusaoExame = String.Empty;
                PermissionamentoFacade.mapaConfiguracoes.TryGetValue(ConfigurationConstans.ASO_INCLUSAO_EXAMES, out produtoInclusaoExame);
                
                /* VERIFICANDO SE O ASO ESTÁ EM CONSTRUÇÃO. CASO ELE ESTEJA EM OUTRO ESTADO
                 * ENTÃO A FINALIZAÇÃO NÃO É PERMITIDA. 
                 * O STATUS DO ASO PODERÁ SER CANCELADO OU FINALIZADO.*/

                if (atendimento.Situacao.Equals(ApplicationConstants.DESATIVADO) || atendimento.Situacao.Equals(ApplicationConstants.FECHADO))
                    throw new AsoException(AsoException.msg9);
                    
                /* VERIFICANDO PARA SABER SE O CLIENTE É UMA CREDENCIADA DE ALGUÉM.
                * * CASO SEJA, ENTÃO O LANCAMENTO FINANCEIRO SERÁ FEITO NA CREDENCIADA. */

                Cliente clienteCobrado = null;
                Cliente clienteCredenciada = null;

                if (atendimento.ClienteFuncaoFuncionario.ClienteFuncao.Cliente.CredenciadaCliente != null)
                {
                    clienteCobrado = atendimento.ClienteFuncaoFuncionario.ClienteFuncao.Cliente.CredenciadaCliente;
                    clienteCobrado = clienteDao.findById((Int64)clienteCobrado.Id, dbConnection);
                    clienteCredenciada = atendimento.ClienteFuncaoFuncionario.ClienteFuncao.Cliente;
                    clienteCredenciada = clienteDao.findById((long)clienteCredenciada.Id, dbConnection);
                }
                else
                {
                    clienteCobrado = atendimento.ClienteFuncaoFuncionario.ClienteFuncao.Cliente;
                    clienteCobrado = clienteDao.findById((Int64)clienteCobrado.Id, dbConnection);
                }

                /* VERIFICANDO PARA VER SE EXISTE ALGUM EXAME PENDENTE DE FINALIZAÇÃO. CASO EXISTA
                 * ENTÃO O ASO NÃO PODERÁ SER FINALIZADO
                 * MONTANDO AS COLEÇÕES DE EXAMES NO ASO. 
                 * OS EXAMES PODEM VIR DO PCMSO OU INCLUÍDOS NO ATENDIMENTO PELO EXAME EXTRA */

                List<ClienteFuncaoExameASo> examesExtra = new List<ClienteFuncaoExameASo>();
                List<GheFonteAgenteExameAso> examesPcmso = new List<GheFonteAgenteExameAso>();
                
                examesExtra = clienteFuncaoExameAsoDao.findAllExamesByAsoInSituacao(atendimento, null, false, false, null, false, dbConnection);
                examesPcmso = gheFonteAgenteExameAsoDao.findAllExamesByAsoInSituacao(atendimento, null, false, false, null, false, dbConnection);
                
                if (examesExtra.Count > 0 || examesPcmso.Count > 0)
                    throw new AsoException(AsoException.msg10);

                
                /* COLOCANDO O ATENDIMENTO COM O FLAG DE FINALIZADO */
                atendimento.Finalizador = PermissionamentoFacade.usuarioAutenticado;
                asoDao.updateStatusAso(atendimento, ApplicationConstants.FECHADO, dbConnection);

                /* verificando questões do movimento financeiro */
                
                /* CASO O CLIENTE USE A INCLUSÃO DO MOVIMENTO POR ITEM ENTÃO NÃO SERÁ
                 * NECESSÁRIO INCLUIR NOVAMENTE NO MOVIMENTO */
                
                if (!Convert.ToBoolean(geraMovimento))
                {
                    #region LANÇAMENTO EXAMES, PRODUTOS E PRODUTOS AUTOMATICOS NO MOVIMENTO

                    /* VERIFICANDO SE O CLIENTE ESTÁ GRAVADO COMO USAR CONTRATO. */
                    if (clienteCobrado.UsaContrato)
                    {
                        /* apurando o contrato vigente do cliente */
                        contratoVigente = contratoDao.findContratoVigenteByData((DateTime)atendimento.DataElaboracao, clienteCobrado, null, dbConnection);

                        if (contratoVigente == null)
                            throw new ContratoException(ContratoException.msg2);
                    
                        #region EXAMES DO PCMSO

                        /* INCLUINDO OS EXAMES NO MOVIMENTO QUE SÃO DO PCMSO */

                        atendimento.ExamePcmso.ForEach(delegate(GheFonteAgenteExameAso gheFonteAgenteExameAso) {
                            if (gheFonteAgenteExameAso.Finalizado && (!gheFonteAgenteExameAso.Cancelado && !gheFonteAgenteExameAso.Transcrito))
                            {
                                /* SOMENTE SERÁ INCLUÍDO NO MOVIMENTO SE O OBJETO EM QUESTÃO NÃO
                                 * TIVER SIDO INCLUÍDO POR ALGUM OUTRO MÉTODO DE INCLUSÃO NO MOVIMENTO. VERIFICAÇÃO
                                 * NECESSÁRIA PORQUE A CHAVE DE CONFIGURAÇÃO FOI INCLUÍDA DEPOIS QUE ALGUNS MOVIMENTOS
                                 * JÁ FORAM GRAVADOS.*/

                                if (!movimentoDao.findMovimentoByGheFonteAgenteExameAso(gheFonteAgenteExameAso, dbConnection))
                                {
                                    /* VERIFICANDO SE O ITEM DO GHE_FONTE_AGENTE_EXAME_ASO TEM ALGUM PRESTADOR.
                                     * CASO TENHA, ENTÃO O CONTRATO QUE SERÁ USADO SERÁ O DO PRESTADOR.
                                     * CASO NÃO TENHA CONTRATO COM O PRESTADOR, ENTÃO SERÁ USADO O CONTRATO DO CLIENTE. */

                                    if (gheFonteAgenteExameAso.Prestador != null)
                                    {
                                        contratoVigentePrestador = contratoDao.findContratoVigenteByData((DateTime)atendimento.DataElaboracao, clienteCobrado, gheFonteAgenteExameAso.Prestador, dbConnection);

                                        if (contratoVigentePrestador == null)
                                            throw new ContratoException(ContratoException.msg8);
                                    }

                                    exame = gheFonteAgenteExameDao.findExameByGheFonteAgenteExame(gheFonteAgenteExameAso.GheFonteAgenteExame, dbConnection);
                                    contratoExame = contratoExameDao.findAllByContratoAndExameAndSituacao(contratoVigentePrestador == null ? contratoVigente : contratoVigentePrestador, exame, ApplicationConstants.ATIVO, dbConnection);


                                    /* CASO NÃO EXISTA O EXAME NO CONTRATO ENTÃO SERÁ AVISADO AO USUÁRIO */

                                    if (contratoExame == null)
                                    {
                                        /* não existe o exame solicitado. Verificado todos os outros exames do atendimento que não estão no contrato do cliente. */
                                        List<Exame> exames = asoDao.FindAllExamesByAsoBySituacao(atendimento, true, null, false, null, false, dbConnection);

                                        HashSet<ContratoExame> examesInContrato = contratoExameDao.findAtivosByContrato(contratoVigentePrestador == null ? contratoVigente : contratoVigentePrestador, dbConnection);

                                        bool result = false;
                                        List<Exame> examesNotInContrato = new List<Exame>();

                                        exames.ForEach(delegate(Exame ex)
                                        {
                                            if (!examesInContrato.ToList().Exists(x => x.Exame.Id == ex.Id))
                                                examesNotInContrato.Add(ex);
                                        });

                                        StringBuilder examesNaoIncluir = new StringBuilder();

                                        examesNotInContrato.ForEach(delegate(Exame ex)
                                        {
                                            examesNaoIncluir.Append("Código: " + ex.Id.ToString().PadLeft(3, '0') + " - " + ex.Descricao + System.Environment.NewLine);
                                            
                                        });

                                        throw new ContratoException("O(s) exame(s) : " + System.Environment.NewLine + examesNaoIncluir.ToString() + ContratoException.msg4);
                                    }


                                    /* CRIANDO MOVIMENTO E EFETUANDO A GRAVACAO */
                                    movimentoDao.insert(new Movimento(null, gheFonteAgenteExameAso, null, clienteCobrado, null, null, DateTime.Now, null, ApplicationConstants.PENDENTE, contratoExame.PrecoContratado, null, null, atendimento, Convert.ToInt32(1), PermissionamentoFacade.usuarioAutenticado, PermissionamentoFacade.usuarioAutenticado.Empresa, contratoExame.CustoContrato, pcmsoDao.pcmsoIsUnidade((Estudo)atendimento.Estudo, dbConnection) ? atendimento.Estudo.ClienteContratante : null, null, DateTime.Now, atendimento.CentroCusto, contratoExame, clienteCredenciada), dbConnection);

                                }

                            }

                        });

                        #endregion

                        #region EXAMES EXTRAS


                        atendimento.ExameExtra.ForEach(delegate(ClienteFuncaoExameASo clienteFuncaoExameAso){
                            if (clienteFuncaoExameAso.Finalizado && (!clienteFuncaoExameAso.Cancelado && !clienteFuncaoExameAso.Transcrito))
                            {
                                /* SOMENTE SERÁ INCLUÍDO NO MOVIMENTO SE O OBJETO EM QUESTÃO NÃO
                                 * TIVER SIDO INCLUÍDO POR ALGUM OUTRO MÉTODO DE INCLUSÃO NO MOVIMENTO. */

                                if (!movimentoDao.findMovimentoByClienteFuncaoExameAso(clienteFuncaoExameAso, dbConnection))
                                {
                                    /* VERIFICANDO SE O ITEM DO CLIENTE_FUNCAO_EXAME_ASO TEM ALGUM PRESTADOR.
                                     * CASO TENHA, ENTÃO O CONTRATO QUE SERÁ USADO SERÁ O DO PRESTADOR.
                                     * CASO NÃO TENHA CONTRATO, ENTÃO SERÁ USADO O CONTRATO DO CLIENTE. */

                                    if (clienteFuncaoExameAso.Prestador != null)
                                    {
                                        contratoVigentePrestador = contratoDao.findContratoVigenteByData((DateTime)atendimento.DataElaboracao, clienteCobrado, clienteFuncaoExameAso.Prestador, dbConnection);

                                        if (contratoVigentePrestador == null)
                                            throw new ContratoException(ContratoException.msg8);
                                    }

                                    exame = clienteFuncaoExameDao.findExameByClienteFuncaoExame(clienteFuncaoExameAso.ClienteFuncaoExame, dbConnection);
                                    contratoExame = contratoExameDao.findAllByContratoAndExameAndSituacao(contratoVigentePrestador == null ? contratoVigente : contratoVigentePrestador, exame, ApplicationConstants.ATIVO, dbConnection);


                                    /* CASO NÃO EXISTA O EXAME NO CONTRATO ENTÃO SERÁ AVISADO AO USUÁRIO */

                                    if (contratoExame == null)
                                    {
                                        /* não existe o exame solicitado. Verificado todos os outros exames do atendimento que não estão no contrato do cliente. */
                                        List<Exame> exames = asoDao.FindAllExamesByAsoBySituacao(atendimento, true, null, false, null, false, dbConnection);

                                        HashSet<ContratoExame> examesInContrato = contratoExameDao.findAtivosByContrato(contratoVigentePrestador == null ? contratoVigente : contratoVigentePrestador, dbConnection);

                                        bool result = false;
                                        List<Exame> examesNotInContrato = new List<Exame>();
                                        
                                        exames.ForEach(delegate(Exame ex)
                                        {
                                            if (!examesInContrato.ToList().Exists(x => x.Exame.Id == ex.Id))
                                                examesNotInContrato.Add(ex);
                                        });

                                        StringBuilder examesNaoIncluir = new StringBuilder();

                                        examesNotInContrato.ForEach(delegate(Exame ex)
                                        {
                                            examesNaoIncluir.Append("Código: " + ex.Id.ToString().PadLeft(3, '0') + " - " + ex.Descricao + System.Environment.NewLine);

                                        });

                                        throw new ContratoException("O(s) exame(s) : " + System.Environment.NewLine + examesNaoIncluir.ToString() + ContratoException.msg4);
                                    }
                                    movimentoDao.insert(new Movimento(null, null, clienteFuncaoExameAso, clienteCobrado, null, null, DateTime.Now, null, ApplicationConstants.PENDENTE, contratoExame.PrecoContratado, null, null, atendimento, Convert.ToInt32(1), PermissionamentoFacade.usuarioAutenticado, PermissionamentoFacade.usuarioAutenticado.Empresa, contratoExame.CustoContrato, pcmsoDao.pcmsoIsUnidade((Estudo)atendimento.Estudo, dbConnection) ? atendimento.Estudo.ClienteContratante : null, null, DateTime.Now, atendimento.CentroCusto, contratoExame, clienteCredenciada), dbConnection);

                                }
                            }
                        });

                        #endregion

                        /* VERIFICANDO SE EXISTE OCORRÊNCIA PARA GRAVAÇÃO DE PRODUTOS
                         * AUTOMÁTICOS (ASO TRANSCRITO E ASO COM INCLUSÕES DE EXAMES */

                        #region PRODUTO AUTOMATICO, ASO TRANSCRITO E ASO COM INCLUSAO DE EXAMES
                        
                        if (Convert.ToBoolean(geraProdutoAutomatico))
                        {
                            /* VERIFICANDO A OCORRENCIA DE PRODUTOS AUTOMATICO */
                            
                            transcrito = asoDao.verificaAsoTranscrito(atendimento, dbConnection);
                            inclusaoExame = asoDao.verificaAsoInclusaoExame(atendimento, dbConnection);

                            if (transcrito)
                            {
                                produtolancamentoAutomatico = produtoDao.findProdutoById(Convert.ToInt64(produtoASoTranscrito), dbConnection);

                                /* RECUPERANDO O CONTRATOPRODUTO PARA REALIZAR A INCLUSÃO NO MOVIMENTO */

                                contratoProduto = contratoProdutoDao.findByContratoAndProduto(contratoVigente, produtolancamentoAutomatico, dbConnection);

                                /* CASO NÃO EXISTA CADASTRADO NO CONTRATO DO CLIENTE, ENTÃO SERÁ AVISADO 
                                 * AO USUÁRIO SOBRE O PROBLEMA */
                                if (contratoProduto == null)
                                    throw new ContratoException("O Produto: " + produtolancamentoAutomatico.Nome + ContratoException.msg4);


                                /* INCLUINDO NO MOVIMENTO O PRODUTO AUTOMÁTICO */
                                    
                                movimentoDao.insert(new Movimento(null, null, null, clienteCobrado, contratoProduto, null, DateTime.Now, null, ApplicationConstants.PENDENTE, contratoProduto.PrecoContratado, null, null, atendimento, Convert.ToInt32(1), PermissionamentoFacade.usuarioAutenticado, PermissionamentoFacade.usuarioAutenticado.Empresa, contratoProduto.CustoContrato, pcmsoDao.pcmsoIsUnidade((Estudo)atendimento.Estudo, dbConnection) ? atendimento.Estudo.ClienteContratante : null, null, DateTime.Now, atendimento.CentroCusto, null, clienteCredenciada), dbConnection); 
                            }
                            else if (inclusaoExame)
                            {
                                produtolancamentoAutomatico = produtoDao.findProdutoById(Convert.ToInt64(produtoInclusaoExame), dbConnection);

                                /* RECUPERANDO O CONTRATOPRODUTO PARA REALIZAR A INCLUSÃO NO MOVIMENTO */

                                contratoProduto = contratoProdutoDao.findByContratoAndProduto(contratoVigente, produtolancamentoAutomatico, dbConnection);

                                /* CASO NÃO EXISTA CADASTRADO NO CONTRATO DO CLIENTE, ENTÃO SERÁ AVISADO 
                                 * AO USUÁRIO SOBRE O PROBLEMA */

                                if (contratoProduto == null)
                                    throw new ContratoException("O Produto: " + produtolancamentoAutomatico.Nome + ContratoException.msg4);
                                
                                movimentoDao.insert(new Movimento(null, null, null, clienteCobrado, contratoProduto, null, DateTime.Now, null, ApplicationConstants.PENDENTE, contratoProduto.PrecoContratado, null, null, atendimento, Convert.ToInt32(1), PermissionamentoFacade.usuarioAutenticado, PermissionamentoFacade.usuarioAutenticado.Empresa, contratoProduto.CustoContrato, pcmsoDao.pcmsoIsUnidade((Estudo)atendimento.Estudo, dbConnection) ? atendimento.Estudo.ClienteContratante : null, null, DateTime.Now, atendimento.CentroCusto, null, clienteCredenciada), dbConnection);
                            }
                        }
                        
                        #endregion
                    }
                    else
                    {
                        /* NESSE CASO, O CLIENTE NÃO UTILIZA CONTRATO ENTÃO SERÁ FEITO A INCLUSÃO
                         * ATRAVÉS DO CONTRATO DO SISTEMA. RECURSO PARA MANTER A COESÃO DO SISTEMA E DO MOVIMENTO, JÁ
                         QUE NO MOVIMENTO EXISTE O RELACIONAMENTO COM O CONTRATO. */

                        Contrato contrato = contratoDao.findContratoSistemaByCliente(clienteCobrado, dbConnection);

                        if (contrato != null)
                            criarMovimentoAsoClienteUsaContratoSistema(atendimento, contrato, null, dbConnection);
                        else
                        {
                            /*CRIANDO CONTRATO PARA O CLIENTE.*/
                            contrato = contratoDao.insert(new Contrato(null, String.Empty, clienteCobrado, PermissionamentoFacade.usuarioAutenticado, null, null, null, ApplicationConstants.CONTRATO_FECHADO, String.Empty, null, null, String.Empty, null, true, null, null, false, false, false, null, false, null, null, null, null, null, false, null, null, null, String.Empty, PermissionamentoFacade.usuarioAutenticado.Empresa), dbConnection);

                            criarMovimentoAsoClienteUsaContratoSistema(atendimento, contrato, null, dbConnection);
                        }

                        /* VERIFICANDO SE O PARAMETRO DA CONFIGURAÇÃO ESTÁ MARCADO PARA EFETUAR COBRANÇA
                         * AUTOMÁTICA DE PRODUTOS */

                        if (Convert.ToBoolean(geraProdutoAutomatico))
                        {
                            /* VERIFICANDO SE O ASO TEM ALGUMA OCORRÊNCIA PARA EFETUAR A COBRANCA
                             * DE ASO TRANSCRITO OU ASO COM INCLUSÃO DE EXAMES. */

                            transcrito = asoDao.verificaAsoTranscrito(atendimento, dbConnection);
                            inclusaoExame = asoDao.verificaAsoInclusaoExame(atendimento, dbConnection);

                            if (transcrito)
                            {
                                PermissionamentoFacade.mapaConfiguracoes.TryGetValue(ConfigurationConstans.ASO_TRANSCRITO, out produtoASoTranscrito);

                                produtolancamentoAutomatico = produtoDao.findProdutoById(Convert.ToInt64(produtoASoTranscrito), dbConnection);

                                criarMovimentoAsoClienteUsaContratoSistema(atendimento, contrato, produtolancamentoAutomatico, dbConnection);

                            }
                            else if (inclusaoExame)
                            {
                                PermissionamentoFacade.mapaConfiguracoes.TryGetValue(ConfigurationConstans.ASO_INCLUSAO_EXAMES, out produtoInclusaoExame);

                                produtolancamentoAutomatico = produtoDao.findProdutoById(Convert.ToInt64(produtoInclusaoExame), dbConnection);

                                criarMovimentoAsoClienteUsaContratoSistema(atendimento, contrato, produtolancamentoAutomatico, dbConnection);
                            }
                        }
                    }

                    #endregion
                }
                else
                {
                    #region SOMENTE PRODUTO AUTOMATICO

                    /* NESSE CASO, O CLIENTE ESTÁ MARCADO COMO GERAR MOVIMENTO AUTOMÁTICO NA FINALIZAÇÃO.
                     * VERIFICANDO SE O PARAMETRO DA CONFIGURAÇÃO ESTÁ MARCADO PARA EFETUAR COBRANÇA
                     * AUTOMÁTICA DE PRODUTOS */
                    
                    if (Convert.ToBoolean(geraProdutoAutomatico))
                    {
                        /* VERIFICANDO SE O ASO TEM ALGUMA OCORRÊNCIA PARA EFETUAR A COBRANCA
                         * DE ASO TRANSCRITO OU ASO COM INCLUSÃO DE EXAMES. */

                        transcrito = asoDao.verificaAsoTranscrito(atendimento, dbConnection);
                        inclusaoExame = asoDao.verificaAsoInclusaoExame(atendimento, dbConnection);

                        if (transcrito)
                        {
                            #region PRODUTO AUTOMATICO ASO TRANSCRITO

                            /* VERIFICANDO SE O CLIENTE ESTÁ MARCADO PARA USAR CONTRATO */
                            if (clienteCobrado.UsaContrato)
                            {
                                contratoVigente = contratoDao.findContratoVigenteByData((DateTime)atendimento.DataElaboracao, clienteCobrado, null, dbConnection);

                                if (contratoVigente == null)
                                    throw new ContratoException(ContratoException.msg2);
                                
                                /* RECUPERANDO O CONTRATO DO CLIENTE ONDE ESTÁ CADASTRADO OS PRODUTOS QUE SERÃO LANÇADOS */

                                produtolancamentoAutomatico = produtoDao.findProdutoById(Convert.ToInt64(produtoASoTranscrito), dbConnection);
                                
                                /* RECUPERANDO O CONTRATOPRODUTO PARA REALIZAR A INCLUSÃO NO MOVIMENTO */
                                contratoProduto = contratoProdutoDao.findByContratoAndProduto(contratoVigente, produtolancamentoAutomatico, dbConnection);
                                
                                /* CASO O CLIENTE NÃO TENHA O PRODUTO AUTOMATICO CADASTRADO NO CONTRATO ,ENTÃO
                                 * * SERÁ INFORMADO AO USUÁRIO */
                                if (contratoProduto == null)
                                    throw new ContratoException("O Produto: " + produtolancamentoAutomatico.Nome + ContratoException.msg4);
                                
                                /* INCLUINDO NO MOVIMENTO O PRODUTO AUTOMATICO */
                                movimentoDao.insert(new Movimento(null, null, null, clienteCobrado, contratoProduto, null, DateTime.Now, null, ApplicationConstants.PENDENTE, contratoProduto.PrecoContratado, null, null, atendimento, Convert.ToInt32(1), PermissionamentoFacade.usuarioAutenticado, PermissionamentoFacade.usuarioAutenticado.Empresa, contratoProduto.CustoContrato, pcmsoDao.pcmsoIsUnidade((Estudo)atendimento.Estudo, dbConnection) ? atendimento.Estudo.ClienteContratante : null, null, DateTime.Now, atendimento.CentroCusto, null, clienteCredenciada), dbConnection);
                            }
                            else
                            {
                                /* O CLIENTE ESTÁ MARCADO PARA NÃO USAR CONTRATO ENTÃO SERÁ
                                 * FEITA A INCLUSÃO DO MOVIMENTO PELO CONTRATO SISTEMA. */

                                Contrato contrato = contratoDao.findContratoSistemaByCliente(clienteCobrado, dbConnection);

                                produtolancamentoAutomatico = produtoDao.findProdutoById(Convert.ToInt64(produtoASoTranscrito), dbConnection);

                                if (contrato != null)
                                    criarMovimentoAsoClienteUsaContratoSistema(atendimento, contrato, produtolancamentoAutomatico, dbConnection);
                                else
                                {
                                    /* CRIANDO CONTRATO PARA O CLIENTE. */
                                    contrato = contratoDao.insert(new Contrato(null, String.Empty, clienteCobrado, PermissionamentoFacade.usuarioAutenticado, null, null, null, ApplicationConstants.CONTRATO_FECHADO, String.Empty, null, null, String.Empty, null, true, null, null, false, false, false, null, false, null, null, null, null, null, false,null, null,null, String.Empty, PermissionamentoFacade.usuarioAutenticado.Empresa), dbConnection);

                                    criarMovimentoAsoClienteUsaContratoSistema(atendimento, contrato, produtolancamentoAutomatico, dbConnection);
                                }
                            }

                            #endregion
                        }
                        else if (inclusaoExame)
                        {
                            #region PRODUTO AUTOMATICO INCLUSAO DE EXAMES

                            /* VERIFICANDO SE O CLIENTE ESTÁ MARCADO PARA USAR CONTRATO */

                            if (clienteCobrado.UsaContrato)
                            {
                                contratoVigente = contratoDao.findContratoVigenteByData((DateTime)atendimento.DataElaboracao, clienteCobrado, null, dbConnection);

                                if (contratoVigente == null)
                                    throw new ContratoException(ContratoException.msg2);
                            
                                /* RECUPERANDO O CONTRATO DO CLIENTE ONDE ESTÁ CADASTRADO OS PRODUTOS QUE SERÃO LANÇADOS */

                                produtolancamentoAutomatico = produtoDao.findProdutoById(Convert.ToInt64(produtoInclusaoExame), dbConnection);
                                
                                /* RECUPERANDO O CONTRATOPRODUTO PARA REALIZAR A INCLUSÃO NO MOVIMENTO */
                                contratoProduto = contratoProdutoDao.findByContratoAndProduto(contratoVigente, produtolancamentoAutomatico, dbConnection);

                                if (contratoProduto == null)
                                    throw new ContratoException("O Produto: " + produtolancamentoAutomatico.Nome + ContratoException.msg4);
                                    
                                movimentoDao.insert(new Movimento(null, null, null, clienteCobrado, contratoProduto, null, DateTime.Now, null, ApplicationConstants.PENDENTE, contratoProduto.PrecoContratado, null, null, atendimento, Convert.ToInt32(1), PermissionamentoFacade.usuarioAutenticado, PermissionamentoFacade.usuarioAutenticado.Empresa, contratoProduto.CustoContrato, pcmsoDao.pcmsoIsUnidade((Estudo)atendimento.Estudo, dbConnection) ? atendimento.Estudo.ClienteContratante : null, null, DateTime.Now, atendimento.CentroCusto, null, clienteCredenciada), dbConnection);
                        
                            }
                            else
                            {
                                /* O CLIENTE ESTÁ MARCADO PARA NÃO USAR CONTRATO ENTÃO SERÁ
                                 * FEITA A INCLUSÃO DO MOVIMENTO PELO CONTRATO SISTEMA. */

                                Contrato contrato = contratoDao.findContratoSistemaByCliente(clienteCobrado, dbConnection);
                                produtolancamentoAutomatico = produtoDao.findProdutoById(Convert.ToInt64(produtoInclusaoExame), dbConnection);

                                if (contrato != null)
                                    criarMovimentoAsoClienteUsaContratoSistema(atendimento, contrato, produtolancamentoAutomatico, dbConnection);
                                else
                                {
                                    /* CRIANDO CONTRATO PARA O CLIENTE. */
                                    contrato = contratoDao.insert(new Contrato(null, String.Empty, clienteCobrado, PermissionamentoFacade.usuarioAutenticado, null, null, null, ApplicationConstants.CONTRATO_FECHADO, String.Empty, null, null, String.Empty, null, true, null, null, false, false, false, null, false, null, null, null, null, null, false, null, null, null, String.Empty, PermissionamentoFacade.usuarioAutenticado.Empresa ), dbConnection);

                                    criarMovimentoAsoClienteUsaContratoSistema(atendimento, contrato, produtolancamentoAutomatico, dbConnection);
                                }

                            }

                            #endregion
                        }
                    }
                    
                    #endregion
                }

                transaction.Commit();
                    
            }
            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - finalizaAso", ex);
                transaction.Rollback();
                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }
            finally
            {
                FabricaConexao.fecharConexao();
            }
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        private void criarMovimentoAsoClienteUsaContratoSistema(Aso aso, Contrato contrato, Produto produto, NpgsqlConnection dbConnection)
        {
            IAsoDao asoDao = FabricaDao.getAsoDao();
            IMovimentoDao movimentoDao = FabricaDao.getMovimentoDao();
            IContratoDao contratoDao = FabricaDao.getContratoDao();
            IProdutoDao produtoDao = FabricaDao.getProdutoDao();
            IContratoExameDao contratoExameDao = FabricaDao.getContratoExameDao();
            IGheFonteAgenteExameDao gheFonteAgenteExameDao = FabricaDao.getGheFonteAgenteExameDao();
            IClienteFuncaoExameDao clienteFuncaoExameDao = FabricaDao.getClienteFuncaoExameDao();
            IClienteFuncaoExameASoDao clienteFuncaoExameAsoDao = FabricaDao.getClienteFuncaoExameASoDao();
            IGheFonteAgenteExameAsoDao gheFonteAgenteExameAsoDao = FabricaDao.getGheFonteAgenteExameAsoDao();
            IClienteDao clienteDao = FabricaDao.getClienteDao();
            IEstudoDao estudoDao = FabricaDao.getEstudoDao();
            IContratoProdutoDao contratoProdutoDao = FabricaDao.getContratoProdutoDao();

            // INCLUINDO OS EXAMES NO MOVIMENTO QUE SÃO DO PCMSO
            Movimento movimento = null;
            Cliente clienteCobrado = null;
            if (aso.ClienteFuncaoFuncionario.ClienteFuncao.Cliente.CredenciadaCliente != null)
            {
                clienteCobrado = aso.ClienteFuncaoFuncionario.ClienteFuncao.Cliente.CredenciadaCliente;
            }
            else
            {
                clienteCobrado = aso.ClienteFuncaoFuncionario.ClienteFuncao.Cliente;
            }

            clienteCobrado = clienteDao.findById((Int64)clienteCobrado.Id, dbConnection);

            /* INCLUINDO EXAMES DO PCMSO */

            /* VERIFICANDO SE EXISTE A NECESSIDADE DE INCLUSÃO DE EXAME OU INCLUIR PRODUTO AUTOMÁTICO */
            if (produto == null)
            {
                foreach (GheFonteAgenteExameAso gheFonteAgenteExameAso in aso.ExamePcmso)
                {
                    if (!gheFonteAgenteExameAso.Cancelado && !gheFonteAgenteExameAso.Transcrito)
                    {

                        /* SOMENTE SERÁ INCLUÍDO NO MOVIMENTO SE O OBJETO EM QUESTÃO NÃO
                        * TIVER SIDO INCLUÍDO POR ALGUM OUTRO MÉTODO DE INCLUSÃO NO MOVIMENTO. */

                        if (!movimentoDao.findMovimentoByGheFonteAgenteExameAso(gheFonteAgenteExameAso, dbConnection))
                        {
                            Exame exame = gheFonteAgenteExameDao.findExameByGheFonteAgenteExame(gheFonteAgenteExameAso.GheFonteAgenteExame, dbConnection);
                            ContratoExame contratoExame = contratoExameDao.findAllByContratoAndExameAndSituacao(contrato, exame, ApplicationConstants.ATIVO, dbConnection);

                            if (contratoExame == null)
                            {
                                // CRIANDO CONTRATO EXAME.
                                contratoExameDao.insert(new ContratoExame(null, exame, contrato,
                                    exame.Preco, PermissionamentoFacade.usuarioAutenticado, exame.Custo, DateTime.Now, ApplicationConstants.ATIVO, true), dbConnection);
                            }
                            else
                            {
                                //ATUALIZANDO VALORES DO CONTRATO COM PREÇO DE TABELA
                                contratoExame.CustoContrato = exame.Custo;
                                contratoExame.PrecoContratado = exame.Preco;
                                contratoExameDao.update(contratoExame, dbConnection);
                            }

                            contratoExame = contratoExameDao.findAllByContratoAndExameAndSituacao(contrato, exame, ApplicationConstants.ATIVO, dbConnection);

                            movimento = new Movimento(null, gheFonteAgenteExameAso, null, clienteCobrado, null, null, DateTime.Now, null, ApplicationConstants.PENDENTE, contratoExame.PrecoContratado, null, null, aso, Convert.ToInt32(1), PermissionamentoFacade.usuarioAutenticado, PermissionamentoFacade.usuarioAutenticado.Empresa, contratoExame.CustoContrato, estudoDao.pcmsoIsUnidade((Estudo)aso.Estudo, dbConnection) ? aso.Estudo.ClienteContratante : null, null, DateTime.Now, aso.CentroCusto, contratoExame, null);

                            movimentoDao.insert(movimento, dbConnection);
                        }
                    }
                }

                /* INCLUINDO OS EXAMES NO MOVIMENTO QUE SÃO DO EXAME EXTRA. */
                foreach (ClienteFuncaoExameASo clienteFuncaoExameAso in aso.ExameExtra)
                {
                    if (!clienteFuncaoExameAso.Cancelado && !clienteFuncaoExameAso.Transcrito)
                    {

                        /* SOMENTE SERÁ INCLUÍDO NO MOVIMENTO SE O OBJETO EM QUESTÃO NÃO
                        * TIVER SIDO INCLUÍDO POR ALGUM OUTRO MÉTODO DE INCLUSÃO NO MOVIMENTO. */

                        if (!movimentoDao.findMovimentoByClienteFuncaoExameAso(clienteFuncaoExameAso, dbConnection))
                        {

                            Exame exame = clienteFuncaoExameDao.findExameByClienteFuncaoExame(clienteFuncaoExameAso.ClienteFuncaoExame, dbConnection);
                            ContratoExame contratoExame = contratoExameDao.findAllByContratoAndExameAndSituacao(contrato, exame, ApplicationConstants.ATIVO, dbConnection);

                            if (contratoExame == null)
                            {
                                // CRIANDO CONTRATO EXAME.
                                contratoExameDao.insert(new ContratoExame(null, exame, contrato,
                                    exame.Preco, PermissionamentoFacade.usuarioAutenticado, exame.Custo, DateTime.Now, ApplicationConstants.ATIVO, true), dbConnection);
                            }
                            else
                            {
                                //ATUALIZANDO VALORES DO CONTRATO COM PREÇO DE TABELA
                                contratoExame.CustoContrato = exame.Custo;
                                contratoExame.PrecoContratado = exame.Preco;
                                contratoExameDao.update(contratoExame, dbConnection);
                            }

                            contratoExame = contratoExameDao.findAllByContratoAndExameAndSituacao(contrato, exame, ApplicationConstants.ATIVO, dbConnection);

                            movimento = new Movimento(null, null, clienteFuncaoExameAso, clienteCobrado, null, null, DateTime.Now, null, ApplicationConstants.PENDENTE, contratoExame.PrecoContratado, null, null, aso, Convert.ToInt32(1), PermissionamentoFacade.usuarioAutenticado, PermissionamentoFacade.usuarioAutenticado.Empresa, contratoExame.CustoContrato, estudoDao.pcmsoIsUnidade((Estudo)aso.Estudo, dbConnection) ? aso.Estudo.ClienteContratante : null, null, DateTime.Now, aso.CentroCusto, contratoExame, null);

                            movimentoDao.insert(movimento, dbConnection);
                        }
                    }
                }
            }
            else
            {

                /* INCLUSÃO DO MOVIMENTO PARA PRODUTOS AUTOMÁTICOS COMO, ASO_TRANSCRITO E ASO_INCLUSAO DE EXAME. */

                ContratoProduto contratoProduto = contratoProdutoDao.findByContratoAndProduto(contrato, produto, dbConnection);

                if (contratoProduto == null)
                {
                    /* CRIANDO CONTRATO PRODUTO. */
                    contratoProdutoDao.insert(new ContratoProduto(null, produto, contrato,
                        produto.Preco, PermissionamentoFacade.usuarioAutenticado, produto.Custo, null, String.Empty, true), dbConnection);
                }
                else
                {
                    /* ATUALIZANDO VALORES DO CONTRATO COM PREÇO DE TABELA */
                    contratoProduto.CustoContrato = produto.Custo;
                    contratoProduto.PrecoContratado = produto.Preco;
                    contratoProdutoDao.update(contratoProduto, dbConnection);
                }

                contratoProduto = contratoProdutoDao.findByContratoAndProduto(contrato, produto, dbConnection);

                movimento = new Movimento(null, null, null, clienteCobrado, contratoProduto, null, DateTime.Now, null, ApplicationConstants.PENDENTE, contratoProduto.PrecoContratado, null, null, aso, Convert.ToInt32(1), PermissionamentoFacade.usuarioAutenticado, PermissionamentoFacade.usuarioAutenticado.Empresa, contratoProduto.CustoContrato, estudoDao.pcmsoIsUnidade((Estudo)aso.Estudo, dbConnection) ? aso.Estudo.ClienteContratante : null, null, DateTime.Now, aso.CentroCusto, null, null);

                movimentoDao.insert(movimento, dbConnection);
            }
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public List<GheFonteAgenteExameAso> findAllExamePcmsoByAsoInSituacao(Aso aso, Boolean? atendimento, Boolean? finalizado, Boolean? cancelado, 
            Boolean? devolvido, Boolean? transcrito)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findAllExamePcmsoByAsoInSituacao");

            NpgsqlConnection dbConnection = FabricaConexao.getConexao();

            IGheFonteAgenteExameAsoDao gheFonteAgenteExameAsoDao = FabricaDao.getGheFonteAgenteExameAsoDao();

            try
            {
                return gheFonteAgenteExameAsoDao.findAllExamesByAsoInSituacao(aso, atendimento, finalizado, cancelado, devolvido, transcrito, dbConnection);

            }
            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findAllExamePcmsoByAsoInSituacao", ex);
                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }
            finally
            {
                FabricaConexao.fecharConexao();
            }
        }


        [MethodImpl(MethodImplOptions.Synchronized)]
        public List<ClienteFuncaoExameASo> findAllExameExtraByAsoInSituacao(Aso aso, Boolean? atendimento,
            Boolean? finalizado, Boolean? cancelado, Boolean? devolvido, Boolean? transcrito)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findAllExameExtraByAsoInSituacao");

            NpgsqlConnection dbConnection = FabricaConexao.getConexao();

            IClienteFuncaoExameASoDao clienteFuncaoExameAsoDao = FabricaDao.getClienteFuncaoExameASoDao();

            try
            {
                return clienteFuncaoExameAsoDao.findAllExamesByAsoInSituacao(aso, atendimento, finalizado, cancelado, devolvido, transcrito, dbConnection);

            }
            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findAllExameExtraByAsoInSituacao", ex);
                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }
            finally
            {
                FabricaConexao.fecharConexao();
            }
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public List<ClienteFuncaoExame> findAllExamesByClienteFuncaoAndIndadeAndPeriodicidade(ClienteFuncao clienteFuncao, Int32 idade, Periodicidade periodicidade)
        {

            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findAllExamesByClienteFuncaoByASO");

            NpgsqlConnection dbConnection = FabricaConexao.getConexao();

            IAsoDao asoDao = FabricaDao.getAsoDao();

            try
            {
                return asoDao.findAllExamesByClienteFuncaoAndIdadeAndPeriodicidade(clienteFuncao, idade, periodicidade, dbConnection);

            }
            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findAllExamesByClienteFuncaoByASO", ex);
                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }
            finally
            {
                FabricaConexao.fecharConexao();
            }
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public LinkedList<Exame> findAllExamesByAso(Aso aso, Exame exame)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findAllExamesByAso");
            NpgsqlConnection dbConnection = FabricaConexao.getConexao();
            IClienteFuncaoExameASoDao clienteFuncaoExameAsoDao = FabricaDao.getClienteFuncaoExameASoDao();
            IGheFonteAgenteExameAsoDao gheFonteAgenteExameAsoDao = FabricaDao.getGheFonteAgenteExameAsoDao();
            IExameDao exameDao = FabricaDao.getExameDao();

            HashSet<Int64> exameInAsoRetornoByGheFonteAgenteExameASO = new HashSet<Int64>();
            HashSet<Int64> exameInAsoRetornoByClienteFuncaoExameASO = new HashSet<Int64>();
            HashSet<Int64> exameInAsoSomatorio = new HashSet<Int64>();

            LinkedList<Exame> exameInAsoRetorno = new LinkedList<Exame>();

            try
            {
                // encontrando os exames que já foram incluídos no GheFonteAgenteExameAso
                exameInAsoRetornoByGheFonteAgenteExameASO = gheFonteAgenteExameAsoDao.findAllExameInAso(aso, dbConnection);

                // encontrando os exames que não foram incluídos no ClienteFuncaoExameASO
                exameInAsoRetornoByClienteFuncaoExameASO = clienteFuncaoExameAsoDao.findAllExameInAso(aso, dbConnection);

                // fazendo a juncao das listas.
                foreach (Int64 id in exameInAsoRetornoByGheFonteAgenteExameASO)
                {
                    exameInAsoSomatorio.Add(id);
                }

                foreach (Int64 id in exameInAsoRetornoByClienteFuncaoExameASO)
                {
                    exameInAsoSomatorio.Add(id);
                }

                exameInAsoRetorno = exameDao.findAtivoNotInAso(exameInAsoSomatorio, exame, dbConnection);

                return exameInAsoRetorno;

            }
            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findAllExamesByAso", ex);
                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }
            finally
            {
                FabricaConexao.fecharConexao();
            }
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public void insertExamesInASoPosGravacao(LinkedList<Exame> exameInsertAso, Aso aso)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findAllExamesByAso");
            NpgsqlConnection dbConnection = FabricaConexao.getConexao();
            NpgsqlTransaction transaction = dbConnection.BeginTransaction();

            IClienteFuncaoExameDao clienteFuncaoExameDao = FabricaDao.getClienteFuncaoExameDao();
            IClienteFuncaoExameASoDao clienteFuncaoExameAsoDao = FabricaDao.getClienteFuncaoExameASoDao();
            IClienteFuncaoExamePeriodicidadeDao clienteFuncaoExamePeriodicidadeDao = FabricaDao.getClienteFuncaoExamePeriodicidadeDao();

            HashSet<ClienteFuncaoExame> clienteFuncaoExameSet = new HashSet<ClienteFuncaoExame>();
            
            try
            {
                // inserindo o clienteFuncaoExame com status desativado para permitir apenas esse exame para esse aso
                foreach (Exame exame in exameInsertAso)
                {
                    ClienteFuncaoExame clienteFuncaoExame = clienteFuncaoExameDao.insert(new ClienteFuncaoExame(null, aso.ClienteFuncaoFuncionario.ClienteFuncao, exame, ApplicationConstants.DESATIVADO, 0), dbConnection);
                    clienteFuncaoExameSet.Add(clienteFuncaoExame);
                    clienteFuncaoExameDao.disable(clienteFuncaoExame, dbConnection);
                }

                // inserindo o cliente_Funcao_Exame_Periodicidade
                foreach (ClienteFuncaoExame clienteFuncaoExame in clienteFuncaoExameSet)
                    clienteFuncaoExamePeriodicidadeDao.insert(new ClienteFuncaoExamePeriodicidade(clienteFuncaoExame, aso.Periodicidade, String.Empty, null), dbConnection);
                
                // inserindo o clienteFuncaoExameAso com status normal para esse exame.
                foreach (ClienteFuncaoExame clienteFuncaoExame in clienteFuncaoExameSet)
                    clienteFuncaoExameAsoDao.insert(new ClienteFuncaoExameASo(null, null, aso, clienteFuncaoExame, DateTime.Now, false, false, null, false, null, null, null, null, false, null, null, null, false, null,null, null,null, true), dbConnection);
                
                transaction.Commit();
            }
            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - insertExamesInASoPosGravacao", ex);
                transaction.Rollback();
                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }
            finally
            {
                FabricaConexao.fecharConexao();
            }
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public void alteracaoAso(Aso aso, List<GheFonteAgenteExameAso> examePcmso, 
            List<ClienteFuncaoExameASo> exameExtra)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método alteracaoAso");
            NpgsqlConnection dbConnection = FabricaConexao.getConexao();
            NpgsqlTransaction transaction = dbConnection.BeginTransaction();

            IClienteFuncaoExameASoDao clienteFuncaoExameAsoDao = FabricaDao.getClienteFuncaoExameASoDao();
            IGheFonteAgenteExameAsoDao gheFonteAgenteExameAsoDao = FabricaDao.getGheFonteAgenteExameAsoDao();
            IAsoDao asoDao = FabricaDao.getAsoDao();

            try
            {
                // alterando o aso 
                asoDao.update(aso, dbConnection);

                // verificando as colecoes e alterando os dados

                if (examePcmso.Count > 0)
                {
                    foreach (GheFonteAgenteExameAso gfaeaDao in examePcmso)
                    {
                        gheFonteAgenteExameAsoDao.alteraGheFonteAgenteExameAso(gfaeaDao, dbConnection);
                    }
                }

                if (exameExtra.Count > 0)
                {
                    foreach (ClienteFuncaoExameASo cfeaDao in exameExtra)
                    {
                        clienteFuncaoExameAsoDao.alteraClienteFuncaoExameAso(cfeaDao, dbConnection);
                    }
                }

                transaction.Commit();
            }
            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - alteracaoAso", ex);
                transaction.Rollback();
                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }
            finally
            {
                FabricaConexao.fecharConexao();
            }
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public Prontuario findProntuarioById(Int64 id)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findProntuarioById");
            NpgsqlConnection dbConnection = FabricaConexao.getConexao();
            IProntuarioDao prontuarioDao = FabricaDao.getProntuarioDao();
            IAsoDao asoDao = FabricaDao.getAsoDao();
            IUsuarioDao usuarioDao = FabricaDao.getUsuarioDao();

            Prontuario prontuarioRetorno = null;

            try
            {
                prontuarioRetorno = prontuarioDao.findProntuarioById(id, dbConnection);

                if (prontuarioRetorno != null)
                {
                    FabricaConexao.fecharConexao();
                    prontuarioRetorno.Aso = findAtendimentoById((Int64)prontuarioRetorno.Aso.Id);
                    dbConnection.Open();
                    prontuarioRetorno.Usuario = usuarioDao.findById((Int64)prontuarioRetorno.Usuario.Id, dbConnection);
                }

                return prontuarioRetorno;
            }
            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findProntuarioById", ex);
                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }
            finally
            {
                FabricaConexao.fecharConexao();
            }
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public Prontuario updateProntuario(Prontuario prontuario)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método updateProntuario");
            NpgsqlConnection dbConnection = FabricaConexao.getConexao();
            IProntuarioDao prontuarioDao = FabricaDao.getProntuarioDao();

            Prontuario prontuarioRetorno = null;

            try
            {
                prontuarioRetorno = prontuarioDao.updateProntuario(prontuario, dbConnection);

                return prontuarioRetorno;
            }
            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - updateProntuario", ex);
                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }
            finally
            {
                FabricaConexao.fecharConexao();
            }
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public Prontuario insertProntuario(Prontuario prontuario)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método insertProntuario");
            NpgsqlConnection dbConnection = FabricaConexao.getConexao();
            IProntuarioDao prontuarioDao = FabricaDao.getProntuarioDao();

            try
            {

                Prontuario prontuarioNew = prontuarioDao.insertProntuario(prontuario, dbConnection);

                return prontuarioNew;
            }
            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - insertProntuario", ex);
                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }
            finally
            {
                FabricaConexao.fecharConexao();
            }
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public Prontuario findProntuarioByAso(Aso aso)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findProntuarioByAso");
            NpgsqlConnection dbConnection = FabricaConexao.getConexao();
            IProntuarioDao prontuarioDao = FabricaDao.getProntuarioDao();
            IUsuarioDao usuarioDao = FabricaDao.getUsuarioDao();

            Prontuario prontuarioRetorno = null;

            try
            {
                prontuarioRetorno = prontuarioDao.findProntuarioByAso(aso, dbConnection);

                if (prontuarioRetorno != null)
                {
                    FabricaConexao.fecharConexao();
                    prontuarioRetorno.Aso = findAtendimentoById((Int64)prontuarioRetorno.Aso.Id);
                    dbConnection.Open();
                    prontuarioRetorno.Usuario = usuarioDao.findById((Int64)prontuarioRetorno.Usuario.Id, dbConnection);
                }
                
                return prontuarioRetorno;
            }
            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findProntuarioByAso", ex);
                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }
            finally
            {
                FabricaConexao.fecharConexao();
            }
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        internal void updateGheFonteAgenteExameAsoLaudoAso(GheFonteAgenteExameAso gfaea)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método updateGheFonteAgenteExameAsoLaudoAso");
            NpgsqlConnection dbConnection = FabricaConexao.getConexao();
            NpgsqlTransaction transaction = dbConnection.BeginTransaction();

            IGheFonteAgenteExameAsoDao gheFonteAgenteExameAsoDao = FabricaDao.getGheFonteAgenteExameAsoDao();

            try
            {
               /* alterando o gheFonteAgenteExameAso com as informações do laudo. */

               gheFonteAgenteExameAsoDao.updateInformacaoLaudo(gfaea, dbConnection);
               
               transaction.Commit();
            }
            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - alteracaoAso", ex);
                transaction.Rollback();
                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }
            finally
            {
                FabricaConexao.fecharConexao();
            }
        }

        internal void updateClienteFuncaoExameAsoLaudoAso(ClienteFuncaoExameASo cfea)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método updateClienteFuncaoExameAsoLaudoAso");
            NpgsqlConnection dbConnection = FabricaConexao.getConexao();
            NpgsqlTransaction transaction = dbConnection.BeginTransaction();

            IClienteFuncaoExameASoDao clienteFuncaoExameAsoDao = FabricaDao.getClienteFuncaoExameASoDao();

            try
            {
                /* alterando o clienteFuncaoExameAsoDao com as informações do laudo. */

                clienteFuncaoExameAsoDao.updateInformacaoLaudo(cfea, dbConnection);

                transaction.Commit();
            }
            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - updateClienteFuncaoExameAsoLaudoAso", ex);
                transaction.Rollback();
                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }
            finally
            {
                FabricaConexao.fecharConexao();
            }
        }

        internal void updateLaudoAtendimento(Aso atendimento)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método updateLaudoAtendimento");
            NpgsqlConnection dbConnection = FabricaConexao.getConexao();
            NpgsqlTransaction transaction = dbConnection.BeginTransaction();

            IAsoDao asoDao = FabricaDao.getAsoDao();

            try
            {
                asoDao.updateInformacaoLaudo(atendimento, dbConnection);

                transaction.Commit();
            }
            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - updateLaudoAtendimento", ex);
                transaction.Rollback();
                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }
            finally
            {
                FabricaConexao.fecharConexao();
            }
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public List<String> findDescricaoSetoresByAso(Aso aso)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findDescricaoSetoresByAso");
            NpgsqlConnection dbConnection = FabricaConexao.getConexao();
            IAsoGheSetorDao asoGheSetorDao = FabricaDao.getAsoGheSetorDao();

            List<String> descricaoSetores = new List<String>();

            try
            {
                descricaoSetores = asoGheSetorDao.findDescricaoSetoresByAso(aso, dbConnection);

                return descricaoSetores;
            }
            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findDescricaoSetoresByAso", ex);
                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }
            finally
            {
                FabricaConexao.fecharConexao();
            }
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public HashSet<SalaExame> buscaSalasAtendemExame(Exame exame)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método buscaSalasAtendemExame");
            NpgsqlConnection dbConnection = FabricaConexao.getConexao();
            ISalaExameDao salaExameDao = FabricaDao.getSalaExameDao();

            HashSet<SalaExame> salaExame = new HashSet<SalaExame>();

            try
            {
                salaExame = salaExameDao.buscaSalasAtendemExame(exame, false, dbConnection);

                return salaExame;
            }
            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - buscaSalasAtendemExame", ex);
                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }
            finally
            {
                FabricaConexao.fecharConexao();
            }
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public String findSenhaByAso(Aso aso)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findSenhaByAso");
            NpgsqlConnection dbConnection = FabricaConexao.getConexao();
            IAsoDao asoDao = FabricaDao.getAsoDao();

            String senha = String.Empty;

            try
            {
                senha = asoDao.findSenhaByAso(aso, dbConnection);

                return senha;
            }
            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findSenhaByAso", ex);
                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }
            finally
            {
                FabricaConexao.fecharConexao();
            }
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public Boolean verificaExisteAtendimentoNoMesmoDia(Cliente cliente, Funcionario funcionario, Periodicidade periodicidade)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método verificaExisteAtendimentoNoMesmoDia");
            NpgsqlConnection dbConnection = FabricaConexao.getConexao();
            IAsoDao asoDao = FabricaDao.getAsoDao();

            try
            {
                return asoDao.verificaExisteAtendimentoNoMesmoDia(cliente, funcionario, periodicidade, dbConnection);
            }
            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - verificaExisteAtendimentoNoMesmoDia", ex);
                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }
            finally
            {
                FabricaConexao.fecharConexao();
            }
        }
        
        [MethodImpl(MethodImplOptions.Synchronized)]
        public void impressaoImpressoraTermica(Aso aso, String numeroVias)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método impressaoImpressoraTermica");

            try
            {
                string printingTaskFileName = Path.GetTempFileName(); // file in %temp%
                String nomeEmpresa = String.Empty;
                String nomeImpressoraTermica = String.Empty;
                String servidorImpressoraTermica = String.Empty;
                String qtdVias = numeroVias;
                string somenteSala = string.Empty;
                HashSet<SalaExame> salaExame = new HashSet<SalaExame>();

                PermissionamentoFacade.mapaConfiguracoes.TryGetValue(ConfigurationConstans.EMPRESA_IMPRESSORA_TERMICA, out nomeEmpresa);
                PermissionamentoFacade.mapaConfiguracoes.TryGetValue(ConfigurationConstans.NOME_IMPRESSORA_TERMICA, out nomeImpressoraTermica);
                PermissionamentoFacade.mapaConfiguracoes.TryGetValue(ConfigurationConstans.SERVIDOR_IMPRESSORA_TERMICA, out servidorImpressoraTermica);
                PermissionamentoFacade.mapaConfiguracoes.TryGetValue(ConfigurationConstans.IMPRIME_SALA, out somenteSala);

                System.IO.FileStream printingTaskFile;
                System.IO.StreamWriter printingTaskStream;

                for (int i = 1; i <= Convert.ToInt32(qtdVias); i++)
                {

                    printingTaskFile = new System.IO.FileStream(printingTaskFileName, FileMode.Append);
                    printingTaskStream = new System.IO.StreamWriter(printingTaskFile, System.Text.Encoding.Default);

                    printingTaskStream.Write("-------------------------------");
                    printingTaskStream.Write(Environment.NewLine);
                    printingTaskStream.Write(EncodingHelper.RemoverAcentos("GUIA DE ENCAMINHAMENTO DE EXAMES"));
                    printingTaskStream.Write(Environment.NewLine);
                    printingTaskStream.Write(EncodingHelper.RemoverAcentos("EMPRESA: " + nomeEmpresa));
                    printingTaskStream.Write(Environment.NewLine);
                    printingTaskStream.Write(EncodingHelper.RemoverAcentos("EMISSÃO: " + aso.DataElaboracao));
                    printingTaskStream.Write(Environment.NewLine);
                    printingTaskStream.Write(EncodingHelper.RemoverAcentos("ATENDIMENTO: " + aso.Codigo));
                    printingTaskStream.Write(Environment.NewLine);
                    printingTaskStream.Write(Environment.NewLine);
                    printingTaskStream.Write(EncodingHelper.RemoverAcentos("NOME...: " + aso.Funcionario.Nome));
                    printingTaskStream.Write(Environment.NewLine);
                    printingTaskStream.Write(EncodingHelper.RemoverAcentos("RG.....: " + aso.Funcionario.Rg));
                    printingTaskStream.Write(Environment.NewLine);
                    printingTaskStream.Write(EncodingHelper.RemoverAcentos("DTNAS..: " + aso.Funcionario.DataNascimento.ToString("dd/MM/yyyy")));
                    printingTaskStream.Write(Environment.NewLine);
                    printingTaskStream.Write(EncodingHelper.RemoverAcentos("CARGO..: " + aso.ClienteFuncaoFuncionario.ClienteFuncao.Funcao.Descricao));
                    printingTaskStream.Write(Environment.NewLine);

                    StringBuilder setores = new StringBuilder();

                    foreach (String setor in asoFacade.findDescricaoSetoresByAso(aso))
                        setores.Append(setor + " ");

                    printingTaskStream.Write(EncodingHelper.RemoverAcentos("SETOR..: " + setores.ToString()));
                    printingTaskStream.Write(Environment.NewLine);

                    printingTaskStream.Write(EncodingHelper.RemoverAcentos("EMPRESA: " + aso.Cliente.RazaoSocial));
                    printingTaskStream.Write(Environment.NewLine);
                    printingTaskStream.Write(Environment.NewLine);
                    printingTaskStream.Write(EncodingHelper.RemoverAcentos("PERIODICIDADE: " + aso.Periodicidade.Descricao));
                    printingTaskStream.Write(Environment.NewLine);
                    printingTaskStream.Write(Environment.NewLine);
                    printingTaskStream.Write(EncodingHelper.RemoverAcentos("EXAMES:"));
                    printingTaskStream.Write(Environment.NewLine);
                    printingTaskStream.Write(Environment.NewLine);

                    String sala = null;
                    String andar = null;


                    List<SalaExameImpressao> exames = new List<SalaExameImpressao>();


                    foreach (GheFonteAgenteExameAso gheFonteAgenteExameAso in aso.ExamePcmso)
                    {
                        if (!gheFonteAgenteExameAso.Transcrito && !gheFonteAgenteExameAso.GheFonteAgenteExame.Exame.Externo)
                        {
                            /* zerando a colecao SalaExame */
                            salaExame.Clear();
                            salaExame = asoFacade.buscaSalasAtendemExame(gheFonteAgenteExameAso.GheFonteAgenteExame.Exame);

                            if (salaExame.Count > 0)
                            {
                                sala = salaExame.First().Sala.Descricao;
                                andar = asoFacade.buscaSalasAtendemExame(gheFonteAgenteExameAso.GheFonteAgenteExame.Exame).First().Sala.Andar.ToString();
                            }
                            else
                            {
                                sala = "--";
                                andar = "--";
                            }
                            
                            exames.Add(new SalaExameImpressao(sala, gheFonteAgenteExameAso.GheFonteAgenteExame.Exame.Descricao));
                        }
                    }
                    
                    foreach (ClienteFuncaoExameASo clienteFuncaoExameAso in aso.ExameExtra)
                    {
                        if (!clienteFuncaoExameAso.Transcrito && !clienteFuncaoExameAso.ClienteFuncaoExame.Exame.Externo)
                        {
                            /* zerando a colecao SalaExame */
                            salaExame.Clear();
                            salaExame = asoFacade.buscaSalasAtendemExame(clienteFuncaoExameAso.ClienteFuncaoExame.Exame);
                            
                            if (salaExame.Count > 0)
                            {
                                sala = salaExame.First().Sala.Descricao;
                                andar = asoFacade.buscaSalasAtendemExame(clienteFuncaoExameAso.ClienteFuncaoExame.Exame).First().Sala.Andar.ToString();
                            }
                            else
                            {
                                sala = "--";
                                andar = "--";
                            }

                            exames.Add(new SalaExameImpressao(sala, clienteFuncaoExameAso.ClienteFuncaoExame.Exame.Descricao));

                        }
                    }
                    
                    /*
                    if (Convert.ToBoolean(somenteSala))
                    {
                        salaExame.Clear();
                        HashSet<SalaExame> salas = new HashSet<SalaExame>();
                    
                        aso.ExamePcmso.ForEach(delegate(GheFonteAgenteExameAso gheFonteAgenteExameAso)
                        {
                            salaExame = asoFacade.buscaSalasAtendemExame(gheFonteAgenteExameAso.GheFonteAgenteExame.Exame);
                            foreach (SalaExame se in salaExame)
                                salas.Add(se);

                        });

                        aso.ExameExtra.ForEach(delegate(ClienteFuncaoExameASo clienteFuncaoExameAso)
                        {
                            salaExame = asoFacade.buscaSalasAtendemExame(clienteFuncaoExameAso.ClienteFuncaoExame.Exame);
                            foreach (SalaExame se in salaExame)
                                salas.Add(se);
                        });

                        foreach (SalaExame salasNoExame in salas)
                        {
                            printingTaskStream.Write(EncodingHelper.RemoverAcentos(salasNoExame.Sala.Descricao));
                            printingTaskStream.Write(Environment.NewLine);
                        }
                    
                    }
                    */

                    exames = exames.OrderBy(x => x.Sala).ThenBy(x => x.Exame).ToList();

                    exames.ForEach(delegate (SalaExameImpressao salaExameImprimir) {
                        printingTaskStream.Write(EncodingHelper.RemoverAcentos(salaExameImprimir.Exame + " (" + salaExameImprimir.Sala + ")"));
                        printingTaskStream.Write(Environment.NewLine);
                    });

                    printingTaskStream.Write(Environment.NewLine);
                    printingTaskStream.Write(EncodingHelper.RemoverAcentos("EXAME(S) EXTERNO(S):"));
                    printingTaskStream.Write(Environment.NewLine);
                    printingTaskStream.Write(Environment.NewLine);

                    foreach (GheFonteAgenteExameAso gheFonteAgenteExameAso in aso.ExamePcmso)
                    {
                        if (gheFonteAgenteExameAso.GheFonteAgenteExame.Exame.Externo)
                        {
                            // mudança para imprimir apenas o nome do exame. 
                            printingTaskStream.Write(EncodingHelper.RemoverAcentos(gheFonteAgenteExameAso.GheFonteAgenteExame.Exame.Descricao));
                            printingTaskStream.Write(Environment.NewLine);
                        }
                    }

                    foreach (ClienteFuncaoExameASo clienteFuncaoExameAso in aso.ExameExtra)
                    {
                        if (clienteFuncaoExameAso.ClienteFuncaoExame.Exame.Externo)
                        {
                            // mudança para imprimir apenas o nome do exame. 
                            printingTaskStream.Write(EncodingHelper.RemoverAcentos(clienteFuncaoExameAso.ClienteFuncaoExame.Exame.Descricao));
                            printingTaskStream.Write(Environment.NewLine);
                        }
                    }

                    printingTaskStream.Write(Environment.NewLine);
                    printingTaskStream.Write(EncodingHelper.RemoverAcentos("OPERADOR: " + aso.Criador.Nome));
                    printingTaskStream.Write(Environment.NewLine);
                    printingTaskStream.Write(EncodingHelper.RemoverAcentos("VIA: "+i) +"  SENHA: "+this.findSenhaByAso(aso));
                    printingTaskStream.Write(Environment.NewLine);
                    printingTaskStream.Write("-------------------------------");

                    printingTaskStream.Flush();
                    printingTaskStream.Close();

                    File.Copy(printingTaskFileName, @"\\" + servidorImpressoraTermica + @"\" + nomeImpressoraTermica, true); 
                    // also can be \\127.0.0.1\PNT5 or smth like that
                    //File.Copy(printingTaskFileName, "ENCAMINHAMENTO.text", true);
                    File.Delete(printingTaskFileName);
                }
            }
            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - impressaoImpressoraTermica", ex);
                throw new Exception("Erro ao imprimir encaminhamento. Entre em contato com o administrador.", ex);
            }
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public List<Exame> FindAllExamesByAsoBySituacao(Aso aso, Boolean? atendido, 
            Boolean?finalizado, Boolean?cancelado, Boolean? devolvido, Boolean? transcrito )
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método FindAllExamesByAsoBySituacao");

            NpgsqlConnection dbConnection = FabricaConexao.getConexao();

            IAsoDao asoDao = FabricaDao.getAsoDao();

            try
            {
                return asoDao.FindAllExamesByAsoBySituacao(aso, atendido, finalizado, cancelado, devolvido, transcrito, dbConnection);

            }
            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - verificaAsoInclusaoExame", ex);
                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }
            finally
            {
                FabricaConexao.fecharConexao();
            }

        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public void gera2Via(Aso aso)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método gera2Via");
            
            NpgsqlConnection dbConnection = FabricaConexao.getConexao();
            NpgsqlTransaction transaction = dbConnection.BeginTransaction();

            IMovimentoDao movimentoDao = FabricaDao.getMovimentoDao();
            IContratoDao contratoDao = FabricaDao.getContratoDao();
            IProdutoDao produtoDao = FabricaDao.getProdutoDao();
            IContratoProdutoDao contratoProdutoDao = FabricaDao.getContratoProdutoDao();
            IClienteDao clienteDao = FabricaDao.getClienteDao();
            IEstudoDao pcmsoDao = FabricaDao.getEstudoDao();
            String produto = String.Empty;
            Cliente cliente = null;

            
            
            try
            {
                /* recuperando informaçao do produto */
                PermissionamentoFacade.mapaConfiguracoes.TryGetValue(ConfigurationConstans.ASO_2_VIA, out produto);

                Produto produto2Via = produtoDao.findProdutoById(Convert.ToInt64(produto), dbConnection);

                /* verificando se o cliente é uma empresa credenciada */

                cliente = aso.ClienteFuncaoFuncionario.ClienteFuncao.Cliente.CredenciadaCliente != null ? 
                    aso.ClienteFuncaoFuncionario.ClienteFuncao.Cliente.CredenciadaCliente : 
                    aso.ClienteFuncaoFuncionario.ClienteFuncao.Cliente;
                
                cliente = clienteDao.findById((Int64)cliente.Id, dbConnection);

                /* verificando se o cliente está marcado para usar contrato */

                if (cliente.UsaContrato)
                {
                    Contrato contratoVigente = contratoDao.findContratoVigenteByData((DateTime)aso.DataElaboracao, cliente, null, dbConnection);

                    if (contratoVigente != null)
                    {
                        /* recuperando o contratoProduto para realizar a inclusão no movimento */

                        ContratoProduto contratoProduto = contratoProdutoDao.findByContratoAndProduto(contratoVigente, produto2Via, dbConnection);

                        if (contratoProduto != null)
                        {
                            Movimento movimento = new Movimento(null, null, null, cliente, contratoProduto, null, DateTime.Now, null, ApplicationConstants.PENDENTE, contratoProduto.PrecoContratado, null, null, aso, Convert.ToInt32(1), PermissionamentoFacade.usuarioAutenticado, PermissionamentoFacade.usuarioAutenticado.Empresa, contratoProduto.CustoContrato, pcmsoDao.pcmsoIsUnidade((Estudo)aso.Estudo, dbConnection) ? aso.Estudo.ClienteContratante : null, null, DateTime.Now, aso.CentroCusto, null, null);

                            movimentoDao.insert(movimento, dbConnection);
                        }
                        else
                        {
                            throw new ContratoException("O Produto: " + produto2Via.Nome + ContratoException.msg4);
                        }
                    }
                    else
                    {
                        throw new ContratoException(ContratoException.msg2);
                    }
                }
                else
                {
                    /* nesse caso o cliente não usa contrato e será feita a inclusão no movimento com o 
                     * contrato sistema. */

                    Contrato contratoSistema = contratoDao.findContratoSistemaByCliente(cliente, dbConnection);

                    if (contratoSistema != null)
                    {
                        criarMovimentoAsoClienteUsaContratoSistema(aso, contratoSistema, produto2Via, dbConnection);
                    }
                    else
                    {
                        /* criando contrato para o cliente. */
                        contratoSistema = contratoDao.insert(new Contrato(null, String.Empty, cliente,
                            PermissionamentoFacade.usuarioAutenticado, null, null, null, ApplicationConstants.CONTRATO_FECHADO, String.Empty, null, null, String.Empty, null, true, null, null, false, false, false, null, false, null, null, null, null, null, false, null, null,null, String.Empty, PermissionamentoFacade.usuarioAutenticado.Empresa), dbConnection);

                        criarMovimentoAsoClienteUsaContratoSistema(aso, contratoSistema, produto2Via, dbConnection);
                    }

                }

                transaction.Commit();
            }
            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - gera2Via", ex);
                transaction.Rollback();
                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }
            finally
            {
                FabricaConexao.fecharConexao();
            }
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public Boolean findAsoInProtocolo(Aso atendimento)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findAsoInProtocolo");

            NpgsqlConnection dbConnection = FabricaConexao.getConexao();
            
            IAsoDao asoDao = FabricaDao.getAsoDao();
            
            try
            {
                return asoDao.findAsoInProtocolo(atendimento, dbConnection);

            }
            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findAsoInProtocolo", ex);
                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }
            finally
            {
                FabricaConexao.fecharConexao();
            }

        }


        [MethodImpl(MethodImplOptions.Synchronized)]
        public Boolean verificaAtendimentoUnidade(Cliente unidade)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método verificaAtendimentoUnidade");

            NpgsqlConnection dbConnection = FabricaConexao.getConexao();

            IAsoDao asoDao = FabricaDao.getAsoDao();

            try
            {
                return asoDao.verificaAtendimentoUnidade(unidade, dbConnection);

            }
            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - verificaAtendimentoUnidade", ex);
                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }
            finally
            {
                FabricaConexao.fecharConexao();
            }

        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public Prontuario findLastProntuarioByFuncionario(Funcionario funcionario)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findLastProntuarioByFuncionario");

            NpgsqlConnection dbConnection = FabricaConexao.getConexao();

            IProntuarioDao prontuarioDao = FabricaDao.getProntuarioDao();
            IUsuarioDao usuarioDao = FabricaDao.getUsuarioDao();
            
            Prontuario prontuario = null;

            try
            {
                prontuario = prontuarioDao.findLastProntuarioByFuncionario(funcionario, dbConnection);

                if (prontuario != null)
                {
                    FabricaConexao.fecharConexao();
                    prontuario.Aso = findAtendimentoById((Int64)prontuario.Aso.Id);
                    dbConnection.Open();
                    prontuario.Usuario = usuarioDao.findById((Int64)prontuario.Usuario.Id, dbConnection);
                }

                return prontuario;

            }
            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findLastProntuarioByFuncionario", ex);
                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }
            finally
            {
                FabricaConexao.fecharConexao();
            }

        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public ArquivoAnexo insertArquivoAnexo(ArquivoAnexo arquivoAnexo)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método insertArquivoAnexo");
            NpgsqlConnection dbConnection = FabricaConexao.getConexao();
            NpgsqlTransaction transaction = dbConnection.BeginTransaction();
            IArquivoAnexoDao arquivoAnexoDao = FabricaDao.getArquivoAnexoDao();
            ArquivoAnexo arquivoAnexoRetorno = null;

            try
            {
                arquivoAnexoRetorno = arquivoAnexoDao.insertArquivoAnexo(arquivoAnexo, dbConnection);

                transaction.Commit();

                return arquivoAnexoRetorno;
            }
            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - insertArquivoAnexo", ex);
                transaction.Rollback();
                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }
            finally
            {
                FabricaConexao.fecharConexao();
            }
        }

        internal DataSet findArquivoAnexoByAso(Aso aso)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findArquivoAnexoByAso");

            NpgsqlConnection dbConnection = FabricaConexao.getConexao();

            IArquivoAnexoDao arquivoAnexoDao = FabricaDao.getArquivoAnexoDao();

            try
            {
                return arquivoAnexoDao.findAllArquivoAnexoByAso(aso, dbConnection);

            }
            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findArquivoAnexoByAso", ex);
                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }
            finally
            {
                FabricaConexao.fecharConexao();
            }
        }

        public ArquivoAnexo findArquivoAnexoById(Int64 idArquivoAnexo)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findArquivoAnexoById");
            NpgsqlConnection dbConnection = FabricaConexao.getConexao();
            IArquivoAnexoDao arquivoAnexoDao = FabricaDao.getArquivoAnexoDao();

            try
            {
                return arquivoAnexoDao.findArquivoAnexoById(idArquivoAnexo, dbConnection);
            }
            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findArquivoAnexoById", ex);
                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }
            finally
            {
                FabricaConexao.fecharConexao();
            }
        }

        internal void deleteArquivoAnexo(ArquivoAnexo arquivoAnexo)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método deleteArquivoAnexo");

            NpgsqlConnection dbConnection = FabricaConexao.getConexao();
            IArquivoAnexoDao arquivoAnexoDao = FabricaDao.getArquivoAnexoDao();
            NpgsqlTransaction transaction = dbConnection.BeginTransaction();

            try
            {

                arquivoAnexoDao.delete(arquivoAnexo, dbConnection);
                
                transaction.Commit();

            }
            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - deleteArquivoAnexo", ex);
                transaction.Rollback();
                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }
            finally
            {
                FabricaConexao.fecharConexao();
            }
        }

        public GheFonteAgenteExameAso findGheFonteAgenteExameAsoById(Int64 id)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findGheFonteAgenteExameAsoById");

            NpgsqlConnection dbConnection = FabricaConexao.getConexao();

            IGheFonteAgenteExameAsoDao gheFonteAgenteExameAsoDao = FabricaDao.getGheFonteAgenteExameAsoDao();
            IAsoDao asoDao = FabricaDao.getAsoDao();
            IClienteFuncaoFuncionarioDao clienteFuncaoFuncionarioDao = FabricaDao.getClienteFuncaoFuncionarioDao();
            IGheFonteAgenteExameDao gheFonteAgenteExameDao = FabricaDao.getGheFonteAgenteExameDao();
            GheFonteAgenteExameAso gfaea = null;

            try
            {

                gfaea = gheFonteAgenteExameAsoDao.findById(id, dbConnection);

                /* preenchendo informação do GheFonteAgenteExameAso */
                /* preenchendo informação do aso */
                gfaea.Aso = asoDao.findById((Int64)gfaea.Aso.Id, dbConnection);

                /* preenchendo informação do clienteFuncaoFuncionario no aso */
                gfaea.Aso.ClienteFuncaoFuncionario = clienteFuncaoFuncionarioDao.findById((Int64)gfaea.Aso.ClienteFuncaoFuncionario.Id, dbConnection);

                /* preenchendo informação do GheFonteAgenteExame */
                gfaea.GheFonteAgenteExame = gheFonteAgenteExameDao.findById((Int64)gfaea.GheFonteAgenteExame.Id, dbConnection);

                return gfaea;
                

            }
            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findGheFonteAgenteExameAsoById", ex);
                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }
            finally
            {
                FabricaConexao.fecharConexao();
            }
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public void updateGheFonteAgenteExameAso(GheFonteAgenteExameAso gfaea)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método updateGheFonteAgenteExameAso");
            NpgsqlConnection dbConnection = FabricaConexao.getConexao();
            NpgsqlTransaction transaction = dbConnection.BeginTransaction();

            IGheFonteAgenteExameAsoDao gheFonteAgenteExameAsoDao = FabricaDao.getGheFonteAgenteExameAsoDao();

            try
            {

                gheFonteAgenteExameAsoDao.alteraGheFonteAgenteExameAso(gfaea, dbConnection);
                transaction.Commit();
            }
            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - updateGheFonteAgenteExameAso", ex);
                transaction.Rollback();
                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }
            finally
            {
                FabricaConexao.fecharConexao();
            }
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public void updateClienteFuncaoExameAso(ClienteFuncaoExameASo cfea)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método updateClienteFuncaoExameAso");
            NpgsqlConnection dbConnection = FabricaConexao.getConexao();
            NpgsqlTransaction transaction = dbConnection.BeginTransaction();

            IClienteFuncaoExameASoDao clienteFuncaoExameAsoDao = FabricaDao.getClienteFuncaoExameASoDao();

            try
            {

                clienteFuncaoExameAsoDao.alteraClienteFuncaoExameAso(cfea, dbConnection);
                transaction.Commit();
            }
            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - updateClienteFuncaoExameAso", ex);
                transaction.Rollback();
                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }
            finally
            {
                FabricaConexao.fecharConexao();
            }
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public Email insertEmail(Email email)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método insertEmail");
            NpgsqlConnection dbConnection = FabricaConexao.getConexao();
            NpgsqlTransaction transaction = dbConnection.BeginTransaction();

            IEmailDao emailDao = FabricaDao.getEmailDao();

            try
            {
                Email newEmail = emailDao.insert(email, dbConnection);
                transaction.Commit();

                return newEmail;
            }
            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - insertEmail", ex);
                transaction.Rollback();
                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }
            finally
            {
                FabricaConexao.fecharConexao();
            }
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public List<Email> findEmailByProntuario(Prontuario prontuario)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findEmailByProntuario");
            NpgsqlConnection dbConnection = FabricaConexao.getConexao();

            IEmailDao emailDao = FabricaDao.getEmailDao();

            try
            {
                return emailDao.findEmailByProntuario(prontuario, dbConnection);

            }
            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findEmailByProntuario", ex);
                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }
            finally
            {
                FabricaConexao.fecharConexao();
            }
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public Periodicidade findPeriodicidadeById(Int64 id)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findPeriodicidadeById");
            NpgsqlConnection dbConnection = FabricaConexao.getConexao();

            IPeriodicidadeDao periodicidadeDao = FabricaDao.getPeriodicidadeDao();

            try
            {
                return periodicidadeDao.findById(id, dbConnection);

            }
            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findPeriodicidadeById", ex);
                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }
            finally
            {
                FabricaConexao.fecharConexao();
            }
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public List<Email> findEmailByUsuario(Usuario usuario)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findEmailByUsuario");
            NpgsqlConnection dbConnection = FabricaConexao.getConexao();

            IEmailDao emailDao = FabricaDao.getEmailDao();

            try
            {
                return emailDao.findEmailByUsuario(usuario, dbConnection);

            }
            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findEmailByUsuario", ex);
                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }
            finally
            {
                FabricaConexao.fecharConexao();
            }
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public List<Aso> findAllAtendimentoNotCanceladoByFuncionario(Funcionario funcionario)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findEmailByUsuario");
            NpgsqlConnection dbConnection = FabricaConexao.getConexao();
            IAsoDao atendimentoDao = FabricaDao.getAsoDao();
            IMedicoDao medicoDao = FabricaDao.getMedicoDao();
            IClienteFuncaoDao clienteFuncaoDao = FabricaDao.getClienteFuncaoDao();
            IClienteDao clienteDao = FabricaDao.getClienteDao();
            IPeriodicidadeDao periodicidadeDao = FabricaDao.getPeriodicidadeDao();
            IUsuarioDao usuarioDao = FabricaDao.getUsuarioDao();
            IEstudoDao estudoDao = FabricaDao.getEstudoDao();
            IProtocoloDao protocoloDao = FabricaDao.getProtocoloDao();
            IEmpresaDao empresaDao = FabricaDao.getEmpresaDao();
            ICentroCustoDao centroCustoDao = FabricaDao.getCentroCustoDao();
           

            List<Aso> atendimentos = new List<Aso>();

            try
            {
                string limitePesquisa;
                PermissionamentoFacade.mapaConfiguracoes.TryGetValue(ConfigurationConstans.PERIODO_PESQUISA, out limitePesquisa);

                atendimentos = atendimentoDao.findAllByFuncionario(funcionario, Convert.ToInt32(limitePesquisa), PermissionamentoFacade.usuarioAutenticado.Empresa, dbConnection);

                /* preenchendo dados do atendimento na lista */

                atendimentos.ForEach(delegate(Aso atendimento)
                {
                    atendimento.Examinador = atendimento.Examinador != null ? medicoDao.findById((long)atendimento.Examinador.Id, dbConnection) : null;
                    atendimento.ClienteFuncaoFuncionario.ClienteFuncao = clienteFuncaoDao.findById((long)atendimento.ClienteFuncaoFuncionario.ClienteFuncao.Id, dbConnection);
                    atendimento.ClienteFuncaoFuncionario.ClienteFuncao.Cliente = clienteDao.findById((long)atendimento.ClienteFuncaoFuncionario.ClienteFuncao.Cliente.Id, dbConnection);
                    atendimento.Cliente = clienteDao.findById((long)atendimento.Cliente.Id, dbConnection);
                    atendimento.Periodicidade = periodicidadeDao.findById((long)atendimento.Periodicidade.Id, dbConnection);
                    atendimento.Coordenador = atendimento.Coordenador != null ? medicoDao.findById((long)atendimento.Coordenador.Id, dbConnection) : null;
                    atendimento.Criador = usuarioDao.findById((long)atendimento.Criador.Id, dbConnection);
                    atendimento.Estudo = atendimento.Estudo != null ? estudoDao.findById((long)atendimento.Estudo.Id, dbConnection) : null;
                    atendimento.Finalizador = atendimento.Finalizador != null ? usuarioDao.findById((long)atendimento.Finalizador.Id, dbConnection) : null;
                    atendimento.Protocolo = atendimento.Protocolo != null ? protocoloDao.findById((long)atendimento.Protocolo.Id, dbConnection) : null;
                    atendimento.Empresa = atendimento.Empresa != null ? empresaDao.findById((long)atendimento.Empresa.Id, dbConnection) : null;
                    atendimento.UsuarioAlteracao = atendimento.UsuarioAlteracao != null ? usuarioDao.findById((long)atendimento.UsuarioAlteracao.Id, dbConnection) : null;
                    if (atendimento.CentroCusto != null)
                    {
                        atendimento.CentroCusto = centroCustoDao.findById((long)atendimento.CentroCusto.Id, dbConnection);
                        atendimento.CentroCusto.Cliente = atendimento.Cliente;
                    }
                });

            }
            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findEmailByUsuario", ex);
                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }
            finally
            {
                FabricaConexao.fecharConexao();
            }

            return atendimentos;
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public List<AtendimentoExame> FindAllExamesByAsoByBySalaInService(Aso aso, Sala sala)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método FindAllExamesByAsoBySituacao");

            NpgsqlConnection dbConnection = FabricaConexao.getConexao();

            IAsoDao asoDao = FabricaDao.getAsoDao();

            try
            {
                return asoDao.FindAllExamesByAsoByBySalaInService(aso, sala, dbConnection);

            }
            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - FindAllExamesByAsoByBySalaInService", ex);
                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }
            finally
            {
                FabricaConexao.fecharConexao();
            }

        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public List<ClienteFuncaoExamePeriodicidade> listAllClienteFuncaoExamePeriodicidadeByClienteFuncaoByPeriodicidade(ClienteFuncao clienteFuncao, Periodicidade periodicidade)
        {

            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método listAllByClienteFuncaoByPeriodicidade");

            NpgsqlConnection dbConnection = FabricaConexao.getConexao();

            IClienteFuncaoExamePeriodicidadeDao clienteFuncaoExamePeriodicidadeDao = FabricaDao.getClienteFuncaoExamePeriodicidadeDao();

            try
            {
                return clienteFuncaoExamePeriodicidadeDao.listAllByClienteFuncaoByPeriodicidade(clienteFuncao, periodicidade, dbConnection);

            }
            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - listAllByClienteFuncaoByPeriodicidade", ex);
                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }
            finally
            {
                FabricaConexao.fecharConexao();
            }
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public List<GheFonteAgenteExamePeriodicidade> listAllGheFonteAgenteExamePeriodicidadeByGheByPeriodicidade(List<Ghe> ghes, Periodicidade periodicidade)
        {

            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método listAllGheFonteAgenteExamePeriodicidadeByGheByPeriodicidade");

            NpgsqlConnection dbConnection = FabricaConexao.getConexao();

            IGheFonteAgenteExamePeriodicidadeDao gheFonteAgenteExamePeriodicidadeDao = FabricaDao.getGheFonteAgenteExamePeriodicidadeDao();

            try
            {
                return gheFonteAgenteExamePeriodicidadeDao.listAllByGheByPeriodicidade(ghes, periodicidade, dbConnection);

            }
            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - listAllGheFonteAgenteExamePeriodicidadeByGheByPeriodicidade", ex);
                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }
            finally
            {
                FabricaConexao.fecharConexao();
            }
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public List<AsoGheSetor> findAsoGheSetorByAso(Aso aso)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findAsoGheSetorByAso");
            NpgsqlConnection dbConnection = FabricaConexao.getConexao();
            IAsoGheSetorDao asoGheSetorDao = FabricaDao.getAsoGheSetorDao();
            try
            {
                return asoGheSetorDao.findByAso(aso, dbConnection);
            }
            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findAsoGheSetorByAso", ex);
                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }
            finally
            {
                FabricaConexao.fecharConexao();
            }
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public AsoAgenteRisco insertAsoAgenteRisco(AsoAgenteRisco asoAgenteRisco)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método insertAsoAgenteRisco");
            NpgsqlConnection dbConnection = FabricaConexao.getConexao();
            NpgsqlTransaction transaction = dbConnection.BeginTransaction();

            IAsoAgenteRiscoDao asoAgenteRiscoDao = FabricaDao.getAsoAgenteRisco();

            try
            {
                AsoAgenteRisco newAsoAgenteRisco = asoAgenteRiscoDao.insert(asoAgenteRisco, dbConnection);
                transaction.Commit();

                return newAsoAgenteRisco;
            }
            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - insertAsoAgenteRisco", ex);
                transaction.Rollback();
                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }
            finally
            {
                FabricaConexao.fecharConexao();
            }
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public void deleteAsoAgenteRisco(AsoAgenteRisco asoAgenteRisco)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método deleteAsoAgenteRisco");
            NpgsqlConnection dbConnection = FabricaConexao.getConexao();
            NpgsqlTransaction transaction = dbConnection.BeginTransaction();

            IAsoAgenteRiscoDao asoAgenteRiscoDao = FabricaDao.getAsoAgenteRisco();

            try
            {
                asoAgenteRiscoDao.delete(asoAgenteRisco, dbConnection);
                transaction.Commit();

            }
            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - insertAsoAgenteRisco", ex);
                transaction.Rollback();
                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }
            finally
            {
                FabricaConexao.fecharConexao();
            }
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public void updateTipoAtendimentoByAtendimento(Aso atendimento)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método updateTipoAtendimentoByAtendimento");
            NpgsqlConnection dbConnection = FabricaConexao.getConexao();
            NpgsqlTransaction transaction = dbConnection.BeginTransaction();

            IAsoDao asoDao = FabricaDao.getAsoDao();

            try
            {
                asoDao.updateTipoAtendimento(atendimento, dbConnection);
                transaction.Commit();

            }
            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - updateTipoAtendimentoByAtendimento", ex);
                transaction.Rollback();
                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }
            finally
            {
                FabricaConexao.fecharConexao();
            }
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public void updateSituacaoAso(Aso atendimento)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método updateSituacaoAso");
            NpgsqlConnection dbConnection = FabricaConexao.getConexao();
            NpgsqlTransaction transaction = dbConnection.BeginTransaction();

            IAsoDao asoDao = FabricaDao.getAsoDao();

            try
            {
                asoDao.updateSituacao(atendimento, dbConnection);
                transaction.Commit();

            }
            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - updateSituacaoAso", ex);
                transaction.Rollback();
                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }
            finally
            {
                FabricaConexao.fecharConexao();
            }
        }


        [MethodImpl(MethodImplOptions.Synchronized)]
        public List<Aso> findAllAtendimentosInSituacaoByClienteAndEmpresaAndPeriodo(Cliente cliente, string situacao, DateTime dataInicial, DateTime dataFinal, bool transcrito, Empresa empresa)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findAllAtendimentosInSituacaoByClienteAndEmpresaAndPeriodo");
            NpgsqlConnection dbConnection = FabricaConexao.getConexao();
            IAsoDao asoDao = FabricaDao.getAsoDao();
            IClienteFuncaoExameASoDao clienteFuncaoExameAsoDao = FabricaDao.getClienteFuncaoExameASoDao();
            IGheFonteAgenteExameAsoDao gheFonteAgenteExameAsoDao = FabricaDao.getGheFonteAgenteExameAsoDao();
            List<Aso> atendimentos = new List<Aso>();
            try
            {
                atendimentos = asoDao.findAllAtendimentoInSituacaoByClienteAndEmpresaAndPeriodo(cliente, situacao, dataInicial, dataFinal, empresa, dbConnection);
                /* recuperando exames do atendimento */
                foreach (Aso atendimento in atendimentos)
                {
                    atendimento.ExameExtra = clienteFuncaoExameAsoDao.findAllExamesByAsoInSituacao(atendimento, true, true, false, false, transcrito, dbConnection);
                    atendimento.ExamePcmso = gheFonteAgenteExameAsoDao.findAllExamesByAsoInSituacao(atendimento, true, true, false, false, transcrito, dbConnection);
                }
                
            }
            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findAllAtendimentosInSituacaoByClienteAndEmpresaAndPeriodo", ex);
                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }
            finally
            {
                FabricaConexao.fecharConexao();
            }
            return atendimentos;
        }

    }
}