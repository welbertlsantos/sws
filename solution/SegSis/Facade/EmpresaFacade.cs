﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using SWS.Entidade;
using SWS.Helper;
using Npgsql;
using SWS.Dao;
using SWS.IDao;
using SWS.Excecao;
using System.Runtime.CompilerServices;

namespace SWS.Facade
{
    class EmpresaFacade
    {
        public static EmpresaFacade empresaFacade = null;

        private EmpresaFacade() { }

        public static EmpresaFacade getInstance()
        {
            if (empresaFacade == null)
            {
                empresaFacade = new EmpresaFacade();
            }

            return empresaFacade;

        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public DataSet findEmpresaByFilter(Empresa empresa)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findEmpresaByFilter");

            NpgsqlConnection dbConnection = FabricaConexao.getConexao();
            
            IEmpresaDao empresaDao = FabricaDao.getEmpresaDao();

            try
            {
                return empresaDao.findByFilter(empresa, dbConnection);

            }
            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findEmpresaByFilter", ex);
                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }
            finally
            {
                FabricaConexao.fecharConexao();
            }
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public Empresa insertEmpresa(Empresa empresa)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método insertEmpresa");

            Empresa empresaNova = null;
            
            NpgsqlConnection dbConnection = FabricaConexao.getConexao();

            IEmpresaDao empresaDao = FabricaDao.getEmpresaDao();
                        
            NpgsqlTransaction transaction = dbConnection.BeginTransaction();
            
            try
            {
                
                // verificando se já existe empresa cadastrada com esse cnpj.

                Empresa empresaProcurada = empresaDao.findByCnpj(empresa.Cnpj, dbConnection);

                if (empresaProcurada == null)
                {
                    // incluindo a nova empresa
                    empresaNova = empresaDao.insert(empresa, dbConnection);

                }
                else
                {
                    throw new EmpresaException(EmpresaException.msg1);
                }
                
                transaction.Commit();
            }
            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - insertEmpresa", ex);
                transaction.Rollback();
                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }
            finally
            {
                FabricaConexao.fecharConexao();
            }

            return empresaNova;
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public Empresa findEmpresaById(Int64 id)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findEmpresaById");

            NpgsqlConnection dbConnection = FabricaConexao.getConexao();

            IEmpresaDao empresaDao = FabricaDao.getEmpresaDao();
                        
            try
            {
                return empresaDao.findById(id, dbConnection);
            }
            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findEmpresaById", ex);
                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }
            finally
            {
                FabricaConexao.fecharConexao();
            }
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public Empresa updateEmpresa(Empresa empresa)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método updateEmpresa");

            NpgsqlConnection dbConnection = FabricaConexao.getConexao();

            IEmpresaDao empresaDao = FabricaDao.getEmpresaDao();

            Empresa empresaAlterada;

            NpgsqlTransaction transaction = dbConnection.BeginTransaction();

            try
            {

                // verificando se já existe empresa cadastrada com esse cnpj.

                Empresa empresaProcurada = empresaDao.findByCnpj(empresa.Cnpj, dbConnection);

                if (empresaProcurada == null || empresaProcurada.Id == empresa.Id)
                {
                    empresaAlterada = empresaDao.update(empresa, dbConnection);

                }
                else
                {
                    throw new EmpresaException(EmpresaException.msg1);
                }

                transaction.Commit();
            }
            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - updateEmpresa", ex);
                transaction.Rollback();
                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }
            finally
            {
                FabricaConexao.fecharConexao();
            }

            return empresaAlterada;
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public List<Empresa> findAllEmpresaAtivo()
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findAllEmpresaAtivo");
            NpgsqlConnection dbConnection = FabricaConexao.getConexao();
            IEmpresaDao empresaDao = FabricaDao.getEmpresaDao();
            
            try
            {
                return empresaDao.findAll(dbConnection);
            }
            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findAllEmpresaAtivo", ex);
                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }
            finally
            {
                FabricaConexao.fecharConexao();
            }
        }

    }
}
