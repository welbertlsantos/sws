﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SWS.Entidade;
using SWS.Excecao;
using SWS.Dao;
using SWS.IDao;
using System.Data;
using Npgsql;
using NpgsqlTypes;
using SWS.View.Resources;
using SWS.Helper;
using System.Runtime.CompilerServices;


namespace SWS.Facade
{
    class FuncionarioFacade
    {
        public static FuncionarioFacade funcionarioFacade = null;
        
        private FuncionarioFacade() { }

        public static FuncionarioFacade getInstance()
        {
            if (funcionarioFacade == null)
            {
                funcionarioFacade = new FuncionarioFacade();
            }

            return funcionarioFacade;
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public DataSet findFuncionarioByFilter(Funcionario funcionario, ClienteFuncao clienteFuncao, Cliente cliente)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findFuncionarioByFilter");

            NpgsqlConnection dbConnection = FabricaConexao.getConexao();
            
            IFuncionarioDao funcionarioDao = FabricaDao.getFuncionarioDao();

            try
            {
                return funcionarioDao.findFuncionarioByFilter(funcionario, clienteFuncao, cliente, dbConnection);

            }
            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findFuncionarioByFilter", ex);
                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }
            finally
            {
                FabricaConexao.fecharConexao();
            }
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public Funcionario incluirFuncionario(Funcionario funcionario)
        {

            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método incluirFuncionario");

            Funcionario funcionarioNovo = null;
            
            NpgsqlConnection dbConnection = FabricaConexao.getConexao();
            NpgsqlTransaction transaction = dbConnection.BeginTransaction();
            
            IFuncionarioDao funcionarioDao = FabricaDao.getFuncionarioDao();
            IClienteFuncaoFuncionarioDao clienteFuncaoFuncionarioDao = FabricaDao.getClienteFuncaoFuncionarioDao();
            

            try
            {
                //verificando se já existe um funcionário cadastrado com esse CPF

                Funcionario funcionarioProcurado = null;

                if (!String.IsNullOrWhiteSpace(funcionario.Cpf))
                {
                    funcionarioProcurado = funcionarioDao.findFuncionarioByCPF((String)funcionario.Cpf, dbConnection);

                    if (funcionarioProcurado != null)
                        throw new FuncionarioException(FuncionarioException.msg1);
                }

                //verificando se já existe um funcionário cadastrado com mesmo nome e rg
                if (!String.IsNullOrWhiteSpace(funcionario.Rg))
                {
                    funcionarioProcurado = funcionarioDao.findFuncionarioByNomeRg(funcionario.Nome, (String)funcionario.Rg, dbConnection);

                    if (funcionarioProcurado != null)
                        throw new FuncionarioException(FuncionarioException.msg8);
                }

                //incluindo funcionário
                funcionarioNovo = funcionarioDao.insert(funcionario, dbConnection);

                // ClienteFuncao Funcionário
                if (funcionario.ClienteFuncaoFuncionario.Count == 0)
                {
                    throw new FuncionarioException(FuncionarioException.msg4);
                }
                else
                {
                    foreach (ClienteFuncaoFuncionario clienteFuncaoFuncionario in funcionario.ClienteFuncaoFuncionario)
                    {
                        clienteFuncaoFuncionario.Funcionario = funcionarioNovo;
                        clienteFuncaoFuncionarioDao.insert(clienteFuncaoFuncionario, dbConnection);
                    }
                }

                transaction.Commit();
                
                return funcionarioNovo;

            }
            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - incluirFuncionario", ex);
                transaction.Rollback();
                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }
            finally
            {
                FabricaConexao.fecharConexao();
            }
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public Funcionario findFuncionarioById(Int64 id)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findFuncionarioById");

            NpgsqlConnection dbConnection = FabricaConexao.getConexao();

            IFuncionarioDao funcionarioDao = FabricaDao.getFuncionarioDao();

            try
            {

                return funcionarioDao.findFuncionarioById(id,dbConnection);

            }
            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findFuncionarioById", ex);
                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }
            finally
            {
                FabricaConexao.fecharConexao();
            }
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public ClienteFuncaoFuncionario findClienteFuncaoFuncionario(Funcionario funcionario)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findClienteFuncaoFuncionario");

            NpgsqlConnection dbConnection = FabricaConexao.getConexao();

            IClienteFuncaoFuncionarioDao clienteFuncaoFuncionarioDao = FabricaDao.getClienteFuncaoFuncionarioDao();
            IClienteDao clienteDao = FabricaDao.getClienteDao();

            try
            {
                return clienteFuncaoFuncionarioDao.findAtivoByFuncionario(funcionario, dbConnection);

            }
            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findClienteFuncaoFuncionario", ex);
                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }
            finally
            {
                FabricaConexao.fecharConexao();
            }
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public Funcionario updateFuncionario(Funcionario funcionario)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método alteraFuncionario");

            NpgsqlConnection dbConnection = FabricaConexao.getConexao();

            NpgsqlTransaction transaction = dbConnection.BeginTransaction();

            IFuncionarioDao funcionarioDao = FabricaDao.getFuncionarioDao();

            Funcionario funcionarioAlterado = null;

            try
            {
                if (funcionarioDao.verificaPodeAlterarFuncionario(funcionario, dbConnection) != null)
                {
                    throw new Exception("Não é possível alterar funcionário pois existe um funcionário com o mesmo nome e RG ou CPF cadastrado.");
                }

                funcionarioAlterado = funcionarioDao.update(funcionario, dbConnection);
                transaction.Commit();
                return funcionarioAlterado;
            }
            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - alteraFuncionario", ex);
                transaction.Rollback();
                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }
            finally
            {
                FabricaConexao.fecharConexao();   
            }
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public void excluiFuncionario(Funcionario funcionario)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método excluiFuncionario");

            NpgsqlConnection dbConnection = FabricaConexao.getConexao();
            NpgsqlTransaction transaction = dbConnection.BeginTransaction();

            IFuncionarioDao funcionarioDao = FabricaDao.getFuncionarioDao();
            IClienteFuncaoFuncionarioDao clienteFuncaoFuncionarioDao = FabricaDao.getClienteFuncaoFuncionarioDao();
            IMonitoramentoDao monitoramentoDao = FabricaDao.getMonitoramentoDao();
            IClienteFuncionarioDao clienteFuncionarioDao = FabricaDao.getclienteFuncionarioDao();
            IAsoDao asoDao = FabricaDao.getAsoDao();

            try
            {
                bool podeExcluir = true;

                /* verificando o cliente_funcao_funcionario */
                List<ClienteFuncaoFuncionario> clienteFuncaoFuncionarioList = clienteFuncaoFuncionarioDao.findByFuncionarioAndSituacaoList(funcionario, false, dbConnection);

                /* verificando o cliente funcionario */
                List<ClienteFuncionario> clienteFuncionarioList = clienteFuncionarioDao.findAllByFuncionario(funcionario, dbConnection);

               
                foreach( ClienteFuncaoFuncionario clienteFuncaoFuncionario in clienteFuncaoFuncionarioList)
                {
                     /* verificando os atendimentos */
                    if (asoDao.findAsoByClienteFuncaoFuncionario(clienteFuncaoFuncionario, dbConnection))
                    {
                        podeExcluir = false;
                        break;
                    }
                    /* verificando o monitoramento do funcionario */
                    if (monitoramentoDao.findAllMonitoramentoByClienteFuncaoFuncionario(clienteFuncaoFuncionario, dbConnection).Count > 0) 
                    {
                        podeExcluir = false;
                        break;
                    }
                }

                if (!podeExcluir)
                {
                    /* Nesse caso não será excluído o funcionário porque ele tem relacionamentos */
                    funcionarioDao.update(funcionario, dbConnection);

                    /* desativando o cliente Funcao Funcionario */
                    foreach (ClienteFuncaoFuncionario clienteFuncaoFuncionario in clienteFuncaoFuncionarioList)
                    {
                        clienteFuncaoFuncionario.Situacao = ApplicationConstants.DESATIVADO;
                        clienteFuncaoFuncionario.DataDesligamento = DateTime.Now;
                        clienteFuncaoFuncionarioDao.update(clienteFuncaoFuncionario, dbConnection);
                    }

                    /* desativado o cliente funcionario */
                    foreach (ClienteFuncionario clienteFuncionario in clienteFuncionarioList)
                    {
                        clienteFuncionario.DataDemissao = DateTime.Now;
                        clienteFuncionarioDao.update(clienteFuncionario, dbConnection);
                    }

                }
                else 
                {
                    /* poderá ser excluído o funcionario porque não tem relacionamentos dependentes */
                    funcionarioDao.deleteFuncionario(funcionario, dbConnection);
                    foreach (ClienteFuncaoFuncionario clienteFuncaoFuncionario in clienteFuncaoFuncionarioList)
                    {
                        clienteFuncaoFuncionarioDao.delete(clienteFuncaoFuncionario, dbConnection);
                    }

                    foreach (ClienteFuncionario clienteFuncionario in clienteFuncionarioList)
                    {
                        clienteFuncionarioDao.delete(clienteFuncionario, dbConnection);
                    }
                }

                transaction.Commit();
                
            }
            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - excluiFuncionario", ex);
                transaction.Rollback();
                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }
            finally
            {
                FabricaConexao.fecharConexao();

            }

        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public DataSet findFuncionarioByFilterByFuncionario(Funcionario funcionario)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findFuncionarioByFilterByFuncionario");

            NpgsqlConnection dbConnection = FabricaConexao.getConexao();

            IFuncionarioDao funcionarioDao = FabricaDao.getFuncionarioDao();

            try
            {
                return funcionarioDao.findFuncionarioByFilterByFuncionario(funcionario, dbConnection);

            }
            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findFuncionarioByFilterByFuncionario", ex);
                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }
            finally
            {
                FabricaConexao.fecharConexao();
            }
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public ClienteFuncaoFuncionario findClienteFuncaoFuncionarioByFuncionarioAndStatus(Funcionario funcionario, String situacao)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findClienteFuncaoFuncionario");

            NpgsqlConnection dbConnection = FabricaConexao.getConexao();

            IClienteFuncaoFuncionarioDao clienteFuncaoFuncionarioDao = FabricaDao.getClienteFuncaoFuncionarioDao();

            try
            {
                return clienteFuncaoFuncionarioDao.findByFuncionarioAndSituacao(funcionario, situacao, dbConnection);

            }
            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findClienteFuncaoFuncionario", ex);
                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }
            finally
            {
                FabricaConexao.fecharConexao();
            }
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public List<ClienteFuncaoFuncionario> findClienteFuncaoFuncionarioByFuncionarioAndSituacao(Funcionario funcionario, bool ativos)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findClienteFuncaoFuncionarioSet");

            NpgsqlConnection dbConnection = FabricaConexao.getConexao();

            IClienteFuncaoFuncionarioDao clienteFuncaoFuncionarioDao = FabricaDao.getClienteFuncaoFuncionarioDao();

            try
            {
               return clienteFuncaoFuncionarioDao.findByFuncionarioAndSituacaoList(funcionario, ativos, dbConnection);

            }
            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findClienteFuncaoFuncionarioSet", ex);
                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }
            finally
            {
                FabricaConexao.fecharConexao();
            }
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public ClienteFuncaoFuncionario incluirClienteFuncaoFuncionario(ClienteFuncaoFuncionario clienteFuncaoFuncionario)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método incluirClienteFuncaoFuncionario");

            NpgsqlConnection dbConnection = FabricaConexao.getConexao();

            NpgsqlTransaction transaction = dbConnection.BeginTransaction();

            IClienteFuncaoFuncionarioDao clienteFuncaoFuncionarioDao = FabricaDao.getClienteFuncaoFuncionarioDao();

            try
            {

                clienteFuncaoFuncionarioDao.insert(clienteFuncaoFuncionario, dbConnection);
                
                transaction.Commit();
                
                return clienteFuncaoFuncionario;

            }
            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - excluiFuncionario", ex);
                transaction.Rollback();
                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }
            finally
            {
                FabricaConexao.fecharConexao();

            }

        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public void updateClienteFuncaoFuncionario(ClienteFuncaoFuncionario clienteFuncaoFuncionario)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método updateClienteFuncaoFuncionario");

            NpgsqlConnection dbConnection = FabricaConexao.getConexao();
            NpgsqlTransaction transaction = dbConnection.BeginTransaction();

            IClienteFuncaoFuncionarioDao clienteFuncaoFuncionarioDao = FabricaDao.getClienteFuncaoFuncionarioDao();

            try
            {
                clienteFuncaoFuncionarioDao.update(clienteFuncaoFuncionario, dbConnection);
            
                transaction.Commit();

            }
            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - updateClienteFuncaoFuncionario", ex);
                transaction.Rollback();
                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }
            finally
            {
                FabricaConexao.fecharConexao();

            }
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public void disableClienteFuncaoFuncionario(ClienteFuncaoFuncionario clienteFuncaoFuncionario, DateTime dataDemissao)
        {

            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método disableClienteFuncaoFuncionario");

            NpgsqlConnection dbConnection = FabricaConexao.getConexao();
            NpgsqlTransaction transaction = dbConnection.BeginTransaction();

            IClienteFuncaoFuncionarioDao clienteFuncaoFuncionarioDao = FabricaDao.getClienteFuncaoFuncionarioDao();

            try
            {
                clienteFuncaoFuncionarioDao.disable(clienteFuncaoFuncionario, dataDemissao, dbConnection);
                transaction.Commit();

            }
            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - disableClienteFuncaoFuncionario", ex);
                transaction.Rollback();
                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }
            finally
            {
                FabricaConexao.fecharConexao();
            }
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public List<ClienteFuncaoFuncionario> findAllFuncionarioByClienteFuncaoFuncionario(ClienteFuncaoFuncionario clienteFuncaoFuncionario)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findAllFuncionarioByCliente");

            NpgsqlConnection dbConnection = FabricaConexao.getConexao();
            IClienteFuncaoFuncionarioDao clienteFuncaoFuncionarioDao = FabricaDao.getClienteFuncaoFuncionarioDao();
            IClienteFuncaoDao clienteFuncaoDao = FabricaDao.getClienteFuncaoDao();
            IFuncionarioDao funcionarioDao = FabricaDao.getFuncionarioDao();

            List<ClienteFuncaoFuncionario> funcionariosInCliente = new List<ClienteFuncaoFuncionario>();

            try
            {
                funcionariosInCliente = clienteFuncaoFuncionarioDao.findAllFuncionarioByClienteAndFuncionario(clienteFuncaoFuncionario.ClienteFuncao.Cliente, clienteFuncaoFuncionario.Funcionario, true, dbConnection);

                foreach (ClienteFuncaoFuncionario cf in funcionariosInCliente)
                {
                    cf.ClienteFuncao.Cliente = clienteFuncaoFuncionario.ClienteFuncao.Cliente;
                    cf.Funcionario = funcionarioDao.findFuncionarioById((long)cf.Funcionario.Id, dbConnection);
                }

                return funcionariosInCliente.OrderBy(x => x.Funcionario.Nome).ToList();

            }
            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findAllFuncionarioByCliente", ex);

                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }
            finally
            {
                FabricaConexao.fecharConexao();
            }
        }

        
        [MethodImpl(MethodImplOptions.Synchronized)]
        public List<ClienteFuncaoFuncionario> findAllFuncionariosAtivosByClienteForMonitoramentoPeriodico(Cliente cliente, DateTime periodo, bool historico)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findAllFuncionariosAtivosByClienteForMonitoramentoPeriodico");

            NpgsqlConnection dbConnection = FabricaConexao.getConexao();
            IClienteFuncaoFuncionarioDao clienteFuncaoFuncionarioDao = FabricaDao.getClienteFuncaoFuncionarioDao();
            IClienteFuncionarioDao clienteFuncionarioDao = FabricaDao.getclienteFuncionarioDao();
            IFuncionarioDao funcionarioDao = FabricaDao.getFuncionarioDao();
            IAsoDao asoDao = FabricaDao.getAsoDao();
            List<ClienteFuncaoFuncionario> funcionariosClassificados = new List<ClienteFuncaoFuncionario>();

            try
            {
                List<ClienteFuncaoFuncionario> clienteFuncaoFuncionarioInCliente = clienteFuncaoFuncionarioDao.findAtivosByCliente(cliente, periodo, dbConnection);
                List<ClienteFuncionario> clienteFuncionarioInCliente = clienteFuncionarioDao.findAtivosByCliente(cliente, periodo, dbConnection);

                /* encontrando o clienteFuncaoFuncionario pelo clienteFuncionario retornado */

                clienteFuncionarioInCliente.ForEach(delegate (ClienteFuncionario cf ) {

                    clienteFuncaoFuncionarioInCliente.Concat(clienteFuncaoFuncionarioDao.findAllFuncionarioByClienteAndFuncionario(cf.Cliente, cf.Funcionario, true, dbConnection));
    
                });


                if (historico)
                {

                    /* verificando quais os funcionários ativos que fizeram atendimento periódico, admissional ou mudança de função no período analisado */

                    DateTime periodoInicial = new DateTime(periodo.Year, periodo.Month, 1);

                    foreach (ClienteFuncaoFuncionario funcionarioCliente in clienteFuncaoFuncionarioInCliente)
                    {
                        funcionarioCliente.Funcionario = funcionarioDao.findFuncionarioById((long)funcionarioCliente.Funcionario.Id, dbConnection);

                        List<Aso> atendimentosEncontrados = asoDao.findAllAtendimentoInPeriodoByFuncionarioInCliente(funcionarioCliente, Convert.ToDateTime(periodoInicial.ToShortDateString() + " 00:00:00"), Convert.ToDateTime(periodo.ToShortDateString() + " 23:59:59"), PermissionamentoFacade.usuarioAutenticado.Empresa, dbConnection);

                        if (atendimentosEncontrados.Count > 0)
                        {
                            funcionariosClassificados.Add(funcionarioCliente);
                        }

                    }

                    return funcionariosClassificados.OrderBy(item => item.Funcionario.Nome).ToList();
                }
                else
                {
                    foreach (ClienteFuncaoFuncionario funcionarioCliente in clienteFuncaoFuncionarioInCliente)
                    {
                        funcionarioCliente.Funcionario = funcionarioDao.findFuncionarioById((long)funcionarioCliente.Funcionario.Id, dbConnection);

                    }

                    return clienteFuncaoFuncionarioInCliente.OrderBy(item => item.Funcionario.Nome).ToList();
                }
                    
            }
            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findAllFuncionariosAtivosByClienteForMonitoramentoPeriodico", ex);

                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }
            finally
            {
                FabricaConexao.fecharConexao();
            }
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public List<ClienteFuncaoFuncionario> findAllByClienteFuncao(ClienteFuncao clienteFuncao)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findAllByClienteFuncao");

            NpgsqlConnection dbConnection = FabricaConexao.getConexao();
            IClienteFuncaoFuncionarioDao clienteFuncaoFuncionarioDao = FabricaDao.getClienteFuncaoFuncionarioDao();
            IFuncionarioDao funcionarioDao = FabricaDao.getFuncionarioDao();
            List<ClienteFuncaoFuncionario> listClienteFuncaoFuncionario = new List<ClienteFuncaoFuncionario>();

            try
            {
                listClienteFuncaoFuncionario = clienteFuncaoFuncionarioDao.findAllByClienteFuncao(clienteFuncao, dbConnection);

                foreach (ClienteFuncaoFuncionario clienteFuncaoFuncionario in listClienteFuncaoFuncionario)
                {
                    clienteFuncaoFuncionario.Funcionario = funcionarioDao.findFuncionarioById((long)clienteFuncaoFuncionario.Funcionario.Id, dbConnection);
                }

            }
            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findAllByClienteFuncao", ex);

                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }
            finally
            {
                FabricaConexao.fecharConexao();
            }
            return listClienteFuncaoFuncionario;
        }


    }
}
