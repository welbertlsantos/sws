﻿using System;
using System.Collections.Generic;
using System.Linq;
using SWS.Entidade;
using SWS.Helper;
using Npgsql;
using SWS.IDao;
using SWS.Dao;
using SWS.Excecao;
using System.Data;
using SWS.View.Resources;
using System.Runtime.CompilerServices;
using SWS.Resources;

namespace SWS.Facade
{
    class FilaFacade
    {
        public static FilaFacade filaFacade = null;

        private FilaFacade() { }

        public static FilaFacade getInstance()
        {
            if (filaFacade == null)
            {
                filaFacade = new FilaFacade();
            }

            return filaFacade;
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public Sala insertSala(Sala sala)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método insertSala");

            Sala novaSala = null;

            NpgsqlConnection dbConnection = FabricaConexao.getConexao();

            ISalaDao salaDao = FabricaDao.getSalaDao();
            ISalaExameDao salaExameDao = FabricaDao.getSalaExameDao();
            
            NpgsqlTransaction transaction = dbConnection.BeginTransaction();

            try
            {
                
                Sala salaProcurada = salaDao.findByDescricao(sala.Descricao, (long)sala.Empresa.Id, dbConnection);

                if (salaProcurada != null)
                    throw new SalaException(SalaException.msg1);
                 
                novaSala = salaDao.insert(sala, dbConnection);
                    
                novaSala.Exames.ForEach(delegate (SalaExame salaExame) {
                    salaExame.Sala = novaSala;
                    salaExameDao.insert(salaExame, dbConnection);

                });
            
                transaction.Commit();
            }
            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - insertSala", ex);
                transaction.Rollback();
                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }
            finally
            {
                FabricaConexao.fecharConexao();
            }

            return novaSala;
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public DataSet findSalaByFilter(Sala sala)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findSalaByFilter");

            NpgsqlConnection dbConnection = FabricaConexao.getConexao();

            ISalaDao salaDao = FabricaDao.getSalaDao();

            try
            {

                return salaDao.findByFilter(sala, dbConnection);

            }
            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findSalaByFilter", ex);
                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }
            finally
            {
                FabricaConexao.fecharConexao();
            }

        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public Sala findSalaById(Int64 id)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findSalaById");

            NpgsqlConnection dbConnection = FabricaConexao.getConexao();

            ISalaDao salaDao = FabricaDao.getSalaDao();
            ISalaExameDao salaExameDao = FabricaDao.getSalaExameDao();
            Sala sala = null;

            try
            {
                sala = salaDao.findById(id, dbConnection);

                /* preechendo exames da sala de atendimento */
                sala.Exames = salaExameDao.findAtivosBySala(sala, dbConnection);

                return sala;

            }
            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findSalaById", ex);
                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }
            finally
            {
                FabricaConexao.fecharConexao();
            }

        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public Sala updateSala(Sala sala)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método updateSala");

            NpgsqlConnection dbConnection = FabricaConexao.getConexao();

            Sala salaAlterada;

            ISalaDao salaDao = FabricaDao.getSalaDao();
            ISalaExameDao salaExameDao = FabricaDao.getSalaExameDao();

            NpgsqlTransaction transaction = dbConnection.BeginTransaction();

            try
            {

                // verificando se pode ser alterada a sala.

                Sala salaProcurada = salaDao.findByDescricao(sala.Descricao, (long)sala.Empresa.Id, dbConnection);

                if (salaProcurada == null || salaProcurada.Id == sala.Id)
                {
                    salaAlterada = salaDao.update(sala, dbConnection);
                    
                    // fazendo update na coleção de salaExames

                    foreach (SalaExame salaExame in salaAlterada.Exames)
                    {
                        salaExameDao.update(salaExame, dbConnection);
                    }
                    
                    transaction.Commit();

                }
                else
                {
                    throw new SalaException(SalaException.msg1);
                }

            }
            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - insertSala", ex);
                transaction.Rollback();
                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }
            finally
            {
                FabricaConexao.fecharConexao();
            }

            return salaAlterada;
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public LinkedList<ItemFilaAtendimento> buscaExamesAtendendo(DateTime dataInicial, DateTime dataFinal, Sala sala)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método buscaExamesAtendendo");

            LinkedList<ItemFilaAtendimento> examesParaAtendimento = new LinkedList<ItemFilaAtendimento>();
            LinkedList<ItemFilaAtendimento> examesParaAtendimentoRetorno = new LinkedList<ItemFilaAtendimento>();
            NpgsqlConnection dbConnection = FabricaConexao.getConexao();
            IAsoDao asoDao = FabricaDao.getAsoDao();
            IPeriodicidadeDao periodicidadeDao = FabricaDao.getPeriodicidadeDao();
            IClienteFuncaoExameASoDao clienteFuncaoExameAsoDao = FabricaDao.getClienteFuncaoExameASoDao();
            IGheFonteAgenteExameAsoDao gheFonteAgenteExameAsoDao = FabricaDao.getGheFonteAgenteExameAsoDao();
            IGheFonteAgenteExameDao gheFonteAgenteExameDao = FabricaDao.getGheFonteAgenteExameDao();
            IClienteFuncaoExameDao clienteFuncaoExameDao = FabricaDao.getClienteFuncaoExameDao();
            IClienteFuncaoFuncionarioDao clienteFuncaoFuncionarioDao = FabricaDao.getClienteFuncaoFuncionarioDao();
            try
            {
                LinkedList<Aso> asos = asoDao.findAsoByPeriodo(dataInicial, dataFinal, dbConnection);
                LinkedList<ClienteFuncaoExameASo> clienteFuncaoExameAso = new LinkedList<ClienteFuncaoExameASo>();
                LinkedList<GheFonteAgenteExameAso> gheFonteAgenteExameASo = new LinkedList<GheFonteAgenteExameAso>();
                LinkedList<ItemFilaAtendimento> filaVip = new LinkedList<ItemFilaAtendimento>();
                ClienteFuncaoFuncionario clienteFuncaoFuncionario = null;
                string ordenacao = string.Empty;
                PermissionamentoFacade.mapaConfiguracoes.TryGetValue(ConfigurationConstans.ORDENACAO_ATENDIMENTO, out ordenacao);

                foreach (Aso aso in asos)
                {
                    clienteFuncaoFuncionario = clienteFuncaoFuncionarioDao.findById(aso.ClienteFuncaoFuncionario.Id, dbConnection);

                    foreach (ClienteFuncaoExameASo cfea in clienteFuncaoExameAsoDao.findAllExamesByAsoInSituacaoBySala(aso, true, false, false, null, sala, Convert.ToBoolean(ordenacao), dbConnection))
                    {
                        if (cfea.Atendido && !cfea.Finalizado)
                        {
                            cfea.Aso = asoDao.findById((Int64) cfea.Aso.Id, dbConnection);
                            if (!(Boolean)clienteFuncaoFuncionario.Vip)
                            {
                                examesParaAtendimento.AddLast(new ItemFilaAtendimento(cfea.Id, clienteFuncaoFuncionario.Funcionario.Nome, 
                                    String.Format("{0:dd/MM/yyyy}", clienteFuncaoFuncionario.Funcionario.DataNascimento), 
                                    clienteFuncaoFuncionario.Funcionario.Cpf, clienteFuncaoFuncionario.ClienteFuncao.Funcao.Descricao, 
                                    clienteFuncaoFuncionario.ClienteFuncao.Cliente.RazaoSocial, cfea.Login, 
                                    ApplicationConstants.ATENDER_EXAME, ApplicationConstants.EXAME_EXTRA, 
                                    clienteFuncaoExameDao.findExameByClienteFuncaoExame(cfea.ClienteFuncaoExame, dbConnection), 
                                    cfea.Devolvido,(DateTime)aso.DataElaboracao, aso.Rg, null, 0, clienteFuncaoFuncionario.Funcionario.Peso, 
                                    clienteFuncaoFuncionario.Funcionario.Altura, 
                                    periodicidadeDao.findById((Int64)cfea.Aso.Periodicidade.Id, dbConnection).Descricao, 
                                    clienteFuncaoFuncionario.Funcionario.getImc(), cfea.DataAtendido, cfea.Aso.Codigo, String.Empty, aso.Prioridade, !String.IsNullOrEmpty(aso.Senha) ? aso.Senha : string.Empty));
                            }
                            else
                            {
                                filaVip.AddFirst(new ItemFilaAtendimento(cfea.Id, clienteFuncaoFuncionario.Funcionario.Nome, String.Format("{0:dd/MM/yyyy}", clienteFuncaoFuncionario.Funcionario.DataNascimento), 
                                    clienteFuncaoFuncionario.Funcionario.Cpf, clienteFuncaoFuncionario.ClienteFuncao.Funcao.Descricao, 
                                    clienteFuncaoFuncionario.ClienteFuncao.Cliente.RazaoSocial, cfea.Login, 
                                    ApplicationConstants.ATENDER_EXAME, ApplicationConstants.EXAME_EXTRA, 
                                    clienteFuncaoExameDao.findExameByClienteFuncaoExame(cfea.ClienteFuncaoExame, dbConnection), 
                                    cfea.Devolvido, (DateTime)aso.DataElaboracao, aso.Rg, null, 0, 
                                    clienteFuncaoFuncionario.Funcionario.Peso, clienteFuncaoFuncionario.Funcionario.Altura, 
                                    periodicidadeDao.findById((Int64)cfea.Aso.Periodicidade.Id, dbConnection).Descricao,
                                    clienteFuncaoFuncionario.Funcionario.getImc(), cfea.DataAtendido, cfea.Aso.Codigo, String.Empty, aso.Prioridade, !String.IsNullOrEmpty(aso.Senha) ? aso.Senha : string.Empty));
                            }

                        }
                    }

                    foreach (GheFonteAgenteExameAso gfaea in gheFonteAgenteExameAsoDao.findAllExamesByAsoInSituacaoBySala(aso, true, false, false, null, sala, Convert.ToBoolean(ordenacao), dbConnection))
                    {
                        if (gfaea.Atendido && !gfaea.Finalizado)
                        {
                            gfaea.Aso = asoDao.findById((Int64)gfaea.Aso.Id, dbConnection);

                            if (!(Boolean)clienteFuncaoFuncionario.Vip)
                            {
                                examesParaAtendimento.AddLast(new ItemFilaAtendimento(gfaea.Id, clienteFuncaoFuncionario.Funcionario.Nome, String.Format("{0:dd/MM/yyyy}", 
                                    clienteFuncaoFuncionario.Funcionario.DataNascimento), clienteFuncaoFuncionario.Funcionario.Cpf, 
                                    clienteFuncaoFuncionario.ClienteFuncao.Funcao.Descricao, 
                                    clienteFuncaoFuncionario.ClienteFuncao.Cliente.RazaoSocial, gfaea.Login, 
                                    ApplicationConstants.ATENDER_EXAME, ApplicationConstants.EXAME_PCMSO, 
                                    gheFonteAgenteExameDao.findExameByGheFonteAgenteExame(gfaea.GheFonteAgenteExame, dbConnection), 
                                    gfaea.Devolvido, (DateTime)aso.DataElaboracao, aso.Rg, null, 0, clienteFuncaoFuncionario.Funcionario.Peso, 
                                    clienteFuncaoFuncionario.Funcionario.Altura, 
                                    periodicidadeDao.findById((Int64)gfaea.Aso.Periodicidade.Id, dbConnection).Descricao, 
                                    clienteFuncaoFuncionario.Funcionario.getImc(), gfaea.DataAtendido, gfaea.Aso.Codigo, String.Empty, aso.Prioridade, !String.IsNullOrEmpty(aso.Senha) ? aso.Senha : string.Empty));
                            }
                            else
                            {
                                filaVip.AddFirst(new ItemFilaAtendimento(gfaea.Id, clienteFuncaoFuncionario.Funcionario.Nome, String.Format("{0:dd/MM/yyyy}", 
                                    clienteFuncaoFuncionario.Funcionario.DataNascimento), clienteFuncaoFuncionario.Funcionario.Cpf, 
                                    clienteFuncaoFuncionario.ClienteFuncao.Funcao.Descricao, clienteFuncaoFuncionario.ClienteFuncao.Cliente.RazaoSocial, gfaea.Login, 
                                    ApplicationConstants.ATENDER_EXAME, ApplicationConstants.EXAME_PCMSO, 
                                    gheFonteAgenteExameDao.findExameByGheFonteAgenteExame(gfaea.GheFonteAgenteExame, dbConnection), 
                                    gfaea.Devolvido, (DateTime)aso.DataElaboracao, aso.Rg, null, 0, 
                                    clienteFuncaoFuncionario.Funcionario.Peso, clienteFuncaoFuncionario.Funcionario.Altura, 
                                    periodicidadeDao.findById((Int64)gfaea.Aso.Periodicidade.Id, dbConnection).Descricao, 
                                    clienteFuncaoFuncionario.Funcionario.getImc(), gfaea.DataAtendido, gfaea.Aso.Codigo, String.Empty, aso.Prioridade, !String.IsNullOrEmpty(aso.Senha) ? aso.Senha : string.Empty));
                            }
                        }
                    }
                }

                examesParaAtendimentoRetorno = new LinkedList<ItemFilaAtendimento>(examesParaAtendimento.OrderBy(delegate(ItemFilaAtendimento p) {
                    return p.DataAtendido;
                }));

                foreach (ItemFilaAtendimento itemFilaVip in filaVip)
                {
                    examesParaAtendimentoRetorno.AddFirst(itemFilaVip);
                }
            }
            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - buscaExamesAtendendo", ex);
                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }
            finally
            {
                FabricaConexao.fecharConexao();
            }

            return examesParaAtendimentoRetorno;
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public LinkedList<ItemFilaAtendimento> buscaExamesNaoFinalizados(DateTime dataInicial, DateTime dataFinal, Sala sala)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método buscaExamesNaoFinalizados");

            LinkedList<ItemFilaAtendimento> examesParaAtendimento = new LinkedList<ItemFilaAtendimento>();
            LinkedList<ItemFilaAtendimento> examesParaAtendimentoRetorno = new LinkedList<ItemFilaAtendimento>();
            NpgsqlConnection dbConnection = FabricaConexao.getConexao();
            IAsoDao asoDao = FabricaDao.getAsoDao();
            IPeriodicidadeDao periodicidadeDao = FabricaDao.getPeriodicidadeDao();
            IClienteFuncaoExameASoDao clienteFuncaoExameAsoDao = FabricaDao.getClienteFuncaoExameASoDao();
            IGheFonteAgenteExameAsoDao gheFonteAgenteExameAsoDao = FabricaDao.getGheFonteAgenteExameAsoDao();
            IGheFonteAgenteExameDao gheFonteAgenteExameDao = FabricaDao.getGheFonteAgenteExameDao();
            IClienteFuncaoExameDao clienteFuncaoExameDao = FabricaDao.getClienteFuncaoExameDao();
            IClienteFuncaoFuncionarioDao clienteFuncaoFuncionarioDao = FabricaDao.getClienteFuncaoFuncionarioDao();
            ICentroCustoDao centroCustoDao = FabricaDao.getCentroCustoDao();
            IClienteDao clienteDao = FabricaDao.getClienteDao();
            try
            {
                LinkedList<Aso> asos = asoDao.findAsoNaoFinalizadoByPeriodo(dataInicial, dataFinal, PermissionamentoFacade.usuarioAutenticado.Empresa, dbConnection);
                LinkedList<ClienteFuncaoExameASo> clienteFuncaoExameAso = new LinkedList<ClienteFuncaoExameASo>();
                LinkedList<GheFonteAgenteExameAso> gheFonteAgenteExameASo = new LinkedList<GheFonteAgenteExameAso>();
                LinkedList<ItemFilaAtendimento> filaPrioridade = new LinkedList<ItemFilaAtendimento>();
                ClienteFuncaoFuncionario clienteFuncaoFuncionario = null;
                /* ordenação padrão da fila de atendimento */
                string ordenacao = string.Empty;
                PermissionamentoFacade.mapaConfiguracoes.TryGetValue(ConfigurationConstans.ORDENACAO_ATENDIMENTO, out ordenacao);

                foreach (Aso aso in asos)
                {
                    clienteFuncaoFuncionario = clienteFuncaoFuncionarioDao.findById(aso.ClienteFuncaoFuncionario.Id, dbConnection);

                    foreach (ClienteFuncaoExameASo cfea in clienteFuncaoExameAsoDao.findAllExamesByAsoInSituacaoBySala(aso, null, false, false, false, sala, Convert.ToBoolean(ordenacao), dbConnection))
                    {
                        if (!cfea.Finalizado)
                        {
                            cfea.Aso = asoDao.findById((Int64)cfea.Aso.Id, dbConnection);
                            if (!aso.Prioridade)
                            {
                                examesParaAtendimento.AddLast(new ItemFilaAtendimento(cfea.Id, clienteFuncaoFuncionario.Funcionario.Nome, 
                                    String.Format("{0:dd/MM/yyyy}", clienteFuncaoFuncionario.Funcionario.DataNascimento), 
                                    clienteFuncaoFuncionario.Funcionario.Cpf, clienteFuncaoFuncionario.ClienteFuncao.Funcao.Descricao, 
                                    clienteFuncaoFuncionario.ClienteFuncao.Cliente.RazaoSocial, cfea.Login,
                                    ApplicationConstants.ATENDER_EXAME, ApplicationConstants.EXAME_EXTRA, 
                                    clienteFuncaoExameDao.findExameByClienteFuncaoExame(cfea.ClienteFuncaoExame, dbConnection), 
                                    cfea.Devolvido, (DateTime)aso.DataElaboracao, clienteFuncaoFuncionario.Funcionario.Rg, null, 0, 
                                    clienteFuncaoFuncionario.Funcionario.Peso, clienteFuncaoFuncionario.Funcionario.Altura, 
                                    periodicidadeDao.findById((Int64)cfea.Aso.Periodicidade.Id, dbConnection).Descricao, 
                                    clienteFuncaoFuncionario.Funcionario.getImc(), cfea.DataAtendido, 
                                    cfea.Aso.Codigo, cfea.Prestador != null ? cfea.Prestador.RazaoSocial : String.Empty, aso.Prioridade, !String.IsNullOrEmpty(aso.Senha) ? aso.Senha : string.Empty));
                            }
                            else
                            {
                                filaPrioridade.AddFirst(new ItemFilaAtendimento(cfea.Id, clienteFuncaoFuncionario.Funcionario.Nome, 
                                    String.Format("{0:dd/MM/yyyy}", clienteFuncaoFuncionario.Funcionario.DataNascimento), 
                                    clienteFuncaoFuncionario.Funcionario.Cpf, clienteFuncaoFuncionario.ClienteFuncao.Funcao.Descricao, 
                                    clienteFuncaoFuncionario.ClienteFuncao.Cliente.RazaoSocial, cfea.Login, 
                                    ApplicationConstants.ATENDER_EXAME, ApplicationConstants.EXAME_EXTRA, 
                                    clienteFuncaoExameDao.findExameByClienteFuncaoExame(cfea.ClienteFuncaoExame, dbConnection),
                                    cfea.Devolvido, (DateTime)aso.DataElaboracao, clienteFuncaoFuncionario.Funcionario.Rg, null, 0, 
                                    clienteFuncaoFuncionario.Funcionario.Peso, clienteFuncaoFuncionario.Funcionario.Altura, 
                                    periodicidadeDao.findById((Int64)cfea.Aso.Periodicidade.Id, dbConnection).Descricao, 
                                    clienteFuncaoFuncionario.Funcionario.getImc(), cfea.DataAtendido,
                                    cfea.Aso.Codigo, cfea.Prestador != null ? cfea.Prestador.RazaoSocial : String.Empty, aso.Prioridade, !String.IsNullOrEmpty(aso.Senha) ? aso.Senha : string.Empty));
                            }
                        }
                    }

                    foreach (GheFonteAgenteExameAso gfaea in gheFonteAgenteExameAsoDao.findAllExamesByAsoInSituacaoBySala(aso, null, false, false, false, sala, Convert.ToBoolean(ordenacao), dbConnection))
                    {
                        if (!gfaea.Finalizado)
                        {
                            gfaea.Aso = asoDao.findById((Int64)gfaea.Aso.Id, dbConnection);

                            if (!aso.Prioridade)
                            {
                                examesParaAtendimento.AddLast(new ItemFilaAtendimento(gfaea.Id, clienteFuncaoFuncionario.Funcionario.Nome, 
                                    String.Format("{0:dd/MM/yyyy}", clienteFuncaoFuncionario.Funcionario.DataNascimento), 
                                    clienteFuncaoFuncionario.Funcionario.Cpf, 
                                    clienteFuncaoFuncionario.ClienteFuncao.Funcao.Descricao, 
                                    clienteFuncaoFuncionario.ClienteFuncao.Cliente.RazaoSocial, gfaea.Login, 
                                    ApplicationConstants.ATENDER_EXAME, ApplicationConstants.EXAME_PCMSO, 
                                    gheFonteAgenteExameDao.findExameByGheFonteAgenteExame(gfaea.GheFonteAgenteExame, dbConnection),
                                    gfaea.Devolvido, (DateTime)aso.DataElaboracao, clienteFuncaoFuncionario.Funcionario.Rg, null, 0, 
                                    clienteFuncaoFuncionario.Funcionario.Peso, 
                                    clienteFuncaoFuncionario.Funcionario.Altura, 
                                    periodicidadeDao.findById((Int64)gfaea.Aso.Periodicidade.Id, dbConnection).Descricao, 
                                    clienteFuncaoFuncionario.Funcionario.getImc(), gfaea.DataAtendido,
                                    gfaea.Aso.Codigo, gfaea.Prestador != null ? gfaea.Prestador.RazaoSocial : String.Empty, aso.Prioridade, !String.IsNullOrEmpty(aso.Senha) ? aso.Senha : string.Empty));
                            }
                            else
                            {
                                filaPrioridade.AddFirst(new ItemFilaAtendimento(gfaea.Id, clienteFuncaoFuncionario.Funcionario.Nome, 
                                    String.Format("{0:dd/MM/yyyy}", clienteFuncaoFuncionario.Funcionario.DataNascimento), 
                                    clienteFuncaoFuncionario.Funcionario.Cpf, 
                                    clienteFuncaoFuncionario.ClienteFuncao.Funcao.Descricao, 
                                    clienteFuncaoFuncionario.ClienteFuncao.Cliente.RazaoSocial, gfaea.Login, 
                                    ApplicationConstants.ATENDER_EXAME, ApplicationConstants.EXAME_PCMSO, 
                                    gheFonteAgenteExameDao.findExameByGheFonteAgenteExame(gfaea.GheFonteAgenteExame, dbConnection),
                                    gfaea.Devolvido, (DateTime)aso.DataElaboracao, clienteFuncaoFuncionario.Funcionario.Rg, null, 0, 
                                    clienteFuncaoFuncionario.Funcionario.Peso, 
                                    clienteFuncaoFuncionario.Funcionario.Altura, 
                                    periodicidadeDao.findById((Int64)gfaea.Aso.Periodicidade.Id, dbConnection).Descricao, 
                                    clienteFuncaoFuncionario.Funcionario.getImc(), gfaea.DataAtendido,
                                    gfaea.Aso.Codigo, gfaea.Prestador != null ? gfaea.Prestador.RazaoSocial : String.Empty, aso.Prioridade, !String.IsNullOrEmpty(aso.Senha) ? aso.Senha : string.Empty));
                            }
                        }
                    }

                    if (aso.CentroCusto != null)
                    {
                        aso.CentroCusto = centroCustoDao.findById((long)aso.CentroCusto.Id, dbConnection);
                        aso.CentroCusto.Cliente = clienteDao.findById((long)aso.CentroCusto.Cliente.Id, dbConnection);
                    }
                }
                
                if (Convert.ToBoolean(ordenacao))
                {
                    /* ordenacao pela senha do atendimento */
                    
                    examesParaAtendimentoRetorno = new LinkedList<ItemFilaAtendimento>(examesParaAtendimento.OrderBy(delegate(ItemFilaAtendimento p) {
                        return p.SenhaAtendimento;
                    }));
                }
                else
                {
                     /*ordenacao pela código do atendimento */
                    examesParaAtendimentoRetorno = new LinkedList<ItemFilaAtendimento>(examesParaAtendimento.OrderBy(delegate(ItemFilaAtendimento p)
                    {
                        return p.CodigoAtendimento;
                    }));
                }

                foreach (ItemFilaAtendimento itemFilaVip in filaPrioridade)
                    examesParaAtendimentoRetorno.AddFirst(itemFilaVip);
            }
            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - buscaExamesNaoFinalizados", ex);
                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }
            finally
            {
                FabricaConexao.fecharConexao();
            }

            return examesParaAtendimentoRetorno;
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public LinkedList<ItemFilaAtendimento> buscaExamesInSituacaoByAso(Aso aso, Boolean? atendido, Boolean? finalizado, Boolean? cancelado, Boolean? devolvido, Boolean? transcrito)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método buscaExamesInSituacaoByAso");

            LinkedList<ItemFilaAtendimento> examesParaAtendimento = new LinkedList<ItemFilaAtendimento>();
            NpgsqlConnection dbConnection = FabricaConexao.getConexao();
            IAsoDao asoDao = FabricaDao.getAsoDao();
            IClienteFuncaoExameASoDao clienteFuncaoExameAsoDao = FabricaDao.getClienteFuncaoExameASoDao();
            IGheFonteAgenteExameAsoDao gheFonteAgenteExameAsoDao = FabricaDao.getGheFonteAgenteExameAsoDao();
            IGheFonteAgenteExameDao gheFonteAgenteExameDao = FabricaDao.getGheFonteAgenteExameDao();
            IClienteFuncaoExameDao clienteFuncaoExameDao = FabricaDao.getClienteFuncaoExameDao();
            IClienteFuncaoFuncionarioDao clienteFuncaoFuncionarioDao = FabricaDao.getClienteFuncaoFuncionarioDao();
            try
            {
                LinkedList<ClienteFuncaoExameASo> clienteFuncaoExameAso = new LinkedList<ClienteFuncaoExameASo>();
                LinkedList<GheFonteAgenteExameAso> gheFonteAgenteExameASo = new LinkedList<GheFonteAgenteExameAso>();

                ClienteFuncaoFuncionario clienteFuncaoFuncionario = null;
                
                clienteFuncaoFuncionario = clienteFuncaoFuncionarioDao.findById(aso.ClienteFuncaoFuncionario.Id, dbConnection);

                foreach (ClienteFuncaoExameASo cfea in clienteFuncaoExameAsoDao.findAllExamesByAsoInSituacao(aso, atendido, finalizado, cancelado, devolvido, transcrito, dbConnection))
                {
                    examesParaAtendimento.AddLast(new ItemFilaAtendimento(cfea.Id, clienteFuncaoFuncionario.Funcionario.Nome,
                        String.Format("{0:dd/MM/yyyy}", clienteFuncaoFuncionario.Funcionario.DataNascimento), 
                        clienteFuncaoFuncionario.Funcionario.Cpf, clienteFuncaoFuncionario.ClienteFuncao.Funcao.Descricao, 
                        clienteFuncaoFuncionario.ClienteFuncao.Cliente.RazaoSocial, cfea.Login, 
                        ApplicationConstants.ATENDER_EXAME, ApplicationConstants.EXAME_EXTRA, clienteFuncaoExameDao.findExameByClienteFuncaoExame(cfea.ClienteFuncaoExame, dbConnection), 
                        cfea.Devolvido, (DateTime)aso.DataElaboracao, aso.Rg, 
                        (cfea.SalaExame != null ? cfea.SalaExame.Sala.Descricao : String.Empty), 
                        (cfea.SalaExame == null ? (Int32?)null : cfea.SalaExame.Sala.Andar), 
                        clienteFuncaoFuncionario.Funcionario.Peso, clienteFuncaoFuncionario.Funcionario.Altura, 
                        cfea.Aso.Periodicidade.Descricao, clienteFuncaoFuncionario.Funcionario.getImc(), 
                        cfea.DataAtendido, cfea.Aso.Codigo,
                        cfea.Prestador != null ? cfea.Prestador.RazaoSocial : String.Empty, aso.Prioridade, !String.IsNullOrEmpty(aso.Senha) ? aso.Senha : string.Empty));
                }

                foreach (GheFonteAgenteExameAso gfaea in gheFonteAgenteExameAsoDao.findAllExamesByAsoInSituacao(aso, atendido, finalizado, cancelado, devolvido, transcrito, dbConnection))
                {
                    examesParaAtendimento.AddLast(new ItemFilaAtendimento(gfaea.Id, clienteFuncaoFuncionario.Funcionario.Nome,
                        String.Format("{0:dd/MM/yyyy}", clienteFuncaoFuncionario.Funcionario.DataNascimento), 
                        clienteFuncaoFuncionario.Funcionario.Cpf, clienteFuncaoFuncionario.ClienteFuncao.Funcao.Descricao, 
                        clienteFuncaoFuncionario.ClienteFuncao.Cliente.RazaoSocial, gfaea.Login, 
                        ApplicationConstants.ATENDER_EXAME, ApplicationConstants.EXAME_PCMSO, gheFonteAgenteExameDao.findExameByGheFonteAgenteExame(gfaea.GheFonteAgenteExame, dbConnection), 
                        gfaea.Devolvido, (DateTime)aso.DataElaboracao, aso.Rg, 
                        (gfaea.SalaExame != null ? gfaea.SalaExame.Sala.Descricao : String.Empty), 
                        (gfaea.SalaExame == null ? (Int32?)null : gfaea.SalaExame.Sala.Andar), 
                        clienteFuncaoFuncionario.Funcionario.Peso, clienteFuncaoFuncionario.Funcionario.Altura, 
                        gfaea.Aso.Periodicidade.Descricao, clienteFuncaoFuncionario.Funcionario.getImc(), 
                        gfaea.DataAtendido, gfaea.Aso.Codigo,
                        gfaea.Prestador != null ? gfaea.Prestador.RazaoSocial : String.Empty, aso.Prioridade, !String.IsNullOrEmpty(aso.Senha) ? aso.Senha : string.Empty));
                }
            }
            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - buscaExamesInSituacaoByAso", ex);
                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }
            finally
            {
                FabricaConexao.fecharConexao();
            }

            return examesParaAtendimento;
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public HashSet<ItemFilaAtendimento> updateFilaAtendimento(ItemFilaAtendimento itemFilaAtendimento, Boolean? atendido, Boolean? finalizado, Boolean? cancelado, 
            Boolean? devolvido, Int64? idSalaExame, Boolean agendaProximoAtendimento, String tipoAtendimento, Usuario usuario, Aso aso, Sala sala)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método updateFilaAtendimento");

            NpgsqlConnection dbConnection = FabricaConexao.getConexao();
            NpgsqlTransaction transaction = dbConnection.BeginTransaction();

            HashSet<ItemFilaAtendimento> itemFilaAtendimentoSet = new HashSet<ItemFilaAtendimento>();

            try
            {
                itemFilaAtendimentoSet = UpdateFilaatendimentoCore(itemFilaAtendimento, atendido, finalizado, cancelado, devolvido, idSalaExame, agendaProximoAtendimento, tipoAtendimento, usuario, aso, sala, dbConnection);

                transaction.Commit();

                return itemFilaAtendimentoSet;
            }
            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - updateFilaAtendimento", ex);
                transaction.Rollback();
                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }
            finally
            {
                FabricaConexao.fecharConexao();
            }

        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        private HashSet<ItemFilaAtendimento> updateFilaAtendimento(ItemFilaAtendimento itemFilaAtendimento, Boolean? atendido, Boolean? finalizado, Boolean? cancelado,
            Boolean? devolvido, Int64? idSalaExame, String tipoAtendimento, Usuario usuario, Aso aso, Sala sala, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método updateFilaAtendimento");

            HashSet<ItemFilaAtendimento> itemFilaAtendimentoSet = new HashSet<ItemFilaAtendimento>();

            try
            {
                itemFilaAtendimentoSet = UpdateFilaatendimentoCore(itemFilaAtendimento, atendido, finalizado, cancelado, devolvido, idSalaExame, false,
                    tipoAtendimento, usuario, aso, sala, dbConnection);

                return itemFilaAtendimentoSet;
            }
            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - updateFilaAtendimento", ex);
                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        private HashSet<ItemFilaAtendimento> UpdateFilaatendimentoCore(ItemFilaAtendimento itemFilaAtendimento, Boolean? atendido, Boolean? finalizado, Boolean? cancelado, Boolean? devolvido, Int64? idSalaExame, Boolean agendaProximoAtendimento, String tipoAtendimento, Usuario usuario, Aso aso, Sala sala, NpgsqlConnection dbConnection)
        {
            IClienteFuncaoExameASoDao clienteFuncaoExameAsoDao = FabricaDao.getClienteFuncaoExameASoDao();
            IGheFonteAgenteExameAsoDao gheFonteAgenteExameAsoDao = FabricaDao.getGheFonteAgenteExameAsoDao();
            IAcompanhamentoAtendimentoDao acompanhamentoAtendimentoDao = FabricaDao.getAcompanhamentoAtendimentoDao();
            HashSet<ItemFilaAtendimento> itemFilaAtendimentoSet = new HashSet<ItemFilaAtendimento>();

            // caso seja exame do pcmso então deverá fazer update da tabela seg_ghe_fonte_agente_exame_aso
            if (String.Equals((String)itemFilaAtendimento.TipoExame, ApplicationConstants.EXAME_PCMSO))
            {
                gheFonteAgenteExameAsoDao.updateGheFonteAgenteExameAsoDaoInFilaAtendimento(itemFilaAtendimento, atendido, finalizado, cancelado,
                    devolvido, idSalaExame, tipoAtendimento, usuario, dbConnection);
            }
            else
            {
                clienteFuncaoExameAsoDao.updateClienteFuncaoExameASoDaoInFilaAtendimento(itemFilaAtendimento, atendido, finalizado,
                    cancelado, devolvido, idSalaExame, tipoAtendimento, usuario, dbConnection);
            }

            if (agendaProximoAtendimento)
            {
                itemFilaAtendimentoSet = this.agendaProximoAtendimento(aso, sala, dbConnection);
            }

            return itemFilaAtendimentoSet;
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public Boolean ehUltimoExame(Aso aso)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método ehUltimoExame");

            NpgsqlConnection dbConnection = FabricaConexao.getConexao();

            IClienteFuncaoExameASoDao clienteFuncaoExameAsoDao = FabricaDao.getClienteFuncaoExameASoDao();
            IGheFonteAgenteExameAsoDao gheFonteAgenteExameAsoDao = FabricaDao.getGheFonteAgenteExameAsoDao();

            Boolean ehUltimoExame = false;

            try
            {
                LinkedList<ClienteFuncaoExameASo> examesExtra = clienteFuncaoExameAsoDao.findAllExamesByAsoInSituacaoNotExterno(aso, null, false, false, false, dbConnection);
                LinkedList<GheFonteAgenteExameAso> examesPcmso = gheFonteAgenteExameAsoDao.findAllExamesByAsoInSituacaoNotExterno(aso, null, false, false, false, dbConnection);

                if (examesExtra.Count == 0 && examesPcmso.Count == 0)
                {
                    ehUltimoExame = true;
                }

                return ehUltimoExame;
            }
            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - ehUltimoExame", ex);
                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }
            finally
            {
                FabricaConexao.fecharConexao();
            }
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public Aso findAsoByItemFilaAtendimento(ItemFilaAtendimento itemFilaAtendimento)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findAsoByItemFilaAtendimento");

            NpgsqlConnection dbConnection = FabricaConexao.getConexao();

            IAsoDao asoDao = FabricaDao.getAsoDao();
            ICentroCustoDao centroCustoDao = FabricaDao.getCentroCustoDao();
            IClienteDao clienteDao = FabricaDao.getClienteDao();

            Aso aso = null;
            
            try
            {
                if (String.Equals((String)itemFilaAtendimento.TipoExame, ApplicationConstants.EXAME_PCMSO))
                {
                    aso = asoDao.findAsoByGheFonteAgenteExameAsoId(itemFilaAtendimento.IdItem, dbConnection);
                    if (aso.CentroCusto != null)
                    {
                        aso.CentroCusto = centroCustoDao.findById((long)aso.CentroCusto.Id, dbConnection);
                        aso.CentroCusto.Cliente = clienteDao.findById((long)aso.CentroCusto.Id, dbConnection);
                    }

                }
                else
                {
                    aso = asoDao.findAsoByClienteFuncaoeExameAsoId(itemFilaAtendimento.IdItem, dbConnection);
                    if (aso.CentroCusto != null)
                    {
                        aso.CentroCusto = centroCustoDao.findById((long)aso.CentroCusto.Id, dbConnection);
                        aso.CentroCusto.Cliente = clienteDao.findById((long)aso.CentroCusto.Id, dbConnection);
                    }
                }
                
                return aso;
            }
            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findAsoByItemFilaAtendimento", ex);
                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }
            finally
            {
                FabricaConexao.fecharConexao();
            }
        }

        /* 
         * Regras:
         * 
         * 1 - Verificar se existe algum exame em atendimento para o aso
         * 2 - Verificar se existe sala para atender ao exame, sendo ele interno
         * 3 - Verificar se existem exames a serem atendidos
         * 4 - Se o funcionário é vip, é gravada uma data de atendimento com hora 0:00:00
         * 5 - Se existem mais exames para serem feitos com a mesma prioridade na mesma sala, colocar todos na fila
         */
        [MethodImpl(MethodImplOptions.Synchronized)]
        public HashSet<ItemFilaAtendimento> agendaProximoAtendimento(Aso aso, Sala salaAtendimento)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método agendaPróximoAtendimento");

            NpgsqlConnection dbConnection = FabricaConexao.getConexao();
            NpgsqlTransaction transaction = dbConnection.BeginTransaction();

            HashSet<ItemFilaAtendimento> itemFilaAtendimentoSet = new HashSet<ItemFilaAtendimento>();

            try
            {
                AgendaProximoAtendimentoCore(aso, salaAtendimento, itemFilaAtendimentoSet, dbConnection);

                transaction.Commit();

                return itemFilaAtendimentoSet;
            }
            catch (EncerrarProcessamentoSemErroException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - Não Reagendando Atendimento", ex);
                return itemFilaAtendimentoSet;
            }
            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - agendaPróximoAtendimento", ex);
                transaction.Rollback();
                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }
            catch (Exception e)
            {
                LogHelper.logger.Error(this.GetType().Name + " - agendaPróximoAtendimento", e);
                transaction.Rollback();
                throw new Exception(e.Message);
            }
            finally
            {
                FabricaConexao.fecharConexao();
            }
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public HashSet<ItemFilaAtendimento> agendaProximoAtendimento(Aso aso, Sala salaAtendimento, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método agendaPróximoAtendimento");

            HashSet<ItemFilaAtendimento> itemFilaAtendimentoSet = new HashSet<ItemFilaAtendimento>();

            try
            {
                AgendaProximoAtendimentoCore(aso, salaAtendimento, itemFilaAtendimentoSet, dbConnection);

                return itemFilaAtendimentoSet;
            }
            catch (EncerrarProcessamentoSemErroException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - Não Reagendando Atendimento", ex);
                return itemFilaAtendimentoSet;
            }
            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - agendaPróximoAtendimento", ex);
                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        private void AgendaProximoAtendimentoCore(Aso aso, Sala salaAtendimento, HashSet<ItemFilaAtendimento> itemFilaAtendimentoSet, NpgsqlConnection dbConnection)
        {

            IAsoDao asoDao = FabricaDao.getAsoDao();
            IGheFonteAgenteExameAsoDao gheFonteAgenteExameAsoDao = FabricaDao.getGheFonteAgenteExameAsoDao();
            IClienteFuncaoExameASoDao clienteFuncaoAsoDao = FabricaDao.getClienteFuncaoExameASoDao();
            IGheFonteAgenteExameDao gheFonteAgenteExameDao = FabricaDao.getGheFonteAgenteExameDao();
            IClienteFuncaoExameDao clienteFuncaoexameDao = FabricaDao.getClienteFuncaoExameDao();
            IClienteFuncaoFuncionarioDao clienteFuncaoFuncionarioDao = FabricaDao.getClienteFuncaoFuncionarioDao();
            IExameDao exameDao = FabricaDao.getExameDao();
            ISalaDao salaDao = FabricaDao.getSalaDao();
            ISalaExameDao salaExameDao = FabricaDao.getSalaExameDao();
            IAcompanhamentoAtendimentoDao acompanhamentoAtendimentoDao = FabricaDao.getAcompanhamentoAtendimentoDao();
            IFuncionarioDao funcionarioDao = FabricaDao.getFuncionarioDao();
            HashSet<GheFonteAgenteExameAso> gheFonteAgenteExameAsoSet = null;
            HashSet<ClienteFuncaoExameASo> clienteFuncaoexameExameAsoSet = null;
            Exame examePcmso = null;
            Exame exameExtra = null;
            Funcionario funcionario = null;
            Cliente cliente = null;
            ClienteFuncaoFuncionario clienteFuncaoFuncionario = null;
            string ordenacao;
            PermissionamentoFacade.mapaConfiguracoes.TryGetValue(ConfigurationConstans.ORDENACAO_ATENDIMENTO, out ordenacao);

            //Verificando se existe algum item em atendimento.
            if (gheFonteAgenteExameAsoDao.verificaExameAtendimento(aso, dbConnection) || (clienteFuncaoAsoDao.verificaExameAtendimento(aso, dbConnection)))
            {
                if (salaAtendimento == null)
                {
                    throw new FilaAtendimentoException("Não é possível agendar o atendimento. Já existem Exames agendados para o funcionário.");
                }
                else
                {
                    Int32 quantidadesAtendidosMesmaSalaPcmso = gheFonteAgenteExameAsoDao.findAllExamesByAsoInSituacaoBySala(aso, true, false, false, false, salaAtendimento, Convert.ToBoolean(ordenacao), dbConnection).Count();
                    Int32 quantidadesAtendidosMesmaSalaExtra = clienteFuncaoAsoDao.findAllExamesByAsoInSituacaoBySala(aso, true, false, false, false, salaAtendimento, Convert.ToBoolean(ordenacao), dbConnection).Count();
                    //São da mesma sala
                    if (quantidadesAtendidosMesmaSalaPcmso > 0 || quantidadesAtendidosMesmaSalaExtra > 0)
                    {
                        //Não escalonando item de atendimento da mesma sala
                        throw new EncerrarProcessamentoSemErroException();
                    }
                    else
                    {
                        throw new FilaAtendimentoException("Não é possível agendar o atendimento. Já existem Exames agendados para o funcionário.");
                    }
                }
            }

            //Buscando todos os exames ainda não atendidos de um ASO.
            gheFonteAgenteExameAsoSet = gheFonteAgenteExameAsoDao.buscaExamesNaoAtendidosComMaiorprioridade(aso, dbConnection);

            //Buscando todos os exames extras ainda não atendidos de um ASO.
            clienteFuncaoexameExameAsoSet = clienteFuncaoAsoDao.buscaExamesNaoAtendidosComMaiorprioridade(aso, dbConnection);

            clienteFuncaoFuncionario = clienteFuncaoFuncionarioDao.findById(aso.ClienteFuncaoFuncionario.Id, dbConnection);

            funcionario = funcionarioDao.findFuncionarioById((Int64)clienteFuncaoFuncionario.Funcionario.Id, dbConnection);
            cliente = clienteFuncaoFuncionario.ClienteFuncao.Cliente;

            //Verificando se exames do PCMSO e extras estão com a mesma ordem de prioridade
            if (gheFonteAgenteExameAsoSet.Count > 0)
            {
                examePcmso = gheFonteAgenteExameDao.findExameByGheFonteAgenteExame(gheFonteAgenteExameAsoSet.First().GheFonteAgenteExame, dbConnection);
            }

            if (clienteFuncaoexameExameAsoSet.Count > 0)
            {
                exameExtra = clienteFuncaoexameDao.findExameByClienteFuncaoExame(clienteFuncaoexameExameAsoSet.First().ClienteFuncaoExame, dbConnection);
            }

            if (examePcmso != null && exameExtra != null)
            {
                if (examePcmso.Prioridade > exameExtra.Prioridade)
                {
                    clienteFuncaoexameExameAsoSet.Clear();
                }
                else if (exameExtra.Prioridade > examePcmso.Prioridade)
                {
                    gheFonteAgenteExameAsoSet.Clear();
                }
            }

            SalaExame salaExamePcmsoEscolhido = null;
            GheFonteAgenteExameAso gheFonteAgenteExameAsoEscolhido = null;
            if (gheFonteAgenteExameAsoSet.Count > 0)
            {
                examePcmso = gheFonteAgenteExameDao.findExameByGheFonteAgenteExame(gheFonteAgenteExameAsoSet.First().GheFonteAgenteExame, dbConnection);

                salaExamePcmsoEscolhido = buscaSalaComMenosAtendimentosParaUmExameByPcmso(examePcmso, cliente.Vip, dbConnection);

                salaExamePcmsoEscolhido.Sala = salaDao.findById((Int64)salaExamePcmsoEscolhido.Sala.Id, dbConnection);
                salaExamePcmsoEscolhido.Exame = exameDao.findById((Int64)salaExamePcmsoEscolhido.Exame.Id, dbConnection);

                gheFonteAgenteExameAsoEscolhido = gheFonteAgenteExameAsoSet.First();

                foreach (GheFonteAgenteExameAso gheFonteAgenteExameAso in gheFonteAgenteExameAsoSet)
                {
                    examePcmso = gheFonteAgenteExameDao.findExameByGheFonteAgenteExame(gheFonteAgenteExameAso.GheFonteAgenteExame, dbConnection);

                    if (buscaSalaComMenosAtendimentosParaUmExameByPcmso(examePcmso, cliente.Vip, dbConnection).getPrevisaoDeAtendimento() < salaExamePcmsoEscolhido.getPrevisaoDeAtendimento())
                    {
                        salaExamePcmsoEscolhido = buscaSalaComMenosAtendimentosParaUmExameByPcmso(examePcmso, cliente.Vip, dbConnection);
                        salaExamePcmsoEscolhido.Sala = salaDao.findById((Int64)salaExamePcmsoEscolhido.Sala.Id, dbConnection);
                        salaExamePcmsoEscolhido.Exame = exameDao.findById((Int64)salaExamePcmsoEscolhido.Exame.Id, dbConnection);
                        gheFonteAgenteExameAsoEscolhido = gheFonteAgenteExameAso;
                    }
                }
            }

            SalaExame salaExameExtraEscolhido = null;
            ClienteFuncaoExameASo clienteFuncaoExameAsoEscolhido = null;

            if (clienteFuncaoexameExameAsoSet.Count > 0)
            {
                exameExtra = clienteFuncaoexameDao.findExameByClienteFuncaoExame(clienteFuncaoexameExameAsoSet.First().ClienteFuncaoExame, dbConnection);

                salaExameExtraEscolhido = buscaSalaComMenosAtendimentosParaUmExameByExameExtra(exameExtra, cliente.Vip, dbConnection);

                salaExameExtraEscolhido.Sala = salaDao.findById((Int64)salaExameExtraEscolhido.Sala.Id, dbConnection);
                salaExameExtraEscolhido.Exame = exameDao.findById((Int64)salaExameExtraEscolhido.Exame.Id, dbConnection);

                clienteFuncaoExameAsoEscolhido = clienteFuncaoexameExameAsoSet.First();

                foreach (ClienteFuncaoExameASo clienteFuncaoExameASo in clienteFuncaoexameExameAsoSet)
                {
                    exameExtra = clienteFuncaoexameDao.findExameByClienteFuncaoExame(clienteFuncaoExameASo.ClienteFuncaoExame, dbConnection);

                    if (buscaSalaComMenosAtendimentosParaUmExameByPcmso(exameExtra, cliente.Vip, dbConnection).getPrevisaoDeAtendimento() < salaExameExtraEscolhido.getPrevisaoDeAtendimento())
                    {
                        salaExameExtraEscolhido = buscaSalaComMenosAtendimentosParaUmExameByPcmso(exameExtra, cliente.Vip, dbConnection);
                        salaExameExtraEscolhido.Sala = salaDao.findById((Int64)salaExameExtraEscolhido.Sala.Id, dbConnection);
                        salaExameExtraEscolhido.Exame = exameDao.findById((Int64)salaExameExtraEscolhido.Exame.Id, dbConnection);
                        clienteFuncaoExameAsoEscolhido = clienteFuncaoExameASo;
                    }
                }
            }

            Exame exame = null;
            Sala sala = null;
            // inicia atendimento.
            if (gheFonteAgenteExameAsoEscolhido != null && clienteFuncaoExameAsoEscolhido != null)
            {
                if (salaExameExtraEscolhido.Sala.Id == salaExamePcmsoEscolhido.Sala.Id)
                {
                    //Criando acompanhamento
                    if (isPrimeiroAtendimento(aso, dbConnection))
                    {
                        sala = salaDao.findById((Int64)salaExamePcmsoEscolhido.Sala.Id, dbConnection);
                    }
                    
                    //Colocar o gheFonteAgenteExameAsoEscolhido em atendimento
                    gheFonteAgenteExameAsoDao.IniciarAtendimentoExame(gheFonteAgenteExameAsoEscolhido, salaExamePcmsoEscolhido, clienteFuncaoFuncionario.Vip, dbConnection);
                    
                    itemFilaAtendimentoSet.Add(new ItemFilaAtendimento(gheFonteAgenteExameAsoEscolhido.Id,
                        null, null, null, null, null, null, null, null, salaExamePcmsoEscolhido.Exame, false, DateTime.Now, null, salaExamePcmsoEscolhido.Sala.Descricao,
                        salaExamePcmsoEscolhido.Sala.Andar, null, null, null, null, null, null, String.Empty, false, string.Empty));


                    //Iniciando atendimento dos exames com mesma prioridade atendidos na mesma sala
                    foreach (GheFonteAgenteExameAso gheFonteAgenteExameAsoMesmaSala in gheFonteAgenteExameAsoDao.buscaExamesNaoAtendidosComMesmaPrioridadeAtendidosNaMesmaSala(aso, exameDao.findById((Int64)salaExamePcmsoEscolhido.Exame.Id, dbConnection).Prioridade, dbConnection))
                    {
                        exame = gheFonteAgenteExameDao.findExameByGheFonteAgenteExame(gheFonteAgenteExameAsoMesmaSala.GheFonteAgenteExame, dbConnection);

                        if (salaExameDao.verificaSalaAtendeExame(salaExamePcmsoEscolhido.Sala, exame, dbConnection))
                        {

                            gheFonteAgenteExameAsoDao.IniciarAtendimentoExame(gheFonteAgenteExameAsoMesmaSala, salaExamePcmsoEscolhido, clienteFuncaoFuncionario.Vip, dbConnection);

                            itemFilaAtendimentoSet.Add(new ItemFilaAtendimento(gheFonteAgenteExameAsoMesmaSala.Id,
                                null, null, null, null, null, null, null, null, exame, false, DateTime.Now, null, salaExamePcmsoEscolhido.Sala.Descricao,
                                salaExamePcmsoEscolhido.Sala.Andar, null, null, null, null, null, null,String.Empty, false, string.Empty));
                        }
                    }

                    //Colocar o clienteFuncaoAsoDao em atendimento
                    clienteFuncaoAsoDao.IniciarAtendimentoExame(clienteFuncaoExameAsoEscolhido, salaExameExtraEscolhido, clienteFuncaoFuncionario.Vip, dbConnection);

                    itemFilaAtendimentoSet.Add(new ItemFilaAtendimento(clienteFuncaoExameAsoEscolhido.Id,
                         null, null, null, null, null, null, null, null, salaExameExtraEscolhido.Exame, false, DateTime.Now, null, salaExameExtraEscolhido.Sala.Descricao,
                         salaExameExtraEscolhido.Sala.Andar, null, null, null, null, null, null,String.Empty, false, string.Empty));

                    //Iniciando atendimento dos exames com mesma prioridade atendidos na mesma sala
                    foreach (ClienteFuncaoExameASo clienteFuncaoExameASoMesmaSala in clienteFuncaoAsoDao.buscaExamesNaoAtendidosComMesmaPrioridadeAtendidosNaMesmaSala(aso, exameDao.findById((Int64)salaExameExtraEscolhido.Exame.Id, dbConnection).Prioridade, dbConnection))
                    {
                        exame = clienteFuncaoexameDao.findExameByClienteFuncaoExame(clienteFuncaoExameASoMesmaSala.ClienteFuncaoExame, dbConnection);

                        if (salaExameDao.verificaSalaAtendeExame(salaExameExtraEscolhido.Sala, exame, dbConnection))
                        {
                            clienteFuncaoAsoDao.IniciarAtendimentoExame(clienteFuncaoExameASoMesmaSala, salaExameExtraEscolhido, clienteFuncaoFuncionario.Vip, dbConnection);

                            itemFilaAtendimentoSet.Add(new ItemFilaAtendimento(clienteFuncaoExameASoMesmaSala.Id,
                             null, null, null, null, null, null, null, null, exame, false, DateTime.Now, null, salaExameExtraEscolhido.Sala.Descricao,
                             salaExameExtraEscolhido.Sala.Andar, null, null, null, null, null, null, String.Empty, false, string.Empty));
                        }
                    }
                }
                else
                {
                    if (salaExameExtraEscolhido.getPrevisaoDeAtendimento() > salaExamePcmsoEscolhido.getPrevisaoDeAtendimento())
                    {
                        //Criando acompanhamento
                        if (isPrimeiroAtendimento(aso, dbConnection))
                        {
                            sala = salaDao.findById((Int64)salaExamePcmsoEscolhido.Sala.Id, dbConnection);
                        }
                        
                        //Colocar o gheFonteAgenteExameAsoEscolhido em atendimento
                        gheFonteAgenteExameAsoDao.IniciarAtendimentoExame(gheFonteAgenteExameAsoEscolhido, salaExamePcmsoEscolhido, clienteFuncaoFuncionario.Vip, dbConnection);

                        itemFilaAtendimentoSet.Add(new ItemFilaAtendimento(gheFonteAgenteExameAsoEscolhido.Id,
                            null, null, null, null, null, null, null, null, salaExamePcmsoEscolhido.Exame, false, DateTime.Now, null, salaExamePcmsoEscolhido.Sala.Descricao,
                            salaExamePcmsoEscolhido.Sala.Andar, null, null, null, null, null, null, String.Empty, false, string.Empty));


                        //Iniciando atendimento dos exames com mesma prioridade atendidos na mesma sala
                        foreach (GheFonteAgenteExameAso gheFonteAgenteExameAsoMesmaSala in gheFonteAgenteExameAsoDao.buscaExamesNaoAtendidosComMesmaPrioridadeAtendidosNaMesmaSala(aso, exameDao.findById((Int64)salaExamePcmsoEscolhido.Exame.Id, dbConnection).Prioridade, dbConnection))
                        {
                            exame = gheFonteAgenteExameDao.findExameByGheFonteAgenteExame(gheFonteAgenteExameAsoMesmaSala.GheFonteAgenteExame, dbConnection);

                            if (salaExameDao.verificaSalaAtendeExame(salaExamePcmsoEscolhido.Sala, exame, dbConnection))
                            {
                                gheFonteAgenteExameAsoDao.IniciarAtendimentoExame(gheFonteAgenteExameAsoMesmaSala, salaExamePcmsoEscolhido, clienteFuncaoFuncionario.Vip, dbConnection);

                                itemFilaAtendimentoSet.Add(new ItemFilaAtendimento(gheFonteAgenteExameAsoMesmaSala.Id,
                                    null, null, null, null, null, null, null, null, exame, false, DateTime.Now, null, salaExamePcmsoEscolhido.Sala.Descricao,
                                    salaExamePcmsoEscolhido.Sala.Andar, null, null, null, null, null, null, String.Empty, false, string.Empty));
                            }
                        }
                    }
                    else
                    {
                        //Criando acompanhamento
                        if (isPrimeiroAtendimento(aso, dbConnection))
                        {
                            sala = salaDao.findById((Int64)salaExameExtraEscolhido.Sala.Id, dbConnection);
                        }

                        //Colocar o clienteFuncaoAsoDao em atendimento
                        clienteFuncaoAsoDao.IniciarAtendimentoExame(clienteFuncaoExameAsoEscolhido, salaExameExtraEscolhido, clienteFuncaoFuncionario.Vip, dbConnection);

                        itemFilaAtendimentoSet.Add(new ItemFilaAtendimento(clienteFuncaoExameAsoEscolhido.Id,
                             null, null, null, null, null, null, null, null, salaExameExtraEscolhido.Exame, false, DateTime.Now, null, salaExameExtraEscolhido.Sala.Descricao,
                             salaExameExtraEscolhido.Sala.Andar, null, null, null, null, null, null, String.Empty, false, string.Empty));

                        //Iniciando atendimento dos exames com mesma prioridade atendidos na mesma sala
                        foreach (ClienteFuncaoExameASo clienteFuncaoExameASoMesmaSala in clienteFuncaoAsoDao.buscaExamesNaoAtendidosComMesmaPrioridadeAtendidosNaMesmaSala(aso, exameDao.findById((Int64)salaExameExtraEscolhido.Exame.Id, dbConnection).Prioridade, dbConnection))
                        {
                            exame = clienteFuncaoexameDao.findExameByClienteFuncaoExame(clienteFuncaoExameASoMesmaSala.ClienteFuncaoExame, dbConnection);

                            if (salaExameDao.verificaSalaAtendeExame(salaExameExtraEscolhido.Sala, exame, dbConnection))
                            {
                                clienteFuncaoAsoDao.IniciarAtendimentoExame(clienteFuncaoExameASoMesmaSala, salaExameExtraEscolhido, clienteFuncaoFuncionario.Vip, dbConnection);

                                itemFilaAtendimentoSet.Add(new ItemFilaAtendimento(clienteFuncaoExameASoMesmaSala.Id,
                                 null, null, null, null, null, null, null, null, exame, false, DateTime.Now, null, salaExameExtraEscolhido.Sala.Descricao,
                                 salaExameExtraEscolhido.Sala.Andar, null, null, null, null, null, null, String.Empty, false, string.Empty));
                            }
                        }
                    }
                }
            }
            else
            {
                if (gheFonteAgenteExameAsoEscolhido != null)
                {
                    //Criando acompanhamento
                    if (isPrimeiroAtendimento(aso, dbConnection))
                    {
                        sala = salaDao.findById((Int64)salaExamePcmsoEscolhido.Sala.Id, dbConnection);
                    }

                    //Colocar o gheFonteAgenteExameAsoEscolhido em atendimento
                    gheFonteAgenteExameAsoDao.IniciarAtendimentoExame(gheFonteAgenteExameAsoEscolhido, salaExamePcmsoEscolhido, clienteFuncaoFuncionario.Vip, dbConnection);


                    itemFilaAtendimentoSet.Add(new ItemFilaAtendimento(gheFonteAgenteExameAsoEscolhido.Id,
                        null, null, null, null, null, null, null, null, salaExamePcmsoEscolhido.Exame, false, DateTime.Now, null, salaExamePcmsoEscolhido.Sala.Descricao,
                        salaExamePcmsoEscolhido.Sala.Andar, null, null, null, null, null,null, String.Empty, false, string.Empty));


                    //Iniciando atendimento dos exames com mesma prioridade atendidos na mesma sala
                    foreach (GheFonteAgenteExameAso gheFonteAgenteExameAsoMesmaSala in gheFonteAgenteExameAsoDao.buscaExamesNaoAtendidosComMesmaPrioridadeAtendidosNaMesmaSala(aso, exameDao.findById((Int64)salaExamePcmsoEscolhido.Exame.Id, dbConnection).Prioridade, dbConnection))
                    {
                        exame = gheFonteAgenteExameDao.findExameByGheFonteAgenteExame(gheFonteAgenteExameAsoMesmaSala.GheFonteAgenteExame, dbConnection);

                        if (salaExameDao.verificaSalaAtendeExame(salaExamePcmsoEscolhido.Sala, exame, dbConnection))
                        {
                            gheFonteAgenteExameAsoDao.IniciarAtendimentoExame(gheFonteAgenteExameAsoMesmaSala, salaExamePcmsoEscolhido, clienteFuncaoFuncionario.Vip, dbConnection);

                            itemFilaAtendimentoSet.Add(new ItemFilaAtendimento(gheFonteAgenteExameAsoMesmaSala.Id,
                                null, null, null, null, null, null, null, null, exame, false, DateTime.Now, null, salaExamePcmsoEscolhido.Sala.Descricao,
                                salaExamePcmsoEscolhido.Sala.Andar, null, null, null, null, null, null, String.Empty, false, string.Empty));
                        }
                    }
                }
                else
                {
                    if (clienteFuncaoExameAsoEscolhido != null)
                    {
                        //Criando acompanhamento
                        if (isPrimeiroAtendimento(aso, dbConnection))
                        {
                            sala = salaDao.findById((Int64)salaExameExtraEscolhido.Sala.Id, dbConnection);
                        }
                        
                        //Colocar o clienteFuncaoAsoDao em atendimento
                        clienteFuncaoAsoDao.IniciarAtendimentoExame(clienteFuncaoExameAsoEscolhido, salaExameExtraEscolhido, clienteFuncaoFuncionario.Vip, dbConnection);

                        itemFilaAtendimentoSet.Add(new ItemFilaAtendimento(clienteFuncaoExameAsoEscolhido.Id,
                             null, null, null, null, null, null, null, null, salaExameExtraEscolhido.Exame, false, DateTime.Now, null, salaExameExtraEscolhido.Sala.Descricao,
                             salaExameExtraEscolhido.Sala.Andar, null, null, null, null, null,null, String.Empty, false, string.Empty));


                        //Iniciando atendimento dos exames com mesma prioridade atendidos na mesma sala
                        foreach (ClienteFuncaoExameASo clienteFuncaoExameASoMesmaSala in clienteFuncaoAsoDao.buscaExamesNaoAtendidosComMesmaPrioridadeAtendidosNaMesmaSala(aso, exameDao.findById((Int64)salaExameExtraEscolhido.Exame.Id, dbConnection).Prioridade, dbConnection))
                        {
                            exame = clienteFuncaoexameDao.findExameByClienteFuncaoExame(clienteFuncaoExameASoMesmaSala.ClienteFuncaoExame, dbConnection);

                            if (salaExameDao.verificaSalaAtendeExame(salaExameExtraEscolhido.Sala, exame, dbConnection))
                            {
                                clienteFuncaoAsoDao.IniciarAtendimentoExame(clienteFuncaoExameASoMesmaSala, salaExameExtraEscolhido, clienteFuncaoFuncionario.Vip, dbConnection);

                                itemFilaAtendimentoSet.Add(new ItemFilaAtendimento(clienteFuncaoExameASoMesmaSala.Id,
                               null, null, null, null, null, null, null, null, exame, false, DateTime.Now, null, salaExameExtraEscolhido.Sala.Descricao,
                               salaExameExtraEscolhido.Sala.Andar, null, null, null, null, null, null, String.Empty, false, string.Empty));
                            }
                        }
                    }
                }
            }
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        private Boolean isPrimeiroAtendimento(Aso aso, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método isPrimeiroAtendimento");
            
            IClienteFuncaoExameASoDao clienteFuncaoExameAsoDao = FabricaDao.getClienteFuncaoExameASoDao();
            IGheFonteAgenteExameAsoDao gheFonteAgenteExameAsoDao = FabricaDao.getGheFonteAgenteExameAsoDao();

            Boolean primeiroExamePcmso = true;
            Boolean primeiroExameExtra = true;

            try
            {
                primeiroExamePcmso = gheFonteAgenteExameAsoDao.isPrimeiroAtendimento(aso, dbConnection);
                primeiroExameExtra = clienteFuncaoExameAsoDao.isPrimeiroAtendimento(aso, dbConnection);

                return (primeiroExamePcmso && primeiroExameExtra);
            }
            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - isPrimeiroAtendimento", ex);
                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        private SalaExame buscaSalaComMenosAtendimentosParaUmExameByPcmso(Exame exame, Boolean clienteVip, NpgsqlConnection dbConnection)
        {
            SalaExame salaExame = null;
            ISalaExameDao salaExameDao = FabricaDao.getSalaExameDao();
            IGheFonteAgenteExameAsoDao gheFonteAgenteExameAsoDao = FabricaDao.getGheFonteAgenteExameAsoDao();
            HashSet<SalaExame> salaExameSet = new HashSet<SalaExame>();
            //Buscar as salas que atendam ao exame considerando vip ou não.
            //Primeiro Buscando salas vip.
            if (clienteVip)
            {
                salaExameSet = salaExameDao.buscaSalasAtendemExame(exame, true, dbConnection);
                
                if (salaExameSet.Count == 0)
                {
                    salaExameSet = salaExameDao.buscaSalasAtendemExame(exame, false, dbConnection);
                }
            }
            else
            {
                salaExameSet = salaExameDao.buscaSalasAtendemExame(exame, false, dbConnection);
            }

            if (salaExameSet.Count == 0)
            {
                throw new FilaAtendimentoException("Não é possível agendar o atendimento. Não existe uma sala para atender o exame "+ exame.Descricao +".");
            }
            else
            {
                if (salaExameSet.Count == 1)
                {
                    salaExame = salaExameSet.First();
                    salaExame.QuantidadeAtendimentos = gheFonteAgenteExameAsoDao.buscaQuantidadesExamesSendoAtendidosEmUmaSala(salaExame, dbConnection);
                }
                else
                { 
                    salaExame = salaExameSet.First();
                    salaExame.QuantidadeAtendimentos = gheFonteAgenteExameAsoDao.buscaQuantidadesExamesSendoAtendidosEmUmaSala(salaExame, dbConnection);
                    
                    foreach (SalaExame salaExameProcurada in salaExameSet)
                    {
                        salaExameProcurada.QuantidadeAtendimentos = gheFonteAgenteExameAsoDao.buscaQuantidadesExamesSendoAtendidosEmUmaSala(salaExameProcurada, dbConnection);

                        if (salaExame.getPrevisaoDeAtendimento() > salaExameProcurada.getPrevisaoDeAtendimento())
                        {
                            salaExame = salaExameProcurada;
                        }
                    }
                }
            }

            return salaExame;
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        private SalaExame buscaSalaComMenosAtendimentosParaUmExameByExameExtra(Exame exame, Boolean clienteVip, NpgsqlConnection dbConnection)
        {
            SalaExame salaExame = null;
            ISalaExameDao salaExameDao = FabricaDao.getSalaExameDao();
            IClienteFuncaoExameASoDao clienteFuncaoExameAsoDao = FabricaDao.getClienteFuncaoExameASoDao();
            HashSet<SalaExame> salaExameSet = new HashSet<SalaExame>();
            //Buscar as salas que atendam ao exame considerando vip ou não.
            //Primeiro Buscando salas vip.
            if (clienteVip)
            {
                salaExameSet = salaExameDao.buscaSalasAtendemExame(exame, true, dbConnection);

                if (salaExameSet.Count == 0)
                {
                    salaExameSet = salaExameDao.buscaSalasAtendemExame(exame, false, dbConnection);
                }
            }
            else
            {
                salaExameSet = salaExameDao.buscaSalasAtendemExame(exame, false, dbConnection);
            }

            if (salaExameSet.Count == 0)
            {
                throw new FilaAtendimentoException("Não existe sala para atender o exame " + exame.Descricao);
            }
            else
            {
                if (salaExameSet.Count == 1)
                {
                    salaExame = salaExameSet.First();
                    salaExame.QuantidadeAtendimentos = clienteFuncaoExameAsoDao.buscaQuantidadesExamesSendoAtendidosEmUmaSala(salaExame, dbConnection);
                }
                else
                {
                    salaExame = salaExameSet.First();
                    salaExame.QuantidadeAtendimentos = clienteFuncaoExameAsoDao.buscaQuantidadesExamesSendoAtendidosEmUmaSala(salaExame, dbConnection);

                    foreach (SalaExame salaExameProcurada in salaExameSet)
                    {
                        salaExameProcurada.QuantidadeAtendimentos = clienteFuncaoExameAsoDao.buscaQuantidadesExamesSendoAtendidosEmUmaSala(salaExameProcurada, dbConnection);

                        if (salaExame.getPrevisaoDeAtendimento() > salaExameProcurada.getPrevisaoDeAtendimento())
                        {
                            salaExame = salaExameProcurada;
                        }
                    }
                }
            }

            return salaExame;
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public SalaExame insertSalaExame(SalaExame salaExame)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método insertSalaExame");

            SalaExame novaSalaExame = null;

            NpgsqlConnection dbConnection = FabricaConexao.getConexao();

            ISalaExameDao salaExameDao = FabricaDao.getSalaExameDao();

            NpgsqlTransaction transaction = dbConnection.BeginTransaction();

            try
            {
                novaSalaExame = salaExameDao.insert(salaExame, dbConnection);
                transaction.Commit();   
            }
            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - insertSalaExame", ex);
                transaction.Rollback();
                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }
            finally
            {
                FabricaConexao.fecharConexao();
            }

            return novaSalaExame;
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public void deleteSalaExame(SalaExame salaExame)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método deleteSalaExame");

            NpgsqlConnection dbConnection = FabricaConexao.getConexao();
            ISalaExameDao salaExameDao = FabricaDao.getSalaExameDao();
            NpgsqlTransaction transaction = dbConnection.BeginTransaction();

            try
            {
                // criar verificacao para saber se a salaExame pode ser excluida.

                if (salaExameDao.verificaPodeExcluirSalaExame(salaExame, dbConnection))
                {
                    salaExame.Situacao = ApplicationConstants.DESATIVADO;
                    salaExameDao.update(salaExame, dbConnection);
                }
                else
                    salaExameDao.delete(salaExame, dbConnection);

                transaction.Commit();

            }
            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - deleteSalaExame", ex);
                transaction.Rollback();
                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }
            finally
            {
                FabricaConexao.fecharConexao();
            }
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public HashSet<Exame> buscaExamesBySala(Sala sala)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método deleteSalaExame");

            NpgsqlConnection dbConnection = FabricaConexao.getConexao();
            ISalaExameDao salaExameDao = FabricaDao.getSalaExameDao();
            
            try
            {
                return salaExameDao.buscaExamesBySala(sala, dbConnection);
            }
            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - deleteSalaExame", ex);
                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }
            finally
            {
                FabricaConexao.fecharConexao();
            }
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public SalaExame buscaSalaExamesBySala(Sala sala)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método buscaSalaExamesBySala");

            NpgsqlConnection dbConnection = FabricaConexao.getConexao();
            ISalaExameDao salaExameDao = FabricaDao.getSalaExameDao();

            try
            {
                return salaExameDao.buscaSalaExameBySala(sala, dbConnection);
            }
            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - buscaSalaExamesBySala", ex);
                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }
            finally
            {
                FabricaConexao.fecharConexao();
            }
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public void finalizaAtendimento(ItemFilaAtendimento itemFila, Aso aso, SalaExame salaExame)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método finalizaAtendimento");
            NpgsqlConnection dbConnection = FabricaConexao.getConexao();
            IGheFonteAgenteExameAsoDao gheFonteAgenteExameAsoDao = FabricaDao.getGheFonteAgenteExameAsoDao();
            IClienteFuncaoExameASoDao clienteFuncaoExameAsoDao = FabricaDao.getClienteFuncaoExameASoDao();

            NpgsqlTransaction transaction = dbConnection.BeginTransaction();

            try
            {

                // verificando que item está em atendimento.
                // PCMSO ou Exame extra
                if (String.Equals(itemFila.TipoExame, ApplicationConstants.EXAME_PCMSO))
                {
                    //verificando se o item da fila está em atendimento no momento.
                    if (!gheFonteAgenteExameAsoDao.verificaGheFonteAgenteExameASOInSituacao((Int64)itemFila.IdItem, true, 
                        false, null, null, dbConnection))
                    {

                        // verificando se o item da fila já foi finalizado.
                        if (!gheFonteAgenteExameAsoDao.verificaGheFonteAgenteExameASOInSituacao((Int64)itemFila.IdItem, true,
                            true, null, null, dbConnection))
                        {
                            // finalizando o item da fila de atendimento.
                            this.updateFilaAtendimento(itemFila, true, true, null, false, salaExame == null ? null : salaExame.Id, 
                                ApplicationConstants.FINALIZAR_EXAME, PermissionamentoFacade.usuarioAutenticado, aso, 
                                salaExame == null ? null : salaExame.Sala, dbConnection);
                        }
                        else
                        {
                            throw new FilaAtendimentoException(FilaAtendimentoException.msg2);
                        }
                        
                    }
                    else
                    {
                        throw new FilaAtendimentoException(FilaAtendimentoException.msg1);
                    }
                }
                else
                {
                    //Verificando se o iteM da fila está em atendimento no momento.
                    if (!clienteFuncaoExameAsoDao.verificaClienteFuncaoExameAsoInSituacao((Int64)itemFila.IdItem, (Boolean)true,
                        (Boolean)false, null, null, dbConnection))
                    {

                        //verificando se o item já foi finalizado.

                        if (!clienteFuncaoExameAsoDao.verificaClienteFuncaoExameAsoInSituacao((Int64)itemFila.IdItem, true, true,
                            null, null, dbConnection))
                        {
                            // finalizadndo o item da fila de atendimento.
                            this.updateFilaAtendimento(itemFila, true, true, null, false, salaExame == null ? null : salaExame.Id,
                                ApplicationConstants.FINALIZAR_EXAME, PermissionamentoFacade.usuarioAutenticado, aso,
                                salaExame == null ? null : salaExame.Sala, dbConnection);
                        }
                        else
                        {
                            throw new FilaAtendimentoException(FilaAtendimentoException.msg2);
                            
                        }
                    }
                    else
                    {
                        throw new FilaAtendimentoException(FilaAtendimentoException.msg1);
                    }
                }

                transaction.Commit();

            }
            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - finalizaAtendimento", ex);
                transaction.Rollback();
                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }
            finally
            {
                FabricaConexao.fecharConexao();
            }
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public void cancelaAtendimento(ItemFilaAtendimento itemFila, Aso atendimento, Boolean exameRealizado, GheFonteAgenteExameAso gheFonteAgenteExameAso, ClienteFuncaoExameASo clienteFuncaoExameAso)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método finalizaAtendimento");
            NpgsqlConnection dbConnection = FabricaConexao.getConexao();
            
            IGheFonteAgenteExameAsoDao gheFonteAgenteExameAsoDao = FabricaDao.getGheFonteAgenteExameAsoDao();
            IClienteFuncaoExameASoDao clienteFuncaoExameAsoDao = FabricaDao.getClienteFuncaoExameASoDao();
            IAsoDao asoDao = FabricaDao.getAsoDao();
            IMovimentoDao movimentoDao = FabricaDao.getMovimentoDao();
            IEstudoDao estudoDao = FabricaDao.getEstudoDao();

            NpgsqlTransaction transaction = dbConnection.BeginTransaction();

            try
            {
                /*  verificando que item está em atendimento. PCMSO ou Exame extra */
                if (String.Equals(itemFila.TipoExame, ApplicationConstants.EXAME_PCMSO))
                {
                    gheFonteAgenteExameAso = gheFonteAgenteExameAsoDao.findById((Int64)itemFila.IdItem, dbConnection);

                    if (!exameRealizado)
                    {
                        /* verificando se o item da fila está em atendimento no momento. */
                        if (gheFonteAgenteExameAsoDao.verificaGheFonteAgenteExameASOInSituacao((Int64)itemFila.IdItem, true, false, null, null, dbConnection))
                           throw new FilaAtendimentoException(FilaAtendimentoException.msg1);
                        
                        /* verificando se o item da fila já foi finalizado. */
                        if (gheFonteAgenteExameAsoDao.verificaGheFonteAgenteExameASOInSituacao((Int64)itemFila.IdItem, true, true, null, null, dbConnection))
                                throw new FilaAtendimentoException(FilaAtendimentoException.msg2);
                    }

                    /* verificando se o item da fila já foi cancelado. */
                    if (gheFonteAgenteExameAsoDao.verificaGheFonteAgenteExameASOInSituacao((Int64)itemFila.IdItem, null, null, true, null, dbConnection))
                        throw new FilaAtendimentoException(FilaAtendimentoException.msg3);

                    CancelaAtendimentoCore(itemFila, atendimento, dbConnection, asoDao);

                    /* procurando o movimento para lançamento do estorno e seu cancelamento */
                    Movimento movimentoProcurado = movimentoDao.findMovimentoByProdutoOrExame(new Movimento(null, gheFonteAgenteExameAso, null, null, null, null, null, null, ApplicationConstants.PENDENTE, null, null, null, null, null, null, null, null, null, null, DateTime.Now, atendimento.CentroCusto, null, null), dbConnection);

                    if (movimentoProcurado != null)
                    {
                        movimentoDao.changeStatus(movimentoProcurado, ApplicationConstants.CONSTRUCAO, dbConnection);
                        /* criando novo movimento com valor contrário para contabilidade do movimento.
                         * inserindo no movimento */
                            
                        movimentoDao.insert(new Movimento(null, gheFonteAgenteExameAso, null, movimentoProcurado.Cliente, null, null, DateTime.Now, null, ApplicationConstants.CONSTRUCAO, movimentoProcurado.PrecoUnit * -1, null, null, atendimento, 1, PermissionamentoFacade.usuarioAutenticado, PermissionamentoFacade.usuarioAutenticado.Empresa, movimentoProcurado.CustoUnitario, estudoDao.pcmsoIsUnidade((Estudo)atendimento.Estudo, dbConnection) ? atendimento.Estudo.ClienteContratante : null, null, DateTime.Now, atendimento.CentroCusto, movimentoProcurado.ContratoExame, null), dbConnection);
                    }
                    
                }
                else
                {
                    clienteFuncaoExameAso = clienteFuncaoExameAsoDao.findById((Int64)itemFila.IdItem, dbConnection);

                    if (!exameRealizado)
                    {
                        /* Verificando se o item da fila está em atendimento no momento. */
                        if (clienteFuncaoExameAsoDao.verificaClienteFuncaoExameAsoInSituacao((Int64)itemFila.IdItem, (Boolean)true, (Boolean)false, null, null, dbConnection))
                            throw new FilaAtendimentoException(FilaAtendimentoException.msg1);

                        /* verificando se o item já foi finalizado. */
                        if (clienteFuncaoExameAsoDao.verificaClienteFuncaoExameAsoInSituacao((Int64)itemFila.IdItem, true, true, null, null, dbConnection))
                            throw new FilaAtendimentoException(FilaAtendimentoException.msg2);

                    }

                    /* verificando se o item já está cancelado. */
                    if (clienteFuncaoExameAsoDao.verificaClienteFuncaoExameAsoInSituacao((Int64)itemFila.IdItem, null, null, true, null, dbConnection))
                        throw new FilaAtendimentoException(FilaAtendimentoException.msg3);
                    
                    CancelaAtendimentoCore(itemFila, atendimento, dbConnection, asoDao);

                    /* item é um exame extra */
                    
                    Movimento movimentoProcurado = movimentoDao.findMovimentoByProdutoOrExame(new Movimento(null, null, clienteFuncaoExameAso, null, null, null, null, null, ApplicationConstants.PENDENTE, null, null, null, null, null, null, null, null, null, null, DateTime.Now, null, null, null), dbConnection);

                    if (movimentoProcurado != null)
                    {
                        movimentoDao.changeStatus(movimentoProcurado, ApplicationConstants.CONSTRUCAO, dbConnection);
                        /* criando novo movimento com valor contrário para contabilidade do movimento. */

                        movimentoDao.insert(new Movimento(null, null, clienteFuncaoExameAso, movimentoProcurado.Cliente, null, null, DateTime.Now, null, ApplicationConstants.CONSTRUCAO, movimentoProcurado.PrecoUnit * -1, null, null, atendimento, 1, PermissionamentoFacade.usuarioAutenticado, PermissionamentoFacade.usuarioAutenticado.Empresa, movimentoProcurado.CustoUnitario, estudoDao.pcmsoIsUnidade((Estudo)atendimento.Estudo, dbConnection) ? atendimento.Estudo.ClienteContratante : null, null, DateTime.Now, atendimento.CentroCusto, movimentoProcurado.ContratoExame, null), dbConnection);
                    }
                }

                transaction.Commit();

            }
            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - finalizaAtendimento", ex);
                transaction.Rollback();
                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }
            finally
            {
                FabricaConexao.fecharConexao();
            }
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        private void CancelaAtendimentoCore(ItemFilaAtendimento itemFila, Aso aso, NpgsqlConnection dbConnection, IAsoDao asoDao)
        {
            Dictionary<string, string> configuracao = PermissionamentoFacade.mapaConfiguracoes;
            string cancelaExameAposAtendimentoFinalizado;
            configuracao.TryGetValue(ConfigurationConstans.CANCELA_EXAME_APOS_FINALIZADO_ATENDIMENTO, out cancelaExameAposAtendimentoFinalizado);

            if (Convert.ToBoolean(cancelaExameAposAtendimentoFinalizado))
            {
                // finalizando o item da fila de atendimento.
                this.updateFilaAtendimento(itemFila, null, null, true, null, null, ApplicationConstants.CANCELAR_EXAME, PermissionamentoFacade.usuarioAutenticado, aso, null, dbConnection);
            }
            else
            {
                // verificando novo status do aso.
                Aso asoBuscar = asoDao.findById((Int64)aso.Id, dbConnection);
                if (!String.Equals(asoBuscar.Situacao, ApplicationConstants.FECHADO))
                {
                    // finalizadndo o item da fila de atendimento.
                    this.updateFilaAtendimento(itemFila, null, null, true, null, null, ApplicationConstants.CANCELAR_EXAME,
                        PermissionamentoFacade.usuarioAutenticado, aso, null, dbConnection);
                }
                else
                {
                    throw new FilaAtendimentoException(FilaAtendimentoException.msg4);
                }
            }
            
        }


        [MethodImpl(MethodImplOptions.Synchronized)]
        public void estornaFinalizacaoAtendimento(ItemFilaAtendimento itemFila, Aso aso, SalaExame salaExame)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método estornaFinalizacaoAtendimento");
            NpgsqlConnection dbConnection = FabricaConexao.getConexao();
            IGheFonteAgenteExameAsoDao gheFonteAgenteExameAsoDao = FabricaDao.getGheFonteAgenteExameAsoDao();
            IClienteFuncaoExameASoDao clienteFuncaoExameAsoDao = FabricaDao.getClienteFuncaoExameASoDao();

            NpgsqlTransaction transaction = dbConnection.BeginTransaction();

            try
            {
                this.updateFilaAtendimento(itemFila, false, false, null, true, salaExame != null ? salaExame.Id : null, ApplicationConstants.ESTORNAR_FINALIZACAO, PermissionamentoFacade.usuarioAutenticado, aso, 
                    salaExame != null ? salaExame.Sala : null, dbConnection);
                transaction.Commit();

            }
            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - estornaFinalizacaoAtendimento", ex);
                transaction.Rollback();
                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }
            finally
            {
                FabricaConexao.fecharConexao();
            }
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public List<GheFonteAgenteExameAso> findAllGheFonteAgenteExameByAsoBySituacaoAtendimento(Aso aso, Boolean? atendido, Boolean? finalizado, Boolean? cancelado, Boolean? devolvido, Boolean transcrito )
        
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findAllGheFonteAgenteExameByAsoBySituacaoAtendimento");
            NpgsqlConnection dbConnection = FabricaConexao.getConexao();
            IGheFonteAgenteExameAsoDao gheFonteAgenteExameAsoDao = FabricaDao.getGheFonteAgenteExameAsoDao();

            try
            {
                return gheFonteAgenteExameAsoDao.findAllExamesByAsoInSituacao(aso, atendido, finalizado, cancelado, devolvido, transcrito, dbConnection);

            }
            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findAllGheFonteAgenteExameByAsoBySituacaoAtendimento", ex);
                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }
            finally
            {
                FabricaConexao.fecharConexao();
            }
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public List<ClienteFuncaoExameASo> findAllClienteFuncaoExameByAsoBySituacaoAtendimento(Aso aso, Boolean? atendido, Boolean? finalizado, Boolean? cancelado, Boolean? devolvido, Boolean transcrito)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findAllClienteFuncaoExameByAsoBySituacaoAtendimento");
            NpgsqlConnection dbConnection = FabricaConexao.getConexao();
            IClienteFuncaoExameASoDao clienteFuncaoExameAsoDao = FabricaDao.getClienteFuncaoExameASoDao();

            try
            {
                return clienteFuncaoExameAsoDao.findAllExamesByAsoInSituacao(aso, atendido, finalizado, cancelado, devolvido, transcrito, dbConnection);

            }
            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findAllClienteFuncaoExameByAsoBySituacaoAtendimento", ex);
                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }
            finally
            {
                FabricaConexao.fecharConexao();
            }
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public ClienteFuncaoExameASo findClienteFuncaoExameAsoById(Int64 id)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findClienteFuncaoExameAsoById");
            NpgsqlConnection dbConnection = FabricaConexao.getConexao();
            IClienteFuncaoExameASoDao clienteFuncaoExameAsoDao = FabricaDao.getClienteFuncaoExameASoDao();
            IAsoDao asoDao = FabricaDao.getAsoDao();
            IClienteFuncaoFuncionarioDao clienteFuncaoFuncionarioDao = FabricaDao.getClienteFuncaoFuncionarioDao();
            IClienteFuncaoExameDao clienteFuncaoExameDao = FabricaDao.getClienteFuncaoExameDao();
            
            ClienteFuncaoExameASo cfea = null;

            try
            {
                cfea = clienteFuncaoExameAsoDao.findById(id, dbConnection);

                /* setando informações do atendimento no clienteFuncaoExameAso */

                cfea.Aso = asoDao.findById((Int64)cfea.Aso.Id, dbConnection);

                /* setando informaçao do clienteFuncaoFuncionario no atendimento */

                cfea.Aso.ClienteFuncaoFuncionario = clienteFuncaoFuncionarioDao.findById((Int64)cfea.Aso.ClienteFuncaoFuncionario.Id, dbConnection);

                /* setando informaçao do clieteFuncaoExame */

                cfea.ClienteFuncaoExame = clienteFuncaoExameDao.findById((Int64)cfea.ClienteFuncaoExame.Id, dbConnection);

                return cfea;

            }
            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findClienteFuncaoExameAsoById", ex);
                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }
            finally
            {
                FabricaConexao.fecharConexao();
            }
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public SalaExame findSalaExameById(Int64 id)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findSalaById");

            NpgsqlConnection dbConnection = FabricaConexao.getConexao();
            ISalaExameDao salaExameDao = FabricaDao.getSalaExameDao();
            SalaExame salaExame = null;

            try
            {
                salaExame = salaExameDao.findById(id, dbConnection);

                return salaExame;

            }
            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findSalaById", ex);
                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }
            finally
            {
                FabricaConexao.fecharConexao();
            }

        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public LinkedList<ItemFilaAtendimento> buscaExamesFinalizados(DateTime dataInicial, DateTime dataFinal, Sala sala)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método buscaExamesFinalizados");

            LinkedList<ItemFilaAtendimento> examesAtendidos = new LinkedList<ItemFilaAtendimento>();
            LinkedList<ItemFilaAtendimento> examesAtendidosOrdenados = new LinkedList<ItemFilaAtendimento>();
            
            NpgsqlConnection dbConnection = FabricaConexao.getConexao();
            
            IAsoDao asoDao = FabricaDao.getAsoDao();
            IPeriodicidadeDao periodicidadeDao = FabricaDao.getPeriodicidadeDao();
            IClienteFuncaoExameASoDao clienteFuncaoExameAsoDao = FabricaDao.getClienteFuncaoExameASoDao();
            IGheFonteAgenteExameAsoDao gheFonteAgenteExameAsoDao = FabricaDao.getGheFonteAgenteExameAsoDao();
            IGheFonteAgenteExameDao gheFonteAgenteExameDao = FabricaDao.getGheFonteAgenteExameDao();
            IClienteFuncaoExameDao clienteFuncaoExameDao = FabricaDao.getClienteFuncaoExameDao();
            IClienteFuncaoFuncionarioDao clienteFuncaoFuncionarioDao = FabricaDao.getClienteFuncaoFuncionarioDao();
            ICentroCustoDao centroCustoDao = FabricaDao.getCentroCustoDao();
            IClienteDao clienteDao = FabricaDao.getClienteDao();
            string ordenacao = string.Empty;
            PermissionamentoFacade.mapaConfiguracoes.TryGetValue(ConfigurationConstans.ORDENACAO_ATENDIMENTO, out ordenacao);
            
            try
            {
                LinkedList<Aso> asos = asoDao.findAsoNotCanceladoByPeriodo(dataInicial, dataFinal, PermissionamentoFacade.usuarioAutenticado.Empresa, dbConnection);
                
                ClienteFuncaoFuncionario clienteFuncaoFuncionario = null;

                asos.ToList().ForEach(delegate(Aso atendimento)
                {
                    clienteFuncaoFuncionario = clienteFuncaoFuncionarioDao.findById((long)atendimento.ClienteFuncaoFuncionario.Id, dbConnection);

                    clienteFuncaoExameAsoDao.findAllExamesByAsoInSituacaoBySala(atendimento, true, true, false, null, sala, Convert.ToBoolean(ordenacao), dbConnection).ToList().ForEach(delegate(ClienteFuncaoExameASo cfea) {

                        cfea.Aso = asoDao.findById((long)atendimento.Id, dbConnection);
                        examesAtendidos.AddFirst(new ItemFilaAtendimento(cfea.Id, clienteFuncaoFuncionario.Funcionario.Nome, String.Format("{0:dd/MM/yyyy}", clienteFuncaoFuncionario.Funcionario.DataNascimento), clienteFuncaoFuncionario.Funcionario.Cpf, clienteFuncaoFuncionario.ClienteFuncao.Funcao.Descricao, clienteFuncaoFuncionario.ClienteFuncao.Cliente.RazaoSocial, cfea.Login, ApplicationConstants.ATENDER_EXAME, ApplicationConstants.EXAME_EXTRA, clienteFuncaoExameDao.findExameByClienteFuncaoExame(cfea.ClienteFuncaoExame, dbConnection), cfea.Devolvido, (DateTime)atendimento.DataElaboracao, clienteFuncaoFuncionario.Funcionario.Rg, null, 0, clienteFuncaoFuncionario.Funcionario.Peso, clienteFuncaoFuncionario.Funcionario.Altura, periodicidadeDao.findById((Int64)cfea.Aso.Periodicidade.Id, dbConnection).Descricao, clienteFuncaoFuncionario.Funcionario.getImc(), cfea.DataAtendido, cfea.Aso.Codigo, cfea.Prestador != null ? cfea.Prestador.RazaoSocial : String.Empty, atendimento.Prioridade, !string.IsNullOrEmpty(atendimento.Senha) ? atendimento.Senha : string.Empty)); 
                    });

                    gheFonteAgenteExameAsoDao.findAllExamesByAsoInSituacaoBySala(atendimento, true, true, false, null, sala, Convert.ToBoolean(ordenacao), dbConnection).ToList().ForEach(delegate(GheFonteAgenteExameAso gfaea)
                    {
                        gfaea.Aso = asoDao.findById((long)atendimento.Id, dbConnection);

                        examesAtendidos.AddFirst(new ItemFilaAtendimento(gfaea.Id, clienteFuncaoFuncionario.Funcionario.Nome, String.Format("{0:dd/MM/yyyy}", clienteFuncaoFuncionario.Funcionario.DataNascimento), clienteFuncaoFuncionario.Funcionario.Cpf, clienteFuncaoFuncionario.ClienteFuncao.Funcao.Descricao, clienteFuncaoFuncionario.ClienteFuncao.Cliente.RazaoSocial, gfaea.Login, ApplicationConstants.ATENDER_EXAME, ApplicationConstants.EXAME_PCMSO, gheFonteAgenteExameDao.findExameByGheFonteAgenteExame(gfaea.GheFonteAgenteExame, dbConnection), gfaea.Devolvido, (DateTime)atendimento.DataElaboracao, clienteFuncaoFuncionario.Funcionario.Rg, null, 0, clienteFuncaoFuncionario.Funcionario.Peso, clienteFuncaoFuncionario.Funcionario.Altura, periodicidadeDao.findById((Int64)gfaea.Aso.Periodicidade.Id, dbConnection).Descricao, clienteFuncaoFuncionario.Funcionario.getImc(), gfaea.DataAtendido, gfaea.Aso.Codigo, gfaea.Prestador != null ? gfaea.Prestador.RazaoSocial : String.Empty, atendimento.Prioridade, !string.IsNullOrEmpty(atendimento.Senha) ? atendimento.Senha : string.Empty));
                    });

                    if (atendimento.CentroCusto != null)
                    {
                        atendimento.CentroCusto = centroCustoDao.findById((long)atendimento.CentroCusto.Id, dbConnection);
                        atendimento.CentroCusto.Cliente = clienteDao.findById((long)atendimento.CentroCusto.Cliente.Id, dbConnection);
                    }

                    if (Convert.ToBoolean(ordenacao))
                    {
                        examesAtendidosOrdenados = new LinkedList<ItemFilaAtendimento>(examesAtendidos.OrderBy(delegate(ItemFilaAtendimento p)
                        {
                            return p.SenhaAtendimento;
                        }));
                    }
                    else
                    {
                        examesAtendidosOrdenados = new LinkedList<ItemFilaAtendimento>(examesAtendidos.OrderBy(delegate(ItemFilaAtendimento p)
                        {
                            return p.CodigoAtendimento;
                        }));

                    }

                });

            }
            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - buscaExamesFinalizados", ex);
                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }
            finally
            {
                FabricaConexao.fecharConexao();
            }

            return examesAtendidosOrdenados;
        }


        [MethodImpl(MethodImplOptions.Synchronized)]
        public SalaExame updateSalaExame(SalaExame salaExame)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método updateSalaExame");

            NpgsqlConnection dbConnection = FabricaConexao.getConexao();
            ISalaExameDao salaExameDao = FabricaDao.getSalaExameDao();
            NpgsqlTransaction transaction = dbConnection.BeginTransaction();

            try
            {
                salaExameDao.update(salaExame, dbConnection);
                transaction.Commit();

            }
            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - updateSalaExame", ex);
                transaction.Rollback();
                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }
            finally
            {
                FabricaConexao.fecharConexao();
            }
            return salaExame;
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public List<Exame> listAllExameNotInSala()
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método listAllExameNotInSala");

            NpgsqlConnection dbConnection = FabricaConexao.getConexao();
            ISalaExameDao salaExameDao = FabricaDao.getSalaExameDao();

            try
            {
                return salaExameDao.listAllNotInSala(dbConnection);

            }
            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - listAllExameNotInSala", ex);
                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }
            finally
            {
                FabricaConexao.fecharConexao();
            }
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public AcompanhamentoAtendimento insertAcompanhamentoAtendimento(AcompanhamentoAtendimento acompanhamentoAtendimento)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método updateSalaExame");

            NpgsqlConnection dbConnection = FabricaConexao.getConexao();
            IAcompanhamentoAtendimentoDao acompanhamentoAtendimentoDao = FabricaDao.getAcompanhamentoAtendimentoDao();
            NpgsqlTransaction transaction = dbConnection.BeginTransaction();
            AcompanhamentoAtendimento acompanhamentoAtendimentoNovo = null;
            try
            {
                acompanhamentoAtendimentoNovo = acompanhamentoAtendimentoDao.insert(acompanhamentoAtendimento, dbConnection);
                transaction.Commit();

            }
            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - insertAcompanhamentoAtendimento", ex);
                transaction.Rollback();
                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }
            finally
            {
                FabricaConexao.fecharConexao();
            }
            return acompanhamentoAtendimentoNovo;
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public List<Sala> findAllSalaByEmpresa(Empresa empresa)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findAllSalaByEmpresa");

            NpgsqlConnection dbConnection = FabricaConexao.getConexao();
            ISalaDao salaDao = FabricaDao.getSalaDao();

            try
            {
                return salaDao.findallSalaByEmpresa(empresa, dbConnection);

            }
            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findAllSalaByEmpresa", ex);
                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }
            finally
            {
                FabricaConexao.fecharConexao();
            }
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public List<SalaExame> findAllSalaExameAtivoByExame(Exame exame)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findAllSalaExameAtivoByExame");

            NpgsqlConnection dbConnection = FabricaConexao.getConexao();
            ISalaExameDao salaExameDao = FabricaDao.getSalaExameDao();

            try
            {
                return salaExameDao.findAllSalaExameAtivoByExame(exame, PermissionamentoFacade.usuarioAutenticado.Empresa, dbConnection);

            }
            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findAllSalaExameAtivoByExame", ex);
                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }
            finally
            {
                FabricaConexao.fecharConexao();
            }
        }
        
    }
}
