﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.CompilerServices;
using SWS.Entidade;
using SWS.Helper;
using Npgsql;
using SWS.Dao;
using SWS.IDao;
using SWS.Excecao;
using System.Data;
using SWS.View.Resources;

namespace SWS.Facade
{
    class ProtocoloFacade
    {
        public static ProtocoloFacade protocoloFacade = null;

        private ProtocoloFacade() { }

        public static ProtocoloFacade getInstance()
        {
            if (protocoloFacade == null)
            {
                protocoloFacade = new ProtocoloFacade();
            }

            return protocoloFacade;
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public Protocolo insertProtocolo(Protocolo protocolo)
        {

            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método insertProtocolo");

            Protocolo novoProtocolo = null;

            NpgsqlConnection dbConnection = FabricaConexao.getConexao();
            IProtocoloDao protocoloDao = FabricaDao.getProtocoloDao();
            IAsoDao asoDao = FabricaDao.getAsoDao();
            IEstudoDao estudoDao = FabricaDao.getEstudoDao();
            IMovimentoDao movimentoDao = FabricaDao.getMovimentoDao();
            IDocumentoProtocoloDao documentoProtocoloDao = FabricaDao.getDocumentoProtocoloDao();
            NpgsqlTransaction transaction = dbConnection.BeginTransaction();

            try
            {

                /* verificando se já existe um protocolo já criado. Caso exista
                 * então somente deverá ser gravado as informações de exames no protocolo. */

                if (protocolo.Id != null)
                {
                    /* incluindo os exames do protocolo */
                    protocoloDao.insertExameInProtocolo(protocolo, dbConnection);
                }
                else
                {
                    /* incluindo o protocolo com as coleções de gheFonteAgenteExameAso e clienteFuncaoExameAso */
                    novoProtocolo = protocoloDao.insertProtocolo(protocolo, dbConnection);

                }

                /* incluindo o atendimento caso exista */
                if (protocolo.Atendimento != null)
                {
                    asoDao.insertAsoInProtocolo(protocolo, dbConnection);
                }

                /* incluindo o pcmso caso exista */
                if (protocolo.Pcmso != null)
                {
                    estudoDao.insertInProtocolo(protocolo, dbConnection);
                }

                /* incluindo o ppra caso exista */
                if (protocolo.Ppra != null)
                {
                    estudoDao.insertInProtocolo(protocolo, dbConnection);
                }

                
                if (protocolo.Movimentos.Count > 0)
                {
                    foreach (Movimento movimento in protocolo.Movimentos)
                    {
                        movimentoDao.insertMovimentoInProtocolo(movimento, protocolo, dbConnection);
                    }
                }

                /* incluindo os documentos caso exista */
                if (protocolo.Documentos.Count > 0)
                {
                    foreach (DocumentoProtocolo documentos in protocolo.Documentos)
                    {
                        documentos.Protocolo = novoProtocolo != null ? novoProtocolo : protocolo;
                        documentoProtocoloDao.insertDocumentoProtocolo(documentos, dbConnection);
                    }
                }

                transaction.Commit();

            }
            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - insertProtocolo", ex);
                transaction.Rollback();
                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }
            finally
            {
                FabricaConexao.fecharConexao();
            }

            return novoProtocolo;

        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public Protocolo updateProtocolo(Protocolo protocolo, String acao)
        {

            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método updateProtocolo");

            NpgsqlConnection dbConnection = FabricaConexao.getConexao();
            IProtocoloDao protocoloDao = FabricaDao.getProtocoloDao();
            NpgsqlTransaction transaction = dbConnection.BeginTransaction();

            Protocolo protocoloAlterado = null;

            try
            {

                /* alterando informações permitidas do protocolo  */

                protocoloAlterado = protocoloDao.updateProtocolo(protocolo, acao, dbConnection);

                transaction.Commit();

            }
            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - updateProtocolo", ex);
                transaction.Rollback();
                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }
            finally
            {
                FabricaConexao.fecharConexao();
            }

            return protocoloAlterado;

        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public DataSet findByFilter(Protocolo protocolo, DateTime? dataGravacaoFinal, 
            DateTime? dataCancelamentoFinal, String tipoItem, Int64? idItem)
        {

            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findByFilter");
            
            NpgsqlConnection dbConnection = FabricaConexao.getConexao();
            
            IProtocoloDao protocoloDao = FabricaDao.getProtocoloDao();

            try
            {
                return protocoloDao.findByFilter(protocolo, dataGravacaoFinal, dataCancelamentoFinal,
                    tipoItem, idItem, dbConnection);

            }
            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findByFilter", ex);
                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }
            finally
            {
                FabricaConexao.fecharConexao();
            }

        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public void changeSituacao(Protocolo protocolo, String situacao)
        {

            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método changeSituacao");

            NpgsqlConnection dbConnection = FabricaConexao.getConexao();
            IProtocoloDao protocoloDao = FabricaDao.getProtocoloDao();
            NpgsqlTransaction transaction = dbConnection.BeginTransaction();

            try
            {

                 protocoloDao.changeSituacao(protocolo, situacao, dbConnection);

                transaction.Commit();

            }
            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - changeSituacao", ex);
                transaction.Rollback();
                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }
            finally
            {
                FabricaConexao.fecharConexao();
            }
        }


        [MethodImpl(MethodImplOptions.Synchronized)]
        public DataSet findAllItensByProtocolo(Protocolo protocolo)
        {

            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findAllItensByProtocolo");

            NpgsqlConnection dbConnection = FabricaConexao.getConexao();

            IProtocoloDao protocoloDao = FabricaDao.getProtocoloDao();

            try
            {
                return protocoloDao.findAllItensByProtocolo(protocolo, dbConnection);

            }
            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findAllItensByProtocolo", ex);
                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }
            finally
            {
                FabricaConexao.fecharConexao();
            }

        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public Protocolo findById(Int64 id)
        {

            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findById");

            NpgsqlConnection dbConnection = FabricaConexao.getConexao();
            IProtocoloDao protocoloDao = FabricaDao.getProtocoloDao();
            IUsuarioDao usuarioDao = FabricaDao.getUsuarioDao();
            IClienteDao clienteDao = FabricaDao.getClienteDao();
            IAsoDao asoDao = FabricaDao.getAsoDao();
            IClienteFuncaoFuncionarioDao clienteFuncaoFuncionarioDao = FabricaDao.getClienteFuncaoFuncionarioDao();
            IEstudoDao estudoDao = FabricaDao.getEstudoDao();
            IMovimentoDao movimentoDao = FabricaDao.getMovimentoDao();

            try
            {
                Protocolo protocolo = null;

                protocolo = protocoloDao.findById(id, dbConnection);

                if (protocolo != null)
                {
                    protocolo.UsuarioGerou = usuarioDao.findById((Int64)protocolo.UsuarioGerou.Id, dbConnection);

                    if (protocolo.UsuarioCancelou != null)
                    {
                        protocolo.UsuarioCancelou = usuarioDao.findById((Int64)protocolo.UsuarioCancelou.Id, dbConnection);
                    }

                    if (protocolo.UsuarioFinalizou != null)
                    {
                        protocolo.UsuarioFinalizou = usuarioDao.findById((Int64)protocolo.UsuarioFinalizou.Id, dbConnection);
                    }

                    protocolo.Cliente = clienteDao.findById((Int64)protocolo.Cliente.Id, dbConnection);

                    #region atendimento

                    LinkedList<Aso> atendimentosUpdate = new LinkedList<Aso>();

                    foreach (Aso aso in protocolo.Atendimentos)
                    {
                        atendimentosUpdate.AddFirst(asoDao.findById((Int64)aso.Id, dbConnection));
                    }

                    protocolo.Atendimentos = atendimentosUpdate;

                    #endregion

                    #region pcmso

                    /* incluindo informações do pcmso no protocolo */

                    LinkedList<Estudo> pcmsosUpdate = new LinkedList<Estudo>();

                    foreach (Estudo pcmso in protocolo.Pcmsos)
                    {
                        pcmsosUpdate.AddFirst(estudoDao.findById((Int64)pcmso.Id, dbConnection));
                    }

                    protocolo.Pcmsos = pcmsosUpdate;
                    
                    #endregion

                    #region ppra

                    LinkedList<Estudo> ppraUpdate = new LinkedList<Estudo>();

                    foreach (Estudo ppra in protocolo.Ppras)
                    {
                        ppraUpdate.AddFirst(estudoDao.findById((Int64)ppra.Id, dbConnection));
                    }

                    protocolo.Ppras = ppraUpdate;

                    #endregion

                    #region movimento

                    LinkedList<Movimento> movimentosUpdate = new LinkedList<Movimento>();

                    foreach (Movimento movimento in protocolo.Movimentos)
                    {
                        movimentosUpdate.AddFirst(movimentoDao.findById((Int64)movimento.Id, dbConnection));
                    }

                    protocolo.Movimentos = movimentosUpdate;
                    
                    #endregion
                }

                return protocolo;

            }
            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findById", ex);
                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }
            finally
            {
                FabricaConexao.fecharConexao();
            }
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public void deleteItemProtocolo(Int64 id, String tipo)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método deleteItemProtocolo");

            NpgsqlConnection dbConnection = FabricaConexao.getConexao();
            
            IProtocoloDao protocoloDao = FabricaDao.getProtocoloDao();

            try
            {
                protocoloDao.deleteItemProtocolo(id, tipo, dbConnection);

            }
            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - deleteItemProtocolo", ex);
                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }
            finally
            {
                FabricaConexao.fecharConexao();
            }
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public void finalizaProtocolo(Protocolo protocolo)
        {

            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método finalizaProtocolo");

            NpgsqlConnection dbConnection = FabricaConexao.getConexao();
            IProtocoloDao protocoloDao = FabricaDao.getProtocoloDao();
            NpgsqlTransaction transaction = dbConnection.BeginTransaction();

            try
            {
                protocoloDao.changeSituacao(protocolo, ApplicationConstants.FECHADO, dbConnection);
                protocoloDao.updateProtocolo(protocolo,ApplicationConstants.FINALIZAPROTOCOLO,dbConnection);
                transaction.Commit();

            }
            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - finalizaProtocolo", ex);
                transaction.Rollback();
                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }
            finally
            {
                FabricaConexao.fecharConexao();
            }
        }


        [MethodImpl(MethodImplOptions.Synchronized)]
        public void cancelaProtocolo(Protocolo protocolo, DataSet ds)
        {

            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método cancelaProtocolo");

            NpgsqlConnection dbConnection = FabricaConexao.getConexao();
            IProtocoloDao protocoloDao = FabricaDao.getProtocoloDao();
            NpgsqlTransaction transaction = dbConnection.BeginTransaction();

            try
            {

                /* mudando status do protocolo */
                protocoloDao.changeSituacao(protocolo, ApplicationConstants.DESATIVADO, dbConnection);

                /* gravando informações de cancelamento no protocolo */
                protocoloDao.updateProtocolo(protocolo, ApplicationConstants.DELETEPROTOCOLO, dbConnection);

                /* excluindo informação do protocolo no item */

                foreach (DataRow dtRow in ds.Tables["ExamesProtocolo"].Rows)
                {
                    Int64 id = (Int64)dtRow["id"];
                    String tipo = (String)dtRow["tipo"];
                    protocoloDao.deleteItemProtocolo(id, tipo, dbConnection);
                }

                /* incluindo na tabela de itens de protocolo */

                foreach (DataRow dtRow in ds.Tables["ExamesProtocolo"].Rows)
                {
                    ProtocoloItem protocoloItem = new ProtocoloItem();
                    
                    protocoloItem.IdItem = (Int64)dtRow["id"];
                    protocoloItem.IdProtocolo = (Int64)protocolo.Id;
                    protocoloItem.CodigoItem = (String)dtRow["codigo_item"];
                    protocoloItem.Cliente = (String)dtRow["cliente"];
                    protocoloItem.Colaborador = dtRow["colaborador"] == DBNull.Value ? String.Empty : (String)dtRow["colaborador"];
                    protocoloItem.Rg = dtRow["rg"] == DBNull.Value ? String.Empty : (String)dtRow["rg"];
                    protocoloItem.Cpf = dtRow["cpf"] == DBNull.Value ? String.Empty : (String)dtRow["cpf"];
                    protocoloItem.Periodicidade = dtRow["periodicidade"] == DBNull.Value ? String.Empty : (String)dtRow["periodicidade"];
                    protocoloItem.DescricaoItem = (String)dtRow["tipo_item"];
                    protocoloItem.DataItem = (DateTime)dtRow["data_item"];
                    protocoloItem.Laudo = dtRow["laudo"] == DBNull.Value ? String.Empty : (String)dtRow["laudo"];
                    protocoloItem.Tipo = (String)dtRow["tipo"];

                    protocoloDao.InsertItemProtocolo(protocoloItem, dbConnection);

                }

                transaction.Commit();

            }
            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - cancelaProtocolo", ex);
                transaction.Rollback();
                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }
            finally
            {
                FabricaConexao.fecharConexao();
            }
        }


        [MethodImpl(MethodImplOptions.Synchronized)]
        public DataSet findDocumentoByFilter(Documento documento)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findDocumentoByFilter");

            NpgsqlConnection dbConnection = FabricaConexao.getConexao();
            
            IDocumentoDao documentoDao = FabricaDao.getDocumentoDao();

            try
            {
                return documentoDao.findByFilter(documento, dbConnection);
                
            }
            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findDocumentoByFilter", ex);
                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }
            finally
            {
                FabricaConexao.fecharConexao();
            }
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public Documento InsertDocumento(Documento documento)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método InsertDocumento");

            NpgsqlConnection dbConnection = FabricaConexao.getConexao();
            NpgsqlTransaction Transaction = dbConnection.BeginTransaction();

            IDocumentoDao documentoDao = FabricaDao.getDocumentoDao();

            Documento documentoNovo = null;
            
            try
            {
                if (documentoDao.findByDescricao(documento.Descricao, dbConnection) != null)
                    throw new DocumentoException(DocumentoException.msg1);
                
                documentoNovo = documentoDao.insert(documento, dbConnection);
                
                Transaction.Commit();

                return documentoNovo;

            }
            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - InsertDocumento", ex);
                Transaction.Rollback();
                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }
            finally
            {
                FabricaConexao.fecharConexao();
            }
        }
        
        [MethodImpl(MethodImplOptions.Synchronized)]
        public Documento updateDocumento(Documento documento)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método updateDocumento");

            NpgsqlConnection dbConnection = FabricaConexao.getConexao();
            NpgsqlTransaction Transaction = dbConnection.BeginTransaction();

            IDocumentoDao documentoDao = FabricaDao.getDocumentoDao();

            Documento documentoAlterado = null;

            try
            {
                /* verificando se o documento pode ser alterado
                 * Caso a descrição já exista não será permitido a alteração. */

                Documento documentoProcurado = documentoDao.findByDescricao(documento.Descricao, dbConnection);

                if (documentoProcurado != null && documentoProcurado.Id != documento.Id)
                    throw new DocumentoException(DocumentoException.msg2);

                
                if (documentoDao.checkCanChanges(documento, dbConnection))
                    throw new DocumentoException(DocumentoException.msg3);
                    
                documentoAlterado = documentoDao.update(documento, dbConnection);
                Transaction.Commit();

                return documentoAlterado;

            }
            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - updateDocumento", ex);
                Transaction.Rollback();
                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }
            finally
            {
                FabricaConexao.fecharConexao();
            }
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public Documento findDocumentoById(Int64 id)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findDocumentoById");

            NpgsqlConnection dbConnection = FabricaConexao.getConexao();

            IDocumentoDao documentoDao = FabricaDao.getDocumentoDao();

            try
            {
                return documentoDao.findById(id, dbConnection);

            }
            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findDocumentoById", ex);
                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }
            finally
            {
                FabricaConexao.fecharConexao();
            }
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public void deleteDocumentoProtocolo(DocumentoProtocolo documentoProtocolo)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método deleteDocumentoProtocolo");

            NpgsqlConnection dbConnection = FabricaConexao.getConexao();
            NpgsqlTransaction Transaction = dbConnection.BeginTransaction();

            IDocumentoProtocoloDao documentoProtocoloDao = FabricaDao.getDocumentoProtocoloDao();

            try
            {
                documentoProtocoloDao.deleteDocumentoProtocolo(documentoProtocolo, dbConnection);
                Transaction.Commit();

            }
            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findDocumentoById", ex);
                Transaction.Rollback();
                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }
            finally
            {
                FabricaConexao.fecharConexao();
            }
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public List<Documento> findAllDocumentos()
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findAllDocumentos");

            NpgsqlConnection dbConnection = FabricaConexao.getConexao();

            IDocumentoDao documentoDao = FabricaDao.getDocumentoDao();

            try
            {
                return documentoDao.findAll(dbConnection);

            }
            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findAllDocumentos", ex);
                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }
            finally
            {
                FabricaConexao.fecharConexao();
            }
        }
    }
}
