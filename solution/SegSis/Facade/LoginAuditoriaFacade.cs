﻿using Npgsql;
using SWS.Dao;
using SWS.Excecao;
using SWS.Helper;
using SWS.IDao;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

namespace SWS.Facade
{
    class LoginAuditoriaFacade
    {
        public static LoginAuditoriaFacade loginAuditoriaFacade = null;

        private LoginAuditoriaFacade() { }

        public static LoginAuditoriaFacade getInstance()
        {
            if (loginAuditoriaFacade == null)
            {
                loginAuditoriaFacade = new LoginAuditoriaFacade();
            }

            return loginAuditoriaFacade;
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public void registrarLoginAuditoria(string login, string ip)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método insertCorrecaoEstudo");

            NpgsqlConnection dbConnection = FabricaConexao.getConexao();

            ILoginAuditoriaDao loginAuditoriaDao = FabricaDao.getLoginAuditoriaDao();
            NpgsqlTransaction transaction = dbConnection.BeginTransaction();

            try
            {
                loginAuditoriaDao.registrarLoginAuditoria(login, ip, dbConnection);
                transaction.Commit();
            }
            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - registrarLoginAuditoria", ex);
                transaction.Rollback();
                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }
            finally
            {
                FabricaConexao.fecharConexao();
            }
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public void desativarLoginAuditoria(string login, string ip)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método desativarLoginAuditoria");

            NpgsqlConnection dbConnection = FabricaConexao.getConexao();

            ILoginAuditoriaDao loginAuditoriaDao = FabricaDao.getLoginAuditoriaDao();
            NpgsqlTransaction transaction = dbConnection.BeginTransaction();

            try
            {
                loginAuditoriaDao.desativarLoginAuditoria(login, ip, dbConnection);
                transaction.Commit();
            }
            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - desativarLoginAuditoria", ex);
                transaction.Rollback();
                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }
            finally
            {
                FabricaConexao.fecharConexao();
            }
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public Boolean isLoginAtivo(String login, String ip)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método isLoginAtivo");

            NpgsqlConnection dbConnection = FabricaConexao.getConexao();
            ILoginAuditoriaDao loginAuditoriaDao = FabricaDao.getLoginAuditoriaDao();

            try
            {
                return loginAuditoriaDao.isLoginAtivo(login, ip, dbConnection);

            }
            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - isLoginAtivo", ex);
                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }
            finally
            {
                FabricaConexao.fecharConexao();
            }
        }
    }
}
