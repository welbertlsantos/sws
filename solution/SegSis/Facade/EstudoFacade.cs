﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data;
using SWS.Entidade;
using SWS.Excecao;
using Npgsql;
using SWS.Dao;
using SWS.IDao;
using SWS.View.Resources;
using SWS.Helper;
using SWS.View.ViewHelper;
using System.Runtime.CompilerServices;

namespace SWS.Facade
{
    class EstudoFacade
    {
        public static EstudoFacade estudoFacade = null;

        private EstudoFacade() { }

        public static EstudoFacade getInstance()
        {
            if (estudoFacade == null)
            {
                estudoFacade = new EstudoFacade();
            }
            return estudoFacade;
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public DataSet findByFilter(Estudo pcmso, DateTime? dataCriacaoFinal)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findByFilter");

            NpgsqlConnection dbConnection = FabricaConexao.getConexao();

            IEstudoDao pcmsoDao = FabricaDao.getEstudoDao();

            try
            {
                return pcmsoDao.findByFilter(pcmso, dataCriacaoFinal, dbConnection);

            }
            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findByFilter", ex);
                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }
            finally
            {
                FabricaConexao.fecharConexao();
            }
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public DataSet findGheByPcmso(Estudo pcmso)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findGheByPcmso");

            NpgsqlConnection dbConnection = FabricaConexao.getConexao();

            IGheDao gheDao = FabricaDao.getGheDao();

            try
            {
                return gheDao.findAtivoByEstudo(pcmso, dbConnection);

            }
            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findGheByPcmso", ex);
                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }
            finally
            {
                FabricaConexao.fecharConexao();
            }
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public DataSet findAllPeriodicidade()
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findAllPeriodicidade");

            NpgsqlConnection dbConnection = FabricaConexao.getConexao();

            IPeriodicidadeDao periodicidadeDao = FabricaDao.getPeriodicidadeDao();

            try
            {
                return periodicidadeDao.findAll(dbConnection);

            }
            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findAllPeriodicidade", ex);
                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }
            finally
            {
                FabricaConexao.fecharConexao();
            }
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public Periodicidade findPeriodicidadeById(Int64 idPeriodicidade)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findPeriodicidadeById");

            NpgsqlConnection dbConnection = FabricaConexao.getConexao();

            IPeriodicidadeDao periodicidadeDao = FabricaDao.getPeriodicidadeDao();

            try
            {
                return periodicidadeDao.findById(idPeriodicidade, dbConnection);
            }
            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findPeriodicidadeById", ex);
                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }
            finally
            {
                FabricaConexao.fecharConexao();
            }
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public DataSet findAllPeriodicidadeByGheFonteAgenteExame(Int64 idGheFonteAgenteExame)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findAllPeriodicidadeByGheFonteAgenteExame");

            NpgsqlConnection dbConnection = FabricaConexao.getConexao();

            IPeriodicidadeDao periodicidadeDao = FabricaDao.getPeriodicidadeDao();

            try
            {
                return periodicidadeDao.findByGheFonteAgenteExame(idGheFonteAgenteExame, dbConnection);

            }
            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findAllPeriodicidadeByGheFonteAgenteExame", ex);
                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }
            finally
            {
                FabricaConexao.fecharConexao();
            }
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public DataSet findExameByFilter(Exame exame)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findExameByFilter");

            NpgsqlConnection dbConnection = FabricaConexao.getConexao();

            IExameDao exameDao = FabricaDao.getExameDao();

            try
            {
                return exameDao.findByFilter(exame, dbConnection);

            }
            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findExameByFilter", ex);
                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }
            finally
            {
                FabricaConexao.fecharConexao();
            }
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public Exame insertExame(Exame exame)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método incluiExame");

            Exame exameNovo = null;
            NpgsqlConnection dbConnection = FabricaConexao.getConexao();
            IExameDao exameDao = FabricaDao.getExameDao();
            IMedicoExameDao medicoExameDao = FabricaDao.getMedicoExameDao();
            IRelatorioExameDao relatorioExameDao = FabricaDao.getRelatorioExameDao();
            NpgsqlTransaction transaction = dbConnection.BeginTransaction();
            try
            {
                // verificando se já existe um exame cadastrado com essa descrição
                exameNovo = exameDao.findByDescricao(Convert.ToString(exame.Descricao), dbConnection);

                if (exameNovo != null)
                    throw new ExameException(ExameException.msg);

                exameNovo = exameDao.insert(exame, dbConnection);

                /* incluindo lista de médicos que realizam o exame */
                exame.MedicoExame.ForEach(delegate(MedicoExame medicoExame)
                {
                    medicoExame.Exame = exameNovo;
                    medicoExameDao.insert(medicoExame, dbConnection);
                });

                /* incluindo o relatorio do exame */
                if (exame.RelatorioExame != null)
                    exame.RelatorioExame = relatorioExameDao.insert(exame.RelatorioExame, dbConnection);


                transaction.Commit();
            }
            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - incluiExame", ex);
                transaction.Rollback();
                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }
            finally
            {
                FabricaConexao.fecharConexao();
            }
            return exameNovo;
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public Exame findExameById(Int64 id)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findExameById");

            Exame exameProcurado = null;
            NpgsqlConnection dbConnection = FabricaConexao.getConexao();
            IExameDao exameDao = FabricaDao.getExameDao();
            IRelatorioExameDao relatorioExameDao = FabricaDao.getRelatorioExameDao();
            try
            {
                exameProcurado = exameDao.findById(id, dbConnection);

                /* recuperando informação de relatorio do exame */
                List<RelatorioExame> relatorios = relatorioExameDao.findAllByExame(exameProcurado, dbConnection);
                exameProcurado.RelatorioExame = relatorios.Find(x => string.Equals(x.Situacao, ApplicationConstants.ATIVO));
            }
            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findExameById", ex);
                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }
            finally
            {
                FabricaConexao.fecharConexao();
            }
            return exameProcurado;
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public Exame updateExame(Exame exame)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método alterarExame");

            Exame exameAlterado = null;
            NpgsqlConnection dbConnection = FabricaConexao.getConexao();
            IExameDao exameDao = FabricaDao.getExameDao();
            NpgsqlTransaction transaction = dbConnection.BeginTransaction();
            try
            {

                /* verificando se já existe um exame cadastrado com essa descrição.*/

                exameAlterado = exameDao.findByDescricao(exame.Descricao, dbConnection);

                if (exameAlterado != null && exameAlterado.Id != exame.Id)
                {
                    throw new ExameException(ExameException.msg);
                }

                exameAlterado = exameDao.update(exame, dbConnection);
                transaction.Commit();

            }
            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - alterarExame", ex);
                transaction.Rollback();
                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }
            finally
            {
                FabricaConexao.fecharConexao();
            }
            return exame;
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public void deleteExame(Exame exame)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método excluiExame");

            NpgsqlConnection dbConnection = FabricaConexao.getConexao();
            IExameDao exameDao = FabricaDao.getExameDao();
            NpgsqlTransaction transaction = dbConnection.BeginTransaction();
            try
            {

                if (exameDao.findInGheFonteAgenteExame(exame, dbConnection) || exameDao.findInExameAso(exame, dbConnection))
                {
                    exame.Situacao = ApplicationConstants.DESATIVADO;
                    exameDao.update(exame, dbConnection);
                }
                else
                {
                    exameDao.delete(exame, dbConnection);
                }

                transaction.Commit();

            }
            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - excluiExame", ex);
                transaction.Rollback();
                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }
            finally
            {
                FabricaConexao.fecharConexao();
            }
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public DataSet findAllExamesByGheFonteAgente(GheFonteAgente gheFonteAgente)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findAllExamesByGheFonteAgente");

            NpgsqlConnection dbConnection = FabricaConexao.getConexao();

            IGheFonteAgenteExameDao gheFonteAgenteExameDao = FabricaDao.getGheFonteAgenteExameDao();

            try
            {
                return gheFonteAgenteExameDao.findAtivosByGheFonteAgente(gheFonteAgente, dbConnection);

            }
            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findAllExamesByGheFonteAgente", ex);
                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }
            finally
            {
                FabricaConexao.fecharConexao();
            }
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public List<GheSetorClienteFuncaoExame> findGheSetorClienteFuncaoExameByGheAndFuncao(Ghe ghe, Funcao funcao)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findGheSetorClienteFuncaoExameByGheAndFuncao");

            NpgsqlConnection dbConnection = FabricaConexao.getConexao();

            IGheSetorClienteFuncaoExameDao gheSetorClienteFuncaoExameDao = FabricaDao.getGheSetorClienteFuncaoExameDao();

            try
            {
                return gheSetorClienteFuncaoExameDao.findByGheAndFuncao(ghe, funcao, dbConnection);

            }
            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findGheSetorClienteFuncaoExameByGheAndFuncao", ex);
                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }
            finally
            {
                FabricaConexao.fecharConexao();
            }
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public DataSet findAllExameAtivoInClienteFuncao(ClienteFuncao clienteFuncao)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findAllExameAtivoInClienteFuncao");

            NpgsqlConnection dbConnection = FabricaConexao.getConexao();

            IExameDao exameDao = FabricaDao.getExameDao();

            try
            {
                return exameDao.findAtivoInClienteFuncao(clienteFuncao, dbConnection);

            }
            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findAllExameAtivoInClienteFuncao", ex);
                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }
            finally
            {
                FabricaConexao.fecharConexao();
            }
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public GheFonteAgenteExame insertGheFonteAgenteExame(GheFonteAgenteExame gheFonteAgenteExame)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método incluirGheFonteAgenteExame");

            NpgsqlConnection dbConnection = FabricaConexao.getConexao();

            GheFonteAgenteExame gheFonteAgenteExameNovo = null;

            IGheFonteAgenteExameDao gheFonteAgenteExameDao = FabricaDao.getGheFonteAgenteExameDao();

            NpgsqlTransaction transaction = dbConnection.BeginTransaction();

            try
            {

                gheFonteAgenteExameNovo = gheFonteAgenteExameDao.insert(gheFonteAgenteExame, dbConnection);
                transaction.Commit();
                return gheFonteAgenteExameNovo;

            }
            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - incluirGheFonteAgenteExame", ex);
                transaction.Rollback();
                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }
            finally
            {
                FabricaConexao.fecharConexao();
            }
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public void insertGheFonteAgenteExamePeriodicidade(GheFonteAgenteExamePeriodicidade gheFonteAgenteExamePeriodicidade)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método incluirGheFonteAgenteExamePeriodicidade");

            NpgsqlConnection dbConnection = FabricaConexao.getConexao();

            IGheFonteAgenteExamePeriodicidadeDao gheFonteAgenteExamePeriodicidadeDao = FabricaDao.getGheFonteAgenteExamePeriodicidadeDao();

            NpgsqlTransaction transaction = dbConnection.BeginTransaction();

            try
            {
                gheFonteAgenteExamePeriodicidadeDao.insert(gheFonteAgenteExamePeriodicidade, dbConnection);
                transaction.Commit();

            }
            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - incluirGheFonteAgenteExamePeriodicidade", ex);
                transaction.Rollback();
                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }
            finally
            {
                FabricaConexao.fecharConexao();
            }
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public DataSet findMaterilHospitalarByFilter(MaterialHospitalar materialHospitalar)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findMaterilHospitalarByFilter");

            NpgsqlConnection dbConnection = FabricaConexao.getConexao();

            IMaterialHospitalarDao materialHospitalarDao = FabricaDao.getMaterialHospitalarDao();

            try
            {
                return materialHospitalarDao.findByFilter(materialHospitalar, dbConnection);

            }
            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findMaterilHospitalarByFilter", ex);
                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }
            finally
            {
                FabricaConexao.fecharConexao();
            }
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public MaterialHospitalar insertMaterialHospitalar(MaterialHospitalar materialHospitalar)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método incluiMaterialHospitalar");

            MaterialHospitalar materialHospitalarNovo = null;
            NpgsqlConnection dbConnection = FabricaConexao.getConexao();
            IMaterialHospitalarDao materialHospitalarDao = FabricaDao.getMaterialHospitalarDao();
            NpgsqlTransaction transaction = dbConnection.BeginTransaction();

            try
            {
                // verificando se já existe um material hospitalar cadastrado com essa descrição
                if (materialHospitalarDao.findByDescricao(materialHospitalar.Descricao, dbConnection) != null)
                    throw new MaterialHospitalarException(MaterialHospitalarException.msg);

                materialHospitalarNovo = materialHospitalarDao.insert(materialHospitalar, dbConnection);

                transaction.Commit();
            }
            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - incluiMaterialHospitalar", ex);
                transaction.Rollback();
                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }
            finally
            {
                FabricaConexao.fecharConexao();
            }

            return materialHospitalarNovo;
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public MaterialHospitalar findMaterialHospitalarById(Int64 id)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findMaterialHospitalarById");

            MaterialHospitalar materialHospitalarProcurado = null;
            NpgsqlConnection dbConnection = FabricaConexao.getConexao();
            IMaterialHospitalarDao materialHospitalarDao = FabricaDao.getMaterialHospitalarDao();

            try
            {
                materialHospitalarProcurado = materialHospitalarDao.findById(id, dbConnection);
            }
            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findMaterialHospitalarById", ex);
                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }
            finally
            {
                FabricaConexao.fecharConexao();
            }

            return materialHospitalarProcurado;
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public MaterialHospitalar updateMaterialHospitalar(MaterialHospitalar materialHospitalar)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método alterarMaterialHospitalar");

            MaterialHospitalar materialHospitalarProcura = null;
            NpgsqlConnection dbConnection = FabricaConexao.getConexao();
            IMaterialHospitalarDao materialHospitalarDao = FabricaDao.getMaterialHospitalarDao();
            NpgsqlTransaction transaction = dbConnection.BeginTransaction();
            try
            {
                // verificando se já existe um material hospitalar cadastrado com essa descrição.
                materialHospitalarProcura = materialHospitalarDao.findByDescricao(Convert.ToString(materialHospitalar.Descricao), dbConnection);

                if (materialHospitalarProcura != null && materialHospitalarProcura.Id != materialHospitalar.Id)
                    throw new MaterialHospitalarException(MaterialHospitalarException.msg);

                materialHospitalar = materialHospitalarDao.update(materialHospitalar, dbConnection);
                transaction.Commit();

            }
            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - alterarMaterialHospitalar", ex);
                transaction.Rollback();
                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }
            finally
            {
                FabricaConexao.fecharConexao();
            }

            return materialHospitalar;
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public void insertMaterialHospitalarEstudo(MaterialHospitalarEstudo materialHospitalarEstudo)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método inserirMaterialHospitalarEstudo");

            NpgsqlConnection dbConnection = FabricaConexao.getConexao();
            IMaterialHospitalarEstudoDao materialHospitalarEstudoDao = FabricaDao.getMaterialHospitalarEstudoDao();
            NpgsqlTransaction transaction = dbConnection.BeginTransaction();
            try
            {

                materialHospitalarEstudoDao.insertMaterialHospitalarEstudo(materialHospitalarEstudo, dbConnection);
                transaction.Commit();

            }
            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - inserirMaterialHospitalarEstudo", ex);
                transaction.Rollback();
                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }
            finally
            {
                FabricaConexao.fecharConexao();
            }
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public DataSet findAllMaterialHospitalarEstudo(Estudo pcmso)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findAllMaterialHospitalarEstudo");

            NpgsqlConnection dbConnection = FabricaConexao.getConexao();

            IMaterialHospitalarEstudoDao materialHospitalarEstudoDao = FabricaDao.getMaterialHospitalarEstudoDao();

            try
            {
                return materialHospitalarEstudoDao.findAllMaterialHospitalarEstudo(pcmso, dbConnection);

            }
            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findAllMaterialHospitalarEstudo", ex);
                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }
            finally
            {
                FabricaConexao.fecharConexao();
            }
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public void updateMaterialHospitalarEstudo(MaterialHospitalarEstudo materialHospitalarEstudo)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método alteraMaterialHospitalarEstudo");

            NpgsqlConnection dbConnection = FabricaConexao.getConexao();
            IMaterialHospitalarEstudoDao materialHospitalarEstudoDao = FabricaDao.getMaterialHospitalarEstudoDao();
            NpgsqlTransaction transaction = dbConnection.BeginTransaction();
            try
            {

                materialHospitalarEstudoDao.updateMaterialHospitalarEstudo(materialHospitalarEstudo, dbConnection);
                transaction.Commit();

            }
            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - alteraMaterialHospitalarEstudo", ex);
                transaction.Rollback();
                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }
            finally
            {
                FabricaConexao.fecharConexao();
            }
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public void deleteMaterialHospitalarEstudo(MaterialHospitalarEstudo materialHospitalarEstudo)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método excluirMaterialHospitalarEstudo");

            NpgsqlConnection dbConnection = FabricaConexao.getConexao();
            IMaterialHospitalarEstudoDao materialHospitalarEstudoDao = FabricaDao.getMaterialHospitalarEstudoDao();
            NpgsqlTransaction transaction = dbConnection.BeginTransaction();
            try
            {

                materialHospitalarEstudoDao.deleteMaterialHospitalarEstudo(materialHospitalarEstudo, dbConnection);
                transaction.Commit();

            }
            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - excluirMaterialHospitalarEstudo", ex);
                transaction.Rollback();
                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }
            finally
            {
                FabricaConexao.fecharConexao();
            }
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public DataSet findHospitalByFilter(Hospital hospital)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findHospitalByFilter");

            NpgsqlConnection dbConnection = FabricaConexao.getConexao();

            IHospitalDao hospitalDao = FabricaDao.getHospitalDao();

            try
            {
                return hospitalDao.findHospitalByFilter(hospital, dbConnection);

            }
            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findHospitalByFilter", ex);
                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }
            finally
            {
                FabricaConexao.fecharConexao();
            }
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public Hospital insertHospital(Hospital hospital)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método incluirHospital");

            Hospital hospitalNovo = null;
            NpgsqlConnection dbConnection = FabricaConexao.getConexao();
            IHospitalDao hospitalDao = FabricaDao.getHospitalDao();
            NpgsqlTransaction transaction = dbConnection.BeginTransaction();
            try
            {
                // verificando se já existe um hospital cadastrado com essa descrição
                if (hospitalDao.findByNome(Convert.ToString(hospital.Nome), dbConnection) != null)
                    throw new HospitalException(HospitalException.msg);

                hospitalNovo = hospitalDao.insert(hospital, dbConnection);

                transaction.Commit();
            }
            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - incluirHospital", ex);
                transaction.Rollback();
                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }
            finally
            {
                FabricaConexao.fecharConexao();
            }

            return hospitalNovo;
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public Hospital findHospitalById(Int64 id)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findHospitalById");

            Hospital hospitalProcurado = null;
            NpgsqlConnection dbConnection = FabricaConexao.getConexao();
            IHospitalDao hospitalDao = FabricaDao.getHospitalDao();

            try
            {
                hospitalProcurado = hospitalDao.findHospitalById(id, dbConnection);
            }
            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findHospitalById", ex);
                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }
            finally
            {
                FabricaConexao.fecharConexao();
            }

            return hospitalProcurado;
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public Hospital updateHospital(Hospital hospital)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método alterarHospital");

            Hospital hospitalAlterado = null;
            NpgsqlConnection dbConnection = FabricaConexao.getConexao();
            IHospitalDao hospitalDao = FabricaDao.getHospitalDao();
            NpgsqlTransaction transaction = dbConnection.BeginTransaction();
            try
            {
                // verificando se já existe um hospital cadastrado com essa descrição.
                hospitalAlterado = hospitalDao.findByNome(Convert.ToString(hospital.Nome), dbConnection);

                if (hospitalAlterado != null && hospitalAlterado.Id != hospital.Id)
                    throw new HospitalException(HospitalException.msg);

                hospitalAlterado = hospitalDao.update(hospital, dbConnection);
                transaction.Commit();

            }
            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - alterarHospital", ex);
                transaction.Rollback();
                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }
            finally
            {
                FabricaConexao.fecharConexao();
            }

            return hospitalAlterado;
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public void insertHospitalEstudo(HospitalEstudo hospitalEstudo)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método incluirHospitalEstudo");

            NpgsqlConnection dbConnection = FabricaConexao.getConexao();

            IHospitalEstudoDao hospitalEstudoDao = FabricaDao.getHospitalEstudoDao();

            NpgsqlTransaction transaction = dbConnection.BeginTransaction();
            try
            {

                hospitalEstudoDao.incluirHospitalEstudo(hospitalEstudo, dbConnection);

                transaction.Commit();
            }
            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - incluirHospitalEstudo", ex);
                transaction.Rollback();
                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }
            finally
            {
                FabricaConexao.fecharConexao();
            }

        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public DataSet findAllHospitalEstudo(Estudo pcmso)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findAllHospitalEstudo");

            NpgsqlConnection dbConnection = FabricaConexao.getConexao();

            IHospitalEstudoDao hospitalEstudoDao = FabricaDao.getHospitalEstudoDao();

            try
            {
                return hospitalEstudoDao.findAllHospitalEstudo(pcmso, dbConnection);

            }
            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findAllHospitalEstudo", ex);
                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }
            finally
            {
                FabricaConexao.fecharConexao();
            }

        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public void deleteHospitalEstudo(HospitalEstudo hospitalEstudo)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método excluirHospitalEstudo");

            NpgsqlConnection dbConnection = FabricaConexao.getConexao();

            IHospitalEstudoDao hospitalEstudoDao = FabricaDao.getHospitalEstudoDao();

            NpgsqlTransaction transaction = dbConnection.BeginTransaction();
            try
            {
                hospitalEstudoDao.excluirHospitalEstudo(hospitalEstudo, dbConnection);

                transaction.Commit();
            }
            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - excluirHospitalEstudo", ex);
                transaction.Rollback();
                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }
            finally
            {
                FabricaConexao.fecharConexao();
            }

        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public DataSet findMedicoByFilter(Medico medico)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findMedicoByFilter");

            NpgsqlConnection dbConnection = FabricaConexao.getConexao();

            IMedicoDao medicoDao = FabricaDao.getMedicoDao();

            try
            {
                return medicoDao.findByFilter(medico, dbConnection);

            }
            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findMedicoByFilter", ex);
                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }
            finally
            {
                FabricaConexao.fecharConexao();
            }
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public Medico insertMedico(Medico medico)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método incluirMedico");

            Medico medicoNovo = null;
            NpgsqlConnection dbConnection = FabricaConexao.getConexao();
            IMedicoDao medicoDao = FabricaDao.getMedicoDao();
            NpgsqlTransaction transaction = dbConnection.BeginTransaction();
            try
            {

                if (medicoDao.findMedicoByCRM(medico.Crm, dbConnection))
                    throw new MedicoException(MedicoException.msg1);

                medicoNovo = medicoDao.insert(medico, dbConnection);
                transaction.Commit();

            }
            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - incluirMedico", ex);
                transaction.Rollback();
                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }
            finally
            {
                FabricaConexao.fecharConexao();
            }

            return medicoNovo;
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public Medico findMedicoById(Int64 id)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findMedicoById");

            Medico medicoProcurado = null;
            NpgsqlConnection dbConnection = FabricaConexao.getConexao();
            IMedicoDao medicoDao = FabricaDao.getMedicoDao();

            try
            {
                medicoProcurado = medicoDao.findById(id, dbConnection);
            }
            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findMedicoById", ex);
                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }
            finally
            {
                FabricaConexao.fecharConexao();
            }
            return medicoProcurado;
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public Medico updateMedico(Medico medico)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método alterarMedico");

            Medico medicoAlterado = null;

            NpgsqlConnection dbConnection = FabricaConexao.getConexao();

            IMedicoDao medicoDao = FabricaDao.getMedicoDao();
            IMedicoEstudoDao medicoEstudoDao = FabricaDao.getMedicoEstudoDao();

            NpgsqlTransaction transaction = dbConnection.BeginTransaction();

            try
            {
                if (!medicoEstudoDao.medicoIsUsed(medico, dbConnection))
                {

                    Medico medicoProcura = medicoDao.findMedicoByCrm(medico.Crm, dbConnection);

                    if (medicoProcura == null || medico.Id == medicoProcura.Id)
                    {
                        medicoAlterado = medicoDao.update(medico, dbConnection);
                        transaction.Commit();
                    }
                    else
                    {
                        throw new MedicoException(MedicoException.msg4);
                    }

                }
                else
                {
                    throw new MedicoException(MedicoException.msg4);
                }

            }
            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - alterarMedico", ex);
                transaction.Rollback();
                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }
            finally
            {
                FabricaConexao.fecharConexao();
            }

            return medicoAlterado;
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public DataSet findAllMedicoEstudo(Estudo pcmso)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findAllMedico");

            NpgsqlConnection dbConnection = FabricaConexao.getConexao();

            IMedicoEstudoDao medicoEstudoDao = FabricaDao.getMedicoEstudoDao();

            try
            {
                return medicoEstudoDao.findAllByEstudo(pcmso, dbConnection);

            }
            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findAllMedico", ex);
                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }
            finally
            {
                FabricaConexao.fecharConexao();
            }
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public MedicoEstudo insertMedicoEstudo(MedicoEstudo medicoEstudo)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método incluirMedicoEstudo");

            NpgsqlConnection dbConnection = FabricaConexao.getConexao();

            IMedicoEstudoDao medicoEstudoDao = FabricaDao.getMedicoEstudoDao();

            MedicoEstudo newMedicoEstudo = null;

            NpgsqlTransaction transaction = dbConnection.BeginTransaction();
            try
            {

                newMedicoEstudo = medicoEstudoDao.insert(medicoEstudo, dbConnection);

                transaction.Commit();
            }
            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - incluirMedicoEstudo", ex);
                transaction.Rollback();
                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }
            finally
            {
                FabricaConexao.fecharConexao();
            }

            return newMedicoEstudo;

        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public void deleteMedicoEstudo(MedicoEstudo medicoEstudo)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método excluirMedicoEstudo");

            NpgsqlConnection dbConnection = FabricaConexao.getConexao();

            IMedicoEstudoDao medicoEstudoDao = FabricaDao.getMedicoEstudoDao();

            NpgsqlTransaction transaction = dbConnection.BeginTransaction();
            try
            {

                medicoEstudoDao.delete(medicoEstudo, dbConnection);

                transaction.Commit();
            }
            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - excluirMedicoEstudo", ex);
                transaction.Rollback();
                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }
            finally
            {
                FabricaConexao.fecharConexao();
            }

        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public HashSet<Nota> findAllNotaByGhe(Ghe ghe)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findAllNotaByGhe");

            NpgsqlConnection dbConnection = FabricaConexao.getConexao();

            IGheNotaDao gheNormaDao = FabricaDao.getGheNormaDao();

            try
            {
                return gheNormaDao.findAllByGhe(ghe, dbConnection);

            }
            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findNormaByFilter", ex);
                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }
            finally
            {
                FabricaConexao.fecharConexao();
            }

        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public void insertGheNota(GheNota gheNota)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método incluirGheNota");

            NpgsqlConnection dbConnection = FabricaConexao.getConexao();
            IGheNotaDao gheNormaDao = FabricaDao.getGheNormaDao();
            NpgsqlTransaction transaction = dbConnection.BeginTransaction();

            try
            {
                gheNormaDao.insert(gheNota, dbConnection);
                transaction.Commit();

            }
            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findNormaByFilter", ex);
                transaction.Rollback();
                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }
            finally
            {
                FabricaConexao.fecharConexao();
            }
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public Cronograma insertCronograma(Cronograma cronograma, Estudo pcmso)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método incluirCronograma");

            Cronograma cronogramaNovo = null;

            NpgsqlConnection dbConnection = FabricaConexao.getConexao();

            ICronogramaDao cronogramaDao = FabricaDao.getCronogramaDao();
            IEstudoDao pcmsoDao = FabricaDao.getEstudoDao();

            NpgsqlTransaction transaction = dbConnection.BeginTransaction();

            try
            {
                cronogramaNovo = cronogramaDao.insert(cronograma, dbConnection);

                // incluindo informação do cronograma no pcmso.

                pcmsoDao.gravaCronograma(cronogramaNovo, pcmso, dbConnection);

                transaction.Commit();
            }
            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - incluirCronograma", ex);
                transaction.Rollback();
                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }
            finally
            {
                FabricaConexao.fecharConexao();
            }
            return cronogramaNovo;
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public CronogramaAtividade insertCronogramaAtividade(CronogramaAtividade cronogramaAtividade)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método incluirCronogramaAtividade");

            NpgsqlConnection dbConnection = FabricaConexao.getConexao();

            ICronogramaAtividadeDao cronogramaAtividadeDao = FabricaDao.getCronogramaAtividadeDao();

            NpgsqlTransaction transaction = dbConnection.BeginTransaction();

            try
            {

                cronogramaAtividade = cronogramaAtividadeDao.insert(cronogramaAtividade, dbConnection);
                transaction.Commit();
            }
            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - incluirCronogramaAtividade", ex);
                transaction.Rollback();
                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }
            finally
            {
                FabricaConexao.fecharConexao();
            }
            return cronogramaAtividade;
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public void updateCronogramaAtividade(CronogramaAtividade cronogramaAtividade)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método alterarCronogramaAtividade");

            NpgsqlConnection dbConnection = FabricaConexao.getConexao();

            ICronogramaAtividadeDao cronogramaAtividadeDao = FabricaDao.getCronogramaAtividadeDao();

            NpgsqlTransaction transaction = dbConnection.BeginTransaction();

            try
            {
                cronogramaAtividadeDao.update(cronogramaAtividade, dbConnection);
                transaction.Commit();
            }
            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - alterarCronogramaAtividade", ex);
                transaction.Rollback();
                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }
            finally
            {
                FabricaConexao.fecharConexao();
            }

        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public void deleteCronogramaAtividade(CronogramaAtividade cronogramaAtividade)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método excluiCronogramaAtividade");

            NpgsqlConnection dbConnection = FabricaConexao.getConexao();

            ICronogramaAtividadeDao cronogramaAtividadeDao = FabricaDao.getCronogramaAtividadeDao();

            NpgsqlTransaction transaction = dbConnection.BeginTransaction();

            try
            {
                cronogramaAtividadeDao.delete(cronogramaAtividade, dbConnection);
                transaction.Commit();
            }
            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - excluiCronogramaAtividade", ex);
                transaction.Rollback();
                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }
            finally
            {
                FabricaConexao.fecharConexao();
            }

        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public Estudo findById(Int64 id)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findById");

            NpgsqlConnection dbConnection = FabricaConexao.getConexao();

            Estudo pcmso = null;

            IEstudoDao estudoDao = FabricaDao.getEstudoDao();
            IClienteVendedorDao clienteVendedorDao = FabricaDao.getClienteVendedorDao();
            ICronogramaAtividadeDao cronogramaAtividadeDao = FabricaDao.getCronogramaAtividadeDao();
            ICronogramaDao cronogramaDao = FabricaDao.getCronogramaDao();
            IUsuarioDao usuarioDao = FabricaDao.getUsuarioDao();
            IClienteDao clienteDao = FabricaDao.getClienteDao();
            IMedicoEstudoDao medicoEstudoDao = FabricaDao.getMedicoEstudoDao();

            try
            {
                pcmso = estudoDao.findById(id, dbConnection);

                pcmso.VendedorCliente = (VendedorCliente)clienteVendedorDao.findById((Int64)pcmso.VendedorCliente.Id, dbConnection);

                if (pcmso.Tecno != null && pcmso.Tecno.Id != null)
                    pcmso.Tecno = usuarioDao.findById((Int64)pcmso.Tecno.Id, dbConnection);

                if (pcmso.ClienteContratante != null)
                    pcmso.ClienteContratante = clienteDao.findById((Int64)pcmso.ClienteContratante.Id, dbConnection);

                if (pcmso.Cronograma != null && pcmso.Cronograma.Id != null)
                {
                    pcmso.Cronograma = cronogramaDao.findById((Int64)pcmso.Cronograma.Id, dbConnection);
                    pcmso.Cronograma.CronogramaAtividade = cronogramaAtividadeDao.findByCronograma(pcmso.Cronograma, dbConnection);
                }

                /* localizando os medicos */

                pcmso.Medicos = medicoEstudoDao.findAllByPcmsoList(pcmso, dbConnection);

            }
            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findById", ex);

                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }
            finally
            {
                FabricaConexao.fecharConexao();
            }

            return pcmso;
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public Cronograma findCronogramaByPcmso(Estudo pcmso)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findCronogramaByPcmso");

            NpgsqlConnection dbConnection = FabricaConexao.getConexao();

            ICronogramaDao cronogramaDao = FabricaDao.getCronogramaDao();

            try
            {
                return cronogramaDao.findByEstudo(pcmso, dbConnection);

            }
            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findCronogramaByPcmso", ex);
                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }
            finally
            {
                FabricaConexao.fecharConexao();

            }

        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public Estudo replicarPcmso(Int64 idPcmso, string comentario, Boolean indCorrecao)
        {
            NpgsqlConnection dbConnection = FabricaConexao.getConexao();
            IEstudoDao estudoDao = FabricaDao.getEstudoDao();
            ICnaeEstudoDao cnaePpraDao = FabricaDao.getCnaeEstudoDao();
            IGheDao gheDao = FabricaDao.getGheDao();
            IGheNotaDao gheNormaDao = FabricaDao.getGheNormaDao();
            IGheSetorDao gheSetorDao = FabricaDao.getGheSetorDao();
            IGheFonteDao gheFonteDao = FabricaDao.getGheFonteDao();
            IGheSetorClienteFuncaoDao gheSetorClienteFuncaoDao = FabricaDao.getGheSetorClienteFuncaoDao();
            IGheFonteAgenteDao gheFonteAgenteDao = FabricaDao.getGheFonteAgenteDao();
            IGheFonteAgenteExameDao gheFonteAgenteExameDao = FabricaDao.getGheFonteAgenteExameDao();
            ICronogramaDao cronogramaDao = FabricaDao.getCronogramaDao();
            ICronogramaAtividadeDao cronogramaAtividadeDao = FabricaDao.getCronogramaAtividadeDao();
            IRevisaoDao revisaoDao = FabricaDao.getRevisaoDao();
            IMedicoDao medicoDao = FabricaDao.getMedicoDao();
            IMedicoEstudoDao medicoEstudoDao = FabricaDao.getMedicoEstudoDao();
            IMaterialHospitalarEstudoDao materialHospitalarEstudoDao = FabricaDao.getMaterialHospitalarEstudoDao();
            IHospitalDao hospitalDao = FabricaDao.getHospitalDao();
            IHospitalEstudoDao hospitalEstudoDao = FabricaDao.getHospitalEstudoDao();
            IGheFonteAgenteExamePeriodicidadeDao gheFonteAgenteExamePeriodicidadeDao = FabricaDao.getGheFonteAgenteExamePeriodicidadeDao();
            IRelatorioDao relatorioDao = FabricaDao.getRelatorioDao();
            IEstimativaEstudoDao estimativaEstudoDao = FabricaDao.getEstimativaEstudoDao();
            IDocumentoEstudoDao documentoEstudoDao = FabricaDao.getDocumentoEstudoDao();
            
            Int64 idPcmsoNovo;
            Estudo novoPcmso = null;
            Ghe novoGhe;
            Ghe gheAntigo;
            Cronograma cronogramaAntigo;
            Cronograma cronogramaNovo;

            NpgsqlTransaction transaction = dbConnection.BeginTransaction();
            try
            {
                Estudo pcmsoOriginal = new Estudo();
                pcmsoOriginal.Id = idPcmso;
                
                //Replicando cronograma
                cronogramaAntigo = cronogramaDao.findByEstudo(new Estudo(idPcmso), dbConnection);
                cronogramaAntigo.CronogramaAtividade = cronogramaAtividadeDao.findByCronograma(cronogramaAntigo, dbConnection);

                cronogramaNovo = cronogramaDao.insert(cronogramaAntigo, dbConnection);
                foreach (CronogramaAtividade cronogramaAtividade in cronogramaAntigo.CronogramaAtividade)
                {
                    cronogramaAtividade.Cronograma = cronogramaNovo;
                    cronogramaAtividadeDao.insert(cronogramaAtividade, dbConnection);
                }

                //Copiando os dados brutos do estudo
                idPcmsoNovo = estudoDao.replicaEstudo(idPcmso, cronogramaNovo.Id, relatorioDao.findUltimaVersaoByTipo(ApplicationConstants.RELATORIO_PCMSO, dbConnection), dbConnection);
                Estudo pcmsoRevisao = new Estudo();
                pcmsoRevisao.Id = idPcmsoNovo;

                //Replicando CNAE_ESTUDO
                cnaePpraDao.replicaCnaeEstudo(idPcmso, idPcmsoNovo, dbConnection);

                //Replica FUNCAO_EPI
                //estudoDao.replicaFuncaoEpi(idEstudo, idEstudoNovo, dbConnection);

                //Replicando Médicos
                foreach (Medico medico in medicoDao.findAllByPcmso(new Estudo(idPcmso), dbConnection))
                {
                    medicoEstudoDao.insert(new MedicoEstudo(new Estudo(idPcmsoNovo), medico, false, ApplicationConstants.ATIVO), dbConnection);
                }

                //Replicando Materiais Hospitalares
                foreach (MaterialHospitalarEstudo materialHospitalarEstudo in materialHospitalarEstudoDao.findAllByEstudo(new Estudo(idPcmso), dbConnection))
                {
                    materialHospitalarEstudo.Estudo = new Estudo(idPcmsoNovo);
                    materialHospitalarEstudoDao.insertMaterialHospitalarEstudo(materialHospitalarEstudo, dbConnection);
                }

                //Replicando Hospitais
                foreach (Hospital hospital in hospitalDao.findAllByEstudo(new Estudo(idPcmso), dbConnection))
                {
                    hospitalEstudoDao.incluirHospitalEstudo(new HospitalEstudo(new Estudo(idPcmsoNovo), hospital), dbConnection);
                }

                foreach (Ghe ghe in gheDao.findAllByEstudo(idPcmso, dbConnection))
                {
                    gheAntigo = gheDao.findById(ghe.Id, dbConnection);
                    gheAntigo.GheSetor = gheSetorDao.findAtivosByGhe(gheAntigo, dbConnection);
                    gheAntigo.GheFonte = gheFonteDao.findAllAtivoByGhe(gheAntigo, dbConnection);

                    //Criando GHE
                    novoPcmso = new Estudo(idPcmsoNovo);
                    ghe.Estudo = novoPcmso;
                    novoGhe = gheDao.insert(ghe, dbConnection);

                    //Replicando Normas
                    GheNota gheNorma;
                    foreach (Nota norma in gheAntigo.Norma)
                    {
                        gheNorma = new GheNota(novoGhe, norma);
                        gheNormaDao.insert(gheNorma, dbConnection);
                    }

                    //Replicando setores dos GHE's
                    GheSetor gheSetorNovo;
                    GheSetor gheSetorAntigo;
                    foreach (GheSetor gheSetor in gheAntigo.GheSetor)
                    {
                        gheSetorAntigo = new GheSetor(gheSetor.Id, gheSetor.Setor, gheSetor.Ghe, gheSetor.Situacao);
                        gheSetor.Ghe = novoGhe;
                        gheSetorNovo = gheSetorDao.insert(gheSetor, dbConnection);

                        //Replicando ghe_setor_cliente_funcao
                        foreach (GheSetorClienteFuncao gheSetorClienteFuncao in gheSetorClienteFuncaoDao.findAllByGheSetorClienteFuncao(new GheSetorClienteFuncao(null, null, gheSetorAntigo, false, false, ApplicationConstants.ATIVO, null, false), dbConnection))
                        {
                            gheSetorClienteFuncao.GheSetor = gheSetorNovo;
                            gheSetorClienteFuncaoDao.insert(gheSetorClienteFuncao, dbConnection);
                        }
                    }

                    //Replicando fontes dos GHE's
                    GheFonte gheFonteNovo;
                    GheFonte gheFonteAntigo;
                    foreach (GheFonte gheFonte in gheAntigo.GheFonte)
                    {
                        gheFonteAntigo = new GheFonte(gheFonte.Id, gheFonte.Fonte, gheFonte.Ghe, gheFonte.Principal, gheFonte.Situacao);
                        gheFonte.Ghe = novoGhe;
                        gheFonteNovo = gheFonteDao.insert(gheFonte, dbConnection);

                        //Replicando ghe_fonte_agente
                        GheFonteAgente gheFonteAgenteNovo;
                        GheFonteAgente gheFonteAgenteAntigo;
                        foreach (GheFonteAgente gheFonteAgente in gheFonteAgenteDao.findAtivosByGheFonteHash(gheFonteAntigo, dbConnection))
                        {
                            gheFonteAgenteAntigo = new GheFonteAgente(gheFonteAgente.Id, gheFonteAgente.GradSoma, gheFonteAgente.GradEfeito,
                                gheFonteAgente.GradExposicao, gheFonteAgente.GheFonte, gheFonteAgente.Agente, gheFonteAgente.TempoExposicao, gheFonteAgente.Situacao);
                            gheFonteAgente.GheFonte = gheFonteNovo;
                            gheFonteAgenteNovo = gheFonteAgenteDao.insert(gheFonteAgente, dbConnection);

                            //Replicando ghe_fonte_agente_exame
                            GheFonteAgenteExame gheFonteAgenteExameAntigo;
                            GheFonteAgenteExame gheFonteAgenteExameNovo;
                            foreach (GheFonteAgenteExame gheFonteAgenteExame in gheFonteAgenteExameDao.findAllByGheFonteAgente(gheFonteAgenteAntigo, dbConnection))
                            {
                                gheFonteAgenteExameAntigo = new GheFonteAgenteExame(gheFonteAgenteExame.Id,
                                    new Exame(gheFonteAgenteExame.Id, gheFonteAgenteExame.Exame.Descricao, gheFonteAgenteExame.Exame.Situacao,
                                        gheFonteAgenteExame.Exame.Laboratorio, 0, 0, 0, false, false, string.Empty, false, null, false), new GheFonteAgente(gheFonteAgenteExame.GheFonteAgente.Id,
                                            gheFonteAgenteExame.GheFonteAgente.GradSoma, gheFonteAgenteExame.GheFonteAgente.GradEfeito,
                                            gheFonteAgenteExame.GheFonteAgente.GradExposicao, gheFonteAgenteExame.GheFonteAgente.GheFonte,
                                            gheFonteAgenteExame.GheFonteAgente.Agente, gheFonteAgente.TempoExposicao, gheFonteAgente.Situacao), gheFonteAgenteExame.IdadeExame,
                                            gheFonteAgenteExame.Situacao, gheFonteAgenteExame.Confinado, gheFonteAgenteExame.Altura, gheFonteAgenteExame.Eletricidade);

                                gheFonteAgenteExame.GheFonteAgente = gheFonteAgenteNovo;
                                gheFonteAgenteExameNovo = gheFonteAgenteExameDao.insert(gheFonteAgenteExame, dbConnection);

                                foreach (GheFonteAgenteExamePeriodicidade gheFonteAgenteExamePeriodicidade in gheFonteAgenteExamePeriodicidadeDao.findAll(gheFonteAgenteExameAntigo, dbConnection))
                                {
                                    gheFonteAgenteExamePeriodicidade.GheFonteAgenteExame = gheFonteAgenteExameNovo;
                                    gheFonteAgenteExamePeriodicidadeDao.insert(gheFonteAgenteExamePeriodicidade, dbConnection);
                                }
                            }
                        }
                    }
                }

                /* replicando dados de estimativa */

                DataSet ds = estimativaEstudoDao.findAllByEstudo(pcmsoOriginal, dbConnection);

                foreach (DataRow row in ds.Tables[0].Rows)
                {
                    EstimativaEstudo newEstimativa = new EstimativaEstudo();
                    Estimativa estimativa = new Estimativa();
                    estimativa.Id = (long)row["ID_ESTIMATIVA"];
                    estimativa.Descricao = row["DESCRICAO"].ToString();
                    estimativa.Situacao = ApplicationConstants.ATIVO;

                    newEstimativa.Estimativa = estimativa;
                    newEstimativa.Estudo = pcmsoRevisao;
                    newEstimativa.Quantidade = Convert.ToInt32(row["QUANTIDADE"]);

                    estimativaEstudoDao.insert(newEstimativa, dbConnection);
                }

                /* replicando relacionamento com documentos */

                List<DocumentoEstudo> documentosInEstudo = new List<DocumentoEstudo>();
                documentosInEstudo = documentoEstudoDao.findAllByEstudoList(pcmsoOriginal, dbConnection);

                foreach (DocumentoEstudo documentoEstudo in documentosInEstudo)
                {
                    DocumentoEstudo newDocumentoEstudo = new DocumentoEstudo();
                    newDocumentoEstudo.Documento = documentoEstudo.Documento;
                    newDocumentoEstudo.Estudo = pcmsoRevisao;
                    documentoEstudoDao.insert(newDocumentoEstudo, dbConnection);
                }

                Int64? idPpraRaiz = revisaoDao.findRaiz(idPcmso, dbConnection);
                string codigoPpra = estudoDao.findCodigoEstudoById(idPcmso, dbConnection);
                if (idPpraRaiz == null)
                {
                    idPpraRaiz = idPcmso;
                }

                //Alimenta Tabela de Revisão
                Int32 versao = Int32.Parse(codigoPpra.Substring(codigoPpra.Length - 2, 2)) + 1;
                revisaoDao.incluir(new Revisao(null, novoPcmso, comentario, new Estudo(idPpraRaiz), new Estudo(idPcmso), "PCMSO", versao.ToString().PadLeft(2, '0'), null, indCorrecao), dbConnection);

                transaction.Commit();

                return estudoDao.findById(idPcmsoNovo, dbConnection);

            }
            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - replicarPcmso", ex);
                transaction.Rollback();
                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }
            finally
            {
                FabricaConexao.fecharConexao();
            }
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public void changeEstadoPcmso(Estudo pcmso, String estado, Produto produtoSelecionado)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método mudaEstadoPcmso");

            NpgsqlConnection dbConnection = FabricaConexao.getConexao();

            NpgsqlTransaction transaction = dbConnection.BeginTransaction();

            IEstudoDao estudoDao = FabricaDao.getEstudoDao();
            ICronogramaDao cronogramaDao = FabricaDao.getCronogramaDao();
            ICronogramaAtividadeDao cronogramaAtividadeDao = FabricaDao.getCronogramaAtividadeDao();
            IContratoDao contratoDao = FabricaDao.getContratoDao();
            IProdutoDao produtoDao = FabricaDao.getProdutoDao();
            IContratoProdutoDao contratoProdutoDao = FabricaDao.getContratoProdutoDao();
            IMovimentoDao movimentoDao = FabricaDao.getMovimentoDao();
            IRevisaoDao revisaoDao = FabricaDao.getRevisaoDao();
            IClienteDao clienteDao = FabricaDao.getClienteDao();

            Dictionary<Int64, CronogramaAtividade> mapaCronograma = new Dictionary<long, CronogramaAtividade>();
            Cronograma cronograma = new Cronograma();
            Cliente cliente = null;
            Cliente clienteCredenciada = null;


            try
            {

                #region validação

                // Regra de Validação 1 - Não pode existir estudo sem técnico.
                // Regra de negócio deixou de existir
                // 23/04/2013 - welbert santos
                /*
                if (pcmso.Tecno == null)
                {
                    throw new PcmsoException(PcmsoException.msg4);
                }
                */

                // Regra de validação 2 -  Não pode existir PCMSO sem cronograma e sem atividade.

                if (pcmso.Cronograma != null)
                {

                    cronograma = cronogramaDao.findById((Int64)pcmso.Cronograma.Id, dbConnection);

                    mapaCronograma = cronogramaAtividadeDao.findByCronogramaDic(cronograma, dbConnection);


                    if (mapaCronograma.Count == 0)
                    {
                        throw new PcmsoException(PcmsoException.msg6);
                    }

                }
                else
                {
                    throw new PcmsoException(PcmsoException.msg5);
                }

                #endregion

                if (pcmso.VendedorCliente.Cliente.CredenciadaCliente != null)
                {
                    cliente = pcmso.VendedorCliente.Cliente.CredenciadaCliente;
                    cliente = clienteDao.findById((Int64)cliente.Id, dbConnection);
                    clienteCredenciada = pcmso.VendedorCliente.Cliente;
                    clienteCredenciada = clienteDao.findById((long)clienteCredenciada.Id, dbConnection);
                }
                else
                {
                    cliente = pcmso.VendedorCliente.Cliente;
                    cliente = clienteDao.findById((Int64)cliente.Id, dbConnection);
                }

                if (String.Equals(estado, ApplicationConstants.ATIVO))
                {
                    estudoDao.changeEstado(pcmso, estado, dbConnection);

                }
                else if (String.Equals(estado, ApplicationConstants.CONSTRUCAO))
                {
                    // retirar elaborador do banco de dados.
                    pcmso.Engenheiro = null;
                    estudoDao.changeEstado(pcmso, estado, dbConnection);
                }

                estudoDao.changeEstado(pcmso, estado, dbConnection);

                if (estado.Equals("F"))
                {
                    estudoDao.finalizaEstudo(pcmso, dbConnection);

                    if (!revisaoDao.isCorrecao(pcmso.Id, dbConnection))
                    {
                        if (pcmso.VendedorCliente.Cliente.UsaContrato)
                        {
                            // buscando informações do contrato do cliente.

                            /* encontrato o contrato em vigência no período */
                            Contrato contratoVigente = contratoDao.findContratoVigenteByData((DateTime)pcmso.DataCriacao, pcmso.VendedorCliente.Cliente, null, dbConnection);

                            /* verificando a existencia do contrato. */
                            if (contratoVigente == null)
                            {
                                throw new ContratoException(ContratoException.msg2);
                            }

                            /* para localizar um estudo do pcmso, usar o id = 2;
                             * arquivo de carga. */

                            ContratoProduto contratoProduto = contratoProdutoDao.findByContratoAndProduto(contratoVigente, produtoSelecionado, dbConnection);

                            if (contratoProduto == null)
                            {
                                throw new ContratoException(ContratoException.msg3);
                            }


                            /* incluindo no movimento o produto. */

                            Movimento movimento = new Movimento(null, null, null, cliente, contratoProduto, null, DateTime.Now, null, ApplicationConstants.PENDENTE, Convert.ToDecimal(contratoProduto.PrecoContratado), Convert.ToDecimal(ValidaCampoHelper.CalculaValorComissao(contratoProduto, pcmso.VendedorCliente.Vendedor)), pcmso, null, Convert.ToInt32(1), PermissionamentoFacade.usuarioAutenticado, PermissionamentoFacade.usuarioAutenticado.Empresa, Convert.ToDecimal(contratoProduto.CustoContrato), !estudoDao.pcmsoIsUnidade(pcmso, dbConnection) ? null : pcmso.ClienteContratante != null ? pcmso.ClienteContratante : null, null, DateTime.Now, null, null, clienteCredenciada);
                            movimentoDao.insert(movimento, dbConnection);

                        }
                        else
                        {
                            // inserindo no movimento sem contrato
                            Contrato contratoSistema = contratoDao.findContratoSistemaByCliente(cliente, dbConnection);

                            if (contratoSistema == null)
                            {
                                // criando contrato para o cliente.
                                contratoSistema = contratoDao.insert(new Contrato(null, String.Empty, cliente,
                                    PermissionamentoFacade.usuarioAutenticado, null, null, null, ApplicationConstants.CONTRATO_FECHADO, String.Empty, null, null, String.Empty, null, true, null, null, false, false, false, null, false, null, null, null, null, null, false, null, null, null, String.Empty, PermissionamentoFacade.usuarioAutenticado.Empresa), dbConnection);

                                // criando contrato produto.
                                contratoProdutoDao.insert(new ContratoProduto(null, produtoSelecionado, contratoSistema,
                                    produtoSelecionado.Preco, PermissionamentoFacade.usuarioAutenticado, produtoSelecionado.Custo, null, String.Empty, true), dbConnection);


                                // buscando contrato produto

                                ContratoProduto contratoProdutoSelecinado = contratoProdutoDao.findByContratoAndProduto(contratoSistema, produtoSelecionado, dbConnection);

                                Movimento movimento = new Movimento(null, null, null, cliente, contratoProdutoSelecinado, null, DateTime.Now, null, ApplicationConstants.PENDENTE, Convert.ToDecimal(contratoProdutoSelecinado.PrecoContratado), Convert.ToDecimal(ValidaCampoHelper.CalculaValorComissao(contratoProdutoSelecinado, pcmso.VendedorCliente.Vendedor)), pcmso, null, Convert.ToInt32(1), PermissionamentoFacade.usuarioAutenticado, PermissionamentoFacade.usuarioAutenticado.Empresa, Convert.ToDecimal(contratoProdutoSelecinado.CustoContrato), !estudoDao.pcmsoIsUnidade(pcmso, dbConnection) ? null : pcmso.ClienteContratante != null ? pcmso.ClienteContratante : null, null, DateTime.Now, null, null, clienteCredenciada);
                                movimentoDao.insert(movimento, dbConnection);

                            }
                            else
                            {

                                ContratoProduto contratoProdutoSelecinado = contratoProdutoDao.findByContratoAndProduto(contratoSistema, produtoSelecionado, dbConnection);

                                if (contratoProdutoSelecinado != null)
                                {
                                    // atualizar o preço do produto
                                    contratoProdutoSelecinado.PrecoContratado = produtoSelecionado.Preco;
                                    contratoProdutoSelecinado.CustoContrato = produtoSelecionado.Custo;
                                    contratoProdutoDao.update(contratoProdutoSelecinado, dbConnection);
                                }
                                else
                                {
                                    contratoProdutoDao.insert(new ContratoProduto(null, produtoSelecionado, contratoSistema,
                                        produtoSelecionado.Preco, PermissionamentoFacade.usuarioAutenticado, null, null, String.Empty, true), dbConnection);

                                }

                                contratoProdutoSelecinado = contratoProdutoDao.findByContratoAndProduto(contratoSistema, produtoSelecionado, dbConnection);

                                Movimento movimento = new Movimento(null, null, null, cliente, contratoProdutoSelecinado, null, DateTime.Now, null, ApplicationConstants.PENDENTE, Convert.ToDecimal(contratoProdutoSelecinado.PrecoContratado), Convert.ToDecimal(ValidaCampoHelper.CalculaValorComissao(contratoProdutoSelecinado, pcmso.VendedorCliente.Vendedor)), pcmso, null, Convert.ToInt32(1), PermissionamentoFacade.usuarioAutenticado, PermissionamentoFacade.usuarioAutenticado.Empresa, Convert.ToDecimal(contratoProdutoSelecinado.CustoContrato), !estudoDao.pcmsoIsUnidade(pcmso, dbConnection) ? null : pcmso.ClienteContratante != null ? pcmso.ClienteContratante : null, null, DateTime.Now, null, null, clienteCredenciada);
                                movimentoDao.insert(movimento, dbConnection);

                            }
                        }
                    }
                }

                transaction.Commit();

            }
            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - mudaEstadoPcmso", ex);
                transaction.Rollback();
                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }
            finally
            {
                FabricaConexao.fecharConexao();
            }
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public void deletePcmso(Estudo pcmso)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método excluiPcmso");

            NpgsqlConnection dbConnection = FabricaConexao.getConexao();

            IEstudoDao estudoDao = FabricaDao.getEstudoDao();
            IGheDao gheDao = FabricaDao.getGheDao();
            IGheFonteDao gheFonteDao = FabricaDao.getGheFonteDao();
            IGheFonteAgenteDao gheFonteAgenteDao = FabricaDao.getGheFonteAgenteDao();
            IGheFonteAgenteExameDao gheFonteAgenteExameDao = FabricaDao.getGheFonteAgenteExameDao();
            IGheFonteAgenteExamePeriodicidadeDao gheFonteAgenteExamePeriodicidadeDao = FabricaDao.getGheFonteAgenteExamePeriodicidadeDao();
            IGheSetorDao gheSetorDao = FabricaDao.getGheSetorDao();
            IGheSetorClienteFuncaoDao gheSetorClienteFuncaoDao = FabricaDao.getGheSetorClienteFuncaoDao();

            NpgsqlTransaction transaction = dbConnection.BeginTransaction();

            try
            {
                /* verificando se o pcmso pode ser excluído */
                bool verificaPodeExcluirPcmso = estudoDao.isPcmsoUsedByAso((long)pcmso.Id, dbConnection);

                if (verificaPodeExcluirPcmso)
                {
                    /* TODO
                     * pcmso já usado. Deverá ser desativado. */
                    estudoDao.disablePcmso((long)pcmso.Id, dbConnection);

                    /*Recuperando os ghes do pcmso */
                    gheDao.findAllByEstudo((long)pcmso.Id, dbConnection).ToList().ForEach(delegate(Ghe ghe)
                    {
                        ghe.Estudo = pcmso;
                        ghe.Situacao = ApplicationConstants.DESATIVADO;
                        gheDao.update(ghe, dbConnection);

                        /* recuperando todos os ghes Fontes ligado ao ghe */

                        gheFonteDao.findAllByGheList(ghe, dbConnection).ForEach(delegate(GheFonte gheFonte)
                        {
                            gheFonte.Ghe = ghe;
                            gheFonte.Situacao = ApplicationConstants.DESATIVADO;
                            gheFonteDao.update(gheFonte, dbConnection);

                            /* recuperando todos os gheFonteAgentes ligados ao gheFonte */

                            gheFonteAgenteDao.findAllByGheFonteList(gheFonte, dbConnection).ForEach(delegate(GheFonteAgente gheFonteAgente)
                            {
                                gheFonteAgente.GheFonte = gheFonte;
                                gheFonteAgente.Situacao = ApplicationConstants.DESATIVADO;
                                gheFonteAgenteDao.update(gheFonteAgente, dbConnection);

                                /* recuperando todos os GheFonteAgentesExames ligados ao GheFonteAgente */
                                gheFonteAgenteExameDao.findAllByGheFonteAgente(gheFonteAgente, dbConnection).ToList().ForEach(delegate(GheFonteAgenteExame gheFonteAgenteExame)
                                {
                                    gheFonteAgenteExame.GheFonteAgente = gheFonteAgente;
                                    gheFonteAgenteExame.Situacao = ApplicationConstants.DESATIVADO;
                                    gheFonteAgenteExameDao.update(gheFonteAgenteExame, dbConnection);

                                    /* recuperando todos as periodicidades dos exames dados um GheFonteAgenteExame */

                                    gheFonteAgenteExamePeriodicidadeDao.findAll(gheFonteAgenteExame, dbConnection).ToList().ForEach(delegate(GheFonteAgenteExamePeriodicidade gheFonteAgenteExamePeriodicidade)
                                    {
                                        gheFonteAgenteExamePeriodicidade.GheFonteAgenteExame = gheFonteAgenteExame;
                                        gheFonteAgenteExamePeriodicidade.Situacao = ApplicationConstants.DESATIVADO;
                                        gheFonteAgenteExamePeriodicidadeDao.update(gheFonteAgenteExamePeriodicidade, dbConnection);

                                    });
                                });
                            });
                        });

                        /* recuperando todos os GheSetores dado um ghe */

                        gheSetorDao.findAtivosByGhe(ghe, dbConnection).ToList().ForEach(delegate(GheSetor gheSetor)
                        {
                            gheSetor.Ghe = ghe;
                            gheSetor.Situacao = ApplicationConstants.DESATIVADO;
                            gheSetorDao.update(gheSetor, dbConnection);

                            /* recuperando todos os gheSetoresClienteFuncoes data um gheSetor */
                            gheSetorClienteFuncaoDao.findAtivosByGheSetor(gheSetor, dbConnection).ToList().ForEach(delegate(GheSetorClienteFuncao gheSetorClienteFuncao)
                            {
                                gheSetorClienteFuncao.GheSetor = gheSetor;
                                gheSetorClienteFuncao.Situacao = ApplicationConstants.DESATIVADO;
                                gheSetorClienteFuncaoDao.update(gheSetorClienteFuncao, dbConnection);
                            });
                        });
                    });
                }
                else
                {
                    /* pcmso pode ser excluído do banco de dados */
                    estudoDao.delete((long)pcmso.Id, dbConnection);
                }
                
                transaction.Commit();

            }
            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - excluiPcmso", ex);
                transaction.Rollback();
                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }
            finally
            {
                FabricaConexao.fecharConexao();
            }
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public Boolean verificaPodeReplicarByRevisao(Int64? idPcmso)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método verificaPodeReplicarByRevisao");

            NpgsqlConnection dbConnection = FabricaConexao.getConexao();
            IRevisaoDao revisaoDao = FabricaDao.getRevisaoDao();

            try
            {
                return revisaoDao.verificaPodeReplicar(idPcmso, dbConnection);

            }
            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - verificaPodeReplicarByRevisao", ex);
                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }
            finally
            {
                FabricaConexao.fecharConexao();
            }
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public LinkedList<Estudo> findLastPcmsoByCliente(Cliente cliente)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findLastPcmsoByCliente");

            NpgsqlConnection dbConnection = FabricaConexao.getConexao();
            IEstudoDao pcmsoDao = FabricaDao.getEstudoDao();
            IRevisaoDao revisaoDao = FabricaDao.getRevisaoDao();
            IClienteDao clienteDao = FabricaDao.getClienteDao();

            LinkedList<Estudo> pcmsos = new LinkedList<Estudo>();
            LinkedList<Estudo> pcmsoVerificado = new LinkedList<Estudo>();
            
            try
            {
                pcmsos = pcmsoDao.findLastPcmsoByCliente(cliente, dbConnection);
                pcmsos.ToList().ForEach(delegate(Estudo estudo)
                {
                    estudo.VendedorCliente.Cliente = clienteDao.findById((long)estudo.VendedorCliente.Cliente.Id, dbConnection);
                });
                

                //retirando da coleção os pcmso que não são ultima versão.

                foreach (Estudo pcmso in pcmsos)
                {
                    if (revisaoDao.verificaPodeReplicar(pcmso.Id, dbConnection))
                    {
                        pcmsoVerificado.AddLast(pcmso);
                    }
                }

            }
            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findLastPcmsoByCliente", ex);
                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }
            finally
            {
                FabricaConexao.fecharConexao();
            }

            return pcmsoVerificado;
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public DataSet listaPeriodicidadesByClienteFuncaoExame(ClienteFuncaoExame clienteFuncaoExame)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método listaPeriodicidadesByClienteFuncaoExame");

            NpgsqlConnection dbConnection = FabricaConexao.getConexao();

            IClienteFuncaoExamePeriodicidadeDao clienteFuncaoexamePeriodicidadeDao = FabricaDao.getClienteFuncaoExamePeriodicidadeDao();

            try
            {
                return clienteFuncaoexamePeriodicidadeDao.listaPeriodicidadesByClienteFuncaoExame(clienteFuncaoExame, dbConnection);

            }
            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - listaPeriodicidadesByClienteFuncaoExame", ex);

                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }
            finally
            {
                FabricaConexao.fecharConexao();
            }
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public ClienteFuncaoExame findClienteFuncaoExameByClienteFuncaoAndExame(ClienteFuncao clienteFuncao, Exame exame)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findClienteFuncaoExameByClienteFuncaoAndExame");

            NpgsqlConnection dbConnection = FabricaConexao.getConexao();
            IClienteFuncaoExameDao clienteFuncaoExameDao = FabricaDao.getClienteFuncaoExameDao();

            try
            {
                return clienteFuncaoExameDao.findByClienteFuncaoAndExame(clienteFuncao, exame, dbConnection);
            }
            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findClienteFuncaoExameByClienteFuncaoAndExame", ex);
                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }
            finally
            {
                FabricaConexao.fecharConexao();
            }
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public Boolean verificaClienteFuncaoExameFoiUsado(ClienteFuncaoExame clienteFuncaoExame)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método verificaClienteFuncaoExameFoiUsado");

            NpgsqlConnection dbConnection = FabricaConexao.getConexao();
            IClienteFuncaoExameDao clienteFuncaoExameDao = FabricaDao.getClienteFuncaoExameDao();

            try
            {
                return clienteFuncaoExameDao.isUsedByClienteFuncaoExame(clienteFuncaoExame, dbConnection);
            }
            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - verificaClienteFuncaoExameFoiUsado", ex);
                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }
            finally
            {
                FabricaConexao.fecharConexao();
            }
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public void deleteClienteFuncaoExame(ClienteFuncaoExame clienteFuncaoExame)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método deleteClienteFuncaoExame");

            NpgsqlConnection dbConnection = FabricaConexao.getConexao();
            IClienteFuncaoExameDao clienteFuncaoExameDao = FabricaDao.getClienteFuncaoExameDao();

            NpgsqlTransaction transaction = dbConnection.BeginTransaction();

            try
            {
                clienteFuncaoExameDao.delete(clienteFuncaoExame, dbConnection);

                transaction.Commit();
            }
            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - deleteClienteFuncaoExame", ex);
                transaction.Rollback();
                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }
            finally
            {
                FabricaConexao.fecharConexao();
            }
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public void deleteGheSetorClienteFuncaoExame(Ghe ghe, Exame exame, Funcao funcao)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método deleteGheSetorClienteFuncaoExame");

            NpgsqlConnection dbConnection = FabricaConexao.getConexao();
            IGheSetorClienteFuncaoExameDao gheSetorClienteFuncaoExameDao = FabricaDao.getGheSetorClienteFuncaoExameDao();

            NpgsqlTransaction transaction = dbConnection.BeginTransaction();

            try
            {
                gheSetorClienteFuncaoExameDao.deleteByGheAndExameAndFuncao(ghe, exame, funcao, dbConnection);

                transaction.Commit();
            }
            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - deleteGheSetorClienteFuncaoExame", ex);
                transaction.Rollback();
                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }
            finally
            {
                FabricaConexao.fecharConexao();
            }
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public void disableClienteFuncaoExame(ClienteFuncaoExame clienteFuncaoExame)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método desativaClienteFuncaoExame");

            NpgsqlConnection dbConnection = FabricaConexao.getConexao();
            IClienteFuncaoExameDao clienteFuncaoExameDao = FabricaDao.getClienteFuncaoExameDao();

            NpgsqlTransaction transaction = dbConnection.BeginTransaction();

            try
            {
                clienteFuncaoExameDao.disable(clienteFuncaoExame, dbConnection);

                transaction.Commit();
            }
            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - desativaClienteFuncaoExame", ex);
                transaction.Rollback();
                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }
            finally
            {
                FabricaConexao.fecharConexao();
            }
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public void updateClienteFuncaoExamePeriodicidade(ClienteFuncaoExamePeriodicidade clienteFuncaoExamePeriodicidade)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método desativaClienteFuncaoExamePeriodicidade");

            NpgsqlConnection dbConnection = FabricaConexao.getConexao();
            IClienteFuncaoExamePeriodicidadeDao clienteFuncaoExamePeriodicidadeDao = FabricaDao.getClienteFuncaoExamePeriodicidadeDao();

            NpgsqlTransaction transaction = dbConnection.BeginTransaction();

            try
            {
                clienteFuncaoExamePeriodicidadeDao.update(clienteFuncaoExamePeriodicidade, dbConnection);

                transaction.Commit();
            }
            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - desativaClienteFuncaoExamePeriodicidade", ex);
                transaction.Rollback();
                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }
            finally
            {
                FabricaConexao.fecharConexao();
            }
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public DataSet findAllPeriodicidadesNotInClienteFuncaoExame(ClienteFuncaoExame clienteFuncaoExame)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findAllPeriodicidadesNotInClienteFuncaoExame");

            NpgsqlConnection dbConnection = FabricaConexao.getConexao();

            IPeriodicidadeDao periodicidadeDao = FabricaDao.getPeriodicidadeDao();

            try
            {
                return periodicidadeDao.findAllNotInClienteFuncaoExame(clienteFuncaoExame, dbConnection);

            }
            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findAllPeriodicidadesNotInClienteFuncaoExame", ex);
                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }
            finally
            {
                FabricaConexao.fecharConexao();
            }
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public DataSet findAllPeriodicidadesNotInGheFonteAgenteExame(Periodicidade periodicidade, GheFonteAgenteExame gheFonteAgenteExame)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findAllPeriodicidadesNotInGheFonteAgenteExame");

            NpgsqlConnection dbConnection = FabricaConexao.getConexao();

            IPeriodicidadeDao periodicidadeDao = FabricaDao.getPeriodicidadeDao();

            try
            {
                return periodicidadeDao.findAllNotInGheFonteAgenteExame(periodicidade, gheFonteAgenteExame, dbConnection);

            }
            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findAllPeriodicidadesNotInGheFonteAgenteExame", ex);
                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }
            finally
            {
                FabricaConexao.fecharConexao();
            }
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public ClienteFuncaoExamePeriodicidade insertClienteFuncaoExamePeriodicidade(ClienteFuncaoExamePeriodicidade clienteFuncaoExamePeriodicidade)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método incluirClienteFuncaoExamePeriodicidade");

            NpgsqlConnection dbConnection = FabricaConexao.getConexao();
            IClienteFuncaoExamePeriodicidadeDao clienteFuncaoExamePeriodicidadeDao = FabricaDao.getClienteFuncaoExamePeriodicidadeDao();

            NpgsqlTransaction transaction = dbConnection.BeginTransaction();

            ClienteFuncaoExamePeriodicidade clienteFuncaoExamePeriodicidadeRetorno = null;

            try
            {
                clienteFuncaoExamePeriodicidadeRetorno = clienteFuncaoExamePeriodicidadeDao.insert(clienteFuncaoExamePeriodicidade, dbConnection);

                transaction.Commit();

                return clienteFuncaoExamePeriodicidadeRetorno;
            }
            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - incluirClienteFuncaoExamePeriodicidade", ex);
                transaction.Rollback();
                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }
            finally
            {
                FabricaConexao.fecharConexao();
            }
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public DataSet findAllAgentesNotInGheFonte(Agente agente, GheFonte gheFonte)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findAllAgentesNotInGheFonte");

            NpgsqlConnection dbConnection = FabricaConexao.getConexao();

            IAgenteDao agenteDao = FabricaDao.getAgenteDao();

            try
            {
                return agenteDao.findAllNotInGheFonte(agente, gheFonte, dbConnection);

            }
            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findAllAgentesNotInGheFonte", ex);
                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }
            finally
            {
                FabricaConexao.fecharConexao();
            }
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public void updateClienteFuncaoExame(ClienteFuncaoExame clienteFuncaoExame)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método updateClienteFuncaoExame");

            NpgsqlConnection dbConnection = FabricaConexao.getConexao();

            IClienteFuncaoExameDao clienteFuncaoExameDao = FabricaDao.getClienteFuncaoExameDao();

            NpgsqlTransaction transaction = dbConnection.BeginTransaction();

            try
            {
                clienteFuncaoExameDao.update(clienteFuncaoExame, dbConnection);
                transaction.Commit();
            }
            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - updateClienteFuncaoExame", ex);
                transaction.Rollback();
                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }
            finally
            {
                FabricaConexao.fecharConexao();
            }
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public void updateGheFonteAgenteExame(GheFonteAgenteExame gheFonteAgenteExame)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método updateGheFonteAgenteExame");

            NpgsqlConnection dbConnection = FabricaConexao.getConexao();

            IGheFonteAgenteExameDao gheFonteAgenteExameDao = FabricaDao.getGheFonteAgenteExameDao();

            NpgsqlTransaction transaction = dbConnection.BeginTransaction();

            try
            {
                gheFonteAgenteExameDao.update(gheFonteAgenteExame, dbConnection);
                transaction.Commit();
            }
            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - updateGheFonteAgenteExame", ex);
                transaction.Rollback();
                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }
            finally
            {
                FabricaConexao.fecharConexao();
            }
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public void deleteGheFonteAgenteExame(GheFonteAgenteExame gheFonteAgenteExame)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método deleteGheFonteAgenteExame");

            NpgsqlConnection dbConnection = FabricaConexao.getConexao();
            IGheFonteAgenteExameDao gheFonteAgenteExameDao = FabricaDao.getGheFonteAgenteExameDao();
            IGheFonteAgenteExamePeriodicidadeDao gheFonteAgenteExamePeriodicidadeDao = FabricaDao.getGheFonteAgenteExamePeriodicidadeDao();
            NpgsqlTransaction transaction = dbConnection.BeginTransaction();

            try
            {
                /* verificando se o GheFonteAgenteExame foi utilizado por um atendimento */

                if (!gheFonteAgenteExameDao.isUsedInService(gheFonteAgenteExame, dbConnection))
                    gheFonteAgenteExameDao.delete(gheFonteAgenteExame, dbConnection);
                else
                {
                    /* desativando o gheFonteAgenteExame */
                    gheFonteAgenteExameDao.update(new GheFonteAgenteExame(gheFonteAgenteExame.Id, gheFonteAgenteExame.Exame, gheFonteAgenteExame.GheFonteAgente, gheFonteAgenteExame.IdadeExame, ApplicationConstants.DESATIVADO, gheFonteAgenteExame.Confinado, gheFonteAgenteExame.Altura, gheFonteAgenteExame.Eletricidade), dbConnection);

                    /* localizando as periodicidades */
                    gheFonteAgenteExamePeriodicidadeDao.findAll(gheFonteAgenteExame, dbConnection).ToList().ForEach(delegate(GheFonteAgenteExamePeriodicidade gheFonteAgenteExamePeriodicidade)
                    {
                        /* desativando as periodicidades */
                        gheFonteAgenteExamePeriodicidadeDao.update(new GheFonteAgenteExamePeriodicidade(gheFonteAgenteExamePeriodicidade.Periodicidade, gheFonteAgenteExamePeriodicidade.GheFonteAgenteExame, ApplicationConstants.DESATIVADO, gheFonteAgenteExamePeriodicidade.PeriodoVencimento), dbConnection);

                    });
                }

                transaction.Commit();
            }
            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - deleteGheFonteAgenteExame", ex);
                transaction.Rollback();
                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }
            finally
            {
                FabricaConexao.fecharConexao();
            }
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public ClienteFuncao insertClienteFuncao(ClienteFuncao clienteFuncao)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método incluirClienteFuncao");

            NpgsqlConnection dbConnection = FabricaConexao.getConexao();

            IClienteFuncaoDao clienteFuncaoDao = FabricaDao.getClienteFuncaoDao();

            NpgsqlTransaction transaction = dbConnection.BeginTransaction();

            try
            {
                /* verificando se já existe o cliente funcao desativado */
                List<ClienteFuncao> clienteFuncaoFind = clienteFuncaoDao.findAllByFuncaoAndCliente(clienteFuncao.Funcao, clienteFuncao.Cliente, dbConnection).ToList();

                if (clienteFuncaoFind.Count > 0)
                {

                    if (string.Equals(clienteFuncaoFind.First().Situacao, ApplicationConstants.DESATIVADO))
                    {
                        /* função está desativada e deverá ser reativada */
                        ClienteFuncao clienteFuncaoUpdate = clienteFuncaoFind.First();
                        clienteFuncaoUpdate.Situacao = ApplicationConstants.ATIVO;
                        clienteFuncao.DataExclusao = null;
                        clienteFuncaoDao.update(clienteFuncaoUpdate, dbConnection);
                        return clienteFuncaoUpdate;

                    }
                }

                ClienteFuncao newClienteFuncao = clienteFuncaoDao.insert(clienteFuncao, dbConnection);
                transaction.Commit();

                return newClienteFuncao;

            }
            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - incluirClienteFuncao", ex);
                transaction.Rollback();
                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }
            finally
            {
                FabricaConexao.fecharConexao();
            }
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public Ghe insertGhePcmso(Ghe ghe)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método incluirGhePcmso");

            Ghe gheNovo = null; // iniciando objeto do tipo Ghe como nulo.
            Ghe gheProcurado = null;

            NpgsqlConnection dbConnection = FabricaConexao.getConexao(); // criando conexao com o banco de dados.

            IGheDao gheDao = FabricaDao.getGheDao(); // pegando assinatura do metodo.
            IGheNotaDao gheNormaDao = FabricaDao.getGheNormaDao();

            NpgsqlTransaction transaction = dbConnection.BeginTransaction();
            try
            {
                gheProcurado = gheDao.findByDescricao(ghe, dbConnection);

                if (gheProcurado == null)
                {
                    //Incluindo ghe
                    gheNovo = gheDao.insert(ghe, dbConnection); // passando para camada Dao para transacao com o banco.

                    //Incluindo normas no GHE

                    if (ghe.Norma != null) // testando se existe algum elemento norma incluido no GHE
                    {

                        GheNota ghenorma = new GheNota();

                        foreach (Nota norma in ghe.Norma)
                        {
                            ghenorma.Ghe = ghe;
                            ghenorma.Nota = norma;

                            gheNormaDao.insert(ghenorma, dbConnection);

                        }
                    }
                }
                else
                {
                    throw new DescricaoGheJaCadastradaException(DescricaoGheJaCadastradaException.msg);
                }

                transaction.Commit();
            }
            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - incluirGhePcmso", ex);
                transaction.Rollback();
                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }
            finally
            {
                FabricaConexao.fecharConexao();
            }

            return gheNovo;
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public GheSetor insertGheSetor(GheSetor gheSetor)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método incluirGheSetor");

            NpgsqlConnection dbConnection = FabricaConexao.getConexao();
            IGheSetorDao gheSetordao = FabricaDao.getGheSetorDao();
            NpgsqlTransaction transaction = dbConnection.BeginTransaction();

            try
            {

                /* verificar se ná inclusao do GheSetor se já existe um gheSetor desativado. 
                 * Caso esteja desativado, o mesmo deverá ser reativado para evitar
                 * problemas de relacionamento com o pcmso.
                 * 18/10-2015 welbert santos */

                LinkedList<GheSetor> gheSetorList = gheSetordao.findAllBySetorAndGhe(gheSetor.Setor,
                    gheSetor.Ghe, dbConnection);

                GheSetor gheSetorFind = gheSetorList.FirstOrDefault(x => String.Equals(x.Situacao, ApplicationConstants.DESATIVADO));

                if (gheSetorFind != null)
                {
                    gheSetorFind.Situacao = ApplicationConstants.ATIVO;
                    gheSetordao.update(gheSetorFind, dbConnection);
                }
                else
                    gheSetor = gheSetordao.insert(gheSetor, dbConnection);

                transaction.Commit();

                return gheSetor;

            }
            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - incluirGheSetor", ex);
                transaction.Rollback();
                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }
            finally
            {
                FabricaConexao.fecharConexao();
            }
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public Boolean verificaExistePcmsoAvusoCadastrado(Cliente cliente, Cliente clienteContratado)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método verificaExistePcmsoAvusoCadastrado");

            NpgsqlConnection dbConnection = FabricaConexao.getConexao();

            IEstudoDao estudoDao = FabricaDao.getEstudoDao();

            try
            {
                return estudoDao.verificaExistePcmsoAvusoCadastrado(cliente, clienteContratado, dbConnection);

            }
            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - verificaExistePcmsoAvusoCadastrado", ex);
                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }
            finally
            {
                FabricaConexao.fecharConexao();
            }
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public HashSet<GheSetor> findAllGheSetor(Ghe ghe)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findAllGheSetor");

            NpgsqlConnection dbConnection = FabricaConexao.getConexao();

            IGheSetorDao gheSetorDao = FabricaDao.getGheSetorDao();

            HashSet<GheSetor> gheSetorRetorno = new HashSet<GheSetor>();

            try
            {
                gheSetorRetorno = gheSetorDao.findAtivosByGhe(ghe, dbConnection);

                return gheSetorRetorno;

            }
            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findAllGheSetor", ex);
                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }
            finally
            {
                FabricaConexao.fecharConexao();
            }
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public HashSet<GheSetorClienteFuncao> findAllGheSetorClienteFuncao(GheSetor gheSetor)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findAllGheSetorClienteFuncao");

            NpgsqlConnection dbConnection = FabricaConexao.getConexao();

            IGheSetorClienteFuncaoDao gheSetorClienteFuncaoDao = FabricaDao.getGheSetorClienteFuncaoDao();

            try
            {
                return gheSetorClienteFuncaoDao.findAtivosByGheSetor(gheSetor, dbConnection);

            }
            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findAllGheSetorClienteFuncao", ex);
                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }
            finally
            {
                FabricaConexao.fecharConexao();
            }
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public HashSet<GheSetorClienteFuncao> findGheSetorClienteFuncaoByGheAndFuncao(Ghe ghe, Funcao funcao)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findGheSetorClienteFuncaoByGheAndFuncao");

            NpgsqlConnection dbConnection = FabricaConexao.getConexao();

            IGheSetorClienteFuncaoDao gheSetorClienteFuncaoDao = FabricaDao.getGheSetorClienteFuncaoDao();

            try
            {
                return gheSetorClienteFuncaoDao.findAtivosByGheAndFuncao(ghe, funcao, dbConnection);

            }
            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findGheSetorClienteFuncaoByGheAndFuncao", ex);
                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }
            finally
            {
                FabricaConexao.fecharConexao();
            }
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public GheSetorClienteFuncao insertGheSetorClienteFuncao(GheSetorClienteFuncao gheSetorClienteFuncao)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método incluirGheSetorClienteFuncao");

            NpgsqlConnection dbConnection = FabricaConexao.getConexao();

            IGheSetorClienteFuncaoDao gheSetorClienteFuncaoDao = FabricaDao.getGheSetorClienteFuncaoDao();

            NpgsqlTransaction transaction = dbConnection.BeginTransaction();

            try
            {
                gheSetorClienteFuncaoDao.insert(gheSetorClienteFuncao, dbConnection);
                transaction.Commit();
                return gheSetorClienteFuncao;

            }
            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - incluirGheSetorClienteFuncao", ex);
                transaction.Rollback();
                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }
            finally
            {
                FabricaConexao.fecharConexao();
            }
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public void deleteGhe(Ghe ghe)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método deleteGhe");
            NpgsqlConnection dbConnection = FabricaConexao.getConexao();
            IGheDao gheDao = FabricaDao.getGheDao();
            IGheSetorDao gheSetorDao = FabricaDao.getGheSetorDao();
            IGheSetorClienteFuncaoDao gheSetorClienteFuncaoDao = FabricaDao.getGheSetorClienteFuncaoDao();
            IGheFonteDao gheFonteDao = FabricaDao.getGheFonteDao();
            IGheFonteAgenteDao gheFonteAgenteDao = FabricaDao.getGheFonteAgenteDao();
            IGheFonteAgenteExameDao gheFonteAgenteExameDao = FabricaDao.getGheFonteAgenteExameDao();
            IGheFonteAgenteExamePeriodicidadeDao gheFonteAgenteExamePeriodicidadeDao = FabricaDao.getGheFonteAgenteExamePeriodicidadeDao();
            NpgsqlTransaction transaction = dbConnection.BeginTransaction();

            try
            {
                if (!gheDao.isGheUsed(ghe, dbConnection))
                    gheDao.delete((Int64)ghe.Id, dbConnection);
                else
                {
                    gheDao.update(new Ghe((long)ghe.Id, ghe.Estudo, ghe.Descricao, ghe.Nexp, ApplicationConstants.DESATIVADO, ghe.NumeroPo), dbConnection);

                    /* desativando todo os relacionamentos do ghe */

                    /* buscando todos os setores do ghe */
                    gheSetorDao.findAtivosByGhe(ghe, dbConnection).ToList().ForEach(delegate(GheSetor gheSetor)
                    {
                        /* desativando todos os gheSetores */
                        gheSetorDao.update(new GheSetor((long)gheSetor.Id, gheSetor.Setor, gheSetor.Ghe, ApplicationConstants.DESATIVADO), dbConnection);

                        /* buscando todos as funções no ghe */
                        gheSetorClienteFuncaoDao.findAtivosByGheSetor(gheSetor, dbConnection).ToList().ForEach(delegate(GheSetorClienteFuncao gheSetorClienteFuncao)
                        {
                            /* desativando todos os GheSetorClienteFuncao */
                            gheSetorClienteFuncaoDao.update(new GheSetorClienteFuncao((long)gheSetorClienteFuncao.Id, gheSetorClienteFuncao.ClienteFuncao, gheSetorClienteFuncao.GheSetor, gheSetorClienteFuncao.Confinado, gheSetorClienteFuncao.Altura, ApplicationConstants.DESATIVADO, gheSetorClienteFuncao.ComentarioFuncao, gheSetorClienteFuncao.Eletricidade), dbConnection);

                        });

                        /* buscando todas as fontes do ghe */
                        gheFonteDao.findAllByGheList(ghe, dbConnection).ForEach(delegate(GheFonte gheFonte)
                        {
                            /* desativando todos os gheFontes */
                            gheFonteDao.update(new GheFonte((long)gheFonte.Id, gheFonte.Fonte, gheFonte.Ghe, gheFonte.Principal, ApplicationConstants.DESATIVADO), dbConnection);

                            /* buscando todos os agentes */
                            gheFonteAgenteDao.findAllByGheFonteList(gheFonte, dbConnection).ForEach(delegate(GheFonteAgente gheFonteAgente)
                            {
                                /* desativando todos os gheFontAgente */
                                gheFonteAgenteDao.update(new GheFonteAgente((long)gheFonteAgente.Id, gheFonteAgente.GradSoma, gheFonteAgente.GradEfeito, gheFonteAgente.GradExposicao, gheFonteAgente.GheFonte, gheFonteAgente.Agente, gheFonteAgente.TempoExposicao, ApplicationConstants.DESATIVADO), dbConnection);

                                /* buscando todos os exames */
                                gheFonteAgenteExameDao.findAllByGheFonteAgente(gheFonteAgente, dbConnection).ToList().ForEach(delegate(GheFonteAgenteExame gheFonteAgenteExame)
                                {
                                    /* desativando todos os ghefonteAgenteExame */

                                    gheFonteAgenteExameDao.update(new GheFonteAgenteExame((long)gheFonteAgenteExame.Id, gheFonteAgenteExame.Exame, gheFonteAgenteExame.GheFonteAgente, gheFonteAgenteExame.IdadeExame, ApplicationConstants.DESATIVADO, gheFonteAgenteExame.Confinado, gheFonteAgenteExame.Altura, gheFonteAgenteExame.Eletricidade), dbConnection);

                                    /* buscando todas as periodicidades */

                                    gheFonteAgenteExamePeriodicidadeDao.findAll(gheFonteAgenteExame, dbConnection).ToList().ForEach(delegate(GheFonteAgenteExamePeriodicidade gheFonteAgenteExamePeriodicidade)
                                    {
                                        gheFonteAgenteExamePeriodicidadeDao.update(new GheFonteAgenteExamePeriodicidade(gheFonteAgenteExamePeriodicidade.Periodicidade, gheFonteAgenteExamePeriodicidade.GheFonteAgenteExame, ApplicationConstants.DESATIVADO, gheFonteAgenteExamePeriodicidade.PeriodoVencimento), dbConnection);
                                    });
                                });
                            });
                        });
                    });

                }

                transaction.Commit();

            }
            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - deleteGhe", ex);
                transaction.Rollback();
                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }
            finally
            {
                FabricaConexao.fecharConexao();
            }
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public void deleteGheSetor(GheSetor gheSetor)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método deleteGheSetor");
            NpgsqlConnection dbConnection = FabricaConexao.getConexao();
            IGheSetorDao gheSetorDao = FabricaDao.getGheSetorDao();
            IGheSetorClienteFuncaoDao gheSetorClienteFuncaoDao = FabricaDao.getGheSetorClienteFuncaoDao();
            NpgsqlTransaction transaction = dbConnection.BeginTransaction();

            try
            {

                /* verificando se o setor já foi utilizado por algum atendimento */

                if (!gheSetorDao.IsUsedInService(gheSetor, dbConnection))
                {
                    gheSetorDao.delete(gheSetor, dbConnection);
                }
                else
                {
                    /* desativando o gheSetor */
                    gheSetorDao.update(new GheSetor((long)gheSetor.Id, gheSetor.Setor, gheSetor.Ghe, ApplicationConstants.DESATIVADO), dbConnection);

                    /* localizando todos os gheSetorClienteFuncao */
                    gheSetorClienteFuncaoDao.findAtivosByGheSetor(gheSetor, dbConnection).ToList().ForEach(delegate(GheSetorClienteFuncao gheSetorClienteFuncao)
                    {
                        /* desativando todos os gheSetoresClienteFuncoes */
                        gheSetorClienteFuncaoDao.update(new GheSetorClienteFuncao((long)gheSetorClienteFuncao.Id, gheSetorClienteFuncao.ClienteFuncao, gheSetorClienteFuncao.GheSetor, gheSetorClienteFuncao.Confinado, gheSetorClienteFuncao.Altura, ApplicationConstants.DESATIVADO, gheSetorClienteFuncao.ComentarioFuncao, gheSetorClienteFuncao.Eletricidade), dbConnection);

                    });

                }

                transaction.Commit();

            }
            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - deleteGheSetor", ex);
                transaction.Rollback();
                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }
            finally
            {
                FabricaConexao.fecharConexao();
            }
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public void deleteGheSetorClienteFuncao(GheSetorClienteFuncao gheSetorClienteFuncao)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método deleteGheSetorClienteFuncao");
            NpgsqlConnection dbConnection = FabricaConexao.getConexao();
            IGheSetorClienteFuncaoDao gheSetorClienteFuncaoDao = FabricaDao.getGheSetorClienteFuncaoDao();
            NpgsqlTransaction transaction = dbConnection.BeginTransaction();

            try
            {
                /* verificando se o gheSetorClienteFuncao já foi utilizado por algum atendimento */

                if (!gheSetorClienteFuncaoDao.isUsedInService(gheSetorClienteFuncao, dbConnection))
                    gheSetorClienteFuncaoDao.delete(gheSetorClienteFuncao, dbConnection);
                else
                    gheSetorClienteFuncaoDao.update(new GheSetorClienteFuncao((long)gheSetorClienteFuncao.Id, gheSetorClienteFuncao.ClienteFuncao, gheSetorClienteFuncao.GheSetor, gheSetorClienteFuncao.Confinado, gheSetorClienteFuncao.Altura, ApplicationConstants.DESATIVADO, gheSetorClienteFuncao.ComentarioFuncao, gheSetorClienteFuncao.Eletricidade), dbConnection);

                transaction.Commit();

            }
            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - deleteGheSetorClienteFuncao", ex);
                transaction.Rollback();
                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }
            finally
            {
                FabricaConexao.fecharConexao();
            }
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public void deleteGheFonteAgente(GheFonteAgente gheFonteAgente)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método deleteGheFonteAgente");
            NpgsqlConnection dbConnection = FabricaConexao.getConexao();
            IGheFonteAgenteDao gheFonteAgenteDao = FabricaDao.getGheFonteAgenteDao();
            IGheFonteAgenteExameDao gheFonteAgenteExameDao = FabricaDao.getGheFonteAgenteExameDao();
            IGheFonteAgenteExamePeriodicidadeDao gheFonteAgenteExamePeriodicidadeDao = FabricaDao.getGheFonteAgenteExamePeriodicidadeDao();
            NpgsqlTransaction transaction = dbConnection.BeginTransaction();

            try
            {
                /* verificando se o gheFonteAgente já foi utilizado por algum atendimento */

                if (!gheFonteAgenteDao.isUsedInService(gheFonteAgente, dbConnection))
                    gheFonteAgenteDao.delete((Int64)gheFonteAgente.Id, dbConnection);
                else
                {
                    /* desativando o gheFonteAgente */
                    gheFonteAgenteDao.update(new GheFonteAgente((long)gheFonteAgente.Id, gheFonteAgente.GradSoma, gheFonteAgente.GradEfeito, gheFonteAgente.GradExposicao, gheFonteAgente.GheFonte, gheFonteAgente.Agente, gheFonteAgente.TempoExposicao, ApplicationConstants.DESATIVADO), dbConnection);

                    /* localizando todos os gheFonteAgenteExame */
                    gheFonteAgenteExameDao.findAllByGheFonteAgente(gheFonteAgente, dbConnection).ToList().ForEach(delegate(GheFonteAgenteExame gheFonteAgenteExame)
                    {
                        /* desativando os gheFonteAgenteExames */
                        gheFonteAgenteExameDao.update(new GheFonteAgenteExame((long)gheFonteAgenteExame.Id, gheFonteAgenteExame.Exame, gheFonteAgenteExame.GheFonteAgente, gheFonteAgenteExame.IdadeExame, ApplicationConstants.DESATIVADO, gheFonteAgenteExame.Confinado, gheFonteAgenteExame.Altura, gheFonteAgenteExame.Eletricidade), dbConnection);

                        /* localizando todas as periodicidade do exame */
                        gheFonteAgenteExamePeriodicidadeDao.findAll(gheFonteAgenteExame, dbConnection).ToList().ForEach(delegate(GheFonteAgenteExamePeriodicidade gheFonteAgenteExamePeriodicidade)
                        {
                            /* desativando as periodicidades */
                            gheFonteAgenteExamePeriodicidadeDao.update(new GheFonteAgenteExamePeriodicidade(gheFonteAgenteExamePeriodicidade.Periodicidade, gheFonteAgenteExamePeriodicidade.GheFonteAgenteExame, ApplicationConstants.DESATIVADO, gheFonteAgenteExamePeriodicidade.PeriodoVencimento), dbConnection);
                        });

                    });
                }

                transaction.Commit();

            }
            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - deleteGheFonteAgente", ex);
                transaction.Rollback();
                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }
            finally
            {
                FabricaConexao.fecharConexao();
            }
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public void updateGheFonteAgenteExamePeriodicidade(GheFonteAgenteExamePeriodicidade gheFonteAgenteExamePeriodicidade)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método excluirGheFonteAgenteExamePeriodicidade");
            NpgsqlConnection dbConnection = FabricaConexao.getConexao();
            IGheFonteAgenteExamePeriodicidadeDao gheFonteAgenteExamePeriodicidadeDao = FabricaDao.getGheFonteAgenteExamePeriodicidadeDao();
            NpgsqlTransaction transaction = dbConnection.BeginTransaction();

            try
            {

                gheFonteAgenteExamePeriodicidadeDao.update(gheFonteAgenteExamePeriodicidade, dbConnection);

                /*
                if (isGheFonteAgenteExameUsed(gheFonteAgenteExamePeriodicidade.getGheFonteAgenteExame(), dbConnection))
                {
                    // Desativando GheSetor
                    gheFonteAgenteExamePeriodicidadeDao.desativaGheFonteAgenteExamePeriodicidade(gheFonteAgenteExamePeriodicidade, dbConnection);
                }
                else
                {
                    gheFonteAgenteExamePeriodicidadeDao.excluirGheFonteAgenteExamePeriodicidade(gheFonteAgenteExamePeriodicidade, dbConnection);
                }
                */

                transaction.Commit();

            }
            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - excluirGheFonteAgenteExamePeriodicidade", ex);
                transaction.Rollback();
                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }
            finally
            {
                FabricaConexao.fecharConexao();
            }
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public void updateGheSetorClienteFuncao(GheSetorClienteFuncao gheSetorClienteFuncao)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método updateGheSetorClienteFuncao");
            NpgsqlConnection dbConnection = FabricaConexao.getConexao();
            IGheSetorClienteFuncaoDao gheSetorClienteFuncaoDao = FabricaDao.getGheSetorClienteFuncaoDao();
            NpgsqlTransaction transaction = dbConnection.BeginTransaction();

            try
            {
                gheSetorClienteFuncaoDao.update(gheSetorClienteFuncao, dbConnection);
                transaction.Commit();
            }
            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - updateGheSetorClienteFuncao", ex);
                transaction.Rollback();
                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }
            finally
            {
                FabricaConexao.fecharConexao();
            }
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public GheFonteAgenteExame findGheFonteAgenteExameById(Int64 id)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findGheFonteAgenteExameById");

            NpgsqlConnection dbConnection = FabricaConexao.getConexao();
            IGheFonteAgenteExameDao gheFonteAgenteExameDao = FabricaDao.getGheFonteAgenteExameDao();
            IExameDao exameDao = FabricaDao.getExameDao();
            IGheFonteAgenteDao gheFonteAgenteDao = FabricaDao.getGheFonteAgenteDao();

            GheFonteAgenteExame gheFonteAgenteExame = null;

            try
            {
                gheFonteAgenteExame = gheFonteAgenteExameDao.findById(id, dbConnection);

                gheFonteAgenteExame.Exame = exameDao.findById((Int64)gheFonteAgenteExame.Exame.Id, dbConnection);
                gheFonteAgenteExame.GheFonteAgente = gheFonteAgenteDao.findById((Int64)gheFonteAgenteExame.GheFonteAgente.Id, dbConnection);

                return gheFonteAgenteExame;

            }
            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findGheFonteAgenteExameById", ex);
                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }
            finally
            {
                FabricaConexao.fecharConexao();
            }
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public HashSet<GheFonte> findAllGheFonteByGhe(Ghe ghe)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findAllGheFonteByGhe");

            NpgsqlConnection dbConnection = FabricaConexao.getConexao();
            IGheFonteDao gheFonteDao = FabricaDao.getGheFonteDao();
            try
            {
                return gheFonteDao.findAllAtivoByGhe(ghe, dbConnection);
            }
            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findAllGheFonteByGhe", ex);
                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }
            finally
            {
                FabricaConexao.fecharConexao();
            }
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public GheFonte insertGheFonte(GheFonte gheFonte)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método incluiGheFonte");
            NpgsqlConnection dbConnection = FabricaConexao.getConexao();
            IGheFonteDao gheFonteDao = FabricaDao.getGheFonteDao();

            try
            {
                return gheFonteDao.insert(gheFonte, dbConnection);
            }
            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - incluiGheFonte", ex);
                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }
            finally
            {
                FabricaConexao.fecharConexao();
            }
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public GheSetorClienteFuncaoExame insertGheSetorClienteFuncaoExame(GheSetorClienteFuncaoExame gheSetorClienteFuncaoExame)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método incluiGheSetorClienteFuncaoExame");
            NpgsqlConnection dbConnection = FabricaConexao.getConexao();
            IGheSetorClienteFuncaoExameDao gheSetorClienteFuncaoExameDao = FabricaDao.getGheSetorClienteFuncaoExameDao();

            try
            {
                return gheSetorClienteFuncaoExameDao.insert(gheSetorClienteFuncaoExame, dbConnection);
            }
            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - incluiGheSetorClienteFuncaoExame", ex);
                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }
            finally
            {
                FabricaConexao.fecharConexao();
            }
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public ClienteFuncaoExame insertClienteFuncaoExame(ClienteFuncaoExame clienteFuncaoExame, HashSet<Periodicidade> periodicidades, Dictionary<long, int?> periodoVencimento)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método inserirClienteFuncaoExamePeriodicidade");

            NpgsqlConnection dbConnection = FabricaConexao.getConexao();
            IClienteFuncaoExameDao clienteFuncaoExameDao = FabricaDao.getClienteFuncaoExameDao();
            IClienteFuncaoExamePeriodicidadeDao clienteFuncaoExamePeriodicidadeDao = FabricaDao.getClienteFuncaoExamePeriodicidadeDao();

            NpgsqlTransaction transaction = dbConnection.BeginTransaction();

            try
            {
                clienteFuncaoExame = clienteFuncaoExameDao.insert(clienteFuncaoExame, dbConnection);
                int? periodoVcto;

                foreach (Periodicidade periodicidade in periodicidades)
                {
                    periodoVencimento.TryGetValue((long)periodicidade.Id, out periodoVcto);
                    clienteFuncaoExamePeriodicidadeDao.insert(new ClienteFuncaoExamePeriodicidade(clienteFuncaoExame, periodicidade, String.Empty, periodoVcto), dbConnection);
                }
                transaction.Commit();
            }
            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - inserirClienteFuncaoExameAgentePeriodicidade", ex);
                transaction.Rollback();
                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }
            finally
            {
                FabricaConexao.fecharConexao();
            }

            return clienteFuncaoExame;
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public Boolean findExameInUsed(Exame exame)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findExameInUsed");

            NpgsqlConnection dbConnection = FabricaConexao.getConexao();
            IClienteFuncaoExameDao clienteFuncaoExameDao = FabricaDao.getClienteFuncaoExameDao();
            IGheFonteAgenteExameDao gheFonteAgenteExameDao = FabricaDao.getGheFonteAgenteExameDao();

            Boolean inUsed = false;

            try
            {
                if (clienteFuncaoExameDao.IsUsedByExame(exame, dbConnection))
                {
                    inUsed = true;
                }

                if (gheFonteAgenteExameDao.isGheFonteAgenteExameUsedByExame(exame, dbConnection))
                {
                    inUsed = true;
                }

                return inUsed;


            }
            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - inserirClienteFuncaoExameAgentePeriodicidade", ex);
                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }
            finally
            {
                FabricaConexao.fecharConexao();
            }
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public Boolean findMedicoEstudoByMedico(Medico medico)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findMedicoEstudoByMedico");

            NpgsqlConnection dbConnection = FabricaConexao.getConexao();

            IMedicoEstudoDao medicoEstudoDao = FabricaDao.getMedicoEstudoDao();

            try
            {
                return medicoEstudoDao.medicoIsUsed(medico, dbConnection);

            }
            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findMedicoEstudoByMedico", ex);
                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }
            finally
            {
                FabricaConexao.fecharConexao();
            }
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public Boolean IsClienteFuncaoPresentInPcmso(Estudo pcmso, ClienteFuncao clienteFuncao)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método IsClienteFuncaoPresentInPcmso");

            NpgsqlConnection dbConnection = FabricaConexao.getConexao();

            IClienteFuncaoDao clienteFuncaoDao = FabricaDao.getClienteFuncaoDao();

            try
            {
                return clienteFuncaoDao.IsPresentInPcmso(pcmso, clienteFuncao, dbConnection);

            }
            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - IsClienteFuncaoPresentInPcmso", ex);
                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }
            finally
            {
                FabricaConexao.fecharConexao();
            }
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public List<Ghe> findAllGheInPcmsoByClienteFuncao(ClienteFuncao clienteFuncao, Estudo pcmso)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findClienteFuncaoByPcmso");

            NpgsqlConnection dbConnection = FabricaConexao.getConexao();

            IGheDao gheDao = FabricaDao.getGheDao();

            try
            {
                return gheDao.findAtivoInEstudoByClienteFuncao(clienteFuncao, pcmso, dbConnection);

            }
            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findAllGheInPcmsoByClienteFuncao", ex);
                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }
            finally
            {
                FabricaConexao.fecharConexao();
            }
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public Dictionary<Int64, Dictionary<Funcao, Int32>> findGheSetorClienteFuncaoExameByGheAndExame(Ghe ghe, Exame exame)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findAllGheSetorClienteFuncaoExameByGheAndExame");

            NpgsqlConnection dbConnection = FabricaConexao.getConexao();

            IGheSetorClienteFuncaoExameDao gheSetorClienteFuncaoExameDao = FabricaDao.getGheSetorClienteFuncaoExameDao();

            try
            {
                return gheSetorClienteFuncaoExameDao.findAllByGheAndExame(ghe, exame, dbConnection);

            }
            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findAllGheSetorClienteFuncaoExameByGheAndExame", ex);
                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }
            finally
            {
                FabricaConexao.fecharConexao();
            }
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public Estudo insertPcmso(Estudo pcmso, Cnae cnaeCliente )
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método incluirEstudo");

            NpgsqlConnection dbConnection = FabricaConexao.getConexao(); 

            HashSet<ClienteCnae> listaCnaeCliente = new HashSet<ClienteCnae>();
            HashSet<CnaeEstudo> ListaCnaePpra = new HashSet<CnaeEstudo>();

            IEstudoDao estudoDao = FabricaDao.getEstudoDao(); 
            ICnaeEstudoDao cnaeEstudoDao = FabricaDao.getCnaeEstudoDao();
            IClienteCnaeDao cnaeClienteDao = FabricaDao.getClienteCnaeDao();
            IRevisaoDao revisaoDao = FabricaDao.getRevisaoDao();
            Cnae cnaePrincipalContratante = new Cnae();
            CnaeEstudo cnaePpraContratante = new CnaeEstudo();


            NpgsqlTransaction transaction = dbConnection.BeginTransaction();
            try
            {
                //Incluindo Estudo
                pcmso = estudoDao.insert(pcmso, dbConnection); 

                // Incluindo cnae do cliente no estudo se pcmso normal
                if (cnaeCliente != null)
                {
                    CnaeEstudo cnaeEstudo = new CnaeEstudo();
                    cnaeEstudo.Cnae = cnaeCliente;
                    cnaeEstudo.Estudo = pcmso;
                    cnaeEstudo.FlagCliente = true;

                    cnaeEstudoDao.insert(cnaeEstudo, dbConnection);
                }

                revisaoDao.incluir(new Revisao(null, pcmso, "Emissão Original", pcmso, pcmso, "PCMSO", pcmso.CodigoEstudo.Substring(pcmso.CodigoEstudo.Length - 2, 2), null, false), dbConnection);

                transaction.Commit();
            }
            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - novoPcmso", ex);
                transaction.Rollback();
                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }
            finally
            {
                FabricaConexao.fecharConexao();
            }

            return pcmso;
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public DataSet findAllFuncaoNotInGheSetorClienteFuncaoExame(Exame exame, Ghe ghe, Funcao funcao)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findAllFuncaoNotInGheSetorClienteFuncaoExame");

            NpgsqlConnection dbConnection = FabricaConexao.getConexao();
            IGheSetorClienteFuncaoExameDao GheSetorClienteFuncaoExameDao = FabricaDao.getGheSetorClienteFuncaoExameDao();

            try
            {
                return GheSetorClienteFuncaoExameDao.findAllFuncaoNotInGheSetorClienteFuncaoExame(exame, ghe, funcao, dbConnection);
            }
            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findAllFuncaoNotInGheSetorClienteFuncaoExame", ex);
                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }
            finally
            {
                FabricaConexao.fecharConexao();
            }
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public void deletePrestadorGheFonteAgenteExameAso(GheFonteAgenteExameAso gheFonteAgenteExameAso)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método excluiPrestadorGheFonteAgenteExameAso");

            NpgsqlConnection dbConnection = FabricaConexao.getConexao(); // criando conexao com o banco de dados.
            NpgsqlTransaction transaction = dbConnection.BeginTransaction();

            IGheFonteAgenteExameAsoDao gheFonteAgenteExameAsoDao = FabricaDao.getGheFonteAgenteExameAsoDao();

            try
            {
                gheFonteAgenteExameAsoDao.excluirPrestadorGheFonteAgenteExameAso(gheFonteAgenteExameAso, dbConnection);

                transaction.Commit();
            }
            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - excluiPrestadorGheFonteAgenteExameAso", ex);
                transaction.Rollback();
                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }
            finally
            {
                FabricaConexao.fecharConexao();
            }

        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public void deletePrestadorClienteFuncaoExameAso(ClienteFuncaoExameASo clienteFuncaoExameAso)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método excluirPrestadorClienteFuncaoExameAso");

            NpgsqlConnection dbConnection = FabricaConexao.getConexao(); // criando conexao com o banco de dados.
            NpgsqlTransaction transaction = dbConnection.BeginTransaction();

            IClienteFuncaoExameASoDao clienteFuncaoExameAsoDao = FabricaDao.getClienteFuncaoExameASoDao();

            try
            {
                clienteFuncaoExameAsoDao.excluirPrestadorClienteFuncaoExameAso(clienteFuncaoExameAso, dbConnection);

                transaction.Commit();
            }
            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - excluirPrestadorClienteFuncaoExameAso", ex);
                transaction.Rollback();
                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }
            finally
            {
                FabricaConexao.fecharConexao();
            }

        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public void insertPrestadorClienteFuncaoExameAso(ClienteFuncaoExameASo clienteFuncaoExameAso)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método incluiPrestadorClienteFuncaoExameAso");

            NpgsqlConnection dbConnection = FabricaConexao.getConexao(); // criando conexao com o banco de dados.
            NpgsqlTransaction transaction = dbConnection.BeginTransaction();

            IClienteFuncaoExameASoDao clienteFuncaoExameAsoDao = FabricaDao.getClienteFuncaoExameASoDao();

            try
            {
                clienteFuncaoExameAsoDao.incluirPrestadorClienteFuncaoExameAso(clienteFuncaoExameAso, dbConnection);

                transaction.Commit();
            }
            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - incluiPrestadorClienteFuncaoExameAso", ex);
                transaction.Rollback();
                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }
            finally
            {
                FabricaConexao.fecharConexao();
            }

        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public void insertPrestadorGheFonteAgenteExameAso
            (GheFonteAgenteExameAso gheFonteAgenteExameAso)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método incluirPrestadorGheFonteAgenteExameAso");

            NpgsqlConnection dbConnection = FabricaConexao.getConexao(); // criando conexao com o banco de dados.
            NpgsqlTransaction transaction = dbConnection.BeginTransaction();

            IGheFonteAgenteExameAsoDao gheFonteAgenteExameAsoDao = FabricaDao.getGheFonteAgenteExameAsoDao();

            try
            {
                gheFonteAgenteExameAsoDao.incluirPrestadorGheFonteAgenteExameAso(gheFonteAgenteExameAso, dbConnection);

                transaction.Commit();
            }
            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - incluirPrestadorGheFonteAgenteExameAso", ex);
                transaction.Rollback();
                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }
            finally
            {
                FabricaConexao.fecharConexao();
            }

        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public Boolean verificaExisteExameNaoLaudadoNoAtendimentoPorCliente(Cliente cliente, DateTime dataInicial, DateTime dataFinal, Cliente unidade,
            Boolean matriz, CentroCusto centroCusto)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método verificaExisteExameNaoLaudadoNoAtendimentoPorCliente");

            NpgsqlConnection dbConnection = FabricaConexao.getConexao();

            IGheFonteAgenteExameAsoDao gheFonteAgenteExameAsoDao = FabricaDao.getGheFonteAgenteExameAsoDao();

            try
            {
                return gheFonteAgenteExameAsoDao.verificaExisteExameNaoLaudadoNoAtendimentoPorCliente(cliente, dataInicial, dataFinal, unidade, matriz, centroCusto, dbConnection);
            }
            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - verificaExisteExameNaoLaudadoNoAtendimentoPorCliente", ex);
                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }
            finally
            {
                FabricaConexao.fecharConexao();
            }
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public LinkedList<Setor> verificaSetoresInRelatorio(Cliente cliente, DateTime dataInicial, DateTime dataFinal,
            Cliente unidade, Boolean matriz, CentroCusto centroCusto)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método verificaSetoresInRelatorio");

            NpgsqlConnection dbConnection = FabricaConexao.getConexao();

            IRelatorioAnualDao relatorioAnualDao = FabricaDao.getRelatorioAnualDao();

            try
            {
                return relatorioAnualDao.verificaSetoresInRelatorio(cliente, dataInicial, dataFinal, unidade, matriz, centroCusto, dbConnection);
            }
            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - verificaSetoresInRelatorio", ex);
                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }
            finally
            {
                FabricaConexao.fecharConexao();
            }
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public RelatorioAnual montaItemRelatorio(LinkedList<Setor> setorInRelatorio,
            RelatorioAnual relatorioAnual, Cliente unidade, Boolean matriz, CentroCusto centroCusto)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método montaItemRelatorio");

            NpgsqlConnection dbConnection = FabricaConexao.getConexao();

            IRelatorioAnualDao relatorioAnualDao = FabricaDao.getRelatorioAnualDao();

            LinkedList<Exame> exameInSetor = new LinkedList<Exame>();
            LinkedList<RelatorioAnualItem> itemRelatorio = new LinkedList<RelatorioAnualItem>();
            LinkedList<Periodicidade> tipoAtendimento = new LinkedList<Periodicidade>();
            Dictionary<String, Int32> previsaoAtendimento = new Dictionary<string, int>();

            Int32 laudoNormal = 0;
            Int32 laudoAnormal = 0;
            Int32 previsao = 0;
            Int32 periodico = 0;
            Int32 admissional = 0;
            Int32 demissional = 0;

            try
            {

                /* verificandos os exames por setor da empresa */

                foreach (Setor setor in setorInRelatorio)
                {
                    exameInSetor = relatorioAnualDao.findExameInSetor(setor, relatorioAnual, unidade, matriz, centroCusto, dbConnection);

                    /* montando para cada exame o total de item normal e anormal laudado, de
                     * acordo com o setor */

                    foreach (Exame exame in exameInSetor)
                    {

                        /* apurando o tipo de atendimento por exame */

                        tipoAtendimento = relatorioAnualDao.findTipoInExame(setor, exame, relatorioAnual,
                            unidade, matriz, centroCusto, dbConnection);

                        foreach (Periodicidade tipo in tipoAtendimento)
                        {

                            laudoNormal = relatorioAnualDao.findTotalInSituacao(setor, exame, relatorioAnual,
                            ApplicationConstants.NORMAL, tipo, unidade, matriz, centroCusto, dbConnection);

                            laudoAnormal = relatorioAnualDao.findTotalInSituacao(setor, exame, relatorioAnual,
                                ApplicationConstants.ALTERADO, tipo, unidade, matriz, centroCusto, dbConnection);


                            /* Apurando a previsão do exame */

                            previsaoAtendimento = relatorioAnualDao.findTipoAtendimentoInSetorInExame(setor, exame, relatorioAnual, unidade, matriz, centroCusto, dbConnection);

                            previsaoAtendimento.TryGetValue("PERIODICO", out periodico);
                            previsaoAtendimento.TryGetValue("ADMISSIONAL", out admissional);
                            previsaoAtendimento.TryGetValue("DEMISSIONAL", out demissional);

                            /* calculando a previsao */

                            previsao = periodico + admissional - demissional;

                            previsao = previsao < 0 ? 0 : previsao;

                            itemRelatorio.AddFirst(new RelatorioAnualItem(null, relatorioAnual, setor, exame, Convert.ToInt32(laudoNormal), laudoAnormal, previsao, tipo));
                        }

                    }

                    /* zerando variáveis auxiliares */
                    exameInSetor.Clear();
                    laudoNormal = 0;
                    laudoAnormal = 0;
                    previsao = 0;
                    periodico = 0;
                    admissional = 0;
                    demissional = 0;

                }

                /* adiciono a colecao no relatório */

                relatorioAnual.ItemRelatorioAnual = itemRelatorio;

                return relatorioAnual;


            }
            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - montaItemRelatorio", ex);
                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }
            finally
            {
                FabricaConexao.fecharConexao();
            }
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public RelatorioAnual insertRelatorioAnual(RelatorioAnual relatorioAnual)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método insertRelatorioAnual");

            NpgsqlConnection dbConnection = FabricaConexao.getConexao();
            NpgsqlTransaction transaction = dbConnection.BeginTransaction();

            IRelatorioAnualDao relatorioAnualDao = FabricaDao.getRelatorioAnualDao();

            try
            {
                relatorioAnualDao.insertRelatorioAnual(relatorioAnual, dbConnection);
                transaction.Commit();
                return relatorioAnual;
            }
            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - insertRelatorioAnual", ex);
                transaction.Rollback();
                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }
            finally
            {
                FabricaConexao.fecharConexao();
            }

        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public Boolean findExameInExameAso(Exame exame)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findExameInExameAso");

            NpgsqlConnection dbConnection = FabricaConexao.getConexao();
            IExameDao exameDao = FabricaDao.getExameDao();
            Boolean inUsed = false;

            try
            {
                if (exameDao.findInExameAso(exame, dbConnection))
                {
                    inUsed = true;
                }

                return inUsed;
            }
            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findExameInExameAso", ex);
                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }
            finally
            {
                FabricaConexao.fecharConexao();
            }
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public GheSetorClienteFuncaoExame updateGheSetorClienteFuncaoExame(GheSetorClienteFuncaoExame gheSetorClienteFuncaoExame)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método updateGheSetorClienteFuncaoExame");

            NpgsqlConnection dbConnection = FabricaConexao.getConexao();
            NpgsqlTransaction transaction = dbConnection.BeginTransaction();

            IGheSetorClienteFuncaoExameDao gheSetorClienteFuncaoExameDao = FabricaDao.getGheSetorClienteFuncaoExameDao();

            try
            {
                gheSetorClienteFuncaoExame = gheSetorClienteFuncaoExameDao.update(gheSetorClienteFuncaoExame, dbConnection);
                transaction.Commit();
                return gheSetorClienteFuncaoExame;
            }
            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - updateGheSetorClienteFuncaoExame", ex);
                transaction.Rollback();
                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }
            finally
            {
                FabricaConexao.fecharConexao();
            }

        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public void deleteGheFonteAgenteExamePeriodicidade(GheFonteAgenteExamePeriodicidade gheFonteAgenteExamePeriodicidade)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método deleteGheFonteAgenteExamePeriodicidade");

            NpgsqlConnection dbConnection = FabricaConexao.getConexao();
            NpgsqlTransaction transaction = dbConnection.BeginTransaction();

            IGheFonteAgenteExamePeriodicidadeDao gheFonteAgenteExamePeriodicidadeDao = FabricaDao.getGheFonteAgenteExamePeriodicidadeDao();

            try
            {
                /* verificando se a periodicidade não foi utilizada ainda por um atendimento. */

                if (!gheFonteAgenteExamePeriodicidadeDao.isUsedInService(gheFonteAgenteExamePeriodicidade, dbConnection))
                    gheFonteAgenteExamePeriodicidadeDao.delete(gheFonteAgenteExamePeriodicidade, dbConnection);
                else
                    gheFonteAgenteExamePeriodicidadeDao.update(new GheFonteAgenteExamePeriodicidade(gheFonteAgenteExamePeriodicidade.Periodicidade, gheFonteAgenteExamePeriodicidade.GheFonteAgenteExame, ApplicationConstants.DESATIVADO, gheFonteAgenteExamePeriodicidade.PeriodoVencimento), dbConnection);

                transaction.Commit();

            }
            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - deleteGheFonteAgenteExamePeriodicidade", ex);
                transaction.Rollback();
                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }
            finally
            {
                FabricaConexao.fecharConexao();
            }

        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public MedicoEstudo updateMedicoEstudo(MedicoEstudo medicoEstudo)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método updateMedicoEstudo");

            NpgsqlConnection dbConnection = FabricaConexao.getConexao();

            IMedicoEstudoDao medicoEstudoDao = FabricaDao.getMedicoEstudoDao();

            NpgsqlTransaction transaction = dbConnection.BeginTransaction();

            try
            {
                medicoEstudoDao.update(medicoEstudo, dbConnection);

                transaction.Commit();
            }
            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - updateMedicoEstudo", ex);
                transaction.Rollback();
                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }
            finally
            {
                FabricaConexao.fecharConexao();
            }

            return medicoEstudo;

        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public List<GheSetorClienteFuncao> findGheSetorClienteFuncaoByGheSetorAndClienteFuncao(GheSetor gheSetor, ClienteFuncao clienteFuncao)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findGheSetorClienteFuncaoByGheSetorAndClienteFuncao");

            NpgsqlConnection dbConnection = FabricaConexao.getConexao();

            IGheSetorClienteFuncaoDao gheSetorClienteFuncaoDao = FabricaDao.getGheSetorClienteFuncaoDao();

            try
            {
                return gheSetorClienteFuncaoDao.findAtivosByGheSetorAndClienteFuncao(gheSetor, clienteFuncao, dbConnection);

            }
            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findGheSetorClienteFuncaoByGheSetorAndClienteFuncao", ex);
                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }
            finally
            {
                FabricaConexao.fecharConexao();
            }
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public List<MedicoExame> findAllMedicoExameByExame(Exame exame)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findAllMedicoExameByExame");

            NpgsqlConnection dbConnection = FabricaConexao.getConexao();
            IMedicoExameDao MedicoExameDao = FabricaDao.getMedicoExameDao();
            IMedicoDao medicoDao = FabricaDao.getMedicoDao();

            List<MedicoExame> medicosExame = new List<MedicoExame>();

            try
            {
                medicosExame = MedicoExameDao.findAllByExame(exame, dbConnection);

                /* preenchendo dados do médico */
                medicosExame.ForEach(delegate(MedicoExame medicoExame)
                {
                    medicoExame.Medico = medicoDao.findById((long)medicoExame.Medico.Id, dbConnection);
                });

                return medicosExame;

            }
            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findAllMedicoExameByExame", ex);
                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }
            finally
            {
                FabricaConexao.fecharConexao();
            }
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public void deleteMedicoExame(MedicoExame medicoExame)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método deleteMedicoExame");

            NpgsqlConnection dbConnection = FabricaConexao.getConexao();
            IMedicoExameDao medicoExameDao = FabricaDao.getMedicoExameDao();
            NpgsqlTransaction transaction = dbConnection.BeginTransaction();

            try
            {
                medicoExameDao.delete(medicoExame, dbConnection);
                transaction.Commit();

            }
            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - insertMedicoExame", ex);
                transaction.Rollback();
                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }
            finally
            {
                FabricaConexao.fecharConexao();
            }
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public Estudo replicarPcmsoAvulso(Estudo pcmso)
        {
            NpgsqlConnection dbConnection = FabricaConexao.getConexao();

            IEstudoDao estudoDao = FabricaDao.getEstudoDao();
            ICnaeEstudoDao cnaePpraDao = FabricaDao.getCnaeEstudoDao();
            IGheDao gheDao = FabricaDao.getGheDao();
            IGheSetorDao gheSetorDao = FabricaDao.getGheSetorDao();
            IClienteFuncaoDao clienteFuncaoDao = FabricaDao.getClienteFuncaoDao();

            IGheFonteDao gheFonteDao = FabricaDao.getGheFonteDao();
            IGheSetorClienteFuncaoDao gheSetorClienteFuncaoDao = FabricaDao.getGheSetorClienteFuncaoDao();
            IGheFonteAgenteDao gheFonteAgenteDao = FabricaDao.getGheFonteAgenteDao();
            IGheFonteAgenteExameDao gheFonteAgenteExameDao = FabricaDao.getGheFonteAgenteExameDao();
            IMedicoDao medicoDao = FabricaDao.getMedicoDao();
            IMedicoEstudoDao medicoEstudoDao = FabricaDao.getMedicoEstudoDao();
            IGheFonteAgenteExamePeriodicidadeDao gheFonteAgenteExamePeriodicidadeDao = FabricaDao.getGheFonteAgenteExamePeriodicidadeDao();

            Estudo pcmsoReplicado = null;

            NpgsqlTransaction transaction = dbConnection.BeginTransaction();
            try
            {
                /* incluindo o novo estudo */
                pcmsoReplicado = estudoDao.insert(new Estudo(null, pcmso.IdRelatorio, string.Empty, pcmso.Cronograma, pcmso.VendedorCliente, pcmso.Engenheiro, pcmso.Tecno, pcmso.ClienteContratante, pcmso.DataCriacao, pcmso.DataVencimento, pcmso.DataFechamento, pcmso.Situacao, pcmso.TipoEstudo, pcmso.DataFimContrato, pcmso.Obra, pcmso.LocalObra, pcmso.Endereco, pcmso.Numero, pcmso.Complemento, pcmso.Bairro, pcmso.Cidade, pcmso.Uf, pcmso.NumContrato, pcmso.GrauRisco, pcmso.VendedorCliente.Cliente.RazaoSocial, pcmso.VendedorCliente.Cliente.Cnpj, pcmso.VendedorCliente.Cliente.Inscricao, pcmso.CliEstFem, pcmso.CliEstMasc, pcmso.VendedorCliente.Cliente.Jornada, pcmso.VendedorCliente.Cliente.Endereco, pcmso.VendedorCliente.Cliente.Numero, pcmso.VendedorCliente.Cliente.Complemento, pcmso.VendedorCliente.Cliente.Bairro, pcmso.VendedorCliente.Cliente.CidadeIbge.Nome, pcmso.VendedorCliente.Cliente.Email, pcmso.VendedorCliente.Cliente.Cep, pcmso.VendedorCliente.Cliente.Uf, pcmso.VendedorCliente.Cliente.Telefone1, pcmso.VendedorCliente.Cliente.Telefone2, pcmso.ClienteContratante != null ? pcmso.ClienteContratante.RazaoSocial : string.Empty, pcmso.ClienteContratante != null ? pcmso.ClienteContratante.Cnpj : string.Empty, pcmso.ClienteContratante != null ? pcmso.ClienteContratante.Endereco : string.Empty, pcmso.ClienteContratante != null ? pcmso.ClienteContratante.Numero : string.Empty, pcmso.ClienteContratante != null ? pcmso.ClienteContratante.Complemento : string.Empty, pcmso.ClienteContratante != null ? pcmso.ClienteContratante.Bairro : string.Empty, pcmso.ClienteContratante != null ? pcmso.ClienteContratante.CidadeIbge.Nome : string.Empty, pcmso.ClienteContratante != null ? pcmso.ClienteContratante.Cep : string.Empty, pcmso.ClienteContratante != null ? pcmso.ClienteContratante.Uf : string.Empty, pcmso.ClienteContratante != null ? pcmso.ClienteContratante.Inscricao : string.Empty, pcmso.DataInicioContrato, pcmso.Cep, pcmso.VendedorCliente.Cliente.Fantasia, string.Empty, pcmso.Avulso, pcmso.Protocolo), dbConnection);


                /*Replicando CNAE_ESTUDO */
                cnaePpraDao.replicaCnaeEstudo((long)pcmso.Id, (long)pcmsoReplicado.Id, dbConnection);

                /* Replicando Médicos coordenador */
                medicoEstudoDao.findAllByPcmsoList(pcmso, dbConnection).ForEach(delegate(MedicoEstudo medicoEstudo)
                {
                    if (medicoEstudo.Coordenador)
                        medicoEstudoDao.insert(new MedicoEstudo(pcmsoReplicado, medicoEstudo.Medico, true, ApplicationConstants.ATIVO), dbConnection);
                });

                /* Replicando GHE e seus relacionamentos */

                gheDao.findAllByEstudo((long)pcmso.Id, dbConnection).ToList().ForEach(delegate(Ghe ghe)
                {
                    if (string.Equals(ghe.Situacao, ApplicationConstants.ATIVO))
                    {
                        Ghe newGhe = gheDao.insert(new Ghe(null, pcmsoReplicado, ghe.Descricao, ghe.Nexp, ApplicationConstants.ATIVO, ghe.NumeroPo), dbConnection);

                        /* replicando os setores do ghe */
                        gheSetorDao.findAtivosByGhe(ghe, dbConnection).ToList().ForEach(delegate(GheSetor gheSetor)
                        {
                            GheSetor newGheSetor = gheSetorDao.insert(new GheSetor(null, gheSetor.Setor, newGhe, ApplicationConstants.ATIVO), dbConnection);

                            /* replicando as funções do Ghe */
                            gheSetorClienteFuncaoDao.findAtivosByGheSetor(gheSetor, dbConnection).ToList().ForEach(delegate(GheSetorClienteFuncao gheSetorClienteFuncao)
                            {
                                GheSetorClienteFuncao newGheSetorClienteFuncao = gheSetorClienteFuncaoDao.insert(new GheSetorClienteFuncao(null, gheSetorClienteFuncao.ClienteFuncao, newGheSetor, gheSetorClienteFuncao.Confinado, gheSetorClienteFuncao.Altura, ApplicationConstants.ATIVO, gheSetorClienteFuncao.ComentarioFuncao, gheSetorClienteFuncao.Eletricidade), dbConnection);
                            });
                        });

                        /*replicando as fontes do Ghe */

                        gheFonteDao.findAllAtivoByGhe(ghe, dbConnection).ToList().ForEach(delegate(GheFonte gheFonte)
                        {
                            GheFonte newGheFonte = gheFonteDao.insert(new GheFonte(null, gheFonte.Fonte, newGhe, gheFonte.Principal, ApplicationConstants.ATIVO), dbConnection);

                            /* replicando os agentes do GheFonte */

                            gheFonteAgenteDao.findAtivosByGheFonteHash(gheFonte, dbConnection).ToList().ForEach(delegate(GheFonteAgente gheFonteAgente)
                            {
                                GheFonteAgente newgheFonteAgente = gheFonteAgenteDao.insert(new GheFonteAgente(null, gheFonteAgente.GradSoma, gheFonteAgente.GradEfeito, gheFonteAgente.GradExposicao, newGheFonte, gheFonteAgente.Agente, gheFonteAgente.TempoExposicao, ApplicationConstants.ATIVO), dbConnection);

                                /* replicando os exames do GheFonteAgente */
                                gheFonteAgenteExameDao.findAllByGheFonteAgente(gheFonteAgente, dbConnection).ToList().ForEach(delegate(GheFonteAgenteExame gheFonteAgenteExame)
                                {
                                    if (string.Equals(gheFonteAgenteExame.Situacao, ApplicationConstants.ATIVO))
                                    {
                                        GheFonteAgenteExame newGheFonteAgenteExame = gheFonteAgenteExameDao.insert(new GheFonteAgenteExame(null, gheFonteAgenteExame.Exame, newgheFonteAgente, gheFonteAgenteExame.IdadeExame, ApplicationConstants.ATIVO, gheFonteAgenteExame.Confinado, gheFonteAgenteExame.Altura, gheFonteAgenteExame.Eletricidade), dbConnection);

                                        /* replicando as periodicidade do GheFonteAgenteExame */

                                        gheFonteAgenteExamePeriodicidadeDao.findAll(gheFonteAgenteExame, dbConnection).ToList().ForEach(delegate(GheFonteAgenteExamePeriodicidade gheFonteAgenteExamePeriodicidade)
                                        {
                                            gheFonteAgenteExamePeriodicidadeDao.insert(new GheFonteAgenteExamePeriodicidade(gheFonteAgenteExamePeriodicidade.Periodicidade, newGheFonteAgenteExame, ApplicationConstants.ATIVO, gheFonteAgenteExamePeriodicidade.PeriodoVencimento), dbConnection);
                                        });
                                    }
                                });
                            });
                        });
                    }
                });
                transaction.Commit();
            }
            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - replicarPcmsoAvulso", ex);
                transaction.Rollback();
                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }
            finally
            {
                FabricaConexao.fecharConexao();
            }

            return pcmsoReplicado;
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public void replicarGhe(Ghe gheNovo, Ghe ghePast)
        {
            NpgsqlConnection dbConnection = FabricaConexao.getConexao();

            IGheDao gheDao = FabricaDao.getGheDao();
            IGheSetorDao gheSetorDao = FabricaDao.getGheSetorDao();

            IGheFonteDao gheFonteDao = FabricaDao.getGheFonteDao();
            IGheSetorClienteFuncaoDao gheSetorClienteFuncaoDao = FabricaDao.getGheSetorClienteFuncaoDao();
            IGheFonteAgenteDao gheFonteAgenteDao = FabricaDao.getGheFonteAgenteDao();
            IGheFonteAgenteExameDao gheFonteAgenteExameDao = FabricaDao.getGheFonteAgenteExameDao();
            IGheFonteAgenteExamePeriodicidadeDao gheFonteAgenteExamePeriodicidadeDao = FabricaDao.getGheFonteAgenteExamePeriodicidadeDao();

            NpgsqlTransaction transaction = dbConnection.BeginTransaction();
            try
            {
                /* replicando os setores */
                gheSetorDao.findAtivosByGhe(ghePast, dbConnection).ToList().ForEach(delegate(GheSetor gheSetor)
                {
                    GheSetor newGheSetor = gheSetorDao.insert(new GheSetor(null, gheSetor.Setor, gheNovo, ApplicationConstants.ATIVO), dbConnection);

                    /* replicando as funções do Ghe */
                    gheSetorClienteFuncaoDao.findAtivosByGheSetor(gheSetor, dbConnection).ToList().ForEach(delegate(GheSetorClienteFuncao gheSetorClienteFuncao)
                    {
                        GheSetorClienteFuncao newGheSetorClienteFuncao = gheSetorClienteFuncaoDao.insert(new GheSetorClienteFuncao(null, gheSetorClienteFuncao.ClienteFuncao, newGheSetor, gheSetorClienteFuncao.Confinado, gheSetorClienteFuncao.Altura, ApplicationConstants.ATIVO, gheSetorClienteFuncao.ComentarioFuncao, gheSetorClienteFuncao.Eletricidade), dbConnection);
                    });
                });

                /*replicando as fontes do Ghe */
                gheFonteDao.findAllAtivoByGhe(ghePast, dbConnection).ToList().ForEach(delegate(GheFonte gheFonte)
                {
                    GheFonte newGheFonte = gheFonteDao.insert(new GheFonte(null, gheFonte.Fonte, gheNovo, gheFonte.Principal, ApplicationConstants.ATIVO), dbConnection);

                    /* replicando os agentes do GheFonte */

                    gheFonteAgenteDao.findAtivosByGheFonteHash(gheFonte, dbConnection).ToList().ForEach(delegate(GheFonteAgente gheFonteAgente)
                    {
                        GheFonteAgente newgheFonteAgente = gheFonteAgenteDao.insert(new GheFonteAgente(null, gheFonteAgente.GradSoma, gheFonteAgente.GradEfeito, gheFonteAgente.GradExposicao, newGheFonte, gheFonteAgente.Agente, gheFonteAgente.TempoExposicao, ApplicationConstants.ATIVO), dbConnection);

                        /* replicando os exames do GheFonteAgente */
                        gheFonteAgenteExameDao.findAllByGheFonteAgente(gheFonteAgente, dbConnection).ToList().ForEach(delegate(GheFonteAgenteExame gheFonteAgenteExame)
                        {
                            if (string.Equals(gheFonteAgenteExame.Situacao, ApplicationConstants.ATIVO))
                            {
                                GheFonteAgenteExame newGheFonteAgenteExame = gheFonteAgenteExameDao.insert(new GheFonteAgenteExame(null, gheFonteAgenteExame.Exame, newgheFonteAgente, gheFonteAgenteExame.IdadeExame, ApplicationConstants.ATIVO, gheFonteAgenteExame.Confinado, gheFonteAgenteExame.Altura, gheFonteAgenteExame.Eletricidade), dbConnection);

                                /* replicando as periodicidade do GheFonteAgenteExame */

                                gheFonteAgenteExamePeriodicidadeDao.findAll(gheFonteAgenteExame, dbConnection).ToList().ForEach(delegate(GheFonteAgenteExamePeriodicidade gheFonteAgenteExamePeriodicidade)
                                {
                                    gheFonteAgenteExamePeriodicidadeDao.insert(new GheFonteAgenteExamePeriodicidade(gheFonteAgenteExamePeriodicidade.Periodicidade, newGheFonteAgenteExame, ApplicationConstants.ATIVO, gheFonteAgenteExamePeriodicidade.PeriodoVencimento), dbConnection);
                                });
                            }
                        });
                    });
                });

                transaction.Commit();
            }
            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - replicarGhe", ex);
                transaction.Rollback();
                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }
            finally
            {
                FabricaConexao.fecharConexao();
            }
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public List<Exame> findAllExameAtivoPadraoContrato()
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findAllExameAtivoPadraoContrato");
            NpgsqlConnection dbConnection = FabricaConexao.getConexao();
            IExameDao exameDao = FabricaDao.getExameDao();
            try
            {
                return exameDao.findAllAtivosPadraoContrato(dbConnection);
            }
            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findAllExameAtivoPadraoContrato", ex);
                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }
            finally
            {
                FabricaConexao.fecharConexao();
            }
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public Estudo updatePcmso(Estudo pcmso)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método updatePcmso");

            NpgsqlConnection dbConnection = FabricaConexao.getConexao();
            IEstudoDao estudoDao = FabricaDao.getEstudoDao();
            ICnaeEstudoDao cnaeEstudoDao = FabricaDao.getCnaeEstudoDao();
            NpgsqlTransaction transaction = dbConnection.BeginTransaction();

            try
            {
                pcmso = estudoDao.update(pcmso, dbConnection);
                transaction.Commit();
                return pcmso;
            }
            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - updatePcmso", ex);
                transaction.Rollback();
                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }
            finally
            {
                FabricaConexao.fecharConexao();
            }
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public GheFonteAgente updateGheFonteAgente(GheFonteAgente gheFonteAgente)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método updateGheFonteAgente");

            NpgsqlConnection dbConnection = FabricaConexao.getConexao();
            IGheFonteAgenteDao gheFonteAgenteDao = FabricaDao.getGheFonteAgenteDao();
            NpgsqlTransaction transaction = dbConnection.BeginTransaction();

            try
            {
                gheFonteAgente = gheFonteAgenteDao.update(gheFonteAgente, dbConnection);
                transaction.Commit();
                return gheFonteAgente;
            }
            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - updatePcmso", ex);
                transaction.Rollback();
                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }
            finally
            {
                FabricaConexao.fecharConexao();
            }
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public List<CronogramaAtividade> findAllAtividadesByCronograma(Cronograma cronograma)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findAllAtividadesByCronograma");

            NpgsqlConnection dbConnection = FabricaConexao.getConexao();
            ICronogramaAtividadeDao cronogramaAtividadeDao = FabricaDao.getCronogramaAtividadeDao();
            try
            {
                return cronogramaAtividadeDao.findByCronograma(cronograma, dbConnection).ToList();
                
            }
            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findAllAtividadesByCronograma", ex);
                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }
            finally
            {
                FabricaConexao.fecharConexao();
            }
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public DocumentoEstudo insertDocumentoEstudo(DocumentoEstudo documentoEstudo)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método insertDocumentoEstudo");

            NpgsqlConnection dbConnection = FabricaConexao.getConexao();
            IDocumentoEstudoDao documentoEstudoDao = FabricaDao.getDocumentoEstudoDao();
            NpgsqlTransaction transaction = dbConnection.BeginTransaction();

            try
            {
                documentoEstudo = documentoEstudoDao.insert(documentoEstudo, dbConnection);
                transaction.Commit();
                return documentoEstudo;
            }
            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - insertDocumentoEstudo", ex);
                transaction.Rollback();
                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }
            finally
            {
                FabricaConexao.fecharConexao();
            }
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public void deleteDocumentoEstudo(DocumentoEstudo documentoEstudo)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método deleteDocumentoEstudo");

            NpgsqlConnection dbConnection = FabricaConexao.getConexao();
            IDocumentoEstudoDao documentoEstudoDao = FabricaDao.getDocumentoEstudoDao();
            NpgsqlTransaction transaction = dbConnection.BeginTransaction();

            try
            {
                documentoEstudoDao.delete(documentoEstudo, dbConnection);
                transaction.Commit();
            }
            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - deleteDocumentoEstudo", ex);
                transaction.Rollback();
                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }
            finally
            {
                FabricaConexao.fecharConexao();
            }
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public DataSet findAllDocumentoByPcmso(Estudo pcmso)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findAllDocumentoByPcmso");

            NpgsqlConnection dbConnection = FabricaConexao.getConexao();
            IDocumentoEstudoDao documentoEstudoDao = FabricaDao.getDocumentoEstudoDao();

            try
            {

                return documentoEstudoDao.findAllByEstudo(pcmso, dbConnection);

            }
            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findAllDocumentoByPcmso", ex);
                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }
            finally
            {
                FabricaConexao.fecharConexao();
            }
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public HashSet<MaterialHospitalarEstudo> findAllMateriaHospitalarByEstudo(Estudo pcmso)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findAllMateriaHospitalarByEstudo");

            NpgsqlConnection dbConnection = FabricaConexao.getConexao();
            IMaterialHospitalarEstudoDao materialHospitalarEstudoDao = FabricaDao.getMaterialHospitalarEstudoDao();
            try
            {

                return materialHospitalarEstudoDao.findAllByEstudo(pcmso, dbConnection);

            }
            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findAllMateriaHospitalarByEstudo", ex);
                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }
            finally
            {
                FabricaConexao.fecharConexao();
            }
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public List<DocumentoEstudo> findAllDocumentoByPcmsoList(Estudo pcmso)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findAllDocumentoByPcmsoList");

            NpgsqlConnection dbConnection = FabricaConexao.getConexao();
            IDocumentoEstudoDao documentoEstudoDao = FabricaDao.getDocumentoEstudoDao();
            try
            {

                return documentoEstudoDao.findAllByEstudoList(pcmso, dbConnection);

            }
            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findAllDocumentoByPcmsoList", ex);
                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }
            finally
            {
                FabricaConexao.fecharConexao();
            }
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public Estudo reativar(Estudo pcmso)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método reativar");

            NpgsqlConnection dbConnection = FabricaConexao.getConexao();

            IEstudoDao estudoDao = FabricaDao.getEstudoDao();

            IGheDao gheDao = FabricaDao.getGheDao();
            IGheSetorDao gheSetorDao = FabricaDao.getGheSetorDao();
            IGheFonteDao gheFonteDao = FabricaDao.getGheFonteDao();
            IGheSetorClienteFuncaoDao gheSetorClienteFuncaoDao = FabricaDao.getGheSetorClienteFuncaoDao();
            IGheFonteAgenteDao gheFonteAgenteDao = FabricaDao.getGheFonteAgenteDao();
            IGheFonteAgenteExameDao gheFonteAgenteExameDao = FabricaDao.getGheFonteAgenteExameDao();
            IGheFonteAgenteExamePeriodicidadeDao gheFonteAgenteExamePeriodicidadeDao = FabricaDao.getGheFonteAgenteExamePeriodicidadeDao();
            IEstimativaEstudoDao estimativaEstudoDao = FabricaDao.getEstimativaEstudoDao();

            NpgsqlTransaction transaction = dbConnection.BeginTransaction();
            try
            {

                /* reativando o estudo */
                pcmso.Situacao = ApplicationConstants.FECHADO;
                estudoDao.update(pcmso, dbConnection);

                /* reativando os ghes */

                foreach (Ghe ghe in gheDao.findAllByEstudo((long)pcmso.Id, dbConnection))
                {
                    ghe.Situacao = ApplicationConstants.ATIVO;
                    gheDao.update(ghe, dbConnection);

                    /* reativando setores */

                    List<GheSetor> gheSetores = new List<GheSetor>();
                    gheSetores = gheSetorDao.findAllByGheList(ghe, dbConnection);

                    foreach (GheSetor gheSetor in gheSetores)
                    {
                        gheSetor.Situacao = ApplicationConstants.ATIVO;
                        gheSetorDao.update(gheSetor, dbConnection);

                        List<GheSetorClienteFuncao> gheSetorClienteFuncoes = new List<GheSetorClienteFuncao>();
                        gheSetorClienteFuncoes = gheSetorClienteFuncaoDao.findAllByGheSetorList(gheSetor, dbConnection);

                        foreach (GheSetorClienteFuncao gheSetorClienteFuncao in gheSetorClienteFuncoes)
                        {
                            gheSetorClienteFuncao.Situacao = ApplicationConstants.ATIVO;
                            gheSetorClienteFuncaoDao.update(gheSetorClienteFuncao, dbConnection);
                        }
                    }

                    /* atualizando o gheFonte */

                    List<GheFonte> gheFontes = new List<GheFonte>();
                    gheFontes = gheFonteDao.findAllByGheList(ghe, dbConnection);

                    foreach (GheFonte gheFonte in gheFontes)
                    {
                        gheFonte.Situacao = ApplicationConstants.ATIVO;
                        gheFonteDao.update(gheFonte, dbConnection);

                        List<GheFonteAgente> gheFonteAgentes = new List<GheFonteAgente>();
                        gheFonteAgentes = gheFonteAgenteDao.findAllByGheFonteList(gheFonte, dbConnection);

                        foreach (GheFonteAgente gheFonteAgente in gheFonteAgentes)
                        {
                            gheFonteAgente.Situacao = ApplicationConstants.ATIVO;
                            gheFonteAgenteDao.update(gheFonteAgente, dbConnection);

                            List<GheFonteAgenteExame> gheFonteAgenteExames = new List<GheFonteAgenteExame>();
                            gheFonteAgenteExames = gheFonteAgenteExameDao.findAllByGheFonteAgente(gheFonteAgente, dbConnection).ToList();

                            foreach (GheFonteAgenteExame gheFonteAgenteExame in gheFonteAgenteExames)
                            {
                                gheFonteAgenteExame.Situacao = ApplicationConstants.ATIVO;
                                gheFonteAgenteExameDao.update(gheFonteAgenteExame, dbConnection);

                                List<GheFonteAgenteExamePeriodicidade> gheFonteAgenteExamePeriodicidades = new List<GheFonteAgenteExamePeriodicidade>();

                                gheFonteAgenteExamePeriodicidades = gheFonteAgenteExamePeriodicidadeDao.findAll(gheFonteAgenteExame, dbConnection).ToList();

                                foreach (GheFonteAgenteExamePeriodicidade gheFonteAgenteExamePeriodicidade in gheFonteAgenteExamePeriodicidades)
                                {
                                    gheFonteAgenteExamePeriodicidade.Situacao = ApplicationConstants.ATIVO;
                                    gheFonteAgenteExamePeriodicidadeDao.update(gheFonteAgenteExamePeriodicidade, dbConnection);
                                }
                            }
                        }
                    }
                }

                /* replicando dados de estimativa */

                DataSet ds = estimativaEstudoDao.findAllByEstudo(pcmso, dbConnection);

                foreach (DataRow row in ds.Tables[0].Rows)
                {
                    EstimativaEstudo estimativaEstudo = new EstimativaEstudo();
                    Estimativa estimativa = new Estimativa();
                    estimativa.Id = (long)row["ID_ESTIMATIVA"];
                    estimativa.Descricao = row["DESCRICAO"].ToString();
                    estimativa.Situacao = ApplicationConstants.ATIVO;

                    estimativaEstudo.Estimativa = estimativa;
                    estimativaEstudo.Estudo = pcmso;
                    estimativaEstudo.Quantidade = Convert.ToInt32(row["QUANTIDADE"]);

                    estimativaEstudoDao.update(estimativaEstudo, dbConnection);
                }

                transaction.Commit();

                return estudoDao.findById((long)pcmso.Id, dbConnection);
            }
            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - replicarPcmso", ex);
                transaction.Rollback();
                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }
            finally
            {
                FabricaConexao.fecharConexao();
            }
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public void duplicarGheSetor(GheSetor gheSetorNovo, GheSetor gheSetorOrigem)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método duplicarGheSetor");

            NpgsqlConnection dbConnection = FabricaConexao.getConexao();
            NpgsqlTransaction transaction = dbConnection.BeginTransaction();
            IGheSetorDao gheSetorDao = FabricaDao.getGheSetorDao();
            IGheSetorClienteFuncaoDao gheSetorClienteFuncaoDao = FabricaDao.getGheSetorClienteFuncaoDao();
            IGheSetorClienteFuncaoExameDao gheSetorClienteFuncaoExameDao = FabricaDao.getGheSetorClienteFuncaoExameDao();

            try
            {
                gheSetorNovo = gheSetorDao.insert(gheSetorNovo, dbConnection);

                List<GheSetorClienteFuncao> gheSetorClienteFuncoes = gheSetorClienteFuncaoDao.findAtivosByGheSetor(gheSetorOrigem, dbConnection).ToList();

                foreach (GheSetorClienteFuncao gheSetorClienteFuncao in gheSetorClienteFuncoes)
                {

                    GheSetorClienteFuncao gheSetorClienteFuncaoOrigem = new GheSetorClienteFuncao();
                    gheSetorClienteFuncaoOrigem = gheSetorClienteFuncao.copy();
                    
                    gheSetorClienteFuncao.GheSetor = gheSetorNovo;
                    GheSetorClienteFuncao gheSetorClienteFuncaoNew = gheSetorClienteFuncaoDao.insert(gheSetorClienteFuncao, dbConnection);

                    /* duplicando gheSetorClientefuncaoExame */

                    List<GheSetorClienteFuncaoExame> gheSetorClienteFuncaoExames = new List<GheSetorClienteFuncaoExame>();
                    gheSetorClienteFuncaoExames = gheSetorClienteFuncaoExameDao.findAllByGheSetorClienteFuncao(gheSetorClienteFuncaoOrigem, dbConnection);

                    foreach (GheSetorClienteFuncaoExame gheSetorClienteFuncaoExame in gheSetorClienteFuncaoExames)
                    {
                        gheSetorClienteFuncaoExame.GheSetorClienteFuncao = gheSetorClienteFuncaoNew;
                        gheSetorClienteFuncaoExameDao.insert(gheSetorClienteFuncaoExame, dbConnection);
                    }
                    
                }

                transaction.Commit();
            }
            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - duplicarGheSetor", ex);
                transaction.Rollback();
                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }
            finally
            {
                FabricaConexao.fecharConexao();
            }
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public void replicarFonte(GheFonte gheFonteOrigem, GheFonte gheFonteReplica)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método replicarFonte");

            NpgsqlConnection dbConnection = FabricaConexao.getConexao();
            NpgsqlTransaction transaction = dbConnection.BeginTransaction();

            IGheFonteDao gheFonteDao = FabricaDao.getGheFonteDao();
            IGheFonteAgenteDao gheFonteAgenteDao = FabricaDao.getGheFonteAgenteDao();
            IGheFonteAgenteExameDao gheFonteAgenteExameDao = FabricaDao.getGheFonteAgenteExameDao();
            IGheFonteAgenteExamePeriodicidadeDao gheFonteAgenteExamePeriodicidadeDao = FabricaDao.getGheFonteAgenteExamePeriodicidadeDao();

            try
            {
                /* incluindo gheFonteNovo */
                gheFonteReplica = gheFonteDao.insert(gheFonteReplica, dbConnection);


                /* replicando o relacionamento com agentes */

                List<GheFonteAgente> gheFonteAgentes = new List<GheFonteAgente>();
                gheFonteAgentes = gheFonteAgenteDao.findAtivosByGheFonteHash(gheFonteOrigem, dbConnection).ToList();

                foreach (GheFonteAgente gheFonteAgente in gheFonteAgentes)
                {
                    GheFonteAgente gheFonteAgenteNew = new GheFonteAgente();
                    gheFonteAgenteNew = gheFonteAgente.copy();

                    gheFonteAgenteNew.GheFonte = gheFonteReplica;
                    gheFonteAgenteNew = gheFonteAgenteDao.insert(gheFonteAgenteNew, dbConnection);

                    /* replicando relacionamento com exames */
                    DataSet ds = gheFonteAgenteExameDao.findAtivosByGheFonteAgente(gheFonteAgente, dbConnection);

                    foreach (DataRow row in ds.Tables[0].Rows)
                    {
                        GheFonteAgenteExame gheFonteAgenteExame = new GheFonteAgenteExame();
                        gheFonteAgenteExame.Id = (long)row["ID_GHE_FONTE_AGENTE_EXAME"];
                        gheFonteAgenteExame.IdadeExame = (int)row["IDADE_EXAME"];
                        gheFonteAgenteExame.Situacao = ApplicationConstants.ATIVO;
                        Exame exame = new Exame();
                        exame.Id = (long)row["ID_EXAME"];
                        gheFonteAgenteExame.Exame = exame;
                        gheFonteAgenteExame.GheFonteAgente = gheFonteAgente;

                        GheFonteAgenteExame gheFonteAgenteExameNew = new GheFonteAgenteExame();
                        gheFonteAgenteExameNew = gheFonteAgenteExame.copy();

                        gheFonteAgenteExameNew.Id = null;
                        gheFonteAgenteExameNew.GheFonteAgente = gheFonteAgenteNew;

                        gheFonteAgenteExameNew = gheFonteAgenteExameDao.insert(gheFonteAgenteExameNew, dbConnection);


                        /* Replicando o gheFonteAgenteExamePeriodicidade */

                        List<GheFonteAgenteExamePeriodicidade> gheFonteAgenteExamePeriodicidades = new List<GheFonteAgenteExamePeriodicidade>();
                        gheFonteAgenteExamePeriodicidades = gheFonteAgenteExamePeriodicidadeDao.findAll(
                            gheFonteAgenteExame, dbConnection).ToList();

                        foreach (GheFonteAgenteExamePeriodicidade gheFonteAgenteExamePeriodicidade in
                            gheFonteAgenteExamePeriodicidades)
                        {
                            GheFonteAgenteExamePeriodicidade gheFonteAgenteExamePeriodicidadeNew = new GheFonteAgenteExamePeriodicidade();
                            gheFonteAgenteExamePeriodicidadeNew = gheFonteAgenteExamePeriodicidade.copy();

                            gheFonteAgenteExamePeriodicidadeNew.GheFonteAgenteExame = gheFonteAgenteExameNew;
                            gheFonteAgenteExamePeriodicidadeDao.insert(gheFonteAgenteExamePeriodicidadeNew, dbConnection);
                        }
                    }
                }

                transaction.Commit();
            }
            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - replicarFonte", ex);
                transaction.Rollback();
                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }
            finally
            {
                FabricaConexao.fecharConexao();
            }
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public void replicarAgente(GheFonteAgente gheFonteAgenteOrigem, GheFonteAgente gheFonteAgenteReplica)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método replicarAgente");

            NpgsqlConnection dbConnection = FabricaConexao.getConexao();
            NpgsqlTransaction transaction = dbConnection.BeginTransaction();

            IGheFonteAgenteDao gheFonteAgenteDao = FabricaDao.getGheFonteAgenteDao();
            IGheFonteAgenteExameDao gheFonteAgenteExameDao = FabricaDao.getGheFonteAgenteExameDao();
            IGheFonteAgenteExamePeriodicidadeDao gheFonteAgenteExamePeriodicidadeDao = FabricaDao.getGheFonteAgenteExamePeriodicidadeDao();

            try
            {
                /* incluindo gheFonteAgenteNovo */
                gheFonteAgenteReplica = gheFonteAgenteDao.insert(gheFonteAgenteReplica, dbConnection);

                /* replicando relacionamento com exames */
                DataSet ds = gheFonteAgenteExameDao.findAtivosByGheFonteAgente(gheFonteAgenteOrigem, dbConnection);

                foreach (DataRow row in ds.Tables[0].Rows)
                {
                    GheFonteAgenteExame gheFonteAgenteExame = new GheFonteAgenteExame();
                    gheFonteAgenteExame.Id = (long)row["ID_GHE_FONTE_AGENTE_EXAME"];
                    gheFonteAgenteExame.IdadeExame = (int)row["IDADE_EXAME"];
                    gheFonteAgenteExame.Situacao = ApplicationConstants.ATIVO;
                    Exame exame = new Exame();
                    exame.Id = (long)row["ID_EXAME"];
                    gheFonteAgenteExame.Exame = exame;
                    gheFonteAgenteExame.GheFonteAgente = gheFonteAgenteOrigem;

                    GheFonteAgenteExame gheFonteAgenteExameNew = new GheFonteAgenteExame();
                    gheFonteAgenteExameNew = gheFonteAgenteExame.copy();

                    gheFonteAgenteExameNew.Id = null;
                    gheFonteAgenteExameNew.GheFonteAgente = gheFonteAgenteReplica;

                    gheFonteAgenteExameNew = gheFonteAgenteExameDao.insert(gheFonteAgenteExameNew, dbConnection);


                    /* Replicando o gheFonteAgenteExamePeriodicidade */

                    List<GheFonteAgenteExamePeriodicidade> gheFonteAgenteExamePeriodicidades = new List<GheFonteAgenteExamePeriodicidade>();
                    gheFonteAgenteExamePeriodicidades = gheFonteAgenteExamePeriodicidadeDao.findAll(
                        gheFonteAgenteExame, dbConnection).ToList();

                    foreach (GheFonteAgenteExamePeriodicidade gheFonteAgenteExamePeriodicidade in
                        gheFonteAgenteExamePeriodicidades)
                    {
                        GheFonteAgenteExamePeriodicidade gheFonteAgenteExamePeriodicidadeNew = new GheFonteAgenteExamePeriodicidade();
                        gheFonteAgenteExamePeriodicidadeNew = gheFonteAgenteExamePeriodicidade.copy();

                        gheFonteAgenteExamePeriodicidadeNew.GheFonteAgenteExame = gheFonteAgenteExameNew;
                        gheFonteAgenteExamePeriodicidadeDao.insert(gheFonteAgenteExamePeriodicidadeNew, dbConnection);
                    }
                }

                transaction.Commit();
            }
            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - replicarAgente", ex);
                transaction.Rollback();
                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }
            finally
            {
                FabricaConexao.fecharConexao();
            }
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public MedicoExame insertMedicoExame(MedicoExame medicoExame)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método insertMedicoExame");

            NpgsqlConnection dbConnection = FabricaConexao.getConexao();
            IMedicoExameDao MedicoExameDao = FabricaDao.getMedicoExameDao();
            NpgsqlTransaction transaction = dbConnection.BeginTransaction();

            MedicoExame medicoExameNovo;

            try
            {
                medicoExameNovo = MedicoExameDao.insert(medicoExame, dbConnection);
                transaction.Commit();

                return medicoExameNovo;

            }
            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - insertMedicoExame", ex);
                transaction.Rollback();
                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }
            finally
            {
                FabricaConexao.fecharConexao();
            }
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public Ghe findGheById(Int64? idGhe)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findGheById");

            NpgsqlConnection dbConnection = FabricaConexao.getConexao();
            IGheDao gheDao = FabricaDao.getGheDao();
            try
            {
                Ghe ghe = gheDao.findById(idGhe, dbConnection);
                return ghe;

            }
            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findGheById", ex);
                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }
            finally
            {
                FabricaConexao.fecharConexao();
            }
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public Ghe insertGhe(Ghe ghe)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método incluirGheEstudo");

            Ghe gheNovo = null; // iniciando objeto do tipo Ghe como nulo.
            Ghe gheProcurado = null;

            NpgsqlConnection dbConnection = FabricaConexao.getConexao(); // criando conexao com o banco de dados.

            IGheDao gheDao = FabricaDao.getGheDao(); // pegando assinatura do metodo.
            IGheNotaDao gheNormaDao = FabricaDao.getGheNormaDao();

            NpgsqlTransaction transaction = dbConnection.BeginTransaction();
            try
            {
                gheProcurado = gheDao.findByDescricao(ghe, dbConnection);

                if (gheProcurado == null)
                {
                    //Incluindo ghe
                    gheNovo = gheDao.insert(ghe, dbConnection); // passando para camada Dao para transacao com o banco.

                    //Incluindo normas no GHE

                    if (ghe.Norma != null) // testando se existe algum elemento norma incluido no GHE
                    {

                        GheNota ghenorma = new GheNota();

                        foreach (Nota norma in ghe.Norma)
                        {
                            ghenorma.Ghe = ghe;
                            ghenorma.Nota = norma;

                            gheNormaDao.insert(ghenorma, dbConnection);

                        }
                    }
                }
                else
                {
                    throw new DescricaoGheJaCadastradaException(DescricaoGheJaCadastradaException.msg);
                }

                transaction.Commit();
            }
            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - novoGhe", ex);
                transaction.Rollback();
                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }
            finally
            {
                FabricaConexao.fecharConexao();
            }

            return gheNovo;
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public Ghe updateGhe(Ghe ghe)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método alterarGhe");

            NpgsqlConnection dbConnection = FabricaConexao.getConexao();
            IGheDao gheDao = FabricaDao.getGheDao();
            NpgsqlTransaction transaction = dbConnection.BeginTransaction();

            try
            {
                ghe = gheDao.update(ghe, dbConnection);

                transaction.Commit();
            }
            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - alterarGhe", ex);
                transaction.Rollback();
                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }
            finally
            {
                FabricaConexao.fecharConexao();
            }

            return ghe;
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public void deleteNotaGhe(GheNota gheNota)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método deleteNormaGhe");

            NpgsqlConnection dbConnection = FabricaConexao.getConexao();

            IGheNotaDao gheNormaDao = FabricaDao.getGheNormaDao();

            NpgsqlTransaction transaction = dbConnection.BeginTransaction();

            try
            {
                gheNormaDao.delete(gheNota, dbConnection);

                transaction.Commit();
            }
            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - deleteNormaGhe", ex);
                transaction.Rollback();
                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }
            finally
            {
                FabricaConexao.fecharConexao();
            }
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public HashSet<GheFonteAgente> findAllGheFonteAgenteByGheFonte(GheFonte gheFonte)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findAllGheFonteAgenteByGheFonte");

            NpgsqlConnection dbConnection = FabricaConexao.getConexao();
            IGheFonteAgenteDao gheFonteAgenteDao = FabricaDao.getGheFonteAgenteDao();
            try
            {
                return gheFonteAgenteDao.findAtivosByGheFonteHash(gheFonte, dbConnection);

            }
            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findAllGheFonteAgenteByGheFonte", ex);
                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }
            finally
            {
                FabricaConexao.fecharConexao();
            }
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public HashSet<GheFonte> findAllFonteByGhe(Ghe ghe)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findAllFonteByGhe");

            NpgsqlConnection dbConnection = FabricaConexao.getConexao();
            HashSet<GheFonte> setGheFonte = new HashSet<GheFonte>();

            IGheFonteDao gheFonteDao = FabricaDao.getGheFonteDao();
            try
            {

                foreach (GheFonte gheFonte in gheFonteDao.findAllAtivoByGhe(ghe, dbConnection))
                {
                    setGheFonte.Add(new GheFonte(gheFonte.Id, gheFonte.Fonte, gheFonte.Ghe, gheFonte.Principal, gheFonte.Situacao));
                }

                return setGheFonte;

            }
            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findAllFonteByGhe", ex);
                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }

            finally
            {
                FabricaConexao.fecharConexao();
            }
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public void deleteGheFonteById(GheFonte gheFonte)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método deleteGheFonteById");

            NpgsqlConnection dbConnection = FabricaConexao.getConexao();
            IGheFonteDao gheFonteDao = FabricaDao.getGheFonteDao();
            IGheFonteAgenteDao gheFonteAgenteDao = FabricaDao.getGheFonteAgenteDao();
            IGheFonteAgenteExameDao gheFonteAgenteExameDao = FabricaDao.getGheFonteAgenteExameDao();
            IGheFonteAgenteExamePeriodicidadeDao gheFonteAgenteExamePeriodicidadeDao = FabricaDao.getGheFonteAgenteExamePeriodicidadeDao();

            NpgsqlTransaction transaction = dbConnection.BeginTransaction();

            try
            {
                /* verificar se o GheFonte pode ser excluído */

                if (!gheFonteDao.validaExclusaoGheFonte(gheFonte, dbConnection))
                {
                    /* gheFonte não pode ser excluído, então deve ser desativado */
                    gheFonte.Situacao = ApplicationConstants.DESATIVADO;
                    gheFonteDao.update(gheFonte, dbConnection);

                    /* alterando os relacionamentos abaixo */

                    List<GheFonteAgente> gheFonteAgentes = gheFonteAgenteDao.findAtivosByGheFonteHash(gheFonte, dbConnection).ToList();

                    foreach (GheFonteAgente gheFonteAgente in gheFonteAgentes)
                    {
                        gheFonteAgente.Situacao = ApplicationConstants.DESATIVADO;
                        gheFonteAgenteDao.update(gheFonteAgente, dbConnection);

                        /* alterando o relacionamento com exames */

                        DataSet ds = gheFonteAgenteExameDao.findAtivosByGheFonteAgente(gheFonteAgente, dbConnection);

                        foreach (DataRow row in ds.Tables[0].Rows)
                        {
                            GheFonteAgenteExame gheFonteAgenteExame = gheFonteAgenteExameDao.findById(
                                (long)row["ID_GHE_FONTE_AGENTE_EXAME"], dbConnection);

                            gheFonteAgenteExame.Situacao = ApplicationConstants.DESATIVADO;
                            gheFonteAgenteExameDao.update(gheFonteAgenteExame, dbConnection);

                            List<GheFonteAgenteExamePeriodicidade> gheFonteAgenteExamePeriodicidades = gheFonteAgenteExamePeriodicidadeDao.findAllAtivosByGheFonteAgenteExame(
                                gheFonteAgenteExame, dbConnection);

                            foreach (GheFonteAgenteExamePeriodicidade gheFonteAgenteExamePeriodicidade in gheFonteAgenteExamePeriodicidades)
                            {
                                gheFonteAgenteExamePeriodicidade.Situacao = ApplicationConstants.DESATIVADO;
                                gheFonteAgenteExamePeriodicidadeDao.update(
                                    gheFonteAgenteExamePeriodicidade, dbConnection);
                            }

                        }
                    }

                }
                else
                {
                    gheFonteDao.deleteGheFonte(gheFonte, dbConnection);
                }

                transaction.Commit();
            }
            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - deleteGheFonteById", ex);
                transaction.Rollback();
                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }
            finally
            {
                FabricaConexao.fecharConexao();
            }
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public DataSet findAllGradExposicao()
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findAllGradExposicao");

            NpgsqlConnection dbConnection = FabricaConexao.getConexao();

            IGradExposicaoDao gradExposicaoDao = FabricaDao.getGradExposicaoDao();

            try
            {
                return gradExposicaoDao.findAllGradExposicao(dbConnection);

            }
            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findAllGradExposicao", ex);
                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }
            finally
            {
                FabricaConexao.fecharConexao();
            }
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public DataSet findAllGradEfeito()
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findAllGradEfeito");

            NpgsqlConnection dbConnection = FabricaConexao.getConexao();

            IGradEfeitoDao gradEfeitoDao = FabricaDao.getGradEfeitoDao();

            try
            {
                return gradEfeitoDao.findAllGradEfeito(dbConnection);

            }
            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findAllGradEfeito", ex);
                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }
            finally
            {
                FabricaConexao.fecharConexao();
            }
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public GradSoma findGradSomaByGradSoma(GradSoma gradSoma)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findGradSomaByGradSoma");

            NpgsqlConnection dbConnection = FabricaConexao.getConexao();

            IGradSomaDao gradSomaDao = FabricaDao.getGradSomaDao();

            try
            {
                return gradSomaDao.findGradSomaByGradSoma(gradSoma, dbConnection);

            }
            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findGradSomaByGradSoma", ex);
                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }
            finally
            {
                FabricaConexao.fecharConexao();
            }
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public GradExposicao findGradExposicaoById(GradExposicao gradExposicao)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findGradExposicaoById");

            NpgsqlConnection dbConnection = FabricaConexao.getConexao();
            IGradExposicaoDao gradExposicaoDao = FabricaDao.getGradExposicaoDao();
            try
            {
                gradExposicao = gradExposicaoDao.findGradExposicaoByGradExposicao(gradExposicao, dbConnection);
                return gradExposicao;

            }
            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findGradExposicaoById", ex);
                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }
            finally
            {
                FabricaConexao.fecharConexao();
            }
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public GradEfeito findGradEfeitoById(GradEfeito gradEfeito)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findGradEfeitoById");

            NpgsqlConnection dbConnection = FabricaConexao.getConexao();
            IGradEfeitoDao gradEfeitoDao = FabricaDao.getGradEfeitoDao();
            try
            {
                gradEfeito = gradEfeitoDao.findGradEfeitoByGradEfeito(gradEfeito, dbConnection);
                return gradEfeito;

            }
            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findGradEfeitoById", ex);
                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }
            finally
            {
                FabricaConexao.fecharConexao();
            }
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public GheFonteAgente insertGheFonteAgente(GheFonteAgente gheFonteAgente)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método incluiGheFonteAgente");

            NpgsqlConnection dbConnection = FabricaConexao.getConexao();
            IGheFonteAgenteDao gheFonteAgenteDao = FabricaDao.getGheFonteAgenteDao();
            try
            {
                gheFonteAgente = gheFonteAgenteDao.insert(gheFonteAgente, dbConnection);

                return gheFonteAgente;
            }
            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - incluiGheFonteAgente", ex);
                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }
            finally
            {
                FabricaConexao.fecharConexao();
            }
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public void deleteGheFonteAgenteById(GheFonteAgente gheFonteAgente)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método deleteGheFonteAgenteById");

            NpgsqlConnection dbConnection = FabricaConexao.getConexao();
            IGheFonteAgenteDao gheFonteAgenteDao = FabricaDao.getGheFonteAgenteDao();
            IGheFonteAgenteExameDao gheFonteAgenteExameDao = FabricaDao.getGheFonteAgenteExameDao();
            IGheFonteAgenteExamePeriodicidadeDao gheFonteAgenteExamePeriodicidadeDao = FabricaDao.getGheFonteAgenteExamePeriodicidadeDao();

            NpgsqlTransaction transaction = dbConnection.BeginTransaction();

            try
            {
                /* verificar se pode ser excluído o gheFonteAgente */

                if (gheFonteAgenteDao.isGheFonteAgenteUsed(gheFonteAgente, dbConnection))
                {
                    gheFonteAgente.Situacao = ApplicationConstants.DESATIVADO;
                    gheFonteAgenteDao.update(gheFonteAgente, dbConnection);

                    /* desativando o relacionamento com exame */

                    DataSet ds = gheFonteAgenteExameDao.findAtivosByGheFonteAgente(gheFonteAgente, dbConnection);

                    foreach (DataRow row in ds.Tables[0].Rows)
                    {
                        GheFonteAgenteExame gheFonteAgenteExame = gheFonteAgenteExameDao.findById(
                            (long)row["ID_GHE_FONTE_AGENTE_EXAME"], dbConnection);

                        gheFonteAgenteExame.Situacao = ApplicationConstants.DESATIVADO;
                        gheFonteAgenteExameDao.update(gheFonteAgenteExame, dbConnection);

                        /* desativando o exame periodicidade */

                        List<GheFonteAgenteExamePeriodicidade> periodicidades =
                            gheFonteAgenteExamePeriodicidadeDao.findAllAtivosByGheFonteAgenteExame(
                            gheFonteAgenteExame, dbConnection);

                        foreach (GheFonteAgenteExamePeriodicidade periodo in periodicidades)
                        {
                            periodo.Situacao = ApplicationConstants.DESATIVADO;
                            gheFonteAgenteExamePeriodicidadeDao.update(
                                periodo, dbConnection);
                        }
                    }
                }
                else
                {
                    gheFonteAgenteDao.delete((long)gheFonteAgente.Id, dbConnection);
                }

                transaction.Commit();
            }
            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - deleteGheFonteAgenteById", ex);
                transaction.Rollback();
                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }
            finally
            {
                FabricaConexao.fecharConexao();
            }
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public GheFonteAgente findGheFonteAgenteById(Int64 id)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findGheFonteAgenteById");

            NpgsqlConnection dbConnection = FabricaConexao.getConexao();

            IGheFonteAgenteDao gheFonteAgenteDao = FabricaDao.getGheFonteAgenteDao();
            IGheFonteDao gheFonteDao = FabricaDao.getGheFonteDao();
            IAgenteDao agenteDao = FabricaDao.getAgenteDao();
            IGradEfeitoDao gradEfeitoDao = FabricaDao.getGradEfeitoDao();
            IGradSomaDao gradSomaDao = FabricaDao.getGradSomaDao();
            IGradExposicaoDao gradExposicaoDao = FabricaDao.getGradExposicaoDao();

            try
            {
                GheFonteAgente gheFonteAgente = gheFonteAgenteDao.findById(id, dbConnection);

                if (gheFonteAgente != null)
                {
                    /* preenchendo demais objetos */
                    if (gheFonteAgente.GheFonte != null)
                        gheFonteAgente.GheFonte = gheFonteDao.findByGheFonte(gheFonteAgente.GheFonte, dbConnection);

                    if (gheFonteAgente.Agente != null)
                        gheFonteAgente.Agente = agenteDao.findById((Int64)gheFonteAgente.Agente.Id, dbConnection);

                    gheFonteAgente.GradSoma = gheFonteAgente.GradSoma == null ? null : gradSomaDao.findGradSomaById(gheFonteAgente.GradSoma, dbConnection);

                    gheFonteAgente.GradEfeito = gheFonteAgente.GradEfeito == null ? null : gradEfeitoDao.findGradEfeitoByGradEfeito(gheFonteAgente.GradEfeito, dbConnection);

                    gheFonteAgente.GradExposicao = gheFonteAgente.GradExposicao == null ? null : gradExposicaoDao.findGradExposicaoByGradExposicao(gheFonteAgente.GradExposicao, dbConnection);
                }

                return gheFonteAgente;

            }
            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findGheFonteAgenteById", ex);
                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }
            finally
            {
                FabricaConexao.fecharConexao();
            }
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public DataSet findAllAgenteByGheFonte(GheFonte gheFonte)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findAllAgenteByGheFonte");

            NpgsqlConnection dbConnection = FabricaConexao.getConexao();

            IGheFonteAgenteDao gheFonteAgenteDao = FabricaDao.getGheFonteAgenteDao();
            try
            {
                return gheFonteAgenteDao.findAllByGheFonte(gheFonte, dbConnection);

            }
            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findAllAgenteByGheFonte", ex);
                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }
            finally
            {
                FabricaConexao.fecharConexao();
            }
        }

        //[MethodImpl(MethodImplOptions.Synchronized)]
        //public ClienteFuncao insertClienteFuncaoDuplicate(ClienteFuncao clienteFuncao)
        //{
        //    LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método incluirClienteFuncao");

        //    NpgsqlConnection dbConnection = FabricaConexao.getConexao();
        //    IClienteFuncaoDao clienteFuncaoDao = FabricaDao.getClienteFuncaoDao();
        //    NpgsqlTransaction transaction = dbConnection.BeginTransaction();

        //    try
        //    {

        //        /* verificando se existe um cliente funcao cadastrado para o cliente e se está desativado.
        //         * caso exista, então esse cliente funcao deverá ser reativado, 
        //         * evitando problemas nos relacionamentos do pcmso.
        //         * 18/10/2015 - welbert santos */

        //        LinkedList<ClienteFuncao> funcaoInCliente = new LinkedList<ClienteFuncao>();
        //        funcaoInCliente = clienteFuncaoDao.findAllByFuncaoAndCliente(clienteFuncao.Funcao, clienteFuncao.Cliente, dbConnection);

        //        if (funcaoInCliente.Count > 0)
        //        {

        //            funcaoInCliente.ToList().ForEach(delegate(ClienteFuncao clienteFuncaoPesquisar)
        //            {
        //                if (string.Equals(clienteFuncaoPesquisar.Situacao, ApplicationConstants.DESATIVADO))
        //                {
        //                    clienteFuncaoPesquisar.Situacao = ApplicationConstants.ATIVO;
        //                    clienteFuncaoPesquisar.DataExclusao = null;
        //                    clienteFuncaoPesquisar.DataCadastro = DateTime.Now;
        //                    clienteFuncaoDao.update(clienteFuncaoPesquisar, dbConnection);
        //                }
        //            });
        //        }
        //        else
        //            clienteFuncaoDao.insert(clienteFuncao, dbConnection);

        //        transaction.Commit();

        //    }
        //    catch (DataBaseConectionException ex)
        //    {
        //        LogHelper.logger.Error(this.GetType().Name + " - incluirClienteFuncao", ex);
        //        transaction.Rollback();
        //        throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
        //    }
        //    finally
        //    {
        //        FabricaConexao.fecharConexao();
        //    }

        //    return clienteFuncao;
        //}

        [MethodImpl(MethodImplOptions.Synchronized)]
        public DataSet findAllClienteFuncao(Cliente cliente)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findAllClienteFuncao");

            NpgsqlConnection dbConnection = FabricaConexao.getConexao();

            IClienteFuncaoDao clienteFuncaoDao = FabricaDao.getClienteFuncaoDao();

            try
            {
                return clienteFuncaoDao.findAtivosByCliente(cliente, dbConnection);

            }
            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findAllClienteFuncao", ex);
                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }
            finally
            {
                FabricaConexao.fecharConexao();
            }
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public void updateClienteFuncao(ClienteFuncao clienteFuncao)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método deleteClienteFuncao");

            NpgsqlConnection dbConnection = FabricaConexao.getConexao();

            IClienteFuncaoDao clienteFuncaoDao = FabricaDao.getClienteFuncaoDao();

            NpgsqlTransaction transaction = dbConnection.BeginTransaction();

            try
            {
                clienteFuncaoDao.update(clienteFuncao, dbConnection);
                transaction.Commit();

            }
            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - deleteClienteFuncao", ex);
                transaction.Rollback();
                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }
            finally
            {
                FabricaConexao.fecharConexao();
            }
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public ClienteFuncao findClienteFuncaoById(Int64 id)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findClienteFuncaoById");

            NpgsqlConnection dbConnection = FabricaConexao.getConexao();

            IClienteFuncaoDao clienteFuncaoDao = FabricaDao.getClienteFuncaoDao();
            IClienteDao clienteDao = FabricaDao.getClienteDao();
            IFuncaoDao funcaoDao = FabricaDao.getFuncaoDao();

            ClienteFuncao clienteFuncaoRetorno = null;

            try
            {
                // buscando o cliente Funcao
                clienteFuncaoRetorno = clienteFuncaoDao.findById(id, dbConnection);

                // preenchendo o clienteFuncao já com os métodos prontos.
                clienteFuncaoRetorno.Cliente = clienteDao.findById((Int64)clienteFuncaoRetorno.Cliente.Id, dbConnection);

                // preenchendo o Funcao já com os métodos prontos.
                clienteFuncaoRetorno.Funcao = funcaoDao.findById((Int64)clienteFuncaoRetorno.Funcao.Id, dbConnection);

                return clienteFuncaoRetorno;

            }
            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findClienteFuncaoById", ex);
                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }
            finally
            {
                FabricaConexao.fecharConexao();
            }
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public DataSet findAllSetorByGhe(Ghe ghe)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findAllSetorByGhe");

            NpgsqlConnection dbConnection = FabricaConexao.getConexao();
            IGheSetorDao gheSetorDao = FabricaDao.getGheSetorDao();

            try
            {
                return gheSetorDao.findAllByGhe(ghe, dbConnection);

            }
            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findAllSetorByGhe", ex);
                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }
            finally
            {
                FabricaConexao.fecharConexao();
            }
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public Atividade insertAtividade(Atividade atividade)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método incluirAtividade");

            Atividade atividadeNova = null;
            NpgsqlConnection dbConnection = FabricaConexao.getConexao();
            IAtividadeDao atividadeDao = FabricaDao.getAtividadeDao();
            NpgsqlTransaction transaction = dbConnection.BeginTransaction();
            try
            {
                if (atividadeDao.findByDescricao(atividade.Descricao, dbConnection) != null)
                    throw new Exception("Já existe uma atividade com esse nome.");

                atividadeNova = atividadeDao.insert(atividade, dbConnection);

                transaction.Commit();
            }
            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - novaAtividade", ex);
                transaction.Rollback();
                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }
            finally
            {
                FabricaConexao.fecharConexao();
            }

            return atividade;
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public DataSet findAtividadeAtivosByDescricao(Atividade atividade)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findAtividadeAtivosByDescricao");

            NpgsqlConnection dbConnection = FabricaConexao.getConexao();
            IAtividadeDao atividadeDao = FabricaDao.getAtividadeDao();

            try
            {
                return atividadeDao.findByAtividade(atividade, dbConnection);

            }
            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findAtividadeAtivosByDescricao", ex);
                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }
            finally
            {
                FabricaConexao.fecharConexao();
            }
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public DataSet findAtividadeByFilter(Atividade atividade)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método FindAtividadeByFilter");

            NpgsqlConnection dbConnection = FabricaConexao.getConexao();
            IAtividadeDao atividadeDao = FabricaDao.getAtividadeDao();

            try
            {
                return atividadeDao.findByFilter(atividade, dbConnection);

            }
            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findAtividadeByFilter", ex);
                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }
            finally
            {
                FabricaConexao.fecharConexao();
            }
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public Atividade findAtividadeById(Int64 idAtividade)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findAtividadeById");

            NpgsqlConnection dbConnection = FabricaConexao.getConexao();
            IAtividadeDao atividadedao = FabricaDao.getAtividadeDao();
            try
            {
                Atividade atividade = atividadedao.findById(idAtividade, dbConnection);
                return atividade;

            }
            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findAtividadeById", ex);
                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }
            finally
            {
                FabricaConexao.fecharConexao();
            }
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public Atividade updateAtividade(Atividade atividade)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método alteraAtividade");

            NpgsqlConnection dbConnection = FabricaConexao.getConexao();
            IAtividadeDao atividadeDao = FabricaDao.getAtividadeDao();
            NpgsqlTransaction transaction = dbConnection.BeginTransaction();
            try
            {
                Atividade atividadeProcura = atividadeDao.findByDescricao(atividade.Descricao, dbConnection);

                if (atividadeProcura != null && atividadeProcura.Id != atividade.Id)
                    throw new Exception("Já existe uma atividade cadastrada com esse nome.");

                atividade = atividadeDao.update(atividade, dbConnection);

                transaction.Commit();
            }
            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - alter arAtividade", ex);
                transaction.Rollback();
                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }
            finally
            {
                FabricaConexao.fecharConexao();
            }

            return atividade;
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public Fonte insertFonte(Fonte fonte)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método incluirFonte");

            Fonte fonteNova = null;
            NpgsqlConnection dbConnection = FabricaConexao.getConexao();
            IFonteDao FonteDao = FabricaDao.getFonteDao();
            NpgsqlTransaction transaction = dbConnection.BeginTransaction();

            try
            {
                if (FonteDao.findByDescricao(fonte.Descricao, dbConnection) != null)
                    throw new Exception("Fonte com essa descrição já está cadastrada.");

                fonteNova = FonteDao.insert(fonte, dbConnection);

                transaction.Commit();
            }
            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - novaFonte", ex);
                transaction.Rollback();
                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }
            finally
            {
                FabricaConexao.fecharConexao();
            }

            return fonte;
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public DataSet findFonteByFilter(Fonte fonte)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método FindFonteByFilter");

            NpgsqlConnection dbConnection = FabricaConexao.getConexao();
            IFonteDao fontedao = FabricaDao.getFonteDao();

            try
            {
                return fontedao.findByFilter(fonte, dbConnection);

            }
            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findFonteByFilter", ex);
                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }
            finally
            {
                FabricaConexao.fecharConexao();
            }
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public Fonte findFonteById(Int64 idFonte)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findFonteById");

            NpgsqlConnection dbConnection = FabricaConexao.getConexao();
            IFonteDao fontedao = FabricaDao.getFonteDao();
            try
            {
                Fonte fonte = fontedao.findById(idFonte, dbConnection);
                return fonte;

            }
            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findFonteById", ex);
                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }
            finally
            {
                FabricaConexao.fecharConexao();
            }
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public Fonte updateFonte(Fonte fonte)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método alteraFonte");

            NpgsqlConnection dbConnection = FabricaConexao.getConexao();
            IFonteDao fonteDao = FabricaDao.getFonteDao();
            NpgsqlTransaction transaction = dbConnection.BeginTransaction();
            try
            {
                Fonte fonteProcura = fonteDao.findByDescricao(fonte.Descricao, dbConnection);

                if (fonteProcura != null && fonteProcura.Id != fonte.Id)
                    throw new Exception("Já existe uma fonte com essa descrição.");

                fonte = fonteDao.update(fonte, dbConnection);
                transaction.Commit();

            }
            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - alterarFonte", ex);
                transaction.Rollback();
                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }
            finally
            {
                FabricaConexao.fecharConexao();
            }

            return fonte;
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public Setor insertSetor(Setor setor)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método incluirSetor");

            Setor setorNovo = null;
            NpgsqlConnection dbConnection = FabricaConexao.getConexao();
            ISetorDao setorDao = FabricaDao.getSetorDao();
            NpgsqlTransaction transaction = dbConnection.BeginTransaction();
            try
            {
                setorNovo = setorDao.insert(setor, dbConnection);

                transaction.Commit();
            }
            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - novoSetor", ex);
                transaction.Rollback();
                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }
            finally
            {
                FabricaConexao.fecharConexao();
            }

            return setor;
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public Setor findSetorByDescricao(String descricao)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findSetorByDescricao");

            NpgsqlConnection dbConnection = FabricaConexao.getConexao();
            ISetorDao setorDao = FabricaDao.getSetorDao();

            try
            {
                Setor setor = setorDao.findSetorByDescricao(descricao, dbConnection);

                return setor;
            }
            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findSetorByDescricao", ex);
                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }
            finally
            {
                FabricaConexao.fecharConexao();
            }
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public DataSet findSetorByFilter(Setor setor)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método FindSetorByFilter");

            NpgsqlConnection dbConnection = FabricaConexao.getConexao();
            ISetorDao setordao = FabricaDao.getSetorDao();

            try
            {
                return setordao.findSetorByFilter(setor, dbConnection);

            }
            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findSetorByFilter", ex);
                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }
            finally
            {
                FabricaConexao.fecharConexao();
            }
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public Setor findSetorById(Int64 idSetor)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findSetorById");

            NpgsqlConnection dbConnection = FabricaConexao.getConexao();
            ISetorDao setordao = FabricaDao.getSetorDao();
            try
            {
                Setor setor = setordao.findSetorById(idSetor, dbConnection);
                return setor;

            }
            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findSetorById", ex);
                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }
            finally
            {
                FabricaConexao.fecharConexao();
            }
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public Setor updateSetor(Setor setorAlterar)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método updateSetor");

            Setor setorNovo = null;
            NpgsqlConnection dbConnection = FabricaConexao.getConexao();
            ISetorDao setorDao = FabricaDao.getSetorDao();
            NpgsqlTransaction transaction = dbConnection.BeginTransaction();
            try
            {
                setorNovo = setorDao.update(setorAlterar, dbConnection);
                transaction.Commit();
            }
            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - updateSetor", ex);
                transaction.Rollback();
                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }
            finally
            {
                FabricaConexao.fecharConexao();
            }

            return setorNovo;
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public DataSet findAllSetorAtivo(Setor setor, Ghe ghe, ClienteFuncao ClienteFuncao)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findAllSetorAtivo");

            NpgsqlConnection dbConnection = FabricaConexao.getConexao();
            ISetorDao setordao = FabricaDao.getSetorDao();

            try
            {
                return setordao.findAllSetorAtivo(setor, ghe, ClienteFuncao, dbConnection);

            }
            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findAllSetorAtivo", ex);
                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }
            finally
            {
                FabricaConexao.fecharConexao();
            }
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public DataSet findEpiByFilter(Epi epi)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findEpiByFilter");

            NpgsqlConnection dbConnection = FabricaConexao.getConexao();
            IEpiDao epidao = FabricaDao.getEpiDao();

            try
            {
                return epidao.findByFilter(epi, dbConnection);

            }
            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findEpiByFilter", ex);
                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }
            finally
            {
                FabricaConexao.fecharConexao();
            }
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public Epi insertEpi(Epi epi)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método incluirEpi");

            Epi epiNovo = null;
            NpgsqlConnection dbConnection = FabricaConexao.getConexao();
            IEpiDao epiDao = FabricaDao.getEpiDao();
            NpgsqlTransaction transaction = dbConnection.BeginTransaction();
            try
            {
                /* verificando se já existe um EPI cadastrado com essa descrição */

                if (epiDao.findByDescricao(epi.Descricao, dbConnection) != null)
                    throw new Exception("Já existe um EPI cadastrado com essa descriçao");

                epiNovo = epiDao.insert(epi, dbConnection);

                transaction.Commit();
            }
            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - novoEpi", ex);
                transaction.Rollback();
                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }
            finally
            {
                FabricaConexao.fecharConexao();
            }
            return epi;
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public Epi updateEpi(Epi epiAlterar)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método alteraEpi");

            Epi epiNovo = null;
            NpgsqlConnection dbConnection = FabricaConexao.getConexao();
            IEpiDao epiDao = FabricaDao.getEpiDao();
            NpgsqlTransaction transaction = dbConnection.BeginTransaction();
            try
            {
                /* verificando se não existe nenhum EPI já cadastrado com esse nome. */

                Epi epiProcurado = epiDao.findByDescricao(epiAlterar.Descricao, dbConnection);

                if (epiProcurado != null && epiProcurado.Id != epiAlterar.Id)
                    throw new Exception("Já existe um EPI cadastrado com essa descrição.");

                epiNovo = epiDao.update(epiAlterar, dbConnection);
                transaction.Commit();
            }
            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - alterarEpi", ex);
                transaction.Rollback();
                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }
            finally
            {
                FabricaConexao.fecharConexao();
            }

            return epiNovo;
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public Epi findEpiById(Int64 idEpi)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findEpiById");

            NpgsqlConnection dbConnection = FabricaConexao.getConexao();
            IEpiDao epiDao = FabricaDao.getEpiDao();
            try
            {
                Epi epi = epiDao.findById(idEpi, dbConnection);
                return epi;

            }
            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findEpiById", ex);
                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }
            finally
            {
                FabricaConexao.fecharConexao();
            }
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public DataSet findAgenteByFilter(Agente agente)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findAgenteByFilter");

            NpgsqlConnection dbConnection = FabricaConexao.getConexao();
            IAgenteDao agenteDao = FabricaDao.getAgenteDao();

            try
            {
                return agenteDao.findByFilter(agente, dbConnection);

            }
            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findAgenteByFilter", ex);
                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }
            finally
            {
                FabricaConexao.fecharConexao();
            }
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public Agente insertAgente(Agente agente)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método incluirAgente");

            Agente agenteNovo = null;
            NpgsqlConnection dbConnection = FabricaConexao.getConexao();
            IAgenteDao agenteDao = FabricaDao.getAgenteDao();
            NpgsqlTransaction transaction = dbConnection.BeginTransaction();
            try
            {
                /* verificando se existe um agente com a descrição utilizada */

                Agente agenteProcura = agenteDao.findByDescricao(agente.Descricao, dbConnection);

                if (agenteProcura != null)
                    throw new AgenteException(AgenteException.msg2);

                agenteNovo = agenteDao.insert(agente, dbConnection);

                transaction.Commit();
            }
            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - novoAgente", ex);
                transaction.Rollback();
                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }
            finally
            {
                FabricaConexao.fecharConexao();
            }
            return agente;
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public Agente findAgenteById(Int64 idAgente)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findAgenteById");

            NpgsqlConnection dbConnection = FabricaConexao.getConexao();
            IAgenteDao agenteDao = FabricaDao.getAgenteDao();
            try
            {
                Agente agente = agenteDao.findById(idAgente, dbConnection);
                return agente;

            }
            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findAgenteById", ex);
                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }
            finally
            {
                FabricaConexao.fecharConexao();
            }
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public Agente updateAgente(Agente agente)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método alteraAgente");

            NpgsqlConnection dbConnection = FabricaConexao.getConexao();
            IAgenteDao agenteDao = FabricaDao.getAgenteDao();
            NpgsqlTransaction transaction = dbConnection.BeginTransaction();

            try
            {
                /* verificando para saber se existe outro agente com essa descrição */

                Agente agenteProcura = agenteDao.findByDescricao(agente.Descricao, dbConnection);

                if (agenteProcura != null && (String.Equals(agenteProcura.Descricao.Trim(), agente.Descricao.Trim()) && agente.Id != agenteProcura.Id))
                    throw new AgenteException(AgenteException.msg2);

                agente = agenteDao.update(agente, dbConnection);
                transaction.Commit();
            }
            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - alterarAgente", ex);
                transaction.Rollback();
                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }
            finally
            {
                FabricaConexao.fecharConexao();
            }

            return agente;
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public DataSet findFuncaoByFilter(Funcao funcao)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método  FindFuncaoByFilter");

            NpgsqlConnection dbConnection = FabricaConexao.getConexao();
            IFuncaoDao funcaoDao = FabricaDao.getFuncaoDao();

            try
            {
                return funcaoDao.findByFilter(funcao, dbConnection);

            }
            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findFuncaoByFilter", ex);
                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }
            finally
            {
                FabricaConexao.fecharConexao();
            }
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public Funcao insertFuncao(Funcao funcao)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método incluirFuncao");

            Funcao funcaoNova = null;
            NpgsqlConnection dbConnection = FabricaConexao.getConexao();
            IFuncaoDao funcaoDao = FabricaDao.getFuncaoDao();
            IComentarioFuncaoDao comentarioFuncaoDao = FabricaDao.getComentarioFuncaoDao();
            NpgsqlTransaction transaction = dbConnection.BeginTransaction();

            try
            {

                /* verificando se nao existe uma função cadastrada com esse nome */

                if (funcaoDao.findByDescricao(funcao.Descricao, dbConnection) != null)
                    throw new Exception("Já existe uma função cadastrada com esse nome.");

                funcaoNova = funcaoDao.insert(funcao, dbConnection);

                /* incluindo as atividades da função */

                funcao.Atividades.ForEach(delegate(ComentarioFuncao comentarioFuncao)
                {
                    comentarioFuncao.Funcao = funcaoNova;
                    comentarioFuncaoDao.insert(comentarioFuncao, dbConnection);
                });

                transaction.Commit();
            }
            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - IncluirFuncao", ex);
                transaction.Rollback();
                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }
            finally
            {
                FabricaConexao.fecharConexao();
            }

            return funcao;
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public Funcao findFuncaoById(Int64 idFuncao)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findFuncaoById");

            NpgsqlConnection dbConnection = FabricaConexao.getConexao();
            IFuncaoDao funcaoDao = FabricaDao.getFuncaoDao();
            IComentarioFuncaoDao comentarioFuncaoDao = FabricaDao.getComentarioFuncaoDao();
            try
            {
                Funcao funcao = funcaoDao.findById(idFuncao, dbConnection);

                /* preenchendo atividades da função. */

                funcao.Atividades = comentarioFuncaoDao.findAtivosByFuncao(funcao, dbConnection);

                return funcao;

            }
            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findFuncaoById", ex);
                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }
            finally
            {
                FabricaConexao.fecharConexao();
            }
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public Funcao updateFuncao(Funcao funcao)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método alteraFuncao");

            Funcao funcaoNova = null;
            NpgsqlConnection dbConnection = FabricaConexao.getConexao();
            IFuncaoDao funcaoDao = FabricaDao.getFuncaoDao();
            NpgsqlTransaction transaction = dbConnection.BeginTransaction();
            try
            {
                /* verificando se já existe uma função cadastrada com esse nome */

                Funcao funcaoProcura = funcaoDao.findByDescricao(funcao.Descricao, dbConnection);

                if (funcaoProcura != null && funcaoProcura.Id != funcao.Id)
                    throw new Exception("Já existe uma função cadastrada com esse nome.");

                funcaoNova = funcaoDao.update(funcao, dbConnection);
                transaction.Commit();
            }
            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - alterarFuncao", ex);
                transaction.Rollback();
                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }
            finally
            {
                FabricaConexao.fecharConexao();
            }

            return funcaoNova;
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public DataSet findNotaByFilter(Nota nota)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findNormaByFilter");

            NpgsqlConnection dbConnection = FabricaConexao.getConexao();

            INotaDao normaDao = FabricaDao.getNormaDao();

            try
            {
                return normaDao.findByFilter(nota, dbConnection);

            }
            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findNormaByFilter", ex);
                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }
            finally
            {
                FabricaConexao.fecharConexao();
            }
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public DataSet findNotaGheByFilter(Nota nota, Ghe ghe)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findNormaGheByFilter");

            NpgsqlConnection dbConnection = FabricaConexao.getConexao();

            INotaDao normaDao = FabricaDao.getNormaDao();

            try
            {
                return normaDao.findByNotaGhe(nota, ghe, dbConnection);

            }
            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findNormaGheByFilter", ex);
                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }
            finally
            {
                FabricaConexao.fecharConexao();
            }
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public Nota insertNota(Nota nota)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método incluirNorma");

            Nota novaNorma = null;
            NpgsqlConnection dbConnection = FabricaConexao.getConexao();
            INotaDao normaDao = FabricaDao.getNormaDao();
            NpgsqlTransaction transaction = dbConnection.BeginTransaction();

            try
            {

                if (normaDao.findByDescricao(nota, dbConnection) != null)
                    throw new AtividadeJaCadastradaException(NormaJaCadastradaException.msg);

                novaNorma = normaDao.insert(nota, dbConnection);
                transaction.Commit();

            }
            catch (DataBaseConectionException ex)
            {

                transaction.Rollback();
                LogHelper.logger.Error(this.GetType().Name + " - incluirNorma", ex);
                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);

            }
            finally
            {
                FabricaConexao.fecharConexao();
            }

            return nota;
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public Nota findNotaById(Int64 id)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findNormaById");

            NpgsqlConnection dbConnection = FabricaConexao.getConexao();

            INotaDao normaDao = FabricaDao.getNormaDao();

            try
            {
                return normaDao.findById(id, dbConnection);

            }
            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findNormaById", ex);
                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }
            finally
            {
                FabricaConexao.fecharConexao();
            }
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public Nota updateNota(Nota nota)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método alterarNorma");

            NpgsqlConnection dbConnection = FabricaConexao.getConexao();
            INotaDao normaDao = FabricaDao.getNormaDao();
            NpgsqlTransaction transaction = dbConnection.BeginTransaction();

            try
            {

                Nota buscaNota = normaDao.findByDescricao(nota, dbConnection);

                if (buscaNota != null && buscaNota.Id != nota.Id)
                    throw new NormaJaCadastradaException(NormaJaCadastradaException.msg);

                if (normaDao.findInGheEstudo(nota, dbConnection))
                    throw new NormaJaUtilizadaException(NormaJaUtilizadaException.msg);

                nota = normaDao.update(nota, dbConnection);
                transaction.Commit();

            }
            catch (DataBaseConectionException ex)
            {

                transaction.Rollback();
                LogHelper.logger.Error(this.GetType().Name + " - incluirNorma", ex);
                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);

            }
            finally
            {
                FabricaConexao.fecharConexao();
            }

            return nota;
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public void montaQuadroVersao(Int64 idPpra)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método montaQuadroRevisao"); ;

            NpgsqlConnection dbConnection = FabricaConexao.getConexao();
            NpgsqlTransaction transaction = dbConnection.BeginTransaction();
            IEstudoDao ppraDao = FabricaDao.getEstudoDao();
            IRevisaoDao revisaoDao = FabricaDao.getRevisaoDao();
            LinkedList<Revisao> revisoes = new LinkedList<Revisao>();
            Revisao revisao = null;

            try
            {
                ppraDao.limpaVersao(idPpra, dbConnection);

                revisao = revisaoDao.findRevisao(idPpra, dbConnection);

                while (true)
                {
                    if (revisao.Estudo.Id == revisao.EstudoRevisao.Id)
                    {
                        revisoes.AddLast(revisao);
                        break;
                    }
                    else
                    {
                        revisoes.AddLast(revisao);
                        revisao = revisaoDao.findRevisao(revisao.EstudoRevisao.Id, dbConnection);
                    }
                }

                // Gravando versões no quadro.
                foreach (Revisao revisaoEncontrada in revisoes)
                {
                    revisaoEncontrada.Estudo.Id = idPpra;

                    if (!revisaoEncontrada.IndCorrecao)
                    {
                        ppraDao.gravaVersao(new Versao(null, revisaoEncontrada.Estudo, revisaoEncontrada.VersaoEstudo, revisaoEncontrada.Comentario, revisaoEncontrada.DataRevisao), dbConnection);
                    }
                }

                transaction.Commit();
            }
            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - montaQuadroRevisao", ex);
                transaction.Rollback();
                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }
            finally
            {
                FabricaConexao.fecharConexao();
            }
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public DataSet findRisco()
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findRisco");

            NpgsqlConnection dbConnection = FabricaConexao.getConexao();

            IRiscoDao riscoDao = FabricaDao.getRiscoDao();

            try
            {
                return riscoDao.findRisco(dbConnection);

            }
            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findRisco", ex);
                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }
            finally
            {
                FabricaConexao.fecharConexao();
            }
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public Boolean findGheFonteAgenteByAgente(Agente agente)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findGheFonteAgenteByAgente");

            NpgsqlConnection dbConnection = FabricaConexao.getConexao();

            IGheFonteAgenteDao gheFonteAgenteDao = FabricaDao.getGheFonteAgenteDao();

            try
            {
                return gheFonteAgenteDao.isUsedByAgente(agente, dbConnection);

            }
            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findGheFonteAgenteByAgente", ex);
                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }
            finally
            {
                FabricaConexao.fecharConexao();
            }
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public DataSet findAllNotasByGhe(Ghe ghe)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findAllNotasByGhe");

            NpgsqlConnection dbConnection = FabricaConexao.getConexao(); // criando conexao com o banco de dados.

            IGheNotaDao gheNormaDao = FabricaDao.getGheNormaDao();

            try
            {
                return gheNormaDao.findAllByGheDataSet(ghe, dbConnection);

            }
            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findAllNotasByGhe", ex);
                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }
            finally
            {
                FabricaConexao.fecharConexao();
            }

        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public DataSet findAllGheSetorClienteFuncaoByGheSetor(GheSetor gheSetor)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findAllGheSetorClienteFuncaoByGheSetor");

            NpgsqlConnection dbConnection = FabricaConexao.getConexao();

            IGheSetorClienteFuncaoDao gheSetorClienteFuncaoDao = FabricaDao.getGheSetorClienteFuncaoDao();

            try
            {
                return gheSetorClienteFuncaoDao.findAllByGheSetor(gheSetor, dbConnection);

            }
            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findAllGheSetorClienteFuncaoByGheSetor", ex);
                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }
            finally
            {
                FabricaConexao.fecharConexao();
            }
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public List<ComentarioFuncao> findAllComentarioFuncaoAtivoByFuncao(Funcao funcao)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findAllComentarioFuncaoAtivoByFuncao");

            NpgsqlConnection dbConnection = FabricaConexao.getConexao();

            IComentarioFuncaoDao comentarioFuncaoDao = FabricaDao.getComentarioFuncaoDao();

            try
            {
                return comentarioFuncaoDao.findAtivosByFuncao(funcao, dbConnection);

            }
            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findAllComentarioFuncaoAtivoByFuncao", ex);
                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }
            finally
            {
                FabricaConexao.fecharConexao();
            }
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public ComentarioFuncao insertComentarioFuncao(ComentarioFuncao comentarioFuncao)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método incluirComentarioFuncao");

            NpgsqlConnection dbConnection = FabricaConexao.getConexao();

            IComentarioFuncaoDao comentarioFuncaoDao = FabricaDao.getComentarioFuncaoDao();

            NpgsqlTransaction transaction = dbConnection.BeginTransaction();

            ComentarioFuncao comentarioFuncaoNovo = null;

            try
            {
                comentarioFuncaoNovo = comentarioFuncaoDao.insert(comentarioFuncao, dbConnection);
                transaction.Commit();
            }
            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - incluirComentarioFuncao", ex);
                transaction.Rollback();
                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }
            finally
            {
                FabricaConexao.fecharConexao();
            }

            return comentarioFuncaoNovo;

        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public void updateComentarioFuncao(ComentarioFuncao comentarioFuncao)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método updateComentarioFuncao");

            NpgsqlConnection dbConnection = FabricaConexao.getConexao();

            IComentarioFuncaoDao comentarioFuncaoDao = FabricaDao.getComentarioFuncaoDao();

            NpgsqlTransaction transaction = dbConnection.BeginTransaction();

            try
            {
                comentarioFuncaoDao.update(comentarioFuncao, dbConnection);
                transaction.Commit();
            }
            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - updateComentarioFuncao", ex);
                transaction.Rollback();
                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }
            finally
            {
                FabricaConexao.fecharConexao();
            }

        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public DataSet findAllEstimativaNotInEstudo(Estudo estudo, Estimativa estimativa)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findAllEstimativaNotInEstudo");

            NpgsqlConnection dbConnection = FabricaConexao.getConexao();

            IEstimativaDao estimativaDao = FabricaDao.getEstimativaDao();

            try
            {
                return estimativaDao.findAllNotInEstudo(estudo, estimativa, dbConnection);
            }
            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findAllEstimativaNotInEstudo", ex);
                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }
            finally
            {
                FabricaConexao.fecharConexao();
            }

        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public EstimativaEstudo insertEstimativaEstudo(EstimativaEstudo estimativaEstudo)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método insertEstimativaEstudo");

            NpgsqlConnection dbConnection = FabricaConexao.getConexao();

            IEstimativaEstudoDao estimativaEstudoDao = FabricaDao.getEstimativaEstudoDao();

            NpgsqlTransaction transaction = dbConnection.BeginTransaction();

            try
            {
                EstimativaEstudo estimativaEstudoNew = estimativaEstudoDao.insert(estimativaEstudo, dbConnection);
                transaction.Commit();
                return estimativaEstudoNew;

            }
            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - insertEstimativaEstudo", ex);
                transaction.Rollback();
                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }
            finally
            {
                FabricaConexao.fecharConexao();
            }

        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public DataSet findAllEstimativaInEstudo(Estudo estudo)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findAllEstimativaInEstudo");

            NpgsqlConnection dbConnection = FabricaConexao.getConexao();

            IEstimativaEstudoDao estimativaEstudoDao = FabricaDao.getEstimativaEstudoDao();

            try
            {
                return estimativaEstudoDao.findAllByEstudo(estudo, dbConnection);
            }
            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findAllEstimativaInEstudo", ex);
                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }
            finally
            {
                FabricaConexao.fecharConexao();
            }

        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public EstimativaEstudo updateEstimativaEstudo(EstimativaEstudo estimativaEstudo)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método updateEstimaEstudo");

            NpgsqlConnection dbConnection = FabricaConexao.getConexao();

            IEstimativaEstudoDao estimativaEstudoDao = FabricaDao.getEstimativaEstudoDao();

            NpgsqlTransaction transaction = dbConnection.BeginTransaction();

            try
            {
                EstimativaEstudo estimativaEstudoUpdate = estimativaEstudoDao.update(estimativaEstudo, dbConnection);
                transaction.Commit();
                return estimativaEstudoUpdate;

            }
            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - updateEstimaEstudo", ex);
                transaction.Rollback();
                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }
            finally
            {
                FabricaConexao.fecharConexao();
            }

        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public void deleteEstimativaEstudo(EstimativaEstudo estimativaEstudo)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método deleteEstimativaEstudo");

            NpgsqlConnection dbConnection = FabricaConexao.getConexao();

            IEstimativaEstudoDao estimativaEstudoDao = FabricaDao.getEstimativaEstudoDao();

            NpgsqlTransaction transaction = dbConnection.BeginTransaction();

            try
            {
                estimativaEstudoDao.delete(estimativaEstudo, dbConnection);
                transaction.Commit();

            }
            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - deleteEstimativaEstudo", ex);
                transaction.Rollback();
                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }
            finally
            {
                FabricaConexao.fecharConexao();
            }

        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public List<CnaeEstudo> findAllCnaEstudoByEstudo(Estudo pcmso)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findAllCnaEstudoByEstudo");

            NpgsqlConnection dbConnection = FabricaConexao.getConexao();
            List<CnaeEstudo> cnaes = new List<CnaeEstudo>();

            ICnaeEstudoDao cnaeEstudoDao = FabricaDao.getCnaeEstudoDao();
            IClienteDao clienteDao = FabricaDao.getClienteDao();
            
            try
            {
                cnaes = cnaeEstudoDao.findAllByPcmso(pcmso, dbConnection);

                cnaes.ForEach(delegate(CnaeEstudo cnaeEstudo) {
                    cnaeEstudo.Cnae = clienteDao.findCnaeById((long)cnaeEstudo.Cnae.Id, dbConnection);
                });

                return cnaes;
            }
            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findAllCnaEstudoByEstudo", ex);
                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }
            finally
            {
                FabricaConexao.fecharConexao();
            }

        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public void deleteCnaeEstudo(CnaeEstudo cnaeEstudo)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método deleteCnaeEstudo");

            NpgsqlConnection dbConnection = FabricaConexao.getConexao();

            ICnaeEstudoDao cnaeEstudoDao = FabricaDao.getCnaeEstudoDao();

            NpgsqlTransaction transaction = dbConnection.BeginTransaction();

            try
            {
                cnaeEstudoDao.delete(cnaeEstudo, dbConnection);
                transaction.Commit();

            }
            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - deleteCnaeEstudo", ex);
                transaction.Rollback();
                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }
            finally
            {
                FabricaConexao.fecharConexao();
            }
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public CnaeEstudo insertCnaeEstudo(CnaeEstudo cnaeEstudo)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método insertCnaeEstudo");

            NpgsqlConnection dbConnection = FabricaConexao.getConexao();

            ICnaeEstudoDao cnaeEstudoDao = FabricaDao.getCnaeEstudoDao();

            NpgsqlTransaction transaction = dbConnection.BeginTransaction();

            try
            {
                cnaeEstudo = cnaeEstudoDao.insert(cnaeEstudo, dbConnection);
                transaction.Commit();
                return cnaeEstudo;

            }
            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - insertCnaeEstudo", ex);
                transaction.Rollback();
                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }
            finally
            {
                FabricaConexao.fecharConexao();
            }
        }
    }

}
