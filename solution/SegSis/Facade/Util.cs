﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.CompilerServices;
using SWS.Entidade;
using SWS.Helper;
using Npgsql;
using SWS.Dao;
using SWS.IDao;
using SWS.Excecao;

namespace SWS.Facade
{
    class Util
    {
        
        public static Util util = null;

        private Util() { }

        public static Util getInstance()
        {
            if (util == null)
            {
                util = new Util();
            }

            return util;
        }
        
        public static string getMD5Hash(string input)
        {
            System.Security.Cryptography.MD5 md5 = System.Security.Cryptography.MD5.Create();
            byte[] inputBytes = System.Text.Encoding.ASCII.GetBytes(input);
            byte[] hash = md5.ComputeHash(inputBytes);
            System.Text.StringBuilder sb = new System.Text.StringBuilder();

            for (int i = 0; i < hash.Length; i++)
            {
                sb.Append(hash[i].ToString("X2"));
            }

            return sb.ToString();
        }


        
        
    }
}
