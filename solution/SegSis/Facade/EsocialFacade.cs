﻿using Npgsql;
using SWS.Dao;
using SWS.Entidade;
using SWS.Excecao;
using SWS.Helper;
using SWS.IDao;
using SWS.View.Resources;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;

namespace SWS.Facade
{
    class EsocialFacade
    {
        public static EsocialFacade esocialFacade = null;

        private EsocialFacade() { }

        public static EsocialFacade getInstance()
        {
            if (esocialFacade == null)
            {
                esocialFacade = new EsocialFacade();
            }

            return esocialFacade;
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public DataSet findEsocialByFilter(LoteEsocial loteEsocial, DateTime? dataCriacaoInicial, DateTime? dataCriacaoFinal)
        {

            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findEsocialByFilter");

            NpgsqlConnection dbConnection = FabricaConexao.getConexao();

            ILoteEsocialDao loteEsocialDao = FabricaDao.getLoteEsocialDao();

            try
            {
                return loteEsocialDao.findByFilter(loteEsocial, dataCriacaoInicial, dataCriacaoFinal, dbConnection);

            }
            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findEsocialByFilter", ex);
                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }
            finally
            {
                FabricaConexao.fecharConexao();
            }
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public DataSet findAtendimentosByLote(LoteEsocial loteEsocial)
        {

            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findAtendimentosByLote");

            NpgsqlConnection dbConnection = FabricaConexao.getConexao();

            IAsoDao asoDao = FabricaDao.getAsoDao();

            try
            {
                return asoDao.findAtendimentosByLoteEsocial(loteEsocial, dbConnection);

            }
            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findAtendimentosByLote", ex);
                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }
            finally
            {
                FabricaConexao.fecharConexao();
            }
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public List<Aso> findAtendimentosByLoteList(LoteEsocial loteEsocial)
        {

            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findAtendimentosByLoteList");

            NpgsqlConnection dbConnection = FabricaConexao.getConexao();

            IAsoDao asoDao = FabricaDao.getAsoDao();
            IMedicoDao medicoDao = FabricaDao.getMedicoDao();
            IClienteFuncaoFuncionarioDao clienteFuncaoFuncioanario = FabricaDao.getClienteFuncaoFuncionarioDao();
            IPeriodicidadeDao periodicidadeDao = FabricaDao.getPeriodicidadeDao();
            
            List<Aso> atendimentos = new List<Aso>();


            try
            {

                atendimentos = asoDao.findAtendimentosByLoteEsocialList(loteEsocial, dbConnection);

                /* preenchendo com outras informações */
                foreach (Aso atendimento in atendimentos)
                {
                    /* medico examinador */
                    atendimento.Examinador = atendimento.Examinador != null
                        ? medicoDao.findById((long)atendimento.Examinador.Id, dbConnection) 
                        : null;

                    /* cliente funcaoFuncionario */
                    /* armazenando a matricula do funcionario */
                    //string matricula = atendimento.ClienteFuncaoFuncionario.Matricula;
                    atendimento.ClienteFuncaoFuncionario = atendimento.ClienteFuncaoFuncionario != null 
                        ? clienteFuncaoFuncioanario.findById((long)atendimento.ClienteFuncaoFuncionario.Id, dbConnection) 
                        : null;
                    //atendimento.ClienteFuncaoFuncionario.Matricula = matricula;

                    /* tipo de atendimento */
                    atendimento.Periodicidade = periodicidadeDao.findById((long)atendimento.Periodicidade.Id, dbConnection);

                    /* medico coordenador */
                    atendimento.Coordenador = atendimento.Coordenador != null
                        ? medicoDao.findById((long)atendimento.Coordenador.Id, dbConnection)
                        : null;
                }


            }
            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findAtendimentosByLoteList", ex);
                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }
            finally
            {
                FabricaConexao.fecharConexao();
            }
            return atendimentos;
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public List<LoteEsocial> gerarLotes(List<long[]> listaAtendimentos, Cliente cliente)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método gerarLotes");

            NpgsqlConnection dbConnection = FabricaConexao.getConexao();
            NpgsqlTransaction transaction = dbConnection.BeginTransaction();
            ILoteEsocialDao loteEsocialDao = new LoteEsocialDao();
            LoteEsocial loteEsocial = null;
            List<LoteEsocial> lotesGerados = new List<LoteEsocial>();
            try
            {
                foreach (long[] idAtendimentos in listaAtendimentos)
                {
                    //Gerar lote
                    loteEsocial = new LoteEsocial();
                    loteEsocial.DataCriacao = DateTime.Now;
                    loteEsocial.cliente = cliente;
                    loteEsocial.Tipo = "2220";
                    loteEsocial = loteEsocialDao.incluirLoteEsocial(loteEsocial, dbConnection);

                    LoteEsocialAso loteEsocialAso = null;
                    for (int i = 0; i < idAtendimentos.Length; i++)
                    {
                        loteEsocialAso = new LoteEsocialAso();
                        loteEsocialAso.idAso = idAtendimentos[i];
                        loteEsocialAso.idLoteEsocialAso = loteEsocial.Id;
                        loteEsocialAso.situacao = ApplicationConstants.LOTE_ENVIADO;
                        
                        //Vincular atendimentos ao lote
                        loteEsocialAso = loteEsocialDao.incluirLoteEsocialAso(loteEsocialAso, dbConnection);
                    }

                    lotesGerados.Add(loteEsocial);
                }

                transaction.Commit();
            }
            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - gerarLotes", ex);
                transaction.Rollback();
                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }
            finally
            {
                FabricaConexao.fecharConexao();
            }

            return lotesGerados;
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public List<ProcedimentoEsocial> findAllProcedimentosByFilter(string textString)
        {

            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findAllProcedimentos");

            NpgsqlConnection dbConnection = FabricaConexao.getConexao();

            IProcedimentoEsocialDao procedimentoEsocialDao = FabricaDao.getProcedimentoEsocialDao();

            try
            {
                return procedimentoEsocialDao.findAllByFilter(textString, dbConnection);

            }
            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findAllProcedimentos", ex);
                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }
            finally
            {
                FabricaConexao.fecharConexao();
            }
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public List<string> cpfDuplicadosNoLote(LoteEsocial esocialLote)
        {

            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método cpfDuplicadosNoLote");

            NpgsqlConnection dbConnection = FabricaConexao.getConexao();

            ILoteEsocialDao loteEsocialDao = FabricaDao.getLoteEsocialDao();

            try
            {
                return loteEsocialDao.cpfDuplicadosNoLote(esocialLote, dbConnection);

            }
            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - cpfDuplicadosNoLote", ex);
                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }
            finally
            {
                FabricaConexao.fecharConexao();
            }
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public LoteEsocial findLoteEsocialById(long id)
        {

            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findLoteEsocialById");

            NpgsqlConnection dbConnection = FabricaConexao.getConexao();

            ILoteEsocialDao loteEsocialDao = FabricaDao.getLoteEsocialDao();
            IClienteDao clienteDao = FabricaDao.getClienteDao();

            try
            {
                LoteEsocial loteEsocial = loteEsocialDao.findById(id, dbConnection);
                /* preenchendo informação do cliente */
                loteEsocial.cliente = clienteDao.findById((long)loteEsocial.cliente.Id, dbConnection);
                return loteEsocial;

            }
            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findLoteEsocialById", ex);
                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }
            finally
            {
                FabricaConexao.fecharConexao();
            }
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public void deleteEsocialLote(LoteEsocial esocialLote)
        {

            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método deleteEsociaLote");

            NpgsqlConnection dbConnection = FabricaConexao.getConexao();
            NpgsqlTransaction transaction = dbConnection.BeginTransaction();

            ILoteEsocialDao loteEsocialDao = FabricaDao.getLoteEsocialDao();

            try
            {
                loteEsocialDao.delete(esocialLote, dbConnection);
                transaction.Commit();
            }
            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - deleteEsociaLote", ex);
                transaction.Rollback();
                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }
            finally
            {
                FabricaConexao.fecharConexao();
            }
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public List<ComplementoAgenteEsocial> findAllComplementoAgenteEsocialByFilter(ComplementoAgenteEsocial complementoAgenteEsocial)
        {

            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findAllProcedimentos");

            NpgsqlConnection dbConnection = FabricaConexao.getConexao();

            IComplementoAgenteEsocialDao complementoAgenteEsocialDao = FabricaDao.getComplementoEsocialDao();

            try
            {
                return complementoAgenteEsocialDao.findAllByFilter(complementoAgenteEsocial, dbConnection);

            }
            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findAllProcedimentos", ex);
                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }
            finally
            {
                FabricaConexao.fecharConexao();
            }
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public Monitoramento insertMonitoramento(Monitoramento monitoramento)
        {

            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método insertMonitoramento");

            NpgsqlConnection dbConnection = FabricaConexao.getConexao();
            NpgsqlTransaction transaction = dbConnection.BeginTransaction();

            IMonitoramentoDao monitoramentoDao = FabricaDao.getMonitoramentoDao();
            Monitoramento monitoramentoNew = null;

            try
            {
                
                monitoramentoNew = monitoramentoDao.insert(monitoramento, dbConnection);
                transaction.Commit();

                return monitoramentoNew;
            }
            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - insertMonitoramento", ex);
                transaction.Rollback();
                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }
            finally
            {
                FabricaConexao.fecharConexao();
            }
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public Monitoramento updateMonitoramento(Monitoramento monitoramento)
        {

            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método updateMonitoramento");

            NpgsqlConnection dbConnection = FabricaConexao.getConexao();
            NpgsqlTransaction transaction = dbConnection.BeginTransaction();

            IMonitoramentoDao monitoramentoDao = FabricaDao.getMonitoramentoDao();

            try
            {

                monitoramentoDao.update(monitoramento, dbConnection);
                transaction.Commit();

                return monitoramento;
            }
            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - updateMonitoramento", ex);
                transaction.Rollback();
                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }
            finally
            {
                FabricaConexao.fecharConexao();
            }
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public Monitoramento findMonitoramentoById(long id)
        {

            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findMonitoramentoById");

            NpgsqlConnection dbConnection = FabricaConexao.getConexao();
            IMonitoramentoDao monitoramentoDao = FabricaDao.getMonitoramentoDao();
            IClienteFuncaoFuncionarioDao clienteFuncaoFuncionarioDao = FabricaDao.getClienteFuncaoFuncionarioDao();
            IUsuarioDao usuarioDao = FabricaDao.getUsuarioDao();
            IFuncionarioDao funcionarioDao = FabricaDao.getFuncionarioDao();
            IClienteDao clienteDao = FabricaDao.getClienteDao();
            IEstudoDao estudoDao = FabricaDao.getEstudoDao();
            IClienteFuncionarioDao clienteFuncionarioDao = FabricaDao.getclienteFuncionarioDao();

            Monitoramento monitoramento;

            try
            {
                monitoramento = monitoramentoDao.findById(id, dbConnection);
                /* clienteFuncaoFuncionario */

                if (monitoramento.ClienteFuncaoFuncionario != null)
                {
                    monitoramento.ClienteFuncaoFuncionario = clienteFuncaoFuncionarioDao.findById((long)monitoramento.ClienteFuncaoFuncionario.Id, dbConnection);
                    monitoramento.ClienteFuncaoFuncionario.Funcionario = funcionarioDao.findFuncionarioById((long)monitoramento.ClienteFuncaoFuncionario.Funcionario.Id, dbConnection);
                    monitoramento.ClienteFuncaoFuncionario.ClienteFuncao.Cliente = clienteDao.findById((long)monitoramento.ClienteFuncaoFuncionario.ClienteFuncao.Cliente.Id, dbConnection);
                }

                /* informações do cliente funcionario */

                if (monitoramento.ClienteFuncionario != null)
                {
                    monitoramento.ClienteFuncionario = clienteFuncionarioDao.findById((long)monitoramento.ClienteFuncionario.Id, dbConnection);
                    monitoramento.ClienteFuncionario.Funcionario = funcionarioDao.findFuncionarioById((long)monitoramento.ClienteFuncionario.Funcionario.Id, dbConnection);
                    monitoramento.ClienteFuncionario.Cliente = clienteDao.findById((long)monitoramento.ClienteFuncionario.Cliente.Id, dbConnection);
                }

                /* informação do usuário responsável */
                monitoramento.UsuarioResponsavel = usuarioDao.findById((long)monitoramento.UsuarioResponsavel.Id, dbConnection);

                /* informações do estudo */
                monitoramento.Estudo = estudoDao.findById((long)monitoramento.Estudo.Id, dbConnection);


                return monitoramento;
            }
            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findMonitoramentoById", ex);
                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }
            finally
            {
                FabricaConexao.fecharConexao();
            }
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public void deleteMonitoramento(Monitoramento monitoramento)
        {

            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método deleteMonitoramento");

            NpgsqlConnection dbConnection = FabricaConexao.getConexao();
            NpgsqlTransaction transaction = dbConnection.BeginTransaction();

            IMonitoramentoDao monitoramentoDao = FabricaDao.getMonitoramentoDao();

            try
            {

                monitoramentoDao.delete(monitoramento, dbConnection);
                transaction.Commit();

            }
            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - deleteMonitoramento", ex);
                transaction.Rollback();
                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }
            finally
            {
                FabricaConexao.fecharConexao();
            }
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public MonitoramentoGheFonteAgente insertMonitoramentoGheFonteAgente(MonitoramentoGheFonteAgente monitoramentoGheFonteAgente)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método insertMonitoramentoGheFonteAgente");

            NpgsqlConnection dbConnection = FabricaConexao.getConexao();
            NpgsqlTransaction transaction = dbConnection.BeginTransaction();

            IMonitoramentoGheFonteAgenteDao monitoramentoGheFonteAgenteDao = FabricaDao.getMonitoramentoGheFonteAgenteDao();
            IEpiMonitoramentoDao epiMonitoramentoDao = FabricaDao.getEpiMonitoramentoDao();
            MonitoramentoGheFonteAgente monitoramentoGheFonteAgenteNew = null;

            try
            {
                monitoramentoGheFonteAgenteNew = monitoramentoGheFonteAgenteDao.insert(monitoramentoGheFonteAgente, dbConnection);

                /* incluindo a coleção de epis */

                foreach (EpiMonitoramento epi in monitoramentoGheFonteAgente.MonitoramentoEpis)
                {
                    epi.MonitoramentoGheFonteAgente = monitoramentoGheFonteAgenteNew;
                    epiMonitoramentoDao.insert(epi, dbConnection);
                }

                transaction.Commit();

                return monitoramentoGheFonteAgente;
            }
            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - insertMonitoramentoGheFonteAgente", ex);
                transaction.Rollback();
                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }
            finally
            {
                FabricaConexao.fecharConexao();
            }
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public MonitoramentoGheFonteAgente updateMonitoramentoGheFonteAgente(MonitoramentoGheFonteAgente monitoramentoGheFonteAgente)
        {

            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método updateMonitoramentoGheFonteAgente");

            NpgsqlConnection dbConnection = FabricaConexao.getConexao();
            NpgsqlTransaction transaction = dbConnection.BeginTransaction();

            IMonitoramentoGheFonteAgenteDao monitoramentoGheFonteAgenteDao = FabricaDao.getMonitoramentoGheFonteAgenteDao();

            try
            {

                monitoramentoGheFonteAgenteDao.update(monitoramentoGheFonteAgente, dbConnection);
                transaction.Commit();

                return monitoramentoGheFonteAgente;
            }
            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - updateMonitoramentoGheFonteAgente", ex);
                transaction.Rollback();
                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }
            finally
            {
                FabricaConexao.fecharConexao();
            }
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public MonitoramentoGheFonteAgente findMonitoramentoGheFonteAgenteById(long id)
        {

            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findMonitoramentoById");

            NpgsqlConnection dbConnection = FabricaConexao.getConexao();
            IMonitoramentoGheFonteAgenteDao monitoramentoGheFonteAgenteDao = FabricaDao.getMonitoramentoGheFonteAgenteDao();
            IMonitoramentoDao monitoramentoDao = FabricaDao.getMonitoramentoDao();
            IClienteFuncaoFuncionarioDao clienteFuncaoFuncionarioDao = FabricaDao.getClienteFuncaoFuncionarioDao();
            IUsuarioDao usuarioDao = FabricaDao.getUsuarioDao();
            IFuncionarioDao funcionarioDao = FabricaDao.getFuncionarioDao();
            IClienteDao clienteDao = FabricaDao.getClienteDao();
            IGheFonteAgenteDao gheFonteAgenteDao = FabricaDao.getGheFonteAgenteDao();
            IEstudoDao estudoDao = FabricaDao.getEstudoDao();

            MonitoramentoGheFonteAgente monitoramentoGheFonteAgente;

            try
            {
                monitoramentoGheFonteAgente = monitoramentoGheFonteAgenteDao.findById(id, dbConnection);
                monitoramentoGheFonteAgente.Monitoramento = monitoramentoDao.findById((long)monitoramentoGheFonteAgente.Monitoramento.Id, dbConnection);
                
                /* clienteFuncionario do monitoramento */
                monitoramentoGheFonteAgente.Monitoramento.ClienteFuncaoFuncionario = clienteFuncaoFuncionarioDao.findById((long)monitoramentoGheFonteAgente.Monitoramento.ClienteFuncaoFuncionario.Id, dbConnection);
                /* funcionario do clienteFuncionario do monitoramento */
                monitoramentoGheFonteAgente.Monitoramento.ClienteFuncaoFuncionario.Funcionario = funcionarioDao.findFuncionarioById((long)monitoramentoGheFonteAgente.Monitoramento.ClienteFuncaoFuncionario.Funcionario.Id, dbConnection);
                /* cliente do clienteFuncionario do monitoramento */
                monitoramentoGheFonteAgente.Monitoramento.ClienteFuncaoFuncionario.ClienteFuncao.Cliente = clienteDao.findById((long)monitoramentoGheFonteAgente.Monitoramento.ClienteFuncaoFuncionario.ClienteFuncao.Cliente.Id, dbConnection);
                /* usuário responsável do monitoramento */
                monitoramentoGheFonteAgente.Monitoramento.UsuarioResponsavel = usuarioDao.findById((long)monitoramentoGheFonteAgente.Monitoramento.UsuarioResponsavel.Id, dbConnection);

                /* informação do gheFonteAgente */
                monitoramentoGheFonteAgente.GheFonteAgente = gheFonteAgenteDao.findById((long)monitoramentoGheFonteAgente.GheFonteAgente.Id, dbConnection);

                /* informações do estudo */
                monitoramentoGheFonteAgente.Monitoramento.Estudo = estudoDao.findById((long)monitoramentoGheFonteAgente.Monitoramento.Estudo.Id, dbConnection);
                return monitoramentoGheFonteAgente;
            }
            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findMonitoramentoById", ex);
                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }
            finally
            {
                FabricaConexao.fecharConexao();
            }
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public void deleteMonitoramentoGheFonteAgente(MonitoramentoGheFonteAgente monitoramentoGheFonteAgente)
        {

            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método deleteMonitoramentoGheFonteAgente");

            NpgsqlConnection dbConnection = FabricaConexao.getConexao();
            NpgsqlTransaction transaction = dbConnection.BeginTransaction();

            IMonitoramentoGheFonteAgenteDao monitoramentoGheFonteAgenteDao = FabricaDao.getMonitoramentoGheFonteAgenteDao();

            try
            {

                monitoramentoGheFonteAgenteDao.delete(monitoramentoGheFonteAgente, dbConnection);
                transaction.Commit();

            }
            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - deleteMonitoramentoGheFonteAgente", ex);
                transaction.Rollback();
                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }
            finally
            {
                FabricaConexao.fecharConexao();
            }
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public EpiMonitoramento insertEpiMonitoramento(EpiMonitoramento epiMonitoramento)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método insertEpiMonitoramento");

            NpgsqlConnection dbConnection = FabricaConexao.getConexao();
            NpgsqlTransaction transaction = dbConnection.BeginTransaction();

            IEpiMonitoramentoDao epiMonitoramentoDao = FabricaDao.getEpiMonitoramentoDao();
            EpiMonitoramento epiMonitoramentoNew = null;

            try
            {

                epiMonitoramentoNew = epiMonitoramentoDao.insert(epiMonitoramento, dbConnection);
                transaction.Commit();

                return epiMonitoramento;
            }
            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - insertEpiMonitoramento", ex);
                transaction.Rollback();
                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }
            finally
            {
                FabricaConexao.fecharConexao();
            }
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public EpiMonitoramento updateEpiMonitoramento(EpiMonitoramento epiMonitoramento)
        {

            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método updateEpiMonitoramento");

            NpgsqlConnection dbConnection = FabricaConexao.getConexao();
            NpgsqlTransaction transaction = dbConnection.BeginTransaction();

            IEpiMonitoramentoDao epiMonitoramentoDao = FabricaDao.getEpiMonitoramentoDao();

            try
            {

                epiMonitoramentoDao.update(epiMonitoramento, dbConnection);
                transaction.Commit();

                return epiMonitoramento;
            }
            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - updateEpiMonitoramento", ex);
                transaction.Rollback();
                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }
            finally
            {
                FabricaConexao.fecharConexao();
            }
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public EpiMonitoramento findEpiMonitoramentoById(long id)
        {

            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findEpiMonitoramentoById");

            NpgsqlConnection dbConnection = FabricaConexao.getConexao();
            IEpiMonitoramentoDao epiMonitoramentoDao = FabricaDao.getEpiMonitoramentoDao();
            IEpiDao epiDao = FabricaDao.getEpiDao();
            IMonitoramentoGheFonteAgenteDao monitoramentoGheFonteAgenteDao = FabricaDao.getMonitoramentoGheFonteAgenteDao();
            IMonitoramentoDao monitoramentoDao = FabricaDao.getMonitoramentoDao();
            IClienteFuncaoFuncionarioDao clienteFuncaoFuncionarioDao = FabricaDao.getClienteFuncaoFuncionarioDao();
            IUsuarioDao usuarioDao = FabricaDao.getUsuarioDao();
            IFuncionarioDao funcionarioDao = FabricaDao.getFuncionarioDao();
            IClienteDao clienteDao = FabricaDao.getClienteDao();
            IGheFonteAgenteDao gheFonteAgenteDao = FabricaDao.getGheFonteAgenteDao();
            IEstudoDao estudoDao = FabricaDao.getEstudoDao();

            EpiMonitoramento epiMonitoramento;

            try
            {
                epiMonitoramento = epiMonitoramentoDao.findById(id, dbConnection);
                if (epiMonitoramento.Epi != null ) epiMonitoramento.Epi = epiDao.findById((long)epiMonitoramento.Epi.Id, dbConnection);

                /* dados do monitoramentoGheFonteAgente */

                epiMonitoramento.MonitoramentoGheFonteAgente = monitoramentoGheFonteAgenteDao.findById(id, dbConnection);
                epiMonitoramento.MonitoramentoGheFonteAgente.Monitoramento = monitoramentoDao.findById((long)epiMonitoramento.MonitoramentoGheFonteAgente.Monitoramento.Id, dbConnection);

                /* clienteFuncionario do monitoramento */
                epiMonitoramento.MonitoramentoGheFonteAgente.Monitoramento.ClienteFuncaoFuncionario = clienteFuncaoFuncionarioDao.findById((long)epiMonitoramento.MonitoramentoGheFonteAgente.Monitoramento.ClienteFuncaoFuncionario.Id, dbConnection);
                /* funcionario do clienteFuncionario do monitoramento */
                epiMonitoramento.MonitoramentoGheFonteAgente.Monitoramento.ClienteFuncaoFuncionario.Funcionario = funcionarioDao.findFuncionarioById((long)epiMonitoramento.MonitoramentoGheFonteAgente.Monitoramento.ClienteFuncaoFuncionario.Funcionario.Id, dbConnection);
                /* cliente do clienteFuncionario do monitoramento */
                epiMonitoramento.MonitoramentoGheFonteAgente.Monitoramento.ClienteFuncaoFuncionario.ClienteFuncao.Cliente = clienteDao.findById((long)epiMonitoramento.MonitoramentoGheFonteAgente.Monitoramento.ClienteFuncaoFuncionario.ClienteFuncao.Cliente.Id, dbConnection);
                /* usuário responsável do monitoramento */
                epiMonitoramento.MonitoramentoGheFonteAgente.Monitoramento.UsuarioResponsavel = usuarioDao.findById((long)epiMonitoramento.MonitoramentoGheFonteAgente.Monitoramento.UsuarioResponsavel.Id, dbConnection);
                /* informações do estudo que realizou o monitoramento */
                epiMonitoramento.MonitoramentoGheFonteAgente.Monitoramento.Estudo = estudoDao.findById((long)epiMonitoramento.MonitoramentoGheFonteAgente.Monitoramento.Estudo.Id, dbConnection);


                /* informação do gheFonteAgente */
                epiMonitoramento.MonitoramentoGheFonteAgente.GheFonteAgente = gheFonteAgenteDao.findById((long)epiMonitoramento.MonitoramentoGheFonteAgente.GheFonteAgente.Id, dbConnection);

                return epiMonitoramento;
            }
            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findEpiMonitoramentoById", ex);
                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }
            finally
            {
                FabricaConexao.fecharConexao();
            }
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public void deleteEpiMonitoramento(EpiMonitoramento epiMonitoramento)
        {

            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método deleteEpiMonitoramento");

            NpgsqlConnection dbConnection = FabricaConexao.getConexao();
            NpgsqlTransaction transaction = dbConnection.BeginTransaction();

            IEpiMonitoramentoDao epiMonitoramentoDao = FabricaDao.getEpiMonitoramentoDao();

            try
            {

                epiMonitoramentoDao.delete(epiMonitoramento, dbConnection);
                transaction.Commit();

            }
            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - deleteEpiMonitoramento", ex);
                transaction.Rollback();
                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }
            finally
            {
                FabricaConexao.fecharConexao();
            }
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public List<Monitoramento> findAllMonitoramentoByClienteAndPeriodo(Cliente cliente, DateTime periodo)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findAllMovimentoByClienteAndPeriodo");

            NpgsqlConnection dbConnection = FabricaConexao.getConexao();
            IMonitoramentoDao monitoramentoDao = FabricaDao.getMonitoramentoDao();
            IClienteFuncaoFuncionarioDao clienteFuncaoFuncionarioDao = FabricaDao.getClienteFuncaoFuncionarioDao();
            IFuncionarioDao funcionarioDao = FabricaDao.getFuncionarioDao();
            IEstudoDao estudoDao = FabricaDao.getEstudoDao();
            IUsuarioDao usuarioDao = FabricaDao.getUsuarioDao();
            IClienteFuncionarioDao clienteFuncionarioDao = FabricaDao.getclienteFuncionarioDao();
            IClienteDao clienteDao = FabricaDao.getClienteDao();
            IClienteFuncaoDao clienteFuncaoDao = FabricaDao.getClienteFuncaoDao();

            List<Monitoramento> monitoramentos = new List<Monitoramento>();

            try
            {
                monitoramentos = monitoramentoDao.findAllByClienteAndPeriodo(cliente, periodo, dbConnection);
                
                foreach (Monitoramento monit in monitoramentos)
                {
                    if (monit.ClienteFuncaoFuncionario != null)
                    {
                        monit.ClienteFuncaoFuncionario = clienteFuncaoFuncionarioDao.findById((long)monit.ClienteFuncaoFuncionario.Id, dbConnection);
                        monit.ClienteFuncaoFuncionario.ClienteFuncao = clienteFuncaoDao.findById((long)monit.ClienteFuncaoFuncionario.ClienteFuncao.Id, dbConnection);
                        monit.ClienteFuncaoFuncionario.ClienteFuncao.Cliente = clienteDao.findById((long)monit.ClienteFuncaoFuncionario.ClienteFuncao.Cliente.Id, dbConnection);
                        monit.ClienteFuncaoFuncionario.Funcionario = funcionarioDao.findFuncionarioById((long)monit.ClienteFuncaoFuncionario.Funcionario.Id, dbConnection);
                    }

                    if (monit.ClienteFuncionario != null)
                    {
                        monit.ClienteFuncionario = clienteFuncionarioDao.findById((long)monit.ClienteFuncionario.Id, dbConnection);
                        monit.ClienteFuncionario.Funcionario = funcionarioDao.findFuncionarioById((long)monit.ClienteFuncionario.Funcionario.Id, dbConnection);
                        monit.ClienteFuncionario.Cliente = clienteDao.findById((long)monit.ClienteFuncionario.Cliente.Id, dbConnection);
                    }

                    /* preenchendo informação do clienteFuncionario */
                    
                    monit.UsuarioResponsavel = usuarioDao.findById((long)monit.UsuarioResponsavel.Id, dbConnection);
                    monit.Cliente = clienteDao.findById((long)monit.Cliente.Id, dbConnection);
                        
                    monit.Estudo = estudoDao.findById((long)monit.Estudo.Id, dbConnection);
                }

                return monitoramentos;

            }
            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findAllMovimentoByClienteAndPeriodo", ex);
                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }
            finally
            {
                FabricaConexao.fecharConexao();
            }
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public LoteEsocial gerarLoteModelo2240(List<Monitoramento> monitoramentos)
        {

            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método gerarLoteModelo2240");

            NpgsqlConnection dbConnection = FabricaConexao.getConexao();
            NpgsqlTransaction transaction = dbConnection.BeginTransaction();
            ILoteEsocialDao loteEsocialDao = FabricaDao.getLoteEsocialDao();

            try
            {

                /* gerando informações do lote */
                LoteEsocial loteEsocial = new LoteEsocial();
                loteEsocial.DataCriacao = DateTime.Now;
                loteEsocial.Tipo = "2240";
                loteEsocial.periodo = monitoramentos[0].Periodo;
                loteEsocial.cliente = monitoramentos[0].Cliente;
                
                loteEsocial = loteEsocialDao.incluirLoteEsocial(loteEsocial, dbConnection);


                /* incluir a relação do lote com o monitoramento */

                
                foreach (Monitoramento monitoramento in monitoramentos)
                {
                    loteEsocialDao.insertLoteEsocialMonitoramento(new LoteEsocialMonitoramento(null, loteEsocial, monitoramento, ApplicationConstants.LOTE_ENVIADO, null, string.Empty), dbConnection);

                }

                transaction.Commit();

                return loteEsocial;
            }
            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - gerarLoteModelo2240", ex);
                transaction.Rollback();
                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }
            finally
            {
                FabricaConexao.fecharConexao();
            }
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public bool verificaExisteLoteJaGeradoByClienteAndPeriodo(Cliente cliente, DateTime periodo)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método verificaExisteLoteJaGeradoByClienteAndPeriodo");

            NpgsqlConnection dbConnection = FabricaConexao.getConexao();
            ILoteEsocialDao loteEsocialDao = FabricaDao.getLoteEsocialDao();
            try
            {
                return loteEsocialDao.verificaExisteLoteJaGeradoByClienteAndPeriodo(cliente, periodo, dbConnection);
            }
            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - verificaExisteLoteJaGeradoByClienteAndPeriodo", ex);
                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }
            finally
            {
                FabricaConexao.fecharConexao();
            }
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public List<Monitoramento> findAllByLote(LoteEsocial lote)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findAllByLote");

            NpgsqlConnection dbConnection = FabricaConexao.getConexao();
            IMonitoramentoDao monitoramentoDao = FabricaDao.getMonitoramentoDao();
            IUsuarioDao usuarioDao = FabricaDao.getUsuarioDao();
            IClienteFuncaoFuncionarioDao clienteFuncaoFuncionarioDao = FabricaDao.getClienteFuncaoFuncionarioDao();
            IClienteFuncionarioDao clienteFuncionarioDao = FabricaDao.getclienteFuncionarioDao();
            IFuncionarioDao funcionarioDao = FabricaDao.getFuncionarioDao();
            IClienteDao clienteDao = FabricaDao.getClienteDao();
            IEstudoDao estudoDao = FabricaDao.getEstudoDao();

            List<Monitoramento> monitoramentosInLote = new List<Monitoramento>();
            
            try
            {
                monitoramentosInLote = monitoramentoDao.findAllByLote(lote, dbConnection);
                Cliente cliente = clienteDao.findById((long)monitoramentosInLote[0].Cliente.Id, dbConnection);

                /* preenchendo dados extras do monitoramento */

                foreach (Monitoramento monitoramento in monitoramentosInLote)
                {

                    if (monitoramento.ClienteFuncaoFuncionario != null)
                    {
                        monitoramento.ClienteFuncaoFuncionario = clienteFuncaoFuncionarioDao.findById((long)monitoramento.ClienteFuncaoFuncionario.Id, dbConnection);
                        monitoramento.ClienteFuncaoFuncionario.Funcionario = funcionarioDao.findFuncionarioById((long)monitoramento.ClienteFuncaoFuncionario.Funcionario.Id, dbConnection);
                        monitoramento.ClienteFuncaoFuncionario.ClienteFuncao.Cliente = cliente;
                    
                    }
                    if (monitoramento.ClienteFuncionario != null)
                    {
                        monitoramento.ClienteFuncionario = clienteFuncionarioDao.findById((long)monitoramento.ClienteFuncionario.Id, dbConnection);
                        monitoramento.ClienteFuncionario.Funcionario = funcionarioDao.findFuncionarioById((long)monitoramento.ClienteFuncionario.Funcionario.Id, dbConnection);
                        monitoramento.ClienteFuncionario.Cliente = clienteDao.findById((long)monitoramento.ClienteFuncionario.Cliente.Id, dbConnection);
                    }
                    
                    
                    monitoramento.UsuarioResponsavel = usuarioDao.findById((long)monitoramento.UsuarioResponsavel.Id, dbConnection);
                    monitoramento.Cliente = cliente;
                    monitoramento.Estudo = estudoDao.findById((long)monitoramento.Estudo.Id, dbConnection);
                    monitoramento.Estudo.ClienteContratante = monitoramento.Estudo.ClienteContratante != null ? clienteDao.findById((long)monitoramento.Estudo.ClienteContratante.Id, dbConnection) : null;
                }

                return monitoramentosInLote.OrderBy(x => x.ClienteFuncaoFuncionario != null ? x.ClienteFuncaoFuncionario.Funcionario.Nome : x.ClienteFuncionario.Funcionario.Nome).ToList();
                
            }
            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findAllByLote", ex);
                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }
            finally
            {
                FabricaConexao.fecharConexao();
            }
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public string findAtividadeDoFuncionarioByMonitoramento(Monitoramento monitoramento)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findAtividadeDoFuncionarioByMonitoramento");

            NpgsqlConnection dbConnection = FabricaConexao.getConexao();
            IMonitoramentoDao monitoramentoDao = FabricaDao.getMonitoramentoDao();
            
            try
            {
                return monitoramentoDao.findAtividadeDoFuncionarioByMonitoramento(monitoramento, dbConnection);

            }
            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findAtividadeDoFuncionarioByMonitoramento", ex);
                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }
            finally
            {
                FabricaConexao.fecharConexao();
            }
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public string findSetorInLotacaoFuncionarioByMonitoramento(Monitoramento monitoramento)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findSetorInLotacaoFuncionarioByMonitoramento");

            NpgsqlConnection dbConnection = FabricaConexao.getConexao();
            IMonitoramentoDao monitoramentoDao = FabricaDao.getMonitoramentoDao();

            try
            {
                return monitoramentoDao.findSetorInLotacaoFuncionarioByMonitoramento(monitoramento, dbConnection);

            }
            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findSetorInLotacaoFuncionarioByMonitoramento", ex);
                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }
            finally
            {
                FabricaConexao.fecharConexao();
            }
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public string findFuncaoLotacaoFuncionarioByMonitoramento(Monitoramento monitoramento)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findFuncaoLotacaoFuncionarioByMonitoramento");

            NpgsqlConnection dbConnection = FabricaConexao.getConexao();
            IMonitoramentoDao monitoramentoDao = FabricaDao.getMonitoramentoDao();

            try
            {
                return monitoramentoDao.findFuncaoLotacaoFuncionarioByMonitoramento(monitoramento, dbConnection);

            }
            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findFuncaoLotacaoFuncionarioByMonitoramento", ex);
                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }
            finally
            {
                FabricaConexao.fecharConexao();
            }
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public List<MonitoramentoGheFonteAgente> findAllMonitoramentoAgenteByMonitoramento(Monitoramento monitoramento)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findAllMonitoramentoAgenteByMonitoramento");

            NpgsqlConnection dbConnection = FabricaConexao.getConexao();
            IMonitoramentoGheFonteAgenteDao monitoramentoGheFonteAgenteDao = FabricaDao.getMonitoramentoGheFonteAgenteDao();
            IGheFonteAgenteDao gheFonteAgenteDao = FabricaDao.getGheFonteAgenteDao();
            IAgenteDao agenteDao = FabricaDao.getAgenteDao();

            List<MonitoramentoGheFonteAgente> monitoramentoAgentes = new List<MonitoramentoGheFonteAgente>();

            try
            {
                monitoramentoAgentes = monitoramentoGheFonteAgenteDao.findAllByMonitoramento(monitoramento, dbConnection);
                foreach (MonitoramentoGheFonteAgente monitoramentoAgente in monitoramentoAgentes)
                {
                    monitoramentoAgente.Monitoramento = monitoramento;
                    monitoramentoAgente.GheFonteAgente = gheFonteAgenteDao.findById((long)monitoramentoAgente.GheFonteAgente.Id, dbConnection);
                    monitoramentoAgente.GheFonteAgente.Agente = agenteDao.findById((long)monitoramentoAgente.GheFonteAgente.Agente.Id, dbConnection);

                }

                return monitoramentoAgentes;

            }
            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findAllMonitoramentoAgenteByMonitoramento", ex);
                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }
            finally
            {
                FabricaConexao.fecharConexao();
            }
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public List<EpiMonitoramento> findAllEpiMonitoramentoByMonitoramentoGheFonteAgente(MonitoramentoGheFonteAgente monitoramentoGheFonteAgente)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findAllEpiMonitoramentoByMonitoramentoGheFonteAgente");

            NpgsqlConnection dbConnection = FabricaConexao.getConexao();
            IEpiMonitoramentoDao epiMonitoramentoDao = FabricaDao.getEpiMonitoramentoDao();
            IEpiDao epiDao = FabricaDao.getEpiDao();
            

            IMonitoramentoGheFonteAgenteDao monitoramentoGheFonteAgenteDao = FabricaDao.getMonitoramentoGheFonteAgenteDao();
            IGheFonteAgenteDao gheFonteAgenteDao = FabricaDao.getGheFonteAgenteDao();
            IAgenteDao agenteDao = FabricaDao.getAgenteDao();

            List<EpiMonitoramento> epiMonitoramentos = new List<EpiMonitoramento>();

            try
            {
                epiMonitoramentos = epiMonitoramentoDao.findAllByMonitoramentoGheFonteAgente(monitoramentoGheFonteAgente, dbConnection);

                foreach (EpiMonitoramento epiMonitoramento in epiMonitoramentos)
                {
                    epiMonitoramento.Epi = epiDao.findById((long)epiMonitoramento.Epi.Id, dbConnection);
                    epiMonitoramento.MonitoramentoGheFonteAgente = monitoramentoGheFonteAgente;
                }

                return epiMonitoramentos;

            }
            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findAllEpiMonitoramentoByMonitoramentoGheFonteAgente", ex);
                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }
            finally
            {
                FabricaConexao.fecharConexao();
            }
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public MonitoramentoGheFonteAgente findMonitoramentoGheFonteAgenteByGheFonteAgente(GheFonteAgente gheFonteAgente, Monitoramento monitoramento)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findMonitoramentoGheFonteAgenteByGheFonteAgente");

            NpgsqlConnection dbConnection = FabricaConexao.getConexao();
            IMonitoramentoGheFonteAgenteDao monitoramentoGheFonteAgenteDao = FabricaDao.getMonitoramentoGheFonteAgenteDao();
            IGheFonteAgenteDao gheFonteAgenteDao = FabricaDao.getGheFonteAgenteDao();
            IAgenteDao agenteDao = FabricaDao.getAgenteDao();
            IMonitoramentoDao monitoramentoDao = FabricaDao.getMonitoramentoDao();

            MonitoramentoGheFonteAgente monitoramentoAgente = new MonitoramentoGheFonteAgente();

            try
            {
                monitoramentoAgente = monitoramentoGheFonteAgenteDao.findMonitoramentoGheFonteAgenteByGheFonteAgente(gheFonteAgente, monitoramento, dbConnection);

                if (monitoramentoAgente != null)
                {
                    monitoramentoAgente.Monitoramento = monitoramentoDao.findById((long)monitoramentoAgente.Monitoramento.Id, dbConnection);
                    monitoramentoAgente.GheFonteAgente = gheFonteAgente;
                }

                return monitoramentoAgente;

            }
            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findMonitoramentoGheFonteAgenteByGheFonteAgente", ex);
                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }
            finally
            {
                FabricaConexao.fecharConexao();
            }
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public List<Monitoramento> findByFilter(Monitoramento monitoramento)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findByFilter");

            NpgsqlConnection dbConnection = FabricaConexao.getConexao();
            IMonitoramentoDao monitoramentoDao = FabricaDao.getMonitoramentoDao();
            IClienteDao clienteDao = FabricaDao.getClienteDao();
            IClienteFuncaoFuncionarioDao clienteFuncaoFuncionarioDao = FabricaDao.getClienteFuncaoFuncionarioDao();
            IFuncionarioDao funcionarioDao = FabricaDao.getFuncionarioDao();
            IClienteFuncionarioDao clienteFuncionarioDao = FabricaDao.getclienteFuncionarioDao();
               

            List<Monitoramento> monitoramentos = new List<Monitoramento>();

            try
            {
                monitoramentos = monitoramentoDao.findAllByFilter(monitoramento, dbConnection);

                /* preechendo com informações do monitoramento */

                foreach (Monitoramento monit in monitoramentos)
                {
                    monit.Cliente = clienteDao.findById((long)monit.Cliente.Id, dbConnection);

                    if (monit.ClienteFuncaoFuncionario != null)
                    {
                        monit.ClienteFuncaoFuncionario = clienteFuncaoFuncionarioDao.findById((long)monit.ClienteFuncaoFuncionario.Id, dbConnection);
                        monit.ClienteFuncaoFuncionario.ClienteFuncao.Cliente = monit.Cliente;
                        monit.ClienteFuncaoFuncionario.Funcionario = funcionarioDao.findFuncionarioById((long)monit.ClienteFuncaoFuncionario.Funcionario.Id, dbConnection);
                    }

                    /* inclusao relacionamento clienteFuncionario -- antigos clientes */
                    if (monit.ClienteFuncionario != null)
                    {
                        monit.ClienteFuncionario = clienteFuncionarioDao.findById((long)monit.ClienteFuncionario.Id, dbConnection);
                        monit.ClienteFuncionario.Funcionario = funcionarioDao.findFuncionarioById((long)monit.ClienteFuncionario.Funcionario.Id, dbConnection);
                        monit.ClienteFuncionario.Cliente = clienteDao.findById((long)monit.ClienteFuncionario.Cliente.Id, dbConnection);
                    }
                    

                }

                return monitoramentos.OrderBy(x => x.ClienteFuncaoFuncionario != null ? x.ClienteFuncaoFuncionario.Funcionario.Nome : x.ClienteFuncionario.Funcionario.Nome).ToList();

            }
            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findByFilter", ex);
                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }
            finally
            {
                FabricaConexao.fecharConexao();
            }
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public void replicarMonitoramento(Monitoramento monitoramento, DateTime periodo)
        {

            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método replicarMonitoramento");

            NpgsqlConnection dbConnection = FabricaConexao.getConexao();
            NpgsqlTransaction transaction = dbConnection.BeginTransaction();
            IMonitoramentoDao monitoramentoDao = FabricaDao.getMonitoramentoDao();
            IMonitoramentoGheFonteAgenteDao monitoramentoGheFonteAgenteDao = FabricaDao.getMonitoramentoGheFonteAgenteDao();
            IEpiMonitoramentoDao epiMonitoramentoDao = FabricaDao.getEpiMonitoramentoDao();

            try
            {
                Monitoramento novoMonitoramento = monitoramento.copy();
                novoMonitoramento.MonitoramentoAnterior = monitoramento;
                novoMonitoramento.Id = null;
                novoMonitoramento.Periodo = periodo;
                monitoramentoDao.insert(novoMonitoramento, dbConnection);

                /* incluindo colecao de agentes do monitoramento */
                List<MonitoramentoGheFonteAgente> monitoramentosAgentes = monitoramentoGheFonteAgenteDao.findAllByMonitoramento(monitoramento, dbConnection);

                monitoramentosAgentes.ForEach(delegate(MonitoramentoGheFonteAgente monitoramentoAgente)
                {
                    MonitoramentoGheFonteAgente novoMonitoramentoAgente = monitoramentoAgente.copy();
                    novoMonitoramentoAgente.Id = null;
                    novoMonitoramentoAgente.Monitoramento = novoMonitoramento;
                    monitoramentoGheFonteAgenteDao.insert(novoMonitoramentoAgente, dbConnection);

                    /* incluindo colecao de epis do monitoramento */

                    List<EpiMonitoramento> episAgentes = epiMonitoramentoDao.findAllByMonitoramentoGheFonteAgente(monitoramentoAgente, dbConnection);
                    episAgentes.ForEach(delegate(EpiMonitoramento epiAgente)
                    {
                        EpiMonitoramento novoEpiMonitoramento = epiAgente.copy();
                        novoEpiMonitoramento.Id = null;
                        novoEpiMonitoramento.MonitoramentoGheFonteAgente = novoMonitoramentoAgente;
                        epiMonitoramentoDao.insert(novoEpiMonitoramento, dbConnection);

                    });

                });
                

                transaction.Commit();

            }
            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - replicarMonitoramento", ex);
                transaction.Rollback();
                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }
            finally
            {
                FabricaConexao.fecharConexao();
            }
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public Monitoramento findLastMonitoramentoByClienteFuncionario(ClienteFuncaoFuncionario clienteFuncaoFuncionario, DateTime periodo)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findLastMonitoramentoByClienteFuncionario");

            NpgsqlConnection dbConnection = FabricaConexao.getConexao();
            IMonitoramentoDao monitoramentoDao = FabricaDao.getMonitoramentoDao();
            IClienteDao clienteDao = FabricaDao.getClienteDao();
            IClienteFuncaoFuncionarioDao clienteFuncaoFuncionarioDao = FabricaDao.getClienteFuncaoFuncionarioDao();
            IFuncionarioDao funcionarioDao = FabricaDao.getFuncionarioDao();

            Monitoramento lastmonitoramento = null;

            try
            {
                lastmonitoramento = monitoramentoDao.findLastMonitoramentoByClienteFuncaoFuncionario(clienteFuncaoFuncionario, periodo, dbConnection);

                /* preechendo com informações do monitoramento */

                if (lastmonitoramento != null)
                {
                    lastmonitoramento.Cliente = clienteDao.findById((long)lastmonitoramento.Cliente.Id, dbConnection);
                    lastmonitoramento.ClienteFuncaoFuncionario = clienteFuncaoFuncionarioDao.findById((long)lastmonitoramento.ClienteFuncaoFuncionario.Id, dbConnection);
                    lastmonitoramento.ClienteFuncaoFuncionario.ClienteFuncao.Cliente = lastmonitoramento.Cliente;
                    lastmonitoramento.ClienteFuncaoFuncionario.Funcionario = funcionarioDao.findFuncionarioById((long)lastmonitoramento.ClienteFuncaoFuncionario.Funcionario.Id, dbConnection);
                }
                
                return lastmonitoramento;

            }
            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findLastMonitoramentoByClienteFuncionario", ex);
                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }
            finally
            {
                FabricaConexao.fecharConexao();
            }
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public bool existeMonitoramentoPassadoByClienteAndPeriodo(Cliente cliente, DateTime periodo)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método existeMonitoramentoPassadoByClienteAndPeriodo");

            NpgsqlConnection dbConnection = FabricaConexao.getConexao();
            IMonitoramentoDao monitoramentoDao = FabricaDao.getMonitoramentoDao();

            try
            {
                return monitoramentoDao.existMonitoramentoPassadoByClienteAndPeriodo(cliente, periodo, dbConnection);

            }
            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - existeMonitoramentoPassadoByClienteAndPeriodo", ex);
                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }
            finally
            {
                FabricaConexao.fecharConexao();
            }
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public List<LoteEsocialAso> findAllCancelados2220ByFilter(Cliente cliente, DateTime periodoInicial, DateTime periodoFinal)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findAllCancelados2220ByFilter");

            NpgsqlConnection dbConnection = FabricaConexao.getConexao();
            ILoteEsocialDao loteEsocialDao = FabricaDao.getLoteEsocialDao();
            IAsoDao asoDao = FabricaDao.getAsoDao();
            List<LoteEsocialAso> listaCancelados = new List<LoteEsocialAso>();

            try
            {
                listaCancelados = loteEsocialDao.findAllCancelados2220ByFilter(cliente, periodoInicial, periodoFinal, dbConnection);

                /* populando a lista com informações */

                foreach (LoteEsocialAso lote in listaCancelados)
                {
                    lote.aso = asoDao.findById((long)lote.idAso, dbConnection);
                    lote.loteEsocial = loteEsocialDao.findById((long)lote.idLoteEsocialAso, dbConnection);
                }
               
            }
            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findAllCancelados2220ByFilter", ex);
                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }
            finally
            {
                FabricaConexao.fecharConexao();
            }
            return listaCancelados;
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public List<LoteEsocialMonitoramento> findAllCancelados2240ByFilter(Cliente cliente, DateTime periodoInicial, DateTime periodoFinal)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findAllCancelados2240ByFilter");

            NpgsqlConnection dbConnection = FabricaConexao.getConexao();
            ILoteEsocialDao loteEsocialDao = FabricaDao.getLoteEsocialDao();
            IMonitoramentoDao monitoramentoDao = FabricaDao.getMonitoramentoDao();
            IClienteFuncaoFuncionarioDao clienteFuncaoFuncionarioDao = FabricaDao.getClienteFuncaoFuncionarioDao();
            IClienteDao clienteDao = FabricaDao.getClienteDao();
            List<LoteEsocialMonitoramento> listCancelados2240 = new List<LoteEsocialMonitoramento>();


            try
            {
                listCancelados2240 = loteEsocialDao.findAllCancelados2240ByFilter(cliente, periodoInicial, periodoFinal, dbConnection);

                foreach (LoteEsocialMonitoramento lote in listCancelados2240)
                {
                    lote.LoteEsocial = loteEsocialDao.findById((long)lote.LoteEsocial.Id, dbConnection);
                    lote.Monitoramento = monitoramentoDao.findById((long)lote.Monitoramento.Id, dbConnection);
                    lote.Monitoramento.ClienteFuncaoFuncionario = clienteFuncaoFuncionarioDao.findById((long)lote.Monitoramento.ClienteFuncaoFuncionario.Id, dbConnection);
                    lote.Monitoramento.Cliente = clienteDao.findById((long)lote.Monitoramento.Cliente.Id, dbConnection);
                }
                
               
            }
            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findAllCancelados2240ByFilter", ex);
                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }
            finally
            {
                FabricaConexao.fecharConexao();
            }
            return listCancelados2240;
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public List<LoteEsocialAso> findAll2220ForCancelByFilter(
            Cliente cliente, 
            ClienteFuncaoFuncionario clienteFuncaoFuncionario, 
            string codigoLote, 
            DateTime periodoInicial, 
            DateTime periodoFinal
            )
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findAll2220ForCancelByFilter");

            NpgsqlConnection dbConnection = FabricaConexao.getConexao();
            ILoteEsocialDao loteEsocialDao = FabricaDao.getLoteEsocialDao();
            IAsoDao asoDao = FabricaDao.getAsoDao();
            List<LoteEsocialAso> listaForCancel = new List<LoteEsocialAso>();

            try
            {
                listaForCancel = loteEsocialDao.findAll2220ForCancelByFilter(cliente, clienteFuncaoFuncionario, codigoLote, periodoInicial, periodoFinal, dbConnection);

                /* populando a lista com informações */

                foreach (LoteEsocialAso lote in listaForCancel)
                {
                    lote.aso = asoDao.findById((long)lote.idAso, dbConnection);
                    lote.loteEsocial = loteEsocialDao.findById((long)lote.idLoteEsocialAso, dbConnection);
                }

            }
            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findAll2220ForCancelByFilter", ex);
                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }
            finally
            {
                FabricaConexao.fecharConexao();
            }
            return listaForCancel;
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public List<LoteEsocialMonitoramento> findAll2240ForCancelByFilter(
            Cliente cliente,
            ClienteFuncaoFuncionario clienteFuncaoFuncionario,
            string codigo,
            DateTime periodoInicial,
            DateTime periodoFinal
            )
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findAll2240ForCancelByFilter");

            NpgsqlConnection dbConnection = FabricaConexao.getConexao();
            ILoteEsocialDao loteEsocialDao = FabricaDao.getLoteEsocialDao();
            IMonitoramentoDao monitoramentoDao = FabricaDao.getMonitoramentoDao();
            IClienteFuncaoFuncionarioDao clienteFuncaoFuncionarioDao = FabricaDao.getClienteFuncaoFuncionarioDao();
            IClienteDao clienteDao = FabricaDao.getClienteDao();
            List<LoteEsocialMonitoramento> list2240ForCancel = new List<LoteEsocialMonitoramento>();


            try
            {
                list2240ForCancel = loteEsocialDao.findAll2240ForCancelByFilter(
                    cliente,
                    clienteFuncaoFuncionario,
                    codigo,
                    periodoInicial, 
                    periodoFinal, 
                    dbConnection
                    );

                foreach (LoteEsocialMonitoramento lote in list2240ForCancel)
                {
                    lote.LoteEsocial = loteEsocialDao.findById((long)lote.LoteEsocial.Id, dbConnection);
                    lote.Monitoramento = monitoramentoDao.findById((long)lote.Monitoramento.Id, dbConnection);
                    lote.Monitoramento.ClienteFuncaoFuncionario = clienteFuncaoFuncionarioDao.findById((long)lote.Monitoramento.ClienteFuncaoFuncionario.Id, dbConnection);
                    lote.Monitoramento.Cliente = clienteDao.findById((long)lote.Monitoramento.Cliente.Id, dbConnection);
                }


            }
            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findAll2240ForCancelByFilter", ex);
                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }
            finally
            {
                FabricaConexao.fecharConexao();
            }
            return list2240ForCancel;
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public LoteEsocialAso findLoteEsocialAsoById(long id)
        {

            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findLoteEsocialAsoById");

            NpgsqlConnection dbConnection = FabricaConexao.getConexao();

            ILoteEsocialDao loteEsocialDao = FabricaDao.getLoteEsocialDao();
            IAsoDao asoDao = FabricaDao.getAsoDao();
            LoteEsocialAso loteEsocialAso = null;
            try
            {

                loteEsocialAso = loteEsocialDao.findEsocialLoteAsoById(id, dbConnection);

                if (loteEsocialAso != null)
                {
                    loteEsocialAso.loteEsocial = loteEsocialDao.findById((long)loteEsocialAso.idLoteEsocialAso, dbConnection);
                    loteEsocialAso.aso = asoDao.findById((long)loteEsocialAso.idAso, dbConnection);
                }
                
            }
            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findLoteEsocialAsoById", ex);
                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }
            finally
            {
                FabricaConexao.fecharConexao();
            }

            return loteEsocialAso;
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public LoteEsocialAso updateEsocialASo (LoteEsocialAso loteEsocialAso)
        {

            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método updateEsocialASo");

            NpgsqlConnection dbConnection = FabricaConexao.getConexao();
            NpgsqlTransaction transaction = dbConnection.BeginTransaction();

            ILoteEsocialDao loteEsocialAsoDao = FabricaDao.getLoteEsocialDao();

            try
            {

                loteEsocialAsoDao.updateLoteEsocialAso(loteEsocialAso, dbConnection);
                transaction.Commit();

                return loteEsocialAso;
            }
            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - updateEsocialASo", ex);
                transaction.Rollback();
                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }
            finally
            {
                FabricaConexao.fecharConexao();
            }
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public LoteEsocialMonitoramento findLoteEsocialMonitoramentoById(long id)
        {

            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findLoteEsocialMonitoramentoById");

            NpgsqlConnection dbConnection = FabricaConexao.getConexao();

            ILoteEsocialDao loteEsocialDao = FabricaDao.getLoteEsocialDao();
            IMonitoramentoDao monitoramentoDao = FabricaDao.getMonitoramentoDao();
            IClienteDao clienteDao = FabricaDao.getClienteDao();
            IClienteFuncaoFuncionarioDao clienteFuncaoFuncionarioDao = FabricaDao.getClienteFuncaoFuncionarioDao();
            LoteEsocialMonitoramento loteEsocialMonitoramento = null;
            try
            {

                loteEsocialMonitoramento = loteEsocialDao.findLoteEsocialMonitoramentoById(id, dbConnection);

                if (loteEsocialMonitoramento != null)
                {
                    loteEsocialMonitoramento.LoteEsocial = loteEsocialDao.findById((long)loteEsocialMonitoramento.LoteEsocial.Id, dbConnection);
                    loteEsocialMonitoramento.Monitoramento = monitoramentoDao.findById((long)loteEsocialMonitoramento.Monitoramento.Id, dbConnection);
                    loteEsocialMonitoramento.Monitoramento.Cliente = clienteDao.findById((long)loteEsocialMonitoramento.Monitoramento.Cliente.Id, dbConnection);
                    loteEsocialMonitoramento.Monitoramento.ClienteFuncaoFuncionario = clienteFuncaoFuncionarioDao.findById((long)loteEsocialMonitoramento.Monitoramento.ClienteFuncaoFuncionario.Id, dbConnection);
                }

            }
            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findLoteEsocialMonitoramentoById", ex);
                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }
            finally
            {
                FabricaConexao.fecharConexao();
            }

            return loteEsocialMonitoramento;
        }


        [MethodImpl(MethodImplOptions.Synchronized)]
        public LoteEsocialMonitoramento updateEsocialMonitoramento(LoteEsocialMonitoramento loteEsocialMonitoramento)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método updateEsocialMonitoramento");

            NpgsqlConnection dbConnection = FabricaConexao.getConexao();
            NpgsqlTransaction transaction = dbConnection.BeginTransaction();

            ILoteEsocialDao loteEsocialAsoDao = FabricaDao.getLoteEsocialDao();

            try
            {

                loteEsocialAsoDao.updateLoteEsocialMonitoramento(loteEsocialMonitoramento, dbConnection);
                transaction.Commit();

            }
            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - updateEsocialMonitoramento", ex);
                transaction.Rollback();
                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }
            finally
            {
                FabricaConexao.fecharConexao();
            }

            return loteEsocialMonitoramento;
        }

    }
}
