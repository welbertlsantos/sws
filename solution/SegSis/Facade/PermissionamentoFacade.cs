﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Npgsql;
using SWS.Dao;
using SWS.IDao;
using SWS.Entidade;
using SWS.Excecao;
using System.Data;
using SWS.Helper;
using System.Runtime.CompilerServices;
using SWS.Resources;

namespace SWS.Facade
{
    class PermissionamentoFacade
    {
        public static PermissionamentoFacade ppraFacade = null;
        public static Dictionary<String, HashSet<Acao>> listaPermissoes = new Dictionary<String, HashSet<Acao>>();
        public static Usuario usuarioAutenticado;
        public static Filial filialAutenticada;

        public static Dictionary<String, String> mapaConfiguracoes = new Dictionary<String, String>();

        private PermissionamentoFacade() { }

        public static PermissionamentoFacade getInstance()
        {
            if (ppraFacade == null)
            {
                ppraFacade = new PermissionamentoFacade();
            }

            return ppraFacade;
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public Usuario autenticaUsuario(Usuario usuario, Filial filial)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método autenticaUsuario");

            filialAutenticada = filial;

            NpgsqlConnection dbConnection = FabricaConexao.getConexao();
            IUsuarioDao usuarioDao = FabricaDao.getUsuarioDao();
            IEmpresaDao empresaDao = FabricaDao.getEmpresaDao();
            IUsuarioClienteDao usuarioClienteDao = FabricaDao.getUsuarioClienteDao();
            IClienteDao clienteDao = FabricaDao.getClienteDao();
            
            try
            {
                usuario = usuarioDao.autentica(usuario, dbConnection);
                
                /* populando dados da empresa do usuário */
                usuario.Empresa = empresaDao.findById((long)usuario.Empresa.Id, dbConnection);

                /* populando dados dos clientes restringidos */
                usuario.UsuarioCliente = usuarioClienteDao.findAllByUsuario(usuario, dbConnection);
                usuario.UsuarioCliente.ForEach(x => x.Cliente = clienteDao.findById((long)x.Cliente.Id, dbConnection));

                populaListaPermissoes(usuario.Id, dbConnection);
                usuarioAutenticado = usuario;
                
                findAllConfiguracao();

                String seralCliente = null;
                mapaConfiguracoes.TryGetValue(ConfigurationConstans.SERIAL_CLIENTE, out seralCliente);
                Int32 ano = Convert.ToInt32(seralCliente.Substring(0, 4))/2;
                Int32 mes = Convert.ToInt32(seralCliente.Substring(4, 2))/2;
                Int32 dia = Convert.ToInt32(seralCliente.Substring(6, 2))/2;
                String valor = ano.ToString() + mes.ToString() + dia.ToString();
                String valor2 = System.DateTime.Now.Year.ToString() + System.DateTime.Now.Month.ToString() + System.DateTime.Now.Day.ToString();

                if (Convert.ToInt32(valor2) > Convert.ToInt32(valor))
                {
                    throw new Exception("O prazo de avalização do sistema terminou. Favor entrar em contato com a empresa NJ Informática.");
                }
            }
            catch (UsuarioNaoAutenticadoException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - autenticaUsuario", ex);
                throw new UsuarioNaoAutenticadoException("Usuário ou senha inválidos.", ex);
            }
            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - autenticaUsuario", ex);
                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }
            catch (Exception ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - autenticaUsuario", ex);
                throw new Exception(ex.Message);
            }
            finally
            {
                FabricaConexao.fecharConexao();
            }

            return usuario;
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public HashSet<Perfil> listaPerfisDoUsuario(Int64? idUsuario)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método listaPerfisDoUsuario");

            NpgsqlConnection dbConnection = FabricaConexao.getConexao();
            IPerfilDao perfilDao = FabricaDao.getPerfilDao();

            try
            {
                return perfilDao.findByUsuario(idUsuario, dbConnection);
            }
            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - listaFuncaoInterna", ex);
                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }
            finally
            {
                FabricaConexao.fecharConexao();
            }
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public DataSet listaTodosPerfis()
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método listaTodosPerfis");

            NpgsqlConnection dbConnection = FabricaConexao.getConexao();
            IPerfilDao perfilDao = FabricaDao.getPerfilDao();

            try
            {
                return perfilDao.findAll(dbConnection);
            }
            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - listaTodosPerfis", ex);
                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }
            finally
            {
                FabricaConexao.fecharConexao();
            }
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public DataSet listaTodasAcoes()
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método listaTodasAcoes");

            NpgsqlConnection dbConnection = FabricaConexao.getConexao();
            IAcaoDao acaoDao = FabricaDao.getAcaoDao();

            try
            {
                return acaoDao.listaTodos(dbConnection);
            }
            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - listaTodasAcoes", ex);
                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }
            finally
            {
                FabricaConexao.fecharConexao();
            }
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public DataSet listaTodasAcoesByDominio(Int64 idDominio)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método listaTodasAcoesByDominio");

            NpgsqlConnection dbConnection = FabricaConexao.getConexao();
            IAcaoDao acaoDao = FabricaDao.getAcaoDao();

            try
            {
                return acaoDao.listaTodosByDominio(idDominio, dbConnection);
            }
            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - listaTodasAcoesByDominio", ex);
                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }
            finally
            {
                FabricaConexao.fecharConexao();
            }
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public DataSet findPerfilByFilter(Perfil perfil)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findPerfilByFilter");

            NpgsqlConnection dbConnection = FabricaConexao.getConexao();
            IPerfilDao perfilDao = FabricaDao.getPerfilDao();

            try
            {
                return perfilDao.findByFilter(perfil, dbConnection);
            }
            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findPerfilByFilter", ex);
                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }
            finally
            {
                FabricaConexao.fecharConexao();
            }
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public void excluirPerfisDeUmUsuario(Int64? idUsuario)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método excluirPerfisDeUmUsuario");

            NpgsqlConnection dbConnection = FabricaConexao.getConexao();
            IUsuarioPerfilDao usuarioPerfilDao = FabricaDao.getUsuarioPerfilDao();
            NpgsqlTransaction transaction = dbConnection.BeginTransaction();

            try
            {
                usuarioPerfilDao.excluirPerfisDeUmUsuario(idUsuario, dbConnection);

                transaction.Commit();
            }
            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - excluirPerfisDeUmUsuario", ex);
                transaction.Rollback();
                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }
            finally
            {
                FabricaConexao.fecharConexao();
            }
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public Boolean verificaAcaoSelecionadaParaPerfilDominio(Int64 idPerfil, Int64 idDominio, Int64 idAcao)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método verificaAcaoSelecionadaParaPerfilDominio");

            NpgsqlConnection dbConnection = FabricaConexao.getConexao();
            IAcaoDao acaoDao = FabricaDao.getAcaoDao();

            try
            {
                return acaoDao.verificaAcaoSelecionadaParaPerfilDominio(idPerfil, idDominio, idAcao, dbConnection);
            }
            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - verificaAcaoSelecionadaParaPerfilDominio", ex);
                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }
            finally
            {
                FabricaConexao.fecharConexao();
            }
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        internal void removeAcaoDominioFromPerfil(Int64 idPerfil, Int64 idDominio, Int64 idAcao)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método removeAcaoDominioFromPerfil");

            NpgsqlConnection dbConnection = FabricaConexao.getConexao();
            IAcaoDao acaoDao = FabricaDao.getAcaoDao();
            NpgsqlTransaction transaction = dbConnection.BeginTransaction();

            try
            {
                acaoDao.removeAcaoDominioFromPerfil(idPerfil, idDominio, idAcao, dbConnection);

                transaction.Commit();
            }
            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - removeAcaoDominioFromPerfil", ex);
                transaction.Rollback();
                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }
            finally
            {
                FabricaConexao.fecharConexao();
            }
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        internal void criaAcaoDominioFromPerfil(Int64 idPerfil, Int64 idDominio, Int64 idAcao)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método criaAcaoDominioFromPerfil");

            NpgsqlConnection dbConnection = FabricaConexao.getConexao();
            IAcaoDao acaoDao = FabricaDao.getAcaoDao();
            NpgsqlTransaction transaction = dbConnection.BeginTransaction();

            try
            {
                acaoDao.criaAcaoDominioFromPerfil(idPerfil, idDominio, idAcao, dbConnection);

                transaction.Commit();
            }
            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - criaAcaoDominioFromPerfil", ex);
                transaction.Rollback();
                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }
            finally
            {
                FabricaConexao.fecharConexao();
            }
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        internal void populaListaPermissoes(Int64? idUsuario, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método populaListaPermissoes");

            IAcaoDao acaoDao = FabricaDao.getAcaoDao();
            IPerfilDao perfilDao = FabricaDao.getPerfilDao();
            IDominioDao dominioDao = FabricaDao.getDominioDao();
            HashSet<Acao> acoes = null;
            HashSet<Perfil> perfis = null;
            HashSet<Dominio> dominios = null;

            try
            {
                perfis = perfilDao.findByUsuario(idUsuario, dbConnection);

                foreach(Perfil perfil in perfis)
                {
                    dominios = dominioDao.findByPerfil(perfil.Id, dbConnection);

                    HashSet<Acao> acoesCadastradas;

                    foreach (Dominio dominio in dominios)
                    {
                        acoes = acaoDao.findByPerfilAndDominio(perfil.Id, dominio.Id, dbConnection);

                        if (!listaPermissoes.ContainsKey(dominio.Descricao))
                        {
                            listaPermissoes.Add(dominio.Descricao, acoes);
                        }
                        else
                        {
                            listaPermissoes.TryGetValue(dominio.Descricao, out acoesCadastradas);
                            acoes.UnionWith(acoesCadastradas);
                            listaPermissoes.Remove(dominio.Descricao);
                            listaPermissoes.Add(dominio.Descricao, acoes);
                        }
                    }
                }
            }
            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - populaListaPermissoes", ex);
                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }
            finally
            {
                FabricaConexao.fecharConexao();
            }
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public Boolean usuarioPossuiDominio(String nomeDominio)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método usuarioPossuiDominio");

            LogHelper.logger.Debug("nomeDominio: " + nomeDominio);

            return listaPermissoes.ContainsKey(nomeDominio);
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public Boolean hasPermission(String dominio, String acao)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método hasPermission");

            Boolean retorno = false;
            HashSet<Acao> acoes = null;

            if (listaPermissoes.ContainsKey(dominio))
            {
                listaPermissoes.TryGetValue(dominio, out acoes);

                foreach (Acao action in acoes)
                {
                    if (action.getDescricao().Equals(acao))
                        retorno = true;
                }
            }
            
            return retorno;
        }


        [MethodImpl(MethodImplOptions.Synchronized)]
        public Dictionary<string, string> findAllConfiguracao()
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findAllConfiguracao");

            NpgsqlConnection dbConnection = FabricaConexao.getConexao();

            IConfiguracaoDao configuracaoDao = FabricaDao.getConfiguracaoDao();

            try
            {
                mapaConfiguracoes = configuracaoDao.findAllConfiguracao(dbConnection);

            }
            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findAllConfiguracao", ex);
                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }
            finally
            {
                FabricaConexao.fecharConexao();
            }

            return mapaConfiguracoes;
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public DataSet findAllPerfilByUsuario (Usuario usuario)
        {
            LogHelper.logger.Info(this.GetType().Name + "findAllPerfilByUsuario");

            NpgsqlConnection dbConnection = FabricaConexao.getConexao();
            IUsuarioPerfilDao usuarioPerfilDao = FabricaDao.getUsuarioPerfilDao();
            try
            {
                return usuarioPerfilDao.findAllPerfilByUsuario(usuario, dbConnection);
            }
            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findUsuarioByLogin", ex);
                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }
            finally
            {
                FabricaConexao.fecharConexao();
            }

        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public void insertUsuarioPerfil(HashSet<UsuarioPerfil> usuarioPerfis)
        {
            LogHelper.logger.Info(this.GetType().Name + "insertUsuarioPerfil");

            NpgsqlConnection dbConnection = FabricaConexao.getConexao();

            IUsuarioPerfilDao usuarioPerfilDao = FabricaDao.getUsuarioPerfilDao();

            NpgsqlTransaction transaction = dbConnection.BeginTransaction();

            try
            {
                foreach (UsuarioPerfil usuarioPerfil in usuarioPerfis)
                {
                    usuarioPerfilDao.incluir(usuarioPerfil, dbConnection);
                }

                transaction.Commit();
            }
            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + "insertUsuarioPerfil", ex);
                transaction.Rollback();
                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }
            finally
            {
                FabricaConexao.fecharConexao();
            }
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public void deleteUsuarioPerfil(UsuarioPerfil usuarioPerfil)
        {
            LogHelper.logger.Info(this.GetType().Name + "deleteUsuarioPerfil");

            NpgsqlConnection dbConnection = FabricaConexao.getConexao();

            IUsuarioPerfilDao usuarioPerfilDao = FabricaDao.getUsuarioPerfilDao();

            NpgsqlTransaction transaction = dbConnection.BeginTransaction();

            try
            {
                usuarioPerfilDao.excluir(usuarioPerfil, dbConnection);
                transaction.Commit();
            }
            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + "deleteUsuarioPerfil", ex);
                transaction.Rollback();
                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }
            finally
            {
                FabricaConexao.fecharConexao();
            }
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public Configuracao updateConfiguracao(Configuracao configuracao)
        {
            LogHelper.logger.Info(this.GetType().Name + "updateConfiguracao");

            NpgsqlConnection dbConnection = FabricaConexao.getConexao();
            IConfiguracaoDao configuracaoDao = FabricaDao.getConfiguracaoDao();
            NpgsqlTransaction transaction = dbConnection.BeginTransaction();

            try
            {
                configuracao = configuracaoDao.update(configuracao, dbConnection);
                transaction.Commit();
                return configuracao;
            }
            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + "updateConfiguracao", ex);
                transaction.Rollback();
                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }
            finally
            {
                FabricaConexao.fecharConexao();
            }
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public List<Configuracao> findAllConfiguracaoList()
        {
            LogHelper.logger.Info(this.GetType().Name + "findAllConfiguracaoList");

            NpgsqlConnection dbConnection = FabricaConexao.getConexao();
            IConfiguracaoDao configuracaoDao = FabricaDao.getConfiguracaoDao();

            try
            {
                return configuracaoDao.findAll(dbConnection);
            }
            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + "findAllConfiguracaoList", ex);
                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }
            finally
            {
                FabricaConexao.fecharConexao();
            }
        }
    }
}
