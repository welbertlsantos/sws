﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SWS.IDao;
using SWS.Entidade;
using Npgsql;
using SWS.Excecao;
using System.Data;
using SWS.Dao;
using SWS.Helper;
using System.Runtime.CompilerServices;
using SWS.View.Resources;

namespace SWS.Facade
{
    class VendedorFacade
    {
        public static VendedorFacade vendedorFacade = null;
        
        private VendedorFacade() { }
        
        public static VendedorFacade getInstance()
        {
            if (vendedorFacade == null)
            {
                vendedorFacade = new VendedorFacade();
            }

            return vendedorFacade;
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public DataSet findVendedorByFilter(Vendedor vendedor)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findVendedorByFilter");

            NpgsqlConnection dbConnection = FabricaConexao.getConexao();
            IVendedorDao vendedorDao = FabricaDao.getVendedorDao();

            try
            {
                return vendedorDao.findVendedorByFilter(vendedor, dbConnection);

            }
            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findVendedorByFilter", ex);
                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }
            finally
            {
                FabricaConexao.fecharConexao();
            }
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public Vendedor insertVendedor(Vendedor vendedor)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método incluirVendedor");

            Vendedor vendedorNovo = null;
            NpgsqlConnection dbConnection = FabricaConexao.getConexao();
            IVendedorDao vendedorDao = FabricaDao.getVendedorDao();
            NpgsqlTransaction transaction = dbConnection.BeginTransaction();
            try
            {
                vendedorNovo = vendedorDao.insert(vendedor, dbConnection);

                transaction.Commit();
            }
            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - incluirVendedor", ex);
                transaction.Rollback();
                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }
            finally
            {
                FabricaConexao.fecharConexao();
            }

            return vendedor;
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public Vendedor findVendedorByCpf(String cpf)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findVendedorByCpf");

            NpgsqlConnection dbConnection = FabricaConexao.getConexao();
            IVendedorDao vendedorDao = FabricaDao.getVendedorDao();

            try
            {
                Vendedor vendedor = vendedorDao.findVendedorByCpf(cpf, dbConnection);

                return vendedor;
            }
            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findVendedorByCpf", ex);
                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }
            finally
            {
                FabricaConexao.fecharConexao();
            }
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public Vendedor findVendedorById(Int64 idVendedor)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findVendedorById");

            NpgsqlConnection dbConnection = FabricaConexao.getConexao();
            IVendedorDao vendedorDao = FabricaDao.getVendedorDao();
            try
            {
                Vendedor vendedor = vendedorDao.findVendedorById(idVendedor, dbConnection);
                return vendedor;

            }
            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findVendedorById", ex);
                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }
            finally
            {
                FabricaConexao.fecharConexao();
            }
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public Vendedor updateVendedor(Vendedor vendedorAltera)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método alteraVendedor");

            Vendedor vendedorNovo = null;
            NpgsqlConnection dbConnection = FabricaConexao.getConexao();
            IVendedorDao vendedorDao = FabricaDao.getVendedorDao();
            NpgsqlTransaction transaction = dbConnection.BeginTransaction();
            try
            {
                vendedorNovo = vendedorDao.update(vendedorAltera, dbConnection);
                transaction.Commit();
            }
            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - alterarVendedor", ex);
                transaction.Rollback();
                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }
            finally
            {
                FabricaConexao.fecharConexao();
            }

            return vendedorNovo;
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        
        public void changeSituacaoVendedor(Vendedor vendedor)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método alteraStatusVendedor");

            NpgsqlConnection dbConnection = FabricaConexao.getConexao();
            
            IVendedorDao vendedorDao = FabricaDao.getVendedorDao();

            NpgsqlTransaction transaction = dbConnection.BeginTransaction();
            
            try
            {
                vendedorDao.changeStatusVendedor(vendedor, dbConnection);

                transaction.Commit();
            }
            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - alterarStatusVendedor", ex);
                transaction.Rollback();
                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }
            finally
            {
                FabricaConexao.fecharConexao();
            }

        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public DataSet findVendedorByName(Vendedor vendedor)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findVendedorByName");

            NpgsqlConnection dbConnection = FabricaConexao.getConexao();
            IVendedorDao vendedorDao = FabricaDao.getVendedorDao();

            try
            {
                return vendedorDao.findVendedorByName(vendedor, dbConnection);

            }
            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findVendedorByName", ex);
                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }
            finally
            {
                FabricaConexao.fecharConexao();
            }
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public VendedorCliente updateVendedorCliente(VendedorCliente vendedor)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método updateVendedorCliente");

            NpgsqlConnection dbConnection = FabricaConexao.getConexao();

            IClienteVendedorDao vendedorClienteDao = FabricaDao.getClienteVendedorDao();

            NpgsqlTransaction transaction = dbConnection.BeginTransaction();

            try
            {
                /* desativando vendedor anterior do cliente */
                VendedorCliente vendedorAnterior = vendedorClienteDao.findByCliente(vendedor.Cliente, dbConnection).Find(x => String.Equals(x.Situacao, ApplicationConstants.ATIVO));
                vendedorClienteDao.update(vendedorAnterior, dbConnection);

                /* incluindo novo vendedor para  o cliente */
                vendedor = vendedorClienteDao.insert(vendedor, dbConnection);
                
                transaction.Commit();

                return vendedor;
            }
            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - updateVendedorCliente", ex);
                transaction.Rollback();
                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }
            finally
            {
                FabricaConexao.fecharConexao();
            }

        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public DataSet findVendedorNotInCliente(Vendedor vendedor, Cliente cliente)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findVendedorNotInCliente");

            NpgsqlConnection dbConnection = FabricaConexao.getConexao();
            IVendedorDao vendedorDao = FabricaDao.getVendedorDao();

            try
            {
                return vendedorDao.findVendedorNotInCliente(vendedor, cliente, dbConnection);

            }
            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findVendedorNotInCliente", ex);
                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }
            finally
            {
                FabricaConexao.fecharConexao();
            }
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public VendedorCliente insertVendedorCliente(VendedorCliente vendedor)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método insertVendedorCliente");

            NpgsqlConnection dbConnection = FabricaConexao.getConexao();

            IClienteVendedorDao vendedorClienteDao = FabricaDao.getClienteVendedorDao();

            NpgsqlTransaction transaction = dbConnection.BeginTransaction();

            VendedorCliente newVendedorCliente = null;

            try
            {
                newVendedorCliente = vendedorClienteDao.insert(vendedor, dbConnection);
                transaction.Commit();

                return newVendedorCliente;
            }
            catch (DataBaseConectionException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - insertVendedorCliente", ex);
                transaction.Rollback();
                throw new DataBaseConectionException(DataBaseConectionException.msg + ex.Message);
            }
            finally
            {
                FabricaConexao.fecharConexao();
            }

        }
    }
}
