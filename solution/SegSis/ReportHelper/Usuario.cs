﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SegSis.ReportHelper
{
    class Usuario
    {

        private Int64 id;
        private String rg;
        private String cpf;
        private String nome;
        private String senha;
        private String login;

        public Int64 Id
        {
            get { return id; }
            set { id = value; }
        }
        
        public String Login
        {
            get { return login; }
            set { login = value; }
        }
        
        public String Senha
        {
            get { return senha; }
            set { senha = value; }
        }
        
        public String Nome
        {
            get { return nome; }
            set { nome = value; }
        }
        
        public String Cpf
        {
            get { return cpf; }
            set { cpf = value; }
        }
        
        public String Rg
        {
            get { return rg; }
            set { rg = value; }
        }
    }
}
