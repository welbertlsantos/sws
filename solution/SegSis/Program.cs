﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using System.IO;
using System.Xml.Linq;
using System.Web;

namespace SWS.View
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Directory.SetCurrentDirectory(getDefaultDirectory());
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            log4net.Config.XmlConfigurator.Configure();
            Application.Run(new Frm_Login());
        }

        private static String getDefaultDirectory()
        {
            Directory.SetCurrentDirectory("C:\\SWS\\Config\\");
            XElement xml = XElement.Load("sws_default_directory.xml");
            String defaultDirectory = String.Empty;

            try
            {
                if (xml.Elements().Count() < 1)
                    throw new Exception("Arquivo de pasta padrão do sistema não encontrada.");

                foreach (XElement x in xml.Elements())
                    defaultDirectory = Convert.ToString(x.Attribute("name").Value);
            } 
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

            return defaultDirectory;
        }
    }
}
