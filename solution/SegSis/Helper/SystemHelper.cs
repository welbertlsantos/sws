﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SWS.Helper
{
    class SystemHelper
    {
        public static Int32 diferencaDatasEmDias(DateTime data1, DateTime data2)
        {
            Int32 retorno = 0;

            if (data2 > data1)
            {
                TimeSpan diferenca = data2 - data1;

                retorno = diferenca.Days;
            }

            return retorno;
        }
    }
}
