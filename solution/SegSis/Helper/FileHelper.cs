﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Drawing;
using Ionic.Zip;

namespace SWS.Helper
{
    class FileHelper
    {
        public static byte[] CarregarArquivoImagem(string nomeArquivo, int ImagemTamanhoMaximo)
        {
            try
            {
                byte[] imagemBytes = null;
                string caminhoCompletoImagem = nomeArquivo;
                FileStream fs = new FileStream(caminhoCompletoImagem, FileMode.Open, FileAccess.Read);
                BinaryReader br = new BinaryReader(fs);
                imagemBytes = br.ReadBytes(ImagemTamanhoMaximo);
                return imagemBytes;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static byte[] CarregarArquivo(string nomeArquivo, int TamanhoMaximo)
        {
            try
            {
                byte[] bytes = null;
                string caminhoCompleto = nomeArquivo;
                FileStream fs = new FileStream(caminhoCompleto, FileMode.Open, FileAccess.Read);
                BinaryReader br = new BinaryReader(fs);
                bytes = br.ReadBytes(TamanhoMaximo);
                return bytes;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static Image byteArrayToImage(byte[] byteArrayIn)
        {
            MemoryStream ms = new MemoryStream(byteArrayIn);
            Image returnImage = Image.FromStream(ms);
            return returnImage;
        }

        public static void OpenInAnotherApp(byte[] data, string filename)
        {
            var tempFolder = System.IO.Path.GetTempPath();
            filename = System.IO.Path.Combine(tempFolder, filename);
            System.IO.File.WriteAllBytes(filename, data);
            System.Diagnostics.Process.Start(filename);
        }

        public static void criarArquivoZip(string[] arquivos, string localDestino)
        {
            using (ZipFile zip = new ZipFile())
            {
                foreach (string item in arquivos)
                {
                    zip.AddFile(item, "");
                }
                zip.Save(localDestino);
            }
        }
    }
}
