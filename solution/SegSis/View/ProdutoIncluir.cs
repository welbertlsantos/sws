﻿using SWS.Entidade;
using SWS.Facade;
using SWS.View.Resources;
using SWS.View.ViewHelper;
using SWS.ViewHelper;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace SWS.View
{
    public partial class frmProdutoIncluir : frmTemplate
    {
        protected Produto produto;

        public Produto Produto
        {
            get { return produto; }
            set { produto = value; }
        }
        
        public frmProdutoIncluir()
        {
            InitializeComponent();
            ActiveControl = textNome;
            ComboHelper.Boolean(cbPadraoContrato, false);
        }

        protected void textPreco_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = ValidaCampoHelper.FormataDigitacaoNumero(sender, e, textPreco.Text, 2);
        }

        protected void textCusto_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = ValidaCampoHelper.FormataDigitacaoNumero(sender, e, textCusto.Text, 2);
        }

        protected void textPreco_Enter(object sender, EventArgs e)
        {
            textPreco.Tag = textPreco.Text;
            textPreco.Text = string.Empty;
        }

        protected void textPreco_Leave(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(textPreco.Text))
                textPreco.Text = ValidaCampoHelper.FormataValorMonetario(textPreco.Tag.ToString());
            else
                textPreco.Text = ValidaCampoHelper.FormataValorMonetario(textPreco.Text);
        }

        protected void textCusto_Enter(object sender, EventArgs e)
        {
            textCusto.Tag = textCusto.Text;
            textCusto.Text = string.Empty;
        }

        protected void textCusto_Leave(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(textCusto.Text))
                textCusto.Text = ValidaCampoHelper.FormataValorMonetario(textCusto.Tag.ToString());
            else
                textCusto.Text = ValidaCampoHelper.FormataValorMonetario(textCusto.Text);
        }

        protected virtual void btnIncluir_Click(object sender, EventArgs e)
        {
            try
            {
                if (ValidaCampoTela())
                {
                    FinanceiroFacade financeiroFacade = FinanceiroFacade.getInstance();
                    produto = financeiroFacade.insertProduto(new Produto(null, textNome.Text, ApplicationConstants.ATIVO, (decimal?)Convert.ToDecimal(textPreco.Text), ApplicationConstants.EVENTUAL, (decimal?)Convert.ToDecimal(textCusto.Text), Convert.ToBoolean(((SelectItem)cbPadraoContrato.SelectedItem).Valor), textDescricao.Text));

                    MessageBox.Show("Produto incluído com sucesso.", "Confirmação", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    this.Close();

                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        protected void btn_fechar_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        protected void frmProdutoIncluir_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter || e.KeyCode == Keys.Enter)
                SendKeys.Send("{TAB}");
        }

        protected bool ValidaCampoTela()
        {
            bool resultado = true;

            try
            {
                if (string.IsNullOrEmpty(textNome.Text.Trim()))
                    throw new Exception("Campo obrigatório.");

            }
            catch (Exception ex)
            {
                resultado = false;
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

            return resultado;
        }
    }
}
