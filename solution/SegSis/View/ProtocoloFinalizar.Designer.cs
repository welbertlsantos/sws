﻿namespace SWS.View
{
    partial class frmProtocoloFinalizar
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            this.flpAcao = new System.Windows.Forms.FlowLayoutPanel();
            this.btnConfirmar = new System.Windows.Forms.Button();
            this.btnFechar = new System.Windows.Forms.Button();
            this.textDataGravacao = new System.Windows.Forms.TextBox();
            this.lblDataGravacao = new System.Windows.Forms.TextBox();
            this.textCodigo = new System.Windows.Forms.MaskedTextBox();
            this.lblCodigo = new System.Windows.Forms.TextBox();
            this.grbItens = new System.Windows.Forms.GroupBox();
            this.dgvItens = new System.Windows.Forms.DataGridView();
            this.lblEnviado = new System.Windows.Forms.TextBox();
            this.lblDocumentoEnvio = new System.Windows.Forms.TextBox();
            this.lblObservacao = new System.Windows.Forms.TextBox();
            this.textEnviado = new System.Windows.Forms.TextBox();
            this.textDocumentoEnvio = new System.Windows.Forms.TextBox();
            this.textObservacao = new System.Windows.Forms.TextBox();
            ((System.ComponentModel.ISupportInitialize)(this.spForm)).BeginInit();
            this.spForm.Panel1.SuspendLayout();
            this.spForm.Panel2.SuspendLayout();
            this.spForm.SuspendLayout();
            this.pnlForm.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.banner)).BeginInit();
            this.flpAcao.SuspendLayout();
            this.grbItens.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvItens)).BeginInit();
            this.SuspendLayout();
            // 
            // spForm
            // 
            // 
            // spForm.Panel1
            // 
            this.spForm.Panel1.Controls.Add(this.flpAcao);
            // 
            // pnlForm
            // 
            this.pnlForm.Controls.Add(this.textObservacao);
            this.pnlForm.Controls.Add(this.textDocumentoEnvio);
            this.pnlForm.Controls.Add(this.textEnviado);
            this.pnlForm.Controls.Add(this.lblObservacao);
            this.pnlForm.Controls.Add(this.lblDocumentoEnvio);
            this.pnlForm.Controls.Add(this.lblEnviado);
            this.pnlForm.Controls.Add(this.grbItens);
            this.pnlForm.Controls.Add(this.textDataGravacao);
            this.pnlForm.Controls.Add(this.lblDataGravacao);
            this.pnlForm.Controls.Add(this.textCodigo);
            this.pnlForm.Controls.Add(this.lblCodigo);
            // 
            // flpAcao
            // 
            this.flpAcao.Controls.Add(this.btnConfirmar);
            this.flpAcao.Controls.Add(this.btnFechar);
            this.flpAcao.Dock = System.Windows.Forms.DockStyle.Fill;
            this.flpAcao.Location = new System.Drawing.Point(0, 0);
            this.flpAcao.Name = "flpAcao";
            this.flpAcao.Size = new System.Drawing.Size(784, 35);
            this.flpAcao.TabIndex = 0;
            // 
            // btnConfirmar
            // 
            this.btnConfirmar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnConfirmar.Image = global::SWS.Properties.Resources.fechar_ico;
            this.btnConfirmar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnConfirmar.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.btnConfirmar.Location = new System.Drawing.Point(3, 3);
            this.btnConfirmar.Name = "btnConfirmar";
            this.btnConfirmar.Size = new System.Drawing.Size(75, 23);
            this.btnConfirmar.TabIndex = 1;
            this.btnConfirmar.Text = "&Confirmar";
            this.btnConfirmar.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnConfirmar.UseVisualStyleBackColor = true;
            this.btnConfirmar.Click += new System.EventHandler(this.btnConfirmar_Click);
            // 
            // btnFechar
            // 
            this.btnFechar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnFechar.Image = global::SWS.Properties.Resources.close;
            this.btnFechar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnFechar.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.btnFechar.Location = new System.Drawing.Point(84, 3);
            this.btnFechar.Name = "btnFechar";
            this.btnFechar.Size = new System.Drawing.Size(75, 23);
            this.btnFechar.TabIndex = 7;
            this.btnFechar.TabStop = false;
            this.btnFechar.Text = "&Fechar";
            this.btnFechar.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnFechar.UseVisualStyleBackColor = true;
            this.btnFechar.Click += new System.EventHandler(this.btnFechar_Click);
            // 
            // textDataGravacao
            // 
            this.textDataGravacao.BackColor = System.Drawing.Color.WhiteSmoke;
            this.textDataGravacao.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textDataGravacao.Location = new System.Drawing.Point(140, 23);
            this.textDataGravacao.Multiline = true;
            this.textDataGravacao.Name = "textDataGravacao";
            this.textDataGravacao.ReadOnly = true;
            this.textDataGravacao.Size = new System.Drawing.Size(137, 21);
            this.textDataGravacao.TabIndex = 49;
            this.textDataGravacao.TabStop = false;
            // 
            // lblDataGravacao
            // 
            this.lblDataGravacao.BackColor = System.Drawing.Color.LightGray;
            this.lblDataGravacao.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblDataGravacao.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDataGravacao.Location = new System.Drawing.Point(3, 23);
            this.lblDataGravacao.Name = "lblDataGravacao";
            this.lblDataGravacao.ReadOnly = true;
            this.lblDataGravacao.Size = new System.Drawing.Size(138, 21);
            this.lblDataGravacao.TabIndex = 48;
            this.lblDataGravacao.TabStop = false;
            this.lblDataGravacao.Text = "Data de Gravação";
            // 
            // textCodigo
            // 
            this.textCodigo.BackColor = System.Drawing.Color.WhiteSmoke;
            this.textCodigo.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textCodigo.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textCodigo.Location = new System.Drawing.Point(140, 3);
            this.textCodigo.Mask = "9999,99,999999";
            this.textCodigo.Name = "textCodigo";
            this.textCodigo.PromptChar = ' ';
            this.textCodigo.ReadOnly = true;
            this.textCodigo.Size = new System.Drawing.Size(137, 21);
            this.textCodigo.TabIndex = 46;
            this.textCodigo.TabStop = false;
            // 
            // lblCodigo
            // 
            this.lblCodigo.BackColor = System.Drawing.Color.LightGray;
            this.lblCodigo.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblCodigo.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCodigo.Location = new System.Drawing.Point(3, 3);
            this.lblCodigo.Name = "lblCodigo";
            this.lblCodigo.ReadOnly = true;
            this.lblCodigo.Size = new System.Drawing.Size(138, 21);
            this.lblCodigo.TabIndex = 47;
            this.lblCodigo.TabStop = false;
            this.lblCodigo.Text = "Código";
            // 
            // grbItens
            // 
            this.grbItens.Controls.Add(this.dgvItens);
            this.grbItens.Location = new System.Drawing.Point(3, 50);
            this.grbItens.Name = "grbItens";
            this.grbItens.Size = new System.Drawing.Size(758, 340);
            this.grbItens.TabIndex = 51;
            this.grbItens.TabStop = false;
            this.grbItens.Text = "Itens do protocolo";
            // 
            // dgvItens
            // 
            this.dgvItens.AllowUserToAddRows = false;
            this.dgvItens.AllowUserToDeleteRows = false;
            this.dgvItens.AllowUserToOrderColumns = true;
            this.dgvItens.AllowUserToResizeRows = false;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.Gainsboro;
            this.dgvItens.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle1;
            this.dgvItens.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.DisplayedCells;
            this.dgvItens.BackgroundColor = System.Drawing.Color.White;
            this.dgvItens.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.Color.LightGray;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvItens.DefaultCellStyle = dataGridViewCellStyle2;
            this.dgvItens.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvItens.Location = new System.Drawing.Point(3, 16);
            this.dgvItens.MultiSelect = false;
            this.dgvItens.Name = "dgvItens";
            this.dgvItens.ReadOnly = true;
            this.dgvItens.RowHeadersVisible = false;
            this.dgvItens.RowHeadersWidth = 25;
            this.dgvItens.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvItens.Size = new System.Drawing.Size(752, 321);
            this.dgvItens.TabIndex = 51;
            this.dgvItens.TabStop = false;
            this.dgvItens.RowPrePaint += new System.Windows.Forms.DataGridViewRowPrePaintEventHandler(this.dgvItens_RowPrePaint);
            // 
            // lblEnviado
            // 
            this.lblEnviado.BackColor = System.Drawing.Color.LightGray;
            this.lblEnviado.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblEnviado.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblEnviado.Location = new System.Drawing.Point(6, 396);
            this.lblEnviado.Name = "lblEnviado";
            this.lblEnviado.ReadOnly = true;
            this.lblEnviado.Size = new System.Drawing.Size(138, 21);
            this.lblEnviado.TabIndex = 52;
            this.lblEnviado.TabStop = false;
            this.lblEnviado.Text = "Envio";
            // 
            // lblDocumentoEnvio
            // 
            this.lblDocumentoEnvio.BackColor = System.Drawing.Color.LightGray;
            this.lblDocumentoEnvio.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblDocumentoEnvio.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDocumentoEnvio.Location = new System.Drawing.Point(6, 416);
            this.lblDocumentoEnvio.Name = "lblDocumentoEnvio";
            this.lblDocumentoEnvio.ReadOnly = true;
            this.lblDocumentoEnvio.Size = new System.Drawing.Size(138, 21);
            this.lblDocumentoEnvio.TabIndex = 53;
            this.lblDocumentoEnvio.TabStop = false;
            this.lblDocumentoEnvio.Text = "Documento envio";
            // 
            // lblObservacao
            // 
            this.lblObservacao.BackColor = System.Drawing.Color.LightGray;
            this.lblObservacao.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblObservacao.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblObservacao.Location = new System.Drawing.Point(6, 436);
            this.lblObservacao.Name = "lblObservacao";
            this.lblObservacao.ReadOnly = true;
            this.lblObservacao.Size = new System.Drawing.Size(138, 21);
            this.lblObservacao.TabIndex = 54;
            this.lblObservacao.TabStop = false;
            this.lblObservacao.Text = "Observação";
            // 
            // textEnviado
            // 
            this.textEnviado.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textEnviado.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.textEnviado.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textEnviado.Location = new System.Drawing.Point(143, 396);
            this.textEnviado.MaxLength = 100;
            this.textEnviado.Name = "textEnviado";
            this.textEnviado.Size = new System.Drawing.Size(618, 21);
            this.textEnviado.TabIndex = 1;
            // 
            // textDocumentoEnvio
            // 
            this.textDocumentoEnvio.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textDocumentoEnvio.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.textDocumentoEnvio.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textDocumentoEnvio.Location = new System.Drawing.Point(143, 416);
            this.textDocumentoEnvio.MaxLength = 100;
            this.textDocumentoEnvio.Name = "textDocumentoEnvio";
            this.textDocumentoEnvio.Size = new System.Drawing.Size(618, 21);
            this.textDocumentoEnvio.TabIndex = 2;
            // 
            // textObservacao
            // 
            this.textObservacao.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textObservacao.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.textObservacao.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textObservacao.Location = new System.Drawing.Point(143, 436);
            this.textObservacao.MaxLength = 455;
            this.textObservacao.Name = "textObservacao";
            this.textObservacao.Size = new System.Drawing.Size(618, 21);
            this.textObservacao.TabIndex = 3;
            // 
            // frmProtocoloFinalizar
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(784, 561);
            this.Name = "frmProtocoloFinalizar";
            this.Text = "FINALIZAR PROTOCOLO";
            this.spForm.Panel1.ResumeLayout(false);
            this.spForm.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.spForm)).EndInit();
            this.spForm.ResumeLayout(false);
            this.pnlForm.ResumeLayout(false);
            this.pnlForm.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.banner)).EndInit();
            this.flpAcao.ResumeLayout(false);
            this.grbItens.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvItens)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.FlowLayoutPanel flpAcao;
        protected System.Windows.Forms.TextBox textDataGravacao;
        protected System.Windows.Forms.TextBox lblDataGravacao;
        protected System.Windows.Forms.MaskedTextBox textCodigo;
        protected System.Windows.Forms.TextBox lblCodigo;
        private System.Windows.Forms.Button btnConfirmar;
        private System.Windows.Forms.Button btnFechar;
        private System.Windows.Forms.GroupBox grbItens;
        private System.Windows.Forms.DataGridView dgvItens;
        protected System.Windows.Forms.TextBox lblEnviado;
        protected System.Windows.Forms.TextBox lblDocumentoEnvio;
        protected System.Windows.Forms.TextBox lblObservacao;
        private System.Windows.Forms.TextBox textObservacao;
        private System.Windows.Forms.TextBox textDocumentoEnvio;
        private System.Windows.Forms.TextBox textEnviado;
    }
}