﻿namespace SWS.View
{
    partial class frmContratos
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            this.flpAcao = new System.Windows.Forms.FlowLayoutPanel();
            this.btnIncluir = new System.Windows.Forms.Button();
            this.btnAlterar = new System.Windows.Forms.Button();
            this.btnExcluir = new System.Windows.Forms.Button();
            this.btnValidar = new System.Windows.Forms.Button();
            this.btnEncerrar = new System.Windows.Forms.Button();
            this.btnConsultar = new System.Windows.Forms.Button();
            this.btnPesquisar = new System.Windows.Forms.Button();
            this.btnLimpar = new System.Windows.Forms.Button();
            this.btnImprimir = new System.Windows.Forms.Button();
            this.btnFechar = new System.Windows.Forms.Button();
            this.lblCodigo = new System.Windows.Forms.TextBox();
            this.lblTipo = new System.Windows.Forms.TextBox();
            this.lblSituacao = new System.Windows.Forms.TextBox();
            this.lblCliente = new System.Windows.Forms.TextBox();
            this.lblDataElaboracao = new System.Windows.Forms.TextBox();
            this.lblDataFechamento = new System.Windows.Forms.TextBox();
            this.lblDataVencimento = new System.Windows.Forms.TextBox();
            this.lblDataCancelamento = new System.Windows.Forms.TextBox();
            this.textCodigo = new System.Windows.Forms.MaskedTextBox();
            this.cbTipo = new SWS.ComboBoxWithBorder();
            this.cbSituacao = new SWS.ComboBoxWithBorder();
            this.btnCliente = new System.Windows.Forms.Button();
            this.textCliente = new System.Windows.Forms.TextBox();
            this.dtElaboracaoFinal = new System.Windows.Forms.DateTimePicker();
            this.dtElaboracaoInicial = new System.Windows.Forms.DateTimePicker();
            this.dtFechamentoFinal = new System.Windows.Forms.DateTimePicker();
            this.dtFechamentoInicial = new System.Windows.Forms.DateTimePicker();
            this.dtVencimentoFinal = new System.Windows.Forms.DateTimePicker();
            this.dtVencimentoInicial = new System.Windows.Forms.DateTimePicker();
            this.dtCancelamentoFinal = new System.Windows.Forms.DateTimePicker();
            this.dtCancelamentoInicial = new System.Windows.Forms.DateTimePicker();
            this.grbContrato = new System.Windows.Forms.GroupBox();
            this.dgvContrato = new System.Windows.Forms.DataGridView();
            this.grbAditivo = new System.Windows.Forms.GroupBox();
            this.dgvAditivo = new System.Windows.Forms.DataGridView();
            this.lblLengendaFechado = new System.Windows.Forms.Label();
            this.lblCorFechado = new System.Windows.Forms.Label();
            this.lblLegendaEncerrado = new System.Windows.Forms.Label();
            this.lblCorEncerrado = new System.Windows.Forms.Label();
            this.lblLegendaConstrucao = new System.Windows.Forms.Label();
            this.lblCorContrucao = new System.Windows.Forms.Label();
            this.lblLegendaCancelado = new System.Windows.Forms.Label();
            this.lblCorCancelado = new System.Windows.Forms.Label();
            this.tpContrato = new System.Windows.Forms.ToolTip(this.components);
            this.btnExcluirCliente = new System.Windows.Forms.Button();
            this.btnCopiar = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.spForm)).BeginInit();
            this.spForm.Panel1.SuspendLayout();
            this.spForm.Panel2.SuspendLayout();
            this.spForm.SuspendLayout();
            this.pnlForm.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.banner)).BeginInit();
            this.flpAcao.SuspendLayout();
            this.grbContrato.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvContrato)).BeginInit();
            this.grbAditivo.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvAditivo)).BeginInit();
            this.SuspendLayout();
            // 
            // spForm
            // 
            // 
            // spForm.Panel1
            // 
            this.spForm.Panel1.Controls.Add(this.flpAcao);
            this.spForm.Panel1MinSize = 65;
            this.spForm.SplitterDistance = 65;
            // 
            // pnlForm
            // 
            this.pnlForm.Controls.Add(this.btnExcluirCliente);
            this.pnlForm.Controls.Add(this.lblLengendaFechado);
            this.pnlForm.Controls.Add(this.lblCorFechado);
            this.pnlForm.Controls.Add(this.lblLegendaEncerrado);
            this.pnlForm.Controls.Add(this.lblCorEncerrado);
            this.pnlForm.Controls.Add(this.lblLegendaConstrucao);
            this.pnlForm.Controls.Add(this.lblCorContrucao);
            this.pnlForm.Controls.Add(this.lblLegendaCancelado);
            this.pnlForm.Controls.Add(this.lblCorCancelado);
            this.pnlForm.Controls.Add(this.grbAditivo);
            this.pnlForm.Controls.Add(this.grbContrato);
            this.pnlForm.Controls.Add(this.dtCancelamentoFinal);
            this.pnlForm.Controls.Add(this.dtCancelamentoInicial);
            this.pnlForm.Controls.Add(this.dtVencimentoFinal);
            this.pnlForm.Controls.Add(this.dtVencimentoInicial);
            this.pnlForm.Controls.Add(this.dtFechamentoFinal);
            this.pnlForm.Controls.Add(this.dtFechamentoInicial);
            this.pnlForm.Controls.Add(this.dtElaboracaoFinal);
            this.pnlForm.Controls.Add(this.dtElaboracaoInicial);
            this.pnlForm.Controls.Add(this.btnCliente);
            this.pnlForm.Controls.Add(this.textCliente);
            this.pnlForm.Controls.Add(this.cbSituacao);
            this.pnlForm.Controls.Add(this.cbTipo);
            this.pnlForm.Controls.Add(this.textCodigo);
            this.pnlForm.Controls.Add(this.lblDataCancelamento);
            this.pnlForm.Controls.Add(this.lblDataVencimento);
            this.pnlForm.Controls.Add(this.lblDataFechamento);
            this.pnlForm.Controls.Add(this.lblDataElaboracao);
            this.pnlForm.Controls.Add(this.lblCliente);
            this.pnlForm.Controls.Add(this.lblSituacao);
            this.pnlForm.Controls.Add(this.lblTipo);
            this.pnlForm.Controls.Add(this.lblCodigo);
            // 
            // flpAcao
            // 
            this.flpAcao.Controls.Add(this.btnIncluir);
            this.flpAcao.Controls.Add(this.btnAlterar);
            this.flpAcao.Controls.Add(this.btnExcluir);
            this.flpAcao.Controls.Add(this.btnValidar);
            this.flpAcao.Controls.Add(this.btnEncerrar);
            this.flpAcao.Controls.Add(this.btnConsultar);
            this.flpAcao.Controls.Add(this.btnPesquisar);
            this.flpAcao.Controls.Add(this.btnLimpar);
            this.flpAcao.Controls.Add(this.btnImprimir);
            this.flpAcao.Controls.Add(this.btnCopiar);
            this.flpAcao.Controls.Add(this.btnFechar);
            this.flpAcao.Dock = System.Windows.Forms.DockStyle.Fill;
            this.flpAcao.Location = new System.Drawing.Point(0, 0);
            this.flpAcao.Name = "flpAcao";
            this.flpAcao.Size = new System.Drawing.Size(784, 65);
            this.flpAcao.TabIndex = 0;
            // 
            // btnIncluir
            // 
            this.btnIncluir.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnIncluir.Image = global::SWS.Properties.Resources.icone_mais;
            this.btnIncluir.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnIncluir.Location = new System.Drawing.Point(3, 3);
            this.btnIncluir.Name = "btnIncluir";
            this.btnIncluir.Size = new System.Drawing.Size(70, 23);
            this.btnIncluir.TabIndex = 8;
            this.btnIncluir.TabStop = false;
            this.btnIncluir.Text = "&Incluir";
            this.btnIncluir.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnIncluir.UseVisualStyleBackColor = true;
            this.btnIncluir.Click += new System.EventHandler(this.btnIncluir_Click);
            // 
            // btnAlterar
            // 
            this.btnAlterar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnAlterar.Image = global::SWS.Properties.Resources.Alterar;
            this.btnAlterar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnAlterar.Location = new System.Drawing.Point(79, 3);
            this.btnAlterar.Name = "btnAlterar";
            this.btnAlterar.Size = new System.Drawing.Size(70, 23);
            this.btnAlterar.TabIndex = 9;
            this.btnAlterar.TabStop = false;
            this.btnAlterar.Text = "&Alterar";
            this.btnAlterar.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnAlterar.UseVisualStyleBackColor = true;
            this.btnAlterar.Click += new System.EventHandler(this.btnAlterar_Click);
            // 
            // btnExcluir
            // 
            this.btnExcluir.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnExcluir.Image = global::SWS.Properties.Resources.lixeira;
            this.btnExcluir.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnExcluir.Location = new System.Drawing.Point(155, 3);
            this.btnExcluir.Name = "btnExcluir";
            this.btnExcluir.Size = new System.Drawing.Size(70, 23);
            this.btnExcluir.TabIndex = 10;
            this.btnExcluir.TabStop = false;
            this.btnExcluir.Text = "&Excluir";
            this.btnExcluir.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnExcluir.UseVisualStyleBackColor = true;
            this.btnExcluir.Click += new System.EventHandler(this.btnExcluir_Click);
            // 
            // btnValidar
            // 
            this.btnValidar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnValidar.Image = global::SWS.Properties.Resources.fechar_ico;
            this.btnValidar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnValidar.Location = new System.Drawing.Point(231, 3);
            this.btnValidar.Name = "btnValidar";
            this.btnValidar.Size = new System.Drawing.Size(70, 23);
            this.btnValidar.TabIndex = 11;
            this.btnValidar.TabStop = false;
            this.btnValidar.Text = "&Validar";
            this.btnValidar.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnValidar.UseVisualStyleBackColor = true;
            this.btnValidar.Click += new System.EventHandler(this.btnValidar_Click);
            // 
            // btnEncerrar
            // 
            this.btnEncerrar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnEncerrar.Image = global::SWS.Properties.Resources.Icone_encerrar_png;
            this.btnEncerrar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnEncerrar.Location = new System.Drawing.Point(307, 3);
            this.btnEncerrar.Name = "btnEncerrar";
            this.btnEncerrar.Size = new System.Drawing.Size(70, 23);
            this.btnEncerrar.TabIndex = 15;
            this.btnEncerrar.TabStop = false;
            this.btnEncerrar.Text = "&Encerrar";
            this.btnEncerrar.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnEncerrar.UseVisualStyleBackColor = true;
            this.btnEncerrar.Click += new System.EventHandler(this.btnEncerrar_Click);
            // 
            // btnConsultar
            // 
            this.btnConsultar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnConsultar.Image = global::SWS.Properties.Resources.lupa;
            this.btnConsultar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnConsultar.Location = new System.Drawing.Point(383, 3);
            this.btnConsultar.Name = "btnConsultar";
            this.btnConsultar.Size = new System.Drawing.Size(70, 23);
            this.btnConsultar.TabIndex = 13;
            this.btnConsultar.TabStop = false;
            this.btnConsultar.Text = "&Consultar";
            this.btnConsultar.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnConsultar.UseVisualStyleBackColor = true;
            this.btnConsultar.Click += new System.EventHandler(this.btnConsultar_Click);
            // 
            // btnPesquisar
            // 
            this.btnPesquisar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnPesquisar.Image = global::SWS.Properties.Resources.lupa;
            this.btnPesquisar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnPesquisar.Location = new System.Drawing.Point(459, 3);
            this.btnPesquisar.Name = "btnPesquisar";
            this.btnPesquisar.Size = new System.Drawing.Size(70, 23);
            this.btnPesquisar.TabIndex = 16;
            this.btnPesquisar.Text = "&Pesquisar";
            this.btnPesquisar.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnPesquisar.UseVisualStyleBackColor = true;
            this.btnPesquisar.Click += new System.EventHandler(this.btnPesquisar_Click);
            // 
            // btnLimpar
            // 
            this.btnLimpar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnLimpar.Image = global::SWS.Properties.Resources.vassoura;
            this.btnLimpar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnLimpar.Location = new System.Drawing.Point(535, 3);
            this.btnLimpar.Name = "btnLimpar";
            this.btnLimpar.Size = new System.Drawing.Size(70, 23);
            this.btnLimpar.TabIndex = 17;
            this.btnLimpar.Text = "&Limpar";
            this.btnLimpar.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnLimpar.UseVisualStyleBackColor = true;
            this.btnLimpar.Click += new System.EventHandler(this.btnLimpar_Click);
            // 
            // btnImprimir
            // 
            this.btnImprimir.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnImprimir.Image = global::SWS.Properties.Resources.impressora1;
            this.btnImprimir.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnImprimir.Location = new System.Drawing.Point(611, 3);
            this.btnImprimir.Name = "btnImprimir";
            this.btnImprimir.Size = new System.Drawing.Size(70, 23);
            this.btnImprimir.TabIndex = 12;
            this.btnImprimir.TabStop = false;
            this.btnImprimir.Text = "&Imprimir";
            this.btnImprimir.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnImprimir.UseVisualStyleBackColor = true;
            this.btnImprimir.Click += new System.EventHandler(this.btnImprimir_Click);
            // 
            // btnFechar
            // 
            this.btnFechar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnFechar.Image = global::SWS.Properties.Resources.close;
            this.btnFechar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnFechar.Location = new System.Drawing.Point(3, 32);
            this.btnFechar.Name = "btnFechar";
            this.btnFechar.Size = new System.Drawing.Size(70, 23);
            this.btnFechar.TabIndex = 14;
            this.btnFechar.TabStop = false;
            this.btnFechar.Text = "&Fechar";
            this.btnFechar.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnFechar.UseVisualStyleBackColor = true;
            this.btnFechar.Click += new System.EventHandler(this.btnFechar_Click);
            // 
            // lblCodigo
            // 
            this.lblCodigo.BackColor = System.Drawing.Color.LightGray;
            this.lblCodigo.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblCodigo.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCodigo.Location = new System.Drawing.Point(8, 19);
            this.lblCodigo.Name = "lblCodigo";
            this.lblCodigo.ReadOnly = true;
            this.lblCodigo.Size = new System.Drawing.Size(117, 21);
            this.lblCodigo.TabIndex = 0;
            this.lblCodigo.TabStop = false;
            this.lblCodigo.Text = "Código";
            // 
            // lblTipo
            // 
            this.lblTipo.BackColor = System.Drawing.Color.LightGray;
            this.lblTipo.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblTipo.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTipo.Location = new System.Drawing.Point(8, 39);
            this.lblTipo.Name = "lblTipo";
            this.lblTipo.ReadOnly = true;
            this.lblTipo.Size = new System.Drawing.Size(117, 21);
            this.lblTipo.TabIndex = 1;
            this.lblTipo.TabStop = false;
            this.lblTipo.Text = "Tipo";
            // 
            // lblSituacao
            // 
            this.lblSituacao.BackColor = System.Drawing.Color.LightGray;
            this.lblSituacao.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblSituacao.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSituacao.Location = new System.Drawing.Point(8, 59);
            this.lblSituacao.Name = "lblSituacao";
            this.lblSituacao.ReadOnly = true;
            this.lblSituacao.Size = new System.Drawing.Size(117, 21);
            this.lblSituacao.TabIndex = 2;
            this.lblSituacao.TabStop = false;
            this.lblSituacao.Text = "Situação";
            // 
            // lblCliente
            // 
            this.lblCliente.BackColor = System.Drawing.Color.LightGray;
            this.lblCliente.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblCliente.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCliente.Location = new System.Drawing.Point(8, 79);
            this.lblCliente.Name = "lblCliente";
            this.lblCliente.ReadOnly = true;
            this.lblCliente.Size = new System.Drawing.Size(117, 21);
            this.lblCliente.TabIndex = 3;
            this.lblCliente.TabStop = false;
            this.lblCliente.Text = "Cliente";
            // 
            // lblDataElaboracao
            // 
            this.lblDataElaboracao.BackColor = System.Drawing.Color.LightGray;
            this.lblDataElaboracao.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblDataElaboracao.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDataElaboracao.Location = new System.Drawing.Point(436, 19);
            this.lblDataElaboracao.Name = "lblDataElaboracao";
            this.lblDataElaboracao.ReadOnly = true;
            this.lblDataElaboracao.Size = new System.Drawing.Size(117, 21);
            this.lblDataElaboracao.TabIndex = 4;
            this.lblDataElaboracao.TabStop = false;
            this.lblDataElaboracao.Text = "Data elaboração";
            // 
            // lblDataFechamento
            // 
            this.lblDataFechamento.BackColor = System.Drawing.Color.LightGray;
            this.lblDataFechamento.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblDataFechamento.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDataFechamento.Location = new System.Drawing.Point(436, 39);
            this.lblDataFechamento.Name = "lblDataFechamento";
            this.lblDataFechamento.ReadOnly = true;
            this.lblDataFechamento.Size = new System.Drawing.Size(117, 21);
            this.lblDataFechamento.TabIndex = 5;
            this.lblDataFechamento.TabStop = false;
            this.lblDataFechamento.Text = "Data fechamento";
            // 
            // lblDataVencimento
            // 
            this.lblDataVencimento.BackColor = System.Drawing.Color.LightGray;
            this.lblDataVencimento.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblDataVencimento.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDataVencimento.Location = new System.Drawing.Point(436, 59);
            this.lblDataVencimento.Name = "lblDataVencimento";
            this.lblDataVencimento.ReadOnly = true;
            this.lblDataVencimento.Size = new System.Drawing.Size(117, 21);
            this.lblDataVencimento.TabIndex = 6;
            this.lblDataVencimento.TabStop = false;
            this.lblDataVencimento.Text = "Data vencimento";
            // 
            // lblDataCancelamento
            // 
            this.lblDataCancelamento.BackColor = System.Drawing.Color.LightGray;
            this.lblDataCancelamento.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblDataCancelamento.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDataCancelamento.Location = new System.Drawing.Point(436, 79);
            this.lblDataCancelamento.Name = "lblDataCancelamento";
            this.lblDataCancelamento.ReadOnly = true;
            this.lblDataCancelamento.Size = new System.Drawing.Size(117, 21);
            this.lblDataCancelamento.TabIndex = 7;
            this.lblDataCancelamento.TabStop = false;
            this.lblDataCancelamento.Text = "Data cancelamento";
            // 
            // textCodigo
            // 
            this.textCodigo.BackColor = System.Drawing.Color.White;
            this.textCodigo.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textCodigo.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textCodigo.ForeColor = System.Drawing.Color.Blue;
            this.textCodigo.Location = new System.Drawing.Point(124, 19);
            this.textCodigo.Mask = "0000,00,000000/00";
            this.textCodigo.Name = "textCodigo";
            this.textCodigo.PromptChar = ' ';
            this.textCodigo.Size = new System.Drawing.Size(297, 21);
            this.textCodigo.TabIndex = 1;
            // 
            // cbTipo
            // 
            this.cbTipo.BackColor = System.Drawing.Color.LightGray;
            this.cbTipo.BorderColor = System.Drawing.Color.DimGray;
            this.cbTipo.BorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid;
            this.cbTipo.FormattingEnabled = true;
            this.cbTipo.Location = new System.Drawing.Point(124, 39);
            this.cbTipo.Name = "cbTipo";
            this.cbTipo.Size = new System.Drawing.Size(297, 21);
            this.cbTipo.TabIndex = 2;
            // 
            // cbSituacao
            // 
            this.cbSituacao.BackColor = System.Drawing.Color.LightGray;
            this.cbSituacao.BorderColor = System.Drawing.Color.DimGray;
            this.cbSituacao.BorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid;
            this.cbSituacao.FormattingEnabled = true;
            this.cbSituacao.Location = new System.Drawing.Point(124, 59);
            this.cbSituacao.Name = "cbSituacao";
            this.cbSituacao.Size = new System.Drawing.Size(297, 21);
            this.cbSituacao.TabIndex = 3;
            // 
            // btnCliente
            // 
            this.btnCliente.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnCliente.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCliente.Image = global::SWS.Properties.Resources.busca;
            this.btnCliente.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnCliente.Location = new System.Drawing.Point(368, 79);
            this.btnCliente.Name = "btnCliente";
            this.btnCliente.Size = new System.Drawing.Size(27, 21);
            this.btnCliente.TabIndex = 4;
            this.btnCliente.TextAlign = System.Drawing.ContentAlignment.TopRight;
            this.btnCliente.UseVisualStyleBackColor = true;
            this.btnCliente.Click += new System.EventHandler(this.btnCliente_Click);
            // 
            // textCliente
            // 
            this.textCliente.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.textCliente.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textCliente.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textCliente.Location = new System.Drawing.Point(124, 79);
            this.textCliente.Name = "textCliente";
            this.textCliente.ReadOnly = true;
            this.textCliente.Size = new System.Drawing.Size(246, 21);
            this.textCliente.TabIndex = 11;
            // 
            // dtElaboracaoFinal
            // 
            this.dtElaboracaoFinal.Checked = false;
            this.dtElaboracaoFinal.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtElaboracaoFinal.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtElaboracaoFinal.Location = new System.Drawing.Point(651, 19);
            this.dtElaboracaoFinal.Name = "dtElaboracaoFinal";
            this.dtElaboracaoFinal.ShowCheckBox = true;
            this.dtElaboracaoFinal.Size = new System.Drawing.Size(100, 21);
            this.dtElaboracaoFinal.TabIndex = 6;
            // 
            // dtElaboracaoInicial
            // 
            this.dtElaboracaoInicial.Checked = false;
            this.dtElaboracaoInicial.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtElaboracaoInicial.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtElaboracaoInicial.Location = new System.Drawing.Point(552, 19);
            this.dtElaboracaoInicial.Name = "dtElaboracaoInicial";
            this.dtElaboracaoInicial.ShowCheckBox = true;
            this.dtElaboracaoInicial.Size = new System.Drawing.Size(100, 21);
            this.dtElaboracaoInicial.TabIndex = 5;
            // 
            // dtFechamentoFinal
            // 
            this.dtFechamentoFinal.Checked = false;
            this.dtFechamentoFinal.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtFechamentoFinal.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtFechamentoFinal.Location = new System.Drawing.Point(651, 39);
            this.dtFechamentoFinal.Name = "dtFechamentoFinal";
            this.dtFechamentoFinal.ShowCheckBox = true;
            this.dtFechamentoFinal.Size = new System.Drawing.Size(100, 21);
            this.dtFechamentoFinal.TabIndex = 8;
            // 
            // dtFechamentoInicial
            // 
            this.dtFechamentoInicial.Checked = false;
            this.dtFechamentoInicial.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtFechamentoInicial.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtFechamentoInicial.Location = new System.Drawing.Point(552, 39);
            this.dtFechamentoInicial.Name = "dtFechamentoInicial";
            this.dtFechamentoInicial.ShowCheckBox = true;
            this.dtFechamentoInicial.Size = new System.Drawing.Size(100, 21);
            this.dtFechamentoInicial.TabIndex = 7;
            // 
            // dtVencimentoFinal
            // 
            this.dtVencimentoFinal.Checked = false;
            this.dtVencimentoFinal.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtVencimentoFinal.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtVencimentoFinal.Location = new System.Drawing.Point(651, 59);
            this.dtVencimentoFinal.Name = "dtVencimentoFinal";
            this.dtVencimentoFinal.ShowCheckBox = true;
            this.dtVencimentoFinal.Size = new System.Drawing.Size(100, 21);
            this.dtVencimentoFinal.TabIndex = 10;
            // 
            // dtVencimentoInicial
            // 
            this.dtVencimentoInicial.Checked = false;
            this.dtVencimentoInicial.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtVencimentoInicial.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtVencimentoInicial.Location = new System.Drawing.Point(552, 59);
            this.dtVencimentoInicial.Name = "dtVencimentoInicial";
            this.dtVencimentoInicial.ShowCheckBox = true;
            this.dtVencimentoInicial.Size = new System.Drawing.Size(100, 21);
            this.dtVencimentoInicial.TabIndex = 9;
            // 
            // dtCancelamentoFinal
            // 
            this.dtCancelamentoFinal.Checked = false;
            this.dtCancelamentoFinal.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtCancelamentoFinal.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtCancelamentoFinal.Location = new System.Drawing.Point(651, 79);
            this.dtCancelamentoFinal.Name = "dtCancelamentoFinal";
            this.dtCancelamentoFinal.ShowCheckBox = true;
            this.dtCancelamentoFinal.Size = new System.Drawing.Size(100, 21);
            this.dtCancelamentoFinal.TabIndex = 12;
            // 
            // dtCancelamentoInicial
            // 
            this.dtCancelamentoInicial.Checked = false;
            this.dtCancelamentoInicial.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtCancelamentoInicial.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtCancelamentoInicial.Location = new System.Drawing.Point(552, 79);
            this.dtCancelamentoInicial.Name = "dtCancelamentoInicial";
            this.dtCancelamentoInicial.ShowCheckBox = true;
            this.dtCancelamentoInicial.Size = new System.Drawing.Size(100, 21);
            this.dtCancelamentoInicial.TabIndex = 11;
            // 
            // grbContrato
            // 
            this.grbContrato.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.grbContrato.Controls.Add(this.dgvContrato);
            this.grbContrato.Location = new System.Drawing.Point(8, 106);
            this.grbContrato.Name = "grbContrato";
            this.grbContrato.Size = new System.Drawing.Size(745, 197);
            this.grbContrato.TabIndex = 2;
            this.grbContrato.TabStop = false;
            this.grbContrato.Text = "Contratos e Propostas";
            // 
            // dgvContrato
            // 
            this.dgvContrato.AllowUserToAddRows = false;
            this.dgvContrato.AllowUserToDeleteRows = false;
            this.dgvContrato.AllowUserToOrderColumns = true;
            this.dgvContrato.AllowUserToResizeRows = false;
            this.dgvContrato.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this.dgvContrato.BackgroundColor = System.Drawing.Color.White;
            this.dgvContrato.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.Color.LightGray;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvContrato.DefaultCellStyle = dataGridViewCellStyle2;
            this.dgvContrato.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvContrato.Location = new System.Drawing.Point(3, 16);
            this.dgvContrato.MultiSelect = false;
            this.dgvContrato.Name = "dgvContrato";
            this.dgvContrato.ReadOnly = true;
            this.dgvContrato.RowHeadersWidth = 30;
            this.dgvContrato.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvContrato.Size = new System.Drawing.Size(739, 178);
            this.dgvContrato.TabIndex = 13;
            this.tpContrato.SetToolTip(this.dgvContrato, "Duplo click no contrato para pesquisar seus aditivos");
            this.dgvContrato.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvContrato_CellContentClick);
            this.dgvContrato.CellEnter += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvContrato_CellEnter);
            this.dgvContrato.CellMouseDoubleClick += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.dgvContrato_CellMouseDoubleClick);
            this.dgvContrato.DataBindingComplete += new System.Windows.Forms.DataGridViewBindingCompleteEventHandler(this.dgvContrato_DataBindingComplete);
            this.dgvContrato.RowPrePaint += new System.Windows.Forms.DataGridViewRowPrePaintEventHandler(this.dgvContrato_RowPrePaint);
            // 
            // grbAditivo
            // 
            this.grbAditivo.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.grbAditivo.Controls.Add(this.dgvAditivo);
            this.grbAditivo.Location = new System.Drawing.Point(8, 309);
            this.grbAditivo.Name = "grbAditivo";
            this.grbAditivo.Size = new System.Drawing.Size(745, 124);
            this.grbAditivo.TabIndex = 21;
            this.grbAditivo.TabStop = false;
            this.grbAditivo.Text = "Aditivo";
            // 
            // dgvAditivo
            // 
            this.dgvAditivo.AllowUserToAddRows = false;
            this.dgvAditivo.AllowUserToDeleteRows = false;
            this.dgvAditivo.AllowUserToOrderColumns = true;
            this.dgvAditivo.AllowUserToResizeRows = false;
            this.dgvAditivo.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this.dgvAditivo.BackgroundColor = System.Drawing.Color.White;
            this.dgvAditivo.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.LightGray;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvAditivo.DefaultCellStyle = dataGridViewCellStyle1;
            this.dgvAditivo.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvAditivo.Location = new System.Drawing.Point(3, 16);
            this.dgvAditivo.MultiSelect = false;
            this.dgvAditivo.Name = "dgvAditivo";
            this.dgvAditivo.ReadOnly = true;
            this.dgvAditivo.RowHeadersWidth = 30;
            this.dgvAditivo.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvAditivo.Size = new System.Drawing.Size(739, 105);
            this.dgvAditivo.TabIndex = 1;
            this.dgvAditivo.TabStop = false;
            this.dgvAditivo.CellDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvAditivo_CellDoubleClick);
            this.dgvAditivo.DataBindingComplete += new System.Windows.Forms.DataGridViewBindingCompleteEventHandler(this.dgvAditivo_DataBindingComplete);
            this.dgvAditivo.RowPrePaint += new System.Windows.Forms.DataGridViewRowPrePaintEventHandler(this.dgvAditivo_RowPrePaint);
            // 
            // lblLengendaFechado
            // 
            this.lblLengendaFechado.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.lblLengendaFechado.AutoSize = true;
            this.lblLengendaFechado.Location = new System.Drawing.Point(394, 439);
            this.lblLengendaFechado.Name = "lblLengendaFechado";
            this.lblLengendaFechado.Size = new System.Drawing.Size(74, 13);
            this.lblLengendaFechado.TabIndex = 29;
            this.lblLengendaFechado.Text = "Contrato Ativo";
            // 
            // lblCorFechado
            // 
            this.lblCorFechado.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.lblCorFechado.AutoSize = true;
            this.lblCorFechado.BackColor = System.Drawing.Color.Green;
            this.lblCorFechado.Location = new System.Drawing.Point(378, 438);
            this.lblCorFechado.Name = "lblCorFechado";
            this.lblCorFechado.Size = new System.Drawing.Size(13, 13);
            this.lblCorFechado.TabIndex = 28;
            this.lblCorFechado.Text = "  ";
            // 
            // lblLegendaEncerrado
            // 
            this.lblLegendaEncerrado.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.lblLegendaEncerrado.AutoSize = true;
            this.lblLegendaEncerrado.Location = new System.Drawing.Point(278, 439);
            this.lblLegendaEncerrado.Name = "lblLegendaEncerrado";
            this.lblLegendaEncerrado.Size = new System.Drawing.Size(99, 13);
            this.lblLegendaEncerrado.TabIndex = 27;
            this.lblLegendaEncerrado.Text = "Contrato Encerrado";
            // 
            // lblCorEncerrado
            // 
            this.lblCorEncerrado.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.lblCorEncerrado.AutoSize = true;
            this.lblCorEncerrado.BackColor = System.Drawing.Color.Blue;
            this.lblCorEncerrado.Location = new System.Drawing.Point(263, 438);
            this.lblCorEncerrado.Name = "lblCorEncerrado";
            this.lblCorEncerrado.Size = new System.Drawing.Size(13, 13);
            this.lblCorEncerrado.TabIndex = 26;
            this.lblCorEncerrado.Text = "  ";
            // 
            // lblLegendaConstrucao
            // 
            this.lblLegendaConstrucao.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.lblLegendaConstrucao.AutoSize = true;
            this.lblLegendaConstrucao.Location = new System.Drawing.Point(141, 439);
            this.lblLegendaConstrucao.Name = "lblLegendaConstrucao";
            this.lblLegendaConstrucao.Size = new System.Drawing.Size(121, 13);
            this.lblLegendaConstrucao.TabIndex = 25;
            this.lblLegendaConstrucao.Text = "Contrato em Construção";
            // 
            // lblCorContrucao
            // 
            this.lblCorContrucao.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.lblCorContrucao.AutoSize = true;
            this.lblCorContrucao.BackColor = System.Drawing.Color.Black;
            this.lblCorContrucao.Location = new System.Drawing.Point(126, 438);
            this.lblCorContrucao.Name = "lblCorContrucao";
            this.lblCorContrucao.Size = new System.Drawing.Size(13, 13);
            this.lblCorContrucao.TabIndex = 24;
            this.lblCorContrucao.Text = "  ";
            // 
            // lblLegendaCancelado
            // 
            this.lblLegendaCancelado.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.lblLegendaCancelado.AutoSize = true;
            this.lblLegendaCancelado.Location = new System.Drawing.Point(24, 439);
            this.lblLegendaCancelado.Name = "lblLegendaCancelado";
            this.lblLegendaCancelado.Size = new System.Drawing.Size(101, 13);
            this.lblLegendaCancelado.TabIndex = 23;
            this.lblLegendaCancelado.Text = "Contrato Cancelado";
            // 
            // lblCorCancelado
            // 
            this.lblCorCancelado.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.lblCorCancelado.AutoSize = true;
            this.lblCorCancelado.BackColor = System.Drawing.Color.Red;
            this.lblCorCancelado.ForeColor = System.Drawing.Color.White;
            this.lblCorCancelado.Location = new System.Drawing.Point(8, 438);
            this.lblCorCancelado.Name = "lblCorCancelado";
            this.lblCorCancelado.Size = new System.Drawing.Size(13, 13);
            this.lblCorCancelado.TabIndex = 22;
            this.lblCorCancelado.Text = "  ";
            // 
            // btnExcluirCliente
            // 
            this.btnExcluirCliente.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnExcluirCliente.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnExcluirCliente.Image = global::SWS.Properties.Resources.close;
            this.btnExcluirCliente.Location = new System.Drawing.Point(394, 79);
            this.btnExcluirCliente.Name = "btnExcluirCliente";
            this.btnExcluirCliente.Size = new System.Drawing.Size(27, 21);
            this.btnExcluirCliente.TabIndex = 30;
            this.btnExcluirCliente.TextAlign = System.Drawing.ContentAlignment.TopRight;
            this.btnExcluirCliente.UseVisualStyleBackColor = true;
            this.btnExcluirCliente.Click += new System.EventHandler(this.btnExcluirCliente_Click);
            // 
            // btnCopiar
            // 
            this.btnCopiar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnCopiar.Image = global::SWS.Properties.Resources.copiar;
            this.btnCopiar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnCopiar.Location = new System.Drawing.Point(687, 3);
            this.btnCopiar.Name = "btnCopiar";
            this.btnCopiar.Size = new System.Drawing.Size(70, 23);
            this.btnCopiar.TabIndex = 18;
            this.btnCopiar.TabStop = false;
            this.btnCopiar.Text = "&Copiar";
            this.btnCopiar.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnCopiar.UseVisualStyleBackColor = true;
            this.btnCopiar.Click += new System.EventHandler(this.btnCopiar_Click);
            // 
            // frmContratos
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(784, 561);
            this.Name = "frmContratos";
            this.Text = "GERENCIAR CONTRATOS";
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.frmContratos_KeyDown);
            this.spForm.Panel1.ResumeLayout(false);
            this.spForm.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.spForm)).EndInit();
            this.spForm.ResumeLayout(false);
            this.pnlForm.ResumeLayout(false);
            this.pnlForm.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.banner)).EndInit();
            this.flpAcao.ResumeLayout(false);
            this.grbContrato.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvContrato)).EndInit();
            this.grbAditivo.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvAditivo)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.FlowLayoutPanel flpAcao;
        private System.Windows.Forms.Button btnEncerrar;
        private System.Windows.Forms.Button btnConsultar;
        private System.Windows.Forms.Button btnFechar;
        private System.Windows.Forms.Button btnImprimir;
        private System.Windows.Forms.Button btnValidar;
        private System.Windows.Forms.Button btnExcluir;
        private System.Windows.Forms.Button btnAlterar;
        private System.Windows.Forms.Button btnIncluir;
        private System.Windows.Forms.TextBox lblCodigo;
        private System.Windows.Forms.TextBox lblTipo;
        private System.Windows.Forms.TextBox lblSituacao;
        private System.Windows.Forms.TextBox lblCliente;
        private System.Windows.Forms.TextBox lblDataElaboracao;
        private System.Windows.Forms.Button btnLimpar;
        private System.Windows.Forms.Button btnPesquisar;
        private System.Windows.Forms.TextBox lblDataFechamento;
        private System.Windows.Forms.TextBox lblDataVencimento;
        private System.Windows.Forms.TextBox lblDataCancelamento;
        private System.Windows.Forms.MaskedTextBox textCodigo;
        private ComboBoxWithBorder cbTipo;
        private ComboBoxWithBorder cbSituacao;
        private System.Windows.Forms.Button btnCliente;
        private System.Windows.Forms.TextBox textCliente;
        private System.Windows.Forms.DateTimePicker dtElaboracaoFinal;
        private System.Windows.Forms.DateTimePicker dtElaboracaoInicial;
        private System.Windows.Forms.DateTimePicker dtFechamentoFinal;
        private System.Windows.Forms.DateTimePicker dtFechamentoInicial;
        private System.Windows.Forms.DateTimePicker dtVencimentoFinal;
        private System.Windows.Forms.DateTimePicker dtVencimentoInicial;
        private System.Windows.Forms.DateTimePicker dtCancelamentoFinal;
        private System.Windows.Forms.DateTimePicker dtCancelamentoInicial;
        private System.Windows.Forms.GroupBox grbContrato;
        private System.Windows.Forms.DataGridView dgvContrato;
        private System.Windows.Forms.GroupBox grbAditivo;
        private System.Windows.Forms.DataGridView dgvAditivo;
        private System.Windows.Forms.Label lblLengendaFechado;
        private System.Windows.Forms.Label lblCorFechado;
        private System.Windows.Forms.Label lblLegendaEncerrado;
        private System.Windows.Forms.Label lblCorEncerrado;
        private System.Windows.Forms.Label lblLegendaConstrucao;
        private System.Windows.Forms.Label lblCorContrucao;
        private System.Windows.Forms.Label lblLegendaCancelado;
        private System.Windows.Forms.Label lblCorCancelado;
        private System.Windows.Forms.ToolTip tpContrato;
        private System.Windows.Forms.Button btnExcluirCliente;
        private System.Windows.Forms.Button btnCopiar;
    }
}