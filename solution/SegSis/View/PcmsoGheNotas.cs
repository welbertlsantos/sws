﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using SWS.Facade;
using SWS.Entidade;
using SWS.View.Resources;
using SWS.Excecao;


namespace SWS.View
{
    public partial class frm_PcmsoGheNotas : BaseFormConsulta
    {

        private static String msg1 = "Confirmação";
        private static String msg2 = "Deseja realmente excluir essa norma";
        private static String msg3 = "Selecione uma linha.";

        private Ghe ghe;
        public Ghe getGhe()
        {
            return this.ghe;
        }
        
        public frm_PcmsoGheNotas(Ghe ghe)
        {
            InitializeComponent();
            this.ghe = ghe;
            montaDataGrid();
        }

        private void btn_incluir_Click(object sender, EventArgs e)
        {
            try
            {
                PpraFacade ppraFacade = PpraFacade.getInstance();

                frm_GheNormaAlterar alterarNormaGhe = new frm_GheNormaAlterar(ghe);
                alterarNormaGhe.ShowDialog();

                if (alterarNormaGhe.NormaSelecionada.Count > 0)
                {
                    HashSet<Nota> normas = alterarNormaGhe.NormaSelecionada;

                    foreach (Nota norma in normas)
                    {
                        ppraFacade.incluiGheNorma(new GheNota(ghe, norma));
                    }

                    montaDataGrid();
                }

            }
            catch (Exception Ex)
            {
                MessageBox.Show(Ex.Message);
            }
        }

        private void btn_fechar_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void montaDataGrid()
        {

            try
            {
                this.Cursor = Cursors.WaitCursor;

                PpraFacade ppraFacade = PpraFacade.getInstance();

                dgNota.Columns.Clear();

                DataSet ds = ppraFacade.findAllNotasByGhe(ghe);

                dgNota.DataSource = ds.Tables[0].DefaultView;

                dgNota.Columns[0].HeaderText = "ID";
                dgNota.Columns[0].Visible = false;

                dgNota.Columns[1].HeaderText = "Título";
                
                dgNota.Columns[2].HeaderText = "Situação";
                dgNota.Columns[2].Visible = false;

                dgNota.Columns[3].HeaderText = "Conteúdo";

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
            
        }

        private void btn_excluir_Click(object sender, EventArgs e)
        {
            try
            {

                PpraFacade ppraFacade = PpraFacade.getInstance();

                if (dgNota.CurrentRow != null)
                {

                    if (MessageBox.Show(msg2, msg1, MessageBoxButtons.YesNo) == DialogResult.Yes)
                    {
                        Int64 id = (Int64)dgNota.CurrentRow.Cells[0].Value;

                        ppraFacade.deleteNormaGhe(new GheNota(ghe, new Nota((Int64)dgNota.CurrentRow.Cells[0].Value, String.Empty, String.Empty, String.Empty)));

                        montaDataGrid();
                    }
                }
                else
                {
                    throw new Exception(msg3);
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atençao", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
        }
    }
}
