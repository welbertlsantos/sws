﻿using System;
namespace SWS.View
{
    partial class frmMonitoramentos
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            this.flowLayoutPanel1 = new System.Windows.Forms.FlowLayoutPanel();
            this.btnIncluir = new System.Windows.Forms.Button();
            this.btnPesquisar = new System.Windows.Forms.Button();
            this.btnAlterar = new System.Windows.Forms.Button();
            this.btnExcluir = new System.Windows.Forms.Button();
            this.btnFechar = new System.Windows.Forms.Button();
            this.lblPeriodo = new System.Windows.Forms.TextBox();
            this.btnExcluirCliente = new System.Windows.Forms.Button();
            this.btnCliente = new System.Windows.Forms.Button();
            this.textCliente = new System.Windows.Forms.TextBox();
            this.lblCliente = new System.Windows.Forms.TextBox();
            this.grbTotal = new System.Windows.Forms.GroupBox();
            this.lblTotalMonitoramento = new System.Windows.Forms.Label();
            this.textTotal = new System.Windows.Forms.TextBox();
            this.grbMonitoramentos = new System.Windows.Forms.GroupBox();
            this.dgvMonitoramento = new System.Windows.Forms.DataGridView();
            this.btnExcluirFuncionarioCliente = new System.Windows.Forms.Button();
            this.btnIncluirFuncionarioCliente = new System.Windows.Forms.Button();
            this.textFuncionarioCliente = new System.Windows.Forms.TextBox();
            this.lblFuncionario = new System.Windows.Forms.TextBox();
            this.cbMesMonitoramento = new SWS.ComboBoxWithBorder();
            this.cbAnoMonitoramento = new SWS.ComboBoxWithBorder();
            this.tpMonitoramento = new System.Windows.Forms.ToolTip(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.spForm)).BeginInit();
            this.spForm.Panel1.SuspendLayout();
            this.spForm.Panel2.SuspendLayout();
            this.spForm.SuspendLayout();
            this.pnlForm.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.banner)).BeginInit();
            this.flowLayoutPanel1.SuspendLayout();
            this.grbTotal.SuspendLayout();
            this.grbMonitoramentos.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvMonitoramento)).BeginInit();
            this.SuspendLayout();
            // 
            // spForm
            // 
            // 
            // spForm.Panel1
            // 
            this.spForm.Panel1.Controls.Add(this.flowLayoutPanel1);
            this.spForm.SplitterDistance = 53;
            // 
            // pnlForm
            // 
            this.pnlForm.Controls.Add(this.cbAnoMonitoramento);
            this.pnlForm.Controls.Add(this.cbMesMonitoramento);
            this.pnlForm.Controls.Add(this.btnExcluirFuncionarioCliente);
            this.pnlForm.Controls.Add(this.btnIncluirFuncionarioCliente);
            this.pnlForm.Controls.Add(this.textFuncionarioCliente);
            this.pnlForm.Controls.Add(this.lblFuncionario);
            this.pnlForm.Controls.Add(this.grbTotal);
            this.pnlForm.Controls.Add(this.grbMonitoramentos);
            this.pnlForm.Controls.Add(this.lblPeriodo);
            this.pnlForm.Controls.Add(this.btnExcluirCliente);
            this.pnlForm.Controls.Add(this.btnCliente);
            this.pnlForm.Controls.Add(this.textCliente);
            this.pnlForm.Controls.Add(this.lblCliente);
            // 
            // flowLayoutPanel1
            // 
            this.flowLayoutPanel1.Controls.Add(this.btnIncluir);
            this.flowLayoutPanel1.Controls.Add(this.btnPesquisar);
            this.flowLayoutPanel1.Controls.Add(this.btnAlterar);
            this.flowLayoutPanel1.Controls.Add(this.btnExcluir);
            this.flowLayoutPanel1.Controls.Add(this.btnFechar);
            this.flowLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.flowLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.flowLayoutPanel1.Name = "flowLayoutPanel1";
            this.flowLayoutPanel1.Size = new System.Drawing.Size(784, 53);
            this.flowLayoutPanel1.TabIndex = 0;
            // 
            // btnIncluir
            // 
            this.btnIncluir.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnIncluir.Image = global::SWS.Properties.Resources.incluir;
            this.btnIncluir.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnIncluir.Location = new System.Drawing.Point(3, 3);
            this.btnIncluir.Name = "btnIncluir";
            this.btnIncluir.Size = new System.Drawing.Size(75, 23);
            this.btnIncluir.TabIndex = 0;
            this.btnIncluir.Text = "Incluir";
            this.btnIncluir.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnIncluir.UseVisualStyleBackColor = true;
            this.btnIncluir.Click += new System.EventHandler(this.btnIncluir_Click);
            // 
            // btnPesquisar
            // 
            this.btnPesquisar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnPesquisar.Image = global::SWS.Properties.Resources.lupa;
            this.btnPesquisar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnPesquisar.Location = new System.Drawing.Point(84, 3);
            this.btnPesquisar.Name = "btnPesquisar";
            this.btnPesquisar.Size = new System.Drawing.Size(75, 23);
            this.btnPesquisar.TabIndex = 3;
            this.btnPesquisar.Text = "Pesquisar";
            this.btnPesquisar.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnPesquisar.UseVisualStyleBackColor = true;
            this.btnPesquisar.Click += new System.EventHandler(this.btnPesquisar_Click);
            // 
            // btnAlterar
            // 
            this.btnAlterar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnAlterar.Image = global::SWS.Properties.Resources.Alterar;
            this.btnAlterar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnAlterar.Location = new System.Drawing.Point(165, 3);
            this.btnAlterar.Name = "btnAlterar";
            this.btnAlterar.Size = new System.Drawing.Size(75, 23);
            this.btnAlterar.TabIndex = 1;
            this.btnAlterar.Text = "Alterar";
            this.btnAlterar.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnAlterar.UseVisualStyleBackColor = true;
            this.btnAlterar.Click += new System.EventHandler(this.btnAlterar_Click);
            // 
            // btnExcluir
            // 
            this.btnExcluir.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnExcluir.Image = global::SWS.Properties.Resources.lixeira;
            this.btnExcluir.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnExcluir.Location = new System.Drawing.Point(246, 3);
            this.btnExcluir.Name = "btnExcluir";
            this.btnExcluir.Size = new System.Drawing.Size(75, 23);
            this.btnExcluir.TabIndex = 4;
            this.btnExcluir.Text = "Excluir";
            this.btnExcluir.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnExcluir.UseVisualStyleBackColor = true;
            this.btnExcluir.Click += new System.EventHandler(this.btnExcluir_Click);
            // 
            // btnFechar
            // 
            this.btnFechar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnFechar.Image = global::SWS.Properties.Resources.close;
            this.btnFechar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnFechar.Location = new System.Drawing.Point(327, 3);
            this.btnFechar.Name = "btnFechar";
            this.btnFechar.Size = new System.Drawing.Size(75, 23);
            this.btnFechar.TabIndex = 2;
            this.btnFechar.Text = "Fechar";
            this.btnFechar.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnFechar.UseVisualStyleBackColor = true;
            this.btnFechar.Click += new System.EventHandler(this.btnFechar_Click);
            // 
            // lblPeriodo
            // 
            this.lblPeriodo.BackColor = System.Drawing.Color.LightGray;
            this.lblPeriodo.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblPeriodo.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPeriodo.Location = new System.Drawing.Point(3, 43);
            this.lblPeriodo.Name = "lblPeriodo";
            this.lblPeriodo.ReadOnly = true;
            this.lblPeriodo.Size = new System.Drawing.Size(145, 21);
            this.lblPeriodo.TabIndex = 77;
            this.lblPeriodo.TabStop = false;
            this.lblPeriodo.Text = "Período de Emissão";
            // 
            // btnExcluirCliente
            // 
            this.btnExcluirCliente.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnExcluirCliente.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnExcluirCliente.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnExcluirCliente.Image = global::SWS.Properties.Resources.close;
            this.btnExcluirCliente.Location = new System.Drawing.Point(726, 3);
            this.btnExcluirCliente.Name = "btnExcluirCliente";
            this.btnExcluirCliente.Size = new System.Drawing.Size(31, 21);
            this.btnExcluirCliente.TabIndex = 76;
            this.btnExcluirCliente.UseVisualStyleBackColor = true;
            this.btnExcluirCliente.Click += new System.EventHandler(this.btnExcluirCliente_Click);
            // 
            // btnCliente
            // 
            this.btnCliente.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnCliente.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnCliente.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCliente.Image = global::SWS.Properties.Resources.busca;
            this.btnCliente.Location = new System.Drawing.Point(696, 3);
            this.btnCliente.Name = "btnCliente";
            this.btnCliente.Size = new System.Drawing.Size(31, 21);
            this.btnCliente.TabIndex = 75;
            this.btnCliente.UseVisualStyleBackColor = true;
            this.btnCliente.Click += new System.EventHandler(this.btnCliente_Click);
            // 
            // textCliente
            // 
            this.textCliente.BackColor = System.Drawing.Color.Gainsboro;
            this.textCliente.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textCliente.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.textCliente.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textCliente.Location = new System.Drawing.Point(147, 3);
            this.textCliente.MaxLength = 100;
            this.textCliente.Name = "textCliente";
            this.textCliente.ReadOnly = true;
            this.textCliente.Size = new System.Drawing.Size(550, 21);
            this.textCliente.TabIndex = 74;
            this.textCliente.TabStop = false;
            // 
            // lblCliente
            // 
            this.lblCliente.BackColor = System.Drawing.Color.LightGray;
            this.lblCliente.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblCliente.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCliente.Location = new System.Drawing.Point(3, 3);
            this.lblCliente.Name = "lblCliente";
            this.lblCliente.ReadOnly = true;
            this.lblCliente.Size = new System.Drawing.Size(145, 21);
            this.lblCliente.TabIndex = 73;
            this.lblCliente.TabStop = false;
            this.lblCliente.Text = "Cliente";
            // 
            // grbTotal
            // 
            this.grbTotal.Controls.Add(this.lblTotalMonitoramento);
            this.grbTotal.Controls.Add(this.textTotal);
            this.grbTotal.Location = new System.Drawing.Point(5, 383);
            this.grbTotal.Name = "grbTotal";
            this.grbTotal.Size = new System.Drawing.Size(332, 42);
            this.grbTotal.TabIndex = 80;
            this.grbTotal.TabStop = false;
            this.grbTotal.Text = "Totais";
            // 
            // lblTotalMonitoramento
            // 
            this.lblTotalMonitoramento.AutoSize = true;
            this.lblTotalMonitoramento.Location = new System.Drawing.Point(20, 17);
            this.lblTotalMonitoramento.Name = "lblTotalMonitoramento";
            this.lblTotalMonitoramento.Size = new System.Drawing.Size(196, 13);
            this.lblTotalMonitoramento.TabIndex = 29;
            this.lblTotalMonitoramento.Text = "Quantidade de  monitoramentos filtrados";
            // 
            // textTotal
            // 
            this.textTotal.BackColor = System.Drawing.Color.LightBlue;
            this.textTotal.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textTotal.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textTotal.ForeColor = System.Drawing.Color.Black;
            this.textTotal.Location = new System.Drawing.Point(223, 13);
            this.textTotal.MaxLength = 10;
            this.textTotal.Name = "textTotal";
            this.textTotal.ReadOnly = true;
            this.textTotal.Size = new System.Drawing.Size(71, 23);
            this.textTotal.TabIndex = 30;
            this.textTotal.TabStop = false;
            this.textTotal.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // grbMonitoramentos
            // 
            this.grbMonitoramentos.BackColor = System.Drawing.Color.White;
            this.grbMonitoramentos.Controls.Add(this.dgvMonitoramento);
            this.grbMonitoramentos.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.grbMonitoramentos.Location = new System.Drawing.Point(5, 70);
            this.grbMonitoramentos.Name = "grbMonitoramentos";
            this.grbMonitoramentos.Size = new System.Drawing.Size(754, 310);
            this.grbMonitoramentos.TabIndex = 79;
            this.grbMonitoramentos.TabStop = false;
            this.grbMonitoramentos.Text = "Monitoramentos Realizados";
            // 
            // dgvMonitoramento
            // 
            this.dgvMonitoramento.AllowUserToAddRows = false;
            this.dgvMonitoramento.AllowUserToDeleteRows = false;
            this.dgvMonitoramento.AllowUserToOrderColumns = true;
            this.dgvMonitoramento.AllowUserToResizeRows = false;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.Gainsboro;
            this.dgvMonitoramento.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle1;
            this.dgvMonitoramento.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.DisplayedCells;
            this.dgvMonitoramento.BackgroundColor = System.Drawing.Color.White;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvMonitoramento.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle2;
            this.dgvMonitoramento.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle3.BackColor = System.Drawing.Color.LightGray;
            dataGridViewCellStyle3.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvMonitoramento.DefaultCellStyle = dataGridViewCellStyle3;
            this.dgvMonitoramento.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvMonitoramento.Location = new System.Drawing.Point(3, 16);
            this.dgvMonitoramento.MultiSelect = false;
            this.dgvMonitoramento.Name = "dgvMonitoramento";
            this.dgvMonitoramento.ReadOnly = true;
            this.dgvMonitoramento.RowHeadersWidth = 35;
            this.dgvMonitoramento.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvMonitoramento.Size = new System.Drawing.Size(748, 291);
            this.dgvMonitoramento.TabIndex = 74;
            // 
            // btnExcluirFuncionarioCliente
            // 
            this.btnExcluirFuncionarioCliente.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnExcluirFuncionarioCliente.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnExcluirFuncionarioCliente.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnExcluirFuncionarioCliente.Image = global::SWS.Properties.Resources.close;
            this.btnExcluirFuncionarioCliente.Location = new System.Drawing.Point(726, 23);
            this.btnExcluirFuncionarioCliente.Name = "btnExcluirFuncionarioCliente";
            this.btnExcluirFuncionarioCliente.Size = new System.Drawing.Size(31, 21);
            this.btnExcluirFuncionarioCliente.TabIndex = 84;
            this.btnExcluirFuncionarioCliente.UseVisualStyleBackColor = true;
            this.btnExcluirFuncionarioCliente.Click += new System.EventHandler(this.btnExcluirFuncionarioCliente_Click);
            // 
            // btnIncluirFuncionarioCliente
            // 
            this.btnIncluirFuncionarioCliente.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnIncluirFuncionarioCliente.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnIncluirFuncionarioCliente.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnIncluirFuncionarioCliente.Image = global::SWS.Properties.Resources.busca;
            this.btnIncluirFuncionarioCliente.Location = new System.Drawing.Point(696, 23);
            this.btnIncluirFuncionarioCliente.Name = "btnIncluirFuncionarioCliente";
            this.btnIncluirFuncionarioCliente.Size = new System.Drawing.Size(31, 21);
            this.btnIncluirFuncionarioCliente.TabIndex = 83;
            this.btnIncluirFuncionarioCliente.UseVisualStyleBackColor = true;
            this.btnIncluirFuncionarioCliente.Click += new System.EventHandler(this.btnIncluirFuncionarioCliente_Click);
            // 
            // textFuncionarioCliente
            // 
            this.textFuncionarioCliente.BackColor = System.Drawing.Color.Gainsboro;
            this.textFuncionarioCliente.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textFuncionarioCliente.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.textFuncionarioCliente.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textFuncionarioCliente.Location = new System.Drawing.Point(147, 23);
            this.textFuncionarioCliente.MaxLength = 100;
            this.textFuncionarioCliente.Name = "textFuncionarioCliente";
            this.textFuncionarioCliente.ReadOnly = true;
            this.textFuncionarioCliente.Size = new System.Drawing.Size(550, 21);
            this.textFuncionarioCliente.TabIndex = 82;
            this.textFuncionarioCliente.TabStop = false;
            // 
            // lblFuncionario
            // 
            this.lblFuncionario.BackColor = System.Drawing.Color.LightGray;
            this.lblFuncionario.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblFuncionario.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblFuncionario.Location = new System.Drawing.Point(3, 23);
            this.lblFuncionario.Name = "lblFuncionario";
            this.lblFuncionario.ReadOnly = true;
            this.lblFuncionario.Size = new System.Drawing.Size(145, 21);
            this.lblFuncionario.TabIndex = 81;
            this.lblFuncionario.TabStop = false;
            this.lblFuncionario.Text = "Funcionário";
            // 
            // cbMesMonitoramento
            // 
            this.cbMesMonitoramento.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.cbMesMonitoramento.BackColor = System.Drawing.Color.LightGray;
            this.cbMesMonitoramento.BorderColor = System.Drawing.Color.DimGray;
            this.cbMesMonitoramento.BorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid;
            this.cbMesMonitoramento.FormattingEnabled = true;
            this.cbMesMonitoramento.Location = new System.Drawing.Point(147, 43);
            this.cbMesMonitoramento.Name = "cbMesMonitoramento";
            this.cbMesMonitoramento.Size = new System.Drawing.Size(74, 21);
            this.cbMesMonitoramento.TabIndex = 85;
            this.tpMonitoramento.SetToolTip(this.cbMesMonitoramento, "Escolha o mês de monitoramento.");
            // 
            // cbAnoMonitoramento
            // 
            this.cbAnoMonitoramento.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.cbAnoMonitoramento.BackColor = System.Drawing.Color.LightGray;
            this.cbAnoMonitoramento.BorderColor = System.Drawing.Color.DimGray;
            this.cbAnoMonitoramento.BorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid;
            this.cbAnoMonitoramento.FormattingEnabled = true;
            this.cbAnoMonitoramento.Location = new System.Drawing.Point(220, 43);
            this.cbAnoMonitoramento.Name = "cbAnoMonitoramento";
            this.cbAnoMonitoramento.Size = new System.Drawing.Size(48, 21);
            this.cbAnoMonitoramento.TabIndex = 86;
            this.tpMonitoramento.SetToolTip(this.cbAnoMonitoramento, "Escolha o ano do monitoramento");
            // 
            // tpMonitoramento
            // 
            this.tpMonitoramento.AutoPopDelay = 7000;
            this.tpMonitoramento.InitialDelay = 500;
            this.tpMonitoramento.ReshowDelay = 100;
            // 
            // frmMonitoramentos
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(784, 561);
            this.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.Name = "frmMonitoramentos";
            this.Text = "GERENCIAR MONITORAMENTOS";
            this.spForm.Panel1.ResumeLayout(false);
            this.spForm.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.spForm)).EndInit();
            this.spForm.ResumeLayout(false);
            this.pnlForm.ResumeLayout(false);
            this.pnlForm.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.banner)).EndInit();
            this.flowLayoutPanel1.ResumeLayout(false);
            this.grbTotal.ResumeLayout(false);
            this.grbTotal.PerformLayout();
            this.grbMonitoramentos.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvMonitoramento)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel1;
        private System.Windows.Forms.Button btnIncluir;
        private System.Windows.Forms.Button btnAlterar;
        private System.Windows.Forms.Button btnFechar;
        private System.Windows.Forms.TextBox lblPeriodo;
        private System.Windows.Forms.Button btnExcluirCliente;
        private System.Windows.Forms.Button btnCliente;
        private System.Windows.Forms.TextBox textCliente;
        private System.Windows.Forms.TextBox lblCliente;
        private System.Windows.Forms.GroupBox grbTotal;
        private System.Windows.Forms.Label lblTotalMonitoramento;
        private System.Windows.Forms.TextBox textTotal;
        private System.Windows.Forms.GroupBox grbMonitoramentos;
        private System.Windows.Forms.DataGridView dgvMonitoramento;
        private System.Windows.Forms.Button btnPesquisar;
        private System.Windows.Forms.Button btnExcluir;
        private System.Windows.Forms.Button btnExcluirFuncionarioCliente;
        private System.Windows.Forms.Button btnIncluirFuncionarioCliente;
        private System.Windows.Forms.TextBox textFuncionarioCliente;
        private System.Windows.Forms.TextBox lblFuncionario;
        protected ComboBoxWithBorder cbAnoMonitoramento;
        private System.Windows.Forms.ToolTip tpMonitoramento;
        protected ComboBoxWithBorder cbMesMonitoramento;
    }
}