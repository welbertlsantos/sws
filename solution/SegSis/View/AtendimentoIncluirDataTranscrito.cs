﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace SWS.View
{
    public partial class frmAtendimentoIncluirDataTranscrito : frmTemplateConsulta
    {
        private DateTime dataExameTranscrito;

        public DateTime DataExameTranscrito
        {
            get { return dataExameTranscrito; }
            set { dataExameTranscrito = value; }
        }

        private DateTime dataVencimentoExameTranscrito;

        public DateTime DataVencimentoExameTranscrito
        {
            get { return dataVencimentoExameTranscrito; }
            set { dataVencimentoExameTranscrito = value; }
        }
        
        public frmAtendimentoIncluirDataTranscrito()
        {
            InitializeComponent();
            ActiveControl = dataTranscrito;
        }

        private void btnGravar_Click(object sender, EventArgs e)
        {
            try
            {
                DataExameTranscrito = Convert.ToDateTime(dataTranscrito.Value);
                DataVencimentoExameTranscrito = Convert.ToDateTime(dataVencimentoTranscrito.Value);
                this.Close();

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btnFechar_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void frmAtendimentoIncluirDataTranscrito_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter || e.KeyCode == Keys.Return)
                SendKeys.Send("{TAB}");
        }
    }
}
