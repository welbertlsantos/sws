﻿using SWS.Entidade;
using SWS.Facade;
using SWS.View.Resources;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace SWS.View
{
    public partial class frmPcmsoBuscar : frmTemplateConsulta
    {
        private Estudo estudo;
        private Cliente cliente;

        public Estudo Estudo
        {
            get { return estudo; }
            set { estudo = value; }
        }

        public frmPcmsoBuscar(Cliente cliente)
        {
            InitializeComponent();
            this.cliente = cliente;
            this.montaGrid();

        }

        private void btnFechar_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void montaGrid()
        {
            try
            {
                this.dgvPcsmo.Columns.Clear();
                this.dgvPcsmo.ColumnCount = 3;
                this.dgvPcsmo.Columns[0].HeaderText = "id";
                this.dgvPcsmo.Columns[0].Visible = false;
                this.dgvPcsmo.Columns[1].HeaderText = "Código";
                this.dgvPcsmo.Columns[2].HeaderText = "Obra";

                EstudoFacade pcmsoFacade = EstudoFacade.getInstance();
                ClienteFacade clienteFacade = ClienteFacade.getInstance();

                Estudo pcmso = new Estudo();
                pcmso.VendedorCliente = clienteFacade.findClienteVendedorByCliente(cliente).Find(x => x.Situacao == ApplicationConstants.ATIVO);
                pcmso.Situacao = ApplicationConstants.TODOS;

                DataSet ds = pcmsoFacade.findByFilter(pcmso, null);


                foreach (DataRow row in ds.Tables[0].Rows)
                {
                    this.dgvPcsmo.Rows.Add(row["ID_ESTUDO"], row["CODIGO_ESTUDO"], row["OBRA"].ToString());
                }

            }
            catch (Exception ex) 
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btnConfirmar_Click(object sender, EventArgs e)
        {
            try
            {
                if (dgvPcsmo.CurrentRow == null) throw new Exception("Deve selecionar primeiro um pcmso na gride");
                EstudoFacade pcmsoFacade = EstudoFacade.getInstance();
                estudo = pcmsoFacade.findById((long)dgvPcsmo.CurrentRow.Cells[0].Value);
                this.Close();
            }
            catch(Exception ex) 
            {
                   MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }
    }
}
