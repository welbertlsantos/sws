﻿using SWS.Entidade;
using SWS.Facade;
using SWS.View.Resources;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace SWS.View
{
    public partial class frmAtendimentoLaudarConclusao : frmTemplateConsulta
    {

        private Aso atendimento;

        public frmAtendimentoLaudarConclusao(Aso atendimento)
        {
            InitializeComponent();
            this.atendimento = atendimento;

            if (String.Equals(atendimento.Conclusao, ApplicationConstants.LAUDOAPTO))
                rb_apto.Checked = true;

            else if (String.Equals(atendimento.Conclusao, ApplicationConstants.LAUDOINAPTO))
                rb_inapto.Checked = true;
            else
            {
                rb_apto.Checked = false;
                rb_inapto.Checked = false;
            }

            text_observacao.Text = atendimento.ObservacaoAso;
            ActiveControl = text_observacao;
        }

        private void btn_gravar_Click(object sender, EventArgs e)
        {
            try
            {
                /* fazendo update no aso gravando as informações no laudo */

                AsoFacade asoFacade = AsoFacade.getInstance();

                String conclusao = rb_apto.Checked ? ApplicationConstants.LAUDOAPTO : ApplicationConstants.LAUDOINAPTO;

                atendimento.Conclusao = conclusao;
                atendimento.ObservacaoAso = text_observacao.Text.Trim();

                asoFacade.updateLaudoAtendimento(atendimento);

                MessageBox.Show("Atendimento laudado com sucesso.", "Confirmação", MessageBoxButtons.OK, MessageBoxIcon.Information);
                this.Close();

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            
        }

        private void btn_fechar_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
