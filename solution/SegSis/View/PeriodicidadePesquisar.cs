﻿using SWS.Entidade;
using SWS.Facade;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace SWS.View
{
    public partial class frmPeriodicidadePesquisar : frmTemplateConsulta
    {
        private Periodicidade tipoAtendimento;

        public Periodicidade TipoAtendimento
        {
            get { return tipoAtendimento; }
            set { tipoAtendimento = value; }
        }
        
        public frmPeriodicidadePesquisar()
        {
            InitializeComponent();
            montaDataGrid();
        }


        private void montaDataGrid()
        {
            try
            {
                this.Cursor = Cursors.Default;
                dgvTipo.Columns.Clear();

                EstudoFacade pcmsoFacade = EstudoFacade.getInstance();

                DataSet ds = pcmsoFacade.findAllPeriodicidade();

                dgvTipo.DataSource = ds.Tables[0].DefaultView;

                dgvTipo.Columns[0].HeaderText = "Código";
                dgvTipo.Columns[0].Visible = false;
                dgvTipo.Columns[1].HeaderText = "Tipo De Atendimento";
                dgvTipo.Columns[2].HeaderText = "Selecionar";
                dgvTipo.Columns[2].Visible = false;

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }

        }

        private void btnSelecionar_Click(object sender, EventArgs e)
        {
            try
            {
                if (dgvTipo.CurrentRow == null)
                    throw new Exception("Selecione um tipo de atendimento.");

                EstudoFacade pcmsoFacade = EstudoFacade.getInstance();

                TipoAtendimento = pcmsoFacade.findPeriodicidadeById((long)dgvTipo.CurrentRow.Cells[0].Value);

                this.Close();

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }

        private void btnFechar_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
