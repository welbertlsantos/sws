﻿namespace SWS.View
{
    partial class frmClienteFuncaoExameIncluir
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            this.flpAcao = new System.Windows.Forms.FlowLayoutPanel();
            this.btn_incluir = new System.Windows.Forms.Button();
            this.btn_fechar = new System.Windows.Forms.Button();
            this.btExame = new System.Windows.Forms.Button();
            this.textExame = new System.Windows.Forms.TextBox();
            this.lblExame = new System.Windows.Forms.TextBox();
            this.lblIdade = new System.Windows.Forms.TextBox();
            this.textIdade = new System.Windows.Forms.TextBox();
            this.grbPeriodicidade = new System.Windows.Forms.GroupBox();
            this.dgvPeriodicidade = new System.Windows.Forms.DataGridView();
            ((System.ComponentModel.ISupportInitialize)(this.scForm)).BeginInit();
            this.scForm.Panel1.SuspendLayout();
            this.scForm.Panel2.SuspendLayout();
            this.scForm.SuspendLayout();
            this.pnlForm.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.banner)).BeginInit();
            this.flpAcao.SuspendLayout();
            this.grbPeriodicidade.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvPeriodicidade)).BeginInit();
            this.SuspendLayout();
            // 
            // scForm
            // 
            // 
            // scForm.Panel1
            // 
            this.scForm.Panel1.Controls.Add(this.flpAcao);
            // 
            // pnlForm
            // 
            this.pnlForm.Controls.Add(this.grbPeriodicidade);
            this.pnlForm.Controls.Add(this.textIdade);
            this.pnlForm.Controls.Add(this.lblIdade);
            this.pnlForm.Controls.Add(this.lblExame);
            this.pnlForm.Controls.Add(this.btExame);
            this.pnlForm.Controls.Add(this.textExame);
            // 
            // flpAcao
            // 
            this.flpAcao.Controls.Add(this.btn_incluir);
            this.flpAcao.Controls.Add(this.btn_fechar);
            this.flpAcao.Dock = System.Windows.Forms.DockStyle.Fill;
            this.flpAcao.Location = new System.Drawing.Point(0, 0);
            this.flpAcao.Name = "flpAcao";
            this.flpAcao.Size = new System.Drawing.Size(534, 30);
            this.flpAcao.TabIndex = 0;
            // 
            // btn_incluir
            // 
            this.btn_incluir.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_incluir.Image = global::SWS.Properties.Resources.fechar_ico;
            this.btn_incluir.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btn_incluir.Location = new System.Drawing.Point(3, 3);
            this.btn_incluir.Name = "btn_incluir";
            this.btn_incluir.Size = new System.Drawing.Size(75, 23);
            this.btn_incluir.TabIndex = 11;
            this.btn_incluir.TabStop = false;
            this.btn_incluir.Text = "&Confirmar";
            this.btn_incluir.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btn_incluir.UseVisualStyleBackColor = true;
            this.btn_incluir.Click += new System.EventHandler(this.btn_incluir_Click);
            // 
            // btn_fechar
            // 
            this.btn_fechar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_fechar.Image = global::SWS.Properties.Resources.close;
            this.btn_fechar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btn_fechar.Location = new System.Drawing.Point(84, 3);
            this.btn_fechar.Name = "btn_fechar";
            this.btn_fechar.Size = new System.Drawing.Size(75, 23);
            this.btn_fechar.TabIndex = 12;
            this.btn_fechar.TabStop = false;
            this.btn_fechar.Text = "&Fechar";
            this.btn_fechar.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btn_fechar.UseVisualStyleBackColor = true;
            this.btn_fechar.Click += new System.EventHandler(this.btn_fechar_Click);
            // 
            // btExame
            // 
            this.btExame.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btExame.Image = global::SWS.Properties.Resources.busca;
            this.btExame.Location = new System.Drawing.Point(468, 17);
            this.btExame.Name = "btExame";
            this.btExame.Size = new System.Drawing.Size(29, 21);
            this.btExame.TabIndex = 1;
            this.btExame.UseVisualStyleBackColor = true;
            this.btExame.Click += new System.EventHandler(this.btExame_Click);
            // 
            // textExame
            // 
            this.textExame.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.textExame.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textExame.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textExame.ForeColor = System.Drawing.Color.Black;
            this.textExame.Location = new System.Drawing.Point(101, 17);
            this.textExame.MaxLength = 100;
            this.textExame.Name = "textExame";
            this.textExame.ReadOnly = true;
            this.textExame.Size = new System.Drawing.Size(368, 21);
            this.textExame.TabIndex = 3;
            this.textExame.TabStop = false;
            // 
            // lblExame
            // 
            this.lblExame.BackColor = System.Drawing.Color.LightGray;
            this.lblExame.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblExame.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblExame.Location = new System.Drawing.Point(12, 17);
            this.lblExame.MaxLength = 15;
            this.lblExame.Name = "lblExame";
            this.lblExame.Size = new System.Drawing.Size(90, 21);
            this.lblExame.TabIndex = 4;
            this.lblExame.TabStop = false;
            this.lblExame.Text = "Exame";
            // 
            // lblIdade
            // 
            this.lblIdade.BackColor = System.Drawing.Color.LightGray;
            this.lblIdade.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblIdade.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblIdade.Location = new System.Drawing.Point(12, 37);
            this.lblIdade.MaxLength = 15;
            this.lblIdade.Name = "lblIdade";
            this.lblIdade.Size = new System.Drawing.Size(90, 21);
            this.lblIdade.TabIndex = 5;
            this.lblIdade.TabStop = false;
            this.lblIdade.Text = "Idade";
            // 
            // textIdade
            // 
            this.textIdade.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textIdade.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textIdade.ForeColor = System.Drawing.Color.Blue;
            this.textIdade.Location = new System.Drawing.Point(101, 37);
            this.textIdade.MaxLength = 3;
            this.textIdade.Name = "textIdade";
            this.textIdade.Size = new System.Drawing.Size(396, 21);
            this.textIdade.TabIndex = 2;
            this.textIdade.Text = "0";
            this.textIdade.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.textIdade_KeyPress);
            // 
            // grbPeriodicidade
            // 
            this.grbPeriodicidade.Controls.Add(this.dgvPeriodicidade);
            this.grbPeriodicidade.Location = new System.Drawing.Point(12, 64);
            this.grbPeriodicidade.Name = "grbPeriodicidade";
            this.grbPeriodicidade.Size = new System.Drawing.Size(485, 210);
            this.grbPeriodicidade.TabIndex = 7;
            this.grbPeriodicidade.TabStop = false;
            this.grbPeriodicidade.Text = "Periodicidade";
            // 
            // dgvPeriodicidade
            // 
            this.dgvPeriodicidade.AllowUserToAddRows = false;
            this.dgvPeriodicidade.AllowUserToDeleteRows = false;
            this.dgvPeriodicidade.AllowUserToOrderColumns = true;
            this.dgvPeriodicidade.AllowUserToResizeRows = false;
            this.dgvPeriodicidade.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.DisplayedCells;
            this.dgvPeriodicidade.BackgroundColor = System.Drawing.Color.White;
            this.dgvPeriodicidade.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.LightGray;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvPeriodicidade.DefaultCellStyle = dataGridViewCellStyle1;
            this.dgvPeriodicidade.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvPeriodicidade.Location = new System.Drawing.Point(3, 16);
            this.dgvPeriodicidade.MultiSelect = false;
            this.dgvPeriodicidade.Name = "dgvPeriodicidade";
            this.dgvPeriodicidade.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.CellSelect;
            this.dgvPeriodicidade.Size = new System.Drawing.Size(479, 191);
            this.dgvPeriodicidade.TabIndex = 8;
            // 
            // frmClienteFuncaoExameIncluir
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(534, 361);
            this.Name = "frmClienteFuncaoExameIncluir";
            this.Text = "INCLUIR EXAMES";
            this.scForm.Panel1.ResumeLayout(false);
            this.scForm.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.scForm)).EndInit();
            this.scForm.ResumeLayout(false);
            this.pnlForm.ResumeLayout(false);
            this.pnlForm.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.banner)).EndInit();
            this.flpAcao.ResumeLayout(false);
            this.grbPeriodicidade.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvPeriodicidade)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.FlowLayoutPanel flpAcao;
        private System.Windows.Forms.Button btn_fechar;
        private System.Windows.Forms.Button btn_incluir;
        private System.Windows.Forms.Button btExame;
        public System.Windows.Forms.TextBox textExame;
        private System.Windows.Forms.TextBox lblExame;
        private System.Windows.Forms.TextBox lblIdade;
        public System.Windows.Forms.TextBox textIdade;
        private System.Windows.Forms.GroupBox grbPeriodicidade;
        private System.Windows.Forms.DataGridView dgvPeriodicidade;
    }
}