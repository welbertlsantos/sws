﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using SWS.Facade;
using SWS.Entidade;
using SWS.Excecao;
using SWS.View.ViewHelper;
using System.Text.RegularExpressions;
using SWS.View.Resources;

namespace SWS.View
{
    public partial class frm_ppraAlterar : frm_ppraIncluirOld
    {

        private frm_ppraOld formPpra;

        private Cliente clienteContratanteCheck = new Cliente();

        public frm_ppraAlterar(Estudo ppra, frm_ppraOld formPpra) : base()
        {
            InitializeComponent();
            this.formPpra = formPpra;
            this.Ppra = ppra;

            if (ppra.Cronograma != null)
            {
                this.cronograma = ppra.Cronograma;
                this.listCronogramaAtividade = ppra.Cronograma.CronogramaAtividade;
            }

            if (ppra.ClienteContratante != null)
            {
                this.text_clienteContratante.Enabled = true;
                this.btn_contratante.Enabled = true;
                this.text_inicioContr.Enabled = true;
                this.text_fimContr.Enabled = true;
            }
            else
            {
                text_clienteContratante.Enabled = false;
                btn_contratante.Enabled = false;
                text_inicioContr.Enabled = false;
                text_fimContr.Enabled = false;
            }

            // preenchendo dados do estudo no form

            // alterando botoes

            btn_salvar.Text = "Gravar";
            btn_cliente.Enabled = false;

            #region estudo

            text_codPpra.Text = ppra.CodigoEstudo;
            text_cliente.Text = ppra.VendedorCliente.getCliente().RazaoSocial;

            if (ppra.ClienteContratante != null)
            {
                this.clienteContratanteCheck = ppra.ClienteContratante;
                text_clienteContratante.Text = ppra.ClienteContratante.RazaoSocial;
                text_inicioContr.Text = ppra.DataInicioContrato;
                text_fimContr.Text = ppra.DataFimContrato;
                text_numContrato.Text = ppra.NumContrato;

                text_inicioContr.Enabled = true;
                text_fimContr.Enabled = true;
                text_inicioContr.Enabled = true;
                text_numContrato.Enabled = true;
            }
            else
            {
                text_inicioContr.Enabled = false;
                text_fimContr.Enabled = false;
                text_inicioContr.Enabled = false;
                text_numContrato.Enabled = false;
            }

            if (ppra.Tecno != null)
            {
                text_tecnico.Text = ppra.Tecno.Nome;
            }

            dt_criacao.Text = Convert.ToString(ppra.DataCriacao);
            dt_vencimento.Text = Convert.ToString(ppra.DataVencimento);
            text_endereco.Text = ppra.Endereco;
            text_numero.Text = ppra.Numero;
            text_complemento.Text = ppra.Complemento;
            text_bairro.Text = ppra.Bairro;
            text_cidade.Text = ppra.Cidade;
            text_obra.Text = ppra.Obra;
            text_localObra.Text = ppra.LocalObra;
            text_cep.Text = ppra.Cep;
            text_comentario.Text = ppra.Comentario;

            #endregion

            #region ghe

            // montando a gride de ghe
            montaGridGhe();

            // carregando a combos de ghe
            iniciaComboGhe(cb_ghe);

            //carregando a combo de ghe_fonte
            iniciaComboGhe(cb_gheAgente);

            //carregando a combo de ghe Epi
            iniciaComboGhe(cb_gheEPI);

            //carregando a combo de ghe Setor
            iniciaComboGhe(cb_gheSetor);

            //carregando a combo de ghe Funcao
            iniciaComboGhe(cb_gheFuncao);

            #endregion

            #region cronograma

            // montando grid de atividades do cronograma.
            MontarGrid();

            // preenchendo o texto do titulo do cronograma.

            #endregion

            MontaGridAnexo();
            montaGridEstimativa();
            montaGridCnaeCliente();
            if (ppra.ClienteContratante != null)
                montaGridCnaeContratante();

        }

        private void frm_ppraAlterar_Load(object sender, EventArgs e)
        {
            enableCommand(Convert.ToBoolean(true));
            cb_uf.SelectedValue = Ppra.Uf;
            cb_grauRisco.SelectedValue = Ppra.GrauRisco;
            dt_criacao.Checked = true;
            dt_vencimento.Checked = true;
            //habilitando os botoes do cliente contratante
            grb_grupo1.Enabled = true;
            this.btn_alterarAtividade.Enabled = true;
            this.btn_excluirAtividade.Enabled = true;
            this.btn_incluirAtividade.Enabled = true;
        }

        private void btn_contratante_Click_1(object sender, EventArgs e)
        {

            if (Ppra.ClienteContratante == clienteContratanteCheck)
            {
                text_clienteContratante.Text = Ppra.ClienteContratante.RazaoSocial;
                text_inicioContr.Text = Ppra.DataInicioContrato;
                text_fimContr.Text = Ppra.DataFimContrato;
                text_numContrato.Text = Ppra.NumContrato;
            }
            else
            {
                text_inicioContr.Text = String.Empty;
                text_fimContr.Text = String.Empty;
                text_inicioContr.Text = String.Empty;
                text_numContrato.Text = String.Empty;
            }
        }

        protected override void montaGridCnaeCliente()
        {
            try
            {
                ClienteFacade clienteFacade = ClienteFacade.getInstance();

                dgvCnaeCliente.Columns.Clear();

                DataSet ds = clienteFacade.findAllCnaeByCliente(ppra.VendedorCliente.getCliente());

                dgvCnaeCliente.DataSource = ds.Tables[0].DefaultView;

                dgvCnaeCliente.Columns[0].HeaderText = "id_cnae";
                dgvCnaeCliente.Columns[0].Visible = false;
                dgvCnaeCliente.Columns[1].HeaderText = "codigo";
                dgvCnaeCliente.Columns[2].HeaderText = "atividade";
                dgvCnaeCliente.Columns[3].HeaderText = "grau_de_risco";
                dgvCnaeCliente.Columns[4].HeaderText = "grupo_cipa";
                dgvCnaeCliente.Columns[5].HeaderText = "situação";
                dgvCnaeCliente.Columns[5].Visible = false;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }

        protected override void montaGridCnaeContratante()
        {
            try
            {
                ClienteFacade clienteFacade = ClienteFacade.getInstance();

                dgvCnaeContratante.Columns.Clear();

                DataSet ds = clienteFacade.findAllCnaeByCliente(ppra.ClienteContratante);

                dgvCnaeContratante.DataSource = ds.Tables[0].DefaultView;

                dgvCnaeContratante.Columns[0].HeaderText = "id_cnae";
                dgvCnaeContratante.Columns[0].Visible = false;
                dgvCnaeContratante.Columns[1].HeaderText = "codigo";
                dgvCnaeContratante.Columns[2].HeaderText = "atividade";
                dgvCnaeContratante.Columns[3].HeaderText = "grau_de_risco";
                dgvCnaeContratante.Columns[4].HeaderText = "grupo_cipa";
                dgvCnaeContratante.Columns[5].HeaderText = "situação";
                dgvCnaeContratante.Columns[5].Visible = false;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }
    }
}
