﻿using SWS.Entidade;
using SWS.View.ViewHelper;
using SWS.ViewHelper;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SWS.View
{
    public partial class frmPcmsoObraAlterar : frmPcmsoObraIncluir

    {
        public frmPcmsoObraAlterar(Obra obra) : base()
        {
            InitializeComponent();
            this.Obra = obra;
            ComboHelper.unidadeFederativa(cbUnidadeFederativa);

            this.textObra.Text = this.Obra.LocalObra;
            this.textEnderecoObra.Text = this.Obra.EnderecoObra;
            this.textNumeroObra.Text = this.Obra.NumeroObra;
            this.textComplementoObra.Text = this.Obra.ComplementoObra;
            this.textBairroObra.Text = this.Obra.BairroObra;
            this.cidadeObra = obra.CidadeObra;
            this.textCidadeObra.Text = this.obra.CidadeObra.Nome;
            this.cbUnidadeFederativa.SelectedValue = this.Obra.UnidadeFederativaObra;
            this.textCEPObra.Text = this.Obra.CepObra;
            this.textDescritivoObra.Text = this.obra.DescritivoObra;

        }

        protected override void btnObraGravar_Click(object sender, EventArgs e)
        {
            try
            {
                if (validaCampos())
                {
                    this.Obra.LocalObra = textObra.Text.Trim();
                    this.Obra.EnderecoObra = textEnderecoObra.Text.Trim();
                    this.Obra.NumeroObra = textNumeroObra.Text.Trim();
                    this.Obra.ComplementoObra = textComplementoObra.Text.Trim();
                    this.Obra.BairroObra = textBairroObra.Text.Trim();
                    this.Obra.UnidadeFederativaObra = ((SelectItem)cbUnidadeFederativa.SelectedItem).Valor;
                    this.Obra.CidadeObra = cidadeObra;
                    this.Obra.CepObra = this.textCEPObra.Text;
                    this.Obra.DescritivoObra = textDescritivoObra.Text.Trim();
                    this.Close();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

    }
}
