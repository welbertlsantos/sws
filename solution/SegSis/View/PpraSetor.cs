﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using SWS.Facade;
using SWS.Entidade;
using SWS.View.Resources;


namespace SWS.View
{
    public partial class frm_PpraSetor : BaseFormConsulta
    {
        private Ghe ghe = null;

        public frm_PpraSetor(Ghe ghe)
        {
            InitializeComponent();
            this.ghe = ghe;
            validaPermissoes();
        }

        private void validaPermissoes()
        {
            PermissionamentoFacade permissionamentoFacade = PermissionamentoFacade.getInstance();
            this.btn_novo.Enabled = permissionamentoFacade.hasPermission("SETOR", "INCLUIR");
        }

        private void btn_novo_Click(object sender, EventArgs e)
        {
            frmSetorIncluir formSetorIncluir = new frmSetorIncluir();
            formSetorIncluir.ShowDialog();

            if (formSetorIncluir.Setor != null)
            {
                text_descricao.Text = formSetorIncluir.Setor.Descricao.ToUpper();
                btn_buscar.PerformClick();
            }


        }

        private void btn_fechar_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btn_buscar_Click(object sender, EventArgs e)
        {
            try
            {
                this.grd_setor.Columns.Clear();
                montaDataGrid();
                text_descricao.Focus();
                
            }
            catch (Exception Ex)
            {
                MessageBox.Show(Ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        public void montaDataGrid()
        {
            try
            {
                PpraFacade ppraFacade = PpraFacade.getInstance();

                Setor setor = new Setor(null, text_descricao.Text.Trim().ToUpper(), ApplicationConstants.ATIVO);

                DataSet ds = ppraFacade.findAllSetorAtivo(setor, ghe, null);

                grd_setor.DataSource = ds.Tables["Setores"].DefaultView;

                grd_setor.Columns[0].HeaderText = "id_setor";
                grd_setor.Columns[0].Visible = false;

                grd_setor.Columns[1].HeaderText = "Descrição";
                grd_setor.Columns[1].ReadOnly = true;

                grd_setor.Columns[2].HeaderText = "Situação";
                grd_setor.Columns[2].Visible = false;

                grd_setor.Columns[3].HeaderText = "Selec";
                grd_setor.Columns[3].DisplayIndex = 0;

            }
            catch (Exception Ex)
            {
                MessageBox.Show(Ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
        }

        private void btn_incluir_Click(object sender, EventArgs e)
        {
            try
            {
                PpraFacade ppraFacade = PpraFacade.getInstance();

                if (grd_setor.CurrentRow == null)
                    throw new Exception("Selecione pelo menos uma fonte.");

                foreach (DataGridViewRow dvRow in grd_setor.Rows)
                        if ((Boolean)dvRow.Cells[3].Value)
                            ppraFacade.incluirGheSetor(new GheSetor(null, new Setor((long)dvRow.Cells[0].Value, (String)dvRow.Cells[1].Value, String.Empty), ghe, ApplicationConstants.ATIVO));

                this.Close();
                
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
    }
}
