﻿using SWS.Entidade;
using SWS.Facade;
using SWS.View.Resources;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace SWS.View
{
    public partial class frmDocumentoAlterar : frmDocumentoIncluir
    {
        public frmDocumentoAlterar(Documento documento) : base()
        {
            InitializeComponent();
            this.Documento = documento;
            textDescricao.Text = documento.Descricao;
        }

        protected override void btnGravar_Click(object sender, EventArgs e)
        {
            try
            {
                if (ValidaCamposTela())
                {
                    ProtocoloFacade protocoloFacade = ProtocoloFacade.getInstance();

                    Documento = protocoloFacade.updateDocumento(new Documento(Documento.Id, textDescricao.Text, ApplicationConstants.ATIVO));
                    MessageBox.Show("Protocolo alterado com suceso.", "Confirmação", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    this.Close();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
    }
}
