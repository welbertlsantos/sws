﻿namespace SWS.View
{
    partial class frmPpraPrincipal
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            this.flpAcao = new System.Windows.Forms.FlowLayoutPanel();
            this.btnIncluir = new System.Windows.Forms.Button();
            this.btnAlterar = new System.Windows.Forms.Button();
            this.btnPesquisar = new System.Windows.Forms.Button();
            this.btnCorrigir = new System.Windows.Forms.Button();
            this.btnRevisar = new System.Windows.Forms.Button();
            this.btnFinalizar = new System.Windows.Forms.Button();
            this.btnAnalisar = new System.Windows.Forms.Button();
            this.btnImprimir = new System.Windows.Forms.Button();
            this.btnExcluir = new System.Windows.Forms.Button();
            this.btnFechar = new System.Windows.Forms.Button();
            this.btnExcluirElaborador = new System.Windows.Forms.Button();
            this.btnExcluirCliente = new System.Windows.Forms.Button();
            this.lblDataElaboracao = new System.Windows.Forms.TextBox();
            this.textCodigo = new System.Windows.Forms.MaskedTextBox();
            this.cbSituacao = new SWS.ComboBoxWithBorder();
            this.btnElaborador = new System.Windows.Forms.Button();
            this.textUsuarioElaborador = new System.Windows.Forms.TextBox();
            this.btCliente = new System.Windows.Forms.Button();
            this.textCliente = new System.Windows.Forms.TextBox();
            this.dataInicial = new System.Windows.Forms.DateTimePicker();
            this.dataFinal = new System.Windows.Forms.DateTimePicker();
            this.lblSituacao = new System.Windows.Forms.TextBox();
            this.lblElaborador = new System.Windows.Forms.TextBox();
            this.lblCliente = new System.Windows.Forms.TextBox();
            this.lblCodigo = new System.Windows.Forms.TextBox();
            this.btnExcluirResponsavel = new System.Windows.Forms.Button();
            this.btnResponsavel = new System.Windows.Forms.Button();
            this.textResponsavel = new System.Windows.Forms.TextBox();
            this.lblResponsavel = new System.Windows.Forms.TextBox();
            this.grb_gride = new System.Windows.Forms.GroupBox();
            this.dgvEstudo = new System.Windows.Forms.DataGridView();
            this.lblTextoInformativoCorrecao = new System.Windows.Forms.Label();
            this.grbLegenda = new System.Windows.Forms.GroupBox();
            this.textLegendaAnalise = new System.Windows.Forms.Label();
            this.lblLegendaAnalise = new System.Windows.Forms.Label();
            this.lblTextoFechado = new System.Windows.Forms.Label();
            this.lblLegendaFechado = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.spForm)).BeginInit();
            this.spForm.Panel1.SuspendLayout();
            this.spForm.Panel2.SuspendLayout();
            this.spForm.SuspendLayout();
            this.pnlForm.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.banner)).BeginInit();
            this.flpAcao.SuspendLayout();
            this.grb_gride.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvEstudo)).BeginInit();
            this.grbLegenda.SuspendLayout();
            this.SuspendLayout();
            // 
            // spForm
            // 
            // 
            // spForm.Panel1
            // 
            this.spForm.Panel1.Controls.Add(this.flpAcao);
            // 
            // pnlForm
            // 
            this.pnlForm.Controls.Add(this.lblTextoInformativoCorrecao);
            this.pnlForm.Controls.Add(this.grbLegenda);
            this.pnlForm.Controls.Add(this.grb_gride);
            this.pnlForm.Controls.Add(this.btnExcluirResponsavel);
            this.pnlForm.Controls.Add(this.btnResponsavel);
            this.pnlForm.Controls.Add(this.textResponsavel);
            this.pnlForm.Controls.Add(this.lblResponsavel);
            this.pnlForm.Controls.Add(this.btnExcluirElaborador);
            this.pnlForm.Controls.Add(this.btnExcluirCliente);
            this.pnlForm.Controls.Add(this.lblDataElaboracao);
            this.pnlForm.Controls.Add(this.textCodigo);
            this.pnlForm.Controls.Add(this.cbSituacao);
            this.pnlForm.Controls.Add(this.btnElaborador);
            this.pnlForm.Controls.Add(this.textUsuarioElaborador);
            this.pnlForm.Controls.Add(this.btCliente);
            this.pnlForm.Controls.Add(this.textCliente);
            this.pnlForm.Controls.Add(this.dataInicial);
            this.pnlForm.Controls.Add(this.dataFinal);
            this.pnlForm.Controls.Add(this.lblSituacao);
            this.pnlForm.Controls.Add(this.lblElaborador);
            this.pnlForm.Controls.Add(this.lblCliente);
            this.pnlForm.Controls.Add(this.lblCodigo);
            // 
            // flpAcao
            // 
            this.flpAcao.Controls.Add(this.btnIncluir);
            this.flpAcao.Controls.Add(this.btnAlterar);
            this.flpAcao.Controls.Add(this.btnPesquisar);
            this.flpAcao.Controls.Add(this.btnCorrigir);
            this.flpAcao.Controls.Add(this.btnRevisar);
            this.flpAcao.Controls.Add(this.btnFinalizar);
            this.flpAcao.Controls.Add(this.btnAnalisar);
            this.flpAcao.Controls.Add(this.btnImprimir);
            this.flpAcao.Controls.Add(this.btnExcluir);
            this.flpAcao.Controls.Add(this.btnFechar);
            this.flpAcao.Dock = System.Windows.Forms.DockStyle.Fill;
            this.flpAcao.Location = new System.Drawing.Point(0, 0);
            this.flpAcao.Name = "flpAcao";
            this.flpAcao.Size = new System.Drawing.Size(784, 35);
            this.flpAcao.TabIndex = 0;
            // 
            // btnIncluir
            // 
            this.btnIncluir.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnIncluir.Image = global::SWS.Properties.Resources.icone_mais;
            this.btnIncluir.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnIncluir.Location = new System.Drawing.Point(3, 3);
            this.btnIncluir.Name = "btnIncluir";
            this.btnIncluir.Size = new System.Drawing.Size(71, 22);
            this.btnIncluir.TabIndex = 102;
            this.btnIncluir.Text = "&Incluir";
            this.btnIncluir.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnIncluir.UseVisualStyleBackColor = true;
            this.btnIncluir.Click += new System.EventHandler(this.btnIncluir_Click);
            // 
            // btnAlterar
            // 
            this.btnAlterar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnAlterar.Image = global::SWS.Properties.Resources.Alterar;
            this.btnAlterar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnAlterar.Location = new System.Drawing.Point(80, 3);
            this.btnAlterar.Name = "btnAlterar";
            this.btnAlterar.Size = new System.Drawing.Size(71, 22);
            this.btnAlterar.TabIndex = 103;
            this.btnAlterar.Text = "&Alterar";
            this.btnAlterar.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnAlterar.UseVisualStyleBackColor = true;
            this.btnAlterar.Click += new System.EventHandler(this.btnAlterar_Click);
            // 
            // btnPesquisar
            // 
            this.btnPesquisar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnPesquisar.Image = global::SWS.Properties.Resources.lupa;
            this.btnPesquisar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnPesquisar.Location = new System.Drawing.Point(157, 3);
            this.btnPesquisar.Name = "btnPesquisar";
            this.btnPesquisar.Size = new System.Drawing.Size(71, 22);
            this.btnPesquisar.TabIndex = 111;
            this.btnPesquisar.Text = "Pesquisar";
            this.btnPesquisar.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnPesquisar.UseVisualStyleBackColor = true;
            this.btnPesquisar.Click += new System.EventHandler(this.bt_pesquisar_Click);
            // 
            // btnCorrigir
            // 
            this.btnCorrigir.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnCorrigir.Image = global::SWS.Properties.Resources.crayon_icon_clip_art_p;
            this.btnCorrigir.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnCorrigir.Location = new System.Drawing.Point(234, 3);
            this.btnCorrigir.Name = "btnCorrigir";
            this.btnCorrigir.Size = new System.Drawing.Size(71, 22);
            this.btnCorrigir.TabIndex = 110;
            this.btnCorrigir.Text = "&Corrigir";
            this.btnCorrigir.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnCorrigir.UseVisualStyleBackColor = true;
            this.btnCorrigir.Click += new System.EventHandler(this.btnCorrigir_Click);
            // 
            // btnRevisar
            // 
            this.btnRevisar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnRevisar.Image = global::SWS.Properties.Resources.devolucao;
            this.btnRevisar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnRevisar.Location = new System.Drawing.Point(311, 3);
            this.btnRevisar.Name = "btnRevisar";
            this.btnRevisar.Size = new System.Drawing.Size(71, 22);
            this.btnRevisar.TabIndex = 108;
            this.btnRevisar.Text = "&Revisão";
            this.btnRevisar.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnRevisar.UseVisualStyleBackColor = true;
            this.btnRevisar.Click += new System.EventHandler(this.btnRevisar_Click);
            // 
            // btnFinalizar
            // 
            this.btnFinalizar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnFinalizar.Image = global::SWS.Properties.Resources.fechar_ico;
            this.btnFinalizar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnFinalizar.Location = new System.Drawing.Point(388, 3);
            this.btnFinalizar.Name = "btnFinalizar";
            this.btnFinalizar.Size = new System.Drawing.Size(71, 22);
            this.btnFinalizar.TabIndex = 107;
            this.btnFinalizar.Text = "&Finalizar";
            this.btnFinalizar.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnFinalizar.UseVisualStyleBackColor = true;
            this.btnFinalizar.Click += new System.EventHandler(this.btnFinalizar_Click);
            // 
            // btnAnalisar
            // 
            this.btnAnalisar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnAnalisar.Image = global::SWS.Properties.Resources.enviar_mensagem_318_10100;
            this.btnAnalisar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnAnalisar.Location = new System.Drawing.Point(465, 3);
            this.btnAnalisar.Name = "btnAnalisar";
            this.btnAnalisar.Size = new System.Drawing.Size(71, 22);
            this.btnAnalisar.TabIndex = 106;
            this.btnAnalisar.Text = "&Enviar";
            this.btnAnalisar.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnAnalisar.UseVisualStyleBackColor = true;
            this.btnAnalisar.Click += new System.EventHandler(this.btnAnalisar_Click);
            // 
            // btnImprimir
            // 
            this.btnImprimir.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnImprimir.Image = global::SWS.Properties.Resources.impressora1;
            this.btnImprimir.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnImprimir.Location = new System.Drawing.Point(542, 3);
            this.btnImprimir.Name = "btnImprimir";
            this.btnImprimir.Size = new System.Drawing.Size(71, 22);
            this.btnImprimir.TabIndex = 105;
            this.btnImprimir.Text = "Imprimir";
            this.btnImprimir.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnImprimir.UseVisualStyleBackColor = true;
            this.btnImprimir.Click += new System.EventHandler(this.btnImprimir_Click);
            // 
            // btnExcluir
            // 
            this.btnExcluir.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnExcluir.Image = global::SWS.Properties.Resources.lixeira;
            this.btnExcluir.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnExcluir.Location = new System.Drawing.Point(619, 3);
            this.btnExcluir.Name = "btnExcluir";
            this.btnExcluir.Size = new System.Drawing.Size(71, 22);
            this.btnExcluir.TabIndex = 104;
            this.btnExcluir.Text = "&Excluir";
            this.btnExcluir.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnExcluir.UseVisualStyleBackColor = true;
            this.btnExcluir.Click += new System.EventHandler(this.btnExcluir_Click);
            // 
            // btnFechar
            // 
            this.btnFechar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnFechar.Image = global::SWS.Properties.Resources.close;
            this.btnFechar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnFechar.Location = new System.Drawing.Point(696, 3);
            this.btnFechar.Name = "btnFechar";
            this.btnFechar.Size = new System.Drawing.Size(71, 22);
            this.btnFechar.TabIndex = 109;
            this.btnFechar.Text = "&Fechar";
            this.btnFechar.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnFechar.UseVisualStyleBackColor = true;
            this.btnFechar.Click += new System.EventHandler(this.btnFechar_Click);
            // 
            // btnExcluirElaborador
            // 
            this.btnExcluirElaborador.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnExcluirElaborador.Image = global::SWS.Properties.Resources.close;
            this.btnExcluirElaborador.Location = new System.Drawing.Point(734, 23);
            this.btnExcluirElaborador.Name = "btnExcluirElaborador";
            this.btnExcluirElaborador.Size = new System.Drawing.Size(30, 21);
            this.btnExcluirElaborador.TabIndex = 52;
            this.btnExcluirElaborador.UseVisualStyleBackColor = true;
            this.btnExcluirElaborador.Click += new System.EventHandler(this.btnExcluirElaborador_Click);
            // 
            // btnExcluirCliente
            // 
            this.btnExcluirCliente.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnExcluirCliente.Image = global::SWS.Properties.Resources.close;
            this.btnExcluirCliente.Location = new System.Drawing.Point(734, 3);
            this.btnExcluirCliente.Name = "btnExcluirCliente";
            this.btnExcluirCliente.Size = new System.Drawing.Size(30, 21);
            this.btnExcluirCliente.TabIndex = 51;
            this.btnExcluirCliente.UseVisualStyleBackColor = true;
            this.btnExcluirCliente.Click += new System.EventHandler(this.btnExcluirCliente_Click);
            // 
            // lblDataElaboracao
            // 
            this.lblDataElaboracao.BackColor = System.Drawing.Color.LightGray;
            this.lblDataElaboracao.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblDataElaboracao.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDataElaboracao.Location = new System.Drawing.Point(3, 23);
            this.lblDataElaboracao.Name = "lblDataElaboracao";
            this.lblDataElaboracao.ReadOnly = true;
            this.lblDataElaboracao.Size = new System.Drawing.Size(138, 21);
            this.lblDataElaboracao.TabIndex = 40;
            this.lblDataElaboracao.TabStop = false;
            this.lblDataElaboracao.Text = "Data de criação";
            // 
            // textCodigo
            // 
            this.textCodigo.BackColor = System.Drawing.Color.White;
            this.textCodigo.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textCodigo.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textCodigo.Location = new System.Drawing.Point(140, 3);
            this.textCodigo.Mask = "0000,00,000000/00";
            this.textCodigo.Name = "textCodigo";
            this.textCodigo.PromptChar = ' ';
            this.textCodigo.Size = new System.Drawing.Size(213, 21);
            this.textCodigo.TabIndex = 39;
            // 
            // cbSituacao
            // 
            this.cbSituacao.BackColor = System.Drawing.Color.LightGray;
            this.cbSituacao.BorderColor = System.Drawing.Color.DimGray;
            this.cbSituacao.BorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid;
            this.cbSituacao.FormattingEnabled = true;
            this.cbSituacao.Location = new System.Drawing.Point(140, 43);
            this.cbSituacao.Name = "cbSituacao";
            this.cbSituacao.Size = new System.Drawing.Size(213, 21);
            this.cbSituacao.TabIndex = 45;
            // 
            // btnElaborador
            // 
            this.btnElaborador.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnElaborador.Image = global::SWS.Properties.Resources.busca;
            this.btnElaborador.Location = new System.Drawing.Point(705, 23);
            this.btnElaborador.Name = "btnElaborador";
            this.btnElaborador.Size = new System.Drawing.Size(30, 21);
            this.btnElaborador.TabIndex = 48;
            this.btnElaborador.UseVisualStyleBackColor = true;
            this.btnElaborador.Click += new System.EventHandler(this.btnElaborador_Click);
            // 
            // textUsuarioElaborador
            // 
            this.textUsuarioElaborador.BackColor = System.Drawing.SystemColors.Control;
            this.textUsuarioElaborador.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textUsuarioElaborador.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textUsuarioElaborador.Location = new System.Drawing.Point(496, 23);
            this.textUsuarioElaborador.MaxLength = 100;
            this.textUsuarioElaborador.Name = "textUsuarioElaborador";
            this.textUsuarioElaborador.ReadOnly = true;
            this.textUsuarioElaborador.Size = new System.Drawing.Size(210, 21);
            this.textUsuarioElaborador.TabIndex = 50;
            this.textUsuarioElaborador.TabStop = false;
            // 
            // btCliente
            // 
            this.btCliente.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btCliente.Image = global::SWS.Properties.Resources.busca;
            this.btCliente.Location = new System.Drawing.Point(705, 3);
            this.btCliente.Name = "btCliente";
            this.btCliente.Size = new System.Drawing.Size(30, 21);
            this.btCliente.TabIndex = 47;
            this.btCliente.UseVisualStyleBackColor = true;
            this.btCliente.Click += new System.EventHandler(this.btCliente_Click);
            // 
            // textCliente
            // 
            this.textCliente.BackColor = System.Drawing.SystemColors.Control;
            this.textCliente.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textCliente.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textCliente.Location = new System.Drawing.Point(496, 3);
            this.textCliente.MaxLength = 100;
            this.textCliente.Name = "textCliente";
            this.textCliente.ReadOnly = true;
            this.textCliente.Size = new System.Drawing.Size(210, 21);
            this.textCliente.TabIndex = 49;
            this.textCliente.TabStop = false;
            // 
            // dataInicial
            // 
            this.dataInicial.Checked = false;
            this.dataInicial.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dataInicial.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dataInicial.Location = new System.Drawing.Point(140, 23);
            this.dataInicial.Name = "dataInicial";
            this.dataInicial.ShowCheckBox = true;
            this.dataInicial.Size = new System.Drawing.Size(107, 21);
            this.dataInicial.TabIndex = 41;
            // 
            // dataFinal
            // 
            this.dataFinal.Checked = false;
            this.dataFinal.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dataFinal.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dataFinal.Location = new System.Drawing.Point(246, 23);
            this.dataFinal.Name = "dataFinal";
            this.dataFinal.ShowCheckBox = true;
            this.dataFinal.Size = new System.Drawing.Size(107, 21);
            this.dataFinal.TabIndex = 43;
            // 
            // lblSituacao
            // 
            this.lblSituacao.BackColor = System.Drawing.Color.LightGray;
            this.lblSituacao.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblSituacao.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSituacao.Location = new System.Drawing.Point(3, 43);
            this.lblSituacao.Name = "lblSituacao";
            this.lblSituacao.ReadOnly = true;
            this.lblSituacao.Size = new System.Drawing.Size(138, 21);
            this.lblSituacao.TabIndex = 46;
            this.lblSituacao.TabStop = false;
            this.lblSituacao.Text = "Situação";
            // 
            // lblElaborador
            // 
            this.lblElaborador.BackColor = System.Drawing.Color.LightGray;
            this.lblElaborador.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblElaborador.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblElaborador.Location = new System.Drawing.Point(359, 23);
            this.lblElaborador.Name = "lblElaborador";
            this.lblElaborador.ReadOnly = true;
            this.lblElaborador.Size = new System.Drawing.Size(138, 21);
            this.lblElaborador.TabIndex = 44;
            this.lblElaborador.TabStop = false;
            this.lblElaborador.Text = "Elaborador";
            // 
            // lblCliente
            // 
            this.lblCliente.BackColor = System.Drawing.Color.LightGray;
            this.lblCliente.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblCliente.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCliente.Location = new System.Drawing.Point(359, 3);
            this.lblCliente.Name = "lblCliente";
            this.lblCliente.ReadOnly = true;
            this.lblCliente.Size = new System.Drawing.Size(138, 21);
            this.lblCliente.TabIndex = 42;
            this.lblCliente.TabStop = false;
            this.lblCliente.Text = "Cliente";
            // 
            // lblCodigo
            // 
            this.lblCodigo.BackColor = System.Drawing.Color.LightGray;
            this.lblCodigo.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblCodigo.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCodigo.Location = new System.Drawing.Point(3, 3);
            this.lblCodigo.Name = "lblCodigo";
            this.lblCodigo.ReadOnly = true;
            this.lblCodigo.Size = new System.Drawing.Size(138, 21);
            this.lblCodigo.TabIndex = 38;
            this.lblCodigo.TabStop = false;
            this.lblCodigo.Text = "Código do Estudo";
            // 
            // btnExcluirResponsavel
            // 
            this.btnExcluirResponsavel.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnExcluirResponsavel.Image = global::SWS.Properties.Resources.close;
            this.btnExcluirResponsavel.Location = new System.Drawing.Point(734, 43);
            this.btnExcluirResponsavel.Name = "btnExcluirResponsavel";
            this.btnExcluirResponsavel.Size = new System.Drawing.Size(30, 21);
            this.btnExcluirResponsavel.TabIndex = 56;
            this.btnExcluirResponsavel.UseVisualStyleBackColor = true;
            this.btnExcluirResponsavel.Click += new System.EventHandler(this.btnExcluirResponsavel_Click);
            // 
            // btnResponsavel
            // 
            this.btnResponsavel.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnResponsavel.Image = global::SWS.Properties.Resources.busca;
            this.btnResponsavel.Location = new System.Drawing.Point(705, 43);
            this.btnResponsavel.Name = "btnResponsavel";
            this.btnResponsavel.Size = new System.Drawing.Size(30, 21);
            this.btnResponsavel.TabIndex = 54;
            this.btnResponsavel.UseVisualStyleBackColor = true;
            this.btnResponsavel.Click += new System.EventHandler(this.btnResponsavel_Click);
            // 
            // textResponsavel
            // 
            this.textResponsavel.BackColor = System.Drawing.SystemColors.Control;
            this.textResponsavel.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textResponsavel.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textResponsavel.Location = new System.Drawing.Point(496, 43);
            this.textResponsavel.MaxLength = 100;
            this.textResponsavel.Name = "textResponsavel";
            this.textResponsavel.ReadOnly = true;
            this.textResponsavel.Size = new System.Drawing.Size(210, 21);
            this.textResponsavel.TabIndex = 55;
            this.textResponsavel.TabStop = false;
            // 
            // lblResponsavel
            // 
            this.lblResponsavel.BackColor = System.Drawing.Color.LightGray;
            this.lblResponsavel.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblResponsavel.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblResponsavel.Location = new System.Drawing.Point(359, 43);
            this.lblResponsavel.Name = "lblResponsavel";
            this.lblResponsavel.ReadOnly = true;
            this.lblResponsavel.Size = new System.Drawing.Size(138, 21);
            this.lblResponsavel.TabIndex = 53;
            this.lblResponsavel.TabStop = false;
            this.lblResponsavel.Text = "Responsável";
            // 
            // grb_gride
            // 
            this.grb_gride.BackColor = System.Drawing.Color.White;
            this.grb_gride.Controls.Add(this.dgvEstudo);
            this.grb_gride.Location = new System.Drawing.Point(3, 70);
            this.grb_gride.Name = "grb_gride";
            this.grb_gride.Size = new System.Drawing.Size(761, 348);
            this.grb_gride.TabIndex = 101;
            this.grb_gride.TabStop = false;
            this.grb_gride.Text = "PPRAs";
            // 
            // dgvEstudo
            // 
            this.dgvEstudo.AllowUserToAddRows = false;
            this.dgvEstudo.AllowUserToDeleteRows = false;
            this.dgvEstudo.AllowUserToOrderColumns = true;
            this.dgvEstudo.AllowUserToResizeRows = false;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.Gainsboro;
            this.dgvEstudo.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle1;
            this.dgvEstudo.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this.dgvEstudo.BackgroundColor = System.Drawing.Color.White;
            this.dgvEstudo.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.Color.LightGray;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvEstudo.DefaultCellStyle = dataGridViewCellStyle2;
            this.dgvEstudo.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvEstudo.Location = new System.Drawing.Point(3, 16);
            this.dgvEstudo.MultiSelect = false;
            this.dgvEstudo.Name = "dgvEstudo";
            this.dgvEstudo.ReadOnly = true;
            this.dgvEstudo.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvEstudo.Size = new System.Drawing.Size(755, 329);
            this.dgvEstudo.TabIndex = 12;
            this.dgvEstudo.CellContentDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvEstudo_CellContentDoubleClick);
            this.dgvEstudo.RowPrePaint += new System.Windows.Forms.DataGridViewRowPrePaintEventHandler(this.dgvEstudo_RowPrePaint);
            // 
            // lblTextoInformativoCorrecao
            // 
            this.lblTextoInformativoCorrecao.AutoSize = true;
            this.lblTextoInformativoCorrecao.ForeColor = System.Drawing.Color.Red;
            this.lblTextoInformativoCorrecao.Location = new System.Drawing.Point(239, 425);
            this.lblTextoInformativoCorrecao.Name = "lblTextoInformativoCorrecao";
            this.lblTextoInformativoCorrecao.Size = new System.Drawing.Size(307, 26);
            this.lblTextoInformativoCorrecao.TabIndex = 104;
            this.lblTextoInformativoCorrecao.Text = "Duplo clique no estudo selecionado para visualizar os históricos\r\nde correções.";
            // 
            // grbLegenda
            // 
            this.grbLegenda.Controls.Add(this.textLegendaAnalise);
            this.grbLegenda.Controls.Add(this.lblLegendaAnalise);
            this.grbLegenda.Controls.Add(this.lblTextoFechado);
            this.grbLegenda.Controls.Add(this.lblLegendaFechado);
            this.grbLegenda.Location = new System.Drawing.Point(6, 415);
            this.grbLegenda.Name = "grbLegenda";
            this.grbLegenda.Size = new System.Drawing.Size(211, 44);
            this.grbLegenda.TabIndex = 103;
            this.grbLegenda.TabStop = false;
            this.grbLegenda.Text = "Legenda";
            // 
            // textLegendaAnalise
            // 
            this.textLegendaAnalise.AutoSize = true;
            this.textLegendaAnalise.Location = new System.Drawing.Point(106, 20);
            this.textLegendaAnalise.Name = "textLegendaAnalise";
            this.textLegendaAnalise.Size = new System.Drawing.Size(58, 13);
            this.textLegendaAnalise.TabIndex = 3;
            this.textLegendaAnalise.Text = "Em análise";
            // 
            // lblLegendaAnalise
            // 
            this.lblLegendaAnalise.AutoSize = true;
            this.lblLegendaAnalise.BackColor = System.Drawing.Color.Blue;
            this.lblLegendaAnalise.ForeColor = System.Drawing.Color.Black;
            this.lblLegendaAnalise.Location = new System.Drawing.Point(87, 20);
            this.lblLegendaAnalise.Name = "lblLegendaAnalise";
            this.lblLegendaAnalise.Size = new System.Drawing.Size(13, 13);
            this.lblLegendaAnalise.TabIndex = 2;
            this.lblLegendaAnalise.Text = "  ";
            // 
            // lblTextoFechado
            // 
            this.lblTextoFechado.AutoSize = true;
            this.lblTextoFechado.Location = new System.Drawing.Point(30, 20);
            this.lblTextoFechado.Name = "lblTextoFechado";
            this.lblTextoFechado.Size = new System.Drawing.Size(54, 13);
            this.lblTextoFechado.TabIndex = 1;
            this.lblTextoFechado.Text = "Finalizado";
            // 
            // lblLegendaFechado
            // 
            this.lblLegendaFechado.AutoSize = true;
            this.lblLegendaFechado.BackColor = System.Drawing.Color.Green;
            this.lblLegendaFechado.ForeColor = System.Drawing.Color.Black;
            this.lblLegendaFechado.Location = new System.Drawing.Point(11, 20);
            this.lblLegendaFechado.Name = "lblLegendaFechado";
            this.lblLegendaFechado.Size = new System.Drawing.Size(13, 13);
            this.lblLegendaFechado.TabIndex = 0;
            this.lblLegendaFechado.Text = "  ";
            // 
            // frmPpraPrincipal
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(784, 561);
            this.Name = "frmPpraPrincipal";
            this.Text = "GERENCIAR PPRAS";
            this.spForm.Panel1.ResumeLayout(false);
            this.spForm.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.spForm)).EndInit();
            this.spForm.ResumeLayout(false);
            this.pnlForm.ResumeLayout(false);
            this.pnlForm.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.banner)).EndInit();
            this.flpAcao.ResumeLayout(false);
            this.grb_gride.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvEstudo)).EndInit();
            this.grbLegenda.ResumeLayout(false);
            this.grbLegenda.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.FlowLayoutPanel flpAcao;
        private System.Windows.Forms.Button btnIncluir;
        private System.Windows.Forms.Button btnAlterar;
        private System.Windows.Forms.Button btnCorrigir;
        private System.Windows.Forms.Button btnRevisar;
        private System.Windows.Forms.Button btnFinalizar;
        private System.Windows.Forms.Button btnAnalisar;
        private System.Windows.Forms.Button btnImprimir;
        private System.Windows.Forms.Button btnExcluir;
        private System.Windows.Forms.Button btnFechar;
        private System.Windows.Forms.Button btnExcluirElaborador;
        private System.Windows.Forms.Button btnExcluirCliente;
        private System.Windows.Forms.TextBox lblDataElaboracao;
        public System.Windows.Forms.MaskedTextBox textCodigo;
        private ComboBoxWithBorder cbSituacao;
        private System.Windows.Forms.Button btnElaborador;
        public System.Windows.Forms.TextBox textUsuarioElaborador;
        private System.Windows.Forms.Button btCliente;
        public System.Windows.Forms.TextBox textCliente;
        private System.Windows.Forms.DateTimePicker dataInicial;
        private System.Windows.Forms.DateTimePicker dataFinal;
        private System.Windows.Forms.TextBox lblSituacao;
        private System.Windows.Forms.TextBox lblElaborador;
        private System.Windows.Forms.TextBox lblCliente;
        private System.Windows.Forms.TextBox lblCodigo;
        private System.Windows.Forms.Button btnExcluirResponsavel;
        private System.Windows.Forms.Button btnResponsavel;
        public System.Windows.Forms.TextBox textResponsavel;
        private System.Windows.Forms.TextBox lblResponsavel;
        private System.Windows.Forms.Button btnPesquisar;
        private System.Windows.Forms.GroupBox grb_gride;
        private System.Windows.Forms.DataGridView dgvEstudo;
        private System.Windows.Forms.Label lblTextoInformativoCorrecao;
        private System.Windows.Forms.GroupBox grbLegenda;
        private System.Windows.Forms.Label textLegendaAnalise;
        private System.Windows.Forms.Label lblLegendaAnalise;
        private System.Windows.Forms.Label lblTextoFechado;
        private System.Windows.Forms.Label lblLegendaFechado;
    }
}