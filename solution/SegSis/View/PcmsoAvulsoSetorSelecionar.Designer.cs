﻿namespace SegSis.View
{
    partial class frm_PcmsoAvulsoSetorSelecionar
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frm_PcmsoAvulsoSetorSelecionar));
            this.btn_buscar = new System.Windows.Forms.Button();
            this.grd_setor = new System.Windows.Forms.DataGridView();
            this.grb_paginacao = new System.Windows.Forms.GroupBox();
            this.btn_incluir = new System.Windows.Forms.Button();
            this.btn_fechar = new System.Windows.Forms.Button();
            this.btn_novo = new System.Windows.Forms.Button();
            this.grb_grid = new System.Windows.Forms.GroupBox();
            this.text_descricao = new System.Windows.Forms.TextBox();
            this.grb_dados = new System.Windows.Forms.GroupBox();
            ((System.ComponentModel.ISupportInitialize)(this.grd_setor)).BeginInit();
            this.grb_paginacao.SuspendLayout();
            this.grb_grid.SuspendLayout();
            this.grb_dados.SuspendLayout();
            this.SuspendLayout();
            // 
            // btn_buscar
            // 
            this.btn_buscar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_buscar.Image = global::SegSis.Properties.Resources.lupa;
            this.btn_buscar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btn_buscar.Location = new System.Drawing.Point(6, 45);
            this.btn_buscar.Name = "btn_buscar";
            this.btn_buscar.Size = new System.Drawing.Size(78, 23);
            this.btn_buscar.TabIndex = 2;
            this.btn_buscar.Text = "&Buscar";
            this.btn_buscar.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btn_buscar.UseVisualStyleBackColor = true;
            this.btn_buscar.Click += new System.EventHandler(this.btn_buscar_Click);
            // 
            // grd_setor
            // 
            this.grd_setor.AllowUserToAddRows = false;
            this.grd_setor.AllowUserToDeleteRows = false;
            this.grd_setor.AllowUserToOrderColumns = true;
            this.grd_setor.AllowUserToResizeRows = false;
            this.grd_setor.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this.grd_setor.BackgroundColor = System.Drawing.Color.White;
            this.grd_setor.Location = new System.Drawing.Point(9, 11);
            this.grd_setor.Name = "grd_setor";
            this.grd_setor.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.grd_setor.Size = new System.Drawing.Size(578, 233);
            this.grd_setor.TabIndex = 3;
            // 
            // grb_paginacao
            // 
            this.grb_paginacao.Controls.Add(this.btn_incluir);
            this.grb_paginacao.Controls.Add(this.btn_fechar);
            this.grb_paginacao.Controls.Add(this.btn_novo);
            this.grb_paginacao.Location = new System.Drawing.Point(4, 345);
            this.grb_paginacao.Name = "grb_paginacao";
            this.grb_paginacao.Size = new System.Drawing.Size(593, 51);
            this.grb_paginacao.TabIndex = 8;
            this.grb_paginacao.TabStop = false;
            // 
            // btn_incluir
            // 
            this.btn_incluir.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_incluir.Image = global::SegSis.Properties.Resources.Gravar;
            this.btn_incluir.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btn_incluir.Location = new System.Drawing.Point(9, 19);
            this.btn_incluir.Name = "btn_incluir";
            this.btn_incluir.Size = new System.Drawing.Size(75, 23);
            this.btn_incluir.TabIndex = 4;
            this.btn_incluir.Text = "&Incluir";
            this.btn_incluir.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btn_incluir.UseVisualStyleBackColor = true;
            this.btn_incluir.Click += new System.EventHandler(this.btn_incluir_Click);
            // 
            // btn_fechar
            // 
            this.btn_fechar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_fechar.Image = global::SegSis.Properties.Resources.close;
            this.btn_fechar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btn_fechar.Location = new System.Drawing.Point(171, 19);
            this.btn_fechar.Name = "btn_fechar";
            this.btn_fechar.Size = new System.Drawing.Size(75, 23);
            this.btn_fechar.TabIndex = 6;
            this.btn_fechar.Text = "&Fechar";
            this.btn_fechar.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btn_fechar.UseVisualStyleBackColor = true;
            this.btn_fechar.Click += new System.EventHandler(this.btn_fechar_Click);
            // 
            // btn_novo
            // 
            this.btn_novo.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_novo.Image = global::SegSis.Properties.Resources.icone_mais;
            this.btn_novo.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btn_novo.Location = new System.Drawing.Point(90, 19);
            this.btn_novo.Name = "btn_novo";
            this.btn_novo.Size = new System.Drawing.Size(75, 23);
            this.btn_novo.TabIndex = 5;
            this.btn_novo.Text = "&Novo";
            this.btn_novo.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btn_novo.UseVisualStyleBackColor = true;
            this.btn_novo.Click += new System.EventHandler(this.btn_novo_Click);
            // 
            // grb_grid
            // 
            this.grb_grid.Controls.Add(this.grd_setor);
            this.grb_grid.Location = new System.Drawing.Point(4, 89);
            this.grb_grid.Name = "grb_grid";
            this.grb_grid.Size = new System.Drawing.Size(593, 250);
            this.grb_grid.TabIndex = 7;
            this.grb_grid.TabStop = false;
            // 
            // text_descricao
            // 
            this.text_descricao.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.text_descricao.Location = new System.Drawing.Point(6, 19);
            this.text_descricao.MaxLength = 100;
            this.text_descricao.Name = "text_descricao";
            this.text_descricao.Size = new System.Drawing.Size(581, 20);
            this.text_descricao.TabIndex = 1;
            // 
            // grb_dados
            // 
            this.grb_dados.Controls.Add(this.btn_buscar);
            this.grb_dados.Controls.Add(this.text_descricao);
            this.grb_dados.Location = new System.Drawing.Point(4, 5);
            this.grb_dados.Name = "grb_dados";
            this.grb_dados.Size = new System.Drawing.Size(593, 78);
            this.grb_dados.TabIndex = 6;
            this.grb_dados.TabStop = false;
            this.grb_dados.Text = "Descrição";
            // 
            // frm_PcmsoAvulsoSetorSelecionar
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(600, 400);
            this.ControlBox = false;
            this.Controls.Add(this.grb_paginacao);
            this.Controls.Add(this.grb_grid);
            this.Controls.Add(this.grb_dados);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "frm_PcmsoAvulsoSetorSelecionar";
            this.Text = "SELECIONAR SETOR";
            ((System.ComponentModel.ISupportInitialize)(this.grd_setor)).EndInit();
            this.grb_paginacao.ResumeLayout(false);
            this.grb_grid.ResumeLayout(false);
            this.grb_dados.ResumeLayout(false);
            this.grb_dados.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btn_buscar;
        private System.Windows.Forms.DataGridView grd_setor;
        private System.Windows.Forms.GroupBox grb_paginacao;
        private System.Windows.Forms.Button btn_incluir;
        private System.Windows.Forms.Button btn_fechar;
        private System.Windows.Forms.Button btn_novo;
        private System.Windows.Forms.GroupBox grb_grid;
        private System.Windows.Forms.TextBox text_descricao;
        private System.Windows.Forms.GroupBox grb_dados;
    }
}