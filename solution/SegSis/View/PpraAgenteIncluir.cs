﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using SWS.Facade;
using SWS.Entidade;
using SWS.View.Resources;
using SWS.ViewHelper;  
using SWS.Excecao;
using SWS.View.ViewHelper;  


namespace SWS.View
{

    public partial class frm_ppraAgenteIncluir : BaseFormConsulta
    {
        private Agente agente;
        private GradSoma gradSoma;
        private GradExposicao gradExposicao;
        private GradEfeito gradEfeito;

        private static String msg1 = "Selecione um agente.";
        private static String msg2 = "Selecione uma exposição.";

        private GheFonte gheFonte;
        
        SelectItem naoIdentificado = new SelectItem("6", "NÃO IDENTIFICADO");

        public frm_ppraAgenteIncluir(GheFonte gheFonte)
        {
            InitializeComponent();
            iniciaComboeExposicao(cb_exposicao);
            iniciaComboeEfeito(cb_efeito);
            this.gheFonte = gheFonte;
        }

        private void btn_limpar_Click(object sender, EventArgs e)
        {
            cb_exposicao.Items.Clear();
            iniciaComboeExposicao(cb_exposicao);
            cb_efeito.Items.Clear();
            iniciaComboeEfeito(cb_efeito);
            grd_soma.Columns.Clear();
            text_soma.Text = String.Empty;
            ComboHelper.tempoExposicao(cb_tempoExposicao);
        }

        private void btn_fechar_Click(object sender, EventArgs e)
        {
            this.Close();

        }

        private void btn_agente_Click(object sender, EventArgs e)
        {
            frm_ppraAgenteBuscar formPpraAgenteBuscar = new frm_ppraAgenteBuscar(gheFonte);
            formPpraAgenteBuscar.ShowDialog();

            if (formPpraAgenteBuscar.getAgente() != null)
            {
                agente = formPpraAgenteBuscar.getAgente();
                
                text_agente.Text = (agente.Descricao);
                cb_efeito.Items.Clear();
                iniciaComboeEfeito(cb_efeito);
                cb_exposicao.Items.Clear();
                iniciaComboeExposicao(cb_exposicao);
                grb_tempo.Enabled = true;
                ComboHelper.tempoExposicao(cb_tempoExposicao);
            }

        }

        public void iniciaComboeExposicao(ComboBox combo)
        {
            PpraFacade ppraFacade = PpraFacade.getInstance();

            SelectItem item = null;
            String valor;
            String descricao;

            combo.Items.Clear();

            foreach (DataRow rows in ppraFacade.findAllGradExposicao().Tables[0].Rows)
            {
                descricao = Convert.ToString(rows[Convert.ToString("id_grad_exposicao")]) + " - " + 
                    Convert.ToString(rows[Convert.ToString("categoria")]);
                valor = Convert.ToString(rows[Convert.ToString("id_grad_exposicao")]);

                item = new SelectItem(valor, descricao);

                combo.Items.Add(item);
            }

            // incluíndo opção no combo para permitir seleção de agente que não foi identificado.
            // 28/04/2013 - welbert santos
            combo.Items.Add(naoIdentificado);
            combo.ValueMember = "nome";
        }

        public void iniciaComboeEfeito(ComboBox combo)
        {
            PpraFacade ppraFacade = PpraFacade.getInstance();

            SelectItem item = null;
            String valor;
            String descricao;

            combo.Items.Clear();

            foreach (DataRow rows in ppraFacade.findAllGradEfeito().Tables[0].Rows)
            {
                descricao = Convert.ToString(rows["id_grad_efeito"]) + "- " + 
                    Convert.ToString(rows[Convert.ToString("categoria")]);
                valor = Convert.ToString(rows[Convert.ToString("id_grad_efeito")]);

                item = new SelectItem(valor, descricao);

                combo.Items.Add(item);
            }

            // incluíndo opção no combo para permitir seleção de agente que não foi identificado.
            // 28/04/2013 - welbert santos
            combo.Items.Add(naoIdentificado);
            combo.ValueMember = "nome";
        }

        private void cb_exposicao_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                if (agente == null)
                {
                    throw new Exception(msg1);
                }

                SelectItem exposicaoSelecionado = (SelectItem)cb_exposicao.SelectedItem;

                if (cb_efeito.SelectedItem != null)
                {
                    SelectItem efeitoSelecionado = (SelectItem)cb_efeito.SelectedItem;

                    if (exposicaoSelecionado != null)
                    {
                        if (String.Equals(exposicaoSelecionado.Valor, "6"))
                        {
                            cb_efeito.SelectedItem = naoIdentificado;
                            cb_efeito.SelectedText = naoIdentificado.Nome;
                            text_soma.Text = String.Empty;
                            grd_soma.Columns.Clear();

                        }

                        if (!String.Equals(exposicaoSelecionado.Valor, "6") && String.Equals(efeitoSelecionado.Valor, "6"))
                        {
                            this.cb_efeito.SelectedIndex = -1;
                        }

                        calculaGradacao();
                    }
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }

        }

        private void cb_efeito_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                if (agente == null)
                {
                    throw new Exception(msg1);
                }

                if (cb_exposicao.SelectedIndex == -1)
                {
                    throw new Exception(msg2);
                }

                SelectItem exposicaoSelecionado = (SelectItem)cb_exposicao.SelectedItem;
                SelectItem efeitoSelecionado = (SelectItem)cb_efeito.SelectedItem;

                if (efeitoSelecionado != null)
                {
                    if (String.Equals(efeitoSelecionado.Valor, "6"))
                    {
                        cb_exposicao.SelectedItem = naoIdentificado;
                        cb_exposicao.SelectedText = naoIdentificado.Nome;
                        text_soma.Text = String.Empty;
                        grd_soma.Columns.Clear();

                    }

                    if (!String.Equals(efeitoSelecionado.Valor, "6") && String.Equals(exposicaoSelecionado.Valor, "6"))
                    {
                        this.cb_exposicao.SelectedIndex = -1;
                    }

                    calculaGradacao();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
        
        }

        public void montaDataGrid()
        {
            try
            {
                grd_soma.Columns.Clear();
                grd_soma.ColumnCount = 4;
                
                grd_soma.Columns[0].HeaderText = "Id_grad_soma";
                grd_soma.Columns[1].HeaderText = "Faixa";
                grd_soma.Columns[2].HeaderText = "Medida de Controle";
                grd_soma.Columns[3].HeaderText = "Descrição";

                grd_soma.Columns[0].Visible = false;
                
                grd_soma.Rows.Add(gradSoma.getId(), gradSoma.getFaixa(), gradSoma.getMedControle(), gradSoma.getDescricao());

            }
            catch (Exception Ex)
            {
                MessageBox.Show(Ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
        }

        public void calculaGradacao()
        {
            try
            {
                PpraFacade ppraFacade = PpraFacade.getInstance();

                SelectItem itemExposicaoSelecionado = (SelectItem)cb_exposicao.SelectedItem;
                SelectItem itemEfeitoSelecionado = (SelectItem)cb_efeito.SelectedItem;

                if (itemExposicaoSelecionado != null && itemEfeitoSelecionado != null)
                {

                    if (String.Equals(itemExposicaoSelecionado.Valor, "6") && String.Equals(itemEfeitoSelecionado.Valor, "6"))
                    {
                        gradExposicao = null;
                        gradEfeito = null;
                    }
                    else
                    {
                        gradExposicao = new GradExposicao(Convert.ToInt64(itemExposicaoSelecionado.Valor), null, null, 0);
                        gradEfeito = new GradEfeito(Convert.ToInt64(itemEfeitoSelecionado.Valor), null, null, null);
                        
                    }

                    // metodo para buscar o valor das gradacoes no banco.

                    if (gradExposicao != null)
                    {
                        gradExposicao = ppraFacade.findGradExposicaoByGradExposicao(gradExposicao);
                    }

                    if (gradEfeito != null)
                    {
                        gradEfeito = ppraFacade.findGradEfeitoByGradEfeito(gradEfeito);
                    }

                    if (gradEfeito != null || gradExposicao != null)
                    {
                        gradSoma = new GradSoma();
                        gradSoma.setFaixa((Convert.ToInt64(gradExposicao.getValor()) + Convert.ToInt64(gradEfeito.getValor())));
                        gradSoma = ppraFacade.findGradSomaByGradSoma(gradSoma);
                        text_soma.Text = Convert.ToString(gradSoma.getFaixa());
                        montaDataGrid();
                        
                    }

                }
                                                
                
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void btn_incluir_Click(object sender, EventArgs e)
        {
            try
            {
                PpraFacade ppraFacade = PpraFacade.getInstance();

                String tempoExposicao = String.Empty;
                
                /* verificando se o usuário selecionou o agente */
                if (agente == null)
                {
                    throw new ExameException (msg1);
                }

                /* verificando para saber se o usuário selecionou a exposição */
                if (cb_exposicao.SelectedItem == null)
                {
                    throw new GradacaoException(GradacaoException.msg);
                }

                /* verificando para saber se o usuário selecionou o efeito */
                if (cb_efeito.SelectedItem == null)
                {
                    throw new GradacaoException(GradacaoException.msg2);
                }

                /* incluindo o tempo de exposição do agente */

                if (cb_tempoExposicao.SelectedIndex != -1)
                {
                    tempoExposicao = Convert.ToString(((SelectItem)cb_tempoExposicao.SelectedItem).Valor);
                }

                ppraFacade.incluiGheFonteAgente(new GheFonteAgente(null, gradSoma, gradEfeito, gradExposicao, gheFonte, agente,tempoExposicao, ApplicationConstants.ATIVO));

                this.Close();
                
            }
            catch (Exception Ex)
            {
                MessageBox.Show(Ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
        }

    }
}
