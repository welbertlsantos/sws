﻿using SWS.Entidade;
using SWS.Facade;
using SWS.View.Resources;
using SWS.View.ViewHelper;
using SWS.ViewHelper;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace SWS.View
{
    public partial class frmUsuarioPrincipal : frmTemplate
    {
        private Usuario usuario;
        
        public frmUsuarioPrincipal()
        {
            InitializeComponent();
            validaPermissoes();
            ComboHelper.comboSituacao(cbSitucao);
            ComboHelper.Boolean(cbExterno, false);
        }

        private void grd_usuarios_RowPrePaint(object sender, DataGridViewRowPrePaintEventArgs e)
        {
            DataGridView dgv = sender as DataGridView;

            if (String.Equals(dgv.Rows[e.RowIndex].Cells[6].Value, "I"))
                dgv.Rows[e.RowIndex].DefaultCellStyle.ForeColor = Color.Red;
        }

        private void validaPermissoes()
        {
            PermissionamentoFacade permissionamentoFacade = PermissionamentoFacade.getInstance();

            btnAlterar.Enabled = permissionamentoFacade.hasPermission("USUARIO", "ALTERAR");
            btnIncluir.Enabled = permissionamentoFacade.hasPermission("USUARIO", "INCLUIR");
            btnExcluir.Enabled = permissionamentoFacade.hasPermission("USUARIO", "EXCLUIR");
            btnDetalhar.Enabled = permissionamentoFacade.hasPermission("USUARIO", "DETALHAR");
            btnReativar.Enabled = permissionamentoFacade.hasPermission("USUARIO", "REATIVAR");
        }

        public void montaDataGrid()
        {
            try
            {
                this.Cursor = Cursors.WaitCursor;

                UsuarioFacade autenticacaoFacade = UsuarioFacade.getInstance();

                DataSet ds = autenticacaoFacade.findUsuarioByFilter(usuario);

                grd_usuarios.DataSource = ds.Tables["Usuarios"].DefaultView;

                grd_usuarios.Columns[0].HeaderText = "ID";
                grd_usuarios.Columns[0].Visible = false;

                grd_usuarios.Columns[1].HeaderText = "Valor";
                grd_usuarios.Columns[2].HeaderText = "CPF";
                grd_usuarios.Columns[3].HeaderText = "RG";
                grd_usuarios.Columns[4].HeaderText = "Data de Admissão";
                grd_usuarios.Columns[5].HeaderText = "Login";
                grd_usuarios.Columns[6].HeaderText = "Situação";
                grd_usuarios.Columns[6].Visible = false;
                grd_usuarios.Columns[7].HeaderText = "Empresa";
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }

        }

        private void btnIncluir_Click(object sender, EventArgs e)
        {
            try
            {
                frmUsuarioIncluir manterUsuario = new frmUsuarioIncluir();
                manterUsuario.ShowDialog();

                if (manterUsuario.Usuario != null)
                {
                    usuario = manterUsuario.Usuario;
                    montaDataGrid();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btnAlterar_Click(object sender, EventArgs e)
        {
            try
            {
                if (grd_usuarios.CurrentRow == null)
                    throw new Exception("Selecione um usuário.");

                UsuarioFacade autenticacaoFacade = UsuarioFacade.getInstance();

                usuario = autenticacaoFacade.findUsuarioById((Int64)grd_usuarios.CurrentRow.Cells[0].Value);

                if (!String.Equals(usuario.Situacao, ApplicationConstants.ATIVO))
                    throw new Exception("Somente usuários ativos podem ser alterados.");

                frmUsuarioAlterar usuarioAlterar = new frmUsuarioAlterar(usuario);
                usuarioAlterar.ShowDialog();

                usuario = usuarioAlterar.Usuario;

                montaDataGrid();

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btnExcluir_Click(object sender, EventArgs e)
        {
            try
            {
                if (grd_usuarios.CurrentRow == null)
                    throw new Exception ("Selecione um usuário.");
             
                UsuarioFacade autenticacaoFacade = UsuarioFacade.getInstance();
                
                usuario = autenticacaoFacade.findUsuarioById((Int64)grd_usuarios.CurrentRow.Cells[0].Value);

                if (!String.Equals(usuario.Situacao, ApplicationConstants.ATIVO))
                    throw new Exception ("Somente usuários ativos podem ser desativados.");
                    
                if (MessageBox.Show("Deseja desativar o usuário selecionado?", "Confirmação" , MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                {
                
                    usuario.Situacao = ApplicationConstants.DESATIVADO;

                    autenticacaoFacade.alterarUsuario(usuario, false);

                    MessageBox.Show("Usuário desativado com sucesso.","Confirmação", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    
                    montaDataGrid();
                    
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btnReativar_Click(object sender, EventArgs e)
        {
            try
            {
                if (grd_usuarios.CurrentRow == null)
                    throw new Exception("Selecione uma linha");
                
                UsuarioFacade usuarioFacade = UsuarioFacade.getInstance();
                PermissionamentoFacade permissionamentoFacade = PermissionamentoFacade.getInstance();
                
                usuario = usuarioFacade.findUsuarioById((Int64)grd_usuarios.CurrentRow.Cells[0].Value);

                if (!String.Equals(usuario.Situacao, ApplicationConstants.DESATIVADO))
                    throw new Exception("Somente usuários desativados podem ser reativados.");
                
                /* validando numero de licenças permitidas por empresa */

                int numeroUsuariosAtivos = usuarioFacade.listaUsuariosAtivos().Tables[0].Rows.Count;
                Dictionary<string, string> configuracoes = permissionamentoFacade.findAllConfiguracao();
                string configNumeroLicencas;

                configuracoes.TryGetValue("NUMERO_LICENCAS", out configNumeroLicencas);

                int numeroLicencas;
                int.TryParse(configNumeroLicencas, out numeroLicencas);

                if (numeroUsuariosAtivos >= numeroLicencas)
                    throw new Exception("Não é possível reativar o funcionário. Número de licenças excedido.");
                
                
                if (MessageBox.Show("Deseja reativar o usuário selecionado?", "Confirmação", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                {
                    usuario.Situacao = ApplicationConstants.ATIVO;

                    usuarioFacade.alterarUsuario(usuario, false);
                            
                    MessageBox.Show("Usuário reativado com sucesso.", "Confirmação", MessageBoxButtons.OK, MessageBoxIcon.Information);
                            
                    montaDataGrid();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btnDetalhar_Click(object sender, EventArgs e)
        {
            try
            {
                if (grd_usuarios.CurrentRow == null)
                    throw new Exception("Selecione um usuário.");

                UsuarioFacade autenticacaoFacade = UsuarioFacade.getInstance();

                usuario = autenticacaoFacade.findUsuarioById((Int64)grd_usuarios.CurrentRow.Cells[0].Value);

                frmUsuarioDetalhar usuarioDetalhar = new frmUsuarioDetalhar(usuario);
                usuarioDetalhar.ShowDialog();

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btnBuscar_Click(object sender, EventArgs e)
        {
            try
            {
                usuario = new Usuario(null, text_nome.Text.Trim(), text_cpf.Text, string.Empty, string.Empty, null, (((SelectItem)cbSitucao.SelectedItem).Valor).ToString(), text_login.Text.Trim(), string.Empty, string.Empty, null, null, string.Empty, false, null, string.Empty, Convert.ToBoolean(((SelectItem)cbExterno.SelectedItem).Valor), string.Empty, string.Empty);
                montaDataGrid();
                text_nome.Focus();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btnFechar_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnLimpar_Click(object sender, EventArgs e)
        {
            text_nome.Text = String.Empty;
            text_cpf.Text = String.Empty;
            text_login.Text = String.Empty;
            grd_usuarios.Columns.Clear();
            text_nome.Focus();
            ComboHelper.comboSituacao(cbSitucao);
        }

        private void grd_usuarios_CellMouseDoubleClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            try
            {
                btnAlterar.PerformClick();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }

        

    }
}
