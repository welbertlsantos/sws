﻿using SWS.Facade;
using SWS.Resources;
using SWS.View.ViewHelper;
using SWS.ViewHelper;
using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Net.Mime;
using System.Text;
using System.Windows.Forms;

namespace SWS.View
{
    public partial class frmEmailEnviar : frmTemplateConsulta
    {
        private MailMessage email;
        public MailMessage Email
        {
            get { return email; }
            set { email = value; }
        }
        
        private bool retorno;
        public bool Retorno
        {
            get { return retorno; }
            set { retorno = value; }
        }

        ArrayList anexosEmail = new ArrayList();
        
        public frmEmailEnviar()
        {
            InitializeComponent();
            ComboHelper.prioridadeEmail(cbPrioridade);
            ActiveControl = textEmail;
        }

        private void btnEnviar_Click(object sender, EventArgs e)
        {
            try
            {
                if (validaDadosTela())
                {
                    this.Cursor = Cursors.WaitCursor;

                    /* criando a mensagem e preparando para o envio */

                    email = new MailMessage();
                    /* para quem será enviado o e-mail */

                    String[] lEmail = textEmail.Text.Split(';');

                    foreach (String s in lEmail)
                        email.To.Add(new MailAddress(s));

                    /* assunto do e-mail */
                    email.Subject = textAssunto.Text.Trim();

                    /* incluíndo a mensagem no e-mail */
                    email.Body = textMensagem.Text.Trim();

                    /* verificando se houve anexos no e-mail */
                    if (anexosEmail.Count > 0)
                    {
                        foreach (string anexo in anexosEmail)
                        {
                            Attachment doc = new Attachment(anexo, MediaTypeNames.Application.Octet);
                            email.Attachments.Add(doc);

                        }
                    }

                    /* definindo a prioridade do e-mail */
                    email.Priority = String.Equals(((SelectItem)cbPrioridade.SelectedItem).Valor, "Normal") ? MailPriority.Normal :
                        String.Equals(((SelectItem)cbPrioridade.SelectedItem).Valor, "Low") ? MailPriority.Low : MailPriority.High;

                    /* recuperando informação de configuração para envio do email */

                    string host = String.Empty;
                    string emailFrom = String.Empty;
                    string port = String.Empty;
                    string ssl = String.Empty;

                    /* senha será alterada quando a tela de configurações estiver pronta */
                    string senha = String.Empty;

                    PermissionamentoFacade.mapaConfiguracoes.TryGetValue(ConfigurationConstans.SERVIDOR_SMTP, out host);
                    PermissionamentoFacade.mapaConfiguracoes.TryGetValue(ConfigurationConstans.EMAIL_ENVIO, out emailFrom);
                    PermissionamentoFacade.mapaConfiguracoes.TryGetValue(ConfigurationConstans.NUMERO_PORTA, out port);
                    PermissionamentoFacade.mapaConfiguracoes.TryGetValue(ConfigurationConstans.SSL, out ssl);
                    PermissionamentoFacade.mapaConfiguracoes.TryGetValue(ConfigurationConstans.SENHA_EMAIL, out senha);

                    /* informando na mensagem o e-mail que está enviando */
                    email.From = new MailAddress(emailFrom.ToLower());
                    

                    /* preparando a mensagem para envio */
                    SmtpClient smtp = new SmtpClient(host, Convert.ToInt32(port));
                    smtp.EnableSsl = Convert.ToBoolean(ssl);
                    smtp.UseDefaultCredentials = false;
                    smtp.Credentials = new NetworkCredential(emailFrom.ToLower(), senha);

                    /* enviando mensagem */
                    smtp.Send(email);

                    MessageBox.Show("Mensagem enviada com sucesso", "Confirmação", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    retorno = true;
                    this.Close();

                }

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.InnerException.ToString(), "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
                retorno = false;
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }

        private void btnAnexo_Click(object sender, EventArgs e)
        {
            try
            {
                if (ofdEmail.ShowDialog() == DialogResult.OK)
                {
                    string[] arrayAnexo = ofdEmail.FileNames;
                    /* inicializando lista de anexos */
                    anexosEmail = new ArrayList();
                    lblListaAnexo.Text = String.Empty;
                    anexosEmail.AddRange(arrayAnexo);

                    foreach (string s in anexosEmail)
                        lblListaAnexo.Text += s + "; ";

                }

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btnCancelar_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private bool validaDadosTela()
        {

            bool retorno = true;

            try
            {
                if (String.IsNullOrEmpty(textEmail.Text.Trim()))
                    throw new Exception("O E-mail não pode ficar em branco.");

                if (String.IsNullOrEmpty(textAssunto.Text.Trim()))
                    throw new Exception("O campo assunto não pode ficar em branco.");

                if (String.IsNullOrEmpty(textMensagem.Text.Trim()))
                    throw new Exception("Você não digitou nenhuma mensagem.");

                /* validando e-mail digitado segundo regras de validação expressões regulares */

                if (!String.IsNullOrEmpty(textEmail.Text.Trim()) &&
                    !ValidaCampoHelper.ValidaEnderecoEmail(textEmail.Text.Trim().ToLower()))
                    throw new Exception("E-mail digitado é inválido. Reveja a informação digitada no campo e-mail.");

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                retorno = false;
            }

            return retorno;
        }

        private void frmEmailEnviar_KeyDown(object sender, KeyEventArgs e)
        {
            if ((e.KeyCode == Keys.Enter) || (e.KeyCode == Keys.Return))
                SendKeys.Send("{TAB}");
        }
    }
}
