﻿using SWS.Entidade;
using SWS.Facade;
using SWS.View.Resources;
using SWS.ViewHelper;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace SWS.View
{
    public partial class frmContaAlterar : frmContaIncluir
    {
        public frmContaAlterar(Conta conta) :base()
        {
            InitializeComponent();
            this.Conta = conta;

            BancoSelecionado = Conta.Banco;
            textBanco.Text = BancoSelecionado.Nome;

            textNome.Text = conta.Nome;
            cbRegistro.SelectedValue = conta.Registro == true ? "true" : "false";
            textAgencia.Text = conta.AgenciaNumero;
            textDigitoAgencia.Text = conta.AgenciaDigito;
            textNumeroConta.Text = conta.ContaNumero;
            textDigitoConta.Text = conta.ContaDigito;
            textNossoNumero.Text = conta.ProximoNumero.ToString();
            textNumeroRemessa.Text = conta.NumeroRemessa;
            textCarteira.Text = conta.Carteira;
            textVariacaoCarteira.Text = conta.VariacaoCarteira;
            textNumeroConvenio.Text = conta.Convenio;
            textCodigoCedente.Text = conta.CodigoCedenteBanco;

            FinanceiroFacade financeiroFacade = FinanceiroFacade.getInstance();

            /* verificando quais atributos poderão ser alterados */

            if (!financeiroFacade.verificaPodeAlterarConta((long)conta.Id))
            {
                btnBanco.Enabled = false;
                cbRegistro.Enabled = false;
            }
        }


        protected override void btnGravar_Click(object sender, EventArgs e)
        {
            try
            {
                if (ValidaCamposObrigatórios())
                {
                    FinanceiroFacade financeiroFacade = FinanceiroFacade.getInstance();
                    Conta = financeiroFacade.updateConta(new Conta(Conta.Id, BancoSelecionado, textNome.Text, textAgencia.Text, textDigitoAgencia.Text, textNumeroConta.Text, textDigitoConta.Text, string.IsNullOrEmpty(textNossoNumero.Text) ? 0 : (long?)Convert.ToInt64(textNossoNumero.Text), textNumeroRemessa.Text, textCarteira.Text, textNumeroConvenio.Text, textVariacaoCarteira.Text, textCodigoCedente.Text, Convert.ToBoolean(((SelectItem)cbRegistro.SelectedItem).Valor), ApplicationConstants.ATIVO, false));

                    MessageBox.Show("Conta Corrente alterada com sucesso", "Confirmação", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    this.Close();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

    }
}
