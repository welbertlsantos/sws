﻿using SWS.Entidade;
using SWS.Facade;
using SWS.View.Resources;
using SWS.View.ViewHelper;
using SWS.ViewHelper;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace SWS.View
{
    public partial class frmDocumentoPrincipal : frmTemplate
    {
        Documento documento;
        
        public frmDocumentoPrincipal()
        {
            InitializeComponent();
            ComboHelper.comboSituacao(cbSituacao);
            ActiveControl = textDescricao;
            validaPermissao();
        }

        private void btnIncluir_Click(object sender, EventArgs e)
        {
            try
            {
                frmDocumentoIncluir incluirDocumento = new frmDocumentoIncluir();
                incluirDocumento.ShowDialog();

                if (incluirDocumento.Documento != null)
                {
                    documento = new Documento();
                    MontaGrid();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }

        private void btnAlterar_Click(object sender, EventArgs e)
        {
            try
            {
                if (dgvDocumento.CurrentRow == null)
                    throw new Exception("Selecione uma linha.");

                ProtocoloFacade protocoloFacade = ProtocoloFacade.getInstance();

                Documento documentoAlterar = protocoloFacade.findDocumentoById((long)dgvDocumento.CurrentRow.Cells[0].Value);

                if (!string.Equals(documentoAlterar.Situacao, ApplicationConstants.ATIVO))
                    throw new Exception("Somente documentos ativos podem ser alterados.");

                frmDocumentoAlterar alteraDocumento = new frmDocumentoAlterar(documentoAlterar);
                alteraDocumento.ShowDialog();

                documento = new Documento();
                MontaGrid();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }

        private void btnPesquisar_Click(object sender, EventArgs e)
        {
            try
            {
                documento = new Documento(null, textDescricao.Text, ((SelectItem)cbSituacao.SelectedItem).Valor);
                MontaGrid();
                textDescricao.Focus();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }

        private void btnDetalhar_Click(object sender, EventArgs e)
        {
            try
            {
                if (dgvDocumento.CurrentRow == null)
                    throw new Exception("Selecione uma linha.");

                ProtocoloFacade protocoloFacade = ProtocoloFacade.getInstance();

                frmDocumentoDetalhar detalharDocumento = new frmDocumentoDetalhar(protocoloFacade.findDocumentoById((long)dgvDocumento.CurrentRow.Cells[0].Value));
                detalharDocumento.ShowDialog();

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            
        }

        private void btnReativar_Click(object sender, EventArgs e)
        {
            try
            {
                if (dgvDocumento.CurrentRow == null)
                    throw new Exception("Selecione uma linha.");

                ProtocoloFacade protocoloFacade = ProtocoloFacade.getInstance();

                Documento documentoReativar = protocoloFacade.findDocumentoById((long)dgvDocumento.CurrentRow.Cells[0].Value);

                if (!string.Equals(documentoReativar.Situacao, ApplicationConstants.DESATIVADO))
                    throw new Exception("Somente documentos inativos podem ser reativados.");

                if (MessageBox.Show("Deseja reativar o documento selecionado?", "Atenção", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                {
                    documentoReativar.Situacao = ApplicationConstants.ATIVO;
                    protocoloFacade.updateDocumento(documentoReativar);
                    MessageBox.Show("Documento reativado com sucesso.", "Confirmação", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    documento = new Documento();
                    MontaGrid();
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }

        private void btnExcluir_Click(object sender, EventArgs e)
        {
            try
            {
                if (dgvDocumento.CurrentRow == null)
                    throw new Exception("Selecione uma linha.");

                ProtocoloFacade protocoloFacade = ProtocoloFacade.getInstance();

                Documento documentoExcluir = protocoloFacade.findDocumentoById((long)dgvDocumento.CurrentRow.Cells[0].Value);

                if (!string.Equals(documentoExcluir.Situacao, ApplicationConstants.ATIVO))
                    throw new Exception("Somente documentos ativos podem ser excluídos.");

                if (MessageBox.Show("Deseja excluir o documento selecionado?", "Atenção", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                {
                    documentoExcluir.Situacao = ApplicationConstants.DESATIVADO ;
                    protocoloFacade.updateDocumento(documentoExcluir);
                    MessageBox.Show("Documento excluído com sucesso.", "Confirmação", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    documento = new Documento();
                    MontaGrid();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }

        private void btnFechar_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void dgvDocumento_RowPrePaint(object sender, DataGridViewRowPrePaintEventArgs e)
        {
            DataGridView dgv = sender as DataGridView;

            if (string.Equals(dgv.Rows[e.RowIndex].Cells[2].Value, ApplicationConstants.DESATIVADO))
                dgv.Rows[e.RowIndex].DefaultCellStyle.ForeColor = Color.Red;
        }
        
        public void MontaGrid()
        {
            try
            {
                this.Cursor = Cursors.WaitCursor;

                ProtocoloFacade protocoloFacade = ProtocoloFacade.getInstance();

                DataSet ds = protocoloFacade.findDocumentoByFilter(documento);

                dgvDocumento.DataSource = ds.Tables["Documentos"].DefaultView;

                dgvDocumento.Columns[0].HeaderText = "id";
                dgvDocumento.Columns[0].Visible = false;

                dgvDocumento.Columns[1].HeaderText = "Descrição";

                dgvDocumento.Columns[2].HeaderText = "Situação";
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }

        public void validaPermissao()
        {
            PermissionamentoFacade permissionamento = PermissionamentoFacade.getInstance();

            btnIncluir.Enabled = permissionamento.hasPermission("DOCUMENTO", "INCLUIR");
            btnAlterar.Enabled = permissionamento.hasPermission("DOCUMENTO", "ALTERAR");
            btnExcluir.Enabled = permissionamento.hasPermission("DOCUMENTO", "EXCLUIR");
            btnReativar.Enabled = permissionamento.hasPermission("DOCUMENTO", "REATIVAR");

        }

        private void dgvDocumento_CellMouseDoubleClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            try
            {
                btnAlterar.PerformClick();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }
    }
}
