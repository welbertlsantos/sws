﻿using SWS.Entidade;
using SWS.Facade;
using SWS.View.Resources;
using SWS.View.ViewHelper;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace SWS.View
{
    public partial class frmCnaeSelecionar : frmTemplateConsulta
    {

        private Cnae cnae;

        public Cnae Cnae
        {
            get { return cnae; }
            set { cnae = value; }
        }

        public frmCnaeSelecionar()
        {
            InitializeComponent();
            validaPermissoes();
            ActiveControl = textCodCnae;
        }

        private void validaPermissoes()
        {
            PermissionamentoFacade permissionamentoFacade = PermissionamentoFacade.getInstance();

            this.btn_novo.Enabled = permissionamentoFacade.hasPermission("CNAE", "INCLUIR");
        }

        private void btn_ok_Click(object sender, EventArgs e)
        {
            try
            {

                if (dgvCnae.CurrentRow == null)
                    throw new Exception("Selecione uma linha.");

                cnae = new Cnae(Convert.ToInt64(dgvCnae.CurrentRow.Cells[0].Value), dgvCnae.CurrentRow.Cells[1].Value.ToString(), dgvCnae.CurrentRow.Cells[2].Value.ToString(), dgvCnae.CurrentRow.Cells[3].Value.ToString(), dgvCnae.CurrentRow.Cells[4].Value.ToString(), dgvCnae.CurrentRow.Cells[5].Value.ToString());
                
                this.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btn_novo_Click(object sender, EventArgs e)
        {
            frmCnaeIncluir formCnaeIncluir = new frmCnaeIncluir();
            formCnaeIncluir.ShowDialog();

            if (formCnaeIncluir.Cnae != null)
                cnae = formCnaeIncluir.Cnae;

            this.Close();
        }

        private void btn_buscar_Click(object sender, EventArgs e)
        {
            try
            {
                montaDataGrid();
                textCodCnae.Focus();
            }
            catch (Exception Ex)
            {
                MessageBox.Show(Ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btn_fechar_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        public void montaDataGrid()
        {
            try
            {
                this.Cursor = Cursors.WaitCursor;

                ClienteFacade clienteFacade = ClienteFacade.getInstance();

                DataSet ds = clienteFacade.findCnaeByFilter(new Cnae(null, ValidaCampoHelper.RetornaCnaSemFormato(textCodCnae.Text).Trim(), textAtividade.Text.Trim(), String.Empty, String.Empty, ApplicationConstants.ATIVO));

                dgvCnae.Columns.Clear();

                dgvCnae.DataSource = ds.Tables["Cnaes"].DefaultView;

                dgvCnae.Columns[0].HeaderText = "Id_Cnae";
                dgvCnae.Columns[0].Visible = false;

                dgvCnae.Columns[1].HeaderText = "Código Cnae";
                dgvCnae.Columns[1].ReadOnly = true;

                dgvCnae.Columns[2].HeaderText = "Atividade";
                dgvCnae.Columns[2].ReadOnly = true;

                dgvCnae.Columns[3].HeaderText = "Grau de Risco";
                dgvCnae.Columns[3].Visible = false;
                
                dgvCnae.Columns[4].HeaderText = "Grupo";
                dgvCnae.Columns[4].Visible = false;
                
                dgvCnae.Columns[5].HeaderText = "Situação";
                dgvCnae.Columns[5].Visible = false;


            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error );
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }

        private void dgvCnae_CellMouseDoubleClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            try
            {
                btn_ok.PerformClick();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }
    }
}
