﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using SWS.Entidade;
using System.Diagnostics;

namespace SWS.View
{
    public partial class frm_RelAsoDuplicado : BaseFormConsulta
    {

        private Cliente cliente;
        
        public frm_RelAsoDuplicado()
        {
            InitializeComponent();
        }

        public void setCliente(Cliente cliente)
        {
            this.cliente = cliente;
        }

        public void setTextoCliente(String texto)
        {
            this.text_cliente.Text = texto;
        }
        
        private void frm_RelAsoDuplicado_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                this.SelectNextControl(this.ActiveControl, !e.Shift, true, true, true);
            }
        }

        private void btn_fechar_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btn_cliente_Click(object sender, EventArgs e)
        {
            frm_RelAsoDuplicadoCliente formRelatorioAsoDuplicadoCliente = new frm_RelAsoDuplicadoCliente(this);
            formRelatorioAsoDuplicadoCliente.ShowDialog();
        }

        private void btn_limpar_Click(object sender, EventArgs e)
        {
            this.cliente = null;
            this.text_cliente.Text = String.Empty;

        }

        private void btn_imprimir_Click(object sender, EventArgs e)
        {
            try
            {
                // validando dados em tela.

                if (validaDados())
                {
                    // criando parämetros da pesquisa.

                    this.Cursor = Cursors.WaitCursor;

                    DateTime dataInicial = Convert.ToDateTime(dt_inicial.Text);
                    DateTime dataFinal = Convert.ToDateTime(dt_final.Text);
                    String nomeRelatorio = cliente != null ? "ASO_DUPLICADO_CLIENTE.JASPER" : "ASO_DUPLICADO.JASPER";
                    Int64? idCliente = cliente != null ? cliente.Id : 0;

                    var process = new Process();

                    process.StartInfo.FileName = "Relatorio\\jre7\\bin\\java";
                    process.StartInfo.Arguments = " -jar Relatorio\\MiddlewareJasper.jar " + "Relatorio\\Report\\" + nomeRelatorio + " DT_INICIAL:" + dataInicial.ToShortDateString() + ":String" + " DT_FINAL:" + dataFinal.ToShortDateString() + ":String " + "ID_CLIENTE:" + idCliente + ":Integer";

                    process.StartInfo.UseShellExecute = false;
                    process.StartInfo.RedirectStandardError = true;
                    process.StartInfo.RedirectStandardInput = true;
                    process.StartInfo.RedirectStandardOutput = true;
                    process.StartInfo.CreateNoWindow = true;

                    process.Start();
                    process.StandardOutput.ReadToEnd();
                    process.WaitForExit();

                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }

        public Boolean validaDados()
        {
            Boolean retorno = true;
            
            try
            {
                if (Convert.ToDateTime(dt_final.Text) < Convert.ToDateTime(dt_inicial.Text))
                {
                    retorno = false;
                    throw new Exception("Data final menor que a data inicial");
                }
                
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }

            return retorno;

            

        }


    }
}
