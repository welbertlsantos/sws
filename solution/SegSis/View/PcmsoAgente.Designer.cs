﻿namespace SWS.View
{
    partial class frmPcmsoAgente
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            this.flpAcao = new System.Windows.Forms.FlowLayoutPanel();
            this.btOk = new System.Windows.Forms.Button();
            this.btBuscar = new System.Windows.Forms.Button();
            this.btNovo = new System.Windows.Forms.Button();
            this.btFechar = new System.Windows.Forms.Button();
            this.textAgente = new System.Windows.Forms.TextBox();
            this.lblAgente = new System.Windows.Forms.TextBox();
            this.grbAgente = new System.Windows.Forms.GroupBox();
            this.dgvAgente = new System.Windows.Forms.DataGridView();
            ((System.ComponentModel.ISupportInitialize)(this.scForm)).BeginInit();
            this.scForm.Panel1.SuspendLayout();
            this.scForm.Panel2.SuspendLayout();
            this.scForm.SuspendLayout();
            this.pnlForm.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.banner)).BeginInit();
            this.flpAcao.SuspendLayout();
            this.grbAgente.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvAgente)).BeginInit();
            this.SuspendLayout();
            // 
            // scForm
            // 
            // 
            // scForm.Panel1
            // 
            this.scForm.Panel1.Controls.Add(this.flpAcao);
            // 
            // pnlForm
            // 
            this.pnlForm.Controls.Add(this.grbAgente);
            this.pnlForm.Controls.Add(this.lblAgente);
            this.pnlForm.Controls.Add(this.textAgente);
            // 
            // flpAcao
            // 
            this.flpAcao.Controls.Add(this.btOk);
            this.flpAcao.Controls.Add(this.btBuscar);
            this.flpAcao.Controls.Add(this.btNovo);
            this.flpAcao.Controls.Add(this.btFechar);
            this.flpAcao.Dock = System.Windows.Forms.DockStyle.Fill;
            this.flpAcao.Location = new System.Drawing.Point(0, 0);
            this.flpAcao.Name = "flpAcao";
            this.flpAcao.Size = new System.Drawing.Size(534, 30);
            this.flpAcao.TabIndex = 0;
            // 
            // btOk
            // 
            this.btOk.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btOk.Image = global::SWS.Properties.Resources.Gravar;
            this.btOk.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btOk.Location = new System.Drawing.Point(3, 3);
            this.btOk.Name = "btOk";
            this.btOk.Size = new System.Drawing.Size(77, 23);
            this.btOk.TabIndex = 8;
            this.btOk.Text = "&Ok";
            this.btOk.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btOk.UseVisualStyleBackColor = true;
            this.btOk.Click += new System.EventHandler(this.btn_ok_Click);
            // 
            // btBuscar
            // 
            this.btBuscar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btBuscar.Image = global::SWS.Properties.Resources.busca;
            this.btBuscar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btBuscar.Location = new System.Drawing.Point(86, 3);
            this.btBuscar.Name = "btBuscar";
            this.btBuscar.Size = new System.Drawing.Size(75, 23);
            this.btBuscar.TabIndex = 7;
            this.btBuscar.Text = "&Buscar";
            this.btBuscar.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btBuscar.UseVisualStyleBackColor = true;
            this.btBuscar.Click += new System.EventHandler(this.btn_buscar_Click);
            // 
            // btNovo
            // 
            this.btNovo.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btNovo.Image = global::SWS.Properties.Resources.icone_mais;
            this.btNovo.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btNovo.Location = new System.Drawing.Point(167, 3);
            this.btNovo.Name = "btNovo";
            this.btNovo.Size = new System.Drawing.Size(75, 23);
            this.btNovo.TabIndex = 9;
            this.btNovo.Text = "&Novo";
            this.btNovo.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btNovo.UseVisualStyleBackColor = true;
            this.btNovo.Click += new System.EventHandler(this.btn_novo_Click);
            // 
            // btFechar
            // 
            this.btFechar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btFechar.Image = global::SWS.Properties.Resources.close;
            this.btFechar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btFechar.Location = new System.Drawing.Point(248, 3);
            this.btFechar.Name = "btFechar";
            this.btFechar.Size = new System.Drawing.Size(75, 23);
            this.btFechar.TabIndex = 10;
            this.btFechar.Text = "&Fechar";
            this.btFechar.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btFechar.UseVisualStyleBackColor = true;
            this.btFechar.Click += new System.EventHandler(this.btn_fechar_Click);
            // 
            // textAgente
            // 
            this.textAgente.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textAgente.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.textAgente.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textAgente.Location = new System.Drawing.Point(139, 13);
            this.textAgente.MaxLength = 100;
            this.textAgente.Name = "textAgente";
            this.textAgente.Size = new System.Drawing.Size(364, 21);
            this.textAgente.TabIndex = 2;
            // 
            // lblAgente
            // 
            this.lblAgente.BackColor = System.Drawing.Color.Silver;
            this.lblAgente.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblAgente.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblAgente.Location = new System.Drawing.Point(3, 13);
            this.lblAgente.MaxLength = 100;
            this.lblAgente.Name = "lblAgente";
            this.lblAgente.ReadOnly = true;
            this.lblAgente.Size = new System.Drawing.Size(137, 21);
            this.lblAgente.TabIndex = 3;
            this.lblAgente.Text = "Agente";
            // 
            // grbAgente
            // 
            this.grbAgente.BackColor = System.Drawing.Color.Transparent;
            this.grbAgente.Controls.Add(this.dgvAgente);
            this.grbAgente.Location = new System.Drawing.Point(2, 40);
            this.grbAgente.Name = "grbAgente";
            this.grbAgente.Size = new System.Drawing.Size(501, 234);
            this.grbAgente.TabIndex = 52;
            this.grbAgente.TabStop = false;
            this.grbAgente.Text = "Agentes";
            // 
            // dgvAgente
            // 
            this.dgvAgente.AllowUserToAddRows = false;
            this.dgvAgente.AllowUserToDeleteRows = false;
            this.dgvAgente.AllowUserToOrderColumns = true;
            this.dgvAgente.AllowUserToResizeRows = false;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.Gainsboro;
            this.dgvAgente.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle1;
            this.dgvAgente.BackgroundColor = System.Drawing.Color.White;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.Color.LightGray;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvAgente.DefaultCellStyle = dataGridViewCellStyle2;
            this.dgvAgente.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvAgente.Location = new System.Drawing.Point(3, 16);
            this.dgvAgente.MultiSelect = false;
            this.dgvAgente.Name = "dgvAgente";
            this.dgvAgente.RowHeadersVisible = false;
            this.dgvAgente.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvAgente.Size = new System.Drawing.Size(495, 215);
            this.dgvAgente.TabIndex = 3;
            this.dgvAgente.CellMouseDoubleClick += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.dgvAgente_CellMouseDoubleClick);
            // 
            // frmPcmsoAgente
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.ClientSize = new System.Drawing.Size(534, 361);
            this.Name = "frmPcmsoAgente";
            this.Text = "PESQUISAR AGENTE";
            this.scForm.Panel1.ResumeLayout(false);
            this.scForm.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.scForm)).EndInit();
            this.scForm.ResumeLayout(false);
            this.pnlForm.ResumeLayout(false);
            this.pnlForm.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.banner)).EndInit();
            this.flpAcao.ResumeLayout(false);
            this.grbAgente.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvAgente)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        protected System.Windows.Forms.FlowLayoutPanel flpAcao;
        protected System.Windows.Forms.Button btFechar;
        protected System.Windows.Forms.Button btBuscar;
        protected System.Windows.Forms.Button btNovo;
        protected System.Windows.Forms.Button btOk;
        protected System.Windows.Forms.TextBox lblAgente;
        protected System.Windows.Forms.TextBox textAgente;
        protected System.Windows.Forms.GroupBox grbAgente;
        protected System.Windows.Forms.DataGridView dgvAgente;


    }
}
