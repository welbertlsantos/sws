﻿using SWS.Entidade;
using SWS.View.ViewHelper;
using SWS.ViewHelper;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SWS.View
{
    public partial class frmPcmsoObraIncluir : frmTemplateConsulta
    {
        protected Obra obra;

        public Obra Obra
        {
            get { return obra; }
            set { obra = value; }
        }

        protected CidadeIbge cidadeObra;
        
        public frmPcmsoObraIncluir()
        {
            InitializeComponent();
            ComboHelper.unidadeFederativa(cbUnidadeFederativa);
            ActiveControl = textObra;

        }

        protected void btnCidadeIncluir_Click(object sender, EventArgs e)
        {
            try
            {
                if (this.cbUnidadeFederativa.SelectedIndex == 0)
                    throw new Exception("Você deve selecionar primeiro a unidade federativa.");

                frmCidadePesquisa cidadeSeleciona = new frmCidadePesquisa(((SelectItem)cbUnidadeFederativa.SelectedItem).Valor.ToString());
                cidadeSeleciona.ShowDialog();

                if (cidadeSeleciona.Cidade != null)
                {
                    this.cidadeObra = cidadeSeleciona.Cidade;
                    this.textCidadeObra.Text = this.cidadeObra.Nome;
                }
                
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }

        protected void btnCidadeExcluir_Click(object sender, EventArgs e)
        {
            try
            {
                if (this.Obra.CidadeObra == null)
                    throw new Exception("Você deve selecionar primeiro a cidade");

                this.cidadeObra = null;
                this.textCidadeObra.Text = string.Empty;
                     
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            
        }

        protected virtual void btnObraGravar_Click(object sender, EventArgs e)
        {
            try
            {
                if (validaCampos())
                {
                    this.Obra = new Obra();
                    Obra.LocalObra = textObra.Text.Trim();
                    Obra.EnderecoObra = textEnderecoObra.Text.Trim();
                    Obra.NumeroObra = textNumeroObra.Text.Trim();
                    Obra.ComplementoObra = textComplementoObra.Text.Trim();
                    Obra.BairroObra = textBairroObra.Text.Trim();
                    Obra.UnidadeFederativaObra = ((SelectItem)cbUnidadeFederativa.SelectedItem).Valor;
                    Obra.CidadeObra = cidadeObra;
                    Obra.CepObra = this.textCEPObra.Text;
                    Obra.DescritivoObra = textDescritivoObra.Text.Trim();
                    this.Close();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }

        protected bool validaCampos()
        {
            bool retorno = true;
            
            try
            {
                /* validando campos obrigatórios */

                if (string.IsNullOrWhiteSpace(textObra.Text))
                    throw new Exception("Campo local da obra obrigatório.");

                if (string.IsNullOrWhiteSpace(textEnderecoObra.Text))
                    throw new Exception("Campo endereço da obra é obrigatório.");

                if (string.IsNullOrWhiteSpace(textNumeroObra.Text))
                    throw new Exception("Campo número da obra é obrigatório.");

                if (string.IsNullOrWhiteSpace(textBairroObra.Text))
                    throw new Exception("Campo bairro da obra é obrigatório.");

                if (cbUnidadeFederativa.SelectedIndex == -1)
                    throw new Exception("Seleção da unidade federativa da obra é obrigatorio.");

                if (this.cidadeObra == null)
                    throw new Exception("A cidade onde a obra está localizada é obrigatória.");

                if (string.IsNullOrWhiteSpace(textCEPObra.Text.Replace(".","").Replace("-","")))
                    throw new Exception("O CEP da obra é obrigatório.");

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
                retorno = false;
            }

            return retorno;

        }

        protected void frmPcmsoObraIncluir_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter || e.KeyCode == Keys.Return)
                SendKeys.Send("{TAB}");
        }

        protected void btnFechar_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }

    
}
