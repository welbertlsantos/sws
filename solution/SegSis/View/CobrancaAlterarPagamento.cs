﻿using SWS.Entidade;
using SWS.Facade;
using SWS.View.Resources;
using SWS.ViewHelper;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace SWS.View
{
    public partial class frmCobrancaAlterarPagamento : frmTemplateConsulta
    {
        private Cobranca cobranca;
        private Banco banco;

        public frmCobrancaAlterarPagamento(Cobranca cobranca)
        {
            InitializeComponent();
            this.cobranca = cobranca;
            carregaComboForma(cbForma);
            ActiveControl = cbForma;

            /*preenchendo informações da cobranca na tela */
            textNumeroNota.Text = cobranca.Nota.NumeroNfe.ToString();
            textDocumento.Text = cobranca.NumeroDocumento;
            textParcela.Text = cobranca.NumeroParcela.ToString();
            textFormaPagamento.Text = cobranca.Forma.Descricao;
            textCliente.Text = cobranca.Nota.Cliente.RazaoSocial;
        }

        private void btnConfirma_Click(object sender, EventArgs e)
        {
            try
            {
                if (cbForma.SelectedIndex == -1)
                    throw new Exception("Você deve selecionar a forma de pagamento para alterar.");
                
                if (string.Equals(((SelectItem)cbForma.SelectedItem).Nome, ApplicationConstants.BOLETO) && banco == null )
                    throw new Exception("Você selecionou a forma boleto. É necessário informar o banco para emissão do mesmo.");

                FinanceiroFacade financeiroFacade = FinanceiroFacade.getInstance();

                if (string.Equals(((SelectItem)cbForma.SelectedItem).Nome, ApplicationConstants.BOLETO))
                {
                    if (dgvConta.CurrentRow == null)
                        throw new Exception("Selecione uma conta de cobrança.");
                    
                    cobranca.Conta = financeiroFacade.findContaById((long)dgvConta.CurrentRow.Cells[0].Value);
                }

                cobranca.Forma = new Forma(Convert.ToInt64(((SelectItem)cbForma.SelectedItem).Valor), (((SelectItem)cbForma.SelectedItem).Nome));
                cobranca.DataAlteracao = DateTime.Now;
                financeiroFacade.alterarFormaPagamentoCobranca(cobranca);
                MessageBox.Show("Forma de pagamento alterada com sucesso. Não esqueça de gerar o novo boleto.", "Confirmação", MessageBoxButtons.OK, MessageBoxIcon.Information);
                this.Close();

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }

        private void btnFechar_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void carregaComboForma(ComboBox combo)
        {
            try
            {
                FinanceiroFacade financeiroFacade = FinanceiroFacade.getInstance();

                combo.DisplayMember = "nome";
                combo.ValueMember = "valor";

                financeiroFacade.findAllFormas().ForEach(delegate(Forma forma)
                {
                    combo.Items.Add(new SelectItem(Convert.ToString(forma.Id), Convert.ToString(forma.Descricao)));
                });

                combo.DropDownStyle = ComboBoxStyle.DropDownList;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void frmCobrancaAlterarPagamento_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter || e.KeyCode == Keys.Return)
                SendKeys.Send("{TAB}");
        }

        private void cbForma_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                btnBanco.Enabled = string.Equals(((SelectItem)cbForma.SelectedItem).Nome, ApplicationConstants.BOLETO) ? true : false;

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }

        private void btnBanco_Click(object sender, EventArgs e)
        {
            try
            {
                frmBancoSelecionar selecionarBanco = new frmBancoSelecionar(false, true);
                selecionarBanco.ShowDialog();

                if (selecionarBanco.Banco != null)
                {
                    banco = selecionarBanco.Banco;
                    textBanco.Text = banco.Nome;
                    montaDataGrid();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }

        public void montaDataGrid()
        {
            try
            {
                this.Cursor = Cursors.WaitCursor;

                dgvConta.Columns.Clear();

                FinanceiroFacade financeiroFacade = FinanceiroFacade.getInstance();

                DataSet ds = financeiroFacade.findContaByFilter(new Conta(null, banco, string.Empty, string.Empty, string.Empty, string.Empty, string.Empty, null, string.Empty, string.Empty, string.Empty, string.Empty, string.Empty, false, ApplicationConstants.ATIVO, false));

                dgvConta.DataSource = ds.Tables[0].DefaultView;

                dgvConta.Columns[0].HeaderText = "Id Conta";
                dgvConta.Columns[0].Visible = false;

                dgvConta.Columns[1].HeaderText = "Nome";
                dgvConta.Columns[2].HeaderText = "Agência";
                dgvConta.Columns[3].HeaderText = "Agência Digito";
                dgvConta.Columns[4].HeaderText = "Conta Corrente";
                dgvConta.Columns[5].HeaderText = "Dígito";
                dgvConta.Columns[6].HeaderText = "Proximo número";
                dgvConta.Columns[6].Visible = false;

                dgvConta.Columns[7].HeaderText = "Numero Remessa";
                dgvConta.Columns[7].Visible = false;

                dgvConta.Columns[8].HeaderText = "Carteira";
                dgvConta.Columns[8].Visible = false;

                dgvConta.Columns[9].HeaderText = "Convenio";
                dgvConta.Columns[9].Visible = false;

                dgvConta.Columns[10].HeaderText = "Variação";
                dgvConta.Columns[10].Visible = false;

                dgvConta.Columns[11].HeaderText = "Codigo Cedente";
                dgvConta.Columns[11].Visible = false;

                dgvConta.Columns[12].HeaderText = "Registro";
                dgvConta.Columns[12].Visible = false;

                dgvConta.Columns[13].HeaderText = "Situacao";
                dgvConta.Columns[13].Visible = false;

                dgvConta.Columns[14].HeaderText = "Id Banco";
                dgvConta.Columns[14].Visible = false;

                dgvConta.Columns[15].HeaderText = "Nome do Banco";
                dgvConta.Columns[15].Visible = false;

                dgvConta.Columns[16].HeaderText = "Codigo Banco";
                dgvConta.Columns[16].Visible = false;

                dgvConta.Columns[17].HeaderText = "Caixa";
                dgvConta.Columns[17].Visible = false;

                dgvConta.Columns[18].HeaderText = "Situacao";
                dgvConta.Columns[18].Visible = false;

                dgvConta.Columns[19].HeaderText = "Conta Privada";
                dgvConta.Columns[19].Visible = false;

                dgvConta.Columns[20].HeaderText = "Banco Privado";
                dgvConta.Columns[20].Visible = false;

                dgvConta.Columns[21].HeaderText = "Banco Boleto";
                dgvConta.Columns[21].Visible = false;

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }


    }
}
