﻿using SWS.Entidade;
using SWS.Facade;
using SWS.View.Resources;
using SWS.ViewHelper;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace SWS.View
{
    public partial class frmProtocoloIncluirItens : frmTemplate
    {
        private Protocolo protocolo;

        public Protocolo Protocolo
        {
            get { return protocolo; }
            set { protocolo = value; }
        }
        private Cliente cliente;
        private List<GheFonteAgenteExameAso> gheFonteAgenteExameAtendimento = new List<GheFonteAgenteExameAso>();
        private List<ClienteFuncaoExameASo> clienteFuncaoExameAtendimento = new List<ClienteFuncaoExameASo>();
        private DocumentoProtocolo documentos;
        private Aso atendimento;
        private Movimento movimentoProduto;
 
        public frmProtocoloIncluirItens(Protocolo protocolo, Cliente cliente)
        {
            InitializeComponent();
            this.protocolo = protocolo;
            this.cliente = cliente;
            this.btnDocumento.Enabled = false;
        }

        private void btnGravar_Click(object sender, EventArgs e)
        {
            try
            {
                /* verificando se alguma coisa foi marcada para a gravação */
                bool verifica = false;

                if (atendimento != null)
                {
                    foreach (DataGridViewRow dvRow in dgvItensProtocolo.Rows)
                    {
                        if ((bool)dvRow.Cells[7].Value == true)
                        {
                            verifica = true;
                            break;
                        }
                    }
                }
                else
                {
                    foreach (DataGridViewRow dvRow in dgvItensProtocolo.Rows)
                    {
                        if ((bool)dvRow.Cells[6].Value == true)
                        {
                            verifica = true;
                            break;
                        }
                    }
                }

                if (!verifica)
                    throw new Exception("Você não selecionou nenhum item.");


                /* verificando o que será incluído no protocolo */
                if (atendimento != null)
                {
                    /* foi incluído itens do atendimento */
                    clienteFuncaoExameAtendimento.Clear();
                    gheFonteAgenteExameAtendimento.Clear();
                    foreach (DataGridViewRow dvRow in this.dgvItensProtocolo.Rows)
                    {
                        if ((bool)dvRow.Cells[7].Value == true)
                        {
                            /* excluindo o documento inserido */
                            if (!string.Equals(dvRow.Cells[6].Value, ApplicationConstants.DOCUMENTO_PROTOCOLO))
                            {
                                if (string.Equals(dvRow.Cells[6].Value, ApplicationConstants.EXAME_PCMSO))
                                    gheFonteAgenteExameAtendimento.Add(new GheFonteAgenteExameAso((long)dvRow.Cells[0].Value, null, atendimento, null, null, true, true, string.Empty, false, null, null, null, null, false, null, string.Empty, string.Empty, false, false, null, protocolo, false, false, null, null, false, true));
                                else if (string.Equals(dvRow.Cells[6].Value, ApplicationConstants.EXAME_EXTRA)) 
                                    clienteFuncaoExameAtendimento.Add(new ClienteFuncaoExameASo((long)dvRow.Cells[0].Value, null, atendimento, null, null, true, true, string.Empty, true, null, null, null, null, false, null, string.Empty, string.Empty, false, null, protocolo, null, null, true ));
                                else protocolo.Atendimento = atendimento;

                            }
                        
                        }
                    }

                    protocolo.GheFonteAgenteExameAsos = gheFonteAgenteExameAtendimento;
                    protocolo.ClienteFuncaoExameAsos = clienteFuncaoExameAtendimento;
                }

                if (documentos != null)  protocolo.Documentos.AddFirst(documentos);

                if (movimentoProduto != null) protocolo.Movimentos.AddFirst(movimentoProduto);
                

                ProtocoloFacade protocoloFacade = ProtocoloFacade.getInstance();
                protocoloFacade.insertProtocolo(protocolo);
                this.Close();

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }

        private void btnFechar_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnAtendimento_Click(object sender, EventArgs e)
        {
            try
            {
                AsoFacade asoFacade = AsoFacade.getInstance();

                frmAtendimentoBuscar buscarAtendimento = new frmAtendimentoBuscar(cliente, new SelectItem(ApplicationConstants.FECHADO, "Finalizado"));
                buscarAtendimento.ShowDialog();

                if (buscarAtendimento.Atendimento != null)
                {
                    /* verificando para saber se o atendimento escolhido já foi incluído em um protocolo */
                    if (asoFacade.findAsoInProtocolo(buscarAtendimento.Atendimento))
                        throw new Exception("O atendimento selecionado já foi incluído em um protocolo.");

                    /* verificando se o atendimento tem exames para ser incluídos no protocolo */
                    /* remontando a coleçaõ de exames no aso para buscar somente os finalizados */
                    gheFonteAgenteExameAtendimento = asoFacade.findAllExamePcmsoByAsoInSituacao(buscarAtendimento.Atendimento, true, true, false, false, false);
                    clienteFuncaoExameAtendimento = asoFacade.findAllExameExtraByAsoInSituacao(buscarAtendimento.Atendimento, true, true, false, false, false);

                    /* excluindo exames transcritos e exames que já tenham sido incluído no protocolo */
                    bool flagExamePcmso, flagExameExtra;

                    flagExamePcmso = gheFonteAgenteExameAtendimento.Exists(x => x.Protocolo == null && x.Transcrito == false);
                    flagExameExtra = clienteFuncaoExameAtendimento.Exists(x => x.Protocolo == null && x.Transcrito == false);

                    if (!flagExamePcmso && !flagExameExtra)
                        throw new Exception("O atendimento não tem nenhum item para ser incluído no protocolo");

                    /* usuário selecionou o atendimento */
                    atendimento = buscarAtendimento.Atendimento;

                    /* incluindo o atendimento da lista de informações da tela */
                    listBoxInformacao.Items.Clear();
                    listBoxInformacao.Items.Add("COLABORADOR(A):" + atendimento.ClienteFuncaoFuncionario.Funcionario.Nome);
                    listBoxInformacao.Items.Add("CPF: " + atendimento.ClienteFuncaoFuncionario.Funcionario.Cpf);
                    listBoxInformacao.Items.Add("RG: " + atendimento.ClienteFuncaoFuncionario.Funcionario.Rg);
                    listBoxInformacao.Items.Add("TIPO DE ATENDIMENTO: " + atendimento.Periodicidade.Descricao);
                    listBoxInformacao.Items.Add("DATA DO ATENDIMENTO: " + atendimento.DataElaboracao);

                    montaDataGridAtendimento();

                    if (verificaExamePermiteDocumento())
                    {
                        MessageBox.Show("O atendimento selecionado tem exames que emitem documentação. Ela será incluída na gride para seleção.", "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        
                        this.dgvItensProtocolo.Rows.Add(
                            atendimento.Id,
                            atendimento.Codigo,        
                            ApplicationConstants.ATENDIMENTO,
                            String.Format("{0:dd/MM/yyyy} ", atendimento.DataElaboracao),
                            String.IsNullOrEmpty(atendimento.Conclusao) ? String.Empty : String.Equals(ApplicationConstants.LAUDOAPTO, atendimento.Conclusao) ? "APTO" : "INAPTO",
                            String.Empty,
                            ApplicationConstants.ATENDIMENTO,
                            true);

                    }
                    
                    /* desabilitando nova inserção de itens caso algum item tenha sido incluído na grid. */
                    if (dgvItensProtocolo.Rows.Count > 0)
                    {
                        btnAtendimento.Enabled = false;
                        btnDocumento.Enabled = true;
                        btnProduto.Enabled = false;
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }

        public bool verificaExamePermiteDocumento()
        {
            bool retorno = false;
            try
            {

                /* testando exames do pcmso */
                foreach (GheFonteAgenteExameAso gfaea in gheFonteAgenteExameAtendimento)
                {
                    if (gfaea.GheFonteAgenteExame.Exame.LiberaDocumento)
                    {
                        retorno = true;
                        break;
                    }
                }

                /* testando exames extras */
                foreach (ClienteFuncaoExameASo cfea in clienteFuncaoExameAtendimento)
                {
                    if (cfea.ClienteFuncaoExame.Exame.LiberaDocumento)
                    {
                        retorno = true;
                        break;
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            return retorno;
        }

        private void btnDocumento_Click(object sender, EventArgs e)
        {
            try
            {
                if (atendimento == null)
                    throw new Exception("Você deve selecionar o atendimento para incluir um documento.");
                
                frmDocumentoBuscar buscarDocumentos = new frmDocumentoBuscar();
                buscarDocumentos.ShowDialog();

                if (buscarDocumentos.Documento != null)
                {
                    /* incluindo informação do documento na lista */
                    this.listBoxInformacao.Items.Clear();
                    this.listBoxInformacao.Items.Add("DOCUMENTO: " + buscarDocumentos.Documento.Descricao);

                    documentos = new DocumentoProtocolo(null, atendimento, buscarDocumentos.Documento, protocolo, DateTime.Now);
                    montaGridItensDocumento();

                }
                
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }

        private void montaDataGridAtendimento()
        {
            try
            {
                this.dgvItensProtocolo.Columns.Clear();
                this.dgvItensProtocolo.ColumnCount = 7;

                this.dgvItensProtocolo.Columns[0].HeaderText = "id";
                this.dgvItensProtocolo.Columns[0].Visible = false;

                this.dgvItensProtocolo.Columns[1].HeaderText = "Código do Atendimento";

                this.dgvItensProtocolo.Columns[2].HeaderText = "Item";

                this.dgvItensProtocolo.Columns[3].HeaderText = "Data";
                this.dgvItensProtocolo.Columns[3].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
                this.dgvItensProtocolo.Columns[3].HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleCenter;

                this.dgvItensProtocolo.Columns[4].HeaderText = "Resultado";

                this.dgvItensProtocolo.Columns[5].HeaderText = "Sala";

                this.dgvItensProtocolo.Columns[6].HeaderText = "Tipo";
                this.dgvItensProtocolo.Columns[6].Visible = false;

                DataGridViewCheckBoxColumn check = new DataGridViewCheckBoxColumn();
                check.Name = "Selecionar";
                this.dgvItensProtocolo.Columns.Add(check);
                this.dgvItensProtocolo.Columns[7].DisplayIndex = 0;

                foreach (GheFonteAgenteExameAso gfaea in gheFonteAgenteExameAtendimento)
                {
                    if (gfaea.Protocolo == null)
                    {
                        if (!gfaea.Transcrito)
                            this.dgvItensProtocolo.Rows.Add(gfaea.Id, atendimento.Codigo, gfaea.GheFonteAgenteExame.Exame.Descricao, String.Format("{0:dd/MM/yyyy} ", gfaea.DataExame), String.IsNullOrEmpty(gfaea.Resultado) ? String.Empty : "JÁ LAUDADO", gfaea.SalaExame != null ? gfaea.SalaExame.Sala.Descricao : String.Empty, ApplicationConstants.EXAME_PCMSO, true);
                    }
                }

                foreach (ClienteFuncaoExameASo cfea in clienteFuncaoExameAtendimento)
                {
                    if (cfea.Protocolo == null)
                    {
                        if (!cfea.Transcrito)
                            this.dgvItensProtocolo.Rows.Add(cfea.Id, atendimento.Codigo, cfea.ClienteFuncaoExame.Exame.Descricao, String.Format("{0:dd/MM/yyyy} ", cfea.DataExame), String.IsNullOrEmpty(cfea.Resultado) ? String.Empty : "JÁ LAUDADO", cfea.SalaExame != null ? cfea.SalaExame.Sala.Descricao : String.Empty, ApplicationConstants.EXAME_EXTRA, true);
                    }
                }

                /* desabilitando controles */
                btnAtendimento.Enabled = false;
                btnDocumento.Enabled = true;
                btnProduto.Enabled = false;

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }


        private void montaGridItensDocumento()
        {
            try
            {
                if (documentos != null)
                {
                    dgvItensProtocolo.Rows.Add(documentos.Id,
                        documentos.Aso.Codigo,
                        documentos.Documento.Descricao,
                        String.Format("{0:dd/MM/yyyy} ", documentos.DataGravacao),
                        string.Empty,
                        string.Empty,
                        ApplicationConstants.DOCUMENTO_PROTOCOLO,
                        true);
                }

                /* desabilitando controles */
                btnAtendimento.Enabled = false;
                btnDocumento.Enabled = false;
                btnProduto.Enabled = false;

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }

        private void btnProduto_Click(object sender, EventArgs e)
        {
            try
            {
                frmProdutoMovimentoBuscar buscarProdutoInMovimento = new frmProdutoMovimentoBuscar(cliente);
                buscarProdutoInMovimento.ShowDialog();

                if (buscarProdutoInMovimento.Movimento != null)
                {
                    movimentoProduto = buscarProdutoInMovimento.Movimento;

                    /* incluindo informação do produto na lista */
                    this.listBoxInformacao.Items.Clear();
                    this.listBoxInformacao.Items.Add("PRODUTO: " + buscarProdutoInMovimento.Movimento.ContratoProduto.Produto.Nome);
                    
                    /* desabilitando controles */
                    btnAtendimento.Enabled = false;
                    btnDocumento.Enabled = false;
                    btnProduto.Enabled = false;

                    montaGridProduto();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }

        private void montaGridProduto()
        {
            try
            {
                this.dgvItensProtocolo.Columns.Clear();
                this.dgvItensProtocolo.ColumnCount = 6;

                this.dgvItensProtocolo.Columns[0].HeaderText = "id";
                this.dgvItensProtocolo.Columns[0].Visible = false;

                this.dgvItensProtocolo.Columns[1].HeaderText = "Produto";
                this.dgvItensProtocolo.Columns[1].ReadOnly = true;

                this.dgvItensProtocolo.Columns[2].HeaderText = "Data";
                this.dgvItensProtocolo.Columns[2].ReadOnly = true;
                this.dgvItensProtocolo.Columns[2].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
                this.dgvItensProtocolo.Columns[2].HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleCenter;

                this.dgvItensProtocolo.Columns[3].HeaderText = "Cliente";
                this.dgvItensProtocolo.Columns[3].ReadOnly = true;

                this.dgvItensProtocolo.Columns[4].HeaderText = "Contratante / Unidade";
                this.dgvItensProtocolo.Columns[4].ReadOnly = true;

                this.dgvItensProtocolo.Columns[5].HeaderText = "Tipo";
                this.dgvItensProtocolo.Columns[5].Visible = false;

                DataGridViewCheckBoxColumn check = new DataGridViewCheckBoxColumn();
                check.Name = "Selec";
                this.dgvItensProtocolo.Columns.Add(check);
                this.dgvItensProtocolo.Columns[6].DisplayIndex = 0;

                /* preenchendo a grid com as informações dos PRODUTOS */

                    dgvItensProtocolo.Rows.Add(movimentoProduto.Id,
                            movimentoProduto.ContratoProduto.Produto.Nome,
                            String.Format("{0:dd/MM/yyyy} ", movimentoProduto.DataInclusao),
                            movimentoProduto.Cliente.RazaoSocial,
                            movimentoProduto.Unidade != null ? movimentoProduto.Unidade.RazaoSocial : String.Empty,
                            ApplicationConstants.PRODUTO,
                            true);

                
                
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void marcarDesmarcarTodos_CheckedChanged(object sender, EventArgs e)
        {
            foreach (DataGridViewRow dvRow in dgvItensProtocolo.Rows)
            {
                if (atendimento != null || movimentoProduto != null)
                    dvRow.Cells[6].Value = this.marcarDesmarcarTodos.Checked ? true : false;
                if (documentos != null)
                    dvRow.Cells[8].Value = this.marcarDesmarcarTodos.Checked ? true : false;

            }

        }

    }
}
