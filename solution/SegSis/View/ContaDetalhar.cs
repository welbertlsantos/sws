﻿using SWS.Entidade;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace SWS.View
{
    public partial class frmContaDetalhar : frmContaIncluir
    {
        public frmContaDetalhar(Conta conta) :base()
        {
            InitializeComponent();

            btnBanco.Enabled = false;
            textBanco.Text = conta.Banco.Nome;
            textNome.Text = conta.Nome;
            textNome.ReadOnly = true;
            cbRegistro.SelectedValue = conta.Registro == true ? "true" : "false";
            cbRegistro.Enabled = false;
            textAgencia.Text = conta.AgenciaNumero;
            textAgencia.ReadOnly = true;
            textDigitoAgencia.Text = conta.AgenciaDigito;
            textDigitoAgencia.ReadOnly = true;
            textNumeroConta.Text = conta.ContaNumero;
            textNumeroConta.ReadOnly = true;
            textDigitoConta.Text = conta.ContaDigito;
            textDigitoConta.ReadOnly = true;
            textNossoNumero.Text = conta.ProximoNumero.ToString();
            textNossoNumero.ReadOnly = true;
            textNumeroRemessa.Text = conta.NumeroRemessa;
            textNumeroRemessa.ReadOnly =  true;
            textCarteira.Text = conta.Carteira;
            textCarteira.ReadOnly = true;
            textVariacaoCarteira.Text = conta.VariacaoCarteira;
            textVariacaoCarteira.ReadOnly = true;
            textNumeroConvenio.Text = conta.Convenio;
            textNumeroConvenio.ReadOnly = true;
            textCodigoCedente.Text = conta.CodigoCedenteBanco;
            textCodigoCedente.ReadOnly = true;
            btnGravar.Enabled = false;
            btnLimpar.Enabled = false;
        }
    }
}
