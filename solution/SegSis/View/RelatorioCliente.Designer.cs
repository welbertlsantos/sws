﻿namespace SWS.View
{
    partial class frmRelatorioCliente
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.flowLayoutPanel1 = new System.Windows.Forms.FlowLayoutPanel();
            this.btnImprimir = new System.Windows.Forms.Button();
            this.btnFechar = new System.Windows.Forms.Button();
            this.btnExcluirCliente = new System.Windows.Forms.Button();
            this.btCliente = new System.Windows.Forms.Button();
            this.textCliente = new System.Windows.Forms.TextBox();
            this.lblCliente = new System.Windows.Forms.TextBox();
            this.lblDataCadastro = new System.Windows.Forms.TextBox();
            this.dataInicial = new System.Windows.Forms.DateTimePicker();
            this.dataFinal = new System.Windows.Forms.DateTimePicker();
            this.cbTipoCliente = new SWS.ComboBoxWithBorder();
            this.lblTipoCliente = new System.Windows.Forms.TextBox();
            this.cbSituacao = new SWS.ComboBoxWithBorder();
            this.lblSituacao = new System.Windows.Forms.TextBox();
            this.cbContrato = new SWS.ComboBoxWithBorder();
            this.lblUtilizaContrato = new System.Windows.Forms.TextBox();
            this.cbTipoRelatorio = new SWS.ComboBoxWithBorder();
            this.lblTipoRelatorio = new System.Windows.Forms.TextBox();
            ((System.ComponentModel.ISupportInitialize)(this.scForm)).BeginInit();
            this.scForm.Panel1.SuspendLayout();
            this.scForm.Panel2.SuspendLayout();
            this.scForm.SuspendLayout();
            this.pnlForm.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.banner)).BeginInit();
            this.flowLayoutPanel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // scForm
            // 
            // 
            // scForm.Panel1
            // 
            this.scForm.Panel1.Controls.Add(this.flowLayoutPanel1);
            // 
            // pnlForm
            // 
            this.pnlForm.Controls.Add(this.cbTipoRelatorio);
            this.pnlForm.Controls.Add(this.lblTipoRelatorio);
            this.pnlForm.Controls.Add(this.cbContrato);
            this.pnlForm.Controls.Add(this.lblUtilizaContrato);
            this.pnlForm.Controls.Add(this.cbSituacao);
            this.pnlForm.Controls.Add(this.lblSituacao);
            this.pnlForm.Controls.Add(this.cbTipoCliente);
            this.pnlForm.Controls.Add(this.lblTipoCliente);
            this.pnlForm.Controls.Add(this.lblDataCadastro);
            this.pnlForm.Controls.Add(this.dataInicial);
            this.pnlForm.Controls.Add(this.dataFinal);
            this.pnlForm.Controls.Add(this.btnExcluirCliente);
            this.pnlForm.Controls.Add(this.lblCliente);
            this.pnlForm.Controls.Add(this.btCliente);
            this.pnlForm.Controls.Add(this.textCliente);
            // 
            // flowLayoutPanel1
            // 
            this.flowLayoutPanel1.Controls.Add(this.btnImprimir);
            this.flowLayoutPanel1.Controls.Add(this.btnFechar);
            this.flowLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.flowLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.flowLayoutPanel1.Name = "flowLayoutPanel1";
            this.flowLayoutPanel1.Size = new System.Drawing.Size(534, 30);
            this.flowLayoutPanel1.TabIndex = 0;
            // 
            // btnImprimir
            // 
            this.btnImprimir.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnImprimir.Image = global::SWS.Properties.Resources.impressora2;
            this.btnImprimir.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnImprimir.Location = new System.Drawing.Point(3, 3);
            this.btnImprimir.Name = "btnImprimir";
            this.btnImprimir.Size = new System.Drawing.Size(75, 23);
            this.btnImprimir.TabIndex = 0;
            this.btnImprimir.Text = "Imprimir";
            this.btnImprimir.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnImprimir.UseVisualStyleBackColor = true;
            this.btnImprimir.Click += new System.EventHandler(this.btnImprimir_Click);
            // 
            // btnFechar
            // 
            this.btnFechar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnFechar.Image = global::SWS.Properties.Resources.close;
            this.btnFechar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnFechar.Location = new System.Drawing.Point(84, 3);
            this.btnFechar.Name = "btnFechar";
            this.btnFechar.Size = new System.Drawing.Size(75, 23);
            this.btnFechar.TabIndex = 2;
            this.btnFechar.Text = "Fechar";
            this.btnFechar.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnFechar.UseVisualStyleBackColor = true;
            this.btnFechar.Click += new System.EventHandler(this.btnFechar_Click);
            // 
            // btnExcluirCliente
            // 
            this.btnExcluirCliente.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnExcluirCliente.Image = global::SWS.Properties.Resources.close;
            this.btnExcluirCliente.Location = new System.Drawing.Point(479, 3);
            this.btnExcluirCliente.Name = "btnExcluirCliente";
            this.btnExcluirCliente.Size = new System.Drawing.Size(30, 21);
            this.btnExcluirCliente.TabIndex = 40;
            this.btnExcluirCliente.UseVisualStyleBackColor = true;
            this.btnExcluirCliente.Click += new System.EventHandler(this.btnExcluirCliente_Click);
            // 
            // btCliente
            // 
            this.btCliente.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btCliente.Image = global::SWS.Properties.Resources.busca;
            this.btCliente.Location = new System.Drawing.Point(450, 3);
            this.btCliente.Name = "btCliente";
            this.btCliente.Size = new System.Drawing.Size(30, 21);
            this.btCliente.TabIndex = 38;
            this.btCliente.UseVisualStyleBackColor = true;
            this.btCliente.Click += new System.EventHandler(this.btCliente_Click);
            // 
            // textCliente
            // 
            this.textCliente.BackColor = System.Drawing.SystemColors.Control;
            this.textCliente.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textCliente.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textCliente.Location = new System.Drawing.Point(140, 3);
            this.textCliente.MaxLength = 100;
            this.textCliente.Name = "textCliente";
            this.textCliente.ReadOnly = true;
            this.textCliente.Size = new System.Drawing.Size(311, 21);
            this.textCliente.TabIndex = 39;
            this.textCliente.TabStop = false;
            // 
            // lblCliente
            // 
            this.lblCliente.BackColor = System.Drawing.Color.LightGray;
            this.lblCliente.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblCliente.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCliente.Location = new System.Drawing.Point(3, 3);
            this.lblCliente.Name = "lblCliente";
            this.lblCliente.ReadOnly = true;
            this.lblCliente.Size = new System.Drawing.Size(138, 21);
            this.lblCliente.TabIndex = 37;
            this.lblCliente.TabStop = false;
            this.lblCliente.Text = "Cliente";
            // 
            // lblDataCadastro
            // 
            this.lblDataCadastro.BackColor = System.Drawing.Color.LightGray;
            this.lblDataCadastro.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblDataCadastro.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDataCadastro.Location = new System.Drawing.Point(3, 103);
            this.lblDataCadastro.Name = "lblDataCadastro";
            this.lblDataCadastro.ReadOnly = true;
            this.lblDataCadastro.Size = new System.Drawing.Size(138, 21);
            this.lblDataCadastro.TabIndex = 41;
            this.lblDataCadastro.TabStop = false;
            this.lblDataCadastro.Text = "Data de Cadastro";
            // 
            // dataInicial
            // 
            this.dataInicial.Checked = false;
            this.dataInicial.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dataInicial.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dataInicial.Location = new System.Drawing.Point(140, 103);
            this.dataInicial.Name = "dataInicial";
            this.dataInicial.ShowCheckBox = true;
            this.dataInicial.Size = new System.Drawing.Size(107, 21);
            this.dataInicial.TabIndex = 42;
            // 
            // dataFinal
            // 
            this.dataFinal.Checked = false;
            this.dataFinal.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dataFinal.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dataFinal.Location = new System.Drawing.Point(246, 103);
            this.dataFinal.Name = "dataFinal";
            this.dataFinal.ShowCheckBox = true;
            this.dataFinal.Size = new System.Drawing.Size(107, 21);
            this.dataFinal.TabIndex = 43;
            // 
            // cbTipoCliente
            // 
            this.cbTipoCliente.BackColor = System.Drawing.Color.LightGray;
            this.cbTipoCliente.BorderColor = System.Drawing.Color.DimGray;
            this.cbTipoCliente.BorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid;
            this.cbTipoCliente.FormattingEnabled = true;
            this.cbTipoCliente.Location = new System.Drawing.Point(140, 23);
            this.cbTipoCliente.Name = "cbTipoCliente";
            this.cbTipoCliente.Size = new System.Drawing.Size(369, 21);
            this.cbTipoCliente.TabIndex = 45;
            // 
            // lblTipoCliente
            // 
            this.lblTipoCliente.BackColor = System.Drawing.Color.LightGray;
            this.lblTipoCliente.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblTipoCliente.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTipoCliente.Location = new System.Drawing.Point(3, 23);
            this.lblTipoCliente.Name = "lblTipoCliente";
            this.lblTipoCliente.ReadOnly = true;
            this.lblTipoCliente.Size = new System.Drawing.Size(138, 21);
            this.lblTipoCliente.TabIndex = 44;
            this.lblTipoCliente.TabStop = false;
            this.lblTipoCliente.Text = "Tipo de Cliente";
            // 
            // cbSituacao
            // 
            this.cbSituacao.BackColor = System.Drawing.Color.LightGray;
            this.cbSituacao.BorderColor = System.Drawing.Color.DimGray;
            this.cbSituacao.BorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid;
            this.cbSituacao.FormattingEnabled = true;
            this.cbSituacao.Location = new System.Drawing.Point(140, 43);
            this.cbSituacao.Name = "cbSituacao";
            this.cbSituacao.Size = new System.Drawing.Size(369, 21);
            this.cbSituacao.TabIndex = 47;
            // 
            // lblSituacao
            // 
            this.lblSituacao.BackColor = System.Drawing.Color.LightGray;
            this.lblSituacao.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblSituacao.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSituacao.Location = new System.Drawing.Point(3, 43);
            this.lblSituacao.Name = "lblSituacao";
            this.lblSituacao.ReadOnly = true;
            this.lblSituacao.Size = new System.Drawing.Size(138, 21);
            this.lblSituacao.TabIndex = 46;
            this.lblSituacao.TabStop = false;
            this.lblSituacao.Text = "Situação";
            // 
            // cbContrato
            // 
            this.cbContrato.BackColor = System.Drawing.Color.LightGray;
            this.cbContrato.BorderColor = System.Drawing.Color.DimGray;
            this.cbContrato.BorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid;
            this.cbContrato.FormattingEnabled = true;
            this.cbContrato.Location = new System.Drawing.Point(140, 63);
            this.cbContrato.Name = "cbContrato";
            this.cbContrato.Size = new System.Drawing.Size(369, 21);
            this.cbContrato.TabIndex = 49;
            // 
            // lblUtilizaContrato
            // 
            this.lblUtilizaContrato.BackColor = System.Drawing.Color.LightGray;
            this.lblUtilizaContrato.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblUtilizaContrato.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblUtilizaContrato.Location = new System.Drawing.Point(3, 63);
            this.lblUtilizaContrato.Name = "lblUtilizaContrato";
            this.lblUtilizaContrato.ReadOnly = true;
            this.lblUtilizaContrato.Size = new System.Drawing.Size(138, 21);
            this.lblUtilizaContrato.TabIndex = 48;
            this.lblUtilizaContrato.TabStop = false;
            this.lblUtilizaContrato.Text = "Utiliza Contrato";
            // 
            // cbTipoRelatorio
            // 
            this.cbTipoRelatorio.BackColor = System.Drawing.Color.LightGray;
            this.cbTipoRelatorio.BorderColor = System.Drawing.Color.DimGray;
            this.cbTipoRelatorio.BorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid;
            this.cbTipoRelatorio.FormattingEnabled = true;
            this.cbTipoRelatorio.Location = new System.Drawing.Point(140, 83);
            this.cbTipoRelatorio.Name = "cbTipoRelatorio";
            this.cbTipoRelatorio.Size = new System.Drawing.Size(369, 21);
            this.cbTipoRelatorio.TabIndex = 51;
            // 
            // lblTipoRelatorio
            // 
            this.lblTipoRelatorio.BackColor = System.Drawing.Color.LightGray;
            this.lblTipoRelatorio.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblTipoRelatorio.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTipoRelatorio.Location = new System.Drawing.Point(3, 83);
            this.lblTipoRelatorio.Name = "lblTipoRelatorio";
            this.lblTipoRelatorio.ReadOnly = true;
            this.lblTipoRelatorio.Size = new System.Drawing.Size(138, 21);
            this.lblTipoRelatorio.TabIndex = 50;
            this.lblTipoRelatorio.TabStop = false;
            this.lblTipoRelatorio.Text = "Tipo de Relatório";
            // 
            // frmRelatorioCliente
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(534, 361);
            this.Name = "frmRelatorioCliente";
            this.Text = "RELATÓRIO DE CLIENTES";
            this.scForm.Panel1.ResumeLayout(false);
            this.scForm.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.scForm)).EndInit();
            this.scForm.ResumeLayout(false);
            this.pnlForm.ResumeLayout(false);
            this.pnlForm.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.banner)).EndInit();
            this.flowLayoutPanel1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel1;
        private System.Windows.Forms.Button btnImprimir;
        private System.Windows.Forms.Button btnFechar;
        private System.Windows.Forms.Button btnExcluirCliente;
        private System.Windows.Forms.TextBox lblCliente;
        private System.Windows.Forms.Button btCliente;
        public System.Windows.Forms.TextBox textCliente;
        private System.Windows.Forms.TextBox lblDataCadastro;
        private System.Windows.Forms.DateTimePicker dataInicial;
        private System.Windows.Forms.DateTimePicker dataFinal;
        private ComboBoxWithBorder cbTipoCliente;
        private System.Windows.Forms.TextBox lblTipoCliente;
        private ComboBoxWithBorder cbSituacao;
        private System.Windows.Forms.TextBox lblSituacao;
        private ComboBoxWithBorder cbContrato;
        private System.Windows.Forms.TextBox lblUtilizaContrato;
        private ComboBoxWithBorder cbTipoRelatorio;
        private System.Windows.Forms.TextBox lblTipoRelatorio;
    }
}