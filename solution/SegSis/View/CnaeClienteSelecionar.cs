﻿using SWS.Entidade;
using SWS.Facade;
using SWS.View.ViewHelper;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace SWS.View
{
    public partial class frmCnaeClienteSelecionar : frmTemplateConsulta
    {
        private ClienteCnae clienteCnae;

        public ClienteCnae ClienteCnae
        {
            get { return clienteCnae; }
            set { clienteCnae = value; }
        }

        private Cliente cliente;
        
        public frmCnaeClienteSelecionar(Cliente cliente)
        {
            InitializeComponent();
            this.cliente = cliente;
            this.montaGrid();
        }

        private void montaGrid()
        {
            try
            {
                this.Cursor = Cursors.WaitCursor;
                this.dgvCnae.Columns.Clear();
                ClienteFacade clienteFacade = ClienteFacade.getInstance();

                List<ClienteCnae> cnaeCliente = clienteFacade.findClienteCnaeByCliente(cliente);

                dgvCnae.ColumnCount = 3;
                dgvCnae.Columns[0].HeaderText = "id";
                dgvCnae.Columns[0].Visible = false;

                dgvCnae.Columns[1].HeaderText = "Código CNAE";

                dgvCnae.Columns[2].HeaderText = "Atividade";

                if (cnaeCliente.Count > 0)
                {
                    cnaeCliente.ForEach(delegate(ClienteCnae clienteCnae) {
                        dgvCnae.Rows.Add(
                            clienteCnae.Cnae.Id,
                            ValidaCampoHelper.RetornaCnaeFormatado(clienteCnae.Cnae.CodCnae),
                            clienteCnae.Cnae.Atividade);
                    });
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }

        }

        private void btnFechar_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnSelecionar_Click(object sender, EventArgs e)
        {
            try
            {
                if (dgvCnae.CurrentRow == null)
                    throw new Exception("Selecione uma linha");

                ClienteFacade clienteFacade = ClienteFacade.getInstance();
                this.ClienteCnae = new ClienteCnae(cliente, clienteFacade.findCnaeById((long)dgvCnae.CurrentRow.Cells[0].Value), true);
                this.Close();
               
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }
    }
}
