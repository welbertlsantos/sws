﻿using SWS.Entidade;
using SWS.Facade;
using SWS.View.Resources;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace SWS.View
{
    public partial class frmAtividadeIncluir : frmTemplate
    {
        private Atividade atividade;

        public Atividade Atividade
        {
            get { return atividade; }
            set { atividade = value; }
        }
        
        public frmAtividadeIncluir()
        {
            InitializeComponent();
            ActiveControl = textAtividade;
        }

        protected virtual void btnGravar_Click(object sender, EventArgs e)
        {
            try
            {
                if (ValidaDadosTela())
                {
                    EstudoFacade pcmsoFacade = EstudoFacade.getInstance();

                    atividade = pcmsoFacade.insertAtividade(new Atividade(null, textAtividade.Text, ApplicationConstants.ATIVO));
                    MessageBox.Show("Atividade incluída com sucesso.");
                    this.Close();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        protected void btnFechar_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        protected bool ValidaDadosTela()
        {
            bool result = true;
            try
            {
                if (string.IsNullOrEmpty(textAtividade.Text.Trim()))
                    throw new Exception("Campo nome da atividade obrigatório.");
            }
            catch (Exception ex)
            {
                result = false;
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            return result;
        }

        protected void frmAtividadeIncluir_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter || e.KeyCode == Keys.Return)
                SendKeys.Send("{TAB}");

        }
    }
}
