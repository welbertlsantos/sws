﻿namespace SWS.View
{
    partial class frm_RelAsoDuplicado
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frm_RelAsoDuplicado));
            this.grb_dados = new System.Windows.Forms.GroupBox();
            this.grb_cliente = new System.Windows.Forms.GroupBox();
            this.grb_periodo = new System.Windows.Forms.GroupBox();
            this.dt_final = new System.Windows.Forms.DateTimePicker();
            this.lbl_periodo = new System.Windows.Forms.Label();
            this.dt_inicial = new System.Windows.Forms.DateTimePicker();
            this.grb_paginacao = new System.Windows.Forms.GroupBox();
            this.btn_fechar = new System.Windows.Forms.Button();
            this.btn_imprimir = new System.Windows.Forms.Button();
            this.text_cliente = new System.Windows.Forms.TextBox();
            this.btn_cliente = new System.Windows.Forms.Button();
            this.btn_limpar = new System.Windows.Forms.Button();
            this.grb_dados.SuspendLayout();
            this.grb_cliente.SuspendLayout();
            this.grb_periodo.SuspendLayout();
            this.grb_paginacao.SuspendLayout();
            this.SuspendLayout();
            // 
            // grb_dados
            // 
            this.grb_dados.Controls.Add(this.grb_cliente);
            this.grb_dados.Controls.Add(this.grb_periodo);
            this.grb_dados.Location = new System.Drawing.Point(3, 3);
            this.grb_dados.Name = "grb_dados";
            this.grb_dados.Size = new System.Drawing.Size(593, 338);
            this.grb_dados.TabIndex = 0;
            this.grb_dados.TabStop = false;
            this.grb_dados.Text = "Dados";
            // 
            // grb_cliente
            // 
            this.grb_cliente.Controls.Add(this.btn_limpar);
            this.grb_cliente.Controls.Add(this.btn_cliente);
            this.grb_cliente.Controls.Add(this.text_cliente);
            this.grb_cliente.Location = new System.Drawing.Point(9, 82);
            this.grb_cliente.Name = "grb_cliente";
            this.grb_cliente.Size = new System.Drawing.Size(578, 81);
            this.grb_cliente.TabIndex = 1;
            this.grb_cliente.TabStop = false;
            this.grb_cliente.Text = "Cliente";
            // 
            // grb_periodo
            // 
            this.grb_periodo.Controls.Add(this.dt_final);
            this.grb_periodo.Controls.Add(this.lbl_periodo);
            this.grb_periodo.Controls.Add(this.dt_inicial);
            this.grb_periodo.Location = new System.Drawing.Point(6, 19);
            this.grb_periodo.Name = "grb_periodo";
            this.grb_periodo.Size = new System.Drawing.Size(243, 56);
            this.grb_periodo.TabIndex = 0;
            this.grb_periodo.TabStop = false;
            this.grb_periodo.Text = "Período";
            // 
            // dt_final
            // 
            this.dt_final.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dt_final.Location = new System.Drawing.Point(132, 20);
            this.dt_final.Name = "dt_final";
            this.dt_final.Size = new System.Drawing.Size(100, 20);
            this.dt_final.TabIndex = 2;
            // 
            // lbl_periodo
            // 
            this.lbl_periodo.AutoSize = true;
            this.lbl_periodo.Location = new System.Drawing.Point(112, 26);
            this.lbl_periodo.Name = "lbl_periodo";
            this.lbl_periodo.Size = new System.Drawing.Size(13, 13);
            this.lbl_periodo.TabIndex = 1;
            this.lbl_periodo.Text = "a";
            // 
            // dt_inicial
            // 
            this.dt_inicial.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dt_inicial.Location = new System.Drawing.Point(7, 20);
            this.dt_inicial.Name = "dt_inicial";
            this.dt_inicial.Size = new System.Drawing.Size(100, 20);
            this.dt_inicial.TabIndex = 0;
            // 
            // grb_paginacao
            // 
            this.grb_paginacao.Controls.Add(this.btn_fechar);
            this.grb_paginacao.Controls.Add(this.btn_imprimir);
            this.grb_paginacao.Location = new System.Drawing.Point(3, 347);
            this.grb_paginacao.Name = "grb_paginacao";
            this.grb_paginacao.Size = new System.Drawing.Size(593, 51);
            this.grb_paginacao.TabIndex = 1;
            this.grb_paginacao.TabStop = false;
            // 
            // btn_fechar
            // 
            this.btn_fechar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_fechar.Image = global::SWS.Properties.Resources.close;
            this.btn_fechar.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btn_fechar.Location = new System.Drawing.Point(90, 22);
            this.btn_fechar.Name = "btn_fechar";
            this.btn_fechar.Size = new System.Drawing.Size(75, 23);
            this.btn_fechar.TabIndex = 1;
            this.btn_fechar.Text = "&Fechar";
            this.btn_fechar.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btn_fechar.UseVisualStyleBackColor = true;
            this.btn_fechar.Click += new System.EventHandler(this.btn_fechar_Click);
            // 
            // btn_imprimir
            // 
            this.btn_imprimir.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_imprimir.Image = global::SWS.Properties.Resources.impressora1;
            this.btn_imprimir.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btn_imprimir.Location = new System.Drawing.Point(9, 22);
            this.btn_imprimir.Name = "btn_imprimir";
            this.btn_imprimir.Size = new System.Drawing.Size(75, 23);
            this.btn_imprimir.TabIndex = 0;
            this.btn_imprimir.Text = "&Imprimir";
            this.btn_imprimir.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btn_imprimir.UseVisualStyleBackColor = true;
            this.btn_imprimir.Click += new System.EventHandler(this.btn_imprimir_Click);
            // 
            // text_cliente
            // 
            this.text_cliente.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.text_cliente.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.text_cliente.Location = new System.Drawing.Point(6, 19);
            this.text_cliente.MaxLength = 100;
            this.text_cliente.Name = "text_cliente";
            this.text_cliente.ReadOnly = true;
            this.text_cliente.Size = new System.Drawing.Size(566, 20);
            this.text_cliente.TabIndex = 0;
            // 
            // btn_cliente
            // 
            this.btn_cliente.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_cliente.Location = new System.Drawing.Point(6, 45);
            this.btn_cliente.Name = "btn_cliente";
            this.btn_cliente.Size = new System.Drawing.Size(75, 23);
            this.btn_cliente.TabIndex = 1;
            this.btn_cliente.Text = "&Cliente";
            this.btn_cliente.UseVisualStyleBackColor = true;
            this.btn_cliente.Click += new System.EventHandler(this.btn_cliente_Click);
            // 
            // btn_limpar
            // 
            this.btn_limpar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_limpar.Location = new System.Drawing.Point(87, 45);
            this.btn_limpar.Name = "btn_limpar";
            this.btn_limpar.Size = new System.Drawing.Size(75, 23);
            this.btn_limpar.TabIndex = 2;
            this.btn_limpar.Text = "&Limpar";
            this.btn_limpar.UseVisualStyleBackColor = true;
            this.btn_limpar.Click += new System.EventHandler(this.btn_limpar_Click);
            // 
            // frm_RelAsoDuplicado
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(600, 400);
            this.ControlBox = false;
            this.Controls.Add(this.grb_paginacao);
            this.Controls.Add(this.grb_dados);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.KeyPreview = true;
            this.Name = "frm_RelAsoDuplicado";
            this.Text = "ATENDIMENTOS DUPLICADOS";
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.frm_RelAsoDuplicado_KeyDown);
            this.grb_dados.ResumeLayout(false);
            this.grb_cliente.ResumeLayout(false);
            this.grb_cliente.PerformLayout();
            this.grb_periodo.ResumeLayout(false);
            this.grb_periodo.PerformLayout();
            this.grb_paginacao.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox grb_dados;
        private System.Windows.Forms.GroupBox grb_paginacao;
        private System.Windows.Forms.Button btn_imprimir;
        private System.Windows.Forms.GroupBox grb_periodo;
        private System.Windows.Forms.DateTimePicker dt_final;
        private System.Windows.Forms.Label lbl_periodo;
        private System.Windows.Forms.DateTimePicker dt_inicial;
        private System.Windows.Forms.Button btn_fechar;
        private System.Windows.Forms.GroupBox grb_cliente;
        private System.Windows.Forms.Button btn_cliente;
        private System.Windows.Forms.TextBox text_cliente;
        private System.Windows.Forms.Button btn_limpar;
    }
}