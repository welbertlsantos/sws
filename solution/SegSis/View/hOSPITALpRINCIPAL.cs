﻿using SWS.Entidade;
using SWS.Facade;
using SWS.View.Resources;
using SWS.View.ViewHelper;
using SWS.ViewHelper;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace SWS.View
{
    public partial class frmHospitalPrincipal : frmTemplate
    {
        Hospital hospital;
        
        public frmHospitalPrincipal()
        {
            InitializeComponent();
            ComboHelper.comboSituacao(cbSituacao);
            ActiveControl = textHospital;
            ValidaPermissoes();
        }

        private void btnIncluir_Click(object sender, EventArgs e)
        {
            try
            {
                frmHospitalIncluir incluirHospital = new frmHospitalIncluir();
                incluirHospital.ShowDialog();

                if (incluirHospital.Hospital != null)
                {
                    hospital = new Hospital();
                    montaGridHospitais();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }

        private void btnAlterar_Click(object sender, EventArgs e)
        {
            try
            {
                if (dgvHospital.CurrentRow == null)
                    throw new Exception("Selecione uma linha.");

                EstudoFacade pcmsoFacade = EstudoFacade.getInstance();

                Hospital hospitalAlterar = pcmsoFacade.findHospitalById((long)dgvHospital.CurrentRow.Cells[0].Value);

                if (!string.Equals(hospitalAlterar.Situacao, ApplicationConstants.ATIVO))
                    throw new Exception("Somente hospitais ativos podem ser alterados.");

                frmHospitalAlterar alterarHospital = new frmHospitalAlterar(hospitalAlterar);
                alterarHospital.ShowDialog();

                hospital = new Hospital();
                montaGridHospitais();

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }

        private void btnPesquisar_Click(object sender, EventArgs e)
        {
            hospital = new Hospital(null, textHospital.Text, string.Empty, string.Empty, string.Empty, string.Empty, string.Empty, string.Empty, string.Empty, string.Empty, string.Empty, ((SelectItem)cbSituacao.SelectedItem).Valor, null);
            montaGridHospitais();
            textHospital.Focus();
        }

        private void btnReativar_Click(object sender, EventArgs e)
        {
            try
            {
                if (dgvHospital.CurrentRow == null)
                    throw new Exception("Selecione uma linha.");

                EstudoFacade pcmsoFacade = EstudoFacade.getInstance();

                Hospital hospitalReativar = pcmsoFacade.findHospitalById((long)dgvHospital.CurrentRow.Cells[0].Value);

                if (!string.Equals(hospitalReativar.Situacao, ApplicationConstants.DESATIVADO))
                    throw new Exception("Somente hospitais inativos podem ser reativados.");
                
                if (MessageBox.Show("Deseja reativar o hospital selecionado?", "Atenção", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                {
                    hospitalReativar.Situacao = ApplicationConstants.ATIVO;
                    pcmsoFacade.updateHospital(hospitalReativar);
                    MessageBox.Show("Hospital reativado com sucesso.", "Confirmação", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    hospital = new Hospital();
                    montaGridHospitais();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }

        private void btnDetalhar_Click(object sender, EventArgs e)
        {
            try
            {
                if (dgvHospital.CurrentRow == null)
                    throw new Exception("Selecione uma linha.");

                EstudoFacade pcmsoFacade = EstudoFacade.getInstance();

                frmHospitalDetalhar detalharHospital = new frmHospitalDetalhar(pcmsoFacade.findHospitalById((long)dgvHospital.CurrentRow.Cells[0].Value));
                detalharHospital.ShowDialog();

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }

        private void btnExcluir_Click(object sender, EventArgs e)
        {
            try
            {
                if (dgvHospital.CurrentRow == null)
                    throw new Exception("Selecione uma linha.");

                EstudoFacade pcmsoFacade = EstudoFacade.getInstance();

                Hospital hospitalExcluir = pcmsoFacade.findHospitalById((long)dgvHospital.CurrentRow.Cells[0].Value);

                if (!string.Equals(hospitalExcluir.Situacao, ApplicationConstants.ATIVO))
                    throw new Exception("Somente hospitais ativos podem ser excluídos.");

                if (MessageBox.Show("Deseja excluir o hospital selecionado?", "Atenção", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                {
                    hospitalExcluir.Situacao = ApplicationConstants.DESATIVADO;
                    pcmsoFacade.updateHospital(hospitalExcluir);
                    MessageBox.Show("Hospital excluído com sucesso.", "Confirmação", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    hospital = new Hospital();
                    montaGridHospitais();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }

        private void btnFechar_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        public void montaGridHospitais()
        {
            try
            {
                this.Cursor = Cursors.WaitCursor;

                EstudoFacade pcmsoFacade = EstudoFacade.getInstance();

                DataSet ds = pcmsoFacade.findHospitalByFilter(hospital);

                dgvHospital.DataSource = ds.Tables[0].DefaultView;

                dgvHospital.Columns[0].HeaderText = "Id";
                dgvHospital.Columns[0].Visible = false;

                dgvHospital.Columns[1].HeaderText = "Nome";
                
                dgvHospital.Columns[2].HeaderText = "Endereço";

                dgvHospital.Columns[3].HeaderText = "Número";

                dgvHospital.Columns[4].HeaderText = "Complemento";

                dgvHospital.Columns[5].HeaderText = "Bairro";

                dgvHospital.Columns[6].HeaderText = "Cidade";

                dgvHospital.Columns[7].HeaderText = "CEP";

                dgvHospital.Columns[8].HeaderText = "UF";

                dgvHospital.Columns[9].HeaderText = "Telefone";

                dgvHospital.Columns[10].HeaderText = "Telefone";

                dgvHospital.Columns[11].HeaderText = "Observação";

                dgvHospital.Columns[12].HeaderText = "Situação";
                dgvHospital.Columns[12].Visible = false;

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }

        }

        private void dgvHospital_RowPrePaint(object sender, DataGridViewRowPrePaintEventArgs e)
        {
            DataGridView dgv = sender as DataGridView;

            if (string.Equals(dgv.Rows[e.RowIndex].Cells[12].Value, ApplicationConstants.DESATIVADO))
                dgv.Rows[e.RowIndex].DefaultCellStyle.ForeColor = Color.Red;
        }

        private void ValidaPermissoes()
        {
            PermissionamentoFacade permissionamentoFacade = PermissionamentoFacade.getInstance();

            btnIncluir.Enabled = permissionamentoFacade.hasPermission("HOSPITAL", "INCLUIR");
            btnAlterar.Enabled = permissionamentoFacade.hasPermission("HOSPITAL", "ALTERAR");
            btnExcluir.Enabled = permissionamentoFacade.hasPermission("HOSPITAL", "EXCLUIR");
            btnReativar.Enabled = permissionamentoFacade.hasPermission("HOSPITAL", "REATIVAR");
        }
    }
}
