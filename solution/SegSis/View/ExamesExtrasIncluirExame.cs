﻿using SWS.Entidade;
using SWS.Facade;
using SWS.View.ViewHelper;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace SWS.View
{
    public partial class frmClienteFuncaoExameIncluir : frmTemplateConsulta
    {
        private Exame exame;
        private ClienteFuncaoExame clienteFuncaoExame;
        private ClienteFuncao clienteFuncao;

        public ClienteFuncaoExame ClienteFuncaoExame
        {
            get { return clienteFuncaoExame; }
            set { clienteFuncaoExame = value; }
        }
        
        public frmClienteFuncaoExameIncluir(ClienteFuncao clienteFuncao)
        {
            InitializeComponent();
            this.clienteFuncao = clienteFuncao;
        }

        private void btn_incluir_Click(object sender, EventArgs e)
        {
            try
            {
                /* verificações na tela */

                if (exame == null)
                    throw new Exception("Inclua um exame primeiro");

                if (String.IsNullOrEmpty(textIdade.Text))
                    throw new Exception("O campo idade não pode ser nulo.");

                bool selecao = false;
                foreach (DataGridViewRow dvRow in dgvPeriodicidade.Rows)
                {
                    if ((bool)dvRow.Cells[2].Value == true)
                    {
                        selecao = true;
                        break;
                    }
                }

                if (!selecao)
                    throw new Exception("Você deve selecionar ao menos uma periodicidade para continuar.");

                /* Verificando para saber se o exame selecionado já está incluído no clienteFuncao */
                EstudoFacade pcmsoFacade = EstudoFacade.getInstance();

                if (pcmsoFacade.findClienteFuncaoExameByClienteFuncaoAndExame(clienteFuncao, exame) != null)
                    throw new Exception("Exame já incluído na função.");

                /* montando colecao de periodicidades */
                HashSet<Periodicidade> periodicidades = new HashSet<Periodicidade>();

                Dictionary<long, int?> periodoVencimento = new Dictionary<long, int?>();

                foreach (DataGridViewRow dvRow in dgvPeriodicidade.Rows)
                    if (Convert.ToBoolean(dvRow.Cells[2].Value) == true)
                    {
                        periodoVencimento.Add((long)dvRow.Cells[0].Value, dvRow.Cells[3].Value == string.Empty ? (int?)null : Convert.ToInt32(dvRow.Cells[3].Value));
                        periodicidades.Add(new Periodicidade((Int64)dvRow.Cells[0].Value, dvRow.Cells[1].Value.ToString(), null));
                    }
                
                clienteFuncaoExame = pcmsoFacade.insertClienteFuncaoExame(new ClienteFuncaoExame(null,clienteFuncao,exame, String.Empty, Convert.ToInt32(textIdade.Text)),periodicidades, periodoVencimento);
                this.Close();

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btn_fechar_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btExame_Click(object sender, EventArgs e)
        {
            try
            {
                frmExameBuscar buscarExame = new frmExameBuscar();
                buscarExame.ShowDialog();

                if (buscarExame.Exame != null)
                {
                    exame = buscarExame.Exame;
                    textExame.Text = exame.Descricao;
                    montaGridPeriodicidade();
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        public void montaGridPeriodicidade()
        {
            try
            {
                EstudoFacade pcmsoFacade = EstudoFacade.getInstance();

                dgvPeriodicidade.Columns.Clear();

                DataSet ds = pcmsoFacade.findAllPeriodicidade();

                dgvPeriodicidade.ColumnCount = 2;

                dgvPeriodicidade.Columns[0].HeaderText = "id_periodicidade";
                dgvPeriodicidade.Columns[0].Visible = false;

                dgvPeriodicidade.Columns[1].HeaderText = "Descrição";
                dgvPeriodicidade.Columns[1].ReadOnly = true;

                DataGridViewCheckBoxColumn check = new DataGridViewCheckBoxColumn();
                check.Name = "Selecione";
                dgvPeriodicidade.Columns.Add(check);
                dgvPeriodicidade.Columns[2].Width = 30;

                DataGridViewComboBoxColumn comboPeriodicidade = new DataGridViewComboBoxColumn();
                comboPeriodicidade.Name = "Vencimento";
                comboPeriodicidade.DefaultCellStyle.BackColor = Color.White;
                dgvPeriodicidade.Columns.Add(comboPeriodicidade);
                ComboHelper.periodoVencimentoCombo(comboPeriodicidade);
                dgvPeriodicidade.Columns[3].Width = 150;
                
                foreach (DataRow dataRow in ds.Tables[0].Rows)
                    dgvPeriodicidade.Rows.Add(Convert.ToInt64(dataRow["id_periodicidade"]), dataRow["descricao"].ToString(), false, exame != null ? exame.PeriodoVencimento.ToString() : string.Empty);


                /* reordenando a grid */
                dgvPeriodicidade.Columns[1].DisplayIndex = 2;
                dgvPeriodicidade.Columns[2].DisplayIndex = 1;
                dgvPeriodicidade.Columns[3].DisplayIndex = 3;

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }

        private void textIdade_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = SWS.View.ViewHelper.ValidaCampoHelper.FormataCampoNumerico(sender, e);
        }

        
    }
}
