﻿using SWS.Entidade;
using SWS.Facade;
using SWS.View.Resources;
using SWS.View.ViewHelper;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace SWS.View
{
    public partial class frmPlanoIncluir : frmTemplate
    {
        private Plano plano;

        public Plano Plano
        {
            get { return plano; }
            set { plano = value; }
        }

        protected List<Parcela> parcelas = new List<Parcela>();
        protected List<PlanoForma> formas = new List<PlanoForma>();

        
        public frmPlanoIncluir()
        {
            InitializeComponent();
            ActiveControl = textDescricao;
        }

        protected void frmPlanoIncluir_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter || e.KeyCode == Keys.Return)
                SendKeys.Send("{TAB}");
        }

        protected virtual void btnGravar_Click(object sender, EventArgs e)
        {
            try
            {
                if (ValidaCampos())
                {
                    FinanceiroFacade financeiroFacade = FinanceiroFacade.getInstance();

                    plano = new Plano(null, textDescricao.Text, ApplicationConstants.ATIVO, false);
                    
                    plano.Parcelas = parcelas;
                    plano.Formas = formas;

                    plano = financeiroFacade.incluirPlano(plano);
                    MessageBox.Show("Plano incluído com sucesso.", "Confirmação", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    this.Close();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }

        protected void btnLimpar_Click(object sender, EventArgs e)
        {
            textDescricao.Text = string.Empty;
            dgvParcela.Columns.Clear();
            dgvForma.Columns.Clear();
        }

        protected void btnFechar_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        protected virtual void btn_incluirParcela_Click(object sender, EventArgs e)
        {
            try
            {
                frmParcelaIncluir incluirParcela = new frmParcelaIncluir();
                incluirParcela.ShowDialog();

                if (incluirParcela.Parcela != null)
                {
                    /*verificando se não existe uma parcela cadastrada com esse dia. */
                    if (parcelas.Exists(x => x.Dias == incluirParcela.Parcela.Dias))
                        throw new Exception("Já existe essa parcela cadastrada.");

                    parcelas.Add(incluirParcela.Parcela);
                    montaDataGridParcelas();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }

        protected virtual void btn_excluirParcela_Click(object sender, EventArgs e)
        {
            try
            {
                if (dgvParcela.CurrentRow == null)
                    throw new Exception("Selecione uma linha.");

                parcelas.RemoveAll(x => x.Dias == (int)dgvParcela.CurrentRow.Cells[1].Value);
                montaDataGridParcelas();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }

        protected virtual void btn_incluirForma_Click(object sender, EventArgs e)
        {
            try
            {
                frmPesquisarForma selecionarForma = new frmPesquisarForma();
                selecionarForma.ShowDialog();

                if (selecionarForma.Formas.Count > 0)
                {
                    /* verificando se já existe alguma forma já incluída no plano */
                    foreach (DataGridViewRow dvRow in dgvForma.Rows)
                        if (selecionarForma.Formas.Exists(x => x.Id == (long)dvRow.Cells[1].Value))
                            throw new Exception("Já existe a forma de recebimento: " + dvRow.Cells[2].Value + " cadastrada para o plano. Refaça a sua pesquisa.");

                    selecionarForma.Formas.ForEach(delegate(Forma forma)
                    {
                        formas.Add(new PlanoForma(null, forma, plano, ApplicationConstants.ATIVO));
                    });

                    montaDataGridForma();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }

        protected virtual void btn_excluir_Click(object sender, EventArgs e)
        {
            try
            {
                if (dgvForma.CurrentRow == null)
                    throw new Exception("Selecione uma linha.");

                formas.RemoveAll(x => x.Forma.Id == (long)dgvForma.CurrentRow.Cells[1].Value);
                montaDataGridForma();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }

        protected void montaDataGridParcelas()
        {
            try
            {
                this.dgvParcela.Columns.Clear();
                this.dgvParcela.ColumnCount = 2;

                this.dgvParcela.Columns[0].HeaderText = "IdParcela ";
                this.dgvParcela.Columns[0].Visible = false;
                
                this.dgvParcela.Columns[1].HeaderText = "Dias";
                this.dgvParcela.Columns[1].Width = 40;
                this.dgvParcela.Columns[1].DefaultCellStyle.BackColor = Color.LightYellow;

                parcelas.ForEach(delegate(Parcela parcela)
                {
                    dgvParcela.Rows.Add(parcela.Id, parcela.Dias);
                });

                this.dgvParcela.Sort(dgvParcela.Columns[1], ListSortDirection.Ascending);

            } 
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        protected void dgvParcela_ColumnAdded(object sender, DataGridViewColumnEventArgs e)
        {
            e.Column.SortMode = DataGridViewColumnSortMode.NotSortable;
        }

        protected bool ValidaCampos()
        {
            bool retorno = true;

            try
            {
                if (String.IsNullOrEmpty(textDescricao.Text))
                    throw new Exception("A descrição não pode ser vazia.");

                if (formas.Count == 0)
                    throw new Exception("O plano deve conter pelo menos uma forma de pagamento");

                if (parcelas.Count == 0)
                    throw new Exception("Você deve incluir pelo menos uma parcela.");
            }
            catch (Exception ex)
            {
                retorno = false;
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

            return retorno;
        }

        protected void dgvForma_ColumnAdded(object sender, DataGridViewColumnEventArgs e)
        {
            e.Column.SortMode = DataGridViewColumnSortMode.NotSortable;
        }

        protected void montaDataGridForma()
        {
            try
            {
                this.Cursor = Cursors.WaitCursor; 
                
                dgvForma.Columns.Clear();
                dgvForma.ColumnCount = 3;

                dgvForma.Columns[0].HeaderText = "id";
                dgvForma.Columns[0].Visible = false;

                dgvForma.Columns[1].HeaderText = "idForma";
                dgvForma.Columns[1].Visible = false;

                dgvForma.Columns[2].HeaderText = "Descrição";

                formas.ForEach(delegate(PlanoForma planoForma)
                {
                    dgvForma.Rows.Add(planoForma.Id, planoForma.Forma.Id, planoForma.Forma.Descricao);
                });

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }

        protected virtual void btnAlterar_Click(object sender, EventArgs e)
        {
            try
            {
                if (dgvParcela.CurrentRow == null)
                    throw new Exception("Selecione uma linha.");

                Parcela parcelaAlterar = parcelas.Find(x => x.Dias == (int)dgvParcela.CurrentRow.Cells[1].Value);

                frmParcelaAlterar alterarParcela = new frmParcelaAlterar(parcelaAlterar);
                alterarParcela.ShowDialog();

                parcelas.RemoveAll(x => x.Dias == parcelaAlterar.Dias);

                parcelas.Add(alterarParcela.Parcela);
                montaDataGridParcelas();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
    }
}
