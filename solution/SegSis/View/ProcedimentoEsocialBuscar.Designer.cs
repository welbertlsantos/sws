﻿namespace SWS.View
{
    partial class frmProcedimentoEsocialBuscar
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            this.flowLayoutPanel1 = new System.Windows.Forms.FlowLayoutPanel();
            this.btnConfirmar = new System.Windows.Forms.Button();
            this.btnFechar = new System.Windows.Forms.Button();
            this.btnProcedimento = new System.Windows.Forms.Button();
            this.textProcedimento = new System.Windows.Forms.TextBox();
            this.lblAgente = new System.Windows.Forms.TextBox();
            this.dgvProcedimento = new System.Windows.Forms.DataGridView();
            ((System.ComponentModel.ISupportInitialize)(this.scForm)).BeginInit();
            this.scForm.Panel1.SuspendLayout();
            this.scForm.Panel2.SuspendLayout();
            this.scForm.SuspendLayout();
            this.pnlForm.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.banner)).BeginInit();
            this.flowLayoutPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvProcedimento)).BeginInit();
            this.SuspendLayout();
            // 
            // scForm
            // 
            // 
            // scForm.Panel1
            // 
            this.scForm.Panel1.Controls.Add(this.flowLayoutPanel1);
            // 
            // pnlForm
            // 
            this.pnlForm.Controls.Add(this.dgvProcedimento);
            this.pnlForm.Controls.Add(this.btnProcedimento);
            this.pnlForm.Controls.Add(this.textProcedimento);
            this.pnlForm.Controls.Add(this.lblAgente);
            // 
            // flowLayoutPanel1
            // 
            this.flowLayoutPanel1.Controls.Add(this.btnConfirmar);
            this.flowLayoutPanel1.Controls.Add(this.btnFechar);
            this.flowLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.flowLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.flowLayoutPanel1.Name = "flowLayoutPanel1";
            this.flowLayoutPanel1.Size = new System.Drawing.Size(534, 30);
            this.flowLayoutPanel1.TabIndex = 0;
            // 
            // btnConfirmar
            // 
            this.btnConfirmar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnConfirmar.Image = global::SWS.Properties.Resources.fechar_ico;
            this.btnConfirmar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnConfirmar.Location = new System.Drawing.Point(3, 3);
            this.btnConfirmar.Name = "btnConfirmar";
            this.btnConfirmar.Size = new System.Drawing.Size(75, 23);
            this.btnConfirmar.TabIndex = 0;
            this.btnConfirmar.Text = "&Confirmar";
            this.btnConfirmar.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnConfirmar.UseVisualStyleBackColor = true;
            this.btnConfirmar.Click += new System.EventHandler(this.btnConfirmar_Click);
            // 
            // btnFechar
            // 
            this.btnFechar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnFechar.Image = global::SWS.Properties.Resources.close;
            this.btnFechar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnFechar.Location = new System.Drawing.Point(84, 3);
            this.btnFechar.Name = "btnFechar";
            this.btnFechar.Size = new System.Drawing.Size(75, 23);
            this.btnFechar.TabIndex = 1;
            this.btnFechar.Text = "Fechar";
            this.btnFechar.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnFechar.UseVisualStyleBackColor = true;
            this.btnFechar.Click += new System.EventHandler(this.btnFechar_Click);
            // 
            // btnProcedimento
            // 
            this.btnProcedimento.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnProcedimento.Image = global::SWS.Properties.Resources.busca;
            this.btnProcedimento.Location = new System.Drawing.Point(477, 3);
            this.btnProcedimento.Name = "btnProcedimento";
            this.btnProcedimento.Size = new System.Drawing.Size(34, 21);
            this.btnProcedimento.TabIndex = 6;
            this.btnProcedimento.UseVisualStyleBackColor = true;
            this.btnProcedimento.Click += new System.EventHandler(this.btnProcedimento_Click);
            // 
            // textProcedimento
            // 
            this.textProcedimento.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textProcedimento.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.textProcedimento.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textProcedimento.Location = new System.Drawing.Point(137, 3);
            this.textProcedimento.MaxLength = 100;
            this.textProcedimento.Name = "textProcedimento";
            this.textProcedimento.Size = new System.Drawing.Size(343, 21);
            this.textProcedimento.TabIndex = 7;
            // 
            // lblAgente
            // 
            this.lblAgente.BackColor = System.Drawing.Color.Silver;
            this.lblAgente.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblAgente.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.lblAgente.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblAgente.Location = new System.Drawing.Point(3, 3);
            this.lblAgente.MaxLength = 100;
            this.lblAgente.Name = "lblAgente";
            this.lblAgente.ReadOnly = true;
            this.lblAgente.Size = new System.Drawing.Size(137, 21);
            this.lblAgente.TabIndex = 8;
            this.lblAgente.Text = "PROCEDIMENTO";
            // 
            // dgvProcedimento
            // 
            this.dgvProcedimento.AllowUserToAddRows = false;
            this.dgvProcedimento.AllowUserToDeleteRows = false;
            this.dgvProcedimento.AllowUserToOrderColumns = true;
            this.dgvProcedimento.AllowUserToResizeColumns = false;
            this.dgvProcedimento.AllowUserToResizeRows = false;
            dataGridViewCellStyle3.BackColor = System.Drawing.Color.Gainsboro;
            this.dgvProcedimento.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle3;
            this.dgvProcedimento.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.DisplayedCells;
            this.dgvProcedimento.BackgroundColor = System.Drawing.Color.White;
            this.dgvProcedimento.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle4.BackColor = System.Drawing.Color.LightGray;
            dataGridViewCellStyle4.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle4.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle4.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle4.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle4.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvProcedimento.DefaultCellStyle = dataGridViewCellStyle4;
            this.dgvProcedimento.Location = new System.Drawing.Point(3, 30);
            this.dgvProcedimento.MultiSelect = false;
            this.dgvProcedimento.Name = "dgvProcedimento";
            this.dgvProcedimento.ReadOnly = true;
            this.dgvProcedimento.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvProcedimento.Size = new System.Drawing.Size(508, 258);
            this.dgvProcedimento.TabIndex = 5;
            this.dgvProcedimento.TabStop = false;
            // 
            // frmProcedimentoEsocialBuscar
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(534, 361);
            this.Name = "frmProcedimentoEsocialBuscar";
            this.Text = "BUSCAR CODIGO DO PROCEDIMENTO";
            this.scForm.Panel1.ResumeLayout(false);
            this.scForm.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.scForm)).EndInit();
            this.scForm.ResumeLayout(false);
            this.pnlForm.ResumeLayout(false);
            this.pnlForm.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.banner)).EndInit();
            this.flowLayoutPanel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvProcedimento)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel1;
        private System.Windows.Forms.Button btnConfirmar;
        private System.Windows.Forms.Button btnFechar;
        private System.Windows.Forms.Button btnProcedimento;
        protected System.Windows.Forms.TextBox textProcedimento;
        protected System.Windows.Forms.TextBox lblAgente;
        private System.Windows.Forms.DataGridView dgvProcedimento;
    }
}