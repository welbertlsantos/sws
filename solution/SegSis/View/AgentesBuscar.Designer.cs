﻿namespace SWS.View
{
    partial class frmAgentesBuscar
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            this.flpAcao = new System.Windows.Forms.FlowLayoutPanel();
            this.btnConfirmar = new System.Windows.Forms.Button();
            this.btnNovo = new System.Windows.Forms.Button();
            this.btnFechar = new System.Windows.Forms.Button();
            this.lblAgente = new System.Windows.Forms.TextBox();
            this.textAgente = new System.Windows.Forms.TextBox();
            this.btnAgente = new System.Windows.Forms.Button();
            this.grbAgente = new System.Windows.Forms.GroupBox();
            this.dgvAgente = new System.Windows.Forms.DataGridView();
            ((System.ComponentModel.ISupportInitialize)(this.scForm)).BeginInit();
            this.scForm.Panel1.SuspendLayout();
            this.scForm.Panel2.SuspendLayout();
            this.scForm.SuspendLayout();
            this.pnlForm.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.banner)).BeginInit();
            this.flpAcao.SuspendLayout();
            this.grbAgente.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvAgente)).BeginInit();
            this.SuspendLayout();
            // 
            // scForm
            // 
            // 
            // scForm.Panel1
            // 
            this.scForm.Panel1.Controls.Add(this.flpAcao);
            // 
            // pnlForm
            // 
            this.pnlForm.Controls.Add(this.grbAgente);
            this.pnlForm.Controls.Add(this.btnAgente);
            this.pnlForm.Controls.Add(this.textAgente);
            this.pnlForm.Controls.Add(this.lblAgente);
            this.pnlForm.Size = new System.Drawing.Size(514, 292);
            // 
            // flpAcao
            // 
            this.flpAcao.Controls.Add(this.btnConfirmar);
            this.flpAcao.Controls.Add(this.btnNovo);
            this.flpAcao.Controls.Add(this.btnFechar);
            this.flpAcao.Dock = System.Windows.Forms.DockStyle.Fill;
            this.flpAcao.Location = new System.Drawing.Point(0, 0);
            this.flpAcao.Name = "flpAcao";
            this.flpAcao.Size = new System.Drawing.Size(534, 30);
            this.flpAcao.TabIndex = 0;
            // 
            // btnConfirmar
            // 
            this.btnConfirmar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnConfirmar.Image = global::SWS.Properties.Resources.fechar_ico;
            this.btnConfirmar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnConfirmar.Location = new System.Drawing.Point(3, 3);
            this.btnConfirmar.Name = "btnConfirmar";
            this.btnConfirmar.Size = new System.Drawing.Size(75, 23);
            this.btnConfirmar.TabIndex = 3;
            this.btnConfirmar.Text = "&Confirmar";
            this.btnConfirmar.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnConfirmar.UseVisualStyleBackColor = true;
            this.btnConfirmar.Click += new System.EventHandler(this.btnConfirmar_Click);
            // 
            // btnNovo
            // 
            this.btnNovo.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnNovo.Image = global::SWS.Properties.Resources.icone_mais;
            this.btnNovo.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnNovo.Location = new System.Drawing.Point(84, 3);
            this.btnNovo.Name = "btnNovo";
            this.btnNovo.Size = new System.Drawing.Size(75, 23);
            this.btnNovo.TabIndex = 4;
            this.btnNovo.Text = "&Novo";
            this.btnNovo.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnNovo.UseVisualStyleBackColor = true;
            this.btnNovo.Click += new System.EventHandler(this.btnNovo_Click);
            // 
            // btnFechar
            // 
            this.btnFechar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnFechar.Image = global::SWS.Properties.Resources.close;
            this.btnFechar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnFechar.Location = new System.Drawing.Point(165, 3);
            this.btnFechar.Name = "btnFechar";
            this.btnFechar.Size = new System.Drawing.Size(75, 23);
            this.btnFechar.TabIndex = 5;
            this.btnFechar.Text = "&Fechar";
            this.btnFechar.UseVisualStyleBackColor = true;
            this.btnFechar.Click += new System.EventHandler(this.btnFechar_Click);
            // 
            // lblAgente
            // 
            this.lblAgente.BackColor = System.Drawing.Color.Silver;
            this.lblAgente.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblAgente.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblAgente.Location = new System.Drawing.Point(7, 12);
            this.lblAgente.MaxLength = 100;
            this.lblAgente.Name = "lblAgente";
            this.lblAgente.ReadOnly = true;
            this.lblAgente.Size = new System.Drawing.Size(137, 21);
            this.lblAgente.TabIndex = 5;
            this.lblAgente.Text = "Agente";
            // 
            // textAgente
            // 
            this.textAgente.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textAgente.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.textAgente.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textAgente.Location = new System.Drawing.Point(143, 12);
            this.textAgente.MaxLength = 100;
            this.textAgente.Name = "textAgente";
            this.textAgente.Size = new System.Drawing.Size(329, 21);
            this.textAgente.TabIndex = 4;
            // 
            // btnAgente
            // 
            this.btnAgente.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnAgente.Image = global::SWS.Properties.Resources.busca;
            this.btnAgente.Location = new System.Drawing.Point(471, 12);
            this.btnAgente.Name = "btnAgente";
            this.btnAgente.Size = new System.Drawing.Size(34, 21);
            this.btnAgente.TabIndex = 1;
            this.btnAgente.UseVisualStyleBackColor = true;
            this.btnAgente.Click += new System.EventHandler(this.btnAgente_Click);
            // 
            // grbAgente
            // 
            this.grbAgente.Controls.Add(this.dgvAgente);
            this.grbAgente.Location = new System.Drawing.Point(7, 39);
            this.grbAgente.Name = "grbAgente";
            this.grbAgente.Size = new System.Drawing.Size(498, 235);
            this.grbAgente.TabIndex = 7;
            this.grbAgente.TabStop = false;
            this.grbAgente.Text = "Agentes";
            // 
            // dgvAgente
            // 
            this.dgvAgente.AllowUserToAddRows = false;
            this.dgvAgente.AllowUserToDeleteRows = false;
            this.dgvAgente.AllowUserToOrderColumns = true;
            this.dgvAgente.AllowUserToResizeRows = false;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.LightGray;
            this.dgvAgente.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle1;
            this.dgvAgente.BackgroundColor = System.Drawing.Color.White;
            this.dgvAgente.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.Color.Silver;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvAgente.DefaultCellStyle = dataGridViewCellStyle2;
            this.dgvAgente.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvAgente.Location = new System.Drawing.Point(3, 16);
            this.dgvAgente.MultiSelect = false;
            this.dgvAgente.Name = "dgvAgente";
            this.dgvAgente.ReadOnly = true;
            this.dgvAgente.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvAgente.Size = new System.Drawing.Size(492, 216);
            this.dgvAgente.TabIndex = 2;
            // 
            // frmAgentesBuscar
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(534, 361);
            this.Name = "frmAgentesBuscar";
            this.Text = "BUSCAR AGENTE E RISCO";
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.frmAgentesBuscar_KeyDown);
            this.scForm.Panel1.ResumeLayout(false);
            this.scForm.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.scForm)).EndInit();
            this.scForm.ResumeLayout(false);
            this.pnlForm.ResumeLayout(false);
            this.pnlForm.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.banner)).EndInit();
            this.flpAcao.ResumeLayout(false);
            this.grbAgente.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvAgente)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.FlowLayoutPanel flpAcao;
        private System.Windows.Forms.Button btnConfirmar;
        private System.Windows.Forms.Button btnNovo;
        private System.Windows.Forms.Button btnFechar;
        protected System.Windows.Forms.TextBox textAgente;
        protected System.Windows.Forms.TextBox lblAgente;
        private System.Windows.Forms.Button btnAgente;
        private System.Windows.Forms.GroupBox grbAgente;
        private System.Windows.Forms.DataGridView dgvAgente;
    }
}