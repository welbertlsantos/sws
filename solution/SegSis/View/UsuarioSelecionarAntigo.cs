﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using SWS.Facade;
using SWS.Entidade;
using SWS.View.Resources;

namespace SWS.View
{
    public partial class frmUsuarioSelecionarAntigo : BaseFormConsulta
    {
        private static String msg1 = " Selecione um linha";

        Usuario usuario;

        public Usuario Usuario
        {
            get { return usuario; }
            set { usuario = value; }
        }

        public frmUsuarioSelecionarAntigo()
        {
            InitializeComponent();
        }

        public void montaDataGrid()
        {
            UsuarioFacade usuarioFacade = UsuarioFacade.getInstance();

            usuario.Nome = textNome.Text;
            usuario.Situacao = ApplicationConstants.ATIVO;

            DataSet ds = usuarioFacade.findUsuarioByFilter(new Usuario(null, textNome.Text, String.Empty, String.Empty, String.Empty, null, ApplicationConstants.ATIVO, String.Empty, String.Empty, String.Empty, null, null, String.Empty, false, null, String.Empty, false, string.Empty, string.Empty));

            dgvUsuario.DataSource = ds.Tables["Usuarios"].DefaultView;
            
            dgvUsuario.AutoResizeColumns();
            dgvUsuario.Columns[0].HeaderText = "ID";
            dgvUsuario.Columns[1].HeaderText = "Nome do Usuário";

            dgvUsuario.Columns[0].Visible = false;

            dgvUsuario.MultiSelect = false; // permitindo apenas uma selecao do cliente.

        }

        private void btn_buscar_Click(object sender, EventArgs e)
        {
            try
            {
                this.dgvUsuario.Columns.Clear();
                montaDataGrid();
                textNome.Focus();
            }
            catch (Exception Ex)
            {
                MessageBox.Show(Ex.Message);
            }
        }

        private void btn_fechar_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btn_ok_Click(object sender, EventArgs e)
        {
            UsuarioFacade usuarioFacede = UsuarioFacade.getInstance();

            try
            {
                if (dgvUsuario.CurrentRow == null)
                    throw new Exception (msg1);
             
   
                usuario = usuarioFacede.findUsuarioPpraById((Int64)dgvUsuario.CurrentRow.Cells[0].Value);
                    
                this.Close();

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
        }
    }
}
