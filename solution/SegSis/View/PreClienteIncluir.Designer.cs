﻿namespace SWS.View
{
    partial class frmPreClienteIncluir
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.flpAcao = new System.Windows.Forms.FlowLayoutPanel();
            this.btnGravar = new System.Windows.Forms.Button();
            this.btnFechar = new System.Windows.Forms.Button();
            this.lblCliente = new System.Windows.Forms.TextBox();
            this.lblFantasia = new System.Windows.Forms.TextBox();
            this.lblCnpj = new System.Windows.Forms.TextBox();
            this.lblInscricao = new System.Windows.Forms.TextBox();
            this.lblEndereco = new System.Windows.Forms.TextBox();
            this.lblNumero = new System.Windows.Forms.TextBox();
            this.lblComplemento = new System.Windows.Forms.TextBox();
            this.lblBairro = new System.Windows.Forms.TextBox();
            this.lblCep = new System.Windows.Forms.TextBox();
            this.lblUf = new System.Windows.Forms.TextBox();
            this.lblCidade = new System.Windows.Forms.TextBox();
            this.lblTelefone = new System.Windows.Forms.TextBox();
            this.lblTelefone2 = new System.Windows.Forms.TextBox();
            this.lblContato = new System.Windows.Forms.TextBox();
            this.lblEmail = new System.Windows.Forms.TextBox();
            this.lblLegendaObrigatorio = new System.Windows.Forms.Label();
            this.textRazaoSocial = new System.Windows.Forms.TextBox();
            this.lblCampo1 = new System.Windows.Forms.Label();
            this.textFantasia = new System.Windows.Forms.TextBox();
            this.textCNPJ = new System.Windows.Forms.MaskedTextBox();
            this.textInscricao = new System.Windows.Forms.TextBox();
            this.textEndereco = new System.Windows.Forms.TextBox();
            this.textNumero = new System.Windows.Forms.TextBox();
            this.textComplemento = new System.Windows.Forms.TextBox();
            this.textBairro = new System.Windows.Forms.TextBox();
            this.textCEP = new System.Windows.Forms.MaskedTextBox();
            this.textEmail = new System.Windows.Forms.TextBox();
            this.textContato = new System.Windows.Forms.TextBox();
            this.textCelular = new System.Windows.Forms.TextBox();
            this.textTelefone = new System.Windows.Forms.TextBox();
            this.cbUf = new SWS.ComboBoxWithBorder();
            this.cbCidade = new SWS.ComboBoxWithBorder();
            this.lblTelefoneObrigatorio = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.scForm)).BeginInit();
            this.scForm.Panel1.SuspendLayout();
            this.scForm.Panel2.SuspendLayout();
            this.scForm.SuspendLayout();
            this.pnlForm.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.banner)).BeginInit();
            this.flpAcao.SuspendLayout();
            this.SuspendLayout();
            // 
            // scForm
            // 
            // 
            // scForm.Panel1
            // 
            this.scForm.Panel1.Controls.Add(this.flpAcao);
            // 
            // pnlForm
            // 
            this.pnlForm.Controls.Add(this.lblTelefoneObrigatorio);
            this.pnlForm.Controls.Add(this.cbCidade);
            this.pnlForm.Controls.Add(this.cbUf);
            this.pnlForm.Controls.Add(this.textEmail);
            this.pnlForm.Controls.Add(this.textContato);
            this.pnlForm.Controls.Add(this.textCelular);
            this.pnlForm.Controls.Add(this.textTelefone);
            this.pnlForm.Controls.Add(this.textCEP);
            this.pnlForm.Controls.Add(this.textBairro);
            this.pnlForm.Controls.Add(this.textComplemento);
            this.pnlForm.Controls.Add(this.textNumero);
            this.pnlForm.Controls.Add(this.textEndereco);
            this.pnlForm.Controls.Add(this.textInscricao);
            this.pnlForm.Controls.Add(this.textCNPJ);
            this.pnlForm.Controls.Add(this.textFantasia);
            this.pnlForm.Controls.Add(this.lblCampo1);
            this.pnlForm.Controls.Add(this.textRazaoSocial);
            this.pnlForm.Controls.Add(this.lblLegendaObrigatorio);
            this.pnlForm.Controls.Add(this.lblEmail);
            this.pnlForm.Controls.Add(this.lblContato);
            this.pnlForm.Controls.Add(this.lblTelefone2);
            this.pnlForm.Controls.Add(this.lblTelefone);
            this.pnlForm.Controls.Add(this.lblCidade);
            this.pnlForm.Controls.Add(this.lblUf);
            this.pnlForm.Controls.Add(this.lblCep);
            this.pnlForm.Controls.Add(this.lblBairro);
            this.pnlForm.Controls.Add(this.lblComplemento);
            this.pnlForm.Controls.Add(this.lblNumero);
            this.pnlForm.Controls.Add(this.lblEndereco);
            this.pnlForm.Controls.Add(this.lblInscricao);
            this.pnlForm.Controls.Add(this.lblCnpj);
            this.pnlForm.Controls.Add(this.lblFantasia);
            this.pnlForm.Controls.Add(this.lblCliente);
            this.pnlForm.Size = new System.Drawing.Size(514, 345);
            // 
            // flpAcao
            // 
            this.flpAcao.Controls.Add(this.btnGravar);
            this.flpAcao.Controls.Add(this.btnFechar);
            this.flpAcao.Dock = System.Windows.Forms.DockStyle.Fill;
            this.flpAcao.Location = new System.Drawing.Point(0, 0);
            this.flpAcao.Name = "flpAcao";
            this.flpAcao.Size = new System.Drawing.Size(534, 30);
            this.flpAcao.TabIndex = 0;
            // 
            // btnGravar
            // 
            this.btnGravar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnGravar.Image = global::SWS.Properties.Resources.Gravar;
            this.btnGravar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnGravar.Location = new System.Drawing.Point(3, 3);
            this.btnGravar.Name = "btnGravar";
            this.btnGravar.Size = new System.Drawing.Size(75, 23);
            this.btnGravar.TabIndex = 19;
            this.btnGravar.TabStop = false;
            this.btnGravar.Text = "&Gravar";
            this.btnGravar.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnGravar.UseVisualStyleBackColor = true;
            this.btnGravar.Click += new System.EventHandler(this.btnGravar_Click);
            // 
            // btnFechar
            // 
            this.btnFechar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnFechar.Image = global::SWS.Properties.Resources.close;
            this.btnFechar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnFechar.Location = new System.Drawing.Point(84, 3);
            this.btnFechar.Name = "btnFechar";
            this.btnFechar.Size = new System.Drawing.Size(75, 23);
            this.btnFechar.TabIndex = 20;
            this.btnFechar.TabStop = false;
            this.btnFechar.Text = "&Fechar";
            this.btnFechar.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnFechar.UseVisualStyleBackColor = true;
            this.btnFechar.Click += new System.EventHandler(this.btnFechar_Click);
            // 
            // lblCliente
            // 
            this.lblCliente.BackColor = System.Drawing.Color.LightGray;
            this.lblCliente.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblCliente.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCliente.Location = new System.Drawing.Point(7, 6);
            this.lblCliente.Name = "lblCliente";
            this.lblCliente.ReadOnly = true;
            this.lblCliente.Size = new System.Drawing.Size(117, 21);
            this.lblCliente.TabIndex = 0;
            this.lblCliente.TabStop = false;
            this.lblCliente.Text = "Cliente";
            // 
            // lblFantasia
            // 
            this.lblFantasia.BackColor = System.Drawing.Color.LightGray;
            this.lblFantasia.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblFantasia.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblFantasia.Location = new System.Drawing.Point(7, 26);
            this.lblFantasia.Name = "lblFantasia";
            this.lblFantasia.ReadOnly = true;
            this.lblFantasia.Size = new System.Drawing.Size(117, 21);
            this.lblFantasia.TabIndex = 1;
            this.lblFantasia.TabStop = false;
            this.lblFantasia.Text = "Nome de fantasia";
            // 
            // lblCnpj
            // 
            this.lblCnpj.BackColor = System.Drawing.Color.LightGray;
            this.lblCnpj.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblCnpj.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCnpj.Location = new System.Drawing.Point(7, 46);
            this.lblCnpj.Name = "lblCnpj";
            this.lblCnpj.ReadOnly = true;
            this.lblCnpj.Size = new System.Drawing.Size(117, 21);
            this.lblCnpj.TabIndex = 2;
            this.lblCnpj.TabStop = false;
            this.lblCnpj.Text = "CNPJ";
            // 
            // lblInscricao
            // 
            this.lblInscricao.BackColor = System.Drawing.Color.LightGray;
            this.lblInscricao.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblInscricao.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblInscricao.Location = new System.Drawing.Point(7, 66);
            this.lblInscricao.Name = "lblInscricao";
            this.lblInscricao.ReadOnly = true;
            this.lblInscricao.Size = new System.Drawing.Size(117, 21);
            this.lblInscricao.TabIndex = 3;
            this.lblInscricao.TabStop = false;
            this.lblInscricao.Text = "Inscrição est/mun";
            // 
            // lblEndereco
            // 
            this.lblEndereco.BackColor = System.Drawing.Color.LightGray;
            this.lblEndereco.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblEndereco.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblEndereco.Location = new System.Drawing.Point(7, 86);
            this.lblEndereco.Name = "lblEndereco";
            this.lblEndereco.ReadOnly = true;
            this.lblEndereco.Size = new System.Drawing.Size(117, 21);
            this.lblEndereco.TabIndex = 4;
            this.lblEndereco.TabStop = false;
            this.lblEndereco.Text = "Endereço";
            // 
            // lblNumero
            // 
            this.lblNumero.BackColor = System.Drawing.Color.LightGray;
            this.lblNumero.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblNumero.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNumero.Location = new System.Drawing.Point(7, 106);
            this.lblNumero.Name = "lblNumero";
            this.lblNumero.ReadOnly = true;
            this.lblNumero.Size = new System.Drawing.Size(117, 21);
            this.lblNumero.TabIndex = 5;
            this.lblNumero.TabStop = false;
            this.lblNumero.Text = "Número";
            // 
            // lblComplemento
            // 
            this.lblComplemento.BackColor = System.Drawing.Color.LightGray;
            this.lblComplemento.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblComplemento.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblComplemento.Location = new System.Drawing.Point(7, 126);
            this.lblComplemento.Name = "lblComplemento";
            this.lblComplemento.ReadOnly = true;
            this.lblComplemento.Size = new System.Drawing.Size(117, 21);
            this.lblComplemento.TabIndex = 6;
            this.lblComplemento.TabStop = false;
            this.lblComplemento.Text = "Complemento";
            // 
            // lblBairro
            // 
            this.lblBairro.BackColor = System.Drawing.Color.LightGray;
            this.lblBairro.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblBairro.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblBairro.Location = new System.Drawing.Point(7, 146);
            this.lblBairro.Name = "lblBairro";
            this.lblBairro.ReadOnly = true;
            this.lblBairro.Size = new System.Drawing.Size(117, 21);
            this.lblBairro.TabIndex = 7;
            this.lblBairro.TabStop = false;
            this.lblBairro.Text = "Bairro";
            // 
            // lblCep
            // 
            this.lblCep.BackColor = System.Drawing.Color.LightGray;
            this.lblCep.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblCep.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCep.Location = new System.Drawing.Point(7, 166);
            this.lblCep.Name = "lblCep";
            this.lblCep.ReadOnly = true;
            this.lblCep.Size = new System.Drawing.Size(117, 21);
            this.lblCep.TabIndex = 8;
            this.lblCep.TabStop = false;
            this.lblCep.Text = "CEP";
            // 
            // lblUf
            // 
            this.lblUf.BackColor = System.Drawing.Color.LightGray;
            this.lblUf.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblUf.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblUf.Location = new System.Drawing.Point(7, 186);
            this.lblUf.Name = "lblUf";
            this.lblUf.ReadOnly = true;
            this.lblUf.Size = new System.Drawing.Size(117, 21);
            this.lblUf.TabIndex = 9;
            this.lblUf.TabStop = false;
            this.lblUf.Text = "UF";
            // 
            // lblCidade
            // 
            this.lblCidade.BackColor = System.Drawing.Color.LightGray;
            this.lblCidade.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblCidade.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCidade.Location = new System.Drawing.Point(7, 206);
            this.lblCidade.Name = "lblCidade";
            this.lblCidade.ReadOnly = true;
            this.lblCidade.Size = new System.Drawing.Size(117, 21);
            this.lblCidade.TabIndex = 10;
            this.lblCidade.TabStop = false;
            this.lblCidade.Text = "Cidade";
            // 
            // lblTelefone
            // 
            this.lblTelefone.BackColor = System.Drawing.Color.LightGray;
            this.lblTelefone.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblTelefone.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTelefone.Location = new System.Drawing.Point(7, 226);
            this.lblTelefone.Name = "lblTelefone";
            this.lblTelefone.ReadOnly = true;
            this.lblTelefone.Size = new System.Drawing.Size(117, 21);
            this.lblTelefone.TabIndex = 11;
            this.lblTelefone.TabStop = false;
            this.lblTelefone.Text = "Telefone";
            // 
            // lblTelefone2
            // 
            this.lblTelefone2.BackColor = System.Drawing.Color.LightGray;
            this.lblTelefone2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblTelefone2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTelefone2.Location = new System.Drawing.Point(7, 246);
            this.lblTelefone2.Name = "lblTelefone2";
            this.lblTelefone2.ReadOnly = true;
            this.lblTelefone2.Size = new System.Drawing.Size(117, 21);
            this.lblTelefone2.TabIndex = 12;
            this.lblTelefone2.TabStop = false;
            this.lblTelefone2.Text = "Celular / Fax";
            // 
            // lblContato
            // 
            this.lblContato.BackColor = System.Drawing.Color.LightGray;
            this.lblContato.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblContato.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblContato.Location = new System.Drawing.Point(7, 266);
            this.lblContato.Name = "lblContato";
            this.lblContato.ReadOnly = true;
            this.lblContato.Size = new System.Drawing.Size(117, 21);
            this.lblContato.TabIndex = 13;
            this.lblContato.TabStop = false;
            this.lblContato.Text = "Contato";
            // 
            // lblEmail
            // 
            this.lblEmail.BackColor = System.Drawing.Color.LightGray;
            this.lblEmail.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblEmail.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblEmail.Location = new System.Drawing.Point(7, 286);
            this.lblEmail.Name = "lblEmail";
            this.lblEmail.ReadOnly = true;
            this.lblEmail.Size = new System.Drawing.Size(117, 21);
            this.lblEmail.TabIndex = 14;
            this.lblEmail.TabStop = false;
            this.lblEmail.Text = "E-Mail";
            // 
            // lblLegendaObrigatorio
            // 
            this.lblLegendaObrigatorio.AutoSize = true;
            this.lblLegendaObrigatorio.BackColor = System.Drawing.Color.White;
            this.lblLegendaObrigatorio.ForeColor = System.Drawing.Color.Red;
            this.lblLegendaObrigatorio.Location = new System.Drawing.Point(4, 321);
            this.lblLegendaObrigatorio.Name = "lblLegendaObrigatorio";
            this.lblLegendaObrigatorio.Size = new System.Drawing.Size(204, 13);
            this.lblLegendaObrigatorio.TabIndex = 31;
            this.lblLegendaObrigatorio.Text = "Campos marcados com X são obrigatórios";
            // 
            // textRazaoSocial
            // 
            this.textRazaoSocial.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textRazaoSocial.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.textRazaoSocial.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textRazaoSocial.Location = new System.Drawing.Point(123, 6);
            this.textRazaoSocial.MaxLength = 100;
            this.textRazaoSocial.Name = "textRazaoSocial";
            this.textRazaoSocial.Size = new System.Drawing.Size(364, 21);
            this.textRazaoSocial.TabIndex = 1;
            // 
            // lblCampo1
            // 
            this.lblCampo1.AutoSize = true;
            this.lblCampo1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCampo1.ForeColor = System.Drawing.Color.Red;
            this.lblCampo1.Location = new System.Drawing.Point(491, 10);
            this.lblCampo1.Name = "lblCampo1";
            this.lblCampo1.Size = new System.Drawing.Size(15, 13);
            this.lblCampo1.TabIndex = 33;
            this.lblCampo1.Text = "X";
            // 
            // textFantasia
            // 
            this.textFantasia.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textFantasia.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.textFantasia.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textFantasia.Location = new System.Drawing.Point(123, 26);
            this.textFantasia.MaxLength = 100;
            this.textFantasia.Name = "textFantasia";
            this.textFantasia.Size = new System.Drawing.Size(364, 21);
            this.textFantasia.TabIndex = 2;
            // 
            // textCNPJ
            // 
            this.textCNPJ.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textCNPJ.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textCNPJ.Location = new System.Drawing.Point(123, 46);
            this.textCNPJ.Mask = "99,999,999/9999,99";
            this.textCNPJ.Name = "textCNPJ";
            this.textCNPJ.PromptChar = ' ';
            this.textCNPJ.Size = new System.Drawing.Size(364, 21);
            this.textCNPJ.TabIndex = 3;
            this.textCNPJ.Leave += new System.EventHandler(this.textCNPJ_Leave);
            // 
            // textInscricao
            // 
            this.textInscricao.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textInscricao.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textInscricao.Location = new System.Drawing.Point(123, 66);
            this.textInscricao.MaxLength = 20;
            this.textInscricao.Name = "textInscricao";
            this.textInscricao.Size = new System.Drawing.Size(364, 21);
            this.textInscricao.TabIndex = 4;
            // 
            // textEndereco
            // 
            this.textEndereco.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textEndereco.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.textEndereco.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textEndereco.Location = new System.Drawing.Point(123, 86);
            this.textEndereco.MaxLength = 100;
            this.textEndereco.Name = "textEndereco";
            this.textEndereco.Size = new System.Drawing.Size(364, 21);
            this.textEndereco.TabIndex = 5;
            // 
            // textNumero
            // 
            this.textNumero.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textNumero.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.textNumero.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textNumero.Location = new System.Drawing.Point(123, 106);
            this.textNumero.MaxLength = 10;
            this.textNumero.Name = "textNumero";
            this.textNumero.Size = new System.Drawing.Size(364, 21);
            this.textNumero.TabIndex = 6;
            // 
            // textComplemento
            // 
            this.textComplemento.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textComplemento.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.textComplemento.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textComplemento.Location = new System.Drawing.Point(123, 126);
            this.textComplemento.MaxLength = 100;
            this.textComplemento.Name = "textComplemento";
            this.textComplemento.Size = new System.Drawing.Size(364, 21);
            this.textComplemento.TabIndex = 7;
            // 
            // textBairro
            // 
            this.textBairro.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBairro.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.textBairro.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBairro.Location = new System.Drawing.Point(123, 146);
            this.textBairro.MaxLength = 100;
            this.textBairro.Name = "textBairro";
            this.textBairro.Size = new System.Drawing.Size(364, 21);
            this.textBairro.TabIndex = 8;
            // 
            // textCEP
            // 
            this.textCEP.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textCEP.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textCEP.Location = new System.Drawing.Point(123, 166);
            this.textCEP.Mask = "99999-999";
            this.textCEP.Name = "textCEP";
            this.textCEP.PromptChar = ' ';
            this.textCEP.Size = new System.Drawing.Size(364, 21);
            this.textCEP.TabIndex = 9;
            // 
            // textEmail
            // 
            this.textEmail.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textEmail.CharacterCasing = System.Windows.Forms.CharacterCasing.Lower;
            this.textEmail.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textEmail.Location = new System.Drawing.Point(123, 286);
            this.textEmail.MaxLength = 50;
            this.textEmail.Name = "textEmail";
            this.textEmail.Size = new System.Drawing.Size(364, 21);
            this.textEmail.TabIndex = 15;
            // 
            // textContato
            // 
            this.textContato.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textContato.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.textContato.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textContato.Location = new System.Drawing.Point(123, 266);
            this.textContato.MaxLength = 100;
            this.textContato.Name = "textContato";
            this.textContato.Size = new System.Drawing.Size(364, 21);
            this.textContato.TabIndex = 14;
            // 
            // textCelular
            // 
            this.textCelular.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textCelular.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.textCelular.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textCelular.Location = new System.Drawing.Point(123, 246);
            this.textCelular.MaxLength = 15;
            this.textCelular.Name = "textCelular";
            this.textCelular.Size = new System.Drawing.Size(364, 21);
            this.textCelular.TabIndex = 13;
            // 
            // textTelefone
            // 
            this.textTelefone.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textTelefone.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.textTelefone.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textTelefone.Location = new System.Drawing.Point(123, 226);
            this.textTelefone.MaxLength = 15;
            this.textTelefone.Name = "textTelefone";
            this.textTelefone.Size = new System.Drawing.Size(364, 21);
            this.textTelefone.TabIndex = 12;
            // 
            // cbUf
            // 
            this.cbUf.BackColor = System.Drawing.Color.LightGray;
            this.cbUf.BorderColor = System.Drawing.Color.DimGray;
            this.cbUf.BorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid;
            this.cbUf.FormattingEnabled = true;
            this.cbUf.Location = new System.Drawing.Point(123, 186);
            this.cbUf.Name = "cbUf";
            this.cbUf.Size = new System.Drawing.Size(364, 21);
            this.cbUf.TabIndex = 10;
            this.cbUf.SelectedIndexChanged += new System.EventHandler(this.cbUf_SelectedIndexChanged);
            // 
            // cbCidade
            // 
            this.cbCidade.BackColor = System.Drawing.Color.LightGray;
            this.cbCidade.BorderColor = System.Drawing.Color.DimGray;
            this.cbCidade.BorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid;
            this.cbCidade.FormattingEnabled = true;
            this.cbCidade.Location = new System.Drawing.Point(123, 206);
            this.cbCidade.Name = "cbCidade";
            this.cbCidade.Size = new System.Drawing.Size(364, 21);
            this.cbCidade.TabIndex = 11;
            this.cbCidade.Click += new System.EventHandler(this.cbCidade_Click);
            // 
            // lblTelefoneObrigatorio
            // 
            this.lblTelefoneObrigatorio.AutoSize = true;
            this.lblTelefoneObrigatorio.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTelefoneObrigatorio.ForeColor = System.Drawing.Color.Red;
            this.lblTelefoneObrigatorio.Location = new System.Drawing.Point(491, 230);
            this.lblTelefoneObrigatorio.Name = "lblTelefoneObrigatorio";
            this.lblTelefoneObrigatorio.Size = new System.Drawing.Size(15, 13);
            this.lblTelefoneObrigatorio.TabIndex = 34;
            this.lblTelefoneObrigatorio.Text = "X";
            // 
            // frmPreClienteIncluir
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(534, 361);
            this.Name = "frmPreClienteIncluir";
            this.Text = "INCLUIR PRÉ CLIENTE";
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.frmPreClienteIncluir_KeyDown);
            this.scForm.Panel1.ResumeLayout(false);
            this.scForm.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.scForm)).EndInit();
            this.scForm.ResumeLayout(false);
            this.pnlForm.ResumeLayout(false);
            this.pnlForm.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.banner)).EndInit();
            this.flpAcao.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.FlowLayoutPanel flpAcao;
        private System.Windows.Forms.Button btnGravar;
        private System.Windows.Forms.Button btnFechar;
        protected System.Windows.Forms.TextBox lblCliente;
        protected System.Windows.Forms.TextBox lblCnpj;
        protected System.Windows.Forms.TextBox lblFantasia;
        protected System.Windows.Forms.TextBox lblInscricao;
        protected System.Windows.Forms.TextBox lblNumero;
        protected System.Windows.Forms.TextBox lblEndereco;
        protected System.Windows.Forms.TextBox lblBairro;
        protected System.Windows.Forms.TextBox lblComplemento;
        protected System.Windows.Forms.TextBox lblCidade;
        protected System.Windows.Forms.TextBox lblUf;
        protected System.Windows.Forms.TextBox lblCep;
        protected System.Windows.Forms.TextBox lblTelefone2;
        protected System.Windows.Forms.TextBox lblTelefone;
        protected System.Windows.Forms.TextBox lblEmail;
        protected System.Windows.Forms.TextBox lblContato;
        private System.Windows.Forms.Label lblLegendaObrigatorio;
        private System.Windows.Forms.TextBox textRazaoSocial;
        private System.Windows.Forms.Label lblCampo1;
        private System.Windows.Forms.TextBox textFantasia;
        private System.Windows.Forms.MaskedTextBox textCNPJ;
        private System.Windows.Forms.TextBox textInscricao;
        private System.Windows.Forms.TextBox textEndereco;
        private System.Windows.Forms.TextBox textNumero;
        private System.Windows.Forms.TextBox textComplemento;
        private System.Windows.Forms.TextBox textBairro;
        private System.Windows.Forms.MaskedTextBox textCEP;
        private System.Windows.Forms.TextBox textEmail;
        private System.Windows.Forms.TextBox textContato;
        private System.Windows.Forms.TextBox textCelular;
        private System.Windows.Forms.TextBox textTelefone;
        private ComboBoxWithBorder cbCidade;
        private ComboBoxWithBorder cbUf;
        private System.Windows.Forms.Label lblTelefoneObrigatorio;
    }
}