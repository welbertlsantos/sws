﻿namespace SWS.View
{
    partial class frmEpiIncluir
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.flpAcao = new System.Windows.Forms.FlowLayoutPanel();
            this.btn_gravar = new System.Windows.Forms.Button();
            this.lbl_limpar = new System.Windows.Forms.Button();
            this.btn_fechar = new System.Windows.Forms.Button();
            this.lblEpi = new System.Windows.Forms.TextBox();
            this.lblFinalidade = new System.Windows.Forms.TextBox();
            this.lblCA = new System.Windows.Forms.TextBox();
            this.text_ca = new System.Windows.Forms.TextBox();
            this.text_finalidade = new System.Windows.Forms.TextBox();
            this.text_descricao = new System.Windows.Forms.TextBox();
            ((System.ComponentModel.ISupportInitialize)(this.spForm)).BeginInit();
            this.spForm.Panel1.SuspendLayout();
            this.spForm.Panel2.SuspendLayout();
            this.spForm.SuspendLayout();
            this.pnlForm.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.banner)).BeginInit();
            this.flpAcao.SuspendLayout();
            this.SuspendLayout();
            // 
            // spForm
            // 
            // 
            // spForm.Panel1
            // 
            this.spForm.Panel1.Controls.Add(this.flpAcao);
            // 
            // pnlForm
            // 
            this.pnlForm.Controls.Add(this.text_ca);
            this.pnlForm.Controls.Add(this.text_finalidade);
            this.pnlForm.Controls.Add(this.text_descricao);
            this.pnlForm.Controls.Add(this.lblCA);
            this.pnlForm.Controls.Add(this.lblFinalidade);
            this.pnlForm.Controls.Add(this.lblEpi);
            // 
            // flpAcao
            // 
            this.flpAcao.Controls.Add(this.btn_gravar);
            this.flpAcao.Controls.Add(this.lbl_limpar);
            this.flpAcao.Controls.Add(this.btn_fechar);
            this.flpAcao.Dock = System.Windows.Forms.DockStyle.Fill;
            this.flpAcao.Location = new System.Drawing.Point(0, 0);
            this.flpAcao.Name = "flpAcao";
            this.flpAcao.Size = new System.Drawing.Size(784, 35);
            this.flpAcao.TabIndex = 0;
            // 
            // btn_gravar
            // 
            this.btn_gravar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_gravar.Image = global::SWS.Properties.Resources.Gravar;
            this.btn_gravar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btn_gravar.Location = new System.Drawing.Point(3, 3);
            this.btn_gravar.Name = "btn_gravar";
            this.btn_gravar.Size = new System.Drawing.Size(77, 23);
            this.btn_gravar.TabIndex = 8;
            this.btn_gravar.TabStop = false;
            this.btn_gravar.Text = "&Gravar";
            this.btn_gravar.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btn_gravar.UseVisualStyleBackColor = true;
            this.btn_gravar.Click += new System.EventHandler(this.btn_gravar_Click);
            // 
            // lbl_limpar
            // 
            this.lbl_limpar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.lbl_limpar.Image = global::SWS.Properties.Resources.vassoura;
            this.lbl_limpar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.lbl_limpar.Location = new System.Drawing.Point(86, 3);
            this.lbl_limpar.Name = "lbl_limpar";
            this.lbl_limpar.Size = new System.Drawing.Size(64, 23);
            this.lbl_limpar.TabIndex = 7;
            this.lbl_limpar.TabStop = false;
            this.lbl_limpar.Text = "&Limpar";
            this.lbl_limpar.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.lbl_limpar.UseVisualStyleBackColor = true;
            this.lbl_limpar.Click += new System.EventHandler(this.lbl_limpar_Click);
            // 
            // btn_fechar
            // 
            this.btn_fechar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_fechar.Image = global::SWS.Properties.Resources.close;
            this.btn_fechar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btn_fechar.Location = new System.Drawing.Point(156, 3);
            this.btn_fechar.Name = "btn_fechar";
            this.btn_fechar.Size = new System.Drawing.Size(77, 23);
            this.btn_fechar.TabIndex = 9;
            this.btn_fechar.TabStop = false;
            this.btn_fechar.Text = "&Fechar";
            this.btn_fechar.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btn_fechar.UseVisualStyleBackColor = true;
            this.btn_fechar.Click += new System.EventHandler(this.btn_fechar_Click);
            // 
            // lblEpi
            // 
            this.lblEpi.BackColor = System.Drawing.Color.LightGray;
            this.lblEpi.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblEpi.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblEpi.Location = new System.Drawing.Point(11, 10);
            this.lblEpi.Name = "lblEpi";
            this.lblEpi.ReadOnly = true;
            this.lblEpi.Size = new System.Drawing.Size(100, 21);
            this.lblEpi.TabIndex = 1;
            this.lblEpi.TabStop = false;
            this.lblEpi.Text = "EPI";
            // 
            // lblFinalidade
            // 
            this.lblFinalidade.BackColor = System.Drawing.Color.LightGray;
            this.lblFinalidade.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblFinalidade.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblFinalidade.Location = new System.Drawing.Point(11, 50);
            this.lblFinalidade.Name = "lblFinalidade";
            this.lblFinalidade.ReadOnly = true;
            this.lblFinalidade.Size = new System.Drawing.Size(100, 21);
            this.lblFinalidade.TabIndex = 2;
            this.lblFinalidade.TabStop = false;
            this.lblFinalidade.Text = "Finalidade";
            // 
            // lblCA
            // 
            this.lblCA.BackColor = System.Drawing.Color.LightGray;
            this.lblCA.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblCA.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCA.Location = new System.Drawing.Point(11, 30);
            this.lblCA.Name = "lblCA";
            this.lblCA.ReadOnly = true;
            this.lblCA.Size = new System.Drawing.Size(100, 21);
            this.lblCA.TabIndex = 3;
            this.lblCA.TabStop = false;
            this.lblCA.Text = "CA";
            // 
            // text_ca
            // 
            this.text_ca.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.text_ca.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.text_ca.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.text_ca.Location = new System.Drawing.Point(110, 30);
            this.text_ca.MaxLength = 50;
            this.text_ca.Name = "text_ca";
            this.text_ca.Size = new System.Drawing.Size(630, 21);
            this.text_ca.TabIndex = 2;
            // 
            // text_finalidade
            // 
            this.text_finalidade.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.text_finalidade.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.text_finalidade.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.text_finalidade.Location = new System.Drawing.Point(110, 50);
            this.text_finalidade.MaxLength = 150;
            this.text_finalidade.Name = "text_finalidade";
            this.text_finalidade.Size = new System.Drawing.Size(630, 21);
            this.text_finalidade.TabIndex = 3;
            // 
            // text_descricao
            // 
            this.text_descricao.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.text_descricao.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.text_descricao.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.text_descricao.Location = new System.Drawing.Point(110, 10);
            this.text_descricao.MaxLength = 200;
            this.text_descricao.Name = "text_descricao";
            this.text_descricao.Size = new System.Drawing.Size(630, 21);
            this.text_descricao.TabIndex = 1;
            // 
            // frmEpiIncluir
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(784, 561);
            this.Name = "frmEpiIncluir";
            this.Text = "INCLUIR EPI";
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.frmEpiIncluir_KeyDown);
            this.spForm.Panel1.ResumeLayout(false);
            this.spForm.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.spForm)).EndInit();
            this.spForm.ResumeLayout(false);
            this.pnlForm.ResumeLayout(false);
            this.pnlForm.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.banner)).EndInit();
            this.flpAcao.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        protected System.Windows.Forms.Button lbl_limpar;
        protected System.Windows.Forms.Button btn_fechar;
        protected System.Windows.Forms.Button btn_gravar;
        protected System.Windows.Forms.TextBox lblCA;
        protected System.Windows.Forms.TextBox lblFinalidade;
        protected System.Windows.Forms.TextBox lblEpi;
        protected System.Windows.Forms.TextBox text_ca;
        protected System.Windows.Forms.TextBox text_finalidade;
        protected System.Windows.Forms.TextBox text_descricao;
        protected System.Windows.Forms.FlowLayoutPanel flpAcao;
    }
}