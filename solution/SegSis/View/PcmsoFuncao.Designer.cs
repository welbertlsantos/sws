﻿namespace SWS.View
{
    partial class frmPcmsoFuncao
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.flpAcao = new System.Windows.Forms.FlowLayoutPanel();
            this.btIncluir = new System.Windows.Forms.Button();
            this.btBuscar = new System.Windows.Forms.Button();
            this.btNovo = new System.Windows.Forms.Button();
            this.btFechar = new System.Windows.Forms.Button();
            this.lblNomeFuncao = new System.Windows.Forms.TextBox();
            this.textDescricao = new System.Windows.Forms.TextBox();
            this.grb_grid = new System.Windows.Forms.GroupBox();
            this.dgvFuncao = new System.Windows.Forms.DataGridView();
            ((System.ComponentModel.ISupportInitialize)(this.scForm)).BeginInit();
            this.scForm.Panel1.SuspendLayout();
            this.scForm.Panel2.SuspendLayout();
            this.scForm.SuspendLayout();
            this.pnlForm.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.banner)).BeginInit();
            this.flpAcao.SuspendLayout();
            this.grb_grid.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvFuncao)).BeginInit();
            this.SuspendLayout();
            // 
            // scForm
            // 
            // 
            // scForm.Panel1
            // 
            this.scForm.Panel1.Controls.Add(this.flpAcao);
            // 
            // pnlForm
            // 
            this.pnlForm.Controls.Add(this.grb_grid);
            this.pnlForm.Controls.Add(this.lblNomeFuncao);
            this.pnlForm.Controls.Add(this.textDescricao);
            // 
            // flpAcao
            // 
            this.flpAcao.Controls.Add(this.btIncluir);
            this.flpAcao.Controls.Add(this.btBuscar);
            this.flpAcao.Controls.Add(this.btNovo);
            this.flpAcao.Controls.Add(this.btFechar);
            this.flpAcao.Dock = System.Windows.Forms.DockStyle.Fill;
            this.flpAcao.Location = new System.Drawing.Point(0, 0);
            this.flpAcao.Name = "flpAcao";
            this.flpAcao.Size = new System.Drawing.Size(534, 30);
            this.flpAcao.TabIndex = 0;
            // 
            // btIncluir
            // 
            this.btIncluir.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btIncluir.Image = global::SWS.Properties.Resources.Gravar;
            this.btIncluir.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btIncluir.Location = new System.Drawing.Point(3, 3);
            this.btIncluir.Name = "btIncluir";
            this.btIncluir.Size = new System.Drawing.Size(75, 23);
            this.btIncluir.TabIndex = 10;
            this.btIncluir.Text = "&Incluir";
            this.btIncluir.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btIncluir.UseVisualStyleBackColor = true;
            this.btIncluir.Click += new System.EventHandler(this.btIncluir_Click);
            // 
            // btBuscar
            // 
            this.btBuscar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btBuscar.Image = global::SWS.Properties.Resources.busca;
            this.btBuscar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btBuscar.Location = new System.Drawing.Point(84, 3);
            this.btBuscar.Name = "btBuscar";
            this.btBuscar.Size = new System.Drawing.Size(75, 23);
            this.btBuscar.TabIndex = 9;
            this.btBuscar.Text = "&Buscar";
            this.btBuscar.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btBuscar.UseVisualStyleBackColor = true;
            this.btBuscar.Click += new System.EventHandler(this.btBuscar_Click);
            // 
            // btNovo
            // 
            this.btNovo.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btNovo.Image = global::SWS.Properties.Resources.icone_mais;
            this.btNovo.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btNovo.Location = new System.Drawing.Point(165, 3);
            this.btNovo.Name = "btNovo";
            this.btNovo.Size = new System.Drawing.Size(75, 23);
            this.btNovo.TabIndex = 12;
            this.btNovo.Text = "&Novo";
            this.btNovo.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btNovo.UseVisualStyleBackColor = true;
            this.btNovo.Click += new System.EventHandler(this.btNovo_Click);
            // 
            // btFechar
            // 
            this.btFechar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btFechar.Image = global::SWS.Properties.Resources.close;
            this.btFechar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btFechar.Location = new System.Drawing.Point(246, 3);
            this.btFechar.Name = "btFechar";
            this.btFechar.Size = new System.Drawing.Size(75, 23);
            this.btFechar.TabIndex = 11;
            this.btFechar.Text = "&Fechar";
            this.btFechar.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btFechar.UseVisualStyleBackColor = true;
            this.btFechar.Click += new System.EventHandler(this.btFechar_Click);
            // 
            // lblNomeFuncao
            // 
            this.lblNomeFuncao.BackColor = System.Drawing.Color.LightGray;
            this.lblNomeFuncao.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblNomeFuncao.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNomeFuncao.ForeColor = System.Drawing.Color.Black;
            this.lblNomeFuncao.Location = new System.Drawing.Point(3, 14);
            this.lblNomeFuncao.MaxLength = 100;
            this.lblNomeFuncao.Name = "lblNomeFuncao";
            this.lblNomeFuncao.ReadOnly = true;
            this.lblNomeFuncao.Size = new System.Drawing.Size(127, 21);
            this.lblNomeFuncao.TabIndex = 11;
            this.lblNomeFuncao.TabStop = false;
            this.lblNomeFuncao.Text = "Nome da Função";
            // 
            // textDescricao
            // 
            this.textDescricao.BackColor = System.Drawing.Color.White;
            this.textDescricao.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textDescricao.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.textDescricao.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textDescricao.ForeColor = System.Drawing.Color.Black;
            this.textDescricao.Location = new System.Drawing.Point(129, 14);
            this.textDescricao.MaxLength = 100;
            this.textDescricao.Name = "textDescricao";
            this.textDescricao.Size = new System.Drawing.Size(382, 21);
            this.textDescricao.TabIndex = 10;
            // 
            // grb_grid
            // 
            this.grb_grid.Controls.Add(this.dgvFuncao);
            this.grb_grid.Location = new System.Drawing.Point(3, 41);
            this.grb_grid.Name = "grb_grid";
            this.grb_grid.Size = new System.Drawing.Size(508, 233);
            this.grb_grid.TabIndex = 12;
            this.grb_grid.TabStop = false;
            // 
            // dgvFuncao
            // 
            this.dgvFuncao.AllowUserToAddRows = false;
            this.dgvFuncao.AllowUserToDeleteRows = false;
            this.dgvFuncao.AllowUserToOrderColumns = true;
            this.dgvFuncao.AllowUserToResizeColumns = false;
            this.dgvFuncao.AllowUserToResizeRows = false;
            this.dgvFuncao.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.DisplayedCells;
            this.dgvFuncao.BackgroundColor = System.Drawing.Color.White;
            this.dgvFuncao.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvFuncao.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvFuncao.Location = new System.Drawing.Point(3, 16);
            this.dgvFuncao.MultiSelect = false;
            this.dgvFuncao.Name = "dgvFuncao";
            this.dgvFuncao.RowHeadersVisible = false;
            this.dgvFuncao.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvFuncao.Size = new System.Drawing.Size(502, 214);
            this.dgvFuncao.TabIndex = 4;
            this.dgvFuncao.CellFormatting += new System.Windows.Forms.DataGridViewCellFormattingEventHandler(this.dgvFuncao_CellFormatting);
            // 
            // frmPcmsoFuncao
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.ClientSize = new System.Drawing.Size(534, 361);
            this.Name = "frmPcmsoFuncao";
            this.Text = "INCLUIR FUNÇÃO";
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.frmPcmsoFuncao_KeyDown);
            this.scForm.Panel1.ResumeLayout(false);
            this.scForm.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.scForm)).EndInit();
            this.scForm.ResumeLayout(false);
            this.pnlForm.ResumeLayout(false);
            this.pnlForm.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.banner)).EndInit();
            this.flpAcao.ResumeLayout(false);
            this.grb_grid.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvFuncao)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.FlowLayoutPanel flpAcao;
        private System.Windows.Forms.Button btIncluir;
        private System.Windows.Forms.Button btBuscar;
        private System.Windows.Forms.Button btNovo;
        private System.Windows.Forms.Button btFechar;
        private System.Windows.Forms.TextBox lblNomeFuncao;
        private System.Windows.Forms.TextBox textDescricao;
        private System.Windows.Forms.GroupBox grb_grid;
        private System.Windows.Forms.DataGridView dgvFuncao;
    }
}
