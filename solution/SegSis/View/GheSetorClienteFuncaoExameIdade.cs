﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using SWS.Entidade;
using SWS.View.ViewHelper;
using SWS.Excecao;

namespace SWS.View
{
    public partial class frm_GheSetorClienteFuncaoExameIdade : BaseFormConsultaAuxiliar
    {
        static String Msg01 = "O campo Idade Mínima é obrigatório.";

        frm_PcmsoFuncaoGheSelecionar formPcmsoFuncaoGheSelecionar;
        Funcao funcao;

        public frm_GheSetorClienteFuncaoExameIdade(frm_PcmsoFuncaoGheSelecionar formPcmsoFuncaoGheSelecionar, Ghe ghe, Cliente cliente, Funcao funcao, Exame exame)
        {
            InitializeComponent();

            this.text_cliente.Text = cliente.RazaoSocial;
            this.text_exame.Text = exame.Descricao;
            this.text_funcao.Text = funcao.Descricao;
            this.text_ghe.Text = ghe.Descricao;

            this.formPcmsoFuncaoGheSelecionar = formPcmsoFuncaoGheSelecionar;
            this.funcao = funcao;

            this.text_idade_minima.Focus();
        }

        private void btn_fechar_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void text_idade_minima_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = ValidaCampoHelper.FormataCampoNumerico(sender, e);
        }

        private void btn_ok_Click(object sender, EventArgs e)
        {
            try
            {
                if (!String.IsNullOrEmpty(text_idade_minima.Text))
                {
                    formPcmsoFuncaoGheSelecionar.formPcmsoExameIncluir.excecaoIdadeFuncao.Add((Int64)funcao.Id, new Dictionary<Funcao, Int32>() { { funcao, Convert.ToInt32(text_idade_minima.Text) } });
                    this.Close();
                    formPcmsoFuncaoGheSelecionar.Close();
                }
                else
                {
                    MessageBox.Show(Msg01);
                    text_idade_minima.Focus();
                }
            }
            catch (Exception)
            {
                throw new RestricaoFuncaoIdadeJaCadastradaException(RestricaoFuncaoIdadeJaCadastradaException.msg);
            }
        }
    }
}
