﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using SWS.Entidade;
using SWS.Facade;
using SWS.View.Resources;

namespace SWS.View
{
    public partial class frmBancoSelecionar : frmTemplateConsulta
    {
        private Banco banco;

        public Banco Banco
        {
            get { return banco; }
            set { banco = value; }
        }

        private bool? caixa;
        private bool? boleto;

        public frmBancoSelecionar(bool? caixa, bool? boleto)
        {
            InitializeComponent();
            ActiveControl = textNome;
            this.caixa = caixa;
            this.boleto = boleto;
        }

        private void btnConfirmar_Click(object sender, EventArgs e)
        {
            try
            {
                if (dgvBanco.CurrentRow == null)
                    throw new Exception("Selecione uma linha.");

                banco = new Banco((long)dgvBanco.CurrentRow.Cells[0].Value, dgvBanco.CurrentRow.Cells[1].Value.ToString(), dgvBanco.CurrentRow.Cells[2].Value.ToString(), (bool?)Convert.ToBoolean(dgvBanco.CurrentRow.Cells[3].Value), dgvBanco.CurrentRow.Cells[4].Value.ToString(), Convert.ToBoolean(dgvBanco.CurrentRow.Cells[5].Value), (bool?)Convert.ToBoolean(dgvBanco.CurrentRow.Cells[6].Value));
                this.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btnFechar_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnPesquisar_Click(object sender, EventArgs e)
        {
            montaGrid();
            textNome.Focus();

        }

        private void montaGrid()
        {
            try
            {
                FinanceiroFacade financeiroFacade = FinanceiroFacade.getInstance();

                dgvBanco.Columns.Clear();

                DataSet ds = financeiroFacade.findBancoByFilter(new Banco(null, textNome.Text, string.Empty, null, ApplicationConstants.ATIVO, false, null), caixa, boleto);

                dgvBanco.DataSource = ds.Tables["Bancos"].DefaultView;

                dgvBanco.Columns[0].HeaderText = "Id";
                dgvBanco.Columns[0].Visible = false;
                
                dgvBanco.Columns[1].HeaderText = "Nome";

                dgvBanco.Columns[2].HeaderText = "Cod Banco";
                dgvBanco.Columns[2].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
                dgvBanco.Columns[2].HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleCenter;

                dgvBanco.Columns[3].HeaderText = "É Caixa?";

                dgvBanco.Columns[4].HeaderText = "Situação";
                dgvBanco.Columns[4].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
                dgvBanco.Columns[4].HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleCenter;

                dgvBanco.Columns[5].HeaderText = "Privado";
                dgvBanco.Columns[5].Visible = false;

                dgvBanco.Columns[6].HeaderText = "Emite Boleto?";
                dgvBanco.Columns[6].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
                dgvBanco.Columns[6].HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleCenter;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }

        private void dgvBanco_CellMouseDoubleClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            try
            {
                btnConfirmar.PerformClick();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }

        
    }
}
