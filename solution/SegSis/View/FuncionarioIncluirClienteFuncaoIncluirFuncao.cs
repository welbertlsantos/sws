﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using SegSis.Facade;
using SegSis.Entidade;
using SegSis.View.Resources;

namespace SegSis.View
{
    public partial class frm_FuncionarioIncluirClienteFuncaoIncluirFuncao : BaseFormConsulta
    {

        private static String msg1 = "Selecione uma linha.";

        ClienteFuncao clienteFuncao = null;

        Cliente cliente = null;

        Funcao funcao = null;

        public frm_FuncionarioIncluirClienteFuncaoIncluirFuncao(Cliente cliente)
        {
            InitializeComponent();
            this.cliente = cliente;
        }

        public ClienteFuncao getClienteFuncao()
        {
            return this.clienteFuncao;
        }

        private void btn_fechar_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btn_pesquisar_Click(object sender, EventArgs e)
        {
            try
            {
                grd_funcao.Columns.Clear();
                montaDataGrid();
                text_descricao.Focus();

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        public void montaDataGrid()
        {
            try
            {
                this.Cursor = Cursors.WaitCursor;

                PpraFacade ppraFacade = PpraFacade.getInstance();

                funcao = new Funcao();
                funcao.setDescricao(text_descricao.Text.ToUpper());
                funcao.setCodCbo(text_cbo.Text);

                DataSet ds = ppraFacade.findAllFuncaoAtivaNotInClient(funcao, cliente);

                grd_funcao.DataSource = ds.Tables["Funcoes"].DefaultView;

                grd_funcao.Columns[0].HeaderText = "id_funcao";
                grd_funcao.Columns[0].Visible = false;

                grd_funcao.Columns[1].HeaderText = "Descricao";

                grd_funcao.Columns[2].HeaderText = "Código CBO";

                grd_funcao.Columns[3].HeaderText = "Situação";
                grd_funcao.Columns[3].Visible = false;

                grd_funcao.Columns[4].HeaderText = "Comentário";

                grd_funcao.Columns[5].HeaderText = "Selec";
                grd_funcao.Columns[5].Visible = false;

                
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }

        }

        private void btn_ok_Click(object sender, EventArgs e)
        {
            try
            {
                this.Cursor = Cursors.WaitCursor;

                if (grd_funcao.CurrentRow != null)
                {
                    PpraFacade ppraFacade = PpraFacade.getInstance();

                    clienteFuncao = new ClienteFuncao(null, cliente,
                        new Funcao((Int64)grd_funcao.CurrentRow.Cells[0].Value,
                            (String)grd_funcao.CurrentRow.Cells[1].Value,
                            (String)grd_funcao.CurrentRow.Cells[2].Value,
                            (String)grd_funcao.CurrentRow.Cells[3].Value,
                            (String)grd_funcao.CurrentRow.Cells[4].Value), ApplicationConstants.ATIVO,
                        DateTime.Now, null);

                    clienteFuncao = ppraFacade.incluirClienteFuncao(clienteFuncao);
                    this.Close();
                }
                else
                {
                    throw new Exception(msg1);
                }


            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }

        private void btn_novo_Click(object sender, EventArgs e)
        {
            try
            {
                this.Cursor = Cursors.WaitCursor;

                PpraFacade ppraFacade = PpraFacade.getInstance();

                frm_funcao_incluir formFuncaoIncluir = new frm_funcao_incluir();
                formFuncaoIncluir.ShowDialog();

                if (formFuncaoIncluir.getFuncao() != null)
                {
                    funcao = formFuncaoIncluir.getFuncao();

                    clienteFuncao = new ClienteFuncao(null, cliente,
                        funcao, ApplicationConstants.ATIVO, DateTime.Now, null);

                    clienteFuncao = ppraFacade.incluirClienteFuncao(clienteFuncao);
                    this.Close();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }

        }


    }
}
