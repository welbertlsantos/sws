﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using SWS.Entidade;
using SWS.View.ViewHelper;
using SWS.Facade;
using SWS.View.Resources;
using SWS.ViewHelper;

namespace SWS.View
{
    public partial class frmAtendimentoIncluir : SWS.View.frmTemplate
    {

        private Aso atendimento;

        public Aso Atendimento
        {
            get { return atendimento; }
            set { atendimento = value; }
        }

        private Funcionario funcionario;
        private Medico medicoExaminador;
        private ClienteFuncaoFuncionario clienteFuncaoFuncionario;
        private Estudo pcmso;
        private List<GheSetor> gheSetorInPcmso = new List<GheSetor>();
        private List<Ghe> gheInPcmso = new List<Ghe>();
        private HashSet<Agente> riscoInPcmso = new HashSet<Agente>();
        private HashSet<GheFonteAgenteExame> examesInPcmso = new HashSet<GheFonteAgenteExame>();
        private HashSet<GheFonteAgenteExame> examesRepetInPcmso = new HashSet<GheFonteAgenteExame>();
        private List<GheSetorClienteFuncao> gheSetorClienteFuncaoInPcmso = new List<GheSetorClienteFuncao>();
        private List<ClienteFuncaoExame> examesExtras = new List<ClienteFuncaoExame>();
        private MedicoEstudo medicoCoordenador;
        private Cliente prestador;
        private Medico medicoCoordenadorAvulso;

        private List<GheSetorAvulso> ghesAvulsos = new List<GheSetorAvulso>();
        private List<AsoAgenteRisco> riscoAvulso = new List<AsoAgenteRisco>();

        private bool altura = false;
        private bool espaco = false;
        private bool eletricidade = false;
        private bool atendimentoDemissioanal = false;

        private List<Aso> atendimentos = new List<Aso>();

        private CentroCusto centroCusto;
        private DateTime dataVencimentoExame = DateTime.Now;
        
        public frmAtendimentoIncluir()
        {
            InitializeComponent();
            ActiveControl = btFuncionario;
            
            ComboHelper.PeriodicidadeASoIncluir(cbTipoAtendimento);
            
            this.MaximizeBox = true;
            this.WindowState = FormWindowState.Maximized;

            ComboHelper.Boolean(this.cbAltura, false);
            ComboHelper.Boolean(this.cbConfinado, false);
            ComboHelper.Boolean(this.cbPrioridade, false);
            ComboHelper.Boolean(this.cbBloqueio, false);
            ComboHelper.Boolean(this.cbEletricidade, false);

            /* verificando parâmetros para atendimento */

            string atendimentoBloqueado;
            PermissionamentoFacade.mapaConfiguracoes.TryGetValue(SWS.Resources.ConfigurationConstans.GRAVA_ATENDIMENTO_BLOQUEADO, out atendimentoBloqueado);

            if (Convert.ToBoolean(atendimentoBloqueado) == true)
            {
                cbBloqueio.Enabled = false;
                cbBloqueio.SelectedValue = "true";
            }


        }

        private void btn_grava_Click(object sender, EventArgs e)
        {
            try
            {
                /* fazendo verificação de dados */

                if (validaDados())
                {
                    /* montando colecao de exames selecionados */
                
                    List<GheFonteAgenteExameAso> examesSelecionadosPcmso = new List<GheFonteAgenteExameAso>();
                    List<ClienteFuncaoExameASo> examesSeleconadosExtra = new List<ClienteFuncaoExameASo>();
                    List<GheFonteAgenteExameAso> examesDuplicadosInPmcso = new List<GheFonteAgenteExameAso>();

                    foreach (DataGridViewRow dvRow in dgvExamePcmso.Rows)
                        if ((bool)dvRow.Cells[4].Value == true)
                            examesSelecionadosPcmso.Add(new GheFonteAgenteExameAso(null, null, null, new GheFonteAgenteExame((long)dvRow.Cells[0].Value, new Exame(Convert.ToInt64(dvRow.Cells[1].Value), dvRow.Cells[2].Value.ToString()), null, (int)0, ApplicationConstants.ATIVO, (bool)dvRow.Cells[6].Value == true ? true : false, (bool)dvRow.Cells[7].Value == true ? true : false, (bool)dvRow.Cells[11].Value == true ? true : false), Convert.ToDateTime(dvRow.Cells[3].Value), (bool)dvRow.Cells[11].Value == true || (bool)dvRow.Cells[5].Value == true ? true : false, (bool)dvRow.Cells[11].Value == true || (bool)dvRow.Cells[5].Value == true ? true : false, PermissionamentoFacade.usuarioAutenticado.Login, false, DateTime.Now, null, DateTime.Now, PermissionamentoFacade.usuarioAutenticado, false, null, String.Empty, String.Empty, false, (bool)dvRow.Cells[5].Value == true ? true : false, prestador, null, (bool)dvRow.Cells[6].Value == true ? true : false, (bool)dvRow.Cells[7].Value == true ? true : false, null, (bool)dvRow.Cells[9].Value == true ? (DateTime?)null : Convert.ToDateTime(dvRow.Cells[10].Value), (bool)dvRow.Cells[12].Value == true ? true : false, (bool)dvRow.Cells[13].Value == true ? false : true));

                    foreach (DataGridViewRow dvRow in dgvExameAvulso.Rows)
                        if ((bool)dvRow.Cells[4].Value == true)
                            examesSeleconadosExtra.Add(new ClienteFuncaoExameASo(null, null, null, new ClienteFuncaoExame((long)dvRow.Cells[0].Value, clienteFuncaoFuncionario.ClienteFuncao, new Exame(Convert.ToInt64(dvRow.Cells[1].Value), dvRow.Cells[2].Value.ToString()), ApplicationConstants.ATIVO, 0), Convert.ToDateTime(dvRow.Cells[3].Value), (bool)dvRow.Cells[9].Value == true || (bool)dvRow.Cells[5].Value == true ? true : false, (bool)dvRow.Cells[9].Value == true || (bool)dvRow.Cells[5].Value == true ? true : false, PermissionamentoFacade.usuarioAutenticado.Login, false, DateTime.Now, null, DateTime.Now, PermissionamentoFacade.usuarioAutenticado, false, null, String.Empty, String.Empty, (bool)dvRow.Cells[5].Value == true ? true : false, prestador, null, null, (bool)dvRow.Cells[7].Value == true ? (DateTime?)null : Convert.ToDateTime(dvRow.Cells[8].Value), (bool)dvRow.Cells[10].Value == true ? false : true));                                                                         


                    /* verificando se existem exames duplicados entre as coleções */

                    foreach (GheFonteAgenteExameAso gheFonteAgenteExameAso in examesSelecionadosPcmso)
                        foreach (ClienteFuncaoExameASo clienteFuncaoExameASo in examesSeleconadosExtra)
                            if (clienteFuncaoExameASo.ClienteFuncaoExame.Exame.Id == gheFonteAgenteExameAso.GheFonteAgenteExame.Exame.Id)
                                throw new Exception("Você marcou exames repetidos. Verifique a seleção de exames do PCMSO com a seleção de exames extras.");
                    
                    /* montando colecao de exames duplicados para manter lógica do fluxo */
                    foreach (GheFonteAgenteExame gheFonteAgenteExame in examesRepetInPcmso)
                        examesDuplicadosInPmcso.Add(new GheFonteAgenteExameAso(null, null, null, gheFonteAgenteExame, Convert.ToDateTime(this.dataAso.Text), false, false, PermissionamentoFacade.usuarioAutenticado.Login, false, null, null, null, PermissionamentoFacade.usuarioAutenticado, false, null, String.Empty, String.Empty, true, false, prestador, null, false, false, null, null, false, true));

                    /* verificando se foi selecionado apenas 1 ghe. Se for e esse ghe tiver o numero de po e no atendimento o número
                     * da po não foi gravado, essa gravação será automática. */

                    if (gheInPcmso.Count == 1 && !string.IsNullOrEmpty(gheInPcmso[0].NumeroPo) && string.IsNullOrEmpty(textCodigoPO.Text))
                        textCodigoPO.Text = gheInPcmso[0].NumeroPo;

                    string situacaoAso = ((SelectItem)cbBloqueio.SelectedItem).Valor == "true" ? ApplicationConstants.BLOQUEADO : ApplicationConstants.CONSTRUCAO;

                    /* verificando se existe algum exame com data maior do que a data do aso */

                    bool existeExameMaiorAso = true;

                    foreach (GheFonteAgenteExameAso gheFonteAgenteExame in examesSelecionadosPcmso)
                    {
                        if (gheFonteAgenteExame.DataExame > Convert.ToDateTime(dataAso.Text))
                        {
                            existeExameMaiorAso = false;
                            break;
                        }
                    }

                    foreach (ClienteFuncaoExameASo clienteFuncaoExameAso in examesSeleconadosExtra)
                    {
                        if (clienteFuncaoExameAso.DataExame > Convert.ToDateTime(dataAso.Text))
                        {
                            existeExameMaiorAso = false;
                            break;
                        }
                    }

                    if (!existeExameMaiorAso)
                        throw new Exception("Não pode existir exames com data maior do que a data do Aso. Reveja os dados na tela. ");


                    /* verificando se é obrigatório gravar o médico examinador no atendimento */
                    string medicoExaminadorAtendimento;
                    PermissionamentoFacade.mapaConfiguracoes.TryGetValue(SWS.Resources.ConfigurationConstans.MEDICO_EXAMINADOR_ATENDIMENTO, out medicoExaminadorAtendimento);

                    if (Convert.ToBoolean(medicoExaminadorAtendimento) == true && medicoExaminador == null)
                    {
                        textMedicoExaminador.Focus();
                        throw new Exception("A gravação não poderá ocorrer. Foi configurado que todo o atendimento deve ter um médico examinador.");
                    }

                    /* gerando aso e enviado informações para a próxima etapa */
                    atendimento = new Aso(null, medicoExaminador, clienteFuncaoFuncionario, String.Empty, Convert.ToDateTime(dataAso.Text), funcionario, clienteFuncaoFuncionario.ClienteFuncao.Cliente, new Periodicidade(Convert.ToInt64(((SelectItem)cbTipoAtendimento.SelectedItem).Valor), ((SelectItem)cbTipoAtendimento.SelectedItem).Nome.ToString(), null), situacaoAso, medicoCoordenador != null ? medicoCoordenador.Medico :  medicoCoordenadorAvulso != null ? medicoCoordenadorAvulso : null  , PermissionamentoFacade.usuarioAutenticado, String.Empty, pcmso, null, null, null, null, DateTime.Now, textObservacao.Text.Trim(), String.Empty, textSenha.Text, null, PermissionamentoFacade.usuarioAutenticado.Empresa, string.Empty, null, centroCusto, dataVencimento.Checked ? (DateTime?)dataVencimento.Value : null, textCodigoPO.Text, Convert.ToBoolean(((SelectItem)cbPrioridade.SelectedItem).Valor), clienteFuncaoFuncionario.Funcionario.PisPasep, string.Empty, clienteFuncaoFuncionario.ClienteFuncao.Funcao.Descricao);
                    atendimento.GheSetorAvulso = ghesAvulsos;
                    atendimento.AsoAgenteRisco = riscoAvulso;
                    atendimento.Altura = Convert.ToBoolean(((SelectItem)cbAltura.SelectedItem).Valor);
                    atendimento.Confinado = Convert.ToBoolean(((SelectItem)cbConfinado.SelectedItem).Valor);
                    atendimento.Eletricidade = Convert.ToBoolean(((SelectItem)cbEletricidade.SelectedItem).Valor);

                    frmAtendimentoIncluirConfirma confirmaAtendimento = new frmAtendimentoIncluirConfirma(examesSelecionadosPcmso, examesSeleconadosExtra, examesDuplicadosInPmcso, atendimento, gheSetorInPcmso, riscoInPcmso);
                    confirmaAtendimento.ShowDialog();

                    if (confirmaAtendimento.Atendimento.Id != null)
                    {
                        atendimento = confirmaAtendimento.Atendimento;
                        this.Close();
                    }
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }

        }

        private bool validaDados()
        {
            bool retorno = true;

            try
            {
                /* validando o funcionário */
                if (funcionario == null)
                {
                    ActiveControl = btFuncionario;
                    throw new Exception("Selecione um funcionário.");
                }
                /* validando o cliente */
                if (clienteFuncaoFuncionario == null)
                {
                    ActiveControl = btCliente;
                    throw new Exception("Selecione um cliente.");
                }
                /*validando se foi selecionado o tipo de atendimento */
                if (cbTipoAtendimento.SelectedIndex == -1)
                {
                    ActiveControl = cbTipoAtendimento;
                    throw new Exception("Selecione um tipo de atendimento");
                }
                /* validando se foi selecionado algum exame */
                bool check = false;

                foreach (DataGridViewRow dvRow in dgvExamePcmso.Rows)
                {
                    if ((bool)dvRow.Cells[4].Value == true)
                    {
                        check = true;
                        break;
                    }
                }

                foreach (DataGridViewRow dvRow in dgvExameAvulso.Rows)
                {
                    if ((bool)dvRow.Cells[4].Value == true)
                    {
                        check = true;
                        break;
                    }
                }

                if (!check)
                {
                    ActiveControl = dgvExameAvulso;
                    throw new Exception("Selecione pelo menos um exame mostrado pelo PCMSO ou mostrado pelo exame avulso.");
                }

                if (clienteFuncaoFuncionario.ClienteFuncao.Cliente.UsaCentroCusto && centroCusto == null)
                {
                    ActiveControl = btnCentroCusto;
                    throw new Exception("Cliente utiliza centro de custo. Você deve selecionar um centro de custo para avançar. ");
                }
                if (clienteFuncaoFuncionario.ClienteFuncao.Cliente.UsaPo && string.IsNullOrEmpty(textCodigoPO.Text.Trim()))
                {
                    ActiveControl = textCodigoPO;
                    throw new Exception("Cliente marcado para usar Código da PO. Inclua o número da PO no atendimento para prosseguir.");
                }

                string ordenacao;
                PermissionamentoFacade.mapaConfiguracoes.TryGetValue(SWS.Resources.ConfigurationConstans.ORDENACAO_ATENDIMENTO, out ordenacao);

                if (Convert.ToBoolean(ordenacao) && string.IsNullOrEmpty(textSenha.Text))
                {
                    ActiveControl = textSenha;
                    throw new Exception("Campo senha deverá ser preenchido. Parâmetro de ordenação está selecionado.");
                }

            }
            catch (Exception ex)
            {
                retorno = false;
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
                 
            }
            return retorno;
        }

        private void btFechar_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btFuncionario_Click(object sender, EventArgs e)
        {
            try
            {
                /* buscando funcionário */

                limpaDados();

                frmFuncionarioPesquisa pesquisaFuncionario = new frmFuncionarioPesquisa();
                pesquisaFuncionario.ShowDialog();

                if (pesquisaFuncionario.Funcionario != null)
                {
                    funcionario = pesquisaFuncionario.Funcionario;
                    textNome.Text = funcionario.Nome;
                    textCpf.Text = funcionario.Cpf;
                    textRg.Text = funcionario.Rg;
                    textIdade.Text = ValidaCampoHelper.CalculaIdade(funcionario.DataNascimento).ToString();

                    montaGridAtendimentos();
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }

        private void limpaDados()
        {
            funcionario = null;
            textNome.Text = String.Empty;
            textCpf.Text = String.Empty;
            textRg.Text = String.Empty;
            textIdade.Text = String.Empty;
            IncluirAtendimentoError.Clear();
            clienteFuncaoFuncionario = null;
            textCliente.Text = String.Empty;
            textCnpj.Text = String.Empty;
            textFuncao.Text = String.Empty;
            dgvExameAvulso.Columns.Clear();
            pcmso = null;
            textPcmso.Text = string.Empty;
            textMedicoExaminador.Text = string.Empty;
            textdataValidadePcmso.Text = string.Empty;
            dgvAtendimentos.Columns.Clear();
            dgvExamePcmso.Columns.Clear();
            dgvGhe.Columns.Clear();
            dgvRisco.Columns.Clear();
        }

        private void btnMedicoExaminador_Click(object sender, EventArgs e)
        {
            try
            {
                /* Selecionando o médico Examinador */

                frmPcmsoMedico selecionarMedico = new frmPcmsoMedico();
                selecionarMedico.Text = "SELECIONAR MÉDICO EXAMINADOR";
                selecionarMedico.ShowDialog();

                if (selecionarMedico.Medico != null)
                {
                    medicoExaminador = selecionarMedico.Medico;
                    textMedicoExaminador.Text = medicoExaminador.Nome + " CRM: " + medicoExaminador.Crm;
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }

        private void btCliente_Click(object sender, EventArgs e)
        {
            try
            {
                /* incluindo o clienteFuncaoFuncionario no atendimento */
                
                IncluirAtendimentoError.Clear();

                if (funcionario == null) 
                {
                    IncluirAtendimentoError.SetError(btFuncionario, "Selecione primeiro o funcionário.");
                    throw new Exception("Selecione o funcionário."); 
                }

                if (cbTipoAtendimento.SelectedIndex == 0)
                {
                    IncluirAtendimentoError.SetError(cbTipoAtendimento, "Selecione primeiro o tipo de atendimento.");
                    throw new Exception("Selecione o tipo de atendimento");
                }

                frmAtendimentoClienteFuncao selecionarClienteFuncaoFuncionario = new frmAtendimentoClienteFuncao(funcionario);
                selecionarClienteFuncaoFuncionario.ShowDialog();

                if (selecionarClienteFuncaoFuncionario.ClienteFuncaoFuncionario != null)
                {
                    /* verificando se poderá ser realizado o atendimento para o cliente */

                    if (selecionarClienteFuncaoFuncionario.ClienteFuncaoFuncionario.ClienteFuncao.Cliente.Bloqueado)
                        throw new Exception("Atendimento não poderá ser realizado. Cliente bloqueado. Verifique com o financeiro o desbloqueio.");
                    
                    clienteFuncaoFuncionario = selecionarClienteFuncaoFuncionario.ClienteFuncaoFuncionario;
                    textCliente.Text = clienteFuncaoFuncionario.ClienteFuncao.Cliente.RazaoSocial;
                    
                    if (clienteFuncaoFuncionario.ClienteFuncao.Cliente.Cnpj.Length != 11)
                    {
                        textCnpj.Text = ValidaCampoHelper.FormataCnpj(clienteFuncaoFuncionario.ClienteFuncao.Cliente.Cnpj);
                        textCnpj.Mask = "999,999,999/9999-99";
                    }
                    else
                    {
                        textCnpj.Text = ValidaCampoHelper.FormataCpf(clienteFuncaoFuncionario.ClienteFuncao.Cliente.Cnpj);
                        textCnpj.Mask = "999,999,999-99";

                    }

                    textFuncao.Text = clienteFuncaoFuncionario.ClienteFuncao.Funcao.Descricao;
                    montaGridExamesExtras();

                    if (clienteFuncaoFuncionario.ClienteFuncao.Cliente.UsaCentroCusto)
                    {
                        btnCentroCusto.Enabled = true;
                        centroCusto = null;
                        textCentroCusto.Text = string.Empty;
                    }
                    else
                    {
                        btnCentroCusto.Enabled = false;
                        centroCusto = null;
                        textCentroCusto.Text = string.Empty;
                        
                    }
                }


            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }

        private void cbTipoAtendimento_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                IncluirAtendimentoError.Clear();
                /* caso o tipo do aso seja demissional então não terá vencimento o aso e nem os exames */
                if (String.Equals(((SelectItem)cbTipoAtendimento.SelectedItem).Nome, "Demissional"))
                {
                    atendimentoDemissioanal = true;
                    dataVencimento.Checked = !atendimentoDemissioanal;
                    dataVencimento.Enabled = !atendimentoDemissioanal;
                }
                else
                {
                    atendimentoDemissioanal = false;
                    dataVencimento.Checked = !atendimentoDemissioanal;
                    dataVencimento.Enabled = !atendimentoDemissioanal;
                }
                
                if (clienteFuncaoFuncionario != null)
                {
                    montaGridExamesExtras();
                    if (pcmso != null)
                    {
                        montaColecaoExamesGhe(gheSetorClienteFuncaoInPcmso.Exists(x => x.Altura == true), gheSetorClienteFuncaoInPcmso.Exists(x => x.Confinado == true), gheSetorClienteFuncaoInPcmso.Exists(x => x.Eletricidade == true));
                        montaGridExamesPCMSO();
                    }
                }

                calculaVencimentoAtendimento();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }

        public void montaGridExamesExtras()
        {
            try
            {
                AsoFacade asoFacade = AsoFacade.getInstance();
                
                this.dgvExameAvulso.Columns.Clear();

                dgvExameAvulso.ColumnCount = 3;

                dgvExameAvulso.Columns[0].Name = "id_cliente_funcao_exame";
                dgvExameAvulso.Columns[0].Visible = false;

                dgvExameAvulso.Columns[1].Name = "Código Exame";
                dgvExameAvulso.Columns[1].ReadOnly = true;
                dgvExameAvulso.Columns[1].Width = 50;

                dgvExameAvulso.Columns[2].Name = "Exame";
                dgvExameAvulso.Columns[2].ReadOnly = true;
                dgvExameAvulso.Columns[2].Width = 300;

                CalendarColumn dataExame = new CalendarColumn();
                dataExame.HeaderText = "Data do Exame";
                dgvExameAvulso.Columns.Add(dataExame);
                dgvExameAvulso.Columns[3].DefaultCellStyle.BackColor = Color.White;
                dgvExameAvulso.Columns[3].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
                dgvExameAvulso.Columns[3].Width = 100;

                DataGridViewCheckBoxColumn check = new DataGridViewCheckBoxColumn();
                check.Name = "Selec";
                dgvExameAvulso.Columns.Add(check);
                dgvExameAvulso.Columns[4].Width = 40;
                dgvExameAvulso.Columns[4].DefaultCellStyle.BackColor = Color.White;

                DataGridViewCheckBoxColumn transcrito = new DataGridViewCheckBoxColumn();
                transcrito.Name = "Transcrito";
                dgvExameAvulso.Columns.Add(transcrito);
                dgvExameAvulso.Columns[5].Width = 60;
                dgvExameAvulso.Columns[5].DefaultCellStyle.BackColor = Color.White;

                DataGridViewTextBoxColumn mesVencimento = new DataGridViewTextBoxColumn();
                mesVencimento.Name = "Periodo de Vencimento";
                dgvExameAvulso.Columns.Add(mesVencimento);
                dgvExameAvulso.Columns[6].Visible = false;

                DataGridViewCheckBoxColumn comVencimento = new DataGridViewCheckBoxColumn();
                comVencimento.Name = "Sem Vencimento?";
                dgvExameAvulso.Columns.Add(comVencimento);
                dgvExameAvulso.Columns[7].Width = 80;
                dgvExameAvulso.Columns[7].DefaultCellStyle.BackColor = Color.White;
                dgvExameAvulso.Columns[7].ReadOnly = atendimentoDemissioanal ? true : false;

                CalendarColumn dataVencimento = new CalendarColumn();
                dataVencimento.HeaderText = "Data do Vencimento";
                dataExame.MinimumWidth = 100;
                dgvExameAvulso.Columns.Add(dataVencimento);
                dgvExameAvulso.Columns[8].DefaultCellStyle.BackColor = Color.White;
                dgvExameAvulso.Columns[8].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
                dgvExameAvulso.Columns[8].Width = 100;
                dgvExameAvulso.Columns[8].ReadOnly = atendimentoDemissioanal ? true : false;

                DataGridViewCheckBoxColumn naoEnviaFila = new DataGridViewCheckBoxColumn();
                naoEnviaFila.HeaderText = "Não envia atendimento?";
                dgvExameAvulso.Columns.Add(naoEnviaFila);
                dgvExameAvulso.Columns[9].DefaultCellStyle.BackColor = Color.White;
                dgvExameAvulso.Columns[9].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
                dgvExameAvulso.Columns[9].Width = 100;
                dgvExameAvulso.Columns[9].ReadOnly = false;

                DataGridViewCheckBoxColumn naoEnviaAso = new DataGridViewCheckBoxColumn();
                naoEnviaAso.HeaderText = "Não envia ASO?";
                dgvExameAvulso.Columns.Add(naoEnviaAso);
                dgvExameAvulso.Columns[10].DefaultCellStyle.BackColor = Color.White;
                dgvExameAvulso.Columns[10].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
                dgvExameAvulso.Columns[10].Width = 100;
                dgvExameAvulso.Columns[10].ReadOnly = false;
                
                /* reidexando a gride */
                dgvExameAvulso.Columns[4].DisplayIndex = 1;
                dgvExameAvulso.Columns[1].DisplayIndex = 2;
                dgvExameAvulso.Columns[2].DisplayIndex = 3;
                dgvExameAvulso.Columns[3].DisplayIndex = 4;
                dgvExameAvulso.Columns[7].DisplayIndex = 5;
                dgvExameAvulso.Columns[8].DisplayIndex = 6;
                dgvExameAvulso.Columns[5].DisplayIndex = 7;
                dgvExameAvulso.Columns[9].DisplayIndex = 8;
                dgvExameAvulso.Columns[10].DisplayIndex = 9;

                examesExtras = asoFacade.findAllExamesByClienteFuncaoAndIndadeAndPeriodicidade(clienteFuncaoFuncionario.ClienteFuncao, ValidaCampoHelper.CalculaIdade(funcionario.DataNascimento), new Periodicidade(Convert.ToInt64(((SelectItem)cbTipoAtendimento.SelectedItem).Valor), ((SelectItem)cbTipoAtendimento.SelectedItem).Nome.ToString(), null));
                List<ClienteFuncaoExamePeriodicidade> periodicidadeByExameExtra = asoFacade.listAllClienteFuncaoExamePeriodicidadeByClienteFuncaoByPeriodicidade(clienteFuncaoFuncionario.ClienteFuncao, new Periodicidade(Convert.ToInt64(((SelectItem)cbTipoAtendimento.SelectedItem).Valor), ((SelectItem)cbTipoAtendimento.SelectedItem).Nome, null));
                
                DateTime dataVencimentoPadrao = dataVencimentoExame;
                foreach (ClienteFuncaoExame clienteFuncaoExame in examesExtras)
                {
                    int? periodoVencimentoMes = null;
                    dataVencimentoExame = dataVencimentoPadrao;
                    periodoVencimentoMes = periodicidadeByExameExtra.Find(x => x.ClienteFuncaoExame.Exame.Id == clienteFuncaoExame.Exame.Id).PeriodoVencimento;

                    if (periodoVencimentoMes != null)
                        dataVencimentoExame = this.dataAso.Value.AddMonths((int)periodoVencimentoMes);

                    this.dgvExameAvulso.Rows.Add(clienteFuncaoExame.Id, Convert.ToString(clienteFuncaoExame.Exame.Id).PadLeft(3, '0'), clienteFuncaoExame.Exame.Descricao, Convert.ToDateTime(dataAso.Text), false, false, periodoVencimentoMes, atendimentoDemissioanal, dataVencimentoExame, false, false);
                }
                
                dgvExameAvulso.Sort(dgvExameAvulso.Columns[2], ListSortDirection.Ascending);

                recalculaVencimentoAtendimento(dgvExameAvulso);

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }

        private void btPcmso_Click(object sender, EventArgs e)
        {
            try
            {
                if (clienteFuncaoFuncionario == null)
                {
                    IncluirAtendimentoError.SetError(btCliente, "Selecione primeiro o cliente.");
                    throw new Exception("Selecione o cliente.");
                }

                frmAtendimentoPcmso selecionarPcmso = new frmAtendimentoPcmso(clienteFuncaoFuncionario);
                selecionarPcmso.ShowDialog();

                EstudoFacade pcmsoFacade = EstudoFacade.getInstance();

                if (selecionarPcmso.Pcmso != null)
                {
                    pcmso = selecionarPcmso.Pcmso;
                    textPcmso.Text = pcmso.CodigoEstudo;
                    /* verificando status da validade do pcsmo */

                    if (pcmso.DataVencimento != null && pcmso.DataVencimento < DateTime.Now)
                        textdataValidadePcmso.ForeColor = Color.Red;

                    textdataValidadePcmso.Text = Convert.ToString(pcmso.DataVencimento);

                    /* recuperando informação do coordenador do pcmso */                                                                                                            
                    medicoCoordenador = pcmso.Medicos.Find (x => String.Equals(x.Situacao, ApplicationConstants.ATIVO));

                    if (medicoCoordenador != null)
                        textCoordenador.Text = medicoCoordenador.Medico.Nome + " CRM: " + medicoCoordenador.Medico.Crm;

                    /* Recuperando todos os Ghe dado um cliente Funcao */
                    gheInPcmso = pcmsoFacade.findAllGheInPcmsoByClienteFuncao(clienteFuncaoFuncionario.ClienteFuncao, pcmso);

                    /* excluíndo informações das grides */
                    dgvExamePcmso.Columns.Clear();
                    dgvExameAvulso.Columns.Clear();
                    dgvRisco.Columns.Clear();


                    /* recuperando todos os GheSetor dado um lista de Ghes */
                    buscaGheSetor();

                    /* montando informação do GheSetor */
                    montaGridgheSetor();

                    if (gheInPcmso.Count < 2)
                    {
                        /* recuperando coleção de riscos do pcmso */
                        buscaRisco();

                        /* montando grid de risco */
                        montaGridRisco();

                        /* montando colecao de gheSetorClienteFuncao */
                        buscaGheSetorClienteFuncao();

                        /* verificando se existe informação de trabalho em altura e / ou espaço confinado.  e / ou eletricidade */
                        altura = gheSetorClienteFuncaoInPcmso.Exists(x => x.Altura == true);
                        espaco = gheSetorClienteFuncaoInPcmso.Exists(x => x.Confinado == true);
                        eletricidade = gheSetorClienteFuncaoInPcmso.Exists(x => x.Eletricidade == true);

                        if (altura || espaco || eletricidade)
                        {
                            MessageBox.Show("A função escolhida tem caracteristicas especiais (altura, espaço confinado e ou eletricidade). Se o colaborador exerce esse tipo de atividade marque a seleção como sim.", "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Question);
                            
                            frmAtendimentoAvalicaoNRs validaCaoCaracteristica = new frmAtendimentoAvalicaoNRs(altura, espaco, eletricidade);
                            validaCaoCaracteristica.ShowDialog();

                            cbAltura.SelectedValue = validaCaoCaracteristica.Altura ? "true" : "false";
                            cbConfinado.SelectedValue = validaCaoCaracteristica.Confinado ? "true" : "false";
                            cbEletricidade.SelectedValue = validaCaoCaracteristica.Eletricidade ? "true" : "false";

                            cbAltura.Enabled = altura;
                            cbConfinado.Enabled = espaco;
                            cbEletricidade.Enabled = eletricidade;
                        }
                        
                        /* recuperando coleção de exames do pcmso */
                        montaColecaoExamesGhe(altura, espaco, eletricidade);
                        /* montando informações de exames trazidos pelo pcmso */
                        montaGridExamesPCMSO();

                        /* desativando recursos de medico, ghe e risco avulso */
                        btnIncluirMedicoCoordenador.Enabled = false;
                        ghesAvulsos.Clear();
                        riscoAvulso.Clear();
                        flpGheSetor.Enabled = true;
                        flpRisco.Enabled = false;
                    }
                    else
                        /* emitindo aviso ao usuário */
                        MessageBox.Show("Essa função foi encontrada em mais de um GHE. Marque um ou mais GHEs onde o colaborador se encontre para buscar os riscos e exames. ", "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
                                        

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }

        private void buscaGheSetorClienteFuncao()
        {
            try
            {

                EstudoFacade pcmsoFacade = EstudoFacade.getInstance();
                gheSetorClienteFuncaoInPcmso.Clear();

                foreach (GheSetor gheSetor in gheSetorInPcmso)
                {
                    gheSetorClienteFuncaoInPcmso.AddRange(pcmsoFacade.findGheSetorClienteFuncaoByGheSetorAndClienteFuncao(gheSetor, clienteFuncaoFuncionario.ClienteFuncao));
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);

            }
        }

        private void buscaGheSetor()
        {
            try
            {
                gheSetorInPcmso.Clear();

                EstudoFacade pcmsoFacade = EstudoFacade.getInstance();

                HashSet<GheSetor> gheSetorHash = new HashSet<GheSetor>();

                foreach (Ghe ghe in gheInPcmso)
                {
                    gheSetorHash = pcmsoFacade.findAllGheSetor(ghe);

                    foreach (GheSetor gheSetor in gheSetorHash)
                        gheSetorInPcmso.Add(gheSetor);
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void chk_examesExtras_CheckedChanged(object sender, EventArgs e)
        {
            if (chkExamesAvulso.Checked)
                foreach (DataGridViewRow row in dgvExameAvulso.Rows)
                    row.Cells[4].Value = true;
                        
            else
                foreach (DataGridViewRow row in dgvExameAvulso.Rows)
                    row.Cells[4].Value = false;
        }

        public void montaGridgheSetor()
        {
            try
            {
                dgvGhe.Columns.Clear();
                dgvGhe.ColumnCount = 6;
                dgvGhe.MultiSelect = false;
                dgvGhe.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.None;
                dgvGhe.SelectionMode = DataGridViewSelectionMode.FullRowSelect;

                bool marcaGheSetor = false;

                dgvGhe.Columns[0].HeaderText = "idGheSetor";
                dgvGhe.Columns[0].Visible = false;

                dgvGhe.Columns[1].HeaderText = "id_Ghe";
                dgvGhe.Columns[1].Visible = false;

                dgvGhe.Columns[2].HeaderText = "Ghe";
                dgvGhe.Columns[2].ReadOnly = true;

                dgvGhe.Columns[3].HeaderText = "idSetor";
                dgvGhe.Columns[3].Visible = false;

                dgvGhe.Columns[4].HeaderText = "Setor";
                dgvGhe.Columns[4].ReadOnly = true;

                dgvGhe.Columns[5].HeaderText = "Número PO";
                dgvGhe.Columns[5].ReadOnly = true;

                DataGridViewCheckBoxColumn gheAvulso = new DataGridViewCheckBoxColumn();
                gheAvulso.Name = "Ghe Avulso?";
                dgvGhe.Columns.Add(gheAvulso);
                dgvGhe.Columns[6].ReadOnly = true;

                DataGridViewCheckBoxColumn check = new DataGridViewCheckBoxColumn();
                check.Name = "Selec";
                dgvGhe.Columns.Add(check);
                dgvGhe.Columns[7].DisplayIndex = 0;

                if (gheSetorInPcmso.Count < 2)
                    marcaGheSetor = true;
                
                EstudoFacade pcmsoFacade = EstudoFacade.getInstance();

                foreach (GheSetor gheSetor in gheSetorInPcmso)
                    dgvGhe.Rows.Add(gheSetor.Id, gheSetor.Ghe.Id, gheSetor.Ghe.Descricao, gheSetor.Setor.Id, gheSetor.Setor.Descricao, gheSetor.Ghe.NumeroPo, false, marcaGheSetor);

                /* incluindo gheSetorAvulso */

                foreach(GheSetorAvulso gheSetorAvulso in ghesAvulsos)
                    dgvGhe.Rows.Add(null, null, gheSetorAvulso.Ghe, null, gheSetorAvulso.Setor, string.Empty, true, true);

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        public void buscaRisco()
        {
            try
            {
                EstudoFacade pcmsoFacade = EstudoFacade.getInstance();

                HashSet<GheFonte> gheFontes = new HashSet<GheFonte>();
                HashSet<GheFonteAgente> gheFonteAgentes = new HashSet<GheFonteAgente>();

                gheFontes.Clear();
                gheFonteAgentes.Clear();
                riscoInPcmso.Clear();

                foreach (Ghe ghe in gheInPcmso)
                    gheFontes.UnionWith(pcmsoFacade.findAllFonteByGhe(ghe));

                foreach (GheFonte gf in gheFontes)
                    gheFonteAgentes.UnionWith(pcmsoFacade.findAllGheFonteAgenteByGheFonte(gf));

                foreach (GheFonteAgente gfa in gheFonteAgentes)
                    riscoInPcmso.Add(gfa.Agente);

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK , MessageBoxIcon.Error);
            }
        }

        public Dictionary<Exame, GheFonteAgenteExame> checkRepeat(HashSet<GheFonteAgenteExame> gheFonteAgenteExame)
        {
            Dictionary<Exame, GheFonteAgenteExame> examesNaoRepetido = new Dictionary<Exame, GheFonteAgenteExame>();

            foreach (GheFonteAgenteExame gfae in gheFonteAgenteExame)
            {
                if (!examesNaoRepetido.ContainsKey(gfae.Exame))
                    examesNaoRepetido.Add(gfae.Exame, gfae);
                else
                    examesRepetInPcmso.Add(gfae);
            }

            return examesNaoRepetido;
        }

        public void montaColecaoExamesGhe(Boolean? altura, Boolean? confinado, bool? eletricidade)
        {
            try
            {
                EstudoFacade pcmsoFacade = EstudoFacade.getInstance();
                AsoFacade AsoFacade = AsoFacade.getInstance();

                examesInPcmso.Clear();

                if (pcmso != null)
                {
                    examesInPcmso = AsoFacade.findAllExamesByPcmsoByASO(pcmso, Convert.ToInt32(textIdade.Text),
                        new Periodicidade(Convert.ToInt64(((SelectItem)cbTipoAtendimento.SelectedItem).Valor), Convert.ToString(((SelectItem)cbTipoAtendimento.SelectedItem).Nome), null), gheInPcmso, altura, confinado, eletricidade);

                    /* Buscando excecao de exame pela função do funcionario e idade. Se a coleção */
                    foreach (Ghe ghe in gheInPcmso)
                    {
                        GheFonteAgenteExame gheFonteAgenteExameExcecao = null;

                        foreach (GheSetorClienteFuncaoExame gheSetorClienteFuncaoExame in pcmsoFacade.findGheSetorClienteFuncaoExameByGheAndFuncao(ghe, clienteFuncaoFuncionario.ClienteFuncao.Funcao))
                        {
                            gheFonteAgenteExameExcecao = pcmsoFacade.findGheFonteAgenteExameById((Int64)gheSetorClienteFuncaoExame.GheFonteAgenteExame.Id);
                            gheFonteAgenteExameExcecao.Exame = pcmsoFacade.findExameById((Int64)gheFonteAgenteExameExcecao.Exame.Id);

                            if (gheSetorClienteFuncaoExame.Idade <= Convert.ToInt32(textIdade.Text))
                                examesInPcmso.Add(gheFonteAgenteExameExcecao);
                            else
                                examesInPcmso.Remove(gheFonteAgenteExameExcecao);
                        }
                    }

                    if (examesInPcmso.Count > 0)
                    {
                        Dictionary<Exame, GheFonteAgenteExame> examesRepetidos = checkRepeat(examesInPcmso);

                        if (examesRepetidos.Count > 0)
                        {
                            examesInPcmso.Clear();

                            foreach (KeyValuePair<Exame, GheFonteAgenteExame> pairExames in examesRepetidos)
                                examesInPcmso.Add((GheFonteAgenteExame)pairExames.Value);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message,"Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void frmAtendimentoIncluir_KeyDown(object sender, KeyEventArgs e)
        {
            if ((e.KeyCode == Keys.Enter) || (e.KeyCode == Keys.Return))
            {
                SendKeys.Send("{TAB}");
            }
        }

        public void montaGridExamesPCMSO()
        {
            try
            {
                this.dgvExamePcmso.Columns.Clear();

                dgvExamePcmso.ColumnCount = 3;

                dgvExamePcmso.Columns[0].Name = "id_ghe_Fonte_Agente_Exame";
                dgvExamePcmso.Columns[0].Visible = false;

                dgvExamePcmso.Columns[1].Name = "Código Exame";
                dgvExamePcmso.Columns[1].ReadOnly = true;
                dgvExamePcmso.Columns[1].Width = 50;

                dgvExamePcmso.Columns[2].Name = "Exame";
                dgvExamePcmso.Columns[2].ReadOnly = true;
                dgvExamePcmso.Columns[2].Width = 300;

                CalendarColumn dataExame = new CalendarColumn();
                dataExame.HeaderText = "Data do Exame";
                dgvExamePcmso.Columns.Add(dataExame);
                dgvExamePcmso.Columns[3].DefaultCellStyle.BackColor = Color.White;
                dgvExamePcmso.Columns[3].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
                dgvExamePcmso.Columns[3].Width = 100;

                DataGridViewCheckBoxColumn check = new DataGridViewCheckBoxColumn();
                check.Name = "Selec";
                dgvExamePcmso.Columns.Add(check);
                dgvExamePcmso.Columns[4].Width = 40;
                dgvExamePcmso.Columns[4].DefaultCellStyle.BackColor = Color.White;

                DataGridViewCheckBoxColumn transcrito = new DataGridViewCheckBoxColumn();
                transcrito.Name = "Transcrito";
                dgvExamePcmso.Columns.Add(transcrito);
                dgvExamePcmso.Columns[5].Width = 60;
                dgvExamePcmso.Columns[5].DefaultCellStyle.BackColor = Color.White;
                
                DataGridViewCheckBoxColumn confinado = new DataGridViewCheckBoxColumn();
                confinado.Name = "Confinado";
                dgvExamePcmso.Columns.Add(confinado);
                dgvExamePcmso.Columns[6].ReadOnly = true;
                dgvExamePcmso.Columns[6].Width = 60;

                DataGridViewCheckBoxColumn altura = new DataGridViewCheckBoxColumn();
                altura.Name = "Altura";
                dgvExamePcmso.Columns.Add(altura);
                dgvExamePcmso.Columns[7].ReadOnly = true;
                dgvExamePcmso.Columns[7].Width = 60;

                DataGridViewTextBoxColumn mesVencimento = new DataGridViewTextBoxColumn();
                mesVencimento.Name = "Periodo de Vencimento";
                dgvExamePcmso.Columns.Add(mesVencimento);
                dgvExamePcmso.Columns[8].Visible = false;

                DataGridViewCheckBoxColumn comVencimento = new DataGridViewCheckBoxColumn();
                comVencimento.Name = "Sem vencimento?";
                dgvExamePcmso.Columns.Add(comVencimento);
                dgvExamePcmso.Columns[9].Width = 80;
                dgvExamePcmso.Columns[9].DefaultCellStyle.BackColor = Color.White;
                dgvExamePcmso.Columns[9].ReadOnly = atendimentoDemissioanal ? true : false;

                CalendarColumn dataVencimento = new CalendarColumn();
                dataVencimento.HeaderText = "Data de Vencimento";
                dgvExamePcmso.Columns.Add(dataVencimento);
                dgvExamePcmso.Columns[10].DefaultCellStyle.BackColor = Color.White;
                dgvExamePcmso.Columns[10].ReadOnly = atendimentoDemissioanal ? true : false;

                DataGridViewCheckBoxColumn naoEnviaAtendimentoPcmso = new DataGridViewCheckBoxColumn();
                naoEnviaAtendimentoPcmso.Name = "Não enviar atendimento?";
                dgvExamePcmso.Columns.Add(naoEnviaAtendimentoPcmso);
                dgvExamePcmso.Columns[11].DefaultCellStyle.BackColor = Color.White;
                dgvExamePcmso.Columns[11].ReadOnly = false;
                dgvExamePcmso.Columns[11].Width = 100;
                dgvExamePcmso.Columns[11].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;

                DataGridViewCheckBoxColumn eletricidade = new DataGridViewCheckBoxColumn();
                eletricidade.Name = "Eletricidade";
                dgvExamePcmso.Columns.Add(eletricidade);
                dgvExamePcmso.Columns[12].ReadOnly = true;
                dgvExamePcmso.Columns[12].Width = 60;

                DataGridViewCheckBoxColumn naoEnviaASO = new DataGridViewCheckBoxColumn();
                naoEnviaASO.Name = "Não enviar ASO";
                dgvExamePcmso.Columns.Add(naoEnviaASO);
                dgvExamePcmso.Columns[13].DefaultCellStyle.BackColor = Color.White;
                dgvExamePcmso.Columns[13].ReadOnly = false;
                dgvExamePcmso.Columns[13].Width = 100;
                dgvExamePcmso.Columns[13].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;

                                
                /* ordenando índices */

                dgvExamePcmso.Columns[4].DisplayIndex = 1;
                dgvExamePcmso.Columns[1].DisplayIndex = 2;
                dgvExamePcmso.Columns[2].DisplayIndex = 3;
                dgvExamePcmso.Columns[3].DisplayIndex = 4;
                dgvExamePcmso.Columns[9].DisplayIndex = 5;
                dgvExamePcmso.Columns[10].DisplayIndex = 6;
                dgvExamePcmso.Columns[5].DisplayIndex = 7;
                dgvExamePcmso.Columns[6].DisplayIndex = 8;
                dgvExamePcmso.Columns[7].DisplayIndex = 9;
                dgvExamePcmso.Columns[12].DisplayIndex = 10;
                dgvExamePcmso.Columns[11].DisplayIndex = 11;
                dgvExamePcmso.Columns[13].DisplayIndex = 12;


                AsoFacade asoFacade = AsoFacade.getInstance();

                List<GheFonteAgenteExamePeriodicidade> periodoVencimentoExamePcmso = asoFacade.listAllGheFonteAgenteExamePeriodicidadeByGheByPeriodicidade(gheInPcmso, new Periodicidade(Convert.ToInt64(((SelectItem)cbTipoAtendimento.SelectedItem).Valor), Convert.ToString(((SelectItem)cbTipoAtendimento.SelectedItem).Nome), null));

                foreach (GheFonteAgenteExame gheFonteAgenteExame in this.examesInPcmso)
                {
                    int? periodoVencimentoMes = null;
                    periodoVencimentoMes = periodoVencimentoExamePcmso.Find(x => x.GheFonteAgenteExame.Exame.Id == gheFonteAgenteExame.Exame.Id).PeriodoVencimento;
                    
                    if (periodoVencimentoMes != null)
                        dataVencimentoExame = this.dataAso.Value.AddMonths((int)periodoVencimentoMes);

                    this.dgvExamePcmso.Rows.Add(gheFonteAgenteExame.Id, Convert.ToString(gheFonteAgenteExame.Exame.Id).PadLeft(3, '0'), gheFonteAgenteExame.Exame.Descricao, Convert.ToDateTime(dataAso.Text), true, false, gheFonteAgenteExame.Confinado, gheFonteAgenteExame.Altura, periodoVencimentoMes, atendimentoDemissioanal, dataVencimentoExame, false, gheFonteAgenteExame.Eletricidade, false);
                }

                /* ordenando o dataGrid pela descricao do exame */

                dgvExamePcmso.Sort(dgvExamePcmso.Columns[2], ListSortDirection.Ascending);

                recalculaVencimentoAtendimento(dgvExamePcmso);

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }

        public void marcandoChekboxGrid(DataGridView datagridView, Boolean status)
        {
            try
            {
                foreach (DataGridViewRow dvrom in datagridView.Rows)
                    dvrom.Cells[4].Value = status;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }

        public void montaGridRisco()
        {
            try
            {
                dgvRisco.Columns.Clear();

                dgvRisco.ColumnCount = 2;

                dgvRisco.Columns[0].HeaderText = "Agente";
                dgvRisco.Columns[0].ReadOnly = true;

                dgvRisco.Columns[1].HeaderText = "Risco";
                dgvRisco.Columns[1].ReadOnly = true;

                DataGridViewCheckBoxColumn check = new DataGridViewCheckBoxColumn();
                check.Name = "Selec";
                dgvRisco.Columns.Add(check);
                dgvRisco.Columns[2].DisplayIndex = 0;

                foreach (Agente agente in this.riscoInPcmso)
                    dgvRisco.Rows.Add(agente.Descricao, agente.Risco.Descricao, true);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void dgvGhe_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                DataGridView dgv = sender as DataGridView;

                if (e.ColumnIndex == 7 && (bool)dgv.Rows[e.RowIndex].Cells[6].Value == false)
                {
                    /* remontando a colecao de GHes e GHesSetores */
                    gheInPcmso.Clear();
                    gheSetorInPcmso.Clear();
                    dgvGhe.EndEdit();
                    foreach (DataGridViewRow dvRow in dgvGhe.Rows)
                    {
                        if ((bool)dvRow.Cells[7].Value == true && (bool)dvRow.Cells[6].Value == false )
                        {
                            gheInPcmso.Add(new Ghe((Int64)dvRow.Cells[1].Value, null, dvRow.Cells[2].Value.ToString(), 0, string.Empty, dvRow.Cells[5].Value.ToString()));
                            gheSetorInPcmso.Add(new GheSetor((Int64)dvRow.Cells[0].Value, new Setor((long)dvRow.Cells[3].Value, dvRow.Cells[4].Value.ToString(), String.Empty), new Ghe((Int64)dvRow.Cells[1].Value, null, dvRow.Cells[2].Value.ToString(), 0, string.Empty, dvRow.Cells[5].Value.ToString()), ApplicationConstants.ATIVO));                               
                        }
                    }

                    /* o sistema estava indo buscar o gheSetor novamente conforme o que o usuário selecionou e com isso
                     * ele não está levando em conta o gheSetor selecionado pelo usuário */
                    //buscaGheSetor();
                    buscaRisco();
                    montaGridRisco();
                    buscaGheSetorClienteFuncao();
                    montaColecaoExamesGhe(gheSetorClienteFuncaoInPcmso.Exists(x => x.Altura == true), gheSetorClienteFuncaoInPcmso.Exists(x => x.Confinado == true), gheSetorClienteFuncaoInPcmso.Exists(x => x.Eletricidade == true));
                    montaGridExamesPCMSO();
                    
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            
        }

        private void btn_incluirExames_Click(object sender, EventArgs e)
        {
            try
            {
                if (clienteFuncaoFuncionario == null)
                    throw new Exception("Selecione primeiro um cliente e depois um funcionário.");

                frmExamesExtras incluirExames = new frmExamesExtras(clienteFuncaoFuncionario.ClienteFuncao);
                incluirExames.ShowDialog();
                montaGridExamesExtras();

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btPrestador_Click(object sender, EventArgs e)
        {
            try
            {
                frmClienteSelecionar clientePrestador = new frmClienteSelecionar(true, null, false, null, null, null, null, false, true);
                clientePrestador.ShowDialog();

                if (clientePrestador.Cliente != null)
                {
                    prestador = clientePrestador.Cliente;
                    textPrestador.Text = prestador.RazaoSocial;
                    textCnpjPrestador.Text = ValidaCampoHelper.FormataCnpj(prestador.Cnpj);
                }
                else
                {
                    prestador = null;
                    textPrestador.Text = String.Empty;
                    textCnpjPrestador.Text = String.Empty;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void cbConfinado_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                if ((SelectItem)cbConfinado.SelectedItem != null && (SelectItem)cbAltura.SelectedItem != null && (SelectItem)cbEletricidade.SelectedItem != null)
                {
                    montaColecaoExamesGhe(Convert.ToBoolean(((SelectItem)cbAltura.SelectedItem).Valor), Convert.ToBoolean(((SelectItem)cbConfinado.SelectedItem).Valor), Convert.ToBoolean(((SelectItem)cbEletricidade.SelectedItem).Valor));
                    /* montando informações de exames trazidos pelo pcmso */
                    montaGridExamesPCMSO();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

                 
        }

        private void cbAltura_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                if ((SelectItem)cbConfinado.SelectedItem != null && (SelectItem)cbAltura.SelectedItem != null && (SelectItem)cbEletricidade.SelectedItem != null)
                {
                    montaColecaoExamesGhe(Convert.ToBoolean(((SelectItem)cbAltura.SelectedItem).Valor), Convert.ToBoolean(((SelectItem)cbConfinado.SelectedItem).Valor), Convert.ToBoolean(((SelectItem)cbEletricidade.SelectedItem).Valor));
                    /* montando informações de exames trazidos pelo pcmso */
                    montaGridExamesPCMSO();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        public void montaGridAtendimentos()
        {
            try
            {
                dgvAtendimentos.Columns.Clear();
                
                dgvAtendimentos.ColumnCount = 15;

                dgvAtendimentos.Columns[0].HeaderText = "id_aso";
                dgvAtendimentos.Columns[0].Visible = false;

                dgvAtendimentos.Columns[1].HeaderText = "Código";
                dgvAtendimentos.Columns[2].HeaderText = "Médico Examinador";
                dgvAtendimentos.Columns[3].HeaderText = "Data Atendimento";
                dgvAtendimentos.Columns[4].HeaderText = "Cliente";
                dgvAtendimentos.Columns[5].HeaderText = "CNPJ";
                dgvAtendimentos.Columns[6].HeaderText = "Tipo de Atendimento";
                dgvAtendimentos.Columns[7].HeaderText = "Status";
                dgvAtendimentos.Columns[8].HeaderText = "Médico Coordenador";
                dgvAtendimentos.Columns[9].HeaderText = "Data de Gravaçao";
                dgvAtendimentos.Columns[10].HeaderText = "Criado por";
                dgvAtendimentos.Columns[11].HeaderText = "Data de Finalização";
                dgvAtendimentos.Columns[12].HeaderText = "Alterado por";
                dgvAtendimentos.Columns[13].HeaderText = "Finalizado por";
                dgvAtendimentos.Columns[14].HeaderText = "Código Protocolo";

                AsoFacade asoFacade = AsoFacade.getInstance();
                atendimentos = asoFacade.findAllAtendimentoNotCanceladoByFuncionario(funcionario);
                List<int> numeros = new List<int>(); numeros.Add(1); numeros.Add(2); numeros.Add(3);

                atendimentos.ForEach(delegate(Aso atendimento)
                {
                    dgvAtendimentos.Rows.Add(atendimento.Id, ValidaCampoHelper.RetornaCodigoAtendimentoFormatado(atendimento.Codigo), atendimento.Examinador != null ? atendimento.Examinador.Nome : string.Empty, string.Format("{0:dd/MM/yyyy}", atendimento.DataElaboracao), atendimento.Cliente.RazaoSocial, !atendimento.ClienteFuncaoFuncionario.ClienteFuncao.Cliente.Fisica ? ValidaCampoHelper.FormataCnpj(atendimento.Cliente.Cnpj) : ValidaCampoHelper.FormataCpf(atendimento.Cliente.Cnpj), numeros.Contains((int)atendimento.Periodicidade.Id) ? "PERIÓDICO" : atendimento.Periodicidade.Descricao, string.Equals(ApplicationConstants.CONSTRUCAO, atendimento.Situacao) ? "EM ABERTO" : string.Equals(ApplicationConstants.FECHADO, atendimento.Situacao) ? "FINALIZADO" : string.Empty, atendimento.Coordenador != null ? atendimento.Coordenador.Nome : string.Empty , string.Format("{0:dd/MM/yyyy}", atendimento.DataGravacao), atendimento.Criador.Login, atendimento.DataFinalizacao != (DateTime?)null ? string.Format("{0:dd/MM/yyyy}", atendimento.DataFinalizacao) : string.Empty, atendimento.UsuarioAlteracao != null ? atendimento.UsuarioAlteracao.Login : string.Empty, atendimento.Finalizador != null ? atendimento.Finalizador.Login : string.Empty, atendimento.Protocolo != null ? atendimento.Protocolo.Numero : string.Empty);

                });

                if (atendimentos.Count == 0)
                    dgvAtendimentos.Columns.Clear();
                

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void dgvAtendimentos_CellFormatting(object sender, DataGridViewCellFormattingEventArgs e)
        {
            DataGridViewCell cell = this.dgvAtendimentos.Rows[e.RowIndex].Cells[e.ColumnIndex];
            cell.ToolTipText = "Duplo clique na linha para visualizar os exames no atendimento";
            
        }

        private void dgvAtendimentos_CellContentDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                if (dgvAtendimentos.CurrentRow == null)
                    throw new Exception("Selecione uma linha");

                Aso atendimento = atendimentos.Find(x => x.Id == (long)dgvAtendimentos.CurrentRow.Cells[0].Value);

                AsoFacade asoFacade = AsoFacade.getInstance();

                /* localizando os exames do pcmcso do atendimento */
                List<GheFonteAgenteExameAso> ghefonteAgenteExameASos = asoFacade.findAllExamePcmsoByAsoInSituacao(atendimento, true, true, false, null, null);
                /* localizando os exames extras do atendimento */
                List<ClienteFuncaoExameASo> clienteFuncaoExameAsos = asoFacade.findAllExameExtraByAsoInSituacao(atendimento, true, true, false, null, null);

                frmAtendimentoIncluirExame mostrarExames = new frmAtendimentoIncluirExame(ghefonteAgenteExameASos, clienteFuncaoExameAsos);
                mostrarExames.ShowDialog();

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btnCentroCusto_Click(object sender, EventArgs e)
        {
            try
            {
                frmClienteCentroCustoSelecionar selecionarCentroCusto = new frmClienteCentroCustoSelecionar(clienteFuncaoFuncionario.ClienteFuncao.Cliente);
                selecionarCentroCusto.ShowDialog();

                if (selecionarCentroCusto.CentroCustoSelecionado != null)
                {
                    centroCusto = selecionarCentroCusto.CentroCustoSelecionado;
                    textCentroCusto.Text = centroCusto.Descricao;
                }
                
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btnAplicarDataTranscrito_Click(object sender, EventArgs e)
        {
            try
            {
                if (dgvExamePcmso.RowCount > 0)
                {
                    frmAtendimentoIncluirDataTranscrito AlterarData = new frmAtendimentoIncluirDataTranscrito();
                    AlterarData.ShowDialog();

                    if (AlterarData.DataExameTranscrito != null)
                    {
                        foreach (DataGridViewRow dvRow in dgvExamePcmso.Rows)
                            if (Convert.ToBoolean(dvRow.Cells[5].Value))
                            {
                                dvRow.Cells[3].Value = AlterarData.DataExameTranscrito;
                                dvRow.Cells[10].Value = AlterarData.DataVencimentoExameTranscrito;
                            }
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btnModificarDataTranscritoExtra_Click(object sender, EventArgs e)
        {
            try
            {
                if (this.dgvExameAvulso.RowCount > 0)
                {
                    frmAtendimentoIncluirDataTranscrito AlterarData = new frmAtendimentoIncluirDataTranscrito();
                    AlterarData.ShowDialog();

                    if (AlterarData.DataExameTranscrito != null)
                    {
                        foreach (DataGridViewRow dvRow in dgvExameAvulso.Rows)
                            if (Convert.ToBoolean(dvRow.Cells[5].Value))
                            {
                                dvRow.Cells[3].Value = AlterarData.DataExameTranscrito;
                                dvRow.Cells[8].Value = AlterarData.DataVencimentoExameTranscrito;
                            }
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void chk_examesPCMSO_CheckedChanged(object sender, EventArgs e)
        {
            if (chk_examesPCMSO.Checked)
                foreach (DataGridViewRow row in dgvExamePcmso.Rows)
                    row.Cells[4].Value = true;

            else
                foreach (DataGridViewRow row in dgvExamePcmso.Rows)
                    row.Cells[4].Value = false;
        }

        private void calculaVencimentoAtendimento()
        {
            /* informando ao usuário a data de validade do atendimento. */
            dataVencimentoExame = dataAso.Value;

            AsoFacade asoFacade = AsoFacade.getInstance();
            Periodicidade periodicidade = asoFacade.findPeriodicidadeById(Convert.ToInt64(((SelectItem)cbTipoAtendimento.SelectedItem).Valor));

            if (periodicidade != null && periodicidade.Dias != null)
            {
                dataVencimento.Checked = true;
                dataVencimento.Value = dataAso.Value.AddDays(Convert.ToInt32(periodicidade.Dias)); 
            }

        }

        private void dataAso_ValueChanged(object sender, EventArgs e)
        {
            /* verificando para saber se já foi preenchida as grides de exames */

            if (dgvExameAvulso.RowCount > 0)
            {
                /* Atualizando a informação do vencimento dos exames */
                //calculaVencimentoAtendimento();
                recalculaVencimentoExames(dgvExameAvulso, 6, 8);
                recalculaVencimentoAtendimento(dgvExameAvulso);
                recalculaDataExame(dgvExameAvulso);
            }
            if (dgvExamePcmso.RowCount > 0)
            {
                /* atualizando a informação do vencimento dos exames */
                recalculaVencimentoExames(dgvExamePcmso, 8, 10);
                recalculaVencimentoAtendimento(dgvExamePcmso);
                recalculaDataExame(dgvExamePcmso);
            }

        }

        private void recalculaVencimentoAtendimento(DataGridView dgv)
        {
            try
            {

                /* encontrando a menor data do vencimento */
                DateTime menorData = DateTime.MaxValue;
                
                /* verificando se o controle é exame extra ou pcmso */

                int indiceVencimento = dgv.ColumnCount == 11 ? 8 : 10;

                foreach (DataGridViewRow dvRow in dgv.Rows)
                {
                    if ((bool)dvRow.Cells[4].Value)
                        if (Convert.ToDateTime(dvRow.Cells[indiceVencimento].Value) < menorData)
                            menorData = Convert.ToDateTime(dvRow.Cells[indiceVencimento].Value);

                }

                /* preenchendo a data do vencimento do atendimento */
                if (dgv.RowCount > 0)
                {
                    if (menorData != DateTime.MaxValue && !string.Equals(menorData.ToShortDateString(), Convert.ToDateTime(dataAso.Text).ToShortDateString()))
                        dataVencimento.Value = menorData;
                    else
                        calculaVencimentoAtendimento();
                }
                
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }

        private void dgvExameAvulso_CellValueChanged(object sender, DataGridViewCellEventArgs e)
        {
            if (e.ColumnIndex == 4)
            {
                /* recalcular a data do atendimento de acordo com os exames que estão marcados. */
                recalculaVencimentoAtendimento(sender as DataGridView);
            } 

        }

        private void dgvExameAvulso_CurrentCellDirtyStateChanged(object sender, EventArgs e)
        {
            if (dgvExameAvulso.IsCurrentCellDirty)
            {
                dgvExameAvulso.CommitEdit(DataGridViewDataErrorContexts.Commit);
            }
        }

        private void dgvExamePcmso_CellValueChanged(object sender, DataGridViewCellEventArgs e)
        {
            if (e.ColumnIndex == 4)
            {
                /* recalcular a data do atendimento de acordo com os exames que estão marcados. */
                recalculaVencimentoAtendimento(sender as DataGridView);
            } 
        }

        private void dgvExamePcmso_CurrentCellDirtyStateChanged(object sender, EventArgs e)
        {
            if (dgvExamePcmso.IsCurrentCellDirty)
            {
                dgvExamePcmso.CommitEdit(DataGridViewDataErrorContexts.Commit);
            }
        }

        private void recalculaVencimentoExames(object sender, int periodoVencimento, int colunaVencimento)
        {
            DataGridView dgv = sender as DataGridView;

            foreach (DataGridViewRow dvRow in dgv.Rows)
            {
                //somente calcular o vencimento do exame que estiver marcado.
                if ((bool)dvRow.Cells[4].Value)
                {
                    if (dvRow.Cells[periodoVencimento].Value != DBNull.Value && dvRow.Cells[periodoVencimento].Value != null)
                    {
                        dvRow.Cells[colunaVencimento].Value = dataAso.Value.AddMonths((int)dvRow.Cells[periodoVencimento].Value);
                    }
                }
            }


        }

        protected void btnIncluirMedicoCoordenador_Click(object sender, EventArgs e)
        {
            try
            {
                frmMedicoBuscar incluirCoordenador = new frmMedicoBuscar();
                incluirCoordenador.ShowDialog();

                if (incluirCoordenador.Medico != null)
                {
                    this.medicoCoordenadorAvulso = incluirCoordenador.Medico;
                    textCoordenador.Text = medicoCoordenadorAvulso.Nome + " CRM:" + medicoCoordenadorAvulso.Crm;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message,"Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }

        private void btnExcluirCoordenador_Click(object sender, EventArgs e)
        {
            try
            {
                medicoCoordenadorAvulso = null;
                textCoordenador.Text = string.Empty;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }

        protected void btnIncluirGhe_Click(object sender, EventArgs e)
        {
             try
            {
                frmAtendimentoGheAvulso incluirGhe = new frmAtendimentoGheAvulso();
                incluirGhe.ShowDialog();

                if (incluirGhe.GheSetor != null)
                {
                    ghesAvulsos.Add(incluirGhe.GheSetor);
                    montaGridgheSetor();
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        protected void btnExcluirGhe_Click(object sender, EventArgs e)
        {
            try
            {
                if (dgvGhe.CurrentRow == null)
                    throw new Exception("Selecione um ghe para excluir.");

                if ((bool)dgvGhe.CurrentRow.Cells[6].Value == false)
                    throw new Exception("Somente é possível excluir um Ghe/Setor avulso");

                string ghe = (string)dgvGhe.CurrentRow.Cells[2].Value;
                string setor = (string)dgvGhe.CurrentRow.Cells[4].Value;

                ghesAvulsos.Remove(ghesAvulsos.Find(x => x.Ghe == ghe && x.Setor == setor));
                dgvGhe.Rows.RemoveAt(dgvGhe.CurrentRow.Index);

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }

        private void btnIncluirRisco_Click(object sender, EventArgs e)
        {
            try
            {
                frmAgentesBuscar incluirAgenteAtendimento = new frmAgentesBuscar();
                incluirAgenteAtendimento.ShowDialog();

                if (incluirAgenteAtendimento.Agente != null)
                {
                    this.riscoAvulso.Add(new AsoAgenteRisco(null, incluirAgenteAtendimento.Agente.Id, incluirAgenteAtendimento.Agente.Descricao, incluirAgenteAtendimento.Agente.Risco.Id, incluirAgenteAtendimento.Agente.Risco.Descricao));
                    montaGridRiscoAvulso();
                }


            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }

            }

        protected void btnExcluirRisco_Click(object sender, EventArgs e)
        {
            try
            {
                if (dgvRisco.CurrentRow == null)
                    throw new Exception("Selecione um agente para excluir");

                riscoAvulso.Remove(riscoAvulso.Find(x => x.DescricaoAgente == dgvRisco.CurrentRow.Cells[0].Value &&
                    x.DescricaoRisco == dgvRisco.CurrentRow.Cells[1].Value));

                dgvRisco.Rows.RemoveAt(dgvRisco.CurrentRow.Index);

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }

        private void textSenha_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = ValidaCampoHelper.FormataNumeroELetras(sender, e);
        }

        public void montaGridRiscoAvulso()
        {
            try
            {
                dgvRisco.Columns.Clear();

                dgvRisco.ColumnCount = 2;
                dgvRisco.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
                dgvRisco.MultiSelect = false;

                dgvRisco.Columns[0].HeaderText = "Agente";
                dgvRisco.Columns[0].ReadOnly = true;

                dgvRisco.Columns[1].HeaderText = "Risco";
                dgvRisco.Columns[1].ReadOnly = true;

                foreach (AsoAgenteRisco agenteRisco in this.riscoAvulso)
                    dgvRisco.Rows.Add(agenteRisco.DescricaoAgente, agenteRisco.DescricaoRisco);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void cbEletricidade_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                if ((SelectItem)cbConfinado.SelectedItem != null && (SelectItem)cbAltura.SelectedItem != null && (SelectItem)cbEletricidade.SelectedItem != null)
                {
                    montaColecaoExamesGhe(Convert.ToBoolean(((SelectItem)cbAltura.SelectedItem).Valor), Convert.ToBoolean(((SelectItem)cbConfinado.SelectedItem).Valor), Convert.ToBoolean(((SelectItem)cbEletricidade.SelectedItem).Valor));
                    /* montando informações de exames trazidos pelo pcmso */
                    montaGridExamesPCMSO();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void recalculaDataExame(object sender)
        {
            try
            {
                DataGridView dgv = sender as DataGridView;

                foreach (DataGridViewRow dvRom in dgv.Rows)
                {
                    if ((bool)dvRom.Cells[5].Value != true)
                    {
                        dvRom.Cells[3].Value = Convert.ToDateTime(dataAso.Text);
                    }
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }

    }
}
