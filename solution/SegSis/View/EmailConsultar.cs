﻿using SWS.Entidade;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace SWS.View
{
    public partial class frmEmailConsultar : frmTemplateConsulta
    {
        private List<Email> emails = new List<Email>();

        public frmEmailConsultar(List<Email> emails)
        {
            InitializeComponent();
            this.emails = emails;
            montaDataGrid();
        }

        private void montaDataGrid()
        {
            try
            {
                dgEmail.Columns.Clear();

                dgEmail.ColumnCount = 8;

                dgEmail.Columns[0].HeaderText = "id";
                dgEmail.Columns[0].Visible = false;

                dgEmail.Columns[1].HeaderText = "Data de Envio";
                dgEmail.Columns[1].DefaultCellStyle.ForeColor = Color.Red;

                dgEmail.Columns[2].HeaderText = "Para";
                dgEmail.Columns[2].DefaultCellStyle.ForeColor = Color.Blue;

                dgEmail.Columns[3].HeaderText = "Assunto";

                dgEmail.Columns[4].HeaderText = "Mensagem";

                dgEmail.Columns[5].HeaderText = "Anexo";

                dgEmail.Columns[6].HeaderText = "Prioridade";
                dgEmail.Columns[6].DefaultCellStyle.ForeColor = Color.Red;

                dgEmail.Columns[7].HeaderText = "Id_Usuario";
                dgEmail.Columns[7].Visible = false;

                foreach (Email e in emails)
                    dgEmail.Rows.Add(e.Id, e.DataEnvio, e.Para, e.Assunto, e.Mensagem, e.Anexo, e.Prioridade, e.Usuario != null ? e.Usuario.Id: null);


            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btnFechar_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void dgEmail_CellContentDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                frmEmailVisualizar emailVisualiar = new frmEmailVisualizar(
                    new Email((Int64)dgEmail.Rows[e.RowIndex].Cells[0].Value,
                    null, (String)dgEmail.Rows[e.RowIndex].Cells[2].Value, (String)dgEmail.Rows[e.RowIndex].Cells[3].Value,
                    (String)dgEmail.Rows[e.RowIndex].Cells[4].Value, (String)dgEmail.Rows[e.RowIndex].Cells[5].Value,
                    (String)dgEmail.Rows[e.RowIndex].Cells[6].Value, (DateTime)dgEmail.Rows[e.RowIndex].Cells[1].Value, dgEmail.Rows[e.RowIndex].Cells[7].Value !=null ? new Usuario((Int64)dgEmail.Rows[e.RowIndex].Cells[7].Value) : null));

                emailVisualiar.ShowDialog();

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
    }
}
