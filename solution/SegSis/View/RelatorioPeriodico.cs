﻿using SWS.Entidade;
using SWS.View.ViewHelper;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace SWS.View
{
    public partial class frmRelatorioPeriodico : frmTemplateConsulta
    {
        private Cliente cliente;

        public Cliente Cliente
        {
            get { return cliente; }
            set { cliente = value; }
        }
        
        public frmRelatorioPeriodico()
        {
            InitializeComponent();
            dtVencimento.Value = new DateTime(DateTime.Now.Year, 12, 31);
        }

        private void btCliente_Click(object sender, EventArgs e)
        {
            try
            {
                frmClienteSelecionar selecionarCliente = new frmClienteSelecionar(null, null, false, null, null, null, null, null, null);
                selecionarCliente.ShowDialog();

                if (selecionarCliente.Cliente != null)
                {
                    cliente = selecionarCliente.Cliente;
                    textCliente.Text = cliente.RazaoSocial;

                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }

        private void btnExcluirCliente_Click(object sender, EventArgs e)
        {
            try
            {
                if (cliente == null)
                    throw new Exception("Selecione primeiro o cliente.");

                cliente = null;
                textCliente.Text = string.Empty;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }

        private void btnFechar_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnImprimir_Click(object sender, EventArgs e)
        {
            try
            {
                /* validando dados da tela */
                if (!validaDadosTela())
                {
                    this.Cursor = Cursors.WaitCursor;

                    string nomeRelatorio = "RELATORIO_PERIODICO_CLIENTE.JASPER";
                    long idCliente = (long)cliente.Id;

                    var process = new Process();

                    process.StartInfo.FileName = "Relatorio\\jre7\\bin\\java";
                    process.StartInfo.Arguments = " -jar Relatorio\\MiddlewareJasper.jar " + "Relatorio\\Report\\" + nomeRelatorio + " ID_CLIENTE:" + idCliente + ":INTEGER" + " DATA_PESQUISA:" + Convert.ToDateTime(dtVencimento.Text).ToShortDateString() + ":STRING";
                    process.StartInfo.UseShellExecute = false;
                    process.StartInfo.RedirectStandardError = true;
                    process.StartInfo.RedirectStandardInput = true;
                    process.StartInfo.RedirectStandardOutput = true;
                    process.StartInfo.CreateNoWindow = true;

                    process.Start();
                    process.StandardOutput.ReadToEnd();
                    process.WaitForExit();
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }

        }

        private bool validaDadosTela()
        {
            bool validaDados = false;
            try
            {
                if (cliente == null)
                    throw new Exception("Você deve selecionar um cliente para emissão do relatório.");

                /* validando caso tenha sido preenchido alguma data */
                if (!dtVencimento.Checked)
                    throw new Exception("Você deve selecionar uma data para a pesquisa.");

            }
            catch (Exception ex)
            {
                validaDados = true;
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

            return validaDados;
        }

        private void textPeriodoDias_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = ValidaCampoHelper.FormataCampoNumerico(sender, e);
        }

        private void frmRelatorioPeriodico_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter || e.KeyCode == Keys.Return)
                SendKeys.Send("{TAB}");
        }

       
    }
}
