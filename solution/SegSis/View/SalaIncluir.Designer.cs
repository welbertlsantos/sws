﻿namespace SWS.View
{
    partial class frmSalaIncluir
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            this.btn_fechar = new System.Windows.Forms.Button();
            this.btn_incluir = new System.Windows.Forms.Button();
            this.flpAcao = new System.Windows.Forms.FlowLayoutPanel();
            this.lblNomeSala = new System.Windows.Forms.TextBox();
            this.lblAndar = new System.Windows.Forms.TextBox();
            this.lblVip = new System.Windows.Forms.TextBox();
            this.textAndar = new System.Windows.Forms.TextBox();
            this.textNome = new System.Windows.Forms.TextBox();
            this.cbVIP = new SWS.ComboBoxWithBorder();
            this.grb_exame = new System.Windows.Forms.GroupBox();
            this.dgvExame = new System.Windows.Forms.DataGridView();
            this.btn_excluirExame = new System.Windows.Forms.Button();
            this.btn_incluirExame = new System.Windows.Forms.Button();
            this.flowLayoutPanel1 = new System.Windows.Forms.FlowLayoutPanel();
            this.lblSlug = new System.Windows.Forms.TextBox();
            this.textSlug = new System.Windows.Forms.TextBox();
            this.lblQtdeChamada = new System.Windows.Forms.TextBox();
            this.textQtdeChamada = new System.Windows.Forms.TextBox();
            ((System.ComponentModel.ISupportInitialize)(this.spForm)).BeginInit();
            this.spForm.Panel1.SuspendLayout();
            this.spForm.Panel2.SuspendLayout();
            this.spForm.SuspendLayout();
            this.pnlForm.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.banner)).BeginInit();
            this.flpAcao.SuspendLayout();
            this.grb_exame.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvExame)).BeginInit();
            this.flowLayoutPanel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // spForm
            // 
            // 
            // spForm.Panel1
            // 
            this.spForm.Panel1.Controls.Add(this.flpAcao);
            // 
            // pnlForm
            // 
            this.pnlForm.Controls.Add(this.textQtdeChamada);
            this.pnlForm.Controls.Add(this.lblQtdeChamada);
            this.pnlForm.Controls.Add(this.textSlug);
            this.pnlForm.Controls.Add(this.lblSlug);
            this.pnlForm.Controls.Add(this.flowLayoutPanel1);
            this.pnlForm.Controls.Add(this.grb_exame);
            this.pnlForm.Controls.Add(this.cbVIP);
            this.pnlForm.Controls.Add(this.textAndar);
            this.pnlForm.Controls.Add(this.textNome);
            this.pnlForm.Controls.Add(this.lblVip);
            this.pnlForm.Controls.Add(this.lblAndar);
            this.pnlForm.Controls.Add(this.lblNomeSala);
            // 
            // btn_fechar
            // 
            this.btn_fechar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_fechar.Image = global::SWS.Properties.Resources.close;
            this.btn_fechar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btn_fechar.Location = new System.Drawing.Point(84, 3);
            this.btn_fechar.Name = "btn_fechar";
            this.btn_fechar.Size = new System.Drawing.Size(75, 23);
            this.btn_fechar.TabIndex = 6;
            this.btn_fechar.Text = "&Fechar";
            this.btn_fechar.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btn_fechar.UseVisualStyleBackColor = true;
            this.btn_fechar.Click += new System.EventHandler(this.btn_fechar_Click);
            // 
            // btn_incluir
            // 
            this.btn_incluir.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_incluir.Image = global::SWS.Properties.Resources.Gravar;
            this.btn_incluir.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btn_incluir.Location = new System.Drawing.Point(3, 3);
            this.btn_incluir.Name = "btn_incluir";
            this.btn_incluir.Size = new System.Drawing.Size(75, 23);
            this.btn_incluir.TabIndex = 5;
            this.btn_incluir.Text = "&Gravar";
            this.btn_incluir.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btn_incluir.UseVisualStyleBackColor = true;
            this.btn_incluir.Click += new System.EventHandler(this.btn_incluir_Click);
            // 
            // flpAcao
            // 
            this.flpAcao.Controls.Add(this.btn_incluir);
            this.flpAcao.Controls.Add(this.btn_fechar);
            this.flpAcao.Dock = System.Windows.Forms.DockStyle.Fill;
            this.flpAcao.Location = new System.Drawing.Point(0, 0);
            this.flpAcao.Name = "flpAcao";
            this.flpAcao.Size = new System.Drawing.Size(784, 35);
            this.flpAcao.TabIndex = 0;
            // 
            // lblNomeSala
            // 
            this.lblNomeSala.BackColor = System.Drawing.Color.Silver;
            this.lblNomeSala.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblNomeSala.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNomeSala.Location = new System.Drawing.Point(12, 18);
            this.lblNomeSala.Name = "lblNomeSala";
            this.lblNomeSala.ReadOnly = true;
            this.lblNomeSala.Size = new System.Drawing.Size(145, 21);
            this.lblNomeSala.TabIndex = 41;
            this.lblNomeSala.TabStop = false;
            this.lblNomeSala.Text = "Nome da Sala";
            // 
            // lblAndar
            // 
            this.lblAndar.BackColor = System.Drawing.Color.Silver;
            this.lblAndar.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblAndar.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblAndar.Location = new System.Drawing.Point(12, 38);
            this.lblAndar.Name = "lblAndar";
            this.lblAndar.ReadOnly = true;
            this.lblAndar.Size = new System.Drawing.Size(145, 21);
            this.lblAndar.TabIndex = 42;
            this.lblAndar.TabStop = false;
            this.lblAndar.Text = "Localização (andar)";
            // 
            // lblVip
            // 
            this.lblVip.BackColor = System.Drawing.Color.Silver;
            this.lblVip.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblVip.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblVip.Location = new System.Drawing.Point(12, 58);
            this.lblVip.Name = "lblVip";
            this.lblVip.ReadOnly = true;
            this.lblVip.Size = new System.Drawing.Size(145, 21);
            this.lblVip.TabIndex = 43;
            this.lblVip.TabStop = false;
            this.lblVip.Text = "Sala VIP";
            // 
            // textAndar
            // 
            this.textAndar.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textAndar.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.textAndar.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textAndar.Location = new System.Drawing.Point(156, 38);
            this.textAndar.MaxLength = 2;
            this.textAndar.Name = "textAndar";
            this.textAndar.Size = new System.Drawing.Size(580, 21);
            this.textAndar.TabIndex = 2;
            this.textAndar.Text = "0";
            this.textAndar.Enter += new System.EventHandler(this.textAndar_Enter);
            this.textAndar.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.text_andar_KeyPress);
            this.textAndar.Leave += new System.EventHandler(this.text_andar_Leave);
            // 
            // textNome
            // 
            this.textNome.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textNome.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.textNome.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textNome.Location = new System.Drawing.Point(156, 18);
            this.textNome.MaxLength = 20;
            this.textNome.Name = "textNome";
            this.textNome.Size = new System.Drawing.Size(580, 21);
            this.textNome.TabIndex = 1;
            // 
            // cbVIP
            // 
            this.cbVIP.BackColor = System.Drawing.Color.LightGray;
            this.cbVIP.BorderColor = System.Drawing.Color.DimGray;
            this.cbVIP.BorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid;
            this.cbVIP.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbVIP.FormattingEnabled = true;
            this.cbVIP.Location = new System.Drawing.Point(156, 58);
            this.cbVIP.Name = "cbVIP";
            this.cbVIP.Size = new System.Drawing.Size(580, 21);
            this.cbVIP.TabIndex = 3;
            // 
            // grb_exame
            // 
            this.grb_exame.Controls.Add(this.dgvExame);
            this.grb_exame.Location = new System.Drawing.Point(12, 125);
            this.grb_exame.Name = "grb_exame";
            this.grb_exame.Size = new System.Drawing.Size(724, 274);
            this.grb_exame.TabIndex = 44;
            this.grb_exame.TabStop = false;
            this.grb_exame.Text = "Exames";
            // 
            // dgvExame
            // 
            this.dgvExame.AllowUserToAddRows = false;
            this.dgvExame.AllowUserToDeleteRows = false;
            this.dgvExame.AllowUserToOrderColumns = true;
            this.dgvExame.AllowUserToResizeRows = false;
            this.dgvExame.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this.dgvExame.BackgroundColor = System.Drawing.Color.White;
            this.dgvExame.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.LightGray;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvExame.DefaultCellStyle = dataGridViewCellStyle1;
            this.dgvExame.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvExame.Location = new System.Drawing.Point(3, 16);
            this.dgvExame.MultiSelect = false;
            this.dgvExame.Name = "dgvExame";
            this.dgvExame.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.CellSelect;
            this.dgvExame.Size = new System.Drawing.Size(718, 255);
            this.dgvExame.TabIndex = 4;
            this.dgvExame.CellLeave += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvExame_CellLeave);
            this.dgvExame.EditingControlShowing += new System.Windows.Forms.DataGridViewEditingControlShowingEventHandler(this.dgvExame_EditingControlShowing);
            this.dgvExame.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.dgvExame_KeyPress);
            // 
            // btn_excluirExame
            // 
            this.btn_excluirExame.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_excluirExame.Image = global::SWS.Properties.Resources.lixeira;
            this.btn_excluirExame.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btn_excluirExame.Location = new System.Drawing.Point(84, 3);
            this.btn_excluirExame.Name = "btn_excluirExame";
            this.btn_excluirExame.Size = new System.Drawing.Size(75, 23);
            this.btn_excluirExame.TabIndex = 2;
            this.btn_excluirExame.Text = "E&xcluir";
            this.btn_excluirExame.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btn_excluirExame.UseVisualStyleBackColor = true;
            this.btn_excluirExame.Click += new System.EventHandler(this.btn_excluirExame_Click);
            // 
            // btn_incluirExame
            // 
            this.btn_incluirExame.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_incluirExame.Image = global::SWS.Properties.Resources.icone_mais;
            this.btn_incluirExame.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btn_incluirExame.Location = new System.Drawing.Point(3, 3);
            this.btn_incluirExame.Name = "btn_incluirExame";
            this.btn_incluirExame.Size = new System.Drawing.Size(75, 23);
            this.btn_incluirExame.TabIndex = 1;
            this.btn_incluirExame.Text = "I&ncluir";
            this.btn_incluirExame.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btn_incluirExame.UseVisualStyleBackColor = true;
            this.btn_incluirExame.Click += new System.EventHandler(this.btn_incluirExame_Click);
            // 
            // flowLayoutPanel1
            // 
            this.flowLayoutPanel1.Controls.Add(this.btn_incluirExame);
            this.flowLayoutPanel1.Controls.Add(this.btn_excluirExame);
            this.flowLayoutPanel1.Location = new System.Drawing.Point(12, 402);
            this.flowLayoutPanel1.Name = "flowLayoutPanel1";
            this.flowLayoutPanel1.Size = new System.Drawing.Size(724, 32);
            this.flowLayoutPanel1.TabIndex = 45;
            // 
            // lblSlug
            // 
            this.lblSlug.BackColor = System.Drawing.Color.Silver;
            this.lblSlug.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblSlug.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSlug.Location = new System.Drawing.Point(12, 78);
            this.lblSlug.Name = "lblSlug";
            this.lblSlug.ReadOnly = true;
            this.lblSlug.Size = new System.Drawing.Size(145, 21);
            this.lblSlug.TabIndex = 46;
            this.lblSlug.TabStop = false;
            this.lblSlug.Text = "Slug";
            // 
            // textSlug
            // 
            this.textSlug.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textSlug.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.textSlug.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textSlug.Location = new System.Drawing.Point(156, 78);
            this.textSlug.MaxLength = 20;
            this.textSlug.Name = "textSlug";
            this.textSlug.Size = new System.Drawing.Size(580, 21);
            this.textSlug.TabIndex = 4;
            // 
            // lblQtdeChamada
            // 
            this.lblQtdeChamada.BackColor = System.Drawing.Color.Silver;
            this.lblQtdeChamada.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblQtdeChamada.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblQtdeChamada.Location = new System.Drawing.Point(12, 98);
            this.lblQtdeChamada.Name = "lblQtdeChamada";
            this.lblQtdeChamada.ReadOnly = true;
            this.lblQtdeChamada.Size = new System.Drawing.Size(145, 21);
            this.lblQtdeChamada.TabIndex = 47;
            this.lblQtdeChamada.TabStop = false;
            this.lblQtdeChamada.Text = "Qtde por chamada";
            // 
            // textQtdeChamada
            // 
            this.textQtdeChamada.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textQtdeChamada.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.textQtdeChamada.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textQtdeChamada.Location = new System.Drawing.Point(156, 98);
            this.textQtdeChamada.MaxLength = 2;
            this.textQtdeChamada.Name = "textQtdeChamada";
            this.textQtdeChamada.Size = new System.Drawing.Size(580, 21);
            this.textQtdeChamada.TabIndex = 48;
            this.textQtdeChamada.Enter += new System.EventHandler(this.textQtdeChamada_Enter);
            this.textQtdeChamada.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.textQtdeChamada_KeyPress);
            this.textQtdeChamada.Leave += new System.EventHandler(this.textQtdeChamada_Leave);
            // 
            // frmSalaIncluir
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(784, 561);
            this.Name = "frmSalaIncluir";
            this.Text = "INCLUIR SALA DE ATENDIMENTO";
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.SalaIncluir_KeyDown);
            this.spForm.Panel1.ResumeLayout(false);
            this.spForm.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.spForm)).EndInit();
            this.spForm.ResumeLayout(false);
            this.pnlForm.ResumeLayout(false);
            this.pnlForm.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.banner)).EndInit();
            this.flpAcao.ResumeLayout(false);
            this.grb_exame.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvExame)).EndInit();
            this.flowLayoutPanel1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        protected System.Windows.Forms.Button btn_fechar;
        protected System.Windows.Forms.Button btn_incluir;
        protected System.Windows.Forms.FlowLayoutPanel flpAcao;
        protected System.Windows.Forms.TextBox lblAndar;
        protected System.Windows.Forms.TextBox lblNomeSala;
        protected System.Windows.Forms.TextBox lblVip;
        protected System.Windows.Forms.TextBox textAndar;
        protected System.Windows.Forms.TextBox textNome;
        protected ComboBoxWithBorder cbVIP;
        protected System.Windows.Forms.FlowLayoutPanel flowLayoutPanel1;
        protected System.Windows.Forms.Button btn_incluirExame;
        protected System.Windows.Forms.Button btn_excluirExame;
        protected System.Windows.Forms.GroupBox grb_exame;
        protected System.Windows.Forms.DataGridView dgvExame;
        protected System.Windows.Forms.TextBox lblSlug;
        protected System.Windows.Forms.TextBox textSlug;
        protected System.Windows.Forms.TextBox textQtdeChamada;
        protected System.Windows.Forms.TextBox lblQtdeChamada;

    }
}