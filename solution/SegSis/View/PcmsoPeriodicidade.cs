﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using SWS.Facade;
using SWS.Entidade;
using SWS.View.ViewHelper;

namespace SWS.View
{
    public partial class frmPcmsoPeriodicidade : SWS.View.frmTemplateConsulta
    {

        private static String msg1 = "Você não selecionou nenhuma periodicidade";
        
        private GheFonteAgenteExame gheFonteAgenteExame;

        public GheFonteAgenteExame GheFonteAgenteExame
        {
            get { return gheFonteAgenteExame; }
            set { gheFonteAgenteExame = value; }
        }

        public frmPcmsoPeriodicidade(GheFonteAgenteExame gheFonteAgenteExame)
        {
            InitializeComponent();
            this.gheFonteAgenteExame = gheFonteAgenteExame;
            ActiveControl = textPeriodicidade;
        }

        private void btConfirmar_Click(object sender, EventArgs e)
        {
            try
            {
                Boolean alteracao = false;
                EstudoFacade pcmsoFacade = EstudoFacade.getInstance();
                
                foreach (DataGridViewRow dvRow in dgvPeriodicidade.Rows)
                {
                    if ((Boolean)dvRow.Cells[2].Value == true)
                    {
                        if (dvRow.Cells[3].Value == DBNull.Value)
                            throw new Exception("Tem item marcado sem estar selecionado a periodidicade.");
                        
                        pcmsoFacade.insertGheFonteAgenteExamePeriodicidade(new GheFonteAgenteExamePeriodicidade(new Periodicidade((Int64)dvRow.Cells[0].Value, dvRow.Cells[1].Value.ToString(), null), gheFonteAgenteExame, String.Empty, dvRow.Cells[3].Value == string.Empty ? (int?)null : Convert.ToInt32(dvRow.Cells[3].Value)));
                        alteracao = true;
                    }
                }

                if (!alteracao)
                    throw new Exception(msg1);
                else
                    this.Close();
            }
            catch (Exception Ex)
            {
                MessageBox.Show(Ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }

        private void btFechar_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btBuscar_Click(object sender, EventArgs e)
        {
            try
            {
                montaDataGrid();
            }
            catch (Exception Ex)
            {
                MessageBox.Show(Ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        public void montaDataGrid()
        {
            try
            {
                EstudoFacade pcmsoFacade = EstudoFacade.getInstance();

                DataSet ds = pcmsoFacade.findAllPeriodicidadesNotInGheFonteAgenteExame(new Periodicidade(null, this.textPeriodicidade.Text, null), gheFonteAgenteExame );

                //dgvPeriodicidade.DataSource = ds.Tables["Periodicidades"].DefaultView;
                //dgvPeriodicidade.AutoResizeColumns();

                dgvPeriodicidade.ColumnCount = 2;

                dgvPeriodicidade.Columns[0].HeaderText = "id_periodicidade";
                dgvPeriodicidade.Columns[0].Visible = false;

                dgvPeriodicidade.Columns[1].HeaderText = "Descrição";

                DataGridViewCheckBoxColumn check = new DataGridViewCheckBoxColumn();
                check.Name = "Selecionar";
                dgvPeriodicidade.Columns.Add(check);
                dgvPeriodicidade.Columns[2].Width = 30;

                DataGridViewComboBoxColumn periodoVencimento = new DataGridViewComboBoxColumn();
                periodoVencimento.Name = "Vencimento";
                dgvPeriodicidade.Columns.Add(periodoVencimento);
                dgvPeriodicidade.Columns[3].Width = 200;
                ComboHelper.periodoVencimentoCombo(periodoVencimento);

                foreach (DataRow dvRow in ds.Tables[0].Rows)
                    dgvPeriodicidade.Rows.Add((long)dvRow["id_periodicidade"], dvRow["descricao"].ToString(), false, gheFonteAgenteExame.Exame.PeriodoVencimento == null ? string.Empty : Convert.ToString(gheFonteAgenteExame.Exame.PeriodoVencimento));

                dgvPeriodicidade.Columns[1].DisplayIndex = 2;
                dgvPeriodicidade.Columns[2].DisplayIndex = 1;
                dgvPeriodicidade.Columns[3].DisplayIndex = 3;


            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
    }
}
