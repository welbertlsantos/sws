﻿namespace SWS.View
{
    partial class frmAtendimentoLaudar
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            this.flpAcao = new System.Windows.Forms.FlowLayoutPanel();
            this.btn_gravar = new System.Windows.Forms.Button();
            this.btn_fechar = new System.Windows.Forms.Button();
            this.text_rgFuncionario = new System.Windows.Forms.TextBox();
            this.text_razaoCliente = new System.Windows.Forms.TextBox();
            this.text_cnpj = new System.Windows.Forms.TextBox();
            this.text_dataAso = new System.Windows.Forms.TextBox();
            this.text_nomeFuncionario = new System.Windows.Forms.TextBox();
            this.text_codigoAso = new System.Windows.Forms.TextBox();
            this.lblCodigo = new System.Windows.Forms.TextBox();
            this.lblFuncionario = new System.Windows.Forms.TextBox();
            this.lblRG = new System.Windows.Forms.TextBox();
            this.lblRazaoSocial = new System.Windows.Forms.TextBox();
            this.lblCNPJ = new System.Windows.Forms.TextBox();
            this.lblDataAtendimento = new System.Windows.Forms.TextBox();
            this.lblLaudo = new System.Windows.Forms.TextBox();
            this.text_observacao = new System.Windows.Forms.TextBox();
            this.text_conclusao = new System.Windows.Forms.TextBox();
            this.lblObservacao = new System.Windows.Forms.TextBox();
            this.grb_exame = new System.Windows.Forms.GroupBox();
            this.dgv_exame = new System.Windows.Forms.DataGridView();
            this.tpDgvLaudo = new System.Windows.Forms.ToolTip(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.spForm)).BeginInit();
            this.spForm.Panel1.SuspendLayout();
            this.spForm.Panel2.SuspendLayout();
            this.spForm.SuspendLayout();
            this.pnlForm.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.banner)).BeginInit();
            this.flpAcao.SuspendLayout();
            this.grb_exame.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgv_exame)).BeginInit();
            this.SuspendLayout();
            // 
            // spForm
            // 
            // 
            // spForm.Panel1
            // 
            this.spForm.Panel1.Controls.Add(this.flpAcao);
            // 
            // pnlForm
            // 
            this.pnlForm.Controls.Add(this.grb_exame);
            this.pnlForm.Controls.Add(this.lblObservacao);
            this.pnlForm.Controls.Add(this.text_observacao);
            this.pnlForm.Controls.Add(this.text_conclusao);
            this.pnlForm.Controls.Add(this.lblLaudo);
            this.pnlForm.Controls.Add(this.lblDataAtendimento);
            this.pnlForm.Controls.Add(this.lblCNPJ);
            this.pnlForm.Controls.Add(this.lblRazaoSocial);
            this.pnlForm.Controls.Add(this.lblRG);
            this.pnlForm.Controls.Add(this.lblFuncionario);
            this.pnlForm.Controls.Add(this.lblCodigo);
            this.pnlForm.Controls.Add(this.text_rgFuncionario);
            this.pnlForm.Controls.Add(this.text_razaoCliente);
            this.pnlForm.Controls.Add(this.text_cnpj);
            this.pnlForm.Controls.Add(this.text_dataAso);
            this.pnlForm.Controls.Add(this.text_nomeFuncionario);
            this.pnlForm.Controls.Add(this.text_codigoAso);
            this.pnlForm.Size = new System.Drawing.Size(764, 477);
            // 
            // flpAcao
            // 
            this.flpAcao.Controls.Add(this.btn_gravar);
            this.flpAcao.Controls.Add(this.btn_fechar);
            this.flpAcao.Dock = System.Windows.Forms.DockStyle.Fill;
            this.flpAcao.Location = new System.Drawing.Point(0, 0);
            this.flpAcao.Name = "flpAcao";
            this.flpAcao.Size = new System.Drawing.Size(784, 35);
            this.flpAcao.TabIndex = 0;
            // 
            // btn_gravar
            // 
            this.btn_gravar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_gravar.Image = global::SWS.Properties.Resources.fechar_ico;
            this.btn_gravar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btn_gravar.Location = new System.Drawing.Point(3, 3);
            this.btn_gravar.Name = "btn_gravar";
            this.btn_gravar.Size = new System.Drawing.Size(75, 23);
            this.btn_gravar.TabIndex = 2;
            this.btn_gravar.Text = "&Gravar";
            this.btn_gravar.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btn_gravar.UseVisualStyleBackColor = true;
            this.btn_gravar.Click += new System.EventHandler(this.btn_gravar_Click);
            // 
            // btn_fechar
            // 
            this.btn_fechar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_fechar.Image = global::SWS.Properties.Resources.close;
            this.btn_fechar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btn_fechar.Location = new System.Drawing.Point(84, 3);
            this.btn_fechar.Name = "btn_fechar";
            this.btn_fechar.Size = new System.Drawing.Size(75, 23);
            this.btn_fechar.TabIndex = 3;
            this.btn_fechar.Text = "&Fechar";
            this.btn_fechar.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btn_fechar.UseVisualStyleBackColor = true;
            this.btn_fechar.Click += new System.EventHandler(this.btn_fechar_Click);
            // 
            // text_rgFuncionario
            // 
            this.text_rgFuncionario.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.text_rgFuncionario.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.text_rgFuncionario.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.text_rgFuncionario.Location = new System.Drawing.Point(135, 54);
            this.text_rgFuncionario.MaxLength = 15;
            this.text_rgFuncionario.Name = "text_rgFuncionario";
            this.text_rgFuncionario.ReadOnly = true;
            this.text_rgFuncionario.Size = new System.Drawing.Size(608, 21);
            this.text_rgFuncionario.TabIndex = 22;
            this.text_rgFuncionario.TabStop = false;
            // 
            // text_razaoCliente
            // 
            this.text_razaoCliente.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.text_razaoCliente.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.text_razaoCliente.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.text_razaoCliente.Location = new System.Drawing.Point(135, 74);
            this.text_razaoCliente.MaxLength = 18;
            this.text_razaoCliente.Name = "text_razaoCliente";
            this.text_razaoCliente.ReadOnly = true;
            this.text_razaoCliente.Size = new System.Drawing.Size(608, 21);
            this.text_razaoCliente.TabIndex = 20;
            this.text_razaoCliente.TabStop = false;
            // 
            // text_cnpj
            // 
            this.text_cnpj.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.text_cnpj.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.text_cnpj.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.text_cnpj.Location = new System.Drawing.Point(135, 94);
            this.text_cnpj.MaxLength = 18;
            this.text_cnpj.Name = "text_cnpj";
            this.text_cnpj.ReadOnly = true;
            this.text_cnpj.Size = new System.Drawing.Size(608, 21);
            this.text_cnpj.TabIndex = 18;
            this.text_cnpj.TabStop = false;
            // 
            // text_dataAso
            // 
            this.text_dataAso.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.text_dataAso.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.text_dataAso.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.text_dataAso.Location = new System.Drawing.Point(135, 114);
            this.text_dataAso.MaxLength = 10;
            this.text_dataAso.Name = "text_dataAso";
            this.text_dataAso.ReadOnly = true;
            this.text_dataAso.Size = new System.Drawing.Size(608, 21);
            this.text_dataAso.TabIndex = 16;
            this.text_dataAso.TabStop = false;
            // 
            // text_nomeFuncionario
            // 
            this.text_nomeFuncionario.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.text_nomeFuncionario.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.text_nomeFuncionario.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.text_nomeFuncionario.Location = new System.Drawing.Point(135, 34);
            this.text_nomeFuncionario.MaxLength = 100;
            this.text_nomeFuncionario.Name = "text_nomeFuncionario";
            this.text_nomeFuncionario.ReadOnly = true;
            this.text_nomeFuncionario.Size = new System.Drawing.Size(608, 21);
            this.text_nomeFuncionario.TabIndex = 14;
            this.text_nomeFuncionario.TabStop = false;
            // 
            // text_codigoAso
            // 
            this.text_codigoAso.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.text_codigoAso.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.text_codigoAso.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.text_codigoAso.Location = new System.Drawing.Point(135, 14);
            this.text_codigoAso.MaxLength = 14;
            this.text_codigoAso.Name = "text_codigoAso";
            this.text_codigoAso.ReadOnly = true;
            this.text_codigoAso.Size = new System.Drawing.Size(608, 21);
            this.text_codigoAso.TabIndex = 12;
            this.text_codigoAso.TabStop = false;
            // 
            // lblCodigo
            // 
            this.lblCodigo.BackColor = System.Drawing.Color.LightGray;
            this.lblCodigo.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblCodigo.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCodigo.Location = new System.Drawing.Point(12, 14);
            this.lblCodigo.MaxLength = 14;
            this.lblCodigo.Name = "lblCodigo";
            this.lblCodigo.ReadOnly = true;
            this.lblCodigo.Size = new System.Drawing.Size(124, 21);
            this.lblCodigo.TabIndex = 24;
            this.lblCodigo.TabStop = false;
            this.lblCodigo.Text = "Código Atendimento";
            // 
            // lblFuncionario
            // 
            this.lblFuncionario.BackColor = System.Drawing.Color.LightGray;
            this.lblFuncionario.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblFuncionario.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblFuncionario.Location = new System.Drawing.Point(12, 34);
            this.lblFuncionario.MaxLength = 14;
            this.lblFuncionario.Name = "lblFuncionario";
            this.lblFuncionario.ReadOnly = true;
            this.lblFuncionario.Size = new System.Drawing.Size(124, 21);
            this.lblFuncionario.TabIndex = 25;
            this.lblFuncionario.TabStop = false;
            this.lblFuncionario.Text = "Funcionário";
            // 
            // lblRG
            // 
            this.lblRG.BackColor = System.Drawing.Color.LightGray;
            this.lblRG.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblRG.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblRG.Location = new System.Drawing.Point(12, 54);
            this.lblRG.MaxLength = 14;
            this.lblRG.Name = "lblRG";
            this.lblRG.ReadOnly = true;
            this.lblRG.Size = new System.Drawing.Size(124, 21);
            this.lblRG.TabIndex = 26;
            this.lblRG.TabStop = false;
            this.lblRG.Text = "RG";
            // 
            // lblRazaoSocial
            // 
            this.lblRazaoSocial.BackColor = System.Drawing.Color.LightGray;
            this.lblRazaoSocial.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblRazaoSocial.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblRazaoSocial.Location = new System.Drawing.Point(12, 74);
            this.lblRazaoSocial.MaxLength = 14;
            this.lblRazaoSocial.Name = "lblRazaoSocial";
            this.lblRazaoSocial.ReadOnly = true;
            this.lblRazaoSocial.Size = new System.Drawing.Size(124, 21);
            this.lblRazaoSocial.TabIndex = 27;
            this.lblRazaoSocial.TabStop = false;
            this.lblRazaoSocial.Text = "Cliente";
            // 
            // lblCNPJ
            // 
            this.lblCNPJ.BackColor = System.Drawing.Color.LightGray;
            this.lblCNPJ.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblCNPJ.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCNPJ.Location = new System.Drawing.Point(12, 94);
            this.lblCNPJ.MaxLength = 14;
            this.lblCNPJ.Name = "lblCNPJ";
            this.lblCNPJ.ReadOnly = true;
            this.lblCNPJ.Size = new System.Drawing.Size(124, 21);
            this.lblCNPJ.TabIndex = 28;
            this.lblCNPJ.TabStop = false;
            this.lblCNPJ.Text = "CNPJ/CPF";
            // 
            // lblDataAtendimento
            // 
            this.lblDataAtendimento.BackColor = System.Drawing.Color.LightGray;
            this.lblDataAtendimento.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblDataAtendimento.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDataAtendimento.Location = new System.Drawing.Point(12, 114);
            this.lblDataAtendimento.MaxLength = 14;
            this.lblDataAtendimento.Name = "lblDataAtendimento";
            this.lblDataAtendimento.ReadOnly = true;
            this.lblDataAtendimento.Size = new System.Drawing.Size(124, 21);
            this.lblDataAtendimento.TabIndex = 29;
            this.lblDataAtendimento.TabStop = false;
            this.lblDataAtendimento.Text = "Data do Atendimento";
            // 
            // lblLaudo
            // 
            this.lblLaudo.BackColor = System.Drawing.Color.LightGray;
            this.lblLaudo.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblLaudo.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblLaudo.Location = new System.Drawing.Point(12, 141);
            this.lblLaudo.MaxLength = 14;
            this.lblLaudo.Name = "lblLaudo";
            this.lblLaudo.ReadOnly = true;
            this.lblLaudo.Size = new System.Drawing.Size(124, 21);
            this.lblLaudo.TabIndex = 30;
            this.lblLaudo.TabStop = false;
            this.lblLaudo.Text = "Laudo";
            // 
            // text_observacao
            // 
            this.text_observacao.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.text_observacao.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.text_observacao.Location = new System.Drawing.Point(135, 161);
            this.text_observacao.Multiline = true;
            this.text_observacao.Name = "text_observacao";
            this.text_observacao.ReadOnly = true;
            this.text_observacao.Size = new System.Drawing.Size(608, 21);
            this.text_observacao.TabIndex = 33;
            // 
            // text_conclusao
            // 
            this.text_conclusao.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.text_conclusao.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.text_conclusao.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.text_conclusao.ForeColor = System.Drawing.SystemColors.WindowText;
            this.text_conclusao.Location = new System.Drawing.Point(135, 141);
            this.text_conclusao.Name = "text_conclusao";
            this.text_conclusao.ReadOnly = true;
            this.text_conclusao.Size = new System.Drawing.Size(608, 21);
            this.text_conclusao.TabIndex = 31;
            // 
            // lblObservacao
            // 
            this.lblObservacao.BackColor = System.Drawing.Color.LightGray;
            this.lblObservacao.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblObservacao.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblObservacao.Location = new System.Drawing.Point(12, 161);
            this.lblObservacao.MaxLength = 14;
            this.lblObservacao.Name = "lblObservacao";
            this.lblObservacao.ReadOnly = true;
            this.lblObservacao.Size = new System.Drawing.Size(124, 21);
            this.lblObservacao.TabIndex = 35;
            this.lblObservacao.TabStop = false;
            this.lblObservacao.Text = "Observação";
            // 
            // grb_exame
            // 
            this.grb_exame.Controls.Add(this.dgv_exame);
            this.grb_exame.Location = new System.Drawing.Point(12, 193);
            this.grb_exame.Name = "grb_exame";
            this.grb_exame.Size = new System.Drawing.Size(733, 259);
            this.grb_exame.TabIndex = 36;
            this.grb_exame.TabStop = false;
            this.grb_exame.Text = "Exames";
            // 
            // dgv_exame
            // 
            this.dgv_exame.AllowUserToAddRows = false;
            this.dgv_exame.AllowUserToDeleteRows = false;
            this.dgv_exame.AllowUserToOrderColumns = true;
            this.dgv_exame.AllowUserToResizeColumns = false;
            this.dgv_exame.AllowUserToResizeRows = false;
            this.dgv_exame.BackgroundColor = System.Drawing.Color.White;
            this.dgv_exame.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.LightGray;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgv_exame.DefaultCellStyle = dataGridViewCellStyle1;
            this.dgv_exame.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgv_exame.Location = new System.Drawing.Point(3, 16);
            this.dgv_exame.Name = "dgv_exame";
            this.dgv_exame.Size = new System.Drawing.Size(727, 240);
            this.dgv_exame.TabIndex = 0;
            this.dgv_exame.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgv_exame_CellContentClick);
            this.dgv_exame.CellFormatting += new System.Windows.Forms.DataGridViewCellFormattingEventHandler(this.dgv_exame_CellFormatting);
            this.dgv_exame.ColumnAdded += new System.Windows.Forms.DataGridViewColumnEventHandler(this.dgv_exame_ColumnAdded);
            this.dgv_exame.CurrentCellDirtyStateChanged += new System.EventHandler(this.dgv_exame_CurrentCellDirtyStateChanged);
            this.dgv_exame.EditingControlShowing += new System.Windows.Forms.DataGridViewEditingControlShowingEventHandler(this.dgv_exame_EditingControlShowing);
            // 
            // frmAtendimentoLaudar
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(784, 561);
            this.Name = "frmAtendimentoLaudar";
            this.Text = "LAUDAR ATENDIMENTO";
            this.spForm.Panel1.ResumeLayout(false);
            this.spForm.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.spForm)).EndInit();
            this.spForm.ResumeLayout(false);
            this.pnlForm.ResumeLayout(false);
            this.pnlForm.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.banner)).EndInit();
            this.flpAcao.ResumeLayout(false);
            this.grb_exame.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgv_exame)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.FlowLayoutPanel flpAcao;
        private System.Windows.Forms.Button btn_fechar;
        private System.Windows.Forms.Button btn_gravar;
        private System.Windows.Forms.TextBox lblDataAtendimento;
        private System.Windows.Forms.TextBox lblCNPJ;
        private System.Windows.Forms.TextBox lblRazaoSocial;
        private System.Windows.Forms.TextBox lblRG;
        private System.Windows.Forms.TextBox lblFuncionario;
        private System.Windows.Forms.TextBox lblCodigo;
        private System.Windows.Forms.TextBox text_rgFuncionario;
        private System.Windows.Forms.TextBox text_razaoCliente;
        private System.Windows.Forms.TextBox text_cnpj;
        private System.Windows.Forms.TextBox text_dataAso;
        private System.Windows.Forms.TextBox text_nomeFuncionario;
        private System.Windows.Forms.TextBox text_codigoAso;
        private System.Windows.Forms.TextBox lblLaudo;
        private System.Windows.Forms.TextBox text_observacao;
        private System.Windows.Forms.TextBox lblObservacao;
        private System.Windows.Forms.TextBox text_conclusao;
        private System.Windows.Forms.GroupBox grb_exame;
        private System.Windows.Forms.DataGridView dgv_exame;
        private System.Windows.Forms.ToolTip tpDgvLaudo;

    }
}