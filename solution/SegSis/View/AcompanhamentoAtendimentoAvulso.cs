﻿using SWS.Entidade;
using SWS.Facade;
using SWS.View.ViewHelper;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace SWS.View
{
    public partial class frmAcompanhamentoAtendimentoAvulso : frmTemplateConsulta
    {
        private Cliente cliente;

        public Cliente Cliente
        {
            get { return cliente; }
            set { cliente = value; }
        }

        private Exame exame;

        public Exame Exame
        {
            get { return exame; }
            set { exame = value; }
        }

        private Sala sala;

        public Sala Sala
        {
            get { return sala; }
            set { sala = value; }
        }

        public frmAcompanhamentoAtendimentoAvulso()
        {
            InitializeComponent();
            ActiveControl = textNome;
            textAndar.Text = "1";
        }

        private void btnFechar_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnConfirmar_Click(object sender, EventArgs e)
        {
            try
            {
                this.Cursor = Cursors.WaitCursor;
                /* iniciando inclusão do chamado avulso no atendimento */

                if (validaDados())
                {
                    FilaFacade filaFacade = FilaFacade.getInstance();
                    filaFacade.insertAcompanhamentoAtendimento(new AcompanhamentoAtendimento(null, textNome.Text, sala == null ? textSala.Text : sala.SlugSala, Convert.ToInt32(textAndar.Text), textCliente.Text, textRg.Text, DateTime.Now, textSenha.Text, false, textCpf.Text.Replace(".", "").Replace("-", "").Trim(), null, (long)0, string.Empty, textExame.Text, (long)PermissionamentoFacade.usuarioAutenticado.Empresa.Id, PermissionamentoFacade.usuarioAutenticado.Empresa.RazaoSocial));
                    MessageBox.Show("Pré Atendimento concluído com sucesso", "Confirmação", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    limpaDadosTela();
                }
                
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                this.Cursor = Cursors.Default;
               
            }
        }

        private void AcompanhamentoAtendimentoAvulso_KeyDown(object sender, KeyEventArgs e)
        {
            {
                if (e.KeyCode == Keys.Enter || e.KeyCode == Keys.Return)
                    SendKeys.Send("{TAB}");
            }
        }

        private void btnCliente_Click(object sender, EventArgs e)
        {
            try
            {
                frmClienteSelecionar selecionarCliente = new frmClienteSelecionar(null, null, null, null, null, null, null, null, true);
                selecionarCliente.ShowDialog();

                if (selecionarCliente.Cliente != null)
                {
                    cliente = selecionarCliente.Cliente;
                    textCliente.Text = cliente.RazaoSocial;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                frmExameBuscar selecionarExame = new frmExameBuscar();
                selecionarExame.ShowDialog();

                if (selecionarExame.Exame != null)
                {
                    exame = selecionarExame.Exame;
                    textExame.Text = exame.Descricao;

                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }

        private void textCpf_Leave(object sender, EventArgs e)
        {
            try
            {
                if (!String.IsNullOrEmpty(textCpf.Text.Replace(".", "").Replace("-", "").Trim()))
                {
                    if (!ValidaCampoHelper.ValidaCPF(textCpf.Text))
                    {
                        throw new Exception("CPF digitado é inválido.");
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
                textCpf.Focus();
                textCpf.Select(0, 0);
            }
        }

        private void btnSala_Click(object sender, EventArgs e)
        {
            try
            {
                frmSalaBuscar selecionarSala = new frmSalaBuscar(PermissionamentoFacade.usuarioAutenticado.Empresa);
                selecionarSala.ShowDialog();
                if (selecionarSala.Sala != null)
                {
                    sala = selecionarSala.Sala;
                    textSala.Text = sala.Descricao + "/" + sala.SlugSala;
                    textAndar.Text = sala.Andar.ToString();

                }
                
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btnCancelarCliente_Click(object sender, EventArgs e)
        {
            try
            {
                if (Cliente == null && string.IsNullOrEmpty(textCliente.Text))
                    throw new Exception("Você deve selecionar ou digitar o nome do cliente");

                cliente = null;
                textCliente.Text = string.Empty;


            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btnCancelarSala_Click(object sender, EventArgs e)
        {
            try
            {
                if (sala == null && string.IsNullOrEmpty(textSala.Text))
                    throw new Exception("Você deve selecionar ou digitar o nome da sala.");

                sala = null;
                textSala.Text = string.Empty;


            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btnCancelarExame_Click(object sender, EventArgs e)
        {
            try
            {
                if (exame == null && string.IsNullOrEmpty(textExame.Text))
                    throw new Exception("Você deve selecionar ou digitar o nome do exame.");

                exame = null;
                textExame.Text = string.Empty;


            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void textSenha_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = ValidaCampoHelper.FormataCampoNumerico(sender, e);
        }

        private bool validaDados()
        {
            bool result = true;
            try
            {
                if (string.IsNullOrEmpty(textNome.Text))
                {
                    ActiveControl = textNome;
                    throw new Exception("O nome do colaborador é obrigatório.");
                }

                if (string.IsNullOrEmpty(textCliente.Text))
                {
                    ActiveControl = textCliente;
                    throw new Exception("O cliente é obrigatório.");
                }


                if (string.IsNullOrEmpty(textSala.Text))
                {
                    ActiveControl = textSala;
                    throw new Exception("A sala de atendimento é obrigatória.");
                }


                if (string.IsNullOrEmpty(textSenha.Text))
                {
                    ActiveControl = textSenha;
                    throw new Exception("A senha de atendimento é obrigatória.");
                }
                    

            }
            catch (Exception ex)
            {
                result = false;
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            return result;
        }

        private void limpaDadosTela()
        {
            textNome.Text = string.Empty;
            textRg.Text = string.Empty;
            textCpf.Text = string.Empty;
            btnCancelarCliente.PerformClick();
            btnCancelarSala.PerformClick();
            textAndar.Text = "1";
            exame = null;
            textExame.Text = string.Empty;
            textSenha.Text = string.Empty;
            ActiveControl = textNome;
        }

    }
}
