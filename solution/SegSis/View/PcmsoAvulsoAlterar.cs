﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using SWS.Entidade;
using SWS.View.ViewHelper;
using SWS.View.Resources;
using SWS.Facade;
using SWS.ViewHelper;

namespace SWS.View
{
    public partial class PcmsoAvulsoAlterar : frmPcmsoAvulsoIncluir
    {
        
        public PcmsoAvulsoAlterar(Estudo estudo) :base()
        {
            InitializeComponent();
            this.estudo = estudo;

            cliente = estudo.VendedorCliente.Cliente;
            string cpfCnpj = cliente.tipoCliente() == 2 ? ValidaCampoHelper.FormataCpf(cliente.Cnpj) : ValidaCampoHelper.FormataCnpj(cliente.Cnpj);
            textCliente.Text = estudo.VendedorCliente.Cliente.RazaoSocial + " CPF/CNPJ: " + cpfCnpj ;

            clienteContratada = estudo.ClienteContratante;
            textContratada.Text = clienteContratada != null ? clienteContratada.RazaoSocial : String.Empty;

            textNumeroContrato.Text = estudo.NumContrato;
            textInicioContrato.Text = estudo.DataInicioContrato;
            textFimContrato.Text = estudo.DataFimContrato;

            textCodigo.Text = estudo.CodigoEstudo;

            ComboHelper.grauRisco(cbGrauRisco);
            cbGrauRisco.SelectedValue = estudo.GrauRisco;

            coordenador = estudo.Medicos.Find(x => String.Equals(x.Situacao, ApplicationConstants.ATIVO));

            if (coordenador != null)
                textCoordenador.Text = coordenador.Medico.Nome + " CRM: " + coordenador.Medico.Crm;

            if (estudo.DataCriacao != null)
            {
                dataCriacao.Checked = true;
                dataCriacao.Value = Convert.ToDateTime(estudo.DataCriacao);
            }

            if (estudo.DataVencimento != null)
            {
                dataValidade.Checked = true;
                dataValidade.Value = Convert.ToDateTime(estudo.DataVencimento);
            }

            textObra.Text = estudo.Obra;

            montaGridEstimativa();
            montaGridGhe();
            habilitaControles();
            btIcluir.Enabled = true;
            btIcluir.Text = "Alterar";
            btCliente.Enabled = false;
            btContratada.Enabled = false;
            btCoordenador.Enabled = true;
            

        }

        protected override void btCoordenador_Click(object sender, EventArgs e)
        {
            try
            {
                frmPcmsoMedico selecionarMedico = new frmPcmsoMedico();
                selecionarMedico.ShowDialog();

                EstudoFacade pcmsoFacade = EstudoFacade.getInstance();

                this.Cursor = Cursors.WaitCursor;

                if (selecionarMedico.Medico != null)
                {

                    /* verificando se o médido já está presente na coleção */

                    if (estudo.Medicos.Count == 0)
                    {
                        coordenador = pcmsoFacade.insertMedicoEstudo(new MedicoEstudo(estudo, selecionarMedico.Medico, true, ApplicationConstants.ATIVO));
                        textCoordenador.Text = coordenador.Medico.Nome + " CRM: " + coordenador.Medico.Crm;
                    }

                    else if (!estudo.Medicos.Exists(x => x.Medico.Id == selecionarMedico.Medico.Id))
                    {
                        coordenador.Situacao = ApplicationConstants.DESATIVADO;
                        pcmsoFacade.updateMedicoEstudo(coordenador);
                        coordenador = pcmsoFacade.insertMedicoEstudo(new MedicoEstudo(estudo, selecionarMedico.Medico, true, String.Empty));
                        textCoordenador.Text = coordenador.Medico.Nome + " CRM: " + coordenador.Medico.Crm;
                    }
                    else
                    {
                        MedicoEstudo coordenadorNovo = estudo.Medicos.Find(x => x.Medico.Id == selecionarMedico.Medico.Id);

                        if (!String.Equals(coordenadorNovo.Situacao, ApplicationConstants.ATIVO))
                        {
                            coordenador.Situacao = ApplicationConstants.DESATIVADO;
                            pcmsoFacade.updateMedicoEstudo(coordenador);
                            coordenadorNovo.Situacao = ApplicationConstants.ATIVO;
                            coordenador = pcmsoFacade.updateMedicoEstudo(coordenadorNovo);
                            textCoordenador.Text = coordenador.Medico.Nome + " CRM: " + coordenador.Medico.Crm;

                        }

                    }


                }

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }

        protected override void btn_pcmso_incluir_Click(object sender, EventArgs e)
        {
            try
            {
                EstudoFacade pcmsoFacade = EstudoFacade.getInstance();

                this.Cursor = Cursors.WaitCursor;

                estudo.DataCriacao = dataCriacao.Checked ? Convert.ToDateTime(dataCriacao.Text) : (DateTime?)null;
                estudo.DataVencimento = dataValidade.Checked ? Convert.ToDateTime(dataValidade.Text) : (DateTime?)null;
                estudo.GrauRisco = ((SelectItem)cbGrauRisco.SelectedItem).Valor;
                estudo.NumContrato = textNumeroContrato.Text;
                estudo.DataInicioContrato = textInicioContrato.Text;
                estudo.DataFimContrato = textFimContrato.Text;
                estudo.Obra = textObra.Text.Trim();

                pcmsoFacade.updatePcmso(estudo);
                MessageBox.Show("PCMSO alterado com sucesso", "Confirmação", MessageBoxButtons.OK, MessageBoxIcon.Information);
                this.Close();

            }
            catch (Exception Ex)
            {
                MessageBox.Show(Ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }


    }
}
