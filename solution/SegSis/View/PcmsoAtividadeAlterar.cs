﻿using SWS.Entidade;
using SWS.Facade;
using SWS.View.ViewHelper;
using SWS.ViewHelper;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SWS.View
{
    public partial class frmPcmsoAtividadeAlterar : frmPcmsoAtividadeIncluir
    {
        protected CronogramaAtividade cronogramaAtividade;
        
        public frmPcmsoAtividadeAlterar(CronogramaAtividade cronogramaAtividade)
        {
            InitializeComponent();
            this.cronogramaAtividade = cronogramaAtividade;
            this.atividade = cronogramaAtividade.Atividade;

            btnAtividadeIncluir.Enabled = false;
            btnAtividadeExcluir.Enabled = false;

            textAtividade.Text = cronogramaAtividade.Atividade.Descricao;

            ComboHelper.comboAno(cbAnoRealizacao);
            ComboHelper.startComboMesSave(cbMesRealizacao);

            cbAnoRealizacao.SelectedValue = cronogramaAtividade.AnoRealizado;
            cbMesRealizacao.SelectedValue = cronogramaAtividade.MesRealizado;

            textPublico.Text = cronogramaAtividade.PublicoAlvo;
            textRegistro.Text = cronogramaAtividade.Evidencia;

        }

        protected override void btnGravar_Click(object sender, EventArgs e)
        {
            try
            {
                if (validaDadosTela())
                {
                    /* criando o conograma */
                    EstudoFacade pcmsoFacade = EstudoFacade.getInstance();

                    cronogramaAtividade.AnoRealizado = ((SelectItem)cbAnoRealizacao.SelectedItem).Valor;
                    cronogramaAtividade.MesRealizado = ((SelectItem)cbMesRealizacao.SelectedItem).Valor;
                    cronogramaAtividade.PublicoAlvo = textPublico.Text.Trim();
                    cronogramaAtividade.Evidencia = textRegistro.Text.Trim();

                    pcmsoFacade.updateCronogramaAtividade(cronogramaAtividade);

                    MessageBox.Show("Atividade alterada com sucesso.", "Confirmação", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    this.Close();

                }

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }
    }
}
