﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using SWS.Facade;
using SWS.Entidade;
using SWS.Excecao;
using SWS.View.ViewHelper;
using System.Text.RegularExpressions;
using SWS.View.Resources;

namespace SWS.View
{
    public partial class frmCronogramaAtividadeIncluir : BaseFormConsulta
    {
        private static String Msg01 = "Atenção";
        private static String Msg02 = "Padrão AAAA";

        public Atividade atividade;

        private Cronograma cronograma;
        private CronogramaAtividade cronogramaAtividade;

        public CronogramaAtividade CronogramaAtividade
        {
            get { return cronogramaAtividade; }
            set { cronogramaAtividade = value; }
        }

        public frmCronogramaAtividadeIncluir(Cronograma cronograma)
        {
            InitializeComponent();
            this.cronograma = cronograma;
            ComboHelper.startComboMesSave(cb_mes);
        }

        private void btn_fechar_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btn_limpar_Click(object sender, EventArgs e)
        {
            ComboHelper.startComboMesSave(cb_mes);
            text_ano.Text = String.Empty;
            text_publico.Text = String.Empty;
            text_evidencia.Text = String.Empty;
        }

        private void btn_atividade_Click(object sender, EventArgs e)
        {
            frm_ppraAtvidadeCronogramaAtividade formAtividadeCronogramaAtividade = new frm_ppraAtvidadeCronogramaAtividade();
            formAtividadeCronogramaAtividade.ShowDialog();

            if (formAtividadeCronogramaAtividade.getAtividade() != null)
            {
                atividade = formAtividadeCronogramaAtividade.getAtividade();
                text_atividade.Text = atividade.Descricao;
            }
        }

        private void btn_incluir_Click(object sender, EventArgs e)
        {
            try
            {
                this.IncluirAtividaePPRAErrorProvider.Clear();

                if (ValidaCamposObrigatorios())
                {
                    cronogramaAtividade = new CronogramaAtividade(atividade, cronograma, Convert.ToString(cb_mes.SelectedValue), text_ano.Text, text_publico.Text.ToUpper(), text_evidencia.Text.ToUpper());
                    this.Close();
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void text_ano_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = ValidaCampoHelper.FormataDigitacaoNumero(sender, e, text_ano.Text);
        }

        private Boolean ValidaCamposObrigatorios()
        {
            Boolean retorno = Convert.ToBoolean(true);

            if (!String.IsNullOrEmpty(this.text_ano.Text) && text_ano.Text.Length != 4)
            {
                MessageBox.Show(Msg02, Msg01);
                retorno = Convert.ToBoolean(false);
            }

            if (String.IsNullOrEmpty(this.text_atividade.Text))
            {
                this.IncluirAtividaePPRAErrorProvider.SetError(this.text_atividade, "Atividade é obrigatória!");
                retorno = Convert.ToBoolean(false);
            }

            return retorno;
        }
    }
}
