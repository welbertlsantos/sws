﻿namespace SWS.View
{
    partial class Frm_Login
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.dlg_login = new System.Windows.Forms.Label();
            this.bt_limpar = new System.Windows.Forms.Button();
            this.bt_ok = new System.Windows.Forms.Button();
            this.text_login = new System.Windows.Forms.TextBox();
            this.text_senha = new System.Windows.Forms.MaskedTextBox();
            this.grb_login = new System.Windows.Forms.GroupBox();
            this.cbFilial = new SWS.ComboBoxWithBorder();
            this.lblEmpresa = new System.Windows.Forms.TextBox();
            this.flpLoginAcao = new System.Windows.Forms.FlowLayoutPanel();
            this.btnCancelar = new System.Windows.Forms.Button();
            this.lblSenha = new System.Windows.Forms.TextBox();
            this.lblLogin = new System.Windows.Forms.TextBox();
            this.stbarr = new System.Windows.Forms.StatusStrip();
            this.stversao = new System.Windows.Forms.ToolStripStatusLabel();
            this.lblEsqueciSenha = new System.Windows.Forms.LinkLabel();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.grb_login.SuspendLayout();
            this.flpLoginAcao.SuspendLayout();
            this.stbarr.SuspendLayout();
            this.SuspendLayout();
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = global::SWS.Properties.Resources.logoSistema;
            this.pictureBox1.Location = new System.Drawing.Point(30, 25);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(365, 138);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 11;
            this.pictureBox1.TabStop = false;
            // 
            // dlg_login
            // 
            this.dlg_login.AutoSize = true;
            this.dlg_login.Font = new System.Drawing.Font("Verdana", 8.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dlg_login.ForeColor = System.Drawing.Color.Red;
            this.dlg_login.Location = new System.Drawing.Point(33, 137);
            this.dlg_login.Name = "dlg_login";
            this.dlg_login.Size = new System.Drawing.Size(0, 13);
            this.dlg_login.TabIndex = 6;
            // 
            // bt_limpar
            // 
            this.bt_limpar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.bt_limpar.Image = global::SWS.Properties.Resources.vassoura;
            this.bt_limpar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.bt_limpar.Location = new System.Drawing.Point(84, 3);
            this.bt_limpar.Name = "bt_limpar";
            this.bt_limpar.Size = new System.Drawing.Size(75, 23);
            this.bt_limpar.TabIndex = 5;
            this.bt_limpar.TabStop = false;
            this.bt_limpar.Text = "Limpar";
            this.bt_limpar.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.bt_limpar.UseVisualStyleBackColor = true;
            this.bt_limpar.Click += new System.EventHandler(this.bt_limpar_Click);
            // 
            // bt_ok
            // 
            this.bt_ok.BackColor = System.Drawing.Color.White;
            this.bt_ok.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.bt_ok.Image = global::SWS.Properties.Resources.fechar_ico;
            this.bt_ok.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.bt_ok.Location = new System.Drawing.Point(3, 3);
            this.bt_ok.Name = "bt_ok";
            this.bt_ok.Size = new System.Drawing.Size(75, 23);
            this.bt_ok.TabIndex = 4;
            this.bt_ok.Text = "Ok";
            this.bt_ok.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.bt_ok.UseVisualStyleBackColor = false;
            this.bt_ok.Click += new System.EventHandler(this.bt_ok_Click);
            // 
            // text_login
            // 
            this.text_login.BackColor = System.Drawing.Color.White;
            this.text_login.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.text_login.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.text_login.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.text_login.ForeColor = System.Drawing.SystemColors.WindowText;
            this.text_login.Location = new System.Drawing.Point(114, 32);
            this.text_login.MaxLength = 30;
            this.text_login.Name = "text_login";
            this.text_login.Size = new System.Drawing.Size(208, 21);
            this.text_login.TabIndex = 1;
            // 
            // text_senha
            // 
            this.text_senha.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.text_senha.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.text_senha.Location = new System.Drawing.Point(114, 52);
            this.text_senha.Name = "text_senha";
            this.text_senha.Size = new System.Drawing.Size(208, 21);
            this.text_senha.TabIndex = 2;
            this.text_senha.UseSystemPasswordChar = true;
            // 
            // grb_login
            // 
            this.grb_login.Controls.Add(this.cbFilial);
            this.grb_login.Controls.Add(this.lblEmpresa);
            this.grb_login.Controls.Add(this.flpLoginAcao);
            this.grb_login.Controls.Add(this.lblSenha);
            this.grb_login.Controls.Add(this.lblLogin);
            this.grb_login.Controls.Add(this.text_login);
            this.grb_login.Controls.Add(this.text_senha);
            this.grb_login.Controls.Add(this.dlg_login);
            this.grb_login.Location = new System.Drawing.Point(30, 183);
            this.grb_login.Name = "grb_login";
            this.grb_login.Size = new System.Drawing.Size(365, 191);
            this.grb_login.TabIndex = 1;
            this.grb_login.TabStop = false;
            this.grb_login.Text = "Login";
            // 
            // cbFilial
            // 
            this.cbFilial.BackColor = System.Drawing.Color.LightGray;
            this.cbFilial.BorderColor = System.Drawing.Color.Black;
            this.cbFilial.BorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid;
            this.cbFilial.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbFilial.FormattingEnabled = true;
            this.cbFilial.Location = new System.Drawing.Point(114, 72);
            this.cbFilial.Name = "cbFilial";
            this.cbFilial.Size = new System.Drawing.Size(208, 23);
            this.cbFilial.TabIndex = 3;
            // 
            // lblEmpresa
            // 
            this.lblEmpresa.BackColor = System.Drawing.Color.LightGray;
            this.lblEmpresa.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblEmpresa.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblEmpresa.ForeColor = System.Drawing.SystemColors.WindowText;
            this.lblEmpresa.Location = new System.Drawing.Point(33, 72);
            this.lblEmpresa.MaxLength = 30;
            this.lblEmpresa.Multiline = true;
            this.lblEmpresa.Name = "lblEmpresa";
            this.lblEmpresa.ReadOnly = true;
            this.lblEmpresa.Size = new System.Drawing.Size(82, 23);
            this.lblEmpresa.TabIndex = 10;
            this.lblEmpresa.TabStop = false;
            this.lblEmpresa.Text = "Empresa";
            // 
            // flpLoginAcao
            // 
            this.flpLoginAcao.Controls.Add(this.bt_ok);
            this.flpLoginAcao.Controls.Add(this.bt_limpar);
            this.flpLoginAcao.Controls.Add(this.btnCancelar);
            this.flpLoginAcao.Location = new System.Drawing.Point(33, 105);
            this.flpLoginAcao.Name = "flpLoginAcao";
            this.flpLoginAcao.Size = new System.Drawing.Size(289, 28);
            this.flpLoginAcao.TabIndex = 9;
            // 
            // btnCancelar
            // 
            this.btnCancelar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnCancelar.Image = global::SWS.Properties.Resources.close;
            this.btnCancelar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnCancelar.Location = new System.Drawing.Point(165, 3);
            this.btnCancelar.Name = "btnCancelar";
            this.btnCancelar.Size = new System.Drawing.Size(75, 23);
            this.btnCancelar.TabIndex = 6;
            this.btnCancelar.TabStop = false;
            this.btnCancelar.Text = "&Cancelar";
            this.btnCancelar.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnCancelar.UseVisualStyleBackColor = true;
            this.btnCancelar.Click += new System.EventHandler(this.btnCancelar_Click);
            // 
            // lblSenha
            // 
            this.lblSenha.BackColor = System.Drawing.Color.LightGray;
            this.lblSenha.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblSenha.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSenha.ForeColor = System.Drawing.SystemColors.WindowText;
            this.lblSenha.Location = new System.Drawing.Point(33, 52);
            this.lblSenha.MaxLength = 30;
            this.lblSenha.Name = "lblSenha";
            this.lblSenha.ReadOnly = true;
            this.lblSenha.Size = new System.Drawing.Size(82, 21);
            this.lblSenha.TabIndex = 8;
            this.lblSenha.TabStop = false;
            this.lblSenha.Text = "Senha";
            // 
            // lblLogin
            // 
            this.lblLogin.BackColor = System.Drawing.Color.LightGray;
            this.lblLogin.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblLogin.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblLogin.ForeColor = System.Drawing.SystemColors.WindowText;
            this.lblLogin.Location = new System.Drawing.Point(33, 32);
            this.lblLogin.MaxLength = 30;
            this.lblLogin.Name = "lblLogin";
            this.lblLogin.ReadOnly = true;
            this.lblLogin.Size = new System.Drawing.Size(82, 21);
            this.lblLogin.TabIndex = 7;
            this.lblLogin.TabStop = false;
            this.lblLogin.Text = "Login";
            // 
            // stbarr
            // 
            this.stbarr.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.stversao});
            this.stbarr.Location = new System.Drawing.Point(0, 398);
            this.stbarr.Name = "stbarr";
            this.stbarr.Size = new System.Drawing.Size(419, 22);
            this.stbarr.TabIndex = 14;
            this.stbarr.Text = "statusStrip1";
            // 
            // stversao
            // 
            this.stversao.Name = "stversao";
            this.stversao.Size = new System.Drawing.Size(44, 17);
            this.stversao.Text = "Versão:";
            this.stversao.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lblEsqueciSenha
            // 
            this.lblEsqueciSenha.AutoSize = true;
            this.lblEsqueciSenha.Location = new System.Drawing.Point(287, 377);
            this.lblEsqueciSenha.Name = "lblEsqueciSenha";
            this.lblEsqueciSenha.Size = new System.Drawing.Size(108, 13);
            this.lblEsqueciSenha.TabIndex = 5;
            this.lblEsqueciSenha.TabStop = true;
            this.lblEsqueciSenha.Text = "Esqueci minha senha";
            this.lblEsqueciSenha.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.lblEsqueciSenha_LinkClicked);
            // 
            // Frm_Login
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.Control;
            this.ClientSize = new System.Drawing.Size(419, 420);
            this.ControlBox = false;
            this.Controls.Add(this.lblEsqueciSenha);
            this.Controls.Add(this.stbarr);
            this.Controls.Add(this.grb_login);
            this.Controls.Add(this.pictureBox1);
            this.Icon = global::SWS.Properties.Resources.icone;
            this.KeyPreview = true;
            this.Name = "Frm_Login";
            this.Text = "SISTEMA SWS - LOGIN";
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Frm_Login_KeyDown);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.grb_login.ResumeLayout(false);
            this.grb_login.PerformLayout();
            this.flpLoginAcao.ResumeLayout(false);
            this.stbarr.ResumeLayout(false);
            this.stbarr.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox text_login;
        private System.Windows.Forms.MaskedTextBox text_senha;
        private System.Windows.Forms.Button bt_limpar;
        private System.Windows.Forms.Button bt_ok;
        private System.Windows.Forms.Label dlg_login;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.GroupBox grb_login;
        private System.Windows.Forms.StatusStrip stbarr;
        private System.Windows.Forms.ToolStripStatusLabel stversao;
        private System.Windows.Forms.LinkLabel lblEsqueciSenha;
        private System.Windows.Forms.TextBox lblSenha;
        private System.Windows.Forms.TextBox lblLogin;
        private System.Windows.Forms.FlowLayoutPanel flpLoginAcao;
        private ComboBoxWithBorder cbFilial;
        private System.Windows.Forms.TextBox lblEmpresa;
        private System.Windows.Forms.Button btnCancelar;
    }
}

