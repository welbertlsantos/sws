﻿namespace SWS.View
{
    partial class frmContratoIncluirSelecionarTipo
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmContratoIncluirSelecionarTipo));
            this.btnIncluirContrato = new System.Windows.Forms.Button();
            this.lblIncluirContrato = new System.Windows.Forms.Label();
            this.lblIncluirProposta = new System.Windows.Forms.Label();
            this.btnIncluirProposta = new System.Windows.Forms.Button();
            this.lblIncluirAditivo = new System.Windows.Forms.Label();
            this.btnIncluirAditivo = new System.Windows.Forms.Button();
            this.tpTipoSelecionar = new System.Windows.Forms.ToolTip(this.components);
            this.SuspendLayout();
            // 
            // btnIncluirContrato
            // 
            this.btnIncluirContrato.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnIncluirContrato.Image = global::SWS.Properties.Resources.contrato;
            this.btnIncluirContrato.Location = new System.Drawing.Point(35, 22);
            this.btnIncluirContrato.Name = "btnIncluirContrato";
            this.btnIncluirContrato.Size = new System.Drawing.Size(123, 68);
            this.btnIncluirContrato.TabIndex = 0;
            this.tpTipoSelecionar.SetToolTip(this.btnIncluirContrato, "Clique aqui para incluir um contrato novo.");
            this.btnIncluirContrato.UseVisualStyleBackColor = true;
            this.btnIncluirContrato.Click += new System.EventHandler(this.btnIncluirContrato_Click);
            // 
            // lblIncluirContrato
            // 
            this.lblIncluirContrato.AutoSize = true;
            this.lblIncluirContrato.Location = new System.Drawing.Point(53, 93);
            this.lblIncluirContrato.Name = "lblIncluirContrato";
            this.lblIncluirContrato.Size = new System.Drawing.Size(78, 13);
            this.lblIncluirContrato.TabIndex = 1;
            this.lblIncluirContrato.Text = "Incluir Contrato";
            // 
            // lblIncluirProposta
            // 
            this.lblIncluirProposta.AutoSize = true;
            this.lblIncluirProposta.Location = new System.Drawing.Point(218, 93);
            this.lblIncluirProposta.Name = "lblIncluirProposta";
            this.lblIncluirProposta.Size = new System.Drawing.Size(80, 13);
            this.lblIncluirProposta.TabIndex = 3;
            this.lblIncluirProposta.Text = "Incluir Proposta";
            // 
            // btnIncluirProposta
            // 
            this.btnIncluirProposta.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnIncluirProposta.Image = global::SWS.Properties.Resources.proposta;
            this.btnIncluirProposta.Location = new System.Drawing.Point(200, 22);
            this.btnIncluirProposta.Name = "btnIncluirProposta";
            this.btnIncluirProposta.Size = new System.Drawing.Size(123, 68);
            this.btnIncluirProposta.TabIndex = 2;
            this.tpTipoSelecionar.SetToolTip(this.btnIncluirProposta, "Clique aqui para incluir uma proposta.");
            this.btnIncluirProposta.UseVisualStyleBackColor = true;
            this.btnIncluirProposta.Click += new System.EventHandler(this.btnIncluirProposta_Click);
            // 
            // lblIncluirAditivo
            // 
            this.lblIncluirAditivo.AutoSize = true;
            this.lblIncluirAditivo.Location = new System.Drawing.Point(389, 93);
            this.lblIncluirAditivo.Name = "lblIncluirAditivo";
            this.lblIncluirAditivo.Size = new System.Drawing.Size(70, 13);
            this.lblIncluirAditivo.TabIndex = 5;
            this.lblIncluirAditivo.Text = "Incluir Aditivo";
            // 
            // btnIncluirAditivo
            // 
            this.btnIncluirAditivo.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnIncluirAditivo.Image = global::SWS.Properties.Resources.aditivo;
            this.btnIncluirAditivo.Location = new System.Drawing.Point(362, 22);
            this.btnIncluirAditivo.Name = "btnIncluirAditivo";
            this.btnIncluirAditivo.Size = new System.Drawing.Size(123, 68);
            this.btnIncluirAditivo.TabIndex = 4;
            this.tpTipoSelecionar.SetToolTip(this.btnIncluirAditivo, "Clique aqui para incluir um aditivo para um contrato.");
            this.btnIncluirAditivo.UseVisualStyleBackColor = true;
            this.btnIncluirAditivo.Click += new System.EventHandler(this.btnIncluirAditivo_Click);
            // 
            // frmContratoIncluirSelecionarTipo
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(555, 132);
            this.Controls.Add(this.lblIncluirAditivo);
            this.Controls.Add(this.btnIncluirAditivo);
            this.Controls.Add(this.lblIncluirProposta);
            this.Controls.Add(this.btnIncluirProposta);
            this.Controls.Add(this.lblIncluirContrato);
            this.Controls.Add(this.btnIncluirContrato);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmContratoIncluirSelecionarTipo";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "SELECIONAR TIPO";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnIncluirContrato;
        private System.Windows.Forms.Label lblIncluirContrato;
        private System.Windows.Forms.ToolTip tpTipoSelecionar;
        private System.Windows.Forms.Label lblIncluirProposta;
        private System.Windows.Forms.Button btnIncluirProposta;
        private System.Windows.Forms.Label lblIncluirAditivo;
        private System.Windows.Forms.Button btnIncluirAditivo;
    }
}