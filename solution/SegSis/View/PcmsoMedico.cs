﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using SWS.Entidade;
using SWS.View.Resources;
using SWS.Facade;


namespace SWS.View
{
    public partial class frmPcmsoMedico : SWS.View.frmTemplateConsulta
    {

        private Medico medico;

        public Medico Medico
        {
            get { return medico; }
            set { medico = value; }
        }
        
        public frmPcmsoMedico()
        {
            InitializeComponent();
            validaPermissoes();
            ActiveControl = textNome;
        }

        private void btConfirma_Click(object sender, EventArgs e)
        {
            try
            {
                this.Cursor = Cursors.WaitCursor;

                if (dgvMedico.CurrentRow == null)
                    throw new Exception("Selecione uma linha");

                EstudoFacade pcmsoFacade = EstudoFacade.getInstance();

                medico = pcmsoFacade.findMedicoById((Int64)dgvMedico.CurrentRow.Cells[0].Value);
                this.Close();

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                Cursor = Cursors.Default;
            }
        }

        private void btBuscar_Click(object sender, EventArgs e)
        {
            montaGrid();
        }

        private void btNovo_Click(object sender, EventArgs e)
        {
            try
            {
                frmMedicoIncluir medicoIncluir = new frmMedicoIncluir();
                medicoIncluir.ShowDialog();

                if (medicoIncluir.Medico != null)
                {
                    this.medico = medicoIncluir.Medico;
                    this.Close();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btFechar_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void validaPermissoes()
        {
            PermissionamentoFacade permissionamento = PermissionamentoFacade.getInstance();
            btNovo.Enabled = permissionamento.hasPermission("MEDICO", "INCLUIR");
        }

        private void montaGrid()
        {
            try
            {
                EstudoFacade pcmsoFacade = EstudoFacade.getInstance();
                
                Cursor = Cursors.WaitCursor;

                dgvMedico.Columns.Clear();

                DataSet ds = pcmsoFacade.findMedicoByFilter(new Medico(null, textNome.Text, String.Empty, ApplicationConstants.ATIVO, string.Empty, string.Empty, string.Empty, string.Empty, string.Empty, string.Empty, string.Empty, string.Empty, string.Empty, null, string.Empty, string.Empty, string.Empty, null, string.Empty, string.Empty, string.Empty));

                dgvMedico.DataSource = ds.Tables[0].DefaultView;

                dgvMedico.Columns[0].HeaderText = "id_medico";
                dgvMedico.Columns[0].Visible = false;

                dgvMedico.Columns[1].HeaderText = "Nome";

                dgvMedico.Columns[2].HeaderText = "CRM";

                dgvMedico.Columns[3].HeaderText = "Situacao";
                dgvMedico.Columns[3].Visible = false;

                dgvMedico.Columns[4].HeaderText = "Telefone1";
                dgvMedico.Columns[4].Visible = false;

                dgvMedico.Columns[5].HeaderText = "Telefone2";
                dgvMedico.Columns[5].Visible = false;

                dgvMedico.Columns[6].HeaderText = "Email";
                dgvMedico.Columns[6].Visible = false;

                dgvMedico.Columns[7].HeaderText = "Endereco";
                dgvMedico.Columns[7].Visible = false;

                dgvMedico.Columns[8].HeaderText = "Numero";
                dgvMedico.Columns[8].Visible = false;

                dgvMedico.Columns[9].HeaderText = "Complemento";
                dgvMedico.Columns[9].Visible = false;

                dgvMedico.Columns[10].HeaderText = "Bairro";
                dgvMedico.Columns[10].Visible = false;

                dgvMedico.Columns[11].HeaderText = "CEP";
                dgvMedico.Columns[11].Visible = false;

                dgvMedico.Columns[12].HeaderText = "Cidade";
                dgvMedico.Columns[12].Visible = false;

                dgvMedico.Columns[13].HeaderText = "UF";
                dgvMedico.Columns[13].Visible = false;

                dgvMedico.Columns[14].HeaderText = "PISPASEP";
                dgvMedico.Columns[14].Visible = false;
                
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                Cursor = Cursors.Default;
            }
        }

        private void dgvMedico_DataBindingComplete(object sender, DataGridViewBindingCompleteEventArgs e)
        {
            DataGridView dgv = sender as DataGridView;
            dgv.ClearSelection();
        }

        private void dgvMedico_CellMouseDoubleClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            try
            {
                btConfirma.PerformClick();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }

    }
}
