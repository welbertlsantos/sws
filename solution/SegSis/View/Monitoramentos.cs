﻿using SWS.Entidade;
using SWS.Facade;
using SWS.View.ViewHelper;
using SWS.ViewHelper;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace SWS.View
{
    
    public partial class frmMonitoramentos : frmTemplate
    {
        private Monitoramento monitoramento { get; set; }
        private Cliente cliente { get; set; }
        private List<Monitoramento> monitoramentos = new List<Monitoramento>();
        private ClienteFuncaoFuncionario clienteFuncaoFuncionario { get; set; }

                
        public frmMonitoramentos()
        {
            InitializeComponent();
            ComboHelper.comboMes(cbMesMonitoramento);
            ComboHelper.comboAno(cbAnoMonitoramento);

            /* setando valores default para os combos */

            cbMesMonitoramento.SelectedValue = DateTime.Now.Month.ToString();
            cbAnoMonitoramento.SelectedValue = DateTime.Now.Year.ToString();


            validaPermissoes();
        }

        private void validaPermissoes()
        {
            PermissionamentoFacade permissionamentoFacade = PermissionamentoFacade.getInstance();

            btnIncluir.Enabled = permissionamentoFacade.hasPermission("MONITORAMENTO", "INCLUIR");
            btnPesquisar.Enabled = permissionamentoFacade.hasPermission("MONITORAMENTO", "PESQUISAR");
            btnAlterar.Enabled = permissionamentoFacade.hasPermission("MONITORAMENTO", "ALTERAR");
            btnExcluir.Enabled = permissionamentoFacade.hasPermission("MONITORAMENTO", "EXCLUIR");
        }

        private void btnIncluir_Click(object sender, EventArgs e)
        {
            frmMonitoramento incluirMonitoramento = new frmMonitoramento();
            incluirMonitoramento.ShowDialog();

        }

        private void btnPesquisar_Click(object sender, EventArgs e)
        {
            try
            {
                if (cliente == null && clienteFuncaoFuncionario == null)
                {
                    if (MessageBox.Show("Você não selecionou nenhum filtro. A pesquisa pode demorar um pouco. Deseja continuar?", "Atenção", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                    {
                        montaGridMonitoramento();
                    }
                }
                else
                {
                    montaGridMonitoramento();
                }
                
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }

        private void btnAlterar_Click(object sender, EventArgs e)
        {
            try
            {
                if (dgvMonitoramento.CurrentRow == null)
                    throw new Exception("Selecione uma linha.");

                EsocialFacade esocialFacade = EsocialFacade.getInstance();

                frmMonitoramentoFuncionarioAlterar alterarMonitoramento = new frmMonitoramentoFuncionarioAlterar(esocialFacade.findMonitoramentoById((long)dgvMonitoramento.CurrentRow.Cells[0].Value));
                alterarMonitoramento.ShowDialog();

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }

        private void btnExcluir_Click(object sender, EventArgs e)
        {
            try
            {
                if (dgvMonitoramento.CurrentRow == null)
                    throw new Exception("Selecione uma linha.");

                EsocialFacade esocialFacace = EsocialFacade.getInstance();
                Monitoramento monitoramentoExcluir = new Monitoramento();

                if (MessageBox.Show("Deseja excluir o monitoramento selecionado?", "Atenção", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                {
                    monitoramentoExcluir.Id = (long)dgvMonitoramento.CurrentRow.Cells[0].Value;
                    esocialFacace.deleteMonitoramento(monitoramentoExcluir);
                    MessageBox.Show("Monitoramento excluído com sucesso", "Confirmação", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    monitoramentoExcluir = monitoramentos.Find(x => x.Id == (long)dgvMonitoramento.CurrentRow.Cells[0].Value);
                    monitoramentos.Remove(monitoramentoExcluir);
                    montaGridMonitoramento();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }

        private void btnFechar_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnCliente_Click(object sender, EventArgs e)
        {
            try
            {
                frmClienteSelecionar selecionarCliente = new frmClienteSelecionar(null, null, false, null, null, false, null, null, true);
                selecionarCliente.ShowDialog();

                if (selecionarCliente.Cliente != null)
                {
                    this.cliente = new Cliente();
                    this.cliente = selecionarCliente.Cliente;
                    this.textCliente.Text = this.cliente.RazaoSocial + " - " + ((int)this.cliente.tipoCliente() == 1 ? ValidaCampoHelper.FormataCnpj(this.cliente.Cnpj) : ValidaCampoHelper.FormataCpf(this.cliente.Cnpj));
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }

        private void btnExcluirCliente_Click(object sender, EventArgs e)
        {
            try
            {
                if (this.cliente == null) throw new Exception("Selecione primeiro o cliente.");
                this.cliente = null;
                textCliente.Text = string.Empty;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btnIncluirFuncionarioCliente_Click(object sender, EventArgs e)
        {
            try
            {
                if (cliente == null)
                    throw new Exception("Você deve selecionar primeiro um cliente.");

                /* selecionando o funcionário do cliente */

                frmClienteFuncaoFuncionarioPesquisa selecionarFuncionario = new frmClienteFuncaoFuncionarioPesquisa(cliente);
                selecionarFuncionario.ShowDialog();

                if (selecionarFuncionario.ClienteFuncaoFuncionario != null)
                {
                    this.clienteFuncaoFuncionario = selecionarFuncionario.ClienteFuncaoFuncionario;
                    textFuncionarioCliente.Text = clienteFuncaoFuncionario.Funcionario.Nome;
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }

        private void montaGridMonitoramento()
        {
            try
            {
                this.Cursor = Cursors.WaitCursor;
                dgvMonitoramento.Columns.Clear();
                dgvMonitoramento.ColumnCount = 5;

                dgvMonitoramento.Columns[0].HeaderText = "idMonitoramento";
                dgvMonitoramento.Columns[0].Visible = false;

                dgvMonitoramento.Columns[1].HeaderText = "Periodo";
                dgvMonitoramento.Columns[2].HeaderText = "Funcionario";
                dgvMonitoramento.Columns[3].HeaderText = "Cliente";
                dgvMonitoramento.Columns[4].HeaderText = "CNPJ";

                EsocialFacade esocialFacade = EsocialFacade.getInstance();

                Monitoramento monitoramentoBusca = new Monitoramento();
                monitoramentoBusca.Cliente = cliente;
                monitoramentoBusca.ClienteFuncaoFuncionario = clienteFuncaoFuncionario;
                int ano = Convert.ToInt32(((SelectItem)cbAnoMonitoramento.SelectedItem).Valor);
                int mes = Convert.ToInt32(((SelectItem)cbMesMonitoramento.SelectedItem).Valor);

                monitoramentoBusca.Periodo = new DateTime(ano, mes , DateTime.DaysInMonth(ano, mes));

                List<Monitoramento> monitoramentosFiltrados = esocialFacade.findByFilter(monitoramentoBusca);

                foreach (Monitoramento monitoramentoFilter in monitoramentosFiltrados)
                    dgvMonitoramento.Rows.Add(
                        monitoramentoFilter.Id,
                        String.Format("{0:dd/MM/yyyy}", monitoramentoFilter.Periodo),
                        monitoramentoFilter.ClienteFuncaoFuncionario != null ? monitoramentoFilter.ClienteFuncaoFuncionario.Funcionario.Nome : monitoramentoFilter.ClienteFuncionario.Funcionario.Nome,
                        monitoramentoFilter.Cliente.RazaoSocial,
                        monitoramentoFilter.Cliente.tipoCliente() == (int)1 ? ValidaCampoHelper.FormataCnpj(monitoramentoFilter.Cliente.Cnpj) : ValidaCampoHelper.FormataCpf(monitoramentoFilter.Cliente.Cnpj));

                textTotal.Text = monitoramentosFiltrados.Count.ToString();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }

        }

        private void btnExcluirFuncionarioCliente_Click(object sender, EventArgs e)
        {
            try
            {
                if (clienteFuncaoFuncionario == null)
                    throw new Exception("Selecione primeiro um funcionário.");

                clienteFuncaoFuncionario = null;
                textFuncionarioCliente.Text = string.Empty;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }

    }
}
