﻿using SWS.Entidade;
using SWS.Facade;
using SWS.View.Resources;
using SWS.View.ViewHelper;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace SWS.View
{
    public partial class frmAtendimentoLaudar : frmTemplate
    {
        
        private Aso atendimento;
        private LinkedList<ItemFilaAtendimento> listaItensAtendimentoFinalizados;

        private bool concluiLaudo;

        private static string msg2 = "Marque essa opção para colocar o laudo como normal." + System.Environment.NewLine +
            "Não esqueça de colocar a observação."; 
        private static string msg3 = "Marque essa opção para colocar o laudo como alterado. " + System.Environment.NewLine +
            "Não esqueça de colocar a observação.";
        private static string msg4 = "Marque essa opção para colocar o laudo como estável. " + System.Environment.NewLine +
            "Não esqueça de colocar a observação.";
        private static string msg5 = "Marque essa opção para colocar o laudo como agravamento. " + System.Environment.NewLine +
            "Não esqueça de colocar a observação.";


        public frmAtendimentoLaudar(Aso atendimento, LinkedList<ItemFilaAtendimento> listaItensAtendimentoFinalizados, Boolean concluiLaudo)
        {
            InitializeComponent();
            this.atendimento = atendimento;
            this.listaItensAtendimentoFinalizados = listaItensAtendimentoFinalizados;
            this.concluiLaudo = concluiLaudo;

            /*
             * preenchendo componentes na tela */

            this.text_codigoAso.Text = ValidaCampoHelper.RetornaCodigoAtendimentoFormatado(atendimento.Codigo);
            this.text_nomeFuncionario.Text = atendimento.ClienteFuncaoFuncionario.Funcionario.Nome;
            this.text_rgFuncionario.Text = atendimento.ClienteFuncaoFuncionario.Funcionario.Rg;
            this.text_cnpj.Text = atendimento.ClienteFuncaoFuncionario.ClienteFuncao.Cliente.Cnpj.Length == 14 ? ValidaCampoHelper.FormataCnpj(atendimento.Cliente.Cnpj) : ValidaCampoHelper.FormataCpf(atendimento.Cliente.Cnpj);
            this.text_razaoCliente.Text = atendimento.Cliente.RazaoSocial;
            this.text_dataAso.Text = String.Format("{0:dd/MM/yyyy}", atendimento.DataElaboracao.ToString());

            string conclusao = String.Equals(atendimento.Conclusao, ApplicationConstants.LAUDOAPTO) ? ApplicationConstants.APTO : String.Equals(atendimento.Conclusao, ApplicationConstants.LAUDOINAPTO) ? ApplicationConstants.INAPTO : string.Empty;

            text_conclusao.Text = conclusao;
            text_observacao.Text = atendimento.ObservacaoAso;

            montaGridExame();
        }

        private void btn_gravar_Click(object sender, EventArgs e)
        {
            try
            {
                this.Cursor = Cursors.WaitCursor;

                if (validaDadosTela())
                {
                    /* verificando o que voi laudado.
                     * Caso tenha sigo laudado o exame pcmso então a coleção
                     * gheFonteAgenteExameAso presente no aso será alterada com as 
                     * informações do laudo.
                     * caso tenha sido laudado o exame extra então a coleção
                     * ClienteFuncaoExameAso presente no aso será alterada com as 
                     * informações do laudo. */

                    bool result = false;

                    AsoFacade asoFacade = AsoFacade.getInstance();

                    foreach (DataGridViewRow dvRow in dgv_exame.Rows)
                    {
                        String laudo = Convert.ToBoolean(dvRow.Cells[3].Value) == true ? ApplicationConstants.NORMAL : Convert.ToBoolean(dvRow.Cells[4].Value) == true ? ApplicationConstants.ALTERADO : Convert.ToBoolean(dvRow.Cells[5].Value) == true ? ApplicationConstants.ESTAVEL : Convert.ToBoolean(dvRow.Cells[6].Value) == true ? ApplicationConstants.AGRAVAMENTO : string.Empty;

                        if (String.Equals(dvRow.Cells[2].Value, ApplicationConstants.EXAME_PCMSO))
                        {
                            GheFonteAgenteExameAso gfaea = new GheFonteAgenteExameAso( Convert.ToInt64(dvRow.Cells[0].Value), null, null, null, null, false, false, String.Empty, false, null, null, null, null, false, null, laudo, Convert.ToString(dvRow.Cells[7].Value).ToUpper(), false, false, null, null, false, false, dvRow.Cells[8].Value == DBNull.Value ? null : new Medico((long?)dvRow.Cells[8].Value, dvRow.Cells[9].Value.ToString(), dvRow.Cells[10].Value.ToString(), string.Empty, string.Empty, string.Empty, string.Empty, string.Empty, string.Empty, string.Empty, string.Empty, string.Empty, string.Empty, null, string.Empty, string.Empty, string.Empty, null, string.Empty, string.Empty, string.Empty), null, false, true);
                            asoFacade.updateGheFonteAgenteExameAsoLaudoAso(gfaea);
                            result = true;
                        }
                        else
                        {
                            ClienteFuncaoExameASo cfea = new ClienteFuncaoExameASo(Convert.ToInt64(dvRow.Cells[0].Value), null, null, null, null, false, false, String.Empty, false, null, null, null, null, false, null, laudo, Convert.ToString(dvRow.Cells[7].Value).ToUpper(), false, null, null, dvRow.Cells[8].Value == DBNull.Value ? null : new Medico((long?)dvRow.Cells[8].Value, dvRow.Cells[9].Value.ToString(), dvRow.Cells[10].Value.ToString(), string.Empty, string.Empty, string.Empty, string.Empty, string.Empty, string.Empty, string.Empty, string.Empty, string.Empty, string.Empty, null, string.Empty, string.Empty, string.Empty, null, string.Empty,string.Empty, string.Empty), null, true);
                            asoFacade.updateClienteFuncaoExameAsoLaudoAso(cfea);
                            result = true;
                        }
                    }

                    if (result)
                        MessageBox.Show("Laudo gravado com sucesso.", "Confirmação", MessageBoxButtons.OK, MessageBoxIcon.Information);

                    /* verificando para saber se o atendimento pode ser laudado */
                    if (concluiLaudo)
                    {
                        if (validaFinalizacaoLaudoAtendimento())
                        {
                            //if (!validaGravacaoMedico())
                            //    throw new Exception("Você deve selecionar todos os médicos examinadores.");

                            if (validaDigitacaoObservacaoCodigoTuss())
                            {
                                frmAtendimentoLaudarConclusao incluirConclusao = new frmAtendimentoLaudarConclusao(atendimento);
                                incluirConclusao.ShowDialog();
                                this.Close();
                            }
                        }
                    }
                
                    this.Close();
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }

        private void btn_fechar_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        
        private void montaGridExame()
        {

            dgv_exame.ColumnCount = 3;

            dgv_exame.Columns[0].HeaderText = "Id";
            dgv_exame.Columns[0].Visible = false;

            dgv_exame.Columns[1].HeaderText = "Exame";
            dgv_exame.Columns[1].ReadOnly = true;
            dgv_exame.Columns[1].Width = 250;
            dgv_exame.Columns[1].DefaultCellStyle.BackColor = Color.LightGray;

            dgv_exame.Columns[2].HeaderText = "Tipo";
            dgv_exame.Columns[2].Visible = false;

            DataGridViewCheckBoxColumn checkNormal = new DataGridViewCheckBoxColumn();
            checkNormal.Name = "Normal";
            dgv_exame.Columns.Add(checkNormal);
            checkNormal.HeaderText = "Normal";
            dgv_exame.Columns["Normal"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
            dgv_exame.Columns["Normal"].HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleCenter;
            dgv_exame.Columns["Normal"].Width = 60;

            DataGridViewCheckBoxColumn checkAlterado = new DataGridViewCheckBoxColumn();
            checkAlterado.Name = "Alterado";
            dgv_exame.Columns.Add(checkAlterado);
            checkAlterado.HeaderText = "Alterado";
            dgv_exame.Columns["Alterado"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
            dgv_exame.Columns["Alterado"].HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleCenter;
            dgv_exame.Columns["Alterado"].Width = 60;

            DataGridViewCheckBoxColumn checkEstavel = new DataGridViewCheckBoxColumn();
            checkEstavel.Name = "Estável";
            dgv_exame.Columns.Add(checkEstavel);
            checkEstavel.HeaderText = "Estável";
            dgv_exame.Columns["Estável"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
            dgv_exame.Columns["Estável"].HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleCenter;
            dgv_exame.Columns["Estável"].Width = 60;

            DataGridViewCheckBoxColumn checkAgravamento = new DataGridViewCheckBoxColumn();
            checkAgravamento.Name = "Agravamento";
            dgv_exame.Columns.Add(checkAgravamento);
            checkAgravamento.HeaderText = "Agravamento";
            dgv_exame.Columns["Agravamento"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
            dgv_exame.Columns["Agravamento"].HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleCenter;
            dgv_exame.Columns["Agravamento"].Width = 80;

            DataGridViewTextBoxColumn textColumn = new DataGridViewTextBoxColumn();
            textColumn.Name = "observacao";
            textColumn.HeaderText = "Observação";
            textColumn.Width = 300;
            textColumn.DefaultCellStyle.BackColor = Color.White;
            dgv_exame.Columns.Add(textColumn);

            DataGridViewTextBoxColumn idMedico = new DataGridViewTextBoxColumn();
            idMedico.Name = "idMedicoExaminador";
            dgv_exame.Columns.Add(idMedico);
            dgv_exame.Columns[8].Visible = false;

            DataGridViewTextBoxColumn textMedico = new DataGridViewTextBoxColumn();
            textMedico.Name = "medicoExaminador";
            textMedico.HeaderText = "Médico Examinador";
            textMedico.Width = 300;
            textMedico.ReadOnly = true;
            dgv_exame.Columns.Add(textMedico);

            DataGridViewTextBoxColumn textCrm = new DataGridViewTextBoxColumn();
            textCrm.Name = "crmMedicoExaminador";
            textCrm.HeaderText = "CRM";
            textCrm.Width = 100;
            textCrm.ReadOnly = true;
            dgv_exame.Columns.Add(textCrm);

            DataGridViewButtonColumn btnMedico = new DataGridViewButtonColumn();
            dgv_exame.Columns.Add(btnMedico);
            
            btnMedico.Text = "...";
            btnMedico.HeaderText = "...";
            btnMedico.Name = "btnMedico";
            btnMedico.Width = 30;
            btnMedico.UseColumnTextForButtonValue = true;
            btnMedico.DefaultCellStyle.BackColor = Color.White;
            dgv_exame.Columns[11].DisplayIndex = 8;
            btnMedico.FlatStyle = FlatStyle.Flat;

            /* preenchendo a grid com valores da colecao GheFonteAgenteExameAso */
            foreach (GheFonteAgenteExameAso gfaea in atendimento.ExamePcmso)
            {
                Boolean normal = String.Equals(gfaea.Resultado, ApplicationConstants.NORMAL) ? true : false;
                Boolean alterado = String.Equals(gfaea.Resultado, ApplicationConstants.ALTERADO) ? true : false;
                Boolean estavel = String.Equals(gfaea.Resultado, ApplicationConstants.ESTAVEL) ? true : false;
                Boolean agravamento = String.Equals(gfaea.Resultado, ApplicationConstants.AGRAVAMENTO) ? true : false;

                if (listaItensAtendimentoFinalizados != null && listaItensAtendimentoFinalizados.Count > 0)
                {
                    foreach (ItemFilaAtendimento itemFilaAtendimento in listaItensAtendimentoFinalizados)
                    {
                        if (itemFilaAtendimento.TipoExame.Equals(ApplicationConstants.EXAME_PCMSO))
                            if (itemFilaAtendimento.IdItem == gfaea.Id)
                            {

                                dgv_exame.Rows.Add(gfaea.Id, gfaea.GheFonteAgenteExame.Exame.Descricao, ApplicationConstants.EXAME_PCMSO, normal, alterado, estavel, agravamento, gfaea.Observacao, gfaea.Medico != null ? gfaea.Medico.Id : null, gfaea.Medico != null ? gfaea.Medico.Nome : string.Empty, gfaea.Medico != null ? gfaea.Medico.Crm : string.Empty );
                            }
                    }
                }
                else
                    dgv_exame.Rows.Add(gfaea.Id, gfaea.GheFonteAgenteExame.Exame.Descricao, ApplicationConstants.EXAME_PCMSO, normal, alterado, estavel, agravamento, gfaea.Observacao, gfaea.Medico != null ? gfaea.Medico.Id : null, gfaea.Medico != null ? gfaea.Medico.Nome : string.Empty, gfaea.Medico != null ? gfaea.Medico.Crm : string.Empty);
            }

            /* preenchendo a grid com valores da colecao clienteFuncaoExameAso */
            foreach (ClienteFuncaoExameASo cfea in atendimento.ExameExtra)
            {
                Boolean normal = String.Equals(cfea.Resultado, ApplicationConstants.NORMAL) ? true : false;
                Boolean alterado = String.Equals(cfea.Resultado, ApplicationConstants.ALTERADO) ? true : false;
                Boolean estavel = String.Equals(cfea.Resultado, ApplicationConstants.ESTAVEL) ? true : false;
                Boolean agravamento = String.Equals(cfea.Resultado, ApplicationConstants.AGRAVAMENTO) ? true : false;

                if (listaItensAtendimentoFinalizados != null && listaItensAtendimentoFinalizados.Count > 0)
                {
                    foreach (ItemFilaAtendimento itemFilaAtendimento in listaItensAtendimentoFinalizados)
                    {
                        if (itemFilaAtendimento.TipoExame.Equals(ApplicationConstants.EXAME_EXTRA))
                            if (itemFilaAtendimento.IdItem == cfea.Id)
                                dgv_exame.Rows.Add(cfea.Id, cfea.ClienteFuncaoExame.Exame.Descricao, ApplicationConstants.EXAME_EXTRA, normal, alterado, estavel, agravamento, cfea.ObservacaoResultado, cfea.Medico != null ? cfea.Medico.Id : null, cfea.Medico != null ? cfea.Medico.Nome : string.Empty, cfea.Medico != null ? cfea.Medico.Crm : string.Empty);
                    }
                }
                else
                    dgv_exame.Rows.Add(cfea.Id, cfea.ClienteFuncaoExame.Exame.Descricao, ApplicationConstants.EXAME_EXTRA, normal, alterado, estavel, agravamento, cfea.ObservacaoResultado, cfea.Medico != null ? cfea.Medico.Id : null, cfea.Medico != null ? cfea.Medico.Nome : string.Empty, cfea.Medico != null ? cfea.Medico.Crm : string.Empty);
            }

            dgv_exame.Sort(dgv_exame.Columns[1], ListSortDirection.Ascending);

        }

        private bool validaDadosTela()
        {
            bool retorno = false;

            /* validando para saber se o usuário 
             * * laudou pelo menos um exame.
             * * caso ele não tenha laudado, a gravação não poderá ser realizada.
             * */
            try
            {
                foreach (DataGridViewRow dvRow in dgv_exame.Rows)
                    if (Convert.ToBoolean(dvRow.Cells[3].Value) == true || Convert.ToBoolean(dvRow.Cells[4].Value) == true || Convert.ToBoolean(dvRow.Cells[5].Value) == true || Convert.ToBoolean(dvRow.Cells[6].Value) == true)
                        return retorno = true;
                if (!retorno)
                    throw new Exception("Você não laudou nenhum exame.");
                
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
                    
            return retorno;

        }

        private bool validaFinalizacaoLaudoAtendimento()
        {
            bool retorno = false;
            
            try
            {
                foreach (DataGridViewRow dvRow in dgv_exame.Rows)
                {
                    if (Convert.ToBoolean(dvRow.Cells[3].Value) == true || Convert.ToBoolean(dvRow.Cells[4].Value) == true || Convert.ToBoolean(dvRow.Cells[5].Value) == true || Convert.ToBoolean(dvRow.Cells[6].Value) == true)
                        retorno = true;
                    else 
                        return retorno = false;
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

            return retorno;
        }

        private bool validaGravacaoMedico()
        {
            bool retorno = false;

            try
            {
                foreach (DataGridViewRow dvRow in dgv_exame.Rows)
                {
                    if (dvRow.Cells[8].Value != DBNull.Value && dvRow.Cells[8].Value != string.Empty && dvRow.Cells[8].Value != null)
                        retorno = true;
                    else
                        return retorno = false;
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

            return retorno;
        }

        private bool validaDigitacaoObservacaoCodigoTuss()
        {
            bool retorno = true;
            EstudoFacade pcmsoFacade = EstudoFacade.getInstance();
            GheFonteAgenteExameAso gheFonteAgenteExameAso = null;
            ClienteFuncaoExameASo clienteFuncaoExameAso = null;
            Exame exame = null;

            try
            {
                foreach (DataGridViewRow dvRow in dgv_exame.Rows)
                {
                    if (string.Equals(dvRow.Cells[2].Value, ApplicationConstants.EXAME_PCMSO))
                        gheFonteAgenteExameAso = atendimento.ExamePcmso.Find(x => x.Id == (long)dvRow.Cells[0].Value);
                    else
                        clienteFuncaoExameAso = atendimento.ExameExtra.Find(x => x.Id == (long)dvRow.Cells[0].Value);

                    /* buscando os dados do exame */

                    if (gheFonteAgenteExameAso != null)
                        exame = pcmsoFacade.findExameById((long)gheFonteAgenteExameAso.GheFonteAgenteExame.Exame.Id);
                    else
                        exame = pcmsoFacade.findExameById((long)clienteFuncaoExameAso.ClienteFuncaoExame.Exame.Id);

                    if (string.IsNullOrEmpty(exame.CodigoTuss) && (dvRow.Cells[7].Value == string.Empty || dvRow.Cells[7].Value == DBNull.Value))
                        throw new Exception("Você não poderá laudar o atendimento porque o exame " + exame.Descricao + " não tem o código da tabela TUSS informado no cadastro e o campo observação do procedimento torna-se-a obrigatório.");
                    
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
                retorno = false;
            }

            return retorno;
        }

        private void dgv_exame_ColumnAdded(object sender, DataGridViewColumnEventArgs e)
        {
            e.Column.SortMode = DataGridViewColumnSortMode.NotSortable;
        }

        private void dgv_exame_CellFormatting(object sender, DataGridViewCellFormattingEventArgs e)
        {
            if (e.ColumnIndex == 3)
            {
                DataGridViewCell cell = this.dgv_exame.Rows[e.RowIndex].Cells[e.ColumnIndex];
                cell.ToolTipText = msg2;
            }
            else if (e.ColumnIndex == 4)
            {
                DataGridViewCell cell = this.dgv_exame.Rows[e.RowIndex].Cells[e.ColumnIndex];
                cell.ToolTipText = msg3;
            }
            else if (e.ColumnIndex == 5)
            {
                DataGridViewCell cell = this.dgv_exame.Rows[e.RowIndex].Cells[e.ColumnIndex];
                cell.ToolTipText = msg4;
            }
            else if (e.ColumnIndex == 6)
            {
                DataGridViewCell cell = this.dgv_exame.Rows[e.RowIndex].Cells[e.ColumnIndex];
                cell.ToolTipText = msg5;
            }
            else if (e.ColumnIndex == 11)
            {
                DataGridViewCell cell = this.dgv_exame.Rows[e.RowIndex].Cells[e.ColumnIndex];
                cell.ToolTipText = "Selecione ao altere o médico examimador.";
            }
        }

        private void dgv_exame_CurrentCellDirtyStateChanged(object sender, EventArgs e)
        {
            if (this.dgv_exame.IsCurrentCellDirty)
                dgv_exame.CommitEdit(DataGridViewDataErrorContexts.Commit);
        }

        private void dgv_exame_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                if (e.ColumnIndex == 3)
                {

                    if (Convert.ToBoolean(dgv_exame.Rows[e.RowIndex].Cells[4].Value) == true)
                        dgv_exame.Rows[e.RowIndex].Cells[4].Value = false;

                    if (Convert.ToBoolean(dgv_exame.Rows[e.RowIndex].Cells[5].Value) == true)
                        dgv_exame.Rows[e.RowIndex].Cells[5].Value = false;

                    if (Convert.ToBoolean(dgv_exame.Rows[e.RowIndex].Cells[6].Value) == true)
                        dgv_exame.Rows[e.RowIndex].Cells[6].Value = false;

                }

                else if (e.ColumnIndex == 4)
                {

                    if (Convert.ToBoolean(dgv_exame.Rows[e.RowIndex].Cells[3].Value) == true)
                        dgv_exame.Rows[e.RowIndex].Cells[3].Value = false;

                    if (Convert.ToBoolean(dgv_exame.Rows[e.RowIndex].Cells[5].Value) == true)
                        dgv_exame.Rows[e.RowIndex].Cells[5].Value = false;

                    if (Convert.ToBoolean(dgv_exame.Rows[e.RowIndex].Cells[6].Value) == true)
                        dgv_exame.Rows[e.RowIndex].Cells[6].Value = false;
                }

                else if (e.ColumnIndex == 5)
                {

                    if (Convert.ToBoolean(dgv_exame.Rows[e.RowIndex].Cells[3].Value) == true)
                        dgv_exame.Rows[e.RowIndex].Cells[3].Value = false;

                    if (Convert.ToBoolean(dgv_exame.Rows[e.RowIndex].Cells[4].Value) == true)
                        dgv_exame.Rows[e.RowIndex].Cells[4].Value = false;

                    if (Convert.ToBoolean(dgv_exame.Rows[e.RowIndex].Cells[6].Value) == true)
                        dgv_exame.Rows[e.RowIndex].Cells[6].Value = false;
                }

                else if (e.ColumnIndex == 6)
                {

                    if (Convert.ToBoolean(dgv_exame.Rows[e.RowIndex].Cells[3].Value) == true)
                        dgv_exame.Rows[e.RowIndex].Cells[3].Value = false;

                    if (Convert.ToBoolean(dgv_exame.Rows[e.RowIndex].Cells[4].Value) == true)
                        dgv_exame.Rows[e.RowIndex].Cells[4].Value = false;

                    if (Convert.ToBoolean(dgv_exame.Rows[e.RowIndex].Cells[5].Value) == true)
                        dgv_exame.Rows[e.RowIndex].Cells[5].Value = false;
                }
                else if (e.ColumnIndex == 11)
                {
                    /* verificar o médico examinador */

                    EstudoFacade pcmsoFacade = EstudoFacade.getInstance();

                    GheFonteAgenteExameAso gheFonteAgenteExameAso = atendimento.ExamePcmso.Find(x => x.Id == (long)dgv_exame.Rows[e.RowIndex].Cells[0].Value);
                    ClienteFuncaoExameASo clienteFuncaoExameAso = atendimento.ExameExtra.Find(x => x.Id == (long)dgv_exame.Rows[e.RowIndex].Cells[0].Value);

                    List<MedicoExame> medicos = new List<MedicoExame>();

                    if (gheFonteAgenteExameAso != null)
                        medicos = pcmsoFacade.findAllMedicoExameByExame(gheFonteAgenteExameAso.GheFonteAgenteExame.Exame);
                    else
                        medicos = pcmsoFacade.findAllMedicoExameByExame(clienteFuncaoExameAso.ClienteFuncaoExame.Exame);

                    if (medicos.Count >= 2)
                    {
                        /* nesse caso, existe mais de um médico cadastrado para o exame. o usuário deverá escolher qual o médico que realizou o exame. */
                        frmMedicoExameBuscar buscarMedico = new frmMedicoExameBuscar(gheFonteAgenteExameAso != null ? gheFonteAgenteExameAso.GheFonteAgenteExame.Exame : clienteFuncaoExameAso.ClienteFuncaoExame.Exame);
                        buscarMedico.ShowDialog();

                        if (buscarMedico.Medico != null)
                        {
                            dgv_exame.Rows[e.RowIndex].Cells[8].Value = buscarMedico.Medico.Id;
                            dgv_exame.Rows[e.RowIndex].Cells[9].Value = buscarMedico.Medico.Nome;
                            dgv_exame.Rows[e.RowIndex].Cells[10].Value = buscarMedico.Medico.Crm;
                        }

                    }
                    else if (medicos.Count <= 1)
                    {
                        /* não existe médico cadastrado para o exame ou existe apenas um médico cadastrado. Então o usuário poderá selecionar o médico que realizou o exame. */

                        frmMedicoBuscar buscarMedico = new frmMedicoBuscar();
                        buscarMedico.ShowDialog();

                        if (buscarMedico.Medico != null)
                        {
                            dgv_exame.Rows[e.RowIndex].Cells[8].Value = buscarMedico.Medico.Id;
                            dgv_exame.Rows[e.RowIndex].Cells[9].Value = buscarMedico.Medico.Nome;
                            dgv_exame.Rows[e.RowIndex].Cells[10].Value = buscarMedico.Medico.Crm;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            
        }

        private void dgv_exame_EditingControlShowing(object sender, DataGridViewEditingControlShowingEventArgs e)
        {
            if (e.Control is TextBox)
            {
                ((TextBox)e.Control).CharacterCasing = CharacterCasing.Upper;

            }
        }

    }
}
