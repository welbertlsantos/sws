﻿using SWS.Entidade;
using SWS.Facade;
using SWS.View.Resources;
using SWS.View.ViewHelper;
using SWS.ViewHelper;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace SWS.View
{
    public partial class frmBancoPrincipal : frmTemplate
    {

        private Banco banco;

        public frmBancoPrincipal()
        {
            InitializeComponent();
            validaPermissoes();
            this.ActiveControl = textBanco;
            ComboHelper.booleanTodos(cbCaixa);
            ComboHelper.booleanTodos(cbBoleto);
        }

        private void btnIncluir_Click(object sender, EventArgs e)
        {
            try
            {
                frmBancoIncluir incluirBanco = new frmBancoIncluir();
                incluirBanco.ShowDialog();

                if (incluirBanco.Banco != null)
                {
                    banco = new Banco(null, string.Empty, string.Empty, null, string.Empty, false, null);
                    montaDataGrid();
                }
            }
            catch(Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }

        private void btnAlterar_Click(object sender, EventArgs e)
        {
            try
            {
                
                if (dgvBanco.CurrentRow == null)
                    throw new Exception("Selecione uma linha.");
                
                FinanceiroFacade financeiroFacade = FinanceiroFacade.getInstance();

                Banco bancoAltera = financeiroFacade.findBancoById((long)dgvBanco.CurrentRow.Cells[0].Value);

                if (!string.Equals(bancoAltera.Situacao, ApplicationConstants.ATIVO))
                    throw new Exception("Somente bancos ativos podem ser alterados.");

                if (bancoAltera.Privado)
                    throw new Exception("Banco de uso exclusivo do sistema. Não pode ser alterado.");

                frmBancoAlterar alterarBanco = new frmBancoAlterar(bancoAltera);
                alterarBanco.ShowDialog();

                banco = new Banco(null, string.Empty, string.Empty, null, string.Empty, false, null);
                montaDataGrid();

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btnPesquisar_Click(object sender, EventArgs e)
        {
            try
            {
                banco = new Banco(null, textBanco.Text, string.Empty, false, string.Empty, false, false); 
                montaDataGrid();
                this.textBanco.Focus();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btnReativar_Click(object sender, EventArgs e)
        {
            try
            {
                if (dgvBanco.CurrentRow == null)
                    throw new Exception("Selecione uma linha.");

                FinanceiroFacade financeiroFacade = FinanceiroFacade.getInstance();

                Banco bancoReativar = financeiroFacade.findBancoById((long)dgvBanco.CurrentRow.Cells[0].Value);

                if (!string.Equals(bancoReativar.Situacao, ApplicationConstants.DESATIVADO))
                    throw new Exception("Somente bancos excluídos podem ser reativados");

                if (MessageBox.Show("Deseja reativar o banco selecionado?", "Atenção", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                {
                    bancoReativar.Situacao = ApplicationConstants.ATIVO;
                    financeiroFacade.alteraBanco(bancoReativar);

                    banco = new Banco(null, string.Empty, string.Empty, null, string.Empty, false, null);

                    MessageBox.Show("Banco reativado com sucesso.", "Confirmação", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    montaDataGrid();
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btnDetalhar_Click(object sender, EventArgs e)
        {
            try
            {
                if (dgvBanco.CurrentRow == null)
                    throw new Exception ("Selecione uma linha.");
                
                FinanceiroFacade financeiroFacade = FinanceiroFacade.getInstance();

                frmBancoDetalhar detalharBanco = new frmBancoDetalhar(financeiroFacade.findBancoById((long)dgvBanco.CurrentRow.Cells[0].Value));
                detalharBanco.ShowDialog();

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btnExcluir_Click(object sender, EventArgs e)
        {
            try
            {
                if (dgvBanco.CurrentRow == null)
                    throw new Exception("Selecione uma linha.");

                FinanceiroFacade financeiroFacade = FinanceiroFacade.getInstance();

                Banco bancoExcluir = financeiroFacade.findBancoById((long)dgvBanco.CurrentRow.Cells[0].Value);

                if (!string.Equals(bancoExcluir.Situacao, ApplicationConstants.ATIVO))
                    throw new Exception("Somente bancos ativos podem ser excluídos.");

                if (bancoExcluir.Privado)
                    throw new Exception("Banco de uso exclusivo do sistema. Não pode ser excluído.");

                if (MessageBox.Show("Deseja excluir o banco selecionado?", "Atenção", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                {
                    bancoExcluir.Situacao = ApplicationConstants.DESATIVADO;
                    financeiroFacade.alteraBanco(bancoExcluir);
                    banco = new Banco(null, string.Empty, string.Empty, null, string.Empty, false, null);

                    MessageBox.Show("Banco excluído com sucesso", "Confirmação", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    montaDataGrid();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btnFechar_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        
        public void validaPermissoes()
        {
            PermissionamentoFacade permissionamentoFacade = PermissionamentoFacade.getInstance();

            btnIncluir.Enabled = permissionamentoFacade.hasPermission("BANCO", "INCLUIR");
            btnAlterar.Enabled = permissionamentoFacade.hasPermission("BANCO", "ALTERAR");
            btnExcluir.Enabled = permissionamentoFacade.hasPermission("BANCO", "EXCLUIR");
            btnReativar.Enabled = permissionamentoFacade.hasPermission("BANCO", "EXCLUIR");

        }

        public void montaDataGrid()
        {
            try
            {
                this.Cursor = Cursors.WaitCursor;

                FinanceiroFacade financeiroFacade = FinanceiroFacade.getInstance();

                dgvBanco.Columns.Clear();
                
                DataSet ds = financeiroFacade.findBancoByFilter(banco, string.IsNullOrEmpty(((SelectItem)cbCaixa.SelectedItem).Valor) ? null : (bool?)Convert.ToBoolean(((SelectItem)cbCaixa.SelectedItem).Valor), string.IsNullOrEmpty(((SelectItem)cbBoleto.SelectedItem).Valor) ? null : (bool?)Convert.ToBoolean(((SelectItem)cbBoleto.SelectedItem).Valor));

                dgvBanco.DataSource = ds.Tables["Bancos"].DefaultView;

                dgvBanco.Columns[0].HeaderText = "Id";
                dgvBanco.Columns[0].Visible = false;

                dgvBanco.Columns[1].HeaderText = "Nome";

                dgvBanco.Columns[2].HeaderText = "Cod Banco";
                dgvBanco.Columns[2].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
                dgvBanco.Columns[2].HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleCenter;

                dgvBanco.Columns[3].HeaderText = "É Caixa?";

                dgvBanco.Columns[4].HeaderText = "Situação";
                dgvBanco.Columns[4].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
                dgvBanco.Columns[4].HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleCenter;

                dgvBanco.Columns[5].HeaderText = "Privado";
                dgvBanco.Columns[5].Visible = false;

                dgvBanco.Columns[6].HeaderText = "Emite Boleto?";
                dgvBanco.Columns[6].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
                dgvBanco.Columns[6].HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleCenter;

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }

        private void dgvBanco_RowPrePaint(object sender, DataGridViewRowPrePaintEventArgs e)
        {
            DataGridView dvg = sender as DataGridView;

            if (String.Equals(dvg.Rows[e.RowIndex].Cells[4].Value.ToString(), ApplicationConstants.DESATIVADO))
                dvg.Rows[e.RowIndex].DefaultCellStyle.ForeColor = Color.Red;
        }

        private void dgvBanco_CellMouseDoubleClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            try
            {
                btnAlterar.PerformClick();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }
    }
}
