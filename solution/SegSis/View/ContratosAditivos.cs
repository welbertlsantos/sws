﻿using SWS.Entidade;
using SWS.Facade;
using SWS.View.Resources;
using SWS.View.ViewHelper;
using SWS.ViewHelper;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace SWS.View
{
    public partial class frmContratosAditivos : frmTemplateConsulta
    {
        private Contrato aditivo;

        public Contrato Aditivo
        {
            get { return aditivo; }
            set { aditivo = value; }
        }
        
        public frmContratosAditivos()
        {
            InitializeComponent();
            ComboHelper.tipoPesquisaContrato(cbFiltro);
            ActiveControl = textChave;
        }

        private void btnConfirmar_Click(object sender, EventArgs e)
        {
            try
            {
                /* caso o usuario não tenha selecionado nenhuma linha será solicitado a ele a açao */
                if (dgvContrato.CurrentRow == null)
                    throw new Exception("Selecione uma linha.");

                /* selecionando o contrato selecionado pelo usuário e gerando um aditivo contratual */

                FinanceiroFacade financeiroFacade = FinanceiroFacade.getInstance();

                Contrato contratoSelecionado = financeiroFacade.findContratoById(Convert.ToInt64(dgvContrato.CurrentRow.Cells[0].Value));

                aditivo = financeiroFacade.insertAditivoInContrato(contratoSelecionado);

                MessageBox.Show("Aditivo Criado com sucesso", "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Information);

                /* abrindo formulário de contrato para alteração do aditivo */

                MessageBox.Show("Você não poderá alterar dados de exames e produtos. Somente incluir novas informações.", "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Information);

                frmContratosAlterar formAditivo = new frmContratosAlterar(aditivo);
                formAditivo.ShowDialog();

                this.Close();

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
        }

        private void btnBuscar_Click(object sender, EventArgs e)
        {
            if (validaDadosTela())
                MontaGrid();
        }

        private void cbFiltro_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                if (String.Equals(((SelectItem)cbFiltro.SelectedItem).Valor, ApplicationConstants.BUSCA_CODIGO_CONTRATO))
                {
                    textChave.Text = String.Empty;
                    textChave.Mask = "0000,00,000000/00";
                    textChave.Focus();

                }
                else if (String.Equals(((SelectItem)cbFiltro.SelectedItem).Valor, ApplicationConstants.BUSCA_CNPJ))
                {
                    textChave.Text = String.Empty;
                    textChave.Mask = "00,000,000/0000-00";
                    textChave.Focus();
                }
                else if (String.Equals(((SelectItem)cbFiltro.SelectedItem).Valor, ApplicationConstants.CPF))
                {
                    textChave.Text = String.Empty;
                    textChave.Mask = "999,999,999-99";
                    textChave.Focus();
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btnFechar_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        public bool validaDadosTela()
        {
            bool retorno = true;

            try
            {
                /* validando o CNPJ digitado */
                if (String.Equals(((SelectItem)cbFiltro.SelectedItem).Valor, ApplicationConstants.BUSCA_CNPJ))
                {
                    if (!ValidaCampoHelper.ValidaCNPJ(textChave.Text))
                        throw new Exception("CNPJ digitado é inválido.");
                }
                else if (String.Equals(((SelectItem)cbFiltro.SelectedItem).Valor, ApplicationConstants.FISICA))
                {
                    if (!ValidaCampoHelper.ValidaCPF(textChave.Text))
                        throw new Exception("CPF digitado é inválido.");
                }
                else if (String.IsNullOrEmpty(textChave.Text.Replace(".", "").Replace("/", "").Trim()))
                    throw new Exception("Informar um código de contrato válido.");

            }
            catch (Exception ex)
            {
                retorno = false;
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);

            }

            return retorno;
        }

        public void MontaGrid()
        {
            bool aptoMontarGrid = true;

            try
            {
                this.Cursor = Cursors.WaitCursor;

                dgvContrato.Columns.Clear();

                DataSet clientes = null;
                Cliente cliente = null;

                ClienteFacade clienteFacade = ClienteFacade.getInstance();

                /* caso o usuário tenha selecionado o filtro cnpj e se a empresa tiver unidade então será
                 * selecionado o cliente matriz de acordo com os parametros unidade e id_cliente_matriz */

                if (String.Equals(((SelectItem)cbFiltro.SelectedItem).Valor, ApplicationConstants.BUSCA_CNPJ))
                {
                    clientes = clienteFacade.findAllClienteByCNPJAptoContrato(textChave.Text.Replace(".", "").Replace("/", "").Replace("-", "").Trim());

                    /* a pesquisa retornou mais de um cliente para o cnpj. O cliente possui unidades cadastradas. Nesse caso
                     * como a matriz é que possui os contratos então será escolhido o cliente matriz */
                    if (clientes.Tables[0].DefaultView.Count > 1)
                    {
                        /* recuperando o cliente matriz */

                        foreach (DataRow row in clientes.Tables[0].Rows)
                            if ((Boolean)row["UNIDADE"] == false && row["ID_CLIENTE_MATRIZ"] == null)
                            {
                                cliente = clienteFacade.findClienteById((Int64)row["ID_CLIENTE"]);
                                aptoMontarGrid = true;
                                break;
                            }
                    }
                    else if (clientes.Tables[0].DefaultView.Count == 1)
                    {
                        cliente = clienteFacade.findClienteById((Int64)clientes.Tables[0].Columns["ID_CLIENTE"].DefaultValue);
                        aptoMontarGrid = true;
                    }
                    else
                        aptoMontarGrid = false;

                }

                FinanceiroFacade financeiroFacade = FinanceiroFacade.getInstance();

                DataSet ds = financeiroFacade.findContratoByFilter(new Contrato(null, String.Equals(((SelectItem)cbFiltro.SelectedItem).Valor, ApplicationConstants.BUSCA_CODIGO_CONTRATO) ? textChave.Text : String.Empty, cliente, null, null, null, null, ApplicationConstants.CONTRATO_FECHADO, String.Empty, null, null, string.Empty, null, false, null, null, false, false, false, null, false, null, null, null, null, null, false, null, null, null, String.Empty, null), null, null, null, null);

                if (aptoMontarGrid)
                {

                    /* preenchendo grid principal. 
                     * Esse data grid não poderá ser preenchido como padrão porque 
                     * o cliente poderá ser um cliente padrão ou um cliente proposta */

                    dgvContrato.DefaultCellStyle.BackColor = Color.LightGray;
                    dgvContrato.ColumnCount = 23;

                    dgvContrato.Columns[0].HeaderText = "id_contrato";
                    dgvContrato.Columns[0].Visible = false;

                    dgvContrato.Columns[1].HeaderText = "Código";
                    dgvContrato.Columns[1].HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleCenter;
                    dgvContrato.Columns[1].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
                    dgvContrato.Columns[1].DefaultCellStyle.BackColor = Color.LightYellow;

                    dgvContrato.Columns[2].HeaderText = "id_cliente";
                    dgvContrato.Columns[2].Visible = false;

                    dgvContrato.Columns[3].HeaderText = "Cliente";

                    dgvContrato.Columns[4].HeaderText = "CNPJ";
                    dgvContrato.Columns[4].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;

                    dgvContrato.Columns[5].HeaderText = "Data de Elaboração";
                    dgvContrato.Columns[5].HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleCenter;
                    dgvContrato.Columns[5].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;

                    dgvContrato.Columns[6].HeaderText = "Data de Vencimento";
                    dgvContrato.Columns[6].HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleCenter;
                    dgvContrato.Columns[6].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;

                    dgvContrato.Columns[7].HeaderText = "Data de Fechamento";
                    dgvContrato.Columns[7].HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleCenter;
                    dgvContrato.Columns[7].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;

                    dgvContrato.Columns[8].HeaderText = "Situação";
                    dgvContrato.Columns[8].Visible = false;

                    dgvContrato.Columns[9].HeaderText = "Observação";
                    dgvContrato.Columns[9].Visible = false;

                    dgvContrato.Columns[10].HeaderText = "id_usuaruio_criador";
                    dgvContrato.Columns[10].Visible = false;

                    dgvContrato.Columns[11].HeaderText = "Usuario criador";

                    dgvContrato.Columns[12].HeaderText = "id_usuario_finalizou";
                    dgvContrato.Columns[12].Visible = false;

                    dgvContrato.Columns[13].HeaderText = "Usuario Finalizador";

                    dgvContrato.Columns[14].HeaderText = "id_usuario_cancelou";
                    dgvContrato.Columns[14].Visible = false;

                    dgvContrato.Columns[15].HeaderText = "Usuario Cancelamento";

                    dgvContrato.Columns[16].HeaderText = "motivo cancelamento";
                    dgvContrato.Columns[16].Visible = false;

                    dgvContrato.Columns[17].HeaderText = "id_cliente_prestador";
                    dgvContrato.Columns[17].Visible = false;

                    dgvContrato.Columns[18].HeaderText = "Prestador";

                    dgvContrato.Columns[19].HeaderText = "Data Cancelamento";
                    dgvContrato.Columns[19].DisplayIndex = 8;
                    dgvContrato.Columns[19].HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleCenter;
                    dgvContrato.Columns[19].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;

                    dgvContrato.Columns[20].HeaderText = "id_contrato_pai";
                    dgvContrato.Columns[20].Visible = false;

                    dgvContrato.Columns[21].HeaderText = "id_contrato_raiz";
                    dgvContrato.Columns[21].Visible = false;

                    dgvContrato.Columns[22].HeaderText = "id_cliente_proposta";
                    dgvContrato.Columns[22].Visible = false;


                    foreach (DataRow row in ds.Tables["Contratos"].Rows)
                    {
                        /* verificando se o cliente é um cliente padrão ou um cliente proposta. 
                         * caso seja um cliente proposta então será necessário preencher os dados do cliente proposta */

                        /* somente contratos fechados são permitidos para aditivos */

                        if (String.Equals(row["situacao"].ToString(), ApplicationConstants.CONTRATO_FECHADO))
                            dgvContrato.Rows.Add( row["id_contrato"], row["codigo_contrato"], row["id_cliente"], row["razao_social"], row["cnpj"], String.IsNullOrEmpty(row["data_elaboracao"].ToString()) ? null : row["data_elaboracao"], String.IsNullOrEmpty(row["data_fim"].ToString()) ? null : row["data_fim"], String.IsNullOrEmpty(row["data_inicio"].ToString()) ? null : row["data_inicio"], row["situacao"], row["observacao"], String.IsNullOrEmpty(row["id_usuario_criou"].ToString()) ? null : row["id_usuario_criou"], row["login_criou"], String.IsNullOrEmpty(row["id_usuario_finalizou"].ToString()) ? null : row["id_usuario_finalizou"], row["login_finalizou"], String.IsNullOrEmpty(row["id_usuario_cancelou"].ToString()) ? null : row["id_usuario_cancelou"], row["login_cancelou"], row["motivo_cancelamento"], String.IsNullOrEmpty(row["id_cliente_prestador"].ToString()) ? null : row["id_cliente_prestador"], row["prestador"], String.IsNullOrEmpty(row["data_cancelamento"].ToString()) ? null : row["data_cancelamento"], String.IsNullOrEmpty(row["id_contrato_pai"].ToString()) ? null : row["id_contrato_pai"], String.IsNullOrEmpty(row["id_contrato_raiz"].ToString()) ? null : row["id_contrato_raiz"], String.IsNullOrEmpty(row["id_cliente_proposta"].ToString()) ? null : row["id_cliente_proposta"]);
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }

        private void frmContratosAditivos_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter || e.KeyCode == Keys.Return)
                SendKeys.Send("{TAB}");
        }

        private void dgvContrato_CellMouseDoubleClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            try
            {
                btnConfirmar.PerformClick();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }
    }
}
