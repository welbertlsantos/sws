﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using SWS.Facade;
using SWS.Entidade;

namespace SWS.View
{
    public partial class frm_PcmsoMedicoSelecionar : BaseFormConsulta
    {

        private static String msg1 = " Selecione um linha";

        Usuario medico = null;

        public Usuario Medico
        {
            get { return medico; }
            set { medico = value; }
        }

        public frm_PcmsoMedicoSelecionar()
        {
            InitializeComponent();
        }

        private void btn_fechar_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btn_buscar_Click(object sender, EventArgs e)
        {
            try
            {
                this.grd_usuario.Columns.Clear();
                this.Cursor = Cursors.WaitCursor;
                montaDataGrid();
                text_nome.Focus();
            }
            catch (Exception Ex)
            {
                MessageBox.Show(Ex.Message);
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }


        public void montaDataGrid()
        {
            Int64 idFuncaoProcurada = 4; // id funcao interna médico

            UsuarioFacade usuarioFacade = UsuarioFacade.getInstance();

            DataSet ds = usuarioFacade.findUsuarioFuncaoAtivoByFilter(new Usuario(null, text_nome.Text.Trim(), String.Empty, String.Empty, String.Empty, null, String.Empty, String.Empty, String.Empty, String.Empty, null, null, String.Empty, false, null, String.Empty, false, false, string.Empty, string.Empty), idFuncaoProcurada);

            grd_usuario.DataSource = ds.Tables["Usuarios"].DefaultView;

            grd_usuario.Columns[0].HeaderText = "ID";
            grd_usuario.Columns[0].Visible = false;
            
            grd_usuario.Columns[1].HeaderText = "Nome do Usuário";

        }

        private void btn_ok_Click(object sender, EventArgs e)
        {
            try
            {
                if (grd_usuario.CurrentRow == null)
                    throw new Exception(msg1);

                UsuarioFacade usuarioFacace = UsuarioFacade.getInstance();
                medico = usuarioFacace.findUsuarioById((Int64)grd_usuario.CurrentRow.Cells[0].Value);
                this.Close();

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
        }
    }
}