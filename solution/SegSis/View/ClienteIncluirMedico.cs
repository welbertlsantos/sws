﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using SegSis.Entidade;
using SegSis.Facade;

namespace SegSis.View
{
    public partial class frm_ClienteIncluirMedico : BaseFormConsulta
    {

        private Medico medico = null;

        public Medico getMedico()
        {
            return this.medico;
        }
        
        frm_cliente_inclui formClienteIncluir;

        private static String Msg01 = " Selecione um linha";
        private static string Msg02 = " Atenção";
        
        
        public frm_ClienteIncluirMedico(frm_cliente_inclui formClienteIncluir)
        {
            InitializeComponent();
            this.formClienteIncluir = formClienteIncluir;
            validaPermissoes();
        }

        private void validaPermissoes()
        {
            PermissionamentoFacade permissionamentoFacade = PermissionamentoFacade.getInstance();

            this.btn_novo.Enabled = permissionamentoFacade.hasPermission("MEDICO", "INCLUIR");
        }

        private void btn_buscar_Click(object sender, EventArgs e)
        {
            try
            {
                this.grd_medico.Columns.Clear();
                medico = new Medico(null, text_nome.Text.ToUpper(), String.Empty, String.Empty);
                montaDataGrid();
                text_nome.Focus();
            }
            catch (Exception Ex)
            {
                MessageBox.Show(Ex.Message);
            }

        }

        public void montaDataGrid()
        {
            PcmsoFacade pcmsoFacade = PcmsoFacade.getInstance();

            DataSet ds = null;

            /* cliente recuperado */
            if (formClienteIncluir.getCliente() != null && formClienteIncluir.getCoordernador() != null)
            {
                ds = pcmsoFacade.findAllMedicoNotInCliente(formClienteIncluir.getCliente(), medico);
            }
            else
            {
                ds = pcmsoFacade.findMedicoByNameByAso(medico);
            }

            grd_medico.DataSource = ds.Tables["Medicos"].DefaultView;

            grd_medico.Columns[0].HeaderText = "ID";
            grd_medico.Columns[0].Visible = false;
            
            grd_medico.Columns[1].HeaderText = "Nome";
            
            grd_medico.Columns[2].HeaderText = "CRM";
            
            grd_medico.Columns[3].HeaderText = "Situação";
            grd_medico.Columns[3].Visible = false;

            grd_medico.Columns[4].HeaderText = "Telefone";
            grd_medico.Columns[4].Visible = false;
            
            grd_medico.Columns[5].HeaderText = "Celular";
            grd_medico.Columns[5].Visible = false;

            grd_medico.Columns[6].HeaderText = "Email";
            grd_medico.Columns[6].Visible = false;
            
            grd_medico.Columns[7].HeaderText = "Endereço";
            grd_medico.Columns[7].Visible = false;
            
            grd_medico.Columns[8].HeaderText = "Número";
            grd_medico.Columns[8].Visible = false;

            grd_medico.Columns[9].HeaderText = "Complemento";
            grd_medico.Columns[9].Visible = false;

            grd_medico.Columns[10].HeaderText = "Bairro";
            grd_medico.Columns[10].Visible = false;

            grd_medico.Columns[11].HeaderText = "CEP";
            grd_medico.Columns[11].Visible = false;
            
            grd_medico.Columns[12].HeaderText = "Cidade";
            grd_medico.Columns[12].Visible = false;
            
            grd_medico.Columns[13].HeaderText = "UF";
            grd_medico.Columns[13].Visible = false;
            
            

        }

        private void btn_fechar_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btn_novo_Click(object sender, EventArgs e)
        {
            frm_MedicoIncluir formMedicoIncluir = new frm_MedicoIncluir();
            formMedicoIncluir.ShowDialog();

            medico = formMedicoIncluir.getMedico();

            this.Close();

        }

        private void btn_ok_Click(object sender, EventArgs e)
        {
            try
            {
                if (grd_medico.CurrentRow != null)
                {

                    medico = new Medico((Int64)grd_medico.CurrentRow.Cells[0].Value,
                        (String)grd_medico.CurrentRow.Cells[1].Value,
                        (String)grd_medico.CurrentRow.Cells[2].Value,
                        (String)grd_medico.CurrentRow.Cells[3].Value,
                        (String)grd_medico.CurrentRow.Cells[4].Value,
                        (String)grd_medico.CurrentRow.Cells[5].Value,
                        (String)grd_medico.CurrentRow.Cells[6].Value,
                        (String)grd_medico.CurrentRow.Cells[7].Value,
                        (String)grd_medico.CurrentRow.Cells[8].Value,
                        (String)grd_medico.CurrentRow.Cells[9].Value,
                        (String)grd_medico.CurrentRow.Cells[10].Value,
                        (String)grd_medico.CurrentRow.Cells[11].Value,
                        (String)grd_medico.CurrentRow.Cells[12].Value,
                        (String)grd_medico.CurrentRow.Cells[13].Value);
                    
                    this.Close();

                }
                else
                {
                    MessageBox.Show(Msg01, Msg02);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
    }
}
