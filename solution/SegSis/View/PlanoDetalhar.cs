﻿using SWS.Entidade;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace SWS.View
{
    public partial class frmPlanoDetalhar : frmPlanoIncluir
    {
        public frmPlanoDetalhar(Plano plano) :base()
        {
            InitializeComponent();
            textDescricao.Text = plano.Descricao;
            formas = plano.Formas;
            parcelas = plano.Parcelas;
            montaDataGridForma();
            montaDataGridParcelas();
            btnGravar.Enabled = false;
            btnIncluirParcela.Enabled = false;
            btnExcluirParcela.Enabled = false;
            btn_incluirForma.Enabled = false;
            btn_excluir.Enabled = false;
            dgvParcela.Enabled = false;
            dgvForma.Enabled = false;
            btnAlterar.Enabled = false;
        }
    }
}
