﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace SWS.View
{
    public partial class frmPcmsoComentarioRevisao : frmTemplateConsulta
    {
        private string comentarioReplicacao;

        public string ComentarioReplicacao
        {
            get { return comentarioReplicacao; }
            set { comentarioReplicacao = value; }
        }
        
        public frmPcmsoComentarioRevisao()
        {
            InitializeComponent();
            ActiveControl = textComentarioRevisao;
        }

        private void btn_incluir_Click(object sender, EventArgs e)
        {
            try
            {
                if (string.IsNullOrEmpty(textComentarioRevisao.Text.Trim()))
                    throw new Exception("Comentário da Revisão é obrigatório");

                if (textComentarioRevisao.Text.Length < 10)
                    throw new Exception("Comentário da revisão não pode ter menos de 10 caracteres");

                ComentarioReplicacao = textComentarioRevisao.Text.Trim();
                this.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }

        private void btn_fechar_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
