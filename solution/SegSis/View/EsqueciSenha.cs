﻿using SWS.Entidade;
using SWS.Facade;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace SWS.View
{
    public partial class frmEsqueciSenha : frmTemplateConsulta
    {
        Usuario usuario;

        public Usuario Usuario
        {
            get { return usuario; }
            set { usuario = value; }
        }

        
        public frmEsqueciSenha()
        {
            InitializeComponent();
            ActiveControl = textNome;
        }

        private void btRecuperar_Click(object sender, EventArgs e)
        {
            try
            {
                /* validando campos */

                if (validaCampos())
                {
                    UsuarioFacade usuarioFacade = UsuarioFacade.getInstance();

                    DataSet ds = usuarioFacade.findUsuarioByFilter(new Usuario(null, textNome.Text.Trim(), textCPF.Text.Trim(), string.Empty, string.Empty, null, "A", textLogin.Text, string.Empty, string.Empty, null, null, string.Empty, false, null, string.Empty, false, string.Empty, string.Empty));

                    if (ds.Tables[0].Rows.Count == 0)
                        throw new Exception("Não foi possível localizar o usuário com as informações fornecidas.");

                    /* se retornar um dataSet esse dataSet só conterá apenas um usuário */
                    foreach (DataRow dvRow in ds.Tables[0].Rows)
                        usuario = new Usuario(Convert.ToInt64(dvRow["id_usuario"]), dvRow["nome"].ToString(), dvRow["cpf"].ToString(), dvRow["rg"].ToString(), string.Empty, Convert.ToDateTime(dvRow["data_admissao"]), dvRow["situacao"].ToString(), dvRow["login"].ToString(), string.Empty, string.Empty, null, null, string.Empty, false, null, string.Empty, false, string.Empty, string.Empty);

                    /* setendo para o usuário a senha padrao */
                    usuario.Senha = Util.getMD5Hash("padrao");

                    usuarioFacade.alterarSenhaUsuario(usuario);

                    usuario = usuarioFacade.findUsuarioById((long)usuario.Id);


                    MessageBox.Show("Senha do usuário resetada com sucesso. A senha do usuário agora é padrao. Altere a senha no próximo login.", "Confirmação", MessageBoxButtons.OK, MessageBoxIcon.Information);

                    this.Close();

                }

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);

            }
        }

        private void btSair_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private bool validaCampos()
        {
            bool retorno = true;

            try
            {
                if (string.IsNullOrEmpty(textNome.Text.Trim()))
                    throw new Exception ("Campo nome deverá ser preenchido.");

                if (String.IsNullOrEmpty(textLogin.Text.Trim()))
                    throw new Exception("Campo login deverá ser preenchido.");

                if (string.IsNullOrEmpty(textCPF.Text.Replace(".","").Replace("-","").Trim()))
                    throw new Exception ("Campo CPF deverá ser preenchido.");
                
            }
            catch (Exception ex)
            {
                retorno = false;
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

            
            return retorno;
        }

        private void frmEsqueciSenha_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter || e.KeyCode == Keys.Return)
                SendKeys.Send("{TAB}");


        }
    }
}
