﻿using SWS.Entidade;
using SWS.Facade;
using SWS.View.Resources;
using SWS.View.ViewHelper;
using SWS.ViewHelper;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace SWS.View
{
    public partial class frmContratosIncluir : frmTemplate
    {
        protected Contrato contrato;

        public Contrato Contrato
        {
            get { return contrato; }
            set { contrato = value; }
        }

        protected Cliente cliente;

        protected Cliente prestador;

        protected List<Exame> exames;
        protected List<Produto> produtos;

        public frmContratosIncluir()
        {
            InitializeComponent();
            ComboHelper.Boolean(cbBloqueio, false);
            ComboHelper.Boolean(cbValorMensal, false);
            ComboHelper.Boolean(cbNumeroVidas, false);
            ComboHelper.Boolean(cbCobrancaAutomatica, false);

            Empresa empresa = PermissionamentoFacade.usuarioAutenticado.Empresa;

            if (empresa != null)
            {
                textEmpresa.Text = empresa.RazaoSocial;
                textNomeFantasia.Text = empresa.Fantasia;
                textCNPJ.Text = ValidaCampoHelper.FormataCnpj(empresa.Cnpj);
            }

            /* recuperando os exames e produtos padrões de contrato */
            EstudoFacade pcmsoFacade = EstudoFacade.getInstance();
            FinanceiroFacade financeiroFacade = FinanceiroFacade.getInstance();
            this.exames = pcmsoFacade.findAllExameAtivoPadraoContrato();
            this.produtos = financeiroFacade.findAllAtivosPadraoContrato();

            if (exames.Count > 0) {
            
                MontaDataGridExame();
                if (dgvExame.ColumnCount > 0)
                {
                    foreach (Exame exame in exames)
                        dgvExame.Rows.Add((long)exame.Id, (string)exame.Descricao, (string)exame.Situacao, (bool)exame.Laboratorio, (decimal)exame.Preco, (int)exame.Prioridade, (decimal)exame.Custo, (bool)exame.Externo, (bool)exame.LiberaDocumento, calculaLucro(exame.Preco, exame.Custo), (bool)exame.PadraoContrato);
                }
            }

            if (produtos.Count > 0)
            {
                MontaDataGridProduto();
                if (dgvProduto.ColumnCount > 0)
                { 
                    foreach (Produto produto in produtos)
                        dgvProduto.Rows.Add((long)produto.Id, (string)produto.Nome, (string)produto.Situacao, (decimal)produto.Preco, (string)produto.Tipo, (decimal)produto.Custo, calculaLucro((decimal)produto.Preco, (decimal)produto.Custo), (bool)produto.PadraoContrato);
                }
            }
        }

        protected void frmContratosIncluir_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter || e.KeyCode == Keys.Return)
                SendKeys.Send("{TAB}");

        }

        protected virtual void btnGravar_Click(object sender, EventArgs e)
        {
            try
            {
                List<ContratoExame> exames = new List<ContratoExame>();
                List<ContratoProduto> produtos = new List<ContratoProduto>();

                if (validaDados())
                {
                    this.Cursor = Cursors.WaitCursor;
                    
                    FinanceiroFacade financeiroFacade = FinanceiroFacade.getInstance();

                    contrato = new Contrato(null, string.Empty, cliente, PermissionamentoFacade.usuarioAutenticado, Convert.ToDateTime(dataElaboracao.Text), this.dataTermino.Checked ? Convert.ToDateTime(dataTermino.Text) : (DateTime?)null, Convert.ToDateTime(dataInicio.Text), ApplicationConstants.CONTRATO_GRAVADO, textComentario.Text, null, null, string.Empty, prestador, false, null, null, Convert.ToBoolean(((SelectItem)cbBloqueio.SelectedItem).Valor), Convert.ToBoolean(((SelectItem)cbValorMensal.SelectedItem).Valor), Convert.ToBoolean(((SelectItem)cbNumeroVidas.SelectedItem).Valor), string.IsNullOrEmpty(textDiaCobranca.Text) ? (int?)null : Convert.ToInt32(textDiaCobranca.Text), Convert.ToBoolean(((SelectItem)cbCobrancaAutomatica.SelectedItem).Valor), String.IsNullOrEmpty(textValorMensalidade.Text) ? 0 : Convert.ToDecimal(textValorMensalidade.Text), string.IsNullOrEmpty(textValorNumeroVidas.Text) ? 0 : Convert.ToDecimal(textValorNumeroVidas.Text), string.IsNullOrEmpty(textDiasBloqueio.Text) ? (int?)null : Convert.ToInt32(textDiasBloqueio.Text), null, null, false, string.Empty, null, null, string.Empty, PermissionamentoFacade.usuarioAutenticado.Empresa);

                    foreach (DataGridViewRow dvRow in dgvExame.Rows)
                        exames.Add(new ContratoExame(null, new Exame((Int64)dvRow.Cells[0].Value, (String)dvRow.Cells[1].Value, (String)dvRow.Cells[2].Value, (Boolean?)dvRow.Cells[3].Value, Convert.ToDecimal(dvRow.Cells[4].Value), (Int32)dvRow.Cells[5].Value, Convert.ToDecimal(dvRow.Cells[6].Value), (Boolean)dvRow.Cells[7].Value, (Boolean)dvRow.Cells[8].Value, string.Empty, false, null, false), contrato, Convert.ToDecimal(dvRow.Cells[4].Value), PermissionamentoFacade.usuarioAutenticado, Convert.ToDecimal(dvRow.Cells[6].Value), DateTime.Now, ApplicationConstants.ATIVO, true));

                    foreach (DataGridViewRow dvRow in dgvProduto.Rows)
                        produtos.Add(new ContratoProduto(null, new Produto((Int64)dvRow.Cells[0].Value, (String)dvRow.Cells[1].Value, (string)dvRow.Cells[2].Value, Convert.ToDecimal(dvRow.Cells[3].Value), (String)dvRow.Cells[4].Value, Convert.ToDecimal(dvRow.Cells[5].Value), (bool)dvRow.Cells[7].Value, string.Empty), contrato, Convert.ToDecimal(dvRow.Cells[3].Value), PermissionamentoFacade.usuarioAutenticado, Convert.ToDecimal(dvRow.Cells[5].Value), DateTime.Now, ApplicationConstants.ATIVO, true));

                    /* verificando se já existe um contrato elaborado para o cliente no mesmo dia */

                    List<Contrato> contratos = financeiroFacade.findAllContratoByCliente(contrato.Cliente, contrato.ClienteProposta);

                    foreach (Contrato ct in contratos)
                        if (((DateTime)ct.DataElaboracao).ToShortDateString() == ((DateTime)contrato.DataElaboracao).ToShortDateString() && (String.Equals(ct.Situacao, ApplicationConstants.CONTRATO_GRAVADO)))
                            throw new SWS.Excecao.ContratoException(SWS.Excecao.ContratoException.msg9);

                    /* verificando situação de exames zerados */
                    if (!verificaValoresZerados())
                    {
                        if (MessageBox.Show("Existem valores zerados no contrato, deseja continuar mesmo assim?", "Confirmação", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.No)
                            throw new Exception("Altere os valores da grid que estão com zero");
                    }

                    contrato = financeiroFacade.insertContrato(contrato, exames, produtos);

                    MessageBox.Show("Contrato criado com sucesso. Anote o número de identificado do contrato." + System.Environment.NewLine + ValidaCampoHelper.RetornaCodigoEstudoFormatado(contrato.CodigoContrato), "Confirmação", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    textCodigo.Text = ValidaCampoHelper.RetornaCodigoEstudoFormatado(contrato.CodigoContrato);
                    this.Close();
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }

        protected void btnFechar_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        
        protected void cbBloqueio_SelectedIndexChanged(object sender, EventArgs e)
        {
            textDiasBloqueio.Enabled = Convert.ToBoolean(((SelectItem)cbBloqueio.SelectedItem).Valor) == true ? true : false;
            textDiasBloqueio.Text = Convert.ToBoolean(((SelectItem)cbBloqueio.SelectedItem).Valor) == true ? textDiasBloqueio.Text : string.Empty;

        }

        protected void cbValorMensal_SelectedIndexChanged(object sender, EventArgs e)
        {
            textValorMensalidade.Enabled = Convert.ToBoolean(((SelectItem)cbValorMensal.SelectedItem).Valor) == true ? true : false;
            textValorMensalidade.Text = Convert.ToBoolean(((SelectItem)cbValorMensal.SelectedItem).Valor) == true ? textValorMensalidade.Text : "0,00";
        }

        protected void cbNumeroVidas_SelectedIndexChanged(object sender, EventArgs e)
        {
            textValorNumeroVidas.Enabled = Convert.ToBoolean(((SelectItem)cbNumeroVidas.SelectedItem).Valor) == true ? true : false;
            textValorNumeroVidas.Text = Convert.ToBoolean(((SelectItem)cbNumeroVidas.SelectedItem).Valor) == true ? textValorNumeroVidas.Text : "0,00";
        }
        
        protected void cbCobrancaAutomatica_SelectedIndexChanged(object sender, EventArgs e)
        {
            textDiaCobranca.Enabled = Convert.ToBoolean(((SelectItem)cbCobrancaAutomatica.SelectedItem).Valor) == true ? true : false;
            textDiaCobranca.Text = Convert.ToBoolean(((SelectItem)cbCobrancaAutomatica.SelectedItem).Valor) == true ? textDiaCobranca.Text : string.Empty;
        }

        protected void textComentario_Enter(object sender, EventArgs e)
        {
            textComentario.BackColor = Color.White;
        }

        protected void textComentario_Leave(object sender, EventArgs e)
        {
            textComentario.BackColor = Color.LightSteelBlue;
        }

        private void btnCliente_Click(object sender, EventArgs e)
        {
            try
            {
                frmClienteSelecionar formCliente = new frmClienteSelecionar(null, null, false, null, true, null, null, null, true);
                formCliente.ShowDialog();

                FinanceiroFacade financeiroFacade = FinanceiroFacade.getInstance();

                if (formCliente.Cliente != null)
                {
                    cliente = formCliente.Cliente;
                    textCliente.Text = cliente.RazaoSocial;

                    /* verificando se existe um contrato para recuperar as informações */

                    Contrato contratoRecuperado = financeiroFacade.findLastContratoByCliente(cliente);

                    if (contratoRecuperado != null)
                    {
                        if (MessageBox.Show("Deseja recuperar informações do ultimo contrato do cliente?", "Confirmação", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                            montatelaInformacaoContrato(contratoRecuperado);
                    }
                    
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        protected virtual void btnPrestador_Click(object sender, EventArgs e)
        {
            try
            {
                frmClienteSelecionar formPrestador = new frmClienteSelecionar(true, false, false, null, null, null, null, false, true);
                formPrestador.ShowDialog();

                if (formPrestador.Cliente != null)
                {
                    prestador = formPrestador.Cliente;
                    textPrestador.Text = prestador.RazaoSocial;
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        protected virtual void btnIncluirExame_Click(object sender, EventArgs e)
        {
            try
            {
                frmExameBuscar exameBuscar = new frmExameBuscar();
                exameBuscar.ShowDialog();

                if (exameBuscar.Exame != null)
                {
                    /* verificando se o exame não está presente na grid */
                    
                    List<Exame> examesInGrid = montaListaExamesInGrid();

                    if (examesInGrid.Contains(exameBuscar.Exame))
                        throw new Exception("Exame já incluído.");
                    
                    /* incluindo a informação do exame na grid de exame */

                    dgvExame.Rows.Add(exameBuscar.Exame.Id, exameBuscar.Exame.Descricao, exameBuscar.Exame.Situacao, exameBuscar.Exame.Laboratorio, exameBuscar.Exame.Preco, exameBuscar.Exame.Prioridade, exameBuscar.Exame.Custo, exameBuscar.Exame.Externo, exameBuscar.Exame.LiberaDocumento, calculaLucro(exameBuscar.Exame.Preco, exameBuscar.Exame.Custo), exameBuscar.Exame.PadraoContrato);
                    dgvExame.CurrentCell = dgvExame[0, dgvExame.RowCount - 1];


                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        protected virtual void btnExcluirExame_Click(object sender, EventArgs e)
        {
            try
            {

                if (dgvExame.CurrentRow == null)
                    throw new Exception("Selecione um exame.");

                dgvExame.Rows.RemoveAt(dgvExame.CurrentRow.Index);
                
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        protected virtual void btnIncluirProduto_Click(object sender, EventArgs e)
        {
            try
            {

                frmProdutoBusca produtoBusca = new frmProdutoBusca();
                produtoBusca.ShowDialog();

                if (produtoBusca.Produto != null)
                {
                    /* verificando se o produto selecionado não está incluído. */

                    List<Produto> produtosInGrid = montaListaProdutosInGrid();

                    if (produtosInGrid.Contains(produtoBusca.Produto))
                        throw new Exception("Produto já incluído.");

                    /* incluíndo a informação na grid de produto. */

                    dgvProduto.Rows.Add(produtoBusca.Produto.Id, produtoBusca.Produto.Nome, produtoBusca.Produto.Situacao, produtoBusca.Produto.Preco, produtoBusca.Produto.Tipo, produtoBusca.Produto.Custo, calculaLucro((Decimal)produtoBusca.Produto.Preco, (Decimal)produtoBusca.Produto.Custo), (bool)produtoBusca.Produto.PadraoContrato);
                    dgvProduto.CurrentCell = dgvProduto[0, dgvProduto.RowCount - 1];
                }
                
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        protected virtual void btnExcluirProduto_Click(object sender, EventArgs e)
        {
            try
            {

                if (dgvProduto.CurrentRow == null)
                    throw new Exception("Selecione um produto.");

                dgvProduto.Rows.RemoveAt(dgvProduto.CurrentRow.Index);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void montatelaInformacaoContrato(Contrato contrato)
        {
            try
            {
                /* recuperando informações do contrato e preenchendo na tela */
                FinanceiroFacade financeiroFacade = FinanceiroFacade.getInstance();

                DataSet dsExame = financeiroFacade.findAllContratoExameByContrato(contrato);
                DataSet dsProduto = financeiroFacade.findAllContratoProdutoByContrato(contrato);
                
                /* incluíndo a informação de exames na grid */

                foreach (DataRow dvRow in dsExame.Tables[0].Rows)
                    dgvExame.Rows.Add((long)dvRow["id_exame"], (string)dvRow["descricao"], (string)dvRow["situacao_exame"], (bool)dvRow["laboratorio"], Convert.ToDecimal(dvRow["preco_contrato"]), (int)dvRow["prioridade"], Convert.ToDecimal(dvRow["custo_contrato"]), (bool)dvRow["externo"], (bool)dvRow["libera_documento"], Math.Round(Convert.ToDecimal(dvRow["lucro"]),2), (bool)dvRow["padrao_contrato"]);

                /* incluindo a informação de produtos na grid */

                foreach (DataRow dvRow in dsProduto.Tables[0].Rows)
                    dgvProduto.Rows.Add((long)dvRow["id_produto"], (string)dvRow["nome"], (string)dvRow["situacao_produto"], Convert.ToDecimal(dvRow["preco_contratado"]), (string)dvRow["tipo"], Convert.ToDecimal(dvRow["custo_contrato"]), Math.Round(Convert.ToDecimal(dvRow["lucro"]),2), (bool)dvRow["padrao_contrato"]);

                /* montando condições do contrato */

                cbBloqueio.SelectedValue = contrato.BloqueiaInadimplencia == true ? "true" : "false";
                textDiasBloqueio.Text = contrato.DiasInadimplenciaBloqueio == null ? string.Empty : contrato.DiasInadimplenciaBloqueio.ToString();

                cbNumeroVidas.SelectedValue = contrato.GeraNumeroVidas == true ? "true" : "false";
                textValorNumeroVidas.Text = Convert.ToString(contrato.ValorNumeroVidas);

                cbCobrancaAutomatica.SelectedValue = contrato.CobrancaAutomatica == true ? "true" : "false";
                textDiaCobranca.Text = contrato.DataCobranca == null ? string.Empty : contrato.DataCobranca.ToString();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        protected virtual void MontaDataGridExame()
        {
            
            dgvExame.ColumnCount = 11;

            
            dgvExame.Columns[0].HeaderText = "Código";
            dgvExame.Columns[0].ReadOnly = true;
            
            dgvExame.Columns[1].HeaderText = "Exame";
            dgvExame.Columns[1].ReadOnly = true;

            dgvExame.Columns[2].HeaderText = "Situação";
            dgvExame.Columns[2].Visible = false;

            dgvExame.Columns[3].HeaderText = "Laboratório";
            dgvExame.Columns[3].Visible = false;

            dgvExame.Columns[4].HeaderText = "Preço no Contrato R$";
            dgvExame.Columns[4].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
            dgvExame.Columns[4].HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleRight;
            dgvExame.Columns[4].DefaultCellStyle.BackColor = Color.LemonChiffon;

            dgvExame.Columns[5].HeaderText = "Prioridade";
            dgvExame.Columns[5].Visible = false;

            dgvExame.Columns[6].HeaderText = "Custo no Contrato R$";
            dgvExame.Columns[6].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
            dgvExame.Columns[6].HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleRight;
            dgvExame.Columns[6].DefaultCellStyle.BackColor = Color.LemonChiffon;

            dgvExame.Columns[7].HeaderText = "Externo";
            dgvExame.Columns[7].Visible = false;

            dgvExame.Columns[8].HeaderText = "Libera Documentação";
            dgvExame.Columns[8].Visible = false;

            dgvExame.Columns[9].HeaderText = "Lucro Bruto %";
            dgvExame.Columns[9].ReadOnly = true;
            dgvExame.Columns[9].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
            dgvExame.Columns[9].HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleRight;
            dgvExame.Columns[9].DefaultCellStyle.BackColor = Color.White;

            dgvExame.Columns[10].HeaderText = "Padrao Contrato";
            dgvExame.Columns[10].Visible = false;


        }

        protected virtual void MontaDataGridProduto()
        {

            dgvProduto.ColumnCount = 8;

            dgvProduto.Columns[0].HeaderText = "Código";
            dgvProduto.Columns[0].ReadOnly = true;

            dgvProduto.Columns[1].HeaderText = "Produto";
            dgvProduto.Columns[1].ReadOnly = true;

            dgvProduto.Columns[2].HeaderText = "Situação";
            dgvProduto.Columns[2].Visible = false;

            dgvProduto.Columns[3].HeaderText = "Preço no Contrato R$";
            dgvProduto.Columns[3].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
            dgvProduto.Columns[3].HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleRight;
            dgvProduto.Columns[3].DefaultCellStyle.BackColor = Color.LemonChiffon;

            dgvProduto.Columns[4].HeaderText = "Tipo";
            dgvProduto.Columns[4].Visible = false;

            dgvProduto.Columns[5].HeaderText = "Custo no Contrato R$";
            dgvProduto.Columns[5].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
            dgvProduto.Columns[5].HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleRight;
            dgvProduto.Columns[5].DefaultCellStyle.BackColor = Color.LemonChiffon;

            dgvProduto.Columns[6].HeaderText = "Lucro Bruto %";
            dgvProduto.Columns[6].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
            dgvProduto.Columns[6].HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleRight;
            dgvProduto.Columns[6].DefaultCellStyle.BackColor = Color.White;
            dgvProduto.Columns[6].ReadOnly = true;

            dgvProduto.Columns[7].HeaderText = "Padrão Contrato";
            dgvProduto.Columns[7].Visible = false;


            
        }

        protected virtual List<Exame> montaListaExamesInGrid()
        {
            List<Exame> examesInGrid = new List<Exame>();

            try
            {
                foreach (DataGridViewRow dvRow in dgvExame.Rows)
                    examesInGrid.Add(new Exame((long)dvRow.Cells[0].Value, dvRow.Cells[1].Value.ToString(), dvRow.Cells[2].Value.ToString(), Convert.ToBoolean(dvRow.Cells[3].Value), Convert.ToDecimal(dvRow.Cells[4].Value), Convert.ToInt32(dvRow.Cells[5].Value), Convert.ToDecimal(dvRow.Cells[6].Value), Convert.ToBoolean(dvRow.Cells[7].Value), Convert.ToBoolean(dvRow.Cells[8].Value), string.Empty, false, null, (bool)dvRow.Cells[10].Value));
                
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

            return examesInGrid;
        }

        protected virtual List<Produto> montaListaProdutosInGrid()
        {

            List<Produto> produtosInGrid = new List<Produto>();
            try
            {
                foreach (DataGridViewRow dvRow in dgvProduto.Rows)
                    produtosInGrid.Add(new Produto((long)dvRow.Cells[0].Value, dvRow.Cells[1].Value.ToString(), dvRow.Cells[3].Value.ToString(), Convert.ToDecimal(dvRow.Cells[3].Value), dvRow.Cells[4].Value.ToString(), Convert.ToDecimal(dvRow.Cells[5].Value), (bool)dvRow.Cells[7].Value, string.Empty));

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

            return produtosInGrid;
        }

        protected virtual bool verificaValoresZerados()
        {
            bool valida = true;
            StringBuilder mensagem = new StringBuilder();

            try
            {
                foreach (DataGridViewRow dvRow in dgvExame.Rows)
                {

                    if (Convert.ToDecimal(dvRow.Cells[4].Value) == Convert.ToDecimal(0))
                    {
                        valida = false;
                        mensagem.Append("Tem exames com preço zerado. Verifique." + System.Environment.NewLine);
                    }
                }
        
                foreach (DataGridViewRow dvRow in dgvProduto.Rows)
                {
                    if (Convert.ToDecimal(dvRow.Cells[3].Value) == Convert.ToDecimal(0))
                    {
                        valida = false;
                        mensagem.Append("Tem produtos com preço zerado. Verifique." + System.Environment.NewLine);
                    }
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

            return valida;
        }

        protected bool validaDados()
        {
            bool retorno = true;
            try
            {
                if (cliente == null)
                    throw new Exception("Cliente é obrigatório.");

                if (dgvExame.Rows.Count == 0 && dgvProduto.Rows.Count == 0)
                    throw new Exception("O contrato deverá pelo menos ter um exame ou um produto cadastrado.");

                if (Convert.ToBoolean(((SelectItem)cbBloqueio.SelectedItem).Valor) && String.IsNullOrEmpty(textDiasBloqueio.Text.Trim()) )
                    throw new Exception("Você marcou a opção bloqueio por inadimplencia. É obrigatório digitar o campo dias para bloquear.");

                if (Convert.ToBoolean(((SelectItem)cbValorMensal.SelectedItem).Valor) && String.IsNullOrEmpty(textValorMensalidade.Text.Trim()))
                    throw new Exception("Você informou que será gerada uma mensalidade para o cliente. Informe o valor da mensalidade.");

                if (Convert.ToBoolean(((SelectItem)cbNumeroVidas.SelectedItem).Valor) &&  String.IsNullOrEmpty(textValorNumeroVidas.Text.Trim()))
                    throw new Exception("Você informou que será gerada uma mensalidade sobre o número de vidas. Informe o valor por vidas.");

                if (Convert.ToBoolean(((SelectItem)cbCobrancaAutomatica.SelectedItem).Valor) && String.IsNullOrEmpty(textDiaCobranca.Text.Trim()))
                    throw new Exception("Você marcou a opção cobrança automática para o cliente. É obrigatório digitar o dia que será gerado a cobrança.");

                if (dataTermino.Checked && Convert.ToDateTime(dataInicio.Text) > Convert.ToDateTime(dataTermino.Text) )
                    throw new Exception("A data Final não pode ser menor que a data inicial. Reveja os valores selecionados.");

            }
            catch (Exception ex)
            {
                retorno = false;
                MessageBox.Show(ex.Message, "Atençao", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                
            }
            return retorno;
        }

        protected virtual void dgvExame_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = ValidaCampoHelper.FormataDigitacaoNumero(sender, e, dgvExame.CurrentCell.EditedFormattedValue.ToString(), 2);
        }

        protected virtual void dgvExame_EditingControlShowing(object sender, DataGridViewEditingControlShowingEventArgs e)
        {
            if (e.Control is DataGridViewTextBoxEditingControl)
                e.Control.KeyPress += new KeyPressEventHandler(dgvExame_KeyPress);
        }

        protected virtual void dgvExame_CellBeginEdit(object sender, DataGridViewCellCancelEventArgs e)
        {
            dgvExame.CurrentCell.Tag = dgvExame.CurrentCell.Value;
            dgvExame.CurrentCell.Value = String.Empty;
        }

        protected virtual void dgvExame_CellEndEdit(object sender, DataGridViewCellEventArgs e)
        {
            if (String.IsNullOrEmpty(dgvExame.CurrentCell.EditedFormattedValue.ToString()))
                dgvExame.CurrentCell.Value = dgvExame.CurrentCell.Tag.ToString();

            dgvExame.CurrentCell.Value = ValidaCampoHelper.FormataValorMonetario(dgvExame.CurrentCell.Value.ToString());

            dgvExame.Rows[e.RowIndex].Cells[9].Value = calculaLucro(Convert.ToDecimal(dgvExame.Rows[e.RowIndex].Cells[4].EditedFormattedValue), Convert.ToDecimal(dgvExame.Rows[e.RowIndex].Cells[6].EditedFormattedValue)); 
            
        }

        protected void dgvProduto_CellBeginEdit(object sender, DataGridViewCellCancelEventArgs e)
        {
            dgvProduto.CurrentCell.Tag = dgvProduto.CurrentCell.Value;
            dgvProduto.CurrentCell.Value = String.Empty;
        }

        protected virtual void dgvProduto_CellEndEdit(object sender, DataGridViewCellEventArgs e)
        {
            if (String.IsNullOrEmpty(dgvProduto.CurrentCell.EditedFormattedValue.ToString()))
                dgvProduto.CurrentCell.Value = dgvProduto.CurrentCell.Tag.ToString();

            dgvProduto.CurrentCell.Value = ValidaCampoHelper.FormataValorMonetario(dgvProduto.CurrentCell.Value.ToString());

            dgvProduto.Rows[e.RowIndex].Cells[6].Value = calculaLucro(Convert.ToDecimal(dgvProduto.Rows[e.RowIndex].Cells[3].EditedFormattedValue), Convert.ToDecimal(dgvProduto.Rows[e.RowIndex].Cells[5].EditedFormattedValue));
        }

        protected void dgvProduto_EditingControlShowing(object sender, DataGridViewEditingControlShowingEventArgs e)
        {
            if (e.Control is DataGridViewTextBoxEditingControl)
                e.Control.KeyPress += new KeyPressEventHandler(dgvProduto_KeyPress);
        }

        protected void dgvProduto_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = ValidaCampoHelper.FormataDigitacaoNumero(sender, e, dgvProduto.CurrentCell.EditedFormattedValue.ToString(), 2);
        }

        protected void textDiasBloqueio_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = ValidaCampoHelper.FormataCampoNumerico(sender, e);
        }

        protected void textValorMensalidade_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = ValidaCampoHelper.FormataDigitacaoNumero(sender, e, textValorMensalidade.Text, 2);
        }

        protected void textValorNumeroVidas_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = ValidaCampoHelper.FormataDigitacaoNumero(sender, e, textValorNumeroVidas.Text, 2);
        }

        protected void textDiaCobranca_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = ValidaCampoHelper.FormataCampoNumerico(sender, e);
        }

        protected void textDiasBloqueio_Enter(object sender, EventArgs e)
        {
            textDiasBloqueio.Tag = textDiasBloqueio.Text;
            textDiasBloqueio.Text = string.Empty;
        }

        protected void textDiasBloqueio_Leave(object sender, EventArgs e)
        {
            if (Convert.ToBoolean(((SelectItem)cbBloqueio.SelectedItem).Valor) && string.IsNullOrEmpty(textDiasBloqueio.Text))
                textDiasBloqueio.Text = textDiasBloqueio.Tag.ToString();
            
        }

        protected void textValorMensalidade_Enter(object sender, EventArgs e)
        {
            textValorMensalidade.Tag = textValorMensalidade.Text;
            textValorMensalidade.Text = string.Empty;
        }

        protected void textValorMensalidade_Leave(object sender, EventArgs e)
        {
            if (Convert.ToBoolean(((SelectItem)cbValorMensal.SelectedItem).Valor) && string.IsNullOrEmpty(textValorMensalidade.Text))
                textValorMensalidade.Text = ValidaCampoHelper.FormataValorMonetario(textValorMensalidade.Tag.ToString());
            else
                textValorMensalidade.Text = ValidaCampoHelper.FormataValorMonetario(textValorMensalidade.Text);
                

        }

        protected void textValorNumeroVidas_Enter(object sender, EventArgs e)
        {
            textValorNumeroVidas.Tag = textValorNumeroVidas.Text;
            textValorNumeroVidas.Text = string.Empty;
        }

        protected void textValorNumeroVidas_Leave(object sender, EventArgs e)
        {
            if (Convert.ToBoolean(((SelectItem)cbNumeroVidas.SelectedItem).Valor) && string.IsNullOrEmpty(textValorNumeroVidas.Text))
                textValorNumeroVidas.Text = ValidaCampoHelper.FormataValorMonetario(textValorNumeroVidas.Tag.ToString());
            else
                textValorNumeroVidas.Text = ValidaCampoHelper.FormataValorMonetario(textValorNumeroVidas.Text);
                
        }

        protected void textDiaCobranca_Enter(object sender, EventArgs e)
        {
            textDiaCobranca.Tag = textDiaCobranca.Text;
            textDiaCobranca.Text = string.Empty;
        }

        protected void textDiaCobranca_Leave(object sender, EventArgs e)
        {
            if (Convert.ToBoolean(((SelectItem)cbCobrancaAutomatica.SelectedItem).Valor) && string.IsNullOrEmpty(textDiaCobranca.Text))
                textDiaCobranca.Text = textDiaCobranca.Tag.ToString();
            
        }

        protected void dgvExame_DataBindingComplete(object sender, DataGridViewBindingCompleteEventArgs e)
        {
            DataGridView dgv = sender as DataGridView;
            dgv.ClearSelection();
        }

        protected void dgvProduto_DataBindingComplete(object sender, DataGridViewBindingCompleteEventArgs e)
        {
            DataGridView dgv = sender as DataGridView;
            dgv.ClearSelection();
        }

        protected string calculaLucro(decimal preco, decimal custo)
        {
            return preco == 0 || custo == 0 ? "0,00" : Convert.ToString(Math.Round((((preco / custo) - 1) * 100),2));
        }

        private void frmContratosIncluir_Load(object sender, EventArgs e)
        {
            MontaDataGridExame();
            MontaDataGridProduto();
        }

        protected virtual void dgvExame_RowPrePaint(object sender, DataGridViewRowPrePaintEventArgs e)
        {

        }

        protected virtual void dgvProduto_RowPrePaint(object sender, DataGridViewRowPrePaintEventArgs e)
        {

        }

        


    }
}
