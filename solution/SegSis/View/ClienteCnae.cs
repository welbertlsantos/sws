﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using SegSis.Facade;
using SegSis.Entidade;
using SegSis.Excecao;
using SegSis.View.ViewHelper;
using System.Text.RegularExpressions;
using SegSis.View.Resources;


namespace SegSis.View
{
    public partial class frm_cliente_cnae_selecionar : BaseFormConsulta
    {

        private HashSet<Cnae> cnaeSelecionados;

        public frm_cliente_cnae_selecionar()
        {
            cnaeSelecionados = new HashSet<Cnae>();
            InitializeComponent();
        }

        private void btn_limpar_Click(object sender, EventArgs e)
        {
            text_codCnae.Text = String.Empty;
            text_atividade.Text = String.Empty;
            this.grd_cnae.Columns.Clear();

        }

        private void btn_fechar_Click(object sender, EventArgs e)
        {
            this.Close();

        }

        private void btn_ok_Click(object sender, EventArgs e)
        {
            Cnae cnae = null;

            foreach (DataGridViewRow dvRow in grd_cnae.SelectedRows)
            {

                Int32 cellValue = dvRow.Index;

                Int32 id = (Int32)grd_cnae.Rows[cellValue].Cells[0].Value; // id do cnae
                String codCnae = (String)grd_cnae.Rows[cellValue].Cells[1].Value; // codigo do cnae
                String atividade = (String)grd_cnae.Rows[cellValue].Cells[2].Value; // Atividade
                
                cnae = new Cnae(id, codCnae, atividade);

                cnaeSelecionados.Add(cnae);
            }

            this.Close();
        }

        

        private void btn_buscar_Click(object sender, EventArgs e)
        {
            try
            {
                montaDataGrid();
                text_codCnae.Focus();
            }
            catch (Exception Ex)
            {
                MessageBox.Show(Ex.Message);
            }
        }

        public void montaDataGrid()
        {
            ClienteFacade clienteFacade = ClienteFacade.getInstance();

            Cnae cnae = new Cnae();

            cnae.setCodCnae(text_codCnae.Text);
            cnae.setAtividade(text_atividade.Text);

            DataSet ds = clienteFacade.findCnaeByFilter(cnae);

            grd_cnae.DataSource = ds.Tables["Cnaes"].DefaultView;
            grd_cnae.RowsDefaultCellStyle.BackColor = Color.White;

            grd_cnae.Columns[0].HeaderText = "Id_Cnae";
            grd_cnae.Columns[1].HeaderText = "Codigo Cnae";
            grd_cnae.Columns[2].HeaderText = "Atividade";

            grd_cnae.Columns[0].Visible = false;
        }

        public HashSet<Cnae> getCnaeSelecionados()
        {
            return this.cnaeSelecionados;
        }
    
    }
}
