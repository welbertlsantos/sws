﻿namespace SWS.View
{
    partial class frmEmpresaPrincipal
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            this.flpAcao = new System.Windows.Forms.FlowLayoutPanel();
            this.bt_incluir = new System.Windows.Forms.Button();
            this.bt_alterar = new System.Windows.Forms.Button();
            this.bt_excluir = new System.Windows.Forms.Button();
            this.btn_reativar = new System.Windows.Forms.Button();
            this.bt_detalhar = new System.Windows.Forms.Button();
            this.bt_limpar = new System.Windows.Forms.Button();
            this.bt_pesquisar = new System.Windows.Forms.Button();
            this.bt_fechar = new System.Windows.Forms.Button();
            this.lblEmpresa = new System.Windows.Forms.TextBox();
            this.lblCNPJ = new System.Windows.Forms.TextBox();
            this.lblSituacao = new System.Windows.Forms.TextBox();
            this.grb_gride = new System.Windows.Forms.GroupBox();
            this.dgv_empresa = new System.Windows.Forms.DataGridView();
            this.lblCorDesativado = new System.Windows.Forms.Label();
            this.lblTextoDesativado = new System.Windows.Forms.Label();
            this.text_cnpj = new System.Windows.Forms.MaskedTextBox();
            this.text_razaoSocial = new System.Windows.Forms.TextBox();
            this.cbSituacao = new SWS.ComboBoxWithBorder();
            ((System.ComponentModel.ISupportInitialize)(this.spForm)).BeginInit();
            this.spForm.Panel1.SuspendLayout();
            this.spForm.Panel2.SuspendLayout();
            this.spForm.SuspendLayout();
            this.pnlForm.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.banner)).BeginInit();
            this.flpAcao.SuspendLayout();
            this.grb_gride.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgv_empresa)).BeginInit();
            this.SuspendLayout();
            // 
            // spForm
            // 
            // 
            // spForm.Panel1
            // 
            this.spForm.Panel1.Controls.Add(this.flpAcao);
            // 
            // pnlForm
            // 
            this.pnlForm.Controls.Add(this.cbSituacao);
            this.pnlForm.Controls.Add(this.text_cnpj);
            this.pnlForm.Controls.Add(this.text_razaoSocial);
            this.pnlForm.Controls.Add(this.lblTextoDesativado);
            this.pnlForm.Controls.Add(this.lblCorDesativado);
            this.pnlForm.Controls.Add(this.grb_gride);
            this.pnlForm.Controls.Add(this.lblSituacao);
            this.pnlForm.Controls.Add(this.lblCNPJ);
            this.pnlForm.Controls.Add(this.lblEmpresa);
            // 
            // flpAcao
            // 
            this.flpAcao.Controls.Add(this.bt_incluir);
            this.flpAcao.Controls.Add(this.bt_alterar);
            this.flpAcao.Controls.Add(this.bt_excluir);
            this.flpAcao.Controls.Add(this.btn_reativar);
            this.flpAcao.Controls.Add(this.bt_detalhar);
            this.flpAcao.Controls.Add(this.bt_limpar);
            this.flpAcao.Controls.Add(this.bt_pesquisar);
            this.flpAcao.Controls.Add(this.bt_fechar);
            this.flpAcao.Dock = System.Windows.Forms.DockStyle.Fill;
            this.flpAcao.Location = new System.Drawing.Point(0, 0);
            this.flpAcao.Name = "flpAcao";
            this.flpAcao.Size = new System.Drawing.Size(784, 35);
            this.flpAcao.TabIndex = 0;
            // 
            // bt_incluir
            // 
            this.bt_incluir.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.bt_incluir.Image = global::SWS.Properties.Resources.icone_mais;
            this.bt_incluir.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.bt_incluir.Location = new System.Drawing.Point(3, 3);
            this.bt_incluir.Name = "bt_incluir";
            this.bt_incluir.Size = new System.Drawing.Size(71, 22);
            this.bt_incluir.TabIndex = 15;
            this.bt_incluir.TabStop = false;
            this.bt_incluir.Text = "&Incluir";
            this.bt_incluir.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.bt_incluir.UseVisualStyleBackColor = true;
            this.bt_incluir.Click += new System.EventHandler(this.bt_incluir_Click);
            // 
            // bt_alterar
            // 
            this.bt_alterar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.bt_alterar.Image = global::SWS.Properties.Resources.Alterar;
            this.bt_alterar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.bt_alterar.Location = new System.Drawing.Point(80, 3);
            this.bt_alterar.Name = "bt_alterar";
            this.bt_alterar.Size = new System.Drawing.Size(71, 22);
            this.bt_alterar.TabIndex = 16;
            this.bt_alterar.TabStop = false;
            this.bt_alterar.Text = "&Alterar";
            this.bt_alterar.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.bt_alterar.UseVisualStyleBackColor = true;
            this.bt_alterar.Click += new System.EventHandler(this.bt_alterar_Click);
            // 
            // bt_excluir
            // 
            this.bt_excluir.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.bt_excluir.Image = global::SWS.Properties.Resources.lixeira;
            this.bt_excluir.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.bt_excluir.Location = new System.Drawing.Point(157, 3);
            this.bt_excluir.Name = "bt_excluir";
            this.bt_excluir.Size = new System.Drawing.Size(71, 22);
            this.bt_excluir.TabIndex = 17;
            this.bt_excluir.TabStop = false;
            this.bt_excluir.Text = "&Excluir";
            this.bt_excluir.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.bt_excluir.UseVisualStyleBackColor = true;
            this.bt_excluir.Click += new System.EventHandler(this.bt_excluir_Click);
            // 
            // btn_reativar
            // 
            this.btn_reativar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_reativar.Image = global::SWS.Properties.Resources.devolucao;
            this.btn_reativar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btn_reativar.Location = new System.Drawing.Point(234, 3);
            this.btn_reativar.Name = "btn_reativar";
            this.btn_reativar.Size = new System.Drawing.Size(71, 22);
            this.btn_reativar.TabIndex = 20;
            this.btn_reativar.TabStop = false;
            this.btn_reativar.Text = "&Reativar";
            this.btn_reativar.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btn_reativar.UseVisualStyleBackColor = true;
            this.btn_reativar.Click += new System.EventHandler(this.btn_reativar_Click);
            // 
            // bt_detalhar
            // 
            this.bt_detalhar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.bt_detalhar.Image = global::SWS.Properties.Resources.lupa;
            this.bt_detalhar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.bt_detalhar.Location = new System.Drawing.Point(311, 3);
            this.bt_detalhar.Name = "bt_detalhar";
            this.bt_detalhar.Size = new System.Drawing.Size(71, 22);
            this.bt_detalhar.TabIndex = 18;
            this.bt_detalhar.TabStop = false;
            this.bt_detalhar.Text = "&Detalhar";
            this.bt_detalhar.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.bt_detalhar.UseVisualStyleBackColor = true;
            this.bt_detalhar.Click += new System.EventHandler(this.bt_detalhar_Click);
            // 
            // bt_limpar
            // 
            this.bt_limpar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.bt_limpar.Image = global::SWS.Properties.Resources.vassoura;
            this.bt_limpar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.bt_limpar.Location = new System.Drawing.Point(388, 3);
            this.bt_limpar.Name = "bt_limpar";
            this.bt_limpar.Size = new System.Drawing.Size(71, 22);
            this.bt_limpar.TabIndex = 14;
            this.bt_limpar.TabStop = false;
            this.bt_limpar.Text = "Limpar";
            this.bt_limpar.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.bt_limpar.UseVisualStyleBackColor = true;
            this.bt_limpar.Click += new System.EventHandler(this.bt_limpar_Click);
            // 
            // bt_pesquisar
            // 
            this.bt_pesquisar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.bt_pesquisar.Image = global::SWS.Properties.Resources.lupa;
            this.bt_pesquisar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.bt_pesquisar.Location = new System.Drawing.Point(465, 3);
            this.bt_pesquisar.Name = "bt_pesquisar";
            this.bt_pesquisar.Size = new System.Drawing.Size(71, 22);
            this.bt_pesquisar.TabIndex = 13;
            this.bt_pesquisar.TabStop = false;
            this.bt_pesquisar.Text = "Pesquisar";
            this.bt_pesquisar.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.bt_pesquisar.UseVisualStyleBackColor = true;
            this.bt_pesquisar.Click += new System.EventHandler(this.bt_pesquisar_Click);
            // 
            // bt_fechar
            // 
            this.bt_fechar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.bt_fechar.Image = global::SWS.Properties.Resources.close;
            this.bt_fechar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.bt_fechar.Location = new System.Drawing.Point(542, 3);
            this.bt_fechar.Name = "bt_fechar";
            this.bt_fechar.Size = new System.Drawing.Size(71, 22);
            this.bt_fechar.TabIndex = 19;
            this.bt_fechar.TabStop = false;
            this.bt_fechar.Text = "&Fechar";
            this.bt_fechar.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.bt_fechar.UseVisualStyleBackColor = true;
            this.bt_fechar.Click += new System.EventHandler(this.bt_fechar_Click);
            // 
            // lblEmpresa
            // 
            this.lblEmpresa.BackColor = System.Drawing.Color.LightGray;
            this.lblEmpresa.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblEmpresa.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblEmpresa.Location = new System.Drawing.Point(8, 8);
            this.lblEmpresa.Name = "lblEmpresa";
            this.lblEmpresa.ReadOnly = true;
            this.lblEmpresa.Size = new System.Drawing.Size(100, 21);
            this.lblEmpresa.TabIndex = 0;
            this.lblEmpresa.TabStop = false;
            this.lblEmpresa.Text = "Empresa";
            // 
            // lblCNPJ
            // 
            this.lblCNPJ.BackColor = System.Drawing.Color.LightGray;
            this.lblCNPJ.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblCNPJ.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCNPJ.Location = new System.Drawing.Point(8, 28);
            this.lblCNPJ.Name = "lblCNPJ";
            this.lblCNPJ.ReadOnly = true;
            this.lblCNPJ.Size = new System.Drawing.Size(100, 21);
            this.lblCNPJ.TabIndex = 1;
            this.lblCNPJ.TabStop = false;
            this.lblCNPJ.Text = "CNPJ";
            // 
            // lblSituacao
            // 
            this.lblSituacao.BackColor = System.Drawing.Color.LightGray;
            this.lblSituacao.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblSituacao.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSituacao.Location = new System.Drawing.Point(8, 48);
            this.lblSituacao.Name = "lblSituacao";
            this.lblSituacao.ReadOnly = true;
            this.lblSituacao.Size = new System.Drawing.Size(100, 21);
            this.lblSituacao.TabIndex = 2;
            this.lblSituacao.TabStop = false;
            this.lblSituacao.Text = "Situação";
            // 
            // grb_gride
            // 
            this.grb_gride.BackColor = System.Drawing.Color.White;
            this.grb_gride.Controls.Add(this.dgv_empresa);
            this.grb_gride.Location = new System.Drawing.Point(8, 75);
            this.grb_gride.Name = "grb_gride";
            this.grb_gride.Size = new System.Drawing.Size(735, 355);
            this.grb_gride.TabIndex = 6;
            this.grb_gride.TabStop = false;
            this.grb_gride.Text = "Empresas";
            // 
            // dgv_empresa
            // 
            this.dgv_empresa.AllowUserToAddRows = false;
            this.dgv_empresa.AllowUserToDeleteRows = false;
            this.dgv_empresa.AllowUserToOrderColumns = true;
            this.dgv_empresa.AllowUserToResizeRows = false;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.Gainsboro;
            this.dgv_empresa.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle1;
            this.dgv_empresa.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this.dgv_empresa.BackgroundColor = System.Drawing.Color.White;
            this.dgv_empresa.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.Color.LightGray;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgv_empresa.DefaultCellStyle = dataGridViewCellStyle2;
            this.dgv_empresa.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgv_empresa.Location = new System.Drawing.Point(3, 16);
            this.dgv_empresa.MultiSelect = false;
            this.dgv_empresa.Name = "dgv_empresa";
            this.dgv_empresa.ReadOnly = true;
            this.dgv_empresa.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgv_empresa.Size = new System.Drawing.Size(729, 336);
            this.dgv_empresa.TabIndex = 5;
            this.dgv_empresa.CellMouseDoubleClick += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.dgv_empresa_CellMouseDoubleClick);
            this.dgv_empresa.DataBindingComplete += new System.Windows.Forms.DataGridViewBindingCompleteEventHandler(this.dgv_empresa_DataBindingComplete);
            this.dgv_empresa.RowPrePaint += new System.Windows.Forms.DataGridViewRowPrePaintEventHandler(this.dgv_empresa_RowPrePaint);
            // 
            // lblCorDesativado
            // 
            this.lblCorDesativado.AutoSize = true;
            this.lblCorDesativado.BackColor = System.Drawing.Color.Red;
            this.lblCorDesativado.Location = new System.Drawing.Point(11, 434);
            this.lblCorDesativado.Name = "lblCorDesativado";
            this.lblCorDesativado.Size = new System.Drawing.Size(13, 13);
            this.lblCorDesativado.TabIndex = 7;
            this.lblCorDesativado.Text = "  ";
            // 
            // lblTextoDesativado
            // 
            this.lblTextoDesativado.AutoSize = true;
            this.lblTextoDesativado.Location = new System.Drawing.Point(31, 434);
            this.lblTextoDesativado.Name = "lblTextoDesativado";
            this.lblTextoDesativado.Size = new System.Drawing.Size(103, 13);
            this.lblTextoDesativado.TabIndex = 8;
            this.lblTextoDesativado.Text = "Empresa desativada";
            // 
            // text_cnpj
            // 
            this.text_cnpj.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.text_cnpj.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.text_cnpj.ForeColor = System.Drawing.Color.Blue;
            this.text_cnpj.Location = new System.Drawing.Point(107, 28);
            this.text_cnpj.Mask = "99,999,999/9999-99";
            this.text_cnpj.Name = "text_cnpj";
            this.text_cnpj.PromptChar = ' ';
            this.text_cnpj.Size = new System.Drawing.Size(634, 21);
            this.text_cnpj.TabIndex = 10;
            // 
            // text_razaoSocial
            // 
            this.text_razaoSocial.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.text_razaoSocial.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.text_razaoSocial.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.text_razaoSocial.Location = new System.Drawing.Point(107, 8);
            this.text_razaoSocial.MaxLength = 100;
            this.text_razaoSocial.Name = "text_razaoSocial";
            this.text_razaoSocial.Size = new System.Drawing.Size(634, 21);
            this.text_razaoSocial.TabIndex = 9;
            // 
            // cbSituacao
            // 
            this.cbSituacao.BackColor = System.Drawing.Color.LightGray;
            this.cbSituacao.BorderColor = System.Drawing.Color.DimGray;
            this.cbSituacao.BorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid;
            this.cbSituacao.FormattingEnabled = true;
            this.cbSituacao.Location = new System.Drawing.Point(107, 48);
            this.cbSituacao.Name = "cbSituacao";
            this.cbSituacao.Size = new System.Drawing.Size(634, 21);
            this.cbSituacao.TabIndex = 11;
            // 
            // frmEmpresaPrincipal
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(784, 561);
            this.Name = "frmEmpresaPrincipal";
            this.Text = "GERENCIAR EMPRESAS";
            this.spForm.Panel1.ResumeLayout(false);
            this.spForm.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.spForm)).EndInit();
            this.spForm.ResumeLayout(false);
            this.pnlForm.ResumeLayout(false);
            this.pnlForm.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.banner)).EndInit();
            this.flpAcao.ResumeLayout(false);
            this.grb_gride.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgv_empresa)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.FlowLayoutPanel flpAcao;
        private System.Windows.Forms.Button bt_limpar;
        private System.Windows.Forms.Button bt_pesquisar;
        private System.Windows.Forms.Button btn_reativar;
        private System.Windows.Forms.Button bt_fechar;
        private System.Windows.Forms.Button bt_detalhar;
        private System.Windows.Forms.Button bt_incluir;
        private System.Windows.Forms.Button bt_excluir;
        private System.Windows.Forms.Button bt_alterar;
        private System.Windows.Forms.TextBox lblSituacao;
        private System.Windows.Forms.TextBox lblCNPJ;
        private System.Windows.Forms.TextBox lblEmpresa;
        private System.Windows.Forms.Label lblTextoDesativado;
        private System.Windows.Forms.Label lblCorDesativado;
        private System.Windows.Forms.GroupBox grb_gride;
        private System.Windows.Forms.DataGridView dgv_empresa;
        private ComboBoxWithBorder cbSituacao;
        private System.Windows.Forms.MaskedTextBox text_cnpj;
        private System.Windows.Forms.TextBox text_razaoSocial;
    }
}