﻿using SWS.Entidade;
using SWS.Facade;
using SWS.View.Resources;
using SWS.View.ViewHelper;
using SWS.ViewHelper;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace SWS.View
{
    public partial class frmProdutoAlterar : frmProdutoIncluir
    {
        public frmProdutoAlterar(Produto produto) :base()
        {
            InitializeComponent();
            this.produto = produto;

            textNome.Text = produto.Nome;
            textPreco.Text = ValidaCampoHelper.FormataValorMonetario(produto.Preco.ToString());
            textCusto.Text = ValidaCampoHelper.FormataValorMonetario(produto.Custo.ToString());
            cbPadraoContrato.SelectedValue = produto.PadraoContrato == true ? "true" : "false";
            textDescricao.Text = produto.Descricao;
        }

        protected override void btnIncluir_Click(object sender, EventArgs e)
        {
            try
            {
                if (ValidaCampoTela())
                {
                    FinanceiroFacade financeiroFacade = FinanceiroFacade.getInstance();
                    produto = financeiroFacade.updateProduto(new Produto(produto.Id, textNome.Text, ApplicationConstants.ATIVO, (decimal?)Convert.ToDecimal(textPreco.Text), produto.Tipo, (decimal?)Convert.ToDecimal(textCusto.Text), Convert.ToBoolean(((SelectItem)cbPadraoContrato.SelectedItem).Valor), textDescricao.Text));

                    MessageBox.Show("Produto alterado com sucesso.", "Confirmação", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    this.Close();

                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
    }
}
