﻿namespace SWS.View
{
    partial class frmExameIncluir
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            this.flpAcao = new System.Windows.Forms.FlowLayoutPanel();
            this.btn_gravar = new System.Windows.Forms.Button();
            this.btn_limpar = new System.Windows.Forms.Button();
            this.btn_fechar = new System.Windows.Forms.Button();
            this.lblExame = new System.Windows.Forms.TextBox();
            this.lblPreco = new System.Windows.Forms.TextBox();
            this.lblPrecoCusto = new System.Windows.Forms.TextBox();
            this.lblPrioridade = new System.Windows.Forms.TextBox();
            this.lblExameLaboratorio = new System.Windows.Forms.TextBox();
            this.lblExamePrestador = new System.Windows.Forms.TextBox();
            this.lblLiberaDocumento = new System.Windows.Forms.TextBox();
            this.cbLaboratorio = new SWS.ComboBoxWithBorder();
            this.cbExterno = new SWS.ComboBoxWithBorder();
            this.cbDocumento = new SWS.ComboBoxWithBorder();
            this.text_descricao = new System.Windows.Forms.TextBox();
            this.text_precoCusto = new System.Windows.Forms.TextBox();
            this.text_preco = new System.Windows.Forms.TextBox();
            this.text_prioridade = new System.Windows.Forms.TextBox();
            this.grbMedicos = new System.Windows.Forms.GroupBox();
            this.dgvMedico = new System.Windows.Forms.DataGridView();
            this.flpAcaoMedico = new System.Windows.Forms.FlowLayoutPanel();
            this.btIncluirMedico = new System.Windows.Forms.Button();
            this.btExcluirMedico = new System.Windows.Forms.Button();
            this.lblCodigoTuss = new System.Windows.Forms.TextBox();
            this.lblExameComplementar = new System.Windows.Forms.TextBox();
            this.textCodigoTuss = new System.Windows.Forms.TextBox();
            this.cbExameComplementar = new SWS.ComboBoxWithBorder();
            this.lblExameRelatorio = new System.Windows.Forms.TextBox();
            this.textRelatorio = new System.Windows.Forms.TextBox();
            this.btnRelatorio = new System.Windows.Forms.Button();
            this.lblPeriodoVencimento = new System.Windows.Forms.TextBox();
            this.cbPeriodoVencimento = new SWS.ComboBoxWithBorder();
            this.lblPadraoContrato = new System.Windows.Forms.TextBox();
            this.cbPadraoContrato = new SWS.ComboBoxWithBorder();
            this.btnProcedimentoEsocial = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.spForm)).BeginInit();
            this.spForm.Panel1.SuspendLayout();
            this.spForm.Panel2.SuspendLayout();
            this.spForm.SuspendLayout();
            this.pnlForm.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.banner)).BeginInit();
            this.flpAcao.SuspendLayout();
            this.grbMedicos.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvMedico)).BeginInit();
            this.flpAcaoMedico.SuspendLayout();
            this.SuspendLayout();
            // 
            // spForm
            // 
            // 
            // spForm.Panel1
            // 
            this.spForm.Panel1.Controls.Add(this.flpAcao);
            // 
            // pnlForm
            // 
            this.pnlForm.Controls.Add(this.btnProcedimentoEsocial);
            this.pnlForm.Controls.Add(this.cbPadraoContrato);
            this.pnlForm.Controls.Add(this.lblPadraoContrato);
            this.pnlForm.Controls.Add(this.cbPeriodoVencimento);
            this.pnlForm.Controls.Add(this.lblPeriodoVencimento);
            this.pnlForm.Controls.Add(this.btnRelatorio);
            this.pnlForm.Controls.Add(this.textRelatorio);
            this.pnlForm.Controls.Add(this.lblExameRelatorio);
            this.pnlForm.Controls.Add(this.cbExameComplementar);
            this.pnlForm.Controls.Add(this.textCodigoTuss);
            this.pnlForm.Controls.Add(this.lblExameComplementar);
            this.pnlForm.Controls.Add(this.lblCodigoTuss);
            this.pnlForm.Controls.Add(this.flpAcaoMedico);
            this.pnlForm.Controls.Add(this.grbMedicos);
            this.pnlForm.Controls.Add(this.text_prioridade);
            this.pnlForm.Controls.Add(this.text_precoCusto);
            this.pnlForm.Controls.Add(this.text_preco);
            this.pnlForm.Controls.Add(this.text_descricao);
            this.pnlForm.Controls.Add(this.cbDocumento);
            this.pnlForm.Controls.Add(this.cbExterno);
            this.pnlForm.Controls.Add(this.cbLaboratorio);
            this.pnlForm.Controls.Add(this.lblLiberaDocumento);
            this.pnlForm.Controls.Add(this.lblExamePrestador);
            this.pnlForm.Controls.Add(this.lblExameLaboratorio);
            this.pnlForm.Controls.Add(this.lblPrioridade);
            this.pnlForm.Controls.Add(this.lblPrecoCusto);
            this.pnlForm.Controls.Add(this.lblPreco);
            this.pnlForm.Controls.Add(this.lblExame);
            this.pnlForm.Size = new System.Drawing.Size(764, 600);
            // 
            // flpAcao
            // 
            this.flpAcao.Controls.Add(this.btn_gravar);
            this.flpAcao.Controls.Add(this.btn_limpar);
            this.flpAcao.Controls.Add(this.btn_fechar);
            this.flpAcao.Dock = System.Windows.Forms.DockStyle.Fill;
            this.flpAcao.Location = new System.Drawing.Point(0, 0);
            this.flpAcao.Name = "flpAcao";
            this.flpAcao.Size = new System.Drawing.Size(784, 35);
            this.flpAcao.TabIndex = 0;
            // 
            // btn_gravar
            // 
            this.btn_gravar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_gravar.Image = global::SWS.Properties.Resources.Gravar;
            this.btn_gravar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btn_gravar.Location = new System.Drawing.Point(3, 3);
            this.btn_gravar.Name = "btn_gravar";
            this.btn_gravar.Size = new System.Drawing.Size(75, 23);
            this.btn_gravar.TabIndex = 10;
            this.btn_gravar.TabStop = false;
            this.btn_gravar.Text = "&Gravar";
            this.btn_gravar.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btn_gravar.UseVisualStyleBackColor = true;
            this.btn_gravar.Click += new System.EventHandler(this.btn_gravar_Click);
            // 
            // btn_limpar
            // 
            this.btn_limpar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_limpar.Image = global::SWS.Properties.Resources.vassoura;
            this.btn_limpar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btn_limpar.Location = new System.Drawing.Point(84, 3);
            this.btn_limpar.Name = "btn_limpar";
            this.btn_limpar.Size = new System.Drawing.Size(75, 23);
            this.btn_limpar.TabIndex = 9;
            this.btn_limpar.TabStop = false;
            this.btn_limpar.Text = "&Limpar";
            this.btn_limpar.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btn_limpar.UseVisualStyleBackColor = true;
            this.btn_limpar.Click += new System.EventHandler(this.btn_limpar_Click);
            // 
            // btn_fechar
            // 
            this.btn_fechar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_fechar.Image = global::SWS.Properties.Resources.close;
            this.btn_fechar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btn_fechar.Location = new System.Drawing.Point(165, 3);
            this.btn_fechar.Name = "btn_fechar";
            this.btn_fechar.Size = new System.Drawing.Size(75, 23);
            this.btn_fechar.TabIndex = 11;
            this.btn_fechar.TabStop = false;
            this.btn_fechar.Text = "&Fechar";
            this.btn_fechar.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btn_fechar.UseVisualStyleBackColor = true;
            this.btn_fechar.Click += new System.EventHandler(this.btn_fechar_Click);
            // 
            // lblExame
            // 
            this.lblExame.BackColor = System.Drawing.Color.LightGray;
            this.lblExame.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblExame.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblExame.Location = new System.Drawing.Point(9, 8);
            this.lblExame.Name = "lblExame";
            this.lblExame.ReadOnly = true;
            this.lblExame.Size = new System.Drawing.Size(231, 21);
            this.lblExame.TabIndex = 0;
            this.lblExame.TabStop = false;
            this.lblExame.Text = "Nome";
            // 
            // lblPreco
            // 
            this.lblPreco.BackColor = System.Drawing.Color.LightGray;
            this.lblPreco.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblPreco.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPreco.Location = new System.Drawing.Point(9, 28);
            this.lblPreco.Name = "lblPreco";
            this.lblPreco.ReadOnly = true;
            this.lblPreco.Size = new System.Drawing.Size(231, 21);
            this.lblPreco.TabIndex = 1;
            this.lblPreco.TabStop = false;
            this.lblPreco.Text = "Preço de Venda";
            // 
            // lblPrecoCusto
            // 
            this.lblPrecoCusto.BackColor = System.Drawing.Color.LightGray;
            this.lblPrecoCusto.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblPrecoCusto.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPrecoCusto.Location = new System.Drawing.Point(9, 48);
            this.lblPrecoCusto.Name = "lblPrecoCusto";
            this.lblPrecoCusto.ReadOnly = true;
            this.lblPrecoCusto.Size = new System.Drawing.Size(231, 21);
            this.lblPrecoCusto.TabIndex = 2;
            this.lblPrecoCusto.TabStop = false;
            this.lblPrecoCusto.Text = "Preço de Custo";
            // 
            // lblPrioridade
            // 
            this.lblPrioridade.BackColor = System.Drawing.Color.LightGray;
            this.lblPrioridade.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblPrioridade.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPrioridade.Location = new System.Drawing.Point(9, 68);
            this.lblPrioridade.Name = "lblPrioridade";
            this.lblPrioridade.ReadOnly = true;
            this.lblPrioridade.Size = new System.Drawing.Size(231, 21);
            this.lblPrioridade.TabIndex = 3;
            this.lblPrioridade.TabStop = false;
            this.lblPrioridade.Text = "Prioridade na fila";
            // 
            // lblExameLaboratorio
            // 
            this.lblExameLaboratorio.BackColor = System.Drawing.Color.LightGray;
            this.lblExameLaboratorio.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblExameLaboratorio.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblExameLaboratorio.Location = new System.Drawing.Point(9, 88);
            this.lblExameLaboratorio.Name = "lblExameLaboratorio";
            this.lblExameLaboratorio.ReadOnly = true;
            this.lblExameLaboratorio.Size = new System.Drawing.Size(231, 21);
            this.lblExameLaboratorio.TabIndex = 4;
            this.lblExameLaboratorio.TabStop = false;
            this.lblExameLaboratorio.Text = "Exame de laboratório?";
            // 
            // lblExamePrestador
            // 
            this.lblExamePrestador.BackColor = System.Drawing.Color.LightGray;
            this.lblExamePrestador.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblExamePrestador.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblExamePrestador.Location = new System.Drawing.Point(9, 108);
            this.lblExamePrestador.Name = "lblExamePrestador";
            this.lblExamePrestador.ReadOnly = true;
            this.lblExamePrestador.Size = new System.Drawing.Size(231, 21);
            this.lblExamePrestador.TabIndex = 5;
            this.lblExamePrestador.TabStop = false;
            this.lblExamePrestador.Text = "Exame externo?";
            // 
            // lblLiberaDocumento
            // 
            this.lblLiberaDocumento.BackColor = System.Drawing.Color.LightGray;
            this.lblLiberaDocumento.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblLiberaDocumento.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblLiberaDocumento.Location = new System.Drawing.Point(9, 128);
            this.lblLiberaDocumento.Name = "lblLiberaDocumento";
            this.lblLiberaDocumento.ReadOnly = true;
            this.lblLiberaDocumento.Size = new System.Drawing.Size(231, 21);
            this.lblLiberaDocumento.TabIndex = 6;
            this.lblLiberaDocumento.TabStop = false;
            this.lblLiberaDocumento.Text = "Permite impressão de ASO e prontuário?";
            // 
            // cbLaboratorio
            // 
            this.cbLaboratorio.BackColor = System.Drawing.Color.LightGray;
            this.cbLaboratorio.BorderColor = System.Drawing.Color.DimGray;
            this.cbLaboratorio.BorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid;
            this.cbLaboratorio.FormattingEnabled = true;
            this.cbLaboratorio.Location = new System.Drawing.Point(239, 88);
            this.cbLaboratorio.Name = "cbLaboratorio";
            this.cbLaboratorio.Size = new System.Drawing.Size(507, 21);
            this.cbLaboratorio.TabIndex = 5;
            // 
            // cbExterno
            // 
            this.cbExterno.BackColor = System.Drawing.Color.LightGray;
            this.cbExterno.BorderColor = System.Drawing.Color.DimGray;
            this.cbExterno.BorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid;
            this.cbExterno.FormattingEnabled = true;
            this.cbExterno.Location = new System.Drawing.Point(239, 108);
            this.cbExterno.Name = "cbExterno";
            this.cbExterno.Size = new System.Drawing.Size(507, 21);
            this.cbExterno.TabIndex = 6;
            // 
            // cbDocumento
            // 
            this.cbDocumento.BackColor = System.Drawing.Color.LightGray;
            this.cbDocumento.BorderColor = System.Drawing.Color.DimGray;
            this.cbDocumento.BorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid;
            this.cbDocumento.FormattingEnabled = true;
            this.cbDocumento.Location = new System.Drawing.Point(239, 128);
            this.cbDocumento.Name = "cbDocumento";
            this.cbDocumento.Size = new System.Drawing.Size(507, 21);
            this.cbDocumento.TabIndex = 7;
            // 
            // text_descricao
            // 
            this.text_descricao.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.text_descricao.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.text_descricao.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.text_descricao.Location = new System.Drawing.Point(239, 8);
            this.text_descricao.MaxLength = 100;
            this.text_descricao.Name = "text_descricao";
            this.text_descricao.Size = new System.Drawing.Size(507, 21);
            this.text_descricao.TabIndex = 1;
            // 
            // text_precoCusto
            // 
            this.text_precoCusto.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.text_precoCusto.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.text_precoCusto.Location = new System.Drawing.Point(239, 48);
            this.text_precoCusto.MaxLength = 15;
            this.text_precoCusto.Name = "text_precoCusto";
            this.text_precoCusto.Size = new System.Drawing.Size(507, 21);
            this.text_precoCusto.TabIndex = 3;
            this.text_precoCusto.Text = "0,00";
            this.text_precoCusto.Enter += new System.EventHandler(this.text_precoCusto_Enter);
            this.text_precoCusto.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.text_precoCusto_KeyPress);
            this.text_precoCusto.Leave += new System.EventHandler(this.text_precoCusto_Leave);
            // 
            // text_preco
            // 
            this.text_preco.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.text_preco.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.text_preco.Location = new System.Drawing.Point(239, 28);
            this.text_preco.MaxLength = 15;
            this.text_preco.Name = "text_preco";
            this.text_preco.Size = new System.Drawing.Size(507, 21);
            this.text_preco.TabIndex = 2;
            this.text_preco.Text = "0,00";
            this.text_preco.Enter += new System.EventHandler(this.text_preco_Enter);
            this.text_preco.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.text_preco_KeyPress);
            this.text_preco.Leave += new System.EventHandler(this.text_preco_Leave);
            // 
            // text_prioridade
            // 
            this.text_prioridade.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.text_prioridade.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.text_prioridade.Location = new System.Drawing.Point(239, 68);
            this.text_prioridade.MaxLength = 1;
            this.text_prioridade.Name = "text_prioridade";
            this.text_prioridade.Size = new System.Drawing.Size(507, 21);
            this.text_prioridade.TabIndex = 4;
            this.text_prioridade.Text = "0";
            this.text_prioridade.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.text_prioridade_KeyPress);
            // 
            // grbMedicos
            // 
            this.grbMedicos.Controls.Add(this.dgvMedico);
            this.grbMedicos.Location = new System.Drawing.Point(9, 258);
            this.grbMedicos.Name = "grbMedicos";
            this.grbMedicos.Size = new System.Drawing.Size(737, 140);
            this.grbMedicos.TabIndex = 8;
            this.grbMedicos.TabStop = false;
            this.grbMedicos.Text = "Médicos que realizam o exame";
            // 
            // dgvMedico
            // 
            this.dgvMedico.AllowUserToAddRows = false;
            this.dgvMedico.AllowUserToDeleteRows = false;
            this.dgvMedico.AllowUserToResizeRows = false;
            dataGridViewCellStyle3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.dgvMedico.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle3;
            this.dgvMedico.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.DisplayedCells;
            this.dgvMedico.BackgroundColor = System.Drawing.Color.White;
            this.dgvMedico.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle4.BackColor = System.Drawing.Color.Silver;
            dataGridViewCellStyle4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle4.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle4.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle4.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle4.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvMedico.DefaultCellStyle = dataGridViewCellStyle4;
            this.dgvMedico.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvMedico.Location = new System.Drawing.Point(3, 16);
            this.dgvMedico.MultiSelect = false;
            this.dgvMedico.Name = "dgvMedico";
            this.dgvMedico.ReadOnly = true;
            this.dgvMedico.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvMedico.Size = new System.Drawing.Size(731, 121);
            this.dgvMedico.TabIndex = 8;
            // 
            // flpAcaoMedico
            // 
            this.flpAcaoMedico.Controls.Add(this.btIncluirMedico);
            this.flpAcaoMedico.Controls.Add(this.btExcluirMedico);
            this.flpAcaoMedico.Location = new System.Drawing.Point(9, 408);
            this.flpAcaoMedico.Name = "flpAcaoMedico";
            this.flpAcaoMedico.Size = new System.Drawing.Size(737, 31);
            this.flpAcaoMedico.TabIndex = 9;
            this.flpAcaoMedico.TabStop = true;
            // 
            // btIncluirMedico
            // 
            this.btIncluirMedico.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btIncluirMedico.Image = global::SWS.Properties.Resources.icone_mais;
            this.btIncluirMedico.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btIncluirMedico.Location = new System.Drawing.Point(3, 3);
            this.btIncluirMedico.Name = "btIncluirMedico";
            this.btIncluirMedico.Size = new System.Drawing.Size(75, 23);
            this.btIncluirMedico.TabIndex = 10;
            this.btIncluirMedico.Text = "&Incluir";
            this.btIncluirMedico.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btIncluirMedico.UseVisualStyleBackColor = true;
            this.btIncluirMedico.Click += new System.EventHandler(this.btIncluirMedico_Click);
            // 
            // btExcluirMedico
            // 
            this.btExcluirMedico.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btExcluirMedico.Image = global::SWS.Properties.Resources.lixeira;
            this.btExcluirMedico.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btExcluirMedico.Location = new System.Drawing.Point(84, 3);
            this.btExcluirMedico.Name = "btExcluirMedico";
            this.btExcluirMedico.Size = new System.Drawing.Size(75, 23);
            this.btExcluirMedico.TabIndex = 11;
            this.btExcluirMedico.Text = "&Excluir";
            this.btExcluirMedico.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btExcluirMedico.UseVisualStyleBackColor = true;
            this.btExcluirMedico.Click += new System.EventHandler(this.btExcluirMedico_Click);
            // 
            // lblCodigoTuss
            // 
            this.lblCodigoTuss.BackColor = System.Drawing.Color.LightGray;
            this.lblCodigoTuss.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblCodigoTuss.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCodigoTuss.Location = new System.Drawing.Point(9, 148);
            this.lblCodigoTuss.Name = "lblCodigoTuss";
            this.lblCodigoTuss.ReadOnly = true;
            this.lblCodigoTuss.Size = new System.Drawing.Size(231, 21);
            this.lblCodigoTuss.TabIndex = 10;
            this.lblCodigoTuss.TabStop = false;
            this.lblCodigoTuss.Text = "Código Tabela 27 e-Social";
            // 
            // lblExameComplementar
            // 
            this.lblExameComplementar.BackColor = System.Drawing.Color.LightGray;
            this.lblExameComplementar.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblExameComplementar.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblExameComplementar.Location = new System.Drawing.Point(9, 168);
            this.lblExameComplementar.Name = "lblExameComplementar";
            this.lblExameComplementar.ReadOnly = true;
            this.lblExameComplementar.Size = new System.Drawing.Size(231, 21);
            this.lblExameComplementar.TabIndex = 11;
            this.lblExameComplementar.TabStop = false;
            this.lblExameComplementar.Text = "Exame Complementar?";
            // 
            // textCodigoTuss
            // 
            this.textCodigoTuss.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.textCodigoTuss.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textCodigoTuss.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.textCodigoTuss.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textCodigoTuss.Location = new System.Drawing.Point(239, 148);
            this.textCodigoTuss.MaxLength = 8;
            this.textCodigoTuss.Name = "textCodigoTuss";
            this.textCodigoTuss.Size = new System.Drawing.Size(475, 21);
            this.textCodigoTuss.TabIndex = 8;
            this.textCodigoTuss.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.textCodigoTuss_KeyPress);
            // 
            // cbExameComplementar
            // 
            this.cbExameComplementar.BackColor = System.Drawing.Color.LightGray;
            this.cbExameComplementar.BorderColor = System.Drawing.Color.DimGray;
            this.cbExameComplementar.BorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid;
            this.cbExameComplementar.FormattingEnabled = true;
            this.cbExameComplementar.Location = new System.Drawing.Point(239, 168);
            this.cbExameComplementar.Name = "cbExameComplementar";
            this.cbExameComplementar.Size = new System.Drawing.Size(507, 21);
            this.cbExameComplementar.TabIndex = 9;
            // 
            // lblExameRelatorio
            // 
            this.lblExameRelatorio.BackColor = System.Drawing.Color.LightGray;
            this.lblExameRelatorio.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblExameRelatorio.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblExameRelatorio.Location = new System.Drawing.Point(9, 188);
            this.lblExameRelatorio.Name = "lblExameRelatorio";
            this.lblExameRelatorio.ReadOnly = true;
            this.lblExameRelatorio.Size = new System.Drawing.Size(231, 21);
            this.lblExameRelatorio.TabIndex = 12;
            this.lblExameRelatorio.TabStop = false;
            this.lblExameRelatorio.Text = "Relatório de Exame";
            // 
            // textRelatorio
            // 
            this.textRelatorio.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.textRelatorio.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textRelatorio.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.textRelatorio.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textRelatorio.Location = new System.Drawing.Point(239, 188);
            this.textRelatorio.MaxLength = 50;
            this.textRelatorio.Name = "textRelatorio";
            this.textRelatorio.ReadOnly = true;
            this.textRelatorio.Size = new System.Drawing.Size(482, 21);
            this.textRelatorio.TabIndex = 13;
            this.textRelatorio.TabStop = false;
            // 
            // btnRelatorio
            // 
            this.btnRelatorio.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnRelatorio.Image = global::SWS.Properties.Resources.busca;
            this.btnRelatorio.Location = new System.Drawing.Point(712, 188);
            this.btnRelatorio.Name = "btnRelatorio";
            this.btnRelatorio.Size = new System.Drawing.Size(34, 21);
            this.btnRelatorio.TabIndex = 10;
            this.btnRelatorio.UseVisualStyleBackColor = true;
            this.btnRelatorio.Click += new System.EventHandler(this.btnRelatorio_Click);
            // 
            // lblPeriodoVencimento
            // 
            this.lblPeriodoVencimento.BackColor = System.Drawing.Color.LightGray;
            this.lblPeriodoVencimento.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblPeriodoVencimento.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPeriodoVencimento.Location = new System.Drawing.Point(9, 208);
            this.lblPeriodoVencimento.Name = "lblPeriodoVencimento";
            this.lblPeriodoVencimento.ReadOnly = true;
            this.lblPeriodoVencimento.Size = new System.Drawing.Size(231, 21);
            this.lblPeriodoVencimento.TabIndex = 14;
            this.lblPeriodoVencimento.TabStop = false;
            this.lblPeriodoVencimento.Text = "Período de Vencimento";
            // 
            // cbPeriodoVencimento
            // 
            this.cbPeriodoVencimento.BackColor = System.Drawing.Color.LightGray;
            this.cbPeriodoVencimento.BorderColor = System.Drawing.Color.DimGray;
            this.cbPeriodoVencimento.BorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid;
            this.cbPeriodoVencimento.FormattingEnabled = true;
            this.cbPeriodoVencimento.Location = new System.Drawing.Point(239, 208);
            this.cbPeriodoVencimento.Name = "cbPeriodoVencimento";
            this.cbPeriodoVencimento.Size = new System.Drawing.Size(507, 21);
            this.cbPeriodoVencimento.TabIndex = 11;
            // 
            // lblPadraoContrato
            // 
            this.lblPadraoContrato.BackColor = System.Drawing.Color.LightGray;
            this.lblPadraoContrato.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblPadraoContrato.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPadraoContrato.Location = new System.Drawing.Point(9, 228);
            this.lblPadraoContrato.Name = "lblPadraoContrato";
            this.lblPadraoContrato.ReadOnly = true;
            this.lblPadraoContrato.Size = new System.Drawing.Size(231, 21);
            this.lblPadraoContrato.TabIndex = 15;
            this.lblPadraoContrato.TabStop = false;
            this.lblPadraoContrato.Text = "Padrão de contrato";
            // 
            // cbPadraoContrato
            // 
            this.cbPadraoContrato.BackColor = System.Drawing.Color.LightGray;
            this.cbPadraoContrato.BorderColor = System.Drawing.Color.DimGray;
            this.cbPadraoContrato.BorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid;
            this.cbPadraoContrato.FormattingEnabled = true;
            this.cbPadraoContrato.Location = new System.Drawing.Point(239, 228);
            this.cbPadraoContrato.Name = "cbPadraoContrato";
            this.cbPadraoContrato.Size = new System.Drawing.Size(507, 21);
            this.cbPadraoContrato.TabIndex = 16;
            // 
            // btnProcedimentoEsocial
            // 
            this.btnProcedimentoEsocial.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnProcedimentoEsocial.Image = global::SWS.Properties.Resources.busca;
            this.btnProcedimentoEsocial.Location = new System.Drawing.Point(712, 148);
            this.btnProcedimentoEsocial.Name = "btnProcedimentoEsocial";
            this.btnProcedimentoEsocial.Size = new System.Drawing.Size(34, 21);
            this.btnProcedimentoEsocial.TabIndex = 17;
            this.btnProcedimentoEsocial.UseVisualStyleBackColor = true;
            this.btnProcedimentoEsocial.Click += new System.EventHandler(this.btnProcedimentoEsocial_Click);
            // 
            // frmExameIncluir
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(784, 561);
            this.Name = "frmExameIncluir";
            this.Text = "INCLUIR EXAMES";
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.frmExameIncluir_KeyDown);
            this.spForm.Panel1.ResumeLayout(false);
            this.spForm.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.spForm)).EndInit();
            this.spForm.ResumeLayout(false);
            this.pnlForm.ResumeLayout(false);
            this.pnlForm.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.banner)).EndInit();
            this.flpAcao.ResumeLayout(false);
            this.grbMedicos.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvMedico)).EndInit();
            this.flpAcaoMedico.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.FlowLayoutPanel flpAcao;
        protected System.Windows.Forms.TextBox lblExame;
        protected System.Windows.Forms.TextBox lblPreco;
        protected System.Windows.Forms.TextBox lblPrecoCusto;
        protected System.Windows.Forms.TextBox lblPrioridade;
        protected System.Windows.Forms.TextBox lblExamePrestador;
        protected System.Windows.Forms.TextBox lblExameLaboratorio;
        protected System.Windows.Forms.TextBox lblLiberaDocumento;
        protected System.Windows.Forms.TextBox text_descricao;
        protected System.Windows.Forms.TextBox text_precoCusto;
        protected System.Windows.Forms.TextBox text_preco;
        protected System.Windows.Forms.TextBox text_prioridade;
        protected ComboBoxWithBorder cbDocumento;
        protected ComboBoxWithBorder cbExterno;
        protected ComboBoxWithBorder cbLaboratorio;
        protected System.Windows.Forms.Button btn_fechar;
        protected System.Windows.Forms.Button btn_gravar;
        protected System.Windows.Forms.Button btn_limpar;
        private System.Windows.Forms.FlowLayoutPanel flpAcaoMedico;
        private System.Windows.Forms.GroupBox grbMedicos;
        protected System.Windows.Forms.DataGridView dgvMedico;
        protected System.Windows.Forms.Button btIncluirMedico;
        protected System.Windows.Forms.Button btExcluirMedico;
        protected ComboBoxWithBorder cbExameComplementar;
        protected System.Windows.Forms.TextBox textCodigoTuss;
        protected System.Windows.Forms.TextBox lblExameComplementar;
        protected System.Windows.Forms.TextBox lblCodigoTuss;
        protected System.Windows.Forms.TextBox textRelatorio;
        protected System.Windows.Forms.TextBox lblExameRelatorio;
        protected System.Windows.Forms.Button btnRelatorio;
        protected ComboBoxWithBorder cbPeriodoVencimento;
        protected System.Windows.Forms.TextBox lblPeriodoVencimento;
        protected ComboBoxWithBorder cbPadraoContrato;
        protected System.Windows.Forms.TextBox lblPadraoContrato;
        protected System.Windows.Forms.Button btnProcedimentoEsocial;
    }
}