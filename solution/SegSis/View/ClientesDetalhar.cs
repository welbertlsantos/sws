﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using SWS.Entidade;
using SWS.View.Resources;
using SWS.View.ViewHelper;
using SWS.Facade;
using SWS.Helper;

namespace SWS.View
{
    public partial class frmClientesDetalhar : SWS.View.frmClientesIncluir
    {
        public frmClientesDetalhar(Cliente cliente)
        {
            InitializeComponent();
            /* montando informações da tela */
            if (cliente.Unidade)
                cbTipoCliente.Text = "Cliente Unidade";
            else if (cliente.Credenciada)
                cbTipoCliente.Text = "Cliente Credenciada";
            else if (!cliente.Unidade && !cliente.Credenciada && !cliente.Fisica)
                cbTipoCliente.Text = "Cliente Empresa Padrão";
            else
                cbTipoCliente.Text = "Cliente Pessoa Física";

            vendedores = cliente.VendedorCliente;
            VendedorCliente vendedorAtivo = vendedores.Find(x => String.Equals(x.Situacao, ApplicationConstants.ATIVO));
            if (vendedorAtivo != null)
                textVendedor.Text = vendedorAtivo.Vendedor.Nome;
            btVendedor.Enabled = false;

            if (cliente.Matriz != null)
            {
                matriz = cliente.Matriz;
                textMatriz.Text = matriz.RazaoSocial;
                btMatriz.Enabled = false;
            }

            if (cliente.CredenciadaCliente != null)
            {
                credenciada = cliente.CredenciadaCliente;
                textCredenciada.Text = credenciada.RazaoSocial;
                btCredenciada.Enabled = false;
            }



            textRazaoSocial.Text = cliente.RazaoSocial;
            textRazaoSocial.ReadOnly = true;
            textCNPJ.Text = cliente.Cnpj;
            textCNPJ.ReadOnly = true;
            textFantasia.Text = cliente.Fantasia;
            textFantasia.ReadOnly = true;
            textInscricao.Text = cliente.Inscricao;
            textInscricao.ReadOnly = true;
            textEmail.Text = cliente.Email;
            textEmail.ReadOnly = true;
            textSite.Text = cliente.Site;
            textSite.ReadOnly = true;

            ramo = cliente.Ramo;
            textRamo.Text = cliente.Ramo != null ? cliente.Ramo.Descricao : String.Empty;
            btRamo.Enabled = false;
            textJornada.Text = cliente.Jornada;
            textJornada.ReadOnly = true;
            textEscopo.Text = cliente.Escopo;
            textEscopo.ReadOnly = true;
            /* salvando páginas de endereco */

            tpEnderecoCopy = new TabPage[] { tpComercial, tpCobranca };
            /* populando informação de endereco. Padrão para todos os clientes */

            /* verificando o tipo de cliente e o tipo de informação exibida em tela */

            if (cliente.Matriz != null)
            {
                /* cliente é unidade */
                tpEndereco.TabPages.Clear();
                tpEndereco.TabPages.Add(tpEnderecoCopy[0]);

                textLogradouroComercial.Text = cliente.Endereco;
                textLogradouroComercial.ReadOnly = true;
                textNumeroComercial.Text = cliente.Numero;
                textNumeroComercial.ReadOnly = true;
                textComplementoComercial.Text = cliente.Complemento;
                textComplementoComercial.ReadOnly = true;
                textBairroComercial.Text = cliente.Bairro;
                textBairroComercial.ReadOnly = true;
                textCEPComercial.Text = cliente.Cep;
                textCEPComercial.ReadOnly = true;
                ComboHelper.startComboUfSave(cbUFComercial);
                cbUFComercial.SelectedValue = cliente.Uf;
                cbUFComercial.Enabled = false;
                cidadeComercial = cliente.CidadeIbge;
                textCidadeComercial.Text = cliente.CidadeIbge.Nome;
                textCidadeComercial.ReadOnly = true;
                textTelefoneComercial.Text = cliente.Telefone1;
                textTelefoneComercial.ReadOnly = true;
                textFaxComercial.Text = cliente.Telefone2;
                textFaxComercial.ReadOnly = true;
                textCodigoCnes.ReadOnly = true;

            }
            else
            {
                /* cliente credenciada ou padrão. Seguem a mesma lógica de tela. */

                textLogradouroComercial.Text = cliente.Endereco;
                textLogradouroComercial.ReadOnly = true;
                textNumeroComercial.Text = cliente.Numero;
                textNumeroComercial.ReadOnly = true;
                textComplementoComercial.Text = cliente.Complemento;
                textComplementoComercial.ReadOnly = true;
                textBairroComercial.Text = cliente.Bairro;
                textBairroComercial.ReadOnly = true;
                textCEPComercial.Text = cliente.Cep;
                textCEPComercial.ReadOnly = true;
                ComboHelper.startComboUfSave(cbUFComercial);
                cbUFComercial.SelectedValue = cliente.Uf;
                cbUFComercial.Enabled = false;
                cidadeComercial = cliente.CidadeIbge;
                textCidadeComercial.Text = cliente.CidadeIbge.Nome;
                textCidadeComercial.ReadOnly = true;
                textTelefoneComercial.Text = cliente.Telefone1;
                textTelefoneComercial.ReadOnly = true;
                textFaxComercial.Text = cliente.Telefone2;
                textFaxComercial.ReadOnly = true;

                textLogradouroCobranca.Text = cliente.EnderecoCob;
                textLogradouroCobranca.ReadOnly = true;
                textNumeroCobranca.Text = cliente.NumeroCob;
                textNumeroCobranca.ReadOnly = true;
                textComplementoCobranca.Text = cliente.ComplementoCob;
                textComplementoCobranca.ReadOnly = true;
                textBairroCobranca.Text = cliente.BairroCob;
                textBairroCobranca.ReadOnly = true;
                textCEPCobranca.Text = cliente.CepCob;
                textCEPCobranca.ReadOnly = true;
                ComboHelper.startComboUfSave(cbUFCobranca);
                cbUFCobranca.SelectedValue = cliente.UfCob;
                cbUFCobranca.Enabled = false;
                cidadeCobranca = cliente.CidadeIbgeCobranca;
                textCidadeCobranca.Text = cliente.CidadeIbgeCobranca.Nome;
                textCidadeCobranca.ReadOnly = true;
                textTelefoneCobranca.Text = cliente.TelefoneCob;
                textTelefoneCobranca.ReadOnly = true;
                textFaxCobranca.Text = cliente.Telefone2Cob;
                textFaxCobranca.ReadOnly = true;
                /* montando dados do cnae */
                cnaes = cliente.ClienteCnae;
                montaGridCnae();
                cbUsaPo.SelectedValue = cliente.UsaPo ? "true" : "false";

                /* montando dados da logo do cliente */

                ClienteFacade clienteFacade = ClienteFacade.getInstance();
                clienteArquivo = clienteFacade.findClienteArquivoByCliente(cliente);
                if (clienteArquivo != null)
                    pbLogo.Image = FileHelper.byteArrayToImage(clienteArquivo.Arquivo.Conteudo);

                /* montando funcoes do cliente */

                funcoes = cliente.Funcoes;
                montaGridFuncoes();

                /* montando informações sobre o contato */

                contatos = clienteFacade.findByCliente(cliente);
                montaGridContato();

                /* montando bloco de condições */

                ComboHelper.Boolean(cbClienteVIP, true);
                ComboHelper.Boolean(cbContrato, true);
                ComboHelper.Boolean(cbParticular, true);
                ComboHelper.Boolean(cbPrestador, true);
                ComboHelper.Boolean(cbValorLiquido, true);
                ComboHelper.Boolean(cbBloqueado, false);
                ComboHelper.Boolean(cbCentroCusto, false);
                ComboHelper.Boolean(cbSimplesNacional, false);

                cbClienteVIP.SelectedValue = cliente.Vip == true ? "true" : "false";
                cbContrato.SelectedValue = cliente.UsaContrato == true ? "true" : "false";
                cbParticular.SelectedValue = cliente.Particular == true ? "true" : "false";
                cbPrestador.SelectedValue = cliente.Prestador == true ? "true" : "false";
                cbValorLiquido.SelectedValue = cliente.GeraCobrancaValorLiquido == true ? "true" : "false";
                cbBloqueado.SelectedValue = cliente.Bloqueado == true ? "true" : "false";
                cbCentroCusto.SelectedValue = cliente.UsaCentroCusto == true ? "true" : "false";
                cbSimplesNacional.SelectedValue = cliente.SimplesNacional == true ? "true" : "false";
            }

            /* bloqueando os controles */
            changeControlesTela(false);
            cbClienteVIP.Enabled = false;
            cbContrato.Enabled = false;
            cbParticular.Enabled = false;
            cbPrestador.Enabled = false;
            cbValorLiquido.Enabled = false;
            cbSimplesNacional.Enabled = false;
            textCodigoCnes.ReadOnly = true;
            grbCentroCusto.Enabled = false;


            btGravar.Enabled = false;
            btLimpar.Enabled = false;

            if (cliente.UsaCentroCusto)
                montaGridCentroCusto();
        }
    }
}
