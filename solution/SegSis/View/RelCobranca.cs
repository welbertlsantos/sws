﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using SWS.Entidade;
using System.Diagnostics;
using SWS.View.Resources;
using SWS.View.ViewHelper;

namespace SWS.View
{
    public partial class frmRelCobranca : BaseFormConsulta
    {
        Cliente cliente;
        private static String msg1 = "A data final não pode ser menor do que a data inicial.";
        private static String msg2 = "Você deve selecionar algum período para consulta.";
        
        public frmRelCobranca()
        {
            InitializeComponent();
        }

        private void btnCliente_Click(object sender, EventArgs e)
        {
            try
            {
                frmClienteSelecionar formCliente = new frmClienteSelecionar(null, null, false, null, null, null, null, null, null);
                formCliente.ShowDialog();

                if (formCliente.Cliente != null)
                {
                    this.cliente = formCliente.Cliente;
                    textCliente.Text = cliente.RazaoSocial.ToUpper();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
        }

        private void btnFechar_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void rbSituacaoCancelada_CheckedChanged(object sender, EventArgs e)
        {
            grbBaixa.Enabled = rbSituacaoCancelada.Checked ? false : true;
            grbCancelamento.Enabled = rbSituacaoCancelada.Checked ? true : false;
            grbEmissao.Enabled = rbSituacaoCancelada.Checked ? false : false;
            grbVencimento.Enabled = rbSituacaoCancelada.Checked ? false : true;
            chkEmissao.Checked = false;
            chkVencimento.Checked = false;
        }

        private void rbSituacaoAberta_CheckedChanged(object sender, EventArgs e)
        {
            grbBaixa.Enabled = rbSituacaoAberta.Checked ? false : true;
            grbCancelamento.Enabled = rbSituacaoAberta.Checked ? false : true;
            grbEmissao.Enabled = rbSituacaoAberta.Checked ? true : false;
            grbVencimento.Enabled = rbSituacaoAberta.Checked ? true : false;
        }

        private void rbSituacaoBaixada_CheckedChanged(object sender, EventArgs e)
        {
            grbBaixa.Enabled = rbSituacaoBaixada.Checked ? true : false;
            grbCancelamento.Enabled = rbSituacaoBaixada.Checked ? false : true;
            grbEmissao.Enabled = rbSituacaoBaixada.Checked ? false : true;
            grbVencimento.Enabled = rbSituacaoBaixada.Checked ? false : true;
            chkEmissao.Checked = false;
            chkVencimento.Checked = false;
        }

        private void chkEmissao_CheckedChanged(object sender, EventArgs e)
        {
            dataEmissaoInicial.Enabled = chkEmissao.Checked ? true : false;
            dataEmissaoFinal.Enabled = chkEmissao.Checked ? true : false;
            chkVencimento.Enabled = chkEmissao.Checked ? false : true;
        }

        private void chkVencimento_CheckedChanged(object sender, EventArgs e)
        {
            dataVencimentoFinal.Enabled = chkVencimento.Checked ? true : false;
            dataVencimentoInicial.Enabled = chkVencimento.Checked ? true : false;
            chkEmissao.Enabled = chkVencimento.Checked ? false : true;
        }

        private void btnImprimir_Click(object sender, EventArgs e)
        {
            try
            {
                this.Cursor = Cursors.WaitCursor;

                if (!validaDados())
                {
                    String dataInicial = chkEmissao.Checked ? Convert.ToDateTime(dataEmissaoInicial.Text).ToShortDateString() : chkVencimento.Checked ? Convert.ToDateTime(dataVencimentoInicial.Text).ToShortDateString() : dataCancelamentoInicial.Enabled == true ? Convert.ToDateTime(dataCancelamentoInicial.Text).ToShortDateString() : Convert.ToDateTime(dataBaixaInicial.Text).ToShortDateString();
                    String dataFinal = chkEmissao.Checked ? Convert.ToDateTime(dataEmissaoFinal.Text).ToShortDateString() : chkVencimento.Checked ? Convert.ToDateTime(dataVencimentoFinal.Text).ToShortDateString() : dataCancelamentoFinal.Enabled == true ? Convert.ToDateTime(dataCancelamentoFinal.Text).ToShortDateString() : Convert.ToDateTime(dataBaixaFinal.Text).ToShortDateString();
                    String nomeRelatorio = "RELATORIO_COBRANCA.JASPER";
                    Int64? id_cliente = cliente == null ? (Int64?)0 : cliente.Id;
                    String tipo = rbSimplificado.Checked ? ApplicationConstants.SINTETICO : ApplicationConstants.ANALITICO;
                    String razao_social = cliente == null ? String.Empty : cliente.RazaoSocial.ToString().ToUpper();
                    String cnpj = cliente == null ? String.Empty : !cliente.Fisica ? ValidaCampoHelper.FormataCnpj(cliente.Cnpj) : ValidaCampoHelper.FormataCpf(cliente.Cnpj);
                    
                    String tipoSituacao = verificaTipoConsulta();

                    var process = new Process();

                    process.StartInfo.FileName = "Relatorio\\jre7\\bin\\java";
                    process.StartInfo.Arguments = " -jar Relatorio\\MiddlewareJasper.jar " + "Relatorio\\Report\\" + nomeRelatorio + " ID_CLIENTE:" + id_cliente + ":Integer" + " SITUACAO:" + tipoSituacao + ":String" + " DATA_INICIAL:" + dataInicial + ":String" + " DATA_FINAL:" + dataFinal + ":String" + " TIPO:" + tipo + ":String" + " CLIENTE:" + '"' + razao_social + '"' + ":String" + " CNPJ:" + cnpj + ":String";
                    process.StartInfo.UseShellExecute = false;
                    process.StartInfo.RedirectStandardError = true;
                    process.StartInfo.RedirectStandardInput = true;
                    process.StartInfo.RedirectStandardOutput = true;
                    process.StartInfo.CreateNoWindow = true;
                    process.Start();
                    process.StandardOutput.ReadToEnd();
                    process.WaitForExit();
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
            
        }


        public Boolean validaDados()
        {
            Boolean retorno = false;
            
            try
            {
                if (chkEmissao.Checked)
                {
                    if (Convert.ToDateTime(dataEmissaoInicial.Text) > Convert.ToDateTime(dataEmissaoFinal.Text))
                    {
                        throw new Exception(msg1);
                    }
                }
                else if (chkVencimento.Checked)
                {
                    if (Convert.ToDateTime(dataVencimentoInicial.Text) > Convert.ToDateTime(dataVencimentoFinal.Text))
                    {
                        throw new Exception(msg1);
                    }
                }
                else if (dataCancelamentoInicial.Enabled == true)
                {
                    if (Convert.ToDateTime(dataCancelamentoInicial.Text) > Convert.ToDateTime(dataCancelamentoFinal.Text))
                    {
                        throw new Exception(msg1);
                    }
                }
                else if (dataBaixaInicial.Enabled == true)
                {
                    if (Convert.ToDateTime(dataBaixaInicial.Text) > Convert.ToDateTime(dataBaixaFinal.Text))
                    {
                        throw new Exception(msg1);
                    }
                }
                else
                {
                    throw new Exception(msg2);
                }

            }
            catch (Exception ex)
            {
                retorno = true;
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }

            return retorno;
        }

        public String verificaTipoConsulta()
        {
            if (chkEmissao.Checked)
            {
                return ApplicationConstants.EMISSAO;
            }
            else if (chkVencimento.Checked)
            {
                return ApplicationConstants.VENCIMENTO;
            }
            else if (dataCancelamentoInicial.Enabled == true)
            {
                return ApplicationConstants.CANCELADA;
            }
            else
            {
                return ApplicationConstants.BAIXADO;
            }
        }

    }
}
