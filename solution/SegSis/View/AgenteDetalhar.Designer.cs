﻿namespace SegSis.View
{
    partial class frm_detalhar_agente
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frm_detalhar_agente));
            this.grb_dados = new System.Windows.Forms.GroupBox();
            this.text_risco = new System.Windows.Forms.TextBox();
            this.grb_situacao = new System.Windows.Forms.GroupBox();
            this.rb_desativado = new System.Windows.Forms.RadioButton();
            this.rb_ativo = new System.Windows.Forms.RadioButton();
            this.lbl_limite = new System.Windows.Forms.Label();
            this.text_limite = new System.Windows.Forms.TextBox();
            this.lbl_dano = new System.Windows.Forms.Label();
            this.text_dano = new System.Windows.Forms.TextBox();
            this.lbl_trajetoria = new System.Windows.Forms.Label();
            this.lbl_risco = new System.Windows.Forms.Label();
            this.text_trajetoria = new System.Windows.Forms.TextBox();
            this.lbl_descricao = new System.Windows.Forms.Label();
            this.text_descricao = new System.Windows.Forms.TextBox();
            this.grb_paginacao = new System.Windows.Forms.GroupBox();
            this.btn_fechar = new System.Windows.Forms.Button();
            this.panel_banner.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox_banner)).BeginInit();
            this.panel.SuspendLayout();
            this.grb_dados.SuspendLayout();
            this.grb_situacao.SuspendLayout();
            this.grb_paginacao.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel
            // 
            this.panel.Controls.Add(this.grb_paginacao);
            this.panel.Controls.Add(this.grb_dados);
            // 
            // grb_dados
            // 
            this.grb_dados.BackColor = System.Drawing.Color.White;
            this.grb_dados.Controls.Add(this.text_risco);
            this.grb_dados.Controls.Add(this.grb_situacao);
            this.grb_dados.Controls.Add(this.lbl_limite);
            this.grb_dados.Controls.Add(this.text_limite);
            this.grb_dados.Controls.Add(this.lbl_dano);
            this.grb_dados.Controls.Add(this.text_dano);
            this.grb_dados.Controls.Add(this.lbl_trajetoria);
            this.grb_dados.Controls.Add(this.lbl_risco);
            this.grb_dados.Controls.Add(this.text_trajetoria);
            this.grb_dados.Controls.Add(this.lbl_descricao);
            this.grb_dados.Controls.Add(this.text_descricao);
            this.grb_dados.Location = new System.Drawing.Point(3, 3);
            this.grb_dados.Name = "grb_dados";
            this.grb_dados.Size = new System.Drawing.Size(781, 455);
            this.grb_dados.TabIndex = 29;
            this.grb_dados.TabStop = false;
            this.grb_dados.Text = "Dados";
            // 
            // text_risco
            // 
            this.text_risco.BackColor = System.Drawing.SystemColors.Control;
            this.text_risco.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.text_risco.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.text_risco.Location = new System.Drawing.Point(551, 32);
            this.text_risco.MaxLength = 100;
            this.text_risco.Name = "text_risco";
            this.text_risco.ReadOnly = true;
            this.text_risco.Size = new System.Drawing.Size(119, 20);
            this.text_risco.TabIndex = 31;
            this.text_risco.TabStop = false;
            // 
            // grb_situacao
            // 
            this.grb_situacao.Controls.Add(this.rb_desativado);
            this.grb_situacao.Controls.Add(this.rb_ativo);
            this.grb_situacao.Enabled = false;
            this.grb_situacao.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.grb_situacao.Location = new System.Drawing.Point(551, 98);
            this.grb_situacao.Name = "grb_situacao";
            this.grb_situacao.Size = new System.Drawing.Size(147, 43);
            this.grb_situacao.TabIndex = 30;
            this.grb_situacao.TabStop = false;
            this.grb_situacao.Text = "Situação";
            // 
            // rb_desativado
            // 
            this.rb_desativado.AutoSize = true;
            this.rb_desativado.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.rb_desativado.Location = new System.Drawing.Point(62, 19);
            this.rb_desativado.Name = "rb_desativado";
            this.rb_desativado.Size = new System.Drawing.Size(78, 17);
            this.rb_desativado.TabIndex = 1;
            this.rb_desativado.Text = "Desativado";
            this.rb_desativado.UseVisualStyleBackColor = true;
            // 
            // rb_ativo
            // 
            this.rb_ativo.AutoSize = true;
            this.rb_ativo.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.rb_ativo.Location = new System.Drawing.Point(7, 19);
            this.rb_ativo.Name = "rb_ativo";
            this.rb_ativo.Size = new System.Drawing.Size(48, 17);
            this.rb_ativo.TabIndex = 0;
            this.rb_ativo.Text = "Ativo";
            this.rb_ativo.UseVisualStyleBackColor = true;
            // 
            // lbl_limite
            // 
            this.lbl_limite.AutoSize = true;
            this.lbl_limite.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_limite.Location = new System.Drawing.Point(548, 56);
            this.lbl_limite.Name = "lbl_limite";
            this.lbl_limite.Size = new System.Drawing.Size(34, 13);
            this.lbl_limite.TabIndex = 15;
            this.lbl_limite.Text = "Limite";
            // 
            // text_limite
            // 
            this.text_limite.BackColor = System.Drawing.SystemColors.Control;
            this.text_limite.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.text_limite.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.text_limite.Location = new System.Drawing.Point(551, 72);
            this.text_limite.MaxLength = 100;
            this.text_limite.Name = "text_limite";
            this.text_limite.ReadOnly = true;
            this.text_limite.Size = new System.Drawing.Size(202, 20);
            this.text_limite.TabIndex = 14;
            this.text_limite.TabStop = false;
            // 
            // lbl_dano
            // 
            this.lbl_dano.AutoSize = true;
            this.lbl_dano.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_dano.Location = new System.Drawing.Point(6, 110);
            this.lbl_dano.Name = "lbl_dano";
            this.lbl_dano.Size = new System.Drawing.Size(33, 13);
            this.lbl_dano.TabIndex = 13;
            this.lbl_dano.Text = "Dano";
            // 
            // text_dano
            // 
            this.text_dano.BackColor = System.Drawing.SystemColors.Control;
            this.text_dano.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.text_dano.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.text_dano.Location = new System.Drawing.Point(9, 126);
            this.text_dano.MaxLength = 255;
            this.text_dano.Multiline = true;
            this.text_dano.Name = "text_dano";
            this.text_dano.ReadOnly = true;
            this.text_dano.Size = new System.Drawing.Size(511, 36);
            this.text_dano.TabIndex = 12;
            this.text_dano.TabStop = false;
            // 
            // lbl_trajetoria
            // 
            this.lbl_trajetoria.AutoSize = true;
            this.lbl_trajetoria.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_trajetoria.Location = new System.Drawing.Point(6, 55);
            this.lbl_trajetoria.Name = "lbl_trajetoria";
            this.lbl_trajetoria.Size = new System.Drawing.Size(51, 13);
            this.lbl_trajetoria.TabIndex = 11;
            this.lbl_trajetoria.Text = "Trajetoria";
            // 
            // lbl_risco
            // 
            this.lbl_risco.AutoSize = true;
            this.lbl_risco.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_risco.Location = new System.Drawing.Point(548, 16);
            this.lbl_risco.Name = "lbl_risco";
            this.lbl_risco.Size = new System.Drawing.Size(34, 13);
            this.lbl_risco.TabIndex = 9;
            this.lbl_risco.Text = "Risco";
            // 
            // text_trajetoria
            // 
            this.text_trajetoria.BackColor = System.Drawing.SystemColors.Control;
            this.text_trajetoria.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.text_trajetoria.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.text_trajetoria.Location = new System.Drawing.Point(9, 71);
            this.text_trajetoria.MaxLength = 255;
            this.text_trajetoria.Multiline = true;
            this.text_trajetoria.Name = "text_trajetoria";
            this.text_trajetoria.ReadOnly = true;
            this.text_trajetoria.Size = new System.Drawing.Size(511, 36);
            this.text_trajetoria.TabIndex = 8;
            this.text_trajetoria.TabStop = false;
            // 
            // lbl_descricao
            // 
            this.lbl_descricao.AutoSize = true;
            this.lbl_descricao.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_descricao.Location = new System.Drawing.Point(6, 16);
            this.lbl_descricao.Name = "lbl_descricao";
            this.lbl_descricao.Size = new System.Drawing.Size(55, 13);
            this.lbl_descricao.TabIndex = 7;
            this.lbl_descricao.Text = "Descrição";
            // 
            // text_descricao
            // 
            this.text_descricao.BackColor = System.Drawing.SystemColors.Control;
            this.text_descricao.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.text_descricao.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.text_descricao.Location = new System.Drawing.Point(9, 32);
            this.text_descricao.MaxLength = 100;
            this.text_descricao.Name = "text_descricao";
            this.text_descricao.ReadOnly = true;
            this.text_descricao.Size = new System.Drawing.Size(511, 20);
            this.text_descricao.TabIndex = 0;
            this.text_descricao.TabStop = false;
            // 
            // grb_paginacao
            // 
            this.grb_paginacao.BackColor = System.Drawing.Color.White;
            this.grb_paginacao.Controls.Add(this.btn_fechar);
            this.grb_paginacao.Location = new System.Drawing.Point(3, 464);
            this.grb_paginacao.Name = "grb_paginacao";
            this.grb_paginacao.Size = new System.Drawing.Size(781, 51);
            this.grb_paginacao.TabIndex = 31;
            this.grb_paginacao.TabStop = false;
            // 
            // btn_fechar
            // 
            this.btn_fechar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_fechar.Image = global::SegSis.Properties.Resources.close;
            this.btn_fechar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btn_fechar.Location = new System.Drawing.Point(9, 19);
            this.btn_fechar.Name = "btn_fechar";
            this.btn_fechar.Size = new System.Drawing.Size(77, 23);
            this.btn_fechar.TabIndex = 1;
            this.btn_fechar.Text = "&Fechar";
            this.btn_fechar.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btn_fechar.UseVisualStyleBackColor = true;
            this.btn_fechar.Click += new System.EventHandler(this.btn_fechar_Click);
            // 
            // frm_detalhar_agente
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(794, 568);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "frm_detalhar_agente";
            this.Text = "DETALHAR AGENTE";
            this.panel_banner.ResumeLayout(false);
            this.panel_banner.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox_banner)).EndInit();
            this.panel.ResumeLayout(false);
            this.grb_dados.ResumeLayout(false);
            this.grb_dados.PerformLayout();
            this.grb_situacao.ResumeLayout(false);
            this.grb_situacao.PerformLayout();
            this.grb_paginacao.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox grb_dados;
        private System.Windows.Forms.GroupBox grb_situacao;
        private System.Windows.Forms.RadioButton rb_desativado;
        private System.Windows.Forms.RadioButton rb_ativo;
        private System.Windows.Forms.Label lbl_limite;
        private System.Windows.Forms.TextBox text_limite;
        private System.Windows.Forms.Label lbl_dano;
        private System.Windows.Forms.TextBox text_dano;
        private System.Windows.Forms.Label lbl_trajetoria;
        private System.Windows.Forms.Label lbl_risco;
        private System.Windows.Forms.TextBox text_trajetoria;
        private System.Windows.Forms.Label lbl_descricao;
        private System.Windows.Forms.TextBox text_descricao;
        private System.Windows.Forms.GroupBox grb_paginacao;
        private System.Windows.Forms.Button btn_fechar;
        private System.Windows.Forms.TextBox text_risco;
    }
}