﻿using SWS.Entidade;
using SWS.Facade;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SWS.View
{
    public partial class frmPcmsoCorrecao : frmTemplateConsulta
    {
        private Estudo pcmso;
        private string historico;

        public string Historico
        {
            get { return historico; }
            set { historico = value; }
        }
        private CorrecaoEstudo correcaoEstudo;

        public CorrecaoEstudo CorrecaoEstudo
        {
            get { return correcaoEstudo; }
            set { correcaoEstudo = value; }
        }
        
        public frmPcmsoCorrecao(Estudo pcmso)
        {
            InitializeComponent();
            this.pcmso = pcmso;
            ActiveControl = textHistorico;
        }

        private void btnFechar_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnGravar_Click(object sender, EventArgs e)
        {
            try
            {
                /* verificando se foi digitado alguma informação no histórico e se ela 
                 * atende o limite estabelecido para histórico */

                if (String.IsNullOrEmpty(textHistorico.Text.Trim()) ||
                    textHistorico.Text.Length < 30)
                    throw new Exception("Histórico muito curto ou em branco. Reveja o campo histórico.");

                this.Historico = textHistorico.Text.Trim();

                this.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }
    }
}
