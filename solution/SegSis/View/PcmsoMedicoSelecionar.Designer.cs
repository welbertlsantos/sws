﻿namespace SWS.View
{
    partial class frm_PcmsoMedicoSelecionar
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frm_PcmsoMedicoSelecionar));
            this.grb_paginacao = new System.Windows.Forms.GroupBox();
            this.btn_fechar = new System.Windows.Forms.Button();
            this.btn_ok = new System.Windows.Forms.Button();
            this.grb_gride = new System.Windows.Forms.GroupBox();
            this.grd_usuario = new System.Windows.Forms.DataGridView();
            this.grb_dados = new System.Windows.Forms.GroupBox();
            this.btn_buscar = new System.Windows.Forms.Button();
            this.lbl_nome = new System.Windows.Forms.Label();
            this.text_nome = new System.Windows.Forms.TextBox();
            this.grb_paginacao.SuspendLayout();
            this.grb_gride.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grd_usuario)).BeginInit();
            this.grb_dados.SuspendLayout();
            this.SuspendLayout();
            // 
            // grb_paginacao
            // 
            this.grb_paginacao.Controls.Add(this.btn_fechar);
            this.grb_paginacao.Controls.Add(this.btn_ok);
            this.grb_paginacao.Location = new System.Drawing.Point(3, 351);
            this.grb_paginacao.Name = "grb_paginacao";
            this.grb_paginacao.Size = new System.Drawing.Size(585, 47);
            this.grb_paginacao.TabIndex = 0;
            this.grb_paginacao.TabStop = false;
            // 
            // btn_fechar
            // 
            this.btn_fechar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_fechar.Image = global::SWS.Properties.Resources.close;
            this.btn_fechar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btn_fechar.Location = new System.Drawing.Point(89, 18);
            this.btn_fechar.Name = "btn_fechar";
            this.btn_fechar.Size = new System.Drawing.Size(75, 23);
            this.btn_fechar.TabIndex = 5;
            this.btn_fechar.Text = "&Fechar";
            this.btn_fechar.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btn_fechar.UseVisualStyleBackColor = true;
            this.btn_fechar.Click += new System.EventHandler(this.btn_fechar_Click);
            // 
            // btn_ok
            // 
            this.btn_ok.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_ok.Image = global::SWS.Properties.Resources.fechar_ico;
            this.btn_ok.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btn_ok.Location = new System.Drawing.Point(6, 18);
            this.btn_ok.Name = "btn_ok";
            this.btn_ok.Size = new System.Drawing.Size(77, 23);
            this.btn_ok.TabIndex = 4;
            this.btn_ok.Text = "&Ok";
            this.btn_ok.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btn_ok.UseVisualStyleBackColor = true;
            this.btn_ok.Click += new System.EventHandler(this.btn_ok_Click);
            // 
            // grb_gride
            // 
            this.grb_gride.BackColor = System.Drawing.Color.Transparent;
            this.grb_gride.Controls.Add(this.grd_usuario);
            this.grb_gride.Location = new System.Drawing.Point(3, 97);
            this.grb_gride.Name = "grb_gride";
            this.grb_gride.Size = new System.Drawing.Size(595, 248);
            this.grb_gride.TabIndex = 0;
            this.grb_gride.TabStop = false;
            this.grb_gride.Text = "Médicos";
            // 
            // grd_usuario
            // 
            this.grd_usuario.AllowUserToAddRows = false;
            this.grd_usuario.AllowUserToDeleteRows = false;
            this.grd_usuario.AllowUserToOrderColumns = true;
            this.grd_usuario.AllowUserToResizeRows = false;
            this.grd_usuario.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this.grd_usuario.BackgroundColor = System.Drawing.Color.White;
            this.grd_usuario.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.grd_usuario.Dock = System.Windows.Forms.DockStyle.Fill;
            this.grd_usuario.Location = new System.Drawing.Point(3, 16);
            this.grd_usuario.MultiSelect = false;
            this.grd_usuario.Name = "grd_usuario";
            this.grd_usuario.ReadOnly = true;
            this.grd_usuario.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.grd_usuario.Size = new System.Drawing.Size(589, 229);
            this.grd_usuario.TabIndex = 3;
            // 
            // grb_dados
            // 
            this.grb_dados.Controls.Add(this.btn_buscar);
            this.grb_dados.Controls.Add(this.lbl_nome);
            this.grb_dados.Controls.Add(this.text_nome);
            this.grb_dados.Location = new System.Drawing.Point(3, 3);
            this.grb_dados.Name = "grb_dados";
            this.grb_dados.Size = new System.Drawing.Size(595, 88);
            this.grb_dados.TabIndex = 0;
            this.grb_dados.TabStop = false;
            this.grb_dados.Text = "Dados";
            // 
            // btn_buscar
            // 
            this.btn_buscar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_buscar.Image = global::SWS.Properties.Resources.lupa;
            this.btn_buscar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btn_buscar.Location = new System.Drawing.Point(6, 58);
            this.btn_buscar.Name = "btn_buscar";
            this.btn_buscar.Size = new System.Drawing.Size(77, 23);
            this.btn_buscar.TabIndex = 2;
            this.btn_buscar.Text = "&Buscar";
            this.btn_buscar.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btn_buscar.UseVisualStyleBackColor = true;
            this.btn_buscar.Click += new System.EventHandler(this.btn_buscar_Click);
            // 
            // lbl_nome
            // 
            this.lbl_nome.AutoSize = true;
            this.lbl_nome.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_nome.Location = new System.Drawing.Point(3, 16);
            this.lbl_nome.Name = "lbl_nome";
            this.lbl_nome.Size = new System.Drawing.Size(35, 13);
            this.lbl_nome.TabIndex = 7;
            this.lbl_nome.Text = "Nome";
            // 
            // text_nome
            // 
            this.text_nome.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.text_nome.Location = new System.Drawing.Point(6, 32);
            this.text_nome.MaxLength = 100;
            this.text_nome.Name = "text_nome";
            this.text_nome.Size = new System.Drawing.Size(560, 20);
            this.text_nome.TabIndex = 1;
            // 
            // frm_PcmsoMedicoSelecionar
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(600, 400);
            this.ControlBox = false;
            this.Controls.Add(this.grb_paginacao);
            this.Controls.Add(this.grb_gride);
            this.Controls.Add(this.grb_dados);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "frm_PcmsoMedicoSelecionar";
            this.Text = "SELECIONAR MÉDICO";
            this.grb_paginacao.ResumeLayout(false);
            this.grb_gride.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.grd_usuario)).EndInit();
            this.grb_dados.ResumeLayout(false);
            this.grb_dados.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox grb_paginacao;
        private System.Windows.Forms.Button btn_fechar;
        private System.Windows.Forms.Button btn_ok;
        private System.Windows.Forms.GroupBox grb_gride;
        private System.Windows.Forms.DataGridView grd_usuario;
        private System.Windows.Forms.GroupBox grb_dados;
        private System.Windows.Forms.Button btn_buscar;
        private System.Windows.Forms.Label lbl_nome;
        private System.Windows.Forms.TextBox text_nome;
    }
}