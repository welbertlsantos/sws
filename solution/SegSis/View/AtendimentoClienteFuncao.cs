﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using SWS.Entidade;
using SWS.Facade;
using SWS.View.Resources;

namespace SWS.View
{
    public partial class frmAtendimentoClienteFuncao : SWS.View.frmTemplateConsulta
    {
        private static String msg1 = "Selecione um cliente.";
        private static String msg2 = "Selecione uma função";

        private Funcionario funcionario;
        private ClienteFuncaoFuncionario clienteFuncaoFuncionario;
        private List<ClienteFuncaoFuncionario> clienteFuncaoFuncionarioList = new List<ClienteFuncaoFuncionario>();

        public ClienteFuncaoFuncionario ClienteFuncaoFuncionario
        {
            get { return clienteFuncaoFuncionario; }
            set { clienteFuncaoFuncionario = value; }
        }
        
        public frmAtendimentoClienteFuncao(Funcionario funcionario)
        {
            InitializeComponent();
            this.funcionario = funcionario;
            montaDataGridCliente();
        }

        private void btFechar_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btConfirma_Click(object sender, EventArgs e)
        {
            try
            {
                if (dgvCliente.CurrentRow == null)
                    throw new Exception(msg1);

                if (dgvFuncao.CurrentRow == null)
                    throw new Exception(msg2);

                clienteFuncaoFuncionario = clienteFuncaoFuncionarioList.Find(x => x.Id == (Int64)dgvFuncao.CurrentRow.Cells[0].Value);
                if (string.Equals(clienteFuncaoFuncionario.ClienteFuncao.Cliente.Situacao, ApplicationConstants.DESATIVADO))
                {
                    clienteFuncaoFuncionario = null;
                    throw new Exception("O cliente selecionado está desativado e não poderá mais ter atendimentos. Solicite a reativação do cliente ou inclua o funcionário em outro cliente.");
                }
                this.Close();

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }

        public void montaDataGridCliente()
        {
            try
            {
                this.Cursor = Cursors.WaitCursor;

                FuncionarioFacade funcionarioFacade = FuncionarioFacade.getInstance();
                ClienteFacade clienteFacade = ClienteFacade.getInstance();

                dgvCliente.Columns.Clear();

                clienteFuncaoFuncionarioList = funcionarioFacade.findClienteFuncaoFuncionarioByFuncionarioAndSituacao(funcionario, true);

                dgvCliente.ColumnCount = 4;
                    
                dgvCliente.Columns[0].HeaderText = "id_cliente";
                dgvCliente.Columns[0].Visible = false;

                dgvCliente.Columns[1].HeaderText = "Razão Social";
                dgvCliente.Columns[2].HeaderText = "CNPJ";

                dgvCliente.Columns[3].HeaderText = "Empresa Credenciadora";
                dgvCliente.DefaultCellStyle.BackColor = Color.LightGray;


                List<long> clientesJaExibidos = new List<long>();
                    
                
                foreach (ClienteFuncaoFuncionario cff in clienteFuncaoFuncionarioList)
                {
                    if (!clientesJaExibidos.Contains(cff.ClienteFuncao.Cliente.Id.Value))
                    {
                        clientesJaExibidos.Add(cff.ClienteFuncao.Cliente.Id.Value);
                        dgvCliente.Rows.Add(cff.ClienteFuncao.Cliente.Id,
                            cff.ClienteFuncao.Cliente.RazaoSocial,
                            cff.ClienteFuncao.Cliente.Cnpj,
                            cff.ClienteFuncao.Cliente.CredenciadaCliente != null ?
                            clienteFacade.findClienteById((Int64)cff.ClienteFuncao.Cliente.CredenciadaCliente.Id).RazaoSocial :
                            String.Empty);
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }

        private void dgvCliente_DataBindingComplete(object sender, DataGridViewBindingCompleteEventArgs e)
        {
            DataGridView dgv = sender as DataGridView;
            dgv.ClearSelection();
        }

        public void montaDataGridFuncao()
        {
            try
            {
                this.Cursor = Cursors.WaitCursor;

                dgvFuncao.Columns.Clear();

                dgvFuncao.ColumnCount = 3;

                dgvFuncao.Columns[0].HeaderText = "id_cliente_funcao_funcionario";
                dgvFuncao.Columns[0].Visible = false;

                dgvFuncao.Columns[1].HeaderText = "Código Função";
                dgvFuncao.Columns[1].Visible = true;

                dgvFuncao.Columns[2].HeaderText = "Função";
                
                foreach (ClienteFuncaoFuncionario cff in clienteFuncaoFuncionarioList)
                {
                    if (cff.ClienteFuncao.Cliente.Id == (Int64)dgvCliente.CurrentRow.Cells[0].Value)
                    {
                        dgvFuncao.Rows.Add(cff.Id,
                            cff.ClienteFuncao.Funcao.Id,
                            cff.ClienteFuncao.Funcao.Descricao);
                    }
                    
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }

        private void dgvCliente_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                if (dgvCliente.CurrentRow == null)
                    throw new Exception(msg1);

                montaDataGridFuncao();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void dgvFuncao_CellContentDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                btConfirma.PerformClick();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }
        
        
    }
}
