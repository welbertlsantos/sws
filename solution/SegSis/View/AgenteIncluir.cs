﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using SegSis.Facade;
using SegSis.Entidade;
using SegSis.Excecao;
using SegSis.View.ViewHelper;
using SegSis.View.Resources;
using SegSis.ViewHelper;

namespace SegSis.View
{
    public partial class frm_agente_incluir : BaseForm
    {
        public static String Ms01 = " Agente incluído com sucesso.";
        public static String Ms02 = " Não é possivel incluir Agente. Descrição já cadastrada.";
        
        Agente agente = null;
        public Agente getAgente()
        {
            return this.agente;
        }

        public frm_agente_incluir()
        {
            InitializeComponent();
            carregaComboRisco(cb_tipo);
        }

        private void lbl_limpar_Click(object sender, EventArgs e)
        {
            this.incluirAgenteErrorProvider.Clear();
            text_descricao.Text = String.Empty;
            text_trajetoria.Text = String.Empty;
            text_dano.Text = String.Empty;
            text_limite.Text = String.Empty;
            carregaComboRisco(cb_tipo);

        }

        private void btn_fechar_Click(object sender, EventArgs e)
        {
            this.Close();

        }

        private void btn_gravar_Click(object sender, EventArgs e)
        {
            this.incluirAgenteErrorProvider.Clear();
            try
            {
                if (validaCamposObrigatorio())
                {

                    PpraFacade PpraFacade = PpraFacade.getInstance();

                    SelectItem riscoSelecionado = (SelectItem)cb_tipo.SelectedItem;
                    Risco risco = new Risco(Convert.ToInt64(riscoSelecionado.Valor), Convert.ToString(riscoSelecionado.Nome));

                    agente = new Agente(null, risco,  Convert.ToString(text_descricao.Text.Trim()),  
                            Convert.ToString(text_trajetoria.Text.Trim()), Convert.ToString(text_dano.Text.Trim()), 
                            Convert.ToString(text_limite.Text.Trim()), ApplicationConstants.ATIVO,false);

                    agente = PpraFacade.incluirAgente(agente);

                    MessageBox.Show(Ms01);

                    this.Close();
                }
            }
            catch (Exception Ex)
            {
                MessageBox.Show(Ex.Message);
            }
        }

        private Boolean validaCamposObrigatorio()
        {
            Boolean retorno = Convert.ToBoolean(true);

            if (string.IsNullOrEmpty(text_descricao.Text))
            {
                this.incluirAgenteErrorProvider.SetError(this.text_descricao, "A Descrição é obrigatória!");
                retorno = Convert.ToBoolean(false);
            }
            return retorno;
        }

        private Boolean validaDescricao()
        {
            Boolean retorno = Convert.ToBoolean(false);

            try
            {
                PpraFacade PpraFacade = PpraFacade.getInstance();
                string descricao = Convert.ToString(text_descricao.Text);

                Agente agente = PpraFacade.findAgenteByDescricao(descricao);

                if (agente != null)
                {
                    retorno = Convert.ToBoolean(true);
                    MessageBox.Show(Ms02);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }

            return retorno;
        }

        private void carregaComboRisco(ComboBox comboRisco)
        {
            try
            {
                PpraFacade ppraFacade = PpraFacade.getInstance();

                Risco risco = new Risco();

                comboRisco.DisplayMember = "nome";
                comboRisco.ValueMember = "valor";

                foreach (DataRow rows in ppraFacade.findRisco().Tables[0].Rows)
                {
                    comboRisco.Items.Add(new SelectItem(Convert.ToString(rows[Convert.ToString("id_risco")]), Convert.ToString(rows[Convert.ToString("descricao")])));
                }


            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void frm_agente_incluir_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                this.SelectNextControl(this.ActiveControl, !e.Shift, true, true, true);
            }

        }

    }
}
