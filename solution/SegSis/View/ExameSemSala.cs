﻿using SWS.Entidade;
using SWS.Facade;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace SWS.View
{
    public partial class frmExameSemSala : frmTemplate
    {
        private List<Exame> exames = new List<Exame>();

        public frmExameSemSala()
        {
            InitializeComponent();
            montaGridExameSemSala();
        }

        public void montaGridExameSemSala()
        {
            try
            {
                this.Cursor = Cursors.WaitCursor;
                dgvExameSemSala.Columns.Clear();
                FilaFacade filaFacade = FilaFacade.getInstance();

                dgvExameSemSala.ColumnCount = 4;

                dgvExameSemSala.Columns[0].HeaderText = "Código";
                dgvExameSemSala.Columns[1].HeaderText = "Exame";
                dgvExameSemSala.Columns[2].HeaderText = "Laboratório";
                dgvExameSemSala.Columns[3].HeaderText = "Externo";

                exames = filaFacade.listAllExameNotInSala();

                exames.ForEach(delegate(Exame exame)
                {
                    dgvExameSemSala.Rows.Add(Convert.ToString(exame.Id).PadLeft(6, '0'), exame.Descricao, exame.Laboratorio == true ? "SIM" : "", exame.Externo == true ? "SIM" : "");
                });

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }

        }

        private void btnImprimir_Click(object sender, EventArgs e)
        {
            try
            {
                this.Cursor = Cursors.WaitCursor;

                var process = new Process();

                process.StartInfo.FileName = "Relatorio\\jre7\\bin\\java";
                process.StartInfo.Arguments = " -jar Relatorio\\MiddlewareJasper.jar " + "Relatorio\\Report\\" + "EXAMES_SEMSALA.JASPER";
                process.StartInfo.UseShellExecute = false;
                process.StartInfo.RedirectStandardError = true;
                process.StartInfo.RedirectStandardInput = true;
                process.StartInfo.RedirectStandardOutput = true;
                process.StartInfo.CreateNoWindow = true;

                process.Start();
                process.StandardOutput.ReadToEnd();
                process.WaitForExit();

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }

        }

        private void btnFechar_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
