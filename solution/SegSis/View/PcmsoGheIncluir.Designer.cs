﻿namespace SWS.View
{
    partial class frmPcmsoGheIncluir
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        protected System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        protected void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmPcmsoGheIncluir));
            this.flpAcao = new System.Windows.Forms.FlowLayoutPanel();
            this.btnSalvar = new System.Windows.Forms.Button();
            this.btnFechar = new System.Windows.Forms.Button();
            this.lblNumeroPo = new System.Windows.Forms.TextBox();
            this.textNumeroPo = new System.Windows.Forms.TextBox();
            this.lblExposto = new System.Windows.Forms.TextBox();
            this.textExposto = new System.Windows.Forms.TextBox();
            this.lblDescricao = new System.Windows.Forms.TextBox();
            this.textDescricao = new System.Windows.Forms.TextBox();
            this.grbNota = new System.Windows.Forms.GroupBox();
            this.dgvNota = new System.Windows.Forms.DataGridView();
            this.flpNota = new System.Windows.Forms.FlowLayoutPanel();
            this.btnNotaIncluir = new System.Windows.Forms.Button();
            this.btnNotaExcluir = new System.Windows.Forms.Button();
            this.lblObrigatorioDescricao = new System.Windows.Forms.Label();
            this.lblObrigatorioNumeroExposto = new System.Windows.Forms.Label();
            this.lblNotaGhe = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.scForm)).BeginInit();
            this.scForm.Panel1.SuspendLayout();
            this.scForm.Panel2.SuspendLayout();
            this.scForm.SuspendLayout();
            this.pnlForm.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.banner)).BeginInit();
            this.flpAcao.SuspendLayout();
            this.grbNota.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvNota)).BeginInit();
            this.flpNota.SuspendLayout();
            this.SuspendLayout();
            // 
            // scForm
            // 
            // 
            // scForm.Panel1
            // 
            this.scForm.Panel1.Controls.Add(this.flpAcao);
            // 
            // pnlForm
            // 
            this.pnlForm.Controls.Add(this.lblNotaGhe);
            this.pnlForm.Controls.Add(this.lblObrigatorioNumeroExposto);
            this.pnlForm.Controls.Add(this.lblObrigatorioDescricao);
            this.pnlForm.Controls.Add(this.flpNota);
            this.pnlForm.Controls.Add(this.grbNota);
            this.pnlForm.Controls.Add(this.lblNumeroPo);
            this.pnlForm.Controls.Add(this.textNumeroPo);
            this.pnlForm.Controls.Add(this.lblExposto);
            this.pnlForm.Controls.Add(this.textExposto);
            this.pnlForm.Controls.Add(this.lblDescricao);
            this.pnlForm.Controls.Add(this.textDescricao);
            // 
            // flpAcao
            // 
            this.flpAcao.Controls.Add(this.btnSalvar);
            this.flpAcao.Controls.Add(this.btnFechar);
            this.flpAcao.Dock = System.Windows.Forms.DockStyle.Fill;
            this.flpAcao.Location = new System.Drawing.Point(0, 0);
            this.flpAcao.Name = "flpAcao";
            this.flpAcao.Size = new System.Drawing.Size(534, 30);
            this.flpAcao.TabIndex = 0;
            // 
            // btnSalvar
            // 
            this.btnSalvar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnSalvar.Image = global::SWS.Properties.Resources.Gravar;
            this.btnSalvar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnSalvar.Location = new System.Drawing.Point(3, 3);
            this.btnSalvar.Name = "btnSalvar";
            this.btnSalvar.Size = new System.Drawing.Size(75, 23);
            this.btnSalvar.TabIndex = 4;
            this.btnSalvar.Text = "&Gravar";
            this.btnSalvar.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnSalvar.UseVisualStyleBackColor = true;
            this.btnSalvar.Click += new System.EventHandler(this.btnSalvar_Click);
            // 
            // btnFechar
            // 
            this.btnFechar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnFechar.Image = global::SWS.Properties.Resources.close;
            this.btnFechar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnFechar.Location = new System.Drawing.Point(84, 3);
            this.btnFechar.Name = "btnFechar";
            this.btnFechar.Size = new System.Drawing.Size(75, 23);
            this.btnFechar.TabIndex = 8;
            this.btnFechar.TabStop = false;
            this.btnFechar.Text = "&Fechar";
            this.btnFechar.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnFechar.UseVisualStyleBackColor = true;
            this.btnFechar.Click += new System.EventHandler(this.btnFechar_Click);
            // 
            // lblNumeroPo
            // 
            this.lblNumeroPo.BackColor = System.Drawing.Color.LightGray;
            this.lblNumeroPo.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblNumeroPo.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNumeroPo.Location = new System.Drawing.Point(6, 47);
            this.lblNumeroPo.Name = "lblNumeroPo";
            this.lblNumeroPo.ReadOnly = true;
            this.lblNumeroPo.Size = new System.Drawing.Size(127, 21);
            this.lblNumeroPo.TabIndex = 19;
            this.lblNumeroPo.TabStop = false;
            this.lblNumeroPo.Text = "Número Po";
            // 
            // textNumeroPo
            // 
            this.textNumeroPo.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textNumeroPo.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.textNumeroPo.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textNumeroPo.Location = new System.Drawing.Point(132, 47);
            this.textNumeroPo.MaxLength = 15;
            this.textNumeroPo.Name = "textNumeroPo";
            this.textNumeroPo.Size = new System.Drawing.Size(356, 21);
            this.textNumeroPo.TabIndex = 3;
            // 
            // lblExposto
            // 
            this.lblExposto.BackColor = System.Drawing.Color.LightGray;
            this.lblExposto.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblExposto.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblExposto.Location = new System.Drawing.Point(6, 27);
            this.lblExposto.Name = "lblExposto";
            this.lblExposto.ReadOnly = true;
            this.lblExposto.Size = new System.Drawing.Size(127, 21);
            this.lblExposto.TabIndex = 18;
            this.lblExposto.TabStop = false;
            this.lblExposto.Text = "Número de Expostos";
            // 
            // textExposto
            // 
            this.textExposto.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textExposto.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.textExposto.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textExposto.Location = new System.Drawing.Point(132, 27);
            this.textExposto.MaxLength = 8;
            this.textExposto.Name = "textExposto";
            this.textExposto.Size = new System.Drawing.Size(356, 21);
            this.textExposto.TabIndex = 2;
            this.textExposto.Text = "0";
            this.textExposto.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.textExposto_KeyPress);
            // 
            // lblDescricao
            // 
            this.lblDescricao.BackColor = System.Drawing.Color.LightGray;
            this.lblDescricao.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblDescricao.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDescricao.Location = new System.Drawing.Point(6, 7);
            this.lblDescricao.Name = "lblDescricao";
            this.lblDescricao.ReadOnly = true;
            this.lblDescricao.Size = new System.Drawing.Size(127, 21);
            this.lblDescricao.TabIndex = 17;
            this.lblDescricao.TabStop = false;
            this.lblDescricao.Text = "Descrição";
            // 
            // textDescricao
            // 
            this.textDescricao.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textDescricao.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.textDescricao.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textDescricao.Location = new System.Drawing.Point(132, 7);
            this.textDescricao.MaxLength = 100;
            this.textDescricao.Name = "textDescricao";
            this.textDescricao.Size = new System.Drawing.Size(356, 21);
            this.textDescricao.TabIndex = 1;
            // 
            // grbNota
            // 
            this.grbNota.Controls.Add(this.dgvNota);
            this.grbNota.Location = new System.Drawing.Point(6, 74);
            this.grbNota.Name = "grbNota";
            this.grbNota.Size = new System.Drawing.Size(482, 143);
            this.grbNota.TabIndex = 20;
            this.grbNota.TabStop = false;
            this.grbNota.Text = "Notas";
            // 
            // dgvNota
            // 
            this.dgvNota.AllowUserToAddRows = false;
            this.dgvNota.AllowUserToDeleteRows = false;
            this.dgvNota.AllowUserToOrderColumns = true;
            this.dgvNota.AllowUserToResizeRows = false;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.Gainsboro;
            this.dgvNota.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle1;
            this.dgvNota.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this.dgvNota.BackgroundColor = System.Drawing.Color.White;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.Color.LightGray;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvNota.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle2;
            this.dgvNota.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle3.BackColor = System.Drawing.Color.LightGray;
            dataGridViewCellStyle3.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvNota.DefaultCellStyle = dataGridViewCellStyle3;
            this.dgvNota.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvNota.Location = new System.Drawing.Point(3, 16);
            this.dgvNota.MultiSelect = false;
            this.dgvNota.Name = "dgvNota";
            this.dgvNota.ReadOnly = true;
            this.dgvNota.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvNota.Size = new System.Drawing.Size(476, 124);
            this.dgvNota.TabIndex = 0;
            // 
            // flpNota
            // 
            this.flpNota.Controls.Add(this.btnNotaIncluir);
            this.flpNota.Controls.Add(this.btnNotaExcluir);
            this.flpNota.Location = new System.Drawing.Point(6, 224);
            this.flpNota.Name = "flpNota";
            this.flpNota.Size = new System.Drawing.Size(210, 30);
            this.flpNota.TabIndex = 21;
            // 
            // btnNotaIncluir
            // 
            this.btnNotaIncluir.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnNotaIncluir.Image = ((System.Drawing.Image)(resources.GetObject("btnNotaIncluir.Image")));
            this.btnNotaIncluir.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnNotaIncluir.Location = new System.Drawing.Point(3, 3);
            this.btnNotaIncluir.Name = "btnNotaIncluir";
            this.btnNotaIncluir.Size = new System.Drawing.Size(75, 23);
            this.btnNotaIncluir.TabIndex = 5;
            this.btnNotaIncluir.Text = "&Incluir";
            this.btnNotaIncluir.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnNotaIncluir.UseVisualStyleBackColor = true;
            this.btnNotaIncluir.Click += new System.EventHandler(this.btnNotaIncluir_Click);
            // 
            // btnNotaExcluir
            // 
            this.btnNotaExcluir.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnNotaExcluir.Image = ((System.Drawing.Image)(resources.GetObject("btnNotaExcluir.Image")));
            this.btnNotaExcluir.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnNotaExcluir.Location = new System.Drawing.Point(84, 3);
            this.btnNotaExcluir.Name = "btnNotaExcluir";
            this.btnNotaExcluir.Size = new System.Drawing.Size(75, 23);
            this.btnNotaExcluir.TabIndex = 48;
            this.btnNotaExcluir.TabStop = false;
            this.btnNotaExcluir.Text = "&Excluir";
            this.btnNotaExcluir.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnNotaExcluir.UseVisualStyleBackColor = true;
            this.btnNotaExcluir.Click += new System.EventHandler(this.btnNotaExcluir_Click);
            // 
            // lblObrigatorioDescricao
            // 
            this.lblObrigatorioDescricao.AutoSize = true;
            this.lblObrigatorioDescricao.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblObrigatorioDescricao.ForeColor = System.Drawing.Color.Red;
            this.lblObrigatorioDescricao.Location = new System.Drawing.Point(494, 8);
            this.lblObrigatorioDescricao.Name = "lblObrigatorioDescricao";
            this.lblObrigatorioDescricao.Size = new System.Drawing.Size(15, 20);
            this.lblObrigatorioDescricao.TabIndex = 92;
            this.lblObrigatorioDescricao.Text = "*";
            // 
            // lblObrigatorioNumeroExposto
            // 
            this.lblObrigatorioNumeroExposto.AutoSize = true;
            this.lblObrigatorioNumeroExposto.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblObrigatorioNumeroExposto.ForeColor = System.Drawing.Color.Red;
            this.lblObrigatorioNumeroExposto.Location = new System.Drawing.Point(494, 27);
            this.lblObrigatorioNumeroExposto.Name = "lblObrigatorioNumeroExposto";
            this.lblObrigatorioNumeroExposto.Size = new System.Drawing.Size(15, 20);
            this.lblObrigatorioNumeroExposto.TabIndex = 93;
            this.lblObrigatorioNumeroExposto.Text = "*";
            // 
            // lblNotaGhe
            // 
            this.lblNotaGhe.AutoSize = true;
            this.lblNotaGhe.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNotaGhe.ForeColor = System.Drawing.Color.Blue;
            this.lblNotaGhe.Location = new System.Drawing.Point(6, 257);
            this.lblNotaGhe.Name = "lblNotaGhe";
            this.lblNotaGhe.Size = new System.Drawing.Size(312, 15);
            this.lblNotaGhe.TabIndex = 94;
            this.lblNotaGhe.Text = "Para incluir uma nota, primeiro você deve gravar o GHE.";
            // 
            // frmPcmsoGheIncluir
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(534, 361);
            this.Name = "frmPcmsoGheIncluir";
            this.Text = "INCLUIR GHE";
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.frmPcmsoGheIncluir_KeyDown);
            this.scForm.Panel1.ResumeLayout(false);
            this.scForm.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.scForm)).EndInit();
            this.scForm.ResumeLayout(false);
            this.pnlForm.ResumeLayout(false);
            this.pnlForm.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.banner)).EndInit();
            this.flpAcao.ResumeLayout(false);
            this.grbNota.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvNota)).EndInit();
            this.flpNota.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        protected System.Windows.Forms.FlowLayoutPanel flpAcao;
        protected System.Windows.Forms.Button btnSalvar;
        protected System.Windows.Forms.Button btnFechar;
        protected System.Windows.Forms.TextBox lblNumeroPo;
        protected System.Windows.Forms.TextBox textNumeroPo;
        protected System.Windows.Forms.TextBox lblExposto;
        protected System.Windows.Forms.TextBox textExposto;
        protected System.Windows.Forms.TextBox lblDescricao;
        protected System.Windows.Forms.TextBox textDescricao;
        protected System.Windows.Forms.GroupBox grbNota;
        protected System.Windows.Forms.DataGridView dgvNota;
        protected System.Windows.Forms.FlowLayoutPanel flpNota;
        protected System.Windows.Forms.Button btnNotaIncluir;
        protected System.Windows.Forms.Button btnNotaExcluir;
        protected System.Windows.Forms.Label lblObrigatorioNumeroExposto;
        protected System.Windows.Forms.Label lblObrigatorioDescricao;
        protected System.Windows.Forms.Label lblNotaGhe;
    }
}