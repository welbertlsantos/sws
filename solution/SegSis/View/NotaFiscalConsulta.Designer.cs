﻿namespace SWS.View
{
    partial class frmNotaFiscalConsulta
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            this.flpAcao = new System.Windows.Forms.FlowLayoutPanel();
            this.btnSair = new System.Windows.Forms.Button();
            this.tb_cabecalho = new System.Windows.Forms.TabControl();
            this.tb_cliente = new System.Windows.Forms.TabPage();
            this.lblEmail = new System.Windows.Forms.TextBox();
            this.lblTelefone = new System.Windows.Forms.TextBox();
            this.lblUf = new System.Windows.Forms.TextBox();
            this.lblCep = new System.Windows.Forms.TextBox();
            this.lblCidade = new System.Windows.Forms.TextBox();
            this.lblBairro = new System.Windows.Forms.TextBox();
            this.lblComplemento = new System.Windows.Forms.TextBox();
            this.lblNumero = new System.Windows.Forms.TextBox();
            this.lblEndereco = new System.Windows.Forms.TextBox();
            this.lblInscricao = new System.Windows.Forms.TextBox();
            this.lblCnpj = new System.Windows.Forms.TextBox();
            this.lblCliente = new System.Windows.Forms.TextBox();
            this.text_email = new System.Windows.Forms.TextBox();
            this.text_telefone = new System.Windows.Forms.TextBox();
            this.text_cep = new System.Windows.Forms.MaskedTextBox();
            this.text_uf = new System.Windows.Forms.TextBox();
            this.text_cidade = new System.Windows.Forms.TextBox();
            this.text_bairro = new System.Windows.Forms.TextBox();
            this.text_complemento = new System.Windows.Forms.TextBox();
            this.text_numeroCliente = new System.Windows.Forms.TextBox();
            this.text_endereco = new System.Windows.Forms.TextBox();
            this.text_inscricao = new System.Windows.Forms.TextBox();
            this.text_cnpjCliente = new System.Windows.Forms.MaskedTextBox();
            this.text_cliente = new System.Windows.Forms.TextBox();
            this.tb_emitente = new System.Windows.Forms.TabPage();
            this.lblMotivoCancelamento = new System.Windows.Forms.TextBox();
            this.lblSituacao = new System.Windows.Forms.TextBox();
            this.lblDataGravacao = new System.Windows.Forms.TextBox();
            this.lblDataEmissao = new System.Windows.Forms.TextBox();
            this.textNumeroNota = new System.Windows.Forms.TextBox();
            this.lblNumeroNota = new System.Windows.Forms.TextBox();
            this.lblCnpjEmpresa = new System.Windows.Forms.TextBox();
            this.lblEmpresa = new System.Windows.Forms.TextBox();
            this.text_motivoCancelamento = new System.Windows.Forms.TextBox();
            this.text_situacao = new System.Windows.Forms.TextBox();
            this.text_dtGravacao = new System.Windows.Forms.TextBox();
            this.text_dtEmissao = new System.Windows.Forms.TextBox();
            this.text_cnpj = new System.Windows.Forms.MaskedTextBox();
            this.text_empresa = new System.Windows.Forms.TextBox();
            this.lbl_numero = new System.Windows.Forms.Label();
            this.grb_itens = new System.Windows.Forms.GroupBox();
            this.tb_movimentoCobranca = new System.Windows.Forms.TabControl();
            this.tbItens = new System.Windows.Forms.TabPage();
            this.dg_itens = new System.Windows.Forms.DataGridView();
            this.tb_cobranca = new System.Windows.Forms.TabPage();
            this.dg_cobranca = new System.Windows.Forms.DataGridView();
            this.grb_total = new System.Windows.Forms.GroupBox();
            this.lblValorLiquido = new System.Windows.Forms.TextBox();
            this.lblCsll = new System.Windows.Forms.TextBox();
            this.lblIr = new System.Windows.Forms.TextBox();
            this.lblIss = new System.Windows.Forms.TextBox();
            this.lblAliquota = new System.Windows.Forms.TextBox();
            this.lblBase = new System.Windows.Forms.TextBox();
            this.lblCofins = new System.Windows.Forms.TextBox();
            this.lblPis = new System.Windows.Forms.TextBox();
            this.lblValorNF = new System.Windows.Forms.TextBox();
            this.text_valorLiquido = new System.Windows.Forms.TextBox();
            this.text_csll = new System.Windows.Forms.TextBox();
            this.text_IR = new System.Windows.Forms.TextBox();
            this.text_valorIss = new System.Windows.Forms.TextBox();
            this.text_aliquotaPercentual = new System.Windows.Forms.TextBox();
            this.text_baseCalculo = new System.Windows.Forms.TextBox();
            this.text_cofins = new System.Windows.Forms.TextBox();
            this.text_pis = new System.Windows.Forms.TextBox();
            this.text_valorTotal = new System.Windows.Forms.TextBox();
            ((System.ComponentModel.ISupportInitialize)(this.spForm)).BeginInit();
            this.spForm.Panel1.SuspendLayout();
            this.spForm.Panel2.SuspendLayout();
            this.spForm.SuspendLayout();
            this.pnlForm.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.banner)).BeginInit();
            this.flpAcao.SuspendLayout();
            this.tb_cabecalho.SuspendLayout();
            this.tb_cliente.SuspendLayout();
            this.tb_emitente.SuspendLayout();
            this.grb_itens.SuspendLayout();
            this.tb_movimentoCobranca.SuspendLayout();
            this.tbItens.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dg_itens)).BeginInit();
            this.tb_cobranca.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dg_cobranca)).BeginInit();
            this.grb_total.SuspendLayout();
            this.SuspendLayout();
            // 
            // spForm
            // 
            // 
            // spForm.Panel1
            // 
            this.spForm.Panel1.Controls.Add(this.flpAcao);
            // 
            // pnlForm
            // 
            this.pnlForm.Controls.Add(this.grb_total);
            this.pnlForm.Controls.Add(this.grb_itens);
            this.pnlForm.Controls.Add(this.tb_cabecalho);
            this.pnlForm.Controls.Add(this.lbl_numero);
            this.pnlForm.Size = new System.Drawing.Size(764, 756);
            // 
            // flpAcao
            // 
            this.flpAcao.Controls.Add(this.btnSair);
            this.flpAcao.Dock = System.Windows.Forms.DockStyle.Fill;
            this.flpAcao.Location = new System.Drawing.Point(0, 0);
            this.flpAcao.Name = "flpAcao";
            this.flpAcao.Size = new System.Drawing.Size(784, 35);
            this.flpAcao.TabIndex = 0;
            // 
            // btnSair
            // 
            this.btnSair.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnSair.Image = global::SWS.Properties.Resources.close;
            this.btnSair.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnSair.Location = new System.Drawing.Point(3, 3);
            this.btnSair.Name = "btnSair";
            this.btnSair.Size = new System.Drawing.Size(75, 23);
            this.btnSair.TabIndex = 1;
            this.btnSair.Text = "&Fechar";
            this.btnSair.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnSair.UseVisualStyleBackColor = true;
            this.btnSair.Click += new System.EventHandler(this.btnSair_Click);
            // 
            // tb_cabecalho
            // 
            this.tb_cabecalho.Controls.Add(this.tb_cliente);
            this.tb_cabecalho.Controls.Add(this.tb_emitente);
            this.tb_cabecalho.Location = new System.Drawing.Point(9, 14);
            this.tb_cabecalho.Name = "tb_cabecalho";
            this.tb_cabecalho.SelectedIndex = 0;
            this.tb_cabecalho.Size = new System.Drawing.Size(752, 280);
            this.tb_cabecalho.TabIndex = 1;
            // 
            // tb_cliente
            // 
            this.tb_cliente.BackColor = System.Drawing.Color.White;
            this.tb_cliente.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tb_cliente.Controls.Add(this.lblEmail);
            this.tb_cliente.Controls.Add(this.lblTelefone);
            this.tb_cliente.Controls.Add(this.lblUf);
            this.tb_cliente.Controls.Add(this.lblCep);
            this.tb_cliente.Controls.Add(this.lblCidade);
            this.tb_cliente.Controls.Add(this.lblBairro);
            this.tb_cliente.Controls.Add(this.lblComplemento);
            this.tb_cliente.Controls.Add(this.lblNumero);
            this.tb_cliente.Controls.Add(this.lblEndereco);
            this.tb_cliente.Controls.Add(this.lblInscricao);
            this.tb_cliente.Controls.Add(this.lblCnpj);
            this.tb_cliente.Controls.Add(this.lblCliente);
            this.tb_cliente.Controls.Add(this.text_email);
            this.tb_cliente.Controls.Add(this.text_telefone);
            this.tb_cliente.Controls.Add(this.text_cep);
            this.tb_cliente.Controls.Add(this.text_uf);
            this.tb_cliente.Controls.Add(this.text_cidade);
            this.tb_cliente.Controls.Add(this.text_bairro);
            this.tb_cliente.Controls.Add(this.text_complemento);
            this.tb_cliente.Controls.Add(this.text_numeroCliente);
            this.tb_cliente.Controls.Add(this.text_endereco);
            this.tb_cliente.Controls.Add(this.text_inscricao);
            this.tb_cliente.Controls.Add(this.text_cnpjCliente);
            this.tb_cliente.Controls.Add(this.text_cliente);
            this.tb_cliente.Location = new System.Drawing.Point(4, 22);
            this.tb_cliente.Name = "tb_cliente";
            this.tb_cliente.Padding = new System.Windows.Forms.Padding(3);
            this.tb_cliente.Size = new System.Drawing.Size(744, 254);
            this.tb_cliente.TabIndex = 1;
            this.tb_cliente.Text = "Cliente";
            // 
            // lblEmail
            // 
            this.lblEmail.BackColor = System.Drawing.Color.LightGray;
            this.lblEmail.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblEmail.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblEmail.Location = new System.Drawing.Point(4, 223);
            this.lblEmail.Name = "lblEmail";
            this.lblEmail.Size = new System.Drawing.Size(119, 21);
            this.lblEmail.TabIndex = 40;
            this.lblEmail.TabStop = false;
            this.lblEmail.Text = "E-mail";
            // 
            // lblTelefone
            // 
            this.lblTelefone.BackColor = System.Drawing.Color.LightGray;
            this.lblTelefone.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblTelefone.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTelefone.Location = new System.Drawing.Point(4, 203);
            this.lblTelefone.Name = "lblTelefone";
            this.lblTelefone.Size = new System.Drawing.Size(119, 21);
            this.lblTelefone.TabIndex = 39;
            this.lblTelefone.TabStop = false;
            this.lblTelefone.Text = "Telefone";
            // 
            // lblUf
            // 
            this.lblUf.BackColor = System.Drawing.Color.LightGray;
            this.lblUf.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblUf.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblUf.Location = new System.Drawing.Point(4, 183);
            this.lblUf.Name = "lblUf";
            this.lblUf.Size = new System.Drawing.Size(119, 21);
            this.lblUf.TabIndex = 38;
            this.lblUf.TabStop = false;
            this.lblUf.Text = "UF";
            // 
            // lblCep
            // 
            this.lblCep.BackColor = System.Drawing.Color.LightGray;
            this.lblCep.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblCep.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCep.Location = new System.Drawing.Point(4, 163);
            this.lblCep.Name = "lblCep";
            this.lblCep.Size = new System.Drawing.Size(119, 21);
            this.lblCep.TabIndex = 37;
            this.lblCep.TabStop = false;
            this.lblCep.Text = "CEP";
            // 
            // lblCidade
            // 
            this.lblCidade.BackColor = System.Drawing.Color.LightGray;
            this.lblCidade.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblCidade.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCidade.Location = new System.Drawing.Point(4, 143);
            this.lblCidade.Name = "lblCidade";
            this.lblCidade.Size = new System.Drawing.Size(119, 21);
            this.lblCidade.TabIndex = 36;
            this.lblCidade.TabStop = false;
            this.lblCidade.Text = "Cidade";
            // 
            // lblBairro
            // 
            this.lblBairro.BackColor = System.Drawing.Color.LightGray;
            this.lblBairro.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblBairro.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblBairro.Location = new System.Drawing.Point(4, 123);
            this.lblBairro.Name = "lblBairro";
            this.lblBairro.Size = new System.Drawing.Size(119, 21);
            this.lblBairro.TabIndex = 35;
            this.lblBairro.TabStop = false;
            this.lblBairro.Text = "Bairro";
            // 
            // lblComplemento
            // 
            this.lblComplemento.BackColor = System.Drawing.Color.LightGray;
            this.lblComplemento.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblComplemento.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblComplemento.Location = new System.Drawing.Point(4, 103);
            this.lblComplemento.Name = "lblComplemento";
            this.lblComplemento.Size = new System.Drawing.Size(119, 21);
            this.lblComplemento.TabIndex = 34;
            this.lblComplemento.TabStop = false;
            this.lblComplemento.Text = "Complemento";
            // 
            // lblNumero
            // 
            this.lblNumero.BackColor = System.Drawing.Color.LightGray;
            this.lblNumero.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblNumero.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNumero.Location = new System.Drawing.Point(4, 83);
            this.lblNumero.Name = "lblNumero";
            this.lblNumero.Size = new System.Drawing.Size(119, 21);
            this.lblNumero.TabIndex = 33;
            this.lblNumero.TabStop = false;
            this.lblNumero.Text = "Número";
            // 
            // lblEndereco
            // 
            this.lblEndereco.BackColor = System.Drawing.Color.LightGray;
            this.lblEndereco.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblEndereco.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblEndereco.Location = new System.Drawing.Point(4, 63);
            this.lblEndereco.Name = "lblEndereco";
            this.lblEndereco.Size = new System.Drawing.Size(119, 21);
            this.lblEndereco.TabIndex = 32;
            this.lblEndereco.TabStop = false;
            this.lblEndereco.Text = "Endereço";
            // 
            // lblInscricao
            // 
            this.lblInscricao.BackColor = System.Drawing.Color.LightGray;
            this.lblInscricao.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblInscricao.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblInscricao.Location = new System.Drawing.Point(4, 43);
            this.lblInscricao.Name = "lblInscricao";
            this.lblInscricao.Size = new System.Drawing.Size(119, 21);
            this.lblInscricao.TabIndex = 31;
            this.lblInscricao.TabStop = false;
            this.lblInscricao.Text = "Inscrição";
            // 
            // lblCnpj
            // 
            this.lblCnpj.BackColor = System.Drawing.Color.LightGray;
            this.lblCnpj.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblCnpj.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCnpj.Location = new System.Drawing.Point(4, 23);
            this.lblCnpj.Name = "lblCnpj";
            this.lblCnpj.Size = new System.Drawing.Size(119, 21);
            this.lblCnpj.TabIndex = 30;
            this.lblCnpj.TabStop = false;
            this.lblCnpj.Text = "CNPJ";
            // 
            // lblCliente
            // 
            this.lblCliente.BackColor = System.Drawing.Color.LightGray;
            this.lblCliente.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblCliente.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCliente.Location = new System.Drawing.Point(4, 3);
            this.lblCliente.Name = "lblCliente";
            this.lblCliente.Size = new System.Drawing.Size(119, 21);
            this.lblCliente.TabIndex = 29;
            this.lblCliente.TabStop = false;
            this.lblCliente.Text = "Cliente";
            // 
            // text_email
            // 
            this.text_email.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.text_email.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.text_email.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.text_email.Location = new System.Drawing.Point(122, 223);
            this.text_email.MaxLength = 50;
            this.text_email.Name = "text_email";
            this.text_email.ReadOnly = true;
            this.text_email.Size = new System.Drawing.Size(612, 21);
            this.text_email.TabIndex = 28;
            this.text_email.TabStop = false;
            // 
            // text_telefone
            // 
            this.text_telefone.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.text_telefone.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.text_telefone.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.text_telefone.Location = new System.Drawing.Point(122, 203);
            this.text_telefone.MaxLength = 50;
            this.text_telefone.Name = "text_telefone";
            this.text_telefone.ReadOnly = true;
            this.text_telefone.Size = new System.Drawing.Size(612, 21);
            this.text_telefone.TabIndex = 26;
            this.text_telefone.TabStop = false;
            // 
            // text_cep
            // 
            this.text_cep.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.text_cep.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.text_cep.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.text_cep.Location = new System.Drawing.Point(122, 163);
            this.text_cep.Mask = "99,999-999";
            this.text_cep.Name = "text_cep";
            this.text_cep.PromptChar = ' ';
            this.text_cep.ReadOnly = true;
            this.text_cep.Size = new System.Drawing.Size(612, 21);
            this.text_cep.TabIndex = 24;
            // 
            // text_uf
            // 
            this.text_uf.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.text_uf.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.text_uf.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.text_uf.Location = new System.Drawing.Point(122, 183);
            this.text_uf.MaxLength = 2;
            this.text_uf.Name = "text_uf";
            this.text_uf.ReadOnly = true;
            this.text_uf.Size = new System.Drawing.Size(612, 21);
            this.text_uf.TabIndex = 23;
            this.text_uf.TabStop = false;
            // 
            // text_cidade
            // 
            this.text_cidade.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.text_cidade.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.text_cidade.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.text_cidade.Location = new System.Drawing.Point(122, 143);
            this.text_cidade.MaxLength = 50;
            this.text_cidade.Name = "text_cidade";
            this.text_cidade.ReadOnly = true;
            this.text_cidade.Size = new System.Drawing.Size(612, 21);
            this.text_cidade.TabIndex = 19;
            this.text_cidade.TabStop = false;
            // 
            // text_bairro
            // 
            this.text_bairro.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.text_bairro.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.text_bairro.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.text_bairro.Location = new System.Drawing.Point(122, 123);
            this.text_bairro.MaxLength = 50;
            this.text_bairro.Name = "text_bairro";
            this.text_bairro.ReadOnly = true;
            this.text_bairro.Size = new System.Drawing.Size(612, 21);
            this.text_bairro.TabIndex = 17;
            this.text_bairro.TabStop = false;
            // 
            // text_complemento
            // 
            this.text_complemento.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.text_complemento.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.text_complemento.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.text_complemento.Location = new System.Drawing.Point(122, 103);
            this.text_complemento.MaxLength = 100;
            this.text_complemento.Name = "text_complemento";
            this.text_complemento.ReadOnly = true;
            this.text_complemento.Size = new System.Drawing.Size(612, 21);
            this.text_complemento.TabIndex = 15;
            this.text_complemento.TabStop = false;
            // 
            // text_numeroCliente
            // 
            this.text_numeroCliente.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.text_numeroCliente.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.text_numeroCliente.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.text_numeroCliente.Location = new System.Drawing.Point(122, 83);
            this.text_numeroCliente.MaxLength = 10;
            this.text_numeroCliente.Name = "text_numeroCliente";
            this.text_numeroCliente.ReadOnly = true;
            this.text_numeroCliente.Size = new System.Drawing.Size(612, 21);
            this.text_numeroCliente.TabIndex = 13;
            this.text_numeroCliente.TabStop = false;
            // 
            // text_endereco
            // 
            this.text_endereco.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.text_endereco.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.text_endereco.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.text_endereco.Location = new System.Drawing.Point(122, 63);
            this.text_endereco.MaxLength = 100;
            this.text_endereco.Name = "text_endereco";
            this.text_endereco.ReadOnly = true;
            this.text_endereco.Size = new System.Drawing.Size(612, 21);
            this.text_endereco.TabIndex = 11;
            this.text_endereco.TabStop = false;
            // 
            // text_inscricao
            // 
            this.text_inscricao.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.text_inscricao.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.text_inscricao.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.text_inscricao.Location = new System.Drawing.Point(122, 43);
            this.text_inscricao.MaxLength = 15;
            this.text_inscricao.Name = "text_inscricao";
            this.text_inscricao.ReadOnly = true;
            this.text_inscricao.Size = new System.Drawing.Size(612, 21);
            this.text_inscricao.TabIndex = 9;
            this.text_inscricao.TabStop = false;
            // 
            // text_cnpjCliente
            // 
            this.text_cnpjCliente.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.text_cnpjCliente.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.text_cnpjCliente.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.text_cnpjCliente.Location = new System.Drawing.Point(122, 23);
            this.text_cnpjCliente.Mask = "99,999,999/9999,99";
            this.text_cnpjCliente.Name = "text_cnpjCliente";
            this.text_cnpjCliente.PromptChar = ' ';
            this.text_cnpjCliente.ReadOnly = true;
            this.text_cnpjCliente.Size = new System.Drawing.Size(612, 21);
            this.text_cnpjCliente.TabIndex = 7;
            // 
            // text_cliente
            // 
            this.text_cliente.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.text_cliente.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.text_cliente.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.text_cliente.Location = new System.Drawing.Point(122, 3);
            this.text_cliente.MaxLength = 100;
            this.text_cliente.Name = "text_cliente";
            this.text_cliente.ReadOnly = true;
            this.text_cliente.Size = new System.Drawing.Size(612, 21);
            this.text_cliente.TabIndex = 5;
            this.text_cliente.TabStop = false;
            // 
            // tb_emitente
            // 
            this.tb_emitente.BackColor = System.Drawing.Color.White;
            this.tb_emitente.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tb_emitente.Controls.Add(this.lblMotivoCancelamento);
            this.tb_emitente.Controls.Add(this.lblSituacao);
            this.tb_emitente.Controls.Add(this.lblDataGravacao);
            this.tb_emitente.Controls.Add(this.lblDataEmissao);
            this.tb_emitente.Controls.Add(this.textNumeroNota);
            this.tb_emitente.Controls.Add(this.lblNumeroNota);
            this.tb_emitente.Controls.Add(this.lblCnpjEmpresa);
            this.tb_emitente.Controls.Add(this.lblEmpresa);
            this.tb_emitente.Controls.Add(this.text_motivoCancelamento);
            this.tb_emitente.Controls.Add(this.text_situacao);
            this.tb_emitente.Controls.Add(this.text_dtGravacao);
            this.tb_emitente.Controls.Add(this.text_dtEmissao);
            this.tb_emitente.Controls.Add(this.text_cnpj);
            this.tb_emitente.Controls.Add(this.text_empresa);
            this.tb_emitente.Location = new System.Drawing.Point(4, 22);
            this.tb_emitente.Name = "tb_emitente";
            this.tb_emitente.Padding = new System.Windows.Forms.Padding(3);
            this.tb_emitente.Size = new System.Drawing.Size(744, 254);
            this.tb_emitente.TabIndex = 0;
            this.tb_emitente.Text = "Emitente";
            // 
            // lblMotivoCancelamento
            // 
            this.lblMotivoCancelamento.BackColor = System.Drawing.Color.LightGray;
            this.lblMotivoCancelamento.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblMotivoCancelamento.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblMotivoCancelamento.Location = new System.Drawing.Point(4, 123);
            this.lblMotivoCancelamento.Name = "lblMotivoCancelamento";
            this.lblMotivoCancelamento.Size = new System.Drawing.Size(119, 21);
            this.lblMotivoCancelamento.TabIndex = 37;
            this.lblMotivoCancelamento.TabStop = false;
            this.lblMotivoCancelamento.Text = "Motivo de Cancelado.";
            // 
            // lblSituacao
            // 
            this.lblSituacao.BackColor = System.Drawing.Color.LightGray;
            this.lblSituacao.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblSituacao.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSituacao.Location = new System.Drawing.Point(4, 103);
            this.lblSituacao.Name = "lblSituacao";
            this.lblSituacao.Size = new System.Drawing.Size(119, 21);
            this.lblSituacao.TabIndex = 36;
            this.lblSituacao.TabStop = false;
            this.lblSituacao.Text = "Situação";
            // 
            // lblDataGravacao
            // 
            this.lblDataGravacao.BackColor = System.Drawing.Color.LightGray;
            this.lblDataGravacao.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblDataGravacao.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDataGravacao.Location = new System.Drawing.Point(4, 83);
            this.lblDataGravacao.Name = "lblDataGravacao";
            this.lblDataGravacao.Size = new System.Drawing.Size(119, 21);
            this.lblDataGravacao.TabIndex = 35;
            this.lblDataGravacao.TabStop = false;
            this.lblDataGravacao.Text = "Data de Gravação";
            // 
            // lblDataEmissao
            // 
            this.lblDataEmissao.BackColor = System.Drawing.Color.LightGray;
            this.lblDataEmissao.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblDataEmissao.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDataEmissao.Location = new System.Drawing.Point(4, 63);
            this.lblDataEmissao.Name = "lblDataEmissao";
            this.lblDataEmissao.Size = new System.Drawing.Size(119, 21);
            this.lblDataEmissao.TabIndex = 34;
            this.lblDataEmissao.TabStop = false;
            this.lblDataEmissao.Text = "Data de Emissão";
            // 
            // textNumeroNota
            // 
            this.textNumeroNota.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.textNumeroNota.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textNumeroNota.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textNumeroNota.ForeColor = System.Drawing.Color.Red;
            this.textNumeroNota.Location = new System.Drawing.Point(122, 43);
            this.textNumeroNota.MaxLength = 100;
            this.textNumeroNota.Name = "textNumeroNota";
            this.textNumeroNota.ReadOnly = true;
            this.textNumeroNota.Size = new System.Drawing.Size(607, 21);
            this.textNumeroNota.TabIndex = 33;
            this.textNumeroNota.TabStop = false;
            // 
            // lblNumeroNota
            // 
            this.lblNumeroNota.BackColor = System.Drawing.Color.LightGray;
            this.lblNumeroNota.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblNumeroNota.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNumeroNota.Location = new System.Drawing.Point(4, 43);
            this.lblNumeroNota.Name = "lblNumeroNota";
            this.lblNumeroNota.Size = new System.Drawing.Size(119, 21);
            this.lblNumeroNota.TabIndex = 32;
            this.lblNumeroNota.TabStop = false;
            this.lblNumeroNota.Text = "Número Nota";
            // 
            // lblCnpjEmpresa
            // 
            this.lblCnpjEmpresa.BackColor = System.Drawing.Color.LightGray;
            this.lblCnpjEmpresa.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblCnpjEmpresa.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCnpjEmpresa.Location = new System.Drawing.Point(4, 23);
            this.lblCnpjEmpresa.Name = "lblCnpjEmpresa";
            this.lblCnpjEmpresa.Size = new System.Drawing.Size(119, 21);
            this.lblCnpjEmpresa.TabIndex = 31;
            this.lblCnpjEmpresa.TabStop = false;
            this.lblCnpjEmpresa.Text = "CNPJ";
            // 
            // lblEmpresa
            // 
            this.lblEmpresa.BackColor = System.Drawing.Color.LightGray;
            this.lblEmpresa.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblEmpresa.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblEmpresa.Location = new System.Drawing.Point(4, 3);
            this.lblEmpresa.Name = "lblEmpresa";
            this.lblEmpresa.Size = new System.Drawing.Size(119, 21);
            this.lblEmpresa.TabIndex = 30;
            this.lblEmpresa.TabStop = false;
            this.lblEmpresa.Text = "Empresa";
            // 
            // text_motivoCancelamento
            // 
            this.text_motivoCancelamento.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.text_motivoCancelamento.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.text_motivoCancelamento.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.text_motivoCancelamento.ForeColor = System.Drawing.Color.Blue;
            this.text_motivoCancelamento.Location = new System.Drawing.Point(122, 123);
            this.text_motivoCancelamento.Name = "text_motivoCancelamento";
            this.text_motivoCancelamento.ReadOnly = true;
            this.text_motivoCancelamento.Size = new System.Drawing.Size(607, 21);
            this.text_motivoCancelamento.TabIndex = 13;
            this.text_motivoCancelamento.TabStop = false;
            // 
            // text_situacao
            // 
            this.text_situacao.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.text_situacao.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.text_situacao.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.text_situacao.ForeColor = System.Drawing.Color.Red;
            this.text_situacao.Location = new System.Drawing.Point(122, 103);
            this.text_situacao.MaxLength = 100;
            this.text_situacao.Name = "text_situacao";
            this.text_situacao.ReadOnly = true;
            this.text_situacao.Size = new System.Drawing.Size(607, 21);
            this.text_situacao.TabIndex = 11;
            this.text_situacao.TabStop = false;
            // 
            // text_dtGravacao
            // 
            this.text_dtGravacao.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.text_dtGravacao.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.text_dtGravacao.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.text_dtGravacao.Location = new System.Drawing.Point(122, 83);
            this.text_dtGravacao.MaxLength = 100;
            this.text_dtGravacao.Name = "text_dtGravacao";
            this.text_dtGravacao.ReadOnly = true;
            this.text_dtGravacao.Size = new System.Drawing.Size(607, 21);
            this.text_dtGravacao.TabIndex = 9;
            this.text_dtGravacao.TabStop = false;
            // 
            // text_dtEmissao
            // 
            this.text_dtEmissao.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.text_dtEmissao.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.text_dtEmissao.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.text_dtEmissao.Location = new System.Drawing.Point(122, 63);
            this.text_dtEmissao.MaxLength = 100;
            this.text_dtEmissao.Name = "text_dtEmissao";
            this.text_dtEmissao.ReadOnly = true;
            this.text_dtEmissao.Size = new System.Drawing.Size(607, 21);
            this.text_dtEmissao.TabIndex = 7;
            this.text_dtEmissao.TabStop = false;
            // 
            // text_cnpj
            // 
            this.text_cnpj.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.text_cnpj.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.text_cnpj.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.text_cnpj.Location = new System.Drawing.Point(122, 23);
            this.text_cnpj.Mask = "99,999,999/9999,99";
            this.text_cnpj.Name = "text_cnpj";
            this.text_cnpj.PromptChar = ' ';
            this.text_cnpj.ReadOnly = true;
            this.text_cnpj.Size = new System.Drawing.Size(607, 21);
            this.text_cnpj.TabIndex = 3;
            // 
            // text_empresa
            // 
            this.text_empresa.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.text_empresa.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.text_empresa.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.text_empresa.Location = new System.Drawing.Point(122, 3);
            this.text_empresa.MaxLength = 100;
            this.text_empresa.Name = "text_empresa";
            this.text_empresa.ReadOnly = true;
            this.text_empresa.Size = new System.Drawing.Size(607, 21);
            this.text_empresa.TabIndex = 1;
            this.text_empresa.TabStop = false;
            // 
            // lbl_numero
            // 
            this.lbl_numero.AutoSize = true;
            this.lbl_numero.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_numero.ForeColor = System.Drawing.Color.Red;
            this.lbl_numero.Location = new System.Drawing.Point(325, 297);
            this.lbl_numero.Name = "lbl_numero";
            this.lbl_numero.Size = new System.Drawing.Size(0, 19);
            this.lbl_numero.TabIndex = 5;
            this.lbl_numero.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // grb_itens
            // 
            this.grb_itens.Controls.Add(this.tb_movimentoCobranca);
            this.grb_itens.Location = new System.Drawing.Point(9, 300);
            this.grb_itens.Name = "grb_itens";
            this.grb_itens.Size = new System.Drawing.Size(752, 309);
            this.grb_itens.TabIndex = 6;
            this.grb_itens.TabStop = false;
            this.grb_itens.Text = "Itens";
            // 
            // tb_movimentoCobranca
            // 
            this.tb_movimentoCobranca.Controls.Add(this.tbItens);
            this.tb_movimentoCobranca.Controls.Add(this.tb_cobranca);
            this.tb_movimentoCobranca.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tb_movimentoCobranca.Location = new System.Drawing.Point(3, 16);
            this.tb_movimentoCobranca.Name = "tb_movimentoCobranca";
            this.tb_movimentoCobranca.SelectedIndex = 0;
            this.tb_movimentoCobranca.Size = new System.Drawing.Size(746, 290);
            this.tb_movimentoCobranca.TabIndex = 0;
            // 
            // tbItens
            // 
            this.tbItens.Controls.Add(this.dg_itens);
            this.tbItens.Location = new System.Drawing.Point(4, 22);
            this.tbItens.Name = "tbItens";
            this.tbItens.Padding = new System.Windows.Forms.Padding(3);
            this.tbItens.Size = new System.Drawing.Size(738, 264);
            this.tbItens.TabIndex = 0;
            this.tbItens.Text = "Itens";
            this.tbItens.UseVisualStyleBackColor = true;
            // 
            // dg_itens
            // 
            this.dg_itens.AllowUserToAddRows = false;
            this.dg_itens.AllowUserToDeleteRows = false;
            this.dg_itens.AllowUserToOrderColumns = true;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.Gainsboro;
            this.dg_itens.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle1;
            this.dg_itens.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.DisplayedCells;
            this.dg_itens.BackgroundColor = System.Drawing.Color.White;
            this.dg_itens.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.Color.LightGray;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dg_itens.DefaultCellStyle = dataGridViewCellStyle2;
            this.dg_itens.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dg_itens.Location = new System.Drawing.Point(3, 3);
            this.dg_itens.MultiSelect = false;
            this.dg_itens.Name = "dg_itens";
            this.dg_itens.ReadOnly = true;
            this.dg_itens.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dg_itens.Size = new System.Drawing.Size(732, 258);
            this.dg_itens.TabIndex = 0;
            // 
            // tb_cobranca
            // 
            this.tb_cobranca.Controls.Add(this.dg_cobranca);
            this.tb_cobranca.Location = new System.Drawing.Point(4, 22);
            this.tb_cobranca.Name = "tb_cobranca";
            this.tb_cobranca.Padding = new System.Windows.Forms.Padding(3);
            this.tb_cobranca.Size = new System.Drawing.Size(738, 264);
            this.tb_cobranca.TabIndex = 1;
            this.tb_cobranca.Text = "Cobranças";
            this.tb_cobranca.UseVisualStyleBackColor = true;
            // 
            // dg_cobranca
            // 
            this.dg_cobranca.AllowUserToAddRows = false;
            this.dg_cobranca.AllowUserToDeleteRows = false;
            this.dg_cobranca.AllowUserToOrderColumns = true;
            this.dg_cobranca.AllowUserToResizeRows = false;
            dataGridViewCellStyle3.BackColor = System.Drawing.Color.Gainsboro;
            this.dg_cobranca.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle3;
            this.dg_cobranca.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.DisplayedCells;
            this.dg_cobranca.BackgroundColor = System.Drawing.Color.White;
            this.dg_cobranca.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle4.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle4.Font = new System.Drawing.Font("Arial", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle4.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle4.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle4.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle4.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dg_cobranca.DefaultCellStyle = dataGridViewCellStyle4;
            this.dg_cobranca.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dg_cobranca.Location = new System.Drawing.Point(3, 3);
            this.dg_cobranca.MultiSelect = false;
            this.dg_cobranca.Name = "dg_cobranca";
            this.dg_cobranca.ReadOnly = true;
            this.dg_cobranca.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dg_cobranca.Size = new System.Drawing.Size(732, 258);
            this.dg_cobranca.TabIndex = 0;
            // 
            // grb_total
            // 
            this.grb_total.Controls.Add(this.lblValorLiquido);
            this.grb_total.Controls.Add(this.lblCsll);
            this.grb_total.Controls.Add(this.lblIr);
            this.grb_total.Controls.Add(this.lblIss);
            this.grb_total.Controls.Add(this.lblAliquota);
            this.grb_total.Controls.Add(this.lblBase);
            this.grb_total.Controls.Add(this.lblCofins);
            this.grb_total.Controls.Add(this.lblPis);
            this.grb_total.Controls.Add(this.lblValorNF);
            this.grb_total.Controls.Add(this.text_valorLiquido);
            this.grb_total.Controls.Add(this.text_csll);
            this.grb_total.Controls.Add(this.text_IR);
            this.grb_total.Controls.Add(this.text_valorIss);
            this.grb_total.Controls.Add(this.text_aliquotaPercentual);
            this.grb_total.Controls.Add(this.text_baseCalculo);
            this.grb_total.Controls.Add(this.text_cofins);
            this.grb_total.Controls.Add(this.text_pis);
            this.grb_total.Controls.Add(this.text_valorTotal);
            this.grb_total.Location = new System.Drawing.Point(9, 615);
            this.grb_total.Name = "grb_total";
            this.grb_total.Size = new System.Drawing.Size(752, 131);
            this.grb_total.TabIndex = 7;
            this.grb_total.TabStop = false;
            this.grb_total.Text = "Totais";
            // 
            // lblValorLiquido
            // 
            this.lblValorLiquido.BackColor = System.Drawing.Color.LightGray;
            this.lblValorLiquido.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblValorLiquido.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblValorLiquido.Location = new System.Drawing.Point(306, 79);
            this.lblValorLiquido.Name = "lblValorLiquido";
            this.lblValorLiquido.Size = new System.Drawing.Size(119, 21);
            this.lblValorLiquido.TabIndex = 46;
            this.lblValorLiquido.TabStop = false;
            this.lblValorLiquido.Text = "Valor Líquido (R$)";
            // 
            // lblCsll
            // 
            this.lblCsll.BackColor = System.Drawing.Color.LightGray;
            this.lblCsll.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblCsll.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCsll.Location = new System.Drawing.Point(306, 59);
            this.lblCsll.Name = "lblCsll";
            this.lblCsll.Size = new System.Drawing.Size(119, 21);
            this.lblCsll.TabIndex = 45;
            this.lblCsll.TabStop = false;
            this.lblCsll.Text = "CSLL (R$)";
            // 
            // lblIr
            // 
            this.lblIr.BackColor = System.Drawing.Color.LightGray;
            this.lblIr.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblIr.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblIr.Location = new System.Drawing.Point(306, 39);
            this.lblIr.Name = "lblIr";
            this.lblIr.Size = new System.Drawing.Size(119, 21);
            this.lblIr.TabIndex = 44;
            this.lblIr.TabStop = false;
            this.lblIr.Text = "IR (R$)";
            // 
            // lblIss
            // 
            this.lblIss.BackColor = System.Drawing.Color.LightGray;
            this.lblIss.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblIss.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblIss.Location = new System.Drawing.Point(306, 19);
            this.lblIss.Name = "lblIss";
            this.lblIss.Size = new System.Drawing.Size(119, 21);
            this.lblIss.TabIndex = 43;
            this.lblIss.TabStop = false;
            this.lblIss.Text = "ISSQN (R$)";
            // 
            // lblAliquota
            // 
            this.lblAliquota.BackColor = System.Drawing.Color.LightGray;
            this.lblAliquota.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblAliquota.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblAliquota.Location = new System.Drawing.Point(7, 99);
            this.lblAliquota.Name = "lblAliquota";
            this.lblAliquota.Size = new System.Drawing.Size(119, 21);
            this.lblAliquota.TabIndex = 42;
            this.lblAliquota.TabStop = false;
            this.lblAliquota.Text = "Alíquota (%)";
            // 
            // lblBase
            // 
            this.lblBase.BackColor = System.Drawing.Color.LightGray;
            this.lblBase.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblBase.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblBase.Location = new System.Drawing.Point(7, 79);
            this.lblBase.Name = "lblBase";
            this.lblBase.Size = new System.Drawing.Size(119, 21);
            this.lblBase.TabIndex = 41;
            this.lblBase.TabStop = false;
            this.lblBase.Text = "Base Cálculo (R$)";
            // 
            // lblCofins
            // 
            this.lblCofins.BackColor = System.Drawing.Color.LightGray;
            this.lblCofins.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblCofins.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCofins.Location = new System.Drawing.Point(7, 59);
            this.lblCofins.Name = "lblCofins";
            this.lblCofins.Size = new System.Drawing.Size(119, 21);
            this.lblCofins.TabIndex = 40;
            this.lblCofins.TabStop = false;
            this.lblCofins.Text = "COFINS (R$)";
            // 
            // lblPis
            // 
            this.lblPis.BackColor = System.Drawing.Color.LightGray;
            this.lblPis.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblPis.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPis.Location = new System.Drawing.Point(7, 39);
            this.lblPis.Name = "lblPis";
            this.lblPis.Size = new System.Drawing.Size(119, 21);
            this.lblPis.TabIndex = 39;
            this.lblPis.TabStop = false;
            this.lblPis.Text = "PIS (R$)";
            // 
            // lblValorNF
            // 
            this.lblValorNF.BackColor = System.Drawing.Color.LightGray;
            this.lblValorNF.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblValorNF.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblValorNF.Location = new System.Drawing.Point(7, 19);
            this.lblValorNF.Name = "lblValorNF";
            this.lblValorNF.Size = new System.Drawing.Size(119, 21);
            this.lblValorNF.TabIndex = 38;
            this.lblValorNF.TabStop = false;
            this.lblValorNF.Text = "Valor NF (R$)";
            // 
            // text_valorLiquido
            // 
            this.text_valorLiquido.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.text_valorLiquido.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.text_valorLiquido.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.text_valorLiquido.ForeColor = System.Drawing.Color.Blue;
            this.text_valorLiquido.Location = new System.Drawing.Point(424, 79);
            this.text_valorLiquido.MaxLength = 255;
            this.text_valorLiquido.Name = "text_valorLiquido";
            this.text_valorLiquido.ReadOnly = true;
            this.text_valorLiquido.Size = new System.Drawing.Size(175, 21);
            this.text_valorLiquido.TabIndex = 37;
            this.text_valorLiquido.TabStop = false;
            // 
            // text_csll
            // 
            this.text_csll.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.text_csll.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.text_csll.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.text_csll.ForeColor = System.Drawing.Color.Blue;
            this.text_csll.Location = new System.Drawing.Point(424, 59);
            this.text_csll.MaxLength = 255;
            this.text_csll.Name = "text_csll";
            this.text_csll.ReadOnly = true;
            this.text_csll.Size = new System.Drawing.Size(175, 21);
            this.text_csll.TabIndex = 35;
            this.text_csll.TabStop = false;
            // 
            // text_IR
            // 
            this.text_IR.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.text_IR.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.text_IR.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.text_IR.ForeColor = System.Drawing.Color.Blue;
            this.text_IR.Location = new System.Drawing.Point(424, 39);
            this.text_IR.MaxLength = 255;
            this.text_IR.Name = "text_IR";
            this.text_IR.ReadOnly = true;
            this.text_IR.Size = new System.Drawing.Size(175, 21);
            this.text_IR.TabIndex = 31;
            this.text_IR.TabStop = false;
            // 
            // text_valorIss
            // 
            this.text_valorIss.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.text_valorIss.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.text_valorIss.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.text_valorIss.ForeColor = System.Drawing.Color.Blue;
            this.text_valorIss.Location = new System.Drawing.Point(424, 19);
            this.text_valorIss.MaxLength = 255;
            this.text_valorIss.Name = "text_valorIss";
            this.text_valorIss.ReadOnly = true;
            this.text_valorIss.Size = new System.Drawing.Size(175, 21);
            this.text_valorIss.TabIndex = 29;
            this.text_valorIss.TabStop = false;
            // 
            // text_aliquotaPercentual
            // 
            this.text_aliquotaPercentual.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.text_aliquotaPercentual.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.text_aliquotaPercentual.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.text_aliquotaPercentual.ForeColor = System.Drawing.Color.Blue;
            this.text_aliquotaPercentual.Location = new System.Drawing.Point(125, 99);
            this.text_aliquotaPercentual.MaxLength = 255;
            this.text_aliquotaPercentual.Name = "text_aliquotaPercentual";
            this.text_aliquotaPercentual.ReadOnly = true;
            this.text_aliquotaPercentual.Size = new System.Drawing.Size(175, 21);
            this.text_aliquotaPercentual.TabIndex = 27;
            this.text_aliquotaPercentual.TabStop = false;
            // 
            // text_baseCalculo
            // 
            this.text_baseCalculo.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.text_baseCalculo.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.text_baseCalculo.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.text_baseCalculo.ForeColor = System.Drawing.Color.Blue;
            this.text_baseCalculo.Location = new System.Drawing.Point(125, 79);
            this.text_baseCalculo.MaxLength = 255;
            this.text_baseCalculo.Name = "text_baseCalculo";
            this.text_baseCalculo.ReadOnly = true;
            this.text_baseCalculo.Size = new System.Drawing.Size(175, 21);
            this.text_baseCalculo.TabIndex = 25;
            this.text_baseCalculo.TabStop = false;
            // 
            // text_cofins
            // 
            this.text_cofins.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.text_cofins.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.text_cofins.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.text_cofins.ForeColor = System.Drawing.Color.Blue;
            this.text_cofins.Location = new System.Drawing.Point(125, 59);
            this.text_cofins.MaxLength = 255;
            this.text_cofins.Name = "text_cofins";
            this.text_cofins.ReadOnly = true;
            this.text_cofins.Size = new System.Drawing.Size(175, 21);
            this.text_cofins.TabIndex = 23;
            this.text_cofins.TabStop = false;
            // 
            // text_pis
            // 
            this.text_pis.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.text_pis.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.text_pis.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.text_pis.ForeColor = System.Drawing.Color.Blue;
            this.text_pis.Location = new System.Drawing.Point(125, 39);
            this.text_pis.MaxLength = 255;
            this.text_pis.Name = "text_pis";
            this.text_pis.ReadOnly = true;
            this.text_pis.Size = new System.Drawing.Size(175, 21);
            this.text_pis.TabIndex = 21;
            this.text_pis.TabStop = false;
            // 
            // text_valorTotal
            // 
            this.text_valorTotal.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.text_valorTotal.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.text_valorTotal.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.text_valorTotal.ForeColor = System.Drawing.Color.Blue;
            this.text_valorTotal.Location = new System.Drawing.Point(125, 19);
            this.text_valorTotal.MaxLength = 255;
            this.text_valorTotal.Name = "text_valorTotal";
            this.text_valorTotal.ReadOnly = true;
            this.text_valorTotal.Size = new System.Drawing.Size(175, 21);
            this.text_valorTotal.TabIndex = 19;
            this.text_valorTotal.TabStop = false;
            // 
            // frmNotaFiscalConsulta
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(784, 561);
            this.Name = "frmNotaFiscalConsulta";
            this.Text = "CONSULTAR NOTA EMITIDA";
            this.spForm.Panel1.ResumeLayout(false);
            this.spForm.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.spForm)).EndInit();
            this.spForm.ResumeLayout(false);
            this.pnlForm.ResumeLayout(false);
            this.pnlForm.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.banner)).EndInit();
            this.flpAcao.ResumeLayout(false);
            this.tb_cabecalho.ResumeLayout(false);
            this.tb_cliente.ResumeLayout(false);
            this.tb_cliente.PerformLayout();
            this.tb_emitente.ResumeLayout(false);
            this.tb_emitente.PerformLayout();
            this.grb_itens.ResumeLayout(false);
            this.tb_movimentoCobranca.ResumeLayout(false);
            this.tbItens.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dg_itens)).EndInit();
            this.tb_cobranca.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dg_cobranca)).EndInit();
            this.grb_total.ResumeLayout(false);
            this.grb_total.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.FlowLayoutPanel flpAcao;
        private System.Windows.Forms.Button btnSair;
        private System.Windows.Forms.TabControl tb_cabecalho;
        private System.Windows.Forms.TabPage tb_cliente;
        private System.Windows.Forms.TextBox lblEmail;
        private System.Windows.Forms.TextBox lblTelefone;
        private System.Windows.Forms.TextBox lblUf;
        private System.Windows.Forms.TextBox lblCep;
        private System.Windows.Forms.TextBox lblCidade;
        private System.Windows.Forms.TextBox lblBairro;
        private System.Windows.Forms.TextBox lblComplemento;
        private System.Windows.Forms.TextBox lblNumero;
        private System.Windows.Forms.TextBox lblEndereco;
        private System.Windows.Forms.TextBox lblInscricao;
        private System.Windows.Forms.TextBox lblCnpj;
        private System.Windows.Forms.TextBox lblCliente;
        private System.Windows.Forms.TextBox text_email;
        private System.Windows.Forms.TextBox text_telefone;
        private System.Windows.Forms.MaskedTextBox text_cep;
        private System.Windows.Forms.TextBox text_uf;
        private System.Windows.Forms.TextBox text_cidade;
        private System.Windows.Forms.TextBox text_bairro;
        private System.Windows.Forms.TextBox text_complemento;
        private System.Windows.Forms.TextBox text_numeroCliente;
        private System.Windows.Forms.TextBox text_endereco;
        private System.Windows.Forms.TextBox text_inscricao;
        private System.Windows.Forms.MaskedTextBox text_cnpjCliente;
        private System.Windows.Forms.TextBox text_cliente;
        private System.Windows.Forms.TabPage tb_emitente;
        private System.Windows.Forms.TextBox lblMotivoCancelamento;
        private System.Windows.Forms.TextBox lblSituacao;
        private System.Windows.Forms.TextBox lblDataGravacao;
        private System.Windows.Forms.TextBox lblDataEmissao;
        private System.Windows.Forms.TextBox textNumeroNota;
        private System.Windows.Forms.TextBox lblNumeroNota;
        private System.Windows.Forms.TextBox lblCnpjEmpresa;
        private System.Windows.Forms.TextBox lblEmpresa;
        private System.Windows.Forms.TextBox text_motivoCancelamento;
        private System.Windows.Forms.TextBox text_situacao;
        private System.Windows.Forms.TextBox text_dtGravacao;
        private System.Windows.Forms.TextBox text_dtEmissao;
        private System.Windows.Forms.MaskedTextBox text_cnpj;
        private System.Windows.Forms.TextBox text_empresa;
        private System.Windows.Forms.Label lbl_numero;
        private System.Windows.Forms.GroupBox grb_itens;
        private System.Windows.Forms.TabControl tb_movimentoCobranca;
        private System.Windows.Forms.TabPage tbItens;
        private System.Windows.Forms.DataGridView dg_itens;
        private System.Windows.Forms.TabPage tb_cobranca;
        private System.Windows.Forms.DataGridView dg_cobranca;
        private System.Windows.Forms.GroupBox grb_total;
        private System.Windows.Forms.TextBox lblValorLiquido;
        private System.Windows.Forms.TextBox lblCsll;
        private System.Windows.Forms.TextBox lblIr;
        private System.Windows.Forms.TextBox lblIss;
        private System.Windows.Forms.TextBox lblAliquota;
        private System.Windows.Forms.TextBox lblBase;
        private System.Windows.Forms.TextBox lblCofins;
        private System.Windows.Forms.TextBox lblPis;
        private System.Windows.Forms.TextBox lblValorNF;
        private System.Windows.Forms.TextBox text_valorLiquido;
        private System.Windows.Forms.TextBox text_csll;
        private System.Windows.Forms.TextBox text_IR;
        private System.Windows.Forms.TextBox text_valorIss;
        private System.Windows.Forms.TextBox text_aliquotaPercentual;
        private System.Windows.Forms.TextBox text_baseCalculo;
        private System.Windows.Forms.TextBox text_cofins;
        private System.Windows.Forms.TextBox text_pis;
        private System.Windows.Forms.TextBox text_valorTotal;
    }
}