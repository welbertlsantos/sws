﻿using SWS.Entidade;
using SWS.Facade;
using SWS.View.Resources;
using SWS.View.ViewHelper;
using SWS.ViewHelper;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace SWS.View
{
    public partial class frmMaterialHospitalarPrincipal : frmTemplate
    {
        private MaterialHospitalar materialHospitalar;

        public frmMaterialHospitalarPrincipal()
        {
            InitializeComponent();
            validaPermissoes();
            ComboHelper.comboSituacao(cbSituacao);
            ActiveControl = textMaterialHospitalar;
        }

        private void dgvMaterial_RowPrePaint(object sender, DataGridViewRowPrePaintEventArgs e)
        {
            DataGridView dgv = sender as DataGridView;

            if (string.Equals(dgv.Rows[e.RowIndex].Cells[3].Value, ApplicationConstants.DESATIVADO))
                dgv.Rows[e.RowIndex].DefaultCellStyle.ForeColor = Color.Red;
        }

        public void montaGridMateriaisHospitalares()
        {
            try
            {
                this.Cursor = Cursors.WaitCursor;

                EstudoFacade pcmsoFacade = EstudoFacade.getInstance();

                DataSet ds = pcmsoFacade.findMaterilHospitalarByFilter(materialHospitalar);

                dgvMaterial.DataSource = ds.Tables[0].DefaultView;

                dgvMaterial.Columns[0].HeaderText = "Id";
                dgvMaterial.Columns[0].Visible = false;

                dgvMaterial.Columns[1].HeaderText = "Descrição";
                dgvMaterial.Columns[2].HeaderText = "Unidade";
                dgvMaterial.Columns[3].HeaderText = "Situação";
                dgvMaterial.Columns[3].Visible = false;

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }

        private void validaPermissoes()
        {
            PermissionamentoFacade permissionamentoFacade = PermissionamentoFacade.getInstance();

            btnAlterar.Enabled = permissionamentoFacade.hasPermission("MATERIAL_HOSPITALAR", "ALTERAR");
            btnIncluir.Enabled = permissionamentoFacade.hasPermission("MATERIAL_HOSPITALAR", "INCLUIR");
            btnExcluir.Enabled = permissionamentoFacade.hasPermission("MATERIAL_HOSPITALAR", "EXCLUIR");
            btnDetalhar.Enabled = permissionamentoFacade.hasPermission("MATERIAL_HOSPITALAR", "DETALHAR");
            btnReativar.Enabled = permissionamentoFacade.hasPermission("MATERIAL_HOSPITALAR", "REATIVAR");
        }

        private void btnAlterar_Click(object sender, EventArgs e)
        {
            try
            {
                if (dgvMaterial.CurrentRow == null)
                    throw new Exception("Selecione uma linha.");

                EstudoFacade pcmsoFacade = EstudoFacade.getInstance();

                MaterialHospitalar materialAlterar = pcmsoFacade.findMaterialHospitalarById((long)dgvMaterial.CurrentRow.Cells[0].Value);

                if (!string.Equals(materialAlterar.Situacao, ApplicationConstants.ATIVO))
                    throw new Exception("Somente materiais hospitalares ativos podem ser alterados.");

                frmMaterialHospitalarAlterar alterarMaterial = new frmMaterialHospitalarAlterar(materialAlterar);
                alterarMaterial.ShowDialog();

                materialHospitalar = new MaterialHospitalar();
                montaGridMateriaisHospitalares();
                
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }

        private void btnIncluir_Click(object sender, EventArgs e)
        {
            try
            {

                frmMaterialHospitalarIncluir incluirMaterial = new frmMaterialHospitalarIncluir();
                incluirMaterial.ShowDialog();

                if (incluirMaterial.MateriaHospitalar != null)
                {
                    materialHospitalar = new MaterialHospitalar();
                    montaGridMateriaisHospitalares();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }

        private void btnPesquisar_Click(object sender, EventArgs e)
        {
            try
            {
                materialHospitalar = new MaterialHospitalar(null, textMaterialHospitalar.Text, string.Empty, ((SelectItem)cbSituacao.SelectedItem).Valor);
                montaGridMateriaisHospitalares();
                textMaterialHospitalar.Focus();

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }

        private void btnReativar_Click(object sender, EventArgs e)
        {
            try
            {
                if (dgvMaterial.CurrentRow == null)
                    throw new Exception("Selecione uma linha.");

                EstudoFacade pcmsoFacade = EstudoFacade.getInstance();

                MaterialHospitalar materialReativar = pcmsoFacade.findMaterialHospitalarById((long)dgvMaterial.CurrentRow.Cells[0].Value);

                if (!string.Equals(materialReativar.Situacao, ApplicationConstants.DESATIVADO))
                    throw new Exception("Somente materiais hospitalares inativos podem ser reativados.");

                if (MessageBox.Show("Deseja reativar o material hospitalar selecionado?", "Atenção", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                {
                    materialReativar.Situacao = ApplicationConstants.ATIVO;
                    pcmsoFacade.updateMaterialHospitalar(materialReativar);
                    MessageBox.Show("Material hospitalar reativado com sucesso.", "Confirmação", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    materialHospitalar = new MaterialHospitalar();
                    montaGridMateriaisHospitalares();
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }

        private void btnDetalhar_Click(object sender, EventArgs e)
        {
            try
            {
                if (dgvMaterial.CurrentRow == null)
                    throw new Exception("Selecione uma linha.");

                EstudoFacade pcmsoFacade = EstudoFacade.getInstance();

                frmMaterialHospitalarDetalhar detalharMaterial = new frmMaterialHospitalarDetalhar(pcmsoFacade.findMaterialHospitalarById((long)dgvMaterial.CurrentRow.Cells[0].Value));
                detalharMaterial.ShowDialog();

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }

        private void btnExcluir_Click(object sender, EventArgs e)
        {
            try
            {
                if (dgvMaterial.CurrentRow == null)
                    throw new Exception("Selecione uma linha.");

                EstudoFacade pcmsoFacade = EstudoFacade.getInstance();

                MaterialHospitalar materialExcluir = pcmsoFacade.findMaterialHospitalarById((long)dgvMaterial.CurrentRow.Cells[0].Value);

                if (!string.Equals(materialExcluir.Situacao, ApplicationConstants.ATIVO))
                    throw new Exception("Somente materiais hospitalares ativos podem ser excluídos.");

                if (MessageBox.Show("Deseja excluir o material hospitalar selecionado?", "Atenção", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                {
                    materialExcluir.Situacao = ApplicationConstants.DESATIVADO;
                    pcmsoFacade.updateMaterialHospitalar(materialExcluir);
                    MessageBox.Show("Material hospitalar excluído com sucesso.", "Confirmação", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    materialHospitalar = new MaterialHospitalar();
                    montaGridMateriaisHospitalares();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }

        private void btnFechar_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void dgvMaterial_CellMouseDoubleClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            try
            {

                btnAlterar.PerformClick();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }
    }
}
