﻿namespace SWS.View
{
    partial class frmClienteFuncaoSeleciona
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            this.flowLayoutPanel1 = new System.Windows.Forms.FlowLayoutPanel();
            this.btConfirmar = new System.Windows.Forms.Button();
            this.btnIncluirFuncao = new System.Windows.Forms.Button();
            this.btBuscar = new System.Windows.Forms.Button();
            this.btFechar = new System.Windows.Forms.Button();
            this.textFuncao = new System.Windows.Forms.TextBox();
            this.lblFuncaoSeparador = new System.Windows.Forms.Label();
            this.lblFuncao = new System.Windows.Forms.TextBox();
            this.textFantasia = new System.Windows.Forms.TextBox();
            this.lblCnpj = new System.Windows.Forms.TextBox();
            this.textCnpj = new System.Windows.Forms.MaskedTextBox();
            this.lblFantasia = new System.Windows.Forms.TextBox();
            this.textRazaoSocial = new System.Windows.Forms.TextBox();
            this.lblRazaoSocial = new System.Windows.Forms.TextBox();
            this.grbFuncao = new System.Windows.Forms.GroupBox();
            this.dgvFuncao = new System.Windows.Forms.DataGridView();
            ((System.ComponentModel.ISupportInitialize)(this.scForm)).BeginInit();
            this.scForm.Panel1.SuspendLayout();
            this.scForm.Panel2.SuspendLayout();
            this.scForm.SuspendLayout();
            this.pnlForm.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.banner)).BeginInit();
            this.flowLayoutPanel1.SuspendLayout();
            this.grbFuncao.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvFuncao)).BeginInit();
            this.SuspendLayout();
            // 
            // scForm
            // 
            // 
            // scForm.Panel1
            // 
            this.scForm.Panel1.Controls.Add(this.flowLayoutPanel1);
            // 
            // pnlForm
            // 
            this.pnlForm.Controls.Add(this.grbFuncao);
            this.pnlForm.Controls.Add(this.textFuncao);
            this.pnlForm.Controls.Add(this.lblFuncaoSeparador);
            this.pnlForm.Controls.Add(this.lblFuncao);
            this.pnlForm.Controls.Add(this.textFantasia);
            this.pnlForm.Controls.Add(this.lblCnpj);
            this.pnlForm.Controls.Add(this.textCnpj);
            this.pnlForm.Controls.Add(this.lblFantasia);
            this.pnlForm.Controls.Add(this.textRazaoSocial);
            this.pnlForm.Controls.Add(this.lblRazaoSocial);
            // 
            // flowLayoutPanel1
            // 
            this.flowLayoutPanel1.Controls.Add(this.btConfirmar);
            this.flowLayoutPanel1.Controls.Add(this.btnIncluirFuncao);
            this.flowLayoutPanel1.Controls.Add(this.btBuscar);
            this.flowLayoutPanel1.Controls.Add(this.btFechar);
            this.flowLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.flowLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.flowLayoutPanel1.Name = "flowLayoutPanel1";
            this.flowLayoutPanel1.Size = new System.Drawing.Size(534, 30);
            this.flowLayoutPanel1.TabIndex = 0;
            // 
            // btConfirmar
            // 
            this.btConfirmar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btConfirmar.Image = global::SWS.Properties.Resources.Gravar;
            this.btConfirmar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btConfirmar.Location = new System.Drawing.Point(3, 3);
            this.btConfirmar.Name = "btConfirmar";
            this.btConfirmar.Size = new System.Drawing.Size(75, 23);
            this.btConfirmar.TabIndex = 3;
            this.btConfirmar.Text = "&Confirmar";
            this.btConfirmar.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btConfirmar.UseVisualStyleBackColor = true;
            this.btConfirmar.Click += new System.EventHandler(this.btConfirmar_Click);
            // 
            // btnIncluirFuncao
            // 
            this.btnIncluirFuncao.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnIncluirFuncao.Image = global::SWS.Properties.Resources.incluir;
            this.btnIncluirFuncao.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnIncluirFuncao.Location = new System.Drawing.Point(84, 3);
            this.btnIncluirFuncao.Name = "btnIncluirFuncao";
            this.btnIncluirFuncao.Size = new System.Drawing.Size(75, 23);
            this.btnIncluirFuncao.TabIndex = 5;
            this.btnIncluirFuncao.Text = "&Nova";
            this.btnIncluirFuncao.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnIncluirFuncao.UseVisualStyleBackColor = true;
            this.btnIncluirFuncao.Click += new System.EventHandler(this.btnIncluirFuncao_Click);
            // 
            // btBuscar
            // 
            this.btBuscar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btBuscar.Image = global::SWS.Properties.Resources.busca;
            this.btBuscar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btBuscar.Location = new System.Drawing.Point(165, 3);
            this.btBuscar.Name = "btBuscar";
            this.btBuscar.Size = new System.Drawing.Size(75, 23);
            this.btBuscar.TabIndex = 49;
            this.btBuscar.Text = "Buscar";
            this.btBuscar.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btBuscar.UseVisualStyleBackColor = true;
            this.btBuscar.Click += new System.EventHandler(this.btBuscar_Click);
            // 
            // btFechar
            // 
            this.btFechar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btFechar.Image = global::SWS.Properties.Resources.close;
            this.btFechar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btFechar.Location = new System.Drawing.Point(246, 3);
            this.btFechar.Name = "btFechar";
            this.btFechar.Size = new System.Drawing.Size(75, 23);
            this.btFechar.TabIndex = 4;
            this.btFechar.Text = "&Fechar";
            this.btFechar.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btFechar.UseVisualStyleBackColor = true;
            this.btFechar.Click += new System.EventHandler(this.btFechar_Click);
            // 
            // textFuncao
            // 
            this.textFuncao.BackColor = System.Drawing.Color.White;
            this.textFuncao.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textFuncao.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.textFuncao.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textFuncao.ForeColor = System.Drawing.Color.Blue;
            this.textFuncao.Location = new System.Drawing.Point(121, 89);
            this.textFuncao.MaxLength = 100;
            this.textFuncao.Name = "textFuncao";
            this.textFuncao.Size = new System.Drawing.Size(369, 21);
            this.textFuncao.TabIndex = 48;
            // 
            // lblFuncaoSeparador
            // 
            this.lblFuncaoSeparador.AutoSize = true;
            this.lblFuncaoSeparador.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblFuncaoSeparador.ForeColor = System.Drawing.Color.Red;
            this.lblFuncaoSeparador.Location = new System.Drawing.Point(3, 71);
            this.lblFuncaoSeparador.Name = "lblFuncaoSeparador";
            this.lblFuncaoSeparador.Size = new System.Drawing.Size(54, 15);
            this.lblFuncaoSeparador.TabIndex = 57;
            this.lblFuncaoSeparador.Text = "Funções";
            // 
            // lblFuncao
            // 
            this.lblFuncao.BackColor = System.Drawing.Color.LightGray;
            this.lblFuncao.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblFuncao.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblFuncao.Location = new System.Drawing.Point(3, 89);
            this.lblFuncao.Name = "lblFuncao";
            this.lblFuncao.ReadOnly = true;
            this.lblFuncao.Size = new System.Drawing.Size(119, 21);
            this.lblFuncao.TabIndex = 56;
            this.lblFuncao.TabStop = false;
            this.lblFuncao.Text = "Nome da Função";
            // 
            // textFantasia
            // 
            this.textFantasia.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.textFantasia.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textFantasia.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.textFantasia.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textFantasia.ForeColor = System.Drawing.Color.Blue;
            this.textFantasia.Location = new System.Drawing.Point(124, 27);
            this.textFantasia.Name = "textFantasia";
            this.textFantasia.ReadOnly = true;
            this.textFantasia.Size = new System.Drawing.Size(366, 21);
            this.textFantasia.TabIndex = 52;
            this.textFantasia.TabStop = false;
            // 
            // lblCnpj
            // 
            this.lblCnpj.BackColor = System.Drawing.Color.LightGray;
            this.lblCnpj.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblCnpj.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCnpj.Location = new System.Drawing.Point(6, 47);
            this.lblCnpj.Name = "lblCnpj";
            this.lblCnpj.ReadOnly = true;
            this.lblCnpj.Size = new System.Drawing.Size(119, 21);
            this.lblCnpj.TabIndex = 54;
            this.lblCnpj.TabStop = false;
            this.lblCnpj.Text = "CNPJ";
            // 
            // textCnpj
            // 
            this.textCnpj.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.textCnpj.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textCnpj.ForeColor = System.Drawing.Color.Blue;
            this.textCnpj.Location = new System.Drawing.Point(124, 47);
            this.textCnpj.Mask = "99,999,999/9999-99";
            this.textCnpj.Name = "textCnpj";
            this.textCnpj.PromptChar = ' ';
            this.textCnpj.ReadOnly = true;
            this.textCnpj.Size = new System.Drawing.Size(366, 21);
            this.textCnpj.TabIndex = 55;
            this.textCnpj.TabStop = false;
            // 
            // lblFantasia
            // 
            this.lblFantasia.BackColor = System.Drawing.Color.LightGray;
            this.lblFantasia.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblFantasia.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblFantasia.Location = new System.Drawing.Point(6, 27);
            this.lblFantasia.Name = "lblFantasia";
            this.lblFantasia.ReadOnly = true;
            this.lblFantasia.Size = new System.Drawing.Size(119, 21);
            this.lblFantasia.TabIndex = 53;
            this.lblFantasia.TabStop = false;
            this.lblFantasia.Text = "Nome de Fantasia";
            // 
            // textRazaoSocial
            // 
            this.textRazaoSocial.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.textRazaoSocial.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textRazaoSocial.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.textRazaoSocial.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textRazaoSocial.ForeColor = System.Drawing.Color.Blue;
            this.textRazaoSocial.Location = new System.Drawing.Point(124, 7);
            this.textRazaoSocial.Name = "textRazaoSocial";
            this.textRazaoSocial.ReadOnly = true;
            this.textRazaoSocial.Size = new System.Drawing.Size(366, 21);
            this.textRazaoSocial.TabIndex = 50;
            this.textRazaoSocial.TabStop = false;
            // 
            // lblRazaoSocial
            // 
            this.lblRazaoSocial.BackColor = System.Drawing.Color.LightGray;
            this.lblRazaoSocial.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblRazaoSocial.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblRazaoSocial.Location = new System.Drawing.Point(6, 7);
            this.lblRazaoSocial.Name = "lblRazaoSocial";
            this.lblRazaoSocial.ReadOnly = true;
            this.lblRazaoSocial.Size = new System.Drawing.Size(119, 21);
            this.lblRazaoSocial.TabIndex = 51;
            this.lblRazaoSocial.TabStop = false;
            this.lblRazaoSocial.Text = "Razão Social";
            // 
            // grbFuncao
            // 
            this.grbFuncao.Controls.Add(this.dgvFuncao);
            this.grbFuncao.Location = new System.Drawing.Point(3, 116);
            this.grbFuncao.Name = "grbFuncao";
            this.grbFuncao.Size = new System.Drawing.Size(487, 158);
            this.grbFuncao.TabIndex = 58;
            this.grbFuncao.TabStop = false;
            this.grbFuncao.Text = "Funções";
            // 
            // dgvFuncao
            // 
            this.dgvFuncao.AllowUserToAddRows = false;
            this.dgvFuncao.AllowUserToDeleteRows = false;
            this.dgvFuncao.AllowUserToOrderColumns = true;
            this.dgvFuncao.AllowUserToResizeRows = false;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.Gainsboro;
            this.dgvFuncao.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle1;
            this.dgvFuncao.BackgroundColor = System.Drawing.Color.White;
            this.dgvFuncao.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.Color.LightGray;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvFuncao.DefaultCellStyle = dataGridViewCellStyle2;
            this.dgvFuncao.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvFuncao.Location = new System.Drawing.Point(3, 16);
            this.dgvFuncao.MultiSelect = false;
            this.dgvFuncao.Name = "dgvFuncao";
            this.dgvFuncao.ReadOnly = true;
            this.dgvFuncao.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvFuncao.Size = new System.Drawing.Size(481, 139);
            this.dgvFuncao.TabIndex = 0;
            this.dgvFuncao.CellMouseDoubleClick += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.dgvFuncao_CellMouseDoubleClick);
            // 
            // frmClienteFuncaoSeleciona
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(534, 361);
            this.Name = "frmClienteFuncaoSeleciona";
            this.Text = "SELECIONAR A FUNÇÃO";
            this.scForm.Panel1.ResumeLayout(false);
            this.scForm.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.scForm)).EndInit();
            this.scForm.ResumeLayout(false);
            this.pnlForm.ResumeLayout(false);
            this.pnlForm.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.banner)).EndInit();
            this.flowLayoutPanel1.ResumeLayout(false);
            this.grbFuncao.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvFuncao)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel1;
        protected System.Windows.Forms.Button btnIncluirFuncao;
        protected System.Windows.Forms.Button btFechar;
        protected System.Windows.Forms.Button btConfirmar;
        protected System.Windows.Forms.Button btBuscar;
        protected System.Windows.Forms.TextBox textFuncao;
        protected System.Windows.Forms.Label lblFuncaoSeparador;
        protected System.Windows.Forms.TextBox lblFuncao;
        protected System.Windows.Forms.TextBox textFantasia;
        protected System.Windows.Forms.TextBox lblCnpj;
        protected System.Windows.Forms.MaskedTextBox textCnpj;
        protected System.Windows.Forms.TextBox lblFantasia;
        protected System.Windows.Forms.TextBox textRazaoSocial;
        protected System.Windows.Forms.TextBox lblRazaoSocial;
        private System.Windows.Forms.GroupBox grbFuncao;
        protected System.Windows.Forms.DataGridView dgvFuncao;
    }
}