﻿namespace SWS.View
{
    partial class frm_CorrecaoEstudoConsultaOld
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            this.grbCorrecao = new System.Windows.Forms.GroupBox();
            this.dgCorrecao = new System.Windows.Forms.DataGridView();
            this.grbAcao = new System.Windows.Forms.GroupBox();
            this.btnFechar = new System.Windows.Forms.Button();
            this.grbCorrecao.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgCorrecao)).BeginInit();
            this.grbAcao.SuspendLayout();
            this.SuspendLayout();
            // 
            // grbCorrecao
            // 
            this.grbCorrecao.Controls.Add(this.dgCorrecao);
            this.grbCorrecao.Location = new System.Drawing.Point(13, 13);
            this.grbCorrecao.Name = "grbCorrecao";
            this.grbCorrecao.Size = new System.Drawing.Size(575, 318);
            this.grbCorrecao.TabIndex = 0;
            this.grbCorrecao.TabStop = false;
            this.grbCorrecao.Text = "Correções";
            // 
            // dgCorrecao
            // 
            this.dgCorrecao.AllowUserToAddRows = false;
            this.dgCorrecao.AllowUserToDeleteRows = false;
            this.dgCorrecao.AllowUserToOrderColumns = true;
            this.dgCorrecao.AllowUserToResizeRows = false;
            this.dgCorrecao.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.DisplayedCells;
            this.dgCorrecao.BackgroundColor = System.Drawing.Color.White;
            this.dgCorrecao.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.Silver;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgCorrecao.DefaultCellStyle = dataGridViewCellStyle1;
            this.dgCorrecao.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgCorrecao.GridColor = System.Drawing.SystemColors.ControlDarkDark;
            this.dgCorrecao.Location = new System.Drawing.Point(3, 16);
            this.dgCorrecao.Name = "dgCorrecao";
            this.dgCorrecao.ReadOnly = true;
            this.dgCorrecao.RowHeadersWidth = 30;
            this.dgCorrecao.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgCorrecao.Size = new System.Drawing.Size(569, 299);
            this.dgCorrecao.TabIndex = 0;
            this.dgCorrecao.TabStop = false;
            // 
            // grbAcao
            // 
            this.grbAcao.Controls.Add(this.btnFechar);
            this.grbAcao.Location = new System.Drawing.Point(12, 338);
            this.grbAcao.Name = "grbAcao";
            this.grbAcao.Size = new System.Drawing.Size(576, 50);
            this.grbAcao.TabIndex = 1;
            this.grbAcao.TabStop = false;
            this.grbAcao.Text = "Ações";
            // 
            // btnFechar
            // 
            this.btnFechar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnFechar.Image = global::SWS.Properties.Resources.close;
            this.btnFechar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnFechar.Location = new System.Drawing.Point(6, 19);
            this.btnFechar.Name = "btnFechar";
            this.btnFechar.Size = new System.Drawing.Size(75, 23);
            this.btnFechar.TabIndex = 0;
            this.btnFechar.Text = "&Fechar";
            this.btnFechar.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnFechar.UseVisualStyleBackColor = true;
            this.btnFechar.Click += new System.EventHandler(this.btnFechar_Click);
            // 
            // frm_CorrecaoEstudoConsulta
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(600, 400);
            this.ControlBox = false;
            this.Controls.Add(this.grbAcao);
            this.Controls.Add(this.grbCorrecao);
            this.Name = "frm_CorrecaoEstudoConsulta";
            this.Text = "CORREÇÃO REALIZADA NO ESTUDO";
            this.grbCorrecao.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgCorrecao)).EndInit();
            this.grbAcao.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox grbCorrecao;
        private System.Windows.Forms.DataGridView dgCorrecao;
        private System.Windows.Forms.GroupBox grbAcao;
        private System.Windows.Forms.Button btnFechar;
    }
}