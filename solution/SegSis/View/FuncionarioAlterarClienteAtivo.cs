﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using SegSis.Facade;
using SegSis.Entidade;
using SegSis.View.Resources;

namespace SegSis.View
{
    public partial class frm_FuncionarioAlterarClienteAtivo : BaseFormConsulta
    {
        
        private Cliente cliente = null;

        private static String Msg01 = " Selecione um linha";
        private static string Msg02 = " Atenção";

        List<ClienteFuncaoFuncionario> clienteFuncaoFuncionarios = null;
        Funcionario funcionario;

        public List<ClienteFuncaoFuncionario> getClienteFuncaoFuncionario()
        {
            return this.clienteFuncaoFuncionarios;
        }
        
        public frm_FuncionarioAlterarClienteAtivo(List<ClienteFuncaoFuncionario> clienteFuncaoFuncionarioSet, Funcionario funcionario)
        {
            InitializeComponent();
            this.clienteFuncaoFuncionarios = clienteFuncaoFuncionarioSet;
            this.funcionario = funcionario;
        }

        private void btn_fechar_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btn_novo_Click(object sender, EventArgs e)
        {
            frm_cliente_inclui formClienteIncluir = new frm_cliente_inclui();
            formClienteIncluir.ShowDialog();

            if (formClienteIncluir.getCliente() != null)
            {
                cliente = formClienteIncluir.getCliente();

                frm_FuncionarioAlterarClienteFuncao formFuncionarioClienteFuncao = new frm_FuncionarioAlterarClienteFuncao(cliente, clienteFuncaoFuncionarios.Count == 0 ? funcionario : clienteFuncaoFuncionarios.ElementAt(0).getFuncionario());
                formFuncionarioClienteFuncao.ShowDialog();

                if (formFuncionarioClienteFuncao.getClienteFuncaoFuncionario() != null)
                {
                    clienteFuncaoFuncionarios.Add(formFuncionarioClienteFuncao.getClienteFuncaoFuncionario());
                    this.Close();
                }
            }
        }

        private void btn_buscar_Click(object sender, EventArgs e)
        {
            try
            {
                this.Cursor = Cursors.WaitCursor;
                this.grd_cliente.Columns.Clear();
                montaDataGrid();
                text_razaoSocial.Focus();
            }
            catch (Exception Ex)
            {
                MessageBox.Show(Ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }

        public void montaDataGrid()
        {
            ClienteFacade clienteFacade = ClienteFacade.getInstance();

            Cliente cliente = new Cliente();
            
            cliente.setRazaoSocial(text_razaoSocial.Text);
            cliente.setFantasia(text_fantasia.Text);
            cliente.setSituacao(ApplicationConstants.ATIVO);

            DataSet ds = clienteFacade.findClienteAtivoByFilterNotInClienteFuncaoFuncionarioSet(cliente, clienteFuncaoFuncionarios);

            grd_cliente.DataSource = ds.Tables["Clientes"].DefaultView;

            grd_cliente.Columns[0].HeaderText = "ID";
            grd_cliente.Columns[1].HeaderText = "Razão Social";

            grd_cliente.Columns[0].Visible = false;

        }

        private void btn_ok_Click(object sender, EventArgs e)
        {
            try
            {
                this.Cursor = Cursors.WaitCursor;

                if (grd_cliente.CurrentRow != null)
                {
                    ClienteFacade clienteFacade = ClienteFacade.getInstance();

                    Int64 id = (Int64)grd_cliente.CurrentRow.Cells[0].Value;

                    cliente = clienteFacade.findClienteById(id);

                    frm_FuncionarioAlterarClienteFuncao formFuncionarioAlterarClienteFuncao = new frm_FuncionarioAlterarClienteFuncao(cliente, clienteFuncaoFuncionarios.Count == 0 ? funcionario : clienteFuncaoFuncionarios.ElementAt(0).getFuncionario());
                    formFuncionarioAlterarClienteFuncao.ShowDialog();

                    if (formFuncionarioAlterarClienteFuncao.getClienteFuncaoFuncionario() != null)
                    {
                        clienteFuncaoFuncionarios.Add(formFuncionarioAlterarClienteFuncao.getClienteFuncaoFuncionario());
                        this.Close();
                    }
                }
                else
                {
                    MessageBox.Show(Msg01, Msg02);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }
    }
}
