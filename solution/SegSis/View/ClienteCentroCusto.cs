﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using SWS.Entidade;

namespace SWS.View
{
    public partial class frmClienteCentroCusto : frmTemplateConsulta
    {
        CentroCusto centroCusto;

        public CentroCusto CentroCusto
        {
            get { return centroCusto; }
            set { centroCusto = value; }
        }
        
        public frmClienteCentroCusto()
        {
            InitializeComponent();
            ActiveControl = textCentroCusto;
        }

        private void btnFechar_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnGravar_Click(object sender, EventArgs e)
        {
            try
            {
                if (string.IsNullOrEmpty(textCentroCusto.Text.Trim()))
                    throw new Exception("O campo centro de custo não pode ser vazio.");

                CentroCusto = new CentroCusto(null, null, textCentroCusto.Text, string.Empty);
                this.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
    }
}
