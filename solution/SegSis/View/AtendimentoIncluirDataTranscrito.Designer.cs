﻿namespace SWS.View
{
    partial class frmAtendimentoIncluirDataTranscrito
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.flpAcao = new System.Windows.Forms.FlowLayoutPanel();
            this.btnGravar = new System.Windows.Forms.Button();
            this.btnFechar = new System.Windows.Forms.Button();
            this.lblData = new System.Windows.Forms.TextBox();
            this.dataTranscrito = new System.Windows.Forms.DateTimePicker();
            this.lblDataVencimentoTranscrito = new System.Windows.Forms.TextBox();
            this.dataVencimentoTranscrito = new System.Windows.Forms.DateTimePicker();
            ((System.ComponentModel.ISupportInitialize)(this.scForm)).BeginInit();
            this.scForm.Panel1.SuspendLayout();
            this.scForm.Panel2.SuspendLayout();
            this.scForm.SuspendLayout();
            this.pnlForm.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.banner)).BeginInit();
            this.flpAcao.SuspendLayout();
            this.SuspendLayout();
            // 
            // scForm
            // 
            // 
            // scForm.Panel1
            // 
            this.scForm.Panel1.Controls.Add(this.flpAcao);
            this.scForm.Size = new System.Drawing.Size(335, 320);
            // 
            // pnlForm
            // 
            this.pnlForm.Controls.Add(this.dataVencimentoTranscrito);
            this.pnlForm.Controls.Add(this.lblDataVencimentoTranscrito);
            this.pnlForm.Controls.Add(this.dataTranscrito);
            this.pnlForm.Controls.Add(this.lblData);
            this.pnlForm.Size = new System.Drawing.Size(335, 84);
            // 
            // banner
            // 
            this.banner.Size = new System.Drawing.Size(335, 41);
            // 
            // flpAcao
            // 
            this.flpAcao.Controls.Add(this.btnGravar);
            this.flpAcao.Controls.Add(this.btnFechar);
            this.flpAcao.Dock = System.Windows.Forms.DockStyle.Fill;
            this.flpAcao.Location = new System.Drawing.Point(0, 0);
            this.flpAcao.Name = "flpAcao";
            this.flpAcao.Size = new System.Drawing.Size(335, 30);
            this.flpAcao.TabIndex = 0;
            // 
            // btnGravar
            // 
            this.btnGravar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnGravar.Image = global::SWS.Properties.Resources.Gravar;
            this.btnGravar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnGravar.Location = new System.Drawing.Point(3, 3);
            this.btnGravar.Name = "btnGravar";
            this.btnGravar.Size = new System.Drawing.Size(75, 23);
            this.btnGravar.TabIndex = 2;
            this.btnGravar.Text = "&Gravar";
            this.btnGravar.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnGravar.UseVisualStyleBackColor = true;
            this.btnGravar.Click += new System.EventHandler(this.btnGravar_Click);
            // 
            // btnFechar
            // 
            this.btnFechar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnFechar.Image = global::SWS.Properties.Resources.close;
            this.btnFechar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnFechar.Location = new System.Drawing.Point(84, 3);
            this.btnFechar.Name = "btnFechar";
            this.btnFechar.Size = new System.Drawing.Size(75, 23);
            this.btnFechar.TabIndex = 1;
            this.btnFechar.TabStop = false;
            this.btnFechar.Text = "&Fechar";
            this.btnFechar.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnFechar.UseVisualStyleBackColor = true;
            this.btnFechar.Click += new System.EventHandler(this.btnFechar_Click);
            // 
            // lblData
            // 
            this.lblData.BackColor = System.Drawing.Color.LightGray;
            this.lblData.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblData.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblData.Location = new System.Drawing.Point(12, 14);
            this.lblData.Name = "lblData";
            this.lblData.ReadOnly = true;
            this.lblData.Size = new System.Drawing.Size(98, 21);
            this.lblData.TabIndex = 0;
            this.lblData.TabStop = false;
            this.lblData.Text = "Data do Exame";
            // 
            // dataTranscrito
            // 
            this.dataTranscrito.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dataTranscrito.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dataTranscrito.Location = new System.Drawing.Point(109, 14);
            this.dataTranscrito.Name = "dataTranscrito";
            this.dataTranscrito.Size = new System.Drawing.Size(210, 21);
            this.dataTranscrito.TabIndex = 1;
            // 
            // lblDataVencimentoTranscrito
            // 
            this.lblDataVencimentoTranscrito.BackColor = System.Drawing.Color.LightGray;
            this.lblDataVencimentoTranscrito.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblDataVencimentoTranscrito.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDataVencimentoTranscrito.Location = new System.Drawing.Point(12, 34);
            this.lblDataVencimentoTranscrito.Name = "lblDataVencimentoTranscrito";
            this.lblDataVencimentoTranscrito.ReadOnly = true;
            this.lblDataVencimentoTranscrito.Size = new System.Drawing.Size(98, 21);
            this.lblDataVencimentoTranscrito.TabIndex = 2;
            this.lblDataVencimentoTranscrito.TabStop = false;
            this.lblDataVencimentoTranscrito.Text = "Data de Vcto";
            // 
            // dataVencimentoTranscrito
            // 
            this.dataVencimentoTranscrito.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dataVencimentoTranscrito.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dataVencimentoTranscrito.Location = new System.Drawing.Point(109, 34);
            this.dataVencimentoTranscrito.Name = "dataVencimentoTranscrito";
            this.dataVencimentoTranscrito.Size = new System.Drawing.Size(210, 21);
            this.dataVencimentoTranscrito.TabIndex = 3;
            // 
            // frmAtendimentoIncluirDataTranscrito
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(346, 162);
            this.Name = "frmAtendimentoIncluirDataTranscrito";
            this.Text = "ALTERAR DATA TRANSCRITO";
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.frmAtendimentoIncluirDataTranscrito_KeyDown);
            this.scForm.Panel1.ResumeLayout(false);
            this.scForm.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.scForm)).EndInit();
            this.scForm.ResumeLayout(false);
            this.pnlForm.ResumeLayout(false);
            this.pnlForm.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.banner)).EndInit();
            this.flpAcao.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.FlowLayoutPanel flpAcao;
        private System.Windows.Forms.Button btnGravar;
        private System.Windows.Forms.Button btnFechar;
        private System.Windows.Forms.DateTimePicker dataTranscrito;
        private System.Windows.Forms.TextBox lblData;
        private System.Windows.Forms.DateTimePicker dataVencimentoTranscrito;
        private System.Windows.Forms.TextBox lblDataVencimentoTranscrito;
    }
}