﻿using SWS.Entidade;
using SWS.Facade;
using SWS.View.Resources;
using SWS.View.ViewHelper;
using SWS.ViewHelper;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace SWS.View
{
    public partial class frmCobrancaPrincipal : frmTemplate
    {
        private Cobranca cobranca;
        
        private Cliente cliente;
        
        public frmCobrancaPrincipal()
        {
            InitializeComponent();
            ActiveControl = textNumeroNota;
            ComboHelper.startSituacaoCobranca(cbSituacao);
            validaPermissoes();
            
        }
        
        private void validaPermissoes()
        {
            PermissionamentoFacade permissionamentoFacade = PermissionamentoFacade.getInstance();

            btnCancelar.Enabled = permissionamentoFacade.hasPermission("COBRANCA", "CANCELAR");
            btnBaixar.Enabled = permissionamentoFacade.hasPermission("COBRANCA", "BAIXAR");
            btnEstornar.Enabled = permissionamentoFacade.hasPermission("COBRANCA", "ESTORNAR_BAIXA");
            btnReativar.Enabled = permissionamentoFacade.hasPermission("COBRANCA", "REATIVAR");
            btnAlterarFormaPgto.Enabled = permissionamentoFacade.hasPermission("COBRANCA", "ALTERAR_FORMA_PAGAMENTO");
            btnProrrogar.Enabled = permissionamentoFacade.hasPermission("COBRANCA", "PRORROGAR");
        }

        private void btnCliente_Click(object sender, EventArgs e)
        {
            try
            {
                frmClienteSelecionar formCliente = new frmClienteSelecionar(null, null, false, null, null, null, null, null, null);
                formCliente.ShowDialog();

                if (formCliente.Cliente != null)
                {
                    cliente = formCliente.Cliente;
                    textCliente.Text = cliente.RazaoSocial;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }

        private void btnExcluirCliente_Click(object sender, EventArgs e)
        {
            try
            {
                if (cliente == null)
                    throw new Exception("Você deve selecionar o cliente primeiro.");

                cliente = null;
                textCliente.Text = string.Empty;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }

        private void btnPesquisar_Click(object sender, EventArgs e)
        {
            try
            {
                if (validaDadosTela())
                {
                    /* verificando se o usuário não selecionou nenhuma opção de filtro */

                    if (cliente == null && string.IsNullOrEmpty(textNumeroNota.Text) && cbSituacao.SelectedIndex == 0 && !dataEmissaoInicial.Checked && !dataEmissaoFinal.Checked && !dataVencimentoInicial.Checked && !dataVencimentoFinal.Checked && !dataBaixaInicial.Checked && !dataBaixaFinal.Checked && !dataCancelamentoInicial.Checked && !dataCancelamentoFinal.Checked)
                    {
                        if (MessageBox.Show("Você não selecionou nenhuma opçao de filtro. Essa consulta poderá demorar um pouco. Deseja continuar?", "Atenção", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                        {
                            cobranca = new Cobranca(null, null, null, null, null, null, null, null, null, 0, DateTime.Now, DateTime.Now, null, (((SelectItem)cbSituacao.SelectedItem).Valor), 0, null, null, string.Empty, null, string.Empty, string.Empty, null, string.Empty, null, null, string.Empty, null, null, string.Empty);
                            montaDataGrid();
                            textNumeroNota.Focus();
                        }
                    }
                    else
                    {
                        cobranca = new Cobranca(null, null, null, null, null, null, null, null, null, 0, DateTime.Now, DateTime.Now, null, (((SelectItem)cbSituacao.SelectedItem).Valor), 0, null, null, string.Empty, null, string.Empty, string.Empty, null, string.Empty, null, null, string.Empty, null, null, string.Empty);
                        montaDataGrid();
                        textNumeroNota.Focus();
                        
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }

        private bool validaDadosTela()
        {
            bool result = true;
            
            try
            {
                /* data de emissão */
                if (dataEmissaoInicial.Checked && !dataEmissaoFinal.Checked)
                    throw new Exception("Você deve selecionar a data de emissão final para a pesquisa");

                if (dataEmissaoFinal.Checked && !dataEmissaoInicial.Checked)
                    throw new Exception("Você deve selecionar a data de emissão inicial para a pesquisa.");

                if (Convert.ToDateTime(dataEmissaoInicial.Text) > Convert.ToDateTime(dataEmissaoFinal.Text))
                    throw new Exception("A data de emissão inicial não pode ser maior que a data de emissão final.");

                /*data de vencimento */
                if (dataVencimentoInicial.Checked && !dataVencimentoFinal.Checked)
                    throw new Exception("Você deve selecionar a data de vencimento final para a pesquisa.");

                if (dataVencimentoFinal.Checked && !dataVencimentoInicial.Checked)
                    throw new Exception("Você deve selecionar a data de vencimento inicial para a pesquisa.");

                if (Convert.ToDateTime(dataVencimentoInicial.Text) > Convert.ToDateTime(dataVencimentoFinal.Text))
                    throw new Exception("A data de vencimento inicial não pode ser maior que a data de vencimento final.");

                /*data de baixa */
                if (dataBaixaInicial.Checked && !dataBaixaFinal.Checked)
                    throw new Exception("Você deve selecionar a data de pagamento final para a pesquisa.");

                if (dataBaixaFinal.Checked && !dataBaixaInicial.Checked)
                    throw new Exception("Você deve selecionar a data de pagamento inicial para a pesquisa.");

                if (Convert.ToDateTime(dataBaixaInicial.Text) > Convert.ToDateTime(dataBaixaFinal.Text))
                    throw new Exception("A data de pagamento inicial não pode ser maior que a data de pagamento final.");

                /*data de baixa */
                if (dataBaixaInicial.Checked && !dataBaixaFinal.Checked)
                    throw new Exception("Você deve selecionar a data de pagamento final para a pesquisa.");

                if (dataBaixaFinal.Checked && !dataBaixaInicial.Checked)
                    throw new Exception("Você deve selecionar a data de pagamento inicial para a pesquisa.");

                if (Convert.ToDateTime(dataBaixaInicial.Text) > Convert.ToDateTime(dataBaixaFinal.Text))
                    throw new Exception("A data de pagamento inicial não pode ser maior que a data de pagamento final.");

                /*data de cancelamento */
                if (dataCancelamentoInicial.Checked && !dataCancelamentoFinal.Checked)
                    throw new Exception("Você deve selecionar a data de cancelamento final para a pesquisa.");

                if (dataCancelamentoFinal.Checked && !dataCancelamentoInicial.Checked)
                    throw new Exception("Você deve selecionar a data de cancelamento inicial para a pesquisa.");

                if (Convert.ToDateTime(dataCancelamentoInicial.Text) > Convert.ToDateTime(dataCancelamentoFinal.Text))
                    throw new Exception("A data de cancelamento inicial não pode ser maior que a data de cancelamento final.");

            }
            catch (Exception ex)
            {
                result = false;
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            
            return result;
        }

        private void btnLimpar_Click(object sender, EventArgs e)
        {
            try
            {
                textNumeroNota.Text = string.Empty;
                ComboHelper.startSituacaoCobranca(cbSituacao);
                cliente = null;
                textCliente.Text = string.Empty;
                dataEmissaoInicial.Checked = false;
                dataEmissaoFinal.Checked = false;
                dataVencimentoInicial.Checked = false;
                dataVencimentoFinal.Checked = false;
                dataBaixaInicial.Checked = false;
                dataBaixaFinal.Checked = false;
                dataCancelamentoInicial.Checked = false;
                dataCancelamentoFinal.Checked = false;
                dgvCobranca.Columns.Clear();
                textQuantidadeCobranca.Text = string.Empty;
                textValorTotal.Text = string.Empty;
                textNumeroNota.Focus();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }

        private void btnImprimir_Click(object sender, EventArgs e)
        {
            try
            {
                if (dgvCobranca.CurrentRow == null)
                    throw new Exception("Selecione uma cobrança para imprimir.");

                FinanceiroFacade financeiroFacade = FinanceiroFacade.getInstance();

                Cobranca cobrancaImprimir = financeiroFacade.findCobrancaById((long)dgvCobranca.CurrentRow.Cells[0].Value);

                if (!string.Equals(cobrancaImprimir.Forma.Descricao, ApplicationConstants.BOLETO))
                    throw new Exception("Só é possível imprimir cobranças com forma de pagamento BOLETO.");

                if (!string.Equals(cobrancaImprimir.Situacao, ApplicationConstants.ABERTA))
                    throw new Exception("Somente é possível imprimir boleto para cobranças em aberto. ");

                frmBoletoImprimir formBoletoImprimir = new frmBoletoImprimir(cobrancaImprimir);
                formBoletoImprimir.ShowDialog();

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }

        private void btnDesdobrar_Click(object sender, EventArgs e)
        {
            try
            {
                if (dgvCobranca.CurrentRow == null)
                    throw new Exception("Seleciona uma cobrança para desdobrar.");

                FinanceiroFacade financeiroFacade = FinanceiroFacade.getInstance();
                Cobranca cobrancaDesdobrar = financeiroFacade.findCobrancaById((long)dgvCobranca.CurrentRow.Cells[0].Value);

                if (!string.Equals(cobrancaDesdobrar.Situacao, ApplicationConstants.ABERTA))
                    throw new Exception("Somente é possível desdobrar uma cobrança em aberto.");

                frmCobrancaDesdobrar desdobrarCobranca = new frmCobrancaDesdobrar(cobrancaDesdobrar);
                desdobrarCobranca.Show();
                btnPesquisar.PerformClick();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btnProrrogar_Click(object sender, EventArgs e)
        {
            try
            {
                if (dgvCobranca.CurrentRow == null)
                    throw new Exception("Seleciona uma cobrança para prorrogar.");

                FinanceiroFacade financeiroFacade = FinanceiroFacade.getInstance();
                Cobranca cobrancaProrrogar = financeiroFacade.findCobrancaById((long)dgvCobranca.CurrentRow.Cells[0].Value);

                if (!string.Equals(cobrancaProrrogar.Situacao, ApplicationConstants.ABERTA))
                    throw new Exception("Somente é possível prorrogar uma cobrança em aberto.");

                frmCobrancaProrrogar prorrogarCobranca = new frmCobrancaProrrogar(cobrancaProrrogar);
                prorrogarCobranca.ShowDialog();
                montaDataGrid();

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }

        private void btnAlterarFormaPgto_Click(object sender, EventArgs e)
        {
            try
            {
                if (dgvCobranca.CurrentRow == null)
                    throw new Exception("Seleciona uma cobrança para alterar.");

                FinanceiroFacade financeiroFacade = FinanceiroFacade.getInstance();

                Cobranca cobrancaAlterarFormaPagamento = financeiroFacade.findCobrancaById((long)dgvCobranca.CurrentRow.Cells[0].Value);

                if (!string.Equals(cobrancaAlterarFormaPagamento.Situacao, ApplicationConstants.ABERTA))
                    throw new Exception("Somente cobranças em aberto podem ser alteradas.");

                frmCobrancaAlterarPagamento alterarForma = new frmCobrancaAlterarPagamento(cobrancaAlterarFormaPagamento);
                alterarForma.ShowDialog();
                montaDataGrid();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }

        private void btnReativar_Click(object sender, EventArgs e)
        {
            try
            {
                if (dgvCobranca.CurrentRow == null)
                    throw new Exception("Selecione uma cobrança para reativar ");

                FinanceiroFacade financeiroFacade = FinanceiroFacade.getInstance();

                Cobranca cobrancaReativar = financeiroFacade.findCobrancaById((long)dgvCobranca.CurrentRow.Cells[0].Value);

                if (!string.Equals(cobrancaReativar.Situacao, ApplicationConstants.CANCELADA))
                    throw new Exception("Somente cobranças canceladas podem ser reativadas.");

                if (string.Equals(cobrancaReativar.Nota.Situacao, ApplicationConstants.CANCELADA))
                    throw new Exception("Não é possível reativar a Cobrança, pois a Nota Fiscal está cancelada.");

                if (MessageBox.Show("Deseja reativar a cobrança selecionada?", "Atenção", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                {
                    cobrancaReativar.Situacao = ApplicationConstants.ABERTA;
                    financeiroFacade.reativarCobranca(cobrancaReativar);
                    MessageBox.Show("Cobrança reativada com sucesso.", "Confirmação", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    montaDataGrid();
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }

        private void btnCancelar_Click(object sender, EventArgs e)
        {
            try
            {
                if (dgvCobranca.CurrentRow == null)
                    throw new Exception("Selecione uma cobrança.");

                FinanceiroFacade financeiroFacade = FinanceiroFacade.getInstance();

                Cobranca cobrancaCancelar = financeiroFacade.findCobrancaById((long)dgvCobranca.CurrentRow.Cells[0].Value);

                if (!string.Equals(cobrancaCancelar.Situacao, ApplicationConstants.ABERTA))
                    throw new Exception("Somente é possível cancelar cobranças em aberto.");

                if (MessageBox.Show("Deseja cancelar a cobrança selecionada?", "Atenção", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                {
                    frmJustificativa justivicativaCancelamento = new frmJustificativa("MOTIVO DE CANCELAMENTO DA COBRANÇA");
                    justivicativaCancelamento.ShowDialog();

                    if (!string.IsNullOrEmpty(justivicativaCancelamento.Justificativa))
                    {
                        cobrancaCancelar.Situacao = ApplicationConstants.CANCELADA;
                        cobrancaCancelar.ObservacaoCancelamento = justivicativaCancelamento.Justificativa;
                        cobrancaCancelar.UsuarioCancela = PermissionamentoFacade.usuarioAutenticado;

                        financeiroFacade.cancelarCobranca(cobrancaCancelar);
                        MessageBox.Show("Cobrança cancelada com sucesso.", "Confirmação", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        montaDataGrid();
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            
        }

        private void btnEstornar_Click(object sender, EventArgs e)
        {
            try
            {
                if (dgvCobranca.CurrentRow == null)
                    throw new Exception("Selecione uma cobrança para estornar.");

                FinanceiroFacade financeiroFacade = FinanceiroFacade.getInstance();

                Cobranca cobrancaEstornar = financeiroFacade.findCobrancaById((long)dgvCobranca.CurrentRow.Cells[0].Value);

                if (!string.Equals(cobrancaEstornar.Situacao, ApplicationConstants.BAIXADO))
                    throw new Exception("Somente cobranças baixadas podem ser estornadas.");

                if (MessageBox.Show("Deseja estonar a baixa na cobrança selecionada?", "Atenção", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                {
                    frmJustificativa justificativaEstorno = new frmJustificativa("INFORME A JUSTIFICATIVA DO ESTORNO");
                    justificativaEstorno.ShowDialog();

                    if (!string.IsNullOrEmpty(justificativaEstorno.Justificativa))
                    {
                        cobrancaEstornar.Situacao = ApplicationConstants.ABERTA;
                        cobrancaEstornar.ObservacaoEstorno = justificativaEstorno.Justificativa;
                        cobrancaEstornar.UsuarioEstorna = PermissionamentoFacade.usuarioAutenticado;

                        financeiroFacade.estornarBaixaCobranca(cobrancaEstornar);
                        MessageBox.Show("Cobrança estornada com sucesso.", "Confirmação", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        montaDataGrid();
                    }
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }

        private void btnBaixar_Click(object sender, EventArgs e)
        {
            try
            {
                if (dgvCobranca.CurrentRow == null)
                    throw new Exception("Selecione uma cobrança para baixar.");

                FinanceiroFacade financeiroFacade = FinanceiroFacade.getInstance();
                Cobranca cobrancaBaixar = financeiroFacade.findCobrancaById((long)dgvCobranca.CurrentRow.Cells[0].Value);

                if (!string.Equals(cobrancaBaixar.Situacao, ApplicationConstants.ATIVO))
                    throw new Exception("Somente cobranças em aberto podem ser baixadas.");

                frmCobrancaBaixar baixarCobranca = new frmCobrancaBaixar(cobrancaBaixar);
                baixarCobranca.ShowDialog();
                montaDataGrid();

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }

        private void btnFechar_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void montaDataGrid()
        {
            try
            {

                this.Cursor = Cursors.WaitCursor;
                this.dgvCobranca.Columns.Clear();
                textQuantidadeCobranca.Text = string.Empty;
                textValorTotal.Text = string.Empty;

                FinanceiroFacade financeitoFacade = FinanceiroFacade.getInstance();

                DataSet ds = financeitoFacade.findCobrancaByFilter(cobranca, cliente, string.IsNullOrEmpty(textNumeroNota.Text) ? null : new Nfe(null, null, null, Convert.ToInt64(textNumeroNota.Text), 0, DateTime.Now, null, null, 0, 0, null, null, null, null, DateTime.Now, null, string.Empty, null, string.Empty, null, null, null), dataEmissaoInicial.Checked ? Convert.ToDateTime(dataEmissaoInicial.Text) : (DateTime?)null, dataEmissaoFinal.Checked ? Convert.ToDateTime(dataEmissaoFinal.Text) : (DateTime?)null, dataVencimentoInicial.Checked ? Convert.ToDateTime(dataVencimentoInicial.Text) : (DateTime?)null, dataVencimentoFinal.Checked ? Convert.ToDateTime(dataVencimentoFinal.Text) : (DateTime?)null, dataBaixaInicial.Checked ? Convert.ToDateTime(dataBaixaInicial.Text) : (DateTime?)null, dataBaixaFinal.Checked ? Convert.ToDateTime(dataBaixaFinal.Text) : (DateTime?)null, dataCancelamentoInicial.Checked ? Convert.ToDateTime(dataCancelamentoInicial.Text) : (DateTime?)null, dataCancelamentoFinal.Checked ? Convert.ToDateTime(dataCancelamentoFinal.Text) : (DateTime?)null);

                dgvCobranca.DataSource = ds.Tables["Cobrancas"].DefaultView;

                dgvCobranca.Columns[0].HeaderText = "ID";
                dgvCobranca.Columns[0].Visible = false;

                dgvCobranca.Columns[1].HeaderText = "Número Nota Fiscal";
                dgvCobranca.Columns[2].HeaderText = "Forma de Pgto";
                dgvCobranca.Columns[3].HeaderText = "Parcela";
                dgvCobranca.Columns[4].HeaderText = "Situação";
                dgvCobranca.Columns[5].HeaderText = "Valor Alterado";
                dgvCobranca.Columns[5].HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleRight;
                dgvCobranca.Columns[5].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
                dgvCobranca.Columns[5].DefaultCellStyle.BackColor = Color.LightYellow;

                dgvCobranca.Columns[6].HeaderText = "Data Emissão";
                dgvCobranca.Columns[7].HeaderText = "Data Vencimento";
                dgvCobranca.Columns[8].HeaderText = "Data Pagamento";
                dgvCobranca.Columns[9].HeaderText = "Documento";
                dgvCobranca.Columns[10].HeaderText = "Banco";
                dgvCobranca.Columns[11].HeaderText = "Agencia";
                dgvCobranca.Columns[12].HeaderText = "DV Agência";
                dgvCobranca.Columns[13].HeaderText = "Conta";
                dgvCobranca.Columns[14].HeaderText = "DV Conta";
                dgvCobranca.Columns[15].HeaderText = "Observação";
                dgvCobranca.Columns[16].HeaderText = "Razão Social";
                dgvCobranca.Columns[17].HeaderText = "Nosso número";

                dgvCobranca.Columns[18].HeaderText = "Valor Original";
                dgvCobranca.Columns[18].HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleRight;
                dgvCobranca.Columns[18].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
                dgvCobranca.Columns[18].DefaultCellStyle.BackColor = Color.LightYellow;
                dgvCobranca.Columns[18].DisplayIndex = 7;

                dgvCobranca.Columns[19].HeaderText = "Valor Baixado";
                dgvCobranca.Columns[19].HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleRight;
                dgvCobranca.Columns[19].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
                dgvCobranca.Columns[19].DefaultCellStyle.BackColor = Color.LightYellow;
                dgvCobranca.Columns[19].DisplayIndex = 8;
                
                /* preenchendo campo total */

                textQuantidadeCobranca.Text = ds.Tables["Totais"].Rows[0]["quantidade"].ToString();
                textValorTotal.Text = ValidaCampoHelper.FormataValorMonetario(ds.Tables["Totais"].Rows[0]["valor"].ToString());


            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }

        private void textNumeroNota_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = ValidaCampoHelper.FormataCampoNumerico(sender, e);
        }

        private void dgvCobranca_RowPrePaint(object sender, DataGridViewRowPrePaintEventArgs e)
        {
            DataGridView dgv = sender as DataGridView;

            if (string.Equals(dgv.Rows[e.RowIndex].Cells[4].Value, ApplicationConstants.DESDOBRADA))
                dgv.Rows[e.RowIndex].DefaultCellStyle.ForeColor = Color.Blue;
            else if ((string.Equals(dgv.Rows[e.RowIndex].Cells[4].Value, ApplicationConstants.BAIXADO)))
                dgv.Rows[e.RowIndex].DefaultCellStyle.ForeColor = Color.DarkGreen;
            else if ((string.Equals(dgv.Rows[e.RowIndex].Cells[4].Value, ApplicationConstants.CANCELADA)))
            {
                dgv.Rows[e.RowIndex].DefaultCellStyle.ForeColor = Color.Red;
            }
        }
       
    }
}
