﻿using SWS.Entidade;
using SWS.Facade;
using SWS.View.ViewHelper;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace SWS.View
{
    public partial class frmCobrancaDesdobrar : frmTemplateConsulta
    {
        private Cobranca cobranca;

        private List<Cobranca> cobrancas = new List<Cobranca>();

        public frmCobrancaDesdobrar(Cobranca cobranca)
        {
            InitializeComponent();
            this.cobranca = cobranca;

            textNumeroNota.Text = cobranca.Nota.NumeroNfe.ToString();
            textDocumento.Text = cobranca.NumeroDocumento;
            textParcela.Text = cobranca.NumeroParcela.ToString();
            textFormaPagamento.Text = cobranca.Forma.Descricao;
            textCliente.Text = cobranca.Nota.Cliente.RazaoSocial;
            textDataEmissao.Text = cobranca.DataEmissao.ToShortDateString();
            textDataVencimento.Text = cobranca.DataVencimento.ToShortDateString();
            textValorCobranca.Text = ValidaCampoHelper.FormataValorMonetario(cobranca.ValorAjustado.ToString());
            ActiveControl = btnIncluirCobranca;
        }

        private void montaDataGridCobranca()
        {
            try
            {
                this.Cursor = Cursors.WaitCursor;

                dgvCobranca.Columns.Clear();

                dgvCobranca.ColumnCount = 19;

                dgvCobranca.Columns[0].HeaderText = "Numero do Documento";
                dgvCobranca.Columns[1].HeaderText = "Prestação";
                dgvCobranca.Columns[1].Visible = false;
                dgvCobranca.Columns[2].HeaderText = "Data de Emissão";
                dgvCobranca.Columns[2].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
                dgvCobranca.Columns[2].HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleCenter;
                dgvCobranca.Columns[3].HeaderText = "Data de Vencimento";
                dgvCobranca.Columns[3].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
                dgvCobranca.Columns[3].HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleCenter;
                dgvCobranca.Columns[4].HeaderText = "Valor Nominal";
                dgvCobranca.Columns[4].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
                dgvCobranca.Columns[4].HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleRight;
                dgvCobranca.Columns[4].DefaultCellStyle.BackColor = Color.LightYellow;
                dgvCobranca.Columns[5].HeaderText = "Data de Pagamento";
                dgvCobranca.Columns[5].Visible = false;
                dgvCobranca.Columns[6].HeaderText = "Data da Baixa";
                dgvCobranca.Columns[6].Visible = false;
                dgvCobranca.Columns[7].HeaderText = "Observação de baixa";
                dgvCobranca.Columns[7].Visible = false;
                dgvCobranca.Columns[8].HeaderText = "Forma de Pagamento";
                dgvCobranca.Columns[9].HeaderText = "Banco";
                dgvCobranca.Columns[9].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
                dgvCobranca.Columns[9].HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleCenter;
                dgvCobranca.Columns[10].HeaderText = "Agência";
                dgvCobranca.Columns[11].HeaderText = "Conta Corrente";
                dgvCobranca.Columns[12].HeaderText = "Nosso Número";
                dgvCobranca.Columns[13].HeaderText = "Data Estorno";
                dgvCobranca.Columns[13].Visible = false;
                dgvCobranca.Columns[14].HeaderText = "Observaçao de estorno";
                dgvCobranca.Columns[14].Visible = false;
                dgvCobranca.Columns[15].HeaderText = "Data de Alteração";
                dgvCobranca.Columns[15].Visible = false;
                dgvCobranca.Columns[16].HeaderText = "Observação cancelamento";
                dgvCobranca.Columns[16].Visible = false;
                dgvCobranca.Columns[17].HeaderText = "Situação";
                dgvCobranca.Columns[17].Visible = false;
                dgvCobranca.Columns[18].HeaderText = "IdTemporario";
                dgvCobranca.Columns[18].Visible = false;

                /* fazendo preenchimento da grid com a colecao de cobrancas presente na nota fiscal */

                cobrancas.ForEach(delegate(Cobranca cobranca)
                {
                    dgvCobranca.Rows.Add(cobranca.NumeroDocumento, cobranca.NumeroParcela, Convert.ToDateTime(cobranca.DataEmissao).ToShortDateString(), Convert.ToDateTime(cobranca.DataVencimento).ToShortDateString(), ValidaCampoHelper.FormataValorMonetario(cobranca.Valor.ToString()), cobranca.DataPagamento == null ? String.Empty : Convert.ToDateTime(cobranca.DataPagamento).ToShortDateString(), cobranca.DataBaixa == null ? String.Empty : Convert.ToDateTime(cobranca.DataBaixa).ToShortDateString(), cobranca.ObservacaoBaixa, cobranca.Forma.Descricao, cobranca.Conta == null ? String.Empty : cobranca.Conta.Banco.Nome, cobranca.Conta == null ? String.Empty : cobranca.Conta.AgenciaNumero + "-" + cobranca.Conta.AgenciaDigito, cobranca.Conta == null ? String.Empty : cobranca.Conta.ContaNumero + "-" + cobranca.Conta.AgenciaDigito, cobranca.Conta == null ? null : cobranca.Conta.ProximoNumero, cobranca.DataEstorno == null ? String.Empty : Convert.ToDateTime(cobranca.DataEstorno).ToShortDateString(), cobranca.ObservacaoEstorno, cobranca.DataAlteracao == null ? String.Empty : Convert.ToDateTime(cobranca.DataAlteracao).ToShortDateString(), cobranca.ObservacaoCancelamento, cobranca.Situacao, cobranca.Id);


                });
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }

        private void btn_novaCobranca_Click(object sender, EventArgs e)
        {
            try
            {
                frmCobrancaDesdobrarNova incluirCobranca = new frmCobrancaDesdobrarNova(cobranca);
                incluirCobranca.ShowDialog();

                if (incluirCobranca.Cobranca != null)
                {
                    Cobranca cobrancaIncluir = incluirCobranca.Cobranca;
                    cobrancaIncluir.Id = Convert.ToInt64(cobrancas.Count + 1);
                    cobrancas.Add(cobrancaIncluir);
                    montaDataGridCobranca();
                    atualizaTotal();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }

        private void atualizaTotal()
        {
            /* atuavlizando valor de cobrancas desdobradas */
            textValorTotalDesdobrada.Text = string.Empty;
            textSaldo.Text = string.Empty;

            decimal? totalDesdobrado = cobrancas.Sum(x => x.Valor);
            textValorTotalDesdobrada.Text = ValidaCampoHelper.FormataValorMonetario(totalDesdobrado.ToString());
            textSaldo.Text = Convert.ToString(cobranca.ValorAjustado - totalDesdobrado);

        }

        private void dgvCobranca_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.KeyCode == Keys.Delete)
                {
                    if (dgvCobranca.CurrentCell == null)
                        throw new Exception("Selecione a cobrança que deseja excluir.");

                    cobrancas.RemoveAll(x => x.Id == (long)dgvCobranca.CurrentRow.Cells[18].Value);
                    montaDataGridCobranca();
                    atualizaTotal();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btnGravar_Click(object sender, EventArgs e)
        {
            try
            {
                if (string.IsNullOrEmpty(textSaldo.Text) || Convert.ToDecimal(textSaldo.Text) != 0)
                    throw new Exception("Valor desdobrando não é igual ao valor da cobrança original.");

                FinanceiroFacade financeiroFacade = FinanceiroFacade.getInstance();
                financeiroFacade.desdobrarCobranca(cobranca, cobrancas);
                MessageBox.Show("Cobrança(s) desdobrada(s) com sucesso.", "Confirmação", MessageBoxButtons.OK, MessageBoxIcon.Information);
                this.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }

        private void btnFechar_Click(object sender, EventArgs e)
        {
            this.Close();
        }

    }
}
