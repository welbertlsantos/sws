﻿namespace SWS.View
{
    partial class frmNotaSelecionar
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            this.flpAcao = new System.Windows.Forms.FlowLayoutPanel();
            this.btnGravarNota = new System.Windows.Forms.Button();
            this.btnNotaIncluir = new System.Windows.Forms.Button();
            this.btnFechar = new System.Windows.Forms.Button();
            this.grbNota = new System.Windows.Forms.GroupBox();
            this.dgvNota = new System.Windows.Forms.DataGridView();
            ((System.ComponentModel.ISupportInitialize)(this.scForm)).BeginInit();
            this.scForm.Panel1.SuspendLayout();
            this.scForm.Panel2.SuspendLayout();
            this.scForm.SuspendLayout();
            this.pnlForm.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.banner)).BeginInit();
            this.flpAcao.SuspendLayout();
            this.grbNota.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvNota)).BeginInit();
            this.SuspendLayout();
            // 
            // scForm
            // 
            // 
            // scForm.Panel1
            // 
            this.scForm.Panel1.Controls.Add(this.flpAcao);
            // 
            // pnlForm
            // 
            this.pnlForm.Controls.Add(this.grbNota);
            // 
            // flpAcao
            // 
            this.flpAcao.Controls.Add(this.btnGravarNota);
            this.flpAcao.Controls.Add(this.btnNotaIncluir);
            this.flpAcao.Controls.Add(this.btnFechar);
            this.flpAcao.Dock = System.Windows.Forms.DockStyle.Fill;
            this.flpAcao.Location = new System.Drawing.Point(0, 0);
            this.flpAcao.Name = "flpAcao";
            this.flpAcao.Size = new System.Drawing.Size(534, 30);
            this.flpAcao.TabIndex = 0;
            // 
            // btnGravarNota
            // 
            this.btnGravarNota.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnGravarNota.Image = global::SWS.Properties.Resources.Gravar;
            this.btnGravarNota.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnGravarNota.Location = new System.Drawing.Point(3, 3);
            this.btnGravarNota.Name = "btnGravarNota";
            this.btnGravarNota.Size = new System.Drawing.Size(75, 23);
            this.btnGravarNota.TabIndex = 15;
            this.btnGravarNota.TabStop = false;
            this.btnGravarNota.Text = "&Gravar";
            this.btnGravarNota.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnGravarNota.UseVisualStyleBackColor = true;
            this.btnGravarNota.Click += new System.EventHandler(this.btnGravarNota_Click);
            // 
            // btnNotaIncluir
            // 
            this.btnNotaIncluir.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnNotaIncluir.Image = global::SWS.Properties.Resources.icone_mais;
            this.btnNotaIncluir.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnNotaIncluir.Location = new System.Drawing.Point(84, 3);
            this.btnNotaIncluir.Name = "btnNotaIncluir";
            this.btnNotaIncluir.Size = new System.Drawing.Size(75, 23);
            this.btnNotaIncluir.TabIndex = 16;
            this.btnNotaIncluir.TabStop = false;
            this.btnNotaIncluir.Text = "&Nova";
            this.btnNotaIncluir.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnNotaIncluir.UseVisualStyleBackColor = true;
            this.btnNotaIncluir.Click += new System.EventHandler(this.btnNotaIncluir_Click);
            // 
            // btnFechar
            // 
            this.btnFechar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnFechar.Image = global::SWS.Properties.Resources.close;
            this.btnFechar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnFechar.Location = new System.Drawing.Point(165, 3);
            this.btnFechar.Name = "btnFechar";
            this.btnFechar.Size = new System.Drawing.Size(75, 23);
            this.btnFechar.TabIndex = 14;
            this.btnFechar.TabStop = false;
            this.btnFechar.Text = "&Fechar";
            this.btnFechar.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnFechar.UseVisualStyleBackColor = true;
            this.btnFechar.Click += new System.EventHandler(this.btnFechar_Click);
            // 
            // grbNota
            // 
            this.grbNota.Controls.Add(this.dgvNota);
            this.grbNota.Dock = System.Windows.Forms.DockStyle.Fill;
            this.grbNota.Location = new System.Drawing.Point(0, 0);
            this.grbNota.Name = "grbNota";
            this.grbNota.Size = new System.Drawing.Size(514, 300);
            this.grbNota.TabIndex = 0;
            this.grbNota.TabStop = false;
            this.grbNota.Text = "Notas";
            // 
            // dgvNota
            // 
            this.dgvNota.AllowUserToAddRows = false;
            this.dgvNota.AllowUserToDeleteRows = false;
            this.dgvNota.AllowUserToOrderColumns = true;
            this.dgvNota.AllowUserToResizeRows = false;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.Gainsboro;
            this.dgvNota.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle1;
            this.dgvNota.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this.dgvNota.BackgroundColor = System.Drawing.Color.White;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvNota.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle2;
            this.dgvNota.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle3.BackColor = System.Drawing.Color.LightGray;
            dataGridViewCellStyle3.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvNota.DefaultCellStyle = dataGridViewCellStyle3;
            this.dgvNota.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvNota.Location = new System.Drawing.Point(3, 16);
            this.dgvNota.MultiSelect = false;
            this.dgvNota.Name = "dgvNota";
            this.dgvNota.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.CellSelect;
            this.dgvNota.Size = new System.Drawing.Size(508, 281);
            this.dgvNota.TabIndex = 0;
            // 
            // frmNotaSelecionar
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(534, 361);
            this.Name = "frmNotaSelecionar";
            this.Text = "SELECIONAR NOTA";
            this.scForm.Panel1.ResumeLayout(false);
            this.scForm.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.scForm)).EndInit();
            this.scForm.ResumeLayout(false);
            this.pnlForm.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.banner)).EndInit();
            this.flpAcao.ResumeLayout(false);
            this.grbNota.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvNota)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.FlowLayoutPanel flpAcao;
        protected System.Windows.Forms.Button btnGravarNota;
        protected System.Windows.Forms.Button btnNotaIncluir;
        protected System.Windows.Forms.Button btnFechar;
        private System.Windows.Forms.GroupBox grbNota;
        private System.Windows.Forms.DataGridView dgvNota;
    }
}