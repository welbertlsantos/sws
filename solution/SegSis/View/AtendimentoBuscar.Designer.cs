﻿namespace SWS.View
{
    partial class frmAtendimentoBuscar
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            this.flpAcao = new System.Windows.Forms.FlowLayoutPanel();
            this.btnPesquisa = new System.Windows.Forms.Button();
            this.btnConfirmar = new System.Windows.Forms.Button();
            this.btnFechar = new System.Windows.Forms.Button();
            this.textCodigo = new System.Windows.Forms.MaskedTextBox();
            this.lblCodigo = new System.Windows.Forms.TextBox();
            this.lblDataGravacao = new System.Windows.Forms.TextBox();
            this.lblDataEmissao = new System.Windows.Forms.TextBox();
            this.dataEmissaoInicial = new System.Windows.Forms.DateTimePicker();
            this.dataEmissaoFinal = new System.Windows.Forms.DateTimePicker();
            this.dataGravacaoFinal = new System.Windows.Forms.DateTimePicker();
            this.dataGravacaoInicial = new System.Windows.Forms.DateTimePicker();
            this.btnExcluirCliente = new System.Windows.Forms.Button();
            this.cbSituacao = new SWS.ComboBoxWithBorder();
            this.btnCliente = new System.Windows.Forms.Button();
            this.textCliente = new System.Windows.Forms.TextBox();
            this.lblCliente = new System.Windows.Forms.TextBox();
            this.lblSituacao = new System.Windows.Forms.TextBox();
            this.dgvAtendimentos = new System.Windows.Forms.DataGridView();
            ((System.ComponentModel.ISupportInitialize)(this.scForm)).BeginInit();
            this.scForm.Panel1.SuspendLayout();
            this.scForm.Panel2.SuspendLayout();
            this.scForm.SuspendLayout();
            this.pnlForm.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.banner)).BeginInit();
            this.flpAcao.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvAtendimentos)).BeginInit();
            this.SuspendLayout();
            // 
            // scForm
            // 
            // 
            // scForm.Panel1
            // 
            this.scForm.Panel1.Controls.Add(this.flpAcao);
            // 
            // pnlForm
            // 
            this.pnlForm.Controls.Add(this.dgvAtendimentos);
            this.pnlForm.Controls.Add(this.btnExcluirCliente);
            this.pnlForm.Controls.Add(this.cbSituacao);
            this.pnlForm.Controls.Add(this.btnCliente);
            this.pnlForm.Controls.Add(this.textCliente);
            this.pnlForm.Controls.Add(this.lblCliente);
            this.pnlForm.Controls.Add(this.lblSituacao);
            this.pnlForm.Controls.Add(this.lblDataGravacao);
            this.pnlForm.Controls.Add(this.lblDataEmissao);
            this.pnlForm.Controls.Add(this.dataEmissaoInicial);
            this.pnlForm.Controls.Add(this.dataEmissaoFinal);
            this.pnlForm.Controls.Add(this.dataGravacaoFinal);
            this.pnlForm.Controls.Add(this.dataGravacaoInicial);
            this.pnlForm.Controls.Add(this.textCodigo);
            this.pnlForm.Controls.Add(this.lblCodigo);
            // 
            // flpAcao
            // 
            this.flpAcao.Controls.Add(this.btnPesquisa);
            this.flpAcao.Controls.Add(this.btnConfirmar);
            this.flpAcao.Controls.Add(this.btnFechar);
            this.flpAcao.Dock = System.Windows.Forms.DockStyle.Fill;
            this.flpAcao.Location = new System.Drawing.Point(0, 0);
            this.flpAcao.Name = "flpAcao";
            this.flpAcao.Size = new System.Drawing.Size(534, 30);
            this.flpAcao.TabIndex = 0;
            // 
            // btnPesquisa
            // 
            this.btnPesquisa.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnPesquisa.Image = global::SWS.Properties.Resources.lupa;
            this.btnPesquisa.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnPesquisa.Location = new System.Drawing.Point(3, 3);
            this.btnPesquisa.Name = "btnPesquisa";
            this.btnPesquisa.Size = new System.Drawing.Size(75, 23);
            this.btnPesquisa.TabIndex = 0;
            this.btnPesquisa.Text = "&Pesquisar";
            this.btnPesquisa.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnPesquisa.UseVisualStyleBackColor = true;
            this.btnPesquisa.Click += new System.EventHandler(this.btnPesquisa_Click);
            // 
            // btnConfirmar
            // 
            this.btnConfirmar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnConfirmar.Image = global::SWS.Properties.Resources.fechar_ico;
            this.btnConfirmar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnConfirmar.Location = new System.Drawing.Point(84, 3);
            this.btnConfirmar.Name = "btnConfirmar";
            this.btnConfirmar.Size = new System.Drawing.Size(75, 23);
            this.btnConfirmar.TabIndex = 1;
            this.btnConfirmar.Text = "&Confirmar";
            this.btnConfirmar.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnConfirmar.UseVisualStyleBackColor = true;
            this.btnConfirmar.Click += new System.EventHandler(this.btnConfirmar_Click);
            // 
            // btnFechar
            // 
            this.btnFechar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnFechar.Image = global::SWS.Properties.Resources.close;
            this.btnFechar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnFechar.Location = new System.Drawing.Point(165, 3);
            this.btnFechar.Name = "btnFechar";
            this.btnFechar.Size = new System.Drawing.Size(75, 23);
            this.btnFechar.TabIndex = 78;
            this.btnFechar.TabStop = false;
            this.btnFechar.Text = "&Fechar";
            this.btnFechar.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnFechar.UseVisualStyleBackColor = true;
            this.btnFechar.Click += new System.EventHandler(this.btnFechar_Click);
            // 
            // textCodigo
            // 
            this.textCodigo.BackColor = System.Drawing.Color.White;
            this.textCodigo.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textCodigo.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textCodigo.Location = new System.Drawing.Point(138, 3);
            this.textCodigo.Mask = "9999,99,999999";
            this.textCodigo.Name = "textCodigo";
            this.textCodigo.PromptChar = ' ';
            this.textCodigo.Size = new System.Drawing.Size(373, 21);
            this.textCodigo.TabIndex = 1;
            // 
            // lblCodigo
            // 
            this.lblCodigo.BackColor = System.Drawing.Color.LightGray;
            this.lblCodigo.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblCodigo.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCodigo.Location = new System.Drawing.Point(3, 3);
            this.lblCodigo.Name = "lblCodigo";
            this.lblCodigo.ReadOnly = true;
            this.lblCodigo.Size = new System.Drawing.Size(136, 21);
            this.lblCodigo.TabIndex = 2;
            this.lblCodigo.TabStop = false;
            this.lblCodigo.Text = "Codigo Atendimento";
            // 
            // lblDataGravacao
            // 
            this.lblDataGravacao.BackColor = System.Drawing.Color.LightGray;
            this.lblDataGravacao.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblDataGravacao.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDataGravacao.Location = new System.Drawing.Point(3, 83);
            this.lblDataGravacao.Name = "lblDataGravacao";
            this.lblDataGravacao.ReadOnly = true;
            this.lblDataGravacao.Size = new System.Drawing.Size(136, 21);
            this.lblDataGravacao.TabIndex = 11;
            this.lblDataGravacao.TabStop = false;
            this.lblDataGravacao.Text = "Data gravação";
            // 
            // lblDataEmissao
            // 
            this.lblDataEmissao.BackColor = System.Drawing.Color.LightGray;
            this.lblDataEmissao.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblDataEmissao.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDataEmissao.Location = new System.Drawing.Point(3, 63);
            this.lblDataEmissao.Name = "lblDataEmissao";
            this.lblDataEmissao.ReadOnly = true;
            this.lblDataEmissao.Size = new System.Drawing.Size(136, 21);
            this.lblDataEmissao.TabIndex = 9;
            this.lblDataEmissao.TabStop = false;
            this.lblDataEmissao.Text = "Data emissão";
            // 
            // dataEmissaoInicial
            // 
            this.dataEmissaoInicial.Checked = false;
            this.dataEmissaoInicial.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dataEmissaoInicial.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dataEmissaoInicial.Location = new System.Drawing.Point(138, 63);
            this.dataEmissaoInicial.Name = "dataEmissaoInicial";
            this.dataEmissaoInicial.ShowCheckBox = true;
            this.dataEmissaoInicial.Size = new System.Drawing.Size(120, 21);
            this.dataEmissaoInicial.TabIndex = 4;
            // 
            // dataEmissaoFinal
            // 
            this.dataEmissaoFinal.Checked = false;
            this.dataEmissaoFinal.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dataEmissaoFinal.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dataEmissaoFinal.Location = new System.Drawing.Point(257, 63);
            this.dataEmissaoFinal.Name = "dataEmissaoFinal";
            this.dataEmissaoFinal.ShowCheckBox = true;
            this.dataEmissaoFinal.Size = new System.Drawing.Size(120, 21);
            this.dataEmissaoFinal.TabIndex = 5;
            // 
            // dataGravacaoFinal
            // 
            this.dataGravacaoFinal.Checked = false;
            this.dataGravacaoFinal.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dataGravacaoFinal.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dataGravacaoFinal.Location = new System.Drawing.Point(257, 83);
            this.dataGravacaoFinal.Name = "dataGravacaoFinal";
            this.dataGravacaoFinal.ShowCheckBox = true;
            this.dataGravacaoFinal.Size = new System.Drawing.Size(120, 21);
            this.dataGravacaoFinal.TabIndex = 7;
            // 
            // dataGravacaoInicial
            // 
            this.dataGravacaoInicial.Checked = false;
            this.dataGravacaoInicial.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dataGravacaoInicial.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dataGravacaoInicial.Location = new System.Drawing.Point(138, 83);
            this.dataGravacaoInicial.Name = "dataGravacaoInicial";
            this.dataGravacaoInicial.ShowCheckBox = true;
            this.dataGravacaoInicial.Size = new System.Drawing.Size(120, 21);
            this.dataGravacaoInicial.TabIndex = 6;
            // 
            // btnExcluirCliente
            // 
            this.btnExcluirCliente.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnExcluirCliente.Image = global::SWS.Properties.Resources.close;
            this.btnExcluirCliente.Location = new System.Drawing.Point(481, 43);
            this.btnExcluirCliente.Name = "btnExcluirCliente";
            this.btnExcluirCliente.Size = new System.Drawing.Size(30, 21);
            this.btnExcluirCliente.TabIndex = 42;
            this.btnExcluirCliente.UseVisualStyleBackColor = true;
            this.btnExcluirCliente.Click += new System.EventHandler(this.btnExcluirCliente_Click);
            // 
            // cbSituacao
            // 
            this.cbSituacao.BackColor = System.Drawing.Color.LightGray;
            this.cbSituacao.BorderColor = System.Drawing.Color.DimGray;
            this.cbSituacao.BorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid;
            this.cbSituacao.FormattingEnabled = true;
            this.cbSituacao.Location = new System.Drawing.Point(138, 23);
            this.cbSituacao.Name = "cbSituacao";
            this.cbSituacao.Size = new System.Drawing.Size(373, 21);
            this.cbSituacao.TabIndex = 2;
            // 
            // btnCliente
            // 
            this.btnCliente.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnCliente.Image = global::SWS.Properties.Resources.busca;
            this.btnCliente.Location = new System.Drawing.Point(452, 43);
            this.btnCliente.Name = "btnCliente";
            this.btnCliente.Size = new System.Drawing.Size(30, 21);
            this.btnCliente.TabIndex = 3;
            this.btnCliente.UseVisualStyleBackColor = true;
            this.btnCliente.Click += new System.EventHandler(this.btnCliente_Click);
            // 
            // textCliente
            // 
            this.textCliente.BackColor = System.Drawing.SystemColors.Control;
            this.textCliente.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textCliente.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textCliente.Location = new System.Drawing.Point(138, 43);
            this.textCliente.MaxLength = 100;
            this.textCliente.Name = "textCliente";
            this.textCliente.ReadOnly = true;
            this.textCliente.Size = new System.Drawing.Size(315, 21);
            this.textCliente.TabIndex = 41;
            this.textCliente.TabStop = false;
            // 
            // lblCliente
            // 
            this.lblCliente.BackColor = System.Drawing.Color.LightGray;
            this.lblCliente.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblCliente.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCliente.Location = new System.Drawing.Point(3, 43);
            this.lblCliente.Name = "lblCliente";
            this.lblCliente.ReadOnly = true;
            this.lblCliente.Size = new System.Drawing.Size(136, 21);
            this.lblCliente.TabIndex = 38;
            this.lblCliente.TabStop = false;
            this.lblCliente.Text = "Cliente";
            // 
            // lblSituacao
            // 
            this.lblSituacao.BackColor = System.Drawing.Color.LightGray;
            this.lblSituacao.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblSituacao.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSituacao.Location = new System.Drawing.Point(3, 23);
            this.lblSituacao.Name = "lblSituacao";
            this.lblSituacao.ReadOnly = true;
            this.lblSituacao.Size = new System.Drawing.Size(136, 21);
            this.lblSituacao.TabIndex = 37;
            this.lblSituacao.TabStop = false;
            this.lblSituacao.Text = "Situação";
            // 
            // dgvAtendimentos
            // 
            this.dgvAtendimentos.AllowUserToAddRows = false;
            this.dgvAtendimentos.AllowUserToDeleteRows = false;
            this.dgvAtendimentos.AllowUserToResizeRows = false;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.Gainsboro;
            this.dgvAtendimentos.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle1;
            this.dgvAtendimentos.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.DisplayedCells;
            this.dgvAtendimentos.BackgroundColor = System.Drawing.Color.White;
            this.dgvAtendimentos.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.Color.LightGray;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvAtendimentos.DefaultCellStyle = dataGridViewCellStyle2;
            this.dgvAtendimentos.Location = new System.Drawing.Point(3, 110);
            this.dgvAtendimentos.MultiSelect = false;
            this.dgvAtendimentos.Name = "dgvAtendimentos";
            this.dgvAtendimentos.ReadOnly = true;
            this.dgvAtendimentos.RowHeadersVisible = false;
            this.dgvAtendimentos.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvAtendimentos.Size = new System.Drawing.Size(508, 170);
            this.dgvAtendimentos.TabIndex = 8;
            this.dgvAtendimentos.CellContentDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvAtendimentos_CellContentDoubleClick);
            // 
            // frmAtendimentoBuscar
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(534, 361);
            this.Name = "frmAtendimentoBuscar";
            this.Text = "BUSCAR ATENDIMENTOS";
            this.scForm.Panel1.ResumeLayout(false);
            this.scForm.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.scForm)).EndInit();
            this.scForm.ResumeLayout(false);
            this.pnlForm.ResumeLayout(false);
            this.pnlForm.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.banner)).EndInit();
            this.flpAcao.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvAtendimentos)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.FlowLayoutPanel flpAcao;
        private System.Windows.Forms.Button btnPesquisa;
        private System.Windows.Forms.Button btnFechar;
        public System.Windows.Forms.MaskedTextBox textCodigo;
        private System.Windows.Forms.TextBox lblCodigo;
        private System.Windows.Forms.TextBox lblDataGravacao;
        private System.Windows.Forms.TextBox lblDataEmissao;
        private System.Windows.Forms.DateTimePicker dataEmissaoInicial;
        private System.Windows.Forms.DateTimePicker dataEmissaoFinal;
        private System.Windows.Forms.DateTimePicker dataGravacaoFinal;
        private System.Windows.Forms.DateTimePicker dataGravacaoInicial;
        private System.Windows.Forms.Button btnExcluirCliente;
        private ComboBoxWithBorder cbSituacao;
        private System.Windows.Forms.Button btnCliente;
        public System.Windows.Forms.TextBox textCliente;
        private System.Windows.Forms.TextBox lblCliente;
        private System.Windows.Forms.TextBox lblSituacao;
        private System.Windows.Forms.Button btnConfirmar;
        private System.Windows.Forms.DataGridView dgvAtendimentos;
    }
}