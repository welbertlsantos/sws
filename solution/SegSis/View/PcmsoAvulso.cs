﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using SWS.Entidade;
using SWS.Facade;
using SWS.View.Resources;

namespace SWS.View
{
    public partial class frmPcmsosAvulso : SWS.View.frmTemplate
    {

        private static String msg1 = "Selecione um Pcmso.";
        private static String msg2 = "Para pesquisar pela contratada é necessário escolher um Cliente.";
        private static String msg3 = "PCMSO incluído com sucesso.";

        public Cliente cliente;
        public Cliente clienteContratada;
        private Estudo pcmso;

        public Estudo Pcmso
        {
            get { return pcmso; }
            set { pcmso = value; }
        }
        
        public frmPcmsosAvulso()
        {
            InitializeComponent();
            validaPermissoes();
            ActiveControl = textCodigo;
            
        }


        private void validaPermissoes()
        {
            PermissionamentoFacade permissionamentoFacade = PermissionamentoFacade.getInstance();

            this.btn_incluir.Enabled = permissionamentoFacade.hasPermission("PCMSO_AVULSO", "INCLUIR");
            this.btn_alterar.Enabled = permissionamentoFacade.hasPermission("PCMSO_AVULSO", "ALTERAR");
            this.btnReplica.Enabled = permissionamentoFacade.hasPermission("PCMSO_AVULSO", "REPLICAR");
            this.btnExcluir.Enabled = permissionamentoFacade.hasPermission("PCMSO_AVULSO", "EXCLUIR");
        }

        private void btn_fechar_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btn_alterar_Click(object sender, EventArgs e)
        {
            try
            {

                if (dgvPcmso.CurrentRow == null)
                    throw new Exception(msg1);

                PcmsoFacade pcmsoFacade = PcmsoFacade.getInstance();

                PcmsoAvulsoAlterar pcmsoAvulsoIncluir = new PcmsoAvulsoAlterar(pcmsoFacade.findById((Int64)dgvPcmso.CurrentRow.Cells[0].Value));
                pcmsoAvulsoIncluir.ShowDialog();

                montaGrid();

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btn_incluir_Click(object sender, EventArgs e)
        {
            try
            {
                frmPcmsoAvulsoIncluir frmPcmoIncluir = new frmPcmsoAvulsoIncluir();
                frmPcmoIncluir.ShowDialog();

                if (frmPcmoIncluir.Estudo != null)
                    MessageBox.Show(msg3, "Confirmação", MessageBoxButtons.OK, MessageBoxIcon.Information);

                montaGrid();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btn_limpar_Click(object sender, EventArgs e)
        {
            this.cliente = null;
            this.clienteContratada = null;
            this.textCliente.Text = String.Empty;
            this.textContratante.Text = String.Empty;
            this.dgvPcmso.Columns.Clear();
            this.textCodigo.Text = String.Empty;

        }

        private void btn_pesquisar_Click(object sender, EventArgs e)
        {
            try
            {
                if (cliente == null && clienteContratada != null)
                    throw new Exception (msg2);
                
                montaGrid();
                
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }

        private void btContrante_Click(object sender, EventArgs e)
        {
            frmClienteSelecionar formCliente = new frmClienteSelecionar(null, null, null, null, null, null, null, null, true);
            formCliente.ShowDialog();

            if (formCliente.Cliente != null)
            {
                clienteContratada = formCliente.Cliente;
                textContratante.Text = clienteContratada.RazaoSocial;
            }
        }

        private void btCliente_Click(object sender, EventArgs e)
        {
            frmClienteSelecionar formCliente = new frmClienteSelecionar(null, null, false, null, null, null, null, null, true);
            formCliente.ShowDialog();

            if (formCliente.Cliente != null)
            {
                cliente = formCliente.Cliente;
                textCliente.Text = cliente.RazaoSocial;
            }

        }

        public void montaGrid()
        {
            try
            {
                PcmsoFacade pcmsoFacade = PcmsoFacade.getInstance();
                
                ClienteFacade clienteFacade = ClienteFacade.getInstance();
                
                pcmso = new Estudo();

                pcmso.CodigoEstudo = textCodigo.Text;

                if (cliente != null)
                    pcmso.VendedorCliente = clienteFacade.findClienteVendedorByCliente(cliente).Find(x => String.Equals(x.getSituacao(), ApplicationConstants.ATIVO));
                
                if (clienteContratada != null)
                    pcmso.ClienteContratante = clienteContratada;

                pcmso.Avulso = true;
                pcmso.Situacao = ApplicationConstants.FECHADO;

                this.Cursor = Cursors.WaitCursor;

                DataSet ds = pcmsoFacade.findByFilter(pcmso, null);

                dgvPcmso.DataSource = ds.Tables["Pcmso"].DefaultView;

                dgvPcmso.Columns[0].HeaderText = "ID";
                dgvPcmso.Columns[0].Visible = false;

                dgvPcmso.Columns[1].HeaderText = "Código PCMSO";
                dgvPcmso.Columns[2].HeaderText = "Data da Criação";
                dgvPcmso.Columns[3].HeaderText = "Data de Vencimento";
                dgvPcmso.Columns[4].HeaderText = "Data de Fechamento";
                dgvPcmso.Columns[5].HeaderText = "Situação";
                dgvPcmso.Columns[6].HeaderText = "Cliente";
                dgvPcmso.Columns[6].DisplayIndex = 0; 
                dgvPcmso.Columns[7].HeaderText = "Cliente Contratado";
                dgvPcmso.Columns[8].HeaderText = "Obra";
                dgvPcmso.Columns[9].HeaderText = "CNPJ Cliente";
                dgvPcmso.Columns[9].DisplayIndex = 1;
                dgvPcmso.Columns[10].HeaderText = "CNPJ Contratante";
                dgvPcmso.Columns[10].DisplayIndex = 9;

                
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }

        private void dgvPcmso_CellMouseDoubleClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            try
            {
                btn_alterar.PerformClick();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            
        }

        private void btnReplica_Click(object sender, EventArgs e)
        {
            try
            {
                if (dgvPcmso.CurrentRow == null)
                    throw new Exception("Selecione uma linha.");

                PcmsoFacade pcmsoFacade = PcmsoFacade.getInstance();

                frmPcmsoAvulsoReplicar replicaPcmso = new frmPcmsoAvulsoReplicar(pcmsoFacade.findById((long)dgvPcmso.CurrentRow.Cells[0].Value));
                replicaPcmso.ShowDialog();

                if (replicaPcmso.PcmsoReplicado != null)
                {
                    textCodigo.Text = replicaPcmso.PcmsoReplicado.CodigoEstudo;
                    montaGrid();
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }

        private void btnExcluir_Click(object sender, EventArgs e)
        {
            try
            {
                if (dgvPcmso.CurrentRow == null)
                    throw new Exception("Selecione uma linha.");

                this.Cursor = Cursors.WaitCursor;
                PcmsoFacade pcmsoFacade = PcmsoFacade.getInstance();
                Estudo pcmsoExcluir = pcmsoFacade.findById((long)dgvPcmso.CurrentRow.Cells[0].Value);

                /* enviando para a exclusão do pcmso */

                pcmsoFacade.excluiPcmso(pcmsoExcluir);
                MessageBox.Show("Pcmso excluído com sucesso", "Confirmação", MessageBoxButtons.OK, MessageBoxIcon.Information);
                montaGrid();

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }

        }
        
       
    }
}
