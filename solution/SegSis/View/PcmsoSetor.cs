﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using SWS.Entidade;
using SWS.Facade;
using SWS.View.Resources;

namespace SWS.View
{
    public partial class frmPcmsoSetor : SWS.View.frmTemplateConsulta
    {
        private List<GheSetor> gheSetor = new List<GheSetor>();

        public List<GheSetor> GheSetor
        {
            get { return gheSetor; }
            set { gheSetor = value; }
        }

        private Ghe ghe;

        public static String Msg1 = "Selecione uma linha.";

        public frmPcmsoSetor(Ghe ghe)
        {
            InitializeComponent();
            validaPermissoes();
            this.ghe = ghe;
            ActiveControl = textDescricao;
        }

        private void btIncluir_Click(object sender, EventArgs e)
        {
            try
            {
                EstudoFacade pcmsoFacade = EstudoFacade.getInstance();
                this.Cursor = Cursors.WaitCursor;

                if (dgvSetor.CurrentRow == null)
                    throw new Exception(Msg1);

                foreach (DataGridViewRow dvRow in dgvSetor.Rows)
                {
                    if ((Boolean)dvRow.Cells[3].Value)
                        gheSetor.Add(pcmsoFacade.insertGheSetor(new GheSetor(null, new Setor(Convert.ToInt64(dvRow.Cells[0].Value), dvRow.Cells[1].Value.ToString(), dvRow.Cells[2].Value.ToString()), ghe, ApplicationConstants.ATIVO)));

                }

                this.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }

        private void btNovo_Click(object sender, EventArgs e)
        {
            try
            {
                frmSetorIncluir formSetorIncluir = new frmSetorIncluir();
                formSetorIncluir.ShowDialog();

                if (formSetorIncluir.Setor != null)
                {
                    textDescricao.Text = formSetorIncluir.Setor.Descricao;
                    btBuscar.PerformClick();

                    if (dgvSetor.RowCount > 0)
                    {
                        EstudoFacade pcmsoFacade = EstudoFacade.getInstance();

                        pcmsoFacade.insertGheSetor(new GheSetor(null, new Setor(Convert.ToInt64(dgvSetor.Rows[0].Cells[0].Value), dgvSetor.Rows[0].Cells[1].Value.ToString(),
                            dgvSetor.Rows[0].Cells[2].Value.ToString()), ghe, ApplicationConstants.ATIVO));

                        this.Close();

                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btBuscar_Click(object sender, EventArgs e)
        {
            try
            {
                this.dgvSetor.Columns.Clear();
                montaDataGrid();
                textDescricao.Focus();

            }
            catch (Exception Ex)
            {
                MessageBox.Show(Ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btFechar_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void validaPermissoes()
        {
            PermissionamentoFacade permissionamentoFacade = PermissionamentoFacade.getInstance();
            this.btNovo.Enabled = permissionamentoFacade.hasPermission("SETOR", "INCLUIR");
        }

        public void montaDataGrid()
        {
            try
            {
                this.Cursor = Cursors.WaitCursor;

                EstudoFacade pcmsoFacade = EstudoFacade.getInstance();

                DataSet ds = pcmsoFacade.findAllSetorAtivo(new Setor(null, textDescricao.Text, ApplicationConstants.ATIVO), ghe, null);

                dgvSetor.DataSource = ds.Tables["Setores"].DefaultView;

                dgvSetor.Columns[0].HeaderText = "id_setor";
                dgvSetor.Columns[0].Visible = false;

                dgvSetor.Columns[1].HeaderText = "Descrição";
                dgvSetor.Columns[1].ReadOnly = true;

                dgvSetor.Columns[2].HeaderText = "Situação";
                dgvSetor.Columns[2].Visible = false;

                dgvSetor.Columns[3].HeaderText = "Selec";
                dgvSetor.Columns[3].DisplayIndex = 0;
                dgvSetor.Columns[3].DefaultCellStyle.BackColor = Color.White;

            }
            catch (Exception Ex)
            {
                MessageBox.Show(Ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
                 
        }


    }
}
