﻿using SWS.Entidade;
using SWS.Facade;
using SWS.View.Resources;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SWS.View
{
    public partial class frmUsuarioSelecionar : frmTemplateConsulta
    {
        private Usuario usuario;

        public Usuario Usuario
        {
          get { return usuario; }
          set { usuario = value; }
        }

        public frmUsuarioSelecionar()
        {
            InitializeComponent();
            montaDataGrid();
        }

        private void btFechar_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        public void montaDataGrid()
        {
            try
            {
                this.dgvUsuario.Columns.Clear();
                this.dgvUsuario.ColumnCount = 3;

                this.dgvUsuario.Columns[0].HeaderText = "idUsuario";
                this.dgvUsuario.Columns[0].Visible = false;
                this.dgvUsuario.Columns[1].HeaderText = "Nome";
                this.dgvUsuario.Columns[2].HeaderText = "Login";

                UsuarioFacade usuarioFacade = UsuarioFacade.getInstance();
                DataSet ds = usuarioFacade.findUsuarioByFilter(new Usuario(null, string.Empty, String.Empty, String.Empty, String.Empty, null, ApplicationConstants.ATIVO, String.Empty, String.Empty, String.Empty, null, null, String.Empty, false, null, String.Empty, false, string.Empty, string.Empty));
                Usuario usuario = new Usuario();

                foreach (DataRow dvRow in usuarioFacade.findUsuarioByFilter(usuario).Tables[0].Rows)
                {
                    dgvUsuario.Rows.Add(
                        dvRow["ID_USUARIO"].ToString(), 
                        dvRow["NOME"].ToString(),
                        dvRow["LOGIN"].ToString()
                        );
                }
                
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btSelecionar_Click(object sender, EventArgs e)
        {
            try
            {
                if (dgvUsuario.CurrentRow == null)
                    throw new Exception("Selecione uma linha");
                
                UsuarioFacade usuarioFacede = UsuarioFacade.getInstance();

                this.Usuario = usuarioFacede.findUsuarioPpraById(Convert.ToInt64(dgvUsuario.CurrentRow.Cells[0].Value));

                this.Close();

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }
    }
}
