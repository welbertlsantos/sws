﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using SWS.Entidade;
using SWS.Facade;
using SWS.View.Resources;

namespace SWS.View
{
    public partial class frm_GheAlterar : BaseFormConsulta
    {
        private Ghe ghe;

        public Ghe Ghe
        {
            get { return ghe; }
            set { ghe = value; }
        }

        public frm_GheAlterar(Ghe ghe)
        {
            InitializeComponent();
            this.Ghe = ghe;
            text_descricaoGhe.Text = ghe.Descricao;
            text_nexp.Text = Convert.ToString(ghe.Nexp);
            montaGride();

        }

        private void btn_inserir_Click(object sender, EventArgs e)
        {
            frm_GheNormaAlterar formGheNormaAlterar = new frm_GheNormaAlterar(ghe);
            formGheNormaAlterar.ShowDialog();

            if (formGheNormaAlterar.NormaSelecionada.Count > 0)
            {
                EstudoFacade pcmsoFacade = EstudoFacade.getInstance();

                foreach (Nota norma in formGheNormaAlterar.NormaSelecionada)
                {
                    pcmsoFacade.insertGheNota(new GheNota(ghe, norma));
                }
                
                montaGride();
            }
        }

        private void btn_excluir_Click(object sender, EventArgs e)
        {
            try
            {
                EstudoFacade pcmsoFacade = EstudoFacade.getInstance();

                if (grd_norma.CurrentRow == null)
                    throw new Exception("Selecione uma nota para excluir.");

                foreach (DataGridViewRow dvRow in grd_norma.SelectedRows)
                    pcmsoFacade.deleteNotaGhe(new GheNota(ghe, new Nota((Int64)dvRow.Cells[0].Value, (String)dvRow.Cells[1].Value, (String)dvRow.Cells[2].Value, (String)dvRow.Cells[3].Value)));
                    
                montaGride();
                

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btn_salvar_Click(object sender, EventArgs e)
        {
            try
            {
                if (string.IsNullOrEmpty(text_descricaoGhe.Text))
                    throw new Exception("O nome do GHE é obrigatório.");

                if (string.IsNullOrEmpty(text_nexp.Text))
                    throw new Exception("Campo número de exposto não pode ser nulo.");

                EstudoFacade pcmsoFacade = EstudoFacade.getInstance();

                ghe = pcmsoFacade.updateGhe(new Ghe((Int64)ghe.Id, ghe.Estudo, text_descricaoGhe.Text, Convert.ToInt32(text_nexp.Text), ApplicationConstants.ATIVO, textNumeroPo.Text ));
                
                this.Close();
                    
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btn_fechar_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        public void montaGride()
        {
            try
            {
                this.Cursor = Cursors.WaitCursor;
                
                grd_norma.Columns.Clear();

                EstudoFacade pcmsoFacade = EstudoFacade.getInstance();

                DataSet ds = pcmsoFacade.findAllNotasByGhe(ghe);

                grd_norma.DataSource = ds.Tables[0].DefaultView;

                grd_norma.Columns[0].HeaderText = "ID";
                grd_norma.Columns[0].Visible = false;

                grd_norma.Columns[1].HeaderText = "Título";

                grd_norma.Columns[2].HeaderText = "Situação";
                grd_norma.Columns[2].Visible = false;

                grd_norma.Columns[3].HeaderText = "Conteúdo";


            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }

        }

    }
}
