﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using SWS.Entidade;
using SWS.Facade;

namespace SWS.View
{
    public partial class frm_CorrecaoEstudo : Form
    {
        private Estudo estudo;
        private CorrecaoEstudo correcaoEstudo;
        public CorrecaoEstudo getCorrecaoEstudo()
        {
            return this.correcaoEstudo;
        }

        public static String msg1 = "Atenção";
        public static String msg2 = "Histórico da correção gravado com sucesso";
        public static String msg3 = "Histórico muito curto ou em branco. Reveja o campo histórico.";
        
        public frm_CorrecaoEstudo(Estudo estudo)
        {
            InitializeComponent();
            this.estudo = estudo;
        }

        private void btnGravar_Click(object sender, EventArgs e)
        {
            try
            {
                /* verificando se foi digitado alguma informação no histórico e se ela 
                 * atende o limite estabelecido para histórico */

                if (String.IsNullOrEmpty(textHistorico.Text.Trim()) ||
                    textHistorico.Text.Length < 30)
                {
                    throw new Exception(msg3);
                }

                LogFacade logFacade = LogFacade.getInstance();

                correcaoEstudo = new CorrecaoEstudo(null,
                    PermissionamentoFacade.usuarioAutenticado,
                    estudo, textHistorico.Text.ToUpper().Trim(),
                    DateTime.Now);

                correcaoEstudo = logFacade.insertCorrecaoEstudo(correcaoEstudo);

                MessageBox.Show(msg2);
                this.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, msg1, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
            
        }

        private void btnFechar_Click(object sender, EventArgs e)
        {
            this.Close();
        }


    }
}
