﻿namespace SWS.View
{
    partial class frm_PpraElaborador
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frm_PpraElaborador));
            this.grb_elaborador = new System.Windows.Forms.GroupBox();
            this.dgv_elaborador = new System.Windows.Forms.DataGridView();
            this.grb_paginacao = new System.Windows.Forms.GroupBox();
            this.btn_fechar = new System.Windows.Forms.Button();
            this.btn_gravar = new System.Windows.Forms.Button();
            this.grb_elaborador.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgv_elaborador)).BeginInit();
            this.grb_paginacao.SuspendLayout();
            this.SuspendLayout();
            // 
            // grb_elaborador
            // 
            this.grb_elaborador.Controls.Add(this.dgv_elaborador);
            this.grb_elaborador.Location = new System.Drawing.Point(12, 12);
            this.grb_elaborador.Name = "grb_elaborador";
            this.grb_elaborador.Size = new System.Drawing.Size(576, 322);
            this.grb_elaborador.TabIndex = 1;
            this.grb_elaborador.TabStop = false;
            this.grb_elaborador.Text = "Elaboradores";
            // 
            // dgv_elaborador
            // 
            this.dgv_elaborador.AllowUserToAddRows = false;
            this.dgv_elaborador.AllowUserToDeleteRows = false;
            this.dgv_elaborador.AllowUserToOrderColumns = true;
            this.dgv_elaborador.AllowUserToResizeRows = false;
            this.dgv_elaborador.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this.dgv_elaborador.BackgroundColor = System.Drawing.Color.White;
            this.dgv_elaborador.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgv_elaborador.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgv_elaborador.Location = new System.Drawing.Point(3, 16);
            this.dgv_elaborador.Name = "dgv_elaborador";
            this.dgv_elaborador.ReadOnly = true;
            this.dgv_elaborador.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgv_elaborador.Size = new System.Drawing.Size(570, 303);
            this.dgv_elaborador.TabIndex = 2;
            // 
            // grb_paginacao
            // 
            this.grb_paginacao.Controls.Add(this.btn_fechar);
            this.grb_paginacao.Controls.Add(this.btn_gravar);
            this.grb_paginacao.Location = new System.Drawing.Point(12, 340);
            this.grb_paginacao.Name = "grb_paginacao";
            this.grb_paginacao.Size = new System.Drawing.Size(576, 51);
            this.grb_paginacao.TabIndex = 3;
            this.grb_paginacao.TabStop = false;
            // 
            // btn_fechar
            // 
            this.btn_fechar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_fechar.Location = new System.Drawing.Point(87, 19);
            this.btn_fechar.Name = "btn_fechar";
            this.btn_fechar.Size = new System.Drawing.Size(75, 23);
            this.btn_fechar.TabIndex = 4;
            this.btn_fechar.Text = "&Fechar";
            this.btn_fechar.UseVisualStyleBackColor = true;
            this.btn_fechar.Click += new System.EventHandler(this.btn_fechar_Click);
            // 
            // btn_gravar
            // 
            this.btn_gravar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_gravar.Location = new System.Drawing.Point(6, 19);
            this.btn_gravar.Name = "btn_gravar";
            this.btn_gravar.Size = new System.Drawing.Size(75, 23);
            this.btn_gravar.TabIndex = 3;
            this.btn_gravar.Text = "&Gravar";
            this.btn_gravar.UseVisualStyleBackColor = true;
            this.btn_gravar.Click += new System.EventHandler(this.btn_gravar_Click);
            // 
            // frm_PpraElaborador
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(600, 400);
            this.ControlBox = false;
            this.Controls.Add(this.grb_paginacao);
            this.Controls.Add(this.grb_elaborador);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "frm_PpraElaborador";
            this.Text = "SELECIONAR ELABORADOR";
            this.grb_elaborador.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgv_elaborador)).EndInit();
            this.grb_paginacao.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox grb_elaborador;
        private System.Windows.Forms.GroupBox grb_paginacao;
        private System.Windows.Forms.Button btn_fechar;
        private System.Windows.Forms.Button btn_gravar;
        private System.Windows.Forms.DataGridView dgv_elaborador;
        
    }
}