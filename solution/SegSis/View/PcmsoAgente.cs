﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using SWS.Entidade;
using SWS.Facade;
using SWS.View.Resources;

namespace SWS.View
{
    public partial class frmPcmsoAgente : SWS.View.frmTemplateConsulta
    {
        protected Ghe ghe;
        protected GheFonte gheFonte;
        
        
        public frmPcmsoAgente(Ghe ghe)
        {
            InitializeComponent();
            this.ghe = ghe;
            validaPermissoes();
            ActiveControl = textAgente;

            /* localizando o gheFonte baseado na fonte padrão do sistema. A fonte padrão é colocada em carga com o id 1. */

            PpraFacade ppraFacade = PpraFacade.getInstance();
            PcmsoFacade pcmsoFacade = PcmsoFacade.getInstance();

            HashSet<GheFonte> gheFontes = ppraFacade.findAllGheFonteByGhe(ghe);
            List<GheFonte> gheFontList = new List<GheFonte>();

            /* criando a coleção de GheFonte */

            foreach (GheFonte gheFonte in gheFontes)
            {
                gheFontList.Add(gheFonte);
            }

            this.gheFonte = gheFontList.Find(x => x.getFonte().Id == 1);

            if (this.gheFonte == null)
                this.gheFonte = pcmsoFacade.insertGheFonte(new GheFonte(null, ppraFacade.findFonteById(1), ghe));

        }

        protected void btn_fechar_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        protected void btn_buscar_Click(object sender, EventArgs e)
        {
            try
            {
                this.dgvAgente.Columns.Clear();
                montaDataGrid();
            }
            catch (Exception Ex)
            {
                MessageBox.Show(Ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        protected void btn_novo_Click(object sender, EventArgs e)
        {
            try
            {
                frmAgentesIncluir incluirAgente = new frmAgentesIncluir();
                incluirAgente.ShowDialog();

                PcmsoFacade pcmsoFacade = PcmsoFacade.getInstance();

                if (incluirAgente.Agente != null)
                {
                    pcmsoFacade.incluiGheFonteAgente(new GheFonteAgente(null, null, null, null, gheFonte, incluirAgente.Agente, String.Empty, ApplicationConstants.ATIVO));
                    this.Close();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK , MessageBoxIcon.Error);
            }
        }

        protected void btn_ok_Click(object sender, EventArgs e)
        {
            try
            {
                PcmsoFacade pcmsoFacade = PcmsoFacade.getInstance();
                PpraFacade ppraFacade = PpraFacade.getInstance();

                foreach (DataGridViewRow dvRow in dgvAgente.Rows)
                {
                    if ((Boolean)dvRow.Cells[8].Value == true)
                        pcmsoFacade.incluiGheFonteAgente(new GheFonteAgente(null, null, null, null, gheFonte, ppraFacade.findAgenteById((Int64)dvRow.Cells[0].Value), String.Empty, ApplicationConstants.ATIVO));
                }

                this.Close();

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void validaPermissoes()
        {
            PermissionamentoFacade permissionamentoFacade = PermissionamentoFacade.getInstance();

            this.btNovo.Enabled = permissionamentoFacade.hasPermission("AGENTE", "INCLUIR");
        }

        public void montaDataGrid()
        {

            PcmsoFacade pcmsoFacade = PcmsoFacade.getInstance();

            DataSet ds = pcmsoFacade.findAllAgentesNotInGheFonte(new Agente(null, null, textAgente.Text, String.Empty, String.Empty, String.Empty, ApplicationConstants.ATIVO, false, string.Empty, string.Empty, string.Empty), gheFonte);
            
            dgvAgente.DataSource = ds.Tables[0].DefaultView;

            dgvAgente.AutoResizeColumns();

            dgvAgente.Columns[0].HeaderText = "id_agente";
            dgvAgente.Columns[0].Visible = false;

            dgvAgente.Columns[1].HeaderText = "id_risco";
            dgvAgente.Columns[1].Visible = false;

            dgvAgente.Columns[2].HeaderText = "Descrição";
            dgvAgente.Columns[2].ReadOnly = true;
            dgvAgente.Columns[2].Width = 350;
            
            dgvAgente.Columns[3].HeaderText = "Trajetória";
            dgvAgente.Columns[3].Visible = false;

            dgvAgente.Columns[4].HeaderText = "Danos";
            dgvAgente.Columns[4].Visible = false;

            dgvAgente.Columns[5].HeaderText = "Limite";
            dgvAgente.Columns[5].Visible = false;

            dgvAgente.Columns[6].HeaderText = "Situação";
            dgvAgente.Columns[6].Visible = false;

            dgvAgente.Columns[7].HeaderText = "Risco";
            dgvAgente.Columns[7].DisplayIndex = 3;
            dgvAgente.Columns[7].ReadOnly = true;

            dgvAgente.Columns[8].HeaderText = "Selecionar";
            dgvAgente.Columns[8].DisplayIndex = 0;

        }

        private void dgvAgente_CellMouseDoubleClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            try
            {
                btOk.PerformClick();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }
    }
}
