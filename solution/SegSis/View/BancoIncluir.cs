﻿using SWS.Entidade;
using SWS.Facade;
using SWS.View.ViewHelper;
using SWS.ViewHelper;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace SWS.View
{
    public partial class frmBancoIncluir : frmTemplate
    {
        private Banco banco;

        public Banco Banco
        {
            get { return banco; }
            set { banco = value; }
        }
        
        public frmBancoIncluir()
        {
            InitializeComponent();
            ComboHelper.Boolean(cbCaixa, false);
            ComboHelper.Boolean(cbBoleto, false);
            ActiveControl = textNome;

        }

        protected virtual void btnGravar_Click(object sender, EventArgs e)
        {
            try
            {
                if (ValidaCampos())
                {
                    FinanceiroFacade financeiroFacade = FinanceiroFacade.getInstance();

                    banco = financeiroFacade.incluirBanco(new Banco(null, this.textNome.Text, textCodigo.Text, (bool?)Convert.ToBoolean(((SelectItem)cbCaixa.SelectedItem).Valor), string.Empty, false, (bool?)Convert.ToBoolean(((SelectItem)cbBoleto.SelectedItem).Valor)));

                    MessageBox.Show("Banco incluído com sucesso", "Confirmação", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    this.Close();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }

        protected void btnLimpar_Click(object sender, EventArgs e)
        {
            textNome.Text = string.Empty;
            textCodigo.Text = string.Empty;
            ComboHelper.Boolean(cbCaixa, false);
            ComboHelper.Boolean(cbBoleto, false);
            textNome.Focus();
        }

        protected void btnFechar_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        protected void frmBancoIncluir_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Return || e.KeyCode == Keys.Enter)
                SendKeys.Send("{TAB}");
        }

        protected void textCodigo_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = ValidaCampoHelper.FormataCampoNumerico(sender, e);
        }

        protected Boolean ValidaCampos()
        {
            bool retorno = true;

            try
            {
                if (String.IsNullOrEmpty(textNome.Text))
                    throw new Exception("O campo nome não pode ser vazio.");

                if (string.Equals(((SelectItem)cbCaixa.SelectedItem).Valor, "false") && string.IsNullOrEmpty(textCodigo.Text))
                    throw new Exception (" O campo código bancário é obrigatório");

            }
            catch (Exception ex)
            {
                retorno = false;
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
                
            }

            return retorno;
        }

        protected void cbCaixa_SelectedValueChanged(object sender, EventArgs e)
        {
            if (string.Equals(((SelectItem)cbCaixa.SelectedItem).Valor, "true"))
            {
                cbBoleto.SelectedValue = "false";
                cbBoleto.Enabled = false;
                textCodigo.Text = string.Empty;
                textCodigo.ReadOnly = true;
                    
            }
            else
            {
                cbBoleto.Enabled = true;
                textCodigo.ReadOnly = false;
            }
        }

        private void cbBoleto_SelectedValueChanged(object sender, EventArgs e)
        {
            if (string.Equals(((SelectItem)cbBoleto.SelectedItem).Valor, "true"))
            {
                cbCaixa.SelectedValue = "false";
                cbCaixa.Enabled = false;

            }
            else
            {
                cbCaixa.Enabled = true;
                textCodigo.ReadOnly = false;
            }
        }
    }
}
