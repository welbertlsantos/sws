﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using SegSis.Facade;
using SegSis.Entidade;
using SegSis.Excecao;
using SegSis.View.ViewHelper;
using System.Text.RegularExpressions;
using SegSis.View.Resources;


namespace SegSis.View
{
    public partial class frm_detalhar_agente : BaseForm
    {
        private frm_agente formAgente;
        private long? idAgente;

        public frm_detalhar_agente(Agente agente, frm_agente formAgente)
        {
            InitializeComponent();

            text_descricao.ReadOnly = true;
            text_trajetoria.ReadOnly = true;
            text_dano.ReadOnly = true;
            text_limite.ReadOnly = true;
                        
            rb_ativo.Enabled = false;
            rb_desativado.Enabled = false;
            
            this.idAgente = agente.getId();

            text_descricao.Text = agente.getDescricao();
            text_trajetoria.Text = agente.getTrajetoria();
            text_dano.Text = agente.getDano();
            text_limite.Text = agente.getLimite();
            
            this.formAgente = formAgente;

            if (ApplicationConstants.ATIVO.Equals(agente.getSituacao()))
            {
                rb_ativo.Checked = true;
            }
            else if (ApplicationConstants.DESATIVADO.Equals(agente.getSituacao()))
            {
                rb_desativado.Checked = true;
            }
            
            text_risco.Text = agente.getRisco().getDescricao();

        }

        private void btn_fechar_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
