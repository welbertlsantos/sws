﻿namespace SWS.View
{
    partial class frm_RelAcompanhamentoAtendimento
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frm_RelAcompanhamentoAtendimento));
            this.grb_filtro = new System.Windows.Forms.GroupBox();
            this.btn_fechar = new System.Windows.Forms.Button();
            this.grb_tecnico = new System.Windows.Forms.GroupBox();
            this.lbl_tecno = new System.Windows.Forms.Label();
            this.btn_usuario = new System.Windows.Forms.Button();
            this.text_usuario = new System.Windows.Forms.TextBox();
            this.grb_listaExame = new System.Windows.Forms.GroupBox();
            this.grd_selecao_exames = new System.Windows.Forms.DataGridView();
            this.grb_salaAtendimento = new System.Windows.Forms.GroupBox();
            this.cb_sala = new System.Windows.Forms.ComboBox();
            this.grb_situacao = new System.Windows.Forms.GroupBox();
            this.cb_situacao_atendimento = new System.Windows.Forms.ComboBox();
            this.btn_limpar = new System.Windows.Forms.Button();
            this.btn_pesquisar = new System.Windows.Forms.Button();
            this.grb_dataCriacao = new System.Windows.Forms.GroupBox();
            this.chk_data = new System.Windows.Forms.CheckBox();
            this.dt_criacaoInicial = new System.Windows.Forms.DateTimePicker();
            this.dt_criacaoFinal = new System.Windows.Forms.DateTimePicker();
            this.panel.SuspendLayout();
            this.grb_filtro.SuspendLayout();
            this.grb_tecnico.SuspendLayout();
            this.grb_listaExame.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grd_selecao_exames)).BeginInit();
            this.grb_salaAtendimento.SuspendLayout();
            this.grb_situacao.SuspendLayout();
            this.grb_dataCriacao.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel
            // 
            this.panel.Controls.Add(this.grb_filtro);
            // 
            // grb_filtro
            // 
            this.grb_filtro.Controls.Add(this.btn_fechar);
            this.grb_filtro.Controls.Add(this.grb_tecnico);
            this.grb_filtro.Controls.Add(this.grb_listaExame);
            this.grb_filtro.Controls.Add(this.grb_salaAtendimento);
            this.grb_filtro.Controls.Add(this.grb_situacao);
            this.grb_filtro.Controls.Add(this.btn_limpar);
            this.grb_filtro.Controls.Add(this.btn_pesquisar);
            this.grb_filtro.Controls.Add(this.grb_dataCriacao);
            this.grb_filtro.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.grb_filtro.Location = new System.Drawing.Point(3, 3);
            this.grb_filtro.Name = "grb_filtro";
            this.grb_filtro.Size = new System.Drawing.Size(781, 304);
            this.grb_filtro.TabIndex = 1;
            this.grb_filtro.TabStop = false;
            this.grb_filtro.Text = "Filtro";
            // 
            // btn_fechar
            // 
            this.btn_fechar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_fechar.Location = new System.Drawing.Point(178, 252);
            this.btn_fechar.Name = "btn_fechar";
            this.btn_fechar.Size = new System.Drawing.Size(75, 23);
            this.btn_fechar.TabIndex = 16;
            this.btn_fechar.Text = "&Fechar";
            this.btn_fechar.UseVisualStyleBackColor = true;
            this.btn_fechar.Click += new System.EventHandler(this.btn_fechar_Click);
            // 
            // grb_tecnico
            // 
            this.grb_tecnico.Controls.Add(this.lbl_tecno);
            this.grb_tecnico.Controls.Add(this.btn_usuario);
            this.grb_tecnico.Controls.Add(this.text_usuario);
            this.grb_tecnico.Location = new System.Drawing.Point(16, 168);
            this.grb_tecnico.Name = "grb_tecnico";
            this.grb_tecnico.Size = new System.Drawing.Size(756, 48);
            this.grb_tecnico.TabIndex = 15;
            this.grb_tecnico.TabStop = false;
            // 
            // lbl_tecno
            // 
            this.lbl_tecno.AutoSize = true;
            this.lbl_tecno.Location = new System.Drawing.Point(6, 0);
            this.lbl_tecno.Name = "lbl_tecno";
            this.lbl_tecno.Size = new System.Drawing.Size(43, 13);
            this.lbl_tecno.TabIndex = 24;
            this.lbl_tecno.Text = "Usuário";
            // 
            // btn_usuario
            // 
            this.btn_usuario.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_usuario.Location = new System.Drawing.Point(697, 14);
            this.btn_usuario.Name = "btn_usuario";
            this.btn_usuario.Size = new System.Drawing.Size(53, 23);
            this.btn_usuario.TabIndex = 2;
            this.btn_usuario.Text = "&Usuário";
            this.btn_usuario.UseVisualStyleBackColor = true;
            this.btn_usuario.Click += new System.EventHandler(this.btn_usuario_Click);
            // 
            // text_usuario
            // 
            this.text_usuario.BackColor = System.Drawing.SystemColors.Info;
            this.text_usuario.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.text_usuario.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.text_usuario.ForeColor = System.Drawing.Color.Blue;
            this.text_usuario.Location = new System.Drawing.Point(6, 16);
            this.text_usuario.MaxLength = 100;
            this.text_usuario.Name = "text_usuario";
            this.text_usuario.ReadOnly = true;
            this.text_usuario.Size = new System.Drawing.Size(685, 20);
            this.text_usuario.TabIndex = 100;
            this.text_usuario.TabStop = false;
            // 
            // grb_listaExame
            // 
            this.grb_listaExame.Controls.Add(this.grd_selecao_exames);
            this.grb_listaExame.Location = new System.Drawing.Point(249, 26);
            this.grb_listaExame.Name = "grb_listaExame";
            this.grb_listaExame.Size = new System.Drawing.Size(529, 136);
            this.grb_listaExame.TabIndex = 14;
            this.grb_listaExame.TabStop = false;
            this.grb_listaExame.Text = "Lista de Exames";
            // 
            // grd_selecao_exames
            // 
            this.grd_selecao_exames.AllowUserToAddRows = false;
            this.grd_selecao_exames.AllowUserToDeleteRows = false;
            this.grd_selecao_exames.AllowUserToOrderColumns = true;
            this.grd_selecao_exames.AllowUserToResizeColumns = false;
            this.grd_selecao_exames.AllowUserToResizeRows = false;
            this.grd_selecao_exames.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this.grd_selecao_exames.BackgroundColor = System.Drawing.Color.White;
            this.grd_selecao_exames.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.grd_selecao_exames.Location = new System.Drawing.Point(6, 17);
            this.grd_selecao_exames.MultiSelect = false;
            this.grd_selecao_exames.Name = "grd_selecao_exames";
            this.grd_selecao_exames.ReadOnly = true;
            this.grd_selecao_exames.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.grd_selecao_exames.Size = new System.Drawing.Size(517, 109);
            this.grd_selecao_exames.TabIndex = 7;
            this.grd_selecao_exames.DataBindingComplete += new System.Windows.Forms.DataGridViewBindingCompleteEventHandler(this.grd_selecao_exames_DataBindingComplete);
            // 
            // grb_salaAtendimento
            // 
            this.grb_salaAtendimento.Controls.Add(this.cb_sala);
            this.grb_salaAtendimento.Location = new System.Drawing.Point(16, 72);
            this.grb_salaAtendimento.Name = "grb_salaAtendimento";
            this.grb_salaAtendimento.Size = new System.Drawing.Size(227, 44);
            this.grb_salaAtendimento.TabIndex = 13;
            this.grb_salaAtendimento.TabStop = false;
            this.grb_salaAtendimento.Text = "Sala de Atendimento";
            // 
            // cb_sala
            // 
            this.cb_sala.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cb_sala.FormattingEnabled = true;
            this.cb_sala.Location = new System.Drawing.Point(6, 17);
            this.cb_sala.Name = "cb_sala";
            this.cb_sala.Size = new System.Drawing.Size(207, 21);
            this.cb_sala.TabIndex = 1;
            // 
            // grb_situacao
            // 
            this.grb_situacao.Controls.Add(this.cb_situacao_atendimento);
            this.grb_situacao.Location = new System.Drawing.Point(16, 122);
            this.grb_situacao.Name = "grb_situacao";
            this.grb_situacao.Size = new System.Drawing.Size(227, 40);
            this.grb_situacao.TabIndex = 12;
            this.grb_situacao.TabStop = false;
            this.grb_situacao.Text = "Situação";
            // 
            // cb_situacao_atendimento
            // 
            this.cb_situacao_atendimento.BackColor = System.Drawing.SystemColors.Window;
            this.cb_situacao_atendimento.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cb_situacao_atendimento.FormattingEnabled = true;
            this.cb_situacao_atendimento.Location = new System.Drawing.Point(6, 13);
            this.cb_situacao_atendimento.Name = "cb_situacao_atendimento";
            this.cb_situacao_atendimento.Size = new System.Drawing.Size(207, 21);
            this.cb_situacao_atendimento.TabIndex = 13;
            // 
            // btn_limpar
            // 
            this.btn_limpar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_limpar.Location = new System.Drawing.Point(97, 252);
            this.btn_limpar.Name = "btn_limpar";
            this.btn_limpar.Size = new System.Drawing.Size(75, 23);
            this.btn_limpar.TabIndex = 10;
            this.btn_limpar.Text = "&Limpar";
            this.btn_limpar.UseVisualStyleBackColor = true;
            this.btn_limpar.Click += new System.EventHandler(this.btn_limpar_Click);
            // 
            // btn_pesquisar
            // 
            this.btn_pesquisar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_pesquisar.Location = new System.Drawing.Point(16, 252);
            this.btn_pesquisar.Name = "btn_pesquisar";
            this.btn_pesquisar.Size = new System.Drawing.Size(75, 23);
            this.btn_pesquisar.TabIndex = 10;
            this.btn_pesquisar.Text = "&Imprimir";
            this.btn_pesquisar.UseVisualStyleBackColor = true;
            this.btn_pesquisar.Click += new System.EventHandler(this.btn_pesquisar_Click);
            // 
            // grb_dataCriacao
            // 
            this.grb_dataCriacao.Controls.Add(this.chk_data);
            this.grb_dataCriacao.Controls.Add(this.dt_criacaoInicial);
            this.grb_dataCriacao.Controls.Add(this.dt_criacaoFinal);
            this.grb_dataCriacao.Location = new System.Drawing.Point(16, 26);
            this.grb_dataCriacao.Name = "grb_dataCriacao";
            this.grb_dataCriacao.Size = new System.Drawing.Size(227, 40);
            this.grb_dataCriacao.TabIndex = 4;
            this.grb_dataCriacao.TabStop = false;
            this.grb_dataCriacao.Text = "Período de Atendimento";
            // 
            // chk_data
            // 
            this.chk_data.AutoSize = true;
            this.chk_data.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.chk_data.Location = new System.Drawing.Point(6, 19);
            this.chk_data.Name = "chk_data";
            this.chk_data.Size = new System.Drawing.Size(12, 11);
            this.chk_data.TabIndex = 4;
            this.chk_data.UseVisualStyleBackColor = true;
            this.chk_data.CheckedChanged += new System.EventHandler(this.chk_data_CheckedChanged);
            // 
            // dt_criacaoInicial
            // 
            this.dt_criacaoInicial.Checked = false;
            this.dt_criacaoInicial.Enabled = false;
            this.dt_criacaoInicial.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dt_criacaoInicial.Location = new System.Drawing.Point(24, 14);
            this.dt_criacaoInicial.Name = "dt_criacaoInicial";
            this.dt_criacaoInicial.Size = new System.Drawing.Size(96, 20);
            this.dt_criacaoInicial.TabIndex = 5;
            // 
            // dt_criacaoFinal
            // 
            this.dt_criacaoFinal.Checked = false;
            this.dt_criacaoFinal.Enabled = false;
            this.dt_criacaoFinal.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dt_criacaoFinal.Location = new System.Drawing.Point(126, 14);
            this.dt_criacaoFinal.Name = "dt_criacaoFinal";
            this.dt_criacaoFinal.Size = new System.Drawing.Size(96, 20);
            this.dt_criacaoFinal.TabIndex = 6;
            // 
            // frm_RelAcompanhamentoAtendimento
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(794, 568);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "frm_RelAcompanhamentoAtendimento";
            this.Text = "RELATÓRIO DE ACOMPANHAMENTO DE ATENDIMENTO";
            this.panel.ResumeLayout(false);
            this.grb_filtro.ResumeLayout(false);
            this.grb_tecnico.ResumeLayout(false);
            this.grb_tecnico.PerformLayout();
            this.grb_listaExame.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.grd_selecao_exames)).EndInit();
            this.grb_salaAtendimento.ResumeLayout(false);
            this.grb_situacao.ResumeLayout(false);
            this.grb_dataCriacao.ResumeLayout(false);
            this.grb_dataCriacao.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox grb_filtro;
        private System.Windows.Forms.GroupBox grb_situacao;
        public System.Windows.Forms.ComboBox cb_situacao_atendimento;
        private System.Windows.Forms.Button btn_limpar;
        private System.Windows.Forms.Button btn_pesquisar;
        private System.Windows.Forms.GroupBox grb_dataCriacao;
        private System.Windows.Forms.CheckBox chk_data;
        private System.Windows.Forms.DateTimePicker dt_criacaoInicial;
        private System.Windows.Forms.DateTimePicker dt_criacaoFinal;
        private System.Windows.Forms.GroupBox grb_salaAtendimento;
        private System.Windows.Forms.ComboBox cb_sala;
        private System.Windows.Forms.GroupBox grb_listaExame;
        private System.Windows.Forms.DataGridView grd_selecao_exames;
        protected System.Windows.Forms.GroupBox grb_tecnico;
        protected System.Windows.Forms.Label lbl_tecno;
        protected System.Windows.Forms.Button btn_usuario;
        public System.Windows.Forms.TextBox text_usuario;
        private System.Windows.Forms.Button btn_fechar;
    }
}