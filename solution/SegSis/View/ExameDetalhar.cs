﻿using SWS.Entidade;
using SWS.Facade;
using SWS.View.ViewHelper;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace SWS.View
{
    public partial class frmExameDetalhar : frmExameIncluir
    {
        public frmExameDetalhar(Exame exame) : base()
        {
            InitializeComponent();

            text_descricao.Text = exame.Descricao;
            text_descricao.ReadOnly = true;

            text_preco.Text = ValidaCampoHelper.FormataValorMonetario(Convert.ToString(exame.Preco));
            text_preco.ReadOnly = true;
            
            text_precoCusto.Text = ValidaCampoHelper.FormataValorMonetario(Convert.ToString(exame.Custo));
            text_precoCusto.ReadOnly = true;
            
            text_prioridade.Text = Convert.ToString(exame.Prioridade);
            text_prioridade.ReadOnly = true;

            cbExterno.SelectedValue = exame.Externo == true ? "true" : "false";
            cbExterno.Enabled = false;

            cbLaboratorio.SelectedValue = exame.Laboratorio == true ? "true" : "false";
            cbLaboratorio.Enabled = false;

            cbDocumento.SelectedValue = exame.LiberaDocumento == true ? "true" : "false";
            cbDocumento.Enabled = false;

            cbPadraoContrato.SelectedValue = exame.PadraoContrato == true ? "true" : "false";
            cbPadraoContrato.Enabled = false;

            btn_gravar.Enabled = false;
            btn_limpar.Enabled = false;

            EstudoFacade pcmsoFacade = EstudoFacade.getInstance();
            MedicoExame = pcmsoFacade.findAllMedicoExameByExame(exame);
            MontaGridMedicoExame();
            dgvMedico.Enabled = false;
            btIncluirMedico.Enabled = false;
            btExcluirMedico.Enabled = false;

            cbPeriodoVencimento.SelectedValue = Convert.ToString(exame.PeriodoVencimento);
            cbPeriodoVencimento.Enabled = false;
        }
    }
}
