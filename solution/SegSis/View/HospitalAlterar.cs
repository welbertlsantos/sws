﻿using SWS.Entidade;
using SWS.Facade;
using SWS.View.Resources;
using SWS.ViewHelper;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace SWS.View
{
    public partial class frmHospitalAlterar : frmHospitalIncluir
    {
        
        
        public frmHospitalAlterar(Hospital hospital) :base()
        {
            InitializeComponent();
            this.Hospital = hospital;

            textNome.Text = Hospital.Nome;
            textEndereco.Text = Hospital.Endereco;
            textNumero.Text = Hospital.Numero;
            textComplemento.Text = Hospital.Complemento;
            textBairro.Text = Hospital.Bairro;
            textCep.Text = Hospital.Cep;
            cbUf.SelectedValue = Hospital.Uf;
            textCidade.Text = Hospital.Cidade != null ? Hospital.Cidade.Nome : string.Empty;
            Cidade = Hospital.Cidade;
            textTelefone.Text = Hospital.Telefone1;
            textTelefone2.Text = hospital.Telefone2;
            textObservacao.Text = hospital.Observacao;
        }


        protected override void btnGravar_Click(object sender, EventArgs e)
        {
            try
            {
                if (validaCamposObrigatorio())
                {
                    EstudoFacade pcmsoFacade = EstudoFacade.getInstance();
                    Hospital = pcmsoFacade.updateHospital(new Hospital(Hospital.Id, textNome.Text, textEndereco.Text, textNumero.Text, textComplemento.Text, textBairro.Text, textCep.Text, ((SelectItem)cbUf.SelectedItem).Valor, textTelefone.Text, textTelefone2.Text, textObservacao.Text, ApplicationConstants.ATIVO, Cidade));
                    MessageBox.Show("Hospital alterado com sucesso.", "Confirmação", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    this.Close();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }
    }
}
