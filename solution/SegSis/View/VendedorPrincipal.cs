﻿using SWS.Entidade;
using SWS.Facade;
using SWS.View.Resources;
using SWS.View.ViewHelper;
using SWS.ViewHelper;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace SWS.View
{
    public partial class frmVendedorPrincipal : frmTemplate
    {
        private Vendedor vendedor;

        public Vendedor Vendedor
        {
            get { return vendedor; }
            set { vendedor = value; }
        }

        private CidadeIbge cidade;

        private void validaPermissoes()
        {
            PermissionamentoFacade permissionamentoFacade = PermissionamentoFacade.getInstance();

            btnAlterar.Enabled = permissionamentoFacade.hasPermission("VENDEDOR", "ALTERAR");
            btnIncluir.Enabled = permissionamentoFacade.hasPermission("VENDEDOR", "INCLUIR");
            btnExcluir.Enabled = permissionamentoFacade.hasPermission("VENDEDOR", "EXCLUIR");
            btnDetalhar.Enabled = permissionamentoFacade.hasPermission("VENDEDOR", "DETALHAR");
            btnReativar.Enabled = permissionamentoFacade.hasPermission("VENDEDOR", "REATIVAR");
        }
        
        public frmVendedorPrincipal()
        {
            InitializeComponent();
            validaPermissoes();
            ComboHelper.unidadeFederativa(cbUf);
            ComboHelper.situacao(cbSituacao);
        }

        private void btnFechar_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnIncluir_Click(object sender, EventArgs e)
        {
            frmVendedorIncluir incluirVendedor = new frmVendedorIncluir();
            incluirVendedor.ShowDialog();

            if (incluirVendedor.Vendedor != null)
            {
                this.Vendedor = incluirVendedor.Vendedor;
                montaDataGrid();
            }
                
        }

        public void montaDataGrid()
        {
            try
            {
                this.Cursor = Cursors.WaitCursor;

                VendedorFacade vendedorFacade = VendedorFacade.getInstance();

                dgvVendedores.Columns.Clear();

                DataSet ds = vendedorFacade.findVendedorByFilter(new Vendedor(null, textNome.Text, textCpf.Text, string.Empty, string.Empty, string.Empty, string.Empty, string.Empty, string.Empty, (((SelectItem)cbUf.SelectedItem).Valor), string.Empty, string.Empty, string.Empty, string.Empty, null, null, (((SelectItem)cbSituacao.SelectedItem).Valor), cidade));

                dgvVendedores.DataSource = ds.Tables["Vendedores"].DefaultView;

                dgvVendedores.Columns[0].HeaderText = "ID";
                dgvVendedores.Columns[0].Visible = false;

                dgvVendedores.Columns[1].HeaderText = "Nome";
                dgvVendedores.Columns[2].HeaderText = "Situação";
                dgvVendedores.Columns[2].Visible = false;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }

        }

        private void btnAlterar_Click(object sender, EventArgs e)
        {
            try
            {
                if (dgvVendedores.CurrentRow == null)
                    throw new Exception("Selecione uma linha.");

                VendedorFacade vendedorFacade = VendedorFacade.getInstance();

                Vendedor vendedorAlterar = vendedorFacade.findVendedorById((long)dgvVendedores.CurrentRow.Cells[0].Value);

                if (!string.Equals(vendedorAlterar.Situacao, ApplicationConstants.ATIVO))
                    throw new Exception("Somente vendedores ativos podem ser alterados");

                frmVendedorAlterar alterarVendedor = new frmVendedorAlterar(vendedorAlterar);
                alterarVendedor.ShowDialog();

                if (alterarVendedor.FlagAlteraVendedor == true)
                {
                    this.Vendedor = alterarVendedor.Vendedor;
                    montaDataGrid();
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }

        private void btnExcluir_Click(object sender, EventArgs e)
        {
            try
            {
                if (dgvVendedores.CurrentRow == null)
                    throw new Exception("Selecione uma linha.");

                if (MessageBox.Show("Deseja realmente desativar o vendedor", "Atenção", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                {
                    VendedorFacade vendedorFacade = VendedorFacade.getInstance();

                    Vendedor vendedorExcluir = vendedorFacade.findVendedorById((long)dgvVendedores.CurrentRow.Cells[0].Value);

                    if (!string.Equals(vendedorExcluir.Situacao, ApplicationConstants.ATIVO))
                        throw new Exception("Somente vendedores ativos podem ser excluídos / desativados.");

                    vendedorExcluir.Situacao = ApplicationConstants.DESATIVADO;

                    this.Vendedor = vendedorFacade.updateVendedor(vendedorExcluir);

                    MessageBox.Show("Vendedor desativado com sucesso.", "Confirmação", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    montaDataGrid();
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }

        private void btnPesquisar_Click(object sender, EventArgs e)
        {
            montaDataGrid();
        }

        private void btnReativar_Click(object sender, EventArgs e)
        {
            try
            {
                if (dgvVendedores.CurrentRow == null)
                    throw new Exception("Selecione uma linha.");
                
                VendedorFacade vendedorFacade = VendedorFacade.getInstance();

                Vendedor vendedorReativar = vendedorFacade.findVendedorById((long)dgvVendedores.CurrentRow.Cells[0].Value);

                if (!string.Equals(vendedorReativar.Situacao, ApplicationConstants.DESATIVADO))
                    throw new Exception("Somente vendedores desativados podem ser reativados.");

                vendedorReativar.Situacao = ApplicationConstants.ATIVO;

                this.Vendedor = vendedorFacade.updateVendedor(vendedorReativar);

                MessageBox.Show("Vendedor reativado com sucesso.", "Confirmação", MessageBoxButtons.OK, MessageBoxIcon.Information);
                montaDataGrid();

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }

        private void btnDetalhar_Click(object sender, EventArgs e)
        {
            try
            {
                if (dgvVendedores.CurrentRow == null)
                    throw new Exception("Selecione uma linha.");

                VendedorFacade vendedorFacade = VendedorFacade.getInstance();

                Vendedor vendedorDetalhar = vendedorFacade.findVendedorById((long)dgvVendedores.CurrentRow.Cells[0].Value);

                frmVendedorDetalhar detalharVendedor = new frmVendedorDetalhar(vendedorDetalhar);
                detalharVendedor.ShowDialog();

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }

        private void btnCidade_Click(object sender, EventArgs e)
        {
            try
            {
                if (this.cbUf.SelectedIndex == 0)
                    throw new Exception("você deve selecionar a unidade federativa");

                frmCidadePesquisa cidadeSeleciona = new frmCidadePesquisa(((SelectItem)this.cbUf.SelectedItem).Valor.ToString());
                cidadeSeleciona.ShowDialog();

                if (cidadeSeleciona.Cidade != null)
                {
                    this.cidade = cidadeSeleciona.Cidade;
                    textCidade.Text = cidade.Nome;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }

        private void btnExcluirCidade_Click(object sender, EventArgs e)
        {
            try
            {
                if (cidade == null)
                    throw new Exception("Você deve primeiro selecionar uma cidade");

                this.cidade = null;
                textCidade.Text = string.Empty;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }

        private void dgvVendedores_RowPrePaint(object sender, DataGridViewRowPrePaintEventArgs e)
        {
            DataGridView dgv = sender as DataGridView;

            if (string.Equals(dgv.Rows[e.RowIndex].Cells[2].Value.ToString(), ApplicationConstants.DESATIVADO))
                dgv.Rows[e.RowIndex].DefaultCellStyle.ForeColor = Color.Red;
        }

        private void dgvVendedores_DoubleClick(object sender, EventArgs e)
        {
            if (dgvVendedores.CurrentRow != null) btnAlterar.PerformClick();
        }

    }
}
