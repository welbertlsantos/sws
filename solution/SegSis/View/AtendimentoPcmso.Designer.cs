﻿namespace SWS.View
{
    partial class frmAtendimentoPcmso
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            this.flpAcao = new System.Windows.Forms.FlowLayoutPanel();
            this.btConfirma = new System.Windows.Forms.Button();
            this.btAltera = new System.Windows.Forms.Button();
            this.btNovo = new System.Windows.Forms.Button();
            this.btFechar = new System.Windows.Forms.Button();
            this.grb_pcmso = new System.Windows.Forms.GroupBox();
            this.lbl_green = new System.Windows.Forms.Label();
            this.lbl_pcmsoAvulso = new System.Windows.Forms.Label();
            this.dgvPcmso = new System.Windows.Forms.DataGridView();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.scForm)).BeginInit();
            this.scForm.Panel1.SuspendLayout();
            this.scForm.Panel2.SuspendLayout();
            this.scForm.SuspendLayout();
            this.pnlForm.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.banner)).BeginInit();
            this.flpAcao.SuspendLayout();
            this.grb_pcmso.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvPcmso)).BeginInit();
            this.SuspendLayout();
            // 
            // scForm
            // 
            // 
            // scForm.Panel1
            // 
            this.scForm.Panel1.Controls.Add(this.flpAcao);
            this.scForm.Size = new System.Drawing.Size(685, 320);
            // 
            // pnlForm
            // 
            this.pnlForm.Controls.Add(this.label1);
            this.pnlForm.Controls.Add(this.label2);
            this.pnlForm.Controls.Add(this.grb_pcmso);
            this.pnlForm.Size = new System.Drawing.Size(665, 288);
            // 
            // banner
            // 
            this.banner.Size = new System.Drawing.Size(685, 41);
            // 
            // flpAcao
            // 
            this.flpAcao.Controls.Add(this.btConfirma);
            this.flpAcao.Controls.Add(this.btAltera);
            this.flpAcao.Controls.Add(this.btNovo);
            this.flpAcao.Controls.Add(this.btFechar);
            this.flpAcao.Dock = System.Windows.Forms.DockStyle.Fill;
            this.flpAcao.Location = new System.Drawing.Point(0, 0);
            this.flpAcao.Name = "flpAcao";
            this.flpAcao.Size = new System.Drawing.Size(685, 30);
            this.flpAcao.TabIndex = 0;
            // 
            // btConfirma
            // 
            this.btConfirma.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btConfirma.Image = global::SWS.Properties.Resources.fechar_ico;
            this.btConfirma.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btConfirma.Location = new System.Drawing.Point(3, 3);
            this.btConfirma.Name = "btConfirma";
            this.btConfirma.Size = new System.Drawing.Size(75, 23);
            this.btConfirma.TabIndex = 8;
            this.btConfirma.Text = "&Confirmar";
            this.btConfirma.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btConfirma.UseVisualStyleBackColor = true;
            this.btConfirma.Click += new System.EventHandler(this.btn_incluir_Click);
            // 
            // btAltera
            // 
            this.btAltera.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btAltera.Image = global::SWS.Properties.Resources.Alterar;
            this.btAltera.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btAltera.Location = new System.Drawing.Point(84, 3);
            this.btAltera.Name = "btAltera";
            this.btAltera.Size = new System.Drawing.Size(75, 23);
            this.btAltera.TabIndex = 9;
            this.btAltera.Text = "&Alterar";
            this.btAltera.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btAltera.UseVisualStyleBackColor = true;
            this.btAltera.Click += new System.EventHandler(this.btn_alterar_Click);
            // 
            // btNovo
            // 
            this.btNovo.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btNovo.Image = global::SWS.Properties.Resources.icone_mais;
            this.btNovo.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btNovo.Location = new System.Drawing.Point(165, 3);
            this.btNovo.Name = "btNovo";
            this.btNovo.Size = new System.Drawing.Size(75, 23);
            this.btNovo.TabIndex = 7;
            this.btNovo.Text = "&Novo";
            this.btNovo.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btNovo.UseVisualStyleBackColor = true;
            this.btNovo.Click += new System.EventHandler(this.btn_novo_Click);
            // 
            // btFechar
            // 
            this.btFechar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btFechar.Image = global::SWS.Properties.Resources.close;
            this.btFechar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btFechar.Location = new System.Drawing.Point(246, 3);
            this.btFechar.Name = "btFechar";
            this.btFechar.Size = new System.Drawing.Size(75, 23);
            this.btFechar.TabIndex = 11;
            this.btFechar.Text = "&Fechar";
            this.btFechar.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btFechar.UseVisualStyleBackColor = true;
            this.btFechar.Click += new System.EventHandler(this.btn_cancelar_Click);
            // 
            // grb_pcmso
            // 
            this.grb_pcmso.Controls.Add(this.lbl_green);
            this.grb_pcmso.Controls.Add(this.lbl_pcmsoAvulso);
            this.grb_pcmso.Controls.Add(this.dgvPcmso);
            this.grb_pcmso.Location = new System.Drawing.Point(10, 3);
            this.grb_pcmso.Name = "grb_pcmso";
            this.grb_pcmso.Size = new System.Drawing.Size(652, 252);
            this.grb_pcmso.TabIndex = 1;
            this.grb_pcmso.TabStop = false;
            this.grb_pcmso.Text = "PCMSO\'s";
            // 
            // lbl_green
            // 
            this.lbl_green.AutoSize = true;
            this.lbl_green.BackColor = System.Drawing.Color.Green;
            this.lbl_green.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lbl_green.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.lbl_green.Location = new System.Drawing.Point(6, 307);
            this.lbl_green.Name = "lbl_green";
            this.lbl_green.Size = new System.Drawing.Size(12, 15);
            this.lbl_green.TabIndex = 2;
            this.lbl_green.Text = " ";
            // 
            // lbl_pcmsoAvulso
            // 
            this.lbl_pcmsoAvulso.AutoSize = true;
            this.lbl_pcmsoAvulso.BackColor = System.Drawing.Color.Transparent;
            this.lbl_pcmsoAvulso.ForeColor = System.Drawing.Color.Green;
            this.lbl_pcmsoAvulso.Location = new System.Drawing.Point(24, 309);
            this.lbl_pcmsoAvulso.Name = "lbl_pcmsoAvulso";
            this.lbl_pcmsoAvulso.Size = new System.Drawing.Size(82, 13);
            this.lbl_pcmsoAvulso.TabIndex = 1;
            this.lbl_pcmsoAvulso.Text = "PCMSO avulso.";
            // 
            // dgvPcmso
            // 
            this.dgvPcmso.AllowUserToAddRows = false;
            this.dgvPcmso.AllowUserToDeleteRows = false;
            this.dgvPcmso.AllowUserToOrderColumns = true;
            this.dgvPcmso.AllowUserToResizeRows = false;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.Gainsboro;
            this.dgvPcmso.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle1;
            this.dgvPcmso.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this.dgvPcmso.BackgroundColor = System.Drawing.Color.White;
            this.dgvPcmso.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.Color.LightGray;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvPcmso.DefaultCellStyle = dataGridViewCellStyle2;
            this.dgvPcmso.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvPcmso.Location = new System.Drawing.Point(3, 16);
            this.dgvPcmso.MultiSelect = false;
            this.dgvPcmso.Name = "dgvPcmso";
            this.dgvPcmso.ReadOnly = true;
            this.dgvPcmso.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvPcmso.Size = new System.Drawing.Size(646, 233);
            this.dgvPcmso.TabIndex = 0;
            this.dgvPcmso.CellMouseDoubleClick += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.dgvPcmso_CellMouseDoubleClick);
            this.dgvPcmso.DataBindingComplete += new System.Windows.Forms.DataGridViewBindingCompleteEventHandler(this.dgvPcmso_DataBindingComplete);
            this.dgvPcmso.RowPrePaint += new System.Windows.Forms.DataGridViewRowPrePaintEventHandler(this.dgvPcmso_RowPrePaint);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Green;
            this.label1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.label1.Location = new System.Drawing.Point(9, 262);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(12, 15);
            this.label1.TabIndex = 4;
            this.label1.Text = " ";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.Transparent;
            this.label2.ForeColor = System.Drawing.Color.Green;
            this.label2.Location = new System.Drawing.Point(27, 264);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(82, 13);
            this.label2.TabIndex = 3;
            this.label2.Text = "PCMSO avulso.";
            // 
            // frmAtendimentoPcmso
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.ClientSize = new System.Drawing.Size(684, 361);
            this.Name = "frmAtendimentoPcmso";
            this.Text = "SELECIONAR PCMSO";
            this.scForm.Panel1.ResumeLayout(false);
            this.scForm.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.scForm)).EndInit();
            this.scForm.ResumeLayout(false);
            this.pnlForm.ResumeLayout(false);
            this.pnlForm.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.banner)).EndInit();
            this.flpAcao.ResumeLayout(false);
            this.grb_pcmso.ResumeLayout(false);
            this.grb_pcmso.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvPcmso)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.FlowLayoutPanel flpAcao;
        private System.Windows.Forms.Button btFechar;
        private System.Windows.Forms.Button btAltera;
        private System.Windows.Forms.Button btNovo;
        private System.Windows.Forms.Button btConfirma;
        private System.Windows.Forms.GroupBox grb_pcmso;
        private System.Windows.Forms.Label lbl_green;
        private System.Windows.Forms.Label lbl_pcmsoAvulso;
        private System.Windows.Forms.DataGridView dgvPcmso;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
    }
}
