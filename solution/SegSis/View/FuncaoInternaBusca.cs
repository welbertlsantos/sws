﻿using SWS.Entidade;
using SWS.Facade;
using SWS.View.Resources;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace SWS.View
{
    public partial class frmFuncaoInternaBusca : frmTemplateConsulta
    {
        private FuncaoInterna funcaoInterna;

        public FuncaoInterna FuncaoInterna
        {
            get { return funcaoInterna; }
            set { funcaoInterna = value; }
        }
        
        public frmFuncaoInternaBusca()
        {
            InitializeComponent();
        }

        private void btn_ok_Click(object sender, EventArgs e)
        {
            try
            {
                if (grd_funcaoInterna.CurrentRow == null)
                    throw new Exception("Selecione uma função interna.");

                UsuarioFacade usuarioFacade = UsuarioFacade.getInstance();

                funcaoInterna = usuarioFacade.findFuncaoInternaById((Int64)grd_funcaoInterna.CurrentRow.Cells[0].Value);

                
                this.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btn_novo_Click(object sender, EventArgs e)
        {
            frmFuncaoInternaIncluir incluirFuncao = new frmFuncaoInternaIncluir();
            incluirFuncao.ShowDialog();

            if (incluirFuncao.FuncaoInterna != null)
            {
                funcaoInterna = incluirFuncao.FuncaoInterna;
                this.Close();
            }
        }

        private void bt_buscar_Click(object sender, EventArgs e)
        {
            try
            {
                montaDataGrid();
                text_nome.Focus();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btn_fechar_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void montaDataGrid()
        {
            try
            {
                UsuarioFacade usuarioFacade = UsuarioFacade.getInstance();

                DataSet ds = usuarioFacade.FindFuncaoInternaByFilter(new FuncaoInterna(null, text_nome.Text.Trim(), ApplicationConstants.ATIVO, string.Empty));

                grd_funcaoInterna.DataSource = ds.Tables["FuncoesInternas"].DefaultView;

                grd_funcaoInterna.Columns[0].HeaderText = "ID";
                grd_funcaoInterna.Columns[0].Visible = false;

                grd_funcaoInterna.Columns[1].HeaderText = "Descrição";
                
                grd_funcaoInterna.Columns[2].HeaderText = "Situação";
                grd_funcaoInterna.Columns[2].Visible = false;

                grd_funcaoInterna.Columns[3].HeaderText = "Privado";
                grd_funcaoInterna.Columns[3].Visible = false;

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }

        private void grd_funcaoInterna_DataBindingComplete(object sender, DataGridViewBindingCompleteEventArgs e)
        {
            DataGridView dgv = sender as DataGridView;
            dgv.ClearSelection();
        }

        private void grd_funcaoInterna_CellMouseDoubleClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            try
            {
                btn_ok.PerformClick();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }
    }
}
