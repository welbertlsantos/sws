﻿using SWS.Entidade;
using SWS.Facade;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace SWS.View
{
    public partial class frmDocumentoBuscar : frmTemplateConsulta
    {

        private Documento documento;

        public Documento Documento
        {
            get { return documento; }
            set { documento = value; }
        }
        
        
        public frmDocumentoBuscar()
        {
            InitializeComponent();
            montaGrid();
        }

        private void btnConfirmar_Click(object sender, EventArgs e)
        {
            try
            {
                if (dgvDocumento.CurrentRow == null)
                    throw new Exception("Você não selecionou nenhum documento");

                documento = new Documento((long)dgvDocumento.CurrentRow.Cells[0].Value, (string)dgvDocumento.CurrentRow.Cells[1].Value, (string)dgvDocumento.CurrentRow.Cells[2].Value);
                this.Close();

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }

        private void btnFechar_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void montaGrid()
        {
            try
            {
                this.dgvDocumento.Columns.Clear();
                ProtocoloFacade protocoloFacade = ProtocoloFacade.getInstance();

                this.dgvDocumento.ColumnCount = 3;

                this.dgvDocumento.Columns[0].HeaderText = "id";
                this.dgvDocumento.Columns[0].Visible = false;
                this.dgvDocumento.Columns[1].HeaderText = "Documento";
                this.dgvDocumento.Columns[2].HeaderText = "Situacao";
                this.dgvDocumento.Columns[2].Visible = false;

                foreach (Documento dvRow in protocoloFacade.findAllDocumentos())
                    dgvDocumento.Rows.Add((long)dvRow.Id, dvRow.Descricao, dvRow.Situacao);

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }

        private void btnNovo_Click(object sender, EventArgs e)
        {
            frmDocumentoIncluir incluirDocumento = new frmDocumentoIncluir();
            incluirDocumento.ShowDialog();

            if (incluirDocumento.Documento != null)
            {
                montaGrid();
            }
        }
    }
}
