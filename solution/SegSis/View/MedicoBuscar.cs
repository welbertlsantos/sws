﻿using SWS.Entidade;
using SWS.Facade;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace SWS.View
{
    public partial class frmMedicoBuscar : frmTemplateConsulta
    {
        private Medico medico;

        public Medico Medico
        {
            get { return medico; }
            set { medico = value; }
        }
        
        public frmMedicoBuscar()
        {
            InitializeComponent();
            validaPermissoes();
        }

        private void btn_ok_Click(object sender, EventArgs e)
        {
            try
            {
                if (grd_medico.CurrentRow == null)
                    throw new Exception("Selecione uma linha.");
                
                EstudoFacade pcmsoFacade = EstudoFacade.getInstance();

                medico = pcmsoFacade.findMedicoById((Int64)grd_medico.CurrentRow.Cells[0].Value);
                
                this.Close();

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btn_novo_Click(object sender, EventArgs e)
        {
            frmMedicoIncluir formMedicoIncluir = new frmMedicoIncluir();
            formMedicoIncluir.ShowDialog();

            if (formMedicoIncluir.Medico != null)
            {
                medico = formMedicoIncluir.Medico;
                this.Close();
            }
        }

        private void btn_buscar_Click(object sender, EventArgs e)
        {
            try
            {
                this.grd_medico.Columns.Clear();
                montaDataGrid();
                text_nome.Focus();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btn_fechar_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void grd_medico_DataBindingComplete(object sender, DataGridViewBindingCompleteEventArgs e)
        {
            DataGridView dgv = sender as DataGridView;
            dgv.ClearSelection();
        }

        public void montaDataGrid()
        {
            try
            {
                EstudoFacade pcmsoFacade = EstudoFacade.getInstance();

                this.Cursor = Cursors.WaitCursor;

                DataSet ds = pcmsoFacade.findMedicoByFilter(new Medico(null, text_nome.Text, string.Empty, "A", string.Empty, string.Empty,string.Empty, string.Empty, string.Empty, string.Empty, string.Empty, string.Empty, string.Empty, null, string.Empty, string.Empty, string.Empty, null, string.Empty, string.Empty, string.Empty));

                grd_medico.DataSource = ds.Tables["Medicos"].DefaultView;

                grd_medico.Columns[0].HeaderText = "ID";
                grd_medico.Columns[0].Visible = false;

                grd_medico.Columns[1].HeaderText = "Nome";
                grd_medico.Columns[2].HeaderText = "CRM";

                grd_medico.Columns[3].HeaderText = "Situação";
                grd_medico.Columns[3].Visible = false;

                grd_medico.Columns[4].HeaderText = "Telefone";
                grd_medico.Columns[5].HeaderText = "Celular";
                grd_medico.Columns[6].HeaderText = "Email";
                grd_medico.Columns[7].HeaderText = "Endereço";
                grd_medico.Columns[8].HeaderText = "Número";
                grd_medico.Columns[9].HeaderText = "Complemento";
                grd_medico.Columns[10].HeaderText = "Bairro";
                grd_medico.Columns[11].HeaderText = "CEP";
                grd_medico.Columns[12].HeaderText = "UF";
                grd_medico.Columns[13].HeaderText = "Cidade";
                grd_medico.Columns[14].HeaderText = "PisPasep";
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }

        private void validaPermissoes()
        {
            PermissionamentoFacade permissionamentoFacade = PermissionamentoFacade.getInstance();

            this.btn_novo.Enabled = permissionamentoFacade.hasPermission("MEDICO", "INCLUIR");
        }

        private void grd_medico_CellMouseDoubleClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            try
            {
                btn_ok.PerformClick();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }
    }
}
