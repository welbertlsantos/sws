﻿namespace SWS.View
{
    partial class frmEmpresaIncluir
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.flpAcao = new System.Windows.Forms.FlowLayoutPanel();
            this.btn_gravar = new System.Windows.Forms.Button();
            this.lbl_limpar = new System.Windows.Forms.Button();
            this.btn_fechar = new System.Windows.Forms.Button();
            this.lblRazaoSocial = new System.Windows.Forms.TextBox();
            this.lblFantasia = new System.Windows.Forms.TextBox();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.lblInscricao = new System.Windows.Forms.TextBox();
            this.lblEndereco = new System.Windows.Forms.TextBox();
            this.lblNumero = new System.Windows.Forms.TextBox();
            this.lblComplemento = new System.Windows.Forms.TextBox();
            this.lblBairro = new System.Windows.Forms.TextBox();
            this.lblCep = new System.Windows.Forms.TextBox();
            this.lblUF = new System.Windows.Forms.TextBox();
            this.lblCidade = new System.Windows.Forms.TextBox();
            this.lblTelefone = new System.Windows.Forms.TextBox();
            this.lblFax = new System.Windows.Forms.TextBox();
            this.lblEmail = new System.Windows.Forms.TextBox();
            this.lblSite = new System.Windows.Forms.TextBox();
            this.bt_abrir_file_dialog = new System.Windows.Forms.Button();
            this.pic_box_logo = new System.Windows.Forms.PictureBox();
            this.lblSimples = new System.Windows.Forms.TextBox();
            this.text_razaoSocial = new System.Windows.Forms.TextBox();
            this.text_inscricao = new System.Windows.Forms.TextBox();
            this.text_fantasia = new System.Windows.Forms.TextBox();
            this.text_cnpj = new System.Windows.Forms.MaskedTextBox();
            this.text_telefone2 = new System.Windows.Forms.TextBox();
            this.text_endereco = new System.Windows.Forms.TextBox();
            this.text_telefone1 = new System.Windows.Forms.TextBox();
            this.text_site = new System.Windows.Forms.TextBox();
            this.text_numero = new System.Windows.Forms.TextBox();
            this.text_email = new System.Windows.Forms.TextBox();
            this.text_cep = new System.Windows.Forms.MaskedTextBox();
            this.text_complemento = new System.Windows.Forms.TextBox();
            this.text_bairro = new System.Windows.Forms.TextBox();
            this.cbUf = new SWS.ComboBoxWithBorder();
            this.cbCidade = new SWS.ComboBoxWithBorder();
            this.cbSimples = new SWS.ComboBoxWithBorder();
            this.empresaErrorProvider = new System.Windows.Forms.ErrorProvider(this.components);
            this.lbl_matriz = new System.Windows.Forms.TextBox();
            this.textMatriz = new System.Windows.Forms.TextBox();
            this.btnMatriz = new System.Windows.Forms.Button();
            this.lblCodigoCNES = new System.Windows.Forms.TextBox();
            this.textCodigoCnes = new System.Windows.Forms.TextBox();
            this.lblVlrMinimoImpostoFederal = new System.Windows.Forms.TextBox();
            this.textValorMinimoImpostoFederal = new System.Windows.Forms.TextBox();
            this.lblVlrMinimoIR = new System.Windows.Forms.TextBox();
            this.textValorMinimoIR = new System.Windows.Forms.TextBox();
            this.lblAliquotaPis = new System.Windows.Forms.TextBox();
            this.textAliquotaPIS = new System.Windows.Forms.TextBox();
            this.lblAliquotaCofins = new System.Windows.Forms.TextBox();
            this.textAliquotaCOFINS = new System.Windows.Forms.TextBox();
            this.lblAliquotaIR = new System.Windows.Forms.TextBox();
            this.textAliquotaIR = new System.Windows.Forms.TextBox();
            this.lblAliquotaCsll = new System.Windows.Forms.TextBox();
            this.textAliquotaCSLL = new System.Windows.Forms.TextBox();
            this.textAliquotaISS = new System.Windows.Forms.TextBox();
            this.lblAliquotaISS = new System.Windows.Forms.TextBox();
            ((System.ComponentModel.ISupportInitialize)(this.spForm)).BeginInit();
            this.spForm.Panel1.SuspendLayout();
            this.spForm.Panel2.SuspendLayout();
            this.spForm.SuspendLayout();
            this.pnlForm.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.banner)).BeginInit();
            this.flpAcao.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pic_box_logo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.empresaErrorProvider)).BeginInit();
            this.SuspendLayout();
            // 
            // spForm
            // 
            // 
            // spForm.Panel1
            // 
            this.spForm.Panel1.Controls.Add(this.flpAcao);
            // 
            // pnlForm
            // 
            this.pnlForm.Controls.Add(this.textAliquotaISS);
            this.pnlForm.Controls.Add(this.lblAliquotaISS);
            this.pnlForm.Controls.Add(this.textAliquotaCSLL);
            this.pnlForm.Controls.Add(this.lblAliquotaCsll);
            this.pnlForm.Controls.Add(this.textAliquotaIR);
            this.pnlForm.Controls.Add(this.lblAliquotaIR);
            this.pnlForm.Controls.Add(this.textAliquotaCOFINS);
            this.pnlForm.Controls.Add(this.lblAliquotaCofins);
            this.pnlForm.Controls.Add(this.textAliquotaPIS);
            this.pnlForm.Controls.Add(this.lblAliquotaPis);
            this.pnlForm.Controls.Add(this.textValorMinimoIR);
            this.pnlForm.Controls.Add(this.lblVlrMinimoIR);
            this.pnlForm.Controls.Add(this.textValorMinimoImpostoFederal);
            this.pnlForm.Controls.Add(this.lblVlrMinimoImpostoFederal);
            this.pnlForm.Controls.Add(this.textCodigoCnes);
            this.pnlForm.Controls.Add(this.lblCodigoCNES);
            this.pnlForm.Controls.Add(this.btnMatriz);
            this.pnlForm.Controls.Add(this.textMatriz);
            this.pnlForm.Controls.Add(this.lbl_matriz);
            this.pnlForm.Controls.Add(this.cbSimples);
            this.pnlForm.Controls.Add(this.cbCidade);
            this.pnlForm.Controls.Add(this.cbUf);
            this.pnlForm.Controls.Add(this.text_razaoSocial);
            this.pnlForm.Controls.Add(this.text_inscricao);
            this.pnlForm.Controls.Add(this.text_fantasia);
            this.pnlForm.Controls.Add(this.text_cnpj);
            this.pnlForm.Controls.Add(this.text_telefone2);
            this.pnlForm.Controls.Add(this.text_endereco);
            this.pnlForm.Controls.Add(this.text_telefone1);
            this.pnlForm.Controls.Add(this.text_site);
            this.pnlForm.Controls.Add(this.text_numero);
            this.pnlForm.Controls.Add(this.text_email);
            this.pnlForm.Controls.Add(this.text_cep);
            this.pnlForm.Controls.Add(this.text_complemento);
            this.pnlForm.Controls.Add(this.text_bairro);
            this.pnlForm.Controls.Add(this.lblSimples);
            this.pnlForm.Controls.Add(this.bt_abrir_file_dialog);
            this.pnlForm.Controls.Add(this.pic_box_logo);
            this.pnlForm.Controls.Add(this.lblSite);
            this.pnlForm.Controls.Add(this.lblEmail);
            this.pnlForm.Controls.Add(this.lblFax);
            this.pnlForm.Controls.Add(this.lblTelefone);
            this.pnlForm.Controls.Add(this.lblCidade);
            this.pnlForm.Controls.Add(this.lblUF);
            this.pnlForm.Controls.Add(this.lblCep);
            this.pnlForm.Controls.Add(this.lblBairro);
            this.pnlForm.Controls.Add(this.lblComplemento);
            this.pnlForm.Controls.Add(this.lblNumero);
            this.pnlForm.Controls.Add(this.lblEndereco);
            this.pnlForm.Controls.Add(this.lblInscricao);
            this.pnlForm.Controls.Add(this.textBox1);
            this.pnlForm.Controls.Add(this.lblFantasia);
            this.pnlForm.Controls.Add(this.lblRazaoSocial);
            this.pnlForm.Size = new System.Drawing.Size(764, 706);
            // 
            // flpAcao
            // 
            this.flpAcao.Controls.Add(this.btn_gravar);
            this.flpAcao.Controls.Add(this.lbl_limpar);
            this.flpAcao.Controls.Add(this.btn_fechar);
            this.flpAcao.Dock = System.Windows.Forms.DockStyle.Fill;
            this.flpAcao.Location = new System.Drawing.Point(0, 0);
            this.flpAcao.Name = "flpAcao";
            this.flpAcao.Size = new System.Drawing.Size(784, 35);
            this.flpAcao.TabIndex = 0;
            // 
            // btn_gravar
            // 
            this.btn_gravar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_gravar.Image = global::SWS.Properties.Resources.fechar_ico;
            this.btn_gravar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btn_gravar.Location = new System.Drawing.Point(3, 3);
            this.btn_gravar.Name = "btn_gravar";
            this.btn_gravar.Size = new System.Drawing.Size(77, 23);
            this.btn_gravar.TabIndex = 19;
            this.btn_gravar.TabStop = false;
            this.btn_gravar.Text = "&Gravar";
            this.btn_gravar.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btn_gravar.UseVisualStyleBackColor = true;
            this.btn_gravar.Click += new System.EventHandler(this.btn_gravar_Click);
            // 
            // lbl_limpar
            // 
            this.lbl_limpar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.lbl_limpar.Image = global::SWS.Properties.Resources.vassoura;
            this.lbl_limpar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.lbl_limpar.Location = new System.Drawing.Point(86, 3);
            this.lbl_limpar.Name = "lbl_limpar";
            this.lbl_limpar.Size = new System.Drawing.Size(77, 23);
            this.lbl_limpar.TabIndex = 20;
            this.lbl_limpar.TabStop = false;
            this.lbl_limpar.Text = "&Limpar";
            this.lbl_limpar.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.lbl_limpar.UseVisualStyleBackColor = true;
            this.lbl_limpar.Click += new System.EventHandler(this.lbl_limpar_Click);
            // 
            // btn_fechar
            // 
            this.btn_fechar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_fechar.Image = global::SWS.Properties.Resources.close;
            this.btn_fechar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btn_fechar.Location = new System.Drawing.Point(169, 3);
            this.btn_fechar.Name = "btn_fechar";
            this.btn_fechar.Size = new System.Drawing.Size(77, 23);
            this.btn_fechar.TabIndex = 21;
            this.btn_fechar.TabStop = false;
            this.btn_fechar.Text = "&Fechar";
            this.btn_fechar.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btn_fechar.UseVisualStyleBackColor = true;
            this.btn_fechar.Click += new System.EventHandler(this.btn_fechar_Click);
            // 
            // lblRazaoSocial
            // 
            this.lblRazaoSocial.BackColor = System.Drawing.Color.LightGray;
            this.lblRazaoSocial.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblRazaoSocial.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblRazaoSocial.Location = new System.Drawing.Point(7, 9);
            this.lblRazaoSocial.Name = "lblRazaoSocial";
            this.lblRazaoSocial.ReadOnly = true;
            this.lblRazaoSocial.Size = new System.Drawing.Size(174, 21);
            this.lblRazaoSocial.TabIndex = 100;
            this.lblRazaoSocial.TabStop = false;
            this.lblRazaoSocial.Text = "Razão Social";
            // 
            // lblFantasia
            // 
            this.lblFantasia.BackColor = System.Drawing.Color.LightGray;
            this.lblFantasia.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblFantasia.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblFantasia.Location = new System.Drawing.Point(7, 29);
            this.lblFantasia.Name = "lblFantasia";
            this.lblFantasia.ReadOnly = true;
            this.lblFantasia.Size = new System.Drawing.Size(174, 21);
            this.lblFantasia.TabIndex = 101;
            this.lblFantasia.TabStop = false;
            this.lblFantasia.Text = "Nome de Fantasia";
            // 
            // textBox1
            // 
            this.textBox1.BackColor = System.Drawing.Color.LightGray;
            this.textBox1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox1.Location = new System.Drawing.Point(7, 49);
            this.textBox1.Name = "textBox1";
            this.textBox1.ReadOnly = true;
            this.textBox1.Size = new System.Drawing.Size(174, 21);
            this.textBox1.TabIndex = 102;
            this.textBox1.TabStop = false;
            this.textBox1.Text = "CNPJ";
            // 
            // lblInscricao
            // 
            this.lblInscricao.BackColor = System.Drawing.Color.LightGray;
            this.lblInscricao.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblInscricao.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblInscricao.Location = new System.Drawing.Point(7, 69);
            this.lblInscricao.Name = "lblInscricao";
            this.lblInscricao.ReadOnly = true;
            this.lblInscricao.Size = new System.Drawing.Size(174, 21);
            this.lblInscricao.TabIndex = 103;
            this.lblInscricao.TabStop = false;
            this.lblInscricao.Text = "Inscriçao Estadual / Municipal";
            // 
            // lblEndereco
            // 
            this.lblEndereco.BackColor = System.Drawing.Color.LightGray;
            this.lblEndereco.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblEndereco.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblEndereco.Location = new System.Drawing.Point(7, 89);
            this.lblEndereco.Name = "lblEndereco";
            this.lblEndereco.ReadOnly = true;
            this.lblEndereco.Size = new System.Drawing.Size(174, 21);
            this.lblEndereco.TabIndex = 104;
            this.lblEndereco.TabStop = false;
            this.lblEndereco.Text = "Endereço";
            // 
            // lblNumero
            // 
            this.lblNumero.BackColor = System.Drawing.Color.LightGray;
            this.lblNumero.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblNumero.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNumero.Location = new System.Drawing.Point(7, 109);
            this.lblNumero.Name = "lblNumero";
            this.lblNumero.ReadOnly = true;
            this.lblNumero.Size = new System.Drawing.Size(174, 21);
            this.lblNumero.TabIndex = 105;
            this.lblNumero.TabStop = false;
            this.lblNumero.Text = "Número";
            // 
            // lblComplemento
            // 
            this.lblComplemento.BackColor = System.Drawing.Color.LightGray;
            this.lblComplemento.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblComplemento.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblComplemento.Location = new System.Drawing.Point(7, 129);
            this.lblComplemento.Name = "lblComplemento";
            this.lblComplemento.ReadOnly = true;
            this.lblComplemento.Size = new System.Drawing.Size(174, 21);
            this.lblComplemento.TabIndex = 106;
            this.lblComplemento.TabStop = false;
            this.lblComplemento.Text = "Complemento";
            // 
            // lblBairro
            // 
            this.lblBairro.BackColor = System.Drawing.Color.LightGray;
            this.lblBairro.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblBairro.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblBairro.Location = new System.Drawing.Point(7, 149);
            this.lblBairro.Name = "lblBairro";
            this.lblBairro.ReadOnly = true;
            this.lblBairro.Size = new System.Drawing.Size(174, 21);
            this.lblBairro.TabIndex = 107;
            this.lblBairro.TabStop = false;
            this.lblBairro.Text = "Bairro";
            // 
            // lblCep
            // 
            this.lblCep.BackColor = System.Drawing.Color.LightGray;
            this.lblCep.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblCep.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCep.Location = new System.Drawing.Point(7, 169);
            this.lblCep.Name = "lblCep";
            this.lblCep.ReadOnly = true;
            this.lblCep.Size = new System.Drawing.Size(174, 21);
            this.lblCep.TabIndex = 108;
            this.lblCep.TabStop = false;
            this.lblCep.Text = "CEP";
            // 
            // lblUF
            // 
            this.lblUF.BackColor = System.Drawing.Color.LightGray;
            this.lblUF.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblUF.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblUF.Location = new System.Drawing.Point(7, 189);
            this.lblUF.Name = "lblUF";
            this.lblUF.ReadOnly = true;
            this.lblUF.Size = new System.Drawing.Size(174, 21);
            this.lblUF.TabIndex = 109;
            this.lblUF.TabStop = false;
            this.lblUF.Text = "Unidade Federativa";
            // 
            // lblCidade
            // 
            this.lblCidade.BackColor = System.Drawing.Color.LightGray;
            this.lblCidade.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblCidade.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCidade.Location = new System.Drawing.Point(7, 209);
            this.lblCidade.Name = "lblCidade";
            this.lblCidade.ReadOnly = true;
            this.lblCidade.Size = new System.Drawing.Size(174, 21);
            this.lblCidade.TabIndex = 110;
            this.lblCidade.TabStop = false;
            this.lblCidade.Text = "Cidade";
            // 
            // lblTelefone
            // 
            this.lblTelefone.BackColor = System.Drawing.Color.LightGray;
            this.lblTelefone.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblTelefone.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTelefone.Location = new System.Drawing.Point(7, 229);
            this.lblTelefone.Name = "lblTelefone";
            this.lblTelefone.ReadOnly = true;
            this.lblTelefone.Size = new System.Drawing.Size(174, 21);
            this.lblTelefone.TabIndex = 111;
            this.lblTelefone.TabStop = false;
            this.lblTelefone.Text = "Telefone";
            // 
            // lblFax
            // 
            this.lblFax.BackColor = System.Drawing.Color.LightGray;
            this.lblFax.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblFax.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblFax.Location = new System.Drawing.Point(7, 249);
            this.lblFax.Name = "lblFax";
            this.lblFax.ReadOnly = true;
            this.lblFax.Size = new System.Drawing.Size(174, 21);
            this.lblFax.TabIndex = 112;
            this.lblFax.TabStop = false;
            this.lblFax.Text = "Fax";
            // 
            // lblEmail
            // 
            this.lblEmail.BackColor = System.Drawing.Color.LightGray;
            this.lblEmail.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblEmail.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblEmail.Location = new System.Drawing.Point(7, 269);
            this.lblEmail.Name = "lblEmail";
            this.lblEmail.ReadOnly = true;
            this.lblEmail.Size = new System.Drawing.Size(174, 21);
            this.lblEmail.TabIndex = 113;
            this.lblEmail.TabStop = false;
            this.lblEmail.Text = "E-mail";
            // 
            // lblSite
            // 
            this.lblSite.BackColor = System.Drawing.Color.LightGray;
            this.lblSite.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblSite.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSite.Location = new System.Drawing.Point(7, 289);
            this.lblSite.Name = "lblSite";
            this.lblSite.ReadOnly = true;
            this.lblSite.Size = new System.Drawing.Size(174, 21);
            this.lblSite.TabIndex = 114;
            this.lblSite.TabStop = false;
            this.lblSite.Text = "Site";
            // 
            // bt_abrir_file_dialog
            // 
            this.bt_abrir_file_dialog.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.bt_abrir_file_dialog.Image = global::SWS.Properties.Resources.Índice;
            this.bt_abrir_file_dialog.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.bt_abrir_file_dialog.Location = new System.Drawing.Point(7, 666);
            this.bt_abrir_file_dialog.Name = "bt_abrir_file_dialog";
            this.bt_abrir_file_dialog.Size = new System.Drawing.Size(70, 23);
            this.bt_abrir_file_dialog.TabIndex = 25;
            this.bt_abrir_file_dialog.Text = "&Abrir";
            this.bt_abrir_file_dialog.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.bt_abrir_file_dialog.UseVisualStyleBackColor = true;
            this.bt_abrir_file_dialog.Click += new System.EventHandler(this.bt_abrir_file_dialog_Click);
            // 
            // pic_box_logo
            // 
            this.pic_box_logo.BackColor = System.Drawing.Color.White;
            this.pic_box_logo.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pic_box_logo.Location = new System.Drawing.Point(7, 524);
            this.pic_box_logo.Name = "pic_box_logo";
            this.pic_box_logo.Size = new System.Drawing.Size(174, 136);
            this.pic_box_logo.TabIndex = 28;
            this.pic_box_logo.TabStop = false;
            // 
            // lblSimples
            // 
            this.lblSimples.BackColor = System.Drawing.Color.LightGray;
            this.lblSimples.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblSimples.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSimples.Location = new System.Drawing.Point(7, 309);
            this.lblSimples.Name = "lblSimples";
            this.lblSimples.ReadOnly = true;
            this.lblSimples.Size = new System.Drawing.Size(174, 21);
            this.lblSimples.TabIndex = 105;
            this.lblSimples.TabStop = false;
            this.lblSimples.Text = "Simples Nacional?";
            // 
            // text_razaoSocial
            // 
            this.text_razaoSocial.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.text_razaoSocial.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.text_razaoSocial.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.text_razaoSocial.Location = new System.Drawing.Point(180, 9);
            this.text_razaoSocial.MaxLength = 100;
            this.text_razaoSocial.Name = "text_razaoSocial";
            this.text_razaoSocial.Size = new System.Drawing.Size(561, 21);
            this.text_razaoSocial.TabIndex = 1;
            // 
            // text_inscricao
            // 
            this.text_inscricao.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.text_inscricao.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.text_inscricao.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.text_inscricao.Location = new System.Drawing.Point(180, 69);
            this.text_inscricao.MaxLength = 20;
            this.text_inscricao.Name = "text_inscricao";
            this.text_inscricao.Size = new System.Drawing.Size(561, 21);
            this.text_inscricao.TabIndex = 4;
            // 
            // text_fantasia
            // 
            this.text_fantasia.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.text_fantasia.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.text_fantasia.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.text_fantasia.Location = new System.Drawing.Point(180, 29);
            this.text_fantasia.MaxLength = 100;
            this.text_fantasia.Name = "text_fantasia";
            this.text_fantasia.Size = new System.Drawing.Size(561, 21);
            this.text_fantasia.TabIndex = 2;
            // 
            // text_cnpj
            // 
            this.text_cnpj.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.text_cnpj.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.text_cnpj.ForeColor = System.Drawing.Color.Black;
            this.text_cnpj.Location = new System.Drawing.Point(180, 49);
            this.text_cnpj.Mask = "99,999,999/9999-99";
            this.text_cnpj.Name = "text_cnpj";
            this.text_cnpj.PromptChar = ' ';
            this.text_cnpj.Size = new System.Drawing.Size(561, 21);
            this.text_cnpj.TabIndex = 3;
            // 
            // text_telefone2
            // 
            this.text_telefone2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.text_telefone2.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.text_telefone2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.text_telefone2.Location = new System.Drawing.Point(180, 249);
            this.text_telefone2.MaxLength = 15;
            this.text_telefone2.Name = "text_telefone2";
            this.text_telefone2.Size = new System.Drawing.Size(561, 21);
            this.text_telefone2.TabIndex = 13;
            // 
            // text_endereco
            // 
            this.text_endereco.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.text_endereco.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.text_endereco.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.text_endereco.Location = new System.Drawing.Point(180, 89);
            this.text_endereco.MaxLength = 100;
            this.text_endereco.Name = "text_endereco";
            this.text_endereco.Size = new System.Drawing.Size(561, 21);
            this.text_endereco.TabIndex = 5;
            // 
            // text_telefone1
            // 
            this.text_telefone1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.text_telefone1.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.text_telefone1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.text_telefone1.Location = new System.Drawing.Point(180, 229);
            this.text_telefone1.MaxLength = 15;
            this.text_telefone1.Name = "text_telefone1";
            this.text_telefone1.Size = new System.Drawing.Size(561, 21);
            this.text_telefone1.TabIndex = 12;
            // 
            // text_site
            // 
            this.text_site.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.text_site.CharacterCasing = System.Windows.Forms.CharacterCasing.Lower;
            this.text_site.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.text_site.Location = new System.Drawing.Point(180, 289);
            this.text_site.MaxLength = 50;
            this.text_site.Name = "text_site";
            this.text_site.Size = new System.Drawing.Size(561, 21);
            this.text_site.TabIndex = 15;
            // 
            // text_numero
            // 
            this.text_numero.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.text_numero.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.text_numero.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.text_numero.Location = new System.Drawing.Point(180, 109);
            this.text_numero.MaxLength = 10;
            this.text_numero.Name = "text_numero";
            this.text_numero.Size = new System.Drawing.Size(561, 21);
            this.text_numero.TabIndex = 6;
            // 
            // text_email
            // 
            this.text_email.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.text_email.CharacterCasing = System.Windows.Forms.CharacterCasing.Lower;
            this.text_email.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.text_email.Location = new System.Drawing.Point(180, 269);
            this.text_email.MaxLength = 50;
            this.text_email.Name = "text_email";
            this.text_email.Size = new System.Drawing.Size(561, 21);
            this.text_email.TabIndex = 14;
            // 
            // text_cep
            // 
            this.text_cep.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.text_cep.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.text_cep.Location = new System.Drawing.Point(180, 169);
            this.text_cep.Mask = "99,999-999";
            this.text_cep.Name = "text_cep";
            this.text_cep.PromptChar = ' ';
            this.text_cep.Size = new System.Drawing.Size(561, 21);
            this.text_cep.TabIndex = 9;
            // 
            // text_complemento
            // 
            this.text_complemento.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.text_complemento.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.text_complemento.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.text_complemento.Location = new System.Drawing.Point(180, 129);
            this.text_complemento.MaxLength = 100;
            this.text_complemento.Name = "text_complemento";
            this.text_complemento.Size = new System.Drawing.Size(561, 21);
            this.text_complemento.TabIndex = 7;
            // 
            // text_bairro
            // 
            this.text_bairro.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.text_bairro.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.text_bairro.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.text_bairro.Location = new System.Drawing.Point(180, 149);
            this.text_bairro.MaxLength = 50;
            this.text_bairro.Name = "text_bairro";
            this.text_bairro.Size = new System.Drawing.Size(561, 21);
            this.text_bairro.TabIndex = 8;
            // 
            // cbUf
            // 
            this.cbUf.BackColor = System.Drawing.Color.LightGray;
            this.cbUf.BorderColor = System.Drawing.Color.DimGray;
            this.cbUf.BorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid;
            this.cbUf.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbUf.FormattingEnabled = true;
            this.cbUf.Location = new System.Drawing.Point(180, 189);
            this.cbUf.Name = "cbUf";
            this.cbUf.Size = new System.Drawing.Size(561, 21);
            this.cbUf.TabIndex = 10;
            this.cbUf.SelectedIndexChanged += new System.EventHandler(this.cbUf_SelectedIndexChanged);
            // 
            // cbCidade
            // 
            this.cbCidade.BackColor = System.Drawing.Color.LightGray;
            this.cbCidade.BorderColor = System.Drawing.Color.DimGray;
            this.cbCidade.BorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid;
            this.cbCidade.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbCidade.FormattingEnabled = true;
            this.cbCidade.Location = new System.Drawing.Point(180, 209);
            this.cbCidade.Name = "cbCidade";
            this.cbCidade.Size = new System.Drawing.Size(561, 21);
            this.cbCidade.TabIndex = 11;
            this.cbCidade.Click += new System.EventHandler(this.cbCidade_Click);
            // 
            // cbSimples
            // 
            this.cbSimples.BackColor = System.Drawing.Color.LightGray;
            this.cbSimples.BorderColor = System.Drawing.Color.DimGray;
            this.cbSimples.BorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid;
            this.cbSimples.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbSimples.FormattingEnabled = true;
            this.cbSimples.Location = new System.Drawing.Point(180, 309);
            this.cbSimples.Name = "cbSimples";
            this.cbSimples.Size = new System.Drawing.Size(561, 21);
            this.cbSimples.TabIndex = 16;
            // 
            // empresaErrorProvider
            // 
            this.empresaErrorProvider.ContainerControl = this;
            // 
            // lbl_matriz
            // 
            this.lbl_matriz.BackColor = System.Drawing.Color.LightGray;
            this.lbl_matriz.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lbl_matriz.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_matriz.Location = new System.Drawing.Point(7, 329);
            this.lbl_matriz.Name = "lbl_matriz";
            this.lbl_matriz.ReadOnly = true;
            this.lbl_matriz.Size = new System.Drawing.Size(174, 21);
            this.lbl_matriz.TabIndex = 106;
            this.lbl_matriz.TabStop = false;
            this.lbl_matriz.Text = "Empresa Matriz";
            // 
            // textMatriz
            // 
            this.textMatriz.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textMatriz.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.textMatriz.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textMatriz.Location = new System.Drawing.Point(180, 329);
            this.textMatriz.MaxLength = 50;
            this.textMatriz.Name = "textMatriz";
            this.textMatriz.ReadOnly = true;
            this.textMatriz.Size = new System.Drawing.Size(538, 21);
            this.textMatriz.TabIndex = 119;
            this.textMatriz.TabStop = false;
            // 
            // btnMatriz
            // 
            this.btnMatriz.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnMatriz.Image = global::SWS.Properties.Resources.busca;
            this.btnMatriz.Location = new System.Drawing.Point(706, 329);
            this.btnMatriz.Name = "btnMatriz";
            this.btnMatriz.Size = new System.Drawing.Size(35, 21);
            this.btnMatriz.TabIndex = 17;
            this.btnMatriz.UseVisualStyleBackColor = true;
            this.btnMatriz.Click += new System.EventHandler(this.btnMatriz_Click);
            // 
            // lblCodigoCNES
            // 
            this.lblCodigoCNES.BackColor = System.Drawing.Color.LightGray;
            this.lblCodigoCNES.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblCodigoCNES.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCodigoCNES.Location = new System.Drawing.Point(7, 349);
            this.lblCodigoCNES.Name = "lblCodigoCNES";
            this.lblCodigoCNES.ReadOnly = true;
            this.lblCodigoCNES.Size = new System.Drawing.Size(174, 21);
            this.lblCodigoCNES.TabIndex = 120;
            this.lblCodigoCNES.TabStop = false;
            this.lblCodigoCNES.Text = "Código CNES";
            // 
            // textCodigoCnes
            // 
            this.textCodigoCnes.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textCodigoCnes.CharacterCasing = System.Windows.Forms.CharacterCasing.Lower;
            this.textCodigoCnes.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textCodigoCnes.Location = new System.Drawing.Point(180, 349);
            this.textCodigoCnes.MaxLength = 7;
            this.textCodigoCnes.Name = "textCodigoCnes";
            this.textCodigoCnes.Size = new System.Drawing.Size(561, 21);
            this.textCodigoCnes.TabIndex = 18;
            this.textCodigoCnes.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.textCodigoCnes_KeyPress);
            // 
            // lblVlrMinimoImpostoFederal
            // 
            this.lblVlrMinimoImpostoFederal.BackColor = System.Drawing.Color.LightGray;
            this.lblVlrMinimoImpostoFederal.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblVlrMinimoImpostoFederal.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblVlrMinimoImpostoFederal.Location = new System.Drawing.Point(7, 369);
            this.lblVlrMinimoImpostoFederal.Name = "lblVlrMinimoImpostoFederal";
            this.lblVlrMinimoImpostoFederal.ReadOnly = true;
            this.lblVlrMinimoImpostoFederal.Size = new System.Drawing.Size(174, 21);
            this.lblVlrMinimoImpostoFederal.TabIndex = 121;
            this.lblVlrMinimoImpostoFederal.TabStop = false;
            this.lblVlrMinimoImpostoFederal.Text = "Valor Mínimo Imposto Federal";
            // 
            // textValorMinimoImpostoFederal
            // 
            this.textValorMinimoImpostoFederal.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textValorMinimoImpostoFederal.CharacterCasing = System.Windows.Forms.CharacterCasing.Lower;
            this.textValorMinimoImpostoFederal.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textValorMinimoImpostoFederal.Location = new System.Drawing.Point(180, 369);
            this.textValorMinimoImpostoFederal.MaxLength = 8;
            this.textValorMinimoImpostoFederal.Name = "textValorMinimoImpostoFederal";
            this.textValorMinimoImpostoFederal.Size = new System.Drawing.Size(561, 21);
            this.textValorMinimoImpostoFederal.TabIndex = 19;
            this.textValorMinimoImpostoFederal.Text = "0,00";
            this.textValorMinimoImpostoFederal.Enter += new System.EventHandler(this.textValorMinimoISS_Enter);
            this.textValorMinimoImpostoFederal.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.textValorMinimoISS_KeyPress);
            this.textValorMinimoImpostoFederal.Leave += new System.EventHandler(this.textValorMinimoISS_Leave);
            // 
            // lblVlrMinimoIR
            // 
            this.lblVlrMinimoIR.BackColor = System.Drawing.Color.LightGray;
            this.lblVlrMinimoIR.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblVlrMinimoIR.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblVlrMinimoIR.Location = new System.Drawing.Point(7, 389);
            this.lblVlrMinimoIR.Name = "lblVlrMinimoIR";
            this.lblVlrMinimoIR.ReadOnly = true;
            this.lblVlrMinimoIR.Size = new System.Drawing.Size(174, 21);
            this.lblVlrMinimoIR.TabIndex = 123;
            this.lblVlrMinimoIR.TabStop = false;
            this.lblVlrMinimoIR.Text = "Valor Mínimo IR";
            // 
            // textValorMinimoIR
            // 
            this.textValorMinimoIR.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textValorMinimoIR.CharacterCasing = System.Windows.Forms.CharacterCasing.Lower;
            this.textValorMinimoIR.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textValorMinimoIR.Location = new System.Drawing.Point(180, 389);
            this.textValorMinimoIR.MaxLength = 8;
            this.textValorMinimoIR.Name = "textValorMinimoIR";
            this.textValorMinimoIR.Size = new System.Drawing.Size(561, 21);
            this.textValorMinimoIR.TabIndex = 20;
            this.textValorMinimoIR.Text = "0,00";
            this.textValorMinimoIR.Enter += new System.EventHandler(this.textValorMinimoIR_Enter);
            this.textValorMinimoIR.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.textValorMinimoIR_KeyPress);
            this.textValorMinimoIR.Leave += new System.EventHandler(this.textValorMinimoIR_Leave);
            // 
            // lblAliquotaPis
            // 
            this.lblAliquotaPis.BackColor = System.Drawing.Color.LightGray;
            this.lblAliquotaPis.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblAliquotaPis.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblAliquotaPis.Location = new System.Drawing.Point(7, 409);
            this.lblAliquotaPis.Name = "lblAliquotaPis";
            this.lblAliquotaPis.ReadOnly = true;
            this.lblAliquotaPis.Size = new System.Drawing.Size(174, 21);
            this.lblAliquotaPis.TabIndex = 125;
            this.lblAliquotaPis.TabStop = false;
            this.lblAliquotaPis.Text = "Aliquota PIS";
            // 
            // textAliquotaPIS
            // 
            this.textAliquotaPIS.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textAliquotaPIS.CharacterCasing = System.Windows.Forms.CharacterCasing.Lower;
            this.textAliquotaPIS.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textAliquotaPIS.Location = new System.Drawing.Point(180, 409);
            this.textAliquotaPIS.MaxLength = 8;
            this.textAliquotaPIS.Name = "textAliquotaPIS";
            this.textAliquotaPIS.Size = new System.Drawing.Size(561, 21);
            this.textAliquotaPIS.TabIndex = 21;
            this.textAliquotaPIS.Text = "0,00";
            this.textAliquotaPIS.Enter += new System.EventHandler(this.textAliquotaPIS_Enter);
            this.textAliquotaPIS.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.textAliquotaPIS_KeyPress);
            this.textAliquotaPIS.Leave += new System.EventHandler(this.textAliquotaPIS_Leave);
            // 
            // lblAliquotaCofins
            // 
            this.lblAliquotaCofins.BackColor = System.Drawing.Color.LightGray;
            this.lblAliquotaCofins.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblAliquotaCofins.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblAliquotaCofins.Location = new System.Drawing.Point(7, 429);
            this.lblAliquotaCofins.Name = "lblAliquotaCofins";
            this.lblAliquotaCofins.ReadOnly = true;
            this.lblAliquotaCofins.Size = new System.Drawing.Size(174, 21);
            this.lblAliquotaCofins.TabIndex = 127;
            this.lblAliquotaCofins.TabStop = false;
            this.lblAliquotaCofins.Text = "Aliquota Cofins";
            // 
            // textAliquotaCOFINS
            // 
            this.textAliquotaCOFINS.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textAliquotaCOFINS.CharacterCasing = System.Windows.Forms.CharacterCasing.Lower;
            this.textAliquotaCOFINS.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textAliquotaCOFINS.Location = new System.Drawing.Point(180, 429);
            this.textAliquotaCOFINS.MaxLength = 8;
            this.textAliquotaCOFINS.Name = "textAliquotaCOFINS";
            this.textAliquotaCOFINS.Size = new System.Drawing.Size(561, 21);
            this.textAliquotaCOFINS.TabIndex = 22;
            this.textAliquotaCOFINS.Text = "0,00";
            this.textAliquotaCOFINS.Enter += new System.EventHandler(this.textAliquotaCOFINS_Enter);
            this.textAliquotaCOFINS.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.textAliquotaCOFINS_KeyPress);
            this.textAliquotaCOFINS.Leave += new System.EventHandler(this.textAliquotaCOFINS_Leave);
            // 
            // lblAliquotaIR
            // 
            this.lblAliquotaIR.BackColor = System.Drawing.Color.LightGray;
            this.lblAliquotaIR.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblAliquotaIR.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblAliquotaIR.Location = new System.Drawing.Point(7, 449);
            this.lblAliquotaIR.Name = "lblAliquotaIR";
            this.lblAliquotaIR.ReadOnly = true;
            this.lblAliquotaIR.Size = new System.Drawing.Size(174, 21);
            this.lblAliquotaIR.TabIndex = 129;
            this.lblAliquotaIR.TabStop = false;
            this.lblAliquotaIR.Text = "Aliquota IR";
            // 
            // textAliquotaIR
            // 
            this.textAliquotaIR.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textAliquotaIR.CharacterCasing = System.Windows.Forms.CharacterCasing.Lower;
            this.textAliquotaIR.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textAliquotaIR.Location = new System.Drawing.Point(180, 449);
            this.textAliquotaIR.MaxLength = 8;
            this.textAliquotaIR.Name = "textAliquotaIR";
            this.textAliquotaIR.Size = new System.Drawing.Size(561, 21);
            this.textAliquotaIR.TabIndex = 23;
            this.textAliquotaIR.Text = "0,00";
            this.textAliquotaIR.Enter += new System.EventHandler(this.textAliquotaIR_Enter);
            this.textAliquotaIR.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.textAliquotaIR_KeyPress);
            this.textAliquotaIR.Leave += new System.EventHandler(this.textAliquotaIR_Leave);
            // 
            // lblAliquotaCsll
            // 
            this.lblAliquotaCsll.BackColor = System.Drawing.Color.LightGray;
            this.lblAliquotaCsll.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblAliquotaCsll.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblAliquotaCsll.Location = new System.Drawing.Point(7, 469);
            this.lblAliquotaCsll.Name = "lblAliquotaCsll";
            this.lblAliquotaCsll.ReadOnly = true;
            this.lblAliquotaCsll.Size = new System.Drawing.Size(174, 21);
            this.lblAliquotaCsll.TabIndex = 131;
            this.lblAliquotaCsll.TabStop = false;
            this.lblAliquotaCsll.Text = "Aliquota CSLL";
            // 
            // textAliquotaCSLL
            // 
            this.textAliquotaCSLL.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textAliquotaCSLL.CharacterCasing = System.Windows.Forms.CharacterCasing.Lower;
            this.textAliquotaCSLL.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textAliquotaCSLL.Location = new System.Drawing.Point(180, 469);
            this.textAliquotaCSLL.MaxLength = 8;
            this.textAliquotaCSLL.Name = "textAliquotaCSLL";
            this.textAliquotaCSLL.Size = new System.Drawing.Size(561, 21);
            this.textAliquotaCSLL.TabIndex = 24;
            this.textAliquotaCSLL.Text = "0,00";
            this.textAliquotaCSLL.Enter += new System.EventHandler(this.textAliquotaCSLL_Enter);
            this.textAliquotaCSLL.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.textAliquotaCSLL_KeyPress);
            this.textAliquotaCSLL.Leave += new System.EventHandler(this.textAliquotaCSLL_Leave);
            // 
            // textAliquotaISS
            // 
            this.textAliquotaISS.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textAliquotaISS.CharacterCasing = System.Windows.Forms.CharacterCasing.Lower;
            this.textAliquotaISS.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textAliquotaISS.Location = new System.Drawing.Point(180, 489);
            this.textAliquotaISS.MaxLength = 8;
            this.textAliquotaISS.Name = "textAliquotaISS";
            this.textAliquotaISS.Size = new System.Drawing.Size(561, 21);
            this.textAliquotaISS.TabIndex = 25;
            this.textAliquotaISS.Text = "0,00";
            this.textAliquotaISS.Enter += new System.EventHandler(this.textAliquotaISS_Enter);
            this.textAliquotaISS.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.textAliquotaISS_KeyPress);
            this.textAliquotaISS.Leave += new System.EventHandler(this.textAliquotaISS_Leave);
            // 
            // lblAliquotaISS
            // 
            this.lblAliquotaISS.BackColor = System.Drawing.Color.LightGray;
            this.lblAliquotaISS.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblAliquotaISS.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblAliquotaISS.Location = new System.Drawing.Point(7, 489);
            this.lblAliquotaISS.Name = "lblAliquotaISS";
            this.lblAliquotaISS.ReadOnly = true;
            this.lblAliquotaISS.Size = new System.Drawing.Size(174, 21);
            this.lblAliquotaISS.TabIndex = 133;
            this.lblAliquotaISS.TabStop = false;
            this.lblAliquotaISS.Text = "Aliquota ISS";
            // 
            // frmEmpresaIncluir
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(784, 561);
            this.Name = "frmEmpresaIncluir";
            this.Text = "INCLUIR EMPRESA";
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.frmEmpresaIncluir_KeyDown);
            this.spForm.Panel1.ResumeLayout(false);
            this.spForm.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.spForm)).EndInit();
            this.spForm.ResumeLayout(false);
            this.pnlForm.ResumeLayout(false);
            this.pnlForm.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.banner)).EndInit();
            this.flpAcao.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pic_box_logo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.empresaErrorProvider)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        protected System.Windows.Forms.FlowLayoutPanel flpAcao;
        protected System.Windows.Forms.Button lbl_limpar;
        protected System.Windows.Forms.Button btn_fechar;
        protected System.Windows.Forms.Button btn_gravar;
        protected System.Windows.Forms.TextBox lblRazaoSocial;
        protected System.Windows.Forms.TextBox lblFantasia;
        protected System.Windows.Forms.TextBox lblInscricao;
        protected System.Windows.Forms.TextBox textBox1;
        protected System.Windows.Forms.TextBox lblEndereco;
        protected System.Windows.Forms.TextBox lblNumero;
        protected System.Windows.Forms.TextBox lblComplemento;
        protected System.Windows.Forms.TextBox lblBairro;
        protected System.Windows.Forms.TextBox lblCep;
        protected System.Windows.Forms.TextBox lblCidade;
        protected System.Windows.Forms.TextBox lblUF;
        protected System.Windows.Forms.TextBox lblSite;
        protected System.Windows.Forms.TextBox lblEmail;
        protected System.Windows.Forms.TextBox lblFax;
        protected System.Windows.Forms.TextBox lblTelefone;
        protected System.Windows.Forms.TextBox lblSimples;
        protected ComboBoxWithBorder cbSimples;
        protected ComboBoxWithBorder cbCidade;
        protected ComboBoxWithBorder cbUf;
        protected System.Windows.Forms.TextBox text_razaoSocial;
        protected System.Windows.Forms.TextBox text_inscricao;
        protected System.Windows.Forms.TextBox text_fantasia;
        protected System.Windows.Forms.MaskedTextBox text_cnpj;
        protected System.Windows.Forms.TextBox text_telefone2;
        protected System.Windows.Forms.TextBox text_endereco;
        protected System.Windows.Forms.TextBox text_telefone1;
        protected System.Windows.Forms.TextBox text_site;
        protected System.Windows.Forms.TextBox text_numero;
        protected System.Windows.Forms.TextBox text_email;
        protected System.Windows.Forms.MaskedTextBox text_cep;
        protected System.Windows.Forms.TextBox text_complemento;
        protected System.Windows.Forms.TextBox text_bairro;
        protected System.Windows.Forms.Button bt_abrir_file_dialog;
        protected System.Windows.Forms.PictureBox pic_box_logo;
        protected System.Windows.Forms.ErrorProvider empresaErrorProvider;
        protected System.Windows.Forms.TextBox textMatriz;
        protected System.Windows.Forms.TextBox lbl_matriz;
        protected System.Windows.Forms.Button btnMatriz;
        protected System.Windows.Forms.TextBox textCodigoCnes;
        protected System.Windows.Forms.TextBox lblCodigoCNES;
        protected System.Windows.Forms.TextBox lblVlrMinimoIR;
        protected System.Windows.Forms.TextBox textValorMinimoImpostoFederal;
        protected System.Windows.Forms.TextBox lblVlrMinimoImpostoFederal;
        protected System.Windows.Forms.TextBox textAliquotaCSLL;
        protected System.Windows.Forms.TextBox lblAliquotaCsll;
        protected System.Windows.Forms.TextBox textAliquotaIR;
        protected System.Windows.Forms.TextBox lblAliquotaIR;
        protected System.Windows.Forms.TextBox textAliquotaCOFINS;
        protected System.Windows.Forms.TextBox lblAliquotaCofins;
        protected System.Windows.Forms.TextBox textAliquotaPIS;
        protected System.Windows.Forms.TextBox lblAliquotaPis;
        protected System.Windows.Forms.TextBox textValorMinimoIR;
        protected System.Windows.Forms.TextBox textAliquotaISS;
        protected System.Windows.Forms.TextBox lblAliquotaISS;
    }
}