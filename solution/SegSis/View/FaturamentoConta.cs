﻿using SWS.Entidade;
using SWS.Facade;
using SWS.View.Resources;
using SWS.ViewHelper;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace SWS.View
{
    public partial class frmFaturamentoConta : frmTemplateConsulta
    {
        private Banco banco;
        private Conta conta;

        public Conta Conta
        {
            get { return conta; }
            set { conta = value; }
        }

        public frmFaturamentoConta()
        {
            InitializeComponent();
            carregaComboBancos(cb_banco);
            ActiveControl = cb_banco;
        }

        public void carregaComboBancos(ComboBox combo)
        {
            try
            {
                FinanceiroFacade financeiroFacade = FinanceiroFacade.getInstance();

                combo.DisplayMember = "nome";
                combo.ValueMember = "valor";
                combo.DropDownStyle = ComboBoxStyle.DropDownList;

                foreach (DataRow rows in financeiroFacade.findBancoByFilter(new Banco(null, string.Empty, string.Empty, null, ApplicationConstants.ATIVO, false, true), null, true).Tables[0].Rows)
                    combo.Items.Add(new SelectItem(Convert.ToString(rows["id_banco"]), Convert.ToString(rows["nome"])));

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void cb_banco_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                if (cb_banco.SelectedIndex != -1)
                {
                    FinanceiroFacade financeiroFacade = FinanceiroFacade.getInstance();

                    banco = financeiroFacade.findBancoById(Convert.ToInt64(((SelectItem)cb_banco.SelectedItem).Valor));
                    montaDataGrid();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        public void montaDataGrid()
        {
            try
            {
                this.Cursor = Cursors.WaitCursor;

                dg_conta.Columns.Clear();

                FinanceiroFacade financeiroFacade = FinanceiroFacade.getInstance();

                DataSet ds = financeiroFacade.findContaByFilter(new Conta(null, banco, string.Empty, string.Empty, string.Empty, string.Empty,string.Empty, null, string.Empty, string.Empty, string.Empty, string.Empty, string.Empty, true, ApplicationConstants.ATIVO, false));

                dg_conta.DataSource = ds.Tables[0].DefaultView;

                dg_conta.Columns[0].HeaderText = "Id Conta";
                dg_conta.Columns[0].Visible = false;

                dg_conta.Columns[1].HeaderText = "Nome";
                dg_conta.Columns[2].HeaderText = "Agência";
                dg_conta.Columns[3].HeaderText = "Agência Digito";
                dg_conta.Columns[4].HeaderText = "Conta Corrente";
                dg_conta.Columns[5].HeaderText = "Dígito";
                dg_conta.Columns[6].HeaderText = "Próximo número";
                dg_conta.Columns[6].Visible = false;

                dg_conta.Columns[7].HeaderText = "Numero Remessa";
                dg_conta.Columns[7].Visible = false;

                dg_conta.Columns[8].HeaderText = "Carteira";
                dg_conta.Columns[8].Visible = false;

                dg_conta.Columns[9].HeaderText = "Convenio";
                dg_conta.Columns[9].Visible = false;

                dg_conta.Columns[10].HeaderText = "Variação";
                dg_conta.Columns[10].Visible = false;

                dg_conta.Columns[11].HeaderText = "Codigo Cedente";
                dg_conta.Columns[11].Visible = false;

                dg_conta.Columns[12].HeaderText = "Registro";
                dg_conta.Columns[12].Visible = false;

                dg_conta.Columns[13].HeaderText = "Situacao";
                dg_conta.Columns[13].Visible = false;

                dg_conta.Columns[14].HeaderText = "Id Banco";
                dg_conta.Columns[14].Visible = false;

                dg_conta.Columns[15].HeaderText = "Nome do Banco";
                dg_conta.Columns[15].Visible = false;

                dg_conta.Columns[16].HeaderText = "Codigo Banco";
                dg_conta.Columns[16].Visible = false;

                dg_conta.Columns[17].HeaderText = "Caixa";
                dg_conta.Columns[17].Visible = false;

                dg_conta.Columns[18].HeaderText = "Situacao";
                dg_conta.Columns[18].Visible = false;

                dg_conta.Columns[19].HeaderText = "Conta Privada";
                dg_conta.Columns[19].Visible = false;

                dg_conta.Columns[20].HeaderText = "Banco privado";
                dg_conta.Columns[20].Visible = false;

                dg_conta.Columns[21].HeaderText = "Banco Boleto";
                dg_conta.Columns[21].Visible = false;

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }

        }

        private void btn_fechar_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btn_confirma_Click(object sender, EventArgs e)
        {
            try
            {
                this.Cursor = Cursors.WaitCursor;

                if (dg_conta.CurrentRow == null)
                    throw new Exception("Selecione uma linha.");
                
                FinanceiroFacade FinanceiroFacade = FinanceiroFacade.getInstance();
                    
                conta = new Conta((Int64)dg_conta.CurrentRow.Cells[0].Value, banco, (String)dg_conta.CurrentRow.Cells[1].Value, (String)dg_conta.CurrentRow.Cells[2].Value, (String)dg_conta.CurrentRow.Cells[3].Value, (String)dg_conta.CurrentRow.Cells[4].Value, (String)dg_conta.CurrentRow.Cells[5].Value, (Int64)dg_conta.CurrentRow.Cells[6].Value, (String)dg_conta.CurrentRow.Cells[7].Value, (String)dg_conta.CurrentRow.Cells[8].Value, (String)dg_conta.CurrentRow.Cells[9].Value, (String)dg_conta.CurrentRow.Cells[10].Value, (String)dg_conta.CurrentRow.Cells[11].Value, (Boolean)dg_conta.CurrentRow.Cells[12].Value, (String)dg_conta.CurrentRow.Cells[13].Value,  (Boolean)dg_conta.CurrentRow.Cells[19].Value); 
                
                this.Close();

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }

        private void dg_conta_CellMouseDoubleClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            try
            {
                btn_confirma.PerformClick();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }
    }
}
