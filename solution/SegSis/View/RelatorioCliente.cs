﻿using SWS.Entidade;
using SWS.View.ViewHelper;
using SWS.ViewHelper;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace SWS.View
{
    public partial class frmRelatorioCliente : frmTemplateConsulta
    {
        private Cliente cliente;
        
        public frmRelatorioCliente()
        {
            InitializeComponent();
            /* carregando os combos */

            ComboHelper.iniciaComboAtivoInativo(cbSituacao);
            ComboHelper.iniciaComboTipoCliente(cbTipoCliente);
            ComboHelper.booleanTodos(cbContrato);
            ComboHelper.tipoRelatorioCliente(cbTipoRelatorio);
        }

        private void btnImprimir_Click(object sender, EventArgs e)
        {
            try
            {
                /* validando dados da tela */
                if (!validaDadosTela())
                {
                    this.Cursor = Cursors.WaitCursor;

                    string nomeRelatorio = !string.Equals(((SelectItem)cbTipoRelatorio.SelectedItem).Nome, "Por Função") ? "RELATORIO_CLIENTES.JASPER" : ((SelectItem)cbTipoRelatorio.SelectedItem).Valor;
                    string dtInicial = dataInicial.Checked ? Convert.ToDateTime(dataInicial.Text).ToShortDateString().ToString() : string.Empty;
                    string dtFinal = dataFinal.Checked ? Convert.ToDateTime(dataFinal.Text).ToShortDateString().ToString() : string.Empty;
                    long idCliente = cliente != null ? (long)cliente.Id : 0;
                    string tipo = ((SelectItem)cbTipoCliente.SelectedItem).Nome;
                    string situacao = ((SelectItem)cbSituacao.SelectedItem).Valor;
                    String tipoRelatorio = ((SelectItem)cbTipoRelatorio.SelectedItem).Valor;
                    String contrato = ((SelectItem)cbContrato.SelectedItem).Valor;

                    var process = new Process();

                    process.StartInfo.FileName = "Relatorio\\jre7\\bin\\java";
                    process.StartInfo.Arguments = " -jar Relatorio\\MiddlewareJasper.jar " + "Relatorio\\Report\\" + nomeRelatorio + " DT_INICIAL:" + dtInicial + ":STRING" + " DT_FINAL:" + dtFinal + ":STRING" + " ID_CLIENTE:" + idCliente + ":INTEGER" + " TIPO:" + tipo + ":STRING" + " SITUACAO:" + situacao + ":STRING" + " TIPO_RELATORIO:" + tipoRelatorio + ":STRING" + " CONTRATO:" + contrato + ":STRING";
                    process.StartInfo.UseShellExecute = false;
                    process.StartInfo.RedirectStandardError = true;
                    process.StartInfo.RedirectStandardInput = true;
                    process.StartInfo.RedirectStandardOutput = true;
                    process.StartInfo.CreateNoWindow = true;

                    process.Start();
                    process.StandardOutput.ReadToEnd();
                    process.WaitForExit();



                }

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }


        }

        private void btnFechar_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btCliente_Click(object sender, EventArgs e)
        {
            try
            {
                frmClienteSelecionar selecionarCliente = new frmClienteSelecionar(null, null, false, null, null, null, null, null, null);
                selecionarCliente.ShowDialog();

                if (selecionarCliente.Cliente != null)
                {
                    cliente = selecionarCliente.Cliente;
                    textCliente.Text = cliente.RazaoSocial;

                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }

        private void btnExcluirCliente_Click(object sender, EventArgs e)
        {
            try
            {
                if (cliente == null)
                    throw new Exception("Selecione primeiro o cliente.");

                cliente = null;
                textCliente.Text = string.Empty;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }

        private bool validaDadosTela()
        {
            bool validaDados = false;
            try
            {
                if (dataInicial.Checked && !dataFinal.Checked)
                    throw new Exception("Você deve marcar a data final");

                if (dataFinal.Checked && !dataInicial.Checked)
                    throw new Exception("Você deve marcar a data inicial");

                if (Convert.ToDateTime(dataInicial.Text) > Convert.ToDateTime(dataFinal.Text))
                    throw new Exception("A data final não pode ser menor que a data inicial. Refaça sua pesquisa.");

                if (string.Equals(((SelectItem)cbTipoRelatorio.SelectedItem).Valor, "INDIVIDUAL") && cliente == null)
                    throw new Exception("Você selecionou o relatório individual. É necessário selecionar o cliente");

                if (string.Equals(((SelectItem)cbTipoRelatorio.SelectedItem).Nome, "Por Função") && cliente == null)
                    throw new Exception("Você selecionou o relatório por função. É necessário selecionar o cliente");
            }
            catch (Exception ex)
            {
                validaDados = true;
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

            return validaDados;
        }
    }
}
