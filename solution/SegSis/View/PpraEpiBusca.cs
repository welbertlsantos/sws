﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using SWS.Facade;
using SWS.Entidade;
using SWS.View.Resources;

namespace SWS.View
{
    public partial class frm_PpraEpiBusca : BaseFormConsulta
    {
        static String Msg01 = "Selecione uma linha ";
        static String Msg02 = "Atenção";

        Epi epi = null;

        public Epi getEpi()
        {
            return this.epi;
        }

        GheFonteAgente gheFonteAgente;

        public frm_PpraEpiBusca(GheFonteAgente gheFonteAgente)
        {
            InitializeComponent();
            this.gheFonteAgente = gheFonteAgente;
            validaPermissoes();

        }

        private void validaPermissoes()
        {
            PermissionamentoFacade permissionamentoFacade = PermissionamentoFacade.getInstance();
            this.btn_incluir.Enabled = permissionamentoFacade.hasPermission("EPI", "INCLUIR");
        }

        private void btn_incluir_Click(object sender, EventArgs e)
        {
            frmEpiIncluir formEpiIncluir = new frmEpiIncluir();
            formEpiIncluir.ShowDialog();

            if (formEpiIncluir.Epi != null)
            {
                text_nome.Text = formEpiIncluir.Epi.Descricao;
                btn_buscar.PerformClick();
            }
        }

        private void btn_fechar_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btn_buscar_Click(object sender, EventArgs e)
        {
            try
            {
                this.grd_epi.Columns.Clear();
                montaDataGrid();
                text_nome.Focus();
            }
            catch (Exception Ex)
            {
                MessageBox.Show(Ex.Message);
            }
        }

        public void montaDataGrid()
        {
            PpraFacade ppraFacade = PpraFacade.getInstance();

            Epi epi = new Epi(null, text_nome.Text.Trim(), String.Empty, String.Empty, ApplicationConstants.ATIVO);

            DataSet ds = ppraFacade.findEpiByFilter(epi);

            grd_epi.DataSource = ds.Tables["Epis"].DefaultView;
            
            grd_epi.Columns[0].HeaderText = "ID";
            grd_epi.Columns[0].Visible = false;

            grd_epi.Columns[1].HeaderText = "Descrição";
            
            grd_epi.Columns[2].HeaderText = "Finalidade";

            grd_epi.Columns[3].HeaderText = "CA";

            grd_epi.Columns[4].HeaderText = "Situação";
            grd_epi.Columns[4].Visible = false;
            
        }

        private void btn_ok_Click(object sender, EventArgs e)
        {
            try
            {
                if (grd_epi.CurrentRow != null)
                {
                    PpraFacade ppraFacade = PpraFacade.getInstance();
                    
                    epi = ppraFacade.findEpiById((Int64)grd_epi.CurrentRow.Cells[0].Value);
                    
                    this.Close();
                }
                else
                {
                    MessageBox.Show(Msg01, Msg02);
                }
            }
            catch (Exception Ex)
            {
                MessageBox.Show(Ex.Message);
            }
            
        }

    }
}
