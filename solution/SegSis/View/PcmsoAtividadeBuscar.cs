﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using SWS.Entidade;
using SWS.Excecao;
using SWS.Facade;
using SWS.View.Resources;

namespace SWS.View
{
    public partial class frm_PcmsoAtividadeBuscar : BaseFormConsulta
    {

        Atividade atividade = null;

        public Atividade getAtividade()
        {
            return this.atividade;
        }

        public static String Msg1 = "Selecione uma linha!";
        public static String Msg2 = "Atenção";

        frm_PcmoAtividadeIncluir formPcmsoAtividadeIncluir;

        public frm_PcmsoAtividadeBuscar(frm_PcmoAtividadeIncluir formPcmsoAtividadeIncluir)
        {
            InitializeComponent();
            this.formPcmsoAtividadeIncluir = formPcmsoAtividadeIncluir;
            validaPermissoes();
        }

        private void validaPermissoes()
        {
            PermissionamentoFacade permissionamentoFacade = PermissionamentoFacade.getInstance();

            btn_novo.Enabled = permissionamentoFacade.hasPermission("ATIVIDADE", "INCLUIR");
        }

        private void btn_fechar_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btn_novo_Click(object sender, EventArgs e)
        {
            frmAtividadeIncluir formAtividadeIncluir = new frmAtividadeIncluir();
            formAtividadeIncluir.ShowDialog();

            if (formAtividadeIncluir.Atividade != null)
            {
                atividade = formAtividadeIncluir.Atividade;
                text_descricao.Text = atividade.Descricao;
                MontaDataGrid();
                btn_incluir.PerformClick();
            }

        }

        private void btn_buscar_Click(object sender, EventArgs e)
        {
            try
            {

                this.grd_atividade.Columns.Clear();
                this.Cursor = Cursors.WaitCursor;
                MontaDataGrid();
                text_descricao.Focus();

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }

        }

        public void MontaDataGrid()
        {
            PcmsoFacade pcmsoFacade = PcmsoFacade.getInstance();

            atividade = new Atividade(null, Convert.ToString(text_descricao.Text), ApplicationConstants.ATIVO);

            DataSet ds = pcmsoFacade.findAllAtividade(atividade, formPcmsoAtividadeIncluir.cronograma);

            grd_atividade.DataSource = ds.Tables["Atividades"].DefaultView;
            
            grd_atividade.Columns[0].HeaderText = "ID";
            grd_atividade.Columns[1].HeaderText = "Descrição";

            grd_atividade.Columns[0].Visible = false;


        }

        private void btn_incluir_Click(object sender, EventArgs e)
        {
            try
            {

                Atividade atividade = new Atividade();
                
                PcmsoFacade pcmsoFacade = PcmsoFacade.getInstance();

                try
                {
                    if (grd_atividade.CurrentRow != null)
                    {

                        Int32 cellValue = grd_atividade.CurrentRow.Index;
                        Int64 id = (Int64)grd_atividade.Rows[cellValue].Cells[0].Value;

                        atividade = pcmsoFacade.findAtividadeById(id);

                        formPcmsoAtividadeIncluir.atividade = atividade;

                        formPcmsoAtividadeIncluir.text_descricao.Text = atividade.Descricao;
                        
                        this.Close();

                    }
                    else
                    {
                        MessageBox.Show(Msg1, Msg2);
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }


            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }

        }



    }
}
