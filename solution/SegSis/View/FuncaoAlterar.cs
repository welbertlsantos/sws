﻿using SWS.Entidade;
using SWS.Facade;
using SWS.View.Resources;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace SWS.View
{
    public partial class frmFuncaoAlterar : frmFuncaoIncluir
    {
        public frmFuncaoAlterar(Funcao funcao) :base()
        {
            InitializeComponent();
            this.Funcao = funcao;
            textDescricao.Text = Funcao.Descricao;
            textCodioCbo.Text = Funcao.CodCbo;
            Comentarios = Funcao.Atividades;
            montaDataGrid();
        }

        protected override void btGravar_Click(object sender, EventArgs e)
        {
            try
            {
                if (validaCamposObrigatorio())
                {
                    EstudoFacade pcmsoFacade = EstudoFacade.getInstance();

                    Funcao = pcmsoFacade.updateFuncao(new Funcao(Funcao.Id, textDescricao.Text, textCodioCbo.Text, ApplicationConstants.ATIVO, string.Empty));

                    MessageBox.Show("Função alterada com sucesso.", "Confirmação", MessageBoxButtons.OK, MessageBoxIcon.Information);

                    this.Close();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        protected override void btIncluirComentario_Click(object sender, EventArgs e)
        {
            try
            {
                frmFuncaoComentarioIncluir formComentarioIncluir = new frmFuncaoComentarioIncluir();
                formComentarioIncluir.ShowDialog();

                if (formComentarioIncluir.ComentarioFuncao != null)
                {
                    if (Comentarios.Exists(x => string.Equals(x.Atividade, formComentarioIncluir.ComentarioFuncao)))
                        throw new Exception("Já existe essa atividade cadastrada para essa função.");

                    ComentarioFuncao comentarioFuncaoIncluir = formComentarioIncluir.ComentarioFuncao;
                    comentarioFuncaoIncluir.Funcao = Funcao;

                    EstudoFacade pcmsoFacade = EstudoFacade.getInstance();
                    Comentarios.Add(pcmsoFacade.insertComentarioFuncao(comentarioFuncaoIncluir));
                    montaDataGrid();

                }

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }

        protected override void btExcluirAtividade_Click(object sender, EventArgs e)
        {
            try
            {
                if (dgvComentario.CurrentRow == null)
                    throw new Exception("Selecione uma linha");

                ComentarioFuncao comentarioFuncaoExcluir = Comentarios.Find(x => x.Id == (long)dgvComentario.CurrentRow.Cells[0].Value);

                if (MessageBox.Show("Deseja excluir a atividade selecionada?", "Atenção", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                {
                    EstudoFacade pcmsoFacade = EstudoFacade.getInstance();
                    comentarioFuncaoExcluir.Situacao = ApplicationConstants.DESATIVADO;
                    pcmsoFacade.updateComentarioFuncao(comentarioFuncaoExcluir);
                    Comentarios.Remove(Comentarios.Find(x => x.Id == (long)dgvComentario.CurrentRow.Cells[0].Value));
                    montaDataGrid();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }

        protected override void dgvComentario_CellContentDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                if (dgvComentario.CurrentRow == null)
                    throw new Exception("Selecione uma linha.");

                frmFuncaoComentarioAlterar alterarComentario = new frmFuncaoComentarioAlterar(Comentarios.Find(x => x.Id == (long)dgvComentario.CurrentRow.Cells[0].Value));
                alterarComentario.ShowDialog();

                if (alterarComentario.ComentarioFuncao != null)
                {
                    EstudoFacade pcmsoFacade = EstudoFacade.getInstance();
                    pcmsoFacade.updateComentarioFuncao(alterarComentario.ComentarioFuncao);
                    montaDataGrid();
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }

    }
}
