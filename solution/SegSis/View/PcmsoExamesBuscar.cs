﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using SWS.Entidade;
using SWS.Facade;

namespace SWS.View
{
    public partial class frm_PcmsoExamesBuscar : BaseFormConsulta
    {
        frm_PcmsoExameIncluir formPcmsoExameIncluir;

        Exame exame = null;
                
        public frm_PcmsoExamesBuscar(frm_PcmsoExameIncluir formPcmsoExameIncluir)
        {
            InitializeComponent();
            this.formPcmsoExameIncluir = formPcmsoExameIncluir;
            validaPermissoes();
        }

        private void validaPermissoes()
        {
            PermissionamentoFacade permissionamentoFacade = PermissionamentoFacade.getInstance();

            this.btn_novo.Enabled = permissionamentoFacade.hasPermission("EXAME", "INCLUIR");
        }

        private void btn_fechar_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btn_pesquisar_Click(object sender, EventArgs e)
        {
            try
            {
                this.grd_exame.Columns.Clear();
                montaDataGrid();
                text_descricao.Focus();
            }
            catch (Exception Ex)
            {
                MessageBox.Show(Ex.Message);
            }
        }

        private void btn_incluir_Click(object sender, EventArgs e)
        {
            try
            {
                // Montando objeto exame da classe pai
                
                Int64 id = (Int64)grd_exame.CurrentRow.Cells[0].Value;
                String descricao = (String)grd_exame.CurrentRow.Cells[1].Value;
                String situacao = (String)grd_exame.CurrentRow.Cells[2].Value;
                Boolean laboratorio = (Boolean)grd_exame.CurrentRow.Cells[3].Value;
                Decimal precoTabela = (Decimal)grd_exame.CurrentRow.Cells[4].Value;
                
                Exame exame = new Exame(id, descricao, situacao, laboratorio, precoTabela,0,0,false,false, string.Empty, false, null, false);

                formPcmsoExameIncluir.exame = exame;
                formPcmsoExameIncluir.text_descricao.Text = descricao;
                formPcmsoExameIncluir.text_idadeExame.Enabled = true;
                formPcmsoExameIncluir.montaGridPeriodicidade();

                this.Close();
                
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }

        }

        public void montaDataGrid()
        {
            try
            {

                PcmsoFacade pcmsoFacade = PcmsoFacade.getInstance();

                Exame exame = new Exame();
                exame.Descricao = text_descricao.Text;

                DataSet ds = pcmsoFacade.findExameByFilter(exame);

                grd_exame.DataSource = ds.Tables["Exames"].DefaultView;

                grd_exame.Columns[0].HeaderText = "IdExame";
                grd_exame.Columns[1].HeaderText = "Descrição";
                grd_exame.Columns[2].HeaderText = "Situação";
                grd_exame.Columns[3].HeaderText = "Laboratório";
                grd_exame.Columns[4].HeaderText = "Preço de Tabela";
                grd_exame.Columns[5].HeaderText = "Prioridade na Fila";
                grd_exame.Columns[6].HeaderText = "Preço de custo";
                grd_exame.Columns[7].HeaderText = "Externo";
                grd_exame.Columns[8].HeaderText = "Documentação";

                grd_exame.Columns[0].Visible = false;
                grd_exame.Columns[2].Visible = false;
                grd_exame.Columns[4].Visible = false;
                grd_exame.Columns[6].Visible = false;
                
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }

        }

        private void btn_novo_Click(object sender, EventArgs e)
        {
            frmExameIncluir formExameIncluir = new frmExameIncluir();
            formExameIncluir.ShowDialog();

            if (formExameIncluir.Exame != null)
            {
                exame = formExameIncluir.Exame;
                text_descricao.Text = exame.Descricao;
                montaDataGrid();
                btn_fechar.PerformClick();
            }
        }

    }
}
