﻿namespace SWS.View
{
    partial class frmProcessamentoMovimento
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.flpAcao = new System.Windows.Forms.FlowLayoutPanel();
            this.btnProcessar = new System.Windows.Forms.Button();
            this.btFechar = new System.Windows.Forms.Button();
            this.btnCliente = new System.Windows.Forms.Button();
            this.lblCliente = new System.Windows.Forms.TextBox();
            this.textCliente = new System.Windows.Forms.TextBox();
            this.btnExcluirCliente = new System.Windows.Forms.Button();
            this.cbAno = new SWS.ComboBoxWithBorder();
            this.cbMes = new SWS.ComboBoxWithBorder();
            this.lblPeriodo = new System.Windows.Forms.TextBox();
            this.listProcessamento = new System.Windows.Forms.ListBox();
            ((System.ComponentModel.ISupportInitialize)(this.spForm)).BeginInit();
            this.spForm.Panel1.SuspendLayout();
            this.spForm.Panel2.SuspendLayout();
            this.spForm.SuspendLayout();
            this.pnlForm.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.banner)).BeginInit();
            this.flpAcao.SuspendLayout();
            this.SuspendLayout();
            // 
            // spForm
            // 
            // 
            // spForm.Panel1
            // 
            this.spForm.Panel1.Controls.Add(this.flpAcao);
            // 
            // pnlForm
            // 
            this.pnlForm.Controls.Add(this.listProcessamento);
            this.pnlForm.Controls.Add(this.cbAno);
            this.pnlForm.Controls.Add(this.cbMes);
            this.pnlForm.Controls.Add(this.lblPeriodo);
            this.pnlForm.Controls.Add(this.btnExcluirCliente);
            this.pnlForm.Controls.Add(this.btnCliente);
            this.pnlForm.Controls.Add(this.lblCliente);
            this.pnlForm.Controls.Add(this.textCliente);
            // 
            // flpAcao
            // 
            this.flpAcao.Controls.Add(this.btnProcessar);
            this.flpAcao.Controls.Add(this.btFechar);
            this.flpAcao.Dock = System.Windows.Forms.DockStyle.Fill;
            this.flpAcao.Location = new System.Drawing.Point(0, 0);
            this.flpAcao.Name = "flpAcao";
            this.flpAcao.Size = new System.Drawing.Size(784, 35);
            this.flpAcao.TabIndex = 0;
            // 
            // btnProcessar
            // 
            this.btnProcessar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnProcessar.Image = global::SWS.Properties.Resources.Gravar;
            this.btnProcessar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnProcessar.Location = new System.Drawing.Point(3, 3);
            this.btnProcessar.Name = "btnProcessar";
            this.btnProcessar.Size = new System.Drawing.Size(75, 23);
            this.btnProcessar.TabIndex = 4;
            this.btnProcessar.TabStop = false;
            this.btnProcessar.Text = "&Processar";
            this.btnProcessar.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnProcessar.UseVisualStyleBackColor = true;
            this.btnProcessar.Click += new System.EventHandler(this.btnProcessar_Click);
            // 
            // btFechar
            // 
            this.btFechar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btFechar.Image = global::SWS.Properties.Resources.close;
            this.btFechar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btFechar.Location = new System.Drawing.Point(84, 3);
            this.btFechar.Name = "btFechar";
            this.btFechar.Size = new System.Drawing.Size(75, 23);
            this.btFechar.TabIndex = 5;
            this.btFechar.TabStop = false;
            this.btFechar.Text = "&Fechar";
            this.btFechar.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btFechar.UseVisualStyleBackColor = true;
            this.btFechar.Click += new System.EventHandler(this.btFechar_Click);
            // 
            // btnCliente
            // 
            this.btnCliente.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnCliente.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnCliente.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCliente.Image = global::SWS.Properties.Resources.busca;
            this.btnCliente.Location = new System.Drawing.Point(689, 3);
            this.btnCliente.Name = "btnCliente";
            this.btnCliente.Size = new System.Drawing.Size(34, 21);
            this.btnCliente.TabIndex = 4;
            this.btnCliente.Text = " ";
            this.btnCliente.UseVisualStyleBackColor = true;
            this.btnCliente.Click += new System.EventHandler(this.btnCliente_Click);
            // 
            // lblCliente
            // 
            this.lblCliente.BackColor = System.Drawing.Color.LightGray;
            this.lblCliente.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblCliente.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCliente.Location = new System.Drawing.Point(3, 3);
            this.lblCliente.MaxLength = 15;
            this.lblCliente.Name = "lblCliente";
            this.lblCliente.ReadOnly = true;
            this.lblCliente.Size = new System.Drawing.Size(147, 21);
            this.lblCliente.TabIndex = 2;
            this.lblCliente.TabStop = false;
            this.lblCliente.Text = "Cliente";
            // 
            // textCliente
            // 
            this.textCliente.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.textCliente.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.textCliente.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textCliente.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.textCliente.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textCliente.ForeColor = System.Drawing.Color.Blue;
            this.textCliente.Location = new System.Drawing.Point(149, 3);
            this.textCliente.MaxLength = 100;
            this.textCliente.Name = "textCliente";
            this.textCliente.ReadOnly = true;
            this.textCliente.Size = new System.Drawing.Size(544, 21);
            this.textCliente.TabIndex = 3;
            this.textCliente.TabStop = false;
            // 
            // btnExcluirCliente
            // 
            this.btnExcluirCliente.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnExcluirCliente.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnExcluirCliente.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnExcluirCliente.Image = global::SWS.Properties.Resources.close;
            this.btnExcluirCliente.Location = new System.Drawing.Point(722, 3);
            this.btnExcluirCliente.Name = "btnExcluirCliente";
            this.btnExcluirCliente.Size = new System.Drawing.Size(34, 21);
            this.btnExcluirCliente.TabIndex = 69;
            this.btnExcluirCliente.TabStop = false;
            this.btnExcluirCliente.UseVisualStyleBackColor = true;
            this.btnExcluirCliente.Click += new System.EventHandler(this.btnExcluirCliente_Click);
            // 
            // cbAno
            // 
            this.cbAno.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.cbAno.BackColor = System.Drawing.Color.LightGray;
            this.cbAno.BorderColor = System.Drawing.Color.DimGray;
            this.cbAno.BorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid;
            this.cbAno.FormattingEnabled = true;
            this.cbAno.Location = new System.Drawing.Point(222, 23);
            this.cbAno.Name = "cbAno";
            this.cbAno.Size = new System.Drawing.Size(48, 21);
            this.cbAno.TabIndex = 91;
            // 
            // cbMes
            // 
            this.cbMes.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.cbMes.BackColor = System.Drawing.Color.LightGray;
            this.cbMes.BorderColor = System.Drawing.Color.DimGray;
            this.cbMes.BorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid;
            this.cbMes.FormattingEnabled = true;
            this.cbMes.Location = new System.Drawing.Point(149, 23);
            this.cbMes.Name = "cbMes";
            this.cbMes.Size = new System.Drawing.Size(74, 21);
            this.cbMes.TabIndex = 90;
            // 
            // lblPeriodo
            // 
            this.lblPeriodo.BackColor = System.Drawing.Color.LightGray;
            this.lblPeriodo.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblPeriodo.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPeriodo.Location = new System.Drawing.Point(3, 23);
            this.lblPeriodo.Name = "lblPeriodo";
            this.lblPeriodo.ReadOnly = true;
            this.lblPeriodo.Size = new System.Drawing.Size(147, 21);
            this.lblPeriodo.TabIndex = 89;
            this.lblPeriodo.TabStop = false;
            this.lblPeriodo.Text = "Período de Emissão";
            // 
            // listProcessamento
            // 
            this.listProcessamento.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.listProcessamento.FormattingEnabled = true;
            this.listProcessamento.ItemHeight = 15;
            this.listProcessamento.Location = new System.Drawing.Point(4, 51);
            this.listProcessamento.Name = "listProcessamento";
            this.listProcessamento.Size = new System.Drawing.Size(752, 394);
            this.listProcessamento.TabIndex = 92;
            // 
            // frmProcessamentoMovimento
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(784, 561);
            this.Name = "frmProcessamentoMovimento";
            this.Text = "REPROCESSAR MOVIMENTO";
            this.spForm.Panel1.ResumeLayout(false);
            this.spForm.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.spForm)).EndInit();
            this.spForm.ResumeLayout(false);
            this.pnlForm.ResumeLayout(false);
            this.pnlForm.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.banner)).EndInit();
            this.flpAcao.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.FlowLayoutPanel flpAcao;
        private System.Windows.Forms.Button btnProcessar;
        private System.Windows.Forms.Button btFechar;
        private System.Windows.Forms.Button btnCliente;
        private System.Windows.Forms.TextBox lblCliente;
        private System.Windows.Forms.TextBox textCliente;
        private System.Windows.Forms.Button btnExcluirCliente;
        protected ComboBoxWithBorder cbAno;
        protected ComboBoxWithBorder cbMes;
        private System.Windows.Forms.TextBox lblPeriodo;
        private System.Windows.Forms.ListBox listProcessamento;
    }
}