﻿using SWS.Entidade;
using SWS.Facade;
using SWS.View.ViewHelper;
using SWS.ViewHelper;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace SWS.View
{
    public partial class frmEsocial2240Lote : frmTemplate
    {
        private Cliente cliente;

        public Cliente Cliente
        {
            get { return cliente; }
            set { cliente = value; }
        }

        private List<Monitoramento> funcionariosInMonitoramento = new List<Monitoramento>();
        
        
        public frmEsocial2240Lote()
        {
            InitializeComponent();

            ComboHelper.comboMes(cbMesMonitoramento);
            ComboHelper.comboAno(cbAnoMonitoramento);

            /* setando valores default para os combos */

            cbMesMonitoramento.SelectedValue = DateTime.Now.Month.ToString();
            cbAnoMonitoramento.SelectedValue = DateTime.Now.Year.ToString();
        }

        private void btnFechar_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnPesquisar_Click(object sender, EventArgs e)
        {
            try
            {
                if (!validaDadosTela())
                {
                    /* iniciando processo de busca dos monitoramentos dos colaboradores */
                    this.Cursor = Cursors.WaitCursor;

                    EsocialFacade esocialFacade = EsocialFacade.getInstance();

                    int ano = Convert.ToInt32(((SelectItem)cbAnoMonitoramento.SelectedItem).Valor);
                    int mes = Convert.ToInt32(((SelectItem)cbMesMonitoramento.SelectedItem).Valor);
                    int dia = DateTime.DaysInMonth(ano, mes);

                    funcionariosInMonitoramento = esocialFacade.findAllMonitoramentoByClienteAndPeriodo(cliente, new DateTime(ano, mes , dia));
                    montaDataGrid();
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }

        private void btnGerar_Click(object sender, EventArgs e)
        {
            try
            {
                /* verificando se já foi gerado algum lote para o cliente no período */
                EsocialFacade esocialFacade = EsocialFacade.getInstance();

                if (esocialFacade.verificaExisteLoteJaGeradoByClienteAndPeriodo(cliente, new DateTime(Convert.ToInt32(((SelectItem)cbAnoMonitoramento.SelectedItem).Valor), Convert.ToInt32(((SelectItem)cbMesMonitoramento.SelectedItem).Valor), DateTime.DaysInMonth(Convert.ToInt32(((SelectItem)cbAnoMonitoramento.SelectedItem).Valor), Convert.ToInt32(((SelectItem)cbMesMonitoramento.SelectedItem).Valor)))))
                    throw new Exception("Já existe um lote E-Social gerado para o cliente nesse periodo.");

                
                /* TODO - > verificando se ainda falta algum funcionário pendentes de exportação dado
                 * a nova regra de negócio. Se não for uma carga inicial, deverá ser verificado se existe algum atendimento
                 * pendente para o cliente selecionado */
                
                /* verificando se existe algum funcionário que falta monitoração no período */


                //List<ClienteFuncionario> funcionariosPendentes = esocialFacade.findAllPendentesInLote(cliente, new DateTime(dtPeriodo.Value.Year, dtPeriodo.Value.Month, DateTime.DaysInMonth(dtPeriodo.Value.Year, dtPeriodo.Value.Month)));
                //if (funcionariosPendentes.Count > 0)
                //    throw new Exception("Ainda restam: " + funcionariosPendentes.Count + " para gerar o monitoramento. O lote não poderá ser exportado ainda.");

                /* gerar o lote */
                esocialFacade.gerarLoteModelo2240(funcionariosInMonitoramento);
                MessageBox.Show("Lote gerado com sucesso.", "Confirmação", MessageBoxButtons.OK, MessageBoxIcon.Information);
                this.Close();

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btnCliente_Click(object sender, EventArgs e)
        {
            try
            {
                frmClienteSelecionar selecionarCliente = new frmClienteSelecionar(null, null, false, null, null, false, null, null, true);
                selecionarCliente.ShowDialog();

                if (selecionarCliente.Cliente != null)
                {
                    this.Cliente = selecionarCliente.Cliente;
                    this.textCliente.Text = this.Cliente.RazaoSocial + " - " + ((int)this.Cliente.tipoCliente() == 1 ? ValidaCampoHelper.FormataCnpj(this.Cliente.Cnpj) : ValidaCampoHelper.FormataCpf(this.Cliente.Cnpj));
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btnExcluirCliente_Click(object sender, EventArgs e)
        {
            try
            {
                if (this.Cliente == null) throw new Exception("Selecione primeiro o cliente.");
                this.Cliente = null;
                textCliente.Text = string.Empty;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private bool validaDadosTela()
        {
            bool validaDados = false;
            try
            {
                /* verificando se o cliente foi selecionado */
                if (this.Cliente == null)
                    throw new Exception("É necessário selecionar o cliente");

                /* verificando se já foi gerado algum lote para o cliente no período */
                EsocialFacade esocialFacade = EsocialFacade.getInstance();

                if (esocialFacade.verificaExisteLoteJaGeradoByClienteAndPeriodo(cliente, new DateTime(Convert.ToInt32(((SelectItem)cbAnoMonitoramento.SelectedItem).Valor), Convert.ToInt32(((SelectItem)cbMesMonitoramento.SelectedItem).Valor), DateTime.DaysInMonth(Convert.ToInt32(((SelectItem)cbAnoMonitoramento.SelectedItem).Valor), Convert.ToInt32(((SelectItem)cbMesMonitoramento.SelectedItem).Valor)))))
                    throw new Exception("Já existe um lote E-Social gerado para o cliente nesse periodo");
            

            }
            catch (Exception ex)
            {
                validaDados = true;
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            return validaDados;

        }

        private void montaDataGrid()
        {
            try
            {
                this.dgvFuncionario.Columns.Clear();
                this.textTotal.Text = string.Empty;
                this.Cursor = Cursors.WaitCursor;

                this.dgvFuncionario.ColumnCount = 10;

                this.dgvFuncionario.Columns[0].HeaderText = "idMonitoramento";
                this.dgvFuncionario.Columns[0].Visible = false;

                this.dgvFuncionario.Columns[1].HeaderText = "idClienteFuncionario";
                this.dgvFuncionario.Columns[1].Visible = false;

                this.dgvFuncionario.Columns[2].HeaderText = "idFuncionario";
                this.dgvFuncionario.Columns[2].Visible = false;

                this.dgvFuncionario.Columns[3].HeaderText = "Nome do Funcionário";
                this.dgvFuncionario.Columns[4].HeaderText = "CPF";
                this.dgvFuncionario.Columns[5].HeaderText = "RG";
                this.dgvFuncionario.Columns[6].HeaderText = "PIS/PASEP";
                this.dgvFuncionario.Columns[7].HeaderText = "Data de Nascimento";
                this.dgvFuncionario.Columns[8].HeaderText = "Data de Admissão";
                this.dgvFuncionario.Columns[9].HeaderText = "Matrícula";

                /* montando a informação da grid através da lista de clienteFuncionario */

                foreach (Monitoramento monitoramento in funcionariosInMonitoramento)
                {
                    this.dgvFuncionario.Rows.Add(
                        monitoramento.Id,
                        monitoramento.ClienteFuncaoFuncionario != null ? monitoramento.ClienteFuncaoFuncionario.Id : monitoramento.ClienteFuncionario.Id,
                        monitoramento.ClienteFuncaoFuncionario != null ? monitoramento.ClienteFuncaoFuncionario.Funcionario.Id : monitoramento.ClienteFuncionario.Funcionario.Id,
                        monitoramento.ClienteFuncaoFuncionario != null ? monitoramento.ClienteFuncaoFuncionario.Funcionario.Nome : monitoramento.ClienteFuncionario.Funcionario.Nome,
                        ValidaCampoHelper.FormataCpf(monitoramento.ClienteFuncaoFuncionario != null ? monitoramento.ClienteFuncaoFuncionario.Funcionario.Cpf : monitoramento.ClienteFuncionario.Funcionario.Cpf),
                        monitoramento.ClienteFuncaoFuncionario != null ? monitoramento.ClienteFuncaoFuncionario.Funcionario.Rg : monitoramento.ClienteFuncionario.Funcionario.Rg,
                        monitoramento.ClienteFuncaoFuncionario != null ? monitoramento.ClienteFuncaoFuncionario.Funcionario.PisPasep : monitoramento.ClienteFuncionario.Funcionario.PisPasep,
                        String.Format("{0:dd/MM/yyyy}", monitoramento.ClienteFuncaoFuncionario != null ? monitoramento.ClienteFuncaoFuncionario.Funcionario.DataNascimento : monitoramento.ClienteFuncionario.Funcionario.DataNascimento),
                        String.Format("{0:dd/MM/yyyy}", monitoramento.ClienteFuncaoFuncionario != null ? monitoramento.ClienteFuncaoFuncionario.DataCadastro : monitoramento.ClienteFuncionario.DataAdmissao),
                        monitoramento.ClienteFuncaoFuncionario != null ? monitoramento.ClienteFuncaoFuncionario.Matricula : monitoramento.ClienteFuncionario.Matricula
                    );
                }

                this.textTotal.Text = funcionariosInMonitoramento.Count.ToString();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }

        }
    }
}
