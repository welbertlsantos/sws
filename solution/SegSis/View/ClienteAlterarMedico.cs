﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using SegSis.Entidade;
using SegSis.Facade;

namespace SegSis.View
{
    public partial class frm_ClienteAlterarMedico : BaseFormConsulta
    {

        private Medico coordenador = null;

        public Medico getCoordenador()
        {
            return this.coordenador;
        }

        private static String Msg01 = " Selecione um linha";
        private static string Msg02 = " Atenção";

        public frm_ClienteAlterarMedico()
        {
            InitializeComponent();
            validaPermissoes();
        }

        private void validaPermissoes()
        {
            PermissionamentoFacade permissionamentoFacade = PermissionamentoFacade.getInstance();

            this.btn_novo.Enabled = permissionamentoFacade.hasPermission("MEDICO", "INCLUIR");
        }

        private void btn_buscar_Click(object sender, EventArgs e)
        {
            try
            {
                montaDataGrid();
                text_nome.Focus();
            }
            catch (Exception Ex)
            {
                MessageBox.Show(Ex.Message);
            }
        }

        private void btn_ok_Click(object sender, EventArgs e)
        {
            try
            {
                if (grd_medico.CurrentRow != null)
                {
                    PcmsoFacade pcmsoFacade = PcmsoFacade.getInstance();
                    coordenador = pcmsoFacade.findMedicoById((Int64)grd_medico.CurrentRow.Cells[0].Value);
                    this.Close();
                }
                else
                {
                    MessageBox.Show(Msg01, Msg02);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }

        }

        private void btn_novo_Click(object sender, EventArgs e)
        {
            frm_MedicoIncluir formMedicoIncluir = new frm_MedicoIncluir();
            formMedicoIncluir.ShowDialog();

            if (formMedicoIncluir.getMedico() != null)
            {
                coordenador = formMedicoIncluir.getMedico();
                text_nome.Text = coordenador.getNome();
                montaDataGrid();
                btn_ok.PerformClick();
            }
            
            this.Close();
        }

        private void btn_fechar_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        public void montaDataGrid()
        {
            PcmsoFacade pcmsoFacade = PcmsoFacade.getInstance();
            
            grd_medico.Columns.Clear();

            DataSet ds = pcmsoFacade.findMedicoByNameByAso(new Medico(null, text_nome.Text.Trim().ToUpper(), String.Empty, String.Empty));

            grd_medico.DataSource = ds.Tables["Medicos"].DefaultView;

            grd_medico.Columns[0].HeaderText = "ID";
            grd_medico.Columns[0].Visible = false;

            grd_medico.Columns[1].HeaderText = "Nome";
            grd_medico.Columns[2].HeaderText = "CRM";
            grd_medico.Columns[3].HeaderText = "Situação";
            grd_medico.Columns[3].Visible = false;
            grd_medico.Columns[4].HeaderText = "Telefone";
            grd_medico.Columns[5].HeaderText = "Celular";
            grd_medico.Columns[6].HeaderText = "Email";
            grd_medico.Columns[7].HeaderText = "Endereço";
            grd_medico.Columns[8].HeaderText = "Número";
            grd_medico.Columns[9].HeaderText = "Complemento";
            grd_medico.Columns[10].HeaderText = "Bairro";
            grd_medico.Columns[11].HeaderText = "CEP";
            grd_medico.Columns[12].HeaderText = "Cidade";
            grd_medico.Columns[13].HeaderText = "UF";
        }
    }
}
