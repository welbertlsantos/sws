﻿using SWS.Entidade;
using SWS.Facade;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace SWS.View
{
    public partial class frmUsuarioTecnicoSelecionar : frmTemplateConsulta
    {
        private Usuario usuario;

        public Usuario Usuario
        {
            get { return usuario; }
            set { usuario = value; }
        }
        
        public frmUsuarioTecnicoSelecionar()
        {
            InitializeComponent();
            montaGrid();
        }

        private void btnSelecionar_Click(object sender, EventArgs e)
        {
            try
            {
                if (dgvUsuarioTecnico.CurrentRow == null) throw new Exception("Selecione uma linha");
                UsuarioFacade usuarioFacade = UsuarioFacade.getInstance();
                this.Usuario = usuarioFacade.findUsuarioById((long)dgvUsuarioTecnico.CurrentRow.Cells[0].Value);
                this.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }

        private void btnFechar_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void montaGrid()
        {
            try
            {
                this.dgvUsuarioTecnico.Columns.Clear();
                this.Cursor = Cursors.WaitCursor;

                UsuarioFacade usuarioFacade = UsuarioFacade.getInstance();
                Usuario usuarioTecnico = new Usuario();

                DataSet ds = usuarioFacade.findUsuarioElaboraDocumento(usuarioTecnico);

                this.dgvUsuarioTecnico.DataSource = ds.Tables["Usuarios"].DefaultView;

                this.dgvUsuarioTecnico.Columns[0].HeaderText = "id";
                this.dgvUsuarioTecnico.Columns[0].Visible = false;
                this.dgvUsuarioTecnico.Columns[1].HeaderText = "Nome do Usuário";

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }

        }

    }
}
