﻿using SWS.Entidade;
using SWS.Facade;
using SWS.View.Resources;
using SWS.View.ViewHelper;
using SWS.ViewHelper;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace SWS.View
{
    public partial class frmCnaeIncluir : frmTemplate
    {
        private Cnae cnae;

        public Cnae Cnae
        {
            get { return cnae; }
            set { cnae = value; }
        }
        
        public frmCnaeIncluir()
        {
            InitializeComponent();
            ComboHelper.grauRisco(cbGrauRisco);
            ActiveControl = textCodigo;
        }

        protected virtual void bt_incluir_Click(object sender, EventArgs e)
        {
            this.IncluirCnaeErrorProvider.Clear();

            try
            {
                if (validaCamposObrigatorio())
                {

                    ClienteFacade clienteFacade = ClienteFacade.getInstance();

                    if (!validadorCnae(textCodigo.Text))
                        throw new Exception("CNAE inválido. Padrão aceito é 99.99-9/99.");

                    cnae = clienteFacade.incluirCnae(new Cnae(null, ValidaCampoHelper.RetornaCnaSemFormato(textCodigo.Text), textAtividade.Text, (((SelectItem)cbGrauRisco.SelectedItem).Valor), textGrupo.Text, ApplicationConstants.ATIVO));

                    MessageBox.Show("CNAE incluído com sucesso", "Confirmação", MessageBoxButtons.OK, MessageBoxIcon.Information);

                    this.Close();
                }
            }
            catch (Exception Ex)
            {
                MessageBox.Show(Ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        protected void bt_limpar_Click(object sender, EventArgs e)
        {
            textAtividade.Text = string.Empty;
            textCodigo.Text = string.Empty;
            ComboHelper.grauRisco(cbGrauRisco);
            textGrupo.Text = string.Empty;
            textCodigo.Focus();
            IncluirCnaeErrorProvider.Clear();
        }

        protected void bt_fechar_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        protected Boolean validaCamposObrigatorio()
        {
            bool retorno = true;

            if (string.IsNullOrEmpty(ValidaCampoHelper.RetornaCnaSemFormato(textCodigo.Text).Trim()))
            {
                this.IncluirCnaeErrorProvider.SetError(this.textCodigo, "Campo obrigatório!");
                retorno = false;
            }

            if (string.IsNullOrEmpty(textAtividade.Text))
            {
                this.IncluirCnaeErrorProvider.SetError(this.textAtividade, "Campo obrigatório!");
                retorno = false;
            }

            return retorno;
        }

        protected Boolean validadorCnae(String codigo)
        {
            bool retorno = true;
            
            if (ValidaCampoHelper.RetornaCnaSemFormato(textCodigo.Text.Trim()).Length < 7)
                retorno = false;

            return retorno;
        }

        private void frmCnaeIncluir_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter || e.KeyCode == Keys.Return)
                SendKeys.Send("{TAB}");
        }
    }
}
