﻿using SWS.Entidade;
using SWS.Facade;
using SWS.View.Resources;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace SWS.View
{
    public partial class frmFonteIncluir : frmTemplate
    {
        private Fonte fonte;

        public Fonte Fonte
        {
            get { return fonte; }
            set { fonte = value; }
        }
        
        public frmFonteIncluir()
        {
            InitializeComponent();
            ActiveControl = textDescricao;
        }

        protected virtual void btnGravar_Click(object sender, EventArgs e)
        {
            try
            {
                if (ValidaTela())
                {
                    EstudoFacade pcmsoFacade = EstudoFacade.getInstance();

                    fonte = pcmsoFacade.insertFonte(new Fonte(null, textDescricao.Text, ApplicationConstants.ATIVO, false));
                    MessageBox.Show("Fonte incluída com sucesso.", "Confirmação", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    this.Close();
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }

        protected void btnFechar_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        protected bool ValidaTela()
        {
            bool result = true;
            
            try
            {
                if (string.IsNullOrEmpty(textDescricao.Text.Trim()))
                    throw new Exception("Campo descrição é obrigatório.");

            }
            catch (Exception ex)
            {
                result = false;
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            return result;
        }

        protected void frmFonteIncluir_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Return || e.KeyCode == Keys.Enter)
                SendKeys.Send("{TAB}");

        }
    }
}
