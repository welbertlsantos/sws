﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using SWS.Entidade;
using SWS.Facade;
using SWS.View.Resources;
using SWS.View.ViewHelper;

namespace SWS.View
{
    public partial class frmPcmsoFuncoesGhe : SWS.View.frmTemplateConsulta
    {
        
        private static String msg1 = "Selecione uma linha";
        private static String msg2 = "Número maior do que o permitido para o campo. ";

        private Exame exame;
        private Ghe ghe;

        private Dictionary<Int64, Dictionary<Funcao, Int32>> excecaoIdadeFuncao = new Dictionary<Int64, Dictionary<Funcao, Int32>>();

        public Dictionary<Int64, Dictionary<Funcao, Int32>> ExcecaoIdadeFuncao
        {
            get { return excecaoIdadeFuncao; }
            set { excecaoIdadeFuncao = value; }
        }

        public frmPcmsoFuncoesGhe(Ghe ghe, Exame exame)
        {
            InitializeComponent();
            this.ghe = ghe;
            this.exame = exame;
            textGhe.Text = ghe.Descricao;
            textExame.Text = exame.Descricao;
            ActiveControl = textFuncao;
            
        }

        private void btnFechar_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnLimpar_Click(object sender, EventArgs e)
        {
            textFuncao.Text = String.Empty;
            dgvFuncao.Columns.Clear();
        }

        public void montaDataGrid()
        {
            try
            {
                EstudoFacade pcmsoFacade = EstudoFacade.getInstance();
                ClienteFacade clienteFacade = ClienteFacade.getInstance();

                this.Cursor = Cursors.WaitCursor;

                dgvFuncao.Columns.Clear();

                DataSet ds = pcmsoFacade.findAllFuncaoNotInGheSetorClienteFuncaoExame(exame, ghe, new Funcao(null, textFuncao.Text, String.Empty, ApplicationConstants.ATIVO, String.Empty));

                dgvFuncao.DataSource = ds.Tables["Funcoes"].DefaultView;

                dgvFuncao.Columns[0].HeaderText = "Código Função";
                dgvFuncao.Columns[0].ReadOnly = true;
                dgvFuncao.Columns[0].Width = 50;

                dgvFuncao.Columns[1].HeaderText = "Descrição";
                dgvFuncao.Columns[1].ReadOnly = true;
                dgvFuncao.Columns[1].Width = 350;

                dgvFuncao.Columns[2].HeaderText = "Selec";
                dgvFuncao.Columns[2].DisplayIndex = 0;
                dgvFuncao.Columns[2].Width = 40;

                DataGridViewTextBoxColumn idade = new DataGridViewTextBoxColumn();
                idade.Name = "Idade Mínima";
                dgvFuncao.Columns.Add(idade);
                dgvFuncao.Columns[3].DefaultCellStyle.BackColor = Color.LightYellow;

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }

        private void btnBuscar_Click(object sender, EventArgs e)
        {
            try
            {
                montaDataGrid();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void dgvFuncao_DataError(object sender, DataGridViewDataErrorEventArgs e)
        {
            try
            {
                /*validando valor digitado para não dar estouro de inteiro32 */

                int number = Int32.Parse((String)dgvFuncao.Rows[e.RowIndex].Cells[e.ColumnIndex].EditedFormattedValue);
            }

            catch (OverflowException)
            {
                MessageBox.Show(msg2, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
        }

        private void dgvFuncao_EditingControlShowing(object sender, DataGridViewEditingControlShowingEventArgs e)
        {
            if (e.Control is DataGridViewTextBoxEditingControl)
            {
                e.Control.KeyPress += new KeyPressEventHandler(dgvFuncao_KeyPress);

            }
        }

        private void dgvFuncao_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = ValidaCampoHelper.FormataCampoNumerico(sender, e);
        }

        private void btnIncluir_Click(object sender, EventArgs e)
        {
            try
            {
                EstudoFacade pcmsoFacade = EstudoFacade.getInstance();
                this.Cursor = Cursors.WaitCursor;

                if (this.dgvFuncao.CurrentRow == null)
                    throw new Exception(msg1);


                foreach (DataGridViewRow dvRow in dgvFuncao.Rows)
                {
                    if ((Boolean)dvRow.Cells[2].Value == true)
                    {
                        Funcao funcao = pcmsoFacade.findFuncaoById((Int64)dvRow.Cells[0].Value);
                        excecaoIdadeFuncao.Add((Int64)dvRow.Cells[0].Value, new Dictionary<Funcao, Int32>() { { funcao, dvRow.Cells[3].Value == null ? 0 : Convert.ToInt32(dvRow.Cells[3].Value) } });
                    }
                }

                this.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }

        private void frmPcmsoFuncoesGhe_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.F4)
                btnIncluir.PerformClick();
        }
    }
}
