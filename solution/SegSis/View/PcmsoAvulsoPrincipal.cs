﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using SegSis.Entidade;
using SegSis.Facade;

namespace SegSis.View
{
    public partial class frm_PcmsoAvulsoPrincipal : BaseForm
    {
        private static String Ms01 = " Selecione um Pcmso.";
        private static String Ms02 = " Para pesquisar pir contratada é necessário escolher um Cliente.";

        public Cliente cliente;
        public Cliente clienteContratada;
        public Estudo pcmso = new Estudo();

        public frm_PcmsoAvulsoPrincipal()
        {
            InitializeComponent();
            validaPermissoes();
        }

        private void validaPermissoes()
        {
            PermissionamentoFacade permissionamentoFacade = PermissionamentoFacade.getInstance();

            this.btn_incluir.Enabled = permissionamentoFacade.hasPermission("PCMSO_AVULSO", "INCLUIR");
            this.btn_alterar.Enabled = permissionamentoFacade.hasPermission("PCMSO_AVULSO", "ALTERAR");
        }

        internal void setTextCliente(String texto)
        {
            this.text_cliente.Text = texto;
        }

        internal void setTextContratada(String texto)
        {
            this.text_contratada.Text = texto;
        }

        private void btn_fechar_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btn_alterar_Click(object sender, EventArgs e)
        {
            if (pcmso.getIdEstudo() != null)
            {
                frm_PcmsoAvulso manterFila = new frm_PcmsoAvulso(pcmso);
                manterFila.ShowDialog();
            }
            else
            {
                MessageBox.Show(Ms01);
            }
        }

        private void btn_incluir_Click(object sender, EventArgs e)
        {
            frm_PcmsoAvulso manterFila = new frm_PcmsoAvulso(null);
            manterFila.ShowDialog();
        }

        private void btn_limpar_Click(object sender, EventArgs e)
        {
            this.cliente = null;
            this.clienteContratada = null;
            text_contratada.Text = String.Empty;
            text_cliente.Text = String.Empty;

            grd_pcmso.Columns.Clear();
        }

        private void btn_pesquisar_Click(object sender, EventArgs e)
        {
            if (cliente == null && clienteContratada != null)
            {
                MessageBox.Show(Ms02);
            }
            else
            {
                montaGridPcmso();
                text_codPcmso.Focus();
            }
        }

        private void btn_cliente_Click(object sender, EventArgs e)
        {
            frm_ClienteTipoSelecionar formCliente = new frm_ClienteTipoSelecionar(null, null, false, null, null);
            formCliente.ShowDialog();

            if (formCliente.getCliente() != null)
            {
                cliente = formCliente.getCliente();
                text_cliente.Text = cliente.getRazaoSocial();
            }
        }

        private void btn_contrato_Click(object sender, EventArgs e)
        {
            frm_ClienteTipoSelecionar formCliente = new frm_ClienteTipoSelecionar(null, null, null, null, null);
            formCliente.ShowDialog();

            if (formCliente.getCliente() != null)
            {
                clienteContratada = formCliente.getCliente();
                text_contratada.Text = clienteContratada.getRazaoSocial();
            }
        }

        public void montaGridPcmso()
        {
            try
            {
                PcmsoFacade pcmsoFacade = PcmsoFacade.getInstance();
                ClienteFacade clienteFacade = ClienteFacade.getInstance();
                pcmso = new Estudo();

                pcmso.setCodEstudo(text_codPcmso.Text);

                if (cliente != null)
                {
                    pcmso.setVendedorCliente(clienteFacade.findClienteVendedorByIdCliente(cliente.getId()));
                }
                else
                {
                    pcmso.setVendedorCliente(new VendedorCliente());
                }
                if (clienteContratada != null)
                {
                    pcmso.setClienteContratante(clienteContratada);
                }

                pcmso.setAvulso(true);

                this.Cursor = Cursors.WaitCursor;

                DataSet ds = pcmsoFacade.findByFilter(pcmso, null);

                grd_pcmso.DataSource = ds.Tables["Pcmso"].DefaultView;

                grd_pcmso.Columns[0].HeaderText = "ID";
                grd_pcmso.Columns[1].HeaderText = "Código PCMSO";
                grd_pcmso.Columns[2].HeaderText = "Data da Criação";
                grd_pcmso.Columns[3].HeaderText = "Data de Vencimento";
                grd_pcmso.Columns[4].HeaderText = "Data de Fechamento";
                grd_pcmso.Columns[5].HeaderText = "Situação";
                grd_pcmso.Columns[6].HeaderText = "Cliente";
                grd_pcmso.Columns[7].HeaderText = "Cliente Contratado";

                grd_pcmso.Columns[0].Visible = false;
                grd_pcmso.Columns[3].Visible = false;
                grd_pcmso.Columns[4].Visible = false;
                grd_pcmso.Columns[5].Visible = false;

                for (int i = 0; i < 6; i++)
                {
                    grd_pcmso.Columns[i].DisplayIndex = i + 1;
                }

                grd_pcmso.Columns[6].DisplayIndex = 0;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                 this.Cursor = Cursors.Default;
            }
        }

        private void grd_pcmso_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                PcmsoFacade pcmsoFacade = PcmsoFacade.getInstance();
                ClienteFacade clienteFacade = ClienteFacade.getInstance();

                Int32 cellValue = grd_pcmso.CurrentRow.Index;
                pcmso = pcmsoFacade.findById((Int64)grd_pcmso.Rows[cellValue].Cells[0].Value);
                if (pcmso.getClienteContratante() != null)
                {
                    pcmso.setClienteContratante(clienteFacade.findClienteById((Int64)pcmso.getClienteContratante().getId()));
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }
    }
}