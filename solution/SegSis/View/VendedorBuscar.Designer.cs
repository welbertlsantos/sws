﻿namespace SWS.View
{
    partial class frmVendedorBuscar
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            this.flpAcao = new System.Windows.Forms.FlowLayoutPanel();
            this.btn_ok = new System.Windows.Forms.Button();
            this.btn_incluir = new System.Windows.Forms.Button();
            this.btn_buscar = new System.Windows.Forms.Button();
            this.btn_fechar = new System.Windows.Forms.Button();
            this.grb_gride = new System.Windows.Forms.GroupBox();
            this.dgvVendedor = new System.Windows.Forms.DataGridView();
            this.lblVendedor = new System.Windows.Forms.TextBox();
            this.text_nome = new System.Windows.Forms.TextBox();
            ((System.ComponentModel.ISupportInitialize)(this.scForm)).BeginInit();
            this.scForm.Panel1.SuspendLayout();
            this.scForm.Panel2.SuspendLayout();
            this.scForm.SuspendLayout();
            this.pnlForm.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.banner)).BeginInit();
            this.flpAcao.SuspendLayout();
            this.grb_gride.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvVendedor)).BeginInit();
            this.SuspendLayout();
            // 
            // scForm
            // 
            // 
            // scForm.Panel1
            // 
            this.scForm.Panel1.Controls.Add(this.flpAcao);
            // 
            // pnlForm
            // 
            this.pnlForm.Controls.Add(this.text_nome);
            this.pnlForm.Controls.Add(this.lblVendedor);
            this.pnlForm.Controls.Add(this.grb_gride);
            // 
            // flpAcao
            // 
            this.flpAcao.Controls.Add(this.btn_ok);
            this.flpAcao.Controls.Add(this.btn_incluir);
            this.flpAcao.Controls.Add(this.btn_buscar);
            this.flpAcao.Controls.Add(this.btn_fechar);
            this.flpAcao.Dock = System.Windows.Forms.DockStyle.Fill;
            this.flpAcao.Location = new System.Drawing.Point(0, 0);
            this.flpAcao.Name = "flpAcao";
            this.flpAcao.Size = new System.Drawing.Size(534, 30);
            this.flpAcao.TabIndex = 0;
            // 
            // btn_ok
            // 
            this.btn_ok.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_ok.Image = global::SWS.Properties.Resources.fechar_ico;
            this.btn_ok.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btn_ok.Location = new System.Drawing.Point(3, 3);
            this.btn_ok.Name = "btn_ok";
            this.btn_ok.Size = new System.Drawing.Size(77, 23);
            this.btn_ok.TabIndex = 8;
            this.btn_ok.TabStop = false;
            this.btn_ok.Text = "&Confirmar";
            this.btn_ok.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btn_ok.UseVisualStyleBackColor = true;
            this.btn_ok.Click += new System.EventHandler(this.btn_ok_Click);
            // 
            // btn_incluir
            // 
            this.btn_incluir.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_incluir.Image = global::SWS.Properties.Resources.icone_mais;
            this.btn_incluir.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btn_incluir.Location = new System.Drawing.Point(86, 3);
            this.btn_incluir.Name = "btn_incluir";
            this.btn_incluir.Size = new System.Drawing.Size(75, 23);
            this.btn_incluir.TabIndex = 9;
            this.btn_incluir.TabStop = false;
            this.btn_incluir.Text = "&Novo";
            this.btn_incluir.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btn_incluir.UseVisualStyleBackColor = true;
            this.btn_incluir.Click += new System.EventHandler(this.btn_incluir_Click);
            // 
            // btn_buscar
            // 
            this.btn_buscar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_buscar.Image = global::SWS.Properties.Resources.lupa;
            this.btn_buscar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btn_buscar.Location = new System.Drawing.Point(167, 3);
            this.btn_buscar.Name = "btn_buscar";
            this.btn_buscar.Size = new System.Drawing.Size(77, 23);
            this.btn_buscar.TabIndex = 7;
            this.btn_buscar.TabStop = false;
            this.btn_buscar.Text = "&Pesquisar";
            this.btn_buscar.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btn_buscar.UseVisualStyleBackColor = true;
            this.btn_buscar.Click += new System.EventHandler(this.btn_buscar_Click);
            // 
            // btn_fechar
            // 
            this.btn_fechar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_fechar.Image = global::SWS.Properties.Resources.close;
            this.btn_fechar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btn_fechar.Location = new System.Drawing.Point(250, 3);
            this.btn_fechar.Name = "btn_fechar";
            this.btn_fechar.Size = new System.Drawing.Size(75, 23);
            this.btn_fechar.TabIndex = 10;
            this.btn_fechar.TabStop = false;
            this.btn_fechar.Text = "&Fechar";
            this.btn_fechar.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btn_fechar.UseVisualStyleBackColor = true;
            this.btn_fechar.Click += new System.EventHandler(this.btn_fechar_Click);
            // 
            // grb_gride
            // 
            this.grb_gride.BackColor = System.Drawing.Color.Transparent;
            this.grb_gride.Controls.Add(this.dgvVendedor);
            this.grb_gride.Location = new System.Drawing.Point(3, 40);
            this.grb_gride.Name = "grb_gride";
            this.grb_gride.Size = new System.Drawing.Size(491, 234);
            this.grb_gride.TabIndex = 35;
            this.grb_gride.TabStop = false;
            this.grb_gride.Text = "Vendedores";
            // 
            // dgvVendedor
            // 
            this.dgvVendedor.AllowUserToAddRows = false;
            this.dgvVendedor.AllowUserToDeleteRows = false;
            this.dgvVendedor.AllowUserToOrderColumns = true;
            this.dgvVendedor.AllowUserToResizeRows = false;
            this.dgvVendedor.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this.dgvVendedor.BackgroundColor = System.Drawing.Color.White;
            this.dgvVendedor.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.LightGray;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvVendedor.DefaultCellStyle = dataGridViewCellStyle1;
            this.dgvVendedor.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvVendedor.Location = new System.Drawing.Point(3, 16);
            this.dgvVendedor.MultiSelect = false;
            this.dgvVendedor.Name = "dgvVendedor";
            this.dgvVendedor.ReadOnly = true;
            this.dgvVendedor.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvVendedor.Size = new System.Drawing.Size(485, 215);
            this.dgvVendedor.TabIndex = 3;
            // 
            // lblVendedor
            // 
            this.lblVendedor.BackColor = System.Drawing.Color.LightGray;
            this.lblVendedor.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblVendedor.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblVendedor.Location = new System.Drawing.Point(6, 13);
            this.lblVendedor.Name = "lblVendedor";
            this.lblVendedor.ReadOnly = true;
            this.lblVendedor.Size = new System.Drawing.Size(100, 21);
            this.lblVendedor.TabIndex = 36;
            this.lblVendedor.TabStop = false;
            this.lblVendedor.Text = "Vendedor";
            // 
            // text_nome
            // 
            this.text_nome.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.text_nome.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.text_nome.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.text_nome.Location = new System.Drawing.Point(105, 13);
            this.text_nome.MaxLength = 100;
            this.text_nome.Name = "text_nome";
            this.text_nome.Size = new System.Drawing.Size(387, 21);
            this.text_nome.TabIndex = 37;
            // 
            // frmVendedorBuscar
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(534, 361);
            this.Name = "frmVendedorBuscar";
            this.Text = "SELECIONAR VENDEDOR";
            this.scForm.Panel1.ResumeLayout(false);
            this.scForm.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.scForm)).EndInit();
            this.scForm.ResumeLayout(false);
            this.pnlForm.ResumeLayout(false);
            this.pnlForm.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.banner)).EndInit();
            this.flpAcao.ResumeLayout(false);
            this.grb_gride.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvVendedor)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.FlowLayoutPanel flpAcao;
        private System.Windows.Forms.Button btn_buscar;
        private System.Windows.Forms.Button btn_incluir;
        private System.Windows.Forms.Button btn_fechar;
        private System.Windows.Forms.Button btn_ok;
        private System.Windows.Forms.TextBox lblVendedor;
        private System.Windows.Forms.GroupBox grb_gride;
        private System.Windows.Forms.DataGridView dgvVendedor;
        private System.Windows.Forms.TextBox text_nome;
    }
}