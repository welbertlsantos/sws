﻿using SWS.Entidade;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace SWS.View
{
    public partial class frmAtendimentoCheckSalaAtendeExame : frmTemplateConsulta
    {
        private SalaExame sala;

        public SalaExame Sala
        {
            get { return sala; }
            set { sala = value; }
        }
        private List<SalaExame> salas = new List<SalaExame>();
        
        public frmAtendimentoCheckSalaAtendeExame(List<SalaExame> salas)
        {
            InitializeComponent();
            this.salas = salas;
            montaGridSala();
        }

        private void btnFechar_Click(object sender, EventArgs e)
        {
            if (sala == null)
                if (MessageBox.Show("Deseja sair sem selecionar uma sala? Isso implicará do exame não ser relacionado a uma sala e impossibilitará o laudo pela fila de atendimento.", "Atenção", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                    this.Close();
            
        }

        private void montaGridSala()
        {
            try
            {
                /* preenchendo o texto exame para mostrar o usuário o exame selecionado */
                textExame.Text = this.salas[0].Exame.Id.ToString().PadLeft(3, '0') + " - " + this.salas[0].Exame.Descricao;
                
                dgvSala.Columns.Clear();
                dgvSala.ColumnCount = 2;

                dgvSala.Columns[0].HeaderText = "idSalaExame";
                dgvSala.Columns[0].Visible = false;

                dgvSala.Columns[1].HeaderText = "Sala";
                dgvSala.Columns[1].Width = 300;
                
                foreach(SalaExame se in this.salas)
                    dgvSala.Rows.Add((long)se.Id, se.Sala.Descricao);

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btnConfirmar_Click(object sender, EventArgs e)
        {
            try
            {
                if (dgvSala.CurrentRow == null)
                    throw new Exception("Selecione uma sala.");

                this.sala = salas.Find(x => x.Id == (long)dgvSala.CurrentRow.Cells[0].Value);
                this.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            
        }
    }
}
