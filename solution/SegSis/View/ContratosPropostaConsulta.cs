﻿using SWS.Entidade;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace SWS.View
{
    public partial class frmContratosPropostaConsulta : frmContratosPropostaIncluir
    {
        public frmContratosPropostaConsulta(Contrato proposta)
        {
            InitializeComponent();
            this.proposta = proposta;

            textCodigo.Text = proposta.CodigoContrato;
            dataElaboracao.Text = proposta.DataElaboracao.ToString();
            dataElaboracao.Enabled = false;
            dataVencimento.Text = proposta.DataFim.ToString();
            dataVencimento.Enabled = false;
            cliente = proposta.Cliente;
            textCliente.Text = proposta.Cliente.RazaoSocial;
            btnCliente.Enabled = false;

            clienteProposta = proposta.ClienteProposta;
            textPreCliente.Text = proposta.ClienteProposta.RazaoSocial;
            btnPreCliente.Enabled = false;

            MontaDataGridExame();
            MontaDataGridProduto();
            textComentario.Text = proposta.Observacao;
            textComentario.ReadOnly = true;
            textFormaPagamento.Text = proposta.PropostaFormaPagamento;
            textFormaPagamento.ReadOnly = true;

            btnGravar.Enabled = false;
            btnImprimir.Enabled = false;
            btnIncluirExame.Enabled = false;
            btnExcluirExame.Enabled = false;
            btnIncluirProduto.Enabled = false;
            btnExcluirProduto.Enabled = false;

                
        }
    }
}
