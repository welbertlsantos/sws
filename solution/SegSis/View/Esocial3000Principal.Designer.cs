﻿namespace SWS.View
{
    partial class frmEsocial3000Principal
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            this.flpAcao = new System.Windows.Forms.FlowLayoutPanel();
            this.btIncluir = new System.Windows.Forms.Button();
            this.btPesquisar = new System.Windows.Forms.Button();
            this.btExportar = new System.Windows.Forms.Button();
            this.btnFechar = new System.Windows.Forms.Button();
            this.btnExcluirCliente = new System.Windows.Forms.Button();
            this.btnCliente = new System.Windows.Forms.Button();
            this.textCliente = new System.Windows.Forms.TextBox();
            this.lblCliente = new System.Windows.Forms.TextBox();
            this.lblDataEmissao = new System.Windows.Forms.TextBox();
            this.lblTipoEsocial = new System.Windows.Forms.TextBox();
            this.cbTipoEvento = new SWS.ComboBoxWithBorder();
            this.grbEventos = new System.Windows.Forms.GroupBox();
            this.dgvLoteCancelado = new System.Windows.Forms.DataGridView();
            this.periodoInicial = new System.Windows.Forms.DateTimePicker();
            this.periodoFinal = new System.Windows.Forms.DateTimePicker();
            ((System.ComponentModel.ISupportInitialize)(this.spForm)).BeginInit();
            this.spForm.Panel1.SuspendLayout();
            this.spForm.Panel2.SuspendLayout();
            this.spForm.SuspendLayout();
            this.pnlForm.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.banner)).BeginInit();
            this.flpAcao.SuspendLayout();
            this.grbEventos.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvLoteCancelado)).BeginInit();
            this.SuspendLayout();
            // 
            // spForm
            // 
            // 
            // spForm.Panel1
            // 
            this.spForm.Panel1.Controls.Add(this.flpAcao);
            // 
            // pnlForm
            // 
            this.pnlForm.Controls.Add(this.periodoInicial);
            this.pnlForm.Controls.Add(this.periodoFinal);
            this.pnlForm.Controls.Add(this.grbEventos);
            this.pnlForm.Controls.Add(this.cbTipoEvento);
            this.pnlForm.Controls.Add(this.lblTipoEsocial);
            this.pnlForm.Controls.Add(this.lblDataEmissao);
            this.pnlForm.Controls.Add(this.btnExcluirCliente);
            this.pnlForm.Controls.Add(this.btnCliente);
            this.pnlForm.Controls.Add(this.textCliente);
            this.pnlForm.Controls.Add(this.lblCliente);
            // 
            // flpAcao
            // 
            this.flpAcao.Controls.Add(this.btIncluir);
            this.flpAcao.Controls.Add(this.btPesquisar);
            this.flpAcao.Controls.Add(this.btExportar);
            this.flpAcao.Controls.Add(this.btnFechar);
            this.flpAcao.Dock = System.Windows.Forms.DockStyle.Fill;
            this.flpAcao.Location = new System.Drawing.Point(0, 0);
            this.flpAcao.Name = "flpAcao";
            this.flpAcao.Size = new System.Drawing.Size(784, 35);
            this.flpAcao.TabIndex = 0;
            // 
            // btIncluir
            // 
            this.btIncluir.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btIncluir.Image = global::SWS.Properties.Resources.icone_mais;
            this.btIncluir.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btIncluir.Location = new System.Drawing.Point(3, 3);
            this.btIncluir.Name = "btIncluir";
            this.btIncluir.Size = new System.Drawing.Size(71, 22);
            this.btIncluir.TabIndex = 21;
            this.btIncluir.TabStop = false;
            this.btIncluir.Text = "&Incluir";
            this.btIncluir.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btIncluir.UseVisualStyleBackColor = true;
            this.btIncluir.Click += new System.EventHandler(this.btIncluir_Click);
            // 
            // btPesquisar
            // 
            this.btPesquisar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btPesquisar.Image = global::SWS.Properties.Resources.lupa;
            this.btPesquisar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btPesquisar.Location = new System.Drawing.Point(80, 3);
            this.btPesquisar.Name = "btPesquisar";
            this.btPesquisar.Size = new System.Drawing.Size(71, 22);
            this.btPesquisar.TabIndex = 24;
            this.btPesquisar.TabStop = false;
            this.btPesquisar.Text = "&Pesquisar";
            this.btPesquisar.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btPesquisar.UseVisualStyleBackColor = true;
            this.btPesquisar.Click += new System.EventHandler(this.btPesquisar_Click);
            // 
            // btExportar
            // 
            this.btExportar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btExportar.Image = global::SWS.Properties.Resources.nota;
            this.btExportar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btExportar.Location = new System.Drawing.Point(157, 3);
            this.btExportar.Name = "btExportar";
            this.btExportar.Size = new System.Drawing.Size(71, 22);
            this.btExportar.TabIndex = 23;
            this.btExportar.TabStop = false;
            this.btExportar.Text = "&Exportar";
            this.btExportar.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btExportar.UseVisualStyleBackColor = true;
            this.btExportar.Click += new System.EventHandler(this.btExportar_Click);
            // 
            // btnFechar
            // 
            this.btnFechar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnFechar.Image = global::SWS.Properties.Resources.close;
            this.btnFechar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnFechar.Location = new System.Drawing.Point(234, 3);
            this.btnFechar.Name = "btnFechar";
            this.btnFechar.Size = new System.Drawing.Size(71, 22);
            this.btnFechar.TabIndex = 25;
            this.btnFechar.TabStop = false;
            this.btnFechar.Text = "&Fechar";
            this.btnFechar.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnFechar.UseVisualStyleBackColor = true;
            this.btnFechar.Click += new System.EventHandler(this.btnFechar_Click);
            // 
            // btnExcluirCliente
            // 
            this.btnExcluirCliente.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnExcluirCliente.Image = global::SWS.Properties.Resources.close;
            this.btnExcluirCliente.Location = new System.Drawing.Point(731, 3);
            this.btnExcluirCliente.Name = "btnExcluirCliente";
            this.btnExcluirCliente.Size = new System.Drawing.Size(30, 21);
            this.btnExcluirCliente.TabIndex = 54;
            this.btnExcluirCliente.UseVisualStyleBackColor = true;
            this.btnExcluirCliente.Click += new System.EventHandler(this.btnExcluirCliente_Click);
            // 
            // btnCliente
            // 
            this.btnCliente.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnCliente.Image = global::SWS.Properties.Resources.busca;
            this.btnCliente.Location = new System.Drawing.Point(702, 3);
            this.btnCliente.Name = "btnCliente";
            this.btnCliente.Size = new System.Drawing.Size(30, 21);
            this.btnCliente.TabIndex = 1;
            this.btnCliente.UseVisualStyleBackColor = true;
            this.btnCliente.Click += new System.EventHandler(this.btnCliente_Click);
            // 
            // textCliente
            // 
            this.textCliente.BackColor = System.Drawing.SystemColors.Control;
            this.textCliente.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textCliente.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textCliente.Location = new System.Drawing.Point(140, 3);
            this.textCliente.MaxLength = 100;
            this.textCliente.Name = "textCliente";
            this.textCliente.ReadOnly = true;
            this.textCliente.Size = new System.Drawing.Size(563, 21);
            this.textCliente.TabIndex = 52;
            this.textCliente.TabStop = false;
            // 
            // lblCliente
            // 
            this.lblCliente.BackColor = System.Drawing.Color.LightGray;
            this.lblCliente.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblCliente.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCliente.Location = new System.Drawing.Point(3, 3);
            this.lblCliente.Name = "lblCliente";
            this.lblCliente.ReadOnly = true;
            this.lblCliente.Size = new System.Drawing.Size(138, 21);
            this.lblCliente.TabIndex = 51;
            this.lblCliente.TabStop = false;
            this.lblCliente.Text = "Cliente";
            // 
            // lblDataEmissao
            // 
            this.lblDataEmissao.BackColor = System.Drawing.Color.LightGray;
            this.lblDataEmissao.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblDataEmissao.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDataEmissao.Location = new System.Drawing.Point(3, 43);
            this.lblDataEmissao.Name = "lblDataEmissao";
            this.lblDataEmissao.ReadOnly = true;
            this.lblDataEmissao.Size = new System.Drawing.Size(138, 21);
            this.lblDataEmissao.TabIndex = 91;
            this.lblDataEmissao.TabStop = false;
            this.lblDataEmissao.Text = "Período de geração";
            // 
            // lblTipoEsocial
            // 
            this.lblTipoEsocial.BackColor = System.Drawing.Color.LightGray;
            this.lblTipoEsocial.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblTipoEsocial.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTipoEsocial.Location = new System.Drawing.Point(3, 23);
            this.lblTipoEsocial.Name = "lblTipoEsocial";
            this.lblTipoEsocial.ReadOnly = true;
            this.lblTipoEsocial.Size = new System.Drawing.Size(138, 21);
            this.lblTipoEsocial.TabIndex = 94;
            this.lblTipoEsocial.TabStop = false;
            this.lblTipoEsocial.Text = "Tipo";
            // 
            // cbTipoEvento
            // 
            this.cbTipoEvento.BackColor = System.Drawing.Color.LightGray;
            this.cbTipoEvento.BorderColor = System.Drawing.Color.DimGray;
            this.cbTipoEvento.BorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid;
            this.cbTipoEvento.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbTipoEvento.FormattingEnabled = true;
            this.cbTipoEvento.Location = new System.Drawing.Point(140, 23);
            this.cbTipoEvento.Name = "cbTipoEvento";
            this.cbTipoEvento.Size = new System.Drawing.Size(107, 21);
            this.cbTipoEvento.TabIndex = 2;
            // 
            // grbEventos
            // 
            this.grbEventos.Controls.Add(this.dgvLoteCancelado);
            this.grbEventos.Location = new System.Drawing.Point(3, 70);
            this.grbEventos.Name = "grbEventos";
            this.grbEventos.Size = new System.Drawing.Size(758, 394);
            this.grbEventos.TabIndex = 96;
            this.grbEventos.TabStop = false;
            this.grbEventos.Text = "Eventos";
            // 
            // dgvLoteCancelado
            // 
            this.dgvLoteCancelado.AllowUserToAddRows = false;
            this.dgvLoteCancelado.AllowUserToDeleteRows = false;
            this.dgvLoteCancelado.AllowUserToOrderColumns = true;
            dataGridViewCellStyle4.BackColor = System.Drawing.Color.Gainsboro;
            this.dgvLoteCancelado.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle4;
            this.dgvLoteCancelado.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this.dgvLoteCancelado.BackgroundColor = System.Drawing.Color.White;
            dataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle5.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle5.Font = new System.Drawing.Font("Arial", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle5.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle5.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle5.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle5.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvLoteCancelado.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle5;
            this.dgvLoteCancelado.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle6.BackColor = System.Drawing.Color.LightGray;
            dataGridViewCellStyle6.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle6.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle6.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle6.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle6.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvLoteCancelado.DefaultCellStyle = dataGridViewCellStyle6;
            this.dgvLoteCancelado.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvLoteCancelado.Location = new System.Drawing.Point(3, 16);
            this.dgvLoteCancelado.MultiSelect = false;
            this.dgvLoteCancelado.Name = "dgvLoteCancelado";
            this.dgvLoteCancelado.ReadOnly = true;
            this.dgvLoteCancelado.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvLoteCancelado.Size = new System.Drawing.Size(752, 375);
            this.dgvLoteCancelado.TabIndex = 5;
            // 
            // periodoInicial
            // 
            this.periodoInicial.Checked = false;
            this.periodoInicial.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.periodoInicial.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.periodoInicial.Location = new System.Drawing.Point(140, 43);
            this.periodoInicial.Name = "periodoInicial";
            this.periodoInicial.ShowCheckBox = true;
            this.periodoInicial.Size = new System.Drawing.Size(107, 21);
            this.periodoInicial.TabIndex = 3;
            // 
            // periodoFinal
            // 
            this.periodoFinal.Checked = false;
            this.periodoFinal.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.periodoFinal.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.periodoFinal.Location = new System.Drawing.Point(246, 43);
            this.periodoFinal.Name = "periodoFinal";
            this.periodoFinal.ShowCheckBox = true;
            this.periodoFinal.Size = new System.Drawing.Size(107, 21);
            this.periodoFinal.TabIndex = 4;
            // 
            // frmEsocial3000Principal
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(784, 561);
            this.Name = "frmEsocial3000Principal";
            this.Text = "GESTÃO DE EXCLUSÃO DE EVENTOS E-SOCIAL";
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.frmEsocial3000Principal_KeyDown);
            this.spForm.Panel1.ResumeLayout(false);
            this.spForm.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.spForm)).EndInit();
            this.spForm.ResumeLayout(false);
            this.pnlForm.ResumeLayout(false);
            this.pnlForm.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.banner)).EndInit();
            this.flpAcao.ResumeLayout(false);
            this.grbEventos.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvLoteCancelado)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.FlowLayoutPanel flpAcao;
        private System.Windows.Forms.Button btIncluir;
        private System.Windows.Forms.Button btExportar;
        private System.Windows.Forms.Button btPesquisar;
        private System.Windows.Forms.Button btnFechar;
        private System.Windows.Forms.Button btnExcluirCliente;
        private System.Windows.Forms.Button btnCliente;
        public System.Windows.Forms.TextBox textCliente;
        private System.Windows.Forms.TextBox lblCliente;
        private System.Windows.Forms.TextBox lblTipoEsocial;
        private System.Windows.Forms.TextBox lblDataEmissao;
        private ComboBoxWithBorder cbTipoEvento;
        private System.Windows.Forms.GroupBox grbEventos;
        public System.Windows.Forms.DataGridView dgvLoteCancelado;
        private System.Windows.Forms.DateTimePicker periodoInicial;
        private System.Windows.Forms.DateTimePicker periodoFinal;
    }
}