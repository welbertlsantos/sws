﻿namespace SWS.View
{
    partial class frmUsuarioPrincipal
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            this.flpAcao = new System.Windows.Forms.FlowLayoutPanel();
            this.btnIncluir = new System.Windows.Forms.Button();
            this.btnAlterar = new System.Windows.Forms.Button();
            this.btnExcluir = new System.Windows.Forms.Button();
            this.btnReativar = new System.Windows.Forms.Button();
            this.btnDetalhar = new System.Windows.Forms.Button();
            this.btnBuscar = new System.Windows.Forms.Button();
            this.btnLimpar = new System.Windows.Forms.Button();
            this.btnFechar = new System.Windows.Forms.Button();
            this.lblNome = new System.Windows.Forms.TextBox();
            this.lblLogin = new System.Windows.Forms.TextBox();
            this.lblCpf = new System.Windows.Forms.TextBox();
            this.lblSituacao = new System.Windows.Forms.TextBox();
            this.cbSitucao = new SWS.ComboBoxWithBorder();
            this.text_cpf = new System.Windows.Forms.MaskedTextBox();
            this.text_login = new System.Windows.Forms.TextBox();
            this.text_nome = new System.Windows.Forms.TextBox();
            this.grb_gride = new System.Windows.Forms.GroupBox();
            this.grd_usuarios = new System.Windows.Forms.DataGridView();
            this.lblCorCancelado = new System.Windows.Forms.Label();
            this.lblTextoCancelado = new System.Windows.Forms.Label();
            this.lblExterno = new System.Windows.Forms.TextBox();
            this.cbExterno = new SWS.ComboBoxWithBorder();
            ((System.ComponentModel.ISupportInitialize)(this.spForm)).BeginInit();
            this.spForm.Panel1.SuspendLayout();
            this.spForm.Panel2.SuspendLayout();
            this.spForm.SuspendLayout();
            this.pnlForm.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.banner)).BeginInit();
            this.flpAcao.SuspendLayout();
            this.grb_gride.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grd_usuarios)).BeginInit();
            this.SuspendLayout();
            // 
            // spForm
            // 
            // 
            // spForm.Panel1
            // 
            this.spForm.Panel1.Controls.Add(this.flpAcao);
            // 
            // pnlForm
            // 
            this.pnlForm.Controls.Add(this.cbExterno);
            this.pnlForm.Controls.Add(this.lblExterno);
            this.pnlForm.Controls.Add(this.lblTextoCancelado);
            this.pnlForm.Controls.Add(this.lblCorCancelado);
            this.pnlForm.Controls.Add(this.grb_gride);
            this.pnlForm.Controls.Add(this.text_cpf);
            this.pnlForm.Controls.Add(this.text_login);
            this.pnlForm.Controls.Add(this.text_nome);
            this.pnlForm.Controls.Add(this.cbSitucao);
            this.pnlForm.Controls.Add(this.lblSituacao);
            this.pnlForm.Controls.Add(this.lblCpf);
            this.pnlForm.Controls.Add(this.lblLogin);
            this.pnlForm.Controls.Add(this.lblNome);
            // 
            // flpAcao
            // 
            this.flpAcao.Controls.Add(this.btnIncluir);
            this.flpAcao.Controls.Add(this.btnAlterar);
            this.flpAcao.Controls.Add(this.btnExcluir);
            this.flpAcao.Controls.Add(this.btnReativar);
            this.flpAcao.Controls.Add(this.btnDetalhar);
            this.flpAcao.Controls.Add(this.btnBuscar);
            this.flpAcao.Controls.Add(this.btnLimpar);
            this.flpAcao.Controls.Add(this.btnFechar);
            this.flpAcao.Dock = System.Windows.Forms.DockStyle.Fill;
            this.flpAcao.Location = new System.Drawing.Point(0, 0);
            this.flpAcao.Name = "flpAcao";
            this.flpAcao.Size = new System.Drawing.Size(784, 35);
            this.flpAcao.TabIndex = 0;
            // 
            // btnIncluir
            // 
            this.btnIncluir.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnIncluir.Image = global::SWS.Properties.Resources.icone_mais;
            this.btnIncluir.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnIncluir.Location = new System.Drawing.Point(3, 3);
            this.btnIncluir.Name = "btnIncluir";
            this.btnIncluir.Size = new System.Drawing.Size(71, 22);
            this.btnIncluir.TabIndex = 18;
            this.btnIncluir.TabStop = false;
            this.btnIncluir.Text = "Incluir";
            this.btnIncluir.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnIncluir.UseVisualStyleBackColor = true;
            this.btnIncluir.Click += new System.EventHandler(this.btnIncluir_Click);
            // 
            // btnAlterar
            // 
            this.btnAlterar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnAlterar.Image = global::SWS.Properties.Resources.Alterar;
            this.btnAlterar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnAlterar.Location = new System.Drawing.Point(80, 3);
            this.btnAlterar.Name = "btnAlterar";
            this.btnAlterar.Size = new System.Drawing.Size(71, 22);
            this.btnAlterar.TabIndex = 19;
            this.btnAlterar.TabStop = false;
            this.btnAlterar.Text = "Alterar";
            this.btnAlterar.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnAlterar.UseVisualStyleBackColor = true;
            this.btnAlterar.Click += new System.EventHandler(this.btnAlterar_Click);
            // 
            // btnExcluir
            // 
            this.btnExcluir.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnExcluir.Image = global::SWS.Properties.Resources.lixeira;
            this.btnExcluir.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnExcluir.Location = new System.Drawing.Point(157, 3);
            this.btnExcluir.Name = "btnExcluir";
            this.btnExcluir.Size = new System.Drawing.Size(71, 22);
            this.btnExcluir.TabIndex = 20;
            this.btnExcluir.TabStop = false;
            this.btnExcluir.Text = "Excluir";
            this.btnExcluir.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnExcluir.UseVisualStyleBackColor = true;
            this.btnExcluir.Click += new System.EventHandler(this.btnExcluir_Click);
            // 
            // btnReativar
            // 
            this.btnReativar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnReativar.Image = global::SWS.Properties.Resources.devolucao;
            this.btnReativar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnReativar.Location = new System.Drawing.Point(234, 3);
            this.btnReativar.Name = "btnReativar";
            this.btnReativar.Size = new System.Drawing.Size(71, 22);
            this.btnReativar.TabIndex = 23;
            this.btnReativar.TabStop = false;
            this.btnReativar.Text = "&Reativar";
            this.btnReativar.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnReativar.UseVisualStyleBackColor = true;
            this.btnReativar.Click += new System.EventHandler(this.btnReativar_Click);
            // 
            // btnDetalhar
            // 
            this.btnDetalhar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnDetalhar.Image = global::SWS.Properties.Resources.lupa;
            this.btnDetalhar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnDetalhar.Location = new System.Drawing.Point(311, 3);
            this.btnDetalhar.Name = "btnDetalhar";
            this.btnDetalhar.Size = new System.Drawing.Size(71, 22);
            this.btnDetalhar.TabIndex = 21;
            this.btnDetalhar.TabStop = false;
            this.btnDetalhar.Text = "Detalhar";
            this.btnDetalhar.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnDetalhar.UseVisualStyleBackColor = true;
            this.btnDetalhar.Click += new System.EventHandler(this.btnDetalhar_Click);
            // 
            // btnBuscar
            // 
            this.btnBuscar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnBuscar.Image = global::SWS.Properties.Resources.busca;
            this.btnBuscar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnBuscar.Location = new System.Drawing.Point(388, 3);
            this.btnBuscar.Name = "btnBuscar";
            this.btnBuscar.Size = new System.Drawing.Size(71, 22);
            this.btnBuscar.TabIndex = 16;
            this.btnBuscar.TabStop = false;
            this.btnBuscar.Text = "Pesquisar";
            this.btnBuscar.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnBuscar.UseVisualStyleBackColor = true;
            this.btnBuscar.Click += new System.EventHandler(this.btnBuscar_Click);
            // 
            // btnLimpar
            // 
            this.btnLimpar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnLimpar.Image = global::SWS.Properties.Resources.vassoura;
            this.btnLimpar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnLimpar.Location = new System.Drawing.Point(465, 3);
            this.btnLimpar.Name = "btnLimpar";
            this.btnLimpar.Size = new System.Drawing.Size(71, 22);
            this.btnLimpar.TabIndex = 17;
            this.btnLimpar.TabStop = false;
            this.btnLimpar.Text = "Limpar";
            this.btnLimpar.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnLimpar.UseVisualStyleBackColor = true;
            this.btnLimpar.Click += new System.EventHandler(this.btnLimpar_Click);
            // 
            // btnFechar
            // 
            this.btnFechar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnFechar.Image = global::SWS.Properties.Resources.close;
            this.btnFechar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnFechar.Location = new System.Drawing.Point(542, 3);
            this.btnFechar.Name = "btnFechar";
            this.btnFechar.Size = new System.Drawing.Size(71, 22);
            this.btnFechar.TabIndex = 22;
            this.btnFechar.TabStop = false;
            this.btnFechar.Text = "Fechar";
            this.btnFechar.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnFechar.UseVisualStyleBackColor = true;
            this.btnFechar.Click += new System.EventHandler(this.btnFechar_Click);
            // 
            // lblNome
            // 
            this.lblNome.BackColor = System.Drawing.Color.LightGray;
            this.lblNome.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblNome.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNome.Location = new System.Drawing.Point(7, 9);
            this.lblNome.Name = "lblNome";
            this.lblNome.ReadOnly = true;
            this.lblNome.Size = new System.Drawing.Size(100, 21);
            this.lblNome.TabIndex = 0;
            this.lblNome.TabStop = false;
            this.lblNome.Text = "Nome";
            // 
            // lblLogin
            // 
            this.lblLogin.BackColor = System.Drawing.Color.LightGray;
            this.lblLogin.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblLogin.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblLogin.Location = new System.Drawing.Point(7, 29);
            this.lblLogin.Name = "lblLogin";
            this.lblLogin.ReadOnly = true;
            this.lblLogin.Size = new System.Drawing.Size(100, 21);
            this.lblLogin.TabIndex = 1;
            this.lblLogin.TabStop = false;
            this.lblLogin.Text = "Login";
            // 
            // lblCpf
            // 
            this.lblCpf.BackColor = System.Drawing.Color.LightGray;
            this.lblCpf.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblCpf.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCpf.Location = new System.Drawing.Point(7, 49);
            this.lblCpf.Name = "lblCpf";
            this.lblCpf.ReadOnly = true;
            this.lblCpf.Size = new System.Drawing.Size(100, 21);
            this.lblCpf.TabIndex = 2;
            this.lblCpf.TabStop = false;
            this.lblCpf.Text = "CPF";
            // 
            // lblSituacao
            // 
            this.lblSituacao.BackColor = System.Drawing.Color.LightGray;
            this.lblSituacao.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblSituacao.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSituacao.Location = new System.Drawing.Point(7, 69);
            this.lblSituacao.Name = "lblSituacao";
            this.lblSituacao.ReadOnly = true;
            this.lblSituacao.Size = new System.Drawing.Size(100, 21);
            this.lblSituacao.TabIndex = 3;
            this.lblSituacao.TabStop = false;
            this.lblSituacao.Text = "Situação";
            // 
            // cbSitucao
            // 
            this.cbSitucao.BackColor = System.Drawing.Color.LightGray;
            this.cbSitucao.BorderColor = System.Drawing.Color.DimGray;
            this.cbSitucao.BorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid;
            this.cbSitucao.FormattingEnabled = true;
            this.cbSitucao.Location = new System.Drawing.Point(106, 69);
            this.cbSitucao.Name = "cbSitucao";
            this.cbSitucao.Size = new System.Drawing.Size(638, 21);
            this.cbSitucao.TabIndex = 4;
            // 
            // text_cpf
            // 
            this.text_cpf.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.text_cpf.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.text_cpf.Location = new System.Drawing.Point(106, 49);
            this.text_cpf.Mask = "999,999,999-99";
            this.text_cpf.Name = "text_cpf";
            this.text_cpf.PromptChar = ' ';
            this.text_cpf.Size = new System.Drawing.Size(638, 21);
            this.text_cpf.TabIndex = 7;
            // 
            // text_login
            // 
            this.text_login.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.text_login.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.text_login.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.text_login.Location = new System.Drawing.Point(106, 29);
            this.text_login.MaxLength = 15;
            this.text_login.Name = "text_login";
            this.text_login.Size = new System.Drawing.Size(638, 21);
            this.text_login.TabIndex = 6;
            // 
            // text_nome
            // 
            this.text_nome.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.text_nome.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.text_nome.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.text_nome.Location = new System.Drawing.Point(106, 9);
            this.text_nome.MaxLength = 100;
            this.text_nome.Name = "text_nome";
            this.text_nome.Size = new System.Drawing.Size(638, 21);
            this.text_nome.TabIndex = 5;
            // 
            // grb_gride
            // 
            this.grb_gride.BackColor = System.Drawing.Color.White;
            this.grb_gride.Controls.Add(this.grd_usuarios);
            this.grb_gride.Location = new System.Drawing.Point(7, 116);
            this.grb_gride.Name = "grb_gride";
            this.grb_gride.Size = new System.Drawing.Size(737, 314);
            this.grb_gride.TabIndex = 8;
            this.grb_gride.TabStop = false;
            this.grb_gride.Text = "Usuários";
            // 
            // grd_usuarios
            // 
            this.grd_usuarios.AllowUserToAddRows = false;
            this.grd_usuarios.AllowUserToDeleteRows = false;
            this.grd_usuarios.AllowUserToOrderColumns = true;
            this.grd_usuarios.AllowUserToResizeRows = false;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.Gainsboro;
            this.grd_usuarios.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle1;
            this.grd_usuarios.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this.grd_usuarios.BackgroundColor = System.Drawing.Color.White;
            this.grd_usuarios.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.Color.LightGray;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.grd_usuarios.DefaultCellStyle = dataGridViewCellStyle2;
            this.grd_usuarios.Dock = System.Windows.Forms.DockStyle.Fill;
            this.grd_usuarios.Location = new System.Drawing.Point(3, 16);
            this.grd_usuarios.MultiSelect = false;
            this.grd_usuarios.Name = "grd_usuarios";
            this.grd_usuarios.ReadOnly = true;
            this.grd_usuarios.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.grd_usuarios.Size = new System.Drawing.Size(731, 295);
            this.grd_usuarios.TabIndex = 9;
            this.grd_usuarios.CellMouseDoubleClick += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.grd_usuarios_CellMouseDoubleClick);
            this.grd_usuarios.RowPrePaint += new System.Windows.Forms.DataGridViewRowPrePaintEventHandler(this.grd_usuarios_RowPrePaint);
            // 
            // lblCorCancelado
            // 
            this.lblCorCancelado.AutoSize = true;
            this.lblCorCancelado.BackColor = System.Drawing.Color.Red;
            this.lblCorCancelado.Location = new System.Drawing.Point(10, 437);
            this.lblCorCancelado.Name = "lblCorCancelado";
            this.lblCorCancelado.Size = new System.Drawing.Size(13, 13);
            this.lblCorCancelado.TabIndex = 9;
            this.lblCorCancelado.Text = "  ";
            // 
            // lblTextoCancelado
            // 
            this.lblTextoCancelado.AutoSize = true;
            this.lblTextoCancelado.Location = new System.Drawing.Point(29, 437);
            this.lblTextoCancelado.Name = "lblTextoCancelado";
            this.lblTextoCancelado.Size = new System.Drawing.Size(103, 13);
            this.lblTextoCancelado.TabIndex = 10;
            this.lblTextoCancelado.Text = "Usuários desativado";
            // 
            // lblExterno
            // 
            this.lblExterno.BackColor = System.Drawing.Color.LightGray;
            this.lblExterno.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblExterno.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblExterno.Location = new System.Drawing.Point(7, 89);
            this.lblExterno.Name = "lblExterno";
            this.lblExterno.ReadOnly = true;
            this.lblExterno.Size = new System.Drawing.Size(100, 21);
            this.lblExterno.TabIndex = 11;
            this.lblExterno.TabStop = false;
            this.lblExterno.Text = "Externo";
            // 
            // cbExterno
            // 
            this.cbExterno.BackColor = System.Drawing.Color.LightGray;
            this.cbExterno.BorderColor = System.Drawing.Color.DimGray;
            this.cbExterno.BorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid;
            this.cbExterno.FormattingEnabled = true;
            this.cbExterno.Location = new System.Drawing.Point(106, 89);
            this.cbExterno.Name = "cbExterno";
            this.cbExterno.Size = new System.Drawing.Size(638, 21);
            this.cbExterno.TabIndex = 12;
            // 
            // frmUsuarioPrincipal
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(784, 561);
            this.Name = "frmUsuarioPrincipal";
            this.Text = "GERENCIAR USUÁRIO";
            this.spForm.Panel1.ResumeLayout(false);
            this.spForm.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.spForm)).EndInit();
            this.spForm.ResumeLayout(false);
            this.pnlForm.ResumeLayout(false);
            this.pnlForm.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.banner)).EndInit();
            this.flpAcao.ResumeLayout(false);
            this.grb_gride.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.grd_usuarios)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.FlowLayoutPanel flpAcao;
        private System.Windows.Forms.Button btnReativar;
        private System.Windows.Forms.Button btnFechar;
        private System.Windows.Forms.Button btnDetalhar;
        private System.Windows.Forms.Button btnIncluir;
        private System.Windows.Forms.Button btnExcluir;
        private System.Windows.Forms.Button btnAlterar;
        private System.Windows.Forms.Button btnLimpar;
        private System.Windows.Forms.Button btnBuscar;
        private System.Windows.Forms.TextBox lblLogin;
        private System.Windows.Forms.TextBox lblNome;
        private System.Windows.Forms.TextBox lblCpf;
        private ComboBoxWithBorder cbSitucao;
        private System.Windows.Forms.TextBox lblSituacao;
        private System.Windows.Forms.MaskedTextBox text_cpf;
        private System.Windows.Forms.TextBox text_login;
        private System.Windows.Forms.TextBox text_nome;
        private System.Windows.Forms.Label lblTextoCancelado;
        private System.Windows.Forms.Label lblCorCancelado;
        private System.Windows.Forms.GroupBox grb_gride;
        private System.Windows.Forms.DataGridView grd_usuarios;
        private ComboBoxWithBorder cbExterno;
        private System.Windows.Forms.TextBox lblExterno;
    }
}