﻿using SWS.Entidade;
using SWS.Facade;
using SWS.View.Resources;
using SWS.View.ViewHelper;
using SWS.ViewHelper;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace SWS.View
{
    public partial class frmNotaFiscalPrincipal : frmTemplate
    {
        private Nfe notaFiscal;
        private Empresa empresa;
        private Cliente cliente;

        public frmNotaFiscalPrincipal()
        {
            InitializeComponent();
            ActiveControl = textNumeroNota;
            validaPermissao();
            ComboHelper.situacaoNotaFiscal(cbSituacao);
        }

        private void btn_pesquisar_Click(object sender, EventArgs e)
        {
            try
            {
                if (validaDadosPesquisa())
                {
                    notaFiscal = new Nfe(null, null, empresa, string.IsNullOrEmpty(textNumeroNota.Text) ? (long?)null : Convert.ToInt64(textNumeroNota.Text), 0, Convert.ToDateTime(dataEmissaoInicial.Text), null, null, 0, 0, null, null, null, cliente, Convert.ToDateTime(dataEmissaoInicial.Text), Convert.ToDateTime(dataCancelamentoInicial.Text), string.Empty, null, ((SelectItem)cbSituacao.SelectedItem).Valor, null, null, null);
                    montaDataGrid();
                    textNumeroNota.Focus();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btn_detalhar_Click(object sender, EventArgs e)
        {
            try
            {
                if (dgvNotaFiscal.CurrentRow == null)
                    throw new Exception("Selecione uma linha.");
                 
                FinanceiroFacade financeiroFacade = FinanceiroFacade.getInstance();

                Nfe notaFiscalConsultar = financeiroFacade.findNotaFiscalById((Int64)dgvNotaFiscal.CurrentRow.Cells[0].Value);

                frmNotaFiscalConsulta formNotaConsulta = new frmNotaFiscalConsulta(notaFiscalConsultar);
                formNotaConsulta.ShowDialog();

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btn_cancelar_Click(object sender, EventArgs e)
        {
            try
            {
                if (dgvNotaFiscal.CurrentRow == null)
                    throw new Exception("Selecione uma linha.");

                FinanceiroFacade financeiroFacade = FinanceiroFacade.getInstance();

                Nfe notaFiscalCancelar = financeiroFacade.findNotaFiscalById((Int64)dgvNotaFiscal.CurrentRow.Cells[0].Value);

                if (!string.Equals(notaFiscalCancelar.Situacao, ApplicationConstants.FECHADO))
                    throw new Exception("Somente notas faturadas podem ser canceladas.");

                if (MessageBox.Show("Deseja cancelar a nota fiscal selecionada?", "Atenção", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                {
                    this.Cursor = Cursors.WaitCursor;

                    frmJustificativa formCancelamento = new frmJustificativa("MOTIVO DO CANCELAMENTO");
                    formCancelamento.ShowDialog();

                    if (String.IsNullOrEmpty(formCancelamento.Justificativa))
                        throw new Exception("Você deve informar o motivo do cancelamento.");

                    notaFiscalCancelar.MotivoCancelamento = formCancelamento.Justificativa;
                    notaFiscalCancelar.Situacao = ApplicationConstants.CANCELADA;

                    financeiroFacade.cancelaNotaFiscal(notaFiscalCancelar);
                    MessageBox.Show("Nota Fiscal cancelada com sucesso.", "Confirmação", MessageBoxButtons.OK, MessageBoxIcon.Information);

                    montaDataGrid();

                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }

        private void btn_imprimir_Click(object sender, EventArgs e)
        {
            try
            {
                this.Cursor = Cursors.WaitCursor;

                if (dgvNotaFiscal.CurrentRow == null)
                    throw new Exception("Selecione uma linha.");

                FinanceiroFacade financeiroFacade = FinanceiroFacade.getInstance();

                Nfe notaFiscalImprimir = financeiroFacade.findNotaFiscalById((Int64)dgvNotaFiscal.CurrentRow.Cells[0].Value);

                if (!string.Equals(notaFiscalImprimir.Situacao, ApplicationConstants.FECHADO))
                    throw new Exception("Só é possível imprimir notas faturadas. ");

                var process = new Process();

                process.StartInfo.FileName = "Relatorio\\jre7\\bin\\java";
                process.StartInfo.Arguments = " -jar Relatorio\\MiddlewareJasper.jar " + "Relatorio\\Report\\" + "NOTAFISCAL.JASPER" + " ID_NFE:" + notaFiscalImprimir.Id + ":INTEGER";
                process.StartInfo.UseShellExecute = false;
                process.StartInfo.RedirectStandardError = true;
                process.StartInfo.RedirectStandardInput = true;
                process.StartInfo.RedirectStandardOutput = true;
                process.StartInfo.CreateNoWindow = true;

                process.Start();
                process.StandardOutput.ReadToEnd();
                process.WaitForExit();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }

        private void btn_fechar_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnEmpresa_Click(object sender, EventArgs e)
        {
            try
            {
                frmEmpresaBuscar selecionarEmpresa = new frmEmpresaBuscar();
                selecionarEmpresa.ShowDialog();

                if (selecionarEmpresa.Empresa != null)
                {
                    empresa = selecionarEmpresa.Empresa;
                    textEmpresa.Text = empresa.Fantasia;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btnExcluirEmpresa_Click(object sender, EventArgs e)
        {
            try
            {
                if (empresa == null)
                    throw new Exception("Você não selecionou nenhuma empresa");
                
                empresa = null;
                textEmpresa.Text = string.Empty;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btnCliente_Click(object sender, EventArgs e)
        {
            try
            {
                frmClienteSelecionar selecionarCliente = new frmClienteSelecionar(null, null, false, null, null, null, null, null, null);
                selecionarCliente.ShowDialog();

                if (selecionarCliente.Cliente != null)
                {
                    cliente = selecionarCliente.Cliente;
                    textCliente.Text = cliente.RazaoSocial;

               }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btnExcluirCliente_Click(object sender, EventArgs e)
        {
            try
            {
                if (cliente == null)
                    throw new Exception("Você ainda não selecionou nenhum cliente.");
                
                cliente = null;
                textCliente.Text = string.Empty;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void validaPermissao()
        {
            PermissionamentoFacade permissionamentoFacade = PermissionamentoFacade.getInstance();

            this.btnCancelar.Enabled = permissionamentoFacade.hasPermission("NOTAFISCAL", "CANCELAR");
            
        }

        private void dgNotaFiscal_RowPrePaint(object sender, DataGridViewRowPrePaintEventArgs e)
        {
            DataGridView dvg = sender as DataGridView;
            
            if (String.Equals(dvg.Rows[e.RowIndex].Cells[10].Value.ToString(), ApplicationConstants.CANCELADA))
                dvg.Rows[e.RowIndex].DefaultCellStyle.ForeColor = Color.Red;
        }

        private void montaDataGrid()
        {
            try
            {
                this.Cursor = Cursors.WaitCursor;

                dgvNotaFiscal.Columns.Clear();

                FinanceiroFacade financeiroFacade = FinanceiroFacade.getInstance();

                DataSet ds = financeiroFacade.findNotaFiscalByFilter(notaFiscal, dataEmissaoInicial.Checked ? Convert.ToDateTime(dataEmissaoInicial.Text + " 00:00:00") : (DateTime?)null, dataEmissaoFinal.Checked ? Convert.ToDateTime(dataEmissaoFinal.Text + " 23:59:59") : (DateTime?)null, dataCancelamentoInicial.Checked ? Convert.ToDateTime(dataCancelamentoInicial.Text + " 00:00:00") : (DateTime?)null, dataCancelamentoFinal.Checked ? Convert.ToDateTime(dataCancelamentoFinal.Text + " 23:59:59") : (DateTime?)null);

                dgvNotaFiscal.DataSource = ds.Tables[0].DefaultView;
                
                dgvNotaFiscal.Columns[0].HeaderText = "Id NotaFiscal";
                dgvNotaFiscal.Columns[0].Visible = false;

                dgvNotaFiscal.Columns[1].HeaderText = "Numero NF"; 

                dgvNotaFiscal.Columns[2].HeaderText = "Data de Emissão"; 
                dgvNotaFiscal.Columns[2].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
                dgvNotaFiscal.Columns[2].HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleCenter;

                dgvNotaFiscal.Columns[3].HeaderText = "Data de Cancelamento"; 
                dgvNotaFiscal.Columns[3].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
                dgvNotaFiscal.Columns[3].HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleCenter;

                dgvNotaFiscal.Columns[4].HeaderText = "Valor Total NF"; 
                dgvNotaFiscal.Columns[4].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
                dgvNotaFiscal.Columns[4].HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleRight;

                dgvNotaFiscal.Columns[5].HeaderText = "Cliente"; 

                dgvNotaFiscal.Columns[6].HeaderText = "CNPJ"; 

                dgvNotaFiscal.Columns[7].HeaderText = "Plano de Pagamento";  

                dgvNotaFiscal.Columns[8].HeaderText = "Gravado por:"; 

                dgvNotaFiscal.Columns[9].HeaderText = "Empresa";

                dgvNotaFiscal.Columns[10].HeaderText = "Situação";
                dgvNotaFiscal.Columns[10].Visible = false;

                /* montando totais */

                text_valorTotal.Text = ValidaCampoHelper.FormataValorMonetario(ds.Tables["Totais"].Rows[0]["valor_total"].ToString());
                textQuantidade.Text = ds.Tables["Totais"].Rows[0]["quantidade"].ToString();
                textValorLiquido.Text = ValidaCampoHelper.FormataValorMonetario(ds.Tables["Totais"].Rows[0]["valor_liquido"].ToString());


            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }

        }

        private bool validaDadosPesquisa()
        {
            bool retorno = true;

            try
            {
                if (dataEmissaoInicial.Checked && !dataEmissaoFinal.Checked)
                    throw new Exception("Você deve marcar a data final de emissão.");

                if (dataEmissaoInicial.Checked && !dataEmissaoInicial.Checked)
                    throw new Exception("Você deve marcar a data inicial da emissão.");

                if (Convert.ToDateTime(dataEmissaoInicial.Text) > Convert.ToDateTime(dataEmissaoFinal.Text))
                    throw new Exception("A data de emissão inicial não pode ser maior que a data de emissão final. Reveja os valores.");


                if (dataCancelamentoInicial.Checked && !dataCancelamentoFinal.Checked)
                    throw new Exception("Você deve marcar a data final de cancelamento.");

                if (dataCancelamentoFinal.Checked && !dataCancelamentoInicial.Checked)
                    throw new Exception("Você deve marcar a data inicial de cancelamento.");

                if (Convert.ToDateTime(dataCancelamentoInicial.Text) > Convert.ToDateTime(dataCancelamentoFinal.Text))
                    throw new Exception("A data de cancelamento inicial não pode ser maior que a data de cancelamento final. Reveja os valores.");
            }
            catch (Exception ex)
            {
                retorno = false;
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

            return retorno;
        }

        private void textNumeroNota_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = ValidaCampoHelper.FormataCampoNumerico(sender, e);
        }
    }
}
