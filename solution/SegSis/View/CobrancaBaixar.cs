﻿using SWS.Entidade;
using SWS.Facade;
using SWS.View.Resources;
using SWS.View.ViewHelper;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace SWS.View
{
    public partial class frmCobrancaBaixar : frmTemplateConsulta
    {
        private Banco banco;
        private Conta conta;
        private Cobranca cobranca;
        
        public frmCobrancaBaixar(Cobranca cobrancaBaixar)
        {
            InitializeComponent();
            cobranca = cobrancaBaixar;

            textNumeroNota.Text = cobranca.Nota.NumeroNfe.ToString();
            textDocumento.Text = cobranca.NumeroDocumento;
            textParcela.Text = cobranca.NumeroParcela.ToString();
            textFormaPagamento.Text = cobranca.Forma.Descricao;
            textCliente.Text = cobranca.Nota.Cliente.RazaoSocial;
            textDataEmissao.Text = string.Format("{0:dd/MM/yyyy}", cobranca.DataEmissao);
            textDataVencimento.Text = string.Format("{0:dd/MM/yyyy}", cobranca.DataVencimento);
            textValorCobranca.Text = ValidaCampoHelper.FormataValorMonetario(cobranca.ValorAjustado.ToString());

            ActiveControl = textValorPago;

            if (cobrancaBaixar.Conta != null)
            {
                this.conta = cobrancaBaixar.Conta;
                btnBanco.Enabled = false;
            }
        }

        private void textValorPago_Enter(object sender, EventArgs e)
        {
            textValorPago.Tag = textValorPago.Text;
            textValorPago.Text = string.Empty;
        }

        private void textValorPago_Leave(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(textValorPago.Text))
                textValorPago.Text = textValorPago.Tag.ToString();
            else
                textValorPago.Text = ValidaCampoHelper.FormataValorMonetario(textValorPago.Text);
        }

        private void textValorPago_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = ValidaCampoHelper.FormataDigitacaoNumero(sender, e, textValorPago.Text, 2);
        }

        private void btnBanco_Click(object sender, EventArgs e)
        {
            try
            {
                frmBancoSelecionar selecionarBanco = new frmBancoSelecionar(true, false);
                selecionarBanco.ShowDialog();

                if (selecionarBanco.Banco != null)
                {
                    banco = selecionarBanco.Banco;
                    textBanco.Text = banco.Nome;
                    montaDataGrid();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }

        public void montaDataGrid()
        {
            try
            {
                this.Cursor = Cursors.WaitCursor;

                dgvConta.Columns.Clear();

                FinanceiroFacade financeiroFacade = FinanceiroFacade.getInstance();

                DataSet ds = financeiroFacade.findContaByFilter(new Conta(null, banco, string.Empty, string.Empty, string.Empty, string.Empty, string.Empty, null, string.Empty, string.Empty, string.Empty, string.Empty, string.Empty, false, ApplicationConstants.ATIVO, false));

                dgvConta.DataSource = ds.Tables[0].DefaultView;

                dgvConta.Columns[0].HeaderText = "Id Conta";
                dgvConta.Columns[0].Visible = false;

                dgvConta.Columns[1].HeaderText = "Nome";
                dgvConta.Columns[2].HeaderText = "Agência";
                dgvConta.Columns[3].HeaderText = "Agência Digito";
                dgvConta.Columns[4].HeaderText = "Conta Corrente";
                dgvConta.Columns[5].HeaderText = "Dígito";
                dgvConta.Columns[6].HeaderText = "Próximo número";
                dgvConta.Columns[6].Visible = false;

                dgvConta.Columns[7].HeaderText = "Numero Remessa";
                dgvConta.Columns[7].Visible = false;

                dgvConta.Columns[8].HeaderText = "Carteira";
                dgvConta.Columns[8].Visible = false;

                dgvConta.Columns[9].HeaderText = "Convenio";
                dgvConta.Columns[9].Visible = false;

                dgvConta.Columns[10].HeaderText = "Variação";
                dgvConta.Columns[10].Visible = false;

                dgvConta.Columns[11].HeaderText = "Codigo Cedente";
                dgvConta.Columns[11].Visible = false;

                dgvConta.Columns[12].HeaderText = "Registro";
                dgvConta.Columns[12].Visible = false;

                dgvConta.Columns[13].HeaderText = "Situacao";
                dgvConta.Columns[13].Visible = false;

                dgvConta.Columns[14].HeaderText = "Id Banco";
                dgvConta.Columns[14].Visible = false;

                dgvConta.Columns[15].HeaderText = "Nome do Banco";
                dgvConta.Columns[15].Visible = false;

                dgvConta.Columns[16].HeaderText = "Codigo Banco";
                dgvConta.Columns[16].Visible = false;

                dgvConta.Columns[17].HeaderText = "Caixa";
                dgvConta.Columns[17].Visible = false;

                dgvConta.Columns[18].HeaderText = "Situacao";
                dgvConta.Columns[18].Visible = false;

                dgvConta.Columns[19].HeaderText = "Conta Privada";
                dgvConta.Columns[19].Visible = false;

                dgvConta.Columns[20].HeaderText = "Banco privado";
                dgvConta.Columns[20].Visible = false;

                dgvConta.Columns[21].HeaderText = "Banco Boleto";
                dgvConta.Columns[21].Visible = false;

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }

        }

        private void btnConfirmar_Click(object sender, EventArgs e)
        {
            try
            {
                if (string.IsNullOrEmpty(textValorPago.Text))
                    throw new Exception("O valor pago não pode ser nulo.");

                if (!dataPagamento.Checked)
                    throw new Exception("Você deve informar uma data de pagamento.");
                
                FinanceiroFacade financeiroFacade = FinanceiroFacade.getInstance();

                PermissionamentoFacade permissionamentoFacade = PermissionamentoFacade.getInstance();
                if (Convert.ToDecimal(cobranca.ValorAjustado) > Convert.ToDecimal(textValorPago.Text))
                    if (!permissionamentoFacade.hasPermission("COBRANCA", "BAIXAR_COBRANCA_COM_VALOR_MENOR"))
                        throw new Exception("Você não tem permissão para baixar a cobrança com um valor menor do que o valor nominal.");

                if (!string.Equals(cobranca.Forma.Descricao, ApplicationConstants.BOLETO) && banco == null)
                    throw new Exception("Você não selecionou o banco para baixar");

                if (banco != null && dgvConta.CurrentRow == null)
                    throw new Exception("Você não selecionou a conta de cobranca");

                if (this.conta == null) 
                {
                    if (dgvConta.CurrentRow == null)
                        throw new Exception("Você deve selecionar uma conta para baixar.");

                    this.conta = financeiroFacade.findContaById((long)dgvConta.CurrentRow.Cells[0].Value);
                }

                if (MessageBox.Show("Deseja baixar a cobrança " + cobranca.NumeroDocumento + " Parcela " + cobranca.NumeroParcela.ToString() + " na data do dia " + dataPagamento.Value.ToShortDateString() + " no valor informado de " + ValidaCampoHelper.FormataValorMonetario(textValorPago.Text) + " ?", "Atenção", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                {
                    /* solicitando a informação da baixa */

                    if (MessageBox.Show("Deseja inserir uma observação na baixa?", "Atenção", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) == DialogResult.Yes)
                    {
                        frmJustificativa justificativa = new frmJustificativa("INFORMAÇÕES DA BAIXA DA COBRANÇA");
                        justificativa.ShowDialog();
                        cobranca.ObservacaoBaixa = justificativa.Justificativa;
                    }

                    
                    
                    cobranca.Conta = conta;
                    cobranca.DataPagamento = dataPagamento.Value;
                    cobranca.Situacao = ApplicationConstants.BAIXADO;
                    
                    cobranca.UsuarioBaixa = PermissionamentoFacade.usuarioAutenticado;
                    cobranca.ValorBaixado = Convert.ToDecimal(textValorPago.Text);
                    financeiroFacade.baixarCobranca(cobranca);
                    MessageBox.Show("Cobrança baixada com sucesso.", "Confirmação", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    this.Close();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }

        private void btnCancelar_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void frmCobrancaBaixar_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter || e.KeyCode == Keys.Return)
                SendKeys.Send("{TAB}");
        }

        

    }
}
