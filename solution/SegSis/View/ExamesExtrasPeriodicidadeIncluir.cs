﻿using SWS.Entidade;
using SWS.Facade;
using SWS.View.ViewHelper;
using SWS.ViewHelper;
using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace SWS.View
{
    public partial class frmExamesExtrasPeriodicidadeIncluir : frmTemplateConsulta
    {

        private ClienteFuncaoExame clienteFuncaoExame;
        
        
        public frmExamesExtrasPeriodicidadeIncluir(ClienteFuncaoExame clienteFuncaoExame)
        {
            InitializeComponent();
            this.clienteFuncaoExame = clienteFuncaoExame;
            montaDataGrid();
              
        }
        
        private void btn_ok_Click(object sender, EventArgs e)
        {
            try
            {
                /* verificando se alguma marcação foi feita */

                bool check = false;
                foreach (DataGridViewRow dvRow in dgvPeriodicidade.Rows)
                    if ((bool)dvRow.Cells[2].Value == true)
                    {
                        check = true;
                        break;
                    }

                if (!check)
                    throw new Exception("Você deve marcar pelo menos um tipo de periodicidade");


                /* gravando as periodicidades selecionadas no banco de dados */
                EstudoFacade pcmsoFacade = EstudoFacade.getInstance();
                foreach (DataGridViewRow dvRow in dgvPeriodicidade.Rows)
                    if ((bool)dvRow.Cells[2].Value == true)
                        pcmsoFacade.insertClienteFuncaoExamePeriodicidade(new ClienteFuncaoExamePeriodicidade(clienteFuncaoExame, new Periodicidade((long)dvRow.Cells[0].Value, dvRow.Cells[1].Value.ToString(), null), String.Empty, Convert.ToInt32(dvRow.Cells[3].Value)));

                this.Close();

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btn_fechar_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        public void montaDataGrid()
        {
            try
            {
                EstudoFacade pcmsoFacade = EstudoFacade.getInstance();

                DataSet ds = pcmsoFacade.findAllPeriodicidadesNotInClienteFuncaoExame(clienteFuncaoExame);

                dgvPeriodicidade.ColumnCount = 2;

                dgvPeriodicidade.Columns[0].HeaderText = "id_periodicidade";
                dgvPeriodicidade.Columns[0].Visible = false;

                dgvPeriodicidade.Columns[1].HeaderText = "Periodicidade";
                dgvPeriodicidade.Columns[1].ReadOnly = true;
                dgvPeriodicidade.Columns[1].Width = 200;

                DataGridViewCheckBoxColumn check = new DataGridViewCheckBoxColumn();
                check.Name = "Selecione";
                dgvPeriodicidade.Columns.Add(check);
                dgvPeriodicidade.Columns[2].HeaderText = "Selecione";
                dgvPeriodicidade.Columns[2].Width = 30;

                /* incluindo atributo periodo de vencimento */

                DataGridViewComboBoxColumn periodoVencimento = new DataGridViewComboBoxColumn();
                dgvPeriodicidade.Columns.Add(periodoVencimento);
                ComboHelper.periodoVencimentoCombo(periodoVencimento);
                dgvPeriodicidade.Columns[3].HeaderText = "Vencimento";
                dgvPeriodicidade.Columns[3].DefaultCellStyle.BackColor = Color.LightYellow;
                dgvPeriodicidade.Columns[3].Width = 150;


                foreach (DataRow dvRow in ds.Tables[0].Rows)
                    dgvPeriodicidade.Rows.Add(Convert.ToInt64(dvRow["id_periodicidade"]), dvRow["descricao"].ToString(), false, Convert.ToString(clienteFuncaoExame.Exame.PeriodoVencimento));
                
                /* reordenando a grid */
                dgvPeriodicidade.Columns[1].DisplayIndex = 2;
                dgvPeriodicidade.Columns[2].DisplayIndex = 1;
                dgvPeriodicidade.Columns[3].DisplayIndex = 3;

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
    }
}
