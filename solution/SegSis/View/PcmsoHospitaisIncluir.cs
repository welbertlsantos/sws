﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using SWS.Facade;
using SWS.Excecao;
using SWS.Entidade;

namespace SWS.View
{
    public partial class frm_PcmsoHospitaisIncluir : BaseFormConsulta
    {

        frm_PcmsoExames formPcmsoExame;
        frm_PcmsoSemPpraIncluir formPcmsoSemPpraIncluir;

        Hospital hospital = new Hospital();
        HospitalEstudo hospitalEstudo;

        public frm_PcmsoHospitaisIncluir(frm_PcmsoExames formPcmsoExame)
        {
            InitializeComponent();
            this.formPcmsoExame = formPcmsoExame;
            validaPermissoes();
        }

        public frm_PcmsoHospitaisIncluir(frm_PcmsoSemPpraIncluir formPcmsoSemPpraIncluir)
        {
            InitializeComponent();
            this.formPcmsoSemPpraIncluir = formPcmsoSemPpraIncluir;
            validaPermissoes();
        }

        private void validaPermissoes()
        {
            PermissionamentoFacade permissionamentoFacade = PermissionamentoFacade.getInstance();

            this.btn_novo.Enabled = permissionamentoFacade.hasPermission("HOSPITAL", "INCLUIR");
        }

        private void btn_fechar_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btn_buscar_Click(object sender, EventArgs e)
        {
            try
            {
               montaDataGrid();
               text_descricao.Focus();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        public void montaDataGrid()
        {
            try
            {
                this.Cursor = Cursors.WaitCursor;

                grd_hospital.Columns.Clear();

                PcmsoFacade pcmsoFacade = PcmsoFacade.getInstance();

                hospital.Nome = text_descricao.Text;

                if (formPcmsoExame != null)
                {
                    hospitalEstudo = new HospitalEstudo(formPcmsoExame.pcmso, hospital);
                }
                else
                {
                    hospitalEstudo = new HospitalEstudo(formPcmsoSemPpraIncluir.pcmso, hospital);
                }

                DataSet ds = pcmsoFacade.findAllHospitaisAtivos(hospitalEstudo);

                grd_hospital.DataSource = ds.Tables["Hospitais"].DefaultView;

                grd_hospital.Columns[0].HeaderText = "IdHospital";
                grd_hospital.Columns[0].Visible = false;

                grd_hospital.Columns[1].HeaderText = "Nome";
                grd_hospital.Columns[2].HeaderText = "Endereço";
                grd_hospital.Columns[3].HeaderText = "Número";
                grd_hospital.Columns[4].HeaderText = "Complemento";
                grd_hospital.Columns[5].HeaderText = "Bairro";
                grd_hospital.Columns[6].HeaderText = "Cidade";
                grd_hospital.Columns[7].HeaderText = "CEP";
                grd_hospital.Columns[8].HeaderText = "UF";
                grd_hospital.Columns[9].HeaderText = "Telefone";
                grd_hospital.Columns[10].HeaderText = "Fax";
                grd_hospital.Columns[11].HeaderText = "Obsevação";
                
                grd_hospital.Columns[12].HeaderText = "Situação";
                grd_hospital.Columns[12].Visible = false;

                grd_hospital.Columns[13].HeaderText = "Id_cidade";
                grd_hospital.Columns[13].Visible = false;

                DataGridViewCheckBoxColumn check = new DataGridViewCheckBoxColumn();
                check.Name = "CheckBox";
                check.HeaderText = "Selecionar";
                grd_hospital.Columns.Add(check);

                // reordenando as colunas da grid para o checkbox vir primeiro.

                for (int i = 0; i <= 12; i++)
                {
                    grd_hospital.Columns[i].DisplayIndex = i + 1;

                }

                grd_hospital.Columns[13].DisplayIndex = 0;

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }

        private void btn_incluir_Click(object sender, EventArgs e)
        {
            try
            {
                PcmsoFacade pcmsoFacade = PcmsoFacade.getInstance();
                Hospital hospital;
                HospitalEstudo hospitalEstudo;

                foreach (DataGridViewRow dvrom in grd_hospital.Rows)
                {
                    if (dvrom.Cells[13].Value != null)
                    {
                        hospital = new Hospital((Int64)dvrom.Cells[0].Value,
                            (String)dvrom.Cells[1].Value,
                            (String)dvrom.Cells[2].Value,
                            (String)dvrom.Cells[3].Value,
                            (String)dvrom.Cells[4].Value,
                            (String)dvrom.Cells[5].Value,
                            (String)dvrom.Cells[6].Value,
                            (String)dvrom.Cells[7].Value,
                            (String)dvrom.Cells[8].Value,
                            (String)dvrom.Cells[9].Value,
                            (String)dvrom.Cells[10].Value,
                            (String)dvrom.Cells[11].Value,
                            null
                            );

                        if (formPcmsoExame != null)
                        {
                            hospitalEstudo = new HospitalEstudo((Estudo)formPcmsoExame.pcmso, hospital);
                        }
                        else
                        {
                            hospitalEstudo = new HospitalEstudo((Estudo)formPcmsoSemPpraIncluir.pcmso, hospital);
                        }

                        pcmsoFacade.incluirHospitalEstudo(hospitalEstudo);
                        
                    }
                }
                if (formPcmsoExame != null)
                {
                    formPcmsoExame.montaDataGridHospital();
                }
                else
                {
                    formPcmsoSemPpraIncluir.montaDataGridHospital();
                }

                this.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void btn_novo_Click(object sender, EventArgs e)
        {
            frmHospitalIncluir formHospitalIncluir = new frmHospitalIncluir();
            formHospitalIncluir.ShowDialog();

            if (formHospitalIncluir.Hospital != null)
            {
                hospital = formHospitalIncluir.Hospital;
                text_descricao.Text = hospital.Nome;
                montaDataGrid();
                
            }

        }
    }
}

