﻿using SWS.Entidade;
using SWS.Facade;
using SWS.View.Resources;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace SWS.View
{
    public partial class frmExameRelatorio : frmTemplateConsulta
    {
        private Relatorio relatorioSelecionado;

        public Relatorio RelatorioSelecionado
        {
            get { return relatorioSelecionado; }
            set { relatorioSelecionado = value; }
        }
        
        public frmExameRelatorio()
        {
            InitializeComponent();
            montaGrid();
        }

        private void montaGrid()
        {
            try
            {
                dgvRelatorio.Columns.Clear();
                this.Cursor = Cursors.WaitCursor;

                RelatorioFacade relatorioFacade = RelatorioFacade.getInstance();

                DataSet dt = relatorioFacade.findAllRelatoriosBySituacao(ApplicationConstants.RELATORIO_EXAME);

                dgvRelatorio.DataSource = dt.Tables[0].DefaultView;

                dgvRelatorio.Columns[0].HeaderText = "id_relatorio";
                dgvRelatorio.Columns[0].Visible = false;
                dgvRelatorio.Columns[1].HeaderText = "nomeRelatorio";
                dgvRelatorio.Columns[1].Visible = false;
                dgvRelatorio.Columns[2].HeaderText = "tipoRelatorio";
                dgvRelatorio.Columns[2].Visible = false;
                dgvRelatorio.Columns[3].HeaderText = "dataCriação";
                dgvRelatorio.Columns[3].Visible = false;
                dgvRelatorio.Columns[4].HeaderText = "situação";
                dgvRelatorio.Columns[4].Visible = false;
                dgvRelatorio.Columns[5].HeaderText = "Relatório";

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }

            
        }

        private void dgvRelatorio_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void dgvRelatorio_DataBindingComplete(object sender, DataGridViewBindingCompleteEventArgs e)
        {
            DataGridView gridView = sender as DataGridView;
            gridView.ClearSelection();
        }

        private void btnSelecionar_Click(object sender, EventArgs e)
        {
            try
            {
                if (dgvRelatorio.CurrentRow == null)
                    throw new Exception("Selecione uma linha.");

                RelatorioSelecionado = new Relatorio((long)dgvRelatorio.CurrentRow.Cells[0].Value, dgvRelatorio.CurrentRow.Cells[1].Value.ToString(), dgvRelatorio.CurrentRow.Cells[2].Value.ToString(), Convert.ToDateTime(dgvRelatorio.CurrentRow.Cells[3].Value), dgvRelatorio.CurrentRow.Cells[4].Value.ToString(), dgvRelatorio.CurrentRow.Cells[5].Value.ToString());
                this.Close();

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }

        private void dgvRelatorio_CellContentDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                if (e.RowIndex < 0)
                    throw new Exception("Selecione uma linha.");

                btnSelecionar.PerformClick();

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }

        private void btnFechar_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
