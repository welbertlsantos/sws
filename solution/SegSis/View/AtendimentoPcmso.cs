﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using SWS.Entidade;
using SWS.Facade;
using SWS.Resources;

namespace SWS.View
{
    public partial class frmAtendimentoPcmso : SWS.View.frmTemplateConsulta
    {

        private static String msg1 = "Selecione uma linha";

        private ClienteFuncaoFuncionario clienteFuncaoFuncionario;

        private Estudo pcmso;

        public Estudo Pcmso
        {
            get { return pcmso; }
            set { pcmso = value; }
        }

        public frmAtendimentoPcmso(ClienteFuncaoFuncionario clienteFuncaoFuncionario)
        {
            InitializeComponent();
            this.clienteFuncaoFuncionario = clienteFuncaoFuncionario;
            validaPermissoes();
            montaDataGrid();
        }

        private void btn_cancelar_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btn_novo_Click(object sender, EventArgs e)
        {
            try
            {
                this.Cursor = Cursors.WaitCursor;

                frmPcmsoAvulsoIncluir pcmsoIncluir = new frmPcmsoAvulsoIncluir();
                pcmsoIncluir.ShowDialog();

                if (pcmsoIncluir.Estudo != null)
                {
                    pcmso = pcmsoIncluir.Estudo;
                    this.Close();   
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }

        private bool validaPcmso()
        {
            bool result = true;

            try
            {
                string validaPcmso;
                PermissionamentoFacade.mapaConfiguracoes.TryGetValue(ConfigurationConstans.PCMSO_VENCIDO_ASO, out validaPcmso);

                /* validando para saber se a funcao no cliente está presente no PCMSO */

                EstudoFacade pcmsoFacade = EstudoFacade.getInstance();

                if (!pcmsoFacade.IsClienteFuncaoPresentInPcmso(new Estudo((Int64)dgvPcmso.CurrentRow.Cells[0].Value), clienteFuncaoFuncionario.ClienteFuncao))
                    throw new Exception("A função não está presente no PCMSO escolhido");

                /* validando a data data de validade do PCMSO escolhido. */

                Estudo pcmsoValido = pcmsoFacade.findById((Int64)dgvPcmso.CurrentRow.Cells[0].Value);

                if (!Convert.ToBoolean(validaPcmso))
                {
                    if (pcmsoValido.DataVencimento != null && pcmsoValido.DataVencimento < DateTime.Now)
                    {
                        if (MessageBox.Show("PCMSO Vencido. Não será impresso nenhuma informação do coordenador no ASO. Deseja continuar?",
                            "Atenção", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.No)
                        {
                            return result = false;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                result = false;
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            
            return result;
        }

        private void btn_alterar_Click(object sender, EventArgs e)
        {
            try
            {
                if (dgvPcmso.CurrentRow == null)
                    throw new Exception(msg1);

                EstudoFacade pcmsoFacade = EstudoFacade.getInstance();

                PcmsoAvulsoAlterar formAlteraPcmso = new PcmsoAvulsoAlterar(pcmsoFacade.findById((Int64)dgvPcmso.CurrentRow.Cells[0].Value));
                formAlteraPcmso.ShowDialog();
                montaDataGrid();

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btn_incluir_Click(object sender, EventArgs e)
        {
            try
            {
                if (dgvPcmso.CurrentRow == null)
                    throw new Exception(msg1);   

                /* verificando se o pcmso escolhido pode ser aceito devido aos critérios estabelecidos pelo método */
                if (validaPcmso())
                {
                    EstudoFacade pcmsoFacade = EstudoFacade.getInstance();
                    pcmso = pcmsoFacade.findById((Int64)dgvPcmso.CurrentRow.Cells[0].Value);
                    this.Close();
                }


            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void validaPermissoes()
        {
            PermissionamentoFacade permissionamentoFacade = PermissionamentoFacade.getInstance();

            btNovo.Enabled = permissionamentoFacade.hasPermission("PCMSO_AVULSO", "INCLUIR");
            btAltera.Enabled = permissionamentoFacade.hasPermission("PCMSO_AVULSO", "ALTERAR");
        }

        public void montaDataGrid()
        {
            try
            {
                EstudoFacade pcmsoFacade = EstudoFacade.getInstance();
                this.Cursor = Cursors.WaitCursor;
                
                this.dgvPcmso.Columns.Clear();

                this.dgvPcmso.ColumnCount = 7;

                this.dgvPcmso.Columns[0].HeaderText = "Id_Estudo";
                this.dgvPcmso.Columns[0].Visible = false;

                this.dgvPcmso.Columns[1].HeaderText = "Código";

                this.dgvPcmso.Columns[2].HeaderText = "Validade";

                this.dgvPcmso.Columns[3].HeaderText = "Cliente";

                this.dgvPcmso.Columns[4].HeaderText = "Contratata";

                this.dgvPcmso.Columns[5].HeaderText = "Avulso";
                this.dgvPcmso.Columns[5].Visible = false;

                this.dgvPcmso.Columns[6].HeaderText = "Obra";
                this.dgvPcmso.Columns[6].DisplayIndex = 5;

                foreach (Estudo pcmso in pcmsoFacade.findLastPcmsoByCliente(clienteFuncaoFuncionario.ClienteFuncao.Cliente))
                    dgvPcmso.Rows.Add(pcmso.Id, pcmso.CodigoEstudo, String.Format("{0:dd/MM/yyyy}",pcmso.DataVencimento), pcmso.VendedorCliente.Cliente.RazaoSocial, pcmso.ContrRazaoSocial, pcmso.Avulso, pcmso.Obra);

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }

        private void dgvPcmso_RowPrePaint(object sender, DataGridViewRowPrePaintEventArgs e)
        {
            DataGridView dgv = sender as DataGridView;

            Boolean valor = (Boolean)dgv.Rows[e.RowIndex].Cells[5].Value;

            if (valor)
                dgv.Rows[e.RowIndex].DefaultCellStyle.ForeColor = Color.Green;
        }

        private void dgvPcmso_DataBindingComplete(object sender, DataGridViewBindingCompleteEventArgs e)
        {
            DataGridView gridView = sender as DataGridView;
            gridView.ClearSelection();
        }

        private void dgvPcmso_CellMouseDoubleClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            try
            {
                btConfirma.PerformClick();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }
    }
}
