﻿namespace SegSis.View
{
    partial class frm_FuncionarioAlterarClienteFuncaoFuncionario
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frm_FuncionarioAlterarClienteFuncaoFuncionario));
            this.pnl_clienteFuncaoFuncionario = new System.Windows.Forms.Panel();
            this.grb_paginacao = new System.Windows.Forms.GroupBox();
            this.btn_fechar = new System.Windows.Forms.Button();
            this.btn_gravar = new System.Windows.Forms.Button();
            this.grb_dados = new System.Windows.Forms.GroupBox();
            this.chk_vip = new System.Windows.Forms.CheckBox();
            this.lbl_matricula = new System.Windows.Forms.Label();
            this.text_matricula = new System.Windows.Forms.TextBox();
            this.pnl_clienteFuncaoFuncionario.SuspendLayout();
            this.grb_paginacao.SuspendLayout();
            this.grb_dados.SuspendLayout();
            this.SuspendLayout();
            // 
            // pnl_clienteFuncaoFuncionario
            // 
            this.pnl_clienteFuncaoFuncionario.BackColor = System.Drawing.Color.White;
            this.pnl_clienteFuncaoFuncionario.Controls.Add(this.grb_paginacao);
            this.pnl_clienteFuncaoFuncionario.Controls.Add(this.grb_dados);
            this.pnl_clienteFuncaoFuncionario.Location = new System.Drawing.Point(3, 5);
            this.pnl_clienteFuncaoFuncionario.Name = "pnl_clienteFuncaoFuncionario";
            this.pnl_clienteFuncaoFuncionario.Size = new System.Drawing.Size(277, 253);
            this.pnl_clienteFuncaoFuncionario.TabIndex = 0;
            // 
            // grb_paginacao
            // 
            this.grb_paginacao.Controls.Add(this.btn_fechar);
            this.grb_paginacao.Controls.Add(this.btn_gravar);
            this.grb_paginacao.Location = new System.Drawing.Point(10, 194);
            this.grb_paginacao.Name = "grb_paginacao";
            this.grb_paginacao.Size = new System.Drawing.Size(259, 51);
            this.grb_paginacao.TabIndex = 1;
            this.grb_paginacao.TabStop = false;
            // 
            // btn_fechar
            // 
            this.btn_fechar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_fechar.Image = global::SegSis.Properties.Resources.close;
            this.btn_fechar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btn_fechar.Location = new System.Drawing.Point(87, 19);
            this.btn_fechar.Name = "btn_fechar";
            this.btn_fechar.Size = new System.Drawing.Size(75, 23);
            this.btn_fechar.TabIndex = 1;
            this.btn_fechar.Text = "&Fechar";
            this.btn_fechar.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btn_fechar.UseVisualStyleBackColor = true;
            this.btn_fechar.Click += new System.EventHandler(this.btn_fechar_Click);
            // 
            // btn_gravar
            // 
            this.btn_gravar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_gravar.Image = global::SegSis.Properties.Resources.Gravar;
            this.btn_gravar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btn_gravar.Location = new System.Drawing.Point(6, 19);
            this.btn_gravar.Name = "btn_gravar";
            this.btn_gravar.Size = new System.Drawing.Size(75, 23);
            this.btn_gravar.TabIndex = 1;
            this.btn_gravar.Text = "&Gravar";
            this.btn_gravar.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btn_gravar.UseVisualStyleBackColor = true;
            this.btn_gravar.Click += new System.EventHandler(this.btn_gravar_Click);
            // 
            // grb_dados
            // 
            this.grb_dados.Controls.Add(this.chk_vip);
            this.grb_dados.Controls.Add(this.lbl_matricula);
            this.grb_dados.Controls.Add(this.text_matricula);
            this.grb_dados.Location = new System.Drawing.Point(10, 8);
            this.grb_dados.Name = "grb_dados";
            this.grb_dados.Size = new System.Drawing.Size(259, 179);
            this.grb_dados.TabIndex = 0;
            this.grb_dados.TabStop = false;
            this.grb_dados.Text = "Dados";
            // 
            // chk_vip
            // 
            this.chk_vip.AutoSize = true;
            this.chk_vip.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.chk_vip.Location = new System.Drawing.Point(6, 60);
            this.chk_vip.Name = "chk_vip";
            this.chk_vip.Size = new System.Drawing.Size(104, 17);
            this.chk_vip.TabIndex = 2;
            this.chk_vip.Text = "Funcionário VIP?";
            this.chk_vip.UseVisualStyleBackColor = true;
            // 
            // lbl_matricula
            // 
            this.lbl_matricula.AutoSize = true;
            this.lbl_matricula.Location = new System.Drawing.Point(6, 18);
            this.lbl_matricula.Name = "lbl_matricula";
            this.lbl_matricula.Size = new System.Drawing.Size(52, 13);
            this.lbl_matricula.TabIndex = 1;
            this.lbl_matricula.Text = "Matrícula";
            // 
            // text_matricula
            // 
            this.text_matricula.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.text_matricula.Location = new System.Drawing.Point(6, 34);
            this.text_matricula.MaxLength = 15;
            this.text_matricula.Name = "text_matricula";
            this.text_matricula.Size = new System.Drawing.Size(220, 20);
            this.text_matricula.TabIndex = 0;
            // 
            // frm_FuncionarioAlterarClienteFuncaoFuncionario
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(284, 262);
            this.ControlBox = false;
            this.Controls.Add(this.pnl_clienteFuncaoFuncionario);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "frm_FuncionarioAlterarClienteFuncaoFuncionario";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.pnl_clienteFuncaoFuncionario.ResumeLayout(false);
            this.grb_paginacao.ResumeLayout(false);
            this.grb_dados.ResumeLayout(false);
            this.grb_dados.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel pnl_clienteFuncaoFuncionario;
        private System.Windows.Forms.GroupBox grb_paginacao;
        private System.Windows.Forms.Button btn_fechar;
        private System.Windows.Forms.Button btn_gravar;
        private System.Windows.Forms.GroupBox grb_dados;
        private System.Windows.Forms.CheckBox chk_vip;
        private System.Windows.Forms.Label lbl_matricula;
        private System.Windows.Forms.TextBox text_matricula;
    }
}