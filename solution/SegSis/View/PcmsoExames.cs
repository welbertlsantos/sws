﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using SWS.Entidade;
using SWS.Facade;
using SWS.Excecao;
using SWS.View.ViewHelper;
using SWS.View.Resources;


namespace SWS.View
{
    public partial class frm_PcmsoExames : BaseForm
    {

        #region mensagens

        public static String msg1 = "Selecione uma linha.";
        public static String msg2 = "Atenção!";
        public static String msg3 = "Cronograma Alterado com sucesso.";
        public static String msg4 = "Confirmação";
        public static String msg5 = "Cronograma incluído com sucesso";
        public static String msg6 = "Deseja excluir essa atividade?";
        public static String msg7 = "Material Hospitalar Salvo com sucesso.";
        public static String msg8 = "Deseja excluir o material hospitalar?";
        public static String msg9 = "Material hospitalar excluído com sucesso.";
        public static String msg10 = "Deseja excluir esse hospital?";
        public static String msg11 = "Hospital excluído com sucesso.";
        public static String msg12 = "Deseja excluir o médico?";
        public static String msg13 = "Médico excluído com sucesso.";
        public static String msg14 = "Deseja excluir esse conteúdo?";
        public static String msg15 = "Conteúdo excluído com sucesso.";
        public static String msg16 = "Deseja excluir o exame selecionado?";

        public static String msg17 = " Selecione um Exame.";

        #endregion

        #region atributos

        private frm_PcmsoIncluir formPcmsoIncluir;
        public Estudo pcmso;
        public Ghe gheSelecionado;
        public Cronograma cronograma = null;
        public CronogramaAtividade cronogramaAtividadeProcurado = new CronogramaAtividade();
        public Int64 idGheFonteAgente;
        private Int64 idGheFonteAgenteExame;
        public Dictionary<Int64, CronogramaAtividade> mapaCronogramaAtividade = new Dictionary<Int64, CronogramaAtividade>();
        private String controleDigitacao;
        private frm_PcmsoAlterar formPcmsoAlterar;
        
        #endregion

        #region construtores

        public frm_PcmsoExames(Estudo pcmso, frm_PcmsoIncluir formPcmsoIncluir)
        {
            InitializeComponent();
            this.pcmso = pcmso;
            this.formPcmsoIncluir = formPcmsoIncluir;
            this.grb_atividade.Enabled = true;
            this.text_descricao.Text = pcmso.Cronograma.Descricao;
            this.text_descricao.ReadOnly = true;
            this.cronograma = pcmso.Cronograma;
        }

        public frm_PcmsoExames(Estudo pcmso, Boolean altera, frm_PcmsoAlterar formPcmsoAltera)
        {
            this.pcmso = pcmso;
            this.formPcmsoAlterar = formPcmsoAltera;
            InitializeComponent();
            // carregando os métodos das classes e preenchendo dados.

            this.montaDataGridHospital();
            this.MontaDataGridMaterialHospitalar();
            this.montaDataGridMedico();
            this.MontaGridAnexo();
            
            // recuperando dados do cronograma.

            PcmsoFacade pcmsoFacade = PcmsoFacade.getInstance();

            cronograma = pcmsoFacade.findCronogramaByPcmso(pcmso);

            if (cronograma != null)
            {

                text_descricao.Text = Convert.ToString(cronograma.Descricao);
                text_descricao.ReadOnly = true;
                mapaCronogramaAtividade = pcmsoFacade.findCronogramaAtividadeByCronograma(cronograma);

            }

            grb_atividade.Enabled = true;
            MontarGridCronogramaAtividade();

        }

        #endregion

        #region Métodos

        public void montaGridGhe()
        {
            try
            {
                grd_ghe.Columns.Clear();

                PcmsoFacade pcmsoFacade = PcmsoFacade.getInstance();

                DataSet ds = pcmsoFacade.findGheByPcmso(pcmso);

                grd_ghe.DataSource = ds.Tables["Ghes"].DefaultView;
                
                grd_ghe.Columns[0].HeaderText = "IdGhe";
                grd_ghe.Columns[1].HeaderText = "IdPpra";
                grd_ghe.Columns[2].HeaderText = "Descrição";
                grd_ghe.Columns[3].HeaderText = "Número de Expostos";
                grd_ghe.Columns[4].HeaderText = "Número Po";


                grd_ghe.Columns[0].Visible = false;
                grd_ghe.Columns[1].Visible = false;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void frm_PcmsoExames_Load(object sender, EventArgs e)
        {
            try
            {
                montaGridGhe();

                this.btn_exame_alterar.Enabled = false;

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }

        }

        private void grd_ghe_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                MontaGridFuncao();
                MontaGridRisco();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }

        }

        // Esse método irá mostrar todas funcões ligadas ao GHE.
        public void MontaGridFuncao()
        {
            try
            {

                grd_funcao.Columns.Clear();

                PcmsoFacade pcmsoFacade = PcmsoFacade.getInstance();

                // Capturando dados do Ghe Selecionado.

                Int32 cellValue = grd_ghe.CurrentRow.Index;

                Int64 idGhe = (Int64)grd_ghe.Rows[cellValue].Cells[0].Value;
                String descricao = (String)grd_ghe.Rows[cellValue].Cells[2].Value;
                Int32 numeroExposto = (Int32)grd_ghe.Rows[cellValue].Cells[3].Value;
                string numeroPo = grd_ghe.Rows[cellValue].Cells[4].Value.ToString();

                // montando dados do ghe.

                gheSelecionado = new Ghe(idGhe, pcmso, descricao, numeroExposto, ApplicationConstants.ATIVO, numeroPo);

                // Buscando todas as funçõs do gheSelecionado.

                DataSet ds = pcmsoFacade.findAllFuncaoByGhe(gheSelecionado);

                grd_funcao.DataSource = ds.Tables["Funcoes"].DefaultView;
                grd_funcao.RowsDefaultCellStyle.BackColor = Color.White;

                grd_funcao.Columns[0].HeaderText = "IdFuncao";
                grd_funcao.Columns[1].HeaderText = "Descricao";
                grd_funcao.Columns[2].HeaderText = "Cod_CBO";
                grd_funcao.Columns[3].HeaderText = "Situacao";
                grd_funcao.Columns[4].HeaderText = "Comentário";

                grd_funcao.Columns[0].Visible = false;
                grd_funcao.Columns[3].Visible = false;

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }

        }

        public void MontaGridRisco()
        {
            try
            {

                grd_risco.Columns.Clear();

                PcmsoFacade pcmsoFacade = PcmsoFacade.getInstance();

                // Capturando dados do Ghe Selecionado.

                Int32 cellValue = grd_ghe.CurrentRow.Index;
                
                Int64 idGhe = (Int64)grd_ghe.Rows[cellValue].Cells[0].Value;
                String descricao = (String)grd_ghe.Rows[cellValue].Cells[2].Value;
                Int32 numeroExposto = (Int32)grd_ghe.Rows[cellValue].Cells[3].Value;
                string numeroPo = grd_ghe.Rows[cellValue].Cells[4].Value.ToString();

                // montando dados do ghe.

                gheSelecionado = new Ghe(idGhe, pcmso, descricao, numeroExposto, ApplicationConstants.ATIVO, numeroPo);

                // Buscando todas as funçõs do gheSelecionado.

                DataSet ds = pcmsoFacade.findAllRiscosByGhe(gheSelecionado);

                if (ds.Tables[0].Rows.Count != 0)
                {
                    grd_risco.DataSource = ds.Tables["Riscos"].DefaultView;

                    grd_risco.Columns[0].HeaderText = "idGheFonteAgente";
                    grd_risco.Columns[1].HeaderText = "id_risco";
                    grd_risco.Columns[2].HeaderText = "Agente";
                    grd_risco.Columns[3].HeaderText = "Danos";
                    grd_risco.Columns[4].HeaderText = "Limite";
                    grd_risco.Columns[5].HeaderText = "Risco";

                    grd_risco.Columns[0].Visible = false;
                    grd_risco.Columns[1].Visible = false;

                    grd_risco.Columns[0].DisplayIndex = 0;
                    grd_risco.Columns[1].DisplayIndex = 1;
                    grd_risco.Columns[2].DisplayIndex = 2;
                    grd_risco.Columns[3].DisplayIndex = 4;
                    grd_risco.Columns[4].DisplayIndex = 5;
                    grd_risco.Columns[5].DisplayIndex = 3;

                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }

        }

        private void btn_voltar_Click(object sender, EventArgs e)
        {
            if (formPcmsoAlterar != null)
            {
                formPcmsoAlterar.Close();
            }

            if (formPcmsoIncluir != null)
            {
                formPcmsoIncluir.Close();
            }
           
            this.Close();

        }

        private void grd_ghe_SelectionChanged(object sender, EventArgs e)
        {
            try
            {
                MontaGridFuncao();
                MontaGridRisco();


            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void btn_incluir_Click(object sender, EventArgs e)
        {
            try
            {
                // Capturando dados do GheFonteAgente Selecionado.

                if (grd_risco.CurrentRow != null)
                {

                    if (!String.IsNullOrEmpty((String)grd_risco.CurrentRow.Cells[0].Value.ToString()))
                    {
                        Int32 cellValue = grd_risco.CurrentRow.Index;
                        idGheFonteAgente = (Int64)grd_risco.Rows[cellValue].Cells[0].Value;

                        frm_PcmsoExameIncluir formPcmsoExameIncluir = new frm_PcmsoExameIncluir(this, pcmso.VendedorCliente.getCliente(), gheSelecionado, null);
                        formPcmsoExameIncluir.ShowDialog();
                    }
                }
                else
                {
                    throw new GheFonteAgenteException(GheFonteAgenteException.msg);
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }

        }

        private void grd_risco_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                montaDataGridExame();
                grd_periodicidade.Columns.Clear();
             
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }

        }

        public void montaDataGridExame()
        {
            try
            {

                grd_exame.Columns.Clear();

                PcmsoFacade pcmsoFacade = PcmsoFacade.getInstance();

                // Capturando dados do GheFonteAgente selecionado

                Int32 cellValue = grd_risco.CurrentRow.Index;

                if (!String.IsNullOrEmpty((String)this.grd_risco.CurrentRow.Cells[0].Value.ToString()))
                {
                    DataSet ds = pcmsoFacade.findAllExamesByGheFonteAgente(new GheFonteAgente((Int64)grd_risco.Rows[cellValue].Cells[0].Value));

                    grd_exame.DataSource = ds.Tables["Exames"].DefaultView;

                    grd_exame.Columns[0].HeaderText = "idGheFonteAgenteExame";
                    grd_exame.Columns[0].Visible = false;
                    grd_exame.Columns[0].DisplayIndex = 0;

                    grd_exame.Columns[1].HeaderText = "IdExame";
                    grd_exame.Columns[1].Visible = false;
                    grd_exame.Columns[1].DisplayIndex = 1;

                    grd_exame.Columns[2].HeaderText = "IdGheFonteAgente";
                    grd_exame.Columns[2].Visible = false;
                    grd_exame.Columns[2].DisplayIndex = 2;

                    grd_exame.Columns[3].HeaderText = "Idade Exame";
                    grd_exame.Columns[3].ValueType = typeof(String);
                    grd_exame.Columns[3].DisplayIndex = 6;

                    grd_exame.Columns[4].HeaderText = "Descrição";
                    grd_exame.Columns[4].ReadOnly = true;
                    grd_exame.Columns[4].DisplayIndex = 3;

                    grd_exame.Columns[5].HeaderText = "Situação";
                    grd_exame.Columns[5].Visible = false;
                    grd_exame.Columns[5].DisplayIndex = 4;

                    grd_exame.Columns[6].HeaderText = "Laboratório";
                    grd_exame.Columns[6].ReadOnly = true;
                    grd_exame.Columns[6].DisplayIndex = 5;

                    grd_exame.Columns[7].HeaderText = "Confinado";
                    grd_exame.Columns[7].ReadOnly = true;
                    grd_exame.Columns[7].DisplayIndex = 7;

                    grd_exame.Columns[8].HeaderText = "Altura";
                    grd_exame.Columns[8].ReadOnly = true;
                    grd_exame.Columns[8].DisplayIndex = 8;

                    grd_exame.Columns[9].HeaderText = "Eletricidade";
                    grd_exame.Columns[9].ReadOnly = true;
                    grd_exame.Columns[9].DisplayIndex = 9;
                    

                }

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        public void MontaGridPeriodicidade()
        {

           try
           {
               grd_periodicidade.Columns.Clear();

               PcmsoFacade pcmsoFacade = PcmsoFacade.getInstance();

               // Capturando dados do GheFonteAgenteExame selecionado

               Int32 cellValue = grd_exame.CurrentRow.Index;
               idGheFonteAgenteExame = (Int64)grd_exame.Rows[cellValue].Cells[0].Value;

               DataSet ds = pcmsoFacade.findAllPeriodicidadeByGheFonteAgenteExame(idGheFonteAgenteExame);


               grd_periodicidade.DataSource = ds.Tables["Periodicidades"].DefaultView;

               grd_periodicidade.Columns[0].HeaderText = "idPeriodicidade";
               grd_periodicidade.Columns[1].HeaderText = "Periodicidade";

               grd_periodicidade.Columns[0].Visible = false;
               
           }
           catch (Exception ex)
           {
               MessageBox.Show(ex.Message);
           }
           
        }

        private void grd_exame_CellClick(object sender, DataGridViewCellEventArgs e)
        {

            try
            {
                MontaGridPeriodicidade();

                this.btn_exame_alterar.Enabled = true;
               
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void btn_nota_Click(object sender, EventArgs e)
        {
           try
           {
               if (grd_ghe.CurrentRow != null)
               {

                   gheSelecionado = new Ghe((Int64)grd_ghe.CurrentRow.Cells[0].Value, pcmso, (String)grd_ghe.CurrentRow.Cells[2].Value, (Int32)grd_ghe.CurrentRow.Cells[3].Value, string.Empty, grd_ghe.CurrentRow.Cells[4].Value.ToString());

                   frm_PcmsoGheNotas formPcmsoGheNotas = new frm_PcmsoGheNotas(gheSelecionado);
                   formPcmsoGheNotas.ShowDialog();

               }
               else
               {
                   throw new GheException(GheException.msg);
               }

           }
           catch (Exception ex)
           {
               MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
           }

        }

        private void btn_incluirAtividade_Click(object sender, EventArgs e)
        {
            frm_PcmoAtividadeIncluir formPcmsoAtividadeIncluir = new frm_PcmoAtividadeIncluir(this);
            formPcmsoAtividadeIncluir.ShowDialog();
           
        }

        private void btn_alterar_Click(object sender, EventArgs e)
        {
            try
            {
                if (grd_atividade.CurrentRow != null)
                {


                    PcmsoFacade pcmsoFacade = PcmsoFacade.getInstance();

                    Int32 cellValue = grd_atividade.CurrentRow.Index;
                   
                    Int64 id_Atividade = (Int64)grd_atividade.Rows[cellValue].Cells[0].Value;
                                      
                    mapaCronogramaAtividade.TryGetValue(id_Atividade, out cronogramaAtividadeProcurado);

                    // objeto cronogramaAtividadeProcurado armazena informação da classe atividadeCronograma.

                    frm_PcmsoAtividadeAlterar formPcmsoAtividadeAlterar = new frm_PcmsoAtividadeAlterar(this);
                    formPcmsoAtividadeAlterar.ShowDialog();


                }
                else
                {
                    MessageBox.Show(msg1, msg2);
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
              
            }

        }

        private void btn_excluirAtividade_Click(object sender, EventArgs e)
        {
            try
            {
                if (grd_atividade.CurrentRow != null)
                {
                    PcmsoFacade pcmsoFacade = PcmsoFacade.getInstance();

                    if (MessageBox.Show(msg6, msg4, MessageBoxButtons.YesNo) == DialogResult.Yes)
                    {
                       
                        Int32 cellValue = grd_atividade.CurrentRow.Index;

                        Int64 id_Atividade = (Int64)grd_atividade.Rows[cellValue].Cells[0].Value;

                        // removendo objeto da lista

                        mapaCronogramaAtividade.TryGetValue(id_Atividade, out cronogramaAtividadeProcurado);

                        pcmsoFacade.excluiCronogramaAtividade(cronogramaAtividadeProcurado);

                        mapaCronogramaAtividade.Remove(id_Atividade);

                        MontarGridCronogramaAtividade();
                       
                    }

                }
                else
                {
                    MessageBox.Show(msg1, msg2);
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }

        }

        public void MontarGridCronogramaAtividade()
        {
            try
            {
                grd_atividade.Columns.Clear();

                //interagindo no mapa.
                              
                foreach (KeyValuePair<Int64,CronogramaAtividade> pairCronogramaAtividade in mapaCronogramaAtividade)
                {

                    grd_atividade.ColumnCount = 7;
                   
                    Int32 rowindex = 0;

                    grd_atividade.Columns[0].Name = "Id_Atividade";
                    grd_atividade.Columns[1].Name = "Id_Cronograma";
                    grd_atividade.Columns[2].Name = "Atividade";
                    grd_atividade.Columns[3].Name = "Mês";
                    grd_atividade.Columns[4].Name = "Ano";
                    grd_atividade.Columns[5].Name = "Público";
                    grd_atividade.Columns[6].Name = "Evidência";

                    this.grd_atividade.Rows.Insert(rowindex, 
                        pairCronogramaAtividade.Value.Atividade.Id,
                        pairCronogramaAtividade.Value.Cronograma.Id,
                        pairCronogramaAtividade.Value.Atividade.Descricao,
                        pairCronogramaAtividade.Value.MesRealizado,
                        pairCronogramaAtividade.Value.AnoRealizado,
                        pairCronogramaAtividade.Value.PublicoAlvo,
                        pairCronogramaAtividade.Value.Evidencia);

                    grd_atividade.Columns[0].Visible = false;
                    grd_atividade.Columns[1].Visible = false;
                    rowindex++;

                }

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void btn_incluirMaterial_Click(object sender, EventArgs e)
        {
            frm_pcmoMaterialIncluir formMaterialIncluir = new frm_pcmoMaterialIncluir(this);
            formMaterialIncluir.ShowDialog();
        }

        public void MontaDataGridMaterialHospitalar()
        {
            try
            {
                PcmsoFacade pcmsoFacade = PcmsoFacade.getInstance();
                 
                grd_material.Columns.Clear();

                DataSet ds = pcmsoFacade.findAllMaterialHospitalarEstudo(pcmso);

                grd_material.DataSource = ds.Tables["MateriaisHospitalaresEstudos"].DefaultView;

                grd_material.Columns[0].HeaderText = "IdEstudo";
                grd_material.Columns[1].HeaderText = "IdMaterial";
                grd_material.Columns[2].HeaderText = "Descrição";
                grd_material.Columns[3].HeaderText = "Quantidade";

                grd_material.Columns[0].Visible = false;
                grd_material.Columns[1].Visible = false;

                grd_material.Columns[2].ReadOnly = true;

                grd_material.Columns[3].DefaultCellStyle.BackColor = Color.Bisque;
               
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void grd_material_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!Char.IsDigit(e.KeyChar) && e.KeyChar != (char)8)
            {
                e.Handled = true;

            }
        }

        private void grd_material_EditingControlShowing(object sender, DataGridViewEditingControlShowingEventArgs e)
        {
            if (e.Control is DataGridViewTextBoxEditingControl)
            {
               e.Control.KeyPress += new KeyPressEventHandler(grd_material_KeyPress);

            }

        }

        private void btn_salvarMaterial_Click(object sender, EventArgs e)
        {
            try
            {
                if (validaQuantidadeZero())
                {
                    PcmsoFacade pcmsoFacade = PcmsoFacade.getInstance();

                    MaterialHospitalar materialHospitalar;
                    MaterialHospitalarEstudo materialHospitalarEstudo;


                    foreach (DataGridViewRow dvRom in grd_material.Rows)
                    {
                        materialHospitalar = new MaterialHospitalar((Int64)dvRom.Cells[1].Value, null, null, null);
                        materialHospitalarEstudo = new MaterialHospitalarEstudo(pcmso, materialHospitalar, (Int32)dvRom.Cells[3].Value);
                        pcmsoFacade.alteraMaterialHospitalarEstudo(materialHospitalarEstudo);


                    }
                    MessageBox.Show(msg7, msg2);

                }
                else
                {
                    throw new MaterialHospitalarException(MaterialHospitalarException.msg3);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private Boolean validaQuantidadeZero()
        {
            Boolean retorno = true;

            foreach (DataGridViewRow dvRom in grd_material.Rows)
            {
                if ((Int32)dvRom.Cells[3].Value == (Int32)0)
                {
                    retorno = false;
                    break;
                }
            }
            return retorno;
        }

        private void btn_excluirMaterial_Click(object sender, EventArgs e)
        {
            try
            {
                if (grd_material.CurrentRow != null)
                {

                    PcmsoFacade pcmsoFacade = PcmsoFacade.getInstance();

                    if (MessageBox.Show(msg8, msg4, MessageBoxButtons.YesNo) == DialogResult.Yes)
                    {

                        Int32 cellValue = grd_material.CurrentRow.Index;
                         
                        Int64 id_Atividade = (Int64)grd_material.Rows[cellValue].Cells[1].Value;

                        pcmsoFacade.excluirMaterialHospitalarEstudo(id_Atividade, pcmso);

                        MessageBox.Show(msg9, msg4);

                        MontaDataGridMaterialHospitalar();

                    }
                }
                else
                {
                    MessageBox.Show(msg1, msg2);
                } 
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        public void montaDataGridHospital()
        {
            try
            {
                PcmsoFacade pcmsoFacade = PcmsoFacade.getInstance();
                grd_hospital.Columns.Clear();

                DataSet ds = pcmsoFacade.findAllHospitalEstudo(pcmso);

                grd_hospital.DataSource = ds.Tables["Hospitais"].DefaultView;

                grd_hospital.Columns[0].HeaderText = "IdHospital";
                grd_hospital.Columns[1].HeaderText = "Nome";
                grd_hospital.Columns[2].HeaderText = "Endereço";
                grd_hospital.Columns[3].HeaderText = "Número";
                grd_hospital.Columns[4].HeaderText = "Complemento";
                grd_hospital.Columns[5].HeaderText = "Bairro";
                grd_hospital.Columns[6].HeaderText = "Cidade";
                grd_hospital.Columns[7].HeaderText = "CEP";
                grd_hospital.Columns[8].HeaderText = "UF";
                grd_hospital.Columns[9].HeaderText = "Telefone";
                grd_hospital.Columns[10].HeaderText = "Fax";
                grd_hospital.Columns[11].HeaderText = "Observação";
                grd_hospital.Columns[12].HeaderText = "Situação";

                grd_hospital.Columns[0].Visible = false;
                grd_hospital.Columns[12].Visible = false;

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
       
        private void btn_incluirHospital_Click(object sender, EventArgs e)
        {
            frm_PcmsoHospitaisIncluir formPcmsoHospitalIncluir = new frm_PcmsoHospitaisIncluir(this);
            formPcmsoHospitalIncluir.ShowDialog();

        }

        private void btn_excluirHospital_Click(object sender, EventArgs e)
        {
            try
            {
                if (grd_hospital.CurrentRow != null)
                {
                    PcmsoFacade pcmsoFacade = PcmsoFacade.getInstance();

                    if (MessageBox.Show(msg10, msg4, MessageBoxButtons.YesNo) == DialogResult.Yes)
                    {

                        Int32 cellValue = grd_hospital.CurrentRow.Index;

                        Int64 id_hospital = (Int64)grd_hospital.Rows[cellValue].Cells[0].Value;

                        Hospital hospital = new Hospital();
                        hospital.Id = id_hospital;
                       
                        HospitalEstudo hospitalEstudo = new HospitalEstudo(pcmso, hospital);

                        pcmsoFacade.excluirHospitalEstudo(hospitalEstudo);
                       
                        MessageBox.Show(msg11, msg4);

                        montaDataGridHospital();

                    }
                }
                else
                {
                    MessageBox.Show(msg1, msg2);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        public void montaDataGridMedico()
        {
            try
            {
                PcmsoFacade pcmsoFacade = PcmsoFacade.getInstance();
                grd_medico.Columns.Clear();

                DataSet ds = pcmsoFacade.findAllMedicoEstudo(pcmso);

                grd_medico.DataSource = ds.Tables["Medicos"].DefaultView;

                grd_medico.Columns[0].HeaderText = "IdMedico";
                grd_medico.Columns[1].HeaderText = "Nome";
                grd_medico.Columns[2].HeaderText = "CRM";
                grd_medico.Columns[3].HeaderText = "Situação";

                grd_medico.Columns[0].Visible = false;
                grd_medico.Columns[3].Visible = false;

           }
           catch (Exception ex)
           {
               MessageBox.Show(ex.Message);
           }
        }

        private void btn_excluirMedico_Click(object sender, EventArgs e)
        {
            try
            {
                if (grd_medico.CurrentRow != null)
                {
                    PcmsoFacade pcmsoFacade = PcmsoFacade.getInstance();

                    if (MessageBox.Show(msg12, msg4, MessageBoxButtons.YesNo) == DialogResult.Yes)
                    {

                        Int32 cellValue = grd_medico.CurrentRow.Index;

                        Int64 id_medico = (Int64)grd_medico.Rows[cellValue].Cells[0].Value;

                        Medico medico = new Medico();
                        medico.Id = id_medico;

                        MedicoEstudo medicoEstudo = new MedicoEstudo(pcmso, medico, false, ApplicationConstants.DESATIVADO);

                        pcmsoFacade.excluirMedicoEstudo(medicoEstudo);

                        MessageBox.Show(msg13, msg4);

                        montaDataGridMedico();

                    }
                }
                else
                {
                    MessageBox.Show(msg1, msg2);
                }


            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }

        }

        private void btn_incluirMedico_Click(object sender, EventArgs e)
        {
            frm_PcmsoMedicoIncluir formPcmsoIncluir = new frm_PcmsoMedicoIncluir(pcmso);
            formPcmsoIncluir.ShowDialog();

            if (formPcmsoIncluir.getMedicoEstudo() != null)
            {
                montaDataGridMedico();
            }
        }

        private void btn_incluirAnexo_Click(object sender, EventArgs e)
        {
            try
            {
                //Validando dados


                if (!String.IsNullOrEmpty(text_conteudo.Text))
                {
                    //Incluindo o anexo no banco de dados

                    PcmsoFacade pcmsoFacade = PcmsoFacade.getInstance();
                    
                    Anexo anexo = new Anexo(null, pcmso, Convert.ToString(text_conteudo.Text.ToUpper()));

                    pcmsoFacade.incluiAnexo(anexo);

                    MontaGridAnexo();

                    text_conteudo.Text = String.Empty;
                    text_conteudo.Focus();

                }
                else
                {
                    throw new AnexoException (AnexoException.msg);
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void tb_pcmso_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (tb_pcmso.SelectedIndex == 4)
            {
                text_conteudo.Focus();
            }

        }

        public void MontaGridAnexo()
        {
            try
            {
                PcmsoFacade pcmsoFacade = PcmsoFacade.getInstance();
                    
                grd_anexo.Columns.Clear();

                DataSet ds = pcmsoFacade.findAllAnexoByPcmso(pcmso);

                grd_anexo.DataSource = ds.Tables["Anexos"].DefaultView;

                grd_anexo.Columns[0].HeaderText = "Id";
                grd_anexo.Columns[1].HeaderText = "Id_Estudo";
                grd_anexo.Columns[2].HeaderText = "Conteudo";
                    
                grd_anexo.Columns[0].Visible = false;
                grd_anexo.Columns[1].Visible = false;

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void grd_anexo_EditingControlShowing(object sender, DataGridViewEditingControlShowingEventArgs e)
        {
            if (grd_anexo.CurrentCell.ColumnIndex == 2)
            {
                if (e.Control is TextBox)
                {
                    ((TextBox)e.Control).CharacterCasing = CharacterCasing.Upper;
                }
            }

        }

        private void btn_excluirAnexo_Click(object sender, EventArgs e)
        {
            try
            {
                if (grd_anexo.CurrentRow != null)
                {
                    PcmsoFacade pcmsoFacade = PcmsoFacade.getInstance();

                    if (MessageBox.Show(msg14, msg4, MessageBoxButtons.YesNo) == DialogResult.Yes)
                    {
                        Int32 cellValue = grd_anexo.CurrentRow.Index;

                        Int64 id = (Int64)grd_anexo.Rows[cellValue].Cells[0].Value;

                        Anexo anexo = new Anexo();
                        anexo.setId(id);

                        pcmsoFacade.excluiAnexo(anexo);

                        MessageBox.Show(msg15, msg4);

                        MontaGridAnexo();
                    }
                }
                else
                {
                    MessageBox.Show(msg1, msg2);
                }


            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }

        }
        
        private void grd_anexo_CellEndEdit(object sender, DataGridViewCellEventArgs e)
        {
            try
            {

                if (grd_anexo.CurrentRow != null)
                {
                    // montando objeto da edição.

                    PcmsoFacade pcmsoFacade = PcmsoFacade.getInstance();

                    Int32 cellValue = grd_anexo.CurrentRow.Index;

                    Int64 id = (Int64)grd_anexo.Rows[cellValue].Cells[0].Value;
                    String conteudo = (String)grd_anexo.Rows[cellValue].Cells[2].Value;
                    
                    Anexo anexo = new Anexo(id, pcmso, conteudo);

                    pcmsoFacade.alteraAnexo(anexo);
                    MontaGridAnexo();
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                MontaGridAnexo();
                
            }
        }

        private void btn_excluir_Click(object sender, EventArgs e)
        {
            try
            {
                if (grd_exame.CurrentRow != null)
                {

                    if (MessageBox.Show(msg16, msg4, MessageBoxButtons.YesNo) == DialogResult.Yes)
                    {
                        PcmsoFacade pcmsoFacade = PcmsoFacade.getInstance();
                        
                        Int64 id = (Int64)grd_exame.CurrentRow.Cells[0].Value;

                        pcmsoFacade.deleteGheFonteAgenteExame(new GheFonteAgenteExame(id, null, null, 0, ApplicationConstants.DESATIVADO, false, false, false));
                        montaDataGridExame();
                    }
                    
                }
                else
                {
                    MessageBox.Show(msg1, msg2);
                    
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }

        }

        private void grd_exame_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (grd_exame.CurrentRow.Cells[3].EditedFormattedValue.ToString().Length < 2)
            {
                e.Handled = ValidaCampoHelper.FormataCampoNumerico(sender, e);
            }
            else
            {
                if (e.KeyChar == (char)8)
                {
                    e.Handled = false;
                }
                else
                {
                    e.Handled = true;
                }
            }

            if (e.KeyChar == (char)27)
            {
                if (!String.IsNullOrEmpty(controleDigitacao))
                {
                    this.grd_exame.CurrentCell.Value = controleDigitacao;
                    controleDigitacao = String.Empty;
                }

            }
        }

        private void grd_exame_EditingControlShowing(object sender, DataGridViewEditingControlShowingEventArgs e)
        {
            if (e.Control is DataGridViewTextBoxEditingControl)
            {

                e.Control.KeyPress += new KeyPressEventHandler(this.grd_exame_KeyPress);

            }
        }

        private void grd_exame_CellEndEdit(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                PcmsoFacade pcmsoFacade = PcmsoFacade.getInstance();

                // montando objeto ghefonteAgenteExame e enviando para update.

                GheFonteAgenteExame gheFonteAgenteExameAlterado = new GheFonteAgenteExame((Int64)grd_exame.CurrentRow.Cells[0].Value, null, null,
                    (Int32)grd_exame.CurrentRow.Cells[3].Value,ApplicationConstants.ATIVO, (Boolean)grd_exame.CurrentRow.Cells[7].Value, (Boolean)grd_exame.CurrentRow.Cells[8].Value, (bool)grd_exame.CurrentRow.Cells[9].Value);

                pcmsoFacade.updateGheFonteAgenteExame(gheFonteAgenteExameAlterado);

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void grd_exame_CellBeginEdit(object sender, DataGridViewCellCancelEventArgs e)
        {

            controleDigitacao = this.grd_exame.CurrentCell.Value.ToString();
            this.grd_exame.CurrentCell.Value = 0;

        }
       
        #endregion

        private void btn_exame_alterar_Click(object sender, EventArgs e)
        {
            try
            {
                PcmsoFacade pcmsoFacade = PcmsoFacade.getInstance();

                if (this.grd_exame.CurrentRow != null)
                {
                    Int64 id = (Int64)grd_exame.CurrentRow.Cells[0].Value;

                    GheFonteAgenteExame GheFonteAgenteExame = pcmsoFacade.findGheFonteAgenteExameById(id);

                    frm_PcmsoExameIncluir formPcmsoExameIncluir = new frm_PcmsoExameIncluir(this, pcmso.VendedorCliente.getCliente(), gheSelecionado, GheFonteAgenteExame);
                    formPcmsoExameIncluir.ShowDialog();

                    montaDataGridExame();
                }
                else
                {
                    MessageBox.Show(msg17);
                }
            }
            catch (Exception Ex)
            {
                MessageBox.Show(Ex.Message);
            }
        }

    }
}
