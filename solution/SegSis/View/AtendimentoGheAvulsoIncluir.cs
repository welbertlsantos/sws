﻿using SWS.Entidade;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace SWS.View
{
    public partial class frmAtendimentoGheAvulso : frmTemplateConsulta
    {
        protected GheSetorAvulso gheSetor;

        public GheSetorAvulso GheSetor
        {
            get { return gheSetor; }
            set { gheSetor = value; }
        }

        public frmAtendimentoGheAvulso()
        {
            InitializeComponent();
            ActiveControl = textGhe;
        }

        protected void frmAtendimentoGheAvulso_KeyDown(object sender, KeyEventArgs e)
        {
            if ((e.KeyCode == Keys.Enter) || (e.KeyCode == Keys.Return))
            {
                SendKeys.Send("{TAB}");
            }
        }

        protected virtual void btnGravar_Click(object sender, EventArgs e)
        {
            try
            {
                if (!validaCampo())
                {
                    GheSetor = new GheSetorAvulso(textGhe.Text, textSetor.Text);
                    this.Close();
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }

        protected bool validaCampo()
        {
            bool retorno = false;
            try
            {
                if (string.IsNullOrEmpty(textGhe.Text))
                    throw new Exception("Campo GHE obrigatório");

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return true;
            }
            return retorno;
            
        }

        protected void btnFechar_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
