﻿using SWS.Entidade;
using SWS.Facade;
using SWS.Resources;
using SWS.View.Resources;
using SWS.View.ViewHelper;
using SWS.ViewHelper;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace SWS.View
{
    public partial class frmAtendimentoAlterar : frmTemplate
    {

        private Aso atendimento;

        public Aso Atendimento
        {
            get { return atendimento; }
            set { atendimento = value; }
        }

        private List<GheSetorAvulso> gheSetorAvulsoInAtendimento = new List<GheSetorAvulso>();

        public List<GheSetorAvulso> GheSetorAvulsoInAtendimento
        {
            get { return gheSetorAvulsoInAtendimento; }
            set { gheSetorAvulsoInAtendimento = value; }
        }

        private List<AsoAgenteRisco> riscosInAtendimento = new List<AsoAgenteRisco>();

        public List<AsoAgenteRisco> RiscosInAtendimento
        {
            get { return riscosInAtendimento; }
            set { riscosInAtendimento = value; }
        }

        private List<AsoGheSetor> ghesSetoresAtendimento = new List<AsoGheSetor>();

        internal List<AsoGheSetor> GhesSetoresAtendimento
        {
            get { return ghesSetoresAtendimento; }
            set { ghesSetoresAtendimento = value; }
        }

        private bool alteracaoGhe;

        private Medico coordenador;
        private Medico examinador;

        public frmAtendimentoAlterar(Aso atendimento)
        {
            InitializeComponent();

            this.atendimento = atendimento;
            ComboHelper.Boolean(cbPrioridade, false);

            /* preenchendo dados do atendimento */
            text_codigo.Text = atendimento.Codigo;
            dataAtendimento.Text = Convert.ToString(atendimento.DataElaboracao);
            textSenha.Text = atendimento.Senha;
            textObservacao.Text = atendimento.ObservacaoAso;
            text_periodicidade.Text = atendimento.Periodicidade.Descricao;
            
            if (atendimento.DataVencimento != null )
                dataVencimento.Value = Convert.ToDateTime(atendimento.DataVencimento);
            else
            {
                dataVencimento.Value = DateTime.Now;
            }

            textCodigoPo.Text = atendimento.CodigoPo;
            cbPrioridade.SelectedValue = atendimento.Prioridade ? "true" : "false";

            /* preenchendo dados do funcionario */
            text_nome.Text = atendimento.Funcionario.Nome;
            text_rg.Text = atendimento.Funcionario.Rg;
            dt_nascimento.Text = Convert.ToString(atendimento.Funcionario.DataNascimento);
            text_cpf.Text = atendimento.Funcionario.Cpf;
            ComboHelper.TipoSague(cb_TipoSangue);
            cb_TipoSangue.SelectedValue = atendimento.Funcionario.TipoSangue;
            ComboHelper.FatorRH(cb_fatorRH);
            cb_fatorRH.SelectedValue = atendimento.Funcionario.FatorRh;
            text_matricula.Text = atendimento.ClienteFuncaoFuncionario.Matricula;
            textPis.Text = atendimento.Pis;
            textFuncaoFuncionario.Text = atendimento.FuncaoFuncionario;

            /* preenchando dados do cliente */

            if (atendimento.ClienteFuncaoFuncionario.ClienteFuncao.Cliente.Cnpj.Length != 11)
            {
                lblRazaoSocial.Text = "Razão Social";
                text_razaoSocial.Text = atendimento.Cliente.RazaoSocial;
                lblCNPJ.Text = "CNPJ";
                text_cnpj.Mask = "99,999,999/9999-99";
                text_cnpj.Text = atendimento.Cliente.Cnpj;
                text_endereco.Text = atendimento.Cliente.Endereco;
                text_numero.Text = atendimento.Cliente.Numero;
                text_complemento.Text = atendimento.Cliente.Complemento;
                text_bairro.Text = atendimento.Cliente.Bairro;
                text_cidade.Text = atendimento.Cliente.CidadeIbge.Nome;
                text_cep.Text = atendimento.Cliente.Cep;
                ComboHelper.startComboUfSave(cb_uf);
                cb_uf.SelectedValue = atendimento.Cliente.Uf;
            }
            else
            {
                lblRazaoSocial.Text = "Nome";
                text_razaoSocial.Text = atendimento.Cliente.RazaoSocial;
                lblCNPJ.Text = "CPF";
                text_cnpj.Mask = "999,999,999-99";
                text_cnpj.Text = atendimento.Cliente.Cnpj;
                text_endereco.Text = atendimento.Cliente.Endereco;
                text_numero.Text = atendimento.Cliente.Numero;
                text_complemento.Text = atendimento.Cliente.Complemento;
                text_bairro.Text = atendimento.Cliente.Bairro;
                text_cidade.Text = atendimento.Cliente.CidadeIbge.Nome;
                text_cep.Text = atendimento.Cliente.Cep;
                ComboHelper.startComboUfSave(cb_uf);
                cb_uf.SelectedValue = atendimento.Cliente.Uf;
            }

            /* preenchendo dados do médico examinador */

            examinador = atendimento.Examinador;
            text_examinador.Text = atendimento.Examinador != null ? atendimento.Examinador.Nome : string.Empty;
            text_crmExaminador.Text = atendimento.Examinador != null ? atendimento.Examinador.Crm + "-" + atendimento.Examinador.UfCrm : string.Empty;
            text_telefoneExaminador.Text = atendimento.Examinador != null ? atendimento.Examinador.Telefone1 : string.Empty;
            text_celularExaminador.Text = atendimento.Examinador != null ? atendimento.Examinador.Telefone2 : string.Empty;

            coordenador = atendimento.Coordenador;
            text_coordenador.Text = atendimento.Coordenador != null ? atendimento.Coordenador.Nome : string.Empty;
            text_crmCoordenador.Text = atendimento.Coordenador != null ? atendimento.Coordenador.Crm + "-" + atendimento.Coordenador.UfCrm : string.Empty;
            text_telefoneCoordenador.Text = atendimento.Coordenador != null ? atendimento.Coordenador.Telefone1 : string.Empty;
            text_celularCoordenador.Text = atendimento.Coordenador != null ? atendimento.Coordenador.Telefone2 : string.Empty;

            /* altura e espaco confinado */
            ComboHelper.Boolean(cbAltura, false);
            ComboHelper.Boolean(cbConfinado, false);
            ComboHelper.Boolean(cbEletricidade, false);
            cbAltura.SelectedValue = atendimento.Altura ? "true" : "false";
            cbConfinado.SelectedValue = atendimento.Confinado ? "true" : "false";
            cbEletricidade.SelectedValue = atendimento.Eletricidade ? "true" : "false";
            
            
            /*
             * montando a coleção de exames no aso.
             * o aso pode existir cliente funcao exame aso e 
             * ghe fonte agente exame aso.
             */

            montaDataGridExame();

            /* preenchendo dados do centro de custo */
            if (atendimento.Cliente.UsaCentroCusto)
            {
                btnCentroCusto.Enabled = true;
                textCentroCusto.Text = atendimento.CentroCusto != null ? atendimento.CentroCusto.Descricao : string.Empty;
            }

            char[] separadorString = {'-'};
            if (atendimento.ListGheSetorAvulso.Length > 0)
            {
                /* recuperando os gheSetores selecionados quando o aso for avulso */
                string[] gheSetor = atendimento.ListGheSetorAvulso.Split(',');
                foreach (string st in gheSetor)
                {
                    string[] gheSetorTemp;
                    gheSetorTemp = st.Split(separadorString, StringSplitOptions.RemoveEmptyEntries);
                    GheSetorAvulsoInAtendimento.Add(new GheSetorAvulso(gheSetorTemp[0].Trim(), gheSetorTemp.Length > 1 ? gheSetorTemp[1].Trim() : string.Empty));
                }
            }
            
            
            /* recuperando a coleção de ghes e setores quando for selecionado o pcmso */
            AsoFacade asoFacade = AsoFacade.getInstance();
            ghesSetoresAtendimento = asoFacade.findAsoGheSetorByAso(atendimento);
            
            
            montaGridGheSetor();

            /* preenchendo informação dos riscos no atendimento */
            RiscosInAtendimento = atendimento.AsoAgenteRisco;
            montaGridAsoAgenteRisco();
            
        }

        private void montaGridAsoAgenteRisco()
        {
            try
            {
                dgvRisco.Columns.Clear();
                dgvRisco.ColumnCount = 5;

                dgvRisco.Columns[0].HeaderText = "idAso";
                dgvRisco.Columns[0].Visible = false;

                dgvRisco.Columns[1].HeaderText = "idAgente";
                dgvRisco.Columns[1].Visible = false;

                dgvRisco.Columns[2].HeaderText = "Agente";
                dgvRisco.Columns[2].Width = 300;

                dgvRisco.Columns[3].HeaderText = "idRisco";
                dgvRisco.Columns[3].Visible = false;

                dgvRisco.Columns[4].HeaderText = "Risco";
                dgvRisco.Columns[4].Width = 150;


                RiscosInAtendimento.ForEach(delegate(AsoAgenteRisco agr)
                {
                    dgvRisco.Rows.Add(agr.IdAso, agr.IdAgente, agr.DescricaoAgente, agr.IdRisco, agr.DescricaoRisco);
                });

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            
        }

        private void montaGridGheSetor()
        {
            try
            {
                this.dgvGheSetor.Columns.Clear();
                this.dgvGheSetor.ColumnCount = 6;

                this.dgvGheSetor.Columns[0].HeaderText = "idAso";
                this.dgvGheSetor.Columns[0].Visible = false;

                this.dgvGheSetor.Columns[1].HeaderText = "idGhe";
                this.dgvGheSetor.Columns[1].Visible = false;

                this.dgvGheSetor.Columns[2].HeaderText = "Ghe";
                this.dgvGheSetor.Columns[2].Width = 200;

                this.dgvGheSetor.Columns[3].HeaderText = "IdSetor";
                this.dgvGheSetor.Columns[3].Visible = false;

                this.dgvGheSetor.Columns[4].HeaderText = "Setor";
                this.dgvGheSetor.Columns[4].Width = 200;

                this.dgvGheSetor.Columns[5].HeaderText = "NumeroPo";
                this.dgvGheSetor.Columns[5].Visible = false;

                DataGridViewCheckBoxColumn isGheAvulso = new DataGridViewCheckBoxColumn();
                isGheAvulso.Name = "Ghe Avulso?";
                this.dgvGheSetor.Columns.Add(isGheAvulso);
                this.dgvGheSetor.Columns[6].ReadOnly = true;

                if (GheSetorAvulsoInAtendimento.Count > 0)
                {
                    gheSetorAvulsoInAtendimento.ForEach(delegate (GheSetorAvulso gsa) {

                        this.dgvGheSetor.Rows.Add(null, null, gsa.Ghe, null, gsa.Setor, string.Empty, true);
                    });
                    
                }
                
                
                ghesSetoresAtendimento.ForEach(delegate(AsoGheSetor ags)
                {
                    this.dgvGheSetor.Rows.Add(ags.IdAso, ags.IdGhe, ags.DescricaoGhe, ags.IdSetor, ags.DescricaoSetor, ags.NumeroPo, false);
                });

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btn_salvar_Click(object sender, EventArgs e)
        {
            try
            {
                this.Cursor = Cursors.WaitCursor;

                if (ValidaCampos())
                {
                    /*
                        * salvando dados do aso para alteracao
                        * bloco de dados do aso.
                    */

                    atendimento.DataElaboracao = Convert.ToDateTime(dataAtendimento.Text);
                    atendimento.UsuarioAlteracao = PermissionamentoFacade.usuarioAutenticado;
                    atendimento.DataVencimento = dataVencimento.Checked ? (DateTime?)dataVencimento.Value : null;
                    atendimento.CodigoPo = textCodigoPo.Text;
                    atendimento.ObservacaoAso = textObservacao.Text;
                    atendimento.Matricula = text_matricula.Text;
                    atendimento.Pis = textPis.Text;
                    atendimento.Senha = textSenha.Text;
                    atendimento.Prioridade = Convert.ToBoolean(((SelectItem)cbPrioridade.SelectedItem).Valor);
                    atendimento.Altura = Convert.ToBoolean(((SelectItem)cbAltura.SelectedItem).Valor);
                    atendimento.Confinado = Convert.ToBoolean(((SelectItem)cbConfinado.SelectedItem).Valor);
                    atendimento.Eletricidade = Convert.ToBoolean(((SelectItem)cbEletricidade.SelectedItem).Valor);

                    /*
                        * bloco de dados do funcionário
                    */
                    
                    Funcionario funcionario = new Funcionario(text_nome.Text.Trim(), text_rg.Text, Convert.ToDateTime(dt_nascimento.Text), text_cpf.Text, ((SelectItem)cb_TipoSangue.SelectedItem).Valor, ((SelectItem)cb_fatorRH.SelectedItem).Valor); 

                    ClienteFuncaoFuncionario clienteFuncaoFuncionario = new ClienteFuncaoFuncionario(atendimento.ClienteFuncaoFuncionario.Id, atendimento.ClienteFuncaoFuncionario.ClienteFuncao, funcionario, ApplicationConstants.ATIVO, atendimento.ClienteFuncaoFuncionario.DataCadastro, atendimento.ClienteFuncaoFuncionario.DataDesligamento, text_matricula.Text.Trim(), atendimento.ClienteFuncaoFuncionario.Vip, string.Empty, string.Empty, false, true);

                    atendimento.ClienteFuncaoFuncionario = clienteFuncaoFuncionario;
                    atendimento.Funcionario = funcionario;
                    atendimento.FuncaoFuncionario = textFuncaoFuncionario.Text.Trim();

                    /*
                        * bloco de dados do cliente
                    */

                    atendimento.Cliente = new Cliente(atendimento.Cliente.Id, text_razaoSocial.Text.Trim(), atendimento.Cliente.Fantasia, text_cnpj.Text, atendimento.Cliente.Inscricao, text_endereco.Text.Trim(), text_numero.Text.Trim(), text_complemento.Text.Trim(), text_bairro.Text.Trim(), text_cep.Text, ((SelectItem)cb_uf.SelectedItem).Nome, atendimento.Cliente.Telefone1, atendimento.Cliente.Telefone2, atendimento.Cliente.Email, atendimento.Cliente.Site, atendimento.Cliente.DataCadastro, atendimento.Cliente.Responsavel, atendimento.Cliente.Cargo, atendimento.Cliente.Documento, atendimento.Cliente.Escopo, atendimento.Cliente.Jornada, atendimento.Cliente.Situacao, atendimento.Cliente.EstMasc, atendimento.Cliente.EstFem, atendimento.Cliente.EnderecoCob, atendimento.Cliente.NumeroCob, atendimento.Cliente.ComplementoCob, atendimento.Cliente.BairroCob, atendimento.Cliente.CepCob, atendimento.Cliente.UfCob, atendimento.Cliente.TelefoneCob, atendimento.Cliente.Coordenador, atendimento.Cliente.Telefone2Cob, atendimento.Cliente.Vip, atendimento.Cliente.TelefoneContato, atendimento.Cliente.UsaContrato, atendimento.Cliente.Matriz, atendimento.Cliente.CredenciadaCliente, atendimento.Cliente.Particular, atendimento.Cliente.Unidade, atendimento.Cliente.Credenciada, atendimento.Cliente.Prestador, new CidadeIbge(null, String.Empty, text_cidade.Text.ToUpper(), string.Empty), atendimento.Cliente.CidadeIbgeCobranca, atendimento.Cliente.DestacaIss, atendimento.Cliente.GeraCobrancaValorLiquido, atendimento.Cliente.AliquotaIss, atendimento.Cliente.CodigoCnes, atendimento.Cliente.UsaCentroCusto, atendimento.Cliente.Bloqueado, atendimento.Cliente.Credenciadora, atendimento.Cliente.Fisica, atendimento.Cliente.UsaPo, atendimento.Cliente.SimplesNacional);

                    /*
                        * bloco de dados do médico examinador e médico coordenador.
                    */

                    string[] listCrmExaminador = text_crmExaminador.Text.Split('-');

                    if (examinador != null) 
                        atendimento.Examinador = new Medico(examinador.Id, text_examinador.Text, listCrmExaminador[0], ApplicationConstants.ATIVO, text_telefoneExaminador.Text, text_celularExaminador.Text, string.Empty, string.Empty, string.Empty, string.Empty, string.Empty, string.Empty, string.Empty, null, string.Empty, listCrmExaminador[1], string.Empty, null, string.Empty, examinador.Cpf, examinador.Rqe);


                    string[] listCrmCoordenador = text_crmCoordenador.Text.Split('-');

                    if (coordenador != null)
                        atendimento.Coordenador = new Medico(coordenador.Id, text_coordenador.Text, listCrmCoordenador[0], ApplicationConstants.ATIVO, text_telefoneCoordenador.Text, text_celularCoordenador.Text, string.Empty, string.Empty, string.Empty, string.Empty, string.Empty, string.Empty, string.Empty, null, string.Empty, listCrmCoordenador[1], string.Empty, null, string.Empty, coordenador.Cpf, coordenador.Rqe);
                        

                    /* criando colecao de exames no aso para alteracao da data. */

                    List<GheFonteAgenteExameAso> colecaoExamePcmso = new List<GheFonteAgenteExameAso>();
                    List<ClienteFuncaoExameASo> colecaoExameExtra = new List<ClienteFuncaoExameASo>();
                     
                     
                    foreach (DataGridViewRow dvRow in dg_exame.Rows)
                    {
                        /* validando data de validade do exame */
                        if (!(bool)dvRow.Cells[9].Value && (dvRow.Cells[8].Value == null)) throw new Exception("Data de validade marcada sem a data preenchida. Reveja os dados da tela.");
                            
                        if (String.Equals((String)dvRow.Cells[1].Value, ApplicationConstants.EXAME_EXTRA))
                            colecaoExameExtra.Add(new ClienteFuncaoExameASo((long)dvRow.Cells[0].Value, null, atendimento, null, (DateTime)dvRow.Cells[5].Value, false, false, null, false, null, null, null, null, false, null, null, null, Convert.ToBoolean(dvRow.Cells[6].Value), null, null, null, !(bool)dvRow.Cells[9].Value ? Convert.ToDateTime(dvRow.Cells[8].Value) : (DateTime?)null, (bool)dvRow.Cells[10].EditedFormattedValue == true ? true : false));
                        else
                            colecaoExamePcmso.Add(new GheFonteAgenteExameAso((long)dvRow.Cells[0].Value, null, null, null, (DateTime)dvRow.Cells[5].Value, false, false, null, false, null, null, null, null, false, null, null, null, false, Convert.ToBoolean(dvRow.Cells[6].Value), null, null, false, false, null, !(bool)dvRow.Cells[9].Value ? Convert.ToDateTime(dvRow.Cells[8].Value) : (DateTime?)null, false, (bool)dvRow.Cells[10].EditedFormattedValue == true ? true : false));
                    }

                    /* verificando se foi feita alguma alteração em GHE ou setor do aso */
                    if (alteracaoGhe)
                    {
                        StringBuilder gheSetorAtendimento = new StringBuilder();
                        int index = 0;
                        gheSetorAvulsoInAtendimento.ForEach(delegate(GheSetorAvulso gsa)
                        {
                            gheSetorAtendimento.Append(gsa.Ghe);
                            if (!string.IsNullOrEmpty(gsa.Setor)) gheSetorAtendimento.Append(" - " + gsa.Setor);
                            if (GheSetorAvulsoInAtendimento.Count - 1 > index) gheSetorAtendimento.Append(",");
                            index++;
                            
                        });

                        atendimento.ListGheSetorAvulso = gheSetorAtendimento.ToString();
                    }

                    /* verifificando se é permitido gravar um aso sem o médico examinador */

                    string medicoExamiadorAtendimento = string.Empty;
                    PermissionamentoFacade.mapaConfiguracoes.TryGetValue(SWS.Resources.ConfigurationConstans.MEDICO_EXAMINADOR_ATENDIMENTO, out medicoExamiadorAtendimento);

                    if (Convert.ToBoolean(medicoExamiadorAtendimento) == true && examinador == null)
                        throw new Exception("A gravação não poderá ocorrer. Foi configurado que todo o atendimento deve ter um médico examinador.");

                    /* enviando dados para alteracao do aso. */

                    frmJustificativa incluirJustificativa = new frmJustificativa("INCLUIR JUSTIFICATIVA DE ALTERAÇÃO");
                    incluirJustificativa.ShowDialog();

                    if (!String.IsNullOrEmpty(incluirJustificativa.Justificativa))
                    {
                        atendimento.JustificativaAlteracao = incluirJustificativa.Justificativa;
                        AsoFacade asoFacade = AsoFacade.getInstance();

                        asoFacade.alteracaoAso(atendimento, colecaoExamePcmso, colecaoExameExtra);
                        MessageBox.Show("Atendimento alterado com sucesso.", "Confirmação", MessageBoxButtons.OK, MessageBoxIcon.Information) ;
                        this.Close();
                    }

                }

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);

            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }

        private void btn_fechar_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        public void montaDataGridExame()
        {
            try
            {
                this.Cursor = Cursors.WaitCursor;

                dg_exame.Columns.Clear();

                dg_exame.ColumnCount = 5;

                dg_exame.Columns[0].HeaderText = "Código Exame";
                dg_exame.Columns[0].Visible = false;

                dg_exame.Columns[1].HeaderText = "Tipo";
                dg_exame.Columns[1].Visible = false;

                dg_exame.Columns[2].HeaderText = "Exame";
                dg_exame.Columns[2].ReadOnly = true;

                dg_exame.Columns[3].HeaderText = "idItem";
                dg_exame.Columns[3].Visible = false;

                dg_exame.Columns[4].HeaderText = "Finalizado";
                dg_exame.Columns[4].Visible = false;

                CalendarColumn col = new CalendarColumn();
                col.Name = "Data do Exame";
                dg_exame.Columns.Add(col);
                dg_exame.Columns[5].DefaultCellStyle.BackColor = Color.White;

                DataGridViewCheckBoxColumn celTranscrito = new DataGridViewCheckBoxColumn();
                celTranscrito.Name = "Transcrito";
                dg_exame.Columns.Add(celTranscrito);

                DataGridViewTextBoxColumn idExame = new DataGridViewTextBoxColumn();
                idExame.Name = "Codigo";
                dg_exame.Columns.Add(idExame);

                CalendarColumn colVencimento = new CalendarColumn();
                colVencimento.Name = "Data de Validade";
                dg_exame.Columns.Add(colVencimento);
                dg_exame.Columns[8].DefaultCellStyle.BackColor = Color.White;

                DataGridViewCheckBoxColumn vencimentoExame = new DataGridViewCheckBoxColumn();
                vencimentoExame.Name = "Sem Vencimento?";
                dg_exame.Columns.Add(vencimentoExame);

                DataGridViewCheckBoxColumn enviaASO = new DataGridViewCheckBoxColumn();
                enviaASO.Name = "Envia ASO?";
                dg_exame.Columns.Add(enviaASO);
                dg_exame.Columns[9].DefaultCellStyle.BackColor = Color.White;

                /* reordenando  os campos */

                dg_exame.Columns[7].DisplayIndex = 0;
                dg_exame.Columns[2].DisplayIndex = 1;
                dg_exame.Columns[6].DisplayIndex = 2;
                dg_exame.Columns[5].DisplayIndex = 3;
                dg_exame.Columns[9].DisplayIndex = 4;
                dg_exame.Columns[8].DisplayIndex = 5;
                dg_exame.Columns[9].DisplayIndex = 6;

                    
                // preenchendo a grid com as coleções que foram montadas no aso.
                
                foreach (ClienteFuncaoExameASo cfea in atendimento.ExameExtra)
                    if (!cfea.Cancelado)
                        dg_exame.Rows.Add(
                            cfea.Id, 
                            ApplicationConstants.EXAME_EXTRA, 
                            cfea.ClienteFuncaoExame.Exame.Descricao, 
                            cfea.Id, 
                            cfea.Finalizado, 
                            cfea.DataExame, 
                            cfea.Transcrito, 
                            cfea.ClienteFuncaoExame.Exame.Id, 
                            cfea.DataValidade, 
                            cfea.DataValidade == (DateTime?)null ? true : false, 
                            cfea.ImprimeASO);

                foreach (GheFonteAgenteExameAso gfaea in atendimento.ExamePcmso)
                        if (gfaea.Cancelado != true)
                            dg_exame.Rows.Add(
                                gfaea.Id, 
                                ApplicationConstants.EXAME_PCMSO, 
                                gfaea.GheFonteAgenteExame.Exame.Descricao, 
                                gfaea.Id, 
                                gfaea.Finalizado, 
                                gfaea.DataExame, 
                                gfaea.Transcrito, 
                                gfaea.GheFonteAgenteExame.Exame.Id, 
                                gfaea.DataValidade, 
                                gfaea.DataValidade == (DateTime?)null ? true : false,  
                                gfaea.ImprimeASO);


            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);

            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }

        public bool ValidaCampos()
        {
            bool retorno = true;

            try
            {
                if (!ValidaCampoHelper.ValidaCPF(text_cpf.Text.Trim()))
                {
                    ActiveControl = text_cpf;
                    throw new Exception("CPF inválido");
                }
                if (atendimento.ClienteFuncaoFuncionario.ClienteFuncao.Cliente.Fisica)
                {
                    if (!ValidaCampoHelper.ValidaCPF(text_cnpj.Text))
                    {
                        ActiveControl = text_cnpj;
                        throw new Exception("CPF inválido.");
                    }
                }
                else
                {
                    if (!ValidaCampoHelper.ValidaCNPJ(text_cnpj.Text))
                    {
                        ActiveControl = text_cnpj;
                        throw new Exception("CNPJ inválido");
                    }
                }

                if (!ValidaCampoHelper.ValidaCepDigitado(text_cep.Text))
                {
                    ActiveControl = text_cep;
                    throw new Exception("CEP inválido");
                }

                string ordenacao;
                PermissionamentoFacade.mapaConfiguracoes.TryGetValue(ConfigurationConstans.ORDENACAO_ATENDIMENTO, out ordenacao);

                if (Convert.ToBoolean(ordenacao) && string.IsNullOrEmpty(textSenha.Text))
                {
                    ActiveControl = textSenha;
                    throw new Exception("Campo senha obrigatório.");
                }

            }
            catch (Exception ex)
            {
                retorno = false;
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);

                
            }

            return retorno;
        }

        private void dg_exame_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            /* método criado em 21/03/2016 */
            /* exame marcado como transcrito *. 
            * 1º Avaliar se a empresa usa finalizacao no movimento ou produto. Caso
            * tenha sido usada finalizacao no movimento então apenas marcar o exame no atendimento
            * como transcrito. Caso tenha finalização por produto então deverá verificar se
            * existe já um movimento gravado para exame. Caso exista entáo deverá ser 
            * incluído um cancelamento no movimento e marcar o exame como transcrito */

            try
            {
                this.Cursor = Cursors.WaitCursor;

                string statusMovimentoEmpresa = String.Empty;
                
                DataGridView dg = (sender as DataGridView);

                if ((sender as DataGridView).CurrentCell is DataGridViewCheckBoxCell)
                {
                    Boolean situacaoCheck = Convert.ToBoolean(((sender as DataGridView).CurrentCell as DataGridViewCheckBoxCell).EditedFormattedValue);

                    PermissionamentoFacade.mapaConfiguracoes.TryGetValue(ConfigurationConstans.GERA_MOVIMENTO_FINALIZACAO_EXAME, out statusMovimentoEmpresa);

                    FinanceiroFacade financeiroFacade = FinanceiroFacade.getInstance();
                    AsoFacade asoFacade = AsoFacade.getInstance();
                    FilaFacade filaFacade = FilaFacade.getInstance();

                    Movimento movimento = null;
                    GheFonteAgenteExameAso gheFonteAgenteExameAso = null;
                    ClienteFuncaoExameASo clienteFuncaoExameAso = null;

                    /* verificando que tipo de exame foi selecionado */
                    if (String.Equals(dg.CurrentRow.Cells[1].Value, ApplicationConstants.EXAME_PCMSO))
                        gheFonteAgenteExameAso = asoFacade.findGheFonteAgenteExameAsoById((Int64)dg.CurrentRow.Cells[3].Value);
                    else
                        clienteFuncaoExameAso = filaFacade.findClienteFuncaoExameAsoById((Int64)dg.CurrentRow.Cells[3].Value);

                    /* caso o exame seja marcado para ser incluido como transcrito entao deverá ser
                     * lançado um estorno no movimento. Caso o exame seja desmarcado como transcrito
                     * então deverá ser incluido no movimento o exame correspondente. Isso só irá ocorrer
                     * se a situação do exame foi finalizado */

                    if (Convert.ToBoolean(dg.CurrentRow.Cells[4].Value))
                    {
                        movimento = financeiroFacade.findMovimentoByProdutoOrExame(new Movimento(null, gheFonteAgenteExameAso, clienteFuncaoExameAso, atendimento.ClienteFuncaoFuncionario.ClienteFuncao.Cliente, null, null, null, null, ApplicationConstants.PENDENTE, null, null, null, null, null, null, null, null, null, null, DateTime.Now, null, null, null));

                        if (situacaoCheck)
                        {
                            /* exame marcado para ser incluido como transcrito. 
                             * Verificar se a empresa inclui movimento por lançamento ou finalizacao do atendimento.
                             * Verificar se existe um movimento e lançar um estorno */

                            if (Convert.ToBoolean(statusMovimentoEmpresa))
                                /* verificando se existe o movimento */
                                if (movimento != null)
                                    /* será solicitado o estorno do movimento */
                                    financeiroFacade.cancelaMovimento(movimento);

                        }
                        else
                        {
                            /* exame marcado para ser removido como transcrito.
                             * Verificar se a empresa inclui movimento por lancamernto ou finalizacao do atendimento.
                             * Lancar o movimento correspondente */

                            if (Convert.ToBoolean(statusMovimentoEmpresa))
                                /* incluindo movimento por item */
                                financeiroFacade.incluirMovimentoPorItem(gheFonteAgenteExameAso, clienteFuncaoExameAso);
                        }

                    }
                    /* fazendo update no objeto  */

                    if (gheFonteAgenteExameAso != null)
                    {
                        /* fazendo update no gheFonteAgenteExameAso */
                        gheFonteAgenteExameAso.Transcrito = situacaoCheck;
                        gheFonteAgenteExameAso.ImprimeASO = (bool)dg.CurrentRow.Cells[10].EditedFormattedValue;
                        asoFacade.updateGheFonteAgenteExameAso(gheFonteAgenteExameAso);

                    }
                    else if (clienteFuncaoExameAso != null)
                    {
                        /* fazendo update do clientefuncaoExameAso */
                        clienteFuncaoExameAso.Transcrito = situacaoCheck;
                        clienteFuncaoExameAso.ImprimeASO = (bool)dg.CurrentRow.Cells[10].EditedFormattedValue;
                        asoFacade.updateClienteFuncaoExameAso(clienteFuncaoExameAso);
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }

        private void btnCentroCusto_Click(object sender, EventArgs e)
        {
            try
            {
                frmClienteCentroCustoSelecionar selecionarCentroCusto = new frmClienteCentroCustoSelecionar(atendimento.Cliente);
                selecionarCentroCusto.ShowDialog();

                if (selecionarCentroCusto.CentroCustoSelecionado != null)
                {
                    atendimento.CentroCusto = selecionarCentroCusto.CentroCustoSelecionado;
                    textCentroCusto.Text = atendimento.CentroCusto.Descricao;
                }
               
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btnTipoAtendimento_Click(object sender, EventArgs e)
        {
            try
            {
                frmPeriodicidadePesquisar selecionarTipoAtendimento = new frmPeriodicidadePesquisar();
                selecionarTipoAtendimento.ShowDialog();

                if (selecionarTipoAtendimento.TipoAtendimento != null && atendimento.Periodicidade.Id != selecionarTipoAtendimento.TipoAtendimento.Id)
                {
                    /* usuário selecionou uma nova periodicidade */

                    AsoFacade asoFacade = AsoFacade.getInstance();
                    EstudoFacade pcmsoFacade = EstudoFacade.getInstance();
                    FilaFacade filaFacade = FilaFacade.getInstance();

                    /* lista de exames atuais do atendimento */
                    List<ItemFilaAtendimento> examesAsoAtual = filaFacade.buscaExamesInSituacaoByAso(atendimento, null, null, null, null, null).ToList();
                    List<ItemFilaAtendimento> examesAsoAtualOriginal = new List<ItemFilaAtendimento>(examesAsoAtual);

                    List<GheFonteAgenteExameAso> listaExamesInAso = asoFacade.findAllExamePcmsoByAsoInSituacao(atendimento, null, null, null, null, null);


                    List<GheFonteAgenteExame> examesPcmso = atendimento.Estudo != null ? asoFacade.findAllExamesByPcmsoByASO(atendimento.Estudo, ValidaCampoHelper.CalculaIdade(atendimento.ClienteFuncaoFuncionario.Funcionario.DataNascimento), selecionarTipoAtendimento.TipoAtendimento, pcmsoFacade.findAllGheInPcmsoByClienteFuncao(atendimento.ClienteFuncaoFuncionario.ClienteFuncao, atendimento.Estudo), listaExamesInAso.Exists(x => x.Altura == true) ? true : (bool?)null, listaExamesInAso.Exists(x => x.Confinado == true) ? true : (bool?)null, listaExamesInAso.Exists(x => x.Confinado == true) ? true : (bool?)null).ToList() : new List<GheFonteAgenteExame>();

                    examesAsoAtualOriginal.ToList().ForEach(delegate(ItemFilaAtendimento item)
                    {
                        if (examesPcmso.ToList().Exists(x => x.Exame.Id == item.Exame.Id))
                        {
                            examesPcmso.Remove(examesPcmso.First(x => x.Exame.Id == item.Exame.Id));
                            examesAsoAtual.Remove(examesAsoAtual.First(x => x.Exame.Id == item.Exame.Id));
                        }
                        
                    });
                    
                    /* enviando informações para a próxima tela */

                    frmAtendimentoAlterarExames alterarExamesInService = new frmAtendimentoAlterarExames(examesAsoAtual, examesPcmso, atendimento, examesAsoAtualOriginal, selecionarTipoAtendimento.TipoAtendimento);
                    alterarExamesInService.ShowDialog();

                    if (alterarExamesInService.Alterado)
                    {
                        /* alterando o tipo do atendimento na tela */
                        text_periodicidade.Text = selecionarTipoAtendimento.TipoAtendimento.Descricao;

                        /* verificando se a periodicidade alterada foi demissional.
                         * caso tenha sido, então o funcionário deverá ser desligado da empresa
                         * mantendo a coerência do processo. */

                        if (selecionarTipoAtendimento.TipoAtendimento.Id == 5)
                        {
                            MessageBox.Show("O aso é um aso Demissional. Você deverá informar a data de demissão do funcionário que será armazenada para posterior impressão do PPP.", "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Information);

                            FuncionarioFacade funcionarioFacade = FuncionarioFacade.getInstance();

                            List<ClienteFuncaoFuncionario> clienteFuncaoFuncionarios = funcionarioFacade.findClienteFuncaoFuncionarioByFuncionarioAndSituacao(atendimento.ClienteFuncaoFuncionario.Funcionario, true);

                            frmClienteFuncaoFuncionarioExcluir desativarFuncionario = new frmClienteFuncaoFuncionarioExcluir(clienteFuncaoFuncionarios.Find(x => x.ClienteFuncao.Id == atendimento.ClienteFuncaoFuncionario.ClienteFuncao.Cliente.Id && x.DataDesligamento == null));
                            desativarFuncionario.ShowDialog();

                            /* desativando o funcionario na função */
                            funcionarioFacade.disableClienteFuncaoFuncionario(atendimento.ClienteFuncaoFuncionario, (DateTime)desativarFuncionario.ClienteFuncaoFuncionario.DataDesligamento);

                            MessageBox.Show("Gravação da Data de demissão concluída com sucesso.", "Confirmação", MessageBoxButtons.OK, MessageBoxIcon.Information);

                        }
                    }
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }

        private void btnIncluirGheSetor_Click(object sender, EventArgs e)
        {
            try
            {
                frmAtendimentoGheAvulso incluirGheAvulso = new frmAtendimentoGheAvulso();
                incluirGheAvulso.ShowDialog();

                if (incluirGheAvulso.GheSetor != null)
                {
                    this.GheSetorAvulsoInAtendimento.Add(incluirGheAvulso.GheSetor);
                    alteracaoGhe = true;
                    montaGridGheSetor();
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }

        private void btnAlterarGheSetor_Click(object sender, EventArgs e)
        {
            try
            {
                if (dgvGheSetor.CurrentRow == null) throw new Exception("Selecione um Ghe para alterar");

                if ((bool)dgvGheSetor.CurrentRow.Cells[6].Value == false)
                    throw new Exception("Somente Ghes Avulsos podem ser alterados");

                /* identificando o elemento na lista */
                frmAtendimentoGheAvulsoAlterar alterarGheSetor = new frmAtendimentoGheAvulsoAlterar(gheSetorAvulsoInAtendimento.Find(x => x.Ghe == dgvGheSetor.CurrentRow.Cells[2].Value && x.Setor == dgvGheSetor.CurrentRow.Cells[4].Value));
                alterarGheSetor.ShowDialog();
                alteracaoGhe = true;
                montaGridGheSetor();

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }

        private void btnExcluirGheSetor_Click(object sender, EventArgs e)
        {
            try
            {
                if (dgvGheSetor.CurrentRow == null) throw new Exception("Selecione um ghe para excluir");
                
                if ((bool)dgvGheSetor.CurrentRow.Cells[6].Value == false)
                    throw new Exception("Somente Ghes Avulsos podem ser excluídos");
                
                gheSetorAvulsoInAtendimento.Remove(gheSetorAvulsoInAtendimento.Find(x => x.Ghe == dgvGheSetor.CurrentRow.Cells[2].Value && x.Setor == dgvGheSetor.CurrentRow.Cells[4].Value));
                alteracaoGhe = true;
                montaGridGheSetor();
                MessageBox.Show("Ghe Excluído com sucesso!", "Confirmação", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }

        private void btnIncluirAgente_Click(object sender, EventArgs e)
        {
            try
            {
                frmAgentesBuscar incluirAgenteAtendimento = new frmAgentesBuscar();
                incluirAgenteAtendimento.ShowDialog();

                if (incluirAgenteAtendimento.Agente != null)
                {
                    /* verificando se o agente já está incluído no atendimento */
                    if (riscosInAtendimento.Find(x => x.IdAgente == incluirAgenteAtendimento.Agente.Id) != null) throw new Exception("Agente já incluído no atendimento");

                    AsoFacade asoFacade = AsoFacade.getInstance();
                    riscosInAtendimento.Add(asoFacade.insertAsoAgenteRisco(new AsoAgenteRisco(atendimento.Id, incluirAgenteAtendimento.Agente.Id, incluirAgenteAtendimento.Agente.Descricao, incluirAgenteAtendimento.Agente.Risco.Id, incluirAgenteAtendimento.Agente.Risco.Descricao)));
                    montaGridAsoAgenteRisco();
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }

        private void btnExcluirAgente_Click(object sender, EventArgs e)
        {
            try
            {
                if (dgvRisco.CurrentRow == null) throw new Exception("Selecione um agente para excluir");

                AsoFacade asoFacade = AsoFacade.getInstance();
                asoFacade.deleteAsoAgenteRisco(new AsoAgenteRisco((long)dgvRisco.CurrentRow.Cells[0].Value, (long)dgvRisco.CurrentRow.Cells[1].Value, dgvRisco.CurrentRow.Cells[2].Value.ToString(), (long)dgvRisco.CurrentRow.Cells[3].Value, dgvRisco.CurrentRow.Cells[4].Value.ToString()));
                riscosInAtendimento.Remove(riscosInAtendimento.Find(x => x.IdAso == (long)dgvRisco.CurrentRow.Cells[0].Value && x.IdAgente == (long)dgvRisco.CurrentRow.Cells[1].Value));
                MessageBox.Show("Agente removido com sucesso!", "Confirmação", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }

        private void btnExcluirExaminador_Click(object sender, EventArgs e)
        {
            try
            {
                if (examinador == null)
                    throw new Exception("Você deve selecionar o médico examinador primeiro.");

                examinador = null;
                text_examinador.Text = string.Empty;
                text_crmExaminador.Text = string.Empty;
                text_telefoneExaminador.Text = string.Empty;
                text_celularExaminador.Text = string.Empty;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }

        private void btnExcluirCoordenador_Click(object sender, EventArgs e)
        {
            try
            {
                if (coordenador == null)
                    throw new Exception("Você deve selecionar primeiro o médico Coordenador");

                coordenador = null;
                text_coordenador.Text = string.Empty;
                text_crmCoordenador.Text = string.Empty;
                text_telefoneCoordenador.Text = string.Empty;
                text_celularCoordenador.Text = string.Empty;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            
        }

        private void btnExaminador_Click(object sender, EventArgs e)
        {
            try
            {
                frmMedicoBuscar selecionarExaminador = new frmMedicoBuscar();
                selecionarExaminador.ShowDialog();

                if (selecionarExaminador.Medico != null)
                {
                    examinador = selecionarExaminador.Medico;
                    text_examinador.Text = examinador.Nome;
                    text_crmExaminador.Text = examinador.Crm + (string.IsNullOrEmpty(examinador.UfCrm) ? "" : "-" + examinador.UfCrm);
                    text_telefoneExaminador.Text = examinador.Telefone1;
                    text_celularExaminador.Text = examinador.Telefone2;
                    atendimento.Examinador = examinador;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }

        private void btnCoordenador_Click(object sender, EventArgs e)
        {
            try
            {
                frmMedicoBuscar selecionarCoordenador = new frmMedicoBuscar();
                selecionarCoordenador.ShowDialog();

                if (selecionarCoordenador.Medico != null)
                {
                    coordenador = selecionarCoordenador.Medico;
                    text_coordenador.Text = coordenador.Nome;
                    text_crmCoordenador.Text = coordenador.Crm + (string.IsNullOrEmpty(coordenador.UfCrm) ? "" : "-" + coordenador.UfCrm);
                    text_telefoneCoordenador.Text = coordenador.Telefone1;
                    text_celularCoordenador.Text = coordenador.Telefone2;
                    atendimento.Coordenador = coordenador;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }

        private void textSenha_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = ValidaCampoHelper.FormataNumeroELetras(sender, e);
        }
    }
}
