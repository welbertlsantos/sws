﻿using SWS.Entidade;
using SWS.Facade;
using SWS.View.Resources;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace SWS.View
{
    public partial class frmFuncaoInternaIncluir : frmTemplate
    {
        protected FuncaoInterna funcaoInterna;

        public FuncaoInterna FuncaoInterna
        {
            get { return funcaoInterna; }
            set { funcaoInterna = value; }
        }
        
        public frmFuncaoInternaIncluir()
        {
            InitializeComponent();
            ActiveControl = text_descricao;
        }

        protected virtual void btn_gravar_Click(object sender, EventArgs e)
        {
            try
            {
                if (string.IsNullOrEmpty(text_descricao.Text.Trim()))
                    throw new Exception("O nome da função é obrigatório.");
                
                UsuarioFacade UsuarioFacade = UsuarioFacade.getInstance();
                
                funcaoInterna = UsuarioFacade.incluirFuncaoInterna(new FuncaoInterna(null, text_descricao.Text, ApplicationConstants.ATIVO, ApplicationConstants.ON));
                
                MessageBox.Show("Função interna gravada com sucesso.", "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Information);

                        
                this.Close();
            }
            catch (Exception Ex)
            {
                MessageBox.Show(Ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        protected void btn_fechar_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
