﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using SWS.Facade;
using SWS.View.Resources;
using SWS.Entidade;
using SWS.ViewHelper;
using SWS.View.ViewHelper;

namespace SWS.View
{
    public partial class frm_RelAcompanhamentoAtendimento : BaseForm
    {
        public static String msg1 = "Selecione uma sala.";
        public static String msg2 = "Selecione o período de atendimento.";
        public static String msg3 = "Selecione a situação.";

        private Usuario usuario;
        
        public void setUsuario(Usuario usuario)
        {
            this.usuario = usuario;
            if (usuario != null)
            {
                this.text_usuario.Text = usuario.Nome;
            }
        }

        public frm_RelAcompanhamentoAtendimento()
        {
            InitializeComponent();
            carregaComboSalas(cb_sala);
            montaGridFiltroExame();
            ComboHelper.startTipoAtendimento(cb_situacao_atendimento);
        }

        private void carregaComboSalas(ComboBox comboSala)
        {
            try
            {
                FilaFacade filaFacade = FilaFacade.getInstance();

                Sala sala = new Sala();
                sala.Situacao = ApplicationConstants.ATIVO;

                comboSala.DisplayMember = "nome";
                comboSala.ValueMember = "valor";

                foreach (DataRow rows in filaFacade.findSalaByFilter(sala).Tables[0].Rows)
                {
                    comboSala.Items.Add(new SelectItem(Convert.ToString(rows[Convert.ToString("id_sala")]), Convert.ToString(rows[Convert.ToString("descricao")])));
                }

                comboSala.DropDownStyle = ComboBoxStyle.DropDownList;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void chk_data_CheckedChanged(object sender, EventArgs e)
        {
            if (chk_data.Checked == true)
            {
                dt_criacaoFinal.Enabled = true;
                dt_criacaoInicial.Enabled = true;
            }
            else
            {
                dt_criacaoFinal.Enabled = false;
                dt_criacaoInicial.Enabled = false;
            }

        }

        public void montaGridFiltroExame()
        {
            try
            {
                PcmsoFacade pcmsoFacade = PcmsoFacade.getInstance();

                DataSet ds = pcmsoFacade.findExameAtivo(new Exame(null, String.Empty));

                grd_selecao_exames.DataSource = ds.Tables["Exames"].DefaultView;
                
                grd_selecao_exames.Columns[0].HeaderText = "IdExame";
                grd_selecao_exames.Columns[1].HeaderText = "Descrição";
                grd_selecao_exames.Columns[2].HeaderText = "Situação";
                grd_selecao_exames.Columns[3].HeaderText = "Laboratório";
                grd_selecao_exames.Columns[4].HeaderText = "Preço";
                grd_selecao_exames.Columns[5].HeaderText = "Prioridade";
                grd_selecao_exames.Columns[6].HeaderText = "Custo";
                grd_selecao_exames.Columns[7].HeaderText = "Externo";
                grd_selecao_exames.Columns[8].HeaderText = "Documentação";

                grd_selecao_exames.Columns[0].Visible = false;
                grd_selecao_exames.Columns[2].Visible = false;
                grd_selecao_exames.Columns[3].Visible = true;
                grd_selecao_exames.Columns[4].Visible = false;
                grd_selecao_exames.Columns[5].Visible = false;
                grd_selecao_exames.Columns[6].Visible = false;
                grd_selecao_exames.Columns[7].Visible = false;
                grd_selecao_exames.Columns[8].Visible = false;

                grd_selecao_exames.Columns[1].ReadOnly = true;
                grd_selecao_exames.Columns[3].ReadOnly = true;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void grd_selecao_exames_DataBindingComplete(object sender, DataGridViewBindingCompleteEventArgs e)
        {
            grd_selecao_exames.ClearSelection();
        }

        private void btn_usuario_Click(object sender, EventArgs e)
        {
            frmUsuarioSelecionar formUsuarioSelecionar = new frmUsuarioSelecionar();
            formUsuarioSelecionar.ShowDialog();

            if (formUsuarioSelecionar.Usuario != null)
            {
                usuario = formUsuarioSelecionar.Usuario;
                text_usuario.Text = usuario.Nome.ToUpper();
            }

        }

        private void btn_pesquisar_Click(object sender, EventArgs e)
        {
            try
            {
                this.Cursor = Cursors.WaitCursor;
                
                validaCamposObrigatorios();
                Int64? idExame = null;

                if (grd_selecao_exames.CurrentRow.Selected)
                {
                    Int32 cellValue = grd_selecao_exames.CurrentRow.Index;
                    idExame = (Int64)grd_selecao_exames.Rows[cellValue].Cells[0].Value;
                }

                String comando = "-jar Relatorio\\MiddlewareJasper.jar " + "Relatorio\\Report\\ATENDIMENTO.jasper DATA_INICIAL:"+dt_criacaoInicial.Text+":STRING DATA_FINAL:"+dt_criacaoFinal.Text+":STRING SITUACAO:"+cb_situacao_atendimento.SelectedValue+":STRING ID_SALA:"+((SelectItem)cb_sala.SelectedItem).Valor+":INTEGER "+(idExame != null ? "ID_EXAME:"+ idExame+":INTEGER " : " ")+(usuario.Id != null ? "ID_USUARIO:"+ usuario.Id+":INTEGER" : "");
                System.Diagnostics.Process.Start("java", comando);

                this.Cursor = Cursors.Default;

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }

        private void validaCamposObrigatorios()
        {
            StringBuilder mensagem = new StringBuilder();
            
            if (((SelectItem)cb_sala.SelectedItem) == null)
            {
                mensagem.Append(msg1);
            }
            if ((String)cb_situacao_atendimento.SelectedValue == null)
            {
                mensagem.Append(System.Environment.NewLine);
                mensagem.Append(msg3);
            }
            if (chk_data.Checked == false)
            {
                mensagem.Append(System.Environment.NewLine);
                mensagem.Append(msg2);
            }

            if (mensagem.Length > 0)
            {
                throw new Exception(mensagem.ToString());
            }
        }

        private void btn_limpar_Click(object sender, EventArgs e)
        {
            cb_sala.Items.Clear();
            carregaComboSalas(cb_sala);
            montaGridFiltroExame();
            ComboHelper.startTipoAtendimento(cb_situacao_atendimento);
            usuario = null;
            text_usuario.Text = "";
            chk_data.Checked = false;
        }

        private void btn_fechar_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
