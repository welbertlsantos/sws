﻿using SWS.Entidade;
using SWS.Facade;
using SWS.View.Resources;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace SWS.View
{
    public partial class frmMaterialHospitalarAlterar : frmMaterialHospitalarIncluir
    {
        public frmMaterialHospitalarAlterar(MaterialHospitalar materiaHospitalar) :base()
        {
            InitializeComponent();
            this.MateriaHospitalar = materiaHospitalar;

            textNome.Text = MateriaHospitalar.Descricao;
            textUnidade.Text = MateriaHospitalar.Unidade;
        }

        protected override void btnGravar_Click(object sender, EventArgs e)
        {
            try
            {
                if (validaCamposObrigatorio())
                {
                    EstudoFacade pcmsoFacade = EstudoFacade.getInstance();

                    MateriaHospitalar = pcmsoFacade.updateMaterialHospitalar(new MaterialHospitalar(MateriaHospitalar.Id, textNome.Text, textUnidade.Text, ApplicationConstants.ATIVO));
                    MessageBox.Show("Material hospitalar alterado com sucesso.", "Confirmação", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    this.Close();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }
    }
}
