﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using SWS.View;
using SWS.Entidade;
using SWS.Facade;
using log4net;
using SWS.Helper;
using System.IO;
using SWS.View.ViewHelper;
using SWS.ViewHelper;
using System.Diagnostics;
using System.Net.Mail;
using System.Net;

namespace SWS.View
{
    
    public partial class frmMenu : BaseForm
    {
        
        private static String Msg01 = " Confirmação";
        private static String Msg02 = " Deseja realmente sair do Sistema?";
        String login = null;
        private Frm_Login frmLogin;
        Usuario usuarioAutenticado;

        
        public frmMenu(Frm_Login frmLogin)
        {
            InitializeComponent();
            this.frmLogin = frmLogin;
            this.StartPosition = FormStartPosition.CenterScreen;
            this.WindowState = FormWindowState.Maximized;
            login = PermissionamentoFacade.usuarioAutenticado.Login;
            Tre_Principal.ImageList = PrincipalImage;
            this.usuarioAutenticado = PermissionamentoFacade.usuarioAutenticado;
            ControlBoxClose.DisableCloseButton(this.Handle.ToInt32());
            lblVersao.Text = "Versão do Sistema: " + frmLogin.VersaoSistema;
            lblUsuario.Text = "Usuário: " + login;
            lblEmpresa.Text = "Empresa: " + PermissionamentoFacade.filialAutenticada.Name;
            


        }

        private void Tre_Principal_DoubleClick(object sender, EventArgs e)
        {
            if (Tre_Principal.SelectedNode.Name == "ManterUsuario")
            {
                frmUsuarioPrincipal manterUsuario = new frmUsuarioPrincipal();
                manterUsuario.ShowDialog();
            }
            else if (Tre_Principal.SelectedNode.Name == "ManterPerfilDominioAcao")
            {
                frmPerfilMontar manterUsuarioPerfil = new frmPerfilMontar();
                manterUsuarioPerfil.ShowDialog();
            }
            else if (Tre_Principal.SelectedNode.Name == "ManterAtividade")
            {
                frmAtividadePrincipal manterAtividade = new frmAtividadePrincipal();
                manterAtividade.ShowDialog();
            }

            else if (Tre_Principal.SelectedNode.Name == "ManterFonte")
            {
                frmFontePrincipal manterFonte = new frmFontePrincipal();
                manterFonte.ShowDialog();
            }

            else if (Tre_Principal.SelectedNode.Name == "ManterSetor")
            {
                frmSetores manterSetor = new frmSetores();
                manterSetor.ShowDialog();
            }

            else if (Tre_Principal.SelectedNode.Name == "ManterEpi")
            {
                frmEpiPrincipal manterEpi = new frmEpiPrincipal();
                manterEpi.ShowDialog();
            }

            else if (Tre_Principal.SelectedNode.Name == "ManterAgente")
            {
                frmAgentesPrincipal manterAgente = new frmAgentesPrincipal();
                manterAgente.ShowDialog();
            }

            else if (Tre_Principal.SelectedNode.Name == "ManterPerfil")
            {
                frmPerfilPrincipal manterPerfil = new frmPerfilPrincipal();
                manterPerfil.ShowDialog();
            }

            else if (Tre_Principal.SelectedNode.Name == "ManterFuncaoInterna")
            {
                frmFuncaoInternaPrincipal manterFuncaoInterna = new frmFuncaoInternaPrincipal();
                manterFuncaoInterna.ShowDialog();
            }

            else if (Tre_Principal.SelectedNode.Name == "ManterFuncao")
            {
                frmFuncaoPrincipal manterFuncao = new frmFuncaoPrincipal();
                manterFuncao.ShowDialog();
            }

            else if (Tre_Principal.SelectedNode.Name == "ManterVendedor")
            {
                frmVendedorPrincipal manterVendedor = new frmVendedorPrincipal();
                manterVendedor.ShowDialog();
            }

            else if (Tre_Principal.SelectedNode.Name == "ManterCliente")
            {
                frmClientePrincipal manterCliente = new frmClientePrincipal();
                manterCliente.ShowDialog();
            }

            else if (Tre_Principal.SelectedNode.Name == "ManterCnae")
            {
                frmCnaePrincipal manterCnae = new frmCnaePrincipal();
                manterCnae.ShowDialog();
            }

            else if (Tre_Principal.SelectedNode.Name == "ManterNorma")
            {
                frmNotaPrincipal manterNorma = new frmNotaPrincipal();
                manterNorma.ShowDialog();
            }

            else if (Tre_Principal.SelectedNode.Name == "ManterPCMSO")
            {
                frmPcmsoPrincipal manterPcmso = new frmPcmsoPrincipal();
                manterPcmso.ShowDialog();
            }

            else if (Tre_Principal.SelectedNode.Name == "ManterExame")
            {
                frmExamePrincipal manterExame = new frmExamePrincipal();
                manterExame.ShowDialog();
            }

            else if (Tre_Principal.SelectedNode.Name == "ManterMaterialHospitalar")
            {
                frmMaterialHospitalarPrincipal manterMaterialHospitalar = new frmMaterialHospitalarPrincipal();
                manterMaterialHospitalar.ShowDialog();
            }

            else if (Tre_Principal.SelectedNode.Name == "ManterHospital")
            {
                frmHospitalPrincipal manterHospital = new frmHospitalPrincipal();
                manterHospital.ShowDialog();
            }

            else if (Tre_Principal.SelectedNode.Name == "ManterMedico")
            {
                frmMedicoPrincipal manterMedico = new frmMedicoPrincipal();
                manterMedico.ShowDialog();
            }

            else if (Tre_Principal.SelectedNode.Name == "ManterFuncionario")
            {
                frmFuncionarioPrincipal manterFuncionario = new frmFuncionarioPrincipal();
                manterFuncionario.ShowDialog();
            }

            else if (Tre_Principal.SelectedNode.Name == "ManterFuncaoCliente")
            {
                frmClienteFuncaoPrincipal manterFuncaoCliente = new frmClienteFuncaoPrincipal();
                manterFuncaoCliente.ShowDialog();
            }

            else if (Tre_Principal.SelectedNode.Name == "ManterAso")
            {
                frmAtendimentoPrincipal manterASoPrincipal = new frmAtendimentoPrincipal();
                manterASoPrincipal.Show();
            }

            else if (Tre_Principal.SelectedNode.Name == "ManterClienteFuncaoExame")
            {
                frmExamesExtras manterClienteFuncaoexame = new frmExamesExtras(null);
                manterClienteFuncaoexame.ShowDialog();
            }

            else if (Tre_Principal.SelectedNode.Name == "ManterSala")
            {
                frmSalas manterSala = new frmSalas();
                manterSala.ShowDialog();
            }

            else if (Tre_Principal.SelectedNode.Name == "ManterContrato")
            {
                frmContratosAdministrativo manterContrato = new frmContratosAdministrativo();
                manterContrato.ShowDialog();
            }

            else if (Tre_Principal.SelectedNode.Name == "ManterFilaAtendimento")
            {
                frmFilaAtendimento manterFila = new frmFilaAtendimento();
                manterFila.Show();
            }

            else if (Tre_Principal.SelectedNode.Name == "ManterProduto")
            {
                frmProdutoPrincipal manterProduto = new frmProdutoPrincipal();
                manterProduto.ShowDialog();
            }

            else if (Tre_Principal.SelectedNode.Name == "ManterMovimento")
            {
                frmMovimentoPrincipal manterMovimento = new frmMovimentoPrincipal();
                manterMovimento.ShowDialog();
            }

            else if (Tre_Principal.SelectedNode.Name == "ManterEmpresa")
            {
                frmEmpresaPrincipal manterEmpresa = new frmEmpresaPrincipal();
                manterEmpresa.ShowDialog();
            }

            else if (Tre_Principal.SelectedNode.Name == "RelAcompanhamentoAtendimento")
            {
                frmRelatorioAtendimentoSala relatorioAtendimentoSala = new frmRelatorioAtendimentoSala();
                relatorioAtendimentoSala.ShowDialog();
            }

            else if (Tre_Principal.SelectedNode.Name == "ManterPlano")
            {
                frmPlanoPrincipal formPlanoPrincipal = new frmPlanoPrincipal();
                formPlanoPrincipal.ShowDialog();

            }

            else if (Tre_Principal.SelectedNode.Name == "ManterBanco")
            {
                frmBancoPrincipal formBancoPrincipal = new frmBancoPrincipal();
                formBancoPrincipal.ShowDialog();

            }

            else if (Tre_Principal.SelectedNode.Name == "ManterConta")
            {
                frmContaPrincipal formConta = new frmContaPrincipal();
                formConta.ShowDialog();
            }

            else if (Tre_Principal.SelectedNode.Name == "ManterCobranca")
            {
                frmCobrancaPrincipal formCobranca = new frmCobrancaPrincipal();
                formCobranca.ShowDialog();
            }

            else if (Tre_Principal.SelectedNode.Name == "relAsoDuplicado")
            {
                frm_RelAsoDuplicado formRelAsoDuplicado = new frm_RelAsoDuplicado();
                formRelAsoDuplicado.ShowDialog();
            }

            else if (Tre_Principal.SelectedNode.Name == "ManterNotaFiscal")
            {
                try
                {
                    this.Cursor = Cursors.WaitCursor;

                    frmNotaFiscalPrincipal formNotaFiscal = new frmNotaFiscalPrincipal();
                    formNotaFiscal.ShowDialog();
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }
                finally
                {
                    this.Cursor = Cursors.Default;
                }
            }

            else if (Tre_Principal.SelectedNode.Name == "RelatorioMovimento")
            {
                frmRelatorioMovimentoFinanceiro formRelatorioMovimento = new frmRelatorioMovimentoFinanceiro();
                formRelatorioMovimento.ShowDialog();
            }

            else if (Tre_Principal.SelectedNode.Name == "ManterProtocolo")
            {
                frmProtocolosPrincipal formProtocolo = new frmProtocolosPrincipal();
                formProtocolo.ShowDialog();
            }

            else if (Tre_Principal.SelectedNode.Name == "ManterDocumento")
            {
                frmDocumentoPrincipal formDocumento = new frmDocumentoPrincipal();
                formDocumento.ShowDialog();
            }
            else if (Tre_Principal.SelectedNode.Name == "RelatorioLaudoAtendimento")
            {
                frmRelLaudoAtendimento relLaudoAtendimento = new frmRelLaudoAtendimento();
                relLaudoAtendimento.ShowDialog();
            }
            else if (Tre_Principal.SelectedNode.Name == "RelatorioCobranca")
            {
                frmRelCobranca relatorioCobranca = new frmRelCobranca();
                relatorioCobranca.ShowDialog();
            }
            else if (Tre_Principal.SelectedNode.Name == "RelatorioAtendimento")
            {
                frmRelatorioAtendimento relatorioAtendimento = new frmRelatorioAtendimento();
                relatorioAtendimento.ShowDialog();
            }
            else if (Tre_Principal.SelectedNode.Name == "RelatorioAnual")
            {
                frmRelatorioAnual relatorioAnual = new frmRelatorioAnual();
                relatorioAnual.ShowDialog();
            }
            else if (Tre_Principal.SelectedNode.Name == "ManterContratoComercial")
            {
                frmContratos formContrato = new frmContratos();
                formContrato.ShowDialog();
            }
            else if (Tre_Principal.SelectedNode.Name == "RelatorioCliente")
            {
                frmRelatorioCliente relatorioCliente = new frmRelatorioCliente();
                relatorioCliente.ShowDialog();
            }
            else if (Tre_Principal.SelectedNode.Name == "RelatorioAtendimentoColaborador")
            {
                frmRelAtendimentoColaboradorAntigo relatorioAtendimentoColaborador = new frmRelAtendimentoColaboradorAntigo();
                relatorioAtendimentoColaborador.ShowDialog();
            }
            else if (Tre_Principal.SelectedNode.Name == "Configuracao")
            {
                frmConfiguracao configuracao = new frmConfiguracao();
                configuracao.ShowDialog();
            }
            else if (Tre_Principal.SelectedNode.Name == "ESOCIAL-2220")
            {
                frmEsocial2220Principal esocial2220Principal = new frmEsocial2220Principal();
                esocial2220Principal.ShowDialog();
            }
            else if (Tre_Principal.SelectedNode.Name == "RelatorioGestaoCredenciada")
            {
                frmRelGestaoCredenciada relatorioGestaoCredenciada = new frmRelGestaoCredenciada();
                relatorioGestaoCredenciada.ShowDialog();
            }
            else if (Tre_Principal.SelectedNode.Name == "chamadoAvulso")
            {
                frmAcompanhamentoAtendimentoAvulso incluirAcompanhamento = new frmAcompanhamentoAtendimentoAvulso();
                incluirAcompanhamento.ShowDialog();
            }
            else if (Tre_Principal.SelectedNode.Name == "ESOCIAL-2240")
            {
                frmEsocial2240Principal esocial2240 = new frmEsocial2240Principal();
                esocial2240.ShowDialog();
            }
            else if (Tre_Principal.SelectedNode.Name == "MONITORAMENTO")
            {
                frmMonitoramentos monitoramento = new frmMonitoramentos();
                monitoramento.ShowDialog();
            }
            else if (Tre_Principal.SelectedNode.Name == "relatorioPeriodico")
            {
                frmRelatorioPeriodico relatorioPeriodico = new frmRelatorioPeriodico();
                relatorioPeriodico.ShowDialog();
            }
            else if (Tre_Principal.SelectedNode.Name == "ESOCIAL-3000")
            {
                frmEsocial3000Principal esocial3000 = new frmEsocial3000Principal();
                esocial3000.ShowDialog();
            }
            else if (Tre_Principal.SelectedNode.Name == "PROCESSAMENTO-MOVIMENTO")
            {
                frmProcessamentoMovimento processamentoMovimento = new frmProcessamentoMovimento();
                processamentoMovimento.ShowDialog();
            }
            else if (Tre_Principal.SelectedNode.Name == "PROCESSAMENTO-MOVIMENTO-TRANSCRITO")
            {
                frmProcessamentoMovimentoTranscrito processamentoMovimentoTranscrito = new frmProcessamentoMovimentoTranscrito();
                processamentoMovimentoTranscrito.ShowDialog();
            }

        }

        private void Frm_Principal_Load(object sender, EventArgs e)
        {

            montaMenuPCMSO();
            montaMenuUsuario();
            montaMenuCadastroBasico();
            montaMenuAso();
            montaMenuFinanceiro();
            montaMenuFilaAtendimento();
            montaMenuProtocolo();
            montaMenuEsocial();
            montaDashBoard();
        }

        private void montaMenuPCMSO()
        {
            PermissionamentoFacade permissionamentoFacade = PermissionamentoFacade.getInstance();
            TreeNode treeNode = null;

            List<TreeNode> treeNodes = new List<TreeNode>();
            List<TreeNode> treeNodeSub = new List<TreeNode>();

            Boolean possuiAlgumaPermissao = false;

            TreeNode pcmso = new TreeNode("PCMSO");
            pcmso.Name = "ManterPCMSO";

            TreeNode clienteFuncaoExame = new TreeNode("Exames Extras");
            clienteFuncaoExame.Name = "ManterClienteFuncaoExame";

            TreeNode relatorioAnual = new TreeNode("Relatório anual");
            relatorioAnual.Name = "RelatorioAnual";

            if (permissionamentoFacade.usuarioPossuiDominio("CLIENTE_FUNCAO_EXAME"))
            {
                treeNodes.Add(clienteFuncaoExame);
                possuiAlgumaPermissao = true;
            }

            if (permissionamentoFacade.usuarioPossuiDominio("PCMSO"))
            {
                treeNodes.Add(pcmso);
                possuiAlgumaPermissao = true;
            }

            if (permissionamentoFacade.usuarioPossuiDominio("RELATORIO_ANUAL"))
            {
                treeNodeSub.Add(relatorioAnual);
                possuiAlgumaPermissao = true;
            }

            if (possuiAlgumaPermissao)
            {
                TreeNode treeNodesSub = new TreeNode("RELATÓRIOS", treeNodeSub.ToArray());
                treeNodes.Add(treeNodesSub);
                treeNode = new TreeNode("PCMSO", treeNodes.ToArray());
                Tre_Principal.Nodes.Add(treeNode);
            }
        }

        private void montaMenuUsuario()
        {
            PermissionamentoFacade permissionamentoFacade = PermissionamentoFacade.getInstance();
            TreeNode treeNode = null;
            
            List<TreeNode> treeNodes = new List<TreeNode>();
            List<TreeNode> treeNodesSub = new List<TreeNode>();

            Boolean possuiAlgumaPermissao = false;
            Boolean possuiAlgumaPermissaoSub = false;

            TreeNode funcaoInterna = new TreeNode("Função Interna");
            funcaoInterna.Name = "ManterFuncaoInterna";

            TreeNode perfilDominioAcao = new TreeNode("Montar Perfil do Usuário");
            perfilDominioAcao.Name = "ManterPerfilDominioAcao";

            TreeNode perfil = new TreeNode("Perfil");
            perfil.Name = "ManterPerfil";

            TreeNode usuario = new TreeNode("Usuário");
            usuario.Name = "ManterUsuario";

            TreeNode configuracao = new TreeNode("Configuração Sistema");
            configuracao.Name = "Configuracao";

            TreeNode processamentoMovimento = new TreeNode("Processamento de exames não incluídos.");
            processamentoMovimento.Name = "PROCESSAMENTO-MOVIMENTO";

            TreeNode processamentoMovimentoTranscrito = new TreeNode("Processamento de exames transcritos.");
            processamentoMovimentoTranscrito.Name = "PROCESSAMENTO-MOVIMENTO-TRANSCRITO";

            if (permissionamentoFacade.usuarioPossuiDominio("FUNCAO_INTERNA"))
            {
                treeNodes.Add(funcaoInterna);
                possuiAlgumaPermissao = true;
            }
            if (permissionamentoFacade.usuarioPossuiDominio("PERFIL_DOMINIO_ACAO"))
            {
                treeNodes.Add(perfilDominioAcao);
                possuiAlgumaPermissao = true;
            }
            if (permissionamentoFacade.usuarioPossuiDominio("PERFIL"))
            {
                treeNodes.Add(perfil);
                possuiAlgumaPermissao = true;
            }
            if (permissionamentoFacade.usuarioPossuiDominio("USUARIO"))
            {
                treeNodes.Add(usuario);
                possuiAlgumaPermissao = true;
            }

            if (permissionamentoFacade.usuarioPossuiDominio("CONFIGURACAO"))
            {
                treeNodes.Add(configuracao);
                possuiAlgumaPermissao = true;
            }

            if (permissionamentoFacade.usuarioPossuiDominio("PROCESSAMENTO-MOVIMENTO"))
            {
                treeNodesSub.Add(processamentoMovimento);
                possuiAlgumaPermissaoSub = true;
            }

            if (permissionamentoFacade.usuarioPossuiDominio("PROCESSAMENTO-MOVIMENTO-TRANSCRITO"))
            {
                treeNodesSub.Add(processamentoMovimentoTranscrito);
                possuiAlgumaPermissaoSub = true;
            }


            if (possuiAlgumaPermissaoSub)
            {
                TreeNode treeNodeSub = new TreeNode("Processamentos", treeNodesSub.ToArray());
                treeNodes.Add(treeNodeSub);
            }

            if (possuiAlgumaPermissao)
            {
                treeNode = new TreeNode("USUÁRIO", treeNodes.ToArray());
                Tre_Principal.Nodes.Add(treeNode);
            }
        }

        private void montaMenuCadastroBasico()
        {
            PermissionamentoFacade permissionamentoFacade = PermissionamentoFacade.getInstance();
            
            TreeNode treeNode = null;
            
            List<TreeNode> treeNodes = new List<TreeNode>();
            List<TreeNode> treeNodesRelatorio = new List<TreeNode>();

            Boolean possuiAlgumaPermissao = false;

            TreeNode agente = new TreeNode("Agentes");
            agente.Name = "ManterAgente";

            TreeNode atividade = new TreeNode("Atividades");
            atividade.Name = "ManterAtividade";

            TreeNode banco = new TreeNode("Bancos");
            banco.Name = "ManterBanco";

            TreeNode cliente = new TreeNode("Clientes");
            cliente.Name = "ManterCliente";

            TreeNode cnae = new TreeNode("CNAE");
            cnae.Name = "ManterCnae";

            TreeNode conta = new TreeNode("Contas");
            conta.Name = "ManterConta";

            TreeNode documento = new TreeNode("Documentos");
            documento.Name = "ManterDocumento";

            TreeNode empresa = new TreeNode("Empresa");
            empresa.Name = "ManterEmpresa";
                                    
            TreeNode equipamentoProtecao = new TreeNode("EPI");
            equipamentoProtecao.Name = "ManterEpi";

            TreeNode exame = new TreeNode("Exames");
            exame.Name = "ManterExame";

            TreeNode fonte = new TreeNode("Fontes de Exposição");
            fonte.Name = "ManterFonte";

            TreeNode funcao = new TreeNode("Funções");
            funcao.Name = "ManterFuncao";

            TreeNode funcaoCliente = new TreeNode("Cadastrar funções no cliente");
            funcaoCliente.Name = "ManterFuncaoCliente";

            TreeNode funcionario = new TreeNode("Funcionários");
            funcionario.Name = "ManterFuncionario";

            TreeNode hospital = new TreeNode("Hospitais");
            hospital.Name = "ManterHospital";

            TreeNode materialHospitalar = new TreeNode("Materiais Hospitalares");
            materialHospitalar.Name = "ManterMaterialHospitalar";

            TreeNode medico = new TreeNode("Médicos");
            medico.Name = "ManterMedico";

            TreeNode norma = new TreeNode("Notas");
            norma.Name = "ManterNorma";

            TreeNode produto = new TreeNode("Produto");
            produto.Name = "ManterProduto";

            TreeNode plano = new TreeNode("Plano de Pagamento");
            plano.Name = "ManterPlano";

            TreeNode sala = new TreeNode("Salas de Atendimento");
            sala.Name = "ManterSala";

            TreeNode setor = new TreeNode("Setores");
            setor.Name = "ManterSetor";
            
            TreeNode vendedor = new TreeNode("Vendedores");
            vendedor.Name = "ManterVendedor";

            TreeNode relatorioCliente = new TreeNode("Relatorio de Clientes");
            relatorioCliente.Name = "RelatorioCliente";

            if (permissionamentoFacade.usuarioPossuiDominio("AGENTE"))
            {
                treeNodes.Add(agente);
                possuiAlgumaPermissao = true;
            }

            if (permissionamentoFacade.usuarioPossuiDominio("ATIVIDADE"))
            {
                treeNodes.Add(atividade);
                possuiAlgumaPermissao = true;
            }

            if (permissionamentoFacade.usuarioPossuiDominio("BANCO"))
            {
                treeNodes.Add(banco);
                possuiAlgumaPermissao = true;
            }


            if (permissionamentoFacade.usuarioPossuiDominio("CLIENTE"))
            {
                treeNodes.Add(cliente);
                possuiAlgumaPermissao = true;
            }

            if (permissionamentoFacade.usuarioPossuiDominio("CNAE"))
            {
                treeNodes.Add(cnae);
                possuiAlgumaPermissao = true;
            }

            if (permissionamentoFacade.usuarioPossuiDominio("CONTA"))
            {
                treeNodes.Add(conta);
                possuiAlgumaPermissao = true;
            }

            if (permissionamentoFacade.usuarioPossuiDominio("DOCUMENTO"))
            {
                treeNodes.Add(documento);
                possuiAlgumaPermissao = true;
            }

            if (permissionamentoFacade.usuarioPossuiDominio("EMPRESA"))
            {
                treeNodes.Add(empresa);
                possuiAlgumaPermissao = true;
            }

            if (permissionamentoFacade.usuarioPossuiDominio("EPI"))
            {
                treeNodes.Add(equipamentoProtecao);
                possuiAlgumaPermissao = true;
            }

            if (permissionamentoFacade.usuarioPossuiDominio("EXAME"))
            {
                treeNodes.Add(exame);
                possuiAlgumaPermissao = true;
            }

            if (permissionamentoFacade.usuarioPossuiDominio("FONTE"))
            {
                treeNodes.Add(fonte);
                possuiAlgumaPermissao = true;
            }

            if (permissionamentoFacade.usuarioPossuiDominio("FUNCAO"))
            {
                treeNodes.Add(funcao);
                possuiAlgumaPermissao = true;
            }

            if (permissionamentoFacade.usuarioPossuiDominio("FUNCAOCLIENTE"))
            {
                treeNodes.Add(funcaoCliente);
                possuiAlgumaPermissao = true;
            }
            
            if (permissionamentoFacade.usuarioPossuiDominio("FUNCIONARIO"))
            {
                treeNodes.Add(funcionario);
                possuiAlgumaPermissao = true;
            }

            if (permissionamentoFacade.usuarioPossuiDominio("HOSPITAL"))
            {
                treeNodes.Add(hospital);
                possuiAlgumaPermissao = true;
            }

            if (permissionamentoFacade.usuarioPossuiDominio("MATERIAL_HOSPITALAR"))
            {
                treeNodes.Add(materialHospitalar);
                possuiAlgumaPermissao = true;
            }

            if (permissionamentoFacade.usuarioPossuiDominio("MEDICO"))
            {
                treeNodes.Add(medico);
                possuiAlgumaPermissao = true;
            }

            if (permissionamentoFacade.usuarioPossuiDominio("NORMA"))
            {
                treeNodes.Add(norma);
                possuiAlgumaPermissao = true;
            }

            if (permissionamentoFacade.usuarioPossuiDominio("PRODUTO"))
            {
                treeNodes.Add(produto);
                possuiAlgumaPermissao = true;
            }

            if (permissionamentoFacade.usuarioPossuiDominio("PLANO"))
            {
                treeNodes.Add(plano);
                possuiAlgumaPermissao = true;
            }

            if (permissionamentoFacade.usuarioPossuiDominio("SALA_ATENDIMENTO"))
            {
                treeNodes.Add(sala);
                possuiAlgumaPermissao = true;
            }

            if (permissionamentoFacade.usuarioPossuiDominio("SETOR"))
            {
                treeNodes.Add(setor);
                possuiAlgumaPermissao = true;
            }
                        
            if (permissionamentoFacade.usuarioPossuiDominio("VENDEDOR"))
            {
                treeNodes.Add(vendedor);
                possuiAlgumaPermissao = true;
            }

            if (permissionamentoFacade.hasPermission("CLIENTE", "IMPRIMIR"))
            {
                treeNodesRelatorio.Add(relatorioCliente);
                possuiAlgumaPermissao = true;
            }
                        
            if (possuiAlgumaPermissao)
            {
                TreeNode treeNodeRelatorio = new TreeNode("RELATÓRIO", treeNodesRelatorio.ToArray());
                treeNodes.Add(treeNodeRelatorio);
                
                treeNode = new TreeNode("CADASTRO BÁSICO", treeNodes.ToArray());
                Tre_Principal.Nodes.Add(treeNode);

            }

        }

        private void montaMenuAso()
        {
            PermissionamentoFacade permissionamentoFacade = PermissionamentoFacade.getInstance();

            List<TreeNode> treeNodes = new List<TreeNode>();
            List<TreeNode> treeNodesSub = new List<TreeNode>();
            
            Boolean possuiAlgumaPermissao = false;
            Boolean possuiAlgumaPermissaoSub = false;

            TreeNode aso = new TreeNode("Atendimentos");
            aso.Name = "ManterAso";

            TreeNode acompanhamentoAtendimento = new TreeNode("Acompanhamento Atendimento");
            acompanhamentoAtendimento.Name = "AcompanhamentoAtendimento";

            TreeNode relAsoDuplicado = new TreeNode("Relatório Atendimentos em Duplicidade");
            relAsoDuplicado.Name = "relAsoDuplicado";
            
            TreeNode relatorioLaudoAtendimento = new TreeNode("Relatório Laudo Atendimento");
            relatorioLaudoAtendimento.Name = "RelatorioLaudoAtendimento";

            TreeNode relatorioAtendimento = new TreeNode("Relatório de atendimento");
            relatorioAtendimento.Name = "RelatorioAtendimento";

            TreeNode relatorioAtendimentoColaborador = new TreeNode("Relatorio de atendimento por colaborador");
            relatorioAtendimentoColaborador.Name = "RelatorioAtendimentoColaborador";

            TreeNode chamadoAvulso = new TreeNode("Chamado Avulso");
            chamadoAvulso.Name = "chamadoAvulso";

            TreeNode relatorioPeriodico = new TreeNode("Relatório de Periódicos.");
            relatorioPeriodico.Name = "relatorioPeriodico";


            if (permissionamentoFacade.usuarioPossuiDominio("ASO"))
            {
                treeNodes.Add(aso);
                possuiAlgumaPermissao = true;
            }

            if (permissionamentoFacade.usuarioPossuiDominio("CHAMADO_AVULSO"))
            {
                treeNodes.Add(chamadoAvulso);
                possuiAlgumaPermissao = true;
            }

            if (permissionamentoFacade.usuarioPossuiDominio("REL_ASO_DUPLICIDADE"))
            {
                treeNodesSub.Add(relAsoDuplicado);
                possuiAlgumaPermissaoSub = true;
            }

            if (permissionamentoFacade.usuarioPossuiDominio("REL_LAUDO_ATENDIMENTO"))
            {
                treeNodesSub.Add(relatorioLaudoAtendimento);
                possuiAlgumaPermissaoSub = true;
            }

            if (permissionamentoFacade.usuarioPossuiDominio("RELATORIO_ATENDIMENTO"))
            {
                treeNodesSub.Add(relatorioAtendimento);
                possuiAlgumaPermissaoSub = true;
            }

            if (permissionamentoFacade.usuarioPossuiDominio("RELATORIO_ATENDIMENTO_COLABORADOR"))
            {
                treeNodesSub.Add(relatorioAtendimentoColaborador);
                possuiAlgumaPermissaoSub = true;
            }

            if (permissionamentoFacade.usuarioPossuiDominio("RELATORIO_PERIODICO"))
            {
                treeNodesSub.Add(relatorioPeriodico);
                possuiAlgumaPermissaoSub = true;
            }

            if (possuiAlgumaPermissaoSub)
            {
                TreeNode treeNodeSub = new TreeNode("RELATÓRIOS", treeNodesSub.ToArray());
                treeNodes.Add(treeNodeSub);
            }

            if (possuiAlgumaPermissao)
            {
                TreeNode treeNode = new TreeNode("ATENDIMENTO", treeNodes.ToArray());
                Tre_Principal.Nodes.Add(treeNode);

            }
        }

        private void montaMenuFinanceiro()
        {
            PermissionamentoFacade permissionamentoFacade = PermissionamentoFacade.getInstance();
            TreeNode treeNode = null;

            List<TreeNode> treeNodes = new List<TreeNode>();
            List<TreeNode> treeNodeSub = new List<TreeNode>();
            Boolean possuiAlgumaPermissao = false;

            TreeNode contrato = new TreeNode("Contratos Antigos");
            contrato.Name = "ManterContrato";

            TreeNode contratoComercial = new TreeNode("Contratos Comerciais");
            contratoComercial.Name = "ManterContratoComercial";

            TreeNode movimento = new TreeNode("Movimento Financeiro");
            movimento.Name = "ManterMovimento";

            //TreeNode movimentoDetalhado = new TreeNode("Relatório Movimento Detalhado");
            //movimentoDetalhado.Name = "RelMovimentoDetalhado";

            TreeNode cobranca = new TreeNode("Cobranças");
            cobranca.Name = "ManterCobranca";

            TreeNode notaFiscal = new TreeNode("Notas Fiscais");
            notaFiscal.Name = "ManterNotaFiscal";

            TreeNode relatorioMovimento = new TreeNode("Relatório de movimento financeiro");
            relatorioMovimento.Name = "RelatorioMovimento";

            TreeNode relatorioCobranca = new TreeNode("Relatório de Cobranças");
            relatorioCobranca.Name = "RelatorioCobranca";

            TreeNode relatorioGestaoCredenciada = new TreeNode("Gestão de Credenciadas.");
            relatorioGestaoCredenciada.Name = "RelatorioGestaoCredenciada";
           

            if (permissionamentoFacade.usuarioPossuiDominio("CONTRATO"))
            {
                treeNodes.Add(contrato);
                possuiAlgumaPermissao = true;
            }

            if (permissionamentoFacade.usuarioPossuiDominio("CONTRATO_COMERCIAL"))
            {
                treeNodes.Add(contratoComercial);
                possuiAlgumaPermissao = true;
            }

            if (permissionamentoFacade.usuarioPossuiDominio("MOVIMENTO"))
            {
                treeNodes.Add(movimento);
                possuiAlgumaPermissao = true;
            }

            //if (permissionamentoFacade.usuarioPossuiDominio("REL_MOVIMENTO_DETALHADO"))
            //{
            //    treeNodeSub.Add(movimento5);
            //    possuiAlgumaPermissao = true;
            //}

            if (permissionamentoFacade.usuarioPossuiDominio("COBRANCA"))
            {
                treeNodes.Add(cobranca);
                possuiAlgumaPermissao = true;
            }

            if (permissionamentoFacade.usuarioPossuiDominio("NOTAFISCAL"))
            {
                treeNodes.Add(notaFiscal);
                possuiAlgumaPermissao = true;
            }

            if (permissionamentoFacade.usuarioPossuiDominio("RELATORIO_MOVIMENTO"))
            {
                treeNodeSub.Add(relatorioMovimento);
                possuiAlgumaPermissao = true;
            }

            if (permissionamentoFacade.usuarioPossuiDominio("RELATORIO_COBRANCA"))
            {
                treeNodeSub.Add(relatorioCobranca);
                possuiAlgumaPermissao = true;
            }

            if (permissionamentoFacade.usuarioPossuiDominio("RELATORIO_GESTAO_CREDENCIADA"))
            {
                treeNodeSub.Add(relatorioGestaoCredenciada);
                possuiAlgumaPermissao = true;
            }

            if (possuiAlgumaPermissao)
            {
                treeNode = new TreeNode("RELATÓRIOS", treeNodeSub.ToArray());
                treeNodes.Add(treeNode);
                treeNode = new TreeNode("FINANCEIRO", treeNodes.ToArray());
                Tre_Principal.Nodes.Add(treeNode);
            }
        }

        private void montaMenuFilaAtendimento()
        {
            PermissionamentoFacade permissionamentoFacade = PermissionamentoFacade.getInstance();
            TreeNode treeNode = null;

            List<TreeNode> treeNodes = new List<TreeNode>();

            Boolean possuiAlgumaPermissao = false;

            TreeNode fila = new TreeNode("Fila de Atendimento");
            fila.Name = "ManterFilaAtendimento";

            TreeNode relatorios = new TreeNode("Relatorio de acompanhamento por Sala.");
            relatorios.Name = "RelAcompanhamentoAtendimento";

            if (permissionamentoFacade.usuarioPossuiDominio("FILA_ATENDIMENTO"))
            {
                treeNodes.Add(fila);
                possuiAlgumaPermissao = true;
            }

            if (permissionamentoFacade.usuarioPossuiDominio("REL_ACOMPANHAMENTO_ATENDIMENTO"))
            {
                treeNodes.Add(relatorios);
                possuiAlgumaPermissao = true;
            }

            if (possuiAlgumaPermissao)
            {
                treeNode = new TreeNode("FILA DE ATENDIMENTO", treeNodes.ToArray());
                Tre_Principal.Nodes.Add(treeNode);
            }
        }

        private void montaMenuProtocolo()
        {
            PermissionamentoFacade permissionamentoFacade = PermissionamentoFacade.getInstance();
            TreeNode treeNode = null;

            List<TreeNode> treeNodes = new List<TreeNode>();
            Boolean possuiAlgumaPermissao = false;

            TreeNode protocolo = new TreeNode("Gerenciar Protocolo");
            protocolo.Name = "ManterProtocolo";

            if (permissionamentoFacade.usuarioPossuiDominio("PROTOCOLO"))
            {
                treeNodes.Add(protocolo);
                possuiAlgumaPermissao = true;
            }

            if (possuiAlgumaPermissao)
            {
                treeNode = new TreeNode("PROTOCOLO", treeNodes.ToArray());
                Tre_Principal.Nodes.Add(treeNode);

            }
        }

        private void montaMenuEsocial()
        {
            PermissionamentoFacade permissionamentoFacade = PermissionamentoFacade.getInstance();
            TreeNode treeNode = null;

            List<TreeNode> treeNodes = new List<TreeNode>();
            Boolean possuiAlgumaPermissao = false;

            TreeNode monitoramento = new TreeNode("Monitoramento Agentes Nocivos");
            monitoramento.Name = "MONITORAMENTO";

            TreeNode esocial2220 = new TreeNode("ESOCIAL-2220");
            esocial2220.Name = "ESOCIAL-2220";

            TreeNode esocial2240 = new TreeNode("ESOCIAL-2240");
            esocial2240.Name = "ESOCIAL-2240";

            TreeNode esocial3000 = new TreeNode("ESOCIAL-3000");
            esocial3000.Name = "ESOCIAL-3000";

            if (permissionamentoFacade.usuarioPossuiDominio("MONITORAMENTO"))
            {
                treeNodes.Add(monitoramento);
                possuiAlgumaPermissao = true;
            }
            
            if (permissionamentoFacade.usuarioPossuiDominio("ESOCIAL-2220"))
            {
                treeNodes.Add(esocial2220);
                possuiAlgumaPermissao = true;
            }

            if (permissionamentoFacade.usuarioPossuiDominio("ESOCIAL-2240"))
            {
                treeNodes.Add(esocial2240);
                possuiAlgumaPermissao = true;
            }

            if (permissionamentoFacade.usuarioPossuiDominio("ESOCIAL-3000"))
            {
                treeNodes.Add(esocial3000);
                possuiAlgumaPermissao = true;
            }

            if (possuiAlgumaPermissao)
            {
                treeNode = new TreeNode("ESOCIAL", treeNodes.ToArray());
                Tre_Principal.Nodes.Add(treeNode);

            }
        }

        private void Frm_Principal_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode.Equals(Keys.F5))
            {
                e.Handled = true;
                Application.Restart();

            }
        }

        private void btn_sair_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show(Msg02, Msg01, MessageBoxButtons.YesNo) == DialogResult.Yes)
            {
                
                /* matando dos os processos iniciados pelo sistema */

                foreach (Process process in Process.GetProcessesByName("SWS"))
                {
                    process.Kill();
                }

                Application.Exit();

            }
        }

        private void btn_alterarSenha_Click(object sender, EventArgs e)
        {
            frm_usuarioSenhaAlterar formAlteraSenha = new frm_usuarioSenhaAlterar(login);
            formAlteraSenha.ShowDialog();
        }

        private void tHorario_Tick(object sender, EventArgs e)
        {
            lblHora.Text = DateTime.Now.ToString();
        }

        private void btEnviarEmail_Click(object sender, EventArgs e)
        {
            try
            {
                frmEmailEnviar formEmail = new frmEmailEnviar();
                formEmail.ShowDialog();

                /* Inicinado gravação do e-mail na base */

                if (formEmail.Email != null)
                {
                    /* usuário envio o e-mail e foi gravado com sucesso */
                    if (formEmail.Retorno)
                    {
                        Email email = new Email(null, null, formEmail.Email.To.ToString().ToLower(), formEmail.Email.Subject,
                            formEmail.Email.Body, String.Empty, formEmail.Email.Priority.ToString(), DateTime.Now, PermissionamentoFacade.usuarioAutenticado);

                        /* verificando se tem anexos e gravando no email. */

                        if (formEmail.Email.Attachments.Count > 0)
                        {
                            foreach (Attachment anexo in formEmail.Email.Attachments)
                                email.Anexo += anexo.Name + "; ";
                        }

                        AsoFacade asoFacade = AsoFacade.getInstance();
                        asoFacade.insertEmail(email);

                    }

                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btConsultar_Click(object sender, EventArgs e)
        {
            /* montando lista de email por usuário */

            AsoFacade asoFacade = AsoFacade.getInstance();
            List<Email> emailUsuario = asoFacade.findEmailByUsuario(PermissionamentoFacade.usuarioAutenticado);

            frmEmailConsultar consultarEmail = new frmEmailConsultar(emailUsuario);
            consultarEmail.ShowDialog();
        }

        private void Tre_Principal_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (char)Keys.Enter)
                Tre_Principal_DoubleClick(sender, e);
        }

        private void montaDashBoard()
        {
            PermissionamentoFacade permissionamentoFacade = PermissionamentoFacade.getInstance();

            if (permissionamentoFacade.usuarioPossuiDominio("DASH_BOARD"))
                pnlEmail.Enabled = true;
            else
                pnlEmail.Enabled = false;
        }

        private void validarLogado(object sender, EventArgs e)
        {
            LoginAuditoriaFacade loginAuditoriaFacade = LoginAuditoriaFacade.getInstance();

            if (!loginAuditoriaFacade.isLoginAtivo(login, Dns.GetHostName())){

                MessageBox.Show("Login efetuado pelo usuário em outra máquina!", "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);

                this.Close();
                this.frmLogin.Show();
            }
        }
        
    }
}
