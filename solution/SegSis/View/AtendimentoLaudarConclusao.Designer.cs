﻿namespace SWS.View
{
    partial class frmAtendimentoLaudarConclusao
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.flpAcao = new System.Windows.Forms.FlowLayoutPanel();
            this.btn_fechar = new System.Windows.Forms.Button();
            this.btn_gravar = new System.Windows.Forms.Button();
            this.grbObservacao = new System.Windows.Forms.GroupBox();
            this.grb_conclusao = new System.Windows.Forms.GroupBox();
            this.rb_inapto = new System.Windows.Forms.RadioButton();
            this.rb_apto = new System.Windows.Forms.RadioButton();
            this.text_observacao = new System.Windows.Forms.TextBox();
            ((System.ComponentModel.ISupportInitialize)(this.scForm)).BeginInit();
            this.scForm.Panel1.SuspendLayout();
            this.scForm.Panel2.SuspendLayout();
            this.scForm.SuspendLayout();
            this.pnlForm.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.banner)).BeginInit();
            this.flpAcao.SuspendLayout();
            this.grbObservacao.SuspendLayout();
            this.grb_conclusao.SuspendLayout();
            this.SuspendLayout();
            // 
            // scForm
            // 
            // 
            // scForm.Panel1
            // 
            this.scForm.Panel1.Controls.Add(this.flpAcao);
            // 
            // pnlForm
            // 
            this.pnlForm.Controls.Add(this.grb_conclusao);
            this.pnlForm.Controls.Add(this.grbObservacao);
            // 
            // flpAcao
            // 
            this.flpAcao.Controls.Add(this.btn_gravar);
            this.flpAcao.Controls.Add(this.btn_fechar);
            this.flpAcao.Dock = System.Windows.Forms.DockStyle.Fill;
            this.flpAcao.Location = new System.Drawing.Point(0, 0);
            this.flpAcao.Name = "flpAcao";
            this.flpAcao.Size = new System.Drawing.Size(534, 30);
            this.flpAcao.TabIndex = 0;
            // 
            // btn_fechar
            // 
            this.btn_fechar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_fechar.Image = global::SWS.Properties.Resources.close;
            this.btn_fechar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btn_fechar.Location = new System.Drawing.Point(84, 3);
            this.btn_fechar.Name = "btn_fechar";
            this.btn_fechar.Size = new System.Drawing.Size(75, 23);
            this.btn_fechar.TabIndex = 5;
            this.btn_fechar.TabStop = false;
            this.btn_fechar.Text = "&Fechar";
            this.btn_fechar.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btn_fechar.UseVisualStyleBackColor = true;
            this.btn_fechar.Click += new System.EventHandler(this.btn_fechar_Click);
            // 
            // btn_gravar
            // 
            this.btn_gravar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_gravar.Image = global::SWS.Properties.Resources.Gravar;
            this.btn_gravar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btn_gravar.Location = new System.Drawing.Point(3, 3);
            this.btn_gravar.Name = "btn_gravar";
            this.btn_gravar.Size = new System.Drawing.Size(75, 23);
            this.btn_gravar.TabIndex = 4;
            this.btn_gravar.TabStop = false;
            this.btn_gravar.Text = "&Gravar";
            this.btn_gravar.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btn_gravar.UseVisualStyleBackColor = true;
            this.btn_gravar.Click += new System.EventHandler(this.btn_gravar_Click);
            // 
            // grbObservacao
            // 
            this.grbObservacao.Controls.Add(this.text_observacao);
            this.grbObservacao.Location = new System.Drawing.Point(12, 63);
            this.grbObservacao.Name = "grbObservacao";
            this.grbObservacao.Size = new System.Drawing.Size(488, 139);
            this.grbObservacao.TabIndex = 9;
            this.grbObservacao.TabStop = false;
            this.grbObservacao.Text = "Observação";
            // 
            // grb_conclusao
            // 
            this.grb_conclusao.BackColor = System.Drawing.Color.WhiteSmoke;
            this.grb_conclusao.Controls.Add(this.rb_inapto);
            this.grb_conclusao.Controls.Add(this.rb_apto);
            this.grb_conclusao.Location = new System.Drawing.Point(12, 11);
            this.grb_conclusao.Name = "grb_conclusao";
            this.grb_conclusao.Size = new System.Drawing.Size(117, 46);
            this.grb_conclusao.TabIndex = 1;
            this.grb_conclusao.TabStop = false;
            this.grb_conclusao.Text = "Conclusão";
            // 
            // rb_inapto
            // 
            this.rb_inapto.AutoSize = true;
            this.rb_inapto.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.rb_inapto.Location = new System.Drawing.Point(58, 19);
            this.rb_inapto.Name = "rb_inapto";
            this.rb_inapto.Size = new System.Drawing.Size(54, 17);
            this.rb_inapto.TabIndex = 2;
            this.rb_inapto.TabStop = true;
            this.rb_inapto.Text = "Inapto";
            this.rb_inapto.UseVisualStyleBackColor = true;
            // 
            // rb_apto
            // 
            this.rb_apto.AutoSize = true;
            this.rb_apto.Checked = true;
            this.rb_apto.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.rb_apto.Location = new System.Drawing.Point(6, 19);
            this.rb_apto.Name = "rb_apto";
            this.rb_apto.Size = new System.Drawing.Size(46, 17);
            this.rb_apto.TabIndex = 1;
            this.rb_apto.TabStop = true;
            this.rb_apto.Text = "Apto";
            this.rb_apto.UseVisualStyleBackColor = true;
            // 
            // text_observacao
            // 
            this.text_observacao.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.text_observacao.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.text_observacao.Dock = System.Windows.Forms.DockStyle.Fill;
            this.text_observacao.ForeColor = System.Drawing.SystemColors.WindowText;
            this.text_observacao.Location = new System.Drawing.Point(3, 16);
            this.text_observacao.Multiline = true;
            this.text_observacao.Name = "text_observacao";
            this.text_observacao.Size = new System.Drawing.Size(482, 120);
            this.text_observacao.TabIndex = 3;
            // 
            // frmAtendimentoLaudarConclusao
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(534, 361);
            this.Name = "frmAtendimentoLaudarConclusao";
            this.Text = "LAUDAR ATENDIMENTO";
            this.scForm.Panel1.ResumeLayout(false);
            this.scForm.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.scForm)).EndInit();
            this.scForm.ResumeLayout(false);
            this.pnlForm.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.banner)).EndInit();
            this.flpAcao.ResumeLayout(false);
            this.grbObservacao.ResumeLayout(false);
            this.grbObservacao.PerformLayout();
            this.grb_conclusao.ResumeLayout(false);
            this.grb_conclusao.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.FlowLayoutPanel flpAcao;
        private System.Windows.Forms.Button btn_fechar;
        private System.Windows.Forms.Button btn_gravar;
        private System.Windows.Forms.GroupBox grb_conclusao;
        private System.Windows.Forms.RadioButton rb_inapto;
        private System.Windows.Forms.RadioButton rb_apto;
        private System.Windows.Forms.GroupBox grbObservacao;
        private System.Windows.Forms.TextBox text_observacao;
    }
}