﻿using SWS.Entidade;
using SWS.Facade;
using SWS.View.Resources;
using SWS.View.ViewHelper;
using SWS.ViewHelper;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace SWS.View
{
    public partial class frmHospitalIncluir : frmTemplate
    {
        private Hospital hospital;

        public Hospital Hospital
        {
            get { return hospital; }
            set { hospital = value; }
        }

        private CidadeIbge cidade;

        public CidadeIbge Cidade
        {
            get { return cidade; }
            set { cidade = value; }
        }
        
        public frmHospitalIncluir()
        {
            InitializeComponent();
            ComboHelper.unidadeFederativa(cbUf);
            ActiveControl = textNome;
        }

        protected virtual void btnGravar_Click(object sender, EventArgs e)
        {
            try
            {
                if (validaCamposObrigatorio())
                {
                    EstudoFacade pcmsoFacade = EstudoFacade.getInstance();
                    hospital = pcmsoFacade.insertHospital(new Hospital(null, textNome.Text, textEndereco.Text, textNumero.Text, textComplemento.Text, textBairro.Text, textCep.Text, ((SelectItem)cbUf.SelectedItem).Valor, textTelefone.Text, textTelefone2.Text, textObservacao.Text, ApplicationConstants.ATIVO, cidade));
                    MessageBox.Show("Hospital incluído com sucesso.", "Confirmação", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    this.Close();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }

        protected void btnLimpar_Click(object sender, EventArgs e)
        {
            textNome.Text = string.Empty;
            textEndereco.Text = string.Empty;
            textNumero.Text = string.Empty;
            textComplemento.Text = string.Empty;
            textBairro.Text = string.Empty;
            textCep.Text = string.Empty;
            ComboHelper.unidadeFederativa(cbUf);
            cidade = null;
            textCidade.Text = string.Empty;
            textTelefone.Text = string.Empty;
            textTelefone2.Text = string.Empty;
            textObservacao.Text = string.Empty;
            textNome.Focus();
        }

        protected void btnFechar_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        protected void frmHospitalIncluir_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter || e.KeyCode == Keys.Return)
                SendKeys.Send("{TAB}");
        }

        protected void btnCidade_Click(object sender, EventArgs e)
        {
            try
            {
                if (cbUf.SelectedIndex == 0)
                    throw new Exception("Você deve selecionar primeiro a UF.");

                frmCidadePesquisa cidadeBuscar = new frmCidadePesquisa(((SelectItem)cbUf.SelectedItem).Valor);
                cidadeBuscar.ShowDialog();

                if (cidadeBuscar.Cidade != null)
                {
                    Cidade = cidadeBuscar.Cidade;
                    textCidade.Text = Cidade.Nome;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }

        protected bool validaCamposObrigatorio()
        {
            bool retorno = true;

            try
            {
                if (string.IsNullOrEmpty(textNome.Text.Trim()))
                    throw new Exception("O nome do hospital é obrigatório.");

                if (!string.IsNullOrEmpty(textCep.Text.Replace(",","").Replace("-","").Trim()) && !ValidaCampoHelper.ValidaCepDigitado(textCep.Text))
                    throw new Exception("O CEP deverá ter o padrão 99.999-99 ");

            }
            catch (Exception ex)
            {
                retorno = false;
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            return retorno;
        }

        
    }
}
