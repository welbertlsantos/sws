﻿using SWS.Entidade;
using SWS.Facade;
using SWS.View.ViewHelper;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SWS.View
{
    public partial class frmClienteCrendiadaSelecionar : frmTemplateConsulta
    {
        private Cliente credenciada;

        public Cliente Credenciada
        {
            get { return credenciada; }
            set { credenciada = value; }
        }

        private Cliente credenciadora;

        public frmClienteCrendiadaSelecionar(Cliente credenciadora)
        {
            InitializeComponent();
            this.credenciadora = credenciadora;
            this.montaGrid();
        }

        
        private void bt_fechar_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void montaGrid()
        {
            try
            {
                this.dgvCredenciadas.Columns.Clear();
                this.dgvCredenciadas.ColumnCount = 4;

                this.dgvCredenciadas.Columns[0].HeaderText = "idCredenciada";
                this.dgvCredenciadas.Columns[0].Visible = false;

                this.dgvCredenciadas.Columns[1].HeaderText = "Credenciada";
                this.dgvCredenciadas.Columns[1].Width = 300;

                this.dgvCredenciadas.Columns[2].HeaderText = "Fantasia";
                this.dgvCredenciadas.Columns[2].Width = 150;

                this.dgvCredenciadas.Columns[3].HeaderText = "CNPJ";
                this.dgvCredenciadas.Columns[3].Width = 150;

                ClienteFacade clienteFacade = ClienteFacade.getInstance();

                DataSet ds = clienteFacade.findAllCredenciadaByCliente(this.credenciadora);

                if (ds.Tables[0].Rows.Count == 0)
                {
                    MessageBox.Show("Não existe nenhuma credenciada para o cliente selecionado.", "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                    this.Close();
                }

                foreach (DataRow row in ds.Tables[0].Rows)
                {
                    dgvCredenciadas.Rows.Add(
                        (long)row["id_cliente"],
                        row["razao_social"],
                        row["fantasia"],
                        row["cnpj"].ToString().Length == 14 ? ValidaCampoHelper.FormataCnpj((string)row["cnpj"]) : ValidaCampoHelper.FormataCpf((string)row["cnpj"]));
                }
                
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btnConfirmar_Click(object sender, EventArgs e)
        {
            try
            {
                if (dgvCredenciadas.CurrentRow == null)
                    throw new Exception("Selecione uma linha");

                ClienteFacade clienteFacade = ClienteFacade.getInstance();
                this.Credenciada = clienteFacade.findClienteById((long)dgvCredenciadas.CurrentRow.Cells[0].Value);
                this.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }

        private void dgvCredenciadas_DoubleClick(object sender, EventArgs e)
        {
            btnConfirmar.PerformClick();
        }
    }
}
