﻿using SWS.Entidade;
using SWS.Facade;
using SWS.View.ViewHelper;
using SWS.ViewHelper;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace SWS.View
{
    public partial class frmPreClienteIncluir : frmTemplateConsulta
    {
        protected ClienteProposta clienteProposta;

        public ClienteProposta ClienteProposta
        {
            get { return clienteProposta; }
            set { clienteProposta = value; }
        }
        
        public frmPreClienteIncluir()
        {
            InitializeComponent();
            ComboHelper.unidadeFederativa(cbUf);
        }

        private void btnGravar_Click(object sender, EventArgs e)
        {
            try
            {
                if (verificaCampoObrigatorio())
                {
                    ClienteFacade clienteFacade = ClienteFacade.getInstance();
                    clienteProposta = new ClienteProposta(null, textRazaoSocial.Text, textFantasia.Text, textCNPJ.Text, textInscricao.Text, textEndereco.Text, textNumero.Text, textComplemento.Text, textBairro.Text, textCEP.Text, clienteFacade.findCidadeIbgeById(Convert.ToInt64(((SelectItem)cbCidade.SelectedItem).Valor)), ((SelectItem)cbUf.SelectedItem).Valor, textTelefone.Text, textCelular.Text, DateTime.Now, textContato.Text, textEmail.Text); 
                    MessageBox.Show("Cliente incluido na proposta com sucesso", "Confirmação", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    this.Close();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
        }

        private void btnFechar_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void textCNPJ_Leave(object sender, EventArgs e)
        {
            try
            {
                if (!String.IsNullOrEmpty(textCNPJ.Text.Replace(".", "").Replace("/", "").Replace("-", "").Trim()))
                {
                    if (!ValidaCampoHelper.ValidaCNPJ(textCNPJ.Text))
                        throw new Exception("CNPJ inválido.");
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        public bool verificaCampoObrigatorio()
        {
            bool retorno = true;

            try
            {
                if (String.IsNullOrEmpty(textRazaoSocial.Text.Trim()))
                    throw new Exception("Campo Nome do cliente é obrigatório.");

                if (String.IsNullOrEmpty(textTelefone.Text.Trim()))
                    throw new Exception("Campo Telefone é obrigatório.");

            }
            catch (Exception ex)
            {
                retorno = false;
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

            return retorno;
        }

        private void frmPreClienteIncluir_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter || e.KeyCode == Keys.Return)
                SendKeys.Send("{TAB}");
        }

        private void cbUf_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                if (cbUf.SelectedIndex == 0)
                    throw new Exception("Selecione uma Unidade Federativa primeiro.");

                ComboHelper.iniciaComboCidade(cbCidade, ((SelectItem)cbUf.SelectedItem).Valor.ToString());

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void cbCidade_Click(object sender, EventArgs e)
        {
            try
            {
                if (cbUf.SelectedIndex == 0)
                    throw new Exception("Selecione uma Unidade Federativa primeiro.");

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        
    }
}
