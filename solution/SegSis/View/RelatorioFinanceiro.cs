﻿using OfficeOpenXml;
using SWS.Entidade;
using SWS.Facade;
using SWS.View.Resources;
using SWS.View.ViewHelper;
using SWS.ViewHelper;
using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace SWS.View
{
    public partial class frmRelatorioMovimentoFinanceiro : frmTemplateConsulta
    {

        private Cliente cliente;
        private Cliente credenciada;

       
        public frmRelatorioMovimentoFinanceiro()
        {
            InitializeComponent();
            /* carregando combos  */

            ComboHelper.tipoRelatorio(cbTipoRelatorio);
            ComboHelper.situacaoMovimentoRelatorio(cbSituacaoMovimento);
            ComboHelper.analiseRelatorioFinanceiro(cbTipoAnalise);
            ComboHelper.ordenacaoRelatorioMovimento(cbOrdenacao);
            ComboHelper.Boolean(cbUnidade, false);


        }

        private void btn_selecionar_Click(object sender, EventArgs e)
        {
            try
            {
                frmClienteSelecionar formClienteSelecionar = new frmClienteSelecionar(null, null, false, null, null, null, null, null, null);
                formClienteSelecionar.ShowDialog();

                if (formClienteSelecionar.Cliente != null)
                {
                    cliente = formClienteSelecionar.Cliente;
                    textCliente.Text = cliente.RazaoSocial;

                    /* alimentando informações do centro de custo do cliente */

                    if (cliente.UsaCentroCusto == true)
                    {
                        cbCentroCusto.Enabled = true;
                        carregaCentroCusto(cliente.CentroCusto, cbCentroCusto);
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btn_limpar_Click(object sender, EventArgs e)
        {
            try
            {
                if (cliente == null)
                    throw new Exception("Selecione primeiro o cliente.");

                cliente = null;
                textCliente.Text = string.Empty;

                /* excluindo informação do centro de custo */
                cbCentroCusto.DataSource = null;
                cbCentroCusto.Items.Clear();
                cbCentroCusto.Enabled = false;

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
           
        }

        private void btn_imprimir_Click(object sender, EventArgs e)
        {
            try
            {
                bool geraRelatorio = false;

                if (validaData())
                {
                    /* verificando parametros */

                    this.Cursor = Cursors.WaitCursor;

                    ClienteFacade clienteFacade = ClienteFacade.getInstance();

                    String tipoAnalise = ((SelectItem)cbTipoAnalise.SelectedItem).Valor; 
                    String situacao = ((SelectItem)cbSituacaoMovimento.SelectedItem).Valor;
                    String ordenacao = ((SelectItem)cbOrdenacao.SelectedItem).Valor;
                    long idCentroCusto = 0;
                    string centroCusto = string.Empty;
                    bool flagGerecaoXls = false;
                    
                    if (cliente != null && cliente.UsaCentroCusto == true)
                    {
                        idCentroCusto = Convert.ToInt64(((SelectItem)cbCentroCusto.SelectedItem).Valor);
                        centroCusto = ((SelectItem)cbCentroCusto.SelectedItem).Nome;
                    }
                    

                    string dataInicialMovimentoPesquisa = dtInicialMovimento.Checked ? dtInicialMovimento.Value.ToShortDateString() : string.Empty;
                    string dataFinalMovimentoPesquisa = dtFinalMovimento.Checked ? dtFinalMovimento.Value.ToShortDateString() : string.Empty;

                    string dataInicialAtendimentoPesquisa = dtInicialAtendimento.Checked ? dtInicialAtendimento.Value.ToShortDateString() : string.Empty;
                    string dataFinalAtendimentoPesquisa = dtFinalAtendimento.Checked ? dtFinalAtendimento.Value.ToShortDateString() : string.Empty;

                    int idCliente = cliente != null ? (int)cliente.Id : 0;
                    String razaoSocialCliente = cliente != null ? cliente.RazaoSocial : String.Empty;

                    int idCredenciada = this.credenciada != null ? (int)this.credenciada.Id : 0;
                    string credenciadaNome = this.credenciada != null ? this.credenciada.RazaoSocial : string.Empty;

                    String nomeRelatorio = String.Empty;

                    if (string.Equals(((SelectItem)cbTipoRelatorio.SelectedItem).Valor, "S"))
                    {
                        nomeRelatorio = "MOVIMENTO_SINTETICO.jasper";
                        geraRelatorio = true;

                    }
                    else if (string.Equals(((SelectItem)cbTipoRelatorio.SelectedItem).Valor, "A"))
                    {
                        if (cliente == null)
                            throw new Exception("É necessário selecionar o cliente.");

                        if (Convert.ToBoolean(((SelectItem)cbUnidade.SelectedItem).Valor))
                        {
                            if (!clienteFacade.findClienteHaveUnidade(cliente))
                                throw new Exception("Cliente não é matriz.");

                            nomeRelatorio = "MOVIMENTO_ANALITIVO_UNIDADE.jasper";
                            geraRelatorio = true;

                        }
                        else
                        {
                            nomeRelatorio = "MOVIMENTO_ANALITICO.jasper";
                            geraRelatorio = true;
                        }
                    }
                    else if (string.Equals(((SelectItem)cbTipoRelatorio.SelectedItem).Valor, "SS"))
                    {
                        if (cliente == null)
                            throw new Exception("É necessário selecionar o cliente ");

                        nomeRelatorio = "MOVIMENTO_SINTETICO_SIMPLIFICADO.jasper";
                        geraRelatorio = true;

                    }
                    else if (string.Equals(((SelectItem)cbTipoRelatorio.SelectedItem).Valor, "FE"))
                    { 
                        if (cliente == null)
                            throw new Exception("É necessário selecionar o cliente.");

                        nomeRelatorio = "relatorio_financeiro_empresas.jasper";
                        geraRelatorio = true;
                    }
                    else
                    {
                        /* usuário selecionou a geração de arquivo em excel */
                        if (cliente == null)
                            throw new Exception("É necessário selecionar o cliente.");

                        if (cliente.Credenciadora && this.credenciada == null)
                            throw new Exception("Cliente é uma credenciadora. Você deve selecionar a credenciada.");

                        if (string.IsNullOrEmpty(dataInicialMovimentoPesquisa) && string.IsNullOrEmpty(dataFinalMovimentoPesquisa))
                            throw new Exception("É necessário selecionar um período de movimento.");

                        /* parâmetros para gerar o arquivo */
                        using (ExcelPackage excel = new ExcelPackage())
                        {
                            FinanceiroFacade financeiroFacade = FinanceiroFacade.getInstance();
                            FileFinanceiroXlsx arquivoXlsx = financeiroFacade.createFileXlsxMovimento(cliente, Convert.ToDateTime(dtInicialMovimento.Text + " 00:00:00"), Convert.ToDateTime(dtFinalMovimento.Text + " 23:59:59"), dtInicialAtendimento.Checked ? Convert.ToDateTime(dtInicialAtendimento.Text + " 00:00:00") : (DateTime?)null, dtFinalAtendimento.Checked ? Convert.ToDateTime(dtFinalAtendimento.Text + " 23:59:59") : (DateTime?)null, situacao, tipoAnalise, idCentroCusto, this.credenciada);

                            /* montando primeira planilha */

                            if (arquivoXlsx.ListExames.Count > 0)
                            {
                                excel.Workbook.Worksheets.Add("Exames");

                                var headerRowExame = new List<string[]>() { new string[] { "Empresa", "CNPJ/CPF", "Centro de Custo", "Nome", "RG", "CPF", "Função", "Data do Atendimento", "Código do Atendimento", "Tipo de Atendimento", "Exame", "Preço Unitário", "Quantidade", "Situação" }};

                                // Determine the header range (e.g. A1:D1)
                                string headerRangeExame = "A1:" + Char.ConvertFromUtf32(headerRowExame[0].Length + 64) + "1";

                                // Target a worksheet
                                var worksheetExame = excel.Workbook.Worksheets["Exames"];

                                // Popular header row data
                                worksheetExame.Cells[headerRangeExame].LoadFromArrays(headerRowExame);

                                worksheetExame.Cells[headerRangeExame].Style.Font.Bold = true;
                                worksheetExame.Cells[headerRangeExame].Style.Font.Size = 12;
                                worksheetExame.Cells[headerRangeExame].Style.Font.Color.SetColor(System.Drawing.Color.Blue);

                                worksheetExame.Column(7).Style.Numberformat.Format = "dd/MM/yyyy";
                                worksheetExame.Column(4).Style.Numberformat.Format = "_-* #.##0,00_-;-* #.##0,00_-;_-* \" - \"??_-;_-@_-";

                                var cellDataExame = arquivoXlsx.ListExames;

                                worksheetExame.Cells[2, 1].LoadFromArrays(cellDataExame);
                                flagGerecaoXls = true;
                            }

                            if (arquivoXlsx.ListProdutos.Count > 0)
                            {
                                excel.Workbook.Worksheets.Add("Produtos");

                                var headerRowProduto = new List<string[]>() { new string[] { "Empresa", "CNPJ/CPF", "Centro de Custo", "Produto", "Preço Unitário", "Quantidade", "Situação", "Data Inclusão" } };

                                // Determine the header range (e.g. A1:D1)
                                string headerRangeProduto = "A1:" + Char.ConvertFromUtf32(headerRowProduto[0].Length + 64) + "1";

                                // Target a worksheet
                                var worksheetProduto = excel.Workbook.Worksheets["Produtos"];

                                // Popular header row data
                                worksheetProduto.Cells[headerRangeProduto].LoadFromArrays(headerRowProduto);

                                worksheetProduto.Cells[headerRangeProduto].Style.Font.Bold = true;
                                worksheetProduto.Cells[headerRangeProduto].Style.Font.Size = 12;
                                worksheetProduto.Cells[headerRangeProduto].Style.Font.Color.SetColor(System.Drawing.Color.Blue);

                                worksheetProduto.Column(8).Style.Numberformat.Format = "dd/MM/yyyy";
                                worksheetProduto.Column(4).Style.Numberformat.Format = "_-* #.##0,00_-;-* #.##0,00_-;_-* \" - \"??_-;_-@_-";

                                var cellDataProduto = arquivoXlsx.ListProdutos;

                                worksheetProduto.Cells[2, 1].LoadFromArrays(cellDataProduto);
                                flagGerecaoXls = true;
                                
                            }

                            if (flagGerecaoXls == true)
                            {
                                SaveFileDialog saveFileDialog1 = new SaveFileDialog();
                                saveFileDialog1.Filter = "Excel File|*.xlsx";
                                saveFileDialog1.Title = "Salvar como...";
                                saveFileDialog1.ShowDialog();

                                // If the file name is not an empty string open it for saving.  
                                if (saveFileDialog1.FileName != "")
                                {
                                    System.IO.FileStream fs = (System.IO.FileStream)saveFileDialog1.OpenFile();
                                    excel.SaveAs(fs);
                                    fs.Close();
                                    MessageBox.Show("Arquivo gerado com sucesso.", "Confirmação", MessageBoxButtons.OK, MessageBoxIcon.Information);
                                }
                            }
                            else
                            {
                                MessageBox.Show("Não existe dados para gerar o relatório.", "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Information);
                            }
                        }
                    }

                    if (geraRelatorio)
                    {
                        var process = new Process();

                        process.StartInfo.FileName = "Relatorio\\jre7\\bin\\java";
                        process.StartInfo.Arguments = " -jar Relatorio\\MiddlewareJasper.jar " + "Relatorio\\Report\\" + nomeRelatorio + " DT_INICIAL:" + dataInicialMovimentoPesquisa + ":STRING" + " DT_FINAL:" + dataFinalMovimentoPesquisa + ":STRING" + " ID_CLIENTE:" + idCliente + ":INTEGER" + " RAZAO_SOCIAL:" + '"' + razaoSocialCliente + '"' + ":STRING" + " TIPO:" + tipoAnalise + ":STRING" + " SITUACAO:" + situacao + ":STRING" + " ORDENACAO:" + ordenacao + ":STRING" + " DT_INICIAL_ATENDIMENTO:" + dataInicialAtendimentoPesquisa + ":STRING" + " DT_FINAL_ATENDIMENTO:" + dataFinalAtendimentoPesquisa + ":STRING" + " ID_EMPRESA:" + PermissionamentoFacade.usuarioAutenticado.Empresa.Id + ":INTEGER" + " ID_CENTRO_CUSTO:" + idCentroCusto + ":INTEGER" + " CENTRO_CUSTO:" + '"' + centroCusto + '"' + ":STRING" + " ID_CREDENCIADA:" + idCredenciada + ":INTEGER" + " CREDENCIADA:" + '"' + credenciadaNome + '"' + ":STRING" ;
                        process.StartInfo.UseShellExecute = false;
                        process.StartInfo.RedirectStandardError = true;
                        process.StartInfo.RedirectStandardInput = true;
                        process.StartInfo.RedirectStandardOutput = true;
                        process.StartInfo.CreateNoWindow = true;

                        process.Start();
                        process.StandardOutput.ReadToEnd();
                        process.WaitForExit();
                    }
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }

        }

        private void button2_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        public bool validaData()
        {
            bool retorno = true;

            try
            {
                /* validando período do movimento */

                if (dtInicialMovimento.Checked && !dtFinalMovimento.Checked)
                    throw new Exception("Selecione a data final do movimento.");

                if (dtFinalMovimento.Checked && !dtInicialMovimento.Checked)
                    throw new Exception("Selecione a data inicial do movimento.");

                if (!dtInicialMovimento.Checked && !dtFinalMovimento.Checked)
                    throw new Exception("Você deve selecionar uma data para pesquisa.");

                if (Convert.ToDateTime(dtInicialMovimento.Value.ToShortDateString()) > Convert.ToDateTime(dtFinalMovimento.Value.ToShortDateString()))
                    throw new Exception("A data final do movimento não pode ser menor que a data inicial.");

                /* validando período do atendimento. */

                if (dtInicialAtendimento.Checked && !dtFinalAtendimento.Checked)
                    throw new Exception("Selecione a data final do atendimento.");

                if (dtFinalAtendimento.Checked && !dtInicialAtendimento.Checked)
                    throw new Exception("Selecione a data inicial do atendimento.");

                if (Convert.ToDateTime(dtInicialAtendimento.Value.ToShortDateString()) > Convert.ToDateTime(dtFinalAtendimento.Value.ToShortDateString()))
                    throw new Exception("A data de atendimento final não pode ser menor que a data inicial.");

                /* validando se foi selecionada uma empresa credenciadora */

                if (this.cliente != null && this.cliente.Credenciadora && this.credenciada == null)
                    throw new Exception("Foi selecionada uma empresa credenciadora porém não foi selecionada uma empresa credenciada.");

            }
            catch (Exception ex)
            {
                retorno = false;
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

            return retorno;
        }

        private void cbTipoRelatório_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (string.Equals(((SelectItem)cbTipoRelatorio.SelectedItem).Valor, "SS"))
            {
                this.cbOrdenacao.Enabled = false;
                this.cbUnidade.Enabled = false;
            }
            else if (string.Equals(((SelectItem)cbTipoRelatorio.SelectedItem).Valor, "S"))
            {
                this.cbOrdenacao.Enabled = true;
                this.cbUnidade.Enabled = false;
            }
            else if (string.Equals(((SelectItem)cbTipoRelatorio.SelectedItem).Valor, "A"))
            {
                this.cbOrdenacao.Enabled = false;
                this.cbUnidade.Enabled = true;
            }
            else
            {
                this.cbOrdenacao.Enabled = false;
                this.cbUnidade.Enabled = false;
            }


        }

        public void carregaCentroCusto(List<CentroCusto> centroCustos, ComboBox cb)
        {
            try
            {
                ArrayList arr = new ArrayList();
                /* incluindo a primeira informação do combo */

                arr.Add(new SelectItem("0", "Todos os centros de custos"));

                centroCustos.OrderBy(item => item.Descricao).ToList().ForEach(delegate(CentroCusto cc)
                {
                    arr.Add(new SelectItem(cc.Id.ToString(), cc.Descricao));
                });

                cb.DataSource = arr;

                cb.DisplayMember = "Nome";
                cb.ValueMember = "Valor";

                cb.DropDownStyle = ComboBoxStyle.DropDownList;



            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btnCredenciada_Click(object sender, EventArgs e)
        {
            try
            {
                if (this.cliente == null)
                {
                    ActiveControl = btnCliente;
                    throw new Exception("Selecione a credenciadora.");
                    
                }

                if (this.cliente.Credenciadora == false)
                {
                    ActiveControl = btnCliente;
                    throw new Exception("O cliente selecionado não é uma empresa credenciadora.");
                }

                frmClienteCrendiadaSelecionar selecionarCredenciada = new frmClienteCrendiadaSelecionar(this.cliente);
                selecionarCredenciada.ShowDialog();

                if (selecionarCredenciada.Credenciada != null)
                {
                    this.credenciada = selecionarCredenciada.Credenciada;
                    this.textCredenciada.Text = this.credenciada.RazaoSocial;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void excluirCredenciada_Click(object sender, EventArgs e)
        {
            try
            {
                if (this.credenciada == null)
                {
                    ActiveControl = btnCredenciada;
                    throw new Exception("Selecione primeiro a credenciadora.");
                }

                this.credenciada = null;
                textCredenciada.Text = string.Empty;
                ActiveControl = btnCredenciada;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }
    }
}
