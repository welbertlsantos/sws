﻿namespace SWS.View
{
    partial class frmAgentesPrincipal
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            this.flpAcao = new System.Windows.Forms.FlowLayoutPanel();
            this.bt_incluir = new System.Windows.Forms.Button();
            this.bt_alterar = new System.Windows.Forms.Button();
            this.bt_excluir = new System.Windows.Forms.Button();
            this.bt_detalhar = new System.Windows.Forms.Button();
            this.btn_reativar = new System.Windows.Forms.Button();
            this.bt_limpar = new System.Windows.Forms.Button();
            this.bt_pesquisar = new System.Windows.Forms.Button();
            this.bt_fechar = new System.Windows.Forms.Button();
            this.grbAgente = new System.Windows.Forms.GroupBox();
            this.dgvAgente = new System.Windows.Forms.DataGridView();
            this.textAgente = new System.Windows.Forms.TextBox();
            this.lblAgente = new System.Windows.Forms.TextBox();
            this.lblRisco = new System.Windows.Forms.TextBox();
            this.cbTipo = new SWS.ComboBoxWithBorder();
            this.cbSituacao = new SWS.ComboBoxWithBorder();
            this.lblSituacao = new System.Windows.Forms.TextBox();
            this.lblLabel = new System.Windows.Forms.Label();
            this.lblSituacaoAgente = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.spForm)).BeginInit();
            this.spForm.Panel1.SuspendLayout();
            this.spForm.Panel2.SuspendLayout();
            this.spForm.SuspendLayout();
            this.pnlForm.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.banner)).BeginInit();
            this.flpAcao.SuspendLayout();
            this.grbAgente.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvAgente)).BeginInit();
            this.SuspendLayout();
            // 
            // spForm
            // 
            // 
            // spForm.Panel1
            // 
            this.spForm.Panel1.Controls.Add(this.flpAcao);
            // 
            // pnlForm
            // 
            this.pnlForm.Controls.Add(this.lblSituacaoAgente);
            this.pnlForm.Controls.Add(this.lblLabel);
            this.pnlForm.Controls.Add(this.cbSituacao);
            this.pnlForm.Controls.Add(this.lblSituacao);
            this.pnlForm.Controls.Add(this.cbTipo);
            this.pnlForm.Controls.Add(this.lblRisco);
            this.pnlForm.Controls.Add(this.lblAgente);
            this.pnlForm.Controls.Add(this.textAgente);
            this.pnlForm.Controls.Add(this.grbAgente);
            // 
            // flpAcao
            // 
            this.flpAcao.Controls.Add(this.bt_incluir);
            this.flpAcao.Controls.Add(this.bt_alterar);
            this.flpAcao.Controls.Add(this.bt_excluir);
            this.flpAcao.Controls.Add(this.bt_detalhar);
            this.flpAcao.Controls.Add(this.btn_reativar);
            this.flpAcao.Controls.Add(this.bt_limpar);
            this.flpAcao.Controls.Add(this.bt_pesquisar);
            this.flpAcao.Controls.Add(this.bt_fechar);
            this.flpAcao.Dock = System.Windows.Forms.DockStyle.Fill;
            this.flpAcao.Location = new System.Drawing.Point(0, 0);
            this.flpAcao.Name = "flpAcao";
            this.flpAcao.Size = new System.Drawing.Size(784, 35);
            this.flpAcao.TabIndex = 0;
            // 
            // bt_incluir
            // 
            this.bt_incluir.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.bt_incluir.Image = global::SWS.Properties.Resources.icone_mais;
            this.bt_incluir.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.bt_incluir.Location = new System.Drawing.Point(3, 3);
            this.bt_incluir.Name = "bt_incluir";
            this.bt_incluir.Size = new System.Drawing.Size(71, 22);
            this.bt_incluir.TabIndex = 17;
            this.bt_incluir.Text = "&Incluir";
            this.bt_incluir.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.bt_incluir.UseVisualStyleBackColor = true;
            this.bt_incluir.Click += new System.EventHandler(this.bt_incluir_Click);
            // 
            // bt_alterar
            // 
            this.bt_alterar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.bt_alterar.Image = global::SWS.Properties.Resources.Alterar;
            this.bt_alterar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.bt_alterar.Location = new System.Drawing.Point(80, 3);
            this.bt_alterar.Name = "bt_alterar";
            this.bt_alterar.Size = new System.Drawing.Size(71, 22);
            this.bt_alterar.TabIndex = 18;
            this.bt_alterar.Text = "&Alterar";
            this.bt_alterar.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.bt_alterar.UseVisualStyleBackColor = true;
            this.bt_alterar.Click += new System.EventHandler(this.bt_alterar_Click);
            // 
            // bt_excluir
            // 
            this.bt_excluir.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.bt_excluir.Image = global::SWS.Properties.Resources.lixeira;
            this.bt_excluir.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.bt_excluir.Location = new System.Drawing.Point(157, 3);
            this.bt_excluir.Name = "bt_excluir";
            this.bt_excluir.Size = new System.Drawing.Size(71, 22);
            this.bt_excluir.TabIndex = 19;
            this.bt_excluir.Text = "&Excluir";
            this.bt_excluir.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.bt_excluir.UseVisualStyleBackColor = true;
            this.bt_excluir.Click += new System.EventHandler(this.bt_excluir_Click);
            // 
            // bt_detalhar
            // 
            this.bt_detalhar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.bt_detalhar.Image = global::SWS.Properties.Resources.lupa;
            this.bt_detalhar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.bt_detalhar.Location = new System.Drawing.Point(234, 3);
            this.bt_detalhar.Name = "bt_detalhar";
            this.bt_detalhar.Size = new System.Drawing.Size(71, 22);
            this.bt_detalhar.TabIndex = 20;
            this.bt_detalhar.Text = "&Detalhar";
            this.bt_detalhar.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.bt_detalhar.UseVisualStyleBackColor = true;
            this.bt_detalhar.Click += new System.EventHandler(this.bt_detalhar_Click);
            // 
            // btn_reativar
            // 
            this.btn_reativar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_reativar.Image = global::SWS.Properties.Resources.devolucao;
            this.btn_reativar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btn_reativar.Location = new System.Drawing.Point(311, 3);
            this.btn_reativar.Name = "btn_reativar";
            this.btn_reativar.Size = new System.Drawing.Size(71, 22);
            this.btn_reativar.TabIndex = 22;
            this.btn_reativar.Text = "&Reativar";
            this.btn_reativar.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btn_reativar.UseVisualStyleBackColor = true;
            this.btn_reativar.Click += new System.EventHandler(this.btn_reativar_Click);
            // 
            // bt_limpar
            // 
            this.bt_limpar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.bt_limpar.Image = global::SWS.Properties.Resources.vassoura;
            this.bt_limpar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.bt_limpar.Location = new System.Drawing.Point(388, 3);
            this.bt_limpar.Name = "bt_limpar";
            this.bt_limpar.Size = new System.Drawing.Size(71, 22);
            this.bt_limpar.TabIndex = 16;
            this.bt_limpar.Text = "&Limpar";
            this.bt_limpar.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.bt_limpar.UseVisualStyleBackColor = true;
            this.bt_limpar.Click += new System.EventHandler(this.bt_limpar_Click);
            // 
            // bt_pesquisar
            // 
            this.bt_pesquisar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.bt_pesquisar.Image = global::SWS.Properties.Resources.lupa;
            this.bt_pesquisar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.bt_pesquisar.Location = new System.Drawing.Point(465, 3);
            this.bt_pesquisar.Name = "bt_pesquisar";
            this.bt_pesquisar.Size = new System.Drawing.Size(71, 22);
            this.bt_pesquisar.TabIndex = 15;
            this.bt_pesquisar.Text = "&Pesquisar";
            this.bt_pesquisar.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.bt_pesquisar.UseVisualStyleBackColor = true;
            this.bt_pesquisar.Click += new System.EventHandler(this.bt_pesquisar_Click);
            // 
            // bt_fechar
            // 
            this.bt_fechar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.bt_fechar.Image = global::SWS.Properties.Resources.close;
            this.bt_fechar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.bt_fechar.Location = new System.Drawing.Point(542, 3);
            this.bt_fechar.Name = "bt_fechar";
            this.bt_fechar.Size = new System.Drawing.Size(71, 22);
            this.bt_fechar.TabIndex = 21;
            this.bt_fechar.Text = "&Fechar";
            this.bt_fechar.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.bt_fechar.UseVisualStyleBackColor = true;
            this.bt_fechar.Click += new System.EventHandler(this.bt_fechar_Click);
            // 
            // grbAgente
            // 
            this.grbAgente.BackColor = System.Drawing.Color.White;
            this.grbAgente.Controls.Add(this.dgvAgente);
            this.grbAgente.Location = new System.Drawing.Point(3, 83);
            this.grbAgente.Name = "grbAgente";
            this.grbAgente.Size = new System.Drawing.Size(758, 351);
            this.grbAgente.TabIndex = 29;
            this.grbAgente.TabStop = false;
            this.grbAgente.Text = "Agentes";
            // 
            // dgvAgente
            // 
            this.dgvAgente.AllowUserToAddRows = false;
            this.dgvAgente.AllowUserToDeleteRows = false;
            this.dgvAgente.AllowUserToOrderColumns = true;
            this.dgvAgente.AllowUserToResizeRows = false;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.Gainsboro;
            this.dgvAgente.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle1;
            this.dgvAgente.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this.dgvAgente.BackgroundColor = System.Drawing.Color.White;
            this.dgvAgente.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.Color.LightGray;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvAgente.DefaultCellStyle = dataGridViewCellStyle2;
            this.dgvAgente.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvAgente.Location = new System.Drawing.Point(3, 16);
            this.dgvAgente.MultiSelect = false;
            this.dgvAgente.Name = "dgvAgente";
            this.dgvAgente.ReadOnly = true;
            this.dgvAgente.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvAgente.Size = new System.Drawing.Size(752, 332);
            this.dgvAgente.TabIndex = 8;
            this.dgvAgente.CellMouseDoubleClick += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.dgvAgente_CellMouseDoubleClick);
            this.dgvAgente.RowPrePaint += new System.Windows.Forms.DataGridViewRowPrePaintEventHandler(this.dgvAgente_RowPrePaint);
            // 
            // textAgente
            // 
            this.textAgente.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textAgente.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.textAgente.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textAgente.Location = new System.Drawing.Point(147, 16);
            this.textAgente.MaxLength = 100;
            this.textAgente.Name = "textAgente";
            this.textAgente.Size = new System.Drawing.Size(608, 21);
            this.textAgente.TabIndex = 31;
            // 
            // lblAgente
            // 
            this.lblAgente.BackColor = System.Drawing.Color.Silver;
            this.lblAgente.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblAgente.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblAgente.Location = new System.Drawing.Point(3, 16);
            this.lblAgente.Name = "lblAgente";
            this.lblAgente.ReadOnly = true;
            this.lblAgente.Size = new System.Drawing.Size(145, 21);
            this.lblAgente.TabIndex = 34;
            this.lblAgente.TabStop = false;
            this.lblAgente.Text = "Agente";
            // 
            // lblRisco
            // 
            this.lblRisco.BackColor = System.Drawing.Color.Silver;
            this.lblRisco.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblRisco.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblRisco.Location = new System.Drawing.Point(3, 36);
            this.lblRisco.Name = "lblRisco";
            this.lblRisco.ReadOnly = true;
            this.lblRisco.Size = new System.Drawing.Size(145, 21);
            this.lblRisco.TabIndex = 35;
            this.lblRisco.TabStop = false;
            this.lblRisco.Text = "Risco";
            // 
            // cbTipo
            // 
            this.cbTipo.BackColor = System.Drawing.Color.LightGray;
            this.cbTipo.BorderColor = System.Drawing.Color.DimGray;
            this.cbTipo.BorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid;
            this.cbTipo.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbTipo.FormattingEnabled = true;
            this.cbTipo.Location = new System.Drawing.Point(147, 36);
            this.cbTipo.Name = "cbTipo";
            this.cbTipo.Size = new System.Drawing.Size(608, 21);
            this.cbTipo.TabIndex = 36;
            // 
            // cbSituacao
            // 
            this.cbSituacao.BackColor = System.Drawing.Color.LightGray;
            this.cbSituacao.BorderColor = System.Drawing.Color.DimGray;
            this.cbSituacao.BorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid;
            this.cbSituacao.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbSituacao.FormattingEnabled = true;
            this.cbSituacao.Location = new System.Drawing.Point(147, 56);
            this.cbSituacao.Name = "cbSituacao";
            this.cbSituacao.Size = new System.Drawing.Size(608, 21);
            this.cbSituacao.TabIndex = 38;
            // 
            // lblSituacao
            // 
            this.lblSituacao.BackColor = System.Drawing.Color.Silver;
            this.lblSituacao.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblSituacao.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSituacao.Location = new System.Drawing.Point(3, 56);
            this.lblSituacao.Name = "lblSituacao";
            this.lblSituacao.ReadOnly = true;
            this.lblSituacao.Size = new System.Drawing.Size(145, 21);
            this.lblSituacao.TabIndex = 37;
            this.lblSituacao.TabStop = false;
            this.lblSituacao.Text = "Situação";
            // 
            // lblLabel
            // 
            this.lblLabel.AutoSize = true;
            this.lblLabel.BackColor = System.Drawing.Color.Red;
            this.lblLabel.ForeColor = System.Drawing.Color.Black;
            this.lblLabel.Location = new System.Drawing.Point(12, 439);
            this.lblLabel.Name = "lblLabel";
            this.lblLabel.Size = new System.Drawing.Size(13, 13);
            this.lblLabel.TabIndex = 39;
            this.lblLabel.Text = "  ";
            // 
            // lblSituacaoAgente
            // 
            this.lblSituacaoAgente.AutoSize = true;
            this.lblSituacaoAgente.Location = new System.Drawing.Point(31, 439);
            this.lblSituacaoAgente.Name = "lblSituacaoAgente";
            this.lblSituacaoAgente.Size = new System.Drawing.Size(44, 13);
            this.lblSituacaoAgente.TabIndex = 40;
            this.lblSituacaoAgente.Text = "Inativos";
            // 
            // frmAgentesPrincipal
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.ClientSize = new System.Drawing.Size(784, 561);
            this.Name = "frmAgentesPrincipal";
            this.Text = "PESQUISAR AGENTE";
            this.spForm.Panel1.ResumeLayout(false);
            this.spForm.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.spForm)).EndInit();
            this.spForm.ResumeLayout(false);
            this.pnlForm.ResumeLayout(false);
            this.pnlForm.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.banner)).EndInit();
            this.flpAcao.ResumeLayout(false);
            this.grbAgente.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvAgente)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.FlowLayoutPanel flpAcao;
        private System.Windows.Forms.Button bt_detalhar;
        private System.Windows.Forms.Button bt_limpar;
        private System.Windows.Forms.Button bt_fechar;
        private System.Windows.Forms.Button btn_reativar;
        private System.Windows.Forms.Button bt_pesquisar;
        private System.Windows.Forms.Button bt_incluir;
        private System.Windows.Forms.Button bt_excluir;
        private System.Windows.Forms.Button bt_alterar;
        private System.Windows.Forms.GroupBox grbAgente;
        private System.Windows.Forms.DataGridView dgvAgente;
        private ComboBoxWithBorder cbSituacao;
        private System.Windows.Forms.TextBox lblSituacao;
        private ComboBoxWithBorder cbTipo;
        private System.Windows.Forms.TextBox lblRisco;
        private System.Windows.Forms.TextBox lblAgente;
        private System.Windows.Forms.TextBox textAgente;
        private System.Windows.Forms.Label lblSituacaoAgente;
        private System.Windows.Forms.Label lblLabel;
    }
}
