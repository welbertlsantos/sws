﻿namespace SWS.View
{
    partial class frmPcmsoExamesIncluir
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            this.flpAcao = new System.Windows.Forms.FlowLayoutPanel();
            this.btIncluir = new System.Windows.Forms.Button();
            this.btFechar = new System.Windows.Forms.Button();
            this.btExame = new System.Windows.Forms.Button();
            this.textGhe = new System.Windows.Forms.TextBox();
            this.textExame = new System.Windows.Forms.TextBox();
            this.lblGhe = new System.Windows.Forms.TextBox();
            this.lblExame = new System.Windows.Forms.TextBox();
            this.llbIdade = new System.Windows.Forms.TextBox();
            this.textIdade = new System.Windows.Forms.TextBox();
            this.lblAltura = new System.Windows.Forms.TextBox();
            this.lblEspaco = new System.Windows.Forms.TextBox();
            this.cbAltura = new SWS.ComboBoxWithBorder();
            this.cbEspaco = new SWS.ComboBoxWithBorder();
            this.btExcluirExcecao = new System.Windows.Forms.Button();
            this.grbExcecao = new System.Windows.Forms.GroupBox();
            this.dgvExcecao = new System.Windows.Forms.DataGridView();
            this.btIncluirExcecao = new System.Windows.Forms.Button();
            this.grbPeriodicidade = new System.Windows.Forms.GroupBox();
            this.dgvPeriodicidade = new System.Windows.Forms.DataGridView();
            this.cbEletricidade = new SWS.ComboBoxWithBorder();
            this.lblEletricidade = new System.Windows.Forms.TextBox();
            ((System.ComponentModel.ISupportInitialize)(this.scForm)).BeginInit();
            this.scForm.Panel1.SuspendLayout();
            this.scForm.Panel2.SuspendLayout();
            this.scForm.SuspendLayout();
            this.pnlForm.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.banner)).BeginInit();
            this.flpAcao.SuspendLayout();
            this.grbExcecao.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvExcecao)).BeginInit();
            this.grbPeriodicidade.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvPeriodicidade)).BeginInit();
            this.SuspendLayout();
            // 
            // scForm
            // 
            this.scForm.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            // 
            // scForm.Panel1
            // 
            this.scForm.Panel1.Controls.Add(this.flpAcao);
            this.scForm.Size = new System.Drawing.Size(534, 570);
            this.scForm.SplitterDistance = 32;
            this.scForm.SplitterWidth = 5;
            // 
            // pnlForm
            // 
            this.pnlForm.Controls.Add(this.btExame);
            this.pnlForm.Controls.Add(this.textExame);
            this.pnlForm.Controls.Add(this.cbEletricidade);
            this.pnlForm.Controls.Add(this.lblEletricidade);
            this.pnlForm.Controls.Add(this.grbPeriodicidade);
            this.pnlForm.Controls.Add(this.btExcluirExcecao);
            this.pnlForm.Controls.Add(this.grbExcecao);
            this.pnlForm.Controls.Add(this.btIncluirExcecao);
            this.pnlForm.Controls.Add(this.cbEspaco);
            this.pnlForm.Controls.Add(this.cbAltura);
            this.pnlForm.Controls.Add(this.lblEspaco);
            this.pnlForm.Controls.Add(this.lblAltura);
            this.pnlForm.Controls.Add(this.textIdade);
            this.pnlForm.Controls.Add(this.llbIdade);
            this.pnlForm.Controls.Add(this.lblExame);
            this.pnlForm.Controls.Add(this.lblGhe);
            this.pnlForm.Controls.Add(this.textGhe);
            this.pnlForm.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.pnlForm.Size = new System.Drawing.Size(514, 580);
            // 
            // flpAcao
            // 
            this.flpAcao.Controls.Add(this.btIncluir);
            this.flpAcao.Controls.Add(this.btFechar);
            this.flpAcao.Location = new System.Drawing.Point(0, 0);
            this.flpAcao.Name = "flpAcao";
            this.flpAcao.Size = new System.Drawing.Size(534, 33);
            this.flpAcao.TabIndex = 0;
            // 
            // btIncluir
            // 
            this.btIncluir.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btIncluir.Image = global::SWS.Properties.Resources.Gravar;
            this.btIncluir.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btIncluir.Location = new System.Drawing.Point(3, 3);
            this.btIncluir.Name = "btIncluir";
            this.btIncluir.Size = new System.Drawing.Size(75, 23);
            this.btIncluir.TabIndex = 6;
            this.btIncluir.Text = "&Gravar";
            this.btIncluir.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btIncluir.UseVisualStyleBackColor = true;
            this.btIncluir.Click += new System.EventHandler(this.btIncluir_Click);
            // 
            // btFechar
            // 
            this.btFechar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btFechar.Image = global::SWS.Properties.Resources.close;
            this.btFechar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btFechar.Location = new System.Drawing.Point(84, 3);
            this.btFechar.Name = "btFechar";
            this.btFechar.Size = new System.Drawing.Size(75, 23);
            this.btFechar.TabIndex = 7;
            this.btFechar.Text = "&Fechar";
            this.btFechar.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btFechar.UseVisualStyleBackColor = true;
            this.btFechar.Click += new System.EventHandler(this.btFechar_Click);
            // 
            // btExame
            // 
            this.btExame.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btExame.Image = global::SWS.Properties.Resources.busca;
            this.btExame.Location = new System.Drawing.Point(479, 29);
            this.btExame.Name = "btExame";
            this.btExame.Size = new System.Drawing.Size(29, 21);
            this.btExame.TabIndex = 2;
            this.btExame.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btExame.UseVisualStyleBackColor = true;
            this.btExame.Click += new System.EventHandler(this.btExame_Click);
            // 
            // textGhe
            // 
            this.textGhe.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.textGhe.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textGhe.Enabled = false;
            this.textGhe.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textGhe.ForeColor = System.Drawing.Color.Black;
            this.textGhe.Location = new System.Drawing.Point(120, 9);
            this.textGhe.MaxLength = 100;
            this.textGhe.Name = "textGhe";
            this.textGhe.ReadOnly = true;
            this.textGhe.Size = new System.Drawing.Size(388, 21);
            this.textGhe.TabIndex = 19;
            // 
            // textExame
            // 
            this.textExame.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.textExame.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textExame.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textExame.ForeColor = System.Drawing.SystemColors.MenuHighlight;
            this.textExame.Location = new System.Drawing.Point(120, 29);
            this.textExame.MaxLength = 100;
            this.textExame.Name = "textExame";
            this.textExame.ReadOnly = true;
            this.textExame.Size = new System.Drawing.Size(361, 21);
            this.textExame.TabIndex = 15;
            this.textExame.TabStop = false;
            // 
            // lblGhe
            // 
            this.lblGhe.BackColor = System.Drawing.Color.Silver;
            this.lblGhe.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblGhe.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblGhe.Location = new System.Drawing.Point(3, 9);
            this.lblGhe.Name = "lblGhe";
            this.lblGhe.ReadOnly = true;
            this.lblGhe.Size = new System.Drawing.Size(118, 21);
            this.lblGhe.TabIndex = 21;
            this.lblGhe.TabStop = false;
            this.lblGhe.Text = "GHE";
            // 
            // lblExame
            // 
            this.lblExame.BackColor = System.Drawing.Color.Silver;
            this.lblExame.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblExame.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblExame.Location = new System.Drawing.Point(3, 29);
            this.lblExame.Name = "lblExame";
            this.lblExame.ReadOnly = true;
            this.lblExame.Size = new System.Drawing.Size(118, 21);
            this.lblExame.TabIndex = 22;
            this.lblExame.TabStop = false;
            this.lblExame.Text = "Exame";
            // 
            // llbIdade
            // 
            this.llbIdade.BackColor = System.Drawing.Color.Silver;
            this.llbIdade.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.llbIdade.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.llbIdade.Location = new System.Drawing.Point(3, 49);
            this.llbIdade.Name = "llbIdade";
            this.llbIdade.ReadOnly = true;
            this.llbIdade.Size = new System.Drawing.Size(118, 21);
            this.llbIdade.TabIndex = 23;
            this.llbIdade.TabStop = false;
            this.llbIdade.Text = "Idade mínima";
            // 
            // textIdade
            // 
            this.textIdade.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textIdade.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textIdade.ForeColor = System.Drawing.Color.Blue;
            this.textIdade.Location = new System.Drawing.Point(120, 49);
            this.textIdade.MaxLength = 3;
            this.textIdade.Name = "textIdade";
            this.textIdade.Size = new System.Drawing.Size(388, 21);
            this.textIdade.TabIndex = 24;
            this.textIdade.Text = "0";
            this.textIdade.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.textIdade_KeyPress);
            // 
            // lblAltura
            // 
            this.lblAltura.BackColor = System.Drawing.Color.Silver;
            this.lblAltura.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblAltura.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblAltura.Location = new System.Drawing.Point(3, 68);
            this.lblAltura.Name = "lblAltura";
            this.lblAltura.ReadOnly = true;
            this.lblAltura.Size = new System.Drawing.Size(118, 21);
            this.lblAltura.TabIndex = 25;
            this.lblAltura.TabStop = false;
            this.lblAltura.Text = "Trabalho em altura";
            // 
            // lblEspaco
            // 
            this.lblEspaco.BackColor = System.Drawing.Color.Silver;
            this.lblEspaco.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblEspaco.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblEspaco.Location = new System.Drawing.Point(3, 87);
            this.lblEspaco.Name = "lblEspaco";
            this.lblEspaco.ReadOnly = true;
            this.lblEspaco.Size = new System.Drawing.Size(118, 21);
            this.lblEspaco.TabIndex = 26;
            this.lblEspaco.TabStop = false;
            this.lblEspaco.Text = "Espaço confinado";
            // 
            // cbAltura
            // 
            this.cbAltura.BorderColor = System.Drawing.Color.DimGray;
            this.cbAltura.BorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid;
            this.cbAltura.FormattingEnabled = true;
            this.cbAltura.Location = new System.Drawing.Point(120, 68);
            this.cbAltura.Name = "cbAltura";
            this.cbAltura.Size = new System.Drawing.Size(388, 21);
            this.cbAltura.TabIndex = 27;
            // 
            // cbEspaco
            // 
            this.cbEspaco.BorderColor = System.Drawing.Color.DimGray;
            this.cbEspaco.BorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid;
            this.cbEspaco.FormattingEnabled = true;
            this.cbEspaco.Location = new System.Drawing.Point(120, 87);
            this.cbEspaco.Name = "cbEspaco";
            this.cbEspaco.Size = new System.Drawing.Size(388, 21);
            this.cbEspaco.TabIndex = 28;
            // 
            // btExcluirExcecao
            // 
            this.btExcluirExcecao.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btExcluirExcecao.Image = global::SWS.Properties.Resources.lixeira;
            this.btExcluirExcecao.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btExcluirExcecao.Location = new System.Drawing.Point(84, 237);
            this.btExcluirExcecao.Name = "btExcluirExcecao";
            this.btExcluirExcecao.Size = new System.Drawing.Size(75, 23);
            this.btExcluirExcecao.TabIndex = 31;
            this.btExcluirExcecao.Text = "&Excluir";
            this.btExcluirExcecao.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btExcluirExcecao.UseVisualStyleBackColor = true;
            this.btExcluirExcecao.Click += new System.EventHandler(this.btExcluirExcecao_Click);
            // 
            // grbExcecao
            // 
            this.grbExcecao.Controls.Add(this.dgvExcecao);
            this.grbExcecao.Location = new System.Drawing.Point(3, 145);
            this.grbExcecao.Name = "grbExcecao";
            this.grbExcecao.Size = new System.Drawing.Size(505, 84);
            this.grbExcecao.TabIndex = 29;
            this.grbExcecao.TabStop = false;
            this.grbExcecao.Text = "Idade mínima para realização do exame por Função no GHE";
            // 
            // dgvExcecao
            // 
            this.dgvExcecao.AllowUserToAddRows = false;
            this.dgvExcecao.AllowUserToDeleteRows = false;
            this.dgvExcecao.AllowUserToOrderColumns = true;
            this.dgvExcecao.AllowUserToResizeRows = false;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.Gainsboro;
            this.dgvExcecao.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle1;
            this.dgvExcecao.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this.dgvExcecao.BackgroundColor = System.Drawing.Color.White;
            this.dgvExcecao.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.Color.LightGray;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvExcecao.DefaultCellStyle = dataGridViewCellStyle2;
            this.dgvExcecao.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvExcecao.Location = new System.Drawing.Point(3, 16);
            this.dgvExcecao.Name = "dgvExcecao";
            this.dgvExcecao.ReadOnly = true;
            this.dgvExcecao.Size = new System.Drawing.Size(499, 65);
            this.dgvExcecao.TabIndex = 3;
            // 
            // btIncluirExcecao
            // 
            this.btIncluirExcecao.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btIncluirExcecao.Image = global::SWS.Properties.Resources.fechar_ico;
            this.btIncluirExcecao.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btIncluirExcecao.Location = new System.Drawing.Point(6, 237);
            this.btIncluirExcecao.Name = "btIncluirExcecao";
            this.btIncluirExcecao.Size = new System.Drawing.Size(75, 23);
            this.btIncluirExcecao.TabIndex = 30;
            this.btIncluirExcecao.Text = "&Incluir";
            this.btIncluirExcecao.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btIncluirExcecao.UseVisualStyleBackColor = true;
            this.btIncluirExcecao.Click += new System.EventHandler(this.btIncluirExcecao_Click);
            // 
            // grbPeriodicidade
            // 
            this.grbPeriodicidade.Controls.Add(this.dgvPeriodicidade);
            this.grbPeriodicidade.Location = new System.Drawing.Point(4, 265);
            this.grbPeriodicidade.Name = "grbPeriodicidade";
            this.grbPeriodicidade.Size = new System.Drawing.Size(507, 223);
            this.grbPeriodicidade.TabIndex = 32;
            this.grbPeriodicidade.TabStop = false;
            this.grbPeriodicidade.Text = "Periodicidade do Exame";
            // 
            // dgvPeriodicidade
            // 
            this.dgvPeriodicidade.AllowUserToAddRows = false;
            this.dgvPeriodicidade.AllowUserToDeleteRows = false;
            this.dgvPeriodicidade.AllowUserToOrderColumns = true;
            this.dgvPeriodicidade.AllowUserToResizeRows = false;
            this.dgvPeriodicidade.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this.dgvPeriodicidade.BackgroundColor = System.Drawing.Color.White;
            this.dgvPeriodicidade.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvPeriodicidade.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvPeriodicidade.Location = new System.Drawing.Point(3, 16);
            this.dgvPeriodicidade.Name = "dgvPeriodicidade";
            this.dgvPeriodicidade.RowHeadersVisible = false;
            this.dgvPeriodicidade.Size = new System.Drawing.Size(501, 204);
            this.dgvPeriodicidade.TabIndex = 3;
            // 
            // cbEletricidade
            // 
            this.cbEletricidade.BorderColor = System.Drawing.Color.DimGray;
            this.cbEletricidade.BorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid;
            this.cbEletricidade.FormattingEnabled = true;
            this.cbEletricidade.Location = new System.Drawing.Point(120, 105);
            this.cbEletricidade.Name = "cbEletricidade";
            this.cbEletricidade.Size = new System.Drawing.Size(388, 21);
            this.cbEletricidade.TabIndex = 34;
            // 
            // lblEletricidade
            // 
            this.lblEletricidade.BackColor = System.Drawing.Color.Silver;
            this.lblEletricidade.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblEletricidade.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblEletricidade.Location = new System.Drawing.Point(3, 105);
            this.lblEletricidade.Name = "lblEletricidade";
            this.lblEletricidade.ReadOnly = true;
            this.lblEletricidade.Size = new System.Drawing.Size(118, 21);
            this.lblEletricidade.TabIndex = 33;
            this.lblEletricidade.TabStop = false;
            this.lblEletricidade.Text = "Serv. em Eletricidade";
            // 
            // frmPcmsoExamesIncluir
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.ClientSize = new System.Drawing.Size(534, 569);
            this.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.Name = "frmPcmsoExamesIncluir";
            this.Text = "INCLUIR EXAMES";
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.frmPcmsoExamesIncluir_KeyDown);
            this.scForm.Panel1.ResumeLayout(false);
            this.scForm.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.scForm)).EndInit();
            this.scForm.ResumeLayout(false);
            this.pnlForm.ResumeLayout(false);
            this.pnlForm.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.banner)).EndInit();
            this.flpAcao.ResumeLayout(false);
            this.grbExcecao.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvExcecao)).EndInit();
            this.grbPeriodicidade.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvPeriodicidade)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        protected System.Windows.Forms.FlowLayoutPanel flpAcao;
        protected System.Windows.Forms.Button btExame;
        protected System.Windows.Forms.Button btIncluir;
        protected System.Windows.Forms.Button btFechar;
        protected System.Windows.Forms.TextBox lblExame;
        protected System.Windows.Forms.TextBox lblGhe;
        protected System.Windows.Forms.TextBox textGhe;
        protected System.Windows.Forms.TextBox textExame;
        protected System.Windows.Forms.TextBox llbIdade;
        protected System.Windows.Forms.TextBox textIdade;
        protected System.Windows.Forms.TextBox lblEspaco;
        protected System.Windows.Forms.TextBox lblAltura;
        protected ComboBoxWithBorder cbEspaco;
        protected ComboBoxWithBorder cbAltura;
        protected System.Windows.Forms.Button btExcluirExcecao;
        protected System.Windows.Forms.GroupBox grbExcecao;
        protected System.Windows.Forms.DataGridView dgvExcecao;
        protected System.Windows.Forms.Button btIncluirExcecao;
        protected System.Windows.Forms.GroupBox grbPeriodicidade;
        protected System.Windows.Forms.DataGridView dgvPeriodicidade;
        protected ComboBoxWithBorder cbEletricidade;
        protected System.Windows.Forms.TextBox lblEletricidade;

    }
}
