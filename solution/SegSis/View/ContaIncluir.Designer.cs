﻿namespace SWS.View
{
    partial class frmContaIncluir
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.flpAcao = new System.Windows.Forms.FlowLayoutPanel();
            this.btnFechar = new System.Windows.Forms.Button();
            this.btnGravar = new System.Windows.Forms.Button();
            this.btnLimpar = new System.Windows.Forms.Button();
            this.lblBanco = new System.Windows.Forms.TextBox();
            this.lblNome = new System.Windows.Forms.TextBox();
            this.lblRegistro = new System.Windows.Forms.TextBox();
            this.lblAgencia = new System.Windows.Forms.TextBox();
            this.lblDigito = new System.Windows.Forms.TextBox();
            this.lblConta = new System.Windows.Forms.TextBox();
            this.lblDígitoConta = new System.Windows.Forms.TextBox();
            this.lblNossoNúmero = new System.Windows.Forms.TextBox();
            this.lblRemessa = new System.Windows.Forms.TextBox();
            this.lblCarteira = new System.Windows.Forms.TextBox();
            this.lblVariacao = new System.Windows.Forms.TextBox();
            this.lblConvenio = new System.Windows.Forms.TextBox();
            this.lblCodigoCedente = new System.Windows.Forms.TextBox();
            this.textBanco = new System.Windows.Forms.TextBox();
            this.btnBanco = new System.Windows.Forms.Button();
            this.textNome = new System.Windows.Forms.TextBox();
            this.cbRegistro = new SWS.ComboBoxWithBorder();
            this.textAgencia = new System.Windows.Forms.TextBox();
            this.textDigitoAgencia = new System.Windows.Forms.TextBox();
            this.textNumeroConta = new System.Windows.Forms.TextBox();
            this.textDigitoConta = new System.Windows.Forms.TextBox();
            this.textNossoNumero = new System.Windows.Forms.TextBox();
            this.textNumeroRemessa = new System.Windows.Forms.TextBox();
            this.textCarteira = new System.Windows.Forms.TextBox();
            this.textVariacaoCarteira = new System.Windows.Forms.TextBox();
            this.textNumeroConvenio = new System.Windows.Forms.TextBox();
            this.textCodigoCedente = new System.Windows.Forms.TextBox();
            ((System.ComponentModel.ISupportInitialize)(this.spForm)).BeginInit();
            this.spForm.Panel1.SuspendLayout();
            this.spForm.Panel2.SuspendLayout();
            this.spForm.SuspendLayout();
            this.pnlForm.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.banner)).BeginInit();
            this.flpAcao.SuspendLayout();
            this.SuspendLayout();
            // 
            // spForm
            // 
            // 
            // spForm.Panel1
            // 
            this.spForm.Panel1.Controls.Add(this.flpAcao);
            // 
            // pnlForm
            // 
            this.pnlForm.Controls.Add(this.textCodigoCedente);
            this.pnlForm.Controls.Add(this.textNumeroConvenio);
            this.pnlForm.Controls.Add(this.textVariacaoCarteira);
            this.pnlForm.Controls.Add(this.textCarteira);
            this.pnlForm.Controls.Add(this.textNumeroRemessa);
            this.pnlForm.Controls.Add(this.textNossoNumero);
            this.pnlForm.Controls.Add(this.textDigitoConta);
            this.pnlForm.Controls.Add(this.textNumeroConta);
            this.pnlForm.Controls.Add(this.textDigitoAgencia);
            this.pnlForm.Controls.Add(this.textAgencia);
            this.pnlForm.Controls.Add(this.cbRegistro);
            this.pnlForm.Controls.Add(this.textNome);
            this.pnlForm.Controls.Add(this.btnBanco);
            this.pnlForm.Controls.Add(this.textBanco);
            this.pnlForm.Controls.Add(this.lblCodigoCedente);
            this.pnlForm.Controls.Add(this.lblConvenio);
            this.pnlForm.Controls.Add(this.lblVariacao);
            this.pnlForm.Controls.Add(this.lblCarteira);
            this.pnlForm.Controls.Add(this.lblRemessa);
            this.pnlForm.Controls.Add(this.lblNossoNúmero);
            this.pnlForm.Controls.Add(this.lblDígitoConta);
            this.pnlForm.Controls.Add(this.lblConta);
            this.pnlForm.Controls.Add(this.lblDigito);
            this.pnlForm.Controls.Add(this.lblAgencia);
            this.pnlForm.Controls.Add(this.lblRegistro);
            this.pnlForm.Controls.Add(this.lblNome);
            this.pnlForm.Controls.Add(this.lblBanco);
            // 
            // flpAcao
            // 
            this.flpAcao.Controls.Add(this.btnGravar);
            this.flpAcao.Controls.Add(this.btnLimpar);
            this.flpAcao.Controls.Add(this.btnFechar);
            this.flpAcao.Dock = System.Windows.Forms.DockStyle.Fill;
            this.flpAcao.Location = new System.Drawing.Point(0, 0);
            this.flpAcao.Name = "flpAcao";
            this.flpAcao.Size = new System.Drawing.Size(784, 35);
            this.flpAcao.TabIndex = 0;
            // 
            // btnFechar
            // 
            this.btnFechar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnFechar.Image = global::SWS.Properties.Resources.close;
            this.btnFechar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnFechar.Location = new System.Drawing.Point(165, 3);
            this.btnFechar.Name = "btnFechar";
            this.btnFechar.Size = new System.Drawing.Size(75, 23);
            this.btnFechar.TabIndex = 17;
            this.btnFechar.Text = "&Fechar";
            this.btnFechar.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnFechar.UseVisualStyleBackColor = true;
            this.btnFechar.Click += new System.EventHandler(this.btnFechar_Click);
            // 
            // btnGravar
            // 
            this.btnGravar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnGravar.Image = global::SWS.Properties.Resources.fechar_ico;
            this.btnGravar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnGravar.Location = new System.Drawing.Point(3, 3);
            this.btnGravar.Name = "btnGravar";
            this.btnGravar.Size = new System.Drawing.Size(75, 23);
            this.btnGravar.TabIndex = 15;
            this.btnGravar.Text = "&Gravar";
            this.btnGravar.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnGravar.UseVisualStyleBackColor = true;
            this.btnGravar.Click += new System.EventHandler(this.btnGravar_Click);
            // 
            // btnLimpar
            // 
            this.btnLimpar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnLimpar.Image = global::SWS.Properties.Resources.vassoura;
            this.btnLimpar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnLimpar.Location = new System.Drawing.Point(84, 3);
            this.btnLimpar.Name = "btnLimpar";
            this.btnLimpar.Size = new System.Drawing.Size(75, 23);
            this.btnLimpar.TabIndex = 16;
            this.btnLimpar.TabStop = false;
            this.btnLimpar.Text = "&Limpar";
            this.btnLimpar.UseVisualStyleBackColor = true;
            this.btnLimpar.Click += new System.EventHandler(this.btnLimpar_Click);
            // 
            // lblBanco
            // 
            this.lblBanco.BackColor = System.Drawing.Color.LightGray;
            this.lblBanco.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblBanco.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblBanco.Location = new System.Drawing.Point(12, 18);
            this.lblBanco.MaxLength = 100;
            this.lblBanco.Name = "lblBanco";
            this.lblBanco.ReadOnly = true;
            this.lblBanco.Size = new System.Drawing.Size(128, 21);
            this.lblBanco.TabIndex = 1;
            this.lblBanco.TabStop = false;
            this.lblBanco.Text = "Banco";
            // 
            // lblNome
            // 
            this.lblNome.BackColor = System.Drawing.Color.LightGray;
            this.lblNome.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblNome.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNome.Location = new System.Drawing.Point(12, 38);
            this.lblNome.MaxLength = 100;
            this.lblNome.Name = "lblNome";
            this.lblNome.ReadOnly = true;
            this.lblNome.Size = new System.Drawing.Size(128, 21);
            this.lblNome.TabIndex = 2;
            this.lblNome.TabStop = false;
            this.lblNome.Text = "Nome";
            // 
            // lblRegistro
            // 
            this.lblRegistro.BackColor = System.Drawing.Color.LightGray;
            this.lblRegistro.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblRegistro.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblRegistro.Location = new System.Drawing.Point(12, 58);
            this.lblRegistro.MaxLength = 100;
            this.lblRegistro.Name = "lblRegistro";
            this.lblRegistro.ReadOnly = true;
            this.lblRegistro.Size = new System.Drawing.Size(128, 21);
            this.lblRegistro.TabIndex = 3;
            this.lblRegistro.TabStop = false;
            this.lblRegistro.Text = "Registro";
            // 
            // lblAgencia
            // 
            this.lblAgencia.BackColor = System.Drawing.Color.LightGray;
            this.lblAgencia.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblAgencia.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblAgencia.Location = new System.Drawing.Point(12, 78);
            this.lblAgencia.MaxLength = 100;
            this.lblAgencia.Name = "lblAgencia";
            this.lblAgencia.ReadOnly = true;
            this.lblAgencia.Size = new System.Drawing.Size(128, 21);
            this.lblAgencia.TabIndex = 4;
            this.lblAgencia.TabStop = false;
            this.lblAgencia.Text = "Agência";
            // 
            // lblDigito
            // 
            this.lblDigito.BackColor = System.Drawing.Color.LightGray;
            this.lblDigito.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblDigito.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDigito.Location = new System.Drawing.Point(12, 98);
            this.lblDigito.MaxLength = 100;
            this.lblDigito.Name = "lblDigito";
            this.lblDigito.ReadOnly = true;
            this.lblDigito.Size = new System.Drawing.Size(128, 21);
            this.lblDigito.TabIndex = 5;
            this.lblDigito.TabStop = false;
            this.lblDigito.Text = "Dígito da Agência";
            // 
            // lblConta
            // 
            this.lblConta.BackColor = System.Drawing.Color.LightGray;
            this.lblConta.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblConta.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblConta.Location = new System.Drawing.Point(12, 118);
            this.lblConta.MaxLength = 100;
            this.lblConta.Name = "lblConta";
            this.lblConta.ReadOnly = true;
            this.lblConta.Size = new System.Drawing.Size(128, 21);
            this.lblConta.TabIndex = 6;
            this.lblConta.TabStop = false;
            this.lblConta.Text = "Número da Conta";
            // 
            // lblDígitoConta
            // 
            this.lblDígitoConta.BackColor = System.Drawing.Color.LightGray;
            this.lblDígitoConta.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblDígitoConta.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDígitoConta.Location = new System.Drawing.Point(12, 138);
            this.lblDígitoConta.MaxLength = 100;
            this.lblDígitoConta.Name = "lblDígitoConta";
            this.lblDígitoConta.ReadOnly = true;
            this.lblDígitoConta.Size = new System.Drawing.Size(128, 21);
            this.lblDígitoConta.TabIndex = 7;
            this.lblDígitoConta.TabStop = false;
            this.lblDígitoConta.Text = "Dígito da Conta";
            // 
            // lblNossoNúmero
            // 
            this.lblNossoNúmero.BackColor = System.Drawing.Color.LightGray;
            this.lblNossoNúmero.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblNossoNúmero.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNossoNúmero.Location = new System.Drawing.Point(12, 158);
            this.lblNossoNúmero.MaxLength = 100;
            this.lblNossoNúmero.Name = "lblNossoNúmero";
            this.lblNossoNúmero.ReadOnly = true;
            this.lblNossoNúmero.Size = new System.Drawing.Size(128, 21);
            this.lblNossoNúmero.TabIndex = 8;
            this.lblNossoNúmero.TabStop = false;
            this.lblNossoNúmero.Text = "Nosso número";
            // 
            // lblRemessa
            // 
            this.lblRemessa.BackColor = System.Drawing.Color.LightGray;
            this.lblRemessa.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblRemessa.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblRemessa.Location = new System.Drawing.Point(12, 178);
            this.lblRemessa.MaxLength = 100;
            this.lblRemessa.Name = "lblRemessa";
            this.lblRemessa.ReadOnly = true;
            this.lblRemessa.Size = new System.Drawing.Size(128, 21);
            this.lblRemessa.TabIndex = 9;
            this.lblRemessa.TabStop = false;
            this.lblRemessa.Text = "Número remessa";
            // 
            // lblCarteira
            // 
            this.lblCarteira.BackColor = System.Drawing.Color.LightGray;
            this.lblCarteira.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblCarteira.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCarteira.Location = new System.Drawing.Point(12, 198);
            this.lblCarteira.MaxLength = 100;
            this.lblCarteira.Name = "lblCarteira";
            this.lblCarteira.ReadOnly = true;
            this.lblCarteira.Size = new System.Drawing.Size(128, 21);
            this.lblCarteira.TabIndex = 10;
            this.lblCarteira.TabStop = false;
            this.lblCarteira.Text = "Carteira";
            // 
            // lblVariacao
            // 
            this.lblVariacao.BackColor = System.Drawing.Color.LightGray;
            this.lblVariacao.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblVariacao.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblVariacao.Location = new System.Drawing.Point(12, 218);
            this.lblVariacao.MaxLength = 100;
            this.lblVariacao.Name = "lblVariacao";
            this.lblVariacao.ReadOnly = true;
            this.lblVariacao.Size = new System.Drawing.Size(128, 21);
            this.lblVariacao.TabIndex = 11;
            this.lblVariacao.TabStop = false;
            this.lblVariacao.Text = "Variação";
            // 
            // lblConvenio
            // 
            this.lblConvenio.BackColor = System.Drawing.Color.LightGray;
            this.lblConvenio.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblConvenio.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblConvenio.Location = new System.Drawing.Point(12, 238);
            this.lblConvenio.MaxLength = 100;
            this.lblConvenio.Name = "lblConvenio";
            this.lblConvenio.ReadOnly = true;
            this.lblConvenio.Size = new System.Drawing.Size(128, 21);
            this.lblConvenio.TabIndex = 12;
            this.lblConvenio.TabStop = false;
            this.lblConvenio.Text = "Número de convênio";
            // 
            // lblCodigoCedente
            // 
            this.lblCodigoCedente.BackColor = System.Drawing.Color.LightGray;
            this.lblCodigoCedente.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblCodigoCedente.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCodigoCedente.Location = new System.Drawing.Point(12, 258);
            this.lblCodigoCedente.MaxLength = 100;
            this.lblCodigoCedente.Name = "lblCodigoCedente";
            this.lblCodigoCedente.ReadOnly = true;
            this.lblCodigoCedente.Size = new System.Drawing.Size(128, 21);
            this.lblCodigoCedente.TabIndex = 13;
            this.lblCodigoCedente.TabStop = false;
            this.lblCodigoCedente.Text = "Código de cedente";
            // 
            // textBanco
            // 
            this.textBanco.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.textBanco.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBanco.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBanco.Location = new System.Drawing.Point(139, 18);
            this.textBanco.MaxLength = 50;
            this.textBanco.Name = "textBanco";
            this.textBanco.ReadOnly = true;
            this.textBanco.Size = new System.Drawing.Size(586, 21);
            this.textBanco.TabIndex = 14;
            this.textBanco.TabStop = false;
            // 
            // btnBanco
            // 
            this.btnBanco.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnBanco.Image = global::SWS.Properties.Resources.busca;
            this.btnBanco.Location = new System.Drawing.Point(722, 18);
            this.btnBanco.Name = "btnBanco";
            this.btnBanco.Size = new System.Drawing.Size(34, 21);
            this.btnBanco.TabIndex = 15;
            this.btnBanco.TabStop = false;
            this.btnBanco.UseVisualStyleBackColor = true;
            this.btnBanco.Click += new System.EventHandler(this.btnBanco_Click);
            // 
            // textNome
            // 
            this.textNome.BackColor = System.Drawing.Color.White;
            this.textNome.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textNome.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textNome.Location = new System.Drawing.Point(139, 38);
            this.textNome.MaxLength = 50;
            this.textNome.Name = "textNome";
            this.textNome.Size = new System.Drawing.Size(617, 21);
            this.textNome.TabIndex = 1;
            // 
            // cbRegistro
            // 
            this.cbRegistro.BackColor = System.Drawing.Color.LightGray;
            this.cbRegistro.BorderColor = System.Drawing.Color.DimGray;
            this.cbRegistro.BorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid;
            this.cbRegistro.FormattingEnabled = true;
            this.cbRegistro.Location = new System.Drawing.Point(139, 58);
            this.cbRegistro.Name = "cbRegistro";
            this.cbRegistro.Size = new System.Drawing.Size(617, 21);
            this.cbRegistro.TabIndex = 2;
            // 
            // textAgencia
            // 
            this.textAgencia.BackColor = System.Drawing.Color.White;
            this.textAgencia.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textAgencia.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textAgencia.Location = new System.Drawing.Point(139, 78);
            this.textAgencia.MaxLength = 5;
            this.textAgencia.Name = "textAgencia";
            this.textAgencia.Size = new System.Drawing.Size(617, 21);
            this.textAgencia.TabIndex = 3;
            this.textAgencia.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.textAgencia_KeyPress);
            // 
            // textDigitoAgencia
            // 
            this.textDigitoAgencia.BackColor = System.Drawing.Color.White;
            this.textDigitoAgencia.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textDigitoAgencia.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textDigitoAgencia.Location = new System.Drawing.Point(139, 98);
            this.textDigitoAgencia.MaxLength = 1;
            this.textDigitoAgencia.Name = "textDigitoAgencia";
            this.textDigitoAgencia.Size = new System.Drawing.Size(617, 21);
            this.textDigitoAgencia.TabIndex = 4;
            this.textDigitoAgencia.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.textDigitoAgencia_KeyPress);
            // 
            // textNumeroConta
            // 
            this.textNumeroConta.BackColor = System.Drawing.Color.White;
            this.textNumeroConta.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textNumeroConta.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textNumeroConta.Location = new System.Drawing.Point(139, 118);
            this.textNumeroConta.MaxLength = 12;
            this.textNumeroConta.Name = "textNumeroConta";
            this.textNumeroConta.Size = new System.Drawing.Size(617, 21);
            this.textNumeroConta.TabIndex = 5;
            this.textNumeroConta.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.textNumeroConta_KeyPress);
            // 
            // textDigitoConta
            // 
            this.textDigitoConta.BackColor = System.Drawing.Color.White;
            this.textDigitoConta.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textDigitoConta.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textDigitoConta.Location = new System.Drawing.Point(139, 138);
            this.textDigitoConta.MaxLength = 1;
            this.textDigitoConta.Name = "textDigitoConta";
            this.textDigitoConta.Size = new System.Drawing.Size(617, 21);
            this.textDigitoConta.TabIndex = 6;
            this.textDigitoConta.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.textDigitoConta_KeyPress);
            // 
            // textNossoNumero
            // 
            this.textNossoNumero.BackColor = System.Drawing.Color.White;
            this.textNossoNumero.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textNossoNumero.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textNossoNumero.Location = new System.Drawing.Point(139, 158);
            this.textNossoNumero.MaxLength = 12;
            this.textNossoNumero.Name = "textNossoNumero";
            this.textNossoNumero.Size = new System.Drawing.Size(617, 21);
            this.textNossoNumero.TabIndex = 7;
            this.textNossoNumero.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.textNossoNumero_KeyPress);
            // 
            // textNumeroRemessa
            // 
            this.textNumeroRemessa.BackColor = System.Drawing.Color.White;
            this.textNumeroRemessa.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textNumeroRemessa.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textNumeroRemessa.Location = new System.Drawing.Point(139, 178);
            this.textNumeroRemessa.MaxLength = 7;
            this.textNumeroRemessa.Name = "textNumeroRemessa";
            this.textNumeroRemessa.Size = new System.Drawing.Size(617, 21);
            this.textNumeroRemessa.TabIndex = 8;
            this.textNumeroRemessa.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.textNumeroRemessa_KeyPress);
            // 
            // textCarteira
            // 
            this.textCarteira.BackColor = System.Drawing.Color.White;
            this.textCarteira.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textCarteira.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textCarteira.Location = new System.Drawing.Point(139, 198);
            this.textCarteira.MaxLength = 3;
            this.textCarteira.Name = "textCarteira";
            this.textCarteira.Size = new System.Drawing.Size(617, 21);
            this.textCarteira.TabIndex = 9;
            this.textCarteira.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.textCarteira_KeyPress);
            // 
            // textVariacaoCarteira
            // 
            this.textVariacaoCarteira.BackColor = System.Drawing.Color.White;
            this.textVariacaoCarteira.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textVariacaoCarteira.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textVariacaoCarteira.Location = new System.Drawing.Point(139, 218);
            this.textVariacaoCarteira.MaxLength = 3;
            this.textVariacaoCarteira.Name = "textVariacaoCarteira";
            this.textVariacaoCarteira.Size = new System.Drawing.Size(617, 21);
            this.textVariacaoCarteira.TabIndex = 10;
            this.textVariacaoCarteira.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.textVariacaoCarteira_KeyPress);
            // 
            // textNumeroConvenio
            // 
            this.textNumeroConvenio.BackColor = System.Drawing.Color.White;
            this.textNumeroConvenio.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textNumeroConvenio.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textNumeroConvenio.Location = new System.Drawing.Point(139, 238);
            this.textNumeroConvenio.MaxLength = 16;
            this.textNumeroConvenio.Name = "textNumeroConvenio";
            this.textNumeroConvenio.Size = new System.Drawing.Size(617, 21);
            this.textNumeroConvenio.TabIndex = 11;
            this.textNumeroConvenio.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.textNumeroConvenio_KeyPress);
            // 
            // textCodigoCedente
            // 
            this.textCodigoCedente.BackColor = System.Drawing.Color.White;
            this.textCodigoCedente.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textCodigoCedente.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textCodigoCedente.Location = new System.Drawing.Point(139, 258);
            this.textCodigoCedente.MaxLength = 20;
            this.textCodigoCedente.Name = "textCodigoCedente";
            this.textCodigoCedente.Size = new System.Drawing.Size(617, 21);
            this.textCodigoCedente.TabIndex = 12;
            this.textCodigoCedente.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.textCodigoCedente_KeyPress);
            // 
            // frmContaIncluir
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(784, 561);
            this.Name = "frmContaIncluir";
            this.Text = "INCLUIR CONTA COBRANÇA";
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.frmContaIncluir_KeyDown);
            this.spForm.Panel1.ResumeLayout(false);
            this.spForm.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.spForm)).EndInit();
            this.spForm.ResumeLayout(false);
            this.pnlForm.ResumeLayout(false);
            this.pnlForm.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.banner)).EndInit();
            this.flpAcao.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        protected System.Windows.Forms.Button btnFechar;
        protected System.Windows.Forms.Button btnGravar;
        protected System.Windows.Forms.Button btnLimpar;
        protected System.Windows.Forms.TextBox lblNome;
        protected System.Windows.Forms.TextBox lblBanco;
        protected System.Windows.Forms.TextBox lblRegistro;
        protected System.Windows.Forms.TextBox lblDigito;
        protected System.Windows.Forms.TextBox lblAgencia;
        protected System.Windows.Forms.TextBox lblDígitoConta;
        protected System.Windows.Forms.TextBox lblConta;
        protected System.Windows.Forms.TextBox lblNossoNúmero;
        protected System.Windows.Forms.TextBox lblRemessa;
        protected System.Windows.Forms.TextBox lblVariacao;
        protected System.Windows.Forms.TextBox lblCarteira;
        protected System.Windows.Forms.TextBox lblConvenio;
        protected System.Windows.Forms.TextBox lblCodigoCedente;
        protected System.Windows.Forms.TextBox textCodigoCedente;
        protected System.Windows.Forms.TextBox textNumeroConvenio;
        protected System.Windows.Forms.TextBox textVariacaoCarteira;
        protected System.Windows.Forms.TextBox textCarteira;
        protected System.Windows.Forms.TextBox textNumeroRemessa;
        protected System.Windows.Forms.TextBox textNossoNumero;
        protected System.Windows.Forms.TextBox textDigitoConta;
        protected System.Windows.Forms.TextBox textNumeroConta;
        protected System.Windows.Forms.TextBox textDigitoAgencia;
        protected System.Windows.Forms.TextBox textAgencia;
        protected ComboBoxWithBorder cbRegistro;
        protected System.Windows.Forms.TextBox textNome;
        protected System.Windows.Forms.Button btnBanco;
        protected System.Windows.Forms.TextBox textBanco;
        protected System.Windows.Forms.FlowLayoutPanel flpAcao;
    }
}