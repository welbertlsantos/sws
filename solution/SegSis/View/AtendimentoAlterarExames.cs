﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using SWS.Entidade;
using SWS.View.Resources;
using SWS.Facade;

namespace SWS.View
{
    public partial class frmAtendimentoAlterarExames : frmTemplate
    {
        private List<ItemFilaAtendimento> examesCancelados;
        private List<GheFonteAgenteExame> examesPcmso;
        private List<ItemFilaAtendimento> examesAtual;
        private Periodicidade tipoAtendimento;
        private Aso atendimento;
        private bool alterado;

        public bool Alterado
        {
            get { return alterado; }
            set { alterado = value; }
        }
        
        public frmAtendimentoAlterarExames(List<ItemFilaAtendimento> examesCancelados, List<GheFonteAgenteExame> examesPcmso, Aso atendimento, List<ItemFilaAtendimento> examesAsoAtual, Periodicidade tipoAtendimento)
        {
            InitializeComponent();
            this.examesCancelados = examesCancelados;
            this.examesPcmso = examesPcmso;
            this.atendimento = atendimento;
            this.examesAtual = examesAsoAtual;
            this.tipoAtendimento = tipoAtendimento;

            montaGridExamesCancelados();
            montaGridExamesIncluidos();
        }

        private void montaGridExamesIncluidos()
        {
            try
            {
                dgvExamesPcmso.Columns.Clear();
                dgvExamesPcmso.ColumnCount = 4;

                dgvExamesPcmso.Columns[0].HeaderText = "idItem";
                dgvExamesPcmso.Columns[0].Visible = false;

                dgvExamesPcmso.Columns[1].HeaderText = "tipoItem";
                dgvExamesPcmso.Columns[1].Width = 100;

                dgvExamesPcmso.Columns[2].HeaderText = "Código";
                dgvExamesPcmso.Columns[2].Width = 50;

                dgvExamesPcmso.Columns[3].HeaderText = "Exame";
                dgvExamesPcmso.Columns[3].Width = 300;


                examesPcmso.ForEach(delegate(GheFonteAgenteExame gfae)
                {
                    dgvExamesPcmso.Rows.Add(gfae.Id, "PCMSO", gfae.Exame.Id.ToString().PadLeft(4, '0'), gfae.Exame.Descricao);
                });

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void montaGridExamesCancelados()
        {
            try
            {
                dgvExamesCancelados.Columns.Clear();
                dgvExamesCancelados.ColumnCount = 4;

                dgvExamesCancelados.Columns[0].HeaderText = "idItem";
                dgvExamesCancelados.Columns[0].Visible = false;

                dgvExamesCancelados.Columns[1].HeaderText = "TipoExame";
                dgvExamesCancelados.Columns[1].Width = 100;

                dgvExamesCancelados.Columns[2].HeaderText = "Código";
                dgvExamesCancelados.Columns[2].Width = 50;

                dgvExamesCancelados.Columns[3].HeaderText = "Exame";
                dgvExamesCancelados.Columns[3].Width = 300;

                examesCancelados.ForEach(delegate(ItemFilaAtendimento item)
                {
                    dgvExamesCancelados.Rows.Add(item.IdItem, string.Equals(ApplicationConstants.EXAME_EXTRA, item.TipoExame) ? "AVULSO" : "PCMSO", item.Exame.Id.ToString().PadLeft(4,'0'), item.Exame.Descricao);
                });

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btnCancelar_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnConfirmar_Click(object sender, EventArgs e)
        {
            try
            {
                /* verificando se pode ser prosseguido a alteração do tipo de atendimento */
                if (examesPcmso.Count > 0) throw new Exception("A alteração não poderá ser realizada porque existe exames que deverão ser incluídos para a troca do tipo de atendimento. Cancele todo o atendimento e faça novamente no tipo desejado.");

                /* verificando se todo o exame do atendimento serão cancelados. Avisar ao usuário que isso irá acontecer */

                FilaFacade filaFacade = FilaFacade.getInstance();
                AsoFacade asoFacade = AsoFacade.getInstance();
                if (examesCancelados.Count == examesAtual.Count)
                {
                    if (MessageBox.Show("Todos os exames do atendimento com a troca do tipo de atendimento serão cancelados. Gostaria de prosseguir com a solicitação de troca? NÃO esqueça de incluir novos exames.", "Atenção", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                    {
                        this.Cursor = Cursors.WaitCursor;
                        /* cancelando os exames do atendimento */
                        examesCancelados.ForEach(delegate(ItemFilaAtendimento item)
                        {
                            filaFacade.cancelaAtendimento(item, atendimento, true, string.Equals(item.TipoExame, ApplicationConstants.EXAME_PCMSO) ? new GheFonteAgenteExameAso(item.IdItem) : null, string.Equals(item.TipoExame, ApplicationConstants.EXAME_EXTRA) ? new ClienteFuncaoExameASo(item.IdItem) : null);
                        });

                    }
                    else return;
                   
                }
                else
                {
                    /* nem todos os exame do atendimento serão cancelados */
                    examesCancelados.ForEach(delegate(ItemFilaAtendimento item)
                    {
                        filaFacade.cancelaAtendimento(item, atendimento, true, string.Equals(item.TipoExame, ApplicationConstants.EXAME_PCMSO) ? new GheFonteAgenteExameAso(item.IdItem) : null, string.Equals(item.TipoExame, ApplicationConstants.EXAME_EXTRA) ? new ClienteFuncaoExameASo(item.IdItem) : null);
                    });
                }

                /* alterando o tipo do atendimento */
                atendimento.Periodicidade = tipoAtendimento;
                asoFacade.updateTipoAtendimentoByAtendimento(atendimento);
                MessageBox.Show("Tipo de atendimento alterado com sucesso", "Confirmação", MessageBoxButtons.OK, MessageBoxIcon.Information);
                alterado = true;
                this.Close();

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                this.Cursor = Cursors.Default;
                this.Close();
            }

        }


    }
}
