﻿using SWS.Entidade;
using SWS.Facade;
using SWS.View.Resources;
using SWS.View.ViewHelper;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SWS.View
{
    public partial class frmPcmsoGheIncluir : frmTemplateConsulta
    {

        protected Ghe ghe;

        public Ghe Ghe
        {
            get { return ghe; }
            set { ghe = value; }
        }
        
        protected Estudo pcmso;
        
        public frmPcmsoGheIncluir(Estudo pcmso)
        {
            InitializeComponent();
            this.grbNota.Enabled = false;
            this.flpNota.Enabled = false;
            this.pcmso = pcmso;
            ActiveControl = textDescricao;
        }

        public frmPcmsoGheIncluir()
        {
            InitializeComponent();
        }

        protected void textExposto_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = ValidaCampoHelper.FormataCampoNumerico(sender, e);
        }

        protected virtual void btnSalvar_Click(object sender, EventArgs e)
        {
            try
            {
                if (validaCampos())
                {
                    EstudoFacade pcmsoFacade = EstudoFacade.getInstance();
                    Ghe ghe = new Ghe();
                    ghe.Estudo = pcmso;
                    ghe.Descricao = textDescricao.Text.Trim();
                    ghe.Nexp = Convert.ToInt32(textExposto.Text);
                    ghe.Situacao = ApplicationConstants.ATIVO;
                    ghe.NumeroPo = textNumeroPo.Text.Trim();
                    
                    this.Ghe = pcmsoFacade.insertGhePcmso(ghe);
                    MessageBox.Show("GHE criado com sucesso.", "Confirmação", MessageBoxButtons.OK, MessageBoxIcon.Information);

                    grbNota.Enabled = true;
                    flpNota.Enabled = true;
                    btnSalvar.Enabled = false;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }

        protected void btnFechar_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        protected void btnNotaIncluir_Click(object sender, EventArgs e)
        {
            try
            {
                if (this.Ghe == null)
                    throw new Exception("É necessário incluir um GHE primeiro.");

                frmNotaSelecionar selecionarNota = new frmNotaSelecionar(this.Ghe);
                selecionarNota.ShowDialog();
                montaGrid();

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }

        protected void btnNotaExcluir_Click(object sender, EventArgs e)
        {
            try
            {
                if (dgvNota.CurrentRow == null)
                    throw new Exception("Selecione uma nota para excluir");

                EstudoFacade pcmsoFacade = EstudoFacade.getInstance();

                if (MessageBox.Show("Deseja excluir a nota selecionada", "Confirmação", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                {
                    Nota nota = new Nota();
                    nota.Id = (long)dgvNota.CurrentRow.Cells[0].Value;
                    nota.Descricao = dgvNota.CurrentRow.Cells[1].Value.ToString();
                    nota.Conteudo = dgvNota.CurrentRow.Cells[2].Value.ToString();
                    nota.Situacao = ApplicationConstants.ATIVO;

                    GheNota gheNota = new GheNota();
                    gheNota.Nota = nota;
                    gheNota.Ghe = ghe;

                    pcmsoFacade.deleteNotaGhe(gheNota);
                    montaGrid();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }

        protected bool validaCampos()
        {
            bool retorno = true;
            try
            {
                if (string.IsNullOrWhiteSpace(textDescricao.Text))
                    throw new Exception("Campo descrição é obrigatório");

                if (string.IsNullOrWhiteSpace(textExposto.Text))
                    throw new Exception("Campo obrigatório. Caso não tenha exposto insira 0.");

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
                retorno = false;
            }
            return retorno;
        }

        protected void montaGrid()
        {
            try
            {
                dgvNota.Columns.Clear();
                dgvNota.ColumnCount = 4;

                dgvNota.Columns[0].HeaderText = "idNorma";
                dgvNota.Columns[0].Visible = false;
                dgvNota.Columns[1].HeaderText = "IdGhe";
                dgvNota.Columns[1].Visible = false;
                dgvNota.Columns[2].HeaderText = "Título";
                dgvNota.Columns[3].HeaderText = "Conteúdo";

                EstudoFacade pcmsoFacade = EstudoFacade.getInstance();
                DataSet ds = pcmsoFacade.findAllNotasByGhe(ghe);

                foreach (DataRow row in ds.Tables[0].Rows)
                    dgvNota.Rows.Add(
                        row["ID_NORMA"],
                        ghe.Id,
                        row["DESCRICAO"].ToString(),
                        row["CONTEUDO"].ToString()
                        );
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }

        private void frmPcmsoGheIncluir_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter || e.KeyCode == Keys.Return)
                SendKeys.Send("{TAB}");
        }
    }
}
