﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using SWS.Entidade;
using SWS.View.Resources;
using SWS.Facade;

namespace SWS.View
{
    public partial class frm_DocumentoPesquisar : BaseFormConsulta
    {

        public static String msg1 = "Selecione uma linha.";

        LinkedList<Documento> documentos = new LinkedList<Documento>();
        
        public LinkedList<Documento> getDocumentos()
        {
            return this.documentos;
        }

        Documento documento = null;

        public Documento getDocumento()
        {
            return this.documento;
        }

        Boolean flagSelecionarVarios;

        public frm_DocumentoPesquisar(Boolean flagSelecionarVarios)
        {
            InitializeComponent();
            this.flagSelecionarVarios = flagSelecionarVarios;
        }

        private void btn_pesquisar_Click(object sender, EventArgs e)
        {
            montaDataGrid();
            textDocumento.Focus();
        }

        public void montaDataGrid()
        {
            try
            {
                /* verificando que tipo de grid será montada */

                this.Cursor = Cursors.WaitCursor;
                
                Documento documento = new Documento(null, textDocumento.Text, ApplicationConstants.ATIVO);

                ProtocoloFacade protocoloFacade = ProtocoloFacade.getInstance();

                DataSet ds = protocoloFacade.findDocumentoByFilter(documento);

                dgDocumento.DataSource = ds.Tables["Documentos"].DefaultView;

                dgDocumento.Columns[0].HeaderText = "id";
                dgDocumento.Columns[0].Visible = false;

                dgDocumento.Columns[1].HeaderText = "Documento";

                dgDocumento.Columns[2].HeaderText = "Situação";
                dgDocumento.Columns[2].Visible = false;

                if (flagSelecionarVarios)
                {
                    DataGridViewCheckBoxColumn check = new DataGridViewCheckBoxColumn();
                    check.Name = "Selec";
                    dgDocumento.Columns.Add(check);
                    dgDocumento.Columns[3].DisplayIndex = 0;
                    dgDocumento.RowHeadersVisible = false;

                    /* atribuindo o valor false para todos os checkbox */

                    foreach (DataGridViewRow dvRow in dgDocumento.Rows)
                    {
                        dvRow.Cells[3].Value = false;
                    }
                }
                
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }

        private void btnConfirmar_Click(object sender, EventArgs e)
        {
            try
            {
                this.Cursor = Cursors.WaitCursor;
                
                /* verificando que tipo de serviço foi solicitado */
                if (flagSelecionarVarios)
                {
                    foreach (DataGridViewRow dvRow in dgDocumento.Rows)
                    {
                        /* montando a coleção de itens de documento */
                        if ((Boolean)(dvRow.Cells[3].Value))
                        {
                            documentos.AddFirst(new Documento((Int64)dvRow.Cells[0].Value, (String)dvRow.Cells[1].Value, (String)dvRow.Cells[2].Value));
                        }
                    }

                    /* verificando se o usuário não selecionou nenhum item */
                    if (documentos.Count == 0)
                    {
                        throw new Exception(msg1);
                    }
                    else
                    {
                        this.Close();
                    }
                }
                else
                {
                    if (dgDocumento.CurrentRow != null)
                    {
                        documento = new Documento((Int64)dgDocumento.CurrentRow.Cells[0].Value,
                            (String)dgDocumento.CurrentRow.Cells[1].Value,
                            (String)dgDocumento.CurrentRow.Cells[2].Value);
                        
                        this.Close();
                    }
                    else
                    {
                        throw new Exception(msg1);
                    }
                }


            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }

        private void btnFechar_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
