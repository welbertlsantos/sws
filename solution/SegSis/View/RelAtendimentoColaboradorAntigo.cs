﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using SWS.Entidade;
using System.Diagnostics;

namespace SWS.View
{
    public partial class frmRelAtendimentoColaboradorAntigo : BaseFormConsulta
    {

        Funcionario funcionario;
        Cliente cliente;
        private static String msg1 = "Você deve selecionar o funcionário primeiro";
        private static String msg2 = "A data inicial deverá ser menor que a data final.";
        private static String msg3 = "O funcionário é obrigatório.";

        public frmRelAtendimentoColaboradorAntigo()
        {
            InitializeComponent();
        }

        private void btnColaborador_Click(object sender, EventArgs e)
        {
            
            /* limpando dados do funcionario e cliente anterior */
            funcionario = null;
            cliente = null;
            textColaborador.Text = String.Empty;
            textCliente.Text = String.Empty;
            
            frmFuncionarioPesquisa pesquisarFuncionario = new frmFuncionarioPesquisa();
            pesquisarFuncionario.ShowDialog();

            if (pesquisarFuncionario.Funcionario != null)
            {
                funcionario = pesquisarFuncionario.Funcionario;
                textColaborador.Text = funcionario.Nome;
                grbCliente.Enabled = true;
            }
        }

        private void btnFechar_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnCliente_Click(object sender, EventArgs e)
        {
            try
            {
                if (funcionario == null)
                    throw new Exception(msg1);

                /* limpando dados do cliente anterior */
                cliente = null;
                textCliente.Text = String.Empty;
                
                frmClienteFuncaoFuncionarioPesquisar pesquisarCliente = new frmClienteFuncaoFuncionarioPesquisar(funcionario);
                pesquisarCliente.ShowDialog();

                if (pesquisarCliente.Cliente != null)
                {
                    cliente = pesquisarCliente.Cliente;
                    textCliente.Text = cliente.RazaoSocial;
                }


            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }

        }

        private void btnImprimir_Click(object sender, EventArgs e)
        {
            try
            {
                if (validaDadosTela())
                {
                    this.Cursor = Cursors.WaitCursor;

                    /* validando situação do atendimento */

                    String situacao = String.Empty;

                    if (rbTodos.Checked)
                        situacao = "T";
                    else if (rbFinalizado.Checked)
                        situacao = "F";
                    else
                        situacao = "P";

                    Int64 idCliente = cliente != null ? (Int64)cliente.Id : Convert.ToInt64(0);

                    var process = new Process();

                    process.StartInfo.FileName = "Relatorio\\jre7\\bin\\java";
                    process.StartInfo.Arguments = " -jar Relatorio\\MiddlewareJasper.jar " + "Relatorio\\Report\\" + "ATENDIMENTO_COLABORADOR.JASPER" + " ID_FUNCIONARIO:" + funcionario.Id + ":INTEGER" + " DATA_INICIAL:" + Convert.ToDateTime(dataInicial.Text).ToShortDateString() + ":STRING" + " DATA_FINAL:" + Convert.ToDateTime
                        (dataFinal.Text).ToShortDateString() + ":STRING" + " ID_CLIENTE:" + idCliente + ":INTEGER" + " SITUACAO:" + situacao + ":STRING";
                    process.StartInfo.UseShellExecute = false;
                    process.StartInfo.RedirectStandardError = true;
                    process.StartInfo.RedirectStandardInput = true;
                    process.StartInfo.RedirectStandardOutput = true;
                    process.StartInfo.CreateNoWindow = true;

                    process.Start();
                    process.StandardOutput.ReadToEnd();
                    process.WaitForExit();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }

        private bool validaDadosTela()
        {
            bool resultado = true;
            
            try
            {
                /* verificando validação de data */

                if (Convert.ToDateTime(dataInicial.Text) > Convert.ToDateTime(dataFinal.Text))
                    throw new Exception(msg2);

                if (funcionario == null)
                    throw new Exception(msg3);

            }
            catch (Exception ex)
            {
                resultado = false;
                MessageBox.Show(ex.Message, "Atençao", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
            
            return resultado;
        }
    }
}
