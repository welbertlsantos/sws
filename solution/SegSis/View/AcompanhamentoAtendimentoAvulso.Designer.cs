﻿namespace SWS.View
{
    partial class frmAcompanhamentoAtendimentoAvulso
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.flpAcao = new System.Windows.Forms.FlowLayoutPanel();
            this.btnConfirmar = new System.Windows.Forms.Button();
            this.btnFechar = new System.Windows.Forms.Button();
            this.lblNomeColaborador = new System.Windows.Forms.TextBox();
            this.lblCliente = new System.Windows.Forms.TextBox();
            this.lblSala = new System.Windows.Forms.TextBox();
            this.lblExame = new System.Windows.Forms.TextBox();
            this.lblRg = new System.Windows.Forms.TextBox();
            this.lblCpf = new System.Windows.Forms.TextBox();
            this.textNome = new System.Windows.Forms.TextBox();
            this.textRg = new System.Windows.Forms.TextBox();
            this.textCpf = new System.Windows.Forms.MaskedTextBox();
            this.textCliente = new System.Windows.Forms.TextBox();
            this.btnCliente = new System.Windows.Forms.Button();
            this.textSala = new System.Windows.Forms.TextBox();
            this.textExame = new System.Windows.Forms.TextBox();
            this.btnExame = new System.Windows.Forms.Button();
            this.textAndar = new System.Windows.Forms.TextBox();
            this.lblAndar = new System.Windows.Forms.TextBox();
            this.textSenha = new System.Windows.Forms.TextBox();
            this.lblSenha = new System.Windows.Forms.TextBox();
            this.btnSala = new System.Windows.Forms.Button();
            this.btnCancelarCliente = new System.Windows.Forms.Button();
            this.btnCancelarSala = new System.Windows.Forms.Button();
            this.btnCancelarExame = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.scForm)).BeginInit();
            this.scForm.Panel1.SuspendLayout();
            this.scForm.Panel2.SuspendLayout();
            this.scForm.SuspendLayout();
            this.pnlForm.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.banner)).BeginInit();
            this.flpAcao.SuspendLayout();
            this.SuspendLayout();
            // 
            // scForm
            // 
            // 
            // scForm.Panel1
            // 
            this.scForm.Panel1.Controls.Add(this.flpAcao);
            // 
            // pnlForm
            // 
            this.pnlForm.Controls.Add(this.btnCancelarExame);
            this.pnlForm.Controls.Add(this.btnCancelarSala);
            this.pnlForm.Controls.Add(this.btnCancelarCliente);
            this.pnlForm.Controls.Add(this.btnSala);
            this.pnlForm.Controls.Add(this.lblSenha);
            this.pnlForm.Controls.Add(this.textSenha);
            this.pnlForm.Controls.Add(this.textAndar);
            this.pnlForm.Controls.Add(this.lblAndar);
            this.pnlForm.Controls.Add(this.btnExame);
            this.pnlForm.Controls.Add(this.textExame);
            this.pnlForm.Controls.Add(this.textSala);
            this.pnlForm.Controls.Add(this.btnCliente);
            this.pnlForm.Controls.Add(this.textCliente);
            this.pnlForm.Controls.Add(this.textCpf);
            this.pnlForm.Controls.Add(this.textRg);
            this.pnlForm.Controls.Add(this.textNome);
            this.pnlForm.Controls.Add(this.lblCpf);
            this.pnlForm.Controls.Add(this.lblRg);
            this.pnlForm.Controls.Add(this.lblExame);
            this.pnlForm.Controls.Add(this.lblSala);
            this.pnlForm.Controls.Add(this.lblCliente);
            this.pnlForm.Controls.Add(this.lblNomeColaborador);
            // 
            // flpAcao
            // 
            this.flpAcao.Controls.Add(this.btnConfirmar);
            this.flpAcao.Controls.Add(this.btnFechar);
            this.flpAcao.Dock = System.Windows.Forms.DockStyle.Fill;
            this.flpAcao.Location = new System.Drawing.Point(0, 0);
            this.flpAcao.Name = "flpAcao";
            this.flpAcao.Size = new System.Drawing.Size(534, 30);
            this.flpAcao.TabIndex = 0;
            // 
            // btnConfirmar
            // 
            this.btnConfirmar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnConfirmar.Image = global::SWS.Properties.Resources.fechar_ico;
            this.btnConfirmar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnConfirmar.Location = new System.Drawing.Point(3, 3);
            this.btnConfirmar.Name = "btnConfirmar";
            this.btnConfirmar.Size = new System.Drawing.Size(75, 23);
            this.btnConfirmar.TabIndex = 0;
            this.btnConfirmar.Text = "Confirmar";
            this.btnConfirmar.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnConfirmar.UseVisualStyleBackColor = true;
            this.btnConfirmar.Click += new System.EventHandler(this.btnConfirmar_Click);
            // 
            // btnFechar
            // 
            this.btnFechar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnFechar.Image = global::SWS.Properties.Resources.close;
            this.btnFechar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnFechar.Location = new System.Drawing.Point(84, 3);
            this.btnFechar.Name = "btnFechar";
            this.btnFechar.Size = new System.Drawing.Size(75, 23);
            this.btnFechar.TabIndex = 1;
            this.btnFechar.Text = "&Fechar";
            this.btnFechar.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnFechar.UseVisualStyleBackColor = true;
            this.btnFechar.Click += new System.EventHandler(this.btnFechar_Click);
            // 
            // lblNomeColaborador
            // 
            this.lblNomeColaborador.BackColor = System.Drawing.Color.Silver;
            this.lblNomeColaborador.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblNomeColaborador.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNomeColaborador.Location = new System.Drawing.Point(12, 15);
            this.lblNomeColaborador.MaxLength = 100;
            this.lblNomeColaborador.Name = "lblNomeColaborador";
            this.lblNomeColaborador.ReadOnly = true;
            this.lblNomeColaborador.Size = new System.Drawing.Size(147, 21);
            this.lblNomeColaborador.TabIndex = 26;
            this.lblNomeColaborador.TabStop = false;
            this.lblNomeColaborador.Text = "Nome Colaborador";
            // 
            // lblCliente
            // 
            this.lblCliente.BackColor = System.Drawing.Color.Silver;
            this.lblCliente.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblCliente.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCliente.Location = new System.Drawing.Point(12, 75);
            this.lblCliente.MaxLength = 100;
            this.lblCliente.Name = "lblCliente";
            this.lblCliente.ReadOnly = true;
            this.lblCliente.Size = new System.Drawing.Size(147, 21);
            this.lblCliente.TabIndex = 27;
            this.lblCliente.TabStop = false;
            this.lblCliente.Text = "Cliente";
            // 
            // lblSala
            // 
            this.lblSala.BackColor = System.Drawing.Color.Silver;
            this.lblSala.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblSala.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSala.Location = new System.Drawing.Point(12, 95);
            this.lblSala.MaxLength = 100;
            this.lblSala.Name = "lblSala";
            this.lblSala.ReadOnly = true;
            this.lblSala.Size = new System.Drawing.Size(147, 21);
            this.lblSala.TabIndex = 28;
            this.lblSala.TabStop = false;
            this.lblSala.Text = "Sala";
            // 
            // lblExame
            // 
            this.lblExame.BackColor = System.Drawing.Color.Silver;
            this.lblExame.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblExame.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblExame.Location = new System.Drawing.Point(12, 135);
            this.lblExame.MaxLength = 100;
            this.lblExame.Name = "lblExame";
            this.lblExame.ReadOnly = true;
            this.lblExame.Size = new System.Drawing.Size(147, 21);
            this.lblExame.TabIndex = 29;
            this.lblExame.TabStop = false;
            this.lblExame.Text = "Exame";
            // 
            // lblRg
            // 
            this.lblRg.BackColor = System.Drawing.Color.Silver;
            this.lblRg.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblRg.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblRg.Location = new System.Drawing.Point(12, 35);
            this.lblRg.MaxLength = 100;
            this.lblRg.Name = "lblRg";
            this.lblRg.ReadOnly = true;
            this.lblRg.Size = new System.Drawing.Size(147, 21);
            this.lblRg.TabIndex = 30;
            this.lblRg.TabStop = false;
            this.lblRg.Text = "RG";
            // 
            // lblCpf
            // 
            this.lblCpf.BackColor = System.Drawing.Color.Silver;
            this.lblCpf.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblCpf.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCpf.Location = new System.Drawing.Point(12, 55);
            this.lblCpf.MaxLength = 100;
            this.lblCpf.Name = "lblCpf";
            this.lblCpf.ReadOnly = true;
            this.lblCpf.Size = new System.Drawing.Size(147, 21);
            this.lblCpf.TabIndex = 31;
            this.lblCpf.TabStop = false;
            this.lblCpf.Text = "CPF";
            // 
            // textNome
            // 
            this.textNome.AcceptsReturn = true;
            this.textNome.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textNome.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.textNome.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textNome.Location = new System.Drawing.Point(158, 15);
            this.textNome.MaxLength = 100;
            this.textNome.Name = "textNome";
            this.textNome.Size = new System.Drawing.Size(337, 21);
            this.textNome.TabIndex = 1;
            // 
            // textRg
            // 
            this.textRg.AcceptsReturn = true;
            this.textRg.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textRg.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.textRg.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textRg.Location = new System.Drawing.Point(158, 35);
            this.textRg.MaxLength = 15;
            this.textRg.Name = "textRg";
            this.textRg.Size = new System.Drawing.Size(337, 21);
            this.textRg.TabIndex = 2;
            // 
            // textCpf
            // 
            this.textCpf.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textCpf.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textCpf.ForeColor = System.Drawing.Color.Black;
            this.textCpf.Location = new System.Drawing.Point(158, 55);
            this.textCpf.Mask = "000,000,000-00";
            this.textCpf.Name = "textCpf";
            this.textCpf.PromptChar = ' ';
            this.textCpf.Size = new System.Drawing.Size(337, 21);
            this.textCpf.TabIndex = 3;
            this.textCpf.Leave += new System.EventHandler(this.textCpf_Leave);
            // 
            // textCliente
            // 
            this.textCliente.AcceptsReturn = true;
            this.textCliente.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textCliente.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.textCliente.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textCliente.Location = new System.Drawing.Point(158, 75);
            this.textCliente.MaxLength = 100;
            this.textCliente.Name = "textCliente";
            this.textCliente.Size = new System.Drawing.Size(274, 21);
            this.textCliente.TabIndex = 4;
            // 
            // btnCliente
            // 
            this.btnCliente.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnCliente.Image = global::SWS.Properties.Resources.busca;
            this.btnCliente.Location = new System.Drawing.Point(428, 75);
            this.btnCliente.Name = "btnCliente";
            this.btnCliente.Size = new System.Drawing.Size(34, 21);
            this.btnCliente.TabIndex = 5;
            this.btnCliente.TabStop = false;
            this.btnCliente.UseVisualStyleBackColor = true;
            this.btnCliente.Click += new System.EventHandler(this.btnCliente_Click);
            // 
            // textSala
            // 
            this.textSala.AcceptsReturn = true;
            this.textSala.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textSala.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.textSala.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textSala.Location = new System.Drawing.Point(158, 95);
            this.textSala.MaxLength = 20;
            this.textSala.Name = "textSala";
            this.textSala.Size = new System.Drawing.Size(274, 21);
            this.textSala.TabIndex = 5;
            // 
            // textExame
            // 
            this.textExame.AcceptsReturn = true;
            this.textExame.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textExame.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.textExame.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textExame.Location = new System.Drawing.Point(158, 135);
            this.textExame.MaxLength = 100;
            this.textExame.Name = "textExame";
            this.textExame.Size = new System.Drawing.Size(274, 21);
            this.textExame.TabIndex = 7;
            // 
            // btnExame
            // 
            this.btnExame.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnExame.Image = global::SWS.Properties.Resources.busca;
            this.btnExame.Location = new System.Drawing.Point(428, 135);
            this.btnExame.Name = "btnExame";
            this.btnExame.Size = new System.Drawing.Size(34, 21);
            this.btnExame.TabIndex = 8;
            this.btnExame.TabStop = false;
            this.btnExame.UseVisualStyleBackColor = true;
            this.btnExame.Click += new System.EventHandler(this.button1_Click);
            // 
            // textAndar
            // 
            this.textAndar.AcceptsReturn = true;
            this.textAndar.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textAndar.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.textAndar.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textAndar.Location = new System.Drawing.Point(158, 115);
            this.textAndar.MaxLength = 1;
            this.textAndar.Name = "textAndar";
            this.textAndar.Size = new System.Drawing.Size(337, 21);
            this.textAndar.TabIndex = 6;
            // 
            // lblAndar
            // 
            this.lblAndar.BackColor = System.Drawing.Color.Silver;
            this.lblAndar.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblAndar.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblAndar.Location = new System.Drawing.Point(12, 115);
            this.lblAndar.MaxLength = 2;
            this.lblAndar.Name = "lblAndar";
            this.lblAndar.ReadOnly = true;
            this.lblAndar.Size = new System.Drawing.Size(147, 21);
            this.lblAndar.TabIndex = 33;
            this.lblAndar.TabStop = false;
            this.lblAndar.Text = "Andar";
            // 
            // textSenha
            // 
            this.textSenha.AcceptsReturn = true;
            this.textSenha.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textSenha.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.textSenha.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textSenha.Location = new System.Drawing.Point(158, 155);
            this.textSenha.MaxLength = 3;
            this.textSenha.Name = "textSenha";
            this.textSenha.Size = new System.Drawing.Size(337, 21);
            this.textSenha.TabIndex = 8;
            this.textSenha.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.textSenha_KeyPress);
            // 
            // lblSenha
            // 
            this.lblSenha.BackColor = System.Drawing.Color.Silver;
            this.lblSenha.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblSenha.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSenha.Location = new System.Drawing.Point(12, 155);
            this.lblSenha.MaxLength = 100;
            this.lblSenha.Name = "lblSenha";
            this.lblSenha.ReadOnly = true;
            this.lblSenha.Size = new System.Drawing.Size(147, 21);
            this.lblSenha.TabIndex = 35;
            this.lblSenha.TabStop = false;
            this.lblSenha.Text = "Senha";
            // 
            // btnSala
            // 
            this.btnSala.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnSala.Image = global::SWS.Properties.Resources.busca;
            this.btnSala.Location = new System.Drawing.Point(428, 95);
            this.btnSala.Name = "btnSala";
            this.btnSala.Size = new System.Drawing.Size(34, 21);
            this.btnSala.TabIndex = 36;
            this.btnSala.TabStop = false;
            this.btnSala.UseVisualStyleBackColor = true;
            this.btnSala.Click += new System.EventHandler(this.btnSala_Click);
            // 
            // btnCancelarCliente
            // 
            this.btnCancelarCliente.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnCancelarCliente.Image = global::SWS.Properties.Resources.close;
            this.btnCancelarCliente.Location = new System.Drawing.Point(461, 75);
            this.btnCancelarCliente.Name = "btnCancelarCliente";
            this.btnCancelarCliente.Size = new System.Drawing.Size(34, 21);
            this.btnCancelarCliente.TabIndex = 37;
            this.btnCancelarCliente.TabStop = false;
            this.btnCancelarCliente.UseVisualStyleBackColor = true;
            this.btnCancelarCliente.Click += new System.EventHandler(this.btnCancelarCliente_Click);
            // 
            // btnCancelarSala
            // 
            this.btnCancelarSala.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnCancelarSala.Image = global::SWS.Properties.Resources.close;
            this.btnCancelarSala.Location = new System.Drawing.Point(461, 95);
            this.btnCancelarSala.Name = "btnCancelarSala";
            this.btnCancelarSala.Size = new System.Drawing.Size(34, 21);
            this.btnCancelarSala.TabIndex = 38;
            this.btnCancelarSala.TabStop = false;
            this.btnCancelarSala.UseVisualStyleBackColor = true;
            this.btnCancelarSala.Click += new System.EventHandler(this.btnCancelarSala_Click);
            // 
            // btnCancelarExame
            // 
            this.btnCancelarExame.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnCancelarExame.Image = global::SWS.Properties.Resources.close;
            this.btnCancelarExame.Location = new System.Drawing.Point(461, 135);
            this.btnCancelarExame.Name = "btnCancelarExame";
            this.btnCancelarExame.Size = new System.Drawing.Size(34, 21);
            this.btnCancelarExame.TabIndex = 39;
            this.btnCancelarExame.TabStop = false;
            this.btnCancelarExame.UseVisualStyleBackColor = true;
            this.btnCancelarExame.Click += new System.EventHandler(this.btnCancelarExame_Click);
            // 
            // frmAcompanhamentoAtendimentoAvulso
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(534, 361);
            this.Name = "frmAcompanhamentoAtendimentoAvulso";
            this.Text = "INCLUSÃO CHAMADA AVULSO NO ATENDIMENTO";
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.AcompanhamentoAtendimentoAvulso_KeyDown);
            this.scForm.Panel1.ResumeLayout(false);
            this.scForm.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.scForm)).EndInit();
            this.scForm.ResumeLayout(false);
            this.pnlForm.ResumeLayout(false);
            this.pnlForm.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.banner)).EndInit();
            this.flpAcao.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.FlowLayoutPanel flpAcao;
        private System.Windows.Forms.Button btnConfirmar;
        private System.Windows.Forms.Button btnFechar;
        private System.Windows.Forms.TextBox lblCpf;
        private System.Windows.Forms.TextBox lblRg;
        private System.Windows.Forms.TextBox lblExame;
        private System.Windows.Forms.TextBox lblSala;
        private System.Windows.Forms.TextBox lblCliente;
        private System.Windows.Forms.TextBox lblNomeColaborador;
        private System.Windows.Forms.TextBox textRg;
        private System.Windows.Forms.TextBox textNome;
        private System.Windows.Forms.TextBox textCliente;
        private System.Windows.Forms.MaskedTextBox textCpf;
        private System.Windows.Forms.Button btnCliente;
        private System.Windows.Forms.Button btnExame;
        private System.Windows.Forms.TextBox textExame;
        private System.Windows.Forms.TextBox textSala;
        private System.Windows.Forms.TextBox lblSenha;
        private System.Windows.Forms.TextBox textSenha;
        private System.Windows.Forms.TextBox textAndar;
        private System.Windows.Forms.TextBox lblAndar;
        private System.Windows.Forms.Button btnSala;
        private System.Windows.Forms.Button btnCancelarExame;
        private System.Windows.Forms.Button btnCancelarSala;
        private System.Windows.Forms.Button btnCancelarCliente;
    }
}