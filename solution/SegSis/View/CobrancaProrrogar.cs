﻿using SWS.Entidade;
using SWS.Facade;
using SWS.Helper;
using SWS.Resources;
using SWS.View.ViewHelper;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace SWS.View
{
    public partial class frmCobrancaProrrogar : frmTemplateConsulta
    {
        private Cobranca cobranca;
        
        public frmCobrancaProrrogar(Cobranca cobrancaProrroga)
        {
            InitializeComponent();
            this.cobranca = cobrancaProrroga;
            ActiveControl = textValorInformado;

            textNumeroNota.Text = cobrancaProrroga.Nota.NumeroNfe.ToString();
            textDocumento.Text = cobrancaProrroga.NumeroDocumento;
            textParcela.Text = cobrancaProrroga.NumeroParcela.ToString();
            textFormaPagamento.Text = cobrancaProrroga.Forma.Descricao;
            textCliente.Text = cobrancaProrroga.Nota.Cliente.RazaoSocial;
            textDataEmissao.Text = string.Format("{0:dd/MM/yyyy}", cobrancaProrroga.DataEmissao);
            textDataVencimento.Text = string.Format("{0:dd/MM/yyyy}", cobrancaProrroga.DataVencimento);
            textValorCobranca.Text = ValidaCampoHelper.FormataValorMonetario(cobrancaProrroga.ValorAjustado.ToString());

            string valorJuros;
            PermissionamentoFacade.mapaConfiguracoes.TryGetValue(ConfigurationConstans.PERCENTUAL_JUROS_MENSAL_PRORROGACAO_CRC, out valorJuros);
            textJurosSugerido.Text = valorJuros;
            calculaValores();
            

        }

        private void btnCancelar_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnConfirmar_Click(object sender, EventArgs e)
        {
            try
            {
                if (string.IsNullOrEmpty(textValorInformado.Text))
                    throw new Exception("Obrigatório informar o valor da cobranca.");

                if (cobranca.DataVencimento >= Convert.ToDateTime(dataProrrogacao.Text))
                    throw new Exception("Não é possível realizar a prorrogação. A data escolhida precisa ser posterior a data de vencimento.");

                if (cobranca.ValorAjustado > Convert.ToDecimal(textValorInformado.Text))
                    throw new Exception("O valor da cobrança não pode ser menor do que o valor nominal da mesma.");

                FinanceiroFacade financeiroFacade = FinanceiroFacade.getInstance();

                frmJustificativa justificativa = new frmJustificativa("INFORME A JUSTIFICATIVA DA PRORROGAÇÃO");
                justificativa.ShowDialog();

                if (string.IsNullOrEmpty(justificativa.Justificativa))
                    throw new Exception("A justificativa da prorrogação é obrigatória.");

                cobranca.UsuarioProrroga = PermissionamentoFacade.usuarioAutenticado;
                cobranca.DataVencimento = Convert.ToDateTime(dataProrrogacao.Text);
                cobranca.ValorAjustado = Convert.ToDecimal(textValorInformado.Text);
                cobranca.ObservacaoProrrogacao = justificativa.Justificativa;

                financeiroFacade.prorrogarCobranca(cobranca);
                MessageBox.Show("Cobrança prorrogada com sucesso.", "Confirmação", MessageBoxButtons.OK, MessageBoxIcon.Information);
                this.Close();


            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }

        private void calculaValores()
        {
            int diferencaDias = SystemHelper.diferencaDatasEmDias(cobranca.DataVencimento, dataProrrogacao.Value);
            decimal valorJurosDia = ((Convert.ToDecimal(textJurosSugerido.Text.Replace(".",",")) / 30) / 100 ) * diferencaDias ;
            textValorCalculado.Text = Convert.ToString(Math.Round(Convert.ToDecimal(cobranca.ValorAjustado) + (Convert.ToDecimal(cobranca.ValorAjustado) * valorJurosDia),2));
        }

        private void dataProrrogacao_ValueChanged(object sender, EventArgs e)
        {
            calculaValores();
        }

        private void frmCobrancaProrrogar_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter || e.KeyCode == Keys.Return)
                SendKeys.Send("{TAB}");
        }

        private void textValorInformado_Enter(object sender, EventArgs e)
        {
            textValorInformado.Tag = textValorInformado.Text;
            textValorInformado.Text = string.Empty;
        }

        private void textValorInformado_Leave(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(textValorInformado.Text))
                textValorInformado.Text = textValorInformado.Tag.ToString();
            else
                textValorInformado.Text = ValidaCampoHelper.FormataValorMonetario(textValorInformado.Text);
        }

        private void textValorInformado_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = ValidaCampoHelper.FormataDigitacaoNumero(sender, e, textValorInformado.Text, 2);
        }

        private void textValorJugerido_Enter(object sender, EventArgs e)
        {
            textJurosSugerido.Tag = textJurosSugerido.Text;
            textJurosSugerido.Text = string.Empty;
        }

        private void textJurosSugerido_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = ValidaCampoHelper.FormataDigitacaoNumero(sender, e, textJurosSugerido.Text, 2);
        }

        private void textJurosSugerido_Leave(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(textJurosSugerido.Text))
            {
                textJurosSugerido.Text = textJurosSugerido.Tag.ToString();
                calculaValores();
            }
            else
            {
                textJurosSugerido.Text = ValidaCampoHelper.FormataValorMonetario(textJurosSugerido.Text);
                calculaValores();
            }
        }
    }
}
