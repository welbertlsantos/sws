﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace SWS.View
{
    public partial class frmEsocialTipoGeracaoArquivo : Form
    {
        private string tipoArquivo;

        public string TipoArquivo
        {
            get { return tipoArquivo; }
            set { tipoArquivo = value; }
        }
        
        public frmEsocialTipoGeracaoArquivo()
        {
            InitializeComponent();
        }

        private void btnIndividual_Click(object sender, EventArgs e)
        {
            tipoArquivo = "INDIVIDUAL";
            this.Close();
               
        }

        private void btnAgrupado_Click(object sender, EventArgs e)
        {
            tipoArquivo = "AGRUPADO";
            this.Close();
        }
    }
}
