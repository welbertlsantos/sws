﻿namespace SWS.View
{
    partial class frmRamoSelecionar
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            this.flpAcao = new System.Windows.Forms.FlowLayoutPanel();
            this.btSelecionar = new System.Windows.Forms.Button();
            this.btNovo = new System.Windows.Forms.Button();
            this.btBuscar = new System.Windows.Forms.Button();
            this.btSair = new System.Windows.Forms.Button();
            this.lblRamo = new System.Windows.Forms.TextBox();
            this.textRamo = new System.Windows.Forms.TextBox();
            this.grbRamo = new System.Windows.Forms.GroupBox();
            this.dgvRamo = new System.Windows.Forms.DataGridView();
            ((System.ComponentModel.ISupportInitialize)(this.scForm)).BeginInit();
            this.scForm.Panel1.SuspendLayout();
            this.scForm.Panel2.SuspendLayout();
            this.scForm.SuspendLayout();
            this.pnlForm.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.banner)).BeginInit();
            this.flpAcao.SuspendLayout();
            this.grbRamo.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvRamo)).BeginInit();
            this.SuspendLayout();
            // 
            // scForm
            // 
            // 
            // scForm.Panel1
            // 
            this.scForm.Panel1.Controls.Add(this.flpAcao);
            // 
            // pnlForm
            // 
            this.pnlForm.Controls.Add(this.grbRamo);
            this.pnlForm.Controls.Add(this.textRamo);
            this.pnlForm.Controls.Add(this.lblRamo);
            // 
            // flpAcao
            // 
            this.flpAcao.Controls.Add(this.btSelecionar);
            this.flpAcao.Controls.Add(this.btNovo);
            this.flpAcao.Controls.Add(this.btBuscar);
            this.flpAcao.Controls.Add(this.btSair);
            this.flpAcao.Dock = System.Windows.Forms.DockStyle.Fill;
            this.flpAcao.Location = new System.Drawing.Point(0, 0);
            this.flpAcao.Name = "flpAcao";
            this.flpAcao.Size = new System.Drawing.Size(534, 30);
            this.flpAcao.TabIndex = 0;
            // 
            // btSelecionar
            // 
            this.btSelecionar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btSelecionar.Image = global::SWS.Properties.Resources.fechar_ico;
            this.btSelecionar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btSelecionar.Location = new System.Drawing.Point(3, 3);
            this.btSelecionar.Name = "btSelecionar";
            this.btSelecionar.Size = new System.Drawing.Size(75, 23);
            this.btSelecionar.TabIndex = 6;
            this.btSelecionar.TabStop = false;
            this.btSelecionar.Text = "&Selecionar";
            this.btSelecionar.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btSelecionar.UseVisualStyleBackColor = true;
            this.btSelecionar.Click += new System.EventHandler(this.btSelecionar_Click);
            // 
            // btNovo
            // 
            this.btNovo.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btNovo.Image = global::SWS.Properties.Resources.incluir;
            this.btNovo.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btNovo.Location = new System.Drawing.Point(84, 3);
            this.btNovo.Name = "btNovo";
            this.btNovo.Size = new System.Drawing.Size(75, 23);
            this.btNovo.TabIndex = 7;
            this.btNovo.TabStop = false;
            this.btNovo.Text = "&Novo";
            this.btNovo.UseVisualStyleBackColor = true;
            this.btNovo.Click += new System.EventHandler(this.btNovo_Click);
            // 
            // btBuscar
            // 
            this.btBuscar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btBuscar.Image = global::SWS.Properties.Resources.busca;
            this.btBuscar.ImageAlign = System.Drawing.ContentAlignment.BottomLeft;
            this.btBuscar.Location = new System.Drawing.Point(165, 3);
            this.btBuscar.Name = "btBuscar";
            this.btBuscar.Size = new System.Drawing.Size(75, 23);
            this.btBuscar.TabIndex = 9;
            this.btBuscar.TabStop = false;
            this.btBuscar.Text = "Buscar";
            this.btBuscar.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btBuscar.UseVisualStyleBackColor = true;
            this.btBuscar.Click += new System.EventHandler(this.btBuscar_Click);
            // 
            // btSair
            // 
            this.btSair.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btSair.Image = global::SWS.Properties.Resources.close;
            this.btSair.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btSair.Location = new System.Drawing.Point(246, 3);
            this.btSair.Name = "btSair";
            this.btSair.Size = new System.Drawing.Size(75, 23);
            this.btSair.TabIndex = 8;
            this.btSair.TabStop = false;
            this.btSair.Text = "&Sair";
            this.btSair.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btSair.UseVisualStyleBackColor = true;
            this.btSair.Click += new System.EventHandler(this.btSair_Click);
            // 
            // lblRamo
            // 
            this.lblRamo.BackColor = System.Drawing.Color.LightGray;
            this.lblRamo.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblRamo.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblRamo.Location = new System.Drawing.Point(9, 8);
            this.lblRamo.Name = "lblRamo";
            this.lblRamo.ReadOnly = true;
            this.lblRamo.Size = new System.Drawing.Size(112, 21);
            this.lblRamo.TabIndex = 0;
            this.lblRamo.TabStop = false;
            this.lblRamo.Text = "Ramo de atividade";
            // 
            // textRamo
            // 
            this.textRamo.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textRamo.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textRamo.Location = new System.Drawing.Point(120, 8);
            this.textRamo.MaxLength = 100;
            this.textRamo.Name = "textRamo";
            this.textRamo.Size = new System.Drawing.Size(379, 21);
            this.textRamo.TabIndex = 2;
            // 
            // grbRamo
            // 
            this.grbRamo.Controls.Add(this.dgvRamo);
            this.grbRamo.Location = new System.Drawing.Point(9, 42);
            this.grbRamo.Name = "grbRamo";
            this.grbRamo.Size = new System.Drawing.Size(492, 232);
            this.grbRamo.TabIndex = 3;
            this.grbRamo.TabStop = false;
            this.grbRamo.Text = "Ramos de atividade";
            // 
            // dgvRamo
            // 
            this.dgvRamo.AllowUserToAddRows = false;
            this.dgvRamo.AllowUserToDeleteRows = false;
            this.dgvRamo.AllowUserToOrderColumns = true;
            this.dgvRamo.AllowUserToResizeColumns = false;
            this.dgvRamo.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.DisplayedCells;
            this.dgvRamo.BackgroundColor = System.Drawing.Color.White;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.DarkGray;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvRamo.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.dgvRamo.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.Color.LightGray;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvRamo.DefaultCellStyle = dataGridViewCellStyle2;
            this.dgvRamo.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvRamo.Location = new System.Drawing.Point(3, 16);
            this.dgvRamo.MultiSelect = false;
            this.dgvRamo.Name = "dgvRamo";
            this.dgvRamo.ReadOnly = true;
            this.dgvRamo.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvRamo.Size = new System.Drawing.Size(486, 213);
            this.dgvRamo.TabIndex = 0;
            this.dgvRamo.TabStop = false;
            this.dgvRamo.CellMouseDoubleClick += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.dgvRamo_CellMouseDoubleClick);
            // 
            // frmRamoSelecionar
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(534, 361);
            this.Name = "frmRamoSelecionar";
            this.Text = "SELECIONAR RAMO DE ATIVIDADE";
            this.scForm.Panel1.ResumeLayout(false);
            this.scForm.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.scForm)).EndInit();
            this.scForm.ResumeLayout(false);
            this.pnlForm.ResumeLayout(false);
            this.pnlForm.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.banner)).EndInit();
            this.flpAcao.ResumeLayout(false);
            this.grbRamo.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvRamo)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.FlowLayoutPanel flpAcao;
        private System.Windows.Forms.Button btSair;
        private System.Windows.Forms.Button btNovo;
        private System.Windows.Forms.Button btSelecionar;
        private System.Windows.Forms.Button btBuscar;
        private System.Windows.Forms.TextBox lblRamo;
        private System.Windows.Forms.TextBox textRamo;
        private System.Windows.Forms.GroupBox grbRamo;
        private System.Windows.Forms.DataGridView dgvRamo;
    }
}