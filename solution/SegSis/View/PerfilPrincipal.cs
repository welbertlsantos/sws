﻿using SWS.Entidade;
using SWS.Facade;
using SWS.View.Resources;
using SWS.View.ViewHelper;
using SWS.ViewHelper;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace SWS.View
{
    public partial class frmPerfilPrincipal : frmTemplate
    {
        Perfil perfil;

        public frmPerfilPrincipal()
        {
            InitializeComponent();
            ComboHelper.comboSituacao(cbSituacao);
            ActiveControl = textPerfil;
            validaPermissoes();
        }

        private void btnIncluir_Click(object sender, EventArgs e)
        {
            try
            {
                frmPerfilIncluir incluirPerfil = new frmPerfilIncluir();
                incluirPerfil.ShowDialog();

                if (incluirPerfil.Perfil != null)
                {
                    perfil = new Perfil(null, string.Empty, string.Empty);
                    montaDataGrid();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btnAlterar_Click(object sender, EventArgs e)
        {
            try
            {
                if (dgvPerfil.CurrentRow == null)
                    throw new Exception("Selecione uma linha.");

                UsuarioFacade usuarioFacade = UsuarioFacade.getInstance();

                Perfil perfilAlterar = usuarioFacade.findPerfilById((long)dgvPerfil.CurrentRow.Cells[0].Value);

                if (!string.Equals(perfilAlterar.Situacao, ApplicationConstants.ATIVO))
                    throw new Exception("Somente perfis ativos podem ser alterados.");

                frmPerfilAlterar alterarPerfil = new frmPerfilAlterar(perfilAlterar);
                alterarPerfil.ShowDialog();

                perfil = new Perfil();
                montaDataGrid();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btnPesquisar_Click(object sender, EventArgs e)
        {
            try
            {
                perfil = new Perfil(null, textPerfil.Text, ((SelectItem)cbSituacao.SelectedItem).Valor);
                montaDataGrid();
                textPerfil.Focus();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btnReativar_Click(object sender, EventArgs e)
        {
            try
            {
                if (dgvPerfil.CurrentRow == null)
                    throw new Exception("Selecione uma linha.");

                UsuarioFacade usuarioFacade = UsuarioFacade.getInstance();

                Perfil perfilReativar = usuarioFacade.findPerfilById((long)dgvPerfil.CurrentRow.Cells[0].Value);

                if (!string.Equals(perfilReativar.Situacao, ApplicationConstants.DESATIVADO))
                    throw new Exception("Somente perfis desativados podem ser reativados.");

                if (MessageBox.Show("Deseja reativar o perfil selecionado?", "Atenção", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                {
                    perfilReativar.Situacao = ApplicationConstants.ATIVO;
                    usuarioFacade.updatePerfil(perfilReativar);
                    MessageBox.Show("Perfil reativado com sucesso.", "Confirmação", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    perfil = new Perfil();
                    montaDataGrid();

                }

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btnDetalhar_Click(object sender, EventArgs e)
        {
            try
            {
                if (dgvPerfil.CurrentRow == null)
                    throw new Exception("Selecione uma linha.");

                UsuarioFacade usuarioFacade = UsuarioFacade.getInstance();

                frmPerfilDetalhar detalharPerfil = new frmPerfilDetalhar(usuarioFacade.findPerfilById((long)dgvPerfil.CurrentRow.Cells[0].Value));
                detalharPerfil.ShowDialog();

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btnExcluir_Click(object sender, EventArgs e)
        {
            try
            {
                if (dgvPerfil.CurrentRow == null)
                    throw new Exception("Selecione uma linha.");

                UsuarioFacade usuarioFacade = UsuarioFacade.getInstance();

                Perfil perfilExcluir = usuarioFacade.findPerfilById((long)dgvPerfil.CurrentRow.Cells[0].Value);

                if (!string.Equals(perfilExcluir.Situacao, ApplicationConstants.ATIVO))
                    throw new Exception("Somente perfis ativos podem ser excluídos.");

                if (MessageBox.Show("Deseja excluir o perfil selecionado?", "Atenção", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                {
                    perfilExcluir.Situacao = ApplicationConstants.DESATIVADO;
                    usuarioFacade.updatePerfil(perfilExcluir);
                    MessageBox.Show("Perfil excluído com sucesso.", "Confirmação", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    perfil = new Perfil();
                    montaDataGrid();

                }

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btnFechar_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void validaPermissoes()
        {
            PermissionamentoFacade permissionamentoFacade = PermissionamentoFacade.getInstance();

            btnAlterar.Enabled = permissionamentoFacade.hasPermission("PERFIL", "ALTERAR");
            btnIncluir.Enabled = permissionamentoFacade.hasPermission("PERFIL", "INCLUIR");
            btnExcluir.Enabled = permissionamentoFacade.hasPermission("PERFIL", "EXCLUIR");
            btnDetalhar.Enabled = permissionamentoFacade.hasPermission("PERFIL", "DETALHAR");
            btnReativar.Enabled = permissionamentoFacade.hasPermission("PERFIL", "REATIVAR");
        }

        private void dgvPerfil_RowPrePaint(object sender, DataGridViewRowPrePaintEventArgs e)
        {
            DataGridView dgv = sender as DataGridView;

            if (string.Equals(dgv.Rows[e.RowIndex].Cells[2].Value, ApplicationConstants.DESATIVADO))
                dgv.Rows[e.RowIndex].DefaultCellStyle.ForeColor = Color.Red;
            
        }

        public void montaDataGrid()
        {
            UsuarioFacade autenticacaoFacade = UsuarioFacade.getInstance();

            DataSet ds = autenticacaoFacade.FindPerfilByFilter(perfil);

            dgvPerfil.Columns.Clear();

            dgvPerfil.DataSource = ds.Tables["Perfis"].DefaultView;

            dgvPerfil.Columns[0].HeaderText = "ID";
            dgvPerfil.Columns[0].Visible = false;

            dgvPerfil.Columns[1].HeaderText = "Descrição";

            dgvPerfil.Columns[2].HeaderText = "Situação";


        }

        private void dgvPerfil_CellMouseDoubleClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            try
            {
                btnAlterar.PerformClick();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }
    }
}
