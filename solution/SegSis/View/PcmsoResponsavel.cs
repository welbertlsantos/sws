﻿using SWS.Entidade;
using SWS.Facade;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SWS.View
{
    public partial class frmPcmsoResponsavel : frmTemplateConsulta
    {

        Usuario usuarioResponsavel;

        public Usuario UsuarioResponsavel
        {
            get { return usuarioResponsavel; }
            set { usuarioResponsavel = value; }
        }
        public frmPcmsoResponsavel()
        {
            InitializeComponent();
            montaDataGrid();
        }

        private void frmPcmsoResponsavel_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter || e.KeyCode == Keys.Return)
                SendKeys.Send("{TAB}");
        }

        private void btn_fechar_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnConfirmar_Click(object sender, EventArgs e)
        {
            try
            {
                if (dgvUsuarioResponsavel.CurrentRow == null)
                    throw new Exception("Você deve selecionar o usuário");

                UsuarioFacade usuarioFacade = UsuarioFacade.getInstance();

                this.UsuarioResponsavel = usuarioFacade.findUsuarioById((long)dgvUsuarioResponsavel.CurrentRow.Cells[0].Value);
                this.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }

        public void montaDataGrid()
        {
            try
            {
                UsuarioFacade usuarioFacade = UsuarioFacade.getInstance();
                Usuario usuario = new Usuario();
                
                DataSet ds = usuarioFacade.findUsuarioFuncaoAtivoByFilter(usuario, 4 );

                dgvUsuarioResponsavel.ColumnCount = 2;
                dgvUsuarioResponsavel.Columns[0].HeaderText = "IdUsuario";
                dgvUsuarioResponsavel.Columns[0].Visible = false;
                dgvUsuarioResponsavel.Columns[1].HeaderText = "Usuário";

                foreach(DataRow row in ds.Tables[0].Rows)
                {
                    dgvUsuarioResponsavel.Rows.Add(
                        row["ID_USUARIO"],
                        row["NOME"].ToString()
                        );
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

    }
}
