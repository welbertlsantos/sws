﻿namespace SWS.View
{
    partial class frm_PcmsoFuncaoGheSelecionar
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frm_PcmsoFuncaoGheSelecionar));
            this.grb_paginacao = new System.Windows.Forms.GroupBox();
            this.btn_incluir = new System.Windows.Forms.Button();
            this.btn_fechar = new System.Windows.Forms.Button();
            this.grd_funcao = new System.Windows.Forms.DataGridView();
            this.grb_dados = new System.Windows.Forms.GroupBox();
            this.btn_limpar = new System.Windows.Forms.Button();
            this.text_ghe = new System.Windows.Forms.TextBox();
            this.lbl_ghe = new System.Windows.Forms.Label();
            this.lbl_cliente = new System.Windows.Forms.Label();
            this.text_cliente = new System.Windows.Forms.TextBox();
            this.lbl_cbo = new System.Windows.Forms.Label();
            this.text_cbo = new System.Windows.Forms.TextBox();
            this.lbl_descricao = new System.Windows.Forms.Label();
            this.btn_buscar = new System.Windows.Forms.Button();
            this.text_descricao = new System.Windows.Forms.TextBox();
            this.grb_grid = new System.Windows.Forms.GroupBox();
            this.grb_paginacao.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grd_funcao)).BeginInit();
            this.grb_dados.SuspendLayout();
            this.grb_grid.SuspendLayout();
            this.SuspendLayout();
            // 
            // grb_paginacao
            // 
            this.grb_paginacao.Controls.Add(this.btn_incluir);
            this.grb_paginacao.Controls.Add(this.btn_fechar);
            this.grb_paginacao.Location = new System.Drawing.Point(4, 409);
            this.grb_paginacao.Name = "grb_paginacao";
            this.grb_paginacao.Size = new System.Drawing.Size(701, 51);
            this.grb_paginacao.TabIndex = 11;
            this.grb_paginacao.TabStop = false;
            // 
            // btn_incluir
            // 
            this.btn_incluir.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_incluir.Image = global::SWS.Properties.Resources.fechar_ico;
            this.btn_incluir.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btn_incluir.Location = new System.Drawing.Point(9, 19);
            this.btn_incluir.Name = "btn_incluir";
            this.btn_incluir.Size = new System.Drawing.Size(75, 23);
            this.btn_incluir.TabIndex = 5;
            this.btn_incluir.Text = "&Incluir";
            this.btn_incluir.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btn_incluir.UseVisualStyleBackColor = true;
            this.btn_incluir.Click += new System.EventHandler(this.btn_incluir_Click);
            // 
            // btn_fechar
            // 
            this.btn_fechar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_fechar.Image = global::SWS.Properties.Resources.close;
            this.btn_fechar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btn_fechar.Location = new System.Drawing.Point(90, 19);
            this.btn_fechar.Name = "btn_fechar";
            this.btn_fechar.Size = new System.Drawing.Size(75, 23);
            this.btn_fechar.TabIndex = 7;
            this.btn_fechar.Text = "&Fechar";
            this.btn_fechar.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btn_fechar.UseVisualStyleBackColor = true;
            this.btn_fechar.Click += new System.EventHandler(this.btn_fechar_Click);
            // 
            // grd_funcao
            // 
            this.grd_funcao.AllowUserToAddRows = false;
            this.grd_funcao.AllowUserToDeleteRows = false;
            this.grd_funcao.AllowUserToOrderColumns = true;
            this.grd_funcao.AllowUserToResizeRows = false;
            this.grd_funcao.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this.grd_funcao.BackgroundColor = System.Drawing.Color.White;
            this.grd_funcao.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.grd_funcao.Dock = System.Windows.Forms.DockStyle.Fill;
            this.grd_funcao.Location = new System.Drawing.Point(3, 16);
            this.grd_funcao.MultiSelect = false;
            this.grd_funcao.Name = "grd_funcao";
            this.grd_funcao.ReadOnly = true;
            this.grd_funcao.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.grd_funcao.Size = new System.Drawing.Size(695, 206);
            this.grd_funcao.TabIndex = 4;
            // 
            // grb_dados
            // 
            this.grb_dados.Controls.Add(this.btn_limpar);
            this.grb_dados.Controls.Add(this.text_ghe);
            this.grb_dados.Controls.Add(this.lbl_ghe);
            this.grb_dados.Controls.Add(this.lbl_cliente);
            this.grb_dados.Controls.Add(this.text_cliente);
            this.grb_dados.Controls.Add(this.lbl_cbo);
            this.grb_dados.Controls.Add(this.text_cbo);
            this.grb_dados.Controls.Add(this.lbl_descricao);
            this.grb_dados.Controls.Add(this.btn_buscar);
            this.grb_dados.Controls.Add(this.text_descricao);
            this.grb_dados.Location = new System.Drawing.Point(4, 12);
            this.grb_dados.Name = "grb_dados";
            this.grb_dados.Size = new System.Drawing.Size(701, 141);
            this.grb_dados.TabIndex = 9;
            this.grb_dados.TabStop = false;
            this.grb_dados.Text = "Filtro";
            // 
            // btn_limpar
            // 
            this.btn_limpar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_limpar.Image = global::SWS.Properties.Resources.vassoura;
            this.btn_limpar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btn_limpar.Location = new System.Drawing.Point(136, 106);
            this.btn_limpar.Name = "btn_limpar";
            this.btn_limpar.Size = new System.Drawing.Size(59, 23);
            this.btn_limpar.TabIndex = 10;
            this.btn_limpar.Text = "&Limpar";
            this.btn_limpar.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btn_limpar.UseVisualStyleBackColor = true;
            this.btn_limpar.Click += new System.EventHandler(this.btn_limpar_Click);
            // 
            // text_ghe
            // 
            this.text_ghe.BackColor = System.Drawing.Color.White;
            this.text_ghe.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.text_ghe.Enabled = false;
            this.text_ghe.ForeColor = System.Drawing.Color.Black;
            this.text_ghe.Location = new System.Drawing.Point(71, 51);
            this.text_ghe.MaxLength = 100;
            this.text_ghe.Name = "text_ghe";
            this.text_ghe.ReadOnly = true;
            this.text_ghe.Size = new System.Drawing.Size(624, 20);
            this.text_ghe.TabIndex = 9;
            // 
            // lbl_ghe
            // 
            this.lbl_ghe.AutoSize = true;
            this.lbl_ghe.Location = new System.Drawing.Point(3, 53);
            this.lbl_ghe.Name = "lbl_ghe";
            this.lbl_ghe.Size = new System.Drawing.Size(30, 13);
            this.lbl_ghe.TabIndex = 8;
            this.lbl_ghe.Text = "GHE";
            // 
            // lbl_cliente
            // 
            this.lbl_cliente.AutoSize = true;
            this.lbl_cliente.Location = new System.Drawing.Point(3, 25);
            this.lbl_cliente.Name = "lbl_cliente";
            this.lbl_cliente.Size = new System.Drawing.Size(39, 13);
            this.lbl_cliente.TabIndex = 7;
            this.lbl_cliente.Text = "Cliente";
            // 
            // text_cliente
            // 
            this.text_cliente.BackColor = System.Drawing.Color.White;
            this.text_cliente.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.text_cliente.Enabled = false;
            this.text_cliente.ForeColor = System.Drawing.Color.Black;
            this.text_cliente.Location = new System.Drawing.Point(71, 23);
            this.text_cliente.MaxLength = 100;
            this.text_cliente.Name = "text_cliente";
            this.text_cliente.ReadOnly = true;
            this.text_cliente.Size = new System.Drawing.Size(624, 20);
            this.text_cliente.TabIndex = 6;
            // 
            // lbl_cbo
            // 
            this.lbl_cbo.AutoSize = true;
            this.lbl_cbo.Location = new System.Drawing.Point(500, 83);
            this.lbl_cbo.Name = "lbl_cbo";
            this.lbl_cbo.Size = new System.Drawing.Size(29, 13);
            this.lbl_cbo.TabIndex = 5;
            this.lbl_cbo.Text = "CBO";
            // 
            // text_cbo
            // 
            this.text_cbo.BackColor = System.Drawing.Color.White;
            this.text_cbo.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.text_cbo.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.text_cbo.ForeColor = System.Drawing.Color.Black;
            this.text_cbo.Location = new System.Drawing.Point(534, 80);
            this.text_cbo.MaxLength = 6;
            this.text_cbo.Name = "text_cbo";
            this.text_cbo.Size = new System.Drawing.Size(161, 20);
            this.text_cbo.TabIndex = 2;
            // 
            // lbl_descricao
            // 
            this.lbl_descricao.AutoSize = true;
            this.lbl_descricao.Location = new System.Drawing.Point(3, 82);
            this.lbl_descricao.Name = "lbl_descricao";
            this.lbl_descricao.Size = new System.Drawing.Size(43, 13);
            this.lbl_descricao.TabIndex = 3;
            this.lbl_descricao.Text = "Função";
            // 
            // btn_buscar
            // 
            this.btn_buscar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_buscar.Image = global::SWS.Properties.Resources.lupa;
            this.btn_buscar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btn_buscar.Location = new System.Drawing.Point(71, 106);
            this.btn_buscar.Name = "btn_buscar";
            this.btn_buscar.Size = new System.Drawing.Size(59, 23);
            this.btn_buscar.TabIndex = 3;
            this.btn_buscar.Text = "&Buscar";
            this.btn_buscar.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btn_buscar.UseVisualStyleBackColor = true;
            this.btn_buscar.Click += new System.EventHandler(this.btn_buscar_Click);
            // 
            // text_descricao
            // 
            this.text_descricao.BackColor = System.Drawing.Color.White;
            this.text_descricao.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.text_descricao.ForeColor = System.Drawing.Color.Black;
            this.text_descricao.Location = new System.Drawing.Point(71, 79);
            this.text_descricao.MaxLength = 100;
            this.text_descricao.Name = "text_descricao";
            this.text_descricao.Size = new System.Drawing.Size(422, 20);
            this.text_descricao.TabIndex = 1;
            // 
            // grb_grid
            // 
            this.grb_grid.Controls.Add(this.grd_funcao);
            this.grb_grid.Location = new System.Drawing.Point(4, 159);
            this.grb_grid.Name = "grb_grid";
            this.grb_grid.Size = new System.Drawing.Size(701, 225);
            this.grb_grid.TabIndex = 10;
            this.grb_grid.TabStop = false;
            this.grb_grid.Text = "Lista de Funções do GHE";
            // 
            // frm_PcmsoFuncaoGheSelecionar
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(717, 472);
            this.ControlBox = false;
            this.Controls.Add(this.grb_paginacao);
            this.Controls.Add(this.grb_dados);
            this.Controls.Add(this.grb_grid);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "frm_PcmsoFuncaoGheSelecionar";
            this.Text = "SELECIONAR SETOR FUNÇÃO DO GHE";
            this.grb_paginacao.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.grd_funcao)).EndInit();
            this.grb_dados.ResumeLayout(false);
            this.grb_dados.PerformLayout();
            this.grb_grid.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox grb_paginacao;
        private System.Windows.Forms.Button btn_incluir;
        private System.Windows.Forms.Button btn_fechar;
        private System.Windows.Forms.DataGridView grd_funcao;
        private System.Windows.Forms.GroupBox grb_dados;
        private System.Windows.Forms.Label lbl_descricao;
        private System.Windows.Forms.Button btn_buscar;
        private System.Windows.Forms.TextBox text_descricao;
        private System.Windows.Forms.GroupBox grb_grid;
        private System.Windows.Forms.TextBox text_ghe;
        private System.Windows.Forms.Label lbl_ghe;
        private System.Windows.Forms.Label lbl_cliente;
        private System.Windows.Forms.TextBox text_cliente;
        private System.Windows.Forms.Label lbl_cbo;
        private System.Windows.Forms.TextBox text_cbo;
        private System.Windows.Forms.Button btn_limpar;
    }
}