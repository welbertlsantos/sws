﻿using SWS.Entidade;
using SWS.Facade;
using SWS.View.ViewHelper;
using SWS.ViewHelper;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace SWS.View
{
    public partial class frmRelGestaoCredenciada : frmTemplateConsulta
    {
        private Cliente credenciadora;
        private Cliente credenciada;
        
        
        public frmRelGestaoCredenciada()
        {
            InitializeComponent();
            ComboHelper.situacaoMovimento(cbSituacao);
        }

        private void btnCredenciadora_Click(object sender, EventArgs e)
        {
            try
            {
                frmClienteSelecionar selecionarCredenciadora = new frmClienteSelecionar(null, null, null, null, null, null, true, false, null);
                selecionarCredenciadora.ShowDialog();

                if (selecionarCredenciadora.Cliente != null)
                {
                    credenciadora = selecionarCredenciadora.Cliente;
                    textCredenciadora.Text = credenciadora.RazaoSocial;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }

        private void btnExcluirCredenciadora_Click(object sender, EventArgs e)
        {
            try
            {
                if (credenciadora == null)
                    throw new Exception("Você não selecionou nenhuma credenciadora.");

                credenciadora = null;
                textCredenciadora.Text = string.Empty;

                    
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }

        private void btnImprimir_Click(object sender, EventArgs e)
        {
            try
            {
                if (validaTela())
                {
                    /* gerando relatorio */
                    this.Cursor = Cursors.WaitCursor;

                    string nomeRelatorio = "RELATORIO_GESTAO_CREDENCIADORA.JASPER";
                    string dataInicialMovimento = dataInicial.Value.ToShortDateString();
                    string dataFinalMovimento = dataFinal.Value.ToShortDateString();
                    string dataInicialAtendimento = dtInicialAtendimento.Value.ToShortDateString();
                    string dataFinalAtendimento = dtFinalAtendimento.Value.ToShortDateString();
                    string situacao = ((SelectItem)cbSituacao.SelectedItem).Valor;
                    long? idCredenciada = this.credenciada != null ? this.credenciada.Id : (long)0;

                    var process = new Process();

                    process.StartInfo.FileName = "Relatorio\\jre7\\bin\\java";
                    process.StartInfo.Arguments = " -jar Relatorio\\MiddlewareJasper.jar " + "Relatorio\\Report\\" + nomeRelatorio + " DT_INICIAL_MOVIMENTO:" + dataInicialMovimento + ":STRING" + " DT_FINAL_MOVIMENTO:" + dataFinalMovimento + ":STRING" + " ID_CLIENTE:" + credenciadora.Id + ":INTEGER" + " SITUACAO:" + situacao + ":STRING" + " ID_CREDENCIADA:" + idCredenciada + ":INTEGER" + " DT_INICIAL_ATENDIMENTO:" + dataInicialAtendimento + ":STRING" + " DT_FINAL_ATENDIMENTO:" + dataFinalAtendimento + ":STRING" + " ID_EMPRESA:" + PermissionamentoFacade.usuarioAutenticado.Empresa.Id + ":INTEGER";
                    process.StartInfo.UseShellExecute = false;
                    process.StartInfo.RedirectStandardError = true;
                    process.StartInfo.RedirectStandardInput = true;
                    process.StartInfo.RedirectStandardOutput = true;
                    process.StartInfo.CreateNoWindow = true;

                    process.Start();
                    process.StandardOutput.ReadToEnd();
                    process.WaitForExit();

                }

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }

        }

        private bool validaTela()
        {
            bool result = true;

            try
            {
                if (cbSituacao.SelectedIndex == 0)
                    throw new Exception("Selecione a situação do relatório.");

                if (credenciadora == null)
                    throw new Exception("Selecione uma credenciadora.");

                if (dataInicial.Checked && !dataFinal.Checked)
                    throw new Exception("Você deve selecionar a data final para a pesquisa.");

                if (dataFinal.Checked && !dataInicial.Checked)
                    throw new Exception("Você deve selecionar a data inicial para a pesquisa.");

                if (Convert.ToDateTime(dataInicial.Text + " 00:00:00") > Convert.ToDateTime(dataFinal.Text + " 23:59:59"))
                    throw new Exception("A data final deverá ser maior do que a data inicial");

                if (dtInicialAtendimento.Checked && !dtFinalAtendimento.Checked)
                    throw new Exception("Você deve selecionar a data final do atendimento.");

                if (dtFinalAtendimento.Checked && !dtInicialAtendimento.Checked)
                    throw new Exception("Você deve selecionar a data inicial do atendimento.");

                if (Convert.ToDateTime(dtInicialAtendimento.Text + " 00:00:00") > Convert.ToDateTime(dtFinalAtendimento.Text + " 23:59:59"))
                    throw new Exception("A data inicial do atendimento não pode ser maior que a data final do atendimento.");

            }
            catch (Exception ex)
            {
                result = false;
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            return result;

        }

        private void btnFechar_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnCredenciada_Click(object sender, EventArgs e)
        {
            try
            {
                if (this.credenciadora != null && this.credenciadora.Credenciadora != true)
                    throw new Exception("O cliente selecionado não é uma empresa credenciadora.");

                frmClienteCrendiadaSelecionar selecionarCredenciada = new frmClienteCrendiadaSelecionar(this.credenciadora);
                selecionarCredenciada.ShowDialog();

                if (selecionarCredenciada.Credenciada != null)
                {
                    this.credenciada = selecionarCredenciada.Credenciada;
                    this.textCredenciada.Text = this.credenciada.RazaoSocial;
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btnExcluirCredenciada_Click(object sender, EventArgs e)
        {
            try
            {
                if (this.credenciada == null)
                    throw new Exception("Você deve primeiro selecioanar a credenciada antes de excluí-la.");

                this.credenciada = null;
                this.textCredenciada.Text = string.Empty;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }


    }
}
