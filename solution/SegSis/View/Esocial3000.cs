﻿using SWS.Entidade;
using SWS.Facade;
using SWS.View.Resources;
using SWS.View.ViewHelper;
using SWS.ViewHelper;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SWS.View
{
    public partial class frmEsocial3000 : frmTemplate
    {
        private Cliente cliente;

        public Cliente Cliente
        {
            get { return cliente; }
            set { cliente = value; }
        }
        private ClienteFuncaoFuncionario clienteFuncaoFuncionario;

        public ClienteFuncaoFuncionario ClienteFuncaoFuncionario
        {
            get { return clienteFuncaoFuncionario; }
            set { clienteFuncaoFuncionario = value; }
        }

        private LoteEsocialMonitoramento lote2240;

        public LoteEsocialMonitoramento Lote2240
        {
            get { return lote2240; }
            set { lote2240 = value; }
        }
        private LoteEsocialAso lote2220;

        public LoteEsocialAso Lote2220
        {
            get { return lote2220; }
            set { lote2220 = value; }
        }

        private bool gerado;

        public bool Gerado
        {
            get { return gerado; }
            set { gerado = value; }
        }

        private string tipo;

        public string Tipo
        {
            get { return tipo; }
            set { tipo = value; }
        }

        private List<LoteEsocialAso> list2220ForCancel = new List<LoteEsocialAso>();
        private List<LoteEsocialMonitoramento> list2240ForCancel = new List<LoteEsocialMonitoramento>();
        
        public frmEsocial3000()
        {
            InitializeComponent();
            ComboHelper.tipoEvento(cbTipoEvento);
        }

        private void btIncluir_Click(object sender, EventArgs e)
        {
            try
            {
                if (dgvLoteCancelado.CurrentRow == null)
                    throw new Exception("Selecione uma linha da tabela.");

                this.Cursor = Cursors.WaitCursor;

                EsocialFacade esocialFacade = EsocialFacade.getInstance();

                /* verificando que tipo de evento será cancelado */
                if (string.Equals(((SelectItem)cbTipoEvento.SelectedItem).Nome, ApplicationConstants.TIPO_2220))
                {
                    if (MessageBox.Show("Deseja incluir o evento de cancelamento para o evento escolhido? ATENÇÃO: Essa operação não poderá ser desfeita.", "Atenção", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                    {
                        frmNumeroRecebidoEsocial incluirNumeroRecibo = new frmNumeroRecebidoEsocial();
                        incluirNumeroRecibo.ShowDialog();

                        if (!string.IsNullOrEmpty(incluirNumeroRecibo.NumeroRecibo))
                        {
                            lote2220 = esocialFacade.findLoteEsocialAsoById((long)dgvLoteCancelado.CurrentRow.Cells[0].Value);
                            lote2220.situacao = ApplicationConstants.LOTE_CANCELADO;
                            lote2220.dataCancelamento = DateTime.Now;
                            lote2220.nrebido = incluirNumeroRecibo.NumeroRecibo;
                            lote2220.usuarioCancelou = PermissionamentoFacade.usuarioAutenticado.Login;

                            esocialFacade.updateEsocialASo(lote2220);
                            MessageBox.Show("Evento de exclusão gravado com sucesso.", "Confirmação", MessageBoxButtons.OK, MessageBoxIcon.Information);
                            gerado = true;
                            tipo = ((SelectItem)cbTipoEvento.SelectedItem).Nome;
                            this.Close();
                        }
                    }
                }
                else
                {
                    if (MessageBox.Show("Deseja incluir o evento de cancelamento para o evento escolhido? ATENÇÃO: Essa operação não poderá ser desfeita.", "Atenção", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                    {
                        frmNumeroRecebidoEsocial incluirNumeroRecibo = new frmNumeroRecebidoEsocial();
                        incluirNumeroRecibo.ShowDialog();

                        if (!string.IsNullOrEmpty(incluirNumeroRecibo.NumeroRecibo))
                        {
                            lote2240 = esocialFacade.findLoteEsocialMonitoramentoById((long)this.dgvLoteCancelado.CurrentRow.Cells[0].Value);
                            lote2240.Situacao = ApplicationConstants.LOTE_CANCELADO;
                            lote2240.DataCancelamento = DateTime.Now;
                            lote2240.NRecibo = incluirNumeroRecibo.NumeroRecibo;
                            lote2240.UsuarioCancelou = PermissionamentoFacade.usuarioAutenticado.Login;

                            esocialFacade.updateEsocialMonitoramento(lote2240);
                            MessageBox.Show("Evento de exclusão gravado com sucesso.", "Confirmação", MessageBoxButtons.OK, MessageBoxIcon.Information);
                            gerado = true;
                            tipo = ((SelectItem)cbTipoEvento.SelectedItem).Nome;
                            this.Close();
                        }

                    }
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }

        }

        private void btPesquisar_Click(object sender, EventArgs e)
        {
            try
            {
                this.Cursor = Cursors.WaitCursor;
                if (validaCampoTela())
                {
                    EsocialFacade esocialFacade = EsocialFacade.getInstance();

                    if (String.Equals(((SelectItem)cbTipoEvento.SelectedItem).Nome, ApplicationConstants.TIPO_2220))
                    {
                        list2240ForCancel.Clear();
                        list2220ForCancel = esocialFacade.findAll2220ForCancelByFilter(
                            cliente,
                            clienteFuncaoFuncionario,
                            textCodigo.Text.Replace(".", "").Replace(",", "").Trim(),
                            Convert.ToDateTime(periodoInicial.Text + " 00:00:00"),
                            Convert.ToDateTime(periodoFinal.Text + " 23:59:59")
                            );
                        montaGrid2220();
                    }
                    else
                    {
                        list2220ForCancel.Clear();
                        list2240ForCancel = esocialFacade.findAll2240ForCancelByFilter(
                            cliente,
                            clienteFuncaoFuncionario,
                            textCodigo.Text.Replace(".", "").Replace(",", "").Trim(),
                            Convert.ToDateTime(periodoInicial.Text + " 00:00:00"),
                            Convert.ToDateTime(periodoFinal.Text + " 23:59:59")
                            );
                        montaGrid2240();
                    }

                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }

        }

        private void montaGrid2240()
        {
            try
            {
                this.Cursor = Cursors.WaitCursor;
                this.dgvLoteCancelado.Columns.Clear();
                this.dgvLoteCancelado.ColumnCount = 8;
                this.dgvLoteCancelado.Columns[0].HeaderText = "idEsociaMonitoramento";
                this.dgvLoteCancelado.Columns[0].Visible = false;
                this.dgvLoteCancelado.Columns[1].HeaderText = "idEsocialLote";
                this.dgvLoteCancelado.Columns[1].Visible = false;
                this.dgvLoteCancelado.Columns[2].HeaderText = "idMonitoramento";
                this.dgvLoteCancelado.Columns[2].Visible = false;
                this.dgvLoteCancelado.Columns[3].HeaderText = "Data de Gravação";
                this.dgvLoteCancelado.Columns[4].HeaderText = "Período";
                this.dgvLoteCancelado.Columns[5].HeaderText = "Código Lote";
                this.dgvLoteCancelado.Columns[6].HeaderText = "Funcionário";
                this.dgvLoteCancelado.Columns[7].HeaderText = "CPF";

                foreach (LoteEsocialMonitoramento lote in list2240ForCancel)
                {
                    dgvLoteCancelado.Rows.Add(
                        lote.Id,
                        lote.LoteEsocial.Id,
                        lote.Monitoramento.Id,
                        String.Format("{0:dd/MM/yyyy}", lote.LoteEsocial.DataCriacao),
                        String.Format("{0:MM/yyyy}", lote.Monitoramento.Periodo),
                        lote.LoteEsocial.codigo,
                        lote.Monitoramento.ClienteFuncaoFuncionario.Funcionario.Nome,
                        ValidaCampoHelper.FormataCpf(lote.Monitoramento.ClienteFuncaoFuncionario.Funcionario.Cpf)
                        );
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }

        private void montaGrid2220()
        {
            try
            {
                this.Cursor = Cursors.WaitCursor;
                this.dgvLoteCancelado.Columns.Clear();
                this.dgvLoteCancelado.ColumnCount = 9;

                this.dgvLoteCancelado.Columns[0].HeaderText = "idEsocialLoteAso";
                this.dgvLoteCancelado.Columns[0].Visible = false;
                this.dgvLoteCancelado.Columns[1].HeaderText = "idAso";
                this.dgvLoteCancelado.Columns[1].Visible = false;
                this.dgvLoteCancelado.Columns[2].HeaderText = "idEsocialLote";
                this.dgvLoteCancelado.Columns[2].Visible = false;
                this.dgvLoteCancelado.Columns[3].HeaderText = "Código";
                this.dgvLoteCancelado.Columns[4].HeaderText = "Data Criação";
                this.dgvLoteCancelado.Columns[5].HeaderText = "Codigo Atendimento";
                this.dgvLoteCancelado.Columns[6].HeaderText = "Funcionário";
                this.dgvLoteCancelado.Columns[7].HeaderText = "CPF";
                this.dgvLoteCancelado.Columns[8].HeaderText = "Tipo Atendimento";


                foreach (LoteEsocialAso lote in this.list2220ForCancel)
                {
                    dgvLoteCancelado.Rows.Add(
                        lote.Id,
                        lote.idAso,
                        lote.idLoteEsocialAso,
                        lote.loteEsocial.codigo,
                        String.Format("{0:dd/MM/yyyy}", lote.loteEsocial.DataCriacao),
                        lote.aso.Codigo,
                        lote.aso.Funcionario.Nome,
                        ValidaCampoHelper.FormataCpf(lote.aso.Funcionario.Cpf),
                        lote.aso.Periodicidade.Descricao
                        );
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }

        private void btnFechar_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnCliente_Click(object sender, EventArgs e)
        {
            try
            {
                frmClienteSelecionar selecionarCliente = new frmClienteSelecionar(null, null, false, null, null, null, null, null, true);
                selecionarCliente.ShowDialog();

                if (selecionarCliente.Cliente != null)
                {
                    this.cliente = selecionarCliente.Cliente;
                    textCliente.Text = this.cliente.RazaoSocial + " - " + (this.cliente.tipoCliente() == 1 ? ValidaCampoHelper.FormataCnpj(this.cliente.Cnpj) : ValidaCampoHelper.FormataCpf(this.cliente.Cnpj));

                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }

        private void btnExcluirCliente_Click(object sender, EventArgs e)
        {
            try
            {
                if (this.cliente == null)
                    throw new Exception("Você deve selecionar primeiro o cliente.");

                this.cliente = null;
                this.clienteFuncaoFuncionario = null;
                this.textFuncionario.Text = string.Empty;
                this.textCliente.Text = string.Empty;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btnExcluirFuncionario_Click(object sender, EventArgs e)
        {
            try
            {
                if (clienteFuncaoFuncionario == null)
                    throw new Exception("Selecione primeiro o funcionário.");

                clienteFuncaoFuncionario = null;
                this.textFuncionario.Text = string.Empty;

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }

        private void btnFuncionario_Click(object sender, EventArgs e)
        {
            try
            {
                if (cliente == null)
                    throw new Exception("Selecione um cliente primeiro.");

                frmClienteFuncaoFuncionarioPesquisa buscarFuncionario = new frmClienteFuncaoFuncionarioPesquisa(this.cliente);
                buscarFuncionario.ShowDialog();

                if (buscarFuncionario.ClienteFuncaoFuncionario != null)
                {
                    clienteFuncaoFuncionario = buscarFuncionario.ClienteFuncaoFuncionario;
                    textFuncionario.Text = clienteFuncaoFuncionario.Funcionario.Nome + " - " + ValidaCampoHelper.FormataCpf(clienteFuncaoFuncionario.Funcionario.Cpf);
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }

        private bool validaCampoTela()
        {
            bool retorno = true;
            try
            {
                if (periodoInicial.Checked && !periodoFinal.Checked)
                    throw new Exception("Você deve selecionar o período final da pesquisa.");

                if (!periodoInicial.Checked & periodoFinal.Checked)
                    throw new Exception("Você deve selecionar o período inicial da pesquisa.");

                if (!periodoInicial.Checked && !periodoFinal.Checked)
                    throw new Exception("Você deve selecionar algum período para pesquisa.");

                if (Convert.ToDateTime(periodoInicial.Text) > Convert.ToDateTime(periodoFinal.Text))
                    throw new Exception("O período inicial não pode ser maior que o período final");

                if (cliente == null)
                    throw new Exception("Você deve selecionar um cliente para a pesquisa");


            }
            catch (Exception ex)
            {
                retorno = false;
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

            return retorno;

        }

        private void frmEsocial3000_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter || e.KeyCode == Keys.Return)
                SendKeys.Send("{TAB}");
        }

    }
}
