﻿using SWS.Entidade;
using SWS.Facade;
using SWS.View.Resources;
using SWS.View.ViewHelper;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace SWS.View
{
    public partial class frmExamesExtras : frmTemplate
    {

        private Cliente cliente;
        private ClienteFuncao clienteFuncao;
        private Exame exame;
        
        public frmExamesExtras(ClienteFuncao clienteFuncao)
        {
            InitializeComponent();
            validaPermissoes();
            this.cliente = clienteFuncao == null ? null : clienteFuncao.Cliente;
            this.clienteFuncao = clienteFuncao;

            /* preenchendo dados da tela */

            if (clienteFuncao != null)
            {
                textCliente.Text = cliente.RazaoSocial;
                textFuncao.Text = clienteFuncao.Funcao.Descricao;
                montaGridExames();
                btCliente.Enabled = false;
                btFuncao.Enabled = false;

            }
        }

        private void validaPermissoes()
        {

            PermissionamentoFacade permissionamentoFacade = PermissionamentoFacade.getInstance();

            this.btIncluirExame.Enabled = permissionamentoFacade.hasPermission("CLIENTE_FUNCAO_EXAME", "INCLUIR");
            this.btIncluirPeriodicidade.Enabled = permissionamentoFacade.hasPermission("CLIENTE_FUNCAO_EXAME", "INCLUIR");

            this.btExcluirExame.Enabled = permissionamentoFacade.hasPermission("CLIENTE_FUNCAO_EXAME", "EXCLUIR");
            this.btExcluirPeriodicidade.Enabled = permissionamentoFacade.hasPermission("CLIENTE_FUNCAO_EXAME", "EXCLUIR");

        }

        private void btCliente_Click(object sender, EventArgs e)
        {
            frmClienteSelecionar formCliente = new frmClienteSelecionar(null, null, false, null, null, null, null, null, true);
            formCliente.ShowDialog();

            if (formCliente.Cliente != null)
            {
                cliente = formCliente.Cliente;
                textCliente.Text = cliente.RazaoSocial;
            }
        }

        private void btFuncao_Click(object sender, EventArgs e)
        {
            try
            {
                if (cliente == null)
                    throw new Exception("Selecione um cliente");
                
                frmExamesExtrasClienteFuncao pesquisaFuncao = new frmExamesExtrasClienteFuncao(cliente);
                pesquisaFuncao.ShowDialog();

                if (pesquisaFuncao.ClienteFuncao != null)
                {
                    clienteFuncao = pesquisaFuncao.ClienteFuncao;
                    textFuncao.Text = clienteFuncao.Funcao.Descricao;
                    montaGridExames();
                }
            }

            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        public void montaGridExames()
        {
            try
            {
                EstudoFacade pcmsoFacade = EstudoFacade.getInstance();

                dgvExame.Columns.Clear();
                dgvPeriodicidade.Columns.Clear();

                DataSet ds = pcmsoFacade.findAllExameAtivoInClienteFuncao(clienteFuncao);

                dgvExame.DataSource = ds.Tables["Exames"].DefaultView;

                dgvExame.Columns[0].HeaderText = "Código";
                dgvExame.Columns[0].ReadOnly = true;

                dgvExame.Columns[1].HeaderText = "Descrição";
                dgvExame.Columns[1].ReadOnly = true;
                
                dgvExame.Columns[2].HeaderText = "Situação";
                dgvExame.Columns[2].Visible = false;

                dgvExame.Columns[3].HeaderText = "Exame de Laboratório";
                dgvExame.Columns[3].ReadOnly = true;

                dgvExame.Columns[4].HeaderText = "Preço";
                dgvExame.Columns[4].Visible = false;

                dgvExame.Columns[5].HeaderText = "Prioridade";
                dgvExame.Columns[5].ReadOnly = true;

                dgvExame.Columns[6].HeaderText = "Custo";
                dgvExame.Columns[6].Visible = false;

                dgvExame.Columns[7].HeaderText = "Externo";
                dgvExame.Columns[7].ReadOnly = true;

                dgvExame.Columns[8].HeaderText = "Idade";
                dgvExame.Columns[8].DefaultCellStyle.BackColor = Color.White;
                
                dgvExame.Columns[9].HeaderText = "Documentação";
                dgvExame.Columns[9].Visible = false;

                dgvExame.Columns[10].HeaderText = "Periodo de Vencimento";
                dgvExame.Columns[10].Visible = false;

                dgvExame.Columns[11].HeaderText = "Padrão Contrato";
                dgvExame.Columns[11].Visible = false;

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        public void montaGridPeriodicidade()
        {
            try
            {
                EstudoFacade pcmsoFacade = EstudoFacade.getInstance();
                dgvPeriodicidade.Columns.Clear();

                ClienteFuncaoExame clienteFuncaoExame = pcmsoFacade.findClienteFuncaoExameByClienteFuncaoAndExame(clienteFuncao, exame);

                DataSet ds = pcmsoFacade.listaPeriodicidadesByClienteFuncaoExame(clienteFuncaoExame);

                dgvPeriodicidade.ColumnCount = 2;

                dgvPeriodicidade.Columns[0].HeaderText = "id_periodicidade";
                dgvPeriodicidade.Columns[0].Visible = false;

                dgvPeriodicidade.Columns[1].HeaderText = "Descrição";
                dgvPeriodicidade.Columns[1].Width = 200;
                dgvPeriodicidade.Columns[1].ReadOnly = true;
                
                DataGridViewComboBoxColumn periodoVencimento = new DataGridViewComboBoxColumn();
                dgvPeriodicidade.Columns.Add(periodoVencimento);
                ComboHelper.periodoVencimentoCombo(periodoVencimento);
                dgvPeriodicidade.Columns[2].HeaderText = "Vencimento";
                dgvPeriodicidade.Columns[2].DefaultCellStyle.BackColor = Color.White;
                dgvPeriodicidade.Columns[2].Width = 150;
                

                foreach (DataRow dvRow in ds.Tables["Periodicidades"].Rows)
                    dgvPeriodicidade.Rows.Add(Convert.ToInt64(dvRow["id_periodicidade"]), dvRow["descricao"].ToString(), dvRow["periodo_vencimento"] == null ? "" : dvRow["periodo_vencimento"].ToString());

            }
            catch (Exception ex)
            { 
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void dgvExame_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                if (dgvExame.CurrentRow == null)
                    throw new Exception("Selecione uma linha");

                exame = new Exame((Int64)dgvExame.CurrentRow.Cells[0].Value, dgvExame.CurrentRow.Cells[1].Value.ToString(), dgvExame.CurrentRow.Cells[2].Value.ToString(), (bool)dgvExame.CurrentRow.Cells[3].Value, (decimal)dgvExame.CurrentRow.Cells[4].Value, (int)dgvExame.CurrentRow.Cells[5].Value, (decimal)dgvExame.CurrentRow.Cells[6].Value, (bool)dgvExame.CurrentRow.Cells[7].Value, (bool)dgvExame.CurrentRow.Cells[9].Value, string.Empty, false, dgvExame.CurrentRow.Cells[10].Value == DBNull.Value ? (int?)null : Convert.ToInt32(dgvExame.CurrentRow.Cells[10].Value), (bool)dgvExame.CurrentRow.Cells[11].Value);

                montaGridPeriodicidade();

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void dgvExame_DataBindingComplete(object sender, DataGridViewBindingCompleteEventArgs e)
        {
            DataGridView dgv = sender as DataGridView;
            dgv.ClearSelection();
        }

        private void btIncluirExame_Click(object sender, EventArgs e)
        {
            try
            {
                if (clienteFuncao == null)
                    throw new Exception("Selecione primeiro um cliente e uma função");
                
                frmClienteFuncaoExameIncluir incluirExame = new frmClienteFuncaoExameIncluir(clienteFuncao);
                incluirExame.ShowDialog();

                if (incluirExame.ClienteFuncaoExame != null)
                    montaGridExames();

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btExcluirExame_Click(object sender, EventArgs e)
        {
            try
            {
                if (exame == null)
                    throw new Exception("Selecione o exame");

                if (MessageBox.Show("Deseja realmente excluir o exame selecionado", "Confirmação", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                {
                    EstudoFacade pcmsoFacade = EstudoFacade.getInstance();
                    
                    /* recuperando o clienteFuncaoExame */
                    ClienteFuncaoExame clienteFuncaoExame = pcmsoFacade.findClienteFuncaoExameByClienteFuncaoAndExame(clienteFuncao, exame);

                    if (pcmsoFacade.verificaClienteFuncaoExameFoiUsado(clienteFuncaoExame))
                        pcmsoFacade.disableClienteFuncaoExame(clienteFuncaoExame);
                    else
                        pcmsoFacade.deleteClienteFuncaoExame(clienteFuncaoExame);

                    //exame = null;
                    montaGridExames();
                    dgvPeriodicidade.Columns.Clear();
                    //montaGridPeriodicidade();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btIncluirPeriodicidade_Click(object sender, EventArgs e)
        {
            try
            {
                if (exame == null)
                    throw new Exception("Selecione primeiro um exame");

                /* recuperando o clienteFuncaoExame */
                EstudoFacade pcmsoFacade = EstudoFacade.getInstance();

                frmExamesExtrasPeriodicidadeIncluir incluirPeriodicidade = new frmExamesExtrasPeriodicidadeIncluir(pcmsoFacade.findClienteFuncaoExameByClienteFuncaoAndExame(clienteFuncao, exame));
                incluirPeriodicidade.ShowDialog();

                montaGridPeriodicidade();
                
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btExcluirPeriodicidade_Click(object sender, EventArgs e)
        {
            try
            {
                if (dgvPeriodicidade.CurrentRow == null)
                    throw new Exception("Selecione uma periodicidade.");

                if (exame == null)
                    throw new Exception("Selecione um exame.");


                if (MessageBox.Show("Deseja excluir a periodicade selecionada?", "Confirmação", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                {
                    EstudoFacade pcmsoFacade = EstudoFacade.getInstance();
                    pcmsoFacade.updateClienteFuncaoExamePeriodicidade(new ClienteFuncaoExamePeriodicidade(pcmsoFacade.findClienteFuncaoExameByClienteFuncaoAndExame(clienteFuncao, exame), new Periodicidade((long)dgvPeriodicidade.CurrentRow.Cells[0].Value, dgvPeriodicidade.CurrentRow.Cells[1].Value.ToString(), null), ApplicationConstants.DESATIVADO, null));
                    montaGridPeriodicidade();
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btn_fechar_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void dgvExame_CellEndEdit(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                if (dgvExame.Rows[e.RowIndex].Cells[e.ColumnIndex].Value.ToString().Length > 2)
                {
                    dgvExame.Rows[e.RowIndex].Cells[e.ColumnIndex].Value = dgvExame.Tag.ToString();
                    throw new Exception("Valor máximo para o campo é 99.");
                }
                
                EstudoFacade pcmsoFacade = EstudoFacade.getInstance();

                /* recuperando o clienteFuncaoExame */
                ClienteFuncaoExame clienteFuncaoExame = pcmsoFacade.findClienteFuncaoExameByClienteFuncaoAndExame(clienteFuncao, exame);
                clienteFuncaoExame.IdadeExame = (int)dgvExame.Rows[e.RowIndex].Cells[8].Value;
                pcmsoFacade.updateClienteFuncaoExame(clienteFuncaoExame);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void dgvExame_KeyPress(object sender, KeyPressEventArgs e)
        {
            try
            {
                e.Handled = ValidaCampoHelper.FormataCampoNumerico(sender, e);
                
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void dgvExame_EditingControlShowing(object sender, DataGridViewEditingControlShowingEventArgs e)
        {
            if (e.Control is DataGridViewTextBoxEditingControl)
                e.Control.KeyPress += new KeyPressEventHandler(this.dgvExame_KeyPress);

        }

        private void dgvExame_CellEnter(object sender, DataGridViewCellEventArgs e)
        {
            dgvExame.Tag = dgvExame.Rows[e.RowIndex].Cells[e.ColumnIndex].Value;
        }

        private void dgvPeriodicidade_CellEndEdit(object sender, DataGridViewCellEventArgs e)
        {
            if (e.ColumnIndex == 2)
            {
                if (dgvPeriodicidade.Rows[e.RowIndex].Cells[2].Value != string.Empty)
                {
                    EstudoFacade pcmsoFacade = EstudoFacade.getInstance();
                    /* alterado o objeto clienteFuncaoExamePeriodicidade */

                    ClienteFuncaoExamePeriodicidade clienteFuncaoExamePeriodicidade = new ClienteFuncaoExamePeriodicidade(pcmsoFacade.findClienteFuncaoExameByClienteFuncaoAndExame(clienteFuncao, exame), new Periodicidade((long)dgvPeriodicidade.Rows[e.RowIndex].Cells[0].Value, dgvPeriodicidade.Rows[e.RowIndex].Cells[1].Value.ToString(), null), ApplicationConstants.ATIVO, Convert.ToInt32(dgvPeriodicidade.Rows[e.RowIndex].Cells[2].Value));
                    pcmsoFacade.updateClienteFuncaoExamePeriodicidade(clienteFuncaoExamePeriodicidade);
                    MessageBox.Show("Alteração realizada com sucesso.", "Confirmação", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }
        }
        
    }
}
