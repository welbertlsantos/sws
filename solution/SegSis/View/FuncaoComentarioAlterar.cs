﻿using SWS.Entidade;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace SWS.View
{
    public partial class frmFuncaoComentarioAlterar : frmFuncaoComentarioIncluir
    {
        private ComentarioFuncao comentarioFuncao;

        public ComentarioFuncao ComentarioFuncao
        {
            get { return comentarioFuncao; }
            set { comentarioFuncao = value; }
        }
        
        public frmFuncaoComentarioAlterar(ComentarioFuncao comentarioFuncao) :base()
        {
            InitializeComponent();
            this.comentarioFuncao = comentarioFuncao;
            textAtividade.Text = comentarioFuncao.Atividade;
            ActiveControl = textAtividade;
        }

        protected override void btnGravar_Click(object sender, EventArgs e)
        {
            try
            {
                if (String.IsNullOrWhiteSpace(textAtividade.Text))
                    throw new Exception("A atividade não pode ser nula");

                ComentarioFuncao.Atividade = textAtividade.Text;
                this.Close();

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
    }
}
