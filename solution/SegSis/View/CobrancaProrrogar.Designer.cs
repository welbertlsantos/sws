﻿namespace SWS.View
{
    partial class frmCobrancaProrrogar
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.flpAcao = new System.Windows.Forms.FlowLayoutPanel();
            this.btnConfirmar = new System.Windows.Forms.Button();
            this.btnCancelar = new System.Windows.Forms.Button();
            this.textValorCobranca = new System.Windows.Forms.TextBox();
            this.lblValorCobranca = new System.Windows.Forms.TextBox();
            this.textDataVencimento = new System.Windows.Forms.TextBox();
            this.lblDataVencimento = new System.Windows.Forms.TextBox();
            this.textDataEmissao = new System.Windows.Forms.TextBox();
            this.lblDataEmissao = new System.Windows.Forms.TextBox();
            this.textNumeroNota = new System.Windows.Forms.TextBox();
            this.textCliente = new System.Windows.Forms.TextBox();
            this.textFormaPagamento = new System.Windows.Forms.TextBox();
            this.textParcela = new System.Windows.Forms.TextBox();
            this.textDocumento = new System.Windows.Forms.TextBox();
            this.lblCliente = new System.Windows.Forms.TextBox();
            this.lblFormaPgto = new System.Windows.Forms.TextBox();
            this.lblDocumento = new System.Windows.Forms.TextBox();
            this.lblParcela = new System.Windows.Forms.TextBox();
            this.lblNumeroNota = new System.Windows.Forms.TextBox();
            this.lblDataProrrogacao = new System.Windows.Forms.TextBox();
            this.lblJurosSugerido = new System.Windows.Forms.TextBox();
            this.lblValorCalculado = new System.Windows.Forms.TextBox();
            this.lblValorInformado = new System.Windows.Forms.TextBox();
            this.dataProrrogacao = new System.Windows.Forms.DateTimePicker();
            this.textJurosSugerido = new System.Windows.Forms.TextBox();
            this.textValorCalculado = new System.Windows.Forms.TextBox();
            this.textValorInformado = new System.Windows.Forms.TextBox();
            ((System.ComponentModel.ISupportInitialize)(this.scForm)).BeginInit();
            this.scForm.Panel1.SuspendLayout();
            this.scForm.Panel2.SuspendLayout();
            this.scForm.SuspendLayout();
            this.pnlForm.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.banner)).BeginInit();
            this.flpAcao.SuspendLayout();
            this.SuspendLayout();
            // 
            // scForm
            // 
            // 
            // scForm.Panel1
            // 
            this.scForm.Panel1.Controls.Add(this.flpAcao);
            // 
            // pnlForm
            // 
            this.pnlForm.Controls.Add(this.textValorInformado);
            this.pnlForm.Controls.Add(this.textValorCalculado);
            this.pnlForm.Controls.Add(this.textJurosSugerido);
            this.pnlForm.Controls.Add(this.lblDataProrrogacao);
            this.pnlForm.Controls.Add(this.dataProrrogacao);
            this.pnlForm.Controls.Add(this.lblValorInformado);
            this.pnlForm.Controls.Add(this.lblValorCalculado);
            this.pnlForm.Controls.Add(this.lblJurosSugerido);
            this.pnlForm.Controls.Add(this.textValorCobranca);
            this.pnlForm.Controls.Add(this.lblValorCobranca);
            this.pnlForm.Controls.Add(this.textDataVencimento);
            this.pnlForm.Controls.Add(this.lblDataVencimento);
            this.pnlForm.Controls.Add(this.textDataEmissao);
            this.pnlForm.Controls.Add(this.lblDataEmissao);
            this.pnlForm.Controls.Add(this.textNumeroNota);
            this.pnlForm.Controls.Add(this.textCliente);
            this.pnlForm.Controls.Add(this.textFormaPagamento);
            this.pnlForm.Controls.Add(this.textParcela);
            this.pnlForm.Controls.Add(this.textDocumento);
            this.pnlForm.Controls.Add(this.lblCliente);
            this.pnlForm.Controls.Add(this.lblFormaPgto);
            this.pnlForm.Controls.Add(this.lblDocumento);
            this.pnlForm.Controls.Add(this.lblParcela);
            this.pnlForm.Controls.Add(this.lblNumeroNota);
            // 
            // flpAcao
            // 
            this.flpAcao.Controls.Add(this.btnConfirmar);
            this.flpAcao.Controls.Add(this.btnCancelar);
            this.flpAcao.Dock = System.Windows.Forms.DockStyle.Fill;
            this.flpAcao.Location = new System.Drawing.Point(0, 0);
            this.flpAcao.Name = "flpAcao";
            this.flpAcao.Size = new System.Drawing.Size(534, 30);
            this.flpAcao.TabIndex = 0;
            // 
            // btnConfirmar
            // 
            this.btnConfirmar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnConfirmar.Image = global::SWS.Properties.Resources.fechar_ico;
            this.btnConfirmar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnConfirmar.Location = new System.Drawing.Point(3, 3);
            this.btnConfirmar.Name = "btnConfirmar";
            this.btnConfirmar.Size = new System.Drawing.Size(75, 23);
            this.btnConfirmar.TabIndex = 4;
            this.btnConfirmar.Text = "&Confirmar";
            this.btnConfirmar.UseVisualStyleBackColor = true;
            this.btnConfirmar.Click += new System.EventHandler(this.btnConfirmar_Click);
            // 
            // btnCancelar
            // 
            this.btnCancelar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnCancelar.Image = global::SWS.Properties.Resources.close;
            this.btnCancelar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnCancelar.Location = new System.Drawing.Point(84, 3);
            this.btnCancelar.Name = "btnCancelar";
            this.btnCancelar.Size = new System.Drawing.Size(75, 23);
            this.btnCancelar.TabIndex = 5;
            this.btnCancelar.Text = "C&ancelar";
            this.btnCancelar.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnCancelar.UseVisualStyleBackColor = true;
            this.btnCancelar.Click += new System.EventHandler(this.btnCancelar_Click);
            // 
            // textValorCobranca
            // 
            this.textValorCobranca.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.textValorCobranca.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textValorCobranca.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textValorCobranca.Location = new System.Drawing.Point(143, 145);
            this.textValorCobranca.Name = "textValorCobranca";
            this.textValorCobranca.ReadOnly = true;
            this.textValorCobranca.Size = new System.Drawing.Size(364, 21);
            this.textValorCobranca.TabIndex = 63;
            this.textValorCobranca.TabStop = false;
            // 
            // lblValorCobranca
            // 
            this.lblValorCobranca.BackColor = System.Drawing.Color.LightGray;
            this.lblValorCobranca.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblValorCobranca.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblValorCobranca.Location = new System.Drawing.Point(8, 145);
            this.lblValorCobranca.Name = "lblValorCobranca";
            this.lblValorCobranca.ReadOnly = true;
            this.lblValorCobranca.Size = new System.Drawing.Size(136, 21);
            this.lblValorCobranca.TabIndex = 62;
            this.lblValorCobranca.TabStop = false;
            this.lblValorCobranca.Text = "Valor R$";
            // 
            // textDataVencimento
            // 
            this.textDataVencimento.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.textDataVencimento.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textDataVencimento.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textDataVencimento.Location = new System.Drawing.Point(143, 125);
            this.textDataVencimento.Name = "textDataVencimento";
            this.textDataVencimento.ReadOnly = true;
            this.textDataVencimento.Size = new System.Drawing.Size(364, 21);
            this.textDataVencimento.TabIndex = 61;
            this.textDataVencimento.TabStop = false;
            // 
            // lblDataVencimento
            // 
            this.lblDataVencimento.BackColor = System.Drawing.Color.LightGray;
            this.lblDataVencimento.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblDataVencimento.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDataVencimento.Location = new System.Drawing.Point(8, 125);
            this.lblDataVencimento.Name = "lblDataVencimento";
            this.lblDataVencimento.ReadOnly = true;
            this.lblDataVencimento.Size = new System.Drawing.Size(136, 21);
            this.lblDataVencimento.TabIndex = 60;
            this.lblDataVencimento.TabStop = false;
            this.lblDataVencimento.Text = "Data de Vencimento";
            // 
            // textDataEmissao
            // 
            this.textDataEmissao.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.textDataEmissao.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textDataEmissao.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textDataEmissao.Location = new System.Drawing.Point(143, 105);
            this.textDataEmissao.Name = "textDataEmissao";
            this.textDataEmissao.ReadOnly = true;
            this.textDataEmissao.Size = new System.Drawing.Size(364, 21);
            this.textDataEmissao.TabIndex = 59;
            this.textDataEmissao.TabStop = false;
            // 
            // lblDataEmissao
            // 
            this.lblDataEmissao.BackColor = System.Drawing.Color.LightGray;
            this.lblDataEmissao.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblDataEmissao.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDataEmissao.Location = new System.Drawing.Point(8, 105);
            this.lblDataEmissao.Name = "lblDataEmissao";
            this.lblDataEmissao.ReadOnly = true;
            this.lblDataEmissao.Size = new System.Drawing.Size(136, 21);
            this.lblDataEmissao.TabIndex = 58;
            this.lblDataEmissao.TabStop = false;
            this.lblDataEmissao.Text = "Data de Emissão";
            // 
            // textNumeroNota
            // 
            this.textNumeroNota.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.textNumeroNota.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textNumeroNota.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textNumeroNota.Location = new System.Drawing.Point(143, 5);
            this.textNumeroNota.Name = "textNumeroNota";
            this.textNumeroNota.ReadOnly = true;
            this.textNumeroNota.Size = new System.Drawing.Size(364, 21);
            this.textNumeroNota.TabIndex = 57;
            this.textNumeroNota.TabStop = false;
            // 
            // textCliente
            // 
            this.textCliente.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.textCliente.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textCliente.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textCliente.Location = new System.Drawing.Point(143, 85);
            this.textCliente.Name = "textCliente";
            this.textCliente.ReadOnly = true;
            this.textCliente.Size = new System.Drawing.Size(364, 21);
            this.textCliente.TabIndex = 56;
            this.textCliente.TabStop = false;
            // 
            // textFormaPagamento
            // 
            this.textFormaPagamento.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.textFormaPagamento.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textFormaPagamento.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textFormaPagamento.Location = new System.Drawing.Point(143, 65);
            this.textFormaPagamento.Name = "textFormaPagamento";
            this.textFormaPagamento.ReadOnly = true;
            this.textFormaPagamento.Size = new System.Drawing.Size(364, 21);
            this.textFormaPagamento.TabIndex = 55;
            this.textFormaPagamento.TabStop = false;
            // 
            // textParcela
            // 
            this.textParcela.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.textParcela.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textParcela.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textParcela.Location = new System.Drawing.Point(143, 45);
            this.textParcela.Name = "textParcela";
            this.textParcela.ReadOnly = true;
            this.textParcela.Size = new System.Drawing.Size(364, 21);
            this.textParcela.TabIndex = 54;
            this.textParcela.TabStop = false;
            // 
            // textDocumento
            // 
            this.textDocumento.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.textDocumento.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textDocumento.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textDocumento.Location = new System.Drawing.Point(143, 25);
            this.textDocumento.Name = "textDocumento";
            this.textDocumento.ReadOnly = true;
            this.textDocumento.Size = new System.Drawing.Size(364, 21);
            this.textDocumento.TabIndex = 53;
            this.textDocumento.TabStop = false;
            // 
            // lblCliente
            // 
            this.lblCliente.BackColor = System.Drawing.Color.LightGray;
            this.lblCliente.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblCliente.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCliente.Location = new System.Drawing.Point(8, 85);
            this.lblCliente.Name = "lblCliente";
            this.lblCliente.ReadOnly = true;
            this.lblCliente.Size = new System.Drawing.Size(136, 21);
            this.lblCliente.TabIndex = 52;
            this.lblCliente.TabStop = false;
            this.lblCliente.Text = "Cliente";
            // 
            // lblFormaPgto
            // 
            this.lblFormaPgto.BackColor = System.Drawing.Color.LightGray;
            this.lblFormaPgto.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblFormaPgto.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblFormaPgto.Location = new System.Drawing.Point(8, 65);
            this.lblFormaPgto.Name = "lblFormaPgto";
            this.lblFormaPgto.ReadOnly = true;
            this.lblFormaPgto.Size = new System.Drawing.Size(136, 21);
            this.lblFormaPgto.TabIndex = 51;
            this.lblFormaPgto.TabStop = false;
            this.lblFormaPgto.Text = "Forma Pagamento";
            // 
            // lblDocumento
            // 
            this.lblDocumento.BackColor = System.Drawing.Color.LightGray;
            this.lblDocumento.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblDocumento.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDocumento.Location = new System.Drawing.Point(8, 25);
            this.lblDocumento.Name = "lblDocumento";
            this.lblDocumento.ReadOnly = true;
            this.lblDocumento.Size = new System.Drawing.Size(136, 21);
            this.lblDocumento.TabIndex = 50;
            this.lblDocumento.TabStop = false;
            this.lblDocumento.Text = "Documento";
            // 
            // lblParcela
            // 
            this.lblParcela.BackColor = System.Drawing.Color.LightGray;
            this.lblParcela.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblParcela.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblParcela.Location = new System.Drawing.Point(8, 45);
            this.lblParcela.Name = "lblParcela";
            this.lblParcela.ReadOnly = true;
            this.lblParcela.Size = new System.Drawing.Size(136, 21);
            this.lblParcela.TabIndex = 49;
            this.lblParcela.TabStop = false;
            this.lblParcela.Text = "Parcela";
            // 
            // lblNumeroNota
            // 
            this.lblNumeroNota.BackColor = System.Drawing.Color.LightGray;
            this.lblNumeroNota.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblNumeroNota.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNumeroNota.Location = new System.Drawing.Point(8, 5);
            this.lblNumeroNota.Name = "lblNumeroNota";
            this.lblNumeroNota.ReadOnly = true;
            this.lblNumeroNota.Size = new System.Drawing.Size(136, 21);
            this.lblNumeroNota.TabIndex = 48;
            this.lblNumeroNota.TabStop = false;
            this.lblNumeroNota.Text = "Número da NF";
            // 
            // lblDataProrrogacao
            // 
            this.lblDataProrrogacao.BackColor = System.Drawing.Color.LightGray;
            this.lblDataProrrogacao.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblDataProrrogacao.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDataProrrogacao.Location = new System.Drawing.Point(8, 172);
            this.lblDataProrrogacao.Name = "lblDataProrrogacao";
            this.lblDataProrrogacao.ReadOnly = true;
            this.lblDataProrrogacao.Size = new System.Drawing.Size(136, 21);
            this.lblDataProrrogacao.TabIndex = 64;
            this.lblDataProrrogacao.TabStop = false;
            this.lblDataProrrogacao.Text = "Data de Prorrogação";
            // 
            // lblJurosSugerido
            // 
            this.lblJurosSugerido.BackColor = System.Drawing.Color.LightGray;
            this.lblJurosSugerido.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblJurosSugerido.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblJurosSugerido.Location = new System.Drawing.Point(8, 192);
            this.lblJurosSugerido.Name = "lblJurosSugerido";
            this.lblJurosSugerido.ReadOnly = true;
            this.lblJurosSugerido.Size = new System.Drawing.Size(136, 21);
            this.lblJurosSugerido.TabIndex = 65;
            this.lblJurosSugerido.TabStop = false;
            this.lblJurosSugerido.Text = "Juros/ mês sugerido %";
            // 
            // lblValorCalculado
            // 
            this.lblValorCalculado.BackColor = System.Drawing.Color.LightGray;
            this.lblValorCalculado.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblValorCalculado.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblValorCalculado.Location = new System.Drawing.Point(8, 212);
            this.lblValorCalculado.Name = "lblValorCalculado";
            this.lblValorCalculado.ReadOnly = true;
            this.lblValorCalculado.Size = new System.Drawing.Size(136, 21);
            this.lblValorCalculado.TabIndex = 66;
            this.lblValorCalculado.TabStop = false;
            this.lblValorCalculado.Text = "Valor Calculado";
            // 
            // lblValorInformado
            // 
            this.lblValorInformado.BackColor = System.Drawing.Color.LightGray;
            this.lblValorInformado.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblValorInformado.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblValorInformado.Location = new System.Drawing.Point(8, 232);
            this.lblValorInformado.Name = "lblValorInformado";
            this.lblValorInformado.ReadOnly = true;
            this.lblValorInformado.Size = new System.Drawing.Size(136, 21);
            this.lblValorInformado.TabIndex = 67;
            this.lblValorInformado.TabStop = false;
            this.lblValorInformado.Text = "Valor Informado";
            // 
            // dataProrrogacao
            // 
            this.dataProrrogacao.CalendarFont = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dataProrrogacao.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dataProrrogacao.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dataProrrogacao.Location = new System.Drawing.Point(143, 172);
            this.dataProrrogacao.Name = "dataProrrogacao";
            this.dataProrrogacao.ShowCheckBox = true;
            this.dataProrrogacao.Size = new System.Drawing.Size(364, 21);
            this.dataProrrogacao.TabIndex = 3;
            this.dataProrrogacao.ValueChanged += new System.EventHandler(this.dataProrrogacao_ValueChanged);
            // 
            // textJurosSugerido
            // 
            this.textJurosSugerido.BackColor = System.Drawing.Color.White;
            this.textJurosSugerido.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textJurosSugerido.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textJurosSugerido.Location = new System.Drawing.Point(143, 192);
            this.textJurosSugerido.Name = "textJurosSugerido";
            this.textJurosSugerido.Size = new System.Drawing.Size(364, 21);
            this.textJurosSugerido.TabIndex = 2;
            this.textJurosSugerido.Enter += new System.EventHandler(this.textValorJugerido_Enter);
            this.textJurosSugerido.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.textJurosSugerido_KeyPress);
            this.textJurosSugerido.Leave += new System.EventHandler(this.textJurosSugerido_Leave);
            // 
            // textValorCalculado
            // 
            this.textValorCalculado.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.textValorCalculado.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textValorCalculado.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textValorCalculado.ForeColor = System.Drawing.Color.Blue;
            this.textValorCalculado.Location = new System.Drawing.Point(143, 212);
            this.textValorCalculado.Name = "textValorCalculado";
            this.textValorCalculado.ReadOnly = true;
            this.textValorCalculado.Size = new System.Drawing.Size(364, 21);
            this.textValorCalculado.TabIndex = 70;
            this.textValorCalculado.TabStop = false;
            // 
            // textValorInformado
            // 
            this.textValorInformado.BackColor = System.Drawing.Color.White;
            this.textValorInformado.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textValorInformado.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textValorInformado.Location = new System.Drawing.Point(143, 232);
            this.textValorInformado.Name = "textValorInformado";
            this.textValorInformado.Size = new System.Drawing.Size(364, 21);
            this.textValorInformado.TabIndex = 1;
            this.textValorInformado.Enter += new System.EventHandler(this.textValorInformado_Enter);
            this.textValorInformado.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.textValorInformado_KeyPress);
            this.textValorInformado.Leave += new System.EventHandler(this.textValorInformado_Leave);
            // 
            // frmCobrancaProrrogar
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(534, 361);
            this.Name = "frmCobrancaProrrogar";
            this.Text = "PRORROGAR COBRANÇA";
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.frmCobrancaProrrogar_KeyDown);
            this.scForm.Panel1.ResumeLayout(false);
            this.scForm.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.scForm)).EndInit();
            this.scForm.ResumeLayout(false);
            this.pnlForm.ResumeLayout(false);
            this.pnlForm.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.banner)).EndInit();
            this.flpAcao.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.FlowLayoutPanel flpAcao;
        private System.Windows.Forms.Button btnConfirmar;
        private System.Windows.Forms.Button btnCancelar;
        private System.Windows.Forms.TextBox textValorInformado;
        private System.Windows.Forms.TextBox textValorCalculado;
        private System.Windows.Forms.TextBox textJurosSugerido;
        private System.Windows.Forms.TextBox lblDataProrrogacao;
        private System.Windows.Forms.DateTimePicker dataProrrogacao;
        private System.Windows.Forms.TextBox lblValorInformado;
        private System.Windows.Forms.TextBox lblValorCalculado;
        private System.Windows.Forms.TextBox lblJurosSugerido;
        private System.Windows.Forms.TextBox textValorCobranca;
        private System.Windows.Forms.TextBox lblValorCobranca;
        private System.Windows.Forms.TextBox textDataVencimento;
        private System.Windows.Forms.TextBox lblDataVencimento;
        private System.Windows.Forms.TextBox textDataEmissao;
        private System.Windows.Forms.TextBox lblDataEmissao;
        private System.Windows.Forms.TextBox textNumeroNota;
        private System.Windows.Forms.TextBox textCliente;
        private System.Windows.Forms.TextBox textFormaPagamento;
        private System.Windows.Forms.TextBox textParcela;
        private System.Windows.Forms.TextBox textDocumento;
        private System.Windows.Forms.TextBox lblCliente;
        private System.Windows.Forms.TextBox lblFormaPgto;
        private System.Windows.Forms.TextBox lblDocumento;
        private System.Windows.Forms.TextBox lblParcela;
        private System.Windows.Forms.TextBox lblNumeroNota;
    }
}