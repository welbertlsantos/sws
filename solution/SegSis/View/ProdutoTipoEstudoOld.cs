﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using SWS.Facade;
using SWS.Entidade;

namespace SWS.View
{
    public partial class frm_ProdutoTipoEstudoOld : Form
    {
        private String tipoEstudoProduto;
        private static String msg1 = "Selecione uma linha.";

        private Produto produtoSelecionado;

        public frm_ProdutoTipoEstudoOld(String tipoEstudo)
        {
            InitializeComponent();
            tipoEstudoProduto = tipoEstudo;
            montaDataGrid();
        }

        public Produto getProduto()
        {
            return this.produtoSelecionado;
        }

        public void montaDataGrid()
        {
            try
            {
                FinanceiroFacade financeiroFacade = FinanceiroFacade.getInstance();

                DataSet ds = financeiroFacade.findProdutoByTipoEstudo(tipoEstudoProduto);

                this.dg_produto.DataSource = ds.Tables["Produtos"].DefaultView;

                dg_produto.Columns[0].HeaderText = "Id_produto";
                dg_produto.Columns[0].Visible = false;

                dg_produto.Columns[1].HeaderText = "Descrição";
                
                dg_produto.Columns[2].HeaderText = "Situação";
                dg_produto.Columns[2].Visible = false;
                
                dg_produto.Columns[3].HeaderText = "Preço de Venda";
                dg_produto.Columns[3].Visible = false;
                
                dg_produto.Columns[4].HeaderText = "FlagCarga";
                dg_produto.Columns[4].Visible = false;
                
                dg_produto.Columns[5].HeaderText = "Preço de Custo";
                dg_produto.Columns[5].Visible = false;
                
                dg_produto.Columns[6].HeaderText = "Tipo de Estudo";
                dg_produto.Columns[6].Visible = false;

            }
            catch (Exception ex)
            {
                MessageBox.Show (ex.Message);
            }
        }

        private void frm_ProdutoTipoEstudo_Load(object sender, EventArgs e)
        {

        }

        private void btn_fechar_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btn_confirma_Click(object sender, EventArgs e)
        {
            try
            {
                this.Cursor = Cursors.WaitCursor;

                if (dg_produto.CurrentRow != null)
                {

                    FinanceiroFacade financeiroFacede = FinanceiroFacade.getInstance();

                    Int64 id = (Int64)dg_produto.CurrentRow.Cells[0].Value;

                    produtoSelecionado = financeiroFacede.findProdutoById(id);

                    this.Close();

                }
                else
                {
                    throw new Exception(msg1);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }

        private void dg_produto_CellMouseDoubleClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            try
            {
                btn_confirma.PerformClick();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }
    }
}
