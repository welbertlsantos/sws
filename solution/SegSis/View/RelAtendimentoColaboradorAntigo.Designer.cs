﻿namespace SWS.View
{
    partial class frmRelAtendimentoColaboradorAntigo
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.grbColaborador = new System.Windows.Forms.GroupBox();
            this.btnColaborador = new System.Windows.Forms.Button();
            this.textColaborador = new System.Windows.Forms.TextBox();
            this.grbCliente = new System.Windows.Forms.GroupBox();
            this.btnCliente = new System.Windows.Forms.Button();
            this.textCliente = new System.Windows.Forms.TextBox();
            this.grbPeriodo = new System.Windows.Forms.GroupBox();
            this.dataFinal = new System.Windows.Forms.DateTimePicker();
            this.lblData = new System.Windows.Forms.Label();
            this.dataInicial = new System.Windows.Forms.DateTimePicker();
            this.grbSituacao = new System.Windows.Forms.GroupBox();
            this.rbPendente = new System.Windows.Forms.RadioButton();
            this.rbFinalizado = new System.Windows.Forms.RadioButton();
            this.rbTodos = new System.Windows.Forms.RadioButton();
            this.grbAcao = new System.Windows.Forms.GroupBox();
            this.btnFechar = new System.Windows.Forms.Button();
            this.btnImprimir = new System.Windows.Forms.Button();
            this.grbColaborador.SuspendLayout();
            this.grbCliente.SuspendLayout();
            this.grbPeriodo.SuspendLayout();
            this.grbSituacao.SuspendLayout();
            this.grbAcao.SuspendLayout();
            this.SuspendLayout();
            // 
            // grbColaborador
            // 
            this.grbColaborador.Controls.Add(this.btnColaborador);
            this.grbColaborador.Controls.Add(this.textColaborador);
            this.grbColaborador.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.grbColaborador.Location = new System.Drawing.Point(13, 13);
            this.grbColaborador.Name = "grbColaborador";
            this.grbColaborador.Size = new System.Drawing.Size(575, 51);
            this.grbColaborador.TabIndex = 0;
            this.grbColaborador.TabStop = false;
            this.grbColaborador.Text = "Dados do colaborador";
            // 
            // btnColaborador
            // 
            this.btnColaborador.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnColaborador.Image = global::SWS.Properties.Resources.lupa;
            this.btnColaborador.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnColaborador.Location = new System.Drawing.Point(494, 19);
            this.btnColaborador.Name = "btnColaborador";
            this.btnColaborador.Size = new System.Drawing.Size(75, 23);
            this.btnColaborador.TabIndex = 1;
            this.btnColaborador.Text = "&Buscar";
            this.btnColaborador.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnColaborador.UseVisualStyleBackColor = true;
            this.btnColaborador.Click += new System.EventHandler(this.btnColaborador_Click);
            // 
            // textColaborador
            // 
            this.textColaborador.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textColaborador.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textColaborador.ForeColor = System.Drawing.SystemColors.WindowText;
            this.textColaborador.Location = new System.Drawing.Point(7, 20);
            this.textColaborador.MaxLength = 100;
            this.textColaborador.Multiline = true;
            this.textColaborador.Name = "textColaborador";
            this.textColaborador.ReadOnly = true;
            this.textColaborador.Size = new System.Drawing.Size(481, 23);
            this.textColaborador.TabIndex = 0;
            this.textColaborador.TabStop = false;
            // 
            // grbCliente
            // 
            this.grbCliente.Controls.Add(this.btnCliente);
            this.grbCliente.Controls.Add(this.textCliente);
            this.grbCliente.Enabled = false;
            this.grbCliente.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.grbCliente.Location = new System.Drawing.Point(13, 71);
            this.grbCliente.Name = "grbCliente";
            this.grbCliente.Size = new System.Drawing.Size(575, 51);
            this.grbCliente.TabIndex = 1;
            this.grbCliente.TabStop = false;
            this.grbCliente.Text = "Dados do Cliente";
            // 
            // btnCliente
            // 
            this.btnCliente.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnCliente.Image = global::SWS.Properties.Resources.lupa;
            this.btnCliente.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnCliente.Location = new System.Drawing.Point(496, 15);
            this.btnCliente.Name = "btnCliente";
            this.btnCliente.Size = new System.Drawing.Size(75, 23);
            this.btnCliente.TabIndex = 2;
            this.btnCliente.Text = "&Buscar";
            this.btnCliente.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnCliente.UseVisualStyleBackColor = true;
            this.btnCliente.Click += new System.EventHandler(this.btnCliente_Click);
            // 
            // textCliente
            // 
            this.textCliente.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textCliente.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textCliente.ForeColor = System.Drawing.SystemColors.WindowText;
            this.textCliente.Location = new System.Drawing.Point(6, 15);
            this.textCliente.MaxLength = 100;
            this.textCliente.Multiline = true;
            this.textCliente.Name = "textCliente";
            this.textCliente.ReadOnly = true;
            this.textCliente.Size = new System.Drawing.Size(482, 23);
            this.textCliente.TabIndex = 2;
            this.textCliente.TabStop = false;
            // 
            // grbPeriodo
            // 
            this.grbPeriodo.Controls.Add(this.dataFinal);
            this.grbPeriodo.Controls.Add(this.lblData);
            this.grbPeriodo.Controls.Add(this.dataInicial);
            this.grbPeriodo.Location = new System.Drawing.Point(13, 129);
            this.grbPeriodo.Name = "grbPeriodo";
            this.grbPeriodo.Size = new System.Drawing.Size(248, 55);
            this.grbPeriodo.TabIndex = 2;
            this.grbPeriodo.TabStop = false;
            this.grbPeriodo.Text = "Período";
            // 
            // dataFinal
            // 
            this.dataFinal.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dataFinal.Location = new System.Drawing.Point(137, 19);
            this.dataFinal.Name = "dataFinal";
            this.dataFinal.Size = new System.Drawing.Size(104, 20);
            this.dataFinal.TabIndex = 4;
            // 
            // lblData
            // 
            this.lblData.AutoSize = true;
            this.lblData.Location = new System.Drawing.Point(113, 22);
            this.lblData.Name = "lblData";
            this.lblData.Size = new System.Drawing.Size(23, 13);
            this.lblData.TabIndex = 1;
            this.lblData.Text = "Até";
            // 
            // dataInicial
            // 
            this.dataInicial.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dataInicial.Location = new System.Drawing.Point(7, 19);
            this.dataInicial.Name = "dataInicial";
            this.dataInicial.Size = new System.Drawing.Size(104, 20);
            this.dataInicial.TabIndex = 3;
            // 
            // grbSituacao
            // 
            this.grbSituacao.Controls.Add(this.rbPendente);
            this.grbSituacao.Controls.Add(this.rbFinalizado);
            this.grbSituacao.Controls.Add(this.rbTodos);
            this.grbSituacao.Location = new System.Drawing.Point(13, 202);
            this.grbSituacao.Name = "grbSituacao";
            this.grbSituacao.Size = new System.Drawing.Size(248, 99);
            this.grbSituacao.TabIndex = 3;
            this.grbSituacao.TabStop = false;
            this.grbSituacao.Text = "Situaçao do Atendimento";
            // 
            // rbPendente
            // 
            this.rbPendente.AutoSize = true;
            this.rbPendente.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.rbPendente.Location = new System.Drawing.Point(7, 66);
            this.rbPendente.Name = "rbPendente";
            this.rbPendente.Size = new System.Drawing.Size(75, 17);
            this.rbPendente.TabIndex = 7;
            this.rbPendente.TabStop = true;
            this.rbPendente.Text = "Pendentes";
            this.rbPendente.UseVisualStyleBackColor = true;
            // 
            // rbFinalizado
            // 
            this.rbFinalizado.AutoSize = true;
            this.rbFinalizado.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.rbFinalizado.Location = new System.Drawing.Point(7, 43);
            this.rbFinalizado.Name = "rbFinalizado";
            this.rbFinalizado.Size = new System.Drawing.Size(76, 17);
            this.rbFinalizado.TabIndex = 6;
            this.rbFinalizado.TabStop = true;
            this.rbFinalizado.Text = "Finalizados";
            this.rbFinalizado.UseVisualStyleBackColor = true;
            // 
            // rbTodos
            // 
            this.rbTodos.AutoSize = true;
            this.rbTodos.Checked = true;
            this.rbTodos.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.rbTodos.Location = new System.Drawing.Point(7, 20);
            this.rbTodos.Name = "rbTodos";
            this.rbTodos.Size = new System.Drawing.Size(54, 17);
            this.rbTodos.TabIndex = 5;
            this.rbTodos.TabStop = true;
            this.rbTodos.Text = "Todos";
            this.rbTodos.UseVisualStyleBackColor = true;
            // 
            // grbAcao
            // 
            this.grbAcao.Controls.Add(this.btnFechar);
            this.grbAcao.Controls.Add(this.btnImprimir);
            this.grbAcao.Location = new System.Drawing.Point(13, 337);
            this.grbAcao.Name = "grbAcao";
            this.grbAcao.Size = new System.Drawing.Size(575, 51);
            this.grbAcao.TabIndex = 4;
            this.grbAcao.TabStop = false;
            this.grbAcao.Text = "Ação";
            // 
            // btnFechar
            // 
            this.btnFechar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnFechar.Image = global::SWS.Properties.Resources.close;
            this.btnFechar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnFechar.Location = new System.Drawing.Point(89, 19);
            this.btnFechar.Name = "btnFechar";
            this.btnFechar.Size = new System.Drawing.Size(75, 23);
            this.btnFechar.TabIndex = 9;
            this.btnFechar.Text = "&Fechar";
            this.btnFechar.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnFechar.UseVisualStyleBackColor = true;
            this.btnFechar.Click += new System.EventHandler(this.btnFechar_Click);
            // 
            // btnImprimir
            // 
            this.btnImprimir.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnImprimir.Image = global::SWS.Properties.Resources.impressora1;
            this.btnImprimir.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnImprimir.Location = new System.Drawing.Point(8, 19);
            this.btnImprimir.Name = "btnImprimir";
            this.btnImprimir.Size = new System.Drawing.Size(75, 23);
            this.btnImprimir.TabIndex = 8;
            this.btnImprimir.Text = "&Imprimir";
            this.btnImprimir.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnImprimir.UseVisualStyleBackColor = true;
            this.btnImprimir.Click += new System.EventHandler(this.btnImprimir_Click);
            // 
            // frmRelAtendimentoColaboradorAntigo
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(600, 400);
            this.Controls.Add(this.grbAcao);
            this.Controls.Add(this.grbSituacao);
            this.Controls.Add(this.grbPeriodo);
            this.Controls.Add(this.grbCliente);
            this.Controls.Add(this.grbColaborador);
            this.Name = "frmRelAtendimentoColaboradorAntigo";
            this.Text = "ATENDIMENTO POR COLABORADOR";
            this.grbColaborador.ResumeLayout(false);
            this.grbColaborador.PerformLayout();
            this.grbCliente.ResumeLayout(false);
            this.grbCliente.PerformLayout();
            this.grbPeriodo.ResumeLayout(false);
            this.grbPeriodo.PerformLayout();
            this.grbSituacao.ResumeLayout(false);
            this.grbSituacao.PerformLayout();
            this.grbAcao.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox grbColaborador;
        private System.Windows.Forms.Button btnColaborador;
        private System.Windows.Forms.TextBox textColaborador;
        private System.Windows.Forms.GroupBox grbCliente;
        private System.Windows.Forms.Button btnCliente;
        private System.Windows.Forms.TextBox textCliente;
        private System.Windows.Forms.GroupBox grbPeriodo;
        private System.Windows.Forms.DateTimePicker dataFinal;
        private System.Windows.Forms.Label lblData;
        private System.Windows.Forms.DateTimePicker dataInicial;
        private System.Windows.Forms.GroupBox grbSituacao;
        private System.Windows.Forms.RadioButton rbPendente;
        private System.Windows.Forms.RadioButton rbFinalizado;
        private System.Windows.Forms.RadioButton rbTodos;
        private System.Windows.Forms.GroupBox grbAcao;
        private System.Windows.Forms.Button btnFechar;
        private System.Windows.Forms.Button btnImprimir;
    }
}