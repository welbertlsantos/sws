﻿using SWS.Entidade;
using SWS.Facade;
using SWS.View.ViewHelper;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace SWS.View
{
    public partial class frmFuncionarioDetalhar : frmFuncionarioIncluir
    {
        public frmFuncionarioDetalhar(Funcionario funcionario)
        {
            InitializeComponent();
            this.funcionario = funcionario;

            /* preenchendo dados da tela */

            textNome.Text = funcionario.Nome;
            textNome.ReadOnly = true;
            textCpf.Text = funcionario.Cpf;
            textCpf.ReadOnly = true;
            textRg.Text = funcionario.Rg;
            textRg.ReadOnly = true;
            dataNascimento.Text = Convert.ToString(funcionario.DataNascimento);
            dataNascimento.Enabled = false;
            textIdade.Text = ValidaCampoHelper.CalculaIdade(funcionario.DataNascimento).ToString();
            textIdade.ReadOnly = true;
            ComboHelper.startSexoAlterar(cbSexo);
            cbSexo.SelectedValue = funcionario.Sexo;
            cbSexo.Enabled = false;
            textEmail.Text = funcionario.Email;
            textEmail.ReadOnly = true;
            ComboHelper.TipoSague(cbTipoSanguineo);
            cbTipoSanguineo.SelectedValue = funcionario.TipoSangue;
            cbTipoSanguineo.Enabled = false;
            ComboHelper.FatorRH(cbFatorRh);
            cbFatorRh.SelectedValue = funcionario.FatorRh;
            cbFatorRh.Enabled = false;
            textPeso.Text = funcionario.Peso.ToString();
            textPeso.ReadOnly = true;
            textAltura.Text = funcionario.Altura.ToString();
            textAltura.ReadOnly = true;
            textCtps.Text = funcionario.Ctps;
            textCtps.ReadOnly = true;
            textPis.Text = funcionario.PisPasep;
            textPis.ReadOnly = true;

            textLogradouro.Text = funcionario.Endereco;
            textLogradouro.ReadOnly = true;
            textNumero.Text = funcionario.Numero;
            textNumero.ReadOnly = true;
            textComplemento.Text = funcionario.Complemento;
            textComplemento.ReadOnly = true;
            textBairro.Text = funcionario.Bairro;
            textBairro.ReadOnly = true;
            textCEP.Text = funcionario.Cep;
            textCEP.ReadOnly = true;
            ComboHelper.unidadeFederativa(cbUF);
            cbUF.SelectedValue = funcionario.Uf;
            cbUF.Enabled = false;
            cidade = funcionario.Cidade;
            if (cidade != null)
                textCidade.Text = cidade.Nome;
            textCidade.ReadOnly = true;
            btCidadeComercial.Enabled = false;
            textTelefone.Text = funcionario.Telefone1;
            textTelefone.ReadOnly = true;
            textCelular.Text = funcionario.Telefone2;
            textCelular.ReadOnly = true;
            cbPcd.SelectedValue = funcionario.Pcd ? "true" : "false";
            cbPcd.Enabled = false;

            textOrgaoEmissor.Text = funcionario.OrgaoEmissor;
            cbUfEmissor.SelectedValue = funcionario.UfEmissor;


            /* montando coleção de clientes que o funcionário foi cadastrado */

            FuncionarioFacade funcionarioFacade = FuncionarioFacade.getInstance();
            clienteFuncaoFuncionarioList = funcionarioFacade.findClienteFuncaoFuncionarioByFuncionarioAndSituacao(funcionario, false);
            montaGridCliente();
            btIncluirCliente.Enabled = false;
            btExcluirCliente.Enabled = false;
            btAlterarCliente.Enabled = false;

            /* montando coleção das funções que o funcionário foi cadastrado */
            btGravar.Enabled = false;
            btLimpar.Enabled = false;
        }
    }
}
