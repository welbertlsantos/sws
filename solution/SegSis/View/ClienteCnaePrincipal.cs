﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using SegSis.Entidade;
using SegSis.View.ViewHelper;
using SegSis.Facade;


namespace SegSis.View
{
    public partial class frm_clienteCnaePrincipalAlterar : BaseForm
    {
        Cnae cnae = null;
        HashSet<Cnae> cnaes = null;

        public Cnae getCnae()
        {
            return this.cnae;
        }
        
        public frm_clienteCnaePrincipalAlterar(HashSet<Cnae> cnaes)
        {
            InitializeComponent();
            this.cnaes = cnaes;
            montaDataGrid();
        }

        public void montaDataGrid()
        {

            foreach (Cnae cnae in cnaes)
            {

                grd_cnae.ColumnCount = 3;

                grd_cnae.Columns[0].Name = "ID";
                grd_cnae.Columns[0].Visible = false;

                grd_cnae.Columns[1].Name = "Código Cnae";
                grd_cnae.Columns[2].Name = "Atividade";

                this.grd_cnae.Rows.Add(cnae.getId(), cnae.getCodCnae(), cnae.getAtividade());
            }
        }

        private void btn_ok_Click(object sender, EventArgs e)
        {
            try
            {
                this.Cursor = Cursors.Default;
                
                ClienteFacade clienteFacade = ClienteFacade.getInstance();

                cnae = clienteFacade.findCnaeById((Int64)grd_cnae.CurrentRow.Cells[0].Value);

                this.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }

    }
}
