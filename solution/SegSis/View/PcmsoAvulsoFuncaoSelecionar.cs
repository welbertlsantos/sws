﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using SegSis.Entidade;
using SegSis.View.Resources;
using SegSis.Facade;

namespace SegSis.View
{
    public partial class frm_PcmsoAvulsoFuncaoSelecionar : BaseFormConsulta
    {
        frm_PcmsoAvulso formPcmsoAvulso;
        Cliente clienteProcurado;
        Funcao funcaoSelecionada;
        GheSetor gheSetor;
        ClienteFuncao clienteFuncao;

        public static String msg1 = "Caso a função participe de alguma atividade em espaço confinado, marque essa caixa ";
        public static String msg2 = "Caso a função participe de alguma atividade em altura, marque essa caixa ";

        
        public frm_PcmsoAvulsoFuncaoSelecionar(frm_PcmsoAvulso formPcmsoAvulso)
        {
            InitializeComponent();
            this.formPcmsoAvulso = formPcmsoAvulso;
            clienteProcurado = formPcmsoAvulso.cliente;
            gheSetor = formPcmsoAvulso.gheSetor;
        }
        
        private void text_cbo_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!Char.IsDigit(e.KeyChar) && e.KeyChar != (char)8)
            {
                e.Handled = true;
            }
        }
        
        public void montaDataGrid()
        {
            PcmsoFacade pcmsoFacade = PcmsoFacade.getInstance();
            ClienteFacade clienteFacade = ClienteFacade.getInstance();

            gheSetor = pcmsoFacade.findGheSetorByGheSetor(new GheSetor(null, formPcmsoAvulso.setor, formPcmsoAvulso.ghe, ApplicationConstants.ATIVO));

            Funcao funcao = new Funcao();

            grd_funcao.Columns.Clear();

            funcao.setDescricao(text_descricao.Text.ToUpper());
            funcao.setCodCbo(text_cbo.Text);

            DataSet ds = clienteFacade.findFuncaoByClienteFuncaoNotInGheSetor(new ClienteFuncao(null, formPcmsoAvulso.cliente, funcao), gheSetor);
                
            grd_funcao.DataSource = ds.Tables["Funcoes"].DefaultView;

            grd_funcao.Columns[0].HeaderText = "id_seg_cliente_funcao";
            grd_funcao.Columns[0].Visible = false;

            grd_funcao.Columns[1].HeaderText = "id_funcao";
            grd_funcao.Columns[1].Visible = false;

            grd_funcao.Columns[2].HeaderText = "Descrição";
            grd_funcao.Columns[2].ReadOnly = true;

            grd_funcao.Columns[3].HeaderText = "CBO";
            grd_funcao.Columns[3].ReadOnly = true;
            
            DataGridViewCheckBoxColumn select = new DataGridViewCheckBoxColumn();
            select.Name = "Selec";

            grd_funcao.Columns.Add(select);
            grd_funcao.Columns[4].HeaderText = "Selec";
            grd_funcao.Columns[4].ReadOnly = false;
            grd_funcao.Columns[4].DisplayIndex = 0;

            DataGridViewCheckBoxColumn confinado = new DataGridViewCheckBoxColumn();
            confinado.Name = "Confinado";

            grd_funcao.Columns.Add(confinado);
            grd_funcao.Columns[5].HeaderText = "Confinado";
            grd_funcao.Columns[5].ReadOnly = false;

            DataGridViewCheckBoxColumn altura = new DataGridViewCheckBoxColumn();
            altura.Name = "Altura";

            grd_funcao.Columns.Add(altura);
            grd_funcao.Columns[6].HeaderText = "Altura";
            grd_funcao.Columns[6].ReadOnly = false;

            foreach (DataGridViewRow dvRow in grd_funcao.Rows)
            {
                dvRow.Cells[4].Value = false;
                dvRow.Cells[5].Value = false;
                dvRow.Cells[6].Value = false;
            }

        }

        private void btn_buscar_Click(object sender, EventArgs e)
        {
            try
            {
                montaDataGrid();
                text_descricao.Focus();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void btn_incluir_Click(object sender, EventArgs e)
        {
            try
            {
                PcmsoFacade pcmsoFacade = PcmsoFacade.getInstance();

                foreach (DataGridViewRow dvRow in grd_funcao.Rows)
                {
                    if ((Boolean)dvRow.Cells[4].Value == true)
                    {
                        pcmsoFacade.incluirGheSetorClienteFuncao(new GheSetorClienteFuncao(
                            null,
                            new ClienteFuncao((Int64)dvRow.Cells[0].Value, clienteProcurado, new Funcao((Int64)dvRow.Cells[1].Value, (String)dvRow.Cells[2].Value, (String)dvRow.Cells[3].Value, String.Empty, String.Empty), ApplicationConstants.ATIVO, null, null),
                            gheSetor,
                            (Boolean)dvRow.Cells[5].Value == true ? true : false,
                            (Boolean)dvRow.Cells[6].Value == true ? true : false));
                    }
                    
                }
                
                formPcmsoAvulso.montaGridFuncao();
                
                this.Close();
                
                
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void btn_fechar_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void Novo_Click(object sender, EventArgs e)
        {
            try
            {
                this.Cursor = Cursors.WaitCursor;

                PpraFacade ppraFacade = PpraFacade.getInstance();
                PcmsoFacade pcmsoFacade = PcmsoFacade.getInstance();

                frm_funcao_incluir formFuncaoInclui = new frm_funcao_incluir();
                formFuncaoInclui.ShowDialog();

                funcaoSelecionada = formFuncaoInclui.getFuncao();

                if (funcaoSelecionada != null)
                {
                    clienteFuncao = new ClienteFuncao(null, clienteProcurado, funcaoSelecionada, ApplicationConstants.ATIVO, null, null);
                    ppraFacade.incluirClienteFuncao(clienteFuncao);

                    GheSetorClienteFuncao gheSetorClienteFuncao = new GheSetorClienteFuncao();
                    gheSetorClienteFuncao.setClienteFuncao(clienteFuncao);
                    gheSetorClienteFuncao.setGheSetor(gheSetor);

                    pcmsoFacade.incluirGheSetorClienteFuncao(gheSetorClienteFuncao);

                    montaDataGrid();

                    if (formPcmsoAvulso != null)
                    {
                        formPcmsoAvulso.montaGridFuncao();
                    }
                }

                this.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }

        private void grd_funcao_CellFormatting(object sender, DataGridViewCellFormattingEventArgs e)
        {
            if (e.ColumnIndex == 5)
            {
                DataGridViewCell cell = this.grd_funcao.Rows[e.RowIndex].Cells[e.ColumnIndex];

                cell.ToolTipText = msg1;
            }

            else if (e.ColumnIndex == 6)
            {
                DataGridViewCell cell = this.grd_funcao.Rows[e.RowIndex].Cells[e.ColumnIndex];

                cell.ToolTipText = msg2;
            }

        }

        
    }
}
