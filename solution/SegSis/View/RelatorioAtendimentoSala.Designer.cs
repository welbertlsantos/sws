﻿namespace SWS.View
{
    partial class frmRelatorioAtendimentoSala
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.flpAcao = new System.Windows.Forms.FlowLayoutPanel();
            this.btnImprimir = new System.Windows.Forms.Button();
            this.btnFechar = new System.Windows.Forms.Button();
            this.lblPeriodo = new System.Windows.Forms.TextBox();
            this.dataInicial = new System.Windows.Forms.DateTimePicker();
            this.dataFinal = new System.Windows.Forms.DateTimePicker();
            this.btSalaExcluir = new System.Windows.Forms.Button();
            this.textSala = new System.Windows.Forms.TextBox();
            this.btSalaIncluir = new System.Windows.Forms.Button();
            this.lblSala = new System.Windows.Forms.TextBox();
            this.btExaminadorExcluir = new System.Windows.Forms.Button();
            this.textExaminador = new System.Windows.Forms.TextBox();
            this.btExaminadorIncluir = new System.Windows.Forms.Button();
            this.lblExaminador = new System.Windows.Forms.TextBox();
            this.cbSituacaoAtendimento = new SWS.ComboBoxWithBorder();
            this.lblSituacao = new System.Windows.Forms.TextBox();
            ((System.ComponentModel.ISupportInitialize)(this.scForm)).BeginInit();
            this.scForm.Panel1.SuspendLayout();
            this.scForm.Panel2.SuspendLayout();
            this.scForm.SuspendLayout();
            this.pnlForm.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.banner)).BeginInit();
            this.flpAcao.SuspendLayout();
            this.SuspendLayout();
            // 
            // scForm
            // 
            // 
            // scForm.Panel1
            // 
            this.scForm.Panel1.Controls.Add(this.flpAcao);
            // 
            // pnlForm
            // 
            this.pnlForm.Controls.Add(this.cbSituacaoAtendimento);
            this.pnlForm.Controls.Add(this.lblSituacao);
            this.pnlForm.Controls.Add(this.btExaminadorExcluir);
            this.pnlForm.Controls.Add(this.textExaminador);
            this.pnlForm.Controls.Add(this.btExaminadorIncluir);
            this.pnlForm.Controls.Add(this.lblExaminador);
            this.pnlForm.Controls.Add(this.btSalaExcluir);
            this.pnlForm.Controls.Add(this.textSala);
            this.pnlForm.Controls.Add(this.btSalaIncluir);
            this.pnlForm.Controls.Add(this.lblSala);
            this.pnlForm.Controls.Add(this.lblPeriodo);
            this.pnlForm.Controls.Add(this.dataInicial);
            this.pnlForm.Controls.Add(this.dataFinal);
            // 
            // flpAcao
            // 
            this.flpAcao.Controls.Add(this.btnImprimir);
            this.flpAcao.Controls.Add(this.btnFechar);
            this.flpAcao.Dock = System.Windows.Forms.DockStyle.Fill;
            this.flpAcao.Location = new System.Drawing.Point(0, 0);
            this.flpAcao.Name = "flpAcao";
            this.flpAcao.Size = new System.Drawing.Size(534, 30);
            this.flpAcao.TabIndex = 0;
            // 
            // btnImprimir
            // 
            this.btnImprimir.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnImprimir.Image = global::SWS.Properties.Resources.impressora2;
            this.btnImprimir.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnImprimir.Location = new System.Drawing.Point(3, 3);
            this.btnImprimir.Name = "btnImprimir";
            this.btnImprimir.Size = new System.Drawing.Size(75, 23);
            this.btnImprimir.TabIndex = 2;
            this.btnImprimir.Text = "&Imprimir";
            this.btnImprimir.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnImprimir.UseVisualStyleBackColor = true;
            this.btnImprimir.Click += new System.EventHandler(this.btnImprimir_Click);
            // 
            // btnFechar
            // 
            this.btnFechar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnFechar.Image = global::SWS.Properties.Resources.close;
            this.btnFechar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnFechar.Location = new System.Drawing.Point(84, 3);
            this.btnFechar.Name = "btnFechar";
            this.btnFechar.Size = new System.Drawing.Size(75, 23);
            this.btnFechar.TabIndex = 3;
            this.btnFechar.Text = "&Fechar";
            this.btnFechar.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnFechar.UseVisualStyleBackColor = true;
            this.btnFechar.Click += new System.EventHandler(this.btnFechar_Click);
            // 
            // lblPeriodo
            // 
            this.lblPeriodo.BackColor = System.Drawing.Color.LightGray;
            this.lblPeriodo.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblPeriodo.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPeriodo.Location = new System.Drawing.Point(3, 3);
            this.lblPeriodo.Name = "lblPeriodo";
            this.lblPeriodo.ReadOnly = true;
            this.lblPeriodo.Size = new System.Drawing.Size(93, 21);
            this.lblPeriodo.TabIndex = 103;
            this.lblPeriodo.TabStop = false;
            this.lblPeriodo.Text = "Período";
            // 
            // dataInicial
            // 
            this.dataInicial.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dataInicial.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dataInicial.Location = new System.Drawing.Point(95, 3);
            this.dataInicial.Name = "dataInicial";
            this.dataInicial.ShowCheckBox = true;
            this.dataInicial.Size = new System.Drawing.Size(107, 21);
            this.dataInicial.TabIndex = 104;
            // 
            // dataFinal
            // 
            this.dataFinal.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dataFinal.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dataFinal.Location = new System.Drawing.Point(199, 3);
            this.dataFinal.Name = "dataFinal";
            this.dataFinal.ShowCheckBox = true;
            this.dataFinal.Size = new System.Drawing.Size(107, 21);
            this.dataFinal.TabIndex = 105;
            // 
            // btSalaExcluir
            // 
            this.btSalaExcluir.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btSalaExcluir.Image = global::SWS.Properties.Resources.close;
            this.btSalaExcluir.Location = new System.Drawing.Point(455, 23);
            this.btSalaExcluir.Name = "btSalaExcluir";
            this.btSalaExcluir.Size = new System.Drawing.Size(30, 21);
            this.btSalaExcluir.TabIndex = 115;
            this.btSalaExcluir.UseVisualStyleBackColor = true;
            this.btSalaExcluir.Click += new System.EventHandler(this.btSalaExcluir_Click);
            // 
            // textSala
            // 
            this.textSala.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.textSala.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textSala.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textSala.Location = new System.Drawing.Point(95, 23);
            this.textSala.Name = "textSala";
            this.textSala.ReadOnly = true;
            this.textSala.Size = new System.Drawing.Size(328, 21);
            this.textSala.TabIndex = 114;
            this.textSala.TabStop = false;
            // 
            // btSalaIncluir
            // 
            this.btSalaIncluir.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btSalaIncluir.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btSalaIncluir.Image = global::SWS.Properties.Resources.busca;
            this.btSalaIncluir.Location = new System.Drawing.Point(422, 23);
            this.btSalaIncluir.Name = "btSalaIncluir";
            this.btSalaIncluir.Size = new System.Drawing.Size(34, 21);
            this.btSalaIncluir.TabIndex = 112;
            this.btSalaIncluir.UseVisualStyleBackColor = true;
            this.btSalaIncluir.Click += new System.EventHandler(this.btSalaIncluir_Click);
            // 
            // lblSala
            // 
            this.lblSala.BackColor = System.Drawing.Color.LightGray;
            this.lblSala.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblSala.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSala.Location = new System.Drawing.Point(3, 23);
            this.lblSala.Name = "lblSala";
            this.lblSala.ReadOnly = true;
            this.lblSala.Size = new System.Drawing.Size(93, 21);
            this.lblSala.TabIndex = 113;
            this.lblSala.TabStop = false;
            this.lblSala.Text = "Sala";
            // 
            // btExaminadorExcluir
            // 
            this.btExaminadorExcluir.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btExaminadorExcluir.Image = global::SWS.Properties.Resources.close;
            this.btExaminadorExcluir.Location = new System.Drawing.Point(455, 43);
            this.btExaminadorExcluir.Name = "btExaminadorExcluir";
            this.btExaminadorExcluir.Size = new System.Drawing.Size(30, 21);
            this.btExaminadorExcluir.TabIndex = 119;
            this.btExaminadorExcluir.UseVisualStyleBackColor = true;
            this.btExaminadorExcluir.Click += new System.EventHandler(this.btExaminadorExcluir_Click);
            // 
            // textExaminador
            // 
            this.textExaminador.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.textExaminador.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textExaminador.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textExaminador.Location = new System.Drawing.Point(95, 43);
            this.textExaminador.Name = "textExaminador";
            this.textExaminador.ReadOnly = true;
            this.textExaminador.Size = new System.Drawing.Size(328, 21);
            this.textExaminador.TabIndex = 118;
            this.textExaminador.TabStop = false;
            // 
            // btExaminadorIncluir
            // 
            this.btExaminadorIncluir.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btExaminadorIncluir.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btExaminadorIncluir.Image = global::SWS.Properties.Resources.busca;
            this.btExaminadorIncluir.Location = new System.Drawing.Point(422, 43);
            this.btExaminadorIncluir.Name = "btExaminadorIncluir";
            this.btExaminadorIncluir.Size = new System.Drawing.Size(34, 21);
            this.btExaminadorIncluir.TabIndex = 116;
            this.btExaminadorIncluir.UseVisualStyleBackColor = true;
            this.btExaminadorIncluir.Click += new System.EventHandler(this.btExaminadorIncluir_Click);
            // 
            // lblExaminador
            // 
            this.lblExaminador.BackColor = System.Drawing.Color.LightGray;
            this.lblExaminador.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblExaminador.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblExaminador.Location = new System.Drawing.Point(3, 43);
            this.lblExaminador.Name = "lblExaminador";
            this.lblExaminador.ReadOnly = true;
            this.lblExaminador.Size = new System.Drawing.Size(93, 21);
            this.lblExaminador.TabIndex = 117;
            this.lblExaminador.TabStop = false;
            this.lblExaminador.Text = "Examinador";
            // 
            // cbSituacaoAtendimento
            // 
            this.cbSituacaoAtendimento.BackColor = System.Drawing.Color.LightGray;
            this.cbSituacaoAtendimento.BorderColor = System.Drawing.Color.DimGray;
            this.cbSituacaoAtendimento.BorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid;
            this.cbSituacaoAtendimento.FormattingEnabled = true;
            this.cbSituacaoAtendimento.Location = new System.Drawing.Point(95, 63);
            this.cbSituacaoAtendimento.Name = "cbSituacaoAtendimento";
            this.cbSituacaoAtendimento.Size = new System.Drawing.Size(390, 21);
            this.cbSituacaoAtendimento.TabIndex = 121;
            // 
            // lblSituacao
            // 
            this.lblSituacao.BackColor = System.Drawing.Color.LightGray;
            this.lblSituacao.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblSituacao.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSituacao.Location = new System.Drawing.Point(3, 63);
            this.lblSituacao.Name = "lblSituacao";
            this.lblSituacao.ReadOnly = true;
            this.lblSituacao.Size = new System.Drawing.Size(93, 21);
            this.lblSituacao.TabIndex = 120;
            this.lblSituacao.TabStop = false;
            this.lblSituacao.Text = "Situação";
            // 
            // frmRelatorioAtendimentoSala
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(534, 361);
            this.Name = "frmRelatorioAtendimentoSala";
            this.Text = "RELATÓRIO DE ATENDIMENTO POR SALA";
            this.scForm.Panel1.ResumeLayout(false);
            this.scForm.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.scForm)).EndInit();
            this.scForm.ResumeLayout(false);
            this.pnlForm.ResumeLayout(false);
            this.pnlForm.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.banner)).EndInit();
            this.flpAcao.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.FlowLayoutPanel flpAcao;
        private System.Windows.Forms.Button btnImprimir;
        private System.Windows.Forms.Button btnFechar;
        private System.Windows.Forms.TextBox lblPeriodo;
        private System.Windows.Forms.DateTimePicker dataInicial;
        private System.Windows.Forms.DateTimePicker dataFinal;
        private System.Windows.Forms.Button btExaminadorExcluir;
        private System.Windows.Forms.TextBox textExaminador;
        private System.Windows.Forms.Button btExaminadorIncluir;
        protected System.Windows.Forms.TextBox lblExaminador;
        private System.Windows.Forms.Button btSalaExcluir;
        private System.Windows.Forms.TextBox textSala;
        private System.Windows.Forms.Button btSalaIncluir;
        protected System.Windows.Forms.TextBox lblSala;
        private ComboBoxWithBorder cbSituacaoAtendimento;
        private System.Windows.Forms.TextBox lblSituacao;
    }
}