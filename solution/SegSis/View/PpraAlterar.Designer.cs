﻿namespace SWS.View
{
    partial class frm_ppraAlterar
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frm_ppraAlterar));
            this.grb_paginacao.SuspendLayout();
            this.grb_dados.SuspendLayout();
            this.grb_grupo1.SuspendLayout();
            this.grb_cliente.SuspendLayout();
            this.grb_tecnico.SuspendLayout();
            this.grb_data.SuspendLayout();
            this.tb_dados.SuspendLayout();
            this.tb_dadosPPRA.SuspendLayout();
            this.tb_cronograma.SuspendLayout();
            this.grb_grauRisco.SuspendLayout();
            this.tb_ghe.SuspendLayout();
            this.tb_gheFonte.SuspendLayout();
            this.tb_setorGHE.SuspendLayout();
            this.grb_ghePPRA.SuspendLayout();
            this.tb_fonteGHE.SuspendLayout();
            this.Fonte.SuspendLayout();
            this.agente.SuspendLayout();
            this.epi.SuspendLayout();
            this.grb_fonteAgente.SuspendLayout();
            this.grb_agente.SuspendLayout();
            this.grb_epi.SuspendLayout();
            this.grb_agenteEpi.SuspendLayout();
            this.grb_fonteEpi.SuspendLayout();
            this.grb_gheEPI.SuspendLayout();
            this.grb_gheFonte.SuspendLayout();
            this.grb_fonte.SuspendLayout();
            this.tb_gheSetor.SuspendLayout();
            this.tb_setor.SuspendLayout();
            this.grb_setor.SuspendLayout();
            this.grb_gheSetor.SuspendLayout();
            this.tb_funcao.SuspendLayout();
            this.grb_funcao.SuspendLayout();
            this.tb_funcaoPPRA.SuspendLayout();
            this.grb_funcaoCliente.SuspendLayout();
            this.grb_setorFuncao.SuspendLayout();
            this.grb_gheFuncao.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.IncluirPPRAErrorProvider)).BeginInit();
            this.grb_gheAgente.SuspendLayout();
            this.grb_comentario.SuspendLayout();
            this.grb_anexo.SuspendLayout();
            this.panel.SuspendLayout();
            this.SuspendLayout();
            // 
            // text_cliente
            // 
            this.text_cliente.MaxLength = 100;
            // 
            // dt_vencimento
            // 
            this.dt_vencimento.Checked = false;
            // 
            // text_numContrato
            // 
            this.text_numContrato.MaxLength = 15;
            // 
            // text_clienteContratante
            // 
            this.text_clienteContratante.MaxLength = 100;
            // 
            // btn_contratante
            // 
            this.btn_contratante.Click += new System.EventHandler(this.btn_contratante_Click_1);
            // 
            // text_tecnico
            // 
            this.text_tecnico.MaxLength = 100;
            // 
            // text_descricaoCronograma
            // 
            // text_localObra
            // 
            this.text_localObra.MaxLength = 50;
            // 
            // cb_uf
            // 
            this.cb_uf.DisplayMember = "Nome";
            this.cb_uf.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cb_uf.ValueMember = "Valor";
            // 
            // text_cidade
            // 
            this.text_cidade.MaxLength = 50;
            // 
            // text_bairro
            // 
            this.text_bairro.MaxLength = 50;
            // 
            // text_complemento
            // 
            this.text_complemento.MaxLength = 100;
            // 
            // text_endereco
            // 
            this.text_endereco.MaxLength = 100;
            // 
            // text_obra
            // 
            this.text_obra.MaxLength = 50;
            // 
            // cb_grauRisco
            // 
            this.cb_grauRisco.DisplayMember = "Nome";
            this.cb_grauRisco.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cb_grauRisco.ValueMember = "Valor";
            // 
            // dt_criacao
            // 
            this.dt_criacao.Checked = false;
            // 
            // text_comentario
            // 
            this.text_comentario.MaxLength = 500;
            // 
            // text_conteudo
            // 
            this.text_conteudo.MaxLength = 50;
            // 
            // frm_ppraAlterar
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(794, 568);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "frm_ppraAlterar";
            this.Text = "ALTERAR PPRA";
            this.Load += new System.EventHandler(this.frm_ppraAlterar_Load);
            this.grb_paginacao.ResumeLayout(false);
            this.grb_dados.ResumeLayout(false);
            this.grb_dados.PerformLayout();
            this.grb_grupo1.ResumeLayout(false);
            this.grb_grupo1.PerformLayout();
            this.grb_cliente.ResumeLayout(false);
            this.grb_cliente.PerformLayout();
            this.grb_tecnico.ResumeLayout(false);
            this.grb_tecnico.PerformLayout();
            this.grb_data.ResumeLayout(false);
            this.grb_data.PerformLayout();
            this.tb_dados.ResumeLayout(false);
            this.tb_dadosPPRA.ResumeLayout(false);
            this.tb_cronograma.ResumeLayout(false);
            this.grb_grauRisco.ResumeLayout(false);
            this.tb_ghe.ResumeLayout(false);
            this.tb_gheFonte.ResumeLayout(false);
            this.tb_setorGHE.ResumeLayout(false);
            this.grb_ghePPRA.ResumeLayout(false);
            this.tb_fonteGHE.ResumeLayout(false);
            this.Fonte.ResumeLayout(false);
            this.agente.ResumeLayout(false);
            this.epi.ResumeLayout(false);
            this.grb_fonteAgente.ResumeLayout(false);
            this.grb_agente.ResumeLayout(false);
            this.grb_epi.ResumeLayout(false);
            this.grb_agenteEpi.ResumeLayout(false);
            this.grb_fonteEpi.ResumeLayout(false);
            this.grb_gheEPI.ResumeLayout(false);
            this.grb_gheFonte.ResumeLayout(false);
            this.grb_fonte.ResumeLayout(false);
            this.tb_gheSetor.ResumeLayout(false);
            this.tb_setor.ResumeLayout(false);
            this.grb_setor.ResumeLayout(false);
            this.grb_gheSetor.ResumeLayout(false);
            this.tb_funcao.ResumeLayout(false);
            this.grb_funcao.ResumeLayout(false);
            this.tb_funcaoPPRA.ResumeLayout(false);
            this.grb_funcaoCliente.ResumeLayout(false);
            this.grb_setorFuncao.ResumeLayout(false);
            this.grb_gheFuncao.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.IncluirPPRAErrorProvider)).EndInit();
            this.grb_gheAgente.ResumeLayout(false);
            this.grb_comentario.ResumeLayout(false);
            this.grb_comentario.PerformLayout();
            this.grb_anexo.ResumeLayout(false);
            this.grb_anexo.PerformLayout();
            this.panel.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion
    }
}