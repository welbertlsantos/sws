﻿using SWS.Entidade;
using SWS.Helper;
using SWS.View.ViewHelper;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace SWS.View
{
    public partial class frmMedicoDetalhar : SWS.View.frmMedicoIncluir
    {
        public frmMedicoDetalhar(Medico medico):base()
        {
            InitializeComponent();
            
            ComboHelper.unidadeFederativa(cb_uf);
            this.cb_uf.SelectedValue = medico.Uf;
            this.cb_uf.Enabled = false;

            if (medico.Cidade != null)
                cbCidade.SelectedValue = medico.Cidade.Id.ToString();

            this.cbCidade.Enabled = false;

            this.text_nome.Text = medico.Nome;
            this.text_nome.Enabled = false;

            this.text_crm.Text = medico.Crm;
            this.text_crm.Enabled = false;

            this.text_telefone1.Text = medico.Telefone1;
            this.text_telefone1.Enabled = false;

            this.text_telefone2.Text = medico.Telefone2;
            this.text_telefone2.Enabled = false;
            
            this.text_email.Text = medico.Email;
            this.text_email.Enabled = false;

            this.text_endereco.Text = medico.Endereco;
            this.text_endereco.Enabled = false;

            this.text_numero.Text = medico.Numero;
            this.text_numero.Enabled = false;

            this.text_complemento.Text = medico.Complemento;
            this.text_complemento.Enabled = false;

            this.text_bairro.Text = medico.Bairro;
            this.text_bairro.Enabled = false;

            this.text_cep.Text = medico.Cep;
            this.text_cep.Enabled = false;

            this.textPisPasep.Text = medico.Pispasep;
            this.textPisPasep.Enabled = false;

            this.textCpf.Text = medico.Cpf;
            this.textCpf.Enabled = false;

            this.textRqe.Text = medico.Rqe;
            this.textRqe.Enabled = false;

            this.bt_gravar.Enabled = false;
            this.bt_limpar.Enabled = false;

            ComboHelper.unidadeFederativa(cbUfCrm);
            this.cbUfCrm.SelectedValue = medico.UfCrm;
            this.cbUfCrm.Enabled = false;

            textDocumentoExtra.Text = medico.DocumentoExtra;
            textDocumentoExtra.ReadOnly = true;

            /* preenchendo informações do arquivo */

            if (medico.Assinatura != null)
            {
                assinatura = new Arquivo(null, String.Empty, medico.Mimetype, 0, medico.Assinatura);
                pbAssinatura.Image = FileHelper.byteArrayToImage(medico.Assinatura);
            }
        }
    }
}
