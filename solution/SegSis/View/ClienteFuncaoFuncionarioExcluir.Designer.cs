﻿namespace SWS.View
{
    partial class frmClienteFuncaoFuncionarioExcluir
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.flpAcao = new System.Windows.Forms.FlowLayoutPanel();
            this.dataDemissao = new System.Windows.Forms.DateTimePicker();
            this.lblFuncionario = new System.Windows.Forms.Label();
            this.lblDataDemissão = new System.Windows.Forms.TextBox();
            this.textFantasia = new System.Windows.Forms.TextBox();
            this.lblCnpj = new System.Windows.Forms.TextBox();
            this.textCnpj = new System.Windows.Forms.MaskedTextBox();
            this.lblFantasia = new System.Windows.Forms.TextBox();
            this.lblCliente = new System.Windows.Forms.Label();
            this.textRazaoSocial = new System.Windows.Forms.TextBox();
            this.lblRazaoSocial = new System.Windows.Forms.TextBox();
            this.epClienteFuncionarioExcluir = new System.Windows.Forms.ErrorProvider(this.components);
            this.btFechar = new System.Windows.Forms.Button();
            this.btConfirmar = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.scForm)).BeginInit();
            this.scForm.Panel1.SuspendLayout();
            this.scForm.Panel2.SuspendLayout();
            this.scForm.SuspendLayout();
            this.pnlForm.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.banner)).BeginInit();
            this.flpAcao.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.epClienteFuncionarioExcluir)).BeginInit();
            this.SuspendLayout();
            // 
            // scForm
            // 
            // 
            // scForm.Panel1
            // 
            this.scForm.Panel1.Controls.Add(this.flpAcao);
            // 
            // pnlForm
            // 
            this.pnlForm.Controls.Add(this.dataDemissao);
            this.pnlForm.Controls.Add(this.lblFuncionario);
            this.pnlForm.Controls.Add(this.lblDataDemissão);
            this.pnlForm.Controls.Add(this.textFantasia);
            this.pnlForm.Controls.Add(this.lblCnpj);
            this.pnlForm.Controls.Add(this.textCnpj);
            this.pnlForm.Controls.Add(this.lblFantasia);
            this.pnlForm.Controls.Add(this.lblCliente);
            this.pnlForm.Controls.Add(this.textRazaoSocial);
            this.pnlForm.Controls.Add(this.lblRazaoSocial);
            // 
            // flpAcao
            // 
            this.flpAcao.Controls.Add(this.btConfirmar);
            this.flpAcao.Controls.Add(this.btFechar);
            this.flpAcao.Dock = System.Windows.Forms.DockStyle.Fill;
            this.flpAcao.Location = new System.Drawing.Point(0, 0);
            this.flpAcao.Name = "flpAcao";
            this.flpAcao.Size = new System.Drawing.Size(534, 30);
            this.flpAcao.TabIndex = 0;
            // 
            // dataDemissao
            // 
            this.dataDemissao.CalendarFont = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dataDemissao.Checked = false;
            this.dataDemissao.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dataDemissao.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dataDemissao.Location = new System.Drawing.Point(211, 131);
            this.dataDemissao.Name = "dataDemissao";
            this.dataDemissao.ShowCheckBox = true;
            this.dataDemissao.Size = new System.Drawing.Size(287, 21);
            this.dataDemissao.TabIndex = 71;
            // 
            // lblFuncionario
            // 
            this.lblFuncionario.AutoSize = true;
            this.lblFuncionario.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblFuncionario.ForeColor = System.Drawing.Color.Red;
            this.lblFuncionario.Location = new System.Drawing.Point(9, 113);
            this.lblFuncionario.Name = "lblFuncionario";
            this.lblFuncionario.Size = new System.Drawing.Size(72, 15);
            this.lblFuncionario.TabIndex = 80;
            this.lblFuncionario.Text = "Funcionário";
            // 
            // lblDataDemissão
            // 
            this.lblDataDemissão.BackColor = System.Drawing.Color.LightGray;
            this.lblDataDemissão.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblDataDemissão.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDataDemissão.Location = new System.Drawing.Point(12, 131);
            this.lblDataDemissão.Name = "lblDataDemissão";
            this.lblDataDemissão.ReadOnly = true;
            this.lblDataDemissão.Size = new System.Drawing.Size(200, 21);
            this.lblDataDemissão.TabIndex = 79;
            this.lblDataDemissão.TabStop = false;
            this.lblDataDemissão.Text = "Data de Demissão";
            // 
            // textFantasia
            // 
            this.textFantasia.BackColor = System.Drawing.Color.White;
            this.textFantasia.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textFantasia.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.textFantasia.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textFantasia.ForeColor = System.Drawing.Color.Blue;
            this.textFantasia.Location = new System.Drawing.Point(211, 55);
            this.textFantasia.Name = "textFantasia";
            this.textFantasia.ReadOnly = true;
            this.textFantasia.Size = new System.Drawing.Size(287, 21);
            this.textFantasia.TabIndex = 75;
            this.textFantasia.TabStop = false;
            // 
            // lblCnpj
            // 
            this.lblCnpj.BackColor = System.Drawing.Color.LightGray;
            this.lblCnpj.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblCnpj.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCnpj.Location = new System.Drawing.Point(12, 75);
            this.lblCnpj.Name = "lblCnpj";
            this.lblCnpj.ReadOnly = true;
            this.lblCnpj.Size = new System.Drawing.Size(200, 21);
            this.lblCnpj.TabIndex = 77;
            this.lblCnpj.TabStop = false;
            this.lblCnpj.Text = "CNPJ";
            // 
            // textCnpj
            // 
            this.textCnpj.BackColor = System.Drawing.Color.White;
            this.textCnpj.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textCnpj.ForeColor = System.Drawing.Color.Blue;
            this.textCnpj.Location = new System.Drawing.Point(211, 75);
            this.textCnpj.Mask = "99,999,999/9999-99";
            this.textCnpj.Name = "textCnpj";
            this.textCnpj.PromptChar = ' ';
            this.textCnpj.ReadOnly = true;
            this.textCnpj.Size = new System.Drawing.Size(287, 21);
            this.textCnpj.TabIndex = 78;
            this.textCnpj.TabStop = false;
            // 
            // lblFantasia
            // 
            this.lblFantasia.BackColor = System.Drawing.Color.LightGray;
            this.lblFantasia.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblFantasia.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblFantasia.Location = new System.Drawing.Point(12, 55);
            this.lblFantasia.Name = "lblFantasia";
            this.lblFantasia.ReadOnly = true;
            this.lblFantasia.Size = new System.Drawing.Size(200, 21);
            this.lblFantasia.TabIndex = 76;
            this.lblFantasia.TabStop = false;
            this.lblFantasia.Text = "Nome de Fantasia";
            // 
            // lblCliente
            // 
            this.lblCliente.AutoSize = true;
            this.lblCliente.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCliente.ForeColor = System.Drawing.Color.Red;
            this.lblCliente.Location = new System.Drawing.Point(9, 17);
            this.lblCliente.Name = "lblCliente";
            this.lblCliente.Size = new System.Drawing.Size(45, 15);
            this.lblCliente.TabIndex = 74;
            this.lblCliente.Text = "Cliente";
            // 
            // textRazaoSocial
            // 
            this.textRazaoSocial.BackColor = System.Drawing.Color.White;
            this.textRazaoSocial.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textRazaoSocial.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.textRazaoSocial.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textRazaoSocial.ForeColor = System.Drawing.Color.Blue;
            this.textRazaoSocial.Location = new System.Drawing.Point(211, 35);
            this.textRazaoSocial.Name = "textRazaoSocial";
            this.textRazaoSocial.ReadOnly = true;
            this.textRazaoSocial.Size = new System.Drawing.Size(287, 21);
            this.textRazaoSocial.TabIndex = 72;
            this.textRazaoSocial.TabStop = false;
            // 
            // lblRazaoSocial
            // 
            this.lblRazaoSocial.BackColor = System.Drawing.Color.LightGray;
            this.lblRazaoSocial.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblRazaoSocial.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblRazaoSocial.Location = new System.Drawing.Point(12, 35);
            this.lblRazaoSocial.Name = "lblRazaoSocial";
            this.lblRazaoSocial.ReadOnly = true;
            this.lblRazaoSocial.Size = new System.Drawing.Size(200, 21);
            this.lblRazaoSocial.TabIndex = 73;
            this.lblRazaoSocial.TabStop = false;
            this.lblRazaoSocial.Text = "Razão Social";
            // 
            // epClienteFuncionarioExcluir
            // 
            this.epClienteFuncionarioExcluir.ContainerControl = this;
            // 
            // btFechar
            // 
            this.btFechar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btFechar.Image = global::SWS.Properties.Resources.close;
            this.btFechar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btFechar.Location = new System.Drawing.Point(84, 3);
            this.btFechar.Name = "btFechar";
            this.btFechar.Size = new System.Drawing.Size(75, 23);
            this.btFechar.TabIndex = 7;
            this.btFechar.Text = "Fechar";
            this.btFechar.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btFechar.UseVisualStyleBackColor = true;
            this.btFechar.Click += new System.EventHandler(this.btFechar_Click);
            // 
            // btConfirmar
            // 
            this.btConfirmar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btConfirmar.Image = global::SWS.Properties.Resources.Gravar;
            this.btConfirmar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btConfirmar.Location = new System.Drawing.Point(3, 3);
            this.btConfirmar.Name = "btConfirmar";
            this.btConfirmar.Size = new System.Drawing.Size(75, 23);
            this.btConfirmar.TabIndex = 6;
            this.btConfirmar.Text = "Gravar";
            this.btConfirmar.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btConfirmar.UseVisualStyleBackColor = true;
            this.btConfirmar.Click += new System.EventHandler(this.btConfirmar_Click);
            // 
            // frmClienteFuncionarioExcluir
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(534, 361);
            this.Name = "frmClienteFuncionarioExcluir";
            this.Text = "DESLIGAR FUNCIONÁRIO";
            this.scForm.Panel1.ResumeLayout(false);
            this.scForm.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.scForm)).EndInit();
            this.scForm.ResumeLayout(false);
            this.pnlForm.ResumeLayout(false);
            this.pnlForm.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.banner)).EndInit();
            this.flpAcao.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.epClienteFuncionarioExcluir)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.FlowLayoutPanel flpAcao;
        protected System.Windows.Forms.DateTimePicker dataDemissao;
        protected System.Windows.Forms.Label lblFuncionario;
        protected System.Windows.Forms.TextBox lblDataDemissão;
        protected System.Windows.Forms.TextBox textFantasia;
        protected System.Windows.Forms.TextBox lblCnpj;
        protected System.Windows.Forms.MaskedTextBox textCnpj;
        protected System.Windows.Forms.TextBox lblFantasia;
        protected System.Windows.Forms.Label lblCliente;
        protected System.Windows.Forms.TextBox textRazaoSocial;
        protected System.Windows.Forms.TextBox lblRazaoSocial;
        protected System.Windows.Forms.Button btConfirmar;
        protected System.Windows.Forms.Button btFechar;
        private System.Windows.Forms.ErrorProvider epClienteFuncionarioExcluir;
    }
}