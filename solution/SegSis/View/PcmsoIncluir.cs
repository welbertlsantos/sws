﻿using SWS.Entidade;
using SWS.Facade;
using SWS.View.Resources;
using SWS.View.ViewHelper;
using SWS.ViewHelper;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SWS.View
{
    public partial class frmPcmsoIncluir : frmTemplate
    {
        protected Estudo pcmso;

        public Estudo Pcmso
        {
            get { return pcmso; }
            set { pcmso = value; }
        }

        protected Cliente cliente;

        protected Usuario elaborador;

        protected Cliente contratante;

        protected Cnae cnaeCliente;

        protected Cnae cnaeContratante;
        
        public frmPcmsoIncluir()
        {
            InitializeComponent();
            this.updateControl(false);
            ComboHelper.grauRisco(cbGrauRisco);
            
        }

        protected virtual void btObservacao_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrWhiteSpace(pcmso.Comentario))
            {
                frmPcmsoObservacao incluirObservacao = new frmPcmsoObservacao();
                incluirObservacao.ShowDialog();

                if (!string.IsNullOrWhiteSpace(incluirObservacao.Observacao))
                    pcmso.Comentario = incluirObservacao.Observacao;
            }
            else
            {
                frmPcmsoObservacao alterarObservacao = new frmPcmsoObservacao(pcmso.Comentario);
                alterarObservacao.ShowDialog();
                pcmso.Comentario = alterarObservacao.Observacao;
            }
        }

        protected void btFechar_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        protected void btGravar_Click(object sender, EventArgs e)
        {
            try
            {
                if (validaCampoNovoPcmso())
                {
                    EstudoFacade pcmsoFacade = EstudoFacade.getInstance();
                    ClienteFacade clienteFacade = ClienteFacade.getInstance();
                    if (pcmso == null)
                    {

                        pcmso = new Estudo();

                        pcmso.TipoEstudo = true;
                        pcmso.Avulso = false;
                        pcmso.DataCriacao = Convert.ToDateTime(dataCriacao.Text);
                        pcmso.DataVencimento = Convert.ToDateTime(dataValidade.Text);
                        pcmso.Situacao = ApplicationConstants.CONSTRUCAO;
                        pcmso.GrauRisco = ((SelectItem)cbGrauRisco.SelectedItem).Valor;
                        pcmso.Tecno = elaborador;

                        pcmso.VendedorCliente = clienteFacade.findClienteVendedorByCliente(cliente).Find(x => String.Equals(x.Situacao, ApplicationConstants.ATIVO));
                        pcmso = pcmsoFacade.insertPcmso(pcmso, cnaeCliente);

                        MessageBox.Show("Pcmso incluído com sucesso", "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Information);

                        this.textCodigo.Text = pcmso.CodigoEstudo;
                        updateControl(true);
                        btnClienteIncluir.Enabled = false;
                        btnClienteExcluir.Enabled = false;
                            
                        btnGravarNovo.Text = "Gravar";
                    }
                    else
                    {
                        /* realizando update em campos do pcmso */
                        pcmso.DataCriacao = Convert.ToDateTime(dataCriacao.Text);
                        pcmso.DataVencimento = Convert.ToDateTime(dataValidade.Text);
                        pcmso.GrauRisco = ((SelectItem)cbGrauRisco.SelectedItem).Valor;
                        pcmso.Tecno = elaborador;

                        if (pcmso.ClienteContratante != null)
                        {
                            pcmso.DataInicioContrato = textIncioContrato.Text.Trim();
                            pcmso.DataFimContrato = textFimContrato.Text.Trim();
                            pcmso.NumContrato = textNumeroContrato.Text.Trim();
                        }

                        pcmso = pcmsoFacade.updatePcmso(pcmso);
                        MessageBox.Show("PCMSO alterado com sucesso", "Confirmação", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }

        protected void updateControl(bool status)
        {
            this.grbEstimativa.Enabled = status;
            this.flpEstimativa.Enabled = status;
            this.grbGhe.Enabled = status;
            this.flpGhe.Enabled = status;
            this.grbSetor.Enabled = status;
            this.flpSetor.Enabled = status;
            this.grbFuncao.Enabled = status;
            this.flpFuncao.Enabled = status;
            this.grbFonte.Enabled = status;
            this.flpFonte.Enabled = status;
            this.grbAgente.Enabled = status;
            this.flpAgente.Enabled = status;
            this.grbExame.Enabled = status;
            this.flpExame.Enabled = status;
            this.grbPeriodicidade.Enabled = status;
            this.flpPeriodicidade.Enabled = status;
            this.grbHosptitais.Enabled = status;
            this.flpHospital.Enabled = status;
            this.grbMedicos.Enabled = status;
            this.flpMedico.Enabled = status;
            this.grbAtividades.Enabled = status;
            this.flpAtividade.Enabled = status;
            this.btnLocalEstudoIncluir.Enabled = status;
            this.btnLocalEstudoExcluir.Enabled = status;
            this.btnObservacao.Enabled = status;
            this.btnContratanteIncluir.Enabled = status;
            this.btnContratanteExcluir.Enabled = status;
        }

        protected void btnClienteIncluir_Click(object sender, EventArgs e)
        {
            frmClienteSelecionar selecionarCliente = new frmClienteSelecionar(null, null, null, null, null, false, null, null, null);
            selecionarCliente.ShowDialog();

            if (selecionarCliente.Cliente != null)
            {

                /* selecionar o cnae que será utilizado no estudo */

                frmCnaeClienteSelecionar selecionarCnaeEstudo = new frmCnaeClienteSelecionar(selecionarCliente.Cliente);
                selecionarCnaeEstudo.ShowDialog();

                if (selecionarCnaeEstudo.ClienteCnae != null)
                {
                    this.cliente = selecionarCliente.Cliente;
                    textCliente.Text = this.cliente.RazaoSocial +
                    " [ " + ValidaCampoHelper.FormataCnpj(this.cliente.Cnpj) + " ] ";


                    cnaeCliente = selecionarCnaeEstudo.ClienteCnae.Cnae;
                    textCnaeCliente.Text =  ValidaCampoHelper.RetornaCnaeFormatado(cnaeCliente.CodCnae) + " - " +
                        cnaeCliente.Atividade;

                    /* verificando se o cnae tem grauRisco vinculado */
                    cbGrauRisco.SelectedValue = cnaeCliente.GrauRisco;

                }
            }

            
        }

        protected void btnClienteExcluir_Click(object sender, EventArgs e)
        {
            try
            {
                if (cliente == null)
                    throw new Exception("Você deve selecionar primeiro o cliente");

                cliente = null;
                this.textCliente.Text = string.Empty;

                cnaeCliente = null;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }

        protected void btnElaboradorIncluir_Click(object sender, EventArgs e)
        {
            frmUsuarioTecnicoSelecionar selecionarElaborador = new frmUsuarioTecnicoSelecionar();
            selecionarElaborador.ShowDialog();

            if (selecionarElaborador.Usuario != null)
            {
                this.elaborador = selecionarElaborador.Usuario;
                this.textElaborador.Text = this.elaborador.Nome;
            }
        }

        protected void btnElaboradorExcluir_Click(object sender, EventArgs e)
        {
            try
            {
                if (this.elaborador == null)
                    throw new Exception("Você deve selecionar primeiro o elaborador do estudo.");

                this.elaborador = null;
                this.textElaborador.Text = string.Empty;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }

        protected void btnContratanteIncluir_Click(object sender, EventArgs e)
        {
            try
            {
                EstudoFacade pcmsoFacade = EstudoFacade.getInstance();

                frmClienteSelecionar selecionarContratante = new frmClienteSelecionar(null, null, null, null, null, false, null, false, null);
                selecionarContratante.ShowDialog();

                if (selecionarContratante.Cliente != null)
                {
                    if (selecionarContratante.Cliente.Id == this.cliente.Id)
                        throw new Exception("O contratante não pode ser igual ao cliente");

                    /* selecionando o cnae do contratante */
                    frmCnaeClienteSelecionar selecionarCnaeContratante = new
                    frmCnaeClienteSelecionar(selecionarContratante.Cliente);
                    selecionarCnaeContratante.ShowDialog();

                    if (selecionarCnaeContratante.ClienteCnae != null)
                    {
                        

                        if (pcmso.ClienteContratante == null)
                        {
                            /* caso não tenha cliente contratante */
                            pcmso.ClienteContratante = selecionarContratante.Cliente;
                            pcmsoFacade.updatePcmso(pcmso);

                            CnaeEstudo cnaeEstudoNovo = new CnaeEstudo();
                            cnaeEstudoNovo.Cnae = selecionarCnaeContratante.ClienteCnae.Cnae;
                            cnaeEstudoNovo.Estudo = pcmso;
                            cnaeEstudoNovo.FlagCliente = false;

                            pcmsoFacade.insertCnaeEstudo(cnaeEstudoNovo);
                        }
                        else if (selecionarContratante.Cliente.Id == pcmso.ClienteContratante.Id)
                        {
                            /* cliente é o mesmo. Então deverá ser verificado a necessidade de alteração da relação de 
                                * cnae. */

                            List<CnaeEstudo> cnaeEstudo = new List<CnaeEstudo>();
                            cnaeEstudo = pcmsoFacade.findAllCnaEstudoByEstudo(pcmso);

                            if (!cnaeEstudo.Exists(x => x.Cnae.Id == selecionarCnaeContratante.ClienteCnae.Cnae.Id && x.FlagCliente == false))
                            {
                                /* o cnae foi alterado e deverá ser excluído para dar sequencia */

                                CnaeEstudo cnaeEstudoExcluir = cnaeEstudo.Find(x => x.FlagCliente == false);
                                pcmsoFacade.deleteCnaeEstudo(cnaeEstudoExcluir);

                                CnaeEstudo cnaeEstudoNovo = new CnaeEstudo();
                                cnaeEstudoNovo.Cnae = selecionarCnaeContratante.ClienteCnae.Cnae;
                                cnaeEstudoNovo.Estudo = pcmso;
                                cnaeEstudoNovo.FlagCliente = false;

                                pcmsoFacade.insertCnaeEstudo(cnaeEstudoNovo);

                            }
                        }
                        else
                        {
                            /* excluir o cliente contratante do pcmso e incluir o novo */
                            pcmso.ClienteContratante = selecionarContratante.Cliente;
                            pcmsoFacade.updatePcmso(pcmso);

                            List<CnaeEstudo> cnaeEstudo = new List<CnaeEstudo>();
                            cnaeEstudo = pcmsoFacade.findAllCnaEstudoByEstudo(pcmso);

                            CnaeEstudo cnaeEstudoExcluir = cnaeEstudo.Find(x => x.FlagCliente == false);
                            pcmsoFacade.deleteCnaeEstudo(cnaeEstudoExcluir);

                            CnaeEstudo cnaeEstudoNovo = new CnaeEstudo();
                            cnaeEstudoNovo.Cnae = selecionarCnaeContratante.ClienteCnae.Cnae;
                            cnaeEstudoNovo.Estudo = pcmso;
                            cnaeEstudoNovo.FlagCliente = false;

                            pcmsoFacade.insertCnaeEstudo(cnaeEstudoNovo);

                        }

                        this.contratante = selecionarContratante.Cliente;
                        this.textContratante.Text = this.contratante.RazaoSocial +
                            " [ " + ValidaCampoHelper.FormataCnpj(this.contratante.Cnpj) + " ]";

                        cnaeContratante = selecionarCnaeContratante.ClienteCnae.Cnae;

                        textCnaeContratante.Text = ValidaCampoHelper.RetornaCnaeFormatado(cnaeContratante.CodCnae) + " - " +
                            cnaeContratante.Atividade;

                        MessageBox.Show("Contratante incluído / Alterado no estudo com sucesso.",
                            "Confirmação", MessageBoxButtons.OK, MessageBoxIcon.Information);

                        MessageBox.Show("Preencha informações sobre os dados do contrato antes de fechar", "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    }
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }

        protected void btnContratanteExcluir_Click(object sender, EventArgs e)
        {
            try
            {
                
                if (this.contratante == null)
                    throw new Exception("Selecione primeiro o contrantante");

                this.contratante = null;
                this.textContratante.Text = string.Empty;
                cnaeContratante = null;
                textCnaeContratante.Text = string.Empty;
                textNumeroContrato.Text = string.Empty;
                textIncioContrato.Text = string.Empty;
                textFimContrato.Text = string.Empty;

                pcmso.ClienteContratante = null;
                pcmso.NumContrato = string.Empty;
                pcmso.DataFimContrato = string.Empty;
                pcmso.DataInicioContrato = string.Empty;

                EstudoFacade pcmsoFacade = EstudoFacade.getInstance();
                pcmsoFacade.updatePcmso(pcmso);

                /* excluindo cnaeEstudo do contratante */
                List<CnaeEstudo> cnaes = pcmsoFacade.findAllCnaEstudoByEstudo(pcmso);

                CnaeEstudo cnaeEstudoExcluir = new CnaeEstudo();
                cnaeEstudoExcluir = cnaes.Find(x => x.FlagCliente == false);

                pcmsoFacade.deleteCnaeEstudo(cnaeEstudoExcluir);

                MessageBox.Show("Contrante excluído com sucesso.", "Confirmação", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }

        protected virtual void btnLocalEstudoIncluir_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrWhiteSpace(pcmso.Obra))
            {
                frmPcmsoObraIncluir selecionarObra = new frmPcmsoObraIncluir();
                selecionarObra.ShowDialog();

                if (selecionarObra.Obra != null)
                {
                    Obra obraNew = selecionarObra.Obra;
                    pcmso.Obra = obraNew.LocalObra;
                    pcmso.Endereco = obraNew.EnderecoObra;
                    pcmso.Numero = obraNew.NumeroObra;
                    pcmso.Complemento = obraNew.ComplementoObra;
                    pcmso.Bairro = obraNew.BairroObra;
                    pcmso.Cidade = obraNew.CidadeObra.Nome;
                    pcmso.Cep = obraNew.CepObra;
                    pcmso.Uf = obraNew.UnidadeFederativaObra;
                    pcmso.LocalObra = obraNew.DescritivoObra;
                    
                    textLocalEstudo.Text = obraNew.LocalObra;
                }
            }
            else
            {
                Obra obra = new Obra();
                obra.LocalObra = pcmso.Obra;
                obra.EnderecoObra = pcmso.Endereco;
                obra.NumeroObra = pcmso.Numero;
                obra.ComplementoObra = pcmso.Complemento;
                obra.BairroObra = pcmso.Bairro;
                obra.CepObra = pcmso.Cep;
                obra.CidadeObra = findCidadeIbgeByCidadeAndUf(pcmso.Cidade, pcmso.Uf);
                obra.UnidadeFederativaObra = pcmso.Uf;
                obra.DescritivoObra = pcmso.LocalObra;

                frmPcmsoObraAlterar alterarObra = new frmPcmsoObraAlterar(obra);
                alterarObra.ShowDialog();

                textLocalEstudo.Text = alterarObra.Obra.LocalObra;

                pcmso.Obra = alterarObra.Obra.LocalObra;
                pcmso.Endereco = alterarObra.Obra.EnderecoObra;
                pcmso.Numero = alterarObra.Obra.NumeroObra;
                pcmso.Complemento = alterarObra.Obra.ComplementoObra;
                pcmso.Bairro = alterarObra.Obra.BairroObra;
                pcmso.Cidade = alterarObra.Obra.CidadeObra.Nome;
                pcmso.Cep = alterarObra.Obra.CepObra;
                pcmso.Uf = alterarObra.Obra.UnidadeFederativaObra;
                pcmso.LocalObra = alterarObra.Obra.DescritivoObra;

            }
            
        }

        protected CidadeIbge findCidadeIbgeByCidadeAndUf(string cidade, string uf)
        {
            CidadeIbge cidadeIbge = new CidadeIbge();
            try
            {
                ClienteFacade clienteFacade = ClienteFacade.getInstance();
                cidadeIbge.Nome = cidade;
                
                DataSet ds = clienteFacade.findCidadesByUf(uf, cidadeIbge);
                
                foreach (DataRow dvRown in ds.Tables[0].Rows)
                {
                    cidadeIbge.Id = (long)dvRown["ID_CIDADE_IBGE"];
                    cidadeIbge.Nome = dvRown["NOME"].ToString();
                    cidadeIbge.Codigo = dvRown["CODIGO"].ToString();
                    cidadeIbge.Uf = dvRown["UF_CIDADE"].ToString();
                }

                return cidadeIbge;

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            return cidadeIbge;
        }

        protected void btnEstimativaIncluir_Click(object sender, EventArgs e)
        {
            try
            {
                frmPcmsoEstimativa formEstimativa = new frmPcmsoEstimativa(pcmso);
                formEstimativa.ShowDialog();
                montaGridEstimativa();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }

        protected void montaGridEstimativa()
        {
            this.dgvEstimativa.Columns.Clear();

            EstudoFacade pcmsoFacade = EstudoFacade.getInstance();

            DataSet ds = pcmsoFacade.findAllEstimativaInEstudo(pcmso);

            dgvEstimativa.DataSource = ds.Tables[0].DefaultView;

            dgvEstimativa.Columns[0].HeaderText = "Id_Estimativa_estudo";
            dgvEstimativa.Columns[0].Visible = false;

            dgvEstimativa.Columns[1].HeaderText = "Id_Estimativa";
            dgvEstimativa.Columns[1].Visible = false;

            dgvEstimativa.Columns[2].HeaderText = "Id Estudo";
            dgvEstimativa.Columns[2].Visible = false;

            dgvEstimativa.Columns[3].HeaderText = "Estimativa";
            dgvEstimativa.Columns[3].ReadOnly = true;

            dgvEstimativa.Columns[4].HeaderText = "Qtde";
            dgvEstimativa.Columns[4].DefaultCellStyle.BackColor = Color.White;

        }

        protected void dgvEstimativa_CellEndEdit(object sender, DataGridViewCellEventArgs e)
        {
            try
            {

                /* validando o valor digitado caso o usuário não tenha digitado nenhuma informação o campo
                 * obrigatoriamente ficará com zero */

                if (String.IsNullOrEmpty(dgvEstimativa.Rows[e.RowIndex].Cells[e.ColumnIndex].EditedFormattedValue.ToString().Trim()))
                    this.dgvEstimativa.Rows[e.RowIndex].Cells[e.ColumnIndex].Value = 0;

                /* gravando a informação no banco de dados */

                EstudoFacade pcmsoFacade = EstudoFacade.getInstance();
                
                Estimativa estimativa = new Estimativa();
                estimativa.Id = (long)dgvEstimativa.Rows[e.RowIndex].Cells[1].Value;
                estimativa.Descricao = dgvEstimativa.Rows[e.RowIndex].Cells[3].Value.ToString();
                
                EstimativaEstudo estimativaEstudo = new EstimativaEstudo();
                estimativaEstudo.Id = (long)dgvEstimativa.Rows[e.RowIndex].Cells[0].Value;
                estimativaEstudo.Estudo = pcmso;
                estimativaEstudo.Quantidade = Convert.ToInt32(dgvEstimativa.Rows[e.RowIndex].Cells[4].EditedFormattedValue);
                estimativaEstudo.Estimativa = estimativa;

                pcmsoFacade.updateEstimativaEstudo(estimativaEstudo);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }

        protected void dgvEstimativa_DataError(object sender, DataGridViewDataErrorEventArgs e)
        {
            try
            {
                /*validando valor digitado para não dar estouro de inteiro32 */
                int number = Int32.Parse((String)dgvEstimativa.Rows[e.RowIndex].Cells[e.ColumnIndex].EditedFormattedValue);
            }
            catch (OverflowException)
            {
                MessageBox.Show("Número maior do que o permitido para o campo. ", "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }

        }

        protected void dgvEstimativa_EditingControlShowing(object sender, DataGridViewEditingControlShowingEventArgs e)
        {
            if (e.Control is DataGridViewTextBoxEditingControl)
                e.Control.KeyPress += new KeyPressEventHandler(dgvEstimativa_KeyPress);
        }

        protected void dgvEstimativa_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!Char.IsDigit(e.KeyChar) && e.KeyChar != (char)8)
                e.Handled = true;
        }

        protected void btnEstimativaExcluir_Click(object sender, EventArgs e)
        {
            try
            {
                if (dgvEstimativa.CurrentRow == null)
                    throw new Exception("Selecione uma estimativa para excluir.");

                EstudoFacade pcmsoFacade = EstudoFacade.getInstance();

                Estimativa estimativa = new Estimativa();
                estimativa.Id = (long)dgvEstimativa.CurrentRow.Cells[1].Value;
                estimativa.Descricao = dgvEstimativa.CurrentRow.Cells[3].Value.ToString();

                EstimativaEstudo estimativaEstudoExcluir = new EstimativaEstudo();
                estimativaEstudoExcluir.Id = (long)dgvEstimativa.CurrentRow.Cells[0].Value;
                estimativaEstudoExcluir.Estudo = pcmso;
                estimativaEstudoExcluir.Quantidade = Convert.ToInt32(dgvEstimativa.CurrentRow.Cells[4].EditedFormattedValue);
                
                pcmsoFacade.deleteEstimativaEstudo(estimativaEstudoExcluir);

                montaGridEstimativa();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }

        protected void btnGheIncluir_Click(object sender, EventArgs e)
        {
            frmPcmsoGheIncluir incluirGhe = new frmPcmsoGheIncluir(pcmso);
            incluirGhe.ShowDialog();
            montaGridGhe();

        }

        protected void montaGridGhe()
        {
            try
            {

                dgvGhe.Columns.Clear();
                EstudoFacade pcmsoFacade = EstudoFacade.getInstance();
                DataSet ds = pcmsoFacade.findGheByPcmso(pcmso);

                dgvGhe.ColumnCount = 5;

                dgvGhe.Columns[0].HeaderText = "id_ghe";
                dgvGhe.Columns[0].Visible = false;

                dgvGhe.Columns[1].HeaderText = "id_estudo";
                dgvGhe.Columns[1].Visible = false;

                dgvGhe.Columns[2].HeaderText = "Descrição";
                dgvGhe.Columns[2].Visible = true;
                dgvGhe.Columns[2].ReadOnly = true;

                dgvGhe.Columns[3].HeaderText = "Nexp";
                dgvGhe.Columns[3].Visible = true;
                dgvGhe.Columns[3].DefaultCellStyle.BackColor = Color.White;

                dgvGhe.Columns[4].HeaderText = "Número PO";
                dgvGhe.Columns[4].Visible = true;
                dgvGhe.Columns[4].DefaultCellStyle.BackColor = Color.White;

                foreach (DataRow row in ds.Tables[0].Rows)
                    dgvGhe.Rows.Add(
                        row["ID_GHE"],
                        pcmso.Id,
                        row["DESCRICAO"].ToString(),
                        row["NEXP"],
                        row["NUMERO_PO"].ToString()
                        );
                
                dgvSetor.Columns.Clear();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }

        protected void btnGheAlterar_Click(object sender, EventArgs e)
        {
            try
            {
                if (dgvGhe.CurrentRow == null)
                    throw new Exception("Selecione um GHE para alterar.");

                EstudoFacade pcmsoFacade = EstudoFacade.getInstance();

                frmPcmsoGheAlterar alterarGhe = new frmPcmsoGheAlterar(pcmsoFacade.findGheById((long)dgvGhe.CurrentRow.Cells[0].Value), pcmso);
                alterarGhe.ShowDialog();

                montaGridGhe();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }

        protected bool validaCampoNovoPcmso()
        {
            bool retorno = true;
            try
            {
                if (cliente == null)
                    throw new Exception("É necessário selecionar o cliente.");

                if (elaborador == null)
                    throw new Exception("É necessário selecionar o elaborador do estudo.");

                if (!dataCriacao.Checked)
                    throw new Exception("É necessário selecionar a data de início do estudo.");

                if (!dataValidade.Checked)
                    throw new Exception("É necessário selecionar a data de validade do estudo.");

                if (Convert.ToDateTime(dataCriacao.Text) > Convert.ToDateTime(dataValidade.Text))
                    throw new Exception("A data de início do estudo não pode ser mair que sua data de validae");

                if (contratante != null && string.IsNullOrWhiteSpace(textNumeroContrato.Text))
                    throw new Exception("Existe um contratante então o número de contrato é obrigatório.");

                if (contratante != null && string.IsNullOrWhiteSpace(textIncioContrato.Text))
                    throw new Exception("Existe um contratante então o período de início do contrato é obrigatório.");

                if (contratante != null && string.IsNullOrWhiteSpace(textFimContrato.Text))
                    throw new Exception("Existe um contratante então o período de Término do contrato é obrigatório.");

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
                retorno = false;
            }
            return retorno;
        }

        protected virtual void btnLocalEstudoExcluir_Click(object sender, EventArgs e)
        {
            try
            {
                if (string.IsNullOrWhiteSpace(pcmso.Obra))
                    throw new Exception("Você deve informar primeiro os dados da obra.");

                if (MessageBox.Show("Você deseja excluir os dados da obra?", "Atenção", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                {
                    pcmso.Obra = string.Empty;
                    pcmso.Endereco = string.Empty;
                    pcmso.Numero = string.Empty;
                    pcmso.Complemento = string.Empty;
                    pcmso.Bairro = string.Empty;
                    pcmso.Cidade = string.Empty;
                    pcmso.Cep = string.Empty;
                    pcmso.Uf = string.Empty;
                    pcmso.LocalObra = string.Empty;

                    textLocalEstudo.Text = string.Empty;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }

        protected void btnGheExcluir_Click(object sender, EventArgs e)
        {
            try
            {
                if (dgvGhe.CurrentRow == null)
                    throw new Exception("Selecione um GHE.");

                if (MessageBox.Show("Gostaria realmente de excluir o Ghe selecionado? ", "Confirmação", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                {
                    this.Cursor = Cursors.WaitCursor;

                    EstudoFacade pcmsoFacade = EstudoFacade.getInstance();
                    Ghe gheExcluir = new Ghe();
                    gheExcluir.Id = (long)dgvGhe.CurrentRow.Cells[0].Value;
                    gheExcluir.Estudo = pcmso;
                    gheExcluir.Descricao = dgvGhe.CurrentRow.Cells[2].Value.ToString();
                    gheExcluir.Nexp = Convert.ToInt32(dgvGhe.CurrentRow.Cells[3].Value);
                    gheExcluir.Situacao = ApplicationConstants.DESATIVADO;
                    gheExcluir.NumeroPo = dgvGhe.CurrentRow.Cells[4].Value.ToString();

                    pcmsoFacade.deleteGhe(gheExcluir);
                    MessageBox.Show("GHE excluído com sucesso", "Confirmaçao", MessageBoxButtons.OK, MessageBoxIcon.Information);

                    dgvSetor.Columns.Clear();
                    dgvFuncao.Columns.Clear();
                    dgvAgente.Columns.Clear();
                    dgvExame.Columns.Clear();
                    dgvPeriodicidade.Columns.Clear();
                    montaGridGhe();
                    dgvFonte.Columns.Clear();
                    dgvAgente.Columns.Clear();
                    dgvExame.Columns.Clear();
                    dgvSetor.Columns.Clear();
                    dgvFuncao.Columns.Clear();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }

        }

        protected void btnGheDuplicar_Click(object sender, EventArgs e)
        {
            try
            {
                if (dgvGhe.CurrentRow == null)
                    throw new Exception("Selecione o GHE que deseja replicar.");

                if (MessageBox.Show("Deseja replicar o GHE selecionado?", "Confirmação", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                {
                    frmPcmsoGheIncluir incluirGhe = new frmPcmsoGheIncluir(pcmso);
                    incluirGhe.ShowDialog();

                    if (incluirGhe.Ghe != null)
                    {
                        /* replicar o Ghe */
                        this.Cursor = Cursors.WaitCursor;
                        EstudoFacade pcmsoFacade = EstudoFacade.getInstance();
                        Ghe gheReplicar = new Ghe();
                        gheReplicar.Id = (long)dgvGhe.CurrentRow.Cells[0].Value;
                        gheReplicar.Estudo = pcmso;
                        gheReplicar.Descricao = dgvGhe.CurrentRow.Cells[2].Value.ToString();
                        gheReplicar.Nexp = Convert.ToInt32(dgvGhe.CurrentRow.Cells[3].Value);
                        gheReplicar.NumeroPo = dgvGhe.CurrentRow.Cells[4].Value.ToString();

                        pcmsoFacade.replicarGhe(incluirGhe.Ghe, gheReplicar);

                        MessageBox.Show("GHE replicado com sucesso.", "Confirmação", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        montaGridGhe();
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }


        }

        protected void dgvGhe_CellEndEdit(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                /* validando o valor digitado caso o usuário não tenha digitado nenhuma informação o campo
                 * obrigatoriamente ficará com zero */

                if (e.ColumnIndex == 3)
                    if (String.IsNullOrEmpty(dgvGhe.Rows[e.RowIndex].Cells[e.ColumnIndex].EditedFormattedValue.ToString().Trim()))
                        this.dgvGhe.Rows[e.RowIndex].Cells[e.ColumnIndex].Value = 0;

                /* gravando a informação no banco de dados */

                EstudoFacade pcmsoFacade = EstudoFacade.getInstance();
                Ghe gheAlterado = new Ghe();
                gheAlterado.Id = (long)dgvGhe.Rows[e.RowIndex].Cells[0].Value;
                gheAlterado.Estudo = pcmso;
                gheAlterado.Descricao = dgvGhe.Rows[e.RowIndex].Cells[2].Value.ToString();
                gheAlterado.Nexp = Convert.ToInt32(dgvGhe.Rows[e.RowIndex].Cells[3].EditedFormattedValue);
                gheAlterado.NumeroPo = dgvGhe.Rows[e.RowIndex].Cells[4].Value.ToString();
                gheAlterado.Situacao = ApplicationConstants.ATIVO;

                pcmsoFacade.updateGhe(gheAlterado);

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }

        protected void dgvGhe_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            montaGridSetor();
            montaGridFonte();
            dgvFuncao.Columns.Clear();
            dgvAgente.Columns.Clear();
            dgvExame.Columns.Clear();
            dgvPeriodicidade.Columns.Clear();
        }

        protected void dgvGhe_DataBindingComplete(object sender, DataGridViewBindingCompleteEventArgs e)
        {
            DataGridView gridView = sender as DataGridView;
            gridView.ClearSelection();
        }

        protected void dgvGhe_DataError(object sender, DataGridViewDataErrorEventArgs e)
        {
            try
            {
                /*validando valor digitado para não dar estouro de inteiro32 */
                int number = Int32.Parse((String)dgvGhe.Rows[e.RowIndex].Cells[3].EditedFormattedValue);
            }
            catch (OverflowException)
            {
                MessageBox.Show("Número maior do que o permitido para o campo.", "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }

        protected void dgvGhe_EditingControlShowing(object sender, DataGridViewEditingControlShowingEventArgs e)
        {
            if (e.Control is DataGridViewTextBoxEditingControl)
                e.Control.KeyPress += new KeyPressEventHandler(dgvGhe_KeyPress);
        }

        protected void dgvGhe_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = ValidaCampoHelper.FormataCampoNumerico(sender, e);
        }

        protected void btnSetorIncluir_Click(object sender, EventArgs e)
        {
            try
            {
                if (dgvGhe.CurrentRow == null)
                    throw new Exception("Selecione primeiro o GHE.");

                Ghe gheSelecionado = new Ghe();
                gheSelecionado.Id = (long)dgvGhe.CurrentRow.Cells[0].Value;
                gheSelecionado.Estudo = pcmso;
                gheSelecionado.Descricao = dgvGhe.CurrentRow.Cells[2].Value.ToString();
                gheSelecionado.Nexp = Convert.ToInt32(dgvGhe.CurrentRow.Cells[3].Value);
                gheSelecionado.NumeroPo = dgvGhe.CurrentRow.Cells[4].Value.ToString();
                gheSelecionado.Situacao = ApplicationConstants.ATIVO;

                frmPcmsoSetor formPcmsoAvulsoSetorSelecionar = new frmPcmsoSetor(gheSelecionado);
                formPcmsoAvulsoSetorSelecionar.ShowDialog();

                montaGridSetor();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }

        protected void montaGridSetor()
        {
            try
            {
                EstudoFacade pcmsoFacade = EstudoFacade.getInstance();

                dgvSetor.Columns.Clear();

                Ghe gheSelecionado = new Ghe();
                gheSelecionado.Id = (long)dgvGhe.CurrentRow.Cells[0].Value;
                gheSelecionado.Estudo = pcmso;
                gheSelecionado.Descricao = dgvGhe.CurrentRow.Cells[2].Value.ToString();
                gheSelecionado.Nexp = Convert.ToInt32(dgvGhe.CurrentRow.Cells[3].Value);
                gheSelecionado.NumeroPo = dgvGhe.CurrentRow.Cells[4].Value.ToString();
                gheSelecionado.Situacao = ApplicationConstants.ATIVO;

                DataSet ds = pcmsoFacade.findAllSetorByGhe(gheSelecionado);

                dgvSetor.ColumnCount = 5;

                dgvSetor.Columns[0].HeaderText = "id_ghe_Setor";
                dgvSetor.Columns[0].Visible = false;
                dgvSetor.Columns[1].HeaderText = "id_setor";
                dgvSetor.Columns[1].Visible = false;
                dgvSetor.Columns[2].HeaderText = "Setor";
                dgvSetor.Columns[3].HeaderText = "Id_ghe";
                dgvSetor.Columns[3].Visible = false;
                dgvSetor.Columns[4].HeaderText = "Ghe";
                dgvSetor.Columns[4].Visible = false;


                foreach (DataRow row in ds.Tables[0].Rows)
                    dgvSetor.Rows.Add(
                        row["ID_GHE_SETOR"],
                        row["ID_SETOR"],
                        row["SETOR"].ToString(),
                        row["ID_GHE"],
                        row["GHE"].ToString()
                        );

                dgvFuncao.Columns.Clear();

            }
            catch (Exception Ex)
            {
                MessageBox.Show(Ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        protected void btnSetorExcluir_Click(object sender, EventArgs e)
        {
            try
            {
                if (dgvSetor.CurrentRow == null)
                    throw new Exception("Você deve selecionar o setor para excluir.");

                if (MessageBox.Show(" Gostaria de excluir o Setor selecionado? ", "Atenção", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                {
                    this.Cursor = Cursors.WaitCursor;
                    EstudoFacade pcmsoFacade = EstudoFacade.getInstance();

                    GheSetor gheSetorExcluir = new GheSetor();
                    Setor setor = new Setor();
                    Ghe ghe = new Ghe();
                    gheSetorExcluir.Id = (long)dgvSetor.CurrentRow.Cells[0].Value;
                    
                    setor.Id = (long)dgvSetor.CurrentRow.Cells[1].Value;
                    setor.Descricao = dgvSetor.CurrentRow.Cells[2].Value.ToString();
                    setor.Situacao = ApplicationConstants.DESATIVADO;

                    ghe.Id = (long)dgvGhe.CurrentRow.Cells[0].Value;
                    ghe.Estudo = pcmso;
                    ghe.Descricao = dgvGhe.CurrentRow.Cells[2].Value.ToString();
                    ghe.Nexp = Convert.ToInt32(dgvGhe.CurrentRow.Cells[3].Value);
                    ghe.NumeroPo = dgvGhe.CurrentRow.Cells[4].Value.ToString();

                    gheSetorExcluir.Ghe = ghe;
                    gheSetorExcluir.Setor = setor;
                    gheSetorExcluir.Situacao = ApplicationConstants.DESATIVADO;
                    
                    pcmsoFacade.deleteGheSetor(gheSetorExcluir);

                    MessageBox.Show("Setor excluído com sucesso.", "Confirmação", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    dgvFuncao.Columns.Clear();
                    montaGridSetor();

                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }

        }

        protected void dgvSetor_DataBindingComplete(object sender, DataGridViewBindingCompleteEventArgs e)
        {
            DataGridView dgv = sender as DataGridView;
            dgv.ClearSelection();
        }

        protected void btnFuncaoIncluir_Click(object sender, EventArgs e)
        {
            try
            {
                if (dgvSetor.CurrentRow == null)
                    throw new Exception("Selecione primeiro o setor.");

                GheSetor gheSetor = new GheSetor();
                Setor setor = new Setor();
                Ghe ghe = new Ghe();

                ghe.Id = (long)dgvGhe.CurrentRow.Cells[0].Value;
                ghe.Estudo = pcmso;
                ghe.Descricao = dgvGhe.CurrentRow.Cells[2].Value.ToString();
                ghe.Nexp = Convert.ToInt32(dgvGhe.CurrentRow.Cells[3].Value);
                ghe.NumeroPo = dgvGhe.CurrentRow.Cells[4].Value.ToString();
                ghe.Situacao = ApplicationConstants.ATIVO;

                setor.Id = (long)dgvSetor.CurrentRow.Cells[1].Value;
                setor.Descricao = dgvSetor.CurrentRow.Cells[2].Value.ToString();
                setor.Situacao = ApplicationConstants.ATIVO;

                gheSetor.Id = (long)dgvSetor.CurrentRow.Cells[0].Value;
                gheSetor.Ghe = ghe;
                gheSetor.Setor = setor;
                gheSetor.Situacao = ApplicationConstants.ATIVO;

                frmPcmsoFuncao formPcmsoAvulsoFuncaoSelecionar = new frmPcmsoFuncao(cliente, gheSetor);
                formPcmsoAvulsoFuncaoSelecionar.ShowDialog();

                montaGridFuncao();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }

        protected void montaGridFuncao()
        {

            try
            {
                EstudoFacade pcmsoFacade = EstudoFacade.getInstance();

                dgvFuncao.Columns.Clear();

                GheSetor gheSetor = new GheSetor();
                Setor setor = new Setor();
                Ghe ghe = new Ghe();

                ghe.Id = (long)dgvGhe.CurrentRow.Cells[0].Value;
                ghe.Estudo = pcmso;
                ghe.Descricao = dgvGhe.CurrentRow.Cells[2].Value.ToString();
                ghe.Nexp = Convert.ToInt32(dgvGhe.CurrentRow.Cells[3].Value);
                ghe.NumeroPo = dgvGhe.CurrentRow.Cells[4].Value.ToString();

                setor.Id = (long)dgvSetor.CurrentRow.Cells[1].Value;
                setor.Descricao = dgvSetor.CurrentRow.Cells[2].Value.ToString();
                setor.Situacao = ApplicationConstants.ATIVO;

                gheSetor.Id = (long)dgvSetor.CurrentRow.Cells[0].Value;
                gheSetor.Ghe = ghe;
                gheSetor.Setor = setor;
                gheSetor.Situacao = ApplicationConstants.ATIVO;

                DataSet ds = pcmsoFacade.findAllGheSetorClienteFuncaoByGheSetor(gheSetor);

                dgvFuncao.ColumnCount = 7;

                dgvFuncao.Columns[0].HeaderText = "id_ghe_setor_cliente_funcao";
                dgvFuncao.Columns[0].Visible = false;
                dgvFuncao.Columns[1].HeaderText = "id_ghe_setor";
                dgvFuncao.Columns[1].Visible = false;
                dgvFuncao.Columns[2].HeaderText = "Código Função";
                dgvFuncao.Columns[2].ReadOnly = true;
                dgvFuncao.Columns[2].Width = 50;
                dgvFuncao.Columns[3].HeaderText = "Descrição";
                dgvFuncao.Columns[3].ReadOnly = true;
                dgvFuncao.Columns[3].Width = 350;
                dgvFuncao.Columns[4].HeaderText = "Atividade";
                dgvFuncao.Columns[4].Visible = true;
                dgvFuncao.Columns[4].ReadOnly = true;
                dgvFuncao.Columns[5].HeaderText = "id_seg_cliente_funcao";
                dgvFuncao.Columns[5].Visible = false;
                dgvFuncao.Columns[6].HeaderText = "id_comentario_funcao";
                dgvFuncao.Columns[6].Visible = false;

                DataGridViewCheckBoxColumn altura = new DataGridViewCheckBoxColumn();
                altura.Name = "Altura";
                dgvFuncao.Columns.Add(altura);
                dgvFuncao.Columns[7].DefaultCellStyle.BackColor = Color.White;
                dgvFuncao.Columns[7].Visible = true;
                dgvFuncao.Columns[7].Width = 60;

                DataGridViewCheckBoxColumn confinado = new DataGridViewCheckBoxColumn();
                confinado.Name = "Confinado";
                dgvFuncao.Columns.Add(confinado);
                dgvFuncao.Columns[8].DefaultCellStyle.BackColor = Color.White;
                dgvFuncao.Columns[8].Visible = true;
                dgvFuncao.Columns[8].Width = 60;

                DataGridViewCheckBoxColumn eletricidade = new DataGridViewCheckBoxColumn();
                eletricidade.Name = "Eletricidade";
                dgvFuncao.Columns.Add(eletricidade);
                dgvFuncao.Columns[9].DefaultCellStyle.BackColor = Color.White;
                dgvFuncao.Columns[9].Visible = true;
                dgvFuncao.Columns[9].Width = 60;

                DataGridViewButtonColumn btn = new DataGridViewButtonColumn();
                dgvFuncao.Columns.Add(btn);

                btn.HeaderText = "...";
                btn.Text = "...";
                btn.Name = "btAtividade";
                btn.UseColumnTextForButtonValue = true;
                dgvFuncao.Columns[10].DisplayIndex = 4;
                dgvFuncao.Columns[10].Width = 30;
                btn.FlatStyle = FlatStyle.Flat;

                foreach (DataRow row in ds.Tables[0].Rows)
                    dgvFuncao.Rows.Add(
                        row["ID_GHE_SETOR_CLIENTE_FUNCAO"],
                        row["ID_GHE_SETOR"],
                        row["ID_FUNCAO"],
                        row["DESCRICAO"].ToString(),
                        row["ATIVIDADE"].ToString(),
                        row["ID_SEG_CLIENTE_FUNCAO"],
                        row["ID_COMENTARIO_FUNCAO"],
                        row["ALTURA"],
                        row["CONFINADO"],
                        row["ELETRICIDADE"]
                        );
            }
            catch (Exception Ex)
            {
                MessageBox.Show(Ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        protected void btnFuncaoExcluir_Click(object sender, EventArgs e)
        {
            try
            {
                if (dgvFuncao.CurrentRow == null)
                    throw new Exception("Selecione uma Função.");

                if (MessageBox.Show("Gostaria realmente de excluir a Função selecionada? ", "Atenção", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                {
                    this.Cursor = Cursors.WaitCursor;
                    EstudoFacade pcmsoFacade = EstudoFacade.getInstance();

                    ClienteFuncao clienteFuncao = new ClienteFuncao();
                    clienteFuncao.Id = (long)dgvFuncao.CurrentRow.Cells[5].Value;
                    clienteFuncao.Situacao = ApplicationConstants.DESATIVADO;

                    GheSetor gheSetor = new GheSetor();
                    gheSetor.Id = (long)dgvFuncao.CurrentRow.Cells[1].Value;
                    gheSetor.Situacao = ApplicationConstants.DESATIVADO;

                    ComentarioFuncao comentarioFuncao = new ComentarioFuncao();
                    if (!String.IsNullOrWhiteSpace(dgvFuncao.CurrentRow.Cells[6].Value.ToString()))
                    {
                        comentarioFuncao.Id = (long)dgvFuncao.CurrentRow.Cells[6].Value;
                        comentarioFuncao.Situacao = ApplicationConstants.DESATIVADO;
                    }
                    
                    GheSetorClienteFuncao gheSetorClienteFuncaoSelecionado = new GheSetorClienteFuncao();
                    gheSetorClienteFuncaoSelecionado.Id = (long)dgvFuncao.CurrentRow.Cells[0].Value;
                    gheSetorClienteFuncaoSelecionado.Altura = Convert.ToBoolean(dgvFuncao.CurrentRow.Cells[7].Value);
                    gheSetorClienteFuncaoSelecionado.Confinado = Convert.ToBoolean(dgvFuncao.CurrentRow.Cells[8].Value);
                    gheSetorClienteFuncaoSelecionado.Eletricidade = Convert.ToBoolean(dgvFuncao.CurrentRow.Cells[9].Value);
                    gheSetorClienteFuncaoSelecionado.Situacao = ApplicationConstants.DESATIVADO;
                    gheSetorClienteFuncaoSelecionado.ClienteFuncao = clienteFuncao;
                    gheSetorClienteFuncaoSelecionado.GheSetor = gheSetor;
                    gheSetorClienteFuncaoSelecionado.ComentarioFuncao = comentarioFuncao;

                    pcmsoFacade.deleteGheSetorClienteFuncao(gheSetorClienteFuncaoSelecionado);

                    MessageBox.Show("Função excluída com sucesso.", "Confirmação", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    
                    montaGridFuncao();
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }

        }

        protected void dgvFuncao_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                if (e.RowIndex != -1)
                {
                    Funcao funcao = new Funcao();
                    funcao.Id = (long)dgvFuncao.Rows[e.RowIndex].Cells[2].Value;
                    funcao.Descricao = dgvFuncao.Rows[e.RowIndex].Cells[3].Value.ToString();
                    funcao.Comentario = dgvFuncao.Rows[e.RowIndex].Cells[4].Value.ToString();
                    funcao.Situacao = ApplicationConstants.ATIVO;
                    
                    ClienteFuncao clienteFuncao = new ClienteFuncao();
                    clienteFuncao.Id = (long)dgvFuncao.Rows[e.RowIndex].Cells[5].Value;
                    clienteFuncao.Cliente = cliente;
                    clienteFuncao.Funcao = funcao;
                    clienteFuncao.Situacao = ApplicationConstants.ATIVO;

                    GheSetor gheSetor = new GheSetor();
                    gheSetor.Id = (long)dgvFuncao.CurrentRow.Cells[1].Value;
                    gheSetor.Situacao = ApplicationConstants.DESATIVADO;

                    ComentarioFuncao comentarioFuncao = new ComentarioFuncao();
                    if (!string.IsNullOrWhiteSpace(dgvFuncao.CurrentRow.Cells[6].Value.ToString()))
                    {
                        comentarioFuncao.Id = (long)dgvFuncao.CurrentRow.Cells[6].Value;
                        comentarioFuncao.Situacao = ApplicationConstants.ATIVO;
                    }
                    
                    GheSetorClienteFuncao gheSetorClienteFuncao = new GheSetorClienteFuncao();
                    gheSetorClienteFuncao.Id = (long)dgvFuncao.Rows[e.RowIndex].Cells[0].Value;
                    gheSetorClienteFuncao.Altura = Convert.ToBoolean(dgvFuncao.CurrentRow.Cells[7].Value);
                    gheSetorClienteFuncao.Confinado = Convert.ToBoolean(dgvFuncao.CurrentRow.Cells[8].Value);
                    gheSetorClienteFuncao.Eletricidade = Convert.ToBoolean(dgvFuncao.CurrentRow.Cells[9].Value);
                    gheSetorClienteFuncao.Situacao = ApplicationConstants.ATIVO;
                    gheSetorClienteFuncao.ClienteFuncao = clienteFuncao;
                    gheSetorClienteFuncao.GheSetor = gheSetor;
                    gheSetorClienteFuncao.ComentarioFuncao = comentarioFuncao;

                    EstudoFacade pcmsoFacade = EstudoFacade.getInstance();

                    // dispara evento para clique do botão na gride de funções
                    if (e.ColumnIndex == 10)
                    {

                        frmFuncaoComentario comentario = new frmFuncaoComentario(funcao);
                        comentario.ShowDialog();

                        if (comentario.ComentarioFuncao != null)
                        {
                            gheSetorClienteFuncao.ComentarioFuncao = comentario.ComentarioFuncao;
                            pcmsoFacade.updateGheSetorClienteFuncao(gheSetorClienteFuncao);
                            montaGridFuncao();
                        }

                    }

                    if (e.ColumnIndex == 7 || e.ColumnIndex == 8 || e.ColumnIndex == 9)
                    {
                        /* atualizar status do trabalho em altura ou espaço confinado e eletricidade */

                        dgvFuncao.EndEdit();

                        gheSetorClienteFuncao.Altura = Convert.ToBoolean(dgvFuncao.Rows[e.RowIndex].Cells[7].EditedFormattedValue);
                        gheSetorClienteFuncao.Confinado = Convert.ToBoolean(dgvFuncao.Rows[e.RowIndex].Cells[8].EditedFormattedValue);
                        gheSetorClienteFuncao.Eletricidade = Convert.ToBoolean(dgvFuncao.Rows[e.RowIndex].Cells[9].EditedFormattedValue);

                        pcmsoFacade.updateGheSetorClienteFuncao(gheSetorClienteFuncao);

                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }

        protected void dgvFuncao_DataBindingComplete(object sender, DataGridViewBindingCompleteEventArgs e)
        {
            DataGridView dgv = sender as DataGridView;
            dgv.ClearSelection();
        }

        protected void btnFonteIncluir_Click(object sender, EventArgs e)
        {
            try
            {
                if (dgvGhe.CurrentRow == null)
                    throw new Exception("Você deve selecionar primeiro um GHE. ");

                EstudoFacade pcmsoFacade = EstudoFacade.getInstance();

                Ghe ghe = new Ghe();
                ghe.Id = (long)dgvGhe.CurrentRow.Cells[0].Value;

                List<GheFonte> fontesInGhe = pcmsoFacade.findAllFonteByGhe(ghe).ToList();
                List<Fonte> fontesInError = new List<Fonte>();

                frmFontesSelecionar incluirFonteEstudo = new frmFontesSelecionar();
                incluirFonteEstudo.ShowDialog();

                if (incluirFonteEstudo.Fontes != null && incluirFonteEstudo.Fontes.Count > 0)
                {
                    foreach (Fonte fonte in incluirFonteEstudo.Fontes)
                    {
                        if (fontesInGhe.Exists(x => x.Fonte.Id == fonte.Id))
                        {
                            fontesInError.Add(fonte);
                        }
                        else
                        {
                            /* incluir o GheFonte */
                            ghe.Estudo = pcmso;
                            ghe.Descricao = dgvGhe.CurrentRow.Cells[2].Value.ToString();
                            ghe.Nexp = Convert.ToInt32(dgvGhe.CurrentRow.Cells[3].Value);
                            ghe.NumeroPo = dgvGhe.CurrentRow.Cells[4].Value.ToString();
                            ghe.Situacao = ApplicationConstants.ATIVO;

                            GheFonte gheFonte = new GheFonte();
                            gheFonte.Fonte = fonte;
                            gheFonte.Ghe = ghe;
                            gheFonte.Situacao = ApplicationConstants.ATIVO;
                            gheFonte.Principal = false;

                            pcmsoFacade.insertGheFonte(gheFonte);
                        }
                    }

                    if (fontesInError.Count > 0)
                    {
                        /* algumas fontes já foram gravadas. Mostrar ao usuário essa situação. */
                        StringBuilder query = new StringBuilder();

                        query.Append("Algumas fontes selecionadas já se encontram no GHE." + System.Environment.NewLine);

                        int i = 1;
                        foreach (Fonte fonte in fontesInError)
                        {
                            query.Append(fonte.Descricao);
                            if (i < fontesInError.Count)
                            {
                                query.Append(", ");
                            }
                            else
                            {
                                query.Append(".");
                            }
                        }
                        MessageBox.Show(query.ToString(), "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                        
                    }
                    else
                        MessageBox.Show("Fonte incluída com sucesso.", "Confirmação", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    
                    montaGridFonte();
                    dgvAgente.Columns.Clear();
                    dgvExame.Columns.Clear();
                    
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        protected void montaGridFonte()
        {
            try
            {
                dgvFonte.Columns.Clear();

                EstudoFacade pcmsoFacade = EstudoFacade.getInstance();

                Ghe ghe = new Ghe();
                ghe.Id = (long)dgvGhe.CurrentRow.Cells[0].Value;
                List<GheFonte> gheFontes = pcmsoFacade.findAllGheFonteByGhe(ghe).ToList();

                dgvFonte.ColumnCount = 4;

                dgvFonte.Columns[0].HeaderText = "idGheFonte";
                dgvFonte.Columns[0].Visible = false;
                dgvFonte.Columns[1].HeaderText = "idGhe";
                dgvFonte.Columns[1].Visible = false;
                dgvFonte.Columns[2].HeaderText = "idFonte";
                dgvFonte.Columns[2].Visible = false;
                dgvFonte.Columns[3].HeaderText = "Fonte";

                foreach (GheFonte gheFonte in gheFontes)
                    dgvFonte.Rows.Add(
                        gheFonte.Id,
                        gheFonte.Ghe.Id,
                        gheFonte.Fonte.Id,
                        gheFonte.Fonte.Descricao
                        );

                dgvAgente.Columns.Clear();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }

        protected void btnFonteExcluir_Click(object sender, EventArgs e)
        {
            try
            {
                if (dgvFonte.CurrentRow == null)
                    throw new Exception("Você precisa selecionar a fonte.");

                if (MessageBox.Show("Deseja excluir a fonte selecionada?", "Confirmação", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                {
                    GheFonte gheFonteExcluir = new GheFonte();
                    gheFonteExcluir.Id = (long)dgvFonte.CurrentRow.Cells[0].Value;
                    gheFonteExcluir.Situacao = ApplicationConstants.DESATIVADO;

                    EstudoFacade pcmsoFacade = EstudoFacade.getInstance();

                    pcmsoFacade.deleteGheFonteById(gheFonteExcluir);
                    MessageBox.Show("Fonte excluída com sucesso.", "Confirmaçaõ", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    montaGridFonte();
                    dgvAgente.Columns.Clear();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }

        protected void dgvFonte_DataBindingComplete(object sender, DataGridViewBindingCompleteEventArgs e)
        {
            DataGridView dgv = sender as DataGridView;
            dgv.ClearSelection();
        }

        protected void btnAgenteIncluir_Click(object sender, EventArgs e)
        {
            try
            {
                if (dgvFonte.CurrentRow == null)
                    throw new Exception("Você deve selecionar uma fonte");

                EstudoFacade pcmsoFacade = EstudoFacade.getInstance();
                GheFonte gheFonte = new GheFonte();
                Fonte fonte = new Fonte();
                
                Ghe ghe = new Ghe();
                ghe.Id = (long)dgvGhe.CurrentRow.Cells[0].Value;
                ghe.Estudo = pcmso;
                ghe.Descricao = dgvGhe.CurrentRow.Cells[2].Value.ToString();
                ghe.Nexp = Convert.ToInt32(dgvGhe.CurrentRow.Cells[3].Value);
                ghe.NumeroPo = dgvGhe.CurrentRow.Cells[4].Value.ToString();

                fonte.Id = (long)dgvFonte.CurrentRow.Cells[2].Value;
                fonte.Descricao = dgvFonte.CurrentRow.Cells[3].Value.ToString();
                fonte.Situacao = ApplicationConstants.ATIVO;

                gheFonte.Id = (long)dgvFonte.CurrentRow.Cells[0].Value;
                gheFonte.Fonte = fonte;
                gheFonte.Ghe = ghe;
                gheFonte.Situacao = ApplicationConstants.ATIVO;
                gheFonte.Principal = false;

                frmPcmsoAgenteIncluir incluirAgente = new frmPcmsoAgenteIncluir(gheFonte);
                incluirAgente.ShowDialog();

                montaGridAgente();

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }

        protected void montaGridAgente()
        {
            try
            {
                EstudoFacade pcmsoFacade = EstudoFacade.getInstance();

                dgvAgente.Columns.Clear();

                GheFonte gheFonte = new GheFonte();
                gheFonte.Id = (long)dgvFonte.CurrentRow.Cells[0].Value;

                DataSet ds = pcmsoFacade.findAllAgenteByGheFonte(gheFonte);

                dgvAgente.ColumnCount = 5;

                dgvAgente.Columns[0].HeaderText = "Id_ghe_fonte_agente";
                dgvAgente.Columns[0].Visible = false;
                dgvAgente.Columns[1].HeaderText = "Id_agente";
                dgvAgente.Columns[1].Visible = false;
                dgvAgente.Columns[2].HeaderText = "Agente";
                dgvAgente.Columns[3].HeaderText = "id_ghe_fonte";
                dgvAgente.Columns[3].Visible = false;
                dgvAgente.Columns[4].HeaderText = "Risco";

                foreach (DataRow row in ds.Tables[0].Rows)
                    dgvAgente.Rows.Add(
                        row["ID_GHE_FONTE_AGENTE"],
                        row["ID_AGENTE"],
                        row["AGENTE"].ToString(),
                        row["ID_GHE_FONTE"],
                        row["RISCO"].ToString()
                        );

                dgvExame.Columns.Clear();
            }
            catch (Exception Ex)
            {
                MessageBox.Show(Ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        protected void btnAgenteAlterar_Click(object sender, EventArgs e)
        {
            try
            {
                if (dgvAgente.CurrentRow == null)
                    throw new Exception("Selecione o agente para alterar.");

                EstudoFacade pcmsoFacade = EstudoFacade.getInstance();

                GheFonteAgente gheFonteAgente = new GheFonteAgente();
                gheFonteAgente = pcmsoFacade.findGheFonteAgenteById((long)dgvAgente.CurrentRow.Cells[0].Value);

                frmPcmsoAgenteAlterar alterarAgente = new frmPcmsoAgenteAlterar(gheFonteAgente);
                alterarAgente.ShowDialog();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }

        protected void btnAgenteExcluir_Click(object sender, EventArgs e)
        {
            try
            {
                if (dgvAgente.CurrentRow == null)
                    throw new Exception("Selecione um agente para excluir");

                if (MessageBox.Show("Deseja excluir o agente selecionado?", "Atenção", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                {
                    EstudoFacade pcmsoFacade = EstudoFacade.getInstance();
                    pcmsoFacade.deleteGheFonteAgenteById(pcmsoFacade.findGheFonteAgenteById((long)dgvAgente.CurrentRow.Cells[0].Value));
                    MessageBox.Show("Agente excluído com sucesso", "Confirmação", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    montaGridAgente();
                    dgvExame.Columns.Clear();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }

        protected void btnExameIncluir_Click(object sender, EventArgs e)
        {
            try
            {
                if (dgvAgente.CurrentRow == null)
                    throw new Exception("Você deve selecionar primeiro o agente.");

                EstudoFacade pcmsoFacade = EstudoFacade.getInstance();

                GheFonteAgente gheFonteAgente = new GheFonteAgente();
                gheFonteAgente = pcmsoFacade.findGheFonteAgenteById((long)dgvAgente.CurrentRow.Cells[0].Value);

                frmPcmsoExamesIncluir incluirExame = new frmPcmsoExamesIncluir(gheFonteAgente);
                incluirExame.ShowDialog();
                montaGridExames();
                dgvPeriodicidade.Columns.Clear();

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        protected void montaGridExames()
        {
            try
            {
                dgvExame.Columns.Clear();

                EstudoFacade pcmsoFacade = EstudoFacade.getInstance();
                
                GheFonteAgente gheFonteAgente = new GheFonteAgente();
                gheFonteAgente.Id = (long)dgvAgente.CurrentRow.Cells[0].Value;

                DataSet ds = pcmsoFacade.findAllExamesByGheFonteAgente(gheFonteAgente);

                dgvExame.ColumnCount = 5;

                dgvExame.Columns[0].HeaderText = "idGheFonteAgenteExame";
                dgvExame.Columns[0].Visible = false;
                
                dgvExame.Columns[1].HeaderText = "IdGheFonteAgente";
                dgvExame.Columns[1].Visible = false;
                
                dgvExame.Columns[2].HeaderText = "Código Exame";
                dgvExame.Columns[2].ReadOnly = true;
                dgvExame.Columns[2].Width = 50;

                dgvExame.Columns[3].HeaderText = "Exame";
                dgvExame.Columns[3].ReadOnly = true;
                dgvExame.Columns[3].Width = 350;

                dgvExame.Columns[4].HeaderText = "Idade Exame";
                dgvExame.Columns[4].Width = 50;
                dgvExame.Columns[4].DefaultCellStyle.BackColor = Color.LightYellow;

                DataGridViewCheckBoxColumn altura = new DataGridViewCheckBoxColumn();
                altura.Name = "Altura";
                dgvExame.Columns.Add(altura);
                dgvExame.Columns[5].DefaultCellStyle.BackColor = Color.White;
                dgvExame.Columns[5].Width = 70;

                DataGridViewCheckBoxColumn confinado = new DataGridViewCheckBoxColumn();
                confinado.Name = "Confinado";
                dgvExame.Columns.Add(confinado);
                dgvExame.Columns[6].DefaultCellStyle.BackColor = Color.White;
                dgvExame.Columns[6].Width = 70;

                DataGridViewCheckBoxColumn eletricidade = new DataGridViewCheckBoxColumn();
                eletricidade.Name = "Eletricidade";
                dgvExame.Columns.Add(eletricidade);
                dgvExame.Columns[7].DefaultCellStyle.BackColor = Color.White;
                dgvExame.Columns[7].Width = 70;

                foreach (DataRow row in ds.Tables[0].Rows)
                    dgvExame.Rows.Add(
                        row["ID_GHE_FONTE_AGENTE_EXAME"],
                        row["ID_GHE_FONTE_AGENTE"],
                        row["ID_EXAME"],
                        row["DESCRICAO"].ToString(),
                        row["IDADE_EXAME"],
                        row["ALTURA"],
                        row["CONFINADO"],
                        row["ELETRICIDADE"]
                        );

                
                dgvPeriodicidade.Columns.Clear();
                
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        protected void btnExameAlterar_Click(object sender, EventArgs e)
        {
            try
            {
                if (dgvExame.CurrentRow == null)
                    throw new Exception("Selecione o exame para alteração");

                GheFonteAgenteExame gheFonteAgenteExame = new GheFonteAgenteExame();
                EstudoFacade pcmsoFacade = EstudoFacade.getInstance();

                gheFonteAgenteExame = pcmsoFacade.
                    findGheFonteAgenteExameById((long)dgvExame.CurrentRow.Cells[0].Value);

                frmPcmsoExamesAlterar examesAltera = new frmPcmsoExamesAlterar(gheFonteAgenteExame);
                examesAltera.ShowDialog();

                montaGridExames();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }

        protected void btnExameExcluir_Click(object sender, EventArgs e)
        {
            try
            {
                if (dgvExame.CurrentRow == null)
                    throw new Exception("Selecione um exame para excluir.");

                if (MessageBox.Show("Deseja excluir o exame selecionado?", "Atenção", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                {
                    EstudoFacade pcmsoFacade = EstudoFacade.getInstance();
                    GheFonteAgenteExame gheFonteAgenteExame = new GheFonteAgenteExame();

                    gheFonteAgenteExame = pcmsoFacade.
                        findGheFonteAgenteExameById((long)dgvExame.CurrentRow.Cells[0].Value);

                    pcmsoFacade.deleteGheFonteAgenteExame(gheFonteAgenteExame);
                    MessageBox.Show("Exame excluído com sucesso.", "Confirmação", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    montaGridExames();
                    dgvPeriodicidade.Columns.Clear();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }

        protected void dgvExame_DataBindingComplete(object sender, DataGridViewBindingCompleteEventArgs e)
        {
            DataGridView dgv = sender as DataGridView;
            dgv.ClearSelection();
        }

        protected void dgvExame_DataError(object sender, DataGridViewDataErrorEventArgs e)
        {
            try
            {
                /*validando valor digitado para não dar estouro de inteiro32 */
                if (e.ColumnIndex == 4)
                {
                    int number = Int32.Parse((String)dgvExame.Rows[e.RowIndex].Cells[e.ColumnIndex].EditedFormattedValue);
                }
                    
            }
            catch (OverflowException)
            {
                MessageBox.Show("Número maior do que o permitido para o campo. ", "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
        }

        protected void dgvExame_EditingControlShowing(object sender, DataGridViewEditingControlShowingEventArgs e)
        {
            if (e.Control is DataGridViewTextBoxEditingControl)
                e.Control.KeyPress += new KeyPressEventHandler(dgvExame_KeyPress);
        }

        protected void dgvExame_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!Char.IsDigit(e.KeyChar) && e.KeyChar != (char)8)
                e.Handled = true;
        }

        protected void dgvExame_CellLeave(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                DataGridView dgv = sender as DataGridView;

                if (e.ColumnIndex == 4)
                    if (String.IsNullOrEmpty(dgv.Rows[e.RowIndex].Cells[e.ColumnIndex].EditedFormattedValue.ToString().Trim()))
                        dgv.Rows[e.RowIndex].Cells[e.ColumnIndex].Value = 0;

                EstudoFacade pcmsoFacade = EstudoFacade.getInstance();
                GheFonteAgenteExame gheFonteAgenteExame = new GheFonteAgenteExame();

                gheFonteAgenteExame = pcmsoFacade.
                    findGheFonteAgenteExameById((long)dgvExame.CurrentRow.Cells[0].Value);

                gheFonteAgenteExame.IdadeExame = Convert.ToInt32(dgv.Rows[e.RowIndex].Cells[4].EditedFormattedValue.ToString());
                gheFonteAgenteExame.Altura = Convert.ToBoolean(dgv.Rows[e.RowIndex].Cells[5].EditedFormattedValue);
                gheFonteAgenteExame.Confinado = Convert.ToBoolean(dgv.Rows[e.RowIndex].Cells[6].EditedFormattedValue);
                gheFonteAgenteExame.Eletricidade = Convert.ToBoolean(dgv.Rows[e.RowIndex].Cells[7].EditedFormattedValue);

                pcmsoFacade.updateGheFonteAgenteExame(gheFonteAgenteExame);

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        protected void btnPeriodicidadeIncluir_Click(object sender, EventArgs e)
        {
            try
            {
                if (dgvExame.CurrentRow == null)
                    throw new Exception("Selecione um exame primeiro.");

                EstudoFacade pcmsoFacade = EstudoFacade.getInstance();
                GheFonteAgenteExame gheFonteAgenteExame = new GheFonteAgenteExame();

                gheFonteAgenteExame = pcmsoFacade.
                    findGheFonteAgenteExameById((long)dgvExame.CurrentRow.Cells[0].Value);

                frmPcmsoPeriodicidade periodicidade = new frmPcmsoPeriodicidade(gheFonteAgenteExame);
                periodicidade.ShowDialog();
                
                MontaGridPeriodicidade();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }

        protected void MontaGridPeriodicidade()
        {
            try
            {
                dgvPeriodicidade.Columns.Clear();

                EstudoFacade pcmsoFacade = EstudoFacade.getInstance();

                DataSet ds = pcmsoFacade.
                    findAllPeriodicidadeByGheFonteAgenteExame((long)dgvExame.CurrentRow.Cells[0].Value);

                //dgvPeriodicidade.DataSource = ds.Tables["Periodicidades"].DefaultView;

                dgvPeriodicidade.ColumnCount = 2;

                dgvPeriodicidade.Columns[0].HeaderText = "idPeriodicidade";
                dgvPeriodicidade.Columns[0].Visible = false;

                dgvPeriodicidade.Columns[1].HeaderText = "Periodicidade";
                dgvPeriodicidade.Columns[1].Width = 200;
                dgvPeriodicidade.Columns[1].ReadOnly = true;

                DataGridViewComboBoxColumn periodoVencimento = new DataGridViewComboBoxColumn();
                dgvPeriodicidade.Columns.Add(periodoVencimento);
                
                ComboHelper.periodoVencimentoCombo(periodoVencimento);
                
                dgvPeriodicidade.Columns[2].HeaderText = "Vencimento";
                dgvPeriodicidade.Columns[2].DefaultCellStyle.BackColor = Color.White;
                dgvPeriodicidade.Columns[2].Width = 150;

                foreach (DataRow dvRow in ds.Tables[0].Rows)
                    dgvPeriodicidade.Rows.Add(
                        dvRow["id_periodicidade"], 
                        dvRow["descricao"].ToString(), 
                        dvRow["periodo_vencimento"] == null ? "" : dvRow["periodo_vencimento"].ToString());

                dgvPeriodicidade.Columns[1].DisplayIndex = 1;
                dgvPeriodicidade.Columns[2].DisplayIndex = 2;

            }

            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }

        protected void btnPeriodicidadeExcluir_Click(object sender, EventArgs e)
        {
            try
            {
                if (dgvPeriodicidade.CurrentRow == null)
                    throw new Exception("Selecione uma Periodicidade.");


                if (MessageBox.Show("Gostaria realmente de excluir a Periodicidade selecionada?", "Confirmação", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                {
                    EstudoFacade pcmsoFacade = EstudoFacade.getInstance();
                    GheFonteAgenteExame gheFonteAgenteExame = new GheFonteAgenteExame();

                    gheFonteAgenteExame = pcmsoFacade.
                        findGheFonteAgenteExameById((long)dgvExame.CurrentRow.Cells[0].Value);

                    GheFonteAgenteExamePeriodicidade gheFonteAgenteExamePeriodicidade = new GheFonteAgenteExamePeriodicidade();
                    Periodicidade periodicidade = new Periodicidade();
                    periodicidade.Id = (long)dgvPeriodicidade.CurrentRow.Cells[0].Value;
                    periodicidade.Descricao = dgvPeriodicidade.CurrentRow.Cells[1].Value.ToString();

                    gheFonteAgenteExamePeriodicidade.Periodicidade = periodicidade;
                    gheFonteAgenteExamePeriodicidade.GheFonteAgenteExame = gheFonteAgenteExame;
                    

                    pcmsoFacade.deleteGheFonteAgenteExamePeriodicidade(gheFonteAgenteExamePeriodicidade);
                    MontaGridPeriodicidade();

                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }

        protected void dgvSetor_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            montaGridFuncao();
        }

        protected void dgvFonte_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            montaGridAgente();
            dgvExame.Columns.Clear();
            dgvPeriodicidade.Columns.Clear();
        }

        protected void dgvAgente_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            montaGridExames();
            dgvPeriodicidade.Columns.Clear();
        }

        protected void dgvExame_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            MontaGridPeriodicidade();
        }

        protected void btnHospitalIncluir_Click(object sender, EventArgs e)
        {
            try
            {
                frmHospitalSelecionar selecionarHospital = new frmHospitalSelecionar();
                selecionarHospital.ShowDialog();

                if (selecionarHospital.Hospital != null)
                {
                    HospitalEstudo hospitalEstudo = new HospitalEstudo();
                    hospitalEstudo.Pcmso = Pcmso;
                    hospitalEstudo.Hospital = selecionarHospital.Hospital;

                    EstudoFacade pcmsoFacade = EstudoFacade.getInstance();
                    pcmsoFacade.insertHospitalEstudo(hospitalEstudo);
                    MessageBox.Show("Hospital adicionado ao estudo com sucesso.", "Confirmação", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    montaGridHospital();

                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }

        protected void montaGridHospital()
        {
            try
            {
                dgvHospitais.Columns.Clear();
                dgvHospitais.ColumnCount = 3;

                EstudoFacade pcmsoFacade = EstudoFacade.getInstance();

                DataSet ds = pcmsoFacade.findAllHospitalEstudo(Pcmso);

                dgvHospitais.Columns[0].HeaderText = "IdHospital";
                dgvHospitais.Columns[0].Visible = false;
                dgvHospitais.Columns[1].HeaderText = "idEstudo";
                dgvHospitais.Columns[1].Visible = false;
                dgvHospitais.Columns[2].HeaderText = "Nome Hospital";

                foreach (DataRow row in ds.Tables[0].Rows)
                    dgvHospitais.Rows.Add(
                        row["ID_HOSPITAL"],
                        Pcmso.Id,
                        row["DESCRICAO"].ToString()
                        );

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }

        protected void btnHospitalExcluir_Click(object sender, EventArgs e)
        {
            try
            {
                if (dgvHospitais.CurrentRow == null)
                    throw new Exception("Você deve selecionar o hospital para excluir.");

                if (MessageBox.Show("Deseja excluir o hospital selecionado?", "Atenção", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                {
                    EstudoFacade pcmsoFacade = EstudoFacade.getInstance();
                    HospitalEstudo hospitalEstudoExcluir = new HospitalEstudo();
                    hospitalEstudoExcluir.Hospital = pcmsoFacade.findHospitalById((long)dgvHospitais.CurrentRow.Cells[0].Value);
                    hospitalEstudoExcluir.Pcmso = Pcmso;

                    pcmsoFacade.deleteHospitalEstudo(hospitalEstudoExcluir);
                    MessageBox.Show("Hospital excluído com sucesso.", "Confirmação", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    montaGridHospital();

                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }

        protected void btnMedicoIncluir_Click(object sender, EventArgs e)
        {
            try
            {
                frmMedicoBuscar incluirMedicoEstudo = new frmMedicoBuscar();
                incluirMedicoEstudo.ShowDialog();

                if (incluirMedicoEstudo.Medico != null)
                {
                    EstudoFacade pcmsoFacade = EstudoFacade.getInstance();
                    MedicoEstudo medicoEstudo = new MedicoEstudo();
                    medicoEstudo.Medico = incluirMedicoEstudo.Medico;
                    medicoEstudo.Pcmso = Pcmso;
                    medicoEstudo.Situacao = ApplicationConstants.ATIVO;
                    medicoEstudo.Coordenador = false;

                    pcmsoFacade.insertMedicoEstudo(medicoEstudo);
                    MessageBox.Show("Médico adicionado ao estudo com sucesso.", "Confirmação", MessageBoxButtons.OK, MessageBoxIcon.Information);

                    montaGridMedico();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        protected void montaGridMedico()
        {
            try
            {
                dgvMedico.Columns.Clear();
                dgvMedico.ColumnCount = 4;

                EstudoFacade pcmsoFacade = EstudoFacade.getInstance();

                DataSet ds = pcmsoFacade.findAllMedicoEstudo(Pcmso);

                dgvMedico.Columns[0].HeaderText = "idMedico";
                dgvMedico.Columns[0].Visible = false;
                dgvMedico.Columns[1].HeaderText = "idEstudo";
                dgvMedico.Columns[1].Visible = false;
                dgvMedico.Columns[2].HeaderText = "Médico";
                dgvMedico.Columns[3].HeaderText = "CRM";

                foreach (DataRow row in ds.Tables[0].Rows)
                    dgvMedico.Rows.Add(
                        row["ID_MEDICO"],
                        Pcmso.Id,
                        row["NOME"].ToString(),
                        row["CRM"].ToString()
                        );


            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        protected void btnMedicoExcluir_Click(object sender, EventArgs e)
        {
            try
            {
                if (dgvMedico.CurrentRow == null)
                    throw new Exception("Selecione um médico para excluir.");

                if (MessageBox.Show("Deseja excluir do estudo o médico selecionado?", "Atenção", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                {
                    EstudoFacade pcmsoFacade = EstudoFacade.getInstance();
                    MedicoEstudo medicoEstudoExcluir = new MedicoEstudo();
                    medicoEstudoExcluir.Coordenador = false;
                    medicoEstudoExcluir.Situacao = ApplicationConstants.DESATIVADO;
                    medicoEstudoExcluir.Pcmso = Pcmso;
                    medicoEstudoExcluir.Medico = pcmsoFacade.findMedicoById((long)dgvMedico.CurrentRow.Cells[0].Value);

                    pcmsoFacade.deleteMedicoEstudo(medicoEstudoExcluir);
                    MessageBox.Show("Médico excluído do sucesso com sucesso.", "Confirmação", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    montaGridMedico();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }

        protected void btnAtividadeIncluir_Click(object sender, EventArgs e)
        {
            frmPcmsoAtividadeIncluir incluirAtividade = new frmPcmsoAtividadeIncluir(pcmso);
            incluirAtividade.ShowDialog();
            montaGridAtividade();


        }

        protected void montaGridAtividade()
        {
            try
            {
                dgvAtividade.Columns.Clear();
                
                EstudoFacade pcmsoFacade = EstudoFacade.getInstance();

                Cronograma cronograma = new Cronograma();

                cronograma = pcmsoFacade.findCronogramaByPcmso(Pcmso);

                if (cronograma != null)
                {
                    List<CronogramaAtividade> atividades = pcmsoFacade.findAllAtividadesByCronograma(cronograma);

                    dgvAtividade.ColumnCount = 7;
                    dgvAtividade.Columns[0].HeaderText = "IdAtividade";
                    dgvAtividade.Columns[0].Visible = false;
                    dgvAtividade.Columns[1].HeaderText = "IdCronograma";
                    dgvAtividade.Columns[1].Visible = false;
                    dgvAtividade.Columns[2].HeaderText = "Atividade";
                    dgvAtividade.Columns[3].HeaderText = "Mês Realização";
                    dgvAtividade.Columns[4].HeaderText = "Ano Realização";
                    dgvAtividade.Columns[5].HeaderText = "Publico";
                    dgvAtividade.Columns[5].Visible = false;
                    dgvAtividade.Columns[6].HeaderText = "Registro";
                    dgvAtividade.Columns[6].Visible = false;

                    foreach (CronogramaAtividade cronogramaAtividade in atividades)
                        dgvAtividade.Rows.Add(
                            cronogramaAtividade.Atividade.Id,
                            cronogramaAtividade.Cronograma.Id,
                            cronogramaAtividade.Atividade.Descricao,
                            cronogramaAtividade.MesRealizado,
                            cronogramaAtividade.AnoRealizado,
                            cronogramaAtividade.PublicoAlvo,
                            cronogramaAtividade.Evidencia
                            );
                }
                
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }

        protected void btnAtividadeExcluir_Click(object sender, EventArgs e)
        {
            try
            {
                if (dgvAtividade.CurrentRow == null)
                    throw new Exception("Selecione uma atividade para excluir.");

                if (MessageBox.Show("Deseja excluir a atividade selecionada?", "Atenção", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                {
                    EstudoFacade pcmsoFacade = EstudoFacade.getInstance();

                    CronogramaAtividade cronogramaAtividadeExcluir = new CronogramaAtividade();
                    Atividade atividade = new Atividade();
                    Cronograma cronograma = new Cronograma();
                    atividade.Id = (long)dgvAtividade.CurrentRow.Cells[0].Value;
                    cronograma.Id = (long)dgvAtividade.CurrentRow.Cells[1].Value;
                    cronogramaAtividadeExcluir.Atividade = atividade;
                    cronogramaAtividadeExcluir.Cronograma = cronograma;

                    pcmsoFacade.deleteCronogramaAtividade(cronogramaAtividadeExcluir);

                    MessageBox.Show("Atividade Excluída com sucesso.", "Confirmação", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    montaGridAtividade();
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }

        protected void btnAtividadeAlterar_Click(object sender, EventArgs e)
        {
            try
            {
                if (dgvAtividade.CurrentRow == null)
                    throw new Exception("Selecione uma atividade para alterar");

                CronogramaAtividade cronogramaAtividade = new CronogramaAtividade();
                Atividade atividade = new Atividade();
                Cronograma cronograma = new Cronograma();

                atividade.Id = (long)dgvAtividade.CurrentRow.Cells[0].Value;
                atividade.Descricao = dgvAtividade.CurrentRow.Cells[2].Value.ToString();
                atividade.Situacao = ApplicationConstants.ATIVO;

                cronograma.Id = (long)dgvAtividade.CurrentRow.Cells[1].Value;

                cronogramaAtividade.Cronograma = cronograma;
                cronogramaAtividade.Atividade = atividade;
                cronogramaAtividade.AnoRealizado = dgvAtividade.CurrentRow.Cells[4].Value.ToString();
                cronogramaAtividade.MesRealizado = dgvAtividade.CurrentRow.Cells[3].Value.ToString();
                cronogramaAtividade.PublicoAlvo = dgvAtividade.CurrentRow.Cells[5].Value.ToString();
                cronogramaAtividade.Evidencia = dgvAtividade.CurrentRow.Cells[6].Value.ToString();

                frmPcmsoAtividadeAlterar alterarAtividade = new frmPcmsoAtividadeAlterar(cronogramaAtividade);
                alterarAtividade.ShowDialog();
                montaGridAtividade();
                
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }

        protected void btnMaterialHospitalarInserir_Click(object sender, EventArgs e)
        {
            frmPcmsoMaterial selecionarMaterial = new frmPcmsoMaterial(pcmso);
            selecionarMaterial.ShowDialog();
            montaGridMateriaisEstudo();

        }

        protected void montaGridMateriaisEstudo()
        {
            try
            {
                dgvMateriais.Columns.Clear();

                dgvMateriais.ColumnCount = 5;

                dgvMateriais.Columns[0].HeaderText = "idEstudo";
                dgvMateriais.Columns[0].Visible = false;
                dgvMateriais.Columns[1].HeaderText = "idMaterial";
                dgvMateriais.Columns[1].Visible = false;
                dgvMateriais.Columns[2].HeaderText = "Material";
                dgvMateriais.Columns[2].ReadOnly = true;
                dgvMateriais.Columns[3].HeaderText = "Unidade";
                dgvMateriais.Columns[3].ReadOnly = true;
                dgvMateriais.Columns[4].HeaderText = "Quantidade";
                dgvMateriais.Columns[4].DefaultCellStyle.BackColor = Color.White;
                dgvMateriais.Columns[4].Width = 40;

                EstudoFacade pcmsoFacade = EstudoFacade.getInstance();

                DataSet ds = pcmsoFacade.findAllMaterialHospitalarEstudo(pcmso);

                foreach (DataRow row in ds.Tables[0].Rows)
                    dgvMateriais.Rows.Add(
                        pcmso.Id,
                        row["ID_MATERIAL_HOSPITALAR"],
                        row["DESCRICAO"].ToString(),
                        row["UNIDADE"].ToString(),
                        row["QUANTIDADE"]);

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }

        protected void dgvMateriais_CellEndEdit(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                /* validando o valor digitado caso o usuário não tenha digitado nenhuma informação o campo
                 * obrigatoriamente ficará com zero */

                if (e.ColumnIndex == 4)
                    if (String.IsNullOrEmpty(dgvMateriais.Rows[e.RowIndex].Cells[e.ColumnIndex].EditedFormattedValue.ToString().Trim()))
                        this.dgvGhe.Rows[e.RowIndex].Cells[e.ColumnIndex].Value = 0;

                /* gravando a informação no banco de dados */

                EstudoFacade pcmsoFacade = EstudoFacade.getInstance();
                MaterialHospitalarEstudo materialEstudo = new MaterialHospitalarEstudo();
                materialEstudo.Estudo = pcmso;
                MaterialHospitalar material = new MaterialHospitalar();
                material.Id = (long)dgvMateriais.Rows[e.RowIndex].Cells[1].Value;
                materialEstudo.MaterialHospitalar = material;
                materialEstudo.Quantidade = Convert.ToInt32(dgvMateriais.Rows[e.RowIndex].Cells[4].EditedFormattedValue);

                pcmsoFacade.updateMaterialHospitalarEstudo(materialEstudo);

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        protected void dgvMateriais_DataBindingComplete(object sender, DataGridViewBindingCompleteEventArgs e)
        {
            DataGridView gridView = sender as DataGridView;
            gridView.ClearSelection();
        }

        protected void dgvMateriais_DataError(object sender, DataGridViewDataErrorEventArgs e)
        {
            try
            {
                /*validando valor digitado para não dar estouro de inteiro32 */
                int number = Int32.Parse((String)dgvGhe.Rows[e.RowIndex].Cells[4].EditedFormattedValue);
            }
            catch (OverflowException)
            {
                MessageBox.Show("Número maior do que o permitido para o campo.", "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        protected void dgvMateriais_EditingControlShowing(object sender, DataGridViewEditingControlShowingEventArgs e)
        {
            if (e.Control is DataGridViewTextBoxEditingControl)
                e.Control.KeyPress += new KeyPressEventHandler(dgvMateriais_KeyPress);
        }

        protected void dgvMateriais_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = ValidaCampoHelper.FormataCampoNumerico(sender, e);
        }

        protected void btnMaterialHospitalarExcluir_Click(object sender, EventArgs e)
        {
            try
            {
                if (dgvMateriais.CurrentRow == null)
                    throw new Exception("Você deve selecionar o material que deseja excluir.");

                if (MessageBox.Show("Deseja excluir o material selecionado?", "Atenção", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                {
                    MaterialHospitalarEstudo materialExcluir = new MaterialHospitalarEstudo();
                    MaterialHospitalar material = new MaterialHospitalar();
                    material.Id = (long)dgvMateriais.CurrentRow.Cells[1].Value;
                    materialExcluir.Estudo = pcmso;
                    materialExcluir.MaterialHospitalar = material;

                    EstudoFacade pcmsoFacade = EstudoFacade.getInstance();

                    pcmsoFacade.deleteMaterialHospitalarEstudo(materialExcluir);
                    MessageBox.Show("Material Excluído com sucesso.", "Confirmação", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    montaGridMateriaisEstudo();

                }
                
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }

        protected void btnDocumentoInserir_Click(object sender, EventArgs e)
        {
            try
            {
                frmDocumentoBuscar selecionarDocumento = new frmDocumentoBuscar();
                selecionarDocumento.ShowDialog();

                if (selecionarDocumento.Documento != null)
                {
                    EstudoFacade pcmsoFacade = EstudoFacade.getInstance();
                    /* verificando se já não está inserido no estudo */

                    List<DocumentoEstudo> documentosInEstudo = new List<DocumentoEstudo>();
                    documentosInEstudo = pcmsoFacade.findAllDocumentoByPcmsoList(pcmso);


                    if (!documentosInEstudo.Exists(x => x.Documento.Id == selecionarDocumento.Documento.Id))
                    {
                        DocumentoEstudo documentoEstudo = new DocumentoEstudo();
                        documentoEstudo.Estudo = pcmso;
                        documentoEstudo.Documento = selecionarDocumento.Documento;


                        pcmsoFacade.insertDocumentoEstudo(documentoEstudo);
                        MessageBox.Show("Documento inserído com sucesso.", "Confirmação", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        montaGridDocumento();
                    }
                    else
                        throw new Exception("Documento já inserido no estudo.");
                    
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        protected void montaGridDocumento()
        {
            try
            {
                dgvDocumentos.Columns.Clear();

                dgvDocumentos.ColumnCount = 4;

                dgvDocumentos.Columns[0].HeaderText = "idDocumentoEstudo";
                dgvDocumentos.Columns[0].Visible = false;
                dgvDocumentos.Columns[1].HeaderText = "idDocumento";
                dgvDocumentos.Columns[1].Visible = false;
                dgvDocumentos.Columns[2].HeaderText = "IdEstudo";
                dgvDocumentos.Columns[2].Visible = false;
                dgvDocumentos.Columns[3].HeaderText = "Documento";

                EstudoFacade pcmsoFacade = EstudoFacade.getInstance();

                DataSet ds = pcmsoFacade.findAllDocumentoByPcmso(pcmso);

                foreach (DataRow row in ds.Tables[0].Rows)
                    dgvDocumentos.Rows.Add(
                        row["ID_DOCUMENTO_ESTUDO"],
                        row["ID_DOCUMENTO"],
                        pcmso.Id,
                        row["DOCUMENTO"].ToString());
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }

        protected void btnDocumentoExcluir_Click(object sender, EventArgs e)
        {
            try
            {
                if (dgvDocumentos.CurrentRow == null)
                    throw new Exception("Selecione o documento para excluir");

                if (MessageBox.Show("Deseja excluir o documento selecionado?", "Atenção", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                {
                    DocumentoEstudo documentoExcluir = new DocumentoEstudo();
                    documentoExcluir.Id = (long)dgvDocumentos.CurrentRow.Cells[0].Value;

                    EstudoFacade pcmsoFacade = EstudoFacade.getInstance();
                    pcmsoFacade.deleteDocumentoEstudo(documentoExcluir);
                    MessageBox.Show("Documento excluído com sucesso.", "Confirmação", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    montaGridDocumento();
                }
                    
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }

        protected void btnSetorDuplicar_Click(object sender, EventArgs e)
        {
            try
            {
                if (dgvSetor.CurrentRow == null)
                    throw new Exception("Selecione o setor para duplicar.");

                if (MessageBox.Show("Deseja duplicar o setor selecionado?", "Atenção", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                {
                    this.Cursor = Cursors.WaitCursor;
                    GheSetor gheSetorOrigem = new GheSetor();
                    Ghe ghe = new Ghe();
                    ghe.Id = (long)dgvSetor.CurrentRow.Cells[3].Value;
                    Setor setor = new Setor();
                    setor.Id = (long)dgvSetor.CurrentRow.Cells[1].Value;
                    gheSetorOrigem.Ghe = ghe;
                    gheSetorOrigem.Setor = setor;
                    gheSetorOrigem.Id = (long)dgvSetor.CurrentRow.Cells[0].Value;

                    /* selecionando o novo setor */

                    Ghe gheSelecionado = new Ghe();
                    gheSelecionado.Id = (long)dgvGhe.CurrentRow.Cells[0].Value;
                    gheSelecionado.Estudo = pcmso;
                    gheSelecionado.Descricao = dgvGhe.CurrentRow.Cells[2].Value.ToString();
                    gheSelecionado.Nexp = Convert.ToInt32(dgvGhe.CurrentRow.Cells[3].Value);
                    gheSelecionado.NumeroPo = dgvGhe.CurrentRow.Cells[4].Value.ToString();
                    gheSelecionado.Situacao = ApplicationConstants.ATIVO;

                    frmSetorSelecionar selecionarSetor = new frmSetorSelecionar();
                    selecionarSetor.ShowDialog();

                    if (selecionarSetor.Setor != null)
                    {
                        GheSetor gheSetorNovo = new GheSetor();
                        gheSetorNovo.Ghe = gheSelecionado;
                        gheSetorNovo.Setor = selecionarSetor.Setor;
                        gheSetorNovo.Situacao = ApplicationConstants.ATIVO;

                        EstudoFacade pcmsoFacade = EstudoFacade.getInstance();
                        pcmsoFacade.duplicarGheSetor(gheSetorNovo, gheSetorOrigem);
                        MessageBox.Show("Setor duplicado com sucesso.", "Confirmação", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        montaGridSetor();
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }

        }

        protected void btnFonteDuplicar_Click(object sender, EventArgs e)
        {
            try
            {
                if (dgvFonte.CurrentRow == null)
                    throw new Exception("Selecione uma fonte para duplicar.");

                if (MessageBox.Show("Deseja duplicar a fonte selecionada?", "Atenção", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                {
                    
                    /* selecionar a fonte */

                    frmFonteSelecionar selecionarFonte = new frmFonteSelecionar();
                    selecionarFonte.ShowDialog();

                    if (selecionarFonte.Fonte != null)
                    {
                        this.Cursor = Cursors.WaitCursor;
                        GheFonte gheFonteOrigem = new GheFonte();
                        Ghe ghe = new Ghe();
                        Fonte fonte = new Fonte();
                        ghe.Id = (long)dgvFonte.CurrentRow.Cells[1].Value;
                        fonte.Id = (long)dgvFonte.CurrentRow.Cells[2].Value;
                        gheFonteOrigem.Id = (long)dgvFonte.CurrentRow.Cells[0].Value;
                        gheFonteOrigem.Ghe = ghe;
                        gheFonteOrigem.Fonte = fonte;

                        GheFonte gheFonteReplica = new GheFonte();
                        gheFonteReplica.Ghe = ghe;
                        gheFonteReplica.Fonte = selecionarFonte.Fonte;
                        gheFonteReplica.Principal = false;
                        gheFonteReplica.Situacao = ApplicationConstants.ATIVO;


                        EstudoFacade pcmsoFacade = EstudoFacade.getInstance();

                        pcmsoFacade.replicarFonte(gheFonteOrigem, gheFonteReplica);
                        MessageBox.Show("Fonte e todo os seus relacionamentos foram replicados com sucesso.",
                            "Confirmação", MessageBoxButtons.OK, MessageBoxIcon.Information);

                        montaGridFonte();
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }

        private void btnAgenteDuplicar_Click(object sender, EventArgs e)
        {
            try
            {
                if (dgvAgente.CurrentRow == null)
                    throw new Exception("Selecione o agente que será duplicado.");

                if (MessageBox.Show("Deseja duplicar o agente selecionado e seus relacionamentos?",
                    "Atenção", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                {
                    this.Cursor = Cursors.WaitCursor;
                    
                    GheFonteAgente gheFonteAgenteOrigem = new GheFonteAgente();
                    EstudoFacade pcmsoFacade = EstudoFacade.getInstance();
                    gheFonteAgenteOrigem = pcmsoFacade.findGheFonteAgenteById((long)dgvAgente.CurrentRow.Cells[0].Value);

                    /* selecionando o agente */

                    frmAgentesBuscar selecionarAgente = new frmAgentesBuscar();
                    selecionarAgente.ShowDialog();

                    if (selecionarAgente.Agente != null)
                    {
                        GheFonteAgente gheFonteAgenteReplica = new GheFonteAgente();
                        gheFonteAgenteReplica = gheFonteAgenteOrigem.copy();

                        gheFonteAgenteReplica.Agente = selecionarAgente.Agente;
                        gheFonteAgenteReplica.Id = null;

                        pcmsoFacade.replicarAgente(gheFonteAgenteOrigem, gheFonteAgenteReplica);
                        MessageBox.Show("Agente e seus relacionamentos replicado com sucesso.",
                            "Confirmação", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        montaGridAgente();

                    }

                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }

        }

    }
}
