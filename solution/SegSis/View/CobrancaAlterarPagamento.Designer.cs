﻿namespace SWS.View
{
    partial class frmCobrancaAlterarPagamento
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            this.flpAcao = new System.Windows.Forms.FlowLayoutPanel();
            this.btnConfirma = new System.Windows.Forms.Button();
            this.btnFechar = new System.Windows.Forms.Button();
            this.textCliente = new System.Windows.Forms.TextBox();
            this.textFormaPagamento = new System.Windows.Forms.TextBox();
            this.textParcela = new System.Windows.Forms.TextBox();
            this.textDocumento = new System.Windows.Forms.TextBox();
            this.lblCliente = new System.Windows.Forms.TextBox();
            this.lblFormaPgto = new System.Windows.Forms.TextBox();
            this.lblDocumento = new System.Windows.Forms.TextBox();
            this.lblParcela = new System.Windows.Forms.TextBox();
            this.lblNumeroNota = new System.Windows.Forms.TextBox();
            this.textNumeroNota = new System.Windows.Forms.TextBox();
            this.lblForma = new System.Windows.Forms.TextBox();
            this.cbForma = new SWS.ComboBoxWithBorder();
            this.lblBanco = new System.Windows.Forms.TextBox();
            this.textBanco = new System.Windows.Forms.TextBox();
            this.btnBanco = new System.Windows.Forms.Button();
            this.grbConta = new System.Windows.Forms.GroupBox();
            this.dgvConta = new System.Windows.Forms.DataGridView();
            ((System.ComponentModel.ISupportInitialize)(this.scForm)).BeginInit();
            this.scForm.Panel1.SuspendLayout();
            this.scForm.Panel2.SuspendLayout();
            this.scForm.SuspendLayout();
            this.pnlForm.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.banner)).BeginInit();
            this.flpAcao.SuspendLayout();
            this.grbConta.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvConta)).BeginInit();
            this.SuspendLayout();
            // 
            // scForm
            // 
            // 
            // scForm.Panel1
            // 
            this.scForm.Panel1.Controls.Add(this.flpAcao);
            // 
            // pnlForm
            // 
            this.pnlForm.Controls.Add(this.grbConta);
            this.pnlForm.Controls.Add(this.btnBanco);
            this.pnlForm.Controls.Add(this.textBanco);
            this.pnlForm.Controls.Add(this.lblBanco);
            this.pnlForm.Controls.Add(this.cbForma);
            this.pnlForm.Controls.Add(this.lblForma);
            this.pnlForm.Controls.Add(this.textNumeroNota);
            this.pnlForm.Controls.Add(this.textCliente);
            this.pnlForm.Controls.Add(this.textFormaPagamento);
            this.pnlForm.Controls.Add(this.textParcela);
            this.pnlForm.Controls.Add(this.textDocumento);
            this.pnlForm.Controls.Add(this.lblCliente);
            this.pnlForm.Controls.Add(this.lblFormaPgto);
            this.pnlForm.Controls.Add(this.lblDocumento);
            this.pnlForm.Controls.Add(this.lblParcela);
            this.pnlForm.Controls.Add(this.lblNumeroNota);
            // 
            // flpAcao
            // 
            this.flpAcao.Controls.Add(this.btnConfirma);
            this.flpAcao.Controls.Add(this.btnFechar);
            this.flpAcao.Dock = System.Windows.Forms.DockStyle.Fill;
            this.flpAcao.Location = new System.Drawing.Point(0, 0);
            this.flpAcao.Name = "flpAcao";
            this.flpAcao.Size = new System.Drawing.Size(534, 30);
            this.flpAcao.TabIndex = 0;
            // 
            // btnConfirma
            // 
            this.btnConfirma.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnConfirma.Image = global::SWS.Properties.Resources.fechar_ico;
            this.btnConfirma.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnConfirma.Location = new System.Drawing.Point(3, 3);
            this.btnConfirma.Name = "btnConfirma";
            this.btnConfirma.Size = new System.Drawing.Size(75, 23);
            this.btnConfirma.TabIndex = 3;
            this.btnConfirma.Text = "&Confirma";
            this.btnConfirma.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnConfirma.UseVisualStyleBackColor = true;
            this.btnConfirma.Click += new System.EventHandler(this.btnConfirma_Click);
            // 
            // btnFechar
            // 
            this.btnFechar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnFechar.Image = global::SWS.Properties.Resources.close;
            this.btnFechar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnFechar.Location = new System.Drawing.Point(84, 3);
            this.btnFechar.Name = "btnFechar";
            this.btnFechar.Size = new System.Drawing.Size(75, 23);
            this.btnFechar.TabIndex = 4;
            this.btnFechar.Text = "&Fechar";
            this.btnFechar.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnFechar.UseVisualStyleBackColor = true;
            this.btnFechar.Click += new System.EventHandler(this.btnFechar_Click);
            // 
            // textCliente
            // 
            this.textCliente.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.textCliente.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textCliente.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textCliente.Location = new System.Drawing.Point(143, 89);
            this.textCliente.Name = "textCliente";
            this.textCliente.ReadOnly = true;
            this.textCliente.Size = new System.Drawing.Size(364, 21);
            this.textCliente.TabIndex = 27;
            this.textCliente.TabStop = false;
            // 
            // textFormaPagamento
            // 
            this.textFormaPagamento.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.textFormaPagamento.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textFormaPagamento.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textFormaPagamento.Location = new System.Drawing.Point(143, 69);
            this.textFormaPagamento.Name = "textFormaPagamento";
            this.textFormaPagamento.ReadOnly = true;
            this.textFormaPagamento.Size = new System.Drawing.Size(364, 21);
            this.textFormaPagamento.TabIndex = 26;
            this.textFormaPagamento.TabStop = false;
            // 
            // textParcela
            // 
            this.textParcela.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.textParcela.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textParcela.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textParcela.Location = new System.Drawing.Point(143, 49);
            this.textParcela.Name = "textParcela";
            this.textParcela.ReadOnly = true;
            this.textParcela.Size = new System.Drawing.Size(364, 21);
            this.textParcela.TabIndex = 25;
            this.textParcela.TabStop = false;
            // 
            // textDocumento
            // 
            this.textDocumento.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.textDocumento.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textDocumento.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textDocumento.Location = new System.Drawing.Point(143, 29);
            this.textDocumento.Name = "textDocumento";
            this.textDocumento.ReadOnly = true;
            this.textDocumento.Size = new System.Drawing.Size(364, 21);
            this.textDocumento.TabIndex = 24;
            this.textDocumento.TabStop = false;
            // 
            // lblCliente
            // 
            this.lblCliente.BackColor = System.Drawing.Color.LightGray;
            this.lblCliente.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblCliente.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCliente.Location = new System.Drawing.Point(8, 89);
            this.lblCliente.Name = "lblCliente";
            this.lblCliente.ReadOnly = true;
            this.lblCliente.Size = new System.Drawing.Size(136, 21);
            this.lblCliente.TabIndex = 20;
            this.lblCliente.TabStop = false;
            this.lblCliente.Text = "Cliente";
            // 
            // lblFormaPgto
            // 
            this.lblFormaPgto.BackColor = System.Drawing.Color.LightGray;
            this.lblFormaPgto.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblFormaPgto.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblFormaPgto.Location = new System.Drawing.Point(8, 69);
            this.lblFormaPgto.Name = "lblFormaPgto";
            this.lblFormaPgto.ReadOnly = true;
            this.lblFormaPgto.Size = new System.Drawing.Size(136, 21);
            this.lblFormaPgto.TabIndex = 19;
            this.lblFormaPgto.TabStop = false;
            this.lblFormaPgto.Text = "Forma Pagamento";
            // 
            // lblDocumento
            // 
            this.lblDocumento.BackColor = System.Drawing.Color.LightGray;
            this.lblDocumento.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblDocumento.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDocumento.Location = new System.Drawing.Point(8, 29);
            this.lblDocumento.Name = "lblDocumento";
            this.lblDocumento.ReadOnly = true;
            this.lblDocumento.Size = new System.Drawing.Size(136, 21);
            this.lblDocumento.TabIndex = 18;
            this.lblDocumento.TabStop = false;
            this.lblDocumento.Text = "Documento";
            // 
            // lblParcela
            // 
            this.lblParcela.BackColor = System.Drawing.Color.LightGray;
            this.lblParcela.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblParcela.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblParcela.Location = new System.Drawing.Point(8, 49);
            this.lblParcela.Name = "lblParcela";
            this.lblParcela.ReadOnly = true;
            this.lblParcela.Size = new System.Drawing.Size(136, 21);
            this.lblParcela.TabIndex = 17;
            this.lblParcela.TabStop = false;
            this.lblParcela.Text = "Parcela";
            // 
            // lblNumeroNota
            // 
            this.lblNumeroNota.BackColor = System.Drawing.Color.LightGray;
            this.lblNumeroNota.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblNumeroNota.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNumeroNota.Location = new System.Drawing.Point(8, 9);
            this.lblNumeroNota.Name = "lblNumeroNota";
            this.lblNumeroNota.ReadOnly = true;
            this.lblNumeroNota.Size = new System.Drawing.Size(136, 21);
            this.lblNumeroNota.TabIndex = 16;
            this.lblNumeroNota.TabStop = false;
            this.lblNumeroNota.Text = "Número da NF";
            // 
            // textNumeroNota
            // 
            this.textNumeroNota.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.textNumeroNota.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textNumeroNota.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textNumeroNota.Location = new System.Drawing.Point(143, 9);
            this.textNumeroNota.Name = "textNumeroNota";
            this.textNumeroNota.ReadOnly = true;
            this.textNumeroNota.Size = new System.Drawing.Size(364, 21);
            this.textNumeroNota.TabIndex = 31;
            this.textNumeroNota.TabStop = false;
            // 
            // lblForma
            // 
            this.lblForma.BackColor = System.Drawing.Color.LightGray;
            this.lblForma.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblForma.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblForma.Location = new System.Drawing.Point(8, 116);
            this.lblForma.Name = "lblForma";
            this.lblForma.ReadOnly = true;
            this.lblForma.Size = new System.Drawing.Size(136, 21);
            this.lblForma.TabIndex = 32;
            this.lblForma.TabStop = false;
            this.lblForma.Text = "Forma de Pagamento";
            // 
            // cbForma
            // 
            this.cbForma.BackColor = System.Drawing.Color.Silver;
            this.cbForma.BorderColor = System.Drawing.Color.DimGray;
            this.cbForma.BorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid;
            this.cbForma.FormattingEnabled = true;
            this.cbForma.Location = new System.Drawing.Point(143, 116);
            this.cbForma.Name = "cbForma";
            this.cbForma.Size = new System.Drawing.Size(364, 21);
            this.cbForma.TabIndex = 1;
            this.cbForma.SelectedIndexChanged += new System.EventHandler(this.cbForma_SelectedIndexChanged);
            // 
            // lblBanco
            // 
            this.lblBanco.BackColor = System.Drawing.Color.LightGray;
            this.lblBanco.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblBanco.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblBanco.Location = new System.Drawing.Point(8, 136);
            this.lblBanco.Name = "lblBanco";
            this.lblBanco.ReadOnly = true;
            this.lblBanco.Size = new System.Drawing.Size(136, 21);
            this.lblBanco.TabIndex = 34;
            this.lblBanco.TabStop = false;
            this.lblBanco.Text = "Banco";
            // 
            // textBanco
            // 
            this.textBanco.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.textBanco.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBanco.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBanco.Location = new System.Drawing.Point(143, 136);
            this.textBanco.Name = "textBanco";
            this.textBanco.ReadOnly = true;
            this.textBanco.Size = new System.Drawing.Size(331, 21);
            this.textBanco.TabIndex = 35;
            this.textBanco.TabStop = false;
            // 
            // btnBanco
            // 
            this.btnBanco.Enabled = false;
            this.btnBanco.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnBanco.Image = global::SWS.Properties.Resources.busca;
            this.btnBanco.Location = new System.Drawing.Point(473, 136);
            this.btnBanco.Name = "btnBanco";
            this.btnBanco.Size = new System.Drawing.Size(34, 21);
            this.btnBanco.TabIndex = 2;
            this.btnBanco.TabStop = false;
            this.btnBanco.UseVisualStyleBackColor = true;
            this.btnBanco.Click += new System.EventHandler(this.btnBanco_Click);
            // 
            // grbConta
            // 
            this.grbConta.Controls.Add(this.dgvConta);
            this.grbConta.Location = new System.Drawing.Point(8, 161);
            this.grbConta.Name = "grbConta";
            this.grbConta.Size = new System.Drawing.Size(499, 113);
            this.grbConta.TabIndex = 36;
            this.grbConta.TabStop = false;
            this.grbConta.Text = "Conta";
            // 
            // dgvConta
            // 
            this.dgvConta.AllowUserToAddRows = false;
            this.dgvConta.AllowUserToDeleteRows = false;
            this.dgvConta.AllowUserToOrderColumns = true;
            this.dgvConta.AllowUserToResizeRows = false;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.Gainsboro;
            this.dgvConta.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle1;
            this.dgvConta.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.DisplayedCells;
            this.dgvConta.BackgroundColor = System.Drawing.Color.White;
            this.dgvConta.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.Color.LightGray;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvConta.DefaultCellStyle = dataGridViewCellStyle2;
            this.dgvConta.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvConta.Location = new System.Drawing.Point(3, 16);
            this.dgvConta.MultiSelect = false;
            this.dgvConta.Name = "dgvConta";
            this.dgvConta.ReadOnly = true;
            this.dgvConta.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvConta.Size = new System.Drawing.Size(493, 94);
            this.dgvConta.TabIndex = 3;
            // 
            // frmCobrancaAlterarPagamento
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(534, 361);
            this.Name = "frmCobrancaAlterarPagamento";
            this.Text = "ALTERAR PAGAMENTO";
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.frmCobrancaAlterarPagamento_KeyDown);
            this.scForm.Panel1.ResumeLayout(false);
            this.scForm.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.scForm)).EndInit();
            this.scForm.ResumeLayout(false);
            this.pnlForm.ResumeLayout(false);
            this.pnlForm.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.banner)).EndInit();
            this.flpAcao.ResumeLayout(false);
            this.grbConta.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvConta)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.FlowLayoutPanel flpAcao;
        private System.Windows.Forms.Button btnConfirma;
        private System.Windows.Forms.Button btnFechar;
        private System.Windows.Forms.TextBox textCliente;
        private System.Windows.Forms.TextBox textFormaPagamento;
        private System.Windows.Forms.TextBox textParcela;
        private System.Windows.Forms.TextBox textDocumento;
        private System.Windows.Forms.TextBox lblCliente;
        private System.Windows.Forms.TextBox lblFormaPgto;
        private System.Windows.Forms.TextBox lblDocumento;
        private System.Windows.Forms.TextBox lblParcela;
        private System.Windows.Forms.TextBox lblNumeroNota;
        private System.Windows.Forms.TextBox textNumeroNota;
        private ComboBoxWithBorder cbForma;
        private System.Windows.Forms.TextBox lblForma;
        private System.Windows.Forms.TextBox textBanco;
        private System.Windows.Forms.TextBox lblBanco;
        private System.Windows.Forms.Button btnBanco;
        private System.Windows.Forms.GroupBox grbConta;
        private System.Windows.Forms.DataGridView dgvConta;
    }
}