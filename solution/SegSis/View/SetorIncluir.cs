﻿using SWS.Entidade;
using SWS.Facade;
using SWS.View.Resources;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace SWS.View
{
    public partial class frmSetorIncluir : frmTemplate
    {
        protected Setor setor;

        public Setor Setor
        {
            get { return setor; }
            set { setor = value; }
        }

        public frmSetorIncluir()
        {
            InitializeComponent();
            ActiveControl = textDescricao;
        }

        protected void btn_fechar_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        protected virtual void btn_gravar_Click(object sender, EventArgs e)
        {
            try
            {
                /* verificando se não foi preenchido a descrição */
                if (string.IsNullOrEmpty(textDescricao.Text.Trim()))
                {
                    textDescricao.Focus();
                    throw new Exception("O campo descrição é obrigatório");
                }

                /* verificando se a descrição não existe no banco de dados */
                EstudoFacade pcmsoFacade = EstudoFacade.getInstance();
                Setor setor = pcmsoFacade.findSetorByDescricao(textDescricao.Text);
                if (setor != null)
                {
                    textDescricao.Focus();
                    throw new Exception("Não é possivel incluir o setor. Descrição já cadastrada.");

                }

                Setor = pcmsoFacade.insertSetor(new Setor(null, textDescricao.Text, ApplicationConstants.ATIVO));
                MessageBox.Show("Setor cadastrado com sucesso", "Confirmação", MessageBoxButtons.OK, MessageBoxIcon.Information);
                this.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }
        
    }
}
