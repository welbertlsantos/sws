﻿namespace SWS.View
{
    partial class frmRelatorioAnual
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.flpAcao = new System.Windows.Forms.FlowLayoutPanel();
            this.btnFechar = new System.Windows.Forms.Button();
            this.btnImprimir = new System.Windows.Forms.Button();
            this.btnExcluirCliente = new System.Windows.Forms.Button();
            this.btCliente = new System.Windows.Forms.Button();
            this.textCliente = new System.Windows.Forms.TextBox();
            this.lblCliente = new System.Windows.Forms.TextBox();
            this.btnExcluirUnidade = new System.Windows.Forms.Button();
            this.btnUnidade = new System.Windows.Forms.Button();
            this.textUnidade = new System.Windows.Forms.TextBox();
            this.lblUnidade = new System.Windows.Forms.TextBox();
            this.lblEmissao = new System.Windows.Forms.TextBox();
            this.dataEmissaoInicial = new System.Windows.Forms.DateTimePicker();
            this.dataEmissaoFinal = new System.Windows.Forms.DateTimePicker();
            this.cbSomenteMatriz = new SWS.ComboBoxWithBorder();
            this.lblSomenteMatriz = new System.Windows.Forms.TextBox();
            this.lbAcompanhamento = new System.Windows.Forms.ListBox();
            this.lblCentroCusto = new System.Windows.Forms.TextBox();
            this.cbCentroCusto = new SWS.ComboBoxWithBorder();
            ((System.ComponentModel.ISupportInitialize)(this.scForm)).BeginInit();
            this.scForm.Panel1.SuspendLayout();
            this.scForm.Panel2.SuspendLayout();
            this.scForm.SuspendLayout();
            this.pnlForm.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.banner)).BeginInit();
            this.flpAcao.SuspendLayout();
            this.SuspendLayout();
            // 
            // scForm
            // 
            // 
            // scForm.Panel1
            // 
            this.scForm.Panel1.Controls.Add(this.flpAcao);
            this.scForm.SplitterDistance = 36;
            // 
            // pnlForm
            // 
            this.pnlForm.Controls.Add(this.cbCentroCusto);
            this.pnlForm.Controls.Add(this.lbAcompanhamento);
            this.pnlForm.Controls.Add(this.cbSomenteMatriz);
            this.pnlForm.Controls.Add(this.lblSomenteMatriz);
            this.pnlForm.Controls.Add(this.lblEmissao);
            this.pnlForm.Controls.Add(this.dataEmissaoInicial);
            this.pnlForm.Controls.Add(this.dataEmissaoFinal);
            this.pnlForm.Controls.Add(this.lblCentroCusto);
            this.pnlForm.Controls.Add(this.btnExcluirUnidade);
            this.pnlForm.Controls.Add(this.btnUnidade);
            this.pnlForm.Controls.Add(this.textUnidade);
            this.pnlForm.Controls.Add(this.lblUnidade);
            this.pnlForm.Controls.Add(this.btnExcluirCliente);
            this.pnlForm.Controls.Add(this.btCliente);
            this.pnlForm.Controls.Add(this.textCliente);
            this.pnlForm.Controls.Add(this.lblCliente);
            // 
            // flpAcao
            // 
            this.flpAcao.Controls.Add(this.btnFechar);
            this.flpAcao.Controls.Add(this.btnImprimir);
            this.flpAcao.Dock = System.Windows.Forms.DockStyle.Fill;
            this.flpAcao.Location = new System.Drawing.Point(0, 0);
            this.flpAcao.Margin = new System.Windows.Forms.Padding(2);
            this.flpAcao.Name = "flpAcao";
            this.flpAcao.Size = new System.Drawing.Size(534, 36);
            this.flpAcao.TabIndex = 0;
            // 
            // btnFechar
            // 
            this.btnFechar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnFechar.Image = global::SWS.Properties.Resources.close;
            this.btnFechar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnFechar.Location = new System.Drawing.Point(3, 3);
            this.btnFechar.Name = "btnFechar";
            this.btnFechar.Size = new System.Drawing.Size(75, 23);
            this.btnFechar.TabIndex = 3;
            this.btnFechar.Text = "&Fechar";
            this.btnFechar.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnFechar.UseVisualStyleBackColor = true;
            this.btnFechar.Click += new System.EventHandler(this.btnFechar_Click);
            // 
            // btnImprimir
            // 
            this.btnImprimir.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnImprimir.Image = global::SWS.Properties.Resources.impressora1;
            this.btnImprimir.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnImprimir.Location = new System.Drawing.Point(84, 3);
            this.btnImprimir.Name = "btnImprimir";
            this.btnImprimir.Size = new System.Drawing.Size(75, 23);
            this.btnImprimir.TabIndex = 2;
            this.btnImprimir.Text = "&Imprimir";
            this.btnImprimir.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnImprimir.UseVisualStyleBackColor = true;
            this.btnImprimir.Click += new System.EventHandler(this.btnImprimir_Click);
            // 
            // btnExcluirCliente
            // 
            this.btnExcluirCliente.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnExcluirCliente.Image = global::SWS.Properties.Resources.close;
            this.btnExcluirCliente.Location = new System.Drawing.Point(474, 7);
            this.btnExcluirCliente.Name = "btnExcluirCliente";
            this.btnExcluirCliente.Size = new System.Drawing.Size(30, 21);
            this.btnExcluirCliente.TabIndex = 40;
            this.btnExcluirCliente.UseVisualStyleBackColor = true;
            this.btnExcluirCliente.Click += new System.EventHandler(this.btnExcluirCliente_Click);
            // 
            // btCliente
            // 
            this.btCliente.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btCliente.Image = global::SWS.Properties.Resources.busca;
            this.btCliente.Location = new System.Drawing.Point(445, 7);
            this.btCliente.Name = "btCliente";
            this.btCliente.Size = new System.Drawing.Size(30, 21);
            this.btCliente.TabIndex = 38;
            this.btCliente.UseVisualStyleBackColor = true;
            this.btCliente.Click += new System.EventHandler(this.btCliente_Click);
            // 
            // textCliente
            // 
            this.textCliente.BackColor = System.Drawing.SystemColors.Control;
            this.textCliente.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textCliente.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textCliente.Location = new System.Drawing.Point(144, 7);
            this.textCliente.MaxLength = 100;
            this.textCliente.Name = "textCliente";
            this.textCliente.ReadOnly = true;
            this.textCliente.Size = new System.Drawing.Size(303, 21);
            this.textCliente.TabIndex = 39;
            this.textCliente.TabStop = false;
            // 
            // lblCliente
            // 
            this.lblCliente.BackColor = System.Drawing.Color.LightGray;
            this.lblCliente.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblCliente.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCliente.Location = new System.Drawing.Point(7, 7);
            this.lblCliente.Name = "lblCliente";
            this.lblCliente.ReadOnly = true;
            this.lblCliente.Size = new System.Drawing.Size(138, 21);
            this.lblCliente.TabIndex = 37;
            this.lblCliente.TabStop = false;
            this.lblCliente.Text = "Cliente";
            // 
            // btnExcluirUnidade
            // 
            this.btnExcluirUnidade.Enabled = false;
            this.btnExcluirUnidade.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnExcluirUnidade.Image = global::SWS.Properties.Resources.close;
            this.btnExcluirUnidade.Location = new System.Drawing.Point(474, 27);
            this.btnExcluirUnidade.Name = "btnExcluirUnidade";
            this.btnExcluirUnidade.Size = new System.Drawing.Size(30, 21);
            this.btnExcluirUnidade.TabIndex = 44;
            this.btnExcluirUnidade.UseVisualStyleBackColor = true;
            this.btnExcluirUnidade.Click += new System.EventHandler(this.btnExcluirUnidade_Click);
            // 
            // btnUnidade
            // 
            this.btnUnidade.Enabled = false;
            this.btnUnidade.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnUnidade.Image = global::SWS.Properties.Resources.busca;
            this.btnUnidade.Location = new System.Drawing.Point(445, 27);
            this.btnUnidade.Name = "btnUnidade";
            this.btnUnidade.Size = new System.Drawing.Size(30, 21);
            this.btnUnidade.TabIndex = 42;
            this.btnUnidade.UseVisualStyleBackColor = true;
            this.btnUnidade.Click += new System.EventHandler(this.btnUnidade_Click);
            // 
            // textUnidade
            // 
            this.textUnidade.BackColor = System.Drawing.SystemColors.Control;
            this.textUnidade.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textUnidade.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textUnidade.Location = new System.Drawing.Point(144, 27);
            this.textUnidade.MaxLength = 100;
            this.textUnidade.Name = "textUnidade";
            this.textUnidade.ReadOnly = true;
            this.textUnidade.Size = new System.Drawing.Size(303, 21);
            this.textUnidade.TabIndex = 43;
            this.textUnidade.TabStop = false;
            // 
            // lblUnidade
            // 
            this.lblUnidade.BackColor = System.Drawing.Color.LightGray;
            this.lblUnidade.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblUnidade.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblUnidade.Location = new System.Drawing.Point(7, 27);
            this.lblUnidade.Name = "lblUnidade";
            this.lblUnidade.ReadOnly = true;
            this.lblUnidade.Size = new System.Drawing.Size(138, 21);
            this.lblUnidade.TabIndex = 41;
            this.lblUnidade.TabStop = false;
            this.lblUnidade.Text = "Unidade";
            // 
            // lblEmissao
            // 
            this.lblEmissao.BackColor = System.Drawing.Color.LightGray;
            this.lblEmissao.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblEmissao.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblEmissao.Location = new System.Drawing.Point(7, 87);
            this.lblEmissao.Name = "lblEmissao";
            this.lblEmissao.ReadOnly = true;
            this.lblEmissao.Size = new System.Drawing.Size(138, 21);
            this.lblEmissao.TabIndex = 49;
            this.lblEmissao.TabStop = false;
            this.lblEmissao.Text = "Período";
            // 
            // dataEmissaoInicial
            // 
            this.dataEmissaoInicial.Checked = false;
            this.dataEmissaoInicial.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dataEmissaoInicial.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dataEmissaoInicial.Location = new System.Drawing.Point(144, 87);
            this.dataEmissaoInicial.Name = "dataEmissaoInicial";
            this.dataEmissaoInicial.ShowCheckBox = true;
            this.dataEmissaoInicial.Size = new System.Drawing.Size(107, 21);
            this.dataEmissaoInicial.TabIndex = 50;
            // 
            // dataEmissaoFinal
            // 
            this.dataEmissaoFinal.Checked = false;
            this.dataEmissaoFinal.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dataEmissaoFinal.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dataEmissaoFinal.Location = new System.Drawing.Point(250, 87);
            this.dataEmissaoFinal.Name = "dataEmissaoFinal";
            this.dataEmissaoFinal.ShowCheckBox = true;
            this.dataEmissaoFinal.Size = new System.Drawing.Size(107, 21);
            this.dataEmissaoFinal.TabIndex = 51;
            // 
            // cbSomenteMatriz
            // 
            this.cbSomenteMatriz.BackColor = System.Drawing.Color.LightGray;
            this.cbSomenteMatriz.BorderColor = System.Drawing.Color.DimGray;
            this.cbSomenteMatriz.BorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid;
            this.cbSomenteMatriz.FormattingEnabled = true;
            this.cbSomenteMatriz.Location = new System.Drawing.Point(144, 67);
            this.cbSomenteMatriz.Name = "cbSomenteMatriz";
            this.cbSomenteMatriz.Size = new System.Drawing.Size(360, 21);
            this.cbSomenteMatriz.TabIndex = 53;
            // 
            // lblSomenteMatriz
            // 
            this.lblSomenteMatriz.BackColor = System.Drawing.Color.LightGray;
            this.lblSomenteMatriz.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblSomenteMatriz.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSomenteMatriz.Location = new System.Drawing.Point(7, 67);
            this.lblSomenteMatriz.Name = "lblSomenteMatriz";
            this.lblSomenteMatriz.ReadOnly = true;
            this.lblSomenteMatriz.Size = new System.Drawing.Size(138, 21);
            this.lblSomenteMatriz.TabIndex = 52;
            this.lblSomenteMatriz.TabStop = false;
            this.lblSomenteMatriz.Text = "Somente Matriz?";
            // 
            // lbAcompanhamento
            // 
            this.lbAcompanhamento.BackColor = System.Drawing.Color.LightGray;
            this.lbAcompanhamento.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.lbAcompanhamento.FormattingEnabled = true;
            this.lbAcompanhamento.Location = new System.Drawing.Point(7, 114);
            this.lbAcompanhamento.Name = "lbAcompanhamento";
            this.lbAcompanhamento.Size = new System.Drawing.Size(497, 156);
            this.lbAcompanhamento.TabIndex = 54;
            // 
            // lblCentroCusto
            // 
            this.lblCentroCusto.BackColor = System.Drawing.Color.LightGray;
            this.lblCentroCusto.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblCentroCusto.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCentroCusto.Location = new System.Drawing.Point(7, 47);
            this.lblCentroCusto.Name = "lblCentroCusto";
            this.lblCentroCusto.ReadOnly = true;
            this.lblCentroCusto.Size = new System.Drawing.Size(138, 21);
            this.lblCentroCusto.TabIndex = 45;
            this.lblCentroCusto.TabStop = false;
            this.lblCentroCusto.Text = "Centro de Custo";
            // 
            // cbCentroCusto
            // 
            this.cbCentroCusto.BackColor = System.Drawing.Color.LightGray;
            this.cbCentroCusto.BorderColor = System.Drawing.Color.DimGray;
            this.cbCentroCusto.BorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid;
            this.cbCentroCusto.Enabled = false;
            this.cbCentroCusto.FormattingEnabled = true;
            this.cbCentroCusto.Location = new System.Drawing.Point(144, 47);
            this.cbCentroCusto.Name = "cbCentroCusto";
            this.cbCentroCusto.Size = new System.Drawing.Size(360, 21);
            this.cbCentroCusto.TabIndex = 55;
            this.cbCentroCusto.SelectedIndexChanged += new System.EventHandler(this.cbCentroCusto_SelectedIndexChanged);
            // 
            // frmRelatorioAnual
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(534, 361);
            this.Margin = new System.Windows.Forms.Padding(2);
            this.Name = "frmRelatorioAnual";
            this.Text = "RELATÓRIO ANUAL";
            this.scForm.Panel1.ResumeLayout(false);
            this.scForm.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.scForm)).EndInit();
            this.scForm.ResumeLayout(false);
            this.pnlForm.ResumeLayout(false);
            this.pnlForm.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.banner)).EndInit();
            this.flpAcao.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.FlowLayoutPanel flpAcao;
        private System.Windows.Forms.Button btnFechar;
        private System.Windows.Forms.Button btnImprimir;
        private System.Windows.Forms.Button btnExcluirCliente;
        private System.Windows.Forms.Button btCliente;
        public System.Windows.Forms.TextBox textCliente;
        private System.Windows.Forms.TextBox lblCliente;
        private System.Windows.Forms.Button btnExcluirUnidade;
        private System.Windows.Forms.Button btnUnidade;
        public System.Windows.Forms.TextBox textUnidade;
        private System.Windows.Forms.TextBox lblUnidade;
        private System.Windows.Forms.TextBox lblEmissao;
        private System.Windows.Forms.DateTimePicker dataEmissaoInicial;
        private System.Windows.Forms.DateTimePicker dataEmissaoFinal;
        private ComboBoxWithBorder cbSomenteMatriz;
        private System.Windows.Forms.TextBox lblSomenteMatriz;
        private System.Windows.Forms.ListBox lbAcompanhamento;
        private ComboBoxWithBorder cbCentroCusto;
        private System.Windows.Forms.TextBox lblCentroCusto;
    }
}