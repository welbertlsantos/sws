﻿using SWS.Entidade;
using SWS.Facade;
using SWS.Resources;
using SWS.View.ViewHelper;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace SWS.View
{
    public partial class frmAtendimentoIncluirConfirma : frmTemplateConsulta
    {
        private List<GheFonteAgenteExameAso> examesInPcmso = new List<GheFonteAgenteExameAso>();
        private List<ClienteFuncaoExameASo> examesExtra = new List<ClienteFuncaoExameASo>();
        private List<GheFonteAgenteExameAso> examesRepetidosPcmso = new List<GheFonteAgenteExameAso>();
        private Aso atendimento;

        public Aso Atendimento
        {
            get { return atendimento; }
            set { atendimento = value; }
        }
        private List<GheSetor> gheSetorInPcmso = new List<GheSetor>();
        private HashSet<Agente> riscoInPcmso = new HashSet<Agente>();
        
        
        public frmAtendimentoIncluirConfirma(List<GheFonteAgenteExameAso> examesInPcmso, List<ClienteFuncaoExameASo> examesExtra, List<GheFonteAgenteExameAso> examesRepetidosPcmso, Aso atendimento, List<GheSetor> gheSetorInPcmso, HashSet<Agente> riscoInPcmso)
        {
            InitializeComponent();
            this.examesInPcmso = examesInPcmso;
            this.examesExtra = examesExtra;
            this.examesRepetidosPcmso = examesRepetidosPcmso;
            this.atendimento = atendimento;
            this.gheSetorInPcmso = gheSetorInPcmso;
            this.riscoInPcmso = riscoInPcmso;
            montaLista();
        }


        private void montaLista()
        {
            listBoxExame.Items.Add("O atendimento terá o(s) seguinte(s) exame(s).");
            listBoxExame.Items.Add("");
            if (examesInPcmso.Count > 0)
            {
                listBoxExame.Items.Add("Exame(s) do PCMSO:");
                listBoxExame.Items.Add("");
            }

            foreach (GheFonteAgenteExameAso gheFonteAgenteExameAso in examesInPcmso)
            {
                string transcrito = gheFonteAgenteExameAso.Transcrito == true ? " - Transcrito" : "";
                listBoxExame.Items.AddRange( new Object[] { "Exame: " + gheFonteAgenteExameAso.GheFonteAgenteExame.Exame.Descricao + " - Data do Exame: " + String.Format("{0:dd/MM/yyyy} ", gheFonteAgenteExameAso.DataExame) + " " + transcrito });
            }
            
            listBoxExame.Items.Add("");

            if (examesExtra.Count > 0)
            {
                listBoxExame.Items.Add("Exame(s) avulso(s)");
                listBoxExame.Items.Add("");
            }

            foreach (ClienteFuncaoExameASo clienteFuncaoExameAso in examesExtra)
            {
                string transcrito = clienteFuncaoExameAso.Transcrito == true ? " - Transcrito" : "";
                listBoxExame.Items.AddRange( new Object [] { "Exame: " + clienteFuncaoExameAso.ClienteFuncaoExame.Exame.Descricao + " - Data do Exame: " + String.Format("{0:dd/MM/yyyy} ", clienteFuncaoExameAso.DataExame) + " " + transcrito } );
            }

            listBoxExame.Items.Add("");

        }

        private void btn_cancela_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnGravar_Click(object sender, EventArgs e)
        {
            try
            {
                /* gravando o atendimento selecionado */
                AsoFacade asoFacade = AsoFacade.getInstance();
                FinanceiroFacade financeiroFacade = FinanceiroFacade.getInstance();

                if (MessageBox.Show("Deseja gravar o atendimento?", "Confirmação", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                {
                    this.Cursor = Cursors.WaitCursor;

                    atendimento = asoFacade.incluirAso(atendimento, examesInPcmso, examesExtra, examesRepetidosPcmso, gheSetorInPcmso, riscoInPcmso);

                    /* preenchendo informação do código do atendimento na lista */

                    listBoxExame.Items.AddRange(new Object[] { "Código do atendimento: " + ValidaCampoHelper.RetornaCodigoAtendimentoFormatado(atendimento.Codigo) });
                    //System.Threading.Thread.Sleep(2000);
                    Refresh();

                    MessageBox.Show("Atendimento Gravado com sucesso", "Confirmação", MessageBoxButtons.OK, MessageBoxIcon.Information);

                    /* verificando se o aso gravado foi um aso demissional. Nesse caso o usuário deverá informar
                     * a data de demissão do funcionário */

                    if (atendimento.Periodicidade.Id == 5)
                    {
                        MessageBox.Show("O aso é um aso Demissional. Você deverá informar a data de demissão do funcionário que será armazenada para posterior impressão do PPP.", "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Information);

                        FuncionarioFacade funcionarioFacade = FuncionarioFacade.getInstance();

                        List<ClienteFuncaoFuncionario> clienteFuncaoFuncionarios = funcionarioFacade.findClienteFuncaoFuncionarioByFuncionarioAndSituacao(atendimento.ClienteFuncaoFuncionario.Funcionario, true);

                        frmClienteFuncaoFuncionarioExcluir desativarFuncionario = new frmClienteFuncaoFuncionarioExcluir(clienteFuncaoFuncionarios.Find(x => x.ClienteFuncao.Cliente.Id == atendimento.ClienteFuncaoFuncionario.ClienteFuncao.Cliente.Id && x.DataDesligamento == null));
                        desativarFuncionario.ShowDialog();

                        /* desativando o funcionário na empresa */
                        funcionarioFacade.disableClienteFuncaoFuncionario(atendimento.ClienteFuncaoFuncionario, (DateTime)desativarFuncionario.ClienteFuncaoFuncionario.DataDesligamento);

                        MessageBox.Show("Gravação da Data de demissão concluída com sucesso.", "Confirmação", MessageBoxButtons.OK, MessageBoxIcon.Information);

                    }

                    /* recuperando informações do atendimento. */

                    atendimento = asoFacade.findAtendimentoById((long)atendimento.Id);

                    /* verificando se existe algum exame que já foi finalizado no momento da criação do atendimento.
                     * caso exista então deverá ser verificado se a empresa trabalha por gravação no movimento pela
                     * finalização do atendimento ou pela finalização do exame. */

                    string gravaMovimentoFinalizacaoAtendimento;
                    Dictionary<string, string> mapaConfiguracao = PermissionamentoFacade.mapaConfiguracoes;
                    mapaConfiguracao.TryGetValue(ConfigurationConstans.GERA_MOVIMENTO_FINALIZACAO_EXAME, out gravaMovimentoFinalizacaoAtendimento);

                    if (Convert.ToBoolean(gravaMovimentoFinalizacaoAtendimento))
                    {
                        examesInPcmso.ForEach(x => x.Aso = atendimento);
                        examesExtra.ForEach(x => x.Aso = atendimento);
                        
                        examesInPcmso.FindAll(x => x.Finalizado == true)
                            .ForEach(delegate(GheFonteAgenteExameAso gfaea)
                        {
                            financeiroFacade.incluirMovimentoPorItem(gfaea, null);
                        });
                        examesExtra.FindAll(x => x.Finalizado == true)
                            .ForEach(delegate(ClienteFuncaoExameASo cfea)
                        {
                            financeiroFacade.incluirMovimentoPorItem(null, cfea);
                        });
                    }
                    
                    frmAtendimentoImprimirEncaminhamento formAsoImprimirEncaminhamento = new frmAtendimentoImprimirEncaminhamento(atendimento, false);
                    formAsoImprimirEncaminhamento.ShowDialog();

                    this.Close();
                }


            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }

        

    }
}
