﻿namespace SWS.View
{
    partial class frmPcmsoObraIncluir
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        protected System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        protected void InitializeComponent()
        {
            this.flpAcao = new System.Windows.Forms.FlowLayoutPanel();
            this.btnObraGravar = new System.Windows.Forms.Button();
            this.btnFechar = new System.Windows.Forms.Button();
            this.lblApelidoObra = new System.Windows.Forms.TextBox();
            this.textObra = new System.Windows.Forms.TextBox();
            this.lblEndereco = new System.Windows.Forms.TextBox();
            this.textEnderecoObra = new System.Windows.Forms.TextBox();
            this.lblNumero = new System.Windows.Forms.TextBox();
            this.textNumeroObra = new System.Windows.Forms.TextBox();
            this.lblComplemento = new System.Windows.Forms.TextBox();
            this.textComplementoObra = new System.Windows.Forms.TextBox();
            this.lblBairro = new System.Windows.Forms.TextBox();
            this.textBairroObra = new System.Windows.Forms.TextBox();
            this.lblUnidadeFederativa = new System.Windows.Forms.TextBox();
            this.cbUnidadeFederativa = new SWS.ComboBoxWithBorder();
            this.lblCidade = new System.Windows.Forms.TextBox();
            this.btnCidadeIncluir = new System.Windows.Forms.Button();
            this.textCidadeObra = new System.Windows.Forms.TextBox();
            this.btnCidadeExcluir = new System.Windows.Forms.Button();
            this.lblCep = new System.Windows.Forms.TextBox();
            this.textCEPObra = new System.Windows.Forms.MaskedTextBox();
            this.lblDescritivo = new System.Windows.Forms.TextBox();
            this.textDescritivoObra = new System.Windows.Forms.TextBox();
            this.lblObrigatorioLocal = new System.Windows.Forms.Label();
            this.lblObrigatorioEndereco = new System.Windows.Forms.Label();
            this.lblObrigatorioNumero = new System.Windows.Forms.Label();
            this.lblObrigatorioBairro = new System.Windows.Forms.Label();
            this.lblObrigatorioUnidadeFederativa = new System.Windows.Forms.Label();
            this.lblObrigatorioCidade = new System.Windows.Forms.Label();
            this.lblObrigatorioCEP = new System.Windows.Forms.Label();
            this.lblExplicacaoObrigatorio = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.scForm)).BeginInit();
            this.scForm.Panel1.SuspendLayout();
            this.scForm.Panel2.SuspendLayout();
            this.scForm.SuspendLayout();
            this.pnlForm.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.banner)).BeginInit();
            this.flpAcao.SuspendLayout();
            this.SuspendLayout();
            // 
            // scForm
            // 
            // 
            // scForm.Panel1
            // 
            this.scForm.Panel1.Controls.Add(this.flpAcao);
            // 
            // pnlForm
            // 
            this.pnlForm.Controls.Add(this.lblExplicacaoObrigatorio);
            this.pnlForm.Controls.Add(this.lblObrigatorioCEP);
            this.pnlForm.Controls.Add(this.lblObrigatorioCidade);
            this.pnlForm.Controls.Add(this.lblObrigatorioUnidadeFederativa);
            this.pnlForm.Controls.Add(this.lblObrigatorioBairro);
            this.pnlForm.Controls.Add(this.lblObrigatorioNumero);
            this.pnlForm.Controls.Add(this.lblObrigatorioEndereco);
            this.pnlForm.Controls.Add(this.lblObrigatorioLocal);
            this.pnlForm.Controls.Add(this.textDescritivoObra);
            this.pnlForm.Controls.Add(this.lblDescritivo);
            this.pnlForm.Controls.Add(this.textCEPObra);
            this.pnlForm.Controls.Add(this.lblCep);
            this.pnlForm.Controls.Add(this.btnCidadeExcluir);
            this.pnlForm.Controls.Add(this.lblCidade);
            this.pnlForm.Controls.Add(this.btnCidadeIncluir);
            this.pnlForm.Controls.Add(this.textCidadeObra);
            this.pnlForm.Controls.Add(this.cbUnidadeFederativa);
            this.pnlForm.Controls.Add(this.lblUnidadeFederativa);
            this.pnlForm.Controls.Add(this.textBairroObra);
            this.pnlForm.Controls.Add(this.lblBairro);
            this.pnlForm.Controls.Add(this.textComplementoObra);
            this.pnlForm.Controls.Add(this.lblComplemento);
            this.pnlForm.Controls.Add(this.textNumeroObra);
            this.pnlForm.Controls.Add(this.lblNumero);
            this.pnlForm.Controls.Add(this.textEnderecoObra);
            this.pnlForm.Controls.Add(this.lblEndereco);
            this.pnlForm.Controls.Add(this.textObra);
            this.pnlForm.Controls.Add(this.lblApelidoObra);
            // 
            // flpAcao
            // 
            this.flpAcao.Controls.Add(this.btnObraGravar);
            this.flpAcao.Controls.Add(this.btnFechar);
            this.flpAcao.Dock = System.Windows.Forms.DockStyle.Fill;
            this.flpAcao.Location = new System.Drawing.Point(0, 0);
            this.flpAcao.Name = "flpAcao";
            this.flpAcao.Size = new System.Drawing.Size(534, 30);
            this.flpAcao.TabIndex = 0;
            // 
            // btnObraGravar
            // 
            this.btnObraGravar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnObraGravar.Image = global::SWS.Properties.Resources.Gravar;
            this.btnObraGravar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnObraGravar.Location = new System.Drawing.Point(3, 3);
            this.btnObraGravar.Name = "btnObraGravar";
            this.btnObraGravar.Size = new System.Drawing.Size(75, 23);
            this.btnObraGravar.TabIndex = 10;
            this.btnObraGravar.Text = "&Gravar";
            this.btnObraGravar.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnObraGravar.UseVisualStyleBackColor = true;
            this.btnObraGravar.Click += new System.EventHandler(this.btnObraGravar_Click);
            // 
            // btnFechar
            // 
            this.btnFechar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnFechar.Image = global::SWS.Properties.Resources.close;
            this.btnFechar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnFechar.Location = new System.Drawing.Point(84, 3);
            this.btnFechar.Name = "btnFechar";
            this.btnFechar.Size = new System.Drawing.Size(75, 23);
            this.btnFechar.TabIndex = 11;
            this.btnFechar.TabStop = false;
            this.btnFechar.Text = "Fechar";
            this.btnFechar.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnFechar.UseVisualStyleBackColor = true;
            this.btnFechar.Click += new System.EventHandler(this.btnFechar_Click);
            // 
            // lblApelidoObra
            // 
            this.lblApelidoObra.BackColor = System.Drawing.Color.LightGray;
            this.lblApelidoObra.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblApelidoObra.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblApelidoObra.Location = new System.Drawing.Point(3, 3);
            this.lblApelidoObra.MaxLength = 15;
            this.lblApelidoObra.Name = "lblApelidoObra";
            this.lblApelidoObra.ReadOnly = true;
            this.lblApelidoObra.Size = new System.Drawing.Size(147, 21);
            this.lblApelidoObra.TabIndex = 1;
            this.lblApelidoObra.TabStop = false;
            this.lblApelidoObra.Text = "Local / Obra";
            // 
            // textObra
            // 
            this.textObra.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.textObra.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textObra.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.textObra.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textObra.Location = new System.Drawing.Point(149, 3);
            this.textObra.MaxLength = 255;
            this.textObra.Name = "textObra";
            this.textObra.Size = new System.Drawing.Size(342, 21);
            this.textObra.TabIndex = 1;
            // 
            // lblEndereco
            // 
            this.lblEndereco.BackColor = System.Drawing.Color.LightGray;
            this.lblEndereco.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblEndereco.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblEndereco.Location = new System.Drawing.Point(3, 23);
            this.lblEndereco.MaxLength = 15;
            this.lblEndereco.Name = "lblEndereco";
            this.lblEndereco.ReadOnly = true;
            this.lblEndereco.Size = new System.Drawing.Size(147, 21);
            this.lblEndereco.TabIndex = 11;
            this.lblEndereco.TabStop = false;
            this.lblEndereco.Text = "Endereço";
            // 
            // textEnderecoObra
            // 
            this.textEnderecoObra.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.textEnderecoObra.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textEnderecoObra.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.textEnderecoObra.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textEnderecoObra.Location = new System.Drawing.Point(149, 23);
            this.textEnderecoObra.MaxLength = 255;
            this.textEnderecoObra.Name = "textEnderecoObra";
            this.textEnderecoObra.Size = new System.Drawing.Size(342, 21);
            this.textEnderecoObra.TabIndex = 2;
            // 
            // lblNumero
            // 
            this.lblNumero.BackColor = System.Drawing.Color.LightGray;
            this.lblNumero.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblNumero.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNumero.Location = new System.Drawing.Point(3, 43);
            this.lblNumero.MaxLength = 15;
            this.lblNumero.Name = "lblNumero";
            this.lblNumero.ReadOnly = true;
            this.lblNumero.Size = new System.Drawing.Size(147, 21);
            this.lblNumero.TabIndex = 13;
            this.lblNumero.TabStop = false;
            this.lblNumero.Text = "Número";
            // 
            // textNumeroObra
            // 
            this.textNumeroObra.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.textNumeroObra.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textNumeroObra.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.textNumeroObra.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textNumeroObra.Location = new System.Drawing.Point(149, 43);
            this.textNumeroObra.MaxLength = 255;
            this.textNumeroObra.Name = "textNumeroObra";
            this.textNumeroObra.Size = new System.Drawing.Size(342, 21);
            this.textNumeroObra.TabIndex = 3;
            // 
            // lblComplemento
            // 
            this.lblComplemento.BackColor = System.Drawing.Color.LightGray;
            this.lblComplemento.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblComplemento.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblComplemento.Location = new System.Drawing.Point(3, 63);
            this.lblComplemento.MaxLength = 15;
            this.lblComplemento.Name = "lblComplemento";
            this.lblComplemento.ReadOnly = true;
            this.lblComplemento.Size = new System.Drawing.Size(147, 21);
            this.lblComplemento.TabIndex = 15;
            this.lblComplemento.TabStop = false;
            this.lblComplemento.Text = "Complemento";
            // 
            // textComplementoObra
            // 
            this.textComplementoObra.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.textComplementoObra.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textComplementoObra.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.textComplementoObra.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textComplementoObra.Location = new System.Drawing.Point(149, 63);
            this.textComplementoObra.MaxLength = 255;
            this.textComplementoObra.Name = "textComplementoObra";
            this.textComplementoObra.Size = new System.Drawing.Size(342, 21);
            this.textComplementoObra.TabIndex = 4;
            // 
            // lblBairro
            // 
            this.lblBairro.BackColor = System.Drawing.Color.LightGray;
            this.lblBairro.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblBairro.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblBairro.Location = new System.Drawing.Point(3, 83);
            this.lblBairro.MaxLength = 15;
            this.lblBairro.Name = "lblBairro";
            this.lblBairro.ReadOnly = true;
            this.lblBairro.Size = new System.Drawing.Size(147, 21);
            this.lblBairro.TabIndex = 17;
            this.lblBairro.TabStop = false;
            this.lblBairro.Text = "Bairro";
            // 
            // textBairroObra
            // 
            this.textBairroObra.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.textBairroObra.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBairroObra.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.textBairroObra.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBairroObra.Location = new System.Drawing.Point(149, 83);
            this.textBairroObra.MaxLength = 255;
            this.textBairroObra.Name = "textBairroObra";
            this.textBairroObra.Size = new System.Drawing.Size(342, 21);
            this.textBairroObra.TabIndex = 5;
            // 
            // lblUnidadeFederativa
            // 
            this.lblUnidadeFederativa.BackColor = System.Drawing.Color.LightGray;
            this.lblUnidadeFederativa.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblUnidadeFederativa.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblUnidadeFederativa.Location = new System.Drawing.Point(3, 103);
            this.lblUnidadeFederativa.MaxLength = 15;
            this.lblUnidadeFederativa.Name = "lblUnidadeFederativa";
            this.lblUnidadeFederativa.ReadOnly = true;
            this.lblUnidadeFederativa.Size = new System.Drawing.Size(147, 21);
            this.lblUnidadeFederativa.TabIndex = 19;
            this.lblUnidadeFederativa.TabStop = false;
            this.lblUnidadeFederativa.Text = "Unidade Federativa";
            // 
            // cbUnidadeFederativa
            // 
            this.cbUnidadeFederativa.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.cbUnidadeFederativa.BackColor = System.Drawing.Color.LightGray;
            this.cbUnidadeFederativa.BorderColor = System.Drawing.Color.DimGray;
            this.cbUnidadeFederativa.BorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid;
            this.cbUnidadeFederativa.FormattingEnabled = true;
            this.cbUnidadeFederativa.Location = new System.Drawing.Point(149, 103);
            this.cbUnidadeFederativa.Name = "cbUnidadeFederativa";
            this.cbUnidadeFederativa.Size = new System.Drawing.Size(342, 21);
            this.cbUnidadeFederativa.TabIndex = 6;
            // 
            // lblCidade
            // 
            this.lblCidade.BackColor = System.Drawing.Color.LightGray;
            this.lblCidade.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblCidade.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCidade.Location = new System.Drawing.Point(3, 123);
            this.lblCidade.Name = "lblCidade";
            this.lblCidade.ReadOnly = true;
            this.lblCidade.Size = new System.Drawing.Size(147, 21);
            this.lblCidade.TabIndex = 43;
            this.lblCidade.TabStop = false;
            this.lblCidade.Text = "Cidade";
            // 
            // btnCidadeIncluir
            // 
            this.btnCidadeIncluir.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnCidadeIncluir.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnCidadeIncluir.Image = global::SWS.Properties.Resources.busca;
            this.btnCidadeIncluir.Location = new System.Drawing.Point(424, 123);
            this.btnCidadeIncluir.Name = "btnCidadeIncluir";
            this.btnCidadeIncluir.Size = new System.Drawing.Size(34, 21);
            this.btnCidadeIncluir.TabIndex = 7;
            this.btnCidadeIncluir.UseVisualStyleBackColor = true;
            this.btnCidadeIncluir.Click += new System.EventHandler(this.btnCidadeIncluir_Click);
            // 
            // textCidadeObra
            // 
            this.textCidadeObra.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.textCidadeObra.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.textCidadeObra.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textCidadeObra.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textCidadeObra.Location = new System.Drawing.Point(149, 123);
            this.textCidadeObra.Name = "textCidadeObra";
            this.textCidadeObra.ReadOnly = true;
            this.textCidadeObra.Size = new System.Drawing.Size(277, 21);
            this.textCidadeObra.TabIndex = 42;
            this.textCidadeObra.TabStop = false;
            // 
            // btnCidadeExcluir
            // 
            this.btnCidadeExcluir.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnCidadeExcluir.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnCidadeExcluir.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCidadeExcluir.Image = global::SWS.Properties.Resources.close;
            this.btnCidadeExcluir.Location = new System.Drawing.Point(457, 123);
            this.btnCidadeExcluir.Name = "btnCidadeExcluir";
            this.btnCidadeExcluir.Size = new System.Drawing.Size(34, 21);
            this.btnCidadeExcluir.TabIndex = 87;
            this.btnCidadeExcluir.TabStop = false;
            this.btnCidadeExcluir.UseVisualStyleBackColor = true;
            this.btnCidadeExcluir.Click += new System.EventHandler(this.btnCidadeExcluir_Click);
            // 
            // lblCep
            // 
            this.lblCep.BackColor = System.Drawing.Color.LightGray;
            this.lblCep.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblCep.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCep.Location = new System.Drawing.Point(3, 143);
            this.lblCep.Name = "lblCep";
            this.lblCep.ReadOnly = true;
            this.lblCep.Size = new System.Drawing.Size(147, 21);
            this.lblCep.TabIndex = 88;
            this.lblCep.TabStop = false;
            this.lblCep.Text = "CEP";
            // 
            // textCEPObra
            // 
            this.textCEPObra.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.textCEPObra.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textCEPObra.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textCEPObra.Location = new System.Drawing.Point(149, 143);
            this.textCEPObra.Mask = "99,999-999";
            this.textCEPObra.Name = "textCEPObra";
            this.textCEPObra.PromptChar = ' ';
            this.textCEPObra.Size = new System.Drawing.Size(342, 21);
            this.textCEPObra.TabIndex = 8;
            // 
            // lblDescritivo
            // 
            this.lblDescritivo.BackColor = System.Drawing.Color.LightGray;
            this.lblDescritivo.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblDescritivo.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDescritivo.Location = new System.Drawing.Point(3, 163);
            this.lblDescritivo.Name = "lblDescritivo";
            this.lblDescritivo.ReadOnly = true;
            this.lblDescritivo.Size = new System.Drawing.Size(147, 21);
            this.lblDescritivo.TabIndex = 90;
            this.lblDescritivo.TabStop = false;
            this.lblDescritivo.Text = "Descritivo";
            // 
            // textDescritivoObra
            // 
            this.textDescritivoObra.BackColor = System.Drawing.Color.White;
            this.textDescritivoObra.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textDescritivoObra.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.textDescritivoObra.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textDescritivoObra.Location = new System.Drawing.Point(149, 163);
            this.textDescritivoObra.Multiline = true;
            this.textDescritivoObra.Name = "textDescritivoObra";
            this.textDescritivoObra.Size = new System.Drawing.Size(342, 79);
            this.textDescritivoObra.TabIndex = 9;
            // 
            // lblObrigatorioLocal
            // 
            this.lblObrigatorioLocal.AutoSize = true;
            this.lblObrigatorioLocal.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblObrigatorioLocal.ForeColor = System.Drawing.Color.Red;
            this.lblObrigatorioLocal.Location = new System.Drawing.Point(496, 4);
            this.lblObrigatorioLocal.Name = "lblObrigatorioLocal";
            this.lblObrigatorioLocal.Size = new System.Drawing.Size(15, 20);
            this.lblObrigatorioLocal.TabIndex = 91;
            this.lblObrigatorioLocal.Text = "*";
            // 
            // lblObrigatorioEndereco
            // 
            this.lblObrigatorioEndereco.AutoSize = true;
            this.lblObrigatorioEndereco.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblObrigatorioEndereco.ForeColor = System.Drawing.Color.Red;
            this.lblObrigatorioEndereco.Location = new System.Drawing.Point(496, 22);
            this.lblObrigatorioEndereco.Name = "lblObrigatorioEndereco";
            this.lblObrigatorioEndereco.Size = new System.Drawing.Size(15, 20);
            this.lblObrigatorioEndereco.TabIndex = 92;
            this.lblObrigatorioEndereco.Text = "*";
            // 
            // lblObrigatorioNumero
            // 
            this.lblObrigatorioNumero.AutoSize = true;
            this.lblObrigatorioNumero.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblObrigatorioNumero.ForeColor = System.Drawing.Color.Red;
            this.lblObrigatorioNumero.Location = new System.Drawing.Point(496, 42);
            this.lblObrigatorioNumero.Name = "lblObrigatorioNumero";
            this.lblObrigatorioNumero.Size = new System.Drawing.Size(15, 20);
            this.lblObrigatorioNumero.TabIndex = 93;
            this.lblObrigatorioNumero.Text = "*";
            // 
            // lblObrigatorioBairro
            // 
            this.lblObrigatorioBairro.AutoSize = true;
            this.lblObrigatorioBairro.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblObrigatorioBairro.ForeColor = System.Drawing.Color.Red;
            this.lblObrigatorioBairro.Location = new System.Drawing.Point(497, 82);
            this.lblObrigatorioBairro.Name = "lblObrigatorioBairro";
            this.lblObrigatorioBairro.Size = new System.Drawing.Size(15, 20);
            this.lblObrigatorioBairro.TabIndex = 94;
            this.lblObrigatorioBairro.Text = "*";
            // 
            // lblObrigatorioUnidadeFederativa
            // 
            this.lblObrigatorioUnidadeFederativa.AutoSize = true;
            this.lblObrigatorioUnidadeFederativa.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblObrigatorioUnidadeFederativa.ForeColor = System.Drawing.Color.Red;
            this.lblObrigatorioUnidadeFederativa.Location = new System.Drawing.Point(497, 104);
            this.lblObrigatorioUnidadeFederativa.Name = "lblObrigatorioUnidadeFederativa";
            this.lblObrigatorioUnidadeFederativa.Size = new System.Drawing.Size(15, 20);
            this.lblObrigatorioUnidadeFederativa.TabIndex = 95;
            this.lblObrigatorioUnidadeFederativa.Text = "*";
            // 
            // lblObrigatorioCidade
            // 
            this.lblObrigatorioCidade.AutoSize = true;
            this.lblObrigatorioCidade.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblObrigatorioCidade.ForeColor = System.Drawing.Color.Red;
            this.lblObrigatorioCidade.Location = new System.Drawing.Point(496, 124);
            this.lblObrigatorioCidade.Name = "lblObrigatorioCidade";
            this.lblObrigatorioCidade.Size = new System.Drawing.Size(15, 20);
            this.lblObrigatorioCidade.TabIndex = 96;
            this.lblObrigatorioCidade.Text = "*";
            // 
            // lblObrigatorioCEP
            // 
            this.lblObrigatorioCEP.AutoSize = true;
            this.lblObrigatorioCEP.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblObrigatorioCEP.ForeColor = System.Drawing.Color.Red;
            this.lblObrigatorioCEP.Location = new System.Drawing.Point(496, 142);
            this.lblObrigatorioCEP.Name = "lblObrigatorioCEP";
            this.lblObrigatorioCEP.Size = new System.Drawing.Size(15, 20);
            this.lblObrigatorioCEP.TabIndex = 97;
            this.lblObrigatorioCEP.Text = "*";
            // 
            // lblExplicacaoObrigatorio
            // 
            this.lblExplicacaoObrigatorio.AutoSize = true;
            this.lblExplicacaoObrigatorio.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblExplicacaoObrigatorio.ForeColor = System.Drawing.Color.Blue;
            this.lblExplicacaoObrigatorio.Location = new System.Drawing.Point(3, 247);
            this.lblExplicacaoObrigatorio.Name = "lblExplicacaoObrigatorio";
            this.lblExplicacaoObrigatorio.Size = new System.Drawing.Size(324, 30);
            this.lblExplicacaoObrigatorio.TabIndex = 98;
            this.lblExplicacaoObrigatorio.Text = "Campos com * são obrigatórios e não será possível gravar\r\no local da obra sem ele" +
    "s.";
            // 
            // frmPcmsoObraIncluir
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(534, 361);
            this.Name = "frmPcmsoObraIncluir";
            this.Text = "INCLUIR INFORMAÇÕES DO LOCAL DO ESTUDO / OBRA";
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.frmPcmsoObraIncluir_KeyDown);
            this.scForm.Panel1.ResumeLayout(false);
            this.scForm.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.scForm)).EndInit();
            this.scForm.ResumeLayout(false);
            this.pnlForm.ResumeLayout(false);
            this.pnlForm.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.banner)).EndInit();
            this.flpAcao.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        protected System.Windows.Forms.FlowLayoutPanel flpAcao;
        protected System.Windows.Forms.TextBox lblApelidoObra;
        protected System.Windows.Forms.TextBox textBairroObra;
        protected System.Windows.Forms.TextBox lblBairro;
        protected System.Windows.Forms.TextBox textComplementoObra;
        protected System.Windows.Forms.TextBox lblComplemento;
        protected System.Windows.Forms.TextBox textNumeroObra;
        protected System.Windows.Forms.TextBox lblNumero;
        protected System.Windows.Forms.TextBox textEnderecoObra;
        protected System.Windows.Forms.TextBox lblEndereco;
        protected System.Windows.Forms.TextBox textObra;
        protected System.Windows.Forms.TextBox lblUnidadeFederativa;
        protected ComboBoxWithBorder cbUnidadeFederativa;
        protected System.Windows.Forms.TextBox lblCidade;
        protected System.Windows.Forms.Button btnCidadeIncluir;
        protected System.Windows.Forms.TextBox textCidadeObra;
        protected System.Windows.Forms.Button btnCidadeExcluir;
        protected System.Windows.Forms.TextBox lblCep;
        protected System.Windows.Forms.MaskedTextBox textCEPObra;
        protected System.Windows.Forms.TextBox lblDescritivo;
        protected System.Windows.Forms.TextBox textDescritivoObra;
        protected System.Windows.Forms.Button btnObraGravar;
        protected System.Windows.Forms.Button btnFechar;
        protected System.Windows.Forms.Label lblExplicacaoObrigatorio;
        protected System.Windows.Forms.Label lblObrigatorioCEP;
        protected System.Windows.Forms.Label lblObrigatorioCidade;
        protected System.Windows.Forms.Label lblObrigatorioUnidadeFederativa;
        protected System.Windows.Forms.Label lblObrigatorioBairro;
        protected System.Windows.Forms.Label lblObrigatorioNumero;
        protected System.Windows.Forms.Label lblObrigatorioEndereco;
        protected System.Windows.Forms.Label lblObrigatorioLocal;
    }
}