﻿namespace SWS.View
{
    partial class frmAtendimentoPrincipal
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            this.flpAcao = new System.Windows.Forms.FlowLayoutPanel();
            this.btPesquisa = new System.Windows.Forms.Button();
            this.btIncluir = new System.Windows.Forms.Button();
            this.btAlterar = new System.Windows.Forms.Button();
            this.btFinalizar = new System.Windows.Forms.Button();
            this.btCancelar = new System.Windows.Forms.Button();
            this.btImprimir = new System.Windows.Forms.Button();
            this.btAnexo = new System.Windows.Forms.Button();
            this.bt2Via = new System.Windows.Forms.Button();
            this.btTermica = new System.Windows.Forms.Button();
            this.btLaudar = new System.Windows.Forms.Button();
            this.btProntuario = new System.Windows.Forms.Button();
            this.btnDesbloquear = new System.Windows.Forms.Button();
            this.btFechar = new System.Windows.Forms.Button();
            this.lblCodigo = new System.Windows.Forms.TextBox();
            this.lblCPF = new System.Windows.Forms.TextBox();
            this.lblRG = new System.Windows.Forms.TextBox();
            this.lblSituacao = new System.Windows.Forms.TextBox();
            this.lblDataEmissao = new System.Windows.Forms.TextBox();
            this.lblDataGravacao = new System.Windows.Forms.TextBox();
            this.lblCliente = new System.Windows.Forms.TextBox();
            this.lblFuncao = new System.Windows.Forms.TextBox();
            this.lblTipo = new System.Windows.Forms.TextBox();
            this.textCodigo = new System.Windows.Forms.MaskedTextBox();
            this.textCpf = new System.Windows.Forms.MaskedTextBox();
            this.textRg = new System.Windows.Forms.TextBox();
            this.cbSituacao = new SWS.ComboBoxWithBorder();
            this.dataGravacaoInicial = new System.Windows.Forms.DateTimePicker();
            this.dataGravacaoFinal = new System.Windows.Forms.DateTimePicker();
            this.dataEmissaoInicial = new System.Windows.Forms.DateTimePicker();
            this.dataEmissaoFinal = new System.Windows.Forms.DateTimePicker();
            this.lblColaborador = new System.Windows.Forms.TextBox();
            this.textNome = new System.Windows.Forms.TextBox();
            this.btCliente = new System.Windows.Forms.Button();
            this.textCliente = new System.Windows.Forms.TextBox();
            this.btFuncao = new System.Windows.Forms.Button();
            this.textFuncao = new System.Windows.Forms.TextBox();
            this.cbTipo = new SWS.ComboBoxWithBorder();
            this.grbAtendimento = new System.Windows.Forms.GroupBox();
            this.dgvAtendimento = new System.Windows.Forms.DataGridView();
            this.lblTextoCancelado = new System.Windows.Forms.Label();
            this.lblLegendaInapto = new System.Windows.Forms.Label();
            this.textTotal = new System.Windows.Forms.TextBox();
            this.lblExamePendente = new System.Windows.Forms.Label();
            this.lblTotalTitulo = new System.Windows.Forms.Label();
            this.lblAsoCancelado = new System.Windows.Forms.Label();
            this.lblLegendaCancelado = new System.Windows.Forms.Label();
            this.grbLegenda = new System.Windows.Forms.GroupBox();
            this.lblBloqueado = new System.Windows.Forms.Label();
            this.lblLegendaBloqueado = new System.Windows.Forms.Label();
            this.lblTextPrioridade = new System.Windows.Forms.Label();
            this.lblLegendaPrioridade = new System.Windows.Forms.Label();
            this.lblTextConstrucao = new System.Windows.Forms.Label();
            this.lblLegendaConstrucao = new System.Windows.Forms.Label();
            this.lblAtendimentoFinalizado = new System.Windows.Forms.Label();
            this.lblLegendaFinalizado = new System.Windows.Forms.Label();
            this.grbTotal = new System.Windows.Forms.GroupBox();
            this.btnExcluirCliente = new System.Windows.Forms.Button();
            this.btnExcluirFuncao = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.spForm)).BeginInit();
            this.spForm.Panel1.SuspendLayout();
            this.spForm.Panel2.SuspendLayout();
            this.spForm.SuspendLayout();
            this.pnlForm.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.banner)).BeginInit();
            this.flpAcao.SuspendLayout();
            this.grbAtendimento.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvAtendimento)).BeginInit();
            this.grbLegenda.SuspendLayout();
            this.grbTotal.SuspendLayout();
            this.SuspendLayout();
            // 
            // spForm
            // 
            this.spForm.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            // 
            // spForm.Panel1
            // 
            this.spForm.Panel1.Controls.Add(this.flpAcao);
            this.spForm.SplitterDistance = 60;
            // 
            // pnlForm
            // 
            this.pnlForm.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.pnlForm.Controls.Add(this.btnExcluirFuncao);
            this.pnlForm.Controls.Add(this.btnExcluirCliente);
            this.pnlForm.Controls.Add(this.cbSituacao);
            this.pnlForm.Controls.Add(this.lblDataGravacao);
            this.pnlForm.Controls.Add(this.lblDataEmissao);
            this.pnlForm.Controls.Add(this.textCodigo);
            this.pnlForm.Controls.Add(this.grbTotal);
            this.pnlForm.Controls.Add(this.cbTipo);
            this.pnlForm.Controls.Add(this.grbLegenda);
            this.pnlForm.Controls.Add(this.lblExamePendente);
            this.pnlForm.Controls.Add(this.grbAtendimento);
            this.pnlForm.Controls.Add(this.btFuncao);
            this.pnlForm.Controls.Add(this.textFuncao);
            this.pnlForm.Controls.Add(this.btCliente);
            this.pnlForm.Controls.Add(this.textCliente);
            this.pnlForm.Controls.Add(this.textNome);
            this.pnlForm.Controls.Add(this.lblColaborador);
            this.pnlForm.Controls.Add(this.dataEmissaoInicial);
            this.pnlForm.Controls.Add(this.dataEmissaoFinal);
            this.pnlForm.Controls.Add(this.dataGravacaoFinal);
            this.pnlForm.Controls.Add(this.dataGravacaoInicial);
            this.pnlForm.Controls.Add(this.textRg);
            this.pnlForm.Controls.Add(this.textCpf);
            this.pnlForm.Controls.Add(this.lblTipo);
            this.pnlForm.Controls.Add(this.lblFuncao);
            this.pnlForm.Controls.Add(this.lblCliente);
            this.pnlForm.Controls.Add(this.lblSituacao);
            this.pnlForm.Controls.Add(this.lblRG);
            this.pnlForm.Controls.Add(this.lblCPF);
            this.pnlForm.Controls.Add(this.lblCodigo);
            this.pnlForm.Size = new System.Drawing.Size(781, 439);
            // 
            // banner
            // 
            this.banner.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            // 
            // flpAcao
            // 
            this.flpAcao.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.flpAcao.Controls.Add(this.btPesquisa);
            this.flpAcao.Controls.Add(this.btIncluir);
            this.flpAcao.Controls.Add(this.btAlterar);
            this.flpAcao.Controls.Add(this.btFinalizar);
            this.flpAcao.Controls.Add(this.btCancelar);
            this.flpAcao.Controls.Add(this.btImprimir);
            this.flpAcao.Controls.Add(this.btAnexo);
            this.flpAcao.Controls.Add(this.bt2Via);
            this.flpAcao.Controls.Add(this.btTermica);
            this.flpAcao.Controls.Add(this.btLaudar);
            this.flpAcao.Controls.Add(this.btProntuario);
            this.flpAcao.Controls.Add(this.btnDesbloquear);
            this.flpAcao.Controls.Add(this.btFechar);
            this.flpAcao.Location = new System.Drawing.Point(0, 0);
            this.flpAcao.Name = "flpAcao";
            this.flpAcao.Size = new System.Drawing.Size(784, 60);
            this.flpAcao.TabIndex = 0;
            // 
            // btPesquisa
            // 
            this.btPesquisa.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btPesquisa.Image = global::SWS.Properties.Resources.lupa;
            this.btPesquisa.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btPesquisa.Location = new System.Drawing.Point(3, 3);
            this.btPesquisa.Name = "btPesquisa";
            this.btPesquisa.Size = new System.Drawing.Size(75, 23);
            this.btPesquisa.TabIndex = 76;
            this.btPesquisa.TabStop = false;
            this.btPesquisa.Text = "&Pesquisar";
            this.btPesquisa.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btPesquisa.UseVisualStyleBackColor = true;
            this.btPesquisa.Click += new System.EventHandler(this.btn_pesquisar_Click);
            // 
            // btIncluir
            // 
            this.btIncluir.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btIncluir.Image = global::SWS.Properties.Resources.icone_mais;
            this.btIncluir.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btIncluir.Location = new System.Drawing.Point(84, 3);
            this.btIncluir.Name = "btIncluir";
            this.btIncluir.Size = new System.Drawing.Size(75, 23);
            this.btIncluir.TabIndex = 27;
            this.btIncluir.TabStop = false;
            this.btIncluir.Text = "&Incluir";
            this.btIncluir.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btIncluir.UseVisualStyleBackColor = true;
            this.btIncluir.Click += new System.EventHandler(this.btn_incluir_Click);
            // 
            // btAlterar
            // 
            this.btAlterar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btAlterar.Image = global::SWS.Properties.Resources.Alterar;
            this.btAlterar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btAlterar.Location = new System.Drawing.Point(165, 3);
            this.btAlterar.Name = "btAlterar";
            this.btAlterar.Size = new System.Drawing.Size(75, 23);
            this.btAlterar.TabIndex = 28;
            this.btAlterar.TabStop = false;
            this.btAlterar.Text = "&Alterar";
            this.btAlterar.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btAlterar.UseVisualStyleBackColor = true;
            this.btAlterar.Click += new System.EventHandler(this.btn_alterar_Click);
            // 
            // btFinalizar
            // 
            this.btFinalizar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btFinalizar.Image = global::SWS.Properties.Resources.fechar_ico;
            this.btFinalizar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btFinalizar.Location = new System.Drawing.Point(246, 3);
            this.btFinalizar.Name = "btFinalizar";
            this.btFinalizar.Size = new System.Drawing.Size(75, 23);
            this.btFinalizar.TabIndex = 30;
            this.btFinalizar.TabStop = false;
            this.btFinalizar.Text = "&Finalizar";
            this.btFinalizar.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btFinalizar.UseVisualStyleBackColor = true;
            this.btFinalizar.Click += new System.EventHandler(this.btn_finalizar_Click);
            // 
            // btCancelar
            // 
            this.btCancelar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btCancelar.Image = global::SWS.Properties.Resources.Icone_cancelar;
            this.btCancelar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btCancelar.Location = new System.Drawing.Point(327, 3);
            this.btCancelar.Name = "btCancelar";
            this.btCancelar.Size = new System.Drawing.Size(75, 23);
            this.btCancelar.TabIndex = 29;
            this.btCancelar.TabStop = false;
            this.btCancelar.Text = "&Cancelar";
            this.btCancelar.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btCancelar.UseVisualStyleBackColor = true;
            this.btCancelar.Click += new System.EventHandler(this.btn_cancelado_Click);
            // 
            // btImprimir
            // 
            this.btImprimir.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btImprimir.Image = global::SWS.Properties.Resources.impressora1;
            this.btImprimir.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btImprimir.Location = new System.Drawing.Point(408, 3);
            this.btImprimir.Name = "btImprimir";
            this.btImprimir.Size = new System.Drawing.Size(75, 23);
            this.btImprimir.TabIndex = 31;
            this.btImprimir.TabStop = false;
            this.btImprimir.Text = "I&mprimir";
            this.btImprimir.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btImprimir.UseVisualStyleBackColor = true;
            this.btImprimir.Click += new System.EventHandler(this.btn_imprimir_Click);
            // 
            // btAnexo
            // 
            this.btAnexo.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btAnexo.Image = global::SWS.Properties.Resources.clip;
            this.btAnexo.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btAnexo.Location = new System.Drawing.Point(489, 3);
            this.btAnexo.Name = "btAnexo";
            this.btAnexo.Size = new System.Drawing.Size(75, 23);
            this.btAnexo.TabIndex = 38;
            this.btAnexo.TabStop = false;
            this.btAnexo.Text = "&Anexos";
            this.btAnexo.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btAnexo.UseVisualStyleBackColor = true;
            this.btAnexo.Click += new System.EventHandler(this.btn_anexos_Click);
            // 
            // bt2Via
            // 
            this.bt2Via.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.bt2Via.Image = global::SWS.Properties.Resources.impressora1;
            this.bt2Via.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.bt2Via.Location = new System.Drawing.Point(570, 3);
            this.bt2Via.Name = "bt2Via";
            this.bt2Via.Size = new System.Drawing.Size(75, 23);
            this.bt2Via.TabIndex = 37;
            this.bt2Via.TabStop = false;
            this.bt2Via.Text = "2ª Via";
            this.bt2Via.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.bt2Via.UseVisualStyleBackColor = true;
            this.bt2Via.Click += new System.EventHandler(this.btn_2via_Click);
            // 
            // btTermica
            // 
            this.btTermica.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btTermica.Image = global::SWS.Properties.Resources.impressora1;
            this.btTermica.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btTermica.Location = new System.Drawing.Point(651, 3);
            this.btTermica.Name = "btTermica";
            this.btTermica.Size = new System.Drawing.Size(75, 23);
            this.btTermica.TabIndex = 36;
            this.btTermica.TabStop = false;
            this.btTermica.Text = "Térmica";
            this.btTermica.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btTermica.UseVisualStyleBackColor = true;
            this.btTermica.Click += new System.EventHandler(this.btn_imprimir_termica_Click);
            // 
            // btLaudar
            // 
            this.btLaudar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btLaudar.Image = global::SWS.Properties.Resources.prontuario;
            this.btLaudar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btLaudar.Location = new System.Drawing.Point(3, 32);
            this.btLaudar.Name = "btLaudar";
            this.btLaudar.Size = new System.Drawing.Size(75, 23);
            this.btLaudar.TabIndex = 35;
            this.btLaudar.TabStop = false;
            this.btLaudar.Text = "&Laudar";
            this.btLaudar.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btLaudar.UseVisualStyleBackColor = true;
            this.btLaudar.Click += new System.EventHandler(this.btn_laudar_Click);
            // 
            // btProntuario
            // 
            this.btProntuario.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btProntuario.Image = global::SWS.Properties.Resources.icones_2;
            this.btProntuario.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btProntuario.Location = new System.Drawing.Point(84, 32);
            this.btProntuario.Name = "btProntuario";
            this.btProntuario.Size = new System.Drawing.Size(75, 23);
            this.btProntuario.TabIndex = 34;
            this.btProntuario.TabStop = false;
            this.btProntuario.Text = "&Prontuário";
            this.btProntuario.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btProntuario.UseVisualStyleBackColor = true;
            this.btProntuario.Click += new System.EventHandler(this.btn_prontuarioDigital_Click);
            // 
            // btnDesbloquear
            // 
            this.btnDesbloquear.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnDesbloquear.Image = global::SWS.Properties.Resources.icone_desbloquear;
            this.btnDesbloquear.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnDesbloquear.Location = new System.Drawing.Point(165, 32);
            this.btnDesbloquear.Name = "btnDesbloquear";
            this.btnDesbloquear.Size = new System.Drawing.Size(75, 23);
            this.btnDesbloquear.TabIndex = 77;
            this.btnDesbloquear.TabStop = false;
            this.btnDesbloquear.Text = "&Desbloq.";
            this.btnDesbloquear.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnDesbloquear.UseVisualStyleBackColor = true;
            this.btnDesbloquear.Click += new System.EventHandler(this.btnDesbloquear_Click);
            // 
            // btFechar
            // 
            this.btFechar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btFechar.Image = global::SWS.Properties.Resources.close;
            this.btFechar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btFechar.Location = new System.Drawing.Point(246, 32);
            this.btFechar.Name = "btFechar";
            this.btFechar.Size = new System.Drawing.Size(75, 23);
            this.btFechar.TabIndex = 33;
            this.btFechar.TabStop = false;
            this.btFechar.Text = "&Fechar";
            this.btFechar.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btFechar.UseVisualStyleBackColor = true;
            this.btFechar.Click += new System.EventHandler(this.btn_fechar_Click);
            // 
            // lblCodigo
            // 
            this.lblCodigo.BackColor = System.Drawing.Color.LightGray;
            this.lblCodigo.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblCodigo.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCodigo.Location = new System.Drawing.Point(3, 3);
            this.lblCodigo.Name = "lblCodigo";
            this.lblCodigo.ReadOnly = true;
            this.lblCodigo.Size = new System.Drawing.Size(138, 21);
            this.lblCodigo.TabIndex = 0;
            this.lblCodigo.TabStop = false;
            this.lblCodigo.Text = "Código de atendimento";
            // 
            // lblCPF
            // 
            this.lblCPF.BackColor = System.Drawing.Color.LightGray;
            this.lblCPF.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblCPF.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCPF.Location = new System.Drawing.Point(423, 23);
            this.lblCPF.Name = "lblCPF";
            this.lblCPF.ReadOnly = true;
            this.lblCPF.Size = new System.Drawing.Size(138, 21);
            this.lblCPF.TabIndex = 1;
            this.lblCPF.TabStop = false;
            this.lblCPF.Text = "CPF";
            // 
            // lblRG
            // 
            this.lblRG.BackColor = System.Drawing.Color.LightGray;
            this.lblRG.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblRG.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblRG.Location = new System.Drawing.Point(423, 3);
            this.lblRG.Name = "lblRG";
            this.lblRG.ReadOnly = true;
            this.lblRG.Size = new System.Drawing.Size(138, 21);
            this.lblRG.TabIndex = 2;
            this.lblRG.TabStop = false;
            this.lblRG.Text = "RG";
            // 
            // lblSituacao
            // 
            this.lblSituacao.BackColor = System.Drawing.Color.LightGray;
            this.lblSituacao.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblSituacao.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSituacao.Location = new System.Drawing.Point(3, 63);
            this.lblSituacao.Name = "lblSituacao";
            this.lblSituacao.ReadOnly = true;
            this.lblSituacao.Size = new System.Drawing.Size(138, 21);
            this.lblSituacao.TabIndex = 3;
            this.lblSituacao.TabStop = false;
            this.lblSituacao.Text = "Situação";
            // 
            // lblDataEmissao
            // 
            this.lblDataEmissao.BackColor = System.Drawing.Color.LightGray;
            this.lblDataEmissao.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblDataEmissao.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDataEmissao.Location = new System.Drawing.Point(3, 23);
            this.lblDataEmissao.Name = "lblDataEmissao";
            this.lblDataEmissao.ReadOnly = true;
            this.lblDataEmissao.Size = new System.Drawing.Size(138, 21);
            this.lblDataEmissao.TabIndex = 4;
            this.lblDataEmissao.TabStop = false;
            this.lblDataEmissao.Text = "Data de emissão";
            // 
            // lblDataGravacao
            // 
            this.lblDataGravacao.BackColor = System.Drawing.Color.LightGray;
            this.lblDataGravacao.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblDataGravacao.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDataGravacao.Location = new System.Drawing.Point(3, 43);
            this.lblDataGravacao.Name = "lblDataGravacao";
            this.lblDataGravacao.ReadOnly = true;
            this.lblDataGravacao.Size = new System.Drawing.Size(138, 21);
            this.lblDataGravacao.TabIndex = 5;
            this.lblDataGravacao.TabStop = false;
            this.lblDataGravacao.Text = "Data de gravação";
            // 
            // lblCliente
            // 
            this.lblCliente.BackColor = System.Drawing.Color.LightGray;
            this.lblCliente.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblCliente.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCliente.Location = new System.Drawing.Point(3, 83);
            this.lblCliente.Name = "lblCliente";
            this.lblCliente.ReadOnly = true;
            this.lblCliente.Size = new System.Drawing.Size(138, 21);
            this.lblCliente.TabIndex = 6;
            this.lblCliente.TabStop = false;
            this.lblCliente.Text = "Cliente";
            // 
            // lblFuncao
            // 
            this.lblFuncao.BackColor = System.Drawing.Color.LightGray;
            this.lblFuncao.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblFuncao.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblFuncao.Location = new System.Drawing.Point(3, 103);
            this.lblFuncao.Name = "lblFuncao";
            this.lblFuncao.ReadOnly = true;
            this.lblFuncao.Size = new System.Drawing.Size(138, 21);
            this.lblFuncao.TabIndex = 7;
            this.lblFuncao.TabStop = false;
            this.lblFuncao.Text = "Função";
            // 
            // lblTipo
            // 
            this.lblTipo.BackColor = System.Drawing.Color.LightGray;
            this.lblTipo.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblTipo.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTipo.Location = new System.Drawing.Point(423, 43);
            this.lblTipo.Name = "lblTipo";
            this.lblTipo.ReadOnly = true;
            this.lblTipo.Size = new System.Drawing.Size(138, 21);
            this.lblTipo.TabIndex = 8;
            this.lblTipo.TabStop = false;
            this.lblTipo.Text = "Tipo";
            // 
            // textCodigo
            // 
            this.textCodigo.BackColor = System.Drawing.Color.White;
            this.textCodigo.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textCodigo.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textCodigo.Location = new System.Drawing.Point(140, 3);
            this.textCodigo.Mask = "9999,99,999999";
            this.textCodigo.Name = "textCodigo";
            this.textCodigo.PromptChar = ' ';
            this.textCodigo.Size = new System.Drawing.Size(213, 21);
            this.textCodigo.TabIndex = 1;
            // 
            // textCpf
            // 
            this.textCpf.BackColor = System.Drawing.Color.White;
            this.textCpf.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textCpf.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textCpf.Location = new System.Drawing.Point(560, 23);
            this.textCpf.Mask = "999,999,999-99";
            this.textCpf.Name = "textCpf";
            this.textCpf.PromptChar = ' ';
            this.textCpf.Size = new System.Drawing.Size(197, 21);
            this.textCpf.TabIndex = 5;
            // 
            // textRg
            // 
            this.textRg.BackColor = System.Drawing.Color.White;
            this.textRg.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textRg.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.textRg.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textRg.Location = new System.Drawing.Point(560, 3);
            this.textRg.MaxLength = 15;
            this.textRg.Name = "textRg";
            this.textRg.Size = new System.Drawing.Size(197, 21);
            this.textRg.TabIndex = 2;
            // 
            // cbSituacao
            // 
            this.cbSituacao.BackColor = System.Drawing.Color.LightGray;
            this.cbSituacao.BorderColor = System.Drawing.Color.DimGray;
            this.cbSituacao.BorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid;
            this.cbSituacao.FormattingEnabled = true;
            this.cbSituacao.Location = new System.Drawing.Point(140, 63);
            this.cbSituacao.Name = "cbSituacao";
            this.cbSituacao.Size = new System.Drawing.Size(213, 21);
            this.cbSituacao.TabIndex = 9;
            // 
            // dataGravacaoInicial
            // 
            this.dataGravacaoInicial.Checked = false;
            this.dataGravacaoInicial.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dataGravacaoInicial.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dataGravacaoInicial.Location = new System.Drawing.Point(140, 43);
            this.dataGravacaoInicial.Name = "dataGravacaoInicial";
            this.dataGravacaoInicial.ShowCheckBox = true;
            this.dataGravacaoInicial.Size = new System.Drawing.Size(107, 21);
            this.dataGravacaoInicial.TabIndex = 3;
            // 
            // dataGravacaoFinal
            // 
            this.dataGravacaoFinal.Checked = false;
            this.dataGravacaoFinal.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dataGravacaoFinal.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dataGravacaoFinal.Location = new System.Drawing.Point(246, 43);
            this.dataGravacaoFinal.Name = "dataGravacaoFinal";
            this.dataGravacaoFinal.ShowCheckBox = true;
            this.dataGravacaoFinal.Size = new System.Drawing.Size(107, 21);
            this.dataGravacaoFinal.TabIndex = 4;
            // 
            // dataEmissaoInicial
            // 
            this.dataEmissaoInicial.Checked = false;
            this.dataEmissaoInicial.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dataEmissaoInicial.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dataEmissaoInicial.Location = new System.Drawing.Point(140, 23);
            this.dataEmissaoInicial.Name = "dataEmissaoInicial";
            this.dataEmissaoInicial.ShowCheckBox = true;
            this.dataEmissaoInicial.Size = new System.Drawing.Size(107, 21);
            this.dataEmissaoInicial.TabIndex = 6;
            // 
            // dataEmissaoFinal
            // 
            this.dataEmissaoFinal.Checked = false;
            this.dataEmissaoFinal.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dataEmissaoFinal.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dataEmissaoFinal.Location = new System.Drawing.Point(246, 23);
            this.dataEmissaoFinal.Name = "dataEmissaoFinal";
            this.dataEmissaoFinal.ShowCheckBox = true;
            this.dataEmissaoFinal.Size = new System.Drawing.Size(107, 21);
            this.dataEmissaoFinal.TabIndex = 7;
            // 
            // lblColaborador
            // 
            this.lblColaborador.BackColor = System.Drawing.Color.LightGray;
            this.lblColaborador.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblColaborador.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblColaborador.Location = new System.Drawing.Point(423, 63);
            this.lblColaborador.Name = "lblColaborador";
            this.lblColaborador.ReadOnly = true;
            this.lblColaborador.Size = new System.Drawing.Size(138, 21);
            this.lblColaborador.TabIndex = 19;
            this.lblColaborador.TabStop = false;
            this.lblColaborador.Text = "Nome do colaborador";
            // 
            // textNome
            // 
            this.textNome.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textNome.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.textNome.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textNome.Location = new System.Drawing.Point(560, 63);
            this.textNome.MaxLength = 100;
            this.textNome.Name = "textNome";
            this.textNome.Size = new System.Drawing.Size(197, 21);
            this.textNome.TabIndex = 10;
            // 
            // btCliente
            // 
            this.btCliente.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btCliente.Image = global::SWS.Properties.Resources.busca;
            this.btCliente.Location = new System.Drawing.Point(352, 83);
            this.btCliente.Name = "btCliente";
            this.btCliente.Size = new System.Drawing.Size(30, 21);
            this.btCliente.TabIndex = 11;
            this.btCliente.UseVisualStyleBackColor = true;
            this.btCliente.Click += new System.EventHandler(this.btn_cliente_Click);
            // 
            // textCliente
            // 
            this.textCliente.BackColor = System.Drawing.SystemColors.Control;
            this.textCliente.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textCliente.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textCliente.Location = new System.Drawing.Point(140, 83);
            this.textCliente.MaxLength = 100;
            this.textCliente.Name = "textCliente";
            this.textCliente.ReadOnly = true;
            this.textCliente.Size = new System.Drawing.Size(213, 21);
            this.textCliente.TabIndex = 21;
            this.textCliente.TabStop = false;
            // 
            // btFuncao
            // 
            this.btFuncao.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btFuncao.Image = global::SWS.Properties.Resources.busca;
            this.btFuncao.Location = new System.Drawing.Point(352, 103);
            this.btFuncao.Name = "btFuncao";
            this.btFuncao.Size = new System.Drawing.Size(30, 21);
            this.btFuncao.TabIndex = 12;
            this.btFuncao.UseVisualStyleBackColor = true;
            this.btFuncao.Click += new System.EventHandler(this.btn_funcao_Click);
            // 
            // textFuncao
            // 
            this.textFuncao.BackColor = System.Drawing.SystemColors.Control;
            this.textFuncao.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textFuncao.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textFuncao.Location = new System.Drawing.Point(140, 103);
            this.textFuncao.MaxLength = 100;
            this.textFuncao.Name = "textFuncao";
            this.textFuncao.ReadOnly = true;
            this.textFuncao.Size = new System.Drawing.Size(213, 21);
            this.textFuncao.TabIndex = 23;
            this.textFuncao.TabStop = false;
            // 
            // cbTipo
            // 
            this.cbTipo.BackColor = System.Drawing.Color.LightGray;
            this.cbTipo.BorderColor = System.Drawing.Color.DimGray;
            this.cbTipo.BorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid;
            this.cbTipo.FormattingEnabled = true;
            this.cbTipo.Location = new System.Drawing.Point(560, 43);
            this.cbTipo.Name = "cbTipo";
            this.cbTipo.Size = new System.Drawing.Size(197, 21);
            this.cbTipo.TabIndex = 8;
            // 
            // grbAtendimento
            // 
            this.grbAtendimento.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.grbAtendimento.Controls.Add(this.dgvAtendimento);
            this.grbAtendimento.Location = new System.Drawing.Point(3, 130);
            this.grbAtendimento.Name = "grbAtendimento";
            this.grbAtendimento.Size = new System.Drawing.Size(775, 220);
            this.grbAtendimento.TabIndex = 26;
            this.grbAtendimento.TabStop = false;
            this.grbAtendimento.Text = "Atendimentos";
            // 
            // dgvAtendimento
            // 
            this.dgvAtendimento.AllowUserToAddRows = false;
            this.dgvAtendimento.AllowUserToDeleteRows = false;
            this.dgvAtendimento.AllowUserToOrderColumns = true;
            this.dgvAtendimento.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this.dgvAtendimento.BackgroundColor = System.Drawing.Color.White;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Arial", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvAtendimento.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.dgvAtendimento.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.Color.LightGray;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvAtendimento.DefaultCellStyle = dataGridViewCellStyle2;
            this.dgvAtendimento.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvAtendimento.Location = new System.Drawing.Point(3, 16);
            this.dgvAtendimento.MultiSelect = false;
            this.dgvAtendimento.Name = "dgvAtendimento";
            this.dgvAtendimento.ReadOnly = true;
            this.dgvAtendimento.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvAtendimento.Size = new System.Drawing.Size(769, 201);
            this.dgvAtendimento.TabIndex = 14;
            this.dgvAtendimento.DataBindingComplete += new System.Windows.Forms.DataGridViewBindingCompleteEventHandler(this.dgvAtendimento_DataBindingComplete);
            this.dgvAtendimento.RowPrePaint += new System.Windows.Forms.DataGridViewRowPrePaintEventHandler(this.grd_aso_RowPrePaint);
            this.dgvAtendimento.DoubleClick += new System.EventHandler(this.grd_aso_DoubleClick);
            // 
            // lblTextoCancelado
            // 
            this.lblTextoCancelado.AutoSize = true;
            this.lblTextoCancelado.Location = new System.Drawing.Point(108, 20);
            this.lblTextoCancelado.Name = "lblTextoCancelado";
            this.lblTextoCancelado.Size = new System.Drawing.Size(70, 13);
            this.lblTextoCancelado.TabIndex = 33;
            this.lblTextoCancelado.Text = "Laudo Inapto";
            // 
            // lblLegendaInapto
            // 
            this.lblLegendaInapto.AutoSize = true;
            this.lblLegendaInapto.BackColor = System.Drawing.Color.Gray;
            this.lblLegendaInapto.Location = new System.Drawing.Point(89, 20);
            this.lblLegendaInapto.Name = "lblLegendaInapto";
            this.lblLegendaInapto.Size = new System.Drawing.Size(13, 13);
            this.lblLegendaInapto.TabIndex = 32;
            this.lblLegendaInapto.Text = "  ";
            // 
            // textTotal
            // 
            this.textTotal.BackColor = System.Drawing.Color.LightBlue;
            this.textTotal.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textTotal.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textTotal.ForeColor = System.Drawing.Color.Black;
            this.textTotal.Location = new System.Drawing.Point(135, 16);
            this.textTotal.MaxLength = 10;
            this.textTotal.Name = "textTotal";
            this.textTotal.ReadOnly = true;
            this.textTotal.Size = new System.Drawing.Size(71, 23);
            this.textTotal.TabIndex = 30;
            this.textTotal.TabStop = false;
            this.textTotal.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // lblExamePendente
            // 
            this.lblExamePendente.AutoSize = true;
            this.lblExamePendente.ForeColor = System.Drawing.Color.Red;
            this.lblExamePendente.Location = new System.Drawing.Point(567, 118);
            this.lblExamePendente.Name = "lblExamePendente";
            this.lblExamePendente.Size = new System.Drawing.Size(194, 13);
            this.lblExamePendente.TabIndex = 28;
            this.lblExamePendente.Text = "Duplo clique para checklist dos exames";
            // 
            // lblTotalTitulo
            // 
            this.lblTotalTitulo.AutoSize = true;
            this.lblTotalTitulo.Location = new System.Drawing.Point(17, 13);
            this.lblTotalTitulo.Name = "lblTotalTitulo";
            this.lblTotalTitulo.Size = new System.Drawing.Size(112, 26);
            this.lblTotalTitulo.TabIndex = 29;
            this.lblTotalTitulo.Text = "Quantidade de \r\natendimentos filtrados:";
            // 
            // lblAsoCancelado
            // 
            this.lblAsoCancelado.AutoSize = true;
            this.lblAsoCancelado.Location = new System.Drawing.Point(25, 20);
            this.lblAsoCancelado.Name = "lblAsoCancelado";
            this.lblAsoCancelado.Size = new System.Drawing.Size(58, 13);
            this.lblAsoCancelado.TabIndex = 27;
            this.lblAsoCancelado.Text = "Cancelado";
            // 
            // lblLegendaCancelado
            // 
            this.lblLegendaCancelado.AutoSize = true;
            this.lblLegendaCancelado.BackColor = System.Drawing.Color.Red;
            this.lblLegendaCancelado.Location = new System.Drawing.Point(6, 20);
            this.lblLegendaCancelado.Name = "lblLegendaCancelado";
            this.lblLegendaCancelado.Size = new System.Drawing.Size(13, 13);
            this.lblLegendaCancelado.TabIndex = 31;
            this.lblLegendaCancelado.Text = "  ";
            // 
            // grbLegenda
            // 
            this.grbLegenda.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.grbLegenda.Controls.Add(this.lblBloqueado);
            this.grbLegenda.Controls.Add(this.lblLegendaBloqueado);
            this.grbLegenda.Controls.Add(this.lblTextPrioridade);
            this.grbLegenda.Controls.Add(this.lblLegendaPrioridade);
            this.grbLegenda.Controls.Add(this.lblTextConstrucao);
            this.grbLegenda.Controls.Add(this.lblLegendaConstrucao);
            this.grbLegenda.Controls.Add(this.lblAtendimentoFinalizado);
            this.grbLegenda.Controls.Add(this.lblLegendaFinalizado);
            this.grbLegenda.Controls.Add(this.lblAsoCancelado);
            this.grbLegenda.Controls.Add(this.lblTextoCancelado);
            this.grbLegenda.Controls.Add(this.lblLegendaInapto);
            this.grbLegenda.Controls.Add(this.lblLegendaCancelado);
            this.grbLegenda.Location = new System.Drawing.Point(6, 351);
            this.grbLegenda.Name = "grbLegenda";
            this.grbLegenda.Size = new System.Drawing.Size(533, 47);
            this.grbLegenda.TabIndex = 34;
            this.grbLegenda.TabStop = false;
            this.grbLegenda.Text = "Legenda";
            // 
            // lblBloqueado
            // 
            this.lblBloqueado.AutoSize = true;
            this.lblBloqueado.Location = new System.Drawing.Point(439, 20);
            this.lblBloqueado.Name = "lblBloqueado";
            this.lblBloqueado.Size = new System.Drawing.Size(58, 13);
            this.lblBloqueado.TabIndex = 41;
            this.lblBloqueado.Text = "Bloqueado";
            // 
            // lblLegendaBloqueado
            // 
            this.lblLegendaBloqueado.AutoSize = true;
            this.lblLegendaBloqueado.BackColor = System.Drawing.Color.Orange;
            this.lblLegendaBloqueado.Location = new System.Drawing.Point(420, 20);
            this.lblLegendaBloqueado.Name = "lblLegendaBloqueado";
            this.lblLegendaBloqueado.Size = new System.Drawing.Size(13, 13);
            this.lblLegendaBloqueado.TabIndex = 40;
            this.lblLegendaBloqueado.Text = "  ";
            // 
            // lblTextPrioridade
            // 
            this.lblTextPrioridade.AutoSize = true;
            this.lblTextPrioridade.Location = new System.Drawing.Point(361, 20);
            this.lblTextPrioridade.Name = "lblTextPrioridade";
            this.lblTextPrioridade.Size = new System.Drawing.Size(54, 13);
            this.lblTextPrioridade.TabIndex = 39;
            this.lblTextPrioridade.Text = "Prioridade";
            // 
            // lblLegendaPrioridade
            // 
            this.lblLegendaPrioridade.AutoSize = true;
            this.lblLegendaPrioridade.BackColor = System.Drawing.Color.RoyalBlue;
            this.lblLegendaPrioridade.Location = new System.Drawing.Point(342, 20);
            this.lblLegendaPrioridade.Name = "lblLegendaPrioridade";
            this.lblLegendaPrioridade.Size = new System.Drawing.Size(13, 13);
            this.lblLegendaPrioridade.TabIndex = 38;
            this.lblLegendaPrioridade.Text = "  ";
            // 
            // lblTextConstrucao
            // 
            this.lblTextConstrucao.AutoSize = true;
            this.lblTextConstrucao.Location = new System.Drawing.Point(282, 20);
            this.lblTextConstrucao.Name = "lblTextConstrucao";
            this.lblTextConstrucao.Size = new System.Drawing.Size(61, 13);
            this.lblTextConstrucao.TabIndex = 37;
            this.lblTextConstrucao.Text = "Construção";
            // 
            // lblLegendaConstrucao
            // 
            this.lblLegendaConstrucao.AutoSize = true;
            this.lblLegendaConstrucao.BackColor = System.Drawing.Color.Black;
            this.lblLegendaConstrucao.Location = new System.Drawing.Point(263, 20);
            this.lblLegendaConstrucao.Name = "lblLegendaConstrucao";
            this.lblLegendaConstrucao.Size = new System.Drawing.Size(13, 13);
            this.lblLegendaConstrucao.TabIndex = 36;
            this.lblLegendaConstrucao.Text = "  ";
            // 
            // lblAtendimentoFinalizado
            // 
            this.lblAtendimentoFinalizado.AutoSize = true;
            this.lblAtendimentoFinalizado.Location = new System.Drawing.Point(203, 20);
            this.lblAtendimentoFinalizado.Name = "lblAtendimentoFinalizado";
            this.lblAtendimentoFinalizado.Size = new System.Drawing.Size(54, 13);
            this.lblAtendimentoFinalizado.TabIndex = 35;
            this.lblAtendimentoFinalizado.Text = "Finalizado";
            // 
            // lblLegendaFinalizado
            // 
            this.lblLegendaFinalizado.AutoSize = true;
            this.lblLegendaFinalizado.BackColor = System.Drawing.Color.Green;
            this.lblLegendaFinalizado.Location = new System.Drawing.Point(184, 20);
            this.lblLegendaFinalizado.Name = "lblLegendaFinalizado";
            this.lblLegendaFinalizado.Size = new System.Drawing.Size(13, 13);
            this.lblLegendaFinalizado.TabIndex = 34;
            this.lblLegendaFinalizado.Text = "  ";
            // 
            // grbTotal
            // 
            this.grbTotal.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.grbTotal.Controls.Add(this.lblTotalTitulo);
            this.grbTotal.Controls.Add(this.textTotal);
            this.grbTotal.Location = new System.Drawing.Point(544, 351);
            this.grbTotal.Name = "grbTotal";
            this.grbTotal.Size = new System.Drawing.Size(213, 47);
            this.grbTotal.TabIndex = 35;
            this.grbTotal.TabStop = false;
            this.grbTotal.Text = "Totais";
            // 
            // btnExcluirCliente
            // 
            this.btnExcluirCliente.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnExcluirCliente.Image = global::SWS.Properties.Resources.close;
            this.btnExcluirCliente.Location = new System.Drawing.Point(381, 83);
            this.btnExcluirCliente.Name = "btnExcluirCliente";
            this.btnExcluirCliente.Size = new System.Drawing.Size(30, 21);
            this.btnExcluirCliente.TabIndex = 36;
            this.btnExcluirCliente.UseVisualStyleBackColor = true;
            this.btnExcluirCliente.Click += new System.EventHandler(this.btnExcluirCliente_Click);
            // 
            // btnExcluirFuncao
            // 
            this.btnExcluirFuncao.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnExcluirFuncao.Image = global::SWS.Properties.Resources.close;
            this.btnExcluirFuncao.Location = new System.Drawing.Point(381, 103);
            this.btnExcluirFuncao.Name = "btnExcluirFuncao";
            this.btnExcluirFuncao.Size = new System.Drawing.Size(30, 21);
            this.btnExcluirFuncao.TabIndex = 37;
            this.btnExcluirFuncao.UseVisualStyleBackColor = true;
            this.btnExcluirFuncao.Click += new System.EventHandler(this.btnExcluirFuncao_Click);
            // 
            // frmAtendimentoPrincipal
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(784, 561);
            this.MaximizeBox = true;
            this.Name = "frmAtendimentoPrincipal";
            this.Text = "GERENCIAR ATENDIMENTO";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.spForm.Panel1.ResumeLayout(false);
            this.spForm.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.spForm)).EndInit();
            this.spForm.ResumeLayout(false);
            this.pnlForm.ResumeLayout(false);
            this.pnlForm.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.banner)).EndInit();
            this.flpAcao.ResumeLayout(false);
            this.grbAtendimento.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvAtendimento)).EndInit();
            this.grbLegenda.ResumeLayout(false);
            this.grbLegenda.PerformLayout();
            this.grbTotal.ResumeLayout(false);
            this.grbTotal.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.FlowLayoutPanel flpAcao;
        private System.Windows.Forms.Button btIncluir;
        private System.Windows.Forms.Button btAlterar;
        private System.Windows.Forms.Button btFinalizar;
        private System.Windows.Forms.Button btCancelar;
        private System.Windows.Forms.Button btImprimir;
        private System.Windows.Forms.Button btAnexo;
        private System.Windows.Forms.Button bt2Via;
        private System.Windows.Forms.Button btTermica;
        private System.Windows.Forms.Button btLaudar;
        private System.Windows.Forms.Button btProntuario;
        private System.Windows.Forms.Button btFechar;
        private System.Windows.Forms.TextBox lblTipo;
        private System.Windows.Forms.TextBox lblFuncao;
        private System.Windows.Forms.TextBox lblCliente;
        private System.Windows.Forms.TextBox lblDataGravacao;
        private System.Windows.Forms.TextBox lblDataEmissao;
        private System.Windows.Forms.TextBox lblSituacao;
        private System.Windows.Forms.TextBox lblRG;
        private System.Windows.Forms.TextBox lblCPF;
        private System.Windows.Forms.TextBox lblCodigo;
        public System.Windows.Forms.MaskedTextBox textCodigo;
        private System.Windows.Forms.MaskedTextBox textCpf;
        private System.Windows.Forms.TextBox textRg;
        private ComboBoxWithBorder cbSituacao;
        private System.Windows.Forms.DateTimePicker dataGravacaoInicial;
        private System.Windows.Forms.DateTimePicker dataGravacaoFinal;
        private System.Windows.Forms.DateTimePicker dataEmissaoInicial;
        private System.Windows.Forms.DateTimePicker dataEmissaoFinal;
        private System.Windows.Forms.TextBox lblColaborador;
        private System.Windows.Forms.TextBox textNome;
        private System.Windows.Forms.Button btCliente;
        public System.Windows.Forms.TextBox textCliente;
        private System.Windows.Forms.Button btFuncao;
        public System.Windows.Forms.TextBox textFuncao;
        private ComboBoxWithBorder cbTipo;
        private System.Windows.Forms.Button btPesquisa;
        private System.Windows.Forms.GroupBox grbAtendimento;
        public System.Windows.Forms.DataGridView dgvAtendimento;
        private System.Windows.Forms.Label lblTextoCancelado;
        private System.Windows.Forms.Label lblLegendaInapto;
        private System.Windows.Forms.TextBox textTotal;
        private System.Windows.Forms.Label lblExamePendente;
        private System.Windows.Forms.Label lblTotalTitulo;
        private System.Windows.Forms.Label lblAsoCancelado;
        private System.Windows.Forms.Label lblLegendaCancelado;
        private System.Windows.Forms.GroupBox grbLegenda;
        private System.Windows.Forms.GroupBox grbTotal;
        private System.Windows.Forms.Label lblAtendimentoFinalizado;
        private System.Windows.Forms.Label lblLegendaFinalizado;
        private System.Windows.Forms.Label lblTextConstrucao;
        private System.Windows.Forms.Label lblLegendaConstrucao;
        private System.Windows.Forms.Button btnExcluirFuncao;
        private System.Windows.Forms.Button btnExcluirCliente;
        private System.Windows.Forms.Label lblTextPrioridade;
        private System.Windows.Forms.Label lblLegendaPrioridade;
        private System.Windows.Forms.Button btnDesbloquear;
        private System.Windows.Forms.Label lblBloqueado;
        private System.Windows.Forms.Label lblLegendaBloqueado;
    }
}