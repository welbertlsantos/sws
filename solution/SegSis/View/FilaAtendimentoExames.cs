﻿using SWS.Entidade;
using SWS.Excecao;
using SWS.Facade;
using SWS.Resources;
using SWS.View.Resources;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace SWS.View
{
    public partial class frmFilaAtendimentoExames : frmTemplateConsulta
    {
        private LinkedList<ItemFilaAtendimento> listaAtendimento;
        private Sala sala;
        private LinkedList<ItemFilaAtendimento> listaItensAtendimentoFinalizados = new LinkedList<ItemFilaAtendimento>();

        public frmFilaAtendimentoExames(LinkedList<ItemFilaAtendimento> listaAtendimento, Sala sala)
        {
            InitializeComponent();
            ActiveControl = dgvExame;
            ItemFilaAtendimento primeiroItemAtendimento = listaAtendimento.First();

            this.listaAtendimento = listaAtendimento;
            this.sala = sala;

            FilaFacade filaFacade = FilaFacade.getInstance();
            listaAtendimento.ToList().ForEach(delegate(ItemFilaAtendimento itemFila)
            {
                filaFacade.updateFilaAtendimento(itemFila, true, false, null, null, filaFacade.buscaSalaExamesBySala(sala).Id, false, ApplicationConstants.ATENDER_EXAME, PermissionamentoFacade.usuarioAutenticado, filaFacade.findAsoByItemFilaAtendimento(itemFila), sala);
                    
            });

            
            montaGridExamesAtendendo();

        }

        private void btnConfirmar_Click(object sender, EventArgs e)
        {
            String geraMovimento = String.Empty;
            ItemFilaAtendimento itemFilaAtendimento = null;
            Aso aso = null;
            SalaExame salaExame = null;
            FilaFacade filaFacade = FilaFacade.getInstance();

            try
            {
                this.Cursor = Cursors.WaitCursor;
                
                AsoFacade asoFacade = AsoFacade.getInstance();
                ClienteFacade clienteFacade = ClienteFacade.getInstance();
                EstudoFacade pcmsoFacade = EstudoFacade.getInstance();
                FinanceiroFacade financeiroFacade = FinanceiroFacade.getInstance();
                Cliente prestadorItem = null;
                GheFonteAgenteExameAso gheFonteAgenteExameAso = null;
                ClienteFuncaoExameASo clienteFuncaoExameAso = null;
                HashSet<ItemFilaAtendimento> itemFilaAtendimentoSet = new HashSet<ItemFilaAtendimento>();
                Boolean validador = false;
                bool marcado = false;

                if (dgvExame.CurrentRow == null)
                    throw new Exception("Selecione um exame para finalizar.");

                foreach (DataGridViewRow dvRow in dgvExame.Rows)
                {
                    if ((bool)dvRow.Cells[17].Value)
                    {

                        /*verificando se o usuário selecionou mais de um atendimento para finalização. Caso tenha selecionado mais de um
                         * atendimento então será dado uma mensagem de erro */

                        string codigoAtendimentoSelecionado = dvRow.Cells[15].Value.ToString();
                        foreach (DataGridViewRow dvRowin in dgvExame.Rows)
                        {
                            if ((bool)dvRowin.Cells[17].Value == true && !string.Equals(dvRowin.Cells[15].Value, codigoAtendimentoSelecionado))
                                throw new FilaAtendimentoException("Você não pode finalizar mais de um atendimento por vez!");
                        }
                     
                        marcado = true;
                    
                        itemFilaAtendimento = new ItemFilaAtendimento((Int64)dvRow.Cells[0].Value, (string)dvRow.Cells[1].Value, null, (String)dvRow.Cells[3].Value, (String)dvRow.Cells[4].Value, (String)dvRow.Cells[5].Value, null, null, (String)dvRow.Cells[6].Value, new Exame((Int64)dvRow.Cells[10].Value, (String)dvRow.Cells[7].Value, null, null, 0, 0, 0, false, false, string.Empty, false, null, false), false, Convert.ToDateTime(dvRow.Cells[8].Value), (string)dvRow.Cells[9].Value, sala.Descricao, sala.Andar, null, null, null, null, null, null, String.Empty, false, string.Empty);

                        salaExame = filaFacade.buscaSalaExamesBySala(sala);

                        aso = filaFacade.findAsoByItemFilaAtendimento(itemFilaAtendimento);
                        aso = asoFacade.findAsoByIdComExamesTranscritos((Int64)aso.Id);
                        aso = asoFacade.findAtendimentoById((Int64)aso.Id);

                        itemFilaAtendimentoSet = filaFacade.updateFilaAtendimento(itemFilaAtendimento, true, true, null, null, (Int64)salaExame.Id, false, ApplicationConstants.FINALIZAR_EXAME, PermissionamentoFacade.usuarioAutenticado, aso, sala);

                        validador = true;

                        listaItensAtendimentoFinalizados.AddFirst(itemFilaAtendimento);
                        /* excluindo da lista o exame finalizado para atualizar o usuario. */
                        listaAtendimento.Remove(listaItensAtendimentoFinalizados.ToList().Find(x => x.IdItem == (long)dvRow.Cells[0].Value));
                        
                        /* verificando se o cliente gera movimento na finalização do exame.
                         * * caso o retorno seja true, então será gerado uma inclusão no movimento financeiro. */
                        
                        PermissionamentoFacade.mapaConfiguracoes.TryGetValue(ConfigurationConstans.GERA_MOVIMENTO_FINALIZACAO_EXAME, out geraMovimento);

                        if (!String.IsNullOrEmpty(this.dgvExame.CurrentRow.Cells[16].Value.ToString()))
                            prestadorItem = clienteFacade.findClienteByRazaoSocical(dvRow.Cells[16].Value.ToString());

                        if (Convert.ToBoolean(geraMovimento))
                        {
                            /* verificando qual objeto será usado. ClienteFuncaoExameAso ou GheFonteAgenteExameAso */
                            if (String.Equals(ApplicationConstants.EXAME_PCMSO, dvRow.Cells[6].Value.ToString()))
                                gheFonteAgenteExameAso = new GheFonteAgenteExameAso((Int64)dvRow.Cells[0].Value, null, aso, new GheFonteAgenteExame(null, pcmsoFacade.findExameById((Int64)dvRow.Cells[10].Value), null, 0, String.Empty, false, false, false), null, false, false, String.Empty, false, null, null, null, null, false, null, String.Empty, String.Empty, false, false, prestadorItem, null, false, false, null, null, false, true);
                            else
                                clienteFuncaoExameAso = new ClienteFuncaoExameASo((Int64)dvRow.Cells[0].Value, null, aso, new ClienteFuncaoExame(null, null, pcmsoFacade.findExameById((Int64)dvRow.Cells[10].Value), String.Empty, 0), null, false, false, String.Empty, false, null, null, null, null, false, null, String.Empty, String.Empty, false, prestadorItem, null, null, null, true);
                        
                            financeiroFacade.incluirMovimentoPorItem(gheFonteAgenteExameAso, clienteFuncaoExameAso);
                        }
                        
                        /* verificando se o exame finalizado é o ultimo exame. caso seja o ultimo então será solicitado ao usuário que finalize 
                         * * o atendimento.*/

                        LinkedList<ItemFilaAtendimento> atendimentos = filaFacade.buscaExamesInSituacaoByAso(aso, false, false, false, false, false);

                        if (atendimentos.Count == 0)
                        {
                            if (MessageBox.Show("Esse exame é o último exame do colaborador. Deseja finalizar o atendimento dele?", "Atenção", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                            {
                                aso = asoFacade.findAtendimentoById((long)aso.Id);
                                asoFacade.finalizaAso(aso);
                                MessageBox.Show("Atendimento finalizado com sucesso", "Confirmação", MessageBoxButtons.OK, MessageBoxIcon.Information);
                            }
                            
                        }
                        
                    }
                }

                if (!marcado)
                    throw new FilaAtendimentoException(FilaAtendimentoException.msg5);

                montaGridExamesAtendendo();

                if (validador)
                {
                    if (MessageBox.Show("Gostaria de laudar o(s) exame(s)? Em caso de NÂO será(ão) laudado(s) como Normal(is) podendo ser alterado depois.", "Confirmaçao", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) == DialogResult.Yes)
                    {
                        frmAtendimentoLaudar formAsoLudar = new frmAtendimentoLaudar(aso, listaItensAtendimentoFinalizados, false);
                        formAsoLudar.ShowDialog();
                    }
                    else
                    {
                        /* incluindo exames como normais */

                        foreach (ItemFilaAtendimento item in listaItensAtendimentoFinalizados)
                        {
                            if (item.TipoExame == ApplicationConstants.EXAME_EXTRA)
                            {
                                /* exame extra */
                                ClienteFuncaoExameASo clienteFuncaoExameAsoLaudar = new ClienteFuncaoExameASo();
                                clienteFuncaoExameAsoLaudar.Id = item.IdItem;
                                clienteFuncaoExameAsoLaudar.Resultado = ApplicationConstants.NORMAL;
                                clienteFuncaoExameAsoLaudar.ObservacaoResultado = string.Empty;
                                asoFacade.updateClienteFuncaoExameAsoLaudoAso(clienteFuncaoExameAsoLaudar);

                            }
                            else
                            {
                                /* exame pcmso */
                                GheFonteAgenteExameAso gheFonteAgenteExameAsoLaudar = new GheFonteAgenteExameAso();
                                gheFonteAgenteExameAsoLaudar.Id = item.IdItem;
                                gheFonteAgenteExameAsoLaudar.Resultado = ApplicationConstants.NORMAL;
                                gheFonteAgenteExameAsoLaudar.Observacao = string.Empty;
                                asoFacade.updateGheFonteAgenteExameAsoLaudoAso(gheFonteAgenteExameAsoLaudar);
                            }
                        }

                    }
                }

                /* não pode fechar a tela enquanto houver atendimento pendente */
                /* TODO- desenvolver aqui */

                if (listaAtendimento.Count() == 0)
                    this.Close();
            }

            catch (FilaAtendimentoException ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            catch (Exception)
            {
                /* o exame não conseguiu ser finalizado porque havia algum erro de contrato,
                 * sendo assim, o exame será finalizado porém ficará pendente no gerenciamento
                 * do aso para ser finalizado, após resolver o problema do contrato */

                if (MessageBox.Show("Gostaria de laudar o(s) exame(s)? Em caso de NÂO será(ão) laudado(s) como Normal(is) podendo ser alterado depois.", "Confirmação", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) == DialogResult.Yes)
                {
                    frmAtendimentoLaudar formAsoLudar = new frmAtendimentoLaudar(aso, listaItensAtendimentoFinalizados, false);
                    formAsoLudar.ShowDialog();
                }
                else
                {
                    AsoFacade asoFacade = AsoFacade.getInstance();

                    /* incluindo laudo como normal padrão */
                    foreach (ItemFilaAtendimento item in listaItensAtendimentoFinalizados)
                    {
                        if (item.TipoExame == ApplicationConstants.EXAME_EXTRA)
                        {
                            /* exame extra */
                            ClienteFuncaoExameASo clienteFuncaoExameAsoLaudar = new ClienteFuncaoExameASo();
                            clienteFuncaoExameAsoLaudar.Id = item.IdItem;
                            clienteFuncaoExameAsoLaudar.Resultado = ApplicationConstants.NORMAL;
                            clienteFuncaoExameAsoLaudar.ObservacaoResultado = string.Empty;
                            asoFacade.updateClienteFuncaoExameAsoLaudoAso(clienteFuncaoExameAsoLaudar);

                        }
                        else
                        {
                            /* exame pcmso */
                            GheFonteAgenteExameAso gheFonteAgenteExameAsoLaudar = new GheFonteAgenteExameAso();
                            gheFonteAgenteExameAsoLaudar.Id = item.IdItem;
                            gheFonteAgenteExameAsoLaudar.Resultado = ApplicationConstants.NORMAL;
                            gheFonteAgenteExameAsoLaudar.Observacao = string.Empty;
                            asoFacade.updateGheFonteAgenteExameAsoLaudoAso(gheFonteAgenteExameAsoLaudar);
                        }
                    }

                }

                filaFacade.updateFilaAtendimento(itemFilaAtendimento, false, false, false, true, (Int64)salaExame.Id, false, ApplicationConstants.DEVOLVER_EXAME, PermissionamentoFacade.usuarioAutenticado, aso, sala);
                this.Close();
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }

        }

        private void btn_fechar_Click(object sender, EventArgs e)
        {
            /* Setando flag atendimento = false e id_sala null */

            FilaFacade filaFacade = FilaFacade.getInstance();
            listaAtendimento.ToList().ForEach(delegate(ItemFilaAtendimento itemFila)
            {
                filaFacade.updateFilaAtendimento(itemFila, false, false, null, null, null, false, ApplicationConstants.ATENDER_EXAME, PermissionamentoFacade.usuarioAutenticado, filaFacade.findAsoByItemFilaAtendimento(itemFila),  sala);
            });

            this.Close();
        }

        public void montaGridExamesAtendendo()
        {
            try
            {
                this.Cursor = Cursors.WaitCursor;

                FilaFacade filaFacade = FilaFacade.getInstance();

                dgvExame.Columns.Clear();

                dgvExame.ColumnCount = 17;

                dgvExame.Columns[0].HeaderText = "IdItem";
                dgvExame.Columns[0].Visible = false;

                dgvExame.Columns[1].HeaderText = "Nome";
                dgvExame.Columns[1].Visible = true;
                dgvExame.Columns[1].ReadOnly = true;

                dgvExame.Columns[2].HeaderText = "Data Nascimento";
                dgvExame.Columns[2].Visible = true;
                dgvExame.Columns[2].ReadOnly = true;

                dgvExame.Columns[3].HeaderText = "CPF";
                dgvExame.Columns[3].Visible = true;

                dgvExame.Columns[4].HeaderText = "Função";
                dgvExame.Columns[4].Visible = true;
                dgvExame.Columns[4].ReadOnly = true;

                dgvExame.Columns[5].HeaderText = "Empresa";
                dgvExame.Columns[5].Visible = true;
                dgvExame.Columns[5].ReadOnly = true;

                dgvExame.Columns[6].HeaderText = "Tipo Exame";
                dgvExame.Columns[6].Visible = false;

                dgvExame.Columns[7].HeaderText = "Exame";
                dgvExame.Columns[7].Visible = true;
                dgvExame.Columns[7].ReadOnly = true;
                dgvExame.Columns[7].DisplayIndex = 2;

                dgvExame.Columns[8].HeaderText = "Data Exame";
                dgvExame.Columns[8].Visible = true;

                dgvExame.Columns[9].HeaderText = "RG";
                dgvExame.Columns[9].Visible = true;
                dgvExame.Columns[9].ReadOnly = true;

                dgvExame.Columns[10].HeaderText = "IdExame";
                dgvExame.Columns[10].Visible = false;

                dgvExame.Columns[11].HeaderText = "Peso";
                dgvExame.Columns[11].Visible = false;
                dgvExame.Columns[11].ReadOnly = true;

                dgvExame.Columns[12].HeaderText = "Altura";
                dgvExame.Columns[12].Visible = false;
                dgvExame.Columns[12].ReadOnly = false;

                dgvExame.Columns[13].HeaderText = "Periodicidade";
                dgvExame.Columns[13].Visible = true;
                dgvExame.Columns[13].ReadOnly = false;

                dgvExame.Columns[14].HeaderText = "IMC";
                dgvExame.Columns[14].Visible = false;
                dgvExame.Columns[14].ReadOnly = true;

                dgvExame.Columns[15].HeaderText = "Cód. Atendimento";
                dgvExame.Columns[15].Visible = true;
                dgvExame.Columns[15].ReadOnly = true;
                dgvExame.Columns[15].DisplayIndex = 1;

                dgvExame.Columns[16].HeaderText = "Prestador";
                dgvExame.Columns[16].Visible = true;

                DataGridViewCheckBoxColumn check = new DataGridViewCheckBoxColumn();
                check.Name = "Select";
                dgvExame.Columns.Add(check);

                dgvExame.Columns[17].HeaderText = "Selecionar";
                dgvExame.Columns[17].DisplayIndex = 0;

                /* ordenando a lista por ordem de atendimento */

                List<ItemFilaAtendimento> listaAtendimentoOrdenada = listaAtendimento.OrderBy(x => x.CodigoAtendimento).ThenBy(x => x.Exame).ToList();

                foreach (ItemFilaAtendimento itemFilaAtendimento in listaAtendimentoOrdenada)
                    dgvExame.Rows.Add(itemFilaAtendimento.IdItem, itemFilaAtendimento.Nome, itemFilaAtendimento.DataNascimento, itemFilaAtendimento.Cpf, itemFilaAtendimento.Funcao, itemFilaAtendimento.Empresa, itemFilaAtendimento.TipoExame, itemFilaAtendimento.Exame.Descricao, itemFilaAtendimento.DataExame, itemFilaAtendimento.Rg, itemFilaAtendimento.Exame.Id, itemFilaAtendimento.Peso, itemFilaAtendimento.Altura, itemFilaAtendimento.Periodicidade, itemFilaAtendimento.Imc, itemFilaAtendimento.CodigoAtendimento, itemFilaAtendimento.Prestador, false);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }

        private void btnImprimir_Click(object sender, EventArgs e)
        {
            try
            {
                /* recuperando o relatório pelo exame selecionado */
                bool exameSelecionado = false;
                foreach (DataGridViewRow dvRow in dgvExame.Rows)
                {
                    if ((bool)dvRow.Cells[17].Value)
                    {
                        exameSelecionado = true;

                        /* verificando se o usuário selecionou mais de um exame para impressão */

                        foreach (DataGridViewRow dvRowIn in dgvExame.Rows)
                            if ((bool)dvRowIn.Cells[17].Value == true && (int)dvRow.Index != (int)dvRowIn.Index)
                                throw new Exception("Você selecionou mais de um exame para impressão.");

                        this.Cursor = Cursors.WaitCursor;

                        EstudoFacade pcmsoFacade = EstudoFacade.getInstance();
                        RelatorioFacade relatorioFacade = RelatorioFacade.getInstance();
                        FilaFacade filaFacade = FilaFacade.getInstance();

                        Exame exame = pcmsoFacade.findExameById((long)dvRow.Cells[10].Value);

                        /* recuperando o relatório vinculado ao exame */

                        Relatorio relatorio = null;

                        /* verirficando se existe relatório vinculado ao exame */
                        if (relatorioFacade.findAllRelatorioExameByExame(exame).Exists(x => string.Equals(x.Situacao, ApplicationConstants.ATIVO)))
                            relatorio = relatorioFacade.findAllRelatorioExameByExame(exame).Find(x => string.Equals(x.Situacao, ApplicationConstants.ATIVO)).Relatorio;

                        if (relatorio == null)
                            throw new Exception("O exame selecionando não tem um relatório configurado. Rever o cadastro do exame.");

                        Aso atendimento = filaFacade.findAsoByItemFilaAtendimento(new ItemFilaAtendimento((long?)dvRow.Cells[0].Value, dvRow.Cells[1].Value.ToString(), dvRow.Cells[2].Value.ToString(), dvRow.Cells[3].Value.ToString(), dvRow.Cells[4].Value.ToString(), dvRow.Cells[5].Value.ToString(), string.Empty, string.Empty, dvRow.Cells[6].Value.ToString(), pcmsoFacade.findExameById((long)dvRow.Cells[10].Value), false, Convert.ToDateTime(dvRow.Cells[8].Value), dvRow.Cells[9].Value.ToString(), string.Empty, null, null, null, dvRow.Cells[13].Value.ToString(), dvRow.Cells[14].Value.ToString(), null, dvRow.Cells[15].Value.ToString(), dvRow.Cells[16].Value.ToString(), false, string.Empty));

                        var process = new Process();

                        process.StartInfo.FileName = "Relatorio\\jre7\\bin\\java";
                        process.StartInfo.Arguments = " -jar Relatorio\\MiddlewareJasper.jar " + "Relatorio\\Report\\" + relatorio.NomeRelatorio + " ID_ASO:" + atendimento.Id + ":Integer";
                        process.StartInfo.UseShellExecute = false;
                        process.StartInfo.RedirectStandardError = true;
                        process.StartInfo.RedirectStandardInput = true;
                        process.StartInfo.RedirectStandardOutput = true;
                        process.StartInfo.CreateNoWindow = true;

                        process.Start();
                        process.StandardOutput.ReadToEnd();
                    }
                }

                if (!exameSelecionado)
                    throw new Exception("Selecione um exame.");

                

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }

    }
}
