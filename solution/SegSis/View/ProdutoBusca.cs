﻿using SWS.Entidade;
using SWS.Facade;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace SWS.View
{
    public partial class frmProdutoBusca : frmTemplateConsulta
    {
        Produto produto;

        public Produto Produto
        {
            get { return produto; }
            set { produto = value; }
        }

        public frmProdutoBusca()
        {
            InitializeComponent();
            ActiveControl = text_descricao;
        }

        private void btn_incluir_Click(object sender, EventArgs e)
        {
            try
            {
                if (dgv_produto.CurrentRow == null)
                    throw new Exception("Selecione uma linha");

                FinanceiroFacade financeiroFacade = FinanceiroFacade.getInstance();

                produto = financeiroFacade.findProdutoById((long)dgv_produto.CurrentRow.Cells[0].Value);

                this.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btn_pesquisar_Click(object sender, EventArgs e)
        {
            montaGridProdutos();
        }

        private void btn_novo_Click(object sender, EventArgs e)
        {
            frmProdutoIncluir formProdutoIncluir = new frmProdutoIncluir();
            formProdutoIncluir.ShowDialog();

            if (formProdutoIncluir.Produto != null)
            {
                produto = formProdutoIncluir.Produto;
                this.Close();
            }
        }

        private void btn_fechar_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void montaGridProdutos()
        {
            try
            {
                this.Cursor = Cursors.WaitCursor;

                this.dgv_produto.Columns.Clear();

                FinanceiroFacade financeiroFacade = FinanceiroFacade.getInstance();

                Produto produto = new Produto(null, text_descricao.Text, null, null, null,null, false, string.Empty);

                DataSet ds = financeiroFacade.findProdutoByFilter(produto);

                dgv_produto.DataSource = ds.Tables["produtos"].DefaultView;

                dgv_produto.Columns[0].HeaderText = "Id Produto ";
                dgv_produto.Columns[0].Visible = false;

                dgv_produto.Columns[1].HeaderText = "Produto ";

                dgv_produto.Columns[2].HeaderText = "Situação ";
                dgv_produto.Columns[2].Visible = false;

                dgv_produto.Columns[3].HeaderText = "Preço de Venda";
                dgv_produto.Columns[3].HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleRight;
                dgv_produto.Columns[3].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;

                dgv_produto.Columns[4].HeaderText = "Tipo ";
                dgv_produto.Columns[4].Visible = false;

                dgv_produto.Columns[5].HeaderText = "Custo";
                dgv_produto.Columns[5].Visible = true;

                dgv_produto.Columns[6].HeaderText = "Padrão Contrato";
                dgv_produto.Columns[6].Visible = false;

                
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }

        }

        private void frmProdutoBusca_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter || e.KeyCode == Keys.Return)
                SendKeys.Send("{TAB}");
        }

        private void dgv_produto_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            btn_incluir.PerformClick();
        }


    }
}
 