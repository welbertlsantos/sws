﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using SWS.Entidade;
using SWS.Facade;
using SWS.View.Resources;
using SWS.View.ViewHelper;
using SWS.ViewHelper;

namespace SWS.View
{
    public partial class frmExameBuscar : frmTemplateConsulta
    {
        private Exame exame;

        public Exame Exame
        {
            get { return exame; }
            set { exame = value; }
        }

        private bool multiplaSelecao = false;

        public bool MultiplaSelecao
        {
            get { return multiplaSelecao; }
            set { multiplaSelecao = value; }
        }

        private List<Exame> exames = new List<Exame>();

        public List<Exame> Exames
        {
            get { return exames; }
            set { exames = value; }
        }
        
        public frmExameBuscar()
        {
            InitializeComponent();
            ValidaPermissoes();
            ActiveControl = textExame;
            ComboHelper.booleanTodos(cbLaboratorio);
            if (!MultiplaSelecao)
                lblTecla.Visible = false;
        }

        public frmExameBuscar(bool multiplaSelecao)
        {
            InitializeComponent();
            ValidaPermissoes();
            ActiveControl = textExame;
            ComboHelper.booleanTodos(cbLaboratorio);
            MultiplaSelecao = multiplaSelecao;
        }

        private void btnPesquisar_Click(object sender, EventArgs e)
        {
            try
            {
                this.dgvExame.Columns.Clear();
                montaDataGrid();
            }
            catch (Exception Ex)
            {
                MessageBox.Show(Ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);

            }
        }

        private void btFechar_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnNovo_Click(object sender, EventArgs e)
        {
            frmExameIncluir formExameIncluir = new frmExameIncluir();
            formExameIncluir.ShowDialog();

            if (formExameIncluir.Exame != null)
            {
                if (!MultiplaSelecao)
                    exame = formExameIncluir.Exame;
                else
                    Exames.Add(formExameIncluir.Exame);
                this.Close();
            }
        }

        private void btnIcluir_Click(object sender, EventArgs e)
        {
            bool nenhumaSelecao = true;
            
            try
            {
                EstudoFacade pcmsoFacade = EstudoFacade.getInstance();
                
                if (!MultiplaSelecao)
                {
                    if (dgvExame.CurrentRow == null)
                        throw new Exception("Selecione uma linha.");

                    exame = pcmsoFacade.findExameById((Int64)dgvExame.CurrentRow.Cells[0].Value);
                }
                else
                {
                    foreach (DataGridViewRow dvRow in dgvExame.Rows)
                    {
                        if (dvRow.Cells[9].Value != null && (bool)dvRow.Cells[9].Value == true)
                        {
                            nenhumaSelecao = false;
                            Exames.Add(pcmsoFacade.findExameById((long)dvRow.Cells[0].Value));
                        }

                    }

                    if (nenhumaSelecao)
                        throw new Exception("Você deve selecionar pelo menos um exame.");
                }
                
                this.Close();

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        public void montaDataGrid()
        {
            try
            {
                EstudoFacade pcmsoFacade = EstudoFacade.getInstance();

                this.Cursor = Cursors.WaitCursor;

                DataSet ds = pcmsoFacade.findExameByFilter(new Exame(null, textExame.Text, ApplicationConstants.ATIVO, string.IsNullOrEmpty(((SelectItem)cbLaboratorio.SelectedItem).Valor) ? null : (bool?)Convert.ToBoolean(((SelectItem)cbLaboratorio.SelectedItem).Valor), 0, 0, 0, false, false, string.Empty, false, null, false));

                dgvExame.DataSource = ds.Tables["Exames"].DefaultView;

                dgvExame.Columns[0].HeaderText = "Código Exame";
                dgvExame.Columns[0].ReadOnly = true;
                dgvExame.Columns[0].Width = 50;

                dgvExame.Columns[1].HeaderText = "Descrição";
                dgvExame.Columns[1].ReadOnly = true;
                dgvExame.Columns[1].Width = 350;

                dgvExame.Columns[2].HeaderText = "Situação";
                dgvExame.Columns[2].Visible = false;

                dgvExame.Columns[3].HeaderText = "Laboratório";
                dgvExame.Columns[3].ReadOnly = true;

                dgvExame.Columns[4].HeaderText = "Preço de Tabela";
                dgvExame.Columns[4].Visible = false;

                dgvExame.Columns[5].HeaderText = "Prioridade na Fila";
                dgvExame.Columns[5].Visible = false;

                dgvExame.Columns[6].HeaderText = "Preço de custo";
                dgvExame.Columns[6].Visible = false;

                dgvExame.Columns[7].HeaderText = "Externo";
                dgvExame.Columns[7].Visible = false;

                dgvExame.Columns[8].HeaderText = "Documentação";
                dgvExame.Columns[8].Visible = false;

                if (MultiplaSelecao)
                {
                    dgvExame.RowHeadersVisible = false;
                    DataGridViewCheckBoxColumn check = new DataGridViewCheckBoxColumn();
                    check.Name = "Selecionar";
                    dgvExame.Columns.Add(check);
                    dgvExame.Columns[9].DisplayIndex = 0;
                    dgvExame.Columns[9].ReadOnly = false;
                    

                    foreach (DataGridViewRow dvRow in dgvExame.Rows)
                        dvRow.Cells[9].Value = false;
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }

        public void ValidaPermissoes()
        {
            PermissionamentoFacade permissionamentoFacade = PermissionamentoFacade.getInstance();
            btnNovo.Enabled = permissionamentoFacade.hasPermission("EXAME", "INCLUIR");
        }

        private void frmExameBuscar_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter || e.KeyCode == Keys.Return)
                SendKeys.Send("{TAB");

            if (MultiplaSelecao)
            {
                if (e.KeyCode == Keys.F2)
                {
                    if (dgvExame.RowCount > 0)
                    {
                        if ((bool)dgvExame.Rows[0].Cells[9].Value)
                        {
                            foreach (DataGridViewRow dvRow in dgvExame.Rows)
                                dvRow.Cells[9].Value = false;
                        }
                        else
                        {
                            foreach (DataGridViewRow dvRow in dgvExame.Rows)
                                dvRow.Cells[9].Value = true;
                        }
                    }
                }
            }
        }

        private void dgvExame_CellContentDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            if (!MultiplaSelecao)
                btnIcluir.PerformClick();
        }

        private void dgvExame_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            if (!MultiplaSelecao)
                btnIcluir.PerformClick();
        }

        
    }
}
