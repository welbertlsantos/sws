﻿namespace SWS.View
{
    partial class frmCnaeIncluir
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.flpAcao = new System.Windows.Forms.FlowLayoutPanel();
            this.bt_incluir = new System.Windows.Forms.Button();
            this.bt_limpar = new System.Windows.Forms.Button();
            this.bt_fechar = new System.Windows.Forms.Button();
            this.lblCodigoCnae = new System.Windows.Forms.TextBox();
            this.textCodigo = new System.Windows.Forms.MaskedTextBox();
            this.lblGrauRisco = new System.Windows.Forms.TextBox();
            this.cbGrauRisco = new SWS.ComboBoxWithBorder();
            this.lblGrupo = new System.Windows.Forms.TextBox();
            this.textGrupo = new System.Windows.Forms.TextBox();
            this.textAtividade = new System.Windows.Forms.TextBox();
            this.lblAtividade = new System.Windows.Forms.TextBox();
            this.IncluirCnaeErrorProvider = new System.Windows.Forms.ErrorProvider(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.spForm)).BeginInit();
            this.spForm.Panel1.SuspendLayout();
            this.spForm.Panel2.SuspendLayout();
            this.spForm.SuspendLayout();
            this.pnlForm.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.banner)).BeginInit();
            this.flpAcao.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.IncluirCnaeErrorProvider)).BeginInit();
            this.SuspendLayout();
            // 
            // spForm
            // 
            // 
            // spForm.Panel1
            // 
            this.spForm.Panel1.Controls.Add(this.flpAcao);
            // 
            // pnlForm
            // 
            this.pnlForm.Controls.Add(this.lblAtividade);
            this.pnlForm.Controls.Add(this.textAtividade);
            this.pnlForm.Controls.Add(this.textGrupo);
            this.pnlForm.Controls.Add(this.lblGrupo);
            this.pnlForm.Controls.Add(this.cbGrauRisco);
            this.pnlForm.Controls.Add(this.lblGrauRisco);
            this.pnlForm.Controls.Add(this.textCodigo);
            this.pnlForm.Controls.Add(this.lblCodigoCnae);
            // 
            // flpAcao
            // 
            this.flpAcao.Controls.Add(this.bt_incluir);
            this.flpAcao.Controls.Add(this.bt_limpar);
            this.flpAcao.Controls.Add(this.bt_fechar);
            this.flpAcao.Dock = System.Windows.Forms.DockStyle.Fill;
            this.flpAcao.Location = new System.Drawing.Point(0, 0);
            this.flpAcao.Name = "flpAcao";
            this.flpAcao.Size = new System.Drawing.Size(784, 35);
            this.flpAcao.TabIndex = 5;
            // 
            // bt_incluir
            // 
            this.bt_incluir.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.bt_incluir.Image = global::SWS.Properties.Resources.fechar_ico;
            this.bt_incluir.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.bt_incluir.Location = new System.Drawing.Point(3, 3);
            this.bt_incluir.Name = "bt_incluir";
            this.bt_incluir.Size = new System.Drawing.Size(71, 22);
            this.bt_incluir.TabIndex = 5;
            this.bt_incluir.Text = "&Gravar";
            this.bt_incluir.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.bt_incluir.UseVisualStyleBackColor = true;
            this.bt_incluir.Click += new System.EventHandler(this.bt_incluir_Click);
            // 
            // bt_limpar
            // 
            this.bt_limpar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.bt_limpar.Image = global::SWS.Properties.Resources.vassoura;
            this.bt_limpar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.bt_limpar.Location = new System.Drawing.Point(80, 3);
            this.bt_limpar.Name = "bt_limpar";
            this.bt_limpar.Size = new System.Drawing.Size(71, 22);
            this.bt_limpar.TabIndex = 6;
            this.bt_limpar.Text = "&Limpar";
            this.bt_limpar.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.bt_limpar.UseVisualStyleBackColor = true;
            this.bt_limpar.Click += new System.EventHandler(this.bt_limpar_Click);
            // 
            // bt_fechar
            // 
            this.bt_fechar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.bt_fechar.Image = global::SWS.Properties.Resources.close;
            this.bt_fechar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.bt_fechar.Location = new System.Drawing.Point(157, 3);
            this.bt_fechar.Name = "bt_fechar";
            this.bt_fechar.Size = new System.Drawing.Size(71, 22);
            this.bt_fechar.TabIndex = 7;
            this.bt_fechar.Text = "&Fechar";
            this.bt_fechar.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.bt_fechar.UseVisualStyleBackColor = true;
            this.bt_fechar.Click += new System.EventHandler(this.bt_fechar_Click);
            // 
            // lblCodigoCnae
            // 
            this.lblCodigoCnae.BackColor = System.Drawing.Color.LightGray;
            this.lblCodigoCnae.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblCodigoCnae.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCodigoCnae.Location = new System.Drawing.Point(12, 17);
            this.lblCodigoCnae.MaxLength = 100;
            this.lblCodigoCnae.Name = "lblCodigoCnae";
            this.lblCodigoCnae.ReadOnly = true;
            this.lblCodigoCnae.Size = new System.Drawing.Size(148, 21);
            this.lblCodigoCnae.TabIndex = 35;
            this.lblCodigoCnae.TabStop = false;
            this.lblCodigoCnae.Text = "Código CNAE";
            // 
            // textCodigo
            // 
            this.textCodigo.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textCodigo.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textCodigo.Location = new System.Drawing.Point(159, 17);
            this.textCodigo.Mask = "00,00-0/00";
            this.textCodigo.Name = "textCodigo";
            this.textCodigo.PromptChar = ' ';
            this.textCodigo.Size = new System.Drawing.Size(584, 21);
            this.textCodigo.TabIndex = 1;
            // 
            // lblGrauRisco
            // 
            this.lblGrauRisco.BackColor = System.Drawing.Color.LightGray;
            this.lblGrauRisco.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblGrauRisco.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblGrauRisco.Location = new System.Drawing.Point(12, 37);
            this.lblGrauRisco.MaxLength = 100;
            this.lblGrauRisco.Name = "lblGrauRisco";
            this.lblGrauRisco.ReadOnly = true;
            this.lblGrauRisco.Size = new System.Drawing.Size(148, 21);
            this.lblGrauRisco.TabIndex = 37;
            this.lblGrauRisco.TabStop = false;
            this.lblGrauRisco.Text = "Grau de Risco";
            // 
            // cbGrauRisco
            // 
            this.cbGrauRisco.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.cbGrauRisco.BackColor = System.Drawing.Color.LightGray;
            this.cbGrauRisco.BorderColor = System.Drawing.Color.DimGray;
            this.cbGrauRisco.BorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid;
            this.cbGrauRisco.FormattingEnabled = true;
            this.cbGrauRisco.Location = new System.Drawing.Point(159, 37);
            this.cbGrauRisco.Name = "cbGrauRisco";
            this.cbGrauRisco.Size = new System.Drawing.Size(584, 21);
            this.cbGrauRisco.TabIndex = 2;
            // 
            // lblGrupo
            // 
            this.lblGrupo.BackColor = System.Drawing.Color.LightGray;
            this.lblGrupo.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblGrupo.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblGrupo.Location = new System.Drawing.Point(12, 57);
            this.lblGrupo.MaxLength = 100;
            this.lblGrupo.Name = "lblGrupo";
            this.lblGrupo.ReadOnly = true;
            this.lblGrupo.Size = new System.Drawing.Size(148, 21);
            this.lblGrupo.TabIndex = 38;
            this.lblGrupo.TabStop = false;
            this.lblGrupo.Text = "Grupo CIPA";
            // 
            // textGrupo
            // 
            this.textGrupo.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textGrupo.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.textGrupo.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textGrupo.Location = new System.Drawing.Point(159, 57);
            this.textGrupo.MaxLength = 5;
            this.textGrupo.Name = "textGrupo";
            this.textGrupo.Size = new System.Drawing.Size(584, 21);
            this.textGrupo.TabIndex = 3;
            // 
            // textAtividade
            // 
            this.textAtividade.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textAtividade.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.textAtividade.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textAtividade.Location = new System.Drawing.Point(159, 77);
            this.textAtividade.MaxLength = 400;
            this.textAtividade.Name = "textAtividade";
            this.textAtividade.Size = new System.Drawing.Size(584, 21);
            this.textAtividade.TabIndex = 4;
            // 
            // lblAtividade
            // 
            this.lblAtividade.BackColor = System.Drawing.Color.LightGray;
            this.lblAtividade.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblAtividade.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblAtividade.Location = new System.Drawing.Point(12, 77);
            this.lblAtividade.MaxLength = 100;
            this.lblAtividade.Name = "lblAtividade";
            this.lblAtividade.ReadOnly = true;
            this.lblAtividade.Size = new System.Drawing.Size(148, 21);
            this.lblAtividade.TabIndex = 41;
            this.lblAtividade.TabStop = false;
            this.lblAtividade.Text = "Atividade";
            // 
            // IncluirCnaeErrorProvider
            // 
            this.IncluirCnaeErrorProvider.ContainerControl = this;
            // 
            // frmCnaeIncluir
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(784, 561);
            this.Name = "frmCnaeIncluir";
            this.Text = "INCLUIR CNAE";
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.frmCnaeIncluir_KeyDown);
            this.spForm.Panel1.ResumeLayout(false);
            this.spForm.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.spForm)).EndInit();
            this.spForm.ResumeLayout(false);
            this.pnlForm.ResumeLayout(false);
            this.pnlForm.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.banner)).EndInit();
            this.flpAcao.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.IncluirCnaeErrorProvider)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        protected System.Windows.Forms.FlowLayoutPanel flpAcao;
        protected System.Windows.Forms.Button bt_incluir;
        protected System.Windows.Forms.Button bt_limpar;
        protected System.Windows.Forms.Button bt_fechar;
        protected System.Windows.Forms.TextBox lblCodigoCnae;
        protected System.Windows.Forms.MaskedTextBox textCodigo;
        protected System.Windows.Forms.TextBox lblGrauRisco;
        protected ComboBoxWithBorder cbGrauRisco;
        protected System.Windows.Forms.TextBox lblGrupo;
        protected System.Windows.Forms.TextBox textGrupo;
        protected System.Windows.Forms.TextBox lblAtividade;
        protected System.Windows.Forms.TextBox textAtividade;
        protected System.Windows.Forms.ErrorProvider IncluirCnaeErrorProvider;

    }
}