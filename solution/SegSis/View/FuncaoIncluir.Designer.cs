﻿namespace SWS.View
{
    partial class frmFuncaoIncluir
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            this.flpAcao = new System.Windows.Forms.FlowLayoutPanel();
            this.btGravar = new System.Windows.Forms.Button();
            this.btFechar = new System.Windows.Forms.Button();
            this.lblCodigoCbo = new System.Windows.Forms.TextBox();
            this.lblNomeFuncao = new System.Windows.Forms.TextBox();
            this.textCodioCbo = new System.Windows.Forms.TextBox();
            this.textDescricao = new System.Windows.Forms.TextBox();
            this.grbAtividade = new System.Windows.Forms.GroupBox();
            this.dgvComentario = new System.Windows.Forms.DataGridView();
            this.flpAtividadeAcao = new System.Windows.Forms.FlowLayoutPanel();
            this.btIncluirComentario = new System.Windows.Forms.Button();
            this.btExcluirAtividade = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.spForm)).BeginInit();
            this.spForm.Panel1.SuspendLayout();
            this.spForm.Panel2.SuspendLayout();
            this.spForm.SuspendLayout();
            this.pnlForm.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.banner)).BeginInit();
            this.flpAcao.SuspendLayout();
            this.grbAtividade.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvComentario)).BeginInit();
            this.flpAtividadeAcao.SuspendLayout();
            this.SuspendLayout();
            // 
            // spForm
            // 
            // 
            // spForm.Panel1
            // 
            this.spForm.Panel1.Controls.Add(this.flpAcao);
            // 
            // pnlForm
            // 
            this.pnlForm.Controls.Add(this.flpAtividadeAcao);
            this.pnlForm.Controls.Add(this.grbAtividade);
            this.pnlForm.Controls.Add(this.lblCodigoCbo);
            this.pnlForm.Controls.Add(this.lblNomeFuncao);
            this.pnlForm.Controls.Add(this.textCodioCbo);
            this.pnlForm.Controls.Add(this.textDescricao);
            // 
            // flpAcao
            // 
            this.flpAcao.Controls.Add(this.btGravar);
            this.flpAcao.Controls.Add(this.btFechar);
            this.flpAcao.Dock = System.Windows.Forms.DockStyle.Fill;
            this.flpAcao.Location = new System.Drawing.Point(0, 0);
            this.flpAcao.Name = "flpAcao";
            this.flpAcao.Size = new System.Drawing.Size(784, 35);
            this.flpAcao.TabIndex = 0;
            // 
            // btGravar
            // 
            this.btGravar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btGravar.Image = global::SWS.Properties.Resources.Gravar;
            this.btGravar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btGravar.Location = new System.Drawing.Point(3, 3);
            this.btGravar.Name = "btGravar";
            this.btGravar.Size = new System.Drawing.Size(77, 23);
            this.btGravar.TabIndex = 8;
            this.btGravar.Text = "&Gravar";
            this.btGravar.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btGravar.UseVisualStyleBackColor = true;
            this.btGravar.Click += new System.EventHandler(this.btGravar_Click);
            // 
            // btFechar
            // 
            this.btFechar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btFechar.Image = global::SWS.Properties.Resources.close;
            this.btFechar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btFechar.Location = new System.Drawing.Point(86, 3);
            this.btFechar.Name = "btFechar";
            this.btFechar.Size = new System.Drawing.Size(77, 23);
            this.btFechar.TabIndex = 9;
            this.btFechar.Text = "&Fechar";
            this.btFechar.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btFechar.UseVisualStyleBackColor = true;
            this.btFechar.Click += new System.EventHandler(this.btFechar_Click);
            // 
            // lblCodigoCbo
            // 
            this.lblCodigoCbo.BackColor = System.Drawing.Color.LightGray;
            this.lblCodigoCbo.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblCodigoCbo.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCodigoCbo.Location = new System.Drawing.Point(12, 32);
            this.lblCodigoCbo.MaxLength = 100;
            this.lblCodigoCbo.Name = "lblCodigoCbo";
            this.lblCodigoCbo.ReadOnly = true;
            this.lblCodigoCbo.Size = new System.Drawing.Size(171, 21);
            this.lblCodigoCbo.TabIndex = 43;
            this.lblCodigoCbo.TabStop = false;
            this.lblCodigoCbo.Text = "Código CBO";
            // 
            // lblNomeFuncao
            // 
            this.lblNomeFuncao.BackColor = System.Drawing.Color.LightGray;
            this.lblNomeFuncao.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblNomeFuncao.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNomeFuncao.Location = new System.Drawing.Point(12, 12);
            this.lblNomeFuncao.MaxLength = 100;
            this.lblNomeFuncao.Name = "lblNomeFuncao";
            this.lblNomeFuncao.ReadOnly = true;
            this.lblNomeFuncao.Size = new System.Drawing.Size(171, 21);
            this.lblNomeFuncao.TabIndex = 42;
            this.lblNomeFuncao.TabStop = false;
            this.lblNomeFuncao.Text = "Nome da Função";
            // 
            // textCodioCbo
            // 
            this.textCodioCbo.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textCodioCbo.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.textCodioCbo.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textCodioCbo.Location = new System.Drawing.Point(182, 32);
            this.textCodioCbo.MaxLength = 6;
            this.textCodioCbo.Name = "textCodioCbo";
            this.textCodioCbo.Size = new System.Drawing.Size(568, 21);
            this.textCodioCbo.TabIndex = 2;
            this.textCodioCbo.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.textCodioCbo_KeyPress);
            // 
            // textDescricao
            // 
            this.textDescricao.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textDescricao.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.textDescricao.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textDescricao.Location = new System.Drawing.Point(182, 12);
            this.textDescricao.MaxLength = 100;
            this.textDescricao.Name = "textDescricao";
            this.textDescricao.Size = new System.Drawing.Size(568, 21);
            this.textDescricao.TabIndex = 1;
            // 
            // grbAtividade
            // 
            this.grbAtividade.Controls.Add(this.dgvComentario);
            this.grbAtividade.Location = new System.Drawing.Point(12, 59);
            this.grbAtividade.Name = "grbAtividade";
            this.grbAtividade.Size = new System.Drawing.Size(737, 210);
            this.grbAtividade.TabIndex = 44;
            this.grbAtividade.TabStop = false;
            this.grbAtividade.Text = "Atividade";
            // 
            // dgvComentario
            // 
            this.dgvComentario.AllowUserToAddRows = false;
            this.dgvComentario.AllowUserToDeleteRows = false;
            this.dgvComentario.AllowUserToOrderColumns = true;
            this.dgvComentario.AllowUserToResizeRows = false;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.Gainsboro;
            this.dgvComentario.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle1;
            this.dgvComentario.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.DisplayedCells;
            this.dgvComentario.BackgroundColor = System.Drawing.Color.White;
            this.dgvComentario.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.Color.LightGray;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvComentario.DefaultCellStyle = dataGridViewCellStyle2;
            this.dgvComentario.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvComentario.GridColor = System.Drawing.SystemColors.ControlDarkDark;
            this.dgvComentario.Location = new System.Drawing.Point(3, 16);
            this.dgvComentario.MultiSelect = false;
            this.dgvComentario.Name = "dgvComentario";
            this.dgvComentario.ReadOnly = true;
            this.dgvComentario.Size = new System.Drawing.Size(731, 191);
            this.dgvComentario.TabIndex = 35;
            this.dgvComentario.CellContentDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvComentario_CellContentDoubleClick);
            // 
            // flpAtividadeAcao
            // 
            this.flpAtividadeAcao.Controls.Add(this.btIncluirComentario);
            this.flpAtividadeAcao.Controls.Add(this.btExcluirAtividade);
            this.flpAtividadeAcao.Location = new System.Drawing.Point(12, 275);
            this.flpAtividadeAcao.Name = "flpAtividadeAcao";
            this.flpAtividadeAcao.Size = new System.Drawing.Size(738, 30);
            this.flpAtividadeAcao.TabIndex = 45;
            // 
            // btIncluirComentario
            // 
            this.btIncluirComentario.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btIncluirComentario.Image = global::SWS.Properties.Resources.icone_mais;
            this.btIncluirComentario.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btIncluirComentario.Location = new System.Drawing.Point(3, 3);
            this.btIncluirComentario.Name = "btIncluirComentario";
            this.btIncluirComentario.Size = new System.Drawing.Size(75, 23);
            this.btIncluirComentario.TabIndex = 38;
            this.btIncluirComentario.Text = "&Incluir";
            this.btIncluirComentario.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btIncluirComentario.UseVisualStyleBackColor = true;
            this.btIncluirComentario.Click += new System.EventHandler(this.btIncluirComentario_Click);
            // 
            // btExcluirAtividade
            // 
            this.btExcluirAtividade.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btExcluirAtividade.Image = global::SWS.Properties.Resources.lixeira;
            this.btExcluirAtividade.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btExcluirAtividade.Location = new System.Drawing.Point(84, 3);
            this.btExcluirAtividade.Name = "btExcluirAtividade";
            this.btExcluirAtividade.Size = new System.Drawing.Size(75, 23);
            this.btExcluirAtividade.TabIndex = 39;
            this.btExcluirAtividade.Text = "&Excluir";
            this.btExcluirAtividade.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btExcluirAtividade.UseVisualStyleBackColor = true;
            this.btExcluirAtividade.Click += new System.EventHandler(this.btExcluirAtividade_Click);
            // 
            // frmFuncaoIncluir
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(784, 561);
            this.Name = "frmFuncaoIncluir";
            this.Text = "INCLUIR FUNÇÃO";
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.frmFuncaoIncluir_KeyDown);
            this.spForm.Panel1.ResumeLayout(false);
            this.spForm.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.spForm)).EndInit();
            this.spForm.ResumeLayout(false);
            this.pnlForm.ResumeLayout(false);
            this.pnlForm.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.banner)).EndInit();
            this.flpAcao.ResumeLayout(false);
            this.grbAtividade.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvComentario)).EndInit();
            this.flpAtividadeAcao.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        protected System.Windows.Forms.FlowLayoutPanel flpAcao;
        protected System.Windows.Forms.Button btGravar;
        protected System.Windows.Forms.Button btFechar;
        protected System.Windows.Forms.TextBox lblCodigoCbo;
        protected System.Windows.Forms.TextBox lblNomeFuncao;
        protected System.Windows.Forms.TextBox textCodioCbo;
        protected System.Windows.Forms.TextBox textDescricao;
        protected System.Windows.Forms.FlowLayoutPanel flpAtividadeAcao;
        protected System.Windows.Forms.GroupBox grbAtividade;
        protected System.Windows.Forms.DataGridView dgvComentario;
        protected System.Windows.Forms.Button btIncluirComentario;
        protected System.Windows.Forms.Button btExcluirAtividade;
    }
}