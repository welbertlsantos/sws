﻿namespace SWS.View
{
    partial class frmAgentesDetalhar
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            ((System.ComponentModel.ISupportInitialize)(this.errorAgenteIncluir)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spForm)).BeginInit();
            this.spForm.Panel2.SuspendLayout();
            this.spForm.SuspendLayout();
            this.pnlForm.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.banner)).BeginInit();
            this.SuspendLayout();
            // 
            // textLimite
            // 
            this.textLimite.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.textLimite.Enabled = false;
            this.textLimite.Margin = new System.Windows.Forms.Padding(5, 5, 5, 5);
            // 
            // lblRisco
            // 
            this.lblRisco.Margin = new System.Windows.Forms.Padding(5, 5, 5, 5);
            // 
            // cbRisco
            // 
            this.cbRisco.Enabled = false;
            this.cbRisco.Size = new System.Drawing.Size(767, 24);
            // 
            // textDano
            // 
            this.textDano.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.textDano.Enabled = false;
            this.textDano.Margin = new System.Windows.Forms.Padding(5, 5, 5, 5);
            // 
            // textTrajetoria
            // 
            this.textTrajetoria.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.textTrajetoria.Enabled = false;
            this.textTrajetoria.Margin = new System.Windows.Forms.Padding(5, 5, 5, 5);
            // 
            // textDescricao
            // 
            this.textDescricao.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.textDescricao.Enabled = false;
            this.textDescricao.Margin = new System.Windows.Forms.Padding(5, 5, 5, 5);
            this.textDescricao.MaxLength = 255;
            // 
            // lblLimite
            // 
            this.lblLimite.Margin = new System.Windows.Forms.Padding(5, 5, 5, 5);
            // 
            // lblTrajetoria
            // 
            this.lblTrajetoria.Margin = new System.Windows.Forms.Padding(5, 5, 5, 5);
            // 
            // lblDano
            // 
            this.lblDano.Margin = new System.Windows.Forms.Padding(5, 5, 5, 5);
            // 
            // lblDescricao
            // 
            this.lblDescricao.Margin = new System.Windows.Forms.Padding(5, 5, 5, 5);
            // 
            // textIntensidade
            // 
            this.textIntensidade.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.textIntensidade.Enabled = false;
            this.textIntensidade.Margin = new System.Windows.Forms.Padding(5, 5, 5, 5);
            // 
            // textTecnica
            // 
            this.textTecnica.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.textTecnica.Enabled = false;
            this.textTecnica.Margin = new System.Windows.Forms.Padding(5, 5, 5, 5);
            // 
            // lblTecnica
            // 
            this.lblTecnica.Margin = new System.Windows.Forms.Padding(5, 5, 5, 5);
            // 
            // lblIntensidade
            // 
            this.lblIntensidade.Margin = new System.Windows.Forms.Padding(5, 5, 5, 5);
            // 
            // textCodigoEsocial
            // 
            this.textCodigoEsocial.Margin = new System.Windows.Forms.Padding(5, 5, 5, 5);
            // 
            // lblCodigoEsocial
            // 
            this.lblCodigoEsocial.Margin = new System.Windows.Forms.Padding(5, 5, 5, 5);
            // 
            // spForm
            // 
            this.spForm.SplitterDistance = 43;
            // 
            // frmAgentesDetalhar
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.ClientSize = new System.Drawing.Size(1045, 690);
            this.Margin = new System.Windows.Forms.Padding(7, 6, 7, 6);
            this.Name = "frmAgentesDetalhar";
            this.Text = "DETALHAR AGENTE";
            ((System.ComponentModel.ISupportInitialize)(this.errorAgenteIncluir)).EndInit();
            this.spForm.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.spForm)).EndInit();
            this.spForm.ResumeLayout(false);
            this.pnlForm.ResumeLayout(false);
            this.pnlForm.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.banner)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion
    }
}
