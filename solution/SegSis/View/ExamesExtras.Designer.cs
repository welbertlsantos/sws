﻿namespace SWS.View
{
    partial class frmExamesExtras
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            this.flpAcao = new System.Windows.Forms.FlowLayoutPanel();
            this.btn_fechar = new System.Windows.Forms.Button();
            this.btCliente = new System.Windows.Forms.Button();
            this.textCliente = new System.Windows.Forms.TextBox();
            this.lblCliente = new System.Windows.Forms.TextBox();
            this.lblFuncao = new System.Windows.Forms.TextBox();
            this.textFuncao = new System.Windows.Forms.TextBox();
            this.btFuncao = new System.Windows.Forms.Button();
            this.grbExame = new System.Windows.Forms.GroupBox();
            this.dgvExame = new System.Windows.Forms.DataGridView();
            this.btExcluirExame = new System.Windows.Forms.Button();
            this.btIncluirExame = new System.Windows.Forms.Button();
            this.grbPeriodicidade = new System.Windows.Forms.GroupBox();
            this.dgvPeriodicidade = new System.Windows.Forms.DataGridView();
            this.btExcluirPeriodicidade = new System.Windows.Forms.Button();
            this.btIncluirPeriodicidade = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.spForm)).BeginInit();
            this.spForm.Panel1.SuspendLayout();
            this.spForm.Panel2.SuspendLayout();
            this.spForm.SuspendLayout();
            this.pnlForm.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.banner)).BeginInit();
            this.flpAcao.SuspendLayout();
            this.grbExame.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvExame)).BeginInit();
            this.grbPeriodicidade.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvPeriodicidade)).BeginInit();
            this.SuspendLayout();
            // 
            // spForm
            // 
            // 
            // spForm.Panel1
            // 
            this.spForm.Panel1.Controls.Add(this.flpAcao);
            this.spForm.SplitterDistance = 44;
            // 
            // pnlForm
            // 
            this.pnlForm.Controls.Add(this.btExcluirPeriodicidade);
            this.pnlForm.Controls.Add(this.grbPeriodicidade);
            this.pnlForm.Controls.Add(this.btExcluirExame);
            this.pnlForm.Controls.Add(this.btIncluirPeriodicidade);
            this.pnlForm.Controls.Add(this.grbExame);
            this.pnlForm.Controls.Add(this.btIncluirExame);
            this.pnlForm.Controls.Add(this.btFuncao);
            this.pnlForm.Controls.Add(this.lblFuncao);
            this.pnlForm.Controls.Add(this.btCliente);
            this.pnlForm.Controls.Add(this.lblCliente);
            this.pnlForm.Controls.Add(this.textCliente);
            this.pnlForm.Controls.Add(this.textFuncao);
            this.pnlForm.Size = new System.Drawing.Size(764, 560);
            // 
            // flpAcao
            // 
            this.flpAcao.Controls.Add(this.btn_fechar);
            this.flpAcao.Dock = System.Windows.Forms.DockStyle.Fill;
            this.flpAcao.Location = new System.Drawing.Point(0, 0);
            this.flpAcao.Name = "flpAcao";
            this.flpAcao.Size = new System.Drawing.Size(784, 44);
            this.flpAcao.TabIndex = 0;
            // 
            // btn_fechar
            // 
            this.btn_fechar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_fechar.Image = global::SWS.Properties.Resources.close;
            this.btn_fechar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btn_fechar.Location = new System.Drawing.Point(3, 3);
            this.btn_fechar.Name = "btn_fechar";
            this.btn_fechar.Size = new System.Drawing.Size(75, 23);
            this.btn_fechar.TabIndex = 9;
            this.btn_fechar.TabStop = false;
            this.btn_fechar.Text = "&Fechar";
            this.btn_fechar.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btn_fechar.UseVisualStyleBackColor = true;
            this.btn_fechar.Click += new System.EventHandler(this.btn_fechar_Click);
            // 
            // btCliente
            // 
            this.btCliente.BackColor = System.Drawing.Color.White;
            this.btCliente.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btCliente.Image = global::SWS.Properties.Resources.busca;
            this.btCliente.Location = new System.Drawing.Point(706, 18);
            this.btCliente.Name = "btCliente";
            this.btCliente.Size = new System.Drawing.Size(29, 21);
            this.btCliente.TabIndex = 1;
            this.btCliente.UseVisualStyleBackColor = false;
            this.btCliente.Click += new System.EventHandler(this.btCliente_Click);
            // 
            // textCliente
            // 
            this.textCliente.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.textCliente.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textCliente.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textCliente.ForeColor = System.Drawing.Color.Black;
            this.textCliente.Location = new System.Drawing.Point(153, 18);
            this.textCliente.Name = "textCliente";
            this.textCliente.ReadOnly = true;
            this.textCliente.Size = new System.Drawing.Size(560, 21);
            this.textCliente.TabIndex = 0;
            this.textCliente.TabStop = false;
            // 
            // lblCliente
            // 
            this.lblCliente.BackColor = System.Drawing.Color.LightGray;
            this.lblCliente.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblCliente.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCliente.Location = new System.Drawing.Point(15, 18);
            this.lblCliente.MaxLength = 15;
            this.lblCliente.Name = "lblCliente";
            this.lblCliente.Size = new System.Drawing.Size(139, 21);
            this.lblCliente.TabIndex = 2;
            this.lblCliente.TabStop = false;
            this.lblCliente.Text = "Cliente";
            // 
            // lblFuncao
            // 
            this.lblFuncao.BackColor = System.Drawing.Color.LightGray;
            this.lblFuncao.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblFuncao.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblFuncao.Location = new System.Drawing.Point(15, 38);
            this.lblFuncao.MaxLength = 15;
            this.lblFuncao.Name = "lblFuncao";
            this.lblFuncao.Size = new System.Drawing.Size(139, 21);
            this.lblFuncao.TabIndex = 3;
            this.lblFuncao.TabStop = false;
            this.lblFuncao.Text = "Função";
            // 
            // textFuncao
            // 
            this.textFuncao.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.textFuncao.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textFuncao.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textFuncao.ForeColor = System.Drawing.Color.Black;
            this.textFuncao.Location = new System.Drawing.Point(153, 38);
            this.textFuncao.Name = "textFuncao";
            this.textFuncao.ReadOnly = true;
            this.textFuncao.Size = new System.Drawing.Size(560, 21);
            this.textFuncao.TabIndex = 4;
            this.textFuncao.TabStop = false;
            // 
            // btFuncao
            // 
            this.btFuncao.BackColor = System.Drawing.Color.White;
            this.btFuncao.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btFuncao.Image = global::SWS.Properties.Resources.busca;
            this.btFuncao.Location = new System.Drawing.Point(706, 38);
            this.btFuncao.Name = "btFuncao";
            this.btFuncao.Size = new System.Drawing.Size(29, 21);
            this.btFuncao.TabIndex = 5;
            this.btFuncao.UseVisualStyleBackColor = false;
            this.btFuncao.Click += new System.EventHandler(this.btFuncao_Click);
            // 
            // grbExame
            // 
            this.grbExame.Controls.Add(this.dgvExame);
            this.grbExame.Location = new System.Drawing.Point(12, 65);
            this.grbExame.Name = "grbExame";
            this.grbExame.Size = new System.Drawing.Size(723, 192);
            this.grbExame.TabIndex = 6;
            this.grbExame.TabStop = false;
            this.grbExame.Text = "Exame(s)";
            // 
            // dgvExame
            // 
            this.dgvExame.AllowUserToAddRows = false;
            this.dgvExame.AllowUserToDeleteRows = false;
            this.dgvExame.AllowUserToOrderColumns = true;
            this.dgvExame.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this.dgvExame.BackgroundColor = System.Drawing.Color.White;
            this.dgvExame.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle3.BackColor = System.Drawing.Color.LightGray;
            dataGridViewCellStyle3.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvExame.DefaultCellStyle = dataGridViewCellStyle3;
            this.dgvExame.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvExame.Location = new System.Drawing.Point(3, 16);
            this.dgvExame.MultiSelect = false;
            this.dgvExame.Name = "dgvExame";
            this.dgvExame.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.CellSelect;
            this.dgvExame.Size = new System.Drawing.Size(717, 173);
            this.dgvExame.TabIndex = 3;
            this.dgvExame.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvExame_CellClick);
            this.dgvExame.CellEndEdit += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvExame_CellEndEdit);
            this.dgvExame.CellEnter += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvExame_CellEnter);
            this.dgvExame.DataBindingComplete += new System.Windows.Forms.DataGridViewBindingCompleteEventHandler(this.dgvExame_DataBindingComplete);
            this.dgvExame.EditingControlShowing += new System.Windows.Forms.DataGridViewEditingControlShowingEventHandler(this.dgvExame_EditingControlShowing);
            this.dgvExame.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.dgvExame_KeyPress);
            // 
            // btExcluirExame
            // 
            this.btExcluirExame.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btExcluirExame.Image = global::SWS.Properties.Resources.lixeira;
            this.btExcluirExame.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btExcluirExame.Location = new System.Drawing.Point(96, 263);
            this.btExcluirExame.Name = "btExcluirExame";
            this.btExcluirExame.Size = new System.Drawing.Size(75, 23);
            this.btExcluirExame.TabIndex = 5;
            this.btExcluirExame.Text = "Excluir";
            this.btExcluirExame.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btExcluirExame.UseVisualStyleBackColor = true;
            this.btExcluirExame.Click += new System.EventHandler(this.btExcluirExame_Click);
            // 
            // btIncluirExame
            // 
            this.btIncluirExame.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btIncluirExame.Image = global::SWS.Properties.Resources.icone_mais;
            this.btIncluirExame.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btIncluirExame.Location = new System.Drawing.Point(15, 263);
            this.btIncluirExame.Name = "btIncluirExame";
            this.btIncluirExame.Size = new System.Drawing.Size(75, 23);
            this.btIncluirExame.TabIndex = 4;
            this.btIncluirExame.Text = "Incluir";
            this.btIncluirExame.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btIncluirExame.UseVisualStyleBackColor = true;
            this.btIncluirExame.Click += new System.EventHandler(this.btIncluirExame_Click);
            // 
            // grbPeriodicidade
            // 
            this.grbPeriodicidade.Controls.Add(this.dgvPeriodicidade);
            this.grbPeriodicidade.Location = new System.Drawing.Point(12, 301);
            this.grbPeriodicidade.Name = "grbPeriodicidade";
            this.grbPeriodicidade.Size = new System.Drawing.Size(723, 198);
            this.grbPeriodicidade.TabIndex = 7;
            this.grbPeriodicidade.TabStop = false;
            this.grbPeriodicidade.Text = "Periodicidade(s)";
            // 
            // dgvPeriodicidade
            // 
            this.dgvPeriodicidade.AllowUserToAddRows = false;
            this.dgvPeriodicidade.AllowUserToDeleteRows = false;
            this.dgvPeriodicidade.AllowUserToOrderColumns = true;
            this.dgvPeriodicidade.AllowUserToResizeColumns = false;
            this.dgvPeriodicidade.AllowUserToResizeRows = false;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.Gainsboro;
            this.dgvPeriodicidade.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle1;
            this.dgvPeriodicidade.BackgroundColor = System.Drawing.Color.White;
            this.dgvPeriodicidade.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.Color.LightGray;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvPeriodicidade.DefaultCellStyle = dataGridViewCellStyle2;
            this.dgvPeriodicidade.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvPeriodicidade.Location = new System.Drawing.Point(3, 16);
            this.dgvPeriodicidade.MultiSelect = false;
            this.dgvPeriodicidade.Name = "dgvPeriodicidade";
            this.dgvPeriodicidade.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.CellSelect;
            this.dgvPeriodicidade.Size = new System.Drawing.Size(717, 179);
            this.dgvPeriodicidade.TabIndex = 6;
            this.dgvPeriodicidade.CellEndEdit += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvPeriodicidade_CellEndEdit);
            // 
            // btExcluirPeriodicidade
            // 
            this.btExcluirPeriodicidade.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btExcluirPeriodicidade.Image = global::SWS.Properties.Resources.lixeira;
            this.btExcluirPeriodicidade.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btExcluirPeriodicidade.Location = new System.Drawing.Point(96, 505);
            this.btExcluirPeriodicidade.Name = "btExcluirPeriodicidade";
            this.btExcluirPeriodicidade.Size = new System.Drawing.Size(75, 23);
            this.btExcluirPeriodicidade.TabIndex = 7;
            this.btExcluirPeriodicidade.Text = "Excluir";
            this.btExcluirPeriodicidade.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btExcluirPeriodicidade.UseVisualStyleBackColor = true;
            this.btExcluirPeriodicidade.Click += new System.EventHandler(this.btExcluirPeriodicidade_Click);
            // 
            // btIncluirPeriodicidade
            // 
            this.btIncluirPeriodicidade.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btIncluirPeriodicidade.Image = global::SWS.Properties.Resources.icone_mais;
            this.btIncluirPeriodicidade.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btIncluirPeriodicidade.Location = new System.Drawing.Point(15, 505);
            this.btIncluirPeriodicidade.Name = "btIncluirPeriodicidade";
            this.btIncluirPeriodicidade.Size = new System.Drawing.Size(75, 23);
            this.btIncluirPeriodicidade.TabIndex = 6;
            this.btIncluirPeriodicidade.Text = "Incluir";
            this.btIncluirPeriodicidade.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btIncluirPeriodicidade.UseVisualStyleBackColor = true;
            this.btIncluirPeriodicidade.Click += new System.EventHandler(this.btIncluirPeriodicidade_Click);
            // 
            // frmExamesExtras
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(784, 561);
            this.Name = "frmExamesExtras";
            this.Text = "INCLUIR EXAMES EXTRAS";
            this.spForm.Panel1.ResumeLayout(false);
            this.spForm.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.spForm)).EndInit();
            this.spForm.ResumeLayout(false);
            this.pnlForm.ResumeLayout(false);
            this.pnlForm.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.banner)).EndInit();
            this.flpAcao.ResumeLayout(false);
            this.grbExame.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvExame)).EndInit();
            this.grbPeriodicidade.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvPeriodicidade)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.FlowLayoutPanel flpAcao;
        private System.Windows.Forms.Button btn_fechar;
        private System.Windows.Forms.Button btCliente;
        private System.Windows.Forms.TextBox textCliente;
        private System.Windows.Forms.TextBox lblCliente;
        private System.Windows.Forms.TextBox lblFuncao;
        private System.Windows.Forms.TextBox textFuncao;
        private System.Windows.Forms.Button btFuncao;
        private System.Windows.Forms.Button btExcluirExame;
        private System.Windows.Forms.GroupBox grbExame;
        private System.Windows.Forms.DataGridView dgvExame;
        private System.Windows.Forms.Button btIncluirExame;
        private System.Windows.Forms.Button btExcluirPeriodicidade;
        private System.Windows.Forms.GroupBox grbPeriodicidade;
        private System.Windows.Forms.DataGridView dgvPeriodicidade;
        private System.Windows.Forms.Button btIncluirPeriodicidade;
    }
}