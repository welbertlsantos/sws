﻿using SWS.Entidade;
using SWS.Facade;
using SWS.View.Resources;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace SWS.View
{
    public partial class frmRamoIncluir : frmTemplate
    {
        protected Ramo ramo;

        public Ramo Ramo
        {
            get { return ramo; }
            set { ramo = value; }
        }

        public frmRamoIncluir()
        {
            InitializeComponent();
        }

        protected virtual void btGravar_Click(object sender, EventArgs e)
        {
            try
            {
                if (validaDadosTela())
                {
                    ClienteFacade clienteFacade = ClienteFacade.getInstance();

                    ramo = clienteFacade.insertRamo(new Ramo(null, textDescricao.Text.Trim().ToUpper(), ApplicationConstants.ATIVO));

                    MessageBox.Show("Ramo de atividade gravado com sucesso.", "Confirmação", MessageBoxButtons.OK, MessageBoxIcon.Information);

                    this.Close();

                }

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
        }

        protected void btFechar_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        protected bool validaDadosTela()
        {
            bool retorno = true;

            try
            {
                if (String.IsNullOrEmpty(textDescricao.Text))
                    throw new Exception("Campo descrição é Obritatório.");
            }
            catch (Exception ex)
            {
                retorno = false;
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

            return retorno;

        }
    }
}
