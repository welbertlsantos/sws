﻿namespace SWS.View
{
    partial class frmPpraIncluir
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle17 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle18 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle15 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle16 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle13 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle14 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle11 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle12 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle9 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle10 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle7 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle8 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            this.flpAcao = new System.Windows.Forms.FlowLayoutPanel();
            this.btnNovoEstudo = new System.Windows.Forms.Button();
            this.btnGravar = new System.Windows.Forms.Button();
            this.btnFechar = new System.Windows.Forms.Button();
            this.lblContratante = new System.Windows.Forms.TextBox();
            this.btnIncluirCliente = new System.Windows.Forms.Button();
            this.lblCliente = new System.Windows.Forms.TextBox();
            this.textCliente = new System.Windows.Forms.TextBox();
            this.btnExcluirCliente = new System.Windows.Forms.Button();
            this.lblCodigoEstudo = new System.Windows.Forms.Label();
            this.btnExcluirContratante = new System.Windows.Forms.Button();
            this.btnIncluirContratante = new System.Windows.Forms.Button();
            this.textContratante = new System.Windows.Forms.TextBox();
            this.lblInfoContratante = new System.Windows.Forms.Label();
            this.lblNumeroContrato = new System.Windows.Forms.TextBox();
            this.textContrato = new System.Windows.Forms.TextBox();
            this.lblPeriodo = new System.Windows.Forms.TextBox();
            this.dtInicioContrato = new System.Windows.Forms.DateTimePicker();
            this.dtTerminoContrato = new System.Windows.Forms.DateTimePicker();
            this.lblElaborador = new System.Windows.Forms.TextBox();
            this.btnExcluirElaborador = new System.Windows.Forms.Button();
            this.btnIncluirElaborador = new System.Windows.Forms.Button();
            this.textElaborador = new System.Windows.Forms.TextBox();
            this.lblDataCriação = new System.Windows.Forms.TextBox();
            this.dataVencimento = new System.Windows.Forms.DateTimePicker();
            this.dataCriação = new System.Windows.Forms.DateTimePicker();
            this.lblDataVencimento = new System.Windows.Forms.TextBox();
            this.lblGrauRisco = new System.Windows.Forms.TextBox();
            this.cbGrauRisco = new SWS.ComboBoxWithBorder();
            this.lblIformacaoObra = new System.Windows.Forms.Label();
            this.lblLocalEstudo = new System.Windows.Forms.TextBox();
            this.textLocalEstudo = new System.Windows.Forms.TextBox();
            this.btnLocalEstudo = new System.Windows.Forms.Button();
            this.btnExcluirLocalEstudo = new System.Windows.Forms.Button();
            this.lblPrevisaoFuncionario = new System.Windows.Forms.Label();
            this.dgPrevisao = new System.Windows.Forms.DataGridView();
            this.flpPrevisao = new System.Windows.Forms.FlowLayoutPanel();
            this.btnIncluirPrevisao = new System.Windows.Forms.Button();
            this.btnExcluirPrevisao = new System.Windows.Forms.Button();
            this.lblCnaeCliente = new System.Windows.Forms.TextBox();
            this.textCnae = new System.Windows.Forms.TextBox();
            this.btnExcluirCnae = new System.Windows.Forms.Button();
            this.btnIncluirCnae = new System.Windows.Forms.Button();
            this.button3 = new System.Windows.Forms.Button();
            this.button4 = new System.Windows.Forms.Button();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.textBox2 = new System.Windows.Forms.TextBox();
            this.lblInformacaoGhe = new System.Windows.Forms.Label();
            this.dgvGhe = new System.Windows.Forms.DataGridView();
            this.flpGhe = new System.Windows.Forms.FlowLayoutPanel();
            this.btnIncluirGhe = new System.Windows.Forms.Button();
            this.btnAlterarGhe = new System.Windows.Forms.Button();
            this.btnExcluirGhe = new System.Windows.Forms.Button();
            this.dgvFonte = new System.Windows.Forms.DataGridView();
            this.lblInformacaoFonte = new System.Windows.Forms.Label();
            this.flpFonte = new System.Windows.Forms.FlowLayoutPanel();
            this.btnIncluirFonte = new System.Windows.Forms.Button();
            this.btnExcluirFonte = new System.Windows.Forms.Button();
            this.lblInformacaoAgente = new System.Windows.Forms.Label();
            this.dgvAgente = new System.Windows.Forms.DataGridView();
            this.flpAgente = new System.Windows.Forms.FlowLayoutPanel();
            this.btn_incluirAgente = new System.Windows.Forms.Button();
            this.btn_excluirAgente = new System.Windows.Forms.Button();
            this.lblInformacaoEpi = new System.Windows.Forms.Label();
            this.dgvEpi = new System.Windows.Forms.DataGridView();
            this.flpEpi = new System.Windows.Forms.FlowLayoutPanel();
            this.btnIncluirEpi = new System.Windows.Forms.Button();
            this.btnExcluirEPI = new System.Windows.Forms.Button();
            this.lblInformacaoSetor = new System.Windows.Forms.Label();
            this.dgvSetor = new System.Windows.Forms.DataGridView();
            this.flpSetor = new System.Windows.Forms.FlowLayoutPanel();
            this.btnIncluirSetor = new System.Windows.Forms.Button();
            this.btnExcluirSetor = new System.Windows.Forms.Button();
            this.lblInformacaoFuncao = new System.Windows.Forms.Label();
            this.dgvFuncao = new System.Windows.Forms.DataGridView();
            this.flpFuncao = new System.Windows.Forms.FlowLayoutPanel();
            this.btnIncluirFuncao = new System.Windows.Forms.Button();
            this.btnExcluirFuncao = new System.Windows.Forms.Button();
            this.lblInformacaoCronograma = new System.Windows.Forms.Label();
            this.dgvAtividade = new System.Windows.Forms.DataGridView();
            this.flpAtividade = new System.Windows.Forms.FlowLayoutPanel();
            this.btn_incluirAtividade = new System.Windows.Forms.Button();
            this.btn_alterarAtividade = new System.Windows.Forms.Button();
            this.btn_excluirAtividade = new System.Windows.Forms.Button();
            this.lblComentario = new System.Windows.Forms.Label();
            this.textComentario = new System.Windows.Forms.TextBox();
            this.lblInformacaoAnexo = new System.Windows.Forms.Label();
            this.lblAnexo = new System.Windows.Forms.TextBox();
            this.textAnexo = new System.Windows.Forms.TextBox();
            this.btnIncluirAnexo = new System.Windows.Forms.Button();
            this.dgvAnexo = new System.Windows.Forms.DataGridView();
            ((System.ComponentModel.ISupportInitialize)(this.spForm)).BeginInit();
            this.spForm.Panel1.SuspendLayout();
            this.spForm.Panel2.SuspendLayout();
            this.spForm.SuspendLayout();
            this.pnlForm.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.banner)).BeginInit();
            this.flpAcao.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgPrevisao)).BeginInit();
            this.flpPrevisao.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvGhe)).BeginInit();
            this.flpGhe.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvFonte)).BeginInit();
            this.flpFonte.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvAgente)).BeginInit();
            this.flpAgente.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvEpi)).BeginInit();
            this.flpEpi.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvSetor)).BeginInit();
            this.flpSetor.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvFuncao)).BeginInit();
            this.flpFuncao.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvAtividade)).BeginInit();
            this.flpAtividade.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvAnexo)).BeginInit();
            this.SuspendLayout();
            // 
            // spForm
            // 
            // 
            // spForm.Panel1
            // 
            this.spForm.Panel1.Controls.Add(this.flpAcao);
            // 
            // pnlForm
            // 
            this.pnlForm.Controls.Add(this.dgvAnexo);
            this.pnlForm.Controls.Add(this.btnIncluirAnexo);
            this.pnlForm.Controls.Add(this.textAnexo);
            this.pnlForm.Controls.Add(this.lblAnexo);
            this.pnlForm.Controls.Add(this.lblInformacaoAnexo);
            this.pnlForm.Controls.Add(this.textComentario);
            this.pnlForm.Controls.Add(this.lblComentario);
            this.pnlForm.Controls.Add(this.flpAtividade);
            this.pnlForm.Controls.Add(this.dgvAtividade);
            this.pnlForm.Controls.Add(this.lblInformacaoCronograma);
            this.pnlForm.Controls.Add(this.flpFuncao);
            this.pnlForm.Controls.Add(this.dgvFuncao);
            this.pnlForm.Controls.Add(this.lblInformacaoFuncao);
            this.pnlForm.Controls.Add(this.flpSetor);
            this.pnlForm.Controls.Add(this.dgvSetor);
            this.pnlForm.Controls.Add(this.flpEpi);
            this.pnlForm.Controls.Add(this.dgvEpi);
            this.pnlForm.Controls.Add(this.lblInformacaoSetor);
            this.pnlForm.Controls.Add(this.lblInformacaoEpi);
            this.pnlForm.Controls.Add(this.dgvAgente);
            this.pnlForm.Controls.Add(this.lblInformacaoAgente);
            this.pnlForm.Controls.Add(this.flpAgente);
            this.pnlForm.Controls.Add(this.lblInformacaoFonte);
            this.pnlForm.Controls.Add(this.dgvFonte);
            this.pnlForm.Controls.Add(this.dgvGhe);
            this.pnlForm.Controls.Add(this.flpFonte);
            this.pnlForm.Controls.Add(this.lblInformacaoGhe);
            this.pnlForm.Controls.Add(this.flpGhe);
            this.pnlForm.Controls.Add(this.button3);
            this.pnlForm.Controls.Add(this.button4);
            this.pnlForm.Controls.Add(this.textBox1);
            this.pnlForm.Controls.Add(this.textBox2);
            this.pnlForm.Controls.Add(this.flpPrevisao);
            this.pnlForm.Controls.Add(this.dgPrevisao);
            this.pnlForm.Controls.Add(this.lblPrevisaoFuncionario);
            this.pnlForm.Controls.Add(this.btnIncluirCnae);
            this.pnlForm.Controls.Add(this.btnExcluirCnae);
            this.pnlForm.Controls.Add(this.textCnae);
            this.pnlForm.Controls.Add(this.lblCnaeCliente);
            this.pnlForm.Controls.Add(this.btnExcluirLocalEstudo);
            this.pnlForm.Controls.Add(this.btnLocalEstudo);
            this.pnlForm.Controls.Add(this.textLocalEstudo);
            this.pnlForm.Controls.Add(this.lblLocalEstudo);
            this.pnlForm.Controls.Add(this.lblIformacaoObra);
            this.pnlForm.Controls.Add(this.cbGrauRisco);
            this.pnlForm.Controls.Add(this.lblGrauRisco);
            this.pnlForm.Controls.Add(this.lblDataVencimento);
            this.pnlForm.Controls.Add(this.dataVencimento);
            this.pnlForm.Controls.Add(this.dataCriação);
            this.pnlForm.Controls.Add(this.lblDataCriação);
            this.pnlForm.Controls.Add(this.btnExcluirElaborador);
            this.pnlForm.Controls.Add(this.btnIncluirElaborador);
            this.pnlForm.Controls.Add(this.textElaborador);
            this.pnlForm.Controls.Add(this.lblElaborador);
            this.pnlForm.Controls.Add(this.dtTerminoContrato);
            this.pnlForm.Controls.Add(this.dtInicioContrato);
            this.pnlForm.Controls.Add(this.lblPeriodo);
            this.pnlForm.Controls.Add(this.textContrato);
            this.pnlForm.Controls.Add(this.lblNumeroContrato);
            this.pnlForm.Controls.Add(this.btnIncluirContratante);
            this.pnlForm.Controls.Add(this.btnExcluirContratante);
            this.pnlForm.Controls.Add(this.textContratante);
            this.pnlForm.Controls.Add(this.lblInfoContratante);
            this.pnlForm.Controls.Add(this.lblContratante);
            this.pnlForm.Controls.Add(this.lblCodigoEstudo);
            this.pnlForm.Controls.Add(this.btnExcluirCliente);
            this.pnlForm.Controls.Add(this.btnIncluirCliente);
            this.pnlForm.Controls.Add(this.lblCliente);
            this.pnlForm.Controls.Add(this.textCliente);
            this.pnlForm.Size = new System.Drawing.Size(764, 2363);
            // 
            // flpAcao
            // 
            this.flpAcao.Controls.Add(this.btnNovoEstudo);
            this.flpAcao.Controls.Add(this.btnGravar);
            this.flpAcao.Controls.Add(this.btnFechar);
            this.flpAcao.Dock = System.Windows.Forms.DockStyle.Fill;
            this.flpAcao.Location = new System.Drawing.Point(0, 0);
            this.flpAcao.Name = "flpAcao";
            this.flpAcao.Size = new System.Drawing.Size(784, 35);
            this.flpAcao.TabIndex = 0;
            // 
            // btnNovoEstudo
            // 
            this.btnNovoEstudo.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnNovoEstudo.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnNovoEstudo.Image = global::SWS.Properties.Resources.icone_mais;
            this.btnNovoEstudo.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnNovoEstudo.Location = new System.Drawing.Point(3, 3);
            this.btnNovoEstudo.Name = "btnNovoEstudo";
            this.btnNovoEstudo.Size = new System.Drawing.Size(77, 23);
            this.btnNovoEstudo.TabIndex = 1;
            this.btnNovoEstudo.Text = "&Novo";
            this.btnNovoEstudo.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnNovoEstudo.UseVisualStyleBackColor = true;
            this.btnNovoEstudo.Click += new System.EventHandler(this.btnNovoEstudo_Click);
            // 
            // btnGravar
            // 
            this.btnGravar.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnGravar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnGravar.Image = global::SWS.Properties.Resources.Gravar;
            this.btnGravar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnGravar.Location = new System.Drawing.Point(86, 3);
            this.btnGravar.Name = "btnGravar";
            this.btnGravar.Size = new System.Drawing.Size(77, 23);
            this.btnGravar.TabIndex = 27;
            this.btnGravar.TabStop = false;
            this.btnGravar.Text = "&Gravar";
            this.btnGravar.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnGravar.UseVisualStyleBackColor = true;
            // 
            // btnFechar
            // 
            this.btnFechar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnFechar.Image = global::SWS.Properties.Resources.close;
            this.btnFechar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnFechar.Location = new System.Drawing.Point(169, 3);
            this.btnFechar.Name = "btnFechar";
            this.btnFechar.Size = new System.Drawing.Size(77, 23);
            this.btnFechar.TabIndex = 26;
            this.btnFechar.TabStop = false;
            this.btnFechar.Text = "&Fechar";
            this.btnFechar.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnFechar.UseVisualStyleBackColor = true;
            this.btnFechar.Click += new System.EventHandler(this.btn_fechar_Click);
            // 
            // lblContratante
            // 
            this.lblContratante.BackColor = System.Drawing.Color.LightGray;
            this.lblContratante.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblContratante.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblContratante.Location = new System.Drawing.Point(3, 181);
            this.lblContratante.MaxLength = 15;
            this.lblContratante.Name = "lblContratante";
            this.lblContratante.ReadOnly = true;
            this.lblContratante.Size = new System.Drawing.Size(147, 21);
            this.lblContratante.TabIndex = 1;
            this.lblContratante.TabStop = false;
            this.lblContratante.Text = "Contratante";
            // 
            // btnIncluirCliente
            // 
            this.btnIncluirCliente.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnIncluirCliente.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnIncluirCliente.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnIncluirCliente.Image = global::SWS.Properties.Resources.busca;
            this.btnIncluirCliente.Location = new System.Drawing.Point(694, 31);
            this.btnIncluirCliente.Name = "btnIncluirCliente";
            this.btnIncluirCliente.Size = new System.Drawing.Size(34, 21);
            this.btnIncluirCliente.TabIndex = 2;
            this.btnIncluirCliente.UseVisualStyleBackColor = true;
            this.btnIncluirCliente.Click += new System.EventHandler(this.btnIncluirCliente_Click);
            // 
            // lblCliente
            // 
            this.lblCliente.BackColor = System.Drawing.Color.LightGray;
            this.lblCliente.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblCliente.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCliente.Location = new System.Drawing.Point(3, 31);
            this.lblCliente.MaxLength = 15;
            this.lblCliente.Name = "lblCliente";
            this.lblCliente.ReadOnly = true;
            this.lblCliente.Size = new System.Drawing.Size(147, 21);
            this.lblCliente.TabIndex = 2;
            this.lblCliente.TabStop = false;
            this.lblCliente.Text = "Cliente";
            // 
            // textCliente
            // 
            this.textCliente.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.textCliente.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.textCliente.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textCliente.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.textCliente.Enabled = false;
            this.textCliente.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textCliente.ForeColor = System.Drawing.Color.Black;
            this.textCliente.Location = new System.Drawing.Point(149, 31);
            this.textCliente.MaxLength = 100;
            this.textCliente.Name = "textCliente";
            this.textCliente.ReadOnly = true;
            this.textCliente.Size = new System.Drawing.Size(547, 21);
            this.textCliente.TabIndex = 3;
            this.textCliente.TabStop = false;
            // 
            // btnExcluirCliente
            // 
            this.btnExcluirCliente.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnExcluirCliente.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnExcluirCliente.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnExcluirCliente.Image = global::SWS.Properties.Resources.close;
            this.btnExcluirCliente.Location = new System.Drawing.Point(727, 31);
            this.btnExcluirCliente.Name = "btnExcluirCliente";
            this.btnExcluirCliente.Size = new System.Drawing.Size(34, 21);
            this.btnExcluirCliente.TabIndex = 5;
            this.btnExcluirCliente.UseVisualStyleBackColor = true;
            this.btnExcluirCliente.Click += new System.EventHandler(this.btnExcluirCliente_Click);
            // 
            // lblCodigoEstudo
            // 
            this.lblCodigoEstudo.AutoSize = true;
            this.lblCodigoEstudo.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCodigoEstudo.ForeColor = System.Drawing.Color.Red;
            this.lblCodigoEstudo.Location = new System.Drawing.Point(533, 5);
            this.lblCodigoEstudo.Name = "lblCodigoEstudo";
            this.lblCodigoEstudo.Size = new System.Drawing.Size(0, 20);
            this.lblCodigoEstudo.TabIndex = 6;
            this.lblCodigoEstudo.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // btnExcluirContratante
            // 
            this.btnExcluirContratante.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnExcluirContratante.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnExcluirContratante.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnExcluirContratante.Image = global::SWS.Properties.Resources.close;
            this.btnExcluirContratante.Location = new System.Drawing.Point(727, 181);
            this.btnExcluirContratante.Name = "btnExcluirContratante";
            this.btnExcluirContratante.Size = new System.Drawing.Size(34, 21);
            this.btnExcluirContratante.TabIndex = 9;
            this.btnExcluirContratante.UseVisualStyleBackColor = true;
            // 
            // btnIncluirContratante
            // 
            this.btnIncluirContratante.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnIncluirContratante.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnIncluirContratante.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnIncluirContratante.Image = global::SWS.Properties.Resources.busca;
            this.btnIncluirContratante.Location = new System.Drawing.Point(694, 181);
            this.btnIncluirContratante.Name = "btnIncluirContratante";
            this.btnIncluirContratante.Size = new System.Drawing.Size(34, 21);
            this.btnIncluirContratante.TabIndex = 8;
            this.btnIncluirContratante.UseVisualStyleBackColor = true;
            // 
            // textContratante
            // 
            this.textContratante.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.textContratante.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.textContratante.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textContratante.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.textContratante.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textContratante.ForeColor = System.Drawing.Color.Black;
            this.textContratante.Location = new System.Drawing.Point(149, 181);
            this.textContratante.MaxLength = 100;
            this.textContratante.Name = "textContratante";
            this.textContratante.ReadOnly = true;
            this.textContratante.Size = new System.Drawing.Size(547, 21);
            this.textContratante.TabIndex = 7;
            this.textContratante.TabStop = false;
            // 
            // lblInfoContratante
            // 
            this.lblInfoContratante.AutoSize = true;
            this.lblInfoContratante.Location = new System.Drawing.Point(3, 165);
            this.lblInfoContratante.Name = "lblInfoContratante";
            this.lblInfoContratante.Size = new System.Drawing.Size(138, 13);
            this.lblInfoContratante.TabIndex = 11;
            this.lblInfoContratante.Text = "Informações do Contratante";
            // 
            // lblNumeroContrato
            // 
            this.lblNumeroContrato.BackColor = System.Drawing.Color.LightGray;
            this.lblNumeroContrato.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblNumeroContrato.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNumeroContrato.Location = new System.Drawing.Point(3, 221);
            this.lblNumeroContrato.MaxLength = 15;
            this.lblNumeroContrato.Name = "lblNumeroContrato";
            this.lblNumeroContrato.ReadOnly = true;
            this.lblNumeroContrato.Size = new System.Drawing.Size(147, 21);
            this.lblNumeroContrato.TabIndex = 12;
            this.lblNumeroContrato.TabStop = false;
            this.lblNumeroContrato.Text = "Número de Contrato";
            // 
            // textContrato
            // 
            this.textContrato.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.textContrato.BackColor = System.Drawing.Color.White;
            this.textContrato.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textContrato.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.textContrato.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textContrato.ForeColor = System.Drawing.Color.Black;
            this.textContrato.Location = new System.Drawing.Point(149, 221);
            this.textContrato.MaxLength = 15;
            this.textContrato.Name = "textContrato";
            this.textContrato.Size = new System.Drawing.Size(612, 21);
            this.textContrato.TabIndex = 13;
            // 
            // lblPeriodo
            // 
            this.lblPeriodo.BackColor = System.Drawing.Color.LightGray;
            this.lblPeriodo.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblPeriodo.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPeriodo.Location = new System.Drawing.Point(3, 241);
            this.lblPeriodo.MaxLength = 15;
            this.lblPeriodo.Name = "lblPeriodo";
            this.lblPeriodo.ReadOnly = true;
            this.lblPeriodo.Size = new System.Drawing.Size(147, 21);
            this.lblPeriodo.TabIndex = 14;
            this.lblPeriodo.TabStop = false;
            this.lblPeriodo.Text = "Período de Contrato";
            // 
            // dtInicioContrato
            // 
            this.dtInicioContrato.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dtInicioContrato.Checked = false;
            this.dtInicioContrato.CustomFormat = "MM/yyyy";
            this.dtInicioContrato.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtInicioContrato.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtInicioContrato.Location = new System.Drawing.Point(149, 241);
            this.dtInicioContrato.Name = "dtInicioContrato";
            this.dtInicioContrato.ShowCheckBox = true;
            this.dtInicioContrato.Size = new System.Drawing.Size(111, 21);
            this.dtInicioContrato.TabIndex = 10;
            // 
            // dtTerminoContrato
            // 
            this.dtTerminoContrato.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dtTerminoContrato.Checked = false;
            this.dtTerminoContrato.CustomFormat = "MM/yyyy";
            this.dtTerminoContrato.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtTerminoContrato.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtTerminoContrato.Location = new System.Drawing.Point(257, 241);
            this.dtTerminoContrato.Name = "dtTerminoContrato";
            this.dtTerminoContrato.ShowCheckBox = true;
            this.dtTerminoContrato.Size = new System.Drawing.Size(111, 21);
            this.dtTerminoContrato.TabIndex = 11;
            // 
            // lblElaborador
            // 
            this.lblElaborador.BackColor = System.Drawing.Color.LightGray;
            this.lblElaborador.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblElaborador.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblElaborador.Location = new System.Drawing.Point(3, 77);
            this.lblElaborador.MaxLength = 15;
            this.lblElaborador.Name = "lblElaborador";
            this.lblElaborador.ReadOnly = true;
            this.lblElaborador.Size = new System.Drawing.Size(147, 21);
            this.lblElaborador.TabIndex = 17;
            this.lblElaborador.TabStop = false;
            this.lblElaborador.Text = "Elaborador";
            // 
            // btnExcluirElaborador
            // 
            this.btnExcluirElaborador.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnExcluirElaborador.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnExcluirElaborador.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnExcluirElaborador.Image = global::SWS.Properties.Resources.close;
            this.btnExcluirElaborador.Location = new System.Drawing.Point(727, 77);
            this.btnExcluirElaborador.Name = "btnExcluirElaborador";
            this.btnExcluirElaborador.Size = new System.Drawing.Size(34, 21);
            this.btnExcluirElaborador.TabIndex = 20;
            this.btnExcluirElaborador.UseVisualStyleBackColor = true;
            // 
            // btnIncluirElaborador
            // 
            this.btnIncluirElaborador.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnIncluirElaborador.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnIncluirElaborador.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnIncluirElaborador.Image = global::SWS.Properties.Resources.busca;
            this.btnIncluirElaborador.Location = new System.Drawing.Point(694, 77);
            this.btnIncluirElaborador.Name = "btnIncluirElaborador";
            this.btnIncluirElaborador.Size = new System.Drawing.Size(34, 21);
            this.btnIncluirElaborador.TabIndex = 4;
            this.btnIncluirElaborador.UseVisualStyleBackColor = true;
            // 
            // textElaborador
            // 
            this.textElaborador.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.textElaborador.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.textElaborador.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textElaborador.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.textElaborador.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textElaborador.ForeColor = System.Drawing.Color.Black;
            this.textElaborador.Location = new System.Drawing.Point(149, 77);
            this.textElaborador.MaxLength = 100;
            this.textElaborador.Name = "textElaborador";
            this.textElaborador.ReadOnly = true;
            this.textElaborador.Size = new System.Drawing.Size(547, 21);
            this.textElaborador.TabIndex = 18;
            this.textElaborador.TabStop = false;
            // 
            // lblDataCriação
            // 
            this.lblDataCriação.BackColor = System.Drawing.Color.LightGray;
            this.lblDataCriação.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblDataCriação.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDataCriação.Location = new System.Drawing.Point(3, 97);
            this.lblDataCriação.MaxLength = 15;
            this.lblDataCriação.Name = "lblDataCriação";
            this.lblDataCriação.ReadOnly = true;
            this.lblDataCriação.Size = new System.Drawing.Size(147, 21);
            this.lblDataCriação.TabIndex = 21;
            this.lblDataCriação.TabStop = false;
            this.lblDataCriação.Text = "Data de Elaboração";
            // 
            // dataVencimento
            // 
            this.dataVencimento.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dataVencimento.Checked = false;
            this.dataVencimento.CustomFormat = "dd/MM/yyyy";
            this.dataVencimento.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dataVencimento.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dataVencimento.Location = new System.Drawing.Point(149, 117);
            this.dataVencimento.Name = "dataVencimento";
            this.dataVencimento.ShowCheckBox = true;
            this.dataVencimento.Size = new System.Drawing.Size(111, 21);
            this.dataVencimento.TabIndex = 6;
            // 
            // dataCriação
            // 
            this.dataCriação.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dataCriação.Checked = false;
            this.dataCriação.CustomFormat = "dd/MM/yyyy";
            this.dataCriação.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dataCriação.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dataCriação.Location = new System.Drawing.Point(149, 97);
            this.dataCriação.Name = "dataCriação";
            this.dataCriação.ShowCheckBox = true;
            this.dataCriação.Size = new System.Drawing.Size(111, 21);
            this.dataCriação.TabIndex = 5;
            // 
            // lblDataVencimento
            // 
            this.lblDataVencimento.BackColor = System.Drawing.Color.LightGray;
            this.lblDataVencimento.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblDataVencimento.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDataVencimento.Location = new System.Drawing.Point(3, 117);
            this.lblDataVencimento.MaxLength = 15;
            this.lblDataVencimento.Name = "lblDataVencimento";
            this.lblDataVencimento.ReadOnly = true;
            this.lblDataVencimento.Size = new System.Drawing.Size(147, 21);
            this.lblDataVencimento.TabIndex = 24;
            this.lblDataVencimento.TabStop = false;
            this.lblDataVencimento.Text = "Data de Vencimento";
            // 
            // lblGrauRisco
            // 
            this.lblGrauRisco.BackColor = System.Drawing.Color.LightGray;
            this.lblGrauRisco.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblGrauRisco.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblGrauRisco.Location = new System.Drawing.Point(3, 137);
            this.lblGrauRisco.MaxLength = 15;
            this.lblGrauRisco.Name = "lblGrauRisco";
            this.lblGrauRisco.ReadOnly = true;
            this.lblGrauRisco.Size = new System.Drawing.Size(147, 21);
            this.lblGrauRisco.TabIndex = 25;
            this.lblGrauRisco.TabStop = false;
            this.lblGrauRisco.Text = "Grau de Risco";
            // 
            // cbGrauRisco
            // 
            this.cbGrauRisco.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.cbGrauRisco.BackColor = System.Drawing.Color.LightGray;
            this.cbGrauRisco.BorderColor = System.Drawing.Color.DimGray;
            this.cbGrauRisco.BorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid;
            this.cbGrauRisco.FormattingEnabled = true;
            this.cbGrauRisco.Location = new System.Drawing.Point(149, 137);
            this.cbGrauRisco.Name = "cbGrauRisco";
            this.cbGrauRisco.Size = new System.Drawing.Size(111, 21);
            this.cbGrauRisco.TabIndex = 7;
            // 
            // lblIformacaoObra
            // 
            this.lblIformacaoObra.AutoSize = true;
            this.lblIformacaoObra.Location = new System.Drawing.Point(3, 268);
            this.lblIformacaoObra.Name = "lblIformacaoObra";
            this.lblIformacaoObra.Size = new System.Drawing.Size(242, 13);
            this.lblIformacaoObra.TabIndex = 27;
            this.lblIformacaoObra.Text = "Informações do local onde será aplicado o estudo";
            // 
            // lblLocalEstudo
            // 
            this.lblLocalEstudo.BackColor = System.Drawing.Color.LightGray;
            this.lblLocalEstudo.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblLocalEstudo.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblLocalEstudo.Location = new System.Drawing.Point(3, 284);
            this.lblLocalEstudo.MaxLength = 15;
            this.lblLocalEstudo.Name = "lblLocalEstudo";
            this.lblLocalEstudo.ReadOnly = true;
            this.lblLocalEstudo.Size = new System.Drawing.Size(147, 21);
            this.lblLocalEstudo.TabIndex = 28;
            this.lblLocalEstudo.TabStop = false;
            this.lblLocalEstudo.Text = "Incluir local Estudo";
            // 
            // textLocalEstudo
            // 
            this.textLocalEstudo.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.textLocalEstudo.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.textLocalEstudo.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textLocalEstudo.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.textLocalEstudo.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textLocalEstudo.ForeColor = System.Drawing.Color.Black;
            this.textLocalEstudo.Location = new System.Drawing.Point(149, 284);
            this.textLocalEstudo.MaxLength = 500;
            this.textLocalEstudo.Name = "textLocalEstudo";
            this.textLocalEstudo.ReadOnly = true;
            this.textLocalEstudo.Size = new System.Drawing.Size(547, 21);
            this.textLocalEstudo.TabIndex = 29;
            this.textLocalEstudo.TabStop = false;
            // 
            // btnLocalEstudo
            // 
            this.btnLocalEstudo.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnLocalEstudo.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnLocalEstudo.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnLocalEstudo.Image = global::SWS.Properties.Resources.busca;
            this.btnLocalEstudo.Location = new System.Drawing.Point(694, 284);
            this.btnLocalEstudo.Name = "btnLocalEstudo";
            this.btnLocalEstudo.Size = new System.Drawing.Size(34, 21);
            this.btnLocalEstudo.TabIndex = 12;
            this.btnLocalEstudo.UseVisualStyleBackColor = true;
            // 
            // btnExcluirLocalEstudo
            // 
            this.btnExcluirLocalEstudo.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnExcluirLocalEstudo.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnExcluirLocalEstudo.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnExcluirLocalEstudo.Image = global::SWS.Properties.Resources.close;
            this.btnExcluirLocalEstudo.Location = new System.Drawing.Point(727, 284);
            this.btnExcluirLocalEstudo.Name = "btnExcluirLocalEstudo";
            this.btnExcluirLocalEstudo.Size = new System.Drawing.Size(34, 21);
            this.btnExcluirLocalEstudo.TabIndex = 31;
            this.btnExcluirLocalEstudo.UseVisualStyleBackColor = true;
            // 
            // lblPrevisaoFuncionario
            // 
            this.lblPrevisaoFuncionario.AutoSize = true;
            this.lblPrevisaoFuncionario.Location = new System.Drawing.Point(3, 308);
            this.lblPrevisaoFuncionario.Name = "lblPrevisaoFuncionario";
            this.lblPrevisaoFuncionario.Size = new System.Drawing.Size(212, 13);
            this.lblPrevisaoFuncionario.TabIndex = 32;
            this.lblPrevisaoFuncionario.Text = "Informações sobre previsão de funcionários";
            // 
            // dgPrevisao
            // 
            this.dgPrevisao.AllowUserToAddRows = false;
            this.dgPrevisao.AllowUserToDeleteRows = false;
            this.dgPrevisao.AllowUserToOrderColumns = true;
            this.dgPrevisao.AllowUserToResizeRows = false;
            dataGridViewCellStyle17.BackColor = System.Drawing.Color.Gainsboro;
            this.dgPrevisao.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle17;
            this.dgPrevisao.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.DisplayedCells;
            this.dgPrevisao.BackgroundColor = System.Drawing.Color.White;
            this.dgPrevisao.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dataGridViewCellStyle18.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle18.BackColor = System.Drawing.Color.LightGray;
            dataGridViewCellStyle18.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle18.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle18.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle18.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle18.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgPrevisao.DefaultCellStyle = dataGridViewCellStyle18;
            this.dgPrevisao.Location = new System.Drawing.Point(2, 324);
            this.dgPrevisao.Name = "dgPrevisao";
            this.dgPrevisao.RowHeadersWidth = 30;
            this.dgPrevisao.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.CellSelect;
            this.dgPrevisao.Size = new System.Drawing.Size(759, 147);
            this.dgPrevisao.TabIndex = 33;
            // 
            // flpPrevisao
            // 
            this.flpPrevisao.Controls.Add(this.btnIncluirPrevisao);
            this.flpPrevisao.Controls.Add(this.btnExcluirPrevisao);
            this.flpPrevisao.Location = new System.Drawing.Point(2, 477);
            this.flpPrevisao.Name = "flpPrevisao";
            this.flpPrevisao.Size = new System.Drawing.Size(758, 32);
            this.flpPrevisao.TabIndex = 34;
            // 
            // btnIncluirPrevisao
            // 
            this.btnIncluirPrevisao.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnIncluirPrevisao.Image = global::SWS.Properties.Resources.icone_mais;
            this.btnIncluirPrevisao.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnIncluirPrevisao.Location = new System.Drawing.Point(3, 3);
            this.btnIncluirPrevisao.Name = "btnIncluirPrevisao";
            this.btnIncluirPrevisao.Size = new System.Drawing.Size(75, 23);
            this.btnIncluirPrevisao.TabIndex = 13;
            this.btnIncluirPrevisao.Text = "Incluir";
            this.btnIncluirPrevisao.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnIncluirPrevisao.UseVisualStyleBackColor = true;
            // 
            // btnExcluirPrevisao
            // 
            this.btnExcluirPrevisao.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnExcluirPrevisao.Image = global::SWS.Properties.Resources.lixeira;
            this.btnExcluirPrevisao.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnExcluirPrevisao.Location = new System.Drawing.Point(84, 3);
            this.btnExcluirPrevisao.Name = "btnExcluirPrevisao";
            this.btnExcluirPrevisao.Size = new System.Drawing.Size(75, 23);
            this.btnExcluirPrevisao.TabIndex = 4;
            this.btnExcluirPrevisao.TabStop = false;
            this.btnExcluirPrevisao.Text = "Excluir";
            this.btnExcluirPrevisao.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnExcluirPrevisao.UseVisualStyleBackColor = true;
            // 
            // lblCnaeCliente
            // 
            this.lblCnaeCliente.BackColor = System.Drawing.Color.LightGray;
            this.lblCnaeCliente.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblCnaeCliente.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCnaeCliente.Location = new System.Drawing.Point(3, 50);
            this.lblCnaeCliente.MaxLength = 15;
            this.lblCnaeCliente.Name = "lblCnaeCliente";
            this.lblCnaeCliente.ReadOnly = true;
            this.lblCnaeCliente.Size = new System.Drawing.Size(147, 21);
            this.lblCnaeCliente.TabIndex = 35;
            this.lblCnaeCliente.TabStop = false;
            this.lblCnaeCliente.Text = "CNAE utilizado";
            // 
            // textCnae
            // 
            this.textCnae.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.textCnae.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.textCnae.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textCnae.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.textCnae.Enabled = false;
            this.textCnae.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textCnae.ForeColor = System.Drawing.Color.Black;
            this.textCnae.Location = new System.Drawing.Point(149, 50);
            this.textCnae.MaxLength = 100;
            this.textCnae.Name = "textCnae";
            this.textCnae.ReadOnly = true;
            this.textCnae.Size = new System.Drawing.Size(547, 21);
            this.textCnae.TabIndex = 36;
            this.textCnae.TabStop = false;
            // 
            // btnExcluirCnae
            // 
            this.btnExcluirCnae.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnExcluirCnae.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnExcluirCnae.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnExcluirCnae.Image = global::SWS.Properties.Resources.close;
            this.btnExcluirCnae.Location = new System.Drawing.Point(727, 50);
            this.btnExcluirCnae.Name = "btnExcluirCnae";
            this.btnExcluirCnae.Size = new System.Drawing.Size(34, 21);
            this.btnExcluirCnae.TabIndex = 38;
            this.btnExcluirCnae.UseVisualStyleBackColor = true;
            this.btnExcluirCnae.Click += new System.EventHandler(this.btnExcluirCnae_Click);
            // 
            // btnIncluirCnae
            // 
            this.btnIncluirCnae.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnIncluirCnae.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnIncluirCnae.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnIncluirCnae.Image = global::SWS.Properties.Resources.busca;
            this.btnIncluirCnae.Location = new System.Drawing.Point(694, 50);
            this.btnIncluirCnae.Name = "btnIncluirCnae";
            this.btnIncluirCnae.Size = new System.Drawing.Size(34, 21);
            this.btnIncluirCnae.TabIndex = 3;
            this.btnIncluirCnae.UseVisualStyleBackColor = true;
            this.btnIncluirCnae.Click += new System.EventHandler(this.btnIncluirCnae_Click);
            // 
            // button3
            // 
            this.button3.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.button3.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button3.Image = global::SWS.Properties.Resources.busca;
            this.button3.Location = new System.Drawing.Point(694, 201);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(34, 21);
            this.button3.TabIndex = 9;
            this.button3.UseVisualStyleBackColor = true;
            // 
            // button4
            // 
            this.button4.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.button4.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button4.Image = global::SWS.Properties.Resources.close;
            this.button4.Location = new System.Drawing.Point(727, 201);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(34, 21);
            this.button4.TabIndex = 42;
            this.button4.UseVisualStyleBackColor = true;
            // 
            // textBox1
            // 
            this.textBox1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.textBox1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.textBox1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox1.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.textBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox1.ForeColor = System.Drawing.Color.Black;
            this.textBox1.Location = new System.Drawing.Point(149, 201);
            this.textBox1.MaxLength = 100;
            this.textBox1.Name = "textBox1";
            this.textBox1.ReadOnly = true;
            this.textBox1.Size = new System.Drawing.Size(547, 21);
            this.textBox1.TabIndex = 40;
            this.textBox1.TabStop = false;
            // 
            // textBox2
            // 
            this.textBox2.BackColor = System.Drawing.Color.LightGray;
            this.textBox2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox2.Location = new System.Drawing.Point(3, 201);
            this.textBox2.MaxLength = 15;
            this.textBox2.Name = "textBox2";
            this.textBox2.ReadOnly = true;
            this.textBox2.Size = new System.Drawing.Size(147, 21);
            this.textBox2.TabIndex = 39;
            this.textBox2.TabStop = false;
            this.textBox2.Text = "CNAE utilizado";
            // 
            // lblInformacaoGhe
            // 
            this.lblInformacaoGhe.AutoSize = true;
            this.lblInformacaoGhe.Location = new System.Drawing.Point(3, 512);
            this.lblInformacaoGhe.Name = "lblInformacaoGhe";
            this.lblInformacaoGhe.Size = new System.Drawing.Size(129, 13);
            this.lblInformacaoGhe.TabIndex = 43;
            this.lblInformacaoGhe.Text = "Informações sobre o GHE";
            // 
            // dgvGhe
            // 
            this.dgvGhe.AllowUserToAddRows = false;
            this.dgvGhe.AllowUserToDeleteRows = false;
            dataGridViewCellStyle15.BackColor = System.Drawing.Color.Gainsboro;
            this.dgvGhe.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle15;
            this.dgvGhe.BackgroundColor = System.Drawing.Color.White;
            this.dgvGhe.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dataGridViewCellStyle16.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle16.BackColor = System.Drawing.Color.LightGray;
            dataGridViewCellStyle16.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle16.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle16.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle16.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle16.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvGhe.DefaultCellStyle = dataGridViewCellStyle16;
            this.dgvGhe.Location = new System.Drawing.Point(2, 528);
            this.dgvGhe.MultiSelect = false;
            this.dgvGhe.Name = "dgvGhe";
            this.dgvGhe.ReadOnly = true;
            this.dgvGhe.Size = new System.Drawing.Size(759, 157);
            this.dgvGhe.TabIndex = 44;
            // 
            // flpGhe
            // 
            this.flpGhe.Controls.Add(this.btnIncluirGhe);
            this.flpGhe.Controls.Add(this.btnAlterarGhe);
            this.flpGhe.Controls.Add(this.btnExcluirGhe);
            this.flpGhe.Location = new System.Drawing.Point(2, 691);
            this.flpGhe.Name = "flpGhe";
            this.flpGhe.Size = new System.Drawing.Size(758, 32);
            this.flpGhe.TabIndex = 35;
            // 
            // btnIncluirGhe
            // 
            this.btnIncluirGhe.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnIncluirGhe.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnIncluirGhe.Image = global::SWS.Properties.Resources.icone_mais;
            this.btnIncluirGhe.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnIncluirGhe.Location = new System.Drawing.Point(3, 3);
            this.btnIncluirGhe.Name = "btnIncluirGhe";
            this.btnIncluirGhe.Size = new System.Drawing.Size(77, 23);
            this.btnIncluirGhe.TabIndex = 14;
            this.btnIncluirGhe.Text = "&Incluir";
            this.btnIncluirGhe.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnIncluirGhe.UseVisualStyleBackColor = true;
            // 
            // btnAlterarGhe
            // 
            this.btnAlterarGhe.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnAlterarGhe.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnAlterarGhe.Image = global::SWS.Properties.Resources.Alterar;
            this.btnAlterarGhe.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnAlterarGhe.Location = new System.Drawing.Point(86, 3);
            this.btnAlterarGhe.Name = "btnAlterarGhe";
            this.btnAlterarGhe.Size = new System.Drawing.Size(77, 23);
            this.btnAlterarGhe.TabIndex = 15;
            this.btnAlterarGhe.Text = "&Alterar";
            this.btnAlterarGhe.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnAlterarGhe.UseVisualStyleBackColor = true;
            // 
            // btnExcluirGhe
            // 
            this.btnExcluirGhe.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnExcluirGhe.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnExcluirGhe.Image = global::SWS.Properties.Resources.lixeira;
            this.btnExcluirGhe.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnExcluirGhe.Location = new System.Drawing.Point(169, 3);
            this.btnExcluirGhe.Name = "btnExcluirGhe";
            this.btnExcluirGhe.Size = new System.Drawing.Size(77, 23);
            this.btnExcluirGhe.TabIndex = 6;
            this.btnExcluirGhe.TabStop = false;
            this.btnExcluirGhe.Text = "&Excluir";
            this.btnExcluirGhe.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnExcluirGhe.UseVisualStyleBackColor = true;
            // 
            // dgvFonte
            // 
            this.dgvFonte.AllowUserToAddRows = false;
            this.dgvFonte.AllowUserToDeleteRows = false;
            this.dgvFonte.AllowUserToOrderColumns = true;
            this.dgvFonte.AllowUserToResizeRows = false;
            dataGridViewCellStyle13.BackColor = System.Drawing.Color.Gainsboro;
            this.dgvFonte.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle13;
            this.dgvFonte.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this.dgvFonte.BackgroundColor = System.Drawing.Color.White;
            this.dgvFonte.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dataGridViewCellStyle14.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle14.BackColor = System.Drawing.Color.LightGray;
            dataGridViewCellStyle14.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle14.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle14.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle14.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle14.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvFonte.DefaultCellStyle = dataGridViewCellStyle14;
            this.dgvFonte.Location = new System.Drawing.Point(2, 741);
            this.dgvFonte.Name = "dgvFonte";
            this.dgvFonte.ReadOnly = true;
            this.dgvFonte.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvFonte.Size = new System.Drawing.Size(759, 157);
            this.dgvFonte.TabIndex = 45;
            // 
            // lblInformacaoFonte
            // 
            this.lblInformacaoFonte.AutoSize = true;
            this.lblInformacaoFonte.Location = new System.Drawing.Point(3, 726);
            this.lblInformacaoFonte.Name = "lblInformacaoFonte";
            this.lblInformacaoFonte.Size = new System.Drawing.Size(143, 13);
            this.lblInformacaoFonte.TabIndex = 46;
            this.lblInformacaoFonte.Text = "Informações sobre as Fontes";
            // 
            // flpFonte
            // 
            this.flpFonte.Controls.Add(this.btnIncluirFonte);
            this.flpFonte.Controls.Add(this.btnExcluirFonte);
            this.flpFonte.Location = new System.Drawing.Point(2, 901);
            this.flpFonte.Name = "flpFonte";
            this.flpFonte.Size = new System.Drawing.Size(758, 32);
            this.flpFonte.TabIndex = 36;
            // 
            // btnIncluirFonte
            // 
            this.btnIncluirFonte.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnIncluirFonte.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnIncluirFonte.Image = global::SWS.Properties.Resources.icone_mais;
            this.btnIncluirFonte.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnIncluirFonte.Location = new System.Drawing.Point(3, 3);
            this.btnIncluirFonte.Name = "btnIncluirFonte";
            this.btnIncluirFonte.Size = new System.Drawing.Size(77, 23);
            this.btnIncluirFonte.TabIndex = 16;
            this.btnIncluirFonte.Text = "&Incluir";
            this.btnIncluirFonte.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnIncluirFonte.UseVisualStyleBackColor = true;
            // 
            // btnExcluirFonte
            // 
            this.btnExcluirFonte.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnExcluirFonte.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnExcluirFonte.Image = global::SWS.Properties.Resources.lixeira;
            this.btnExcluirFonte.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnExcluirFonte.Location = new System.Drawing.Point(86, 3);
            this.btnExcluirFonte.Name = "btnExcluirFonte";
            this.btnExcluirFonte.Size = new System.Drawing.Size(77, 23);
            this.btnExcluirFonte.TabIndex = 5;
            this.btnExcluirFonte.TabStop = false;
            this.btnExcluirFonte.Text = "&Excluir";
            this.btnExcluirFonte.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnExcluirFonte.UseVisualStyleBackColor = true;
            // 
            // lblInformacaoAgente
            // 
            this.lblInformacaoAgente.AutoSize = true;
            this.lblInformacaoAgente.Location = new System.Drawing.Point(3, 939);
            this.lblInformacaoAgente.Name = "lblInformacaoAgente";
            this.lblInformacaoAgente.Size = new System.Drawing.Size(150, 13);
            this.lblInformacaoAgente.TabIndex = 47;
            this.lblInformacaoAgente.Text = "Informações sobre os Agentes";
            // 
            // dgvAgente
            // 
            this.dgvAgente.AllowUserToAddRows = false;
            this.dgvAgente.AllowUserToDeleteRows = false;
            this.dgvAgente.AllowUserToOrderColumns = true;
            this.dgvAgente.AllowUserToResizeRows = false;
            dataGridViewCellStyle11.BackColor = System.Drawing.Color.Gainsboro;
            this.dgvAgente.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle11;
            this.dgvAgente.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.DisplayedCells;
            this.dgvAgente.BackgroundColor = System.Drawing.Color.White;
            this.dgvAgente.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dataGridViewCellStyle12.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle12.BackColor = System.Drawing.Color.LightGray;
            dataGridViewCellStyle12.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle12.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle12.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle12.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle12.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvAgente.DefaultCellStyle = dataGridViewCellStyle12;
            this.dgvAgente.Location = new System.Drawing.Point(2, 958);
            this.dgvAgente.Name = "dgvAgente";
            this.dgvAgente.ReadOnly = true;
            this.dgvAgente.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvAgente.Size = new System.Drawing.Size(759, 157);
            this.dgvAgente.TabIndex = 48;
            // 
            // flpAgente
            // 
            this.flpAgente.Controls.Add(this.btn_incluirAgente);
            this.flpAgente.Controls.Add(this.btn_excluirAgente);
            this.flpAgente.Location = new System.Drawing.Point(2, 1120);
            this.flpAgente.Name = "flpAgente";
            this.flpAgente.Size = new System.Drawing.Size(758, 32);
            this.flpAgente.TabIndex = 37;
            // 
            // btn_incluirAgente
            // 
            this.btn_incluirAgente.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btn_incluirAgente.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_incluirAgente.Image = global::SWS.Properties.Resources.icone_mais;
            this.btn_incluirAgente.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btn_incluirAgente.Location = new System.Drawing.Point(3, 3);
            this.btn_incluirAgente.Name = "btn_incluirAgente";
            this.btn_incluirAgente.Size = new System.Drawing.Size(77, 23);
            this.btn_incluirAgente.TabIndex = 17;
            this.btn_incluirAgente.Text = "&Incluir";
            this.btn_incluirAgente.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btn_incluirAgente.UseVisualStyleBackColor = true;
            // 
            // btn_excluirAgente
            // 
            this.btn_excluirAgente.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btn_excluirAgente.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_excluirAgente.Image = global::SWS.Properties.Resources.lixeira;
            this.btn_excluirAgente.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btn_excluirAgente.Location = new System.Drawing.Point(86, 3);
            this.btn_excluirAgente.Name = "btn_excluirAgente";
            this.btn_excluirAgente.Size = new System.Drawing.Size(77, 23);
            this.btn_excluirAgente.TabIndex = 6;
            this.btn_excluirAgente.TabStop = false;
            this.btn_excluirAgente.Text = "&Excluir";
            this.btn_excluirAgente.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btn_excluirAgente.UseVisualStyleBackColor = true;
            // 
            // lblInformacaoEpi
            // 
            this.lblInformacaoEpi.AutoSize = true;
            this.lblInformacaoEpi.Location = new System.Drawing.Point(3, 1157);
            this.lblInformacaoEpi.Name = "lblInformacaoEpi";
            this.lblInformacaoEpi.Size = new System.Drawing.Size(160, 13);
            this.lblInformacaoEpi.TabIndex = 49;
            this.lblInformacaoEpi.Text = "Informações sobre os EPI / EPC";
            // 
            // dgvEpi
            // 
            this.dgvEpi.AllowUserToAddRows = false;
            this.dgvEpi.AllowUserToDeleteRows = false;
            this.dgvEpi.AllowUserToOrderColumns = true;
            this.dgvEpi.AllowUserToResizeRows = false;
            dataGridViewCellStyle9.BackColor = System.Drawing.Color.Gainsboro;
            this.dgvEpi.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle9;
            this.dgvEpi.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this.dgvEpi.BackgroundColor = System.Drawing.Color.White;
            this.dgvEpi.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dataGridViewCellStyle10.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle10.BackColor = System.Drawing.Color.LightGray;
            dataGridViewCellStyle10.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle10.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle10.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle10.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle10.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvEpi.DefaultCellStyle = dataGridViewCellStyle10;
            this.dgvEpi.Location = new System.Drawing.Point(2, 1175);
            this.dgvEpi.Name = "dgvEpi";
            this.dgvEpi.ReadOnly = true;
            this.dgvEpi.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvEpi.Size = new System.Drawing.Size(759, 157);
            this.dgvEpi.TabIndex = 50;
            // 
            // flpEpi
            // 
            this.flpEpi.Controls.Add(this.btnIncluirEpi);
            this.flpEpi.Controls.Add(this.btnExcluirEPI);
            this.flpEpi.Location = new System.Drawing.Point(3, 1336);
            this.flpEpi.Name = "flpEpi";
            this.flpEpi.Size = new System.Drawing.Size(758, 32);
            this.flpEpi.TabIndex = 51;
            // 
            // btnIncluirEpi
            // 
            this.btnIncluirEpi.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnIncluirEpi.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnIncluirEpi.Image = global::SWS.Properties.Resources.icone_mais;
            this.btnIncluirEpi.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnIncluirEpi.Location = new System.Drawing.Point(3, 3);
            this.btnIncluirEpi.Name = "btnIncluirEpi";
            this.btnIncluirEpi.Size = new System.Drawing.Size(77, 23);
            this.btnIncluirEpi.TabIndex = 18;
            this.btnIncluirEpi.Text = "&Incluir";
            this.btnIncluirEpi.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnIncluirEpi.UseVisualStyleBackColor = true;
            // 
            // btnExcluirEPI
            // 
            this.btnExcluirEPI.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnExcluirEPI.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnExcluirEPI.Image = global::SWS.Properties.Resources.lixeira;
            this.btnExcluirEPI.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnExcluirEPI.Location = new System.Drawing.Point(86, 3);
            this.btnExcluirEPI.Name = "btnExcluirEPI";
            this.btnExcluirEPI.Size = new System.Drawing.Size(77, 23);
            this.btnExcluirEPI.TabIndex = 7;
            this.btnExcluirEPI.TabStop = false;
            this.btnExcluirEPI.Text = "&Excluir";
            this.btnExcluirEPI.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnExcluirEPI.UseVisualStyleBackColor = true;
            // 
            // lblInformacaoSetor
            // 
            this.lblInformacaoSetor.AutoSize = true;
            this.lblInformacaoSetor.Location = new System.Drawing.Point(3, 1375);
            this.lblInformacaoSetor.Name = "lblInformacaoSetor";
            this.lblInformacaoSetor.Size = new System.Drawing.Size(183, 13);
            this.lblInformacaoSetor.TabIndex = 52;
            this.lblInformacaoSetor.Text = "Informações sobre os Setore do GHE";
            // 
            // dgvSetor
            // 
            this.dgvSetor.AllowUserToAddRows = false;
            this.dgvSetor.AllowUserToDeleteRows = false;
            this.dgvSetor.AllowUserToOrderColumns = true;
            this.dgvSetor.AllowUserToResizeRows = false;
            dataGridViewCellStyle7.BackColor = System.Drawing.Color.WhiteSmoke;
            this.dgvSetor.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle7;
            this.dgvSetor.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this.dgvSetor.BackgroundColor = System.Drawing.Color.White;
            this.dgvSetor.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dataGridViewCellStyle8.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle8.BackColor = System.Drawing.Color.LightGray;
            dataGridViewCellStyle8.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle8.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle8.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle8.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle8.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvSetor.DefaultCellStyle = dataGridViewCellStyle8;
            this.dgvSetor.Location = new System.Drawing.Point(2, 1392);
            this.dgvSetor.Name = "dgvSetor";
            this.dgvSetor.ReadOnly = true;
            this.dgvSetor.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvSetor.Size = new System.Drawing.Size(759, 157);
            this.dgvSetor.TabIndex = 53;
            // 
            // flpSetor
            // 
            this.flpSetor.Controls.Add(this.btnIncluirSetor);
            this.flpSetor.Controls.Add(this.btnExcluirSetor);
            this.flpSetor.Location = new System.Drawing.Point(3, 1554);
            this.flpSetor.Name = "flpSetor";
            this.flpSetor.Size = new System.Drawing.Size(758, 32);
            this.flpSetor.TabIndex = 54;
            // 
            // btnIncluirSetor
            // 
            this.btnIncluirSetor.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnIncluirSetor.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnIncluirSetor.Image = global::SWS.Properties.Resources.icone_mais;
            this.btnIncluirSetor.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnIncluirSetor.Location = new System.Drawing.Point(3, 3);
            this.btnIncluirSetor.Name = "btnIncluirSetor";
            this.btnIncluirSetor.Size = new System.Drawing.Size(77, 23);
            this.btnIncluirSetor.TabIndex = 19;
            this.btnIncluirSetor.Text = "&Incluir";
            this.btnIncluirSetor.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnIncluirSetor.UseVisualStyleBackColor = true;
            // 
            // btnExcluirSetor
            // 
            this.btnExcluirSetor.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnExcluirSetor.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnExcluirSetor.Image = global::SWS.Properties.Resources.lixeira;
            this.btnExcluirSetor.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnExcluirSetor.Location = new System.Drawing.Point(86, 3);
            this.btnExcluirSetor.Name = "btnExcluirSetor";
            this.btnExcluirSetor.Size = new System.Drawing.Size(77, 23);
            this.btnExcluirSetor.TabIndex = 5;
            this.btnExcluirSetor.TabStop = false;
            this.btnExcluirSetor.Text = "&Excluir";
            this.btnExcluirSetor.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnExcluirSetor.UseVisualStyleBackColor = true;
            // 
            // lblInformacaoFuncao
            // 
            this.lblInformacaoFuncao.AutoSize = true;
            this.lblInformacaoFuncao.Location = new System.Drawing.Point(3, 1592);
            this.lblInformacaoFuncao.Name = "lblInformacaoFuncao";
            this.lblInformacaoFuncao.Size = new System.Drawing.Size(147, 13);
            this.lblInformacaoFuncao.TabIndex = 55;
            this.lblInformacaoFuncao.Text = "Informação sobre as Funções";
            // 
            // dgvFuncao
            // 
            this.dgvFuncao.AllowUserToAddRows = false;
            this.dgvFuncao.AllowUserToDeleteRows = false;
            this.dgvFuncao.AllowUserToOrderColumns = true;
            this.dgvFuncao.AllowUserToResizeRows = false;
            dataGridViewCellStyle5.BackColor = System.Drawing.Color.Gainsboro;
            this.dgvFuncao.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle5;
            this.dgvFuncao.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this.dgvFuncao.BackgroundColor = System.Drawing.Color.White;
            this.dgvFuncao.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle6.BackColor = System.Drawing.Color.LightGray;
            dataGridViewCellStyle6.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle6.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle6.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle6.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle6.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvFuncao.DefaultCellStyle = dataGridViewCellStyle6;
            this.dgvFuncao.Location = new System.Drawing.Point(2, 1612);
            this.dgvFuncao.Name = "dgvFuncao";
            this.dgvFuncao.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvFuncao.Size = new System.Drawing.Size(759, 157);
            this.dgvFuncao.TabIndex = 56;
            // 
            // flpFuncao
            // 
            this.flpFuncao.Controls.Add(this.btnIncluirFuncao);
            this.flpFuncao.Controls.Add(this.btnExcluirFuncao);
            this.flpFuncao.Location = new System.Drawing.Point(3, 1774);
            this.flpFuncao.Name = "flpFuncao";
            this.flpFuncao.Size = new System.Drawing.Size(758, 32);
            this.flpFuncao.TabIndex = 57;
            // 
            // btnIncluirFuncao
            // 
            this.btnIncluirFuncao.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnIncluirFuncao.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnIncluirFuncao.Image = global::SWS.Properties.Resources.icone_mais;
            this.btnIncluirFuncao.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnIncluirFuncao.Location = new System.Drawing.Point(3, 3);
            this.btnIncluirFuncao.Name = "btnIncluirFuncao";
            this.btnIncluirFuncao.Size = new System.Drawing.Size(77, 23);
            this.btnIncluirFuncao.TabIndex = 20;
            this.btnIncluirFuncao.Text = "&Incluir";
            this.btnIncluirFuncao.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnIncluirFuncao.UseVisualStyleBackColor = true;
            // 
            // btnExcluirFuncao
            // 
            this.btnExcluirFuncao.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnExcluirFuncao.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnExcluirFuncao.Image = global::SWS.Properties.Resources.lixeira;
            this.btnExcluirFuncao.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnExcluirFuncao.Location = new System.Drawing.Point(86, 3);
            this.btnExcluirFuncao.Name = "btnExcluirFuncao";
            this.btnExcluirFuncao.Size = new System.Drawing.Size(77, 23);
            this.btnExcluirFuncao.TabIndex = 6;
            this.btnExcluirFuncao.TabStop = false;
            this.btnExcluirFuncao.Text = "&Excluir";
            this.btnExcluirFuncao.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnExcluirFuncao.UseVisualStyleBackColor = true;
            // 
            // lblInformacaoCronograma
            // 
            this.lblInformacaoCronograma.AutoSize = true;
            this.lblInformacaoCronograma.Location = new System.Drawing.Point(3, 1813);
            this.lblInformacaoCronograma.Name = "lblInformacaoCronograma";
            this.lblInformacaoCronograma.Size = new System.Drawing.Size(211, 13);
            this.lblInformacaoCronograma.TabIndex = 58;
            this.lblInformacaoCronograma.Text = "Informações sobre o Cronograma de Ações";
            // 
            // dgvAtividade
            // 
            this.dgvAtividade.AllowUserToAddRows = false;
            this.dgvAtividade.AllowUserToDeleteRows = false;
            this.dgvAtividade.AllowUserToOrderColumns = true;
            dataGridViewCellStyle3.BackColor = System.Drawing.Color.Gainsboro;
            this.dgvAtividade.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle3;
            this.dgvAtividade.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.DisplayedCells;
            this.dgvAtividade.BackgroundColor = System.Drawing.Color.White;
            this.dgvAtividade.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle4.BackColor = System.Drawing.Color.LightGray;
            dataGridViewCellStyle4.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle4.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle4.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle4.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle4.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvAtividade.DefaultCellStyle = dataGridViewCellStyle4;
            this.dgvAtividade.Location = new System.Drawing.Point(2, 1830);
            this.dgvAtividade.Name = "dgvAtividade";
            this.dgvAtividade.ReadOnly = true;
            this.dgvAtividade.Size = new System.Drawing.Size(759, 157);
            this.dgvAtividade.TabIndex = 59;
            // 
            // flpAtividade
            // 
            this.flpAtividade.Controls.Add(this.btn_incluirAtividade);
            this.flpAtividade.Controls.Add(this.btn_alterarAtividade);
            this.flpAtividade.Controls.Add(this.btn_excluirAtividade);
            this.flpAtividade.Location = new System.Drawing.Point(3, 1991);
            this.flpAtividade.Name = "flpAtividade";
            this.flpAtividade.Size = new System.Drawing.Size(758, 32);
            this.flpAtividade.TabIndex = 60;
            // 
            // btn_incluirAtividade
            // 
            this.btn_incluirAtividade.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btn_incluirAtividade.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_incluirAtividade.Image = global::SWS.Properties.Resources.icone_mais;
            this.btn_incluirAtividade.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btn_incluirAtividade.Location = new System.Drawing.Point(3, 3);
            this.btn_incluirAtividade.Name = "btn_incluirAtividade";
            this.btn_incluirAtividade.Size = new System.Drawing.Size(77, 23);
            this.btn_incluirAtividade.TabIndex = 21;
            this.btn_incluirAtividade.Text = "&Incluir";
            this.btn_incluirAtividade.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btn_incluirAtividade.UseVisualStyleBackColor = true;
            // 
            // btn_alterarAtividade
            // 
            this.btn_alterarAtividade.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btn_alterarAtividade.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_alterarAtividade.Image = global::SWS.Properties.Resources.Alterar;
            this.btn_alterarAtividade.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btn_alterarAtividade.Location = new System.Drawing.Point(86, 3);
            this.btn_alterarAtividade.Name = "btn_alterarAtividade";
            this.btn_alterarAtividade.Size = new System.Drawing.Size(77, 23);
            this.btn_alterarAtividade.TabIndex = 22;
            this.btn_alterarAtividade.Text = "&Alterar";
            this.btn_alterarAtividade.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btn_alterarAtividade.UseVisualStyleBackColor = true;
            // 
            // btn_excluirAtividade
            // 
            this.btn_excluirAtividade.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btn_excluirAtividade.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_excluirAtividade.Image = global::SWS.Properties.Resources.lixeira;
            this.btn_excluirAtividade.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btn_excluirAtividade.Location = new System.Drawing.Point(169, 3);
            this.btn_excluirAtividade.Name = "btn_excluirAtividade";
            this.btn_excluirAtividade.Size = new System.Drawing.Size(77, 23);
            this.btn_excluirAtividade.TabIndex = 8;
            this.btn_excluirAtividade.TabStop = false;
            this.btn_excluirAtividade.Text = "&Excluir";
            this.btn_excluirAtividade.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btn_excluirAtividade.UseVisualStyleBackColor = true;
            // 
            // lblComentario
            // 
            this.lblComentario.AutoSize = true;
            this.lblComentario.Location = new System.Drawing.Point(3, 2031);
            this.lblComentario.Name = "lblComentario";
            this.lblComentario.Size = new System.Drawing.Size(150, 13);
            this.lblComentario.TabIndex = 61;
            this.lblComentario.Text = "Informações sobre Comentário";
            // 
            // textComentario
            // 
            this.textComentario.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.textComentario.Location = new System.Drawing.Point(3, 2052);
            this.textComentario.Multiline = true;
            this.textComentario.Name = "textComentario";
            this.textComentario.Size = new System.Drawing.Size(758, 80);
            this.textComentario.TabIndex = 23;
            // 
            // lblInformacaoAnexo
            // 
            this.lblInformacaoAnexo.AutoSize = true;
            this.lblInformacaoAnexo.Location = new System.Drawing.Point(3, 2141);
            this.lblInformacaoAnexo.Name = "lblInformacaoAnexo";
            this.lblInformacaoAnexo.Size = new System.Drawing.Size(146, 13);
            this.lblInformacaoAnexo.TabIndex = 63;
            this.lblInformacaoAnexo.Text = "Informações sobre os Anexos";
            // 
            // lblAnexo
            // 
            this.lblAnexo.BackColor = System.Drawing.Color.LightGray;
            this.lblAnexo.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblAnexo.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblAnexo.Location = new System.Drawing.Point(3, 2164);
            this.lblAnexo.MaxLength = 15;
            this.lblAnexo.Name = "lblAnexo";
            this.lblAnexo.ReadOnly = true;
            this.lblAnexo.Size = new System.Drawing.Size(147, 21);
            this.lblAnexo.TabIndex = 64;
            this.lblAnexo.TabStop = false;
            this.lblAnexo.Text = "Informação de Anexos";
            // 
            // textAnexo
            // 
            this.textAnexo.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.textAnexo.BackColor = System.Drawing.Color.White;
            this.textAnexo.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textAnexo.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.textAnexo.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textAnexo.ForeColor = System.Drawing.Color.Black;
            this.textAnexo.Location = new System.Drawing.Point(149, 2164);
            this.textAnexo.MaxLength = 15;
            this.textAnexo.Name = "textAnexo";
            this.textAnexo.Size = new System.Drawing.Size(579, 21);
            this.textAnexo.TabIndex = 24;
            // 
            // btnIncluirAnexo
            // 
            this.btnIncluirAnexo.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnIncluirAnexo.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnIncluirAnexo.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnIncluirAnexo.Image = global::SWS.Properties.Resources.Gravar;
            this.btnIncluirAnexo.Location = new System.Drawing.Point(726, 2164);
            this.btnIncluirAnexo.Name = "btnIncluirAnexo";
            this.btnIncluirAnexo.Size = new System.Drawing.Size(34, 21);
            this.btnIncluirAnexo.TabIndex = 25;
            this.btnIncluirAnexo.UseVisualStyleBackColor = true;
            // 
            // dgvAnexo
            // 
            this.dgvAnexo.AllowUserToAddRows = false;
            this.dgvAnexo.AllowUserToDeleteRows = false;
            this.dgvAnexo.AllowUserToOrderColumns = true;
            this.dgvAnexo.AllowUserToResizeRows = false;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.Gainsboro;
            this.dgvAnexo.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle1;
            this.dgvAnexo.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.DisplayedCells;
            this.dgvAnexo.BackgroundColor = System.Drawing.Color.White;
            this.dgvAnexo.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.Color.LightGray;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvAnexo.DefaultCellStyle = dataGridViewCellStyle2;
            this.dgvAnexo.Location = new System.Drawing.Point(2, 2189);
            this.dgvAnexo.Name = "dgvAnexo";
            this.dgvAnexo.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvAnexo.Size = new System.Drawing.Size(759, 157);
            this.dgvAnexo.TabIndex = 4;
            // 
            // frmPpraIncluir
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(784, 561);
            this.Name = "frmPpraIncluir";
            this.Text = "INCLUIR PPRA";
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.frmPpraIncluir_KeyDown);
            this.spForm.Panel1.ResumeLayout(false);
            this.spForm.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.spForm)).EndInit();
            this.spForm.ResumeLayout(false);
            this.pnlForm.ResumeLayout(false);
            this.pnlForm.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.banner)).EndInit();
            this.flpAcao.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgPrevisao)).EndInit();
            this.flpPrevisao.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvGhe)).EndInit();
            this.flpGhe.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvFonte)).EndInit();
            this.flpFonte.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvAgente)).EndInit();
            this.flpAgente.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvEpi)).EndInit();
            this.flpEpi.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvSetor)).EndInit();
            this.flpSetor.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvFuncao)).EndInit();
            this.flpFuncao.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvAtividade)).EndInit();
            this.flpAtividade.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvAnexo)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.FlowLayoutPanel flpAcao;
        protected System.Windows.Forms.Button btnNovoEstudo;
        protected System.Windows.Forms.Button btnFechar;
        private System.Windows.Forms.TextBox lblContratante;
        private System.Windows.Forms.Button btnIncluirCliente;
        private System.Windows.Forms.TextBox lblCliente;
        private System.Windows.Forms.TextBox textCliente;
        private System.Windows.Forms.Button btnExcluirCliente;
        private System.Windows.Forms.Label lblCodigoEstudo;
        private System.Windows.Forms.Button btnExcluirContratante;
        private System.Windows.Forms.Button btnIncluirContratante;
        private System.Windows.Forms.TextBox textContratante;
        private System.Windows.Forms.Label lblInfoContratante;
        private System.Windows.Forms.TextBox textContrato;
        private System.Windows.Forms.TextBox lblNumeroContrato;
        private System.Windows.Forms.TextBox lblPeriodo;
        private System.Windows.Forms.DateTimePicker dtTerminoContrato;
        private System.Windows.Forms.DateTimePicker dtInicioContrato;
        private System.Windows.Forms.Button btnExcluirElaborador;
        private System.Windows.Forms.Button btnIncluirElaborador;
        private System.Windows.Forms.TextBox textElaborador;
        private System.Windows.Forms.TextBox lblElaborador;
        private System.Windows.Forms.TextBox lblDataCriação;
        private System.Windows.Forms.TextBox lblDataVencimento;
        private System.Windows.Forms.DateTimePicker dataVencimento;
        private System.Windows.Forms.DateTimePicker dataCriação;
        private System.Windows.Forms.TextBox lblGrauRisco;
        private ComboBoxWithBorder cbGrauRisco;
        private System.Windows.Forms.TextBox lblLocalEstudo;
        private System.Windows.Forms.Label lblIformacaoObra;
        private System.Windows.Forms.Button btnExcluirLocalEstudo;
        private System.Windows.Forms.Button btnLocalEstudo;
        private System.Windows.Forms.TextBox textLocalEstudo;
        private System.Windows.Forms.Label lblPrevisaoFuncionario;
        protected System.Windows.Forms.DataGridView dgPrevisao;
        private System.Windows.Forms.FlowLayoutPanel flpPrevisao;
        protected System.Windows.Forms.Button btnIncluirPrevisao;
        protected System.Windows.Forms.Button btnExcluirPrevisao;
        private System.Windows.Forms.TextBox lblCnaeCliente;
        private System.Windows.Forms.Button btnIncluirCnae;
        private System.Windows.Forms.Button btnExcluirCnae;
        private System.Windows.Forms.TextBox textCnae;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.TextBox textBox2;
        private System.Windows.Forms.Label lblInformacaoGhe;
        public System.Windows.Forms.DataGridView dgvGhe;
        private System.Windows.Forms.FlowLayoutPanel flpGhe;
        protected System.Windows.Forms.Button btnIncluirGhe;
        protected System.Windows.Forms.Button btnAlterarGhe;
        protected System.Windows.Forms.Button btnExcluirGhe;
        private System.Windows.Forms.Label lblInformacaoFonte;
        protected System.Windows.Forms.DataGridView dgvFonte;
        private System.Windows.Forms.FlowLayoutPanel flpFonte;
        protected System.Windows.Forms.Button btnIncluirFonte;
        protected System.Windows.Forms.Button btnExcluirFonte;
        protected System.Windows.Forms.Button btnGravar;
        private System.Windows.Forms.Label lblInformacaoAgente;
        protected System.Windows.Forms.DataGridView dgvAgente;
        private System.Windows.Forms.FlowLayoutPanel flpAgente;
        protected System.Windows.Forms.Button btn_incluirAgente;
        protected System.Windows.Forms.Button btn_excluirAgente;
        private System.Windows.Forms.Label lblInformacaoEpi;
        private System.Windows.Forms.FlowLayoutPanel flpEpi;
        protected System.Windows.Forms.DataGridView dgvEpi;
        protected System.Windows.Forms.Button btnIncluirEpi;
        protected System.Windows.Forms.Button btnExcluirEPI;
        private System.Windows.Forms.Label lblInformacaoSetor;
        private System.Windows.Forms.FlowLayoutPanel flpSetor;
        protected System.Windows.Forms.DataGridView dgvSetor;
        protected System.Windows.Forms.Button btnIncluirSetor;
        protected System.Windows.Forms.Button btnExcluirSetor;
        private System.Windows.Forms.Label lblInformacaoFuncao;
        private System.Windows.Forms.FlowLayoutPanel flpFuncao;
        public System.Windows.Forms.DataGridView dgvFuncao;
        protected System.Windows.Forms.Button btnIncluirFuncao;
        protected System.Windows.Forms.Button btnExcluirFuncao;
        private System.Windows.Forms.Label lblInformacaoCronograma;
        private System.Windows.Forms.FlowLayoutPanel flpAtividade;
        public System.Windows.Forms.DataGridView dgvAtividade;
        protected System.Windows.Forms.Button btn_incluirAtividade;
        protected System.Windows.Forms.Button btn_alterarAtividade;
        protected System.Windows.Forms.Button btn_excluirAtividade;
        private System.Windows.Forms.TextBox textComentario;
        private System.Windows.Forms.Label lblComentario;
        private System.Windows.Forms.Label lblInformacaoAnexo;
        private System.Windows.Forms.Button btnIncluirAnexo;
        private System.Windows.Forms.TextBox textAnexo;
        private System.Windows.Forms.TextBox lblAnexo;
        protected System.Windows.Forms.DataGridView dgvAnexo;
    }
}