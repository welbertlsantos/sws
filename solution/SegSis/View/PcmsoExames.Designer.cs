﻿namespace SWS.View
{
    partial class frm_PcmsoExames
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle7 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frm_PcmsoExames));
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle8 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle9 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle10 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle11 = new System.Windows.Forms.DataGridViewCellStyle();
            this.tb_pcmso = new System.Windows.Forms.TabControl();
            this.pg_pcmsoExame = new System.Windows.Forms.TabPage();
            this.btn_nota = new System.Windows.Forms.Button();
            this.btn_exame_alterar = new System.Windows.Forms.Button();
            this.grb_periodicidade = new System.Windows.Forms.GroupBox();
            this.grd_periodicidade = new System.Windows.Forms.DataGridView();
            this.btn_excluir = new System.Windows.Forms.Button();
            this.grb_exames = new System.Windows.Forms.GroupBox();
            this.grd_exame = new System.Windows.Forms.DataGridView();
            this.btn_incluir = new System.Windows.Forms.Button();
            this.grb_risco = new System.Windows.Forms.GroupBox();
            this.grd_risco = new System.Windows.Forms.DataGridView();
            this.grb_funcao = new System.Windows.Forms.GroupBox();
            this.grd_funcao = new System.Windows.Forms.DataGridView();
            this.grb_ghe = new System.Windows.Forms.GroupBox();
            this.grd_ghe = new System.Windows.Forms.DataGridView();
            this.tb_cronograma = new System.Windows.Forms.TabPage();
            this.text_descricao = new System.Windows.Forms.TextBox();
            this.btn_excluirAtividade = new System.Windows.Forms.Button();
            this.lbl_descricao = new System.Windows.Forms.Label();
            this.grb_atividade = new System.Windows.Forms.GroupBox();
            this.grd_atividade = new System.Windows.Forms.DataGridView();
            this.btn_alterar = new System.Windows.Forms.Button();
            this.btn_incluirAtividade = new System.Windows.Forms.Button();
            this.tb_hospitais = new System.Windows.Forms.TabPage();
            this.btn_excluirMedico = new System.Windows.Forms.Button();
            this.btn_excluirHospital = new System.Windows.Forms.Button();
            this.btn_incluirMedico = new System.Windows.Forms.Button();
            this.grb_medico = new System.Windows.Forms.GroupBox();
            this.grd_medico = new System.Windows.Forms.DataGridView();
            this.btn_incluirHospital = new System.Windows.Forms.Button();
            this.grb_hospital = new System.Windows.Forms.GroupBox();
            this.grd_hospital = new System.Windows.Forms.DataGridView();
            this.tb_equipamento = new System.Windows.Forms.TabPage();
            this.btn_excluirMaterial = new System.Windows.Forms.Button();
            this.grb_paginacao = new System.Windows.Forms.GroupBox();
            this.btn_salvarMaterial = new System.Windows.Forms.Button();
            this.btn_incluirMaterial = new System.Windows.Forms.Button();
            this.grb_material = new System.Windows.Forms.GroupBox();
            this.grd_material = new System.Windows.Forms.DataGridView();
            this.tb_anexo = new System.Windows.Forms.TabPage();
            this.lbl_anexo = new System.Windows.Forms.Label();
            this.btn_excluirAnexo = new System.Windows.Forms.Button();
            this.btn_incluirAnexo = new System.Windows.Forms.Button();
            this.grb_anexo = new System.Windows.Forms.GroupBox();
            this.grd_anexo = new System.Windows.Forms.DataGridView();
            this.text_conteudo = new System.Windows.Forms.TextBox();
            this.btn_fechar = new System.Windows.Forms.Button();
            this.panel_banner.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox_banner)).BeginInit();
            this.panel.SuspendLayout();
            this.tb_pcmso.SuspendLayout();
            this.pg_pcmsoExame.SuspendLayout();
            this.grb_periodicidade.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grd_periodicidade)).BeginInit();
            this.grb_exames.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grd_exame)).BeginInit();
            this.grb_risco.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grd_risco)).BeginInit();
            this.grb_funcao.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grd_funcao)).BeginInit();
            this.grb_ghe.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grd_ghe)).BeginInit();
            this.tb_cronograma.SuspendLayout();
            this.grb_atividade.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grd_atividade)).BeginInit();
            this.tb_hospitais.SuspendLayout();
            this.grb_medico.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grd_medico)).BeginInit();
            this.grb_hospital.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grd_hospital)).BeginInit();
            this.tb_equipamento.SuspendLayout();
            this.grb_paginacao.SuspendLayout();
            this.grb_material.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grd_material)).BeginInit();
            this.tb_anexo.SuspendLayout();
            this.grb_anexo.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grd_anexo)).BeginInit();
            this.SuspendLayout();
            // 
            // panel
            // 
            this.panel.Controls.Add(this.btn_fechar);
            this.panel.Controls.Add(this.tb_pcmso);
            // 
            // tb_pcmso
            // 
            this.tb_pcmso.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.tb_pcmso.Controls.Add(this.pg_pcmsoExame);
            this.tb_pcmso.Controls.Add(this.tb_cronograma);
            this.tb_pcmso.Controls.Add(this.tb_hospitais);
            this.tb_pcmso.Controls.Add(this.tb_equipamento);
            this.tb_pcmso.Controls.Add(this.tb_anexo);
            this.tb_pcmso.Location = new System.Drawing.Point(3, 3);
            this.tb_pcmso.Name = "tb_pcmso";
            this.tb_pcmso.SelectedIndex = 0;
            this.tb_pcmso.Size = new System.Drawing.Size(781, 486);
            this.tb_pcmso.TabIndex = 0;
            this.tb_pcmso.SelectedIndexChanged += new System.EventHandler(this.tb_pcmso_SelectedIndexChanged);
            // 
            // pg_pcmsoExame
            // 
            this.pg_pcmsoExame.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pg_pcmsoExame.Controls.Add(this.btn_nota);
            this.pg_pcmsoExame.Controls.Add(this.btn_exame_alterar);
            this.pg_pcmsoExame.Controls.Add(this.grb_periodicidade);
            this.pg_pcmsoExame.Controls.Add(this.btn_excluir);
            this.pg_pcmsoExame.Controls.Add(this.grb_exames);
            this.pg_pcmsoExame.Controls.Add(this.btn_incluir);
            this.pg_pcmsoExame.Controls.Add(this.grb_risco);
            this.pg_pcmsoExame.Controls.Add(this.grb_funcao);
            this.pg_pcmsoExame.Controls.Add(this.grb_ghe);
            this.pg_pcmsoExame.Location = new System.Drawing.Point(4, 22);
            this.pg_pcmsoExame.Name = "pg_pcmsoExame";
            this.pg_pcmsoExame.Padding = new System.Windows.Forms.Padding(3);
            this.pg_pcmsoExame.Size = new System.Drawing.Size(773, 460);
            this.pg_pcmsoExame.TabIndex = 0;
            this.pg_pcmsoExame.Text = "Exames";
            this.pg_pcmsoExame.UseVisualStyleBackColor = true;
            // 
            // btn_nota
            // 
            this.btn_nota.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_nota.Image = global::SWS.Properties.Resources.nota;
            this.btn_nota.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btn_nota.Location = new System.Drawing.Point(9, 129);
            this.btn_nota.Name = "btn_nota";
            this.btn_nota.Size = new System.Drawing.Size(69, 23);
            this.btn_nota.TabIndex = 1;
            this.btn_nota.Text = "&Notas";
            this.btn_nota.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btn_nota.UseVisualStyleBackColor = true;
            this.btn_nota.Click += new System.EventHandler(this.btn_nota_Click);
            // 
            // btn_exame_alterar
            // 
            this.btn_exame_alterar.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btn_exame_alterar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_exame_alterar.Image = global::SWS.Properties.Resources.Alterar;
            this.btn_exame_alterar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btn_exame_alterar.Location = new System.Drawing.Point(159, 426);
            this.btn_exame_alterar.Name = "btn_exame_alterar";
            this.btn_exame_alterar.Size = new System.Drawing.Size(75, 23);
            this.btn_exame_alterar.TabIndex = 8;
            this.btn_exame_alterar.Text = "&Alterar";
            this.btn_exame_alterar.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btn_exame_alterar.UseVisualStyleBackColor = true;
            this.btn_exame_alterar.Click += new System.EventHandler(this.btn_exame_alterar_Click);
            // 
            // grb_periodicidade
            // 
            this.grb_periodicidade.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.grb_periodicidade.Controls.Add(this.grd_periodicidade);
            this.grb_periodicidade.Location = new System.Drawing.Point(402, 295);
            this.grb_periodicidade.Name = "grb_periodicidade";
            this.grb_periodicidade.Size = new System.Drawing.Size(362, 126);
            this.grb_periodicidade.TabIndex = 4;
            this.grb_periodicidade.TabStop = false;
            this.grb_periodicidade.Text = "Periodicidade";
            // 
            // grd_periodicidade
            // 
            this.grd_periodicidade.AllowUserToAddRows = false;
            this.grd_periodicidade.AllowUserToDeleteRows = false;
            this.grd_periodicidade.AllowUserToResizeRows = false;
            this.grd_periodicidade.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this.grd_periodicidade.BackgroundColor = System.Drawing.Color.White;
            this.grd_periodicidade.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.grd_periodicidade.DefaultCellStyle = dataGridViewCellStyle1;
            this.grd_periodicidade.Dock = System.Windows.Forms.DockStyle.Fill;
            this.grd_periodicidade.Location = new System.Drawing.Point(3, 16);
            this.grd_periodicidade.MultiSelect = false;
            this.grd_periodicidade.Name = "grd_periodicidade";
            this.grd_periodicidade.ReadOnly = true;
            this.grd_periodicidade.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.grd_periodicidade.Size = new System.Drawing.Size(356, 107);
            this.grd_periodicidade.TabIndex = 7;
            // 
            // btn_excluir
            // 
            this.btn_excluir.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btn_excluir.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_excluir.Image = global::SWS.Properties.Resources.lixeira;
            this.btn_excluir.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btn_excluir.Location = new System.Drawing.Point(85, 427);
            this.btn_excluir.Name = "btn_excluir";
            this.btn_excluir.Size = new System.Drawing.Size(68, 23);
            this.btn_excluir.TabIndex = 6;
            this.btn_excluir.Text = "&Excluir";
            this.btn_excluir.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btn_excluir.UseVisualStyleBackColor = true;
            this.btn_excluir.Click += new System.EventHandler(this.btn_excluir_Click);
            // 
            // grb_exames
            // 
            this.grb_exames.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.grb_exames.Controls.Add(this.grd_exame);
            this.grb_exames.Location = new System.Drawing.Point(7, 295);
            this.grb_exames.Name = "grb_exames";
            this.grb_exames.Size = new System.Drawing.Size(390, 126);
            this.grb_exames.TabIndex = 3;
            this.grb_exames.TabStop = false;
            this.grb_exames.Text = "Exames";
            // 
            // grd_exame
            // 
            this.grd_exame.AllowUserToAddRows = false;
            this.grd_exame.AllowUserToDeleteRows = false;
            this.grd_exame.AllowUserToOrderColumns = true;
            this.grd_exame.AllowUserToResizeRows = false;
            this.grd_exame.BackgroundColor = System.Drawing.Color.GhostWhite;
            this.grd_exame.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.grd_exame.DefaultCellStyle = dataGridViewCellStyle2;
            this.grd_exame.Dock = System.Windows.Forms.DockStyle.Fill;
            this.grd_exame.Location = new System.Drawing.Point(3, 16);
            this.grd_exame.Name = "grd_exame";
            this.grd_exame.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.CellSelect;
            this.grd_exame.Size = new System.Drawing.Size(384, 107);
            this.grd_exame.TabIndex = 4;
            this.grd_exame.CellBeginEdit += new System.Windows.Forms.DataGridViewCellCancelEventHandler(this.grd_exame_CellBeginEdit);
            this.grd_exame.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.grd_exame_CellClick);
            this.grd_exame.CellEndEdit += new System.Windows.Forms.DataGridViewCellEventHandler(this.grd_exame_CellEndEdit);
            this.grd_exame.EditingControlShowing += new System.Windows.Forms.DataGridViewEditingControlShowingEventHandler(this.grd_exame_EditingControlShowing);
            this.grd_exame.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.grd_exame_KeyPress);
            // 
            // btn_incluir
            // 
            this.btn_incluir.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btn_incluir.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_incluir.Image = global::SWS.Properties.Resources.fechar_ico;
            this.btn_incluir.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btn_incluir.Location = new System.Drawing.Point(10, 427);
            this.btn_incluir.Name = "btn_incluir";
            this.btn_incluir.Size = new System.Drawing.Size(68, 23);
            this.btn_incluir.TabIndex = 5;
            this.btn_incluir.Text = "&Incluir";
            this.btn_incluir.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btn_incluir.UseVisualStyleBackColor = true;
            this.btn_incluir.Click += new System.EventHandler(this.btn_incluir_Click);
            // 
            // grb_risco
            // 
            this.grb_risco.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.grb_risco.Controls.Add(this.grd_risco);
            this.grb_risco.Location = new System.Drawing.Point(7, 157);
            this.grb_risco.Name = "grb_risco";
            this.grb_risco.Size = new System.Drawing.Size(758, 132);
            this.grb_risco.TabIndex = 2;
            this.grb_risco.TabStop = false;
            this.grb_risco.Text = "Riscos";
            // 
            // grd_risco
            // 
            this.grd_risco.AllowUserToAddRows = false;
            this.grd_risco.AllowUserToDeleteRows = false;
            this.grd_risco.AllowUserToOrderColumns = true;
            this.grd_risco.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this.grd_risco.BackgroundColor = System.Drawing.Color.GhostWhite;
            this.grd_risco.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle3.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle3.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.grd_risco.DefaultCellStyle = dataGridViewCellStyle3;
            this.grd_risco.Dock = System.Windows.Forms.DockStyle.Fill;
            this.grd_risco.Location = new System.Drawing.Point(3, 16);
            this.grd_risco.MultiSelect = false;
            this.grd_risco.Name = "grd_risco";
            this.grd_risco.ReadOnly = true;
            this.grd_risco.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.grd_risco.Size = new System.Drawing.Size(752, 113);
            this.grd_risco.TabIndex = 3;
            this.grd_risco.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.grd_risco_CellClick);
            // 
            // grb_funcao
            // 
            this.grb_funcao.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.grb_funcao.Controls.Add(this.grd_funcao);
            this.grb_funcao.Location = new System.Drawing.Point(393, 2);
            this.grb_funcao.Name = "grb_funcao";
            this.grb_funcao.Size = new System.Drawing.Size(371, 149);
            this.grb_funcao.TabIndex = 1;
            this.grb_funcao.TabStop = false;
            this.grb_funcao.Text = "Funções";
            // 
            // grd_funcao
            // 
            this.grd_funcao.AllowUserToAddRows = false;
            this.grd_funcao.AllowUserToDeleteRows = false;
            this.grd_funcao.AllowUserToOrderColumns = true;
            this.grd_funcao.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this.grd_funcao.BackgroundColor = System.Drawing.Color.White;
            this.grd_funcao.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle4.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle4.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle4.ForeColor = System.Drawing.Color.Blue;
            dataGridViewCellStyle4.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle4.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle4.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.grd_funcao.DefaultCellStyle = dataGridViewCellStyle4;
            this.grd_funcao.Dock = System.Windows.Forms.DockStyle.Fill;
            this.grd_funcao.Location = new System.Drawing.Point(3, 16);
            this.grd_funcao.MultiSelect = false;
            this.grd_funcao.Name = "grd_funcao";
            this.grd_funcao.ReadOnly = true;
            this.grd_funcao.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.grd_funcao.Size = new System.Drawing.Size(365, 130);
            this.grd_funcao.TabIndex = 0;
            this.grd_funcao.TabStop = false;
            // 
            // grb_ghe
            // 
            this.grb_ghe.Controls.Add(this.grd_ghe);
            this.grb_ghe.Location = new System.Drawing.Point(6, 2);
            this.grb_ghe.Name = "grb_ghe";
            this.grb_ghe.Size = new System.Drawing.Size(381, 121);
            this.grb_ghe.TabIndex = 0;
            this.grb_ghe.TabStop = false;
            this.grb_ghe.Text = "GHE";
            // 
            // grd_ghe
            // 
            this.grd_ghe.AllowUserToAddRows = false;
            this.grd_ghe.AllowUserToDeleteRows = false;
            this.grd_ghe.AllowUserToOrderColumns = true;
            this.grd_ghe.AllowUserToResizeRows = false;
            this.grd_ghe.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this.grd_ghe.BackgroundColor = System.Drawing.Color.White;
            dataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle5.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle5.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle5.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle5.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle5.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle5.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.grd_ghe.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle5;
            this.grd_ghe.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle6.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle6.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle6.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle6.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle6.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle6.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.grd_ghe.DefaultCellStyle = dataGridViewCellStyle6;
            this.grd_ghe.Dock = System.Windows.Forms.DockStyle.Fill;
            this.grd_ghe.Location = new System.Drawing.Point(3, 16);
            this.grd_ghe.MultiSelect = false;
            this.grd_ghe.Name = "grd_ghe";
            this.grd_ghe.ReadOnly = true;
            this.grd_ghe.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.grd_ghe.Size = new System.Drawing.Size(375, 102);
            this.grd_ghe.TabIndex = 2;
            this.grd_ghe.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.grd_ghe_CellClick);
            this.grd_ghe.SelectionChanged += new System.EventHandler(this.grd_ghe_SelectionChanged);
            // 
            // tb_cronograma
            // 
            this.tb_cronograma.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tb_cronograma.Controls.Add(this.text_descricao);
            this.tb_cronograma.Controls.Add(this.btn_excluirAtividade);
            this.tb_cronograma.Controls.Add(this.lbl_descricao);
            this.tb_cronograma.Controls.Add(this.grb_atividade);
            this.tb_cronograma.Controls.Add(this.btn_alterar);
            this.tb_cronograma.Controls.Add(this.btn_incluirAtividade);
            this.tb_cronograma.Location = new System.Drawing.Point(4, 22);
            this.tb_cronograma.Name = "tb_cronograma";
            this.tb_cronograma.Padding = new System.Windows.Forms.Padding(3);
            this.tb_cronograma.Size = new System.Drawing.Size(773, 460);
            this.tb_cronograma.TabIndex = 1;
            this.tb_cronograma.Text = "Cronograma";
            this.tb_cronograma.UseVisualStyleBackColor = true;
            // 
            // text_descricao
            // 
            this.text_descricao.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.text_descricao.Location = new System.Drawing.Point(6, 20);
            this.text_descricao.MaxLength = 100;
            this.text_descricao.Name = "text_descricao";
            this.text_descricao.Size = new System.Drawing.Size(756, 20);
            this.text_descricao.TabIndex = 1;
            // 
            // btn_excluirAtividade
            // 
            this.btn_excluirAtividade.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btn_excluirAtividade.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_excluirAtividade.Image = global::SWS.Properties.Resources.lixeira;
            this.btn_excluirAtividade.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btn_excluirAtividade.Location = new System.Drawing.Point(172, 429);
            this.btn_excluirAtividade.Name = "btn_excluirAtividade";
            this.btn_excluirAtividade.Size = new System.Drawing.Size(75, 23);
            this.btn_excluirAtividade.TabIndex = 6;
            this.btn_excluirAtividade.Text = "&Excluir";
            this.btn_excluirAtividade.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btn_excluirAtividade.UseVisualStyleBackColor = true;
            this.btn_excluirAtividade.Click += new System.EventHandler(this.btn_excluirAtividade_Click);
            // 
            // lbl_descricao
            // 
            this.lbl_descricao.AutoSize = true;
            this.lbl_descricao.Location = new System.Drawing.Point(4, 4);
            this.lbl_descricao.Name = "lbl_descricao";
            this.lbl_descricao.Size = new System.Drawing.Size(55, 13);
            this.lbl_descricao.TabIndex = 0;
            this.lbl_descricao.Text = "Descriçao";
            // 
            // grb_atividade
            // 
            this.grb_atividade.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.grb_atividade.Controls.Add(this.grd_atividade);
            this.grb_atividade.Enabled = false;
            this.grb_atividade.Location = new System.Drawing.Point(7, 46);
            this.grb_atividade.Name = "grb_atividade";
            this.grb_atividade.Size = new System.Drawing.Size(758, 377);
            this.grb_atividade.TabIndex = 1;
            this.grb_atividade.TabStop = false;
            this.grb_atividade.Text = "Atividade";
            // 
            // grd_atividade
            // 
            this.grd_atividade.AllowUserToAddRows = false;
            this.grd_atividade.AllowUserToDeleteRows = false;
            this.grd_atividade.AllowUserToOrderColumns = true;
            this.grd_atividade.AllowUserToResizeRows = false;
            this.grd_atividade.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this.grd_atividade.BackgroundColor = System.Drawing.Color.White;
            this.grd_atividade.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dataGridViewCellStyle7.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle7.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle7.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle7.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle7.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle7.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle7.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.grd_atividade.DefaultCellStyle = dataGridViewCellStyle7;
            this.grd_atividade.Dock = System.Windows.Forms.DockStyle.Fill;
            this.grd_atividade.Location = new System.Drawing.Point(3, 16);
            this.grd_atividade.MultiSelect = false;
            this.grd_atividade.Name = "grd_atividade";
            this.grd_atividade.ReadOnly = true;
            this.grd_atividade.Size = new System.Drawing.Size(752, 358);
            this.grd_atividade.TabIndex = 3;
            // 
            // btn_alterar
            // 
            this.btn_alterar.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btn_alterar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_alterar.Image = global::SWS.Properties.Resources.Alterar;
            this.btn_alterar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btn_alterar.Location = new System.Drawing.Point(91, 429);
            this.btn_alterar.Name = "btn_alterar";
            this.btn_alterar.Size = new System.Drawing.Size(75, 23);
            this.btn_alterar.TabIndex = 5;
            this.btn_alterar.Text = "&Alterar";
            this.btn_alterar.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btn_alterar.UseVisualStyleBackColor = true;
            this.btn_alterar.Click += new System.EventHandler(this.btn_alterar_Click);
            // 
            // btn_incluirAtividade
            // 
            this.btn_incluirAtividade.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btn_incluirAtividade.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_incluirAtividade.Image = global::SWS.Properties.Resources.fechar_ico;
            this.btn_incluirAtividade.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btn_incluirAtividade.Location = new System.Drawing.Point(10, 429);
            this.btn_incluirAtividade.Name = "btn_incluirAtividade";
            this.btn_incluirAtividade.Size = new System.Drawing.Size(75, 23);
            this.btn_incluirAtividade.TabIndex = 4;
            this.btn_incluirAtividade.Text = "&Incluir";
            this.btn_incluirAtividade.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btn_incluirAtividade.UseVisualStyleBackColor = true;
            this.btn_incluirAtividade.Click += new System.EventHandler(this.btn_incluirAtividade_Click);
            // 
            // tb_hospitais
            // 
            this.tb_hospitais.Controls.Add(this.btn_excluirMedico);
            this.tb_hospitais.Controls.Add(this.btn_excluirHospital);
            this.tb_hospitais.Controls.Add(this.btn_incluirMedico);
            this.tb_hospitais.Controls.Add(this.grb_medico);
            this.tb_hospitais.Controls.Add(this.btn_incluirHospital);
            this.tb_hospitais.Controls.Add(this.grb_hospital);
            this.tb_hospitais.Location = new System.Drawing.Point(4, 22);
            this.tb_hospitais.Name = "tb_hospitais";
            this.tb_hospitais.Padding = new System.Windows.Forms.Padding(3);
            this.tb_hospitais.Size = new System.Drawing.Size(773, 460);
            this.tb_hospitais.TabIndex = 2;
            this.tb_hospitais.Text = "Médicos e Hospitais";
            this.tb_hospitais.UseVisualStyleBackColor = true;
            // 
            // btn_excluirMedico
            // 
            this.btn_excluirMedico.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btn_excluirMedico.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_excluirMedico.Image = ((System.Drawing.Image)(resources.GetObject("btn_excluirMedico.Image")));
            this.btn_excluirMedico.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btn_excluirMedico.Location = new System.Drawing.Point(91, 431);
            this.btn_excluirMedico.Name = "btn_excluirMedico";
            this.btn_excluirMedico.Size = new System.Drawing.Size(75, 23);
            this.btn_excluirMedico.TabIndex = 6;
            this.btn_excluirMedico.Text = "&Excluir";
            this.btn_excluirMedico.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btn_excluirMedico.UseVisualStyleBackColor = true;
            this.btn_excluirMedico.Click += new System.EventHandler(this.btn_excluirMedico_Click);
            // 
            // btn_excluirHospital
            // 
            this.btn_excluirHospital.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btn_excluirHospital.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_excluirHospital.Image = ((System.Drawing.Image)(resources.GetObject("btn_excluirHospital.Image")));
            this.btn_excluirHospital.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btn_excluirHospital.Location = new System.Drawing.Point(91, 178);
            this.btn_excluirHospital.Name = "btn_excluirHospital";
            this.btn_excluirHospital.Size = new System.Drawing.Size(75, 23);
            this.btn_excluirHospital.TabIndex = 3;
            this.btn_excluirHospital.Text = "&Excluir";
            this.btn_excluirHospital.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btn_excluirHospital.UseVisualStyleBackColor = true;
            this.btn_excluirHospital.Click += new System.EventHandler(this.btn_excluirHospital_Click);
            // 
            // btn_incluirMedico
            // 
            this.btn_incluirMedico.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btn_incluirMedico.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_incluirMedico.Image = ((System.Drawing.Image)(resources.GetObject("btn_incluirMedico.Image")));
            this.btn_incluirMedico.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btn_incluirMedico.Location = new System.Drawing.Point(11, 431);
            this.btn_incluirMedico.Name = "btn_incluirMedico";
            this.btn_incluirMedico.Size = new System.Drawing.Size(75, 23);
            this.btn_incluirMedico.TabIndex = 5;
            this.btn_incluirMedico.Text = "&Incluir";
            this.btn_incluirMedico.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btn_incluirMedico.UseVisualStyleBackColor = true;
            this.btn_incluirMedico.Click += new System.EventHandler(this.btn_incluirMedico_Click);
            // 
            // grb_medico
            // 
            this.grb_medico.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.grb_medico.Controls.Add(this.grd_medico);
            this.grb_medico.Location = new System.Drawing.Point(7, 207);
            this.grb_medico.Name = "grb_medico";
            this.grb_medico.Size = new System.Drawing.Size(760, 218);
            this.grb_medico.TabIndex = 1;
            this.grb_medico.TabStop = false;
            this.grb_medico.Text = "Médicos";
            // 
            // grd_medico
            // 
            this.grd_medico.AllowUserToAddRows = false;
            this.grd_medico.AllowUserToDeleteRows = false;
            this.grd_medico.AllowUserToOrderColumns = true;
            this.grd_medico.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.DisplayedCells;
            this.grd_medico.BackgroundColor = System.Drawing.Color.White;
            this.grd_medico.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dataGridViewCellStyle8.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle8.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle8.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle8.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle8.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle8.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle8.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.grd_medico.DefaultCellStyle = dataGridViewCellStyle8;
            this.grd_medico.Dock = System.Windows.Forms.DockStyle.Fill;
            this.grd_medico.Location = new System.Drawing.Point(3, 16);
            this.grd_medico.MultiSelect = false;
            this.grd_medico.Name = "grd_medico";
            this.grd_medico.ReadOnly = true;
            this.grd_medico.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.grd_medico.Size = new System.Drawing.Size(754, 199);
            this.grd_medico.TabIndex = 4;
            // 
            // btn_incluirHospital
            // 
            this.btn_incluirHospital.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btn_incluirHospital.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_incluirHospital.Image = ((System.Drawing.Image)(resources.GetObject("btn_incluirHospital.Image")));
            this.btn_incluirHospital.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btn_incluirHospital.Location = new System.Drawing.Point(10, 178);
            this.btn_incluirHospital.Name = "btn_incluirHospital";
            this.btn_incluirHospital.Size = new System.Drawing.Size(75, 23);
            this.btn_incluirHospital.TabIndex = 2;
            this.btn_incluirHospital.Text = "&Incluir";
            this.btn_incluirHospital.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btn_incluirHospital.UseVisualStyleBackColor = true;
            this.btn_incluirHospital.Click += new System.EventHandler(this.btn_incluirHospital_Click);
            // 
            // grb_hospital
            // 
            this.grb_hospital.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.grb_hospital.Controls.Add(this.grd_hospital);
            this.grb_hospital.Location = new System.Drawing.Point(7, 7);
            this.grb_hospital.Name = "grb_hospital";
            this.grb_hospital.Size = new System.Drawing.Size(760, 165);
            this.grb_hospital.TabIndex = 0;
            this.grb_hospital.TabStop = false;
            this.grb_hospital.Text = "Hospitais";
            // 
            // grd_hospital
            // 
            this.grd_hospital.AllowUserToAddRows = false;
            this.grd_hospital.AllowUserToDeleteRows = false;
            this.grd_hospital.AllowUserToOrderColumns = true;
            this.grd_hospital.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.DisplayedCells;
            this.grd_hospital.BackgroundColor = System.Drawing.Color.White;
            this.grd_hospital.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dataGridViewCellStyle9.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle9.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle9.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle9.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle9.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle9.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle9.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.grd_hospital.DefaultCellStyle = dataGridViewCellStyle9;
            this.grd_hospital.Dock = System.Windows.Forms.DockStyle.Fill;
            this.grd_hospital.Location = new System.Drawing.Point(3, 16);
            this.grd_hospital.MultiSelect = false;
            this.grd_hospital.Name = "grd_hospital";
            this.grd_hospital.ReadOnly = true;
            this.grd_hospital.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.grd_hospital.Size = new System.Drawing.Size(754, 146);
            this.grd_hospital.TabIndex = 1;
            // 
            // tb_equipamento
            // 
            this.tb_equipamento.Controls.Add(this.btn_excluirMaterial);
            this.tb_equipamento.Controls.Add(this.grb_paginacao);
            this.tb_equipamento.Controls.Add(this.btn_incluirMaterial);
            this.tb_equipamento.Controls.Add(this.grb_material);
            this.tb_equipamento.Location = new System.Drawing.Point(4, 22);
            this.tb_equipamento.Name = "tb_equipamento";
            this.tb_equipamento.Padding = new System.Windows.Forms.Padding(3);
            this.tb_equipamento.Size = new System.Drawing.Size(773, 460);
            this.tb_equipamento.TabIndex = 3;
            this.tb_equipamento.Text = "Material Hospitalar";
            this.tb_equipamento.UseVisualStyleBackColor = true;
            // 
            // btn_excluirMaterial
            // 
            this.btn_excluirMaterial.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btn_excluirMaterial.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_excluirMaterial.Image = global::SWS.Properties.Resources.lixeira;
            this.btn_excluirMaterial.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btn_excluirMaterial.Location = new System.Drawing.Point(90, 375);
            this.btn_excluirMaterial.Name = "btn_excluirMaterial";
            this.btn_excluirMaterial.Size = new System.Drawing.Size(75, 23);
            this.btn_excluirMaterial.TabIndex = 2;
            this.btn_excluirMaterial.Text = "&Excluir";
            this.btn_excluirMaterial.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btn_excluirMaterial.UseVisualStyleBackColor = true;
            this.btn_excluirMaterial.Click += new System.EventHandler(this.btn_excluirMaterial_Click);
            // 
            // grb_paginacao
            // 
            this.grb_paginacao.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.grb_paginacao.Controls.Add(this.btn_salvarMaterial);
            this.grb_paginacao.Location = new System.Drawing.Point(6, 404);
            this.grb_paginacao.Name = "grb_paginacao";
            this.grb_paginacao.Size = new System.Drawing.Size(761, 50);
            this.grb_paginacao.TabIndex = 4;
            this.grb_paginacao.TabStop = false;
            // 
            // btn_salvarMaterial
            // 
            this.btn_salvarMaterial.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_salvarMaterial.Image = global::SWS.Properties.Resources.Gravar;
            this.btn_salvarMaterial.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btn_salvarMaterial.Location = new System.Drawing.Point(3, 19);
            this.btn_salvarMaterial.Name = "btn_salvarMaterial";
            this.btn_salvarMaterial.Size = new System.Drawing.Size(75, 23);
            this.btn_salvarMaterial.TabIndex = 4;
            this.btn_salvarMaterial.Text = "&Salvar";
            this.btn_salvarMaterial.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btn_salvarMaterial.UseVisualStyleBackColor = true;
            this.btn_salvarMaterial.Click += new System.EventHandler(this.btn_salvarMaterial_Click);
            // 
            // btn_incluirMaterial
            // 
            this.btn_incluirMaterial.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btn_incluirMaterial.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_incluirMaterial.Image = global::SWS.Properties.Resources.fechar_ico;
            this.btn_incluirMaterial.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btn_incluirMaterial.Location = new System.Drawing.Point(9, 375);
            this.btn_incluirMaterial.Name = "btn_incluirMaterial";
            this.btn_incluirMaterial.Size = new System.Drawing.Size(75, 23);
            this.btn_incluirMaterial.TabIndex = 1;
            this.btn_incluirMaterial.Text = "&Incluir";
            this.btn_incluirMaterial.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btn_incluirMaterial.UseVisualStyleBackColor = true;
            this.btn_incluirMaterial.Click += new System.EventHandler(this.btn_incluirMaterial_Click);
            // 
            // grb_material
            // 
            this.grb_material.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.grb_material.Controls.Add(this.grd_material);
            this.grb_material.Location = new System.Drawing.Point(6, 6);
            this.grb_material.Name = "grb_material";
            this.grb_material.Size = new System.Drawing.Size(761, 363);
            this.grb_material.TabIndex = 0;
            this.grb_material.TabStop = false;
            this.grb_material.Text = "Materiais Hospitalares";
            // 
            // grd_material
            // 
            this.grd_material.AllowUserToAddRows = false;
            this.grd_material.AllowUserToDeleteRows = false;
            this.grd_material.AllowUserToOrderColumns = true;
            this.grd_material.AllowUserToResizeRows = false;
            this.grd_material.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this.grd_material.BackgroundColor = System.Drawing.Color.White;
            this.grd_material.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dataGridViewCellStyle10.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle10.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle10.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle10.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle10.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle10.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle10.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.grd_material.DefaultCellStyle = dataGridViewCellStyle10;
            this.grd_material.Dock = System.Windows.Forms.DockStyle.Fill;
            this.grd_material.Location = new System.Drawing.Point(3, 16);
            this.grd_material.MultiSelect = false;
            this.grd_material.Name = "grd_material";
            this.grd_material.Size = new System.Drawing.Size(755, 344);
            this.grd_material.TabIndex = 3;
            this.grd_material.EditingControlShowing += new System.Windows.Forms.DataGridViewEditingControlShowingEventHandler(this.grd_material_EditingControlShowing);
            this.grd_material.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.grd_material_KeyPress);
            // 
            // tb_anexo
            // 
            this.tb_anexo.Controls.Add(this.lbl_anexo);
            this.tb_anexo.Controls.Add(this.btn_excluirAnexo);
            this.tb_anexo.Controls.Add(this.btn_incluirAnexo);
            this.tb_anexo.Controls.Add(this.grb_anexo);
            this.tb_anexo.Controls.Add(this.text_conteudo);
            this.tb_anexo.Location = new System.Drawing.Point(4, 22);
            this.tb_anexo.Name = "tb_anexo";
            this.tb_anexo.Padding = new System.Windows.Forms.Padding(3);
            this.tb_anexo.Size = new System.Drawing.Size(773, 460);
            this.tb_anexo.TabIndex = 4;
            this.tb_anexo.Text = "Anexos";
            this.tb_anexo.UseVisualStyleBackColor = true;
            // 
            // lbl_anexo
            // 
            this.lbl_anexo.AutoSize = true;
            this.lbl_anexo.Location = new System.Drawing.Point(6, 4);
            this.lbl_anexo.Name = "lbl_anexo";
            this.lbl_anexo.Size = new System.Drawing.Size(55, 13);
            this.lbl_anexo.TabIndex = 5;
            this.lbl_anexo.Text = "Descrição";
            // 
            // btn_excluirAnexo
            // 
            this.btn_excluirAnexo.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btn_excluirAnexo.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_excluirAnexo.Image = global::SWS.Properties.Resources.lixeira;
            this.btn_excluirAnexo.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btn_excluirAnexo.Location = new System.Drawing.Point(9, 415);
            this.btn_excluirAnexo.Name = "btn_excluirAnexo";
            this.btn_excluirAnexo.Size = new System.Drawing.Size(75, 23);
            this.btn_excluirAnexo.TabIndex = 4;
            this.btn_excluirAnexo.Text = "Excluir";
            this.btn_excluirAnexo.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btn_excluirAnexo.UseVisualStyleBackColor = true;
            this.btn_excluirAnexo.Click += new System.EventHandler(this.btn_excluirAnexo_Click);
            // 
            // btn_incluirAnexo
            // 
            this.btn_incluirAnexo.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_incluirAnexo.Image = global::SWS.Properties.Resources.fechar_ico;
            this.btn_incluirAnexo.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btn_incluirAnexo.Location = new System.Drawing.Point(670, 18);
            this.btn_incluirAnexo.Name = "btn_incluirAnexo";
            this.btn_incluirAnexo.Size = new System.Drawing.Size(75, 23);
            this.btn_incluirAnexo.TabIndex = 2;
            this.btn_incluirAnexo.Text = "Incluir";
            this.btn_incluirAnexo.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btn_incluirAnexo.UseVisualStyleBackColor = true;
            this.btn_incluirAnexo.Click += new System.EventHandler(this.btn_incluirAnexo_Click);
            // 
            // grb_anexo
            // 
            this.grb_anexo.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.grb_anexo.Controls.Add(this.grd_anexo);
            this.grb_anexo.Location = new System.Drawing.Point(6, 47);
            this.grb_anexo.Name = "grb_anexo";
            this.grb_anexo.Size = new System.Drawing.Size(760, 362);
            this.grb_anexo.TabIndex = 0;
            this.grb_anexo.TabStop = false;
            this.grb_anexo.Text = "Anexos";
            // 
            // grd_anexo
            // 
            this.grd_anexo.AllowUserToAddRows = false;
            this.grd_anexo.AllowUserToDeleteRows = false;
            this.grd_anexo.AllowUserToOrderColumns = true;
            this.grd_anexo.AllowUserToResizeRows = false;
            this.grd_anexo.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.DisplayedCells;
            this.grd_anexo.BackgroundColor = System.Drawing.Color.White;
            this.grd_anexo.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dataGridViewCellStyle11.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle11.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle11.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle11.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle11.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle11.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle11.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.grd_anexo.DefaultCellStyle = dataGridViewCellStyle11;
            this.grd_anexo.Dock = System.Windows.Forms.DockStyle.Fill;
            this.grd_anexo.Location = new System.Drawing.Point(3, 16);
            this.grd_anexo.Name = "grd_anexo";
            this.grd_anexo.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.grd_anexo.Size = new System.Drawing.Size(754, 343);
            this.grd_anexo.TabIndex = 3;
            this.grd_anexo.CellEndEdit += new System.Windows.Forms.DataGridViewCellEventHandler(this.grd_anexo_CellEndEdit);
            this.grd_anexo.EditingControlShowing += new System.Windows.Forms.DataGridViewEditingControlShowingEventHandler(this.grd_anexo_EditingControlShowing);
            // 
            // text_conteudo
            // 
            this.text_conteudo.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.text_conteudo.Location = new System.Drawing.Point(7, 21);
            this.text_conteudo.MaxLength = 100;
            this.text_conteudo.Name = "text_conteudo";
            this.text_conteudo.Size = new System.Drawing.Size(657, 20);
            this.text_conteudo.TabIndex = 1;
            // 
            // btn_fechar
            // 
            this.btn_fechar.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btn_fechar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_fechar.Image = global::SWS.Properties.Resources.close;
            this.btn_fechar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btn_fechar.Location = new System.Drawing.Point(712, 492);
            this.btn_fechar.Name = "btn_fechar";
            this.btn_fechar.Size = new System.Drawing.Size(68, 23);
            this.btn_fechar.TabIndex = 8;
            this.btn_fechar.Text = "&Fechar";
            this.btn_fechar.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btn_fechar.UseVisualStyleBackColor = true;
            this.btn_fechar.Click += new System.EventHandler(this.btn_voltar_Click);
            // 
            // frm_PcmsoExames
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(794, 568);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = true;
            this.Name = "frm_PcmsoExames";
            this.Text = "INCLUIR EXAMES PCMSO";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.frm_PcmsoExames_Load);
            this.panel_banner.ResumeLayout(false);
            this.panel_banner.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox_banner)).EndInit();
            this.panel.ResumeLayout(false);
            this.tb_pcmso.ResumeLayout(false);
            this.pg_pcmsoExame.ResumeLayout(false);
            this.grb_periodicidade.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.grd_periodicidade)).EndInit();
            this.grb_exames.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.grd_exame)).EndInit();
            this.grb_risco.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.grd_risco)).EndInit();
            this.grb_funcao.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.grd_funcao)).EndInit();
            this.grb_ghe.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.grd_ghe)).EndInit();
            this.tb_cronograma.ResumeLayout(false);
            this.tb_cronograma.PerformLayout();
            this.grb_atividade.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.grd_atividade)).EndInit();
            this.tb_hospitais.ResumeLayout(false);
            this.grb_medico.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.grd_medico)).EndInit();
            this.grb_hospital.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.grd_hospital)).EndInit();
            this.tb_equipamento.ResumeLayout(false);
            this.grb_paginacao.ResumeLayout(false);
            this.grb_material.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.grd_material)).EndInit();
            this.tb_anexo.ResumeLayout(false);
            this.tb_anexo.PerformLayout();
            this.grb_anexo.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.grd_anexo)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabControl tb_pcmso;
        private System.Windows.Forms.TabPage pg_pcmsoExame;
        private System.Windows.Forms.GroupBox grb_ghe;
        private System.Windows.Forms.DataGridView grd_ghe;
        private System.Windows.Forms.TabPage tb_cronograma;
        private System.Windows.Forms.Button btn_nota;
        private System.Windows.Forms.GroupBox grb_exames;
        private System.Windows.Forms.Button btn_fechar;
        private System.Windows.Forms.Button btn_excluir;
        private System.Windows.Forms.Button btn_incluir;
        private System.Windows.Forms.DataGridView grd_exame;
        private System.Windows.Forms.GroupBox grb_risco;
        private System.Windows.Forms.DataGridView grd_risco;
        private System.Windows.Forms.GroupBox grb_funcao;
        private System.Windows.Forms.DataGridView grd_funcao;
        private System.Windows.Forms.GroupBox grb_periodicidade;
        private System.Windows.Forms.DataGridView grd_periodicidade;
        private System.Windows.Forms.TextBox text_descricao;
        private System.Windows.Forms.Label lbl_descricao;
        private System.Windows.Forms.GroupBox grb_atividade;
        private System.Windows.Forms.DataGridView grd_atividade;
        private System.Windows.Forms.Button btn_alterar;
        private System.Windows.Forms.Button btn_incluirAtividade;
        private System.Windows.Forms.Button btn_excluirAtividade;
        private System.Windows.Forms.TabPage tb_hospitais;
        private System.Windows.Forms.TabPage tb_equipamento;
        private System.Windows.Forms.GroupBox grb_paginacao;
        private System.Windows.Forms.Button btn_salvarMaterial;
        private System.Windows.Forms.GroupBox grb_material;
        private System.Windows.Forms.Button btn_excluirMaterial;
        private System.Windows.Forms.Button btn_incluirMaterial;
        private System.Windows.Forms.DataGridView grd_material;
        private System.Windows.Forms.GroupBox grb_medico;
        private System.Windows.Forms.Button btn_excluirMedico;
        private System.Windows.Forms.Button btn_incluirMedico;
        protected System.Windows.Forms.DataGridView grd_medico;
        private System.Windows.Forms.GroupBox grb_hospital;
        private System.Windows.Forms.Button btn_excluirHospital;
        private System.Windows.Forms.Button btn_incluirHospital;
        protected System.Windows.Forms.DataGridView grd_hospital;
        private System.Windows.Forms.TabPage tb_anexo;
        private System.Windows.Forms.GroupBox grb_anexo;
        private System.Windows.Forms.DataGridView grd_anexo;
        private System.Windows.Forms.Button btn_incluirAnexo;
        private System.Windows.Forms.TextBox text_conteudo;
        private System.Windows.Forms.Button btn_excluirAnexo;
        private System.Windows.Forms.Button btn_exame_alterar;
        private System.Windows.Forms.Label lbl_anexo;

    }
}