﻿using SWS.Entidade;
using SWS.Facade;
using SWS.View.Resources;
using SWS.View.ViewHelper;
using SWS.ViewHelper;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Windows.Forms;

namespace SWS.View
{
    public partial class frmVendedorIncluir : frmTemplate
    {
        
        protected Vendedor vendedor;
        protected CidadeIbge cidade;

        public Vendedor Vendedor
        {
            get { return vendedor; }
            set { vendedor = value; }
        }
        
        public frmVendedorIncluir()
        {
            InitializeComponent();
            ComboHelper.unidadeFederativa(cbUf);
            ActiveControl = textNome;
        }

        protected void btnFechar_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        protected void btnLimpar_Click(object sender, EventArgs e)
        {
            this.errorIncluirVendedor.Clear();
            this.textNome.Text = string.Empty;
            this.textCpf.Text = string.Empty;
            this.textRG.Text = string.Empty;
            this.textEndereco.Text = string.Empty;
            this.textNumero.Text = string.Empty;
            this.textComplemento.Text = string.Empty;
            this.textBairro.Text = string.Empty;
            ComboHelper.unidadeFederativa(cbUf);
            cidade = null;
            textCidade.Text = string.Empty;
            textCep.Text = string.Empty;
            textEmail.Text = string.Empty;
            textTelefone.Text = string.Empty;
            textCelular.Text = string.Empty;
            textComissao.Text = "0.00";
            
        }

        protected virtual void btnGravar_Click(object sender, EventArgs e)
        {
            try
            {
                this.errorIncluirVendedor.Clear();
                VendedorFacade vendedorFacade = VendedorFacade.getInstance();

                if (!this.validaCamposObrigatorio())
                    throw new Exception("Reveja os campos obrigatórios.");

                if (!validaCpf())
                    throw new Exception("Reveja o valor digitado do cpf.");

                if (!ValidaCampoHelper.ValidaCepDigitado(textCep.Text))
                    throw new Exception("Reveja o CEP digitado");


                /* verificando se o cpf já foi utilizado no sistema */
                if (vendedorFacade.findVendedorByCpf(this.textCpf.Text.Replace(".", "").Replace(",", "").Replace("-", "")) != null)
                    throw new Exception("Cpf já utilizado em outro Vendedor.");


                this.Vendedor = new Vendedor(null, textNome.Text, textCpf.Text, textRG.Text, textEndereco.Text, textNumero.Text, textComplemento.Text, textBairro.Text, textCidade.Text, Convert.ToString(cbUf.SelectedValue), textCep.Text, textTelefone.Text, textCelular.Text, textEmail.Text, Convert.ToDecimal(textComissao.Text.Replace(".", ",")), null, ApplicationConstants.ATIVO, cidade);

                vendedor = vendedorFacade.insertVendedor(vendedor);

                MessageBox.Show("Vendedor incluído com sucesso", "Confirmação", MessageBoxButtons.OK, MessageBoxIcon.Information);

                this.Close();

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }

        protected void btCidade_Click(object sender, EventArgs e)
        {
            try
            {
                if (this.cbUf.SelectedIndex == 0)
                    throw new Exception("Você deve primeiro selecionar a unidade federativa.");

                frmCidadePesquisa cidadeSeleciona = new frmCidadePesquisa(((SelectItem)this.cbUf.SelectedItem).Valor.ToString());
                cidadeSeleciona.ShowDialog();

                if (cidadeSeleciona.Cidade != null)
                {
                    cidade = cidadeSeleciona.Cidade;
                    textCidade.Text = cidade.Nome;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }

        protected bool validaCamposObrigatorio()
        {
            bool retorno = true;

            if (string.IsNullOrEmpty(textNome.Text))
            {
                this.errorIncluirVendedor.SetError(this.textNome, "O Nome é obrigatório!");
                retorno = false;
            }

            if (string.IsNullOrEmpty(textCpf.Text.Replace(".", "").Replace(",", "").Replace("-", "")))
            {
                this.errorIncluirVendedor.SetError(this.textCpf, "O CPF é obrigatório!");
                retorno = false;
            }

            if (string.IsNullOrEmpty(this.textRG.Text))
            {
                this.errorIncluirVendedor.SetError(this.textRG, "O RG é obrigatório!");
                retorno = false;
            }

            if (string.IsNullOrEmpty(textComissao.Text))
            {
                this.errorIncluirVendedor.SetError(this.textComissao, "Especificar o valor da comissao padrão!");
                retorno = false;
            }

            return retorno;
        }

        protected bool validaCpf()
        {
            bool retorno = true;

            if (Regex.IsMatch(Convert.ToString(this.textCpf), @"\d"))
            {
                if (!ValidaCampoHelper.ValidaCPF(this.textCpf.Text))
                {
                    this.errorIncluirVendedor.SetError(this.textCpf, "CPF inválido");
                    retorno = false;
                }
            }

            return retorno;
        }

        protected void textComissao_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = ValidaCampoHelper.FormataDigitacaoNumero(sender, e, textComissao.Text, 2);
        }

        protected void frmVendedorIncluir_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter || e.KeyCode == Keys.Return)
                SendKeys.Send("{TAB}");
        }

        protected void textComissao_Leave(object sender, EventArgs e)
        {
            try
            {
                // verificando valor digitado pelo usuário para não permitir percentual maior que 99,99

                if (string.IsNullOrEmpty(textComissao.Text))
                    textComissao.Text = textComissao.Tag.ToString();

                string texto = textComissao.Text.TrimStart('0');
                int tamanhotexto = texto.Length;

                if (Convert.ToDecimal(texto) > Convert.ToDecimal("99,99"))
                {
                    textComissao.Focus();
                    throw new Exception("maior comissão permitida: 99,99%");
                }

                textComissao.Text = ValidaCampoHelper.FormataValorMonetario(textComissao.Text);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }

        protected virtual void textComissao_Enter(object sender, EventArgs e)
        {
            textComissao.Tag = textComissao.Text;
            textComissao.Text = String.Empty;
        }
    }
}
