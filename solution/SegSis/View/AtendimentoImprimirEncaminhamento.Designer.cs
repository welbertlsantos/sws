﻿namespace SWS.View
{
    partial class frmAtendimentoImprimirEncaminhamento
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.flpAcao = new System.Windows.Forms.FlowLayoutPanel();
            this.btn_sim = new System.Windows.Forms.Button();
            this.btn_nao = new System.Windows.Forms.Button();
            this.text_quantidadeVia = new System.Windows.Forms.TextBox();
            this.lbl_imprimir_encaminhamento = new System.Windows.Forms.Label();
            this.lblQuantidade = new System.Windows.Forms.TextBox();
            this.lblTipo = new System.Windows.Forms.TextBox();
            this.cbTipoImpressora = new SWS.ComboBoxWithBorder();
            ((System.ComponentModel.ISupportInitialize)(this.scForm)).BeginInit();
            this.scForm.Panel1.SuspendLayout();
            this.scForm.Panel2.SuspendLayout();
            this.scForm.SuspendLayout();
            this.pnlForm.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.banner)).BeginInit();
            this.flpAcao.SuspendLayout();
            this.SuspendLayout();
            // 
            // scForm
            // 
            // 
            // scForm.Panel1
            // 
            this.scForm.Panel1.Controls.Add(this.flpAcao);
            // 
            // pnlForm
            // 
            this.pnlForm.Controls.Add(this.cbTipoImpressora);
            this.pnlForm.Controls.Add(this.lblTipo);
            this.pnlForm.Controls.Add(this.lblQuantidade);
            this.pnlForm.Controls.Add(this.text_quantidadeVia);
            this.pnlForm.Controls.Add(this.lbl_imprimir_encaminhamento);
            // 
            // flpAcao
            // 
            this.flpAcao.Controls.Add(this.btn_sim);
            this.flpAcao.Controls.Add(this.btn_nao);
            this.flpAcao.Dock = System.Windows.Forms.DockStyle.Fill;
            this.flpAcao.Location = new System.Drawing.Point(0, 0);
            this.flpAcao.Name = "flpAcao";
            this.flpAcao.Size = new System.Drawing.Size(534, 30);
            this.flpAcao.TabIndex = 0;
            // 
            // btn_sim
            // 
            this.btn_sim.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_sim.Image = global::SWS.Properties.Resources.fechar_ico;
            this.btn_sim.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btn_sim.Location = new System.Drawing.Point(3, 3);
            this.btn_sim.Name = "btn_sim";
            this.btn_sim.Size = new System.Drawing.Size(75, 23);
            this.btn_sim.TabIndex = 1;
            this.btn_sim.Text = "&Sim";
            this.btn_sim.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btn_sim.UseVisualStyleBackColor = true;
            this.btn_sim.Click += new System.EventHandler(this.btn_sim_Click);
            // 
            // btn_nao
            // 
            this.btn_nao.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_nao.Image = global::SWS.Properties.Resources.close;
            this.btn_nao.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btn_nao.Location = new System.Drawing.Point(84, 3);
            this.btn_nao.Name = "btn_nao";
            this.btn_nao.Size = new System.Drawing.Size(75, 23);
            this.btn_nao.TabIndex = 5;
            this.btn_nao.TabStop = false;
            this.btn_nao.Text = "&Não";
            this.btn_nao.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btn_nao.UseVisualStyleBackColor = true;
            this.btn_nao.Click += new System.EventHandler(this.btn_nao_Click);
            // 
            // text_quantidadeVia
            // 
            this.text_quantidadeVia.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.text_quantidadeVia.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.text_quantidadeVia.ForeColor = System.Drawing.Color.Blue;
            this.text_quantidadeVia.Location = new System.Drawing.Point(135, 59);
            this.text_quantidadeVia.MaxLength = 2;
            this.text_quantidadeVia.Name = "text_quantidadeVia";
            this.text_quantidadeVia.Size = new System.Drawing.Size(350, 21);
            this.text_quantidadeVia.TabIndex = 3;
            this.text_quantidadeVia.Enter += new System.EventHandler(this.text_quantidadeVia_Enter);
            this.text_quantidadeVia.Leave += new System.EventHandler(this.text_quantidadeVia_Leave);
            // 
            // lbl_imprimir_encaminhamento
            // 
            this.lbl_imprimir_encaminhamento.AutoSize = true;
            this.lbl_imprimir_encaminhamento.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_imprimir_encaminhamento.Location = new System.Drawing.Point(12, 15);
            this.lbl_imprimir_encaminhamento.Name = "lbl_imprimir_encaminhamento";
            this.lbl_imprimir_encaminhamento.Size = new System.Drawing.Size(282, 13);
            this.lbl_imprimir_encaminhamento.TabIndex = 4;
            this.lbl_imprimir_encaminhamento.Text = "Deseja imprimir o encaminhamento? Se sim escolha o tipo.";
            // 
            // lblQuantidade
            // 
            this.lblQuantidade.BackColor = System.Drawing.Color.LightGray;
            this.lblQuantidade.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblQuantidade.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblQuantidade.ForeColor = System.Drawing.Color.Black;
            this.lblQuantidade.Location = new System.Drawing.Point(15, 59);
            this.lblQuantidade.MaxLength = 2;
            this.lblQuantidade.Name = "lblQuantidade";
            this.lblQuantidade.Size = new System.Drawing.Size(121, 21);
            this.lblQuantidade.TabIndex = 9;
            this.lblQuantidade.TabStop = false;
            this.lblQuantidade.Text = "Quantidade de vias";
            // 
            // lblTipo
            // 
            this.lblTipo.BackColor = System.Drawing.Color.LightGray;
            this.lblTipo.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblTipo.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTipo.ForeColor = System.Drawing.Color.Black;
            this.lblTipo.Location = new System.Drawing.Point(15, 39);
            this.lblTipo.MaxLength = 2;
            this.lblTipo.Name = "lblTipo";
            this.lblTipo.Size = new System.Drawing.Size(121, 21);
            this.lblTipo.TabIndex = 10;
            this.lblTipo.TabStop = false;
            this.lblTipo.Text = "Tipo de Impressora";
            // 
            // cbTipoImpressora
            // 
            this.cbTipoImpressora.BackColor = System.Drawing.Color.Gainsboro;
            this.cbTipoImpressora.BorderColor = System.Drawing.Color.Black;
            this.cbTipoImpressora.BorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid;
            this.cbTipoImpressora.FormattingEnabled = true;
            this.cbTipoImpressora.Location = new System.Drawing.Point(135, 39);
            this.cbTipoImpressora.Name = "cbTipoImpressora";
            this.cbTipoImpressora.Size = new System.Drawing.Size(350, 21);
            this.cbTipoImpressora.TabIndex = 2;
            // 
            // frmAtendimentoImprimirEncaminhamento
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(534, 361);
            this.Name = "frmAtendimentoImprimirEncaminhamento";
            this.Text = "IMPRIMIR ENCAMINHAMENTO DE EXAMES";
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.frmAtendimentoImprimirEncaminhamento_KeyDown);
            this.scForm.Panel1.ResumeLayout(false);
            this.scForm.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.scForm)).EndInit();
            this.scForm.ResumeLayout(false);
            this.pnlForm.ResumeLayout(false);
            this.pnlForm.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.banner)).EndInit();
            this.flpAcao.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.FlowLayoutPanel flpAcao;
        private System.Windows.Forms.Button btn_nao;
        private System.Windows.Forms.Button btn_sim;
        private ComboBoxWithBorder cbTipoImpressora;
        private System.Windows.Forms.TextBox lblTipo;
        private System.Windows.Forms.TextBox lblQuantidade;
        private System.Windows.Forms.TextBox text_quantidadeVia;
        private System.Windows.Forms.Label lbl_imprimir_encaminhamento;
    }
}