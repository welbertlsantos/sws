﻿using SWS.Entidade;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace SWS.View
{
    public partial class frmAtendimentoIncluirExame : frmTemplateConsulta
    {
        List<GheFonteAgenteExameAso> examesPcmso = new List<GheFonteAgenteExameAso>();
        List<ClienteFuncaoExameASo> examesExtras = new List<ClienteFuncaoExameASo>();
        
        public frmAtendimentoIncluirExame(List<GheFonteAgenteExameAso> examesPcmso, List<ClienteFuncaoExameASo> examesExtras)
        {
            InitializeComponent();
            this.examesPcmso = examesPcmso;
            this.examesExtras = examesExtras;
            montaGridExame();
        }

        public void montaGridExame()
        {
            try
            {
                dgvExames.ColumnCount = 4;
                
                dgvExames.Columns[0].HeaderText = "Exame";
                dgvExames.Columns[1].HeaderText = "Código TUSS";
                dgvExames.Columns[2].HeaderText = "Data Exame";
                dgvExames.Columns[3].HeaderText = "Data Vencimento";

                DataGridViewCheckBoxColumn transcrito = new DataGridViewCheckBoxColumn();
                transcrito.Name = "exameTranscrito";
                transcrito.HeaderText = "Transcrito";
                dgvExames.Columns.Add(transcrito);


                examesPcmso.ForEach(delegate(GheFonteAgenteExameAso exames)
                {
                    dgvExames.Rows.Add(
                        exames.GheFonteAgenteExame.Exame.Descricao,
                        exames.GheFonteAgenteExame.Exame.CodigoTuss,
                        string.Format("{0:dd/MM/yyyy}", exames.DataExame),
                        string.Format("{0:dd/MM/yyyy}", exames.DataValidade),
                        exames.Transcrito);
                });

                examesExtras.ForEach(delegate(ClienteFuncaoExameASo exames)
                {
                    dgvExames.Rows.Add(
                        exames.ClienteFuncaoExame.Exame.Descricao,
                        exames.ClienteFuncaoExame.Exame.CodigoTuss,
                        string.Format("{0:dd/MM/yyyy}", exames.DataExame),
                        string.Format("{0:dd/MM/yyyy}", exames.DataValidade),
                        exames.Transcrito);

                });

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
                
        }

        private void btnFechar_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void dgvExames_RowPrePaint(object sender, DataGridViewRowPrePaintEventArgs e)
        {
            DataGridView dgv = sender as DataGridView;

            if (Convert.ToBoolean(dgv.Rows[e.RowIndex].Cells[4].Value) == true)
                dgv.Rows[e.RowIndex].DefaultCellStyle.ForeColor = Color.Green;

        }


    }
}
