﻿using SWS.Entidade;
using SWS.View.Resources;
using SWS.View.ViewHelper;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace SWS.View
{
    public partial class frmNotaFiscalConsulta : frmTemplate
    {
        private Nfe notaFiscal;
        
        public frmNotaFiscalConsulta(Nfe notaFiscal)
        {
            InitializeComponent();
            this.notaFiscal = notaFiscal;
            mostrarCabecalho();
            mostrarTotais();
            montaDataGridItens();
            montaDataGridCobranca();

        }

        private void montaDataGridCobranca()
        {
            try
            {
                dg_cobranca.Columns.Clear();

                dg_cobranca.ColumnCount = 18;

                dg_cobranca.Columns[0].HeaderText = "Numero do Documento";
                dg_cobranca.Columns[1].HeaderText = "Prest.";
                dg_cobranca.Columns[1].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
                dg_cobranca.Columns[1].HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleCenter;

                dg_cobranca.Columns[2].HeaderText = "Data de Emissão";
                dg_cobranca.Columns[2].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
                dg_cobranca.Columns[2].HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleCenter;

                dg_cobranca.Columns[3].HeaderText = "Data de Vencimento";
                dg_cobranca.Columns[3].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
                dg_cobranca.Columns[3].HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleCenter;

                dg_cobranca.Columns[4].HeaderText = "Valor Nominal";
                dg_cobranca.Columns[4].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
                dg_cobranca.Columns[4].HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleRight;

                dg_cobranca.Columns[5].HeaderText = "Data de Pagamento";
                dg_cobranca.Columns[5].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
                dg_cobranca.Columns[5].HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleCenter;

                dg_cobranca.Columns[6].HeaderText = "Data da Baixa";
                dg_cobranca.Columns[6].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
                dg_cobranca.Columns[6].HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleCenter;

                dg_cobranca.Columns[7].HeaderText = "Observação de baixa";
                dg_cobranca.Columns[8].HeaderText = "Forma de Pagamento";

                dg_cobranca.Columns[9].HeaderText = "Banco";
                dg_cobranca.Columns[9].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
                dg_cobranca.Columns[9].HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleCenter;

                dg_cobranca.Columns[10].HeaderText = "Agência";
                dg_cobranca.Columns[11].HeaderText = "Conta Corrente";
                dg_cobranca.Columns[12].HeaderText = "Nosso Número";

                dg_cobranca.Columns[13].HeaderText = "Data Estorno";
                dg_cobranca.Columns[13].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
                dg_cobranca.Columns[13].HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleCenter;

                dg_cobranca.Columns[14].HeaderText = "Observaçao de estorno";

                dg_cobranca.Columns[15].HeaderText = "Data de Alteração";
                dg_cobranca.Columns[15].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
                dg_cobranca.Columns[15].HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleCenter;

                dg_cobranca.Columns[16].HeaderText = "Observação cancelamento";
                dg_cobranca.Columns[17].HeaderText = "Situação";

                notaFiscal.ListaCobranca.ToList().ForEach(delegate(Cobranca cobranca)
                {
                    dg_cobranca.Rows.Add(cobranca.NumeroDocumento, cobranca.NumeroParcela, Convert.ToDateTime(cobranca.DataEmissao).ToShortDateString(), Convert.ToDateTime(cobranca.DataVencimento).ToShortDateString(), ValidaCampoHelper.FormataValorMonetario(cobranca.Valor.ToString()), cobranca.DataPagamento == null ? String.Empty : Convert.ToDateTime(cobranca.DataPagamento).ToShortDateString(), cobranca.DataBaixa == null ? String.Empty : Convert.ToDateTime(cobranca.DataBaixa).ToShortDateString(), cobranca.ObservacaoBaixa, cobranca.Forma.Descricao, cobranca.Conta == null ? String.Empty : cobranca.Conta.Banco.Nome, cobranca.Conta == null ? String.Empty : cobranca.Conta.AgenciaNumero + "-" + cobranca.Conta.AgenciaDigito, cobranca.Conta == null ? String.Empty : cobranca.Conta.ContaNumero + "-" + cobranca.Conta.AgenciaDigito, cobranca.Conta == null ? null : cobranca.Conta.ProximoNumero, cobranca.DataEstorno == null ? String.Empty : Convert.ToDateTime(cobranca.DataEstorno).ToShortDateString(), cobranca.ObservacaoEstorno, cobranca.DataAlteracao == null ? String.Empty : Convert.ToDateTime(cobranca.DataAlteracao).ToShortDateString(), cobranca.ObservacaoCancelamento, cobranca.Situacao);
                });
                
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void montaDataGridItens()
        {
            try
            {
                dg_itens.Columns.Clear();

                dg_itens.ColumnCount = 7;

                dg_itens.Columns[0].HeaderText = "Item";
                dg_itens.Columns[1].HeaderText = "Quantidade";
                dg_itens.Columns[1].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
                dg_itens.Columns[1].HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleCenter;
                dg_itens.Columns[2].HeaderText = "Valor Unitário";
                dg_itens.Columns[2].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
                dg_itens.Columns[2].HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleRight;
                dg_itens.Columns[3].HeaderText = "Valor Total";
                dg_itens.Columns[3].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
                dg_itens.Columns[3].HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleRight;
                dg_itens.Columns[4].HeaderText = "Data Inclusão";
                dg_itens.Columns[4].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
                dg_itens.Columns[4].HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleCenter;
                dg_itens.Columns[5].HeaderText = "Nome do Colaborador";
                dg_itens.Columns[6].HeaderText = "Código do Atendimento";
                dg_itens.Columns[6].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
                dg_itens.Columns[6].HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleCenter;

                notaFiscal.ListaMovimento.ForEach(delegate(Movimento movimento)
                {
                    if (movimento.GheFonteAgenteExameAso != null)
                    {
                        dg_itens.Rows.Add(movimento.GheFonteAgenteExameAso.GheFonteAgenteExame.Exame.Descricao, movimento.Quantidade, ValidaCampoHelper.FormataValorMonetario(movimento.PrecoUnit.ToString()), ValidaCampoHelper.FormataValorMonetario(Convert.ToString(movimento.PrecoUnit * movimento.Quantidade)), Convert.ToDateTime(movimento.DataInclusao).ToShortDateString(), movimento.Aso.ClienteFuncaoFuncionario.Funcionario.Nome, movimento.Aso.Codigo);
                    }
                    else if (movimento.ClienteFuncaoExameAso != null)
                    {
                        dg_itens.Rows.Add(movimento.ClienteFuncaoExameAso.ClienteFuncaoExame.Exame.Descricao, movimento.Quantidade, ValidaCampoHelper.FormataValorMonetario(movimento.PrecoUnit.ToString()), ValidaCampoHelper.FormataValorMonetario(Convert.ToString(movimento.PrecoUnit * movimento.Quantidade)), Convert.ToDateTime(movimento.DataInclusao).ToShortDateString(), movimento.Aso.ClienteFuncaoFuncionario.Funcionario.Nome, movimento.Aso.Codigo);
                    }
                    else 
                    {
                        dg_itens.Rows.Add(movimento.ContratoProduto.Produto.Nome, movimento.Quantidade, ValidaCampoHelper.FormataValorMonetario(movimento.PrecoUnit.ToString()), ValidaCampoHelper.FormataValorMonetario(Convert.ToString(movimento.PrecoUnit * movimento.Quantidade)), Convert.ToDateTime(movimento.DataInclusao).ToShortDateString(), null, null);
                    }

                });

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            
        }

        private void mostrarTotais()
        {
            try
            {
                text_valorTotal.Text = ValidaCampoHelper.FormataValorMonetario(notaFiscal.ValorTotalNf.ToString());
                text_pis.Text = ValidaCampoHelper.FormataValorMonetario(notaFiscal.ValorPis.ToString());
                text_cofins.Text = ValidaCampoHelper.FormataValorMonetario(notaFiscal.ValorCofins.ToString());
                text_baseCalculo.Text = ValidaCampoHelper.FormataValorMonetario(notaFiscal.BaseCalculo.ToString());
                text_aliquotaPercentual.Text = ValidaCampoHelper.FormataValorMonetario(notaFiscal.AliquotaIss.ToString());
                text_valorIss.Text = ValidaCampoHelper.FormataValorMonetario(notaFiscal.ValorIss.ToString());
                text_IR.Text = ValidaCampoHelper.FormataValorMonetario(notaFiscal.ValorIr.ToString());
                text_csll.Text = ValidaCampoHelper.FormataValorMonetario(notaFiscal.ValorCsll.ToString());
                text_valorLiquido.Text = ValidaCampoHelper.FormataValorMonetario(notaFiscal.ValorLiquido.ToString());

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            
        }

        private void mostrarCabecalho()
        {
            try
            {
                text_empresa.Text = notaFiscal.Empresa.RazaoSocial;
                text_cnpj.Text = notaFiscal.Empresa.Cnpj;
                textNumeroNota.Text = notaFiscal.NumeroNfe.ToString();
                text_dtGravacao.Text = notaFiscal.DataGravacao.ToShortDateString();
                text_dtEmissao.Text = notaFiscal.DataEmissao.ToShortDateString();
                text_situacao.Text = string.Equals(ApplicationConstants.FECHADO, notaFiscal.Situacao) ? "FATURADA" : "CANCELADA";
                text_motivoCancelamento.Text = notaFiscal.MotivoCancelamento;

                text_cliente.Text = notaFiscal.Cliente.RazaoSocial;
                text_cnpjCliente.Text = notaFiscal.Cliente.Cnpj;
                text_inscricao.Text = notaFiscal.Cliente.Inscricao;
                text_endereco.Text = notaFiscal.Cliente.Endereco;
                text_numeroCliente.Text = notaFiscal.Cliente.Numero;
                text_complemento.Text = notaFiscal.Cliente.Complemento;
                text_bairro.Text = notaFiscal.Cliente.Bairro;
                text_cidade.Text = notaFiscal.Cliente.CidadeIbge.Nome;
                text_cep.Text = notaFiscal.Cliente.Cep;
                text_uf.Text = notaFiscal.Cliente.Uf;
                text_telefone.Text = notaFiscal.Cliente.Telefone1.ToString() + (string.IsNullOrEmpty(notaFiscal.Cliente.Telefone2) ? "/" + notaFiscal.Cliente.Telefone2 : "");
                text_email.Text = notaFiscal.Cliente.Email;

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btnSair_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
