﻿namespace SegSis.View
{
    partial class frm_SelecionarEstimativa
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frm_SelecionarEstimativa));
            this.grbFiltro = new System.Windows.Forms.GroupBox();
            this.btnBusar = new System.Windows.Forms.Button();
            this.textEstimativa = new System.Windows.Forms.TextBox();
            this.grbEstimativa = new System.Windows.Forms.GroupBox();
            this.dgEstimativa = new System.Windows.Forms.DataGridView();
            this.grbAcao = new System.Windows.Forms.GroupBox();
            this.btnFechar = new System.Windows.Forms.Button();
            this.btnConfirma = new System.Windows.Forms.Button();
            this.grbFiltro.SuspendLayout();
            this.grbEstimativa.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgEstimativa)).BeginInit();
            this.grbAcao.SuspendLayout();
            this.SuspendLayout();
            // 
            // grbFiltro
            // 
            this.grbFiltro.Controls.Add(this.btnBusar);
            this.grbFiltro.Controls.Add(this.textEstimativa);
            this.grbFiltro.Location = new System.Drawing.Point(13, 13);
            this.grbFiltro.Name = "grbFiltro";
            this.grbFiltro.Size = new System.Drawing.Size(575, 51);
            this.grbFiltro.TabIndex = 0;
            this.grbFiltro.TabStop = false;
            this.grbFiltro.Text = "Filtro";
            // 
            // btnBusar
            // 
            this.btnBusar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnBusar.Image = global::SegSis.Properties.Resources.lupa;
            this.btnBusar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnBusar.Location = new System.Drawing.Point(490, 16);
            this.btnBusar.Name = "btnBusar";
            this.btnBusar.Size = new System.Drawing.Size(75, 23);
            this.btnBusar.TabIndex = 1;
            this.btnBusar.Text = "&Buscar";
            this.btnBusar.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnBusar.UseVisualStyleBackColor = true;
            this.btnBusar.Click += new System.EventHandler(this.btnBusar_Click);
            // 
            // textEstimativa
            // 
            this.textEstimativa.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textEstimativa.Location = new System.Drawing.Point(6, 19);
            this.textEstimativa.MaxLength = 100;
            this.textEstimativa.Name = "textEstimativa";
            this.textEstimativa.Size = new System.Drawing.Size(478, 20);
            this.textEstimativa.TabIndex = 0;
            // 
            // grbEstimativa
            // 
            this.grbEstimativa.Controls.Add(this.dgEstimativa);
            this.grbEstimativa.Location = new System.Drawing.Point(13, 71);
            this.grbEstimativa.Name = "grbEstimativa";
            this.grbEstimativa.Size = new System.Drawing.Size(575, 258);
            this.grbEstimativa.TabIndex = 1;
            this.grbEstimativa.TabStop = false;
            this.grbEstimativa.Text = "Estimativa";
            // 
            // dgEstimativa
            // 
            this.dgEstimativa.AllowUserToAddRows = false;
            this.dgEstimativa.AllowUserToDeleteRows = false;
            this.dgEstimativa.AllowUserToOrderColumns = true;
            this.dgEstimativa.AllowUserToResizeColumns = false;
            this.dgEstimativa.AllowUserToResizeRows = false;
            this.dgEstimativa.BackgroundColor = System.Drawing.Color.White;
            this.dgEstimativa.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.LightGray;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgEstimativa.DefaultCellStyle = dataGridViewCellStyle1;
            this.dgEstimativa.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgEstimativa.Location = new System.Drawing.Point(3, 16);
            this.dgEstimativa.Name = "dgEstimativa";
            this.dgEstimativa.RowHeadersVisible = false;
            this.dgEstimativa.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.CellSelect;
            this.dgEstimativa.Size = new System.Drawing.Size(569, 239);
            this.dgEstimativa.TabIndex = 0;
            // 
            // grbAcao
            // 
            this.grbAcao.Controls.Add(this.btnFechar);
            this.grbAcao.Controls.Add(this.btnConfirma);
            this.grbAcao.Location = new System.Drawing.Point(13, 335);
            this.grbAcao.Name = "grbAcao";
            this.grbAcao.Size = new System.Drawing.Size(575, 53);
            this.grbAcao.TabIndex = 2;
            this.grbAcao.TabStop = false;
            this.grbAcao.Text = "Ação";
            // 
            // btnFechar
            // 
            this.btnFechar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnFechar.Image = global::SegSis.Properties.Resources.close;
            this.btnFechar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnFechar.Location = new System.Drawing.Point(87, 19);
            this.btnFechar.Name = "btnFechar";
            this.btnFechar.Size = new System.Drawing.Size(75, 23);
            this.btnFechar.TabIndex = 1;
            this.btnFechar.Text = "&Fechar";
            this.btnFechar.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnFechar.UseVisualStyleBackColor = true;
            this.btnFechar.Click += new System.EventHandler(this.btnFechar_Click);
            // 
            // btnConfirma
            // 
            this.btnConfirma.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnConfirma.Image = global::SegSis.Properties.Resources.fechar_ico;
            this.btnConfirma.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnConfirma.Location = new System.Drawing.Point(6, 19);
            this.btnConfirma.Name = "btnConfirma";
            this.btnConfirma.Size = new System.Drawing.Size(75, 23);
            this.btnConfirma.TabIndex = 0;
            this.btnConfirma.Text = "&Confirmar";
            this.btnConfirma.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnConfirma.UseVisualStyleBackColor = true;
            this.btnConfirma.Click += new System.EventHandler(this.btnConfirma_Click);
            // 
            // frm_SelecionarEstimativa
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(600, 400);
            this.ControlBox = false;
            this.Controls.Add(this.grbAcao);
            this.Controls.Add(this.grbEstimativa);
            this.Controls.Add(this.grbFiltro);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "frm_SelecionarEstimativa";
            this.Text = "SELECIONAR ESTIMATIVA";
            this.grbFiltro.ResumeLayout(false);
            this.grbFiltro.PerformLayout();
            this.grbEstimativa.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgEstimativa)).EndInit();
            this.grbAcao.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox grbFiltro;
        private System.Windows.Forms.Button btnBusar;
        private System.Windows.Forms.TextBox textEstimativa;
        private System.Windows.Forms.GroupBox grbEstimativa;
        private System.Windows.Forms.DataGridView dgEstimativa;
        private System.Windows.Forms.GroupBox grbAcao;
        private System.Windows.Forms.Button btnFechar;
        private System.Windows.Forms.Button btnConfirma;
    }
}