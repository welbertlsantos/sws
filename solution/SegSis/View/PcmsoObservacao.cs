﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SWS.View
{
    public partial class frmPcmsoObservacao : frmTemplateConsulta
    {
        string observacao;

        public string Observacao
        {
            get { return observacao; }
            set { observacao = value; }
        }
        
        public frmPcmsoObservacao()
        {
            InitializeComponent();
            ActiveControl = txtObservacao;
        }

        public frmPcmsoObservacao(string observacao)
        {
            InitializeComponent();
            ActiveControl = txtObservacao;
            this.txtObservacao.Text = observacao;
            this.Text = "ALTERAR OBSERVAÇÃO NO PCMSO";
        }

        private void btIcluirObservacao_Click(object sender, EventArgs e)
        {
            try
            {
                Observacao = txtObservacao.Text.Trim();
                this.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }

        private void btn_fechar_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
