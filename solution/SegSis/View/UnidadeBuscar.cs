﻿using SWS.Entidade;
using SWS.Facade;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace SWS.View
{
    public partial class frmUnidadeBuscar : frmTemplateConsulta
    {
        private Cliente cliente;
        private Cliente unidade;

        public Cliente Unidade
        {
            get { return unidade; }
            set { unidade = value; }
        }
        
        public frmUnidadeBuscar(Cliente cliente)
        {
            InitializeComponent();
            this.cliente = cliente;
            montaDataGrid();
        }

        private void montaDataGrid()
        {
            try
            {
                ClienteFacade clienteFacade = ClienteFacade.getInstance();

                this.Cursor = Cursors.WaitCursor;

                DataSet ds = clienteFacade.findAllUnidadeByCliente(cliente);

                dg_cliente.DataSource = ds.Tables["Unidades"].DefaultView;

                dg_cliente.Columns[0].HeaderText = "ID";
                dg_cliente.Columns[0].Visible = false;

                dg_cliente.Columns[1].HeaderText = "Razão Social";
                dg_cliente.Columns[2].HeaderText = "Fantasia";

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }

        private void btn_fechar_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btn_ok_Click(object sender, EventArgs e)
        {
            try
            {
                if (dg_cliente.CurrentRow == null)
                    throw new Exception("Selecione uma linha");
                
                ClienteFacade clienteFacade = ClienteFacade.getInstance();
                this.Cursor = Cursors.WaitCursor;
                
                unidade = clienteFacade.findClienteById((Int64)dg_cliente.CurrentRow.Cells[0].Value);

                this.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }
    }
}
