﻿namespace SWS.View
{
    partial class frmProntuario
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            this.flpAcao = new System.Windows.Forms.FlowLayoutPanel();
            this.btn_gravar = new System.Windows.Forms.Button();
            this.btnImprimir = new System.Windows.Forms.Button();
            this.btnEmail = new System.Windows.Forms.Button();
            this.btnVerEmail = new System.Windows.Forms.Button();
            this.btn_fechar = new System.Windows.Forms.Button();
            this.grbBloco1 = new System.Windows.Forms.GroupBox();
            this.dgBloco1 = new System.Windows.Forms.DataGridView();
            this.grbBloco2 = new System.Windows.Forms.GroupBox();
            this.cbLassegue = new SWS.ComboBoxWithBorder();
            this.btnTabelaIMC = new System.Windows.Forms.Button();
            this.textlblLassegue = new System.Windows.Forms.TextBox();
            this.textLblIMC = new System.Windows.Forms.TextBox();
            this.textLblAltura = new System.Windows.Forms.TextBox();
            this.textLblPeso = new System.Windows.Forms.TextBox();
            this.textLblFrequencia = new System.Windows.Forms.TextBox();
            this.textLblPressao = new System.Windows.Forms.TextBox();
            this.textAltura = new System.Windows.Forms.TextBox();
            this.textPeso = new System.Windows.Forms.TextBox();
            this.textFrequencia = new System.Windows.Forms.TextBox();
            this.textPressao = new System.Windows.Forms.TextBox();
            this.textIMC = new System.Windows.Forms.TextBox();
            this.grbBloco3 = new System.Windows.Forms.GroupBox();
            this.dgBloco3 = new System.Windows.Forms.DataGridView();
            this.grbBloco4 = new System.Windows.Forms.GroupBox();
            this.dgBloco4 = new System.Windows.Forms.DataGridView();
            this.grbObservacao = new System.Windows.Forms.GroupBox();
            this.textComentarioAparelho = new System.Windows.Forms.TextBox();
            this.grbBloco5 = new System.Windows.Forms.GroupBox();
            this.cbContraceptivo = new SWS.ComboBoxWithBorder();
            this.btnContraceptivoOutros = new System.Windows.Forms.Button();
            this.dataUltimaMenstruacao = new System.Windows.Forms.DateTimePicker();
            this.textLblDataMenstruacao = new System.Windows.Forms.TextBox();
            this.textLblContraCeptivo = new System.Windows.Forms.TextBox();
            this.textLblParto = new System.Windows.Forms.TextBox();
            this.textParto = new System.Windows.Forms.TextBox();
            this.grbBloco6 = new System.Windows.Forms.GroupBox();
            this.dgBloco6 = new System.Windows.Forms.DataGridView();
            this.grbBloco7 = new System.Windows.Forms.GroupBox();
            this.grbJustificativaAfastamento = new System.Windows.Forms.GroupBox();
            this.textCausa = new System.Windows.Forms.TextBox();
            this.lblPrevidenciaDataAfastamento = new System.Windows.Forms.TextBox();
            this.cbPrevidenciaRecurso = new SWS.ComboBoxWithBorder();
            this.lblPrevidenciaRecurso = new System.Windows.Forms.TextBox();
            this.cbPrevidenciaAfastado = new SWS.ComboBoxWithBorder();
            this.lblPrevidenciaAfastado = new System.Windows.Forms.TextBox();
            this.dataAfastamento = new System.Windows.Forms.DateTimePicker();
            this.grbBloco8 = new System.Windows.Forms.GroupBox();
            this.dgBloco8 = new System.Windows.Forms.DataGridView();
            this.grbComentario = new System.Windows.Forms.GroupBox();
            this.textComentario = new System.Windows.Forms.TextBox();
            ((System.ComponentModel.ISupportInitialize)(this.spForm)).BeginInit();
            this.spForm.Panel1.SuspendLayout();
            this.spForm.Panel2.SuspendLayout();
            this.spForm.SuspendLayout();
            this.pnlForm.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.banner)).BeginInit();
            this.flpAcao.SuspendLayout();
            this.grbBloco1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgBloco1)).BeginInit();
            this.grbBloco2.SuspendLayout();
            this.grbBloco3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgBloco3)).BeginInit();
            this.grbBloco4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgBloco4)).BeginInit();
            this.grbObservacao.SuspendLayout();
            this.grbBloco5.SuspendLayout();
            this.grbBloco6.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgBloco6)).BeginInit();
            this.grbBloco7.SuspendLayout();
            this.grbJustificativaAfastamento.SuspendLayout();
            this.grbBloco8.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgBloco8)).BeginInit();
            this.grbComentario.SuspendLayout();
            this.SuspendLayout();
            // 
            // spForm
            // 
            // 
            // spForm.Panel1
            // 
            this.spForm.Panel1.Controls.Add(this.flpAcao);
            this.spForm.SplitterDistance = 43;
            // 
            // pnlForm
            // 
            this.pnlForm.Controls.Add(this.grbComentario);
            this.pnlForm.Controls.Add(this.grbBloco8);
            this.pnlForm.Controls.Add(this.grbBloco7);
            this.pnlForm.Controls.Add(this.grbBloco6);
            this.pnlForm.Controls.Add(this.grbBloco5);
            this.pnlForm.Controls.Add(this.grbObservacao);
            this.pnlForm.Controls.Add(this.grbBloco4);
            this.pnlForm.Controls.Add(this.grbBloco3);
            this.pnlForm.Controls.Add(this.grbBloco2);
            this.pnlForm.Controls.Add(this.grbBloco1);
            this.pnlForm.Margin = new System.Windows.Forms.Padding(5, 5, 5, 5);
            this.pnlForm.Size = new System.Drawing.Size(1019, 2940);
            // 
            // flpAcao
            // 
            this.flpAcao.Controls.Add(this.btn_gravar);
            this.flpAcao.Controls.Add(this.btnImprimir);
            this.flpAcao.Controls.Add(this.btnEmail);
            this.flpAcao.Controls.Add(this.btnVerEmail);
            this.flpAcao.Controls.Add(this.btn_fechar);
            this.flpAcao.Dock = System.Windows.Forms.DockStyle.Fill;
            this.flpAcao.Location = new System.Drawing.Point(0, 0);
            this.flpAcao.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.flpAcao.Name = "flpAcao";
            this.flpAcao.Size = new System.Drawing.Size(1045, 43);
            this.flpAcao.TabIndex = 0;
            // 
            // btn_gravar
            // 
            this.btn_gravar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_gravar.Image = global::SWS.Properties.Resources.Gravar;
            this.btn_gravar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btn_gravar.Location = new System.Drawing.Point(4, 4);
            this.btn_gravar.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.btn_gravar.Name = "btn_gravar";
            this.btn_gravar.Size = new System.Drawing.Size(100, 28);
            this.btn_gravar.TabIndex = 22;
            this.btn_gravar.TabStop = false;
            this.btn_gravar.Text = "&Gravar";
            this.btn_gravar.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btn_gravar.UseVisualStyleBackColor = true;
            this.btn_gravar.Click += new System.EventHandler(this.btn_gravar_Click);
            // 
            // btnImprimir
            // 
            this.btnImprimir.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnImprimir.Image = global::SWS.Properties.Resources.impressora1;
            this.btnImprimir.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnImprimir.Location = new System.Drawing.Point(112, 4);
            this.btnImprimir.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.btnImprimir.Name = "btnImprimir";
            this.btnImprimir.Size = new System.Drawing.Size(100, 28);
            this.btnImprimir.TabIndex = 23;
            this.btnImprimir.TabStop = false;
            this.btnImprimir.Text = "&Imprimir";
            this.btnImprimir.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnImprimir.UseVisualStyleBackColor = true;
            this.btnImprimir.Click += new System.EventHandler(this.btnImprimir_Click);
            // 
            // btnEmail
            // 
            this.btnEmail.Enabled = false;
            this.btnEmail.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnEmail.Image = global::SWS.Properties.Resources.email;
            this.btnEmail.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnEmail.Location = new System.Drawing.Point(220, 4);
            this.btnEmail.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.btnEmail.Name = "btnEmail";
            this.btnEmail.Size = new System.Drawing.Size(100, 28);
            this.btnEmail.TabIndex = 24;
            this.btnEmail.TabStop = false;
            this.btnEmail.Text = "&E-mail";
            this.btnEmail.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnEmail.UseVisualStyleBackColor = true;
            this.btnEmail.Click += new System.EventHandler(this.btnEmail_Click);
            // 
            // btnVerEmail
            // 
            this.btnVerEmail.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnVerEmail.Image = global::SWS.Properties.Resources.lupa;
            this.btnVerEmail.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnVerEmail.Location = new System.Drawing.Point(328, 4);
            this.btnVerEmail.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.btnVerEmail.Name = "btnVerEmail";
            this.btnVerEmail.Size = new System.Drawing.Size(100, 28);
            this.btnVerEmail.TabIndex = 25;
            this.btnVerEmail.TabStop = false;
            this.btnVerEmail.Text = "&Ver Email";
            this.btnVerEmail.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnVerEmail.UseVisualStyleBackColor = true;
            this.btnVerEmail.Click += new System.EventHandler(this.btnVerEmail_Click);
            // 
            // btn_fechar
            // 
            this.btn_fechar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_fechar.Image = global::SWS.Properties.Resources.close;
            this.btn_fechar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btn_fechar.Location = new System.Drawing.Point(436, 4);
            this.btn_fechar.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.btn_fechar.Name = "btn_fechar";
            this.btn_fechar.Size = new System.Drawing.Size(100, 28);
            this.btn_fechar.TabIndex = 26;
            this.btn_fechar.TabStop = false;
            this.btn_fechar.Text = "&Fechar";
            this.btn_fechar.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btn_fechar.UseVisualStyleBackColor = true;
            this.btn_fechar.Click += new System.EventHandler(this.btn_fechar_Click);
            // 
            // grbBloco1
            // 
            this.grbBloco1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.grbBloco1.Controls.Add(this.dgBloco1);
            this.grbBloco1.Location = new System.Drawing.Point(5, 5);
            this.grbBloco1.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.grbBloco1.Name = "grbBloco1";
            this.grbBloco1.Padding = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.grbBloco1.Size = new System.Drawing.Size(1009, 297);
            this.grbBloco1.TabIndex = 101;
            this.grbBloco1.TabStop = false;
            this.grbBloco1.Text = "Dados do Colaborador";
            // 
            // dgBloco1
            // 
            this.dgBloco1.AllowUserToAddRows = false;
            this.dgBloco1.AllowUserToDeleteRows = false;
            this.dgBloco1.AllowUserToOrderColumns = true;
            this.dgBloco1.AllowUserToResizeColumns = false;
            this.dgBloco1.AllowUserToResizeRows = false;
            this.dgBloco1.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.DisplayedCells;
            this.dgBloco1.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.DisplayedCells;
            this.dgBloco1.BackgroundColor = System.Drawing.Color.White;
            this.dgBloco1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle5.BackColor = System.Drawing.Color.LightGray;
            dataGridViewCellStyle5.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle5.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle5.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle5.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle5.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgBloco1.DefaultCellStyle = dataGridViewCellStyle5;
            this.dgBloco1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgBloco1.Location = new System.Drawing.Point(4, 19);
            this.dgBloco1.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.dgBloco1.MultiSelect = false;
            this.dgBloco1.Name = "dgBloco1";
            this.dgBloco1.ReadOnly = true;
            this.dgBloco1.RowHeadersVisible = false;
            this.dgBloco1.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgBloco1.Size = new System.Drawing.Size(1001, 274);
            this.dgBloco1.TabIndex = 50;
            this.dgBloco1.TabStop = false;
            // 
            // grbBloco2
            // 
            this.grbBloco2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.grbBloco2.Controls.Add(this.cbLassegue);
            this.grbBloco2.Controls.Add(this.btnTabelaIMC);
            this.grbBloco2.Controls.Add(this.textlblLassegue);
            this.grbBloco2.Controls.Add(this.textLblIMC);
            this.grbBloco2.Controls.Add(this.textLblAltura);
            this.grbBloco2.Controls.Add(this.textLblPeso);
            this.grbBloco2.Controls.Add(this.textLblFrequencia);
            this.grbBloco2.Controls.Add(this.textLblPressao);
            this.grbBloco2.Controls.Add(this.textAltura);
            this.grbBloco2.Controls.Add(this.textPeso);
            this.grbBloco2.Controls.Add(this.textFrequencia);
            this.grbBloco2.Controls.Add(this.textPressao);
            this.grbBloco2.Controls.Add(this.textIMC);
            this.grbBloco2.Location = new System.Drawing.Point(4, 309);
            this.grbBloco2.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.grbBloco2.Name = "grbBloco2";
            this.grbBloco2.Padding = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.grbBloco2.Size = new System.Drawing.Size(1011, 199);
            this.grbBloco2.TabIndex = 102;
            this.grbBloco2.TabStop = false;
            this.grbBloco2.Text = "Avaliações";
            // 
            // cbLassegue
            // 
            this.cbLassegue.BackColor = System.Drawing.Color.LightGray;
            this.cbLassegue.BorderColor = System.Drawing.Color.DimGray;
            this.cbLassegue.BorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid;
            this.cbLassegue.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbLassegue.FormattingEnabled = true;
            this.cbLassegue.Location = new System.Drawing.Point(264, 146);
            this.cbLassegue.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.cbLassegue.Name = "cbLassegue";
            this.cbLassegue.Size = new System.Drawing.Size(260, 25);
            this.cbLassegue.TabIndex = 5;
            // 
            // btnTabelaIMC
            // 
            this.btnTabelaIMC.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnTabelaIMC.Image = global::SWS.Properties.Resources.busca;
            this.btnTabelaIMC.Location = new System.Drawing.Point(524, 122);
            this.btnTabelaIMC.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.btnTabelaIMC.Name = "btnTabelaIMC";
            this.btnTabelaIMC.Size = new System.Drawing.Size(35, 26);
            this.btnTabelaIMC.TabIndex = 23;
            this.btnTabelaIMC.TabStop = false;
            this.btnTabelaIMC.Text = "...";
            this.btnTabelaIMC.UseVisualStyleBackColor = true;
            this.btnTabelaIMC.Click += new System.EventHandler(this.btnTabelaIMC_Click);
            // 
            // textlblLassegue
            // 
            this.textlblLassegue.BackColor = System.Drawing.Color.LightGray;
            this.textlblLassegue.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textlblLassegue.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textlblLassegue.Location = new System.Drawing.Point(8, 146);
            this.textlblLassegue.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.textlblLassegue.MaxLength = 15;
            this.textlblLassegue.Multiline = true;
            this.textlblLassegue.Name = "textlblLassegue";
            this.textlblLassegue.ReadOnly = true;
            this.textlblLassegue.Size = new System.Drawing.Size(254, 25);
            this.textlblLassegue.TabIndex = 17;
            this.textlblLassegue.TabStop = false;
            this.textlblLassegue.Text = "M Lassègue";
            // 
            // textLblIMC
            // 
            this.textLblIMC.BackColor = System.Drawing.Color.LightGray;
            this.textLblIMC.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textLblIMC.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textLblIMC.Location = new System.Drawing.Point(8, 122);
            this.textLblIMC.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.textLblIMC.MaxLength = 15;
            this.textLblIMC.Name = "textLblIMC";
            this.textLblIMC.ReadOnly = true;
            this.textLblIMC.Size = new System.Drawing.Size(254, 24);
            this.textLblIMC.TabIndex = 16;
            this.textLblIMC.TabStop = false;
            this.textLblIMC.Text = "Índice de Massa Corporal(IMC%)";
            // 
            // textLblAltura
            // 
            this.textLblAltura.BackColor = System.Drawing.Color.LightGray;
            this.textLblAltura.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textLblAltura.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textLblAltura.Location = new System.Drawing.Point(8, 97);
            this.textLblAltura.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.textLblAltura.MaxLength = 15;
            this.textLblAltura.Name = "textLblAltura";
            this.textLblAltura.ReadOnly = true;
            this.textLblAltura.Size = new System.Drawing.Size(254, 24);
            this.textLblAltura.TabIndex = 15;
            this.textLblAltura.TabStop = false;
            this.textLblAltura.Text = "Altura (m)";
            // 
            // textLblPeso
            // 
            this.textLblPeso.BackColor = System.Drawing.Color.LightGray;
            this.textLblPeso.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textLblPeso.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textLblPeso.Location = new System.Drawing.Point(8, 73);
            this.textLblPeso.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.textLblPeso.MaxLength = 15;
            this.textLblPeso.Name = "textLblPeso";
            this.textLblPeso.ReadOnly = true;
            this.textLblPeso.Size = new System.Drawing.Size(254, 24);
            this.textLblPeso.TabIndex = 14;
            this.textLblPeso.TabStop = false;
            this.textLblPeso.Text = "Peso Corporal (kg)";
            // 
            // textLblFrequencia
            // 
            this.textLblFrequencia.BackColor = System.Drawing.Color.LightGray;
            this.textLblFrequencia.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textLblFrequencia.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textLblFrequencia.Location = new System.Drawing.Point(8, 48);
            this.textLblFrequencia.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.textLblFrequencia.MaxLength = 15;
            this.textLblFrequencia.Name = "textLblFrequencia";
            this.textLblFrequencia.ReadOnly = true;
            this.textLblFrequencia.Size = new System.Drawing.Size(254, 24);
            this.textLblFrequencia.TabIndex = 13;
            this.textLblFrequencia.TabStop = false;
            this.textLblFrequencia.Text = "Frequência cardíaca (bpm)";
            // 
            // textLblPressao
            // 
            this.textLblPressao.BackColor = System.Drawing.Color.LightGray;
            this.textLblPressao.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textLblPressao.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textLblPressao.Location = new System.Drawing.Point(8, 23);
            this.textLblPressao.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.textLblPressao.MaxLength = 15;
            this.textLblPressao.Name = "textLblPressao";
            this.textLblPressao.ReadOnly = true;
            this.textLblPressao.Size = new System.Drawing.Size(254, 24);
            this.textLblPressao.TabIndex = 12;
            this.textLblPressao.TabStop = false;
            this.textLblPressao.Text = "Pressão Arterial (mmHg)";
            // 
            // textAltura
            // 
            this.textAltura.BackColor = System.Drawing.Color.White;
            this.textAltura.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textAltura.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textAltura.Location = new System.Drawing.Point(264, 97);
            this.textAltura.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.textAltura.MaxLength = 10;
            this.textAltura.Name = "textAltura";
            this.textAltura.Size = new System.Drawing.Size(261, 24);
            this.textAltura.TabIndex = 4;
            this.textAltura.Enter += new System.EventHandler(this.textAltura_Enter);
            this.textAltura.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.textAltura_KeyPress);
            this.textAltura.Leave += new System.EventHandler(this.textAltura_Leave);
            // 
            // textPeso
            // 
            this.textPeso.BackColor = System.Drawing.Color.White;
            this.textPeso.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textPeso.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textPeso.Location = new System.Drawing.Point(264, 73);
            this.textPeso.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.textPeso.MaxLength = 6;
            this.textPeso.Name = "textPeso";
            this.textPeso.Size = new System.Drawing.Size(261, 24);
            this.textPeso.TabIndex = 3;
            this.textPeso.Enter += new System.EventHandler(this.textPeso_Enter);
            this.textPeso.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.textPeso_KeyPress);
            this.textPeso.Leave += new System.EventHandler(this.textPeso_Leave);
            // 
            // textFrequencia
            // 
            this.textFrequencia.BackColor = System.Drawing.Color.White;
            this.textFrequencia.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textFrequencia.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textFrequencia.Location = new System.Drawing.Point(264, 48);
            this.textFrequencia.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.textFrequencia.MaxLength = 15;
            this.textFrequencia.Name = "textFrequencia";
            this.textFrequencia.Size = new System.Drawing.Size(261, 24);
            this.textFrequencia.TabIndex = 2;
            // 
            // textPressao
            // 
            this.textPressao.BackColor = System.Drawing.Color.White;
            this.textPressao.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textPressao.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textPressao.Location = new System.Drawing.Point(264, 23);
            this.textPressao.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.textPressao.MaxLength = 15;
            this.textPressao.Name = "textPressao";
            this.textPressao.Size = new System.Drawing.Size(261, 24);
            this.textPressao.TabIndex = 1;
            // 
            // textIMC
            // 
            this.textIMC.BackColor = System.Drawing.Color.White;
            this.textIMC.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textIMC.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textIMC.Location = new System.Drawing.Point(264, 122);
            this.textIMC.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.textIMC.MaxLength = 10;
            this.textIMC.Name = "textIMC";
            this.textIMC.Size = new System.Drawing.Size(261, 24);
            this.textIMC.TabIndex = 10;
            this.textIMC.TabStop = false;
            // 
            // grbBloco3
            // 
            this.grbBloco3.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.grbBloco3.Controls.Add(this.dgBloco3);
            this.grbBloco3.Location = new System.Drawing.Point(5, 516);
            this.grbBloco3.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.grbBloco3.Name = "grbBloco3";
            this.grbBloco3.Padding = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.grbBloco3.Size = new System.Drawing.Size(1013, 917);
            this.grbBloco3.TabIndex = 103;
            this.grbBloco3.TabStop = false;
            this.grbBloco3.Text = "Marque as respostas abaixo";
            // 
            // dgBloco3
            // 
            this.dgBloco3.AllowUserToAddRows = false;
            this.dgBloco3.AllowUserToDeleteRows = false;
            this.dgBloco3.AllowUserToOrderColumns = true;
            this.dgBloco3.AllowUserToResizeColumns = false;
            this.dgBloco3.AllowUserToResizeRows = false;
            this.dgBloco3.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.DisplayedCells;
            this.dgBloco3.BackgroundColor = System.Drawing.Color.White;
            this.dgBloco3.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle4.BackColor = System.Drawing.Color.LightGray;
            dataGridViewCellStyle4.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle4.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle4.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle4.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle4.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgBloco3.DefaultCellStyle = dataGridViewCellStyle4;
            this.dgBloco3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgBloco3.Location = new System.Drawing.Point(4, 19);
            this.dgBloco3.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.dgBloco3.MultiSelect = false;
            this.dgBloco3.Name = "dgBloco3";
            this.dgBloco3.RowHeadersVisible = false;
            this.dgBloco3.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.CellSelect;
            this.dgBloco3.Size = new System.Drawing.Size(1005, 894);
            this.dgBloco3.TabIndex = 6;
            this.dgBloco3.ColumnAdded += new System.Windows.Forms.DataGridViewColumnEventHandler(this.dgBloco3_ColumnAdded);
            // 
            // grbBloco4
            // 
            this.grbBloco4.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.grbBloco4.Controls.Add(this.dgBloco4);
            this.grbBloco4.Location = new System.Drawing.Point(5, 1440);
            this.grbBloco4.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.grbBloco4.Name = "grbBloco4";
            this.grbBloco4.Padding = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.grbBloco4.Size = new System.Drawing.Size(1013, 452);
            this.grbBloco4.TabIndex = 104;
            this.grbBloco4.TabStop = false;
            this.grbBloco4.Text = "Marque apenas os aparelhos alterados.";
            // 
            // dgBloco4
            // 
            this.dgBloco4.AllowUserToAddRows = false;
            this.dgBloco4.AllowUserToDeleteRows = false;
            this.dgBloco4.AllowUserToOrderColumns = true;
            this.dgBloco4.AllowUserToResizeColumns = false;
            this.dgBloco4.AllowUserToResizeRows = false;
            this.dgBloco4.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.DisplayedCells;
            this.dgBloco4.BackgroundColor = System.Drawing.Color.White;
            this.dgBloco4.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle3.BackColor = System.Drawing.Color.LightGray;
            dataGridViewCellStyle3.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgBloco4.DefaultCellStyle = dataGridViewCellStyle3;
            this.dgBloco4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgBloco4.Location = new System.Drawing.Point(4, 19);
            this.dgBloco4.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.dgBloco4.MultiSelect = false;
            this.dgBloco4.Name = "dgBloco4";
            this.dgBloco4.RowHeadersVisible = false;
            this.dgBloco4.Size = new System.Drawing.Size(1005, 429);
            this.dgBloco4.TabIndex = 7;
            this.dgBloco4.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgBloco4_CellContentClick);
            this.dgBloco4.CellMouseClick += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.dgBloco4_CellMouseClick);
            this.dgBloco4.ColumnAdded += new System.Windows.Forms.DataGridViewColumnEventHandler(this.dgBloco4_ColumnAdded);
            // 
            // grbObservacao
            // 
            this.grbObservacao.Controls.Add(this.textComentarioAparelho);
            this.grbObservacao.Location = new System.Drawing.Point(5, 1895);
            this.grbObservacao.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.grbObservacao.Name = "grbObservacao";
            this.grbObservacao.Padding = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.grbObservacao.Size = new System.Drawing.Size(1013, 116);
            this.grbObservacao.TabIndex = 105;
            this.grbObservacao.TabStop = false;
            this.grbObservacao.Text = "Observação";
            // 
            // textComentarioAparelho
            // 
            this.textComentarioAparelho.BackColor = System.Drawing.Color.LightSteelBlue;
            this.textComentarioAparelho.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textComentarioAparelho.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.textComentarioAparelho.Dock = System.Windows.Forms.DockStyle.Fill;
            this.textComentarioAparelho.Location = new System.Drawing.Point(4, 19);
            this.textComentarioAparelho.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.textComentarioAparelho.Multiline = true;
            this.textComentarioAparelho.Name = "textComentarioAparelho";
            this.textComentarioAparelho.Size = new System.Drawing.Size(1005, 93);
            this.textComentarioAparelho.TabIndex = 8;
            // 
            // grbBloco5
            // 
            this.grbBloco5.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.grbBloco5.Controls.Add(this.cbContraceptivo);
            this.grbBloco5.Controls.Add(this.btnContraceptivoOutros);
            this.grbBloco5.Controls.Add(this.dataUltimaMenstruacao);
            this.grbBloco5.Controls.Add(this.textLblDataMenstruacao);
            this.grbBloco5.Controls.Add(this.textLblContraCeptivo);
            this.grbBloco5.Controls.Add(this.textLblParto);
            this.grbBloco5.Controls.Add(this.textParto);
            this.grbBloco5.Location = new System.Drawing.Point(4, 2015);
            this.grbBloco5.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.grbBloco5.Name = "grbBloco5";
            this.grbBloco5.Padding = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.grbBloco5.Size = new System.Drawing.Size(1015, 113);
            this.grbBloco5.TabIndex = 106;
            this.grbBloco5.TabStop = false;
            this.grbBloco5.Text = "Exclusivo do sexo feminino";
            // 
            // cbContraceptivo
            // 
            this.cbContraceptivo.BackColor = System.Drawing.Color.LightGray;
            this.cbContraceptivo.BorderColor = System.Drawing.Color.DimGray;
            this.cbContraceptivo.BorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid;
            this.cbContraceptivo.FormattingEnabled = true;
            this.cbContraceptivo.Location = new System.Drawing.Point(289, 52);
            this.cbContraceptivo.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.cbContraceptivo.Name = "cbContraceptivo";
            this.cbContraceptivo.Size = new System.Drawing.Size(260, 24);
            this.cbContraceptivo.TabIndex = 10;
            // 
            // btnContraceptivoOutros
            // 
            this.btnContraceptivoOutros.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnContraceptivoOutros.Image = global::SWS.Properties.Resources.busca;
            this.btnContraceptivoOutros.Location = new System.Drawing.Point(549, 52);
            this.btnContraceptivoOutros.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.btnContraceptivoOutros.Name = "btnContraceptivoOutros";
            this.btnContraceptivoOutros.Size = new System.Drawing.Size(40, 26);
            this.btnContraceptivoOutros.TabIndex = 25;
            this.btnContraceptivoOutros.TabStop = false;
            this.btnContraceptivoOutros.Text = "...";
            this.btnContraceptivoOutros.UseVisualStyleBackColor = true;
            this.btnContraceptivoOutros.Click += new System.EventHandler(this.btnContraceptivoOutros_Click);
            // 
            // dataUltimaMenstruacao
            // 
            this.dataUltimaMenstruacao.Checked = false;
            this.dataUltimaMenstruacao.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dataUltimaMenstruacao.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dataUltimaMenstruacao.Location = new System.Drawing.Point(289, 76);
            this.dataUltimaMenstruacao.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.dataUltimaMenstruacao.Name = "dataUltimaMenstruacao";
            this.dataUltimaMenstruacao.ShowCheckBox = true;
            this.dataUltimaMenstruacao.Size = new System.Drawing.Size(260, 24);
            this.dataUltimaMenstruacao.TabIndex = 11;
            // 
            // textLblDataMenstruacao
            // 
            this.textLblDataMenstruacao.BackColor = System.Drawing.Color.White;
            this.textLblDataMenstruacao.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textLblDataMenstruacao.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textLblDataMenstruacao.Location = new System.Drawing.Point(8, 76);
            this.textLblDataMenstruacao.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.textLblDataMenstruacao.MaxLength = 15;
            this.textLblDataMenstruacao.Multiline = true;
            this.textLblDataMenstruacao.Name = "textLblDataMenstruacao";
            this.textLblDataMenstruacao.ReadOnly = true;
            this.textLblDataMenstruacao.Size = new System.Drawing.Size(279, 24);
            this.textLblDataMenstruacao.TabIndex = 19;
            this.textLblDataMenstruacao.TabStop = false;
            this.textLblDataMenstruacao.Text = "Data da última menstruação";
            // 
            // textLblContraCeptivo
            // 
            this.textLblContraCeptivo.BackColor = System.Drawing.Color.White;
            this.textLblContraCeptivo.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textLblContraCeptivo.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textLblContraCeptivo.Location = new System.Drawing.Point(8, 52);
            this.textLblContraCeptivo.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.textLblContraCeptivo.MaxLength = 15;
            this.textLblContraCeptivo.Multiline = true;
            this.textLblContraCeptivo.Name = "textLblContraCeptivo";
            this.textLblContraCeptivo.ReadOnly = true;
            this.textLblContraCeptivo.Size = new System.Drawing.Size(279, 25);
            this.textLblContraCeptivo.TabIndex = 15;
            this.textLblContraCeptivo.TabStop = false;
            this.textLblContraCeptivo.Text = "Método contraceptivo";
            // 
            // textLblParto
            // 
            this.textLblParto.BackColor = System.Drawing.Color.White;
            this.textLblParto.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textLblParto.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textLblParto.Location = new System.Drawing.Point(8, 27);
            this.textLblParto.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.textLblParto.MaxLength = 15;
            this.textLblParto.Name = "textLblParto";
            this.textLblParto.ReadOnly = true;
            this.textLblParto.Size = new System.Drawing.Size(279, 24);
            this.textLblParto.TabIndex = 14;
            this.textLblParto.TabStop = false;
            this.textLblParto.Text = "Quantidade de parto e ou gestações";
            // 
            // textParto
            // 
            this.textParto.BackColor = System.Drawing.Color.White;
            this.textParto.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textParto.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textParto.Location = new System.Drawing.Point(289, 27);
            this.textParto.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.textParto.MaxLength = 2;
            this.textParto.Name = "textParto";
            this.textParto.Size = new System.Drawing.Size(261, 24);
            this.textParto.TabIndex = 9;
            // 
            // grbBloco6
            // 
            this.grbBloco6.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.grbBloco6.Controls.Add(this.dgBloco6);
            this.grbBloco6.Location = new System.Drawing.Point(5, 2137);
            this.grbBloco6.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.grbBloco6.Name = "grbBloco6";
            this.grbBloco6.Padding = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.grbBloco6.Size = new System.Drawing.Size(1013, 225);
            this.grbBloco6.TabIndex = 107;
            this.grbBloco6.TabStop = false;
            this.grbBloco6.Text = "Resultado de Exames";
            // 
            // dgBloco6
            // 
            this.dgBloco6.AllowUserToAddRows = false;
            this.dgBloco6.AllowUserToDeleteRows = false;
            this.dgBloco6.AllowUserToOrderColumns = true;
            this.dgBloco6.AllowUserToResizeColumns = false;
            this.dgBloco6.AllowUserToResizeRows = false;
            this.dgBloco6.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.DisplayedCells;
            this.dgBloco6.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.DisplayedCells;
            this.dgBloco6.BackgroundColor = System.Drawing.Color.White;
            this.dgBloco6.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.Color.LightGray;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgBloco6.DefaultCellStyle = dataGridViewCellStyle2;
            this.dgBloco6.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgBloco6.Location = new System.Drawing.Point(4, 19);
            this.dgBloco6.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.dgBloco6.MultiSelect = false;
            this.dgBloco6.Name = "dgBloco6";
            this.dgBloco6.ReadOnly = true;
            this.dgBloco6.RowHeadersVisible = false;
            this.dgBloco6.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgBloco6.Size = new System.Drawing.Size(1005, 202);
            this.dgBloco6.TabIndex = 0;
            this.dgBloco6.TabStop = false;
            this.dgBloco6.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgBloco6_CellContentClick);
            this.dgBloco6.CellEnter += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgBloco6_CellEnter);
            this.dgBloco6.CellLeave += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgBloco6_CellLeave);
            this.dgBloco6.CurrentCellDirtyStateChanged += new System.EventHandler(this.dgBloco6_CurrentCellDirtyStateChanged);
            this.dgBloco6.EditingControlShowing += new System.Windows.Forms.DataGridViewEditingControlShowingEventHandler(this.dgBloco6_EditingControlShowing);
            // 
            // grbBloco7
            // 
            this.grbBloco7.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.grbBloco7.Controls.Add(this.grbJustificativaAfastamento);
            this.grbBloco7.Controls.Add(this.lblPrevidenciaDataAfastamento);
            this.grbBloco7.Controls.Add(this.cbPrevidenciaRecurso);
            this.grbBloco7.Controls.Add(this.lblPrevidenciaRecurso);
            this.grbBloco7.Controls.Add(this.cbPrevidenciaAfastado);
            this.grbBloco7.Controls.Add(this.lblPrevidenciaAfastado);
            this.grbBloco7.Controls.Add(this.dataAfastamento);
            this.grbBloco7.Location = new System.Drawing.Point(5, 2366);
            this.grbBloco7.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.grbBloco7.Name = "grbBloco7";
            this.grbBloco7.Padding = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.grbBloco7.Size = new System.Drawing.Size(1013, 197);
            this.grbBloco7.TabIndex = 108;
            this.grbBloco7.TabStop = false;
            this.grbBloco7.Text = "Previdência Social";
            // 
            // grbJustificativaAfastamento
            // 
            this.grbJustificativaAfastamento.Controls.Add(this.textCausa);
            this.grbJustificativaAfastamento.Location = new System.Drawing.Point(4, 106);
            this.grbJustificativaAfastamento.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.grbJustificativaAfastamento.Name = "grbJustificativaAfastamento";
            this.grbJustificativaAfastamento.Padding = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.grbJustificativaAfastamento.Size = new System.Drawing.Size(995, 80);
            this.grbJustificativaAfastamento.TabIndex = 20;
            this.grbJustificativaAfastamento.TabStop = false;
            this.grbJustificativaAfastamento.Text = "Causa do afastamento";
            // 
            // textCausa
            // 
            this.textCausa.BackColor = System.Drawing.Color.LightSteelBlue;
            this.textCausa.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textCausa.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.textCausa.Dock = System.Windows.Forms.DockStyle.Fill;
            this.textCausa.Enabled = false;
            this.textCausa.Location = new System.Drawing.Point(4, 19);
            this.textCausa.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.textCausa.Multiline = true;
            this.textCausa.Name = "textCausa";
            this.textCausa.Size = new System.Drawing.Size(987, 57);
            this.textCausa.TabIndex = 15;
            // 
            // lblPrevidenciaDataAfastamento
            // 
            this.lblPrevidenciaDataAfastamento.BackColor = System.Drawing.Color.LightGray;
            this.lblPrevidenciaDataAfastamento.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblPrevidenciaDataAfastamento.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPrevidenciaDataAfastamento.Location = new System.Drawing.Point(4, 73);
            this.lblPrevidenciaDataAfastamento.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.lblPrevidenciaDataAfastamento.Name = "lblPrevidenciaDataAfastamento";
            this.lblPrevidenciaDataAfastamento.ReadOnly = true;
            this.lblPrevidenciaDataAfastamento.Size = new System.Drawing.Size(257, 24);
            this.lblPrevidenciaDataAfastamento.TabIndex = 19;
            this.lblPrevidenciaDataAfastamento.TabStop = false;
            this.lblPrevidenciaDataAfastamento.Text = "Data de Afastamento";
            // 
            // cbPrevidenciaRecurso
            // 
            this.cbPrevidenciaRecurso.BackColor = System.Drawing.Color.LightGray;
            this.cbPrevidenciaRecurso.BorderColor = System.Drawing.Color.DimGray;
            this.cbPrevidenciaRecurso.BorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid;
            this.cbPrevidenciaRecurso.FormattingEnabled = true;
            this.cbPrevidenciaRecurso.Location = new System.Drawing.Point(263, 48);
            this.cbPrevidenciaRecurso.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.cbPrevidenciaRecurso.Name = "cbPrevidenciaRecurso";
            this.cbPrevidenciaRecurso.Size = new System.Drawing.Size(735, 24);
            this.cbPrevidenciaRecurso.TabIndex = 13;
            // 
            // lblPrevidenciaRecurso
            // 
            this.lblPrevidenciaRecurso.BackColor = System.Drawing.Color.LightGray;
            this.lblPrevidenciaRecurso.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblPrevidenciaRecurso.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPrevidenciaRecurso.Location = new System.Drawing.Point(4, 48);
            this.lblPrevidenciaRecurso.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.lblPrevidenciaRecurso.Name = "lblPrevidenciaRecurso";
            this.lblPrevidenciaRecurso.ReadOnly = true;
            this.lblPrevidenciaRecurso.Size = new System.Drawing.Size(257, 24);
            this.lblPrevidenciaRecurso.TabIndex = 17;
            this.lblPrevidenciaRecurso.TabStop = false;
            this.lblPrevidenciaRecurso.Text = "Já foi solicitado recurso ao INSS ?";
            // 
            // cbPrevidenciaAfastado
            // 
            this.cbPrevidenciaAfastado.BackColor = System.Drawing.Color.LightGray;
            this.cbPrevidenciaAfastado.BorderColor = System.Drawing.Color.DimGray;
            this.cbPrevidenciaAfastado.BorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid;
            this.cbPrevidenciaAfastado.FormattingEnabled = true;
            this.cbPrevidenciaAfastado.Location = new System.Drawing.Point(263, 23);
            this.cbPrevidenciaAfastado.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.cbPrevidenciaAfastado.Name = "cbPrevidenciaAfastado";
            this.cbPrevidenciaAfastado.Size = new System.Drawing.Size(735, 24);
            this.cbPrevidenciaAfastado.TabIndex = 12;
            this.cbPrevidenciaAfastado.SelectedIndexChanged += new System.EventHandler(this.cbPrevidenciaAfastado_SelectedIndexChanged);
            // 
            // lblPrevidenciaAfastado
            // 
            this.lblPrevidenciaAfastado.BackColor = System.Drawing.Color.LightGray;
            this.lblPrevidenciaAfastado.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblPrevidenciaAfastado.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPrevidenciaAfastado.Location = new System.Drawing.Point(4, 23);
            this.lblPrevidenciaAfastado.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.lblPrevidenciaAfastado.Name = "lblPrevidenciaAfastado";
            this.lblPrevidenciaAfastado.ReadOnly = true;
            this.lblPrevidenciaAfastado.Size = new System.Drawing.Size(257, 24);
            this.lblPrevidenciaAfastado.TabIndex = 15;
            this.lblPrevidenciaAfastado.TabStop = false;
            this.lblPrevidenciaAfastado.Text = "O colaborador está afastado ?";
            // 
            // dataAfastamento
            // 
            this.dataAfastamento.Checked = false;
            this.dataAfastamento.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dataAfastamento.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dataAfastamento.Location = new System.Drawing.Point(263, 73);
            this.dataAfastamento.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.dataAfastamento.Name = "dataAfastamento";
            this.dataAfastamento.ShowCheckBox = true;
            this.dataAfastamento.Size = new System.Drawing.Size(735, 24);
            this.dataAfastamento.TabIndex = 14;
            // 
            // grbBloco8
            // 
            this.grbBloco8.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.grbBloco8.Controls.Add(this.dgBloco8);
            this.grbBloco8.Location = new System.Drawing.Point(4, 2567);
            this.grbBloco8.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.grbBloco8.Name = "grbBloco8";
            this.grbBloco8.Padding = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.grbBloco8.Size = new System.Drawing.Size(1019, 209);
            this.grbBloco8.TabIndex = 109;
            this.grbBloco8.TabStop = false;
            this.grbBloco8.Text = "Conclusão";
            // 
            // dgBloco8
            // 
            this.dgBloco8.AllowUserToAddRows = false;
            this.dgBloco8.AllowUserToDeleteRows = false;
            this.dgBloco8.AllowUserToOrderColumns = true;
            this.dgBloco8.AllowUserToResizeColumns = false;
            this.dgBloco8.AllowUserToResizeRows = false;
            this.dgBloco8.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.DisplayedCells;
            this.dgBloco8.BackgroundColor = System.Drawing.Color.White;
            this.dgBloco8.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.LightGray;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgBloco8.DefaultCellStyle = dataGridViewCellStyle1;
            this.dgBloco8.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgBloco8.Location = new System.Drawing.Point(4, 19);
            this.dgBloco8.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.dgBloco8.MultiSelect = false;
            this.dgBloco8.Name = "dgBloco8";
            this.dgBloco8.RowHeadersVisible = false;
            this.dgBloco8.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgBloco8.Size = new System.Drawing.Size(1011, 186);
            this.dgBloco8.TabIndex = 16;
            this.dgBloco8.CellValueChanged += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgBloco8_CellValueChanged);
            this.dgBloco8.CurrentCellDirtyStateChanged += new System.EventHandler(this.dgBloco8_CurrentCellDirtyStateChanged);
            // 
            // grbComentario
            // 
            this.grbComentario.Controls.Add(this.textComentario);
            this.grbComentario.Location = new System.Drawing.Point(4, 2780);
            this.grbComentario.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.grbComentario.Name = "grbComentario";
            this.grbComentario.Padding = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.grbComentario.Size = new System.Drawing.Size(1015, 143);
            this.grbComentario.TabIndex = 110;
            this.grbComentario.TabStop = false;
            this.grbComentario.Text = "Comentário Geral";
            // 
            // textComentario
            // 
            this.textComentario.BackColor = System.Drawing.Color.LightSteelBlue;
            this.textComentario.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textComentario.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.textComentario.Dock = System.Windows.Forms.DockStyle.Fill;
            this.textComentario.Location = new System.Drawing.Point(4, 19);
            this.textComentario.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.textComentario.Multiline = true;
            this.textComentario.Name = "textComentario";
            this.textComentario.Size = new System.Drawing.Size(1007, 120);
            this.textComentario.TabIndex = 17;
            // 
            // frmProntuario
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1045, 690);
            this.Margin = new System.Windows.Forms.Padding(5, 5, 5, 5);
            this.Name = "frmProntuario";
            this.Text = "PRONTUÁRIO";
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.frmProntuario_KeyDown);
            this.spForm.Panel1.ResumeLayout(false);
            this.spForm.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.spForm)).EndInit();
            this.spForm.ResumeLayout(false);
            this.pnlForm.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.banner)).EndInit();
            this.flpAcao.ResumeLayout(false);
            this.grbBloco1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgBloco1)).EndInit();
            this.grbBloco2.ResumeLayout(false);
            this.grbBloco2.PerformLayout();
            this.grbBloco3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgBloco3)).EndInit();
            this.grbBloco4.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgBloco4)).EndInit();
            this.grbObservacao.ResumeLayout(false);
            this.grbObservacao.PerformLayout();
            this.grbBloco5.ResumeLayout(false);
            this.grbBloco5.PerformLayout();
            this.grbBloco6.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgBloco6)).EndInit();
            this.grbBloco7.ResumeLayout(false);
            this.grbBloco7.PerformLayout();
            this.grbJustificativaAfastamento.ResumeLayout(false);
            this.grbJustificativaAfastamento.PerformLayout();
            this.grbBloco8.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgBloco8)).EndInit();
            this.grbComentario.ResumeLayout(false);
            this.grbComentario.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.FlowLayoutPanel flpAcao;
        private System.Windows.Forms.Button btn_gravar;
        private System.Windows.Forms.Button btnImprimir;
        private System.Windows.Forms.Button btnEmail;
        private System.Windows.Forms.Button btnVerEmail;
        private System.Windows.Forms.Button btn_fechar;
        private System.Windows.Forms.GroupBox grbBloco1;
        private System.Windows.Forms.DataGridView dgBloco1;
        private System.Windows.Forms.GroupBox grbBloco2;
        private System.Windows.Forms.Button btnTabelaIMC;
        private System.Windows.Forms.TextBox textlblLassegue;
        private System.Windows.Forms.TextBox textLblIMC;
        private System.Windows.Forms.TextBox textLblAltura;
        private System.Windows.Forms.TextBox textLblPeso;
        private System.Windows.Forms.TextBox textLblFrequencia;
        private System.Windows.Forms.TextBox textLblPressao;
        private System.Windows.Forms.TextBox textIMC;
        private System.Windows.Forms.TextBox textAltura;
        private System.Windows.Forms.TextBox textPeso;
        private System.Windows.Forms.TextBox textFrequencia;
        private System.Windows.Forms.TextBox textPressao;
        private System.Windows.Forms.GroupBox grbBloco3;
        private System.Windows.Forms.DataGridView dgBloco3;
        private System.Windows.Forms.GroupBox grbBloco4;
        private System.Windows.Forms.DataGridView dgBloco4;
        private System.Windows.Forms.GroupBox grbObservacao;
        private System.Windows.Forms.TextBox textComentarioAparelho;
        private System.Windows.Forms.GroupBox grbBloco5;
        private System.Windows.Forms.Button btnContraceptivoOutros;
        private System.Windows.Forms.DateTimePicker dataUltimaMenstruacao;
        private System.Windows.Forms.TextBox textLblDataMenstruacao;
        private System.Windows.Forms.TextBox textLblContraCeptivo;
        private System.Windows.Forms.TextBox textLblParto;
        private System.Windows.Forms.TextBox textParto;
        private ComboBoxWithBorder cbLassegue;
        private ComboBoxWithBorder cbContraceptivo;
        private System.Windows.Forms.GroupBox grbBloco6;
        private System.Windows.Forms.DataGridView dgBloco6;
        private System.Windows.Forms.GroupBox grbBloco7;
        private System.Windows.Forms.GroupBox grbJustificativaAfastamento;
        private System.Windows.Forms.TextBox textCausa;
        private System.Windows.Forms.TextBox lblPrevidenciaDataAfastamento;
        private ComboBoxWithBorder cbPrevidenciaRecurso;
        private System.Windows.Forms.TextBox lblPrevidenciaRecurso;
        private ComboBoxWithBorder cbPrevidenciaAfastado;
        private System.Windows.Forms.TextBox lblPrevidenciaAfastado;
        private System.Windows.Forms.DateTimePicker dataAfastamento;
        private System.Windows.Forms.GroupBox grbBloco8;
        private System.Windows.Forms.DataGridView dgBloco8;
        private System.Windows.Forms.GroupBox grbComentario;
        private System.Windows.Forms.TextBox textComentario;
    }
}