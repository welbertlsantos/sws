﻿using SWS.Entidade;
using SWS.Facade;
using SWS.View.Resources;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace SWS.View
{
    public partial class frmRamoSelecionar : frmTemplateConsulta
    {

        private Ramo ramo;

        public Ramo Ramo
        {
            get { return ramo; }
            set { ramo = value; }
        }
        
        public frmRamoSelecionar()
        {
            InitializeComponent();
        }

        private void btSelecionar_Click(object sender, EventArgs e)
        {
            try
            {
                if (dgvRamo.CurrentRow == null)
                    throw new Exception("Selecione uma linha.");

                ClienteFacade clienteFacade = ClienteFacade.getInstance();

                ramo = clienteFacade.findRamoById(Convert.ToInt64(dgvRamo.CurrentRow.Cells[0].Value));

                this.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btNovo_Click(object sender, EventArgs e)
        {
            frmRamoIncluir ramoIncluir = new frmRamoIncluir();
            ramoIncluir.ShowDialog();

            if (ramoIncluir.Ramo != null)
            {
                ramo = ramoIncluir.Ramo;
                this.Close();
            }
        }

        private void btBuscar_Click(object sender, EventArgs e)
        {
            ramo = new Ramo(null, textRamo.Text.Trim().ToUpper(), ApplicationConstants.ATIVO);
            montaDataGrid();
            textRamo.Focus();
        }

        private void btSair_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void montaDataGrid()
        {
            try
            {
                this.Cursor = Cursors.WaitCursor;

                this.dgvRamo.Columns.Clear();

                ClienteFacade clienteFacade = ClienteFacade.getInstance();

                DataSet ds = clienteFacade.findRamoByFilter(ramo);

                dgvRamo.ColumnCount = 3;

                dgvRamo.Columns[0].HeaderText = "id";
                dgvRamo.Columns[0].Visible = false;

                dgvRamo.Columns[1].HeaderText = "Ramo";

                dgvRamo.Columns[2].HeaderText = "Situação";
                dgvRamo.Columns[2].Visible = false;

                foreach (DataRow row in ds.Tables[0].Rows)
                    dgvRamo.Rows.Add(Convert.ToInt64(row["id"]), row["descricao"], row["situacao"]);

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }

        private void dgvRamo_CellMouseDoubleClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            try
            {

                btSelecionar.PerformClick();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }
    }
}
