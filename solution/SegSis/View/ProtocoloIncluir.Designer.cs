﻿namespace SWS.View
{
    partial class frmProtocoloIncluir
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        protected System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        protected void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            this.flpAcao = new System.Windows.Forms.FlowLayoutPanel();
            this.btn_novo = new System.Windows.Forms.Button();
            this.btnFinalizar = new System.Windows.Forms.Button();
            this.btnFechar = new System.Windows.Forms.Button();
            this.btnIncluirItem = new System.Windows.Forms.Button();
            this.btnExcluirItem = new System.Windows.Forms.Button();
            this.textCodigo = new System.Windows.Forms.MaskedTextBox();
            this.lblCodigo = new System.Windows.Forms.TextBox();
            this.lblDataGravacao = new System.Windows.Forms.TextBox();
            this.btnExcluirCliente = new System.Windows.Forms.Button();
            this.btnCliente = new System.Windows.Forms.Button();
            this.textCliente = new System.Windows.Forms.TextBox();
            this.lblCliente = new System.Windows.Forms.TextBox();
            this.textDataGravacao = new System.Windows.Forms.TextBox();
            this.grdItensProtocolo = new System.Windows.Forms.GroupBox();
            this.dgItens = new System.Windows.Forms.DataGridView();
            this.lblLegenda = new System.Windows.Forms.Label();
            this.lblTextLegenda2 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.lblTextLegenda1 = new System.Windows.Forms.Label();
            this.lblLegenda1 = new System.Windows.Forms.Label();
            this.grbTotaisItens = new System.Windows.Forms.GroupBox();
            this.textTotalDocumento = new System.Windows.Forms.TextBox();
            this.lblTotalDocumento = new System.Windows.Forms.Label();
            this.textQuantidadeProduto = new System.Windows.Forms.TextBox();
            this.lblTotalProduto = new System.Windows.Forms.Label();
            this.textEstudo = new System.Windows.Forms.TextBox();
            this.lblEstudo = new System.Windows.Forms.Label();
            this.textQuantidadeAso = new System.Windows.Forms.TextBox();
            this.lblQuantidadeAso = new System.Windows.Forms.Label();
            this.textExameLaudado = new System.Windows.Forms.TextBox();
            this.lblExameLaudado = new System.Windows.Forms.Label();
            this.textTotalExtra = new System.Windows.Forms.TextBox();
            this.lblExamesExtra = new System.Windows.Forms.Label();
            this.textTotalPcmso = new System.Windows.Forms.TextBox();
            this.lblTotalExamesPcmso = new System.Windows.Forms.Label();
            this.textTotalExame = new System.Windows.Forms.TextBox();
            this.lblTotalItem = new System.Windows.Forms.Label();
            this.flowLayoutPanel1 = new System.Windows.Forms.FlowLayoutPanel();
            ((System.ComponentModel.ISupportInitialize)(this.spForm)).BeginInit();
            this.spForm.Panel1.SuspendLayout();
            this.spForm.Panel2.SuspendLayout();
            this.spForm.SuspendLayout();
            this.pnlForm.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.banner)).BeginInit();
            this.flpAcao.SuspendLayout();
            this.grdItensProtocolo.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgItens)).BeginInit();
            this.grbTotaisItens.SuspendLayout();
            this.flowLayoutPanel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // spForm
            // 
            // 
            // spForm.Panel1
            // 
            this.spForm.Panel1.Controls.Add(this.flpAcao);
            // 
            // pnlForm
            // 
            this.pnlForm.Controls.Add(this.flowLayoutPanel1);
            this.pnlForm.Controls.Add(this.grbTotaisItens);
            this.pnlForm.Controls.Add(this.lblLegenda);
            this.pnlForm.Controls.Add(this.lblTextLegenda2);
            this.pnlForm.Controls.Add(this.label2);
            this.pnlForm.Controls.Add(this.lblTextLegenda1);
            this.pnlForm.Controls.Add(this.lblLegenda1);
            this.pnlForm.Controls.Add(this.grdItensProtocolo);
            this.pnlForm.Controls.Add(this.textDataGravacao);
            this.pnlForm.Controls.Add(this.btnExcluirCliente);
            this.pnlForm.Controls.Add(this.btnCliente);
            this.pnlForm.Controls.Add(this.textCliente);
            this.pnlForm.Controls.Add(this.lblCliente);
            this.pnlForm.Controls.Add(this.lblDataGravacao);
            this.pnlForm.Controls.Add(this.textCodigo);
            this.pnlForm.Controls.Add(this.lblCodigo);
            // 
            // flpAcao
            // 
            this.flpAcao.Controls.Add(this.btn_novo);
            this.flpAcao.Controls.Add(this.btnFinalizar);
            this.flpAcao.Controls.Add(this.btnFechar);
            this.flpAcao.Dock = System.Windows.Forms.DockStyle.Fill;
            this.flpAcao.Location = new System.Drawing.Point(0, 0);
            this.flpAcao.Name = "flpAcao";
            this.flpAcao.Size = new System.Drawing.Size(784, 35);
            this.flpAcao.TabIndex = 0;
            // 
            // btn_novo
            // 
            this.btn_novo.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_novo.Image = global::SWS.Properties.Resources.icone_mais;
            this.btn_novo.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btn_novo.Location = new System.Drawing.Point(3, 3);
            this.btn_novo.Name = "btn_novo";
            this.btn_novo.Size = new System.Drawing.Size(75, 23);
            this.btn_novo.TabIndex = 0;
            this.btn_novo.Text = "&Novo";
            this.btn_novo.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btn_novo.UseVisualStyleBackColor = true;
            this.btn_novo.Click += new System.EventHandler(this.btn_novo_Click);
            // 
            // btnFinalizar
            // 
            this.btnFinalizar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnFinalizar.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnFinalizar.Image = global::SWS.Properties.Resources.fechar_ico;
            this.btnFinalizar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnFinalizar.Location = new System.Drawing.Point(84, 3);
            this.btnFinalizar.Name = "btnFinalizar";
            this.btnFinalizar.Size = new System.Drawing.Size(75, 23);
            this.btnFinalizar.TabIndex = 1;
            this.btnFinalizar.Text = "&Finalizar";
            this.btnFinalizar.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnFinalizar.UseVisualStyleBackColor = true;
            this.btnFinalizar.Click += new System.EventHandler(this.btnFinalizar_Click);
            // 
            // btnFechar
            // 
            this.btnFechar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnFechar.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnFechar.Image = global::SWS.Properties.Resources.close;
            this.btnFechar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnFechar.Location = new System.Drawing.Point(165, 3);
            this.btnFechar.Name = "btnFechar";
            this.btnFechar.Size = new System.Drawing.Size(75, 23);
            this.btnFechar.TabIndex = 8;
            this.btnFechar.TabStop = false;
            this.btnFechar.Text = "&Fechar";
            this.btnFechar.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnFechar.UseVisualStyleBackColor = true;
            this.btnFechar.Click += new System.EventHandler(this.btnFechar_Click);
            // 
            // btnIncluirItem
            // 
            this.btnIncluirItem.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnIncluirItem.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnIncluirItem.Image = global::SWS.Properties.Resources.protocolo;
            this.btnIncluirItem.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnIncluirItem.Location = new System.Drawing.Point(3, 3);
            this.btnIncluirItem.Name = "btnIncluirItem";
            this.btnIncluirItem.Size = new System.Drawing.Size(75, 23);
            this.btnIncluirItem.TabIndex = 0;
            this.btnIncluirItem.Text = "&Incluir Item";
            this.btnIncluirItem.UseVisualStyleBackColor = true;
            this.btnIncluirItem.Click += new System.EventHandler(this.btnProtocolo_Click);
            // 
            // btnExcluirItem
            // 
            this.btnExcluirItem.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnExcluirItem.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnExcluirItem.Image = global::SWS.Properties.Resources.lixeira;
            this.btnExcluirItem.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnExcluirItem.Location = new System.Drawing.Point(84, 3);
            this.btnExcluirItem.Name = "btnExcluirItem";
            this.btnExcluirItem.Size = new System.Drawing.Size(75, 23);
            this.btnExcluirItem.TabIndex = 1;
            this.btnExcluirItem.Text = "&Excluir";
            this.btnExcluirItem.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnExcluirItem.UseVisualStyleBackColor = true;
            this.btnExcluirItem.Click += new System.EventHandler(this.btnExcluir_Click);
            // 
            // textCodigo
            // 
            this.textCodigo.BackColor = System.Drawing.Color.WhiteSmoke;
            this.textCodigo.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textCodigo.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textCodigo.Location = new System.Drawing.Point(140, 23);
            this.textCodigo.Mask = "9999,99,999999";
            this.textCodigo.Name = "textCodigo";
            this.textCodigo.PromptChar = ' ';
            this.textCodigo.ReadOnly = true;
            this.textCodigo.Size = new System.Drawing.Size(621, 21);
            this.textCodigo.TabIndex = 3;
            // 
            // lblCodigo
            // 
            this.lblCodigo.BackColor = System.Drawing.Color.LightGray;
            this.lblCodigo.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblCodigo.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCodigo.Location = new System.Drawing.Point(3, 23);
            this.lblCodigo.Name = "lblCodigo";
            this.lblCodigo.ReadOnly = true;
            this.lblCodigo.Size = new System.Drawing.Size(138, 21);
            this.lblCodigo.TabIndex = 4;
            this.lblCodigo.TabStop = false;
            this.lblCodigo.Text = "Código";
            // 
            // lblDataGravacao
            // 
            this.lblDataGravacao.BackColor = System.Drawing.Color.LightGray;
            this.lblDataGravacao.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblDataGravacao.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDataGravacao.Location = new System.Drawing.Point(3, 43);
            this.lblDataGravacao.Name = "lblDataGravacao";
            this.lblDataGravacao.ReadOnly = true;
            this.lblDataGravacao.Size = new System.Drawing.Size(138, 21);
            this.lblDataGravacao.TabIndex = 17;
            this.lblDataGravacao.TabStop = false;
            this.lblDataGravacao.Text = "Data de Gravação";
            // 
            // btnExcluirCliente
            // 
            this.btnExcluirCliente.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnExcluirCliente.Image = global::SWS.Properties.Resources.close;
            this.btnExcluirCliente.Location = new System.Drawing.Point(731, 3);
            this.btnExcluirCliente.Name = "btnExcluirCliente";
            this.btnExcluirCliente.Size = new System.Drawing.Size(30, 21);
            this.btnExcluirCliente.TabIndex = 2;
            this.btnExcluirCliente.TabStop = false;
            this.btnExcluirCliente.UseVisualStyleBackColor = true;
            this.btnExcluirCliente.Click += new System.EventHandler(this.btnExcluirCliente_Click);
            // 
            // btnCliente
            // 
            this.btnCliente.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnCliente.Image = global::SWS.Properties.Resources.busca;
            this.btnCliente.Location = new System.Drawing.Point(702, 3);
            this.btnCliente.Name = "btnCliente";
            this.btnCliente.Size = new System.Drawing.Size(30, 21);
            this.btnCliente.TabIndex = 1;
            this.btnCliente.UseVisualStyleBackColor = true;
            this.btnCliente.Click += new System.EventHandler(this.btCliente_Click);
            // 
            // textCliente
            // 
            this.textCliente.BackColor = System.Drawing.Color.WhiteSmoke;
            this.textCliente.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textCliente.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textCliente.Location = new System.Drawing.Point(140, 3);
            this.textCliente.MaxLength = 100;
            this.textCliente.Name = "textCliente";
            this.textCliente.ReadOnly = true;
            this.textCliente.Size = new System.Drawing.Size(564, 21);
            this.textCliente.TabIndex = 43;
            this.textCliente.TabStop = false;
            // 
            // lblCliente
            // 
            this.lblCliente.BackColor = System.Drawing.Color.LightGray;
            this.lblCliente.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblCliente.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCliente.Location = new System.Drawing.Point(3, 3);
            this.lblCliente.Name = "lblCliente";
            this.lblCliente.ReadOnly = true;
            this.lblCliente.Size = new System.Drawing.Size(138, 21);
            this.lblCliente.TabIndex = 42;
            this.lblCliente.TabStop = false;
            this.lblCliente.Text = "Cliente";
            // 
            // textDataGravacao
            // 
            this.textDataGravacao.BackColor = System.Drawing.Color.WhiteSmoke;
            this.textDataGravacao.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textDataGravacao.Location = new System.Drawing.Point(140, 43);
            this.textDataGravacao.Multiline = true;
            this.textDataGravacao.Name = "textDataGravacao";
            this.textDataGravacao.ReadOnly = true;
            this.textDataGravacao.Size = new System.Drawing.Size(621, 21);
            this.textDataGravacao.TabIndex = 45;
            this.textDataGravacao.TabStop = false;
            // 
            // grdItensProtocolo
            // 
            this.grdItensProtocolo.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.grdItensProtocolo.Controls.Add(this.dgItens);
            this.grdItensProtocolo.Location = new System.Drawing.Point(6, 70);
            this.grdItensProtocolo.Name = "grdItensProtocolo";
            this.grdItensProtocolo.Size = new System.Drawing.Size(753, 270);
            this.grdItensProtocolo.TabIndex = 46;
            this.grdItensProtocolo.TabStop = false;
            this.grdItensProtocolo.Text = "Itens no Protocolo";
            // 
            // dgItens
            // 
            this.dgItens.AllowUserToAddRows = false;
            this.dgItens.AllowUserToDeleteRows = false;
            this.dgItens.AllowUserToOrderColumns = true;
            this.dgItens.AllowUserToResizeRows = false;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.Gainsboro;
            this.dgItens.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle1;
            this.dgItens.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.DisplayedCells;
            this.dgItens.BackgroundColor = System.Drawing.Color.White;
            this.dgItens.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.Color.LightGray;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgItens.DefaultCellStyle = dataGridViewCellStyle2;
            this.dgItens.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgItens.Location = new System.Drawing.Point(3, 16);
            this.dgItens.MultiSelect = false;
            this.dgItens.Name = "dgItens";
            this.dgItens.RowHeadersVisible = false;
            this.dgItens.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgItens.Size = new System.Drawing.Size(747, 251);
            this.dgItens.TabIndex = 5;
            // 
            // lblLegenda
            // 
            this.lblLegenda.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.lblLegenda.AutoSize = true;
            this.lblLegenda.Location = new System.Drawing.Point(6, 385);
            this.lblLegenda.Name = "lblLegenda";
            this.lblLegenda.Size = new System.Drawing.Size(52, 13);
            this.lblLegenda.TabIndex = 51;
            this.lblLegenda.Text = "Legenda:";
            // 
            // lblTextLegenda2
            // 
            this.lblTextLegenda2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.lblTextLegenda2.AutoSize = true;
            this.lblTextLegenda2.BackColor = System.Drawing.Color.White;
            this.lblTextLegenda2.Location = new System.Drawing.Point(222, 385);
            this.lblTextLegenda2.Name = "lblTextLegenda2";
            this.lblTextLegenda2.Size = new System.Drawing.Size(67, 13);
            this.lblTextLegenda2.TabIndex = 50;
            this.lblTextLegenda2.Text = "Documentos";
            // 
            // label2
            // 
            this.label2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.Green;
            this.label2.Location = new System.Drawing.Point(203, 385);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(13, 13);
            this.label2.TabIndex = 49;
            this.label2.Text = "  ";
            // 
            // lblTextLegenda1
            // 
            this.lblTextLegenda1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.lblTextLegenda1.AutoSize = true;
            this.lblTextLegenda1.Location = new System.Drawing.Point(83, 385);
            this.lblTextLegenda1.Name = "lblTextLegenda1";
            this.lblTextLegenda1.Size = new System.Drawing.Size(114, 13);
            this.lblTextLegenda1.TabIndex = 48;
            this.lblTextLegenda1.Text = "PPRA, PCMSO e ASO";
            // 
            // lblLegenda1
            // 
            this.lblLegenda1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.lblLegenda1.AutoSize = true;
            this.lblLegenda1.BackColor = System.Drawing.Color.Red;
            this.lblLegenda1.Location = new System.Drawing.Point(64, 385);
            this.lblLegenda1.Name = "lblLegenda1";
            this.lblLegenda1.Size = new System.Drawing.Size(13, 13);
            this.lblLegenda1.TabIndex = 47;
            this.lblLegenda1.Text = "  ";
            // 
            // grbTotaisItens
            // 
            this.grbTotaisItens.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.grbTotaisItens.BackColor = System.Drawing.Color.White;
            this.grbTotaisItens.Controls.Add(this.textTotalDocumento);
            this.grbTotaisItens.Controls.Add(this.lblTotalDocumento);
            this.grbTotaisItens.Controls.Add(this.textQuantidadeProduto);
            this.grbTotaisItens.Controls.Add(this.lblTotalProduto);
            this.grbTotaisItens.Controls.Add(this.textEstudo);
            this.grbTotaisItens.Controls.Add(this.lblEstudo);
            this.grbTotaisItens.Controls.Add(this.textQuantidadeAso);
            this.grbTotaisItens.Controls.Add(this.lblQuantidadeAso);
            this.grbTotaisItens.Controls.Add(this.textExameLaudado);
            this.grbTotaisItens.Controls.Add(this.lblExameLaudado);
            this.grbTotaisItens.Controls.Add(this.textTotalExtra);
            this.grbTotaisItens.Controls.Add(this.lblExamesExtra);
            this.grbTotaisItens.Controls.Add(this.textTotalPcmso);
            this.grbTotaisItens.Controls.Add(this.lblTotalExamesPcmso);
            this.grbTotaisItens.Controls.Add(this.textTotalExame);
            this.grbTotaisItens.Controls.Add(this.lblTotalItem);
            this.grbTotaisItens.Location = new System.Drawing.Point(8, 401);
            this.grbTotaisItens.Name = "grbTotaisItens";
            this.grbTotaisItens.Size = new System.Drawing.Size(753, 67);
            this.grbTotaisItens.TabIndex = 52;
            this.grbTotaisItens.TabStop = false;
            this.grbTotaisItens.Text = "Totais";
            // 
            // textTotalDocumento
            // 
            this.textTotalDocumento.BackColor = System.Drawing.Color.SteelBlue;
            this.textTotalDocumento.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textTotalDocumento.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textTotalDocumento.Location = new System.Drawing.Point(656, 32);
            this.textTotalDocumento.Name = "textTotalDocumento";
            this.textTotalDocumento.Size = new System.Drawing.Size(89, 20);
            this.textTotalDocumento.TabIndex = 17;
            this.textTotalDocumento.TabStop = false;
            this.textTotalDocumento.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // lblTotalDocumento
            // 
            this.lblTotalDocumento.AutoSize = true;
            this.lblTotalDocumento.Location = new System.Drawing.Point(653, 16);
            this.lblTotalDocumento.Name = "lblTotalDocumento";
            this.lblTotalDocumento.Size = new System.Drawing.Size(94, 13);
            this.lblTotalDocumento.TabIndex = 16;
            this.lblTotalDocumento.Text = "Total Documentos";
            // 
            // textQuantidadeProduto
            // 
            this.textQuantidadeProduto.BackColor = System.Drawing.Color.SteelBlue;
            this.textQuantidadeProduto.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textQuantidadeProduto.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textQuantidadeProduto.Location = new System.Drawing.Point(559, 32);
            this.textQuantidadeProduto.Name = "textQuantidadeProduto";
            this.textQuantidadeProduto.Size = new System.Drawing.Size(89, 20);
            this.textQuantidadeProduto.TabIndex = 15;
            this.textQuantidadeProduto.TabStop = false;
            this.textQuantidadeProduto.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // lblTotalProduto
            // 
            this.lblTotalProduto.AutoSize = true;
            this.lblTotalProduto.Location = new System.Drawing.Point(556, 16);
            this.lblTotalProduto.Name = "lblTotalProduto";
            this.lblTotalProduto.Size = new System.Drawing.Size(91, 13);
            this.lblTotalProduto.TabIndex = 14;
            this.lblTotalProduto.Text = "Total de Produtos";
            // 
            // textEstudo
            // 
            this.textEstudo.BackColor = System.Drawing.Color.SteelBlue;
            this.textEstudo.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textEstudo.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textEstudo.Location = new System.Drawing.Point(464, 32);
            this.textEstudo.Name = "textEstudo";
            this.textEstudo.Size = new System.Drawing.Size(89, 20);
            this.textEstudo.TabIndex = 13;
            this.textEstudo.TabStop = false;
            this.textEstudo.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // lblEstudo
            // 
            this.lblEstudo.AutoSize = true;
            this.lblEstudo.Location = new System.Drawing.Point(461, 16);
            this.lblEstudo.Name = "lblEstudo";
            this.lblEstudo.Size = new System.Drawing.Size(87, 13);
            this.lblEstudo.TabIndex = 12;
            this.lblEstudo.Text = "Total de Estudos\r\n";
            // 
            // textQuantidadeAso
            // 
            this.textQuantidadeAso.BackColor = System.Drawing.Color.SteelBlue;
            this.textQuantidadeAso.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textQuantidadeAso.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textQuantidadeAso.Location = new System.Drawing.Point(369, 32);
            this.textQuantidadeAso.Name = "textQuantidadeAso";
            this.textQuantidadeAso.Size = new System.Drawing.Size(89, 20);
            this.textQuantidadeAso.TabIndex = 11;
            this.textQuantidadeAso.TabStop = false;
            this.textQuantidadeAso.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // lblQuantidadeAso
            // 
            this.lblQuantidadeAso.AutoSize = true;
            this.lblQuantidadeAso.Location = new System.Drawing.Point(366, 16);
            this.lblQuantidadeAso.Name = "lblQuantidadeAso";
            this.lblQuantidadeAso.Size = new System.Drawing.Size(76, 13);
            this.lblQuantidadeAso.TabIndex = 10;
            this.lblQuantidadeAso.Text = "Total de ASOs\r\n";
            // 
            // textExameLaudado
            // 
            this.textExameLaudado.BackColor = System.Drawing.Color.SteelBlue;
            this.textExameLaudado.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textExameLaudado.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textExameLaudado.Location = new System.Drawing.Point(274, 32);
            this.textExameLaudado.Name = "textExameLaudado";
            this.textExameLaudado.Size = new System.Drawing.Size(89, 20);
            this.textExameLaudado.TabIndex = 9;
            this.textExameLaudado.TabStop = false;
            this.textExameLaudado.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // lblExameLaudado
            // 
            this.lblExameLaudado.AutoSize = true;
            this.lblExameLaudado.Location = new System.Drawing.Point(271, 16);
            this.lblExameLaudado.Name = "lblExameLaudado";
            this.lblExameLaudado.Size = new System.Drawing.Size(94, 13);
            this.lblExameLaudado.TabIndex = 8;
            this.lblExameLaudado.Text = "Exames Laudados";
            // 
            // textTotalExtra
            // 
            this.textTotalExtra.BackColor = System.Drawing.Color.SteelBlue;
            this.textTotalExtra.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textTotalExtra.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textTotalExtra.Location = new System.Drawing.Point(192, 32);
            this.textTotalExtra.Name = "textTotalExtra";
            this.textTotalExtra.Size = new System.Drawing.Size(73, 20);
            this.textTotalExtra.TabIndex = 5;
            this.textTotalExtra.TabStop = false;
            this.textTotalExtra.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // lblExamesExtra
            // 
            this.lblExamesExtra.AutoSize = true;
            this.lblExamesExtra.Location = new System.Drawing.Point(189, 16);
            this.lblExamesExtra.Name = "lblExamesExtra";
            this.lblExamesExtra.Size = new System.Drawing.Size(76, 13);
            this.lblExamesExtra.TabIndex = 4;
            this.lblExamesExtra.Text = "Exames Extras";
            // 
            // textTotalPcmso
            // 
            this.textTotalPcmso.BackColor = System.Drawing.Color.SteelBlue;
            this.textTotalPcmso.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textTotalPcmso.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textTotalPcmso.Location = new System.Drawing.Point(101, 32);
            this.textTotalPcmso.Name = "textTotalPcmso";
            this.textTotalPcmso.Size = new System.Drawing.Size(82, 20);
            this.textTotalPcmso.TabIndex = 3;
            this.textTotalPcmso.TabStop = false;
            this.textTotalPcmso.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // lblTotalExamesPcmso
            // 
            this.lblTotalExamesPcmso.AutoSize = true;
            this.lblTotalExamesPcmso.Location = new System.Drawing.Point(98, 16);
            this.lblTotalExamesPcmso.Name = "lblTotalExamesPcmso";
            this.lblTotalExamesPcmso.Size = new System.Drawing.Size(85, 13);
            this.lblTotalExamesPcmso.TabIndex = 2;
            this.lblTotalExamesPcmso.Text = "Exames PCMSO";
            // 
            // textTotalExame
            // 
            this.textTotalExame.BackColor = System.Drawing.Color.SteelBlue;
            this.textTotalExame.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textTotalExame.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textTotalExame.Location = new System.Drawing.Point(9, 32);
            this.textTotalExame.Name = "textTotalExame";
            this.textTotalExame.Size = new System.Drawing.Size(83, 20);
            this.textTotalExame.TabIndex = 1;
            this.textTotalExame.TabStop = false;
            this.textTotalExame.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // lblTotalItem
            // 
            this.lblTotalItem.AutoSize = true;
            this.lblTotalItem.Location = new System.Drawing.Point(6, 16);
            this.lblTotalItem.Name = "lblTotalItem";
            this.lblTotalItem.Size = new System.Drawing.Size(86, 13);
            this.lblTotalItem.TabIndex = 0;
            this.lblTotalItem.Text = "Total de Exames";
            // 
            // flowLayoutPanel1
            // 
            this.flowLayoutPanel1.Controls.Add(this.btnIncluirItem);
            this.flowLayoutPanel1.Controls.Add(this.btnExcluirItem);
            this.flowLayoutPanel1.Location = new System.Drawing.Point(6, 343);
            this.flowLayoutPanel1.Name = "flowLayoutPanel1";
            this.flowLayoutPanel1.Size = new System.Drawing.Size(752, 30);
            this.flowLayoutPanel1.TabIndex = 53;
            // 
            // frmProtocoloIncluir
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(784, 561);
            this.Name = "frmProtocoloIncluir";
            this.Text = "INCLUIR NOVO PROTOCOLO";
            this.spForm.Panel1.ResumeLayout(false);
            this.spForm.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.spForm)).EndInit();
            this.spForm.ResumeLayout(false);
            this.pnlForm.ResumeLayout(false);
            this.pnlForm.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.banner)).EndInit();
            this.flpAcao.ResumeLayout(false);
            this.grdItensProtocolo.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgItens)).EndInit();
            this.grbTotaisItens.ResumeLayout(false);
            this.grbTotaisItens.PerformLayout();
            this.flowLayoutPanel1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        protected System.Windows.Forms.FlowLayoutPanel flpAcao;
        protected System.Windows.Forms.Button btnFinalizar;
        protected System.Windows.Forms.Button btnFechar;
        protected System.Windows.Forms.Button btnExcluirItem;
        protected System.Windows.Forms.Button btnIncluirItem;
        protected System.Windows.Forms.Label lblLegenda;
        protected System.Windows.Forms.Label lblTextLegenda2;
        protected System.Windows.Forms.Label label2;
        protected System.Windows.Forms.Label lblTextLegenda1;
        protected System.Windows.Forms.Label lblLegenda1;
        protected System.Windows.Forms.GroupBox grbTotaisItens;
        protected System.Windows.Forms.TextBox textTotalDocumento;
        protected System.Windows.Forms.Label lblTotalDocumento;
        protected System.Windows.Forms.TextBox textQuantidadeProduto;
        protected System.Windows.Forms.Label lblTotalProduto;
        protected System.Windows.Forms.TextBox textEstudo;
        protected System.Windows.Forms.Label lblEstudo;
        protected System.Windows.Forms.TextBox textQuantidadeAso;
        protected System.Windows.Forms.Label lblQuantidadeAso;
        protected System.Windows.Forms.TextBox textExameLaudado;
        protected System.Windows.Forms.Label lblExameLaudado;
        protected System.Windows.Forms.TextBox textTotalExtra;
        protected System.Windows.Forms.Label lblExamesExtra;
        protected System.Windows.Forms.TextBox textTotalPcmso;
        protected System.Windows.Forms.Label lblTotalExamesPcmso;
        protected System.Windows.Forms.TextBox textTotalExame;
        protected System.Windows.Forms.Label lblTotalItem;
        protected System.Windows.Forms.MaskedTextBox textCodigo;
        protected System.Windows.Forms.TextBox lblCodigo;
        protected System.Windows.Forms.TextBox lblDataGravacao;
        protected System.Windows.Forms.Button btnExcluirCliente;
        protected System.Windows.Forms.Button btnCliente;
        protected System.Windows.Forms.TextBox textCliente;
        protected System.Windows.Forms.TextBox lblCliente;
        protected System.Windows.Forms.TextBox textDataGravacao;
        protected System.Windows.Forms.GroupBox grdItensProtocolo;
        protected System.Windows.Forms.DataGridView dgItens;
        protected System.Windows.Forms.FlowLayoutPanel flowLayoutPanel1;
        protected System.Windows.Forms.Button btn_novo;
    }
}