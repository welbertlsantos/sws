﻿namespace SWS.View
{
    partial class frmPcmsoAgenteIncluir
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.flpAcao = new System.Windows.Forms.FlowLayoutPanel();
            this.btnGravar = new System.Windows.Forms.Button();
            this.btnFechar = new System.Windows.Forms.Button();
            this.btnAgenteIncluir = new System.Windows.Forms.Button();
            this.textAgente = new System.Windows.Forms.TextBox();
            this.lblAgente = new System.Windows.Forms.TextBox();
            this.btnAgenteExcluir = new System.Windows.Forms.Button();
            this.lblExposicao = new System.Windows.Forms.TextBox();
            this.lblEfeito = new System.Windows.Forms.TextBox();
            this.lblTempoExposicao = new System.Windows.Forms.TextBox();
            this.lbMedicaControle = new System.Windows.Forms.ListBox();
            this.grbMedicaControle = new System.Windows.Forms.GroupBox();
            this.cbExposicao = new SWS.ComboBoxWithBorder();
            this.cbEfeito = new SWS.ComboBoxWithBorder();
            this.cbTempoExposicao = new SWS.ComboBoxWithBorder();
            ((System.ComponentModel.ISupportInitialize)(this.scForm)).BeginInit();
            this.scForm.Panel1.SuspendLayout();
            this.scForm.Panel2.SuspendLayout();
            this.scForm.SuspendLayout();
            this.pnlForm.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.banner)).BeginInit();
            this.flpAcao.SuspendLayout();
            this.grbMedicaControle.SuspendLayout();
            this.SuspendLayout();
            // 
            // scForm
            // 
            // 
            // scForm.Panel1
            // 
            this.scForm.Panel1.Controls.Add(this.flpAcao);
            // 
            // pnlForm
            // 
            this.pnlForm.Controls.Add(this.cbTempoExposicao);
            this.pnlForm.Controls.Add(this.cbEfeito);
            this.pnlForm.Controls.Add(this.cbExposicao);
            this.pnlForm.Controls.Add(this.grbMedicaControle);
            this.pnlForm.Controls.Add(this.lblTempoExposicao);
            this.pnlForm.Controls.Add(this.lblEfeito);
            this.pnlForm.Controls.Add(this.lblExposicao);
            this.pnlForm.Controls.Add(this.btnAgenteExcluir);
            this.pnlForm.Controls.Add(this.btnAgenteIncluir);
            this.pnlForm.Controls.Add(this.textAgente);
            this.pnlForm.Controls.Add(this.lblAgente);
            // 
            // flpAcao
            // 
            this.flpAcao.Controls.Add(this.btnGravar);
            this.flpAcao.Controls.Add(this.btnFechar);
            this.flpAcao.Dock = System.Windows.Forms.DockStyle.Fill;
            this.flpAcao.Location = new System.Drawing.Point(0, 0);
            this.flpAcao.Name = "flpAcao";
            this.flpAcao.Size = new System.Drawing.Size(534, 30);
            this.flpAcao.TabIndex = 0;
            // 
            // btnGravar
            // 
            this.btnGravar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnGravar.Image = global::SWS.Properties.Resources.Gravar;
            this.btnGravar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnGravar.Location = new System.Drawing.Point(3, 3);
            this.btnGravar.Name = "btnGravar";
            this.btnGravar.Size = new System.Drawing.Size(77, 23);
            this.btnGravar.TabIndex = 11;
            this.btnGravar.Text = "&Gravar";
            this.btnGravar.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnGravar.UseVisualStyleBackColor = true;
            this.btnGravar.Click += new System.EventHandler(this.btnGravar_Click);
            // 
            // btnFechar
            // 
            this.btnFechar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnFechar.Image = global::SWS.Properties.Resources.close;
            this.btnFechar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnFechar.Location = new System.Drawing.Point(86, 3);
            this.btnFechar.Name = "btnFechar";
            this.btnFechar.Size = new System.Drawing.Size(75, 23);
            this.btnFechar.TabIndex = 12;
            this.btnFechar.Text = "&Fechar";
            this.btnFechar.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnFechar.UseVisualStyleBackColor = true;
            this.btnFechar.Click += new System.EventHandler(this.btnFechar_Click);
            // 
            // btnAgenteIncluir
            // 
            this.btnAgenteIncluir.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnAgenteIncluir.Image = global::SWS.Properties.Resources.busca;
            this.btnAgenteIncluir.Location = new System.Drawing.Point(444, 3);
            this.btnAgenteIncluir.Name = "btnAgenteIncluir";
            this.btnAgenteIncluir.Size = new System.Drawing.Size(34, 21);
            this.btnAgenteIncluir.TabIndex = 6;
            this.btnAgenteIncluir.TabStop = false;
            this.btnAgenteIncluir.UseVisualStyleBackColor = true;
            this.btnAgenteIncluir.Click += new System.EventHandler(this.btnAgenteIncluir_Click);
            // 
            // textAgente
            // 
            this.textAgente.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.textAgente.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textAgente.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.textAgente.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textAgente.Location = new System.Drawing.Point(139, 3);
            this.textAgente.MaxLength = 100;
            this.textAgente.Name = "textAgente";
            this.textAgente.ReadOnly = true;
            this.textAgente.Size = new System.Drawing.Size(306, 21);
            this.textAgente.TabIndex = 7;
            this.textAgente.TabStop = false;
            // 
            // lblAgente
            // 
            this.lblAgente.BackColor = System.Drawing.Color.Silver;
            this.lblAgente.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblAgente.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblAgente.Location = new System.Drawing.Point(3, 3);
            this.lblAgente.MaxLength = 100;
            this.lblAgente.Name = "lblAgente";
            this.lblAgente.ReadOnly = true;
            this.lblAgente.Size = new System.Drawing.Size(137, 21);
            this.lblAgente.TabIndex = 8;
            this.lblAgente.Text = "Agente";
            // 
            // btnAgenteExcluir
            // 
            this.btnAgenteExcluir.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnAgenteExcluir.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnAgenteExcluir.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAgenteExcluir.Image = global::SWS.Properties.Resources.close;
            this.btnAgenteExcluir.Location = new System.Drawing.Point(477, 3);
            this.btnAgenteExcluir.Name = "btnAgenteExcluir";
            this.btnAgenteExcluir.Size = new System.Drawing.Size(34, 21);
            this.btnAgenteExcluir.TabIndex = 70;
            this.btnAgenteExcluir.TabStop = false;
            this.btnAgenteExcluir.UseVisualStyleBackColor = true;
            this.btnAgenteExcluir.Click += new System.EventHandler(this.btnAgenteExcluir_Click);
            // 
            // lblExposicao
            // 
            this.lblExposicao.BackColor = System.Drawing.Color.Silver;
            this.lblExposicao.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblExposicao.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblExposicao.Location = new System.Drawing.Point(3, 23);
            this.lblExposicao.MaxLength = 100;
            this.lblExposicao.Name = "lblExposicao";
            this.lblExposicao.ReadOnly = true;
            this.lblExposicao.Size = new System.Drawing.Size(137, 21);
            this.lblExposicao.TabIndex = 71;
            this.lblExposicao.Text = "Exposição";
            // 
            // lblEfeito
            // 
            this.lblEfeito.BackColor = System.Drawing.Color.Silver;
            this.lblEfeito.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblEfeito.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblEfeito.Location = new System.Drawing.Point(3, 43);
            this.lblEfeito.MaxLength = 100;
            this.lblEfeito.Name = "lblEfeito";
            this.lblEfeito.ReadOnly = true;
            this.lblEfeito.Size = new System.Drawing.Size(137, 21);
            this.lblEfeito.TabIndex = 72;
            this.lblEfeito.Text = "Efeito";
            // 
            // lblTempoExposicao
            // 
            this.lblTempoExposicao.BackColor = System.Drawing.Color.Silver;
            this.lblTempoExposicao.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblTempoExposicao.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTempoExposicao.Location = new System.Drawing.Point(3, 63);
            this.lblTempoExposicao.MaxLength = 100;
            this.lblTempoExposicao.Name = "lblTempoExposicao";
            this.lblTempoExposicao.ReadOnly = true;
            this.lblTempoExposicao.Size = new System.Drawing.Size(137, 21);
            this.lblTempoExposicao.TabIndex = 73;
            this.lblTempoExposicao.Text = "Tempo de Exposição";
            // 
            // lbMedicaControle
            // 
            this.lbMedicaControle.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lbMedicaControle.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbMedicaControle.ForeColor = System.Drawing.Color.Blue;
            this.lbMedicaControle.FormattingEnabled = true;
            this.lbMedicaControle.HorizontalScrollbar = true;
            this.lbMedicaControle.ItemHeight = 15;
            this.lbMedicaControle.Location = new System.Drawing.Point(3, 16);
            this.lbMedicaControle.Name = "lbMedicaControle";
            this.lbMedicaControle.ScrollAlwaysVisible = true;
            this.lbMedicaControle.Size = new System.Drawing.Size(502, 165);
            this.lbMedicaControle.TabIndex = 74;
            // 
            // grbMedicaControle
            // 
            this.grbMedicaControle.Controls.Add(this.lbMedicaControle);
            this.grbMedicaControle.Location = new System.Drawing.Point(3, 90);
            this.grbMedicaControle.Name = "grbMedicaControle";
            this.grbMedicaControle.Size = new System.Drawing.Size(508, 184);
            this.grbMedicaControle.TabIndex = 75;
            this.grbMedicaControle.TabStop = false;
            this.grbMedicaControle.Text = "Índice Medida de Controle";
            // 
            // cbExposicao
            // 
            this.cbExposicao.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.cbExposicao.BackColor = System.Drawing.Color.White;
            this.cbExposicao.BorderColor = System.Drawing.Color.DimGray;
            this.cbExposicao.BorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid;
            this.cbExposicao.FormattingEnabled = true;
            this.cbExposicao.Location = new System.Drawing.Point(139, 23);
            this.cbExposicao.Name = "cbExposicao";
            this.cbExposicao.Size = new System.Drawing.Size(372, 21);
            this.cbExposicao.TabIndex = 83;
            this.cbExposicao.SelectedIndexChanged += new System.EventHandler(this.cbExposicao_SelectedIndexChanged);
            // 
            // cbEfeito
            // 
            this.cbEfeito.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.cbEfeito.BackColor = System.Drawing.Color.White;
            this.cbEfeito.BorderColor = System.Drawing.Color.DimGray;
            this.cbEfeito.BorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid;
            this.cbEfeito.FormattingEnabled = true;
            this.cbEfeito.Location = new System.Drawing.Point(139, 43);
            this.cbEfeito.Name = "cbEfeito";
            this.cbEfeito.Size = new System.Drawing.Size(372, 21);
            this.cbEfeito.TabIndex = 84;
            this.cbEfeito.SelectedIndexChanged += new System.EventHandler(this.cbEfeito_SelectedIndexChanged);
            // 
            // cbTempoExposicao
            // 
            this.cbTempoExposicao.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.cbTempoExposicao.BackColor = System.Drawing.Color.White;
            this.cbTempoExposicao.BorderColor = System.Drawing.Color.DimGray;
            this.cbTempoExposicao.BorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid;
            this.cbTempoExposicao.FormattingEnabled = true;
            this.cbTempoExposicao.Location = new System.Drawing.Point(139, 63);
            this.cbTempoExposicao.Name = "cbTempoExposicao";
            this.cbTempoExposicao.Size = new System.Drawing.Size(372, 21);
            this.cbTempoExposicao.TabIndex = 85;
            // 
            // frmPcmsoAgenteIncluir
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(534, 361);
            this.Name = "frmPcmsoAgenteIncluir";
            this.Text = "INCLUIR AGENTE";
            this.scForm.Panel1.ResumeLayout(false);
            this.scForm.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.scForm)).EndInit();
            this.scForm.ResumeLayout(false);
            this.pnlForm.ResumeLayout(false);
            this.pnlForm.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.banner)).EndInit();
            this.flpAcao.ResumeLayout(false);
            this.grbMedicaControle.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        protected System.Windows.Forms.FlowLayoutPanel flpAcao;
        protected System.Windows.Forms.Button btnGravar;
        protected System.Windows.Forms.Button btnFechar;
        protected System.Windows.Forms.Button btnAgenteIncluir;
        protected System.Windows.Forms.TextBox textAgente;
        protected System.Windows.Forms.TextBox lblAgente;
        protected System.Windows.Forms.Button btnAgenteExcluir;
        protected System.Windows.Forms.TextBox lblExposicao;
        protected System.Windows.Forms.TextBox lblEfeito;
        protected System.Windows.Forms.TextBox lblTempoExposicao;
        protected System.Windows.Forms.GroupBox grbMedicaControle;
        protected System.Windows.Forms.ListBox lbMedicaControle;
        protected ComboBoxWithBorder cbTempoExposicao;
        protected ComboBoxWithBorder cbEfeito;
        protected ComboBoxWithBorder cbExposicao;
    }
}