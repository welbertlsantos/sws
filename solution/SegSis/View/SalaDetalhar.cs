﻿using SWS.Entidade;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace SWS.View
{
    public partial class frmSalaDetalhar : frmSalaIncluir
    {
        public frmSalaDetalhar(Sala sala) :base()
        {
            InitializeComponent();
            this.Sala = sala;
            this.ExamesInSala = sala.Exames;
            textNome.Text = sala.Descricao;
            textNome.ReadOnly = true;
            textAndar.Text = sala.Andar.ToString();
            textAndar.Enabled = false;
            cbVIP.SelectedValue = sala.Vip ? "true" : "false";
            cbVIP.Enabled = false;
            montaGrid();
            dgvExame.ReadOnly = true;
            btn_incluir.Enabled = false;
            btn_incluirExame.Enabled = false;
            btn_excluirExame.Enabled = false;
            this.textSlug.Text = sala.SlugSala;

        }
    }
}
