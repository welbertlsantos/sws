﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace SWS.View
{
    public partial class frm_ppraReplicarOld : BaseFormConsultaAuxiliar
    {
        frm_ppraOld formPpra;

        public static String Msg01 = " É obrigatório inserir um comentário ";
        public static String Msg02 = " Atenção ";
        private int tipoForm = 0;

        frm_pcmso formPcmso;

        public frm_ppraReplicarOld(frm_ppraOld formPpra)
        {
            InitializeComponent();
            this.formPpra = formPpra;
            tipoForm = 1;
        }

        public frm_ppraReplicarOld(frm_pcmso formPcmso)
        {
            InitializeComponent();
            this.formPcmso = formPcmso;
            tipoForm = 2;
        }

        private void bt_ok_Click(object sender, EventArgs e)
        {
            try
            {
                if (!String.IsNullOrEmpty(text_comentario.Text))
                {
                    if (tipoForm == 1)
                    {
                        formPpra.comentario = Convert.ToString(text_comentario.Text.ToUpper());
                        this.Close();
                    }
                    else
                    {
                        formPcmso.comentario = Convert.ToString(text_comentario.Text.ToUpper());
                        this.Close();
                    }
                }
                else
                {
                    MessageBox.Show(Msg01, Msg02);
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }

        }

        private void frm_ppraReplicar_Load(object sender, EventArgs e)
        {
            text_comentario.Focus();
        }
    }
}
