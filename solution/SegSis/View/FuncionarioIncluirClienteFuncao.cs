﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using SegSis.Facade;
using SegSis.Entidade;
using SegSis.Excecao;

namespace SegSis.View
{
    public partial class frm_FuncionarioIncluirClienteFuncao : BaseFormConsulta
    {

        public static String msg1 = "Selecione uma linha.";
        public static String msg2 = "Atençao";

        Cliente cliente = null;

        ClienteFuncaoFuncionario clienteFuncaoFuncionario = null;

        public ClienteFuncaoFuncionario getClienteFuncaoFuncionario()
        {
            return this.clienteFuncaoFuncionario;
        }
        
        public frm_FuncionarioIncluirClienteFuncao(Cliente cliente)
        {
            InitializeComponent();
            this.cliente = cliente;
        }

        private void btn_pesquisar_Click(object sender, EventArgs e)
        {
            try
            {
                grd_funcao.Columns.Clear();
                montaDataGrid();
                text_descricao.Focus();

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }

        }

        private void btn_ok_Click(object sender, EventArgs e)
        {
            try
            {
                if (grd_funcao.CurrentRow != null)
                {
                    Int64 id = (Int64)grd_funcao.CurrentRow.Cells[0].Value; 
                    
                    PpraFacade ppraFacade = PpraFacade.getInstance();

                    clienteFuncaoFuncionario = new ClienteFuncaoFuncionario(null, ppraFacade.findClienteFuncaoById(id), null, null, null, null, string.Empty, false);

                    frm_FuncionarioIncluirClienteFuncaoFuncionario formFuncionarioIncluirClienteFuncaoFuncionario = new frm_FuncionarioIncluirClienteFuncaoFuncionario(clienteFuncaoFuncionario);
                    formFuncionarioIncluirClienteFuncaoFuncionario.ShowDialog();

                    clienteFuncaoFuncionario = formFuncionarioIncluirClienteFuncaoFuncionario.getClienteFuncaoFuncionario();

                    this.Close();

                }
                else
                {
                    MessageBox.Show(msg1, msg2);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }

        }

        private void btn_fechar_Click(object sender, EventArgs e)
        {
            this.Close();

        }

        public void montaDataGrid()
        {
            try
            {
                this.Cursor = Cursors.WaitCursor;

                ClienteFacade clienteFacade = ClienteFacade.getInstance();

                Funcao funcao = new Funcao();

                funcao.setDescricao(text_descricao.Text.ToUpper());
                funcao.setCodCbo(text_cbo.Text);

                ClienteFuncao clienteFuncao = new ClienteFuncao();

                clienteFuncao.setCliente(cliente);
                clienteFuncao.setFuncao(funcao);

                DataSet ds = clienteFacade.findFuncaoByClienteFuncao(clienteFuncao);

                grd_funcao.DataSource = ds.Tables["Funcoes"].DefaultView;

                grd_funcao.Columns[0].HeaderText = "id_seg_cliente_funcao";
                grd_funcao.Columns[0].Visible = false;

                grd_funcao.Columns[1].HeaderText = "ID_FUNCAO";
                grd_funcao.Columns[1].Visible = false;

                grd_funcao.Columns[2].HeaderText = "Descrição";
                
                grd_funcao.Columns[3].HeaderText = "Codigo CBO";

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }

        }

        private void text_cbo_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!Char.IsDigit(e.KeyChar) && e.KeyChar != (char)8)
            {
                e.Handled = true;

            }
        }

        private void btn_novo_Click(object sender, EventArgs e)
        {
            frm_FuncionarioIncluirClienteFuncaoIncluirFuncao formFuncionarioIncluirFuncao = new frm_FuncionarioIncluirClienteFuncaoIncluirFuncao(cliente);
            formFuncionarioIncluirFuncao.ShowDialog();

            if (formFuncionarioIncluirFuncao.getClienteFuncao() != null)
            {

                clienteFuncaoFuncionario = new ClienteFuncaoFuncionario(null, formFuncionarioIncluirFuncao.getClienteFuncao(), null, null,
                    null, null, string.Empty, false);

                frm_FuncionarioIncluirClienteFuncaoFuncionario formFuncionarioIncluirClienteFuncaoFuncionario = new frm_FuncionarioIncluirClienteFuncaoFuncionario(clienteFuncaoFuncionario);
                formFuncionarioIncluirClienteFuncaoFuncionario.ShowDialog();

                if (formFuncionarioIncluirClienteFuncaoFuncionario.getClienteFuncaoFuncionario() != null)
                {
                    clienteFuncaoFuncionario = formFuncionarioIncluirClienteFuncaoFuncionario.getClienteFuncaoFuncionario();
                    
                    this.Close();
                }

            }
        }
    }
}
