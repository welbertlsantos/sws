﻿using SWS.Entidade;
using SWS.Facade;
using SWS.View.Resources;
using SWS.View.ViewHelper;
using SWS.ViewHelper;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace SWS.View
{
    public partial class frmSalas : frmTemplate
    {
        private Sala sala;

        public Sala Sala
        {
            get { return sala; }
            set { sala = value; }
        }
        
        public frmSalas()
        {
            InitializeComponent();
            validaPermissoes();
            ComboHelper.comboSituacao(cbSituacao);
        }

        private void btn_incluir_Click(object sender, EventArgs e)
        {
            try
            {
                frmSalaIncluir formSalaIncluir = new frmSalaIncluir();
                formSalaIncluir.ShowDialog();

                if (formSalaIncluir.Sala != null)
                {
                    sala = new Sala(null, string.Empty, string.Empty, 0, false, PermissionamentoFacade.usuarioAutenticado.Empresa, string.Empty, 0);
                    montaDataGrid();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btn_alterar_Click(object sender, EventArgs e)
        {
            try
            {
                if (this.dg_sala.CurrentRow == null)
                    throw new Exception("Selecione uma linha.");
                    
                FilaFacade filaFacade = FilaFacade.getInstance();

                sala = filaFacade.findSalaById((long)this.dg_sala.CurrentRow.Cells[0].Value);

                frmSalaAlterar formSalaAlterar = new frmSalaAlterar(sala);
                formSalaAlterar.ShowDialog();

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }

        private void btn_excluir_Click(object sender, EventArgs e)
        {
            try
            {
                if (this.dg_sala.CurrentRow == null)
                    throw new Exception("Selecione uma linha.");

                if (MessageBox.Show("Deseja excluir a sala selecionada?", "Atenção", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                {

                    this.Cursor = Cursors.WaitCursor;
                    FilaFacade filaFacade = FilaFacade.getInstance();

                    sala = filaFacade.findSalaById((long)this.dg_sala.CurrentRow.Cells[0].Value);

                    if (!String.Equals(sala.Situacao, ApplicationConstants.ATIVO))
                        throw new Exception("Somente salas ativas podem ser excluídas.");

                    sala.Situacao = ApplicationConstants.DESATIVADO;
                    filaFacade.updateSala(sala);

                    MessageBox.Show("Sala excluída com sucesso.", "Confirmação", MessageBoxButtons.OK, MessageBoxIcon.Information);

                    sala = new Sala(null, string.Empty, string.Empty, 0, false, PermissionamentoFacade.usuarioAutenticado.Empresa, string.Empty, 0);
                    this.montaDataGrid();

                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }

        }

        private void btn_reativar_Click(object sender, EventArgs e)
        {
            try
            {
                if (this.dg_sala.CurrentRow == null)
                    throw new Exception("Selecione uma linha.");

                if (MessageBox.Show("Deseja reativar a sala selecionada?", "Atenção", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                {

                    this.Cursor = Cursors.WaitCursor;
                    FilaFacade filaFacade = FilaFacade.getInstance();

                    sala = filaFacade.findSalaById((long)this.dg_sala.CurrentRow.Cells[0].Value);

                    if (!String.Equals(sala.Situacao, ApplicationConstants.DESATIVADO))
                        throw new Exception("Somente salas excluídas podem ser reativadas.");

                    sala.Situacao = ApplicationConstants.ATIVO;
                    filaFacade.updateSala(sala);

                    MessageBox.Show("Sala reativada com sucesso.", "Confirmaçao", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    this.sala = new Sala(null, string.Empty, string.Empty, 0, false, PermissionamentoFacade.usuarioAutenticado.Empresa, string.Empty, 0);

                    this.montaDataGrid();
                }
                
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }

        }

        private void btn_detalhar_Click(object sender, EventArgs e)
        {
            try
            {
                if (this.dg_sala.CurrentRow == null)
                    throw new Exception("Selecione uma linha.");
                
                FilaFacade filaFacade = FilaFacade.getInstance();

                frmSalaDetalhar formSalaDetalhar = new frmSalaDetalhar(filaFacade.findSalaById((long)dg_sala.CurrentRow.Cells[0].Value));
                formSalaDetalhar.ShowDialog();
                
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }

        private void btn_buscar_Click(object sender, EventArgs e)
        {
            try
            {
                this.sala = new Sala(null, textSala.Text, (((SelectItem)cbSituacao.SelectedItem).Valor.ToString()), 0, false, PermissionamentoFacade.usuarioAutenticado.Empresa, string.Empty, 0);
                montaDataGrid();
                textSala.Focus();

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }

        private void btn_fechar_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void validaPermissoes()
        {
            PermissionamentoFacade permissionamentoFacade = PermissionamentoFacade.getInstance();

            this.btnIncluir.Enabled = permissionamentoFacade.hasPermission("SALA_ATENDIMENTO", "INCLUIR");
            this.btnAlterar.Enabled = permissionamentoFacade.hasPermission("SALA_ATENDIMENTO", "ALTERAR");
            this.btnExcluir.Enabled = permissionamentoFacade.hasPermission("SALA_ATENDIMENTO", "EXCLUIR");
            this.btnDetalhar.Enabled = permissionamentoFacade.hasPermission("SALA_ATENDIMENTO", "DETALHAR");
            this.btnReativar.Enabled = permissionamentoFacade.hasPermission("SALA_ATENDIMENTO", "REATIVAR");
        }

        public void montaDataGrid()
        {
            try
            {
                this.dg_sala.Columns.Clear();
                this.Cursor = Cursors.WaitCursor;

                FilaFacade filaFacade = FilaFacade.getInstance();

                DataSet ds = filaFacade.findSalaByFilter(sala);

                dg_sala.DataSource = ds.Tables["Salas"].DefaultView;

                dg_sala.Columns[0].HeaderText = "ID";
                dg_sala.Columns[0].Visible = false;
                dg_sala.Columns[1].HeaderText = "Descrição";
                dg_sala.Columns[2].HeaderText = "Situação";
                dg_sala.Columns[2].Visible = false;
                dg_sala.Columns[3].HeaderText = "Andar";
                dg_sala.Columns[4].HeaderText = "VIP";
                dg_sala.Columns[5].HeaderText = "Slug";

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }


        }

        private void frmSalas_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter || e.KeyCode == Keys.Return)
                SendKeys.Send("{TAB}");
        }

        private void dg_sala_CellContentDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            btnAlterar.PerformClick();
        }

        private void dg_sala_RowPrePaint(object sender, DataGridViewRowPrePaintEventArgs e)
        {
            DataGridView dgv = sender as DataGridView;

            if (string.Equals(dgv.Rows[e.RowIndex].Cells[2].Value, ApplicationConstants.DESATIVADO))
                dgv.Rows[e.RowIndex].DefaultCellStyle.ForeColor = Color.Red;
        }

        private void btnExameSemSala_Click(object sender, EventArgs e)
        {
            frmExameSemSala exameSemSala = new frmExameSemSala();
            exameSemSala.ShowDialog();
        }
    }
}
