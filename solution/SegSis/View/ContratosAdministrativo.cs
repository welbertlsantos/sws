﻿using SWS.Entidade;
using SWS.Facade;
using SWS.View.ViewHelper;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace SWS.View
{
    public partial class frmContratosAdministrativo : frmTemplate
    {
        private Cliente cliente;
        private Contrato contrato;

        public frmContratosAdministrativo()
        {
            InitializeComponent();
        }

        private void btn_fechar_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btn_cliente_Click(object sender, EventArgs e)
        {
            cliente = null;
            text_cliente.Text = String.Empty;

            frmClienteSelecionar formCliente = new frmClienteSelecionar(null, null, null, null, true, null, null, null, null);
            formCliente.ShowDialog();

            if (formCliente.Cliente != null)
            {
                cliente = formCliente.Cliente;
                text_cliente.Text = cliente.RazaoSocial.ToUpper();

                /* verificando os contratos do cliente */

                FinanceiroFacade financeiroFacade = FinanceiroFacade.getInstance();

                List<Contrato> contratos = financeiroFacade.findAllContratoByCliente(cliente, null);

                if (contratos.Count > 0)
                {
                    frmContratosSelecionar selecionarContrato = new frmContratosSelecionar(contratos);
                    selecionarContrato.ShowDialog();

                    if (selecionarContrato.Contrato != null)
                    {
                        contrato = selecionarContrato.Contrato;
                        MontaDataGridExame();
                        MontaDataGridProduto();
                    }
                }

            }
        }

        private void btn_incluirExame_Click(object sender, EventArgs e)
        {
            try
            {
                frmExameBuscar exameBuscar = new frmExameBuscar();
                exameBuscar.ShowDialog();

                if (exameBuscar.Exame != null)
                {
                    /* verificando se o exame não está presente na grid */

                    List<Exame> examesInGrid = montaListaExamesInGrid();

                    if (examesInGrid.Contains(exameBuscar.Exame))
                        throw new Exception("Exame já incluído.");

                    /* incluindo o exame no contrato do cliente */

                    FinanceiroFacade financeiroFacade = FinanceiroFacade.getInstance();

                    financeiroFacade.insertContratoExame(new ContratoExame(null, exameBuscar.Exame, contrato, exameBuscar.Exame.Preco, PermissionamentoFacade.usuarioAutenticado, exameBuscar.Exame.Custo, DateTime.Now, "A", true));
                    MontaDataGridExame();

                    /* localizando o exame na grid e posicionando  seleção no exame inserido. */

                    int index = 0;
                    foreach (DataGridViewRow dvRow in dgvExame.Rows)
                    {
                        if ((long)dvRow.Cells[1].Value == exameBuscar.Exame.Id)
                        {
                            index = dvRow.Index;
                            break;
                        }
                    }
                    dgvExame.CurrentCell = dgvExame[1, index];
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btnExcluirExame_Click(object sender, EventArgs e)
        {
            try
            {

                if (dgvExame.CurrentRow == null)
                    throw new Exception("Selecione um exame.");

                /* excluindo o exame do contrato do cliente */

                FinanceiroFacade financeiroFacade = FinanceiroFacade.getInstance();

                financeiroFacade.deleteContratoExame(new ContratoExame((long)dgvExame.CurrentRow.Cells[0].Value, new Exame((long)dgvExame.CurrentRow.Cells[1].Value, dgvExame.CurrentRow.Cells[2].Value.ToString(), dgvExame.CurrentRow.Cells[8].Value.ToString(), Convert.ToBoolean(dgvExame.CurrentRow.Cells[9].Value), Convert.ToDecimal(dgvExame.CurrentRow.Cells[3].Value), (int)dgvExame.CurrentRow.Cells[10].Value, Convert.ToDecimal(dgvExame.CurrentRow.Cells[4].Value), Convert.ToBoolean(dgvExame.CurrentRow.Cells[11].Value), Convert.ToBoolean(dgvExame.CurrentRow.Cells[12].Value), string.Empty, false, null, false), contrato, Convert.ToDecimal(dgvExame.CurrentRow.Cells[3].Value), PermissionamentoFacade.usuarioAutenticado, Convert.ToDecimal(dgvExame.CurrentRow.Cells[4].Value), Convert.ToDateTime(dgvExame.CurrentRow.Cells[5].Value), dgvExame.CurrentRow.Cells[6].Value.ToString(), Convert.ToBoolean(dgvExame.CurrentRow.Cells[7].Value)));

                MessageBox.Show("Exame excluído com sucesso.", "Confirmação", MessageBoxButtons.OK, MessageBoxIcon.Information);

                MontaDataGridExame();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btnExcluirProduto_Click(object sender, EventArgs e)
        {
            try
            {

                if (dgvProduto.CurrentRow == null)
                    throw new Exception("Selecione um produto.");

                /* excluindo o produto do contrato do cliente */

                FinanceiroFacade financeiroFacade = FinanceiroFacade.getInstance();

                financeiroFacade.deleteContratoProduto(new ContratoProduto((long)dgvProduto.CurrentRow.Cells[0].Value, new Produto((long)dgvProduto.CurrentRow.Cells[1].Value, dgvProduto.CurrentRow.Cells[2].Value.ToString(), dgvProduto.CurrentRow.Cells[8].Value.ToString(), Convert.ToDecimal(dgvProduto.CurrentRow.Cells[3].Value), dgvProduto.CurrentRow.Cells[9].Value.ToString(), Convert.ToDecimal(dgvProduto.CurrentRow.Cells[4].Value), (bool)dgvProduto.CurrentRow.Cells[12].Value, string.Empty), contrato, Convert.ToDecimal(dgvProduto.CurrentRow.Cells[3].Value), PermissionamentoFacade.usuarioAutenticado, Convert.ToDecimal(dgvProduto.CurrentRow.Cells[4].Value), Convert.ToDateTime(dgvProduto.CurrentRow.Cells[5].Value), "I", true));

                MontaDataGridProduto();

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void dgvExame_CellBeginEdit(object sender, DataGridViewCellCancelEventArgs e)
        {
            dgvExame.CurrentCell.Tag = dgvExame.CurrentCell.Value;
            dgvExame.CurrentCell.Value = String.Empty;
        }

        private void dgvExame_CellEndEdit(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                if (String.IsNullOrEmpty(dgvExame.CurrentCell.EditedFormattedValue.ToString()))
                    dgvExame.CurrentCell.Value = dgvExame.CurrentCell.Tag.ToString();

                dgvExame.CurrentCell.Value = ValidaCampoHelper.FormataValorMonetario(dgvExame.CurrentCell.Value.ToString());

                dgvExame.Rows[e.RowIndex].Cells[13].Value = calculaLucro(Convert.ToDecimal(dgvExame.Rows[e.RowIndex].Cells[3].EditedFormattedValue), Convert.ToDecimal(dgvExame.Rows[e.RowIndex].Cells[4].EditedFormattedValue));

                /* realizando update do contratoExame */

                FinanceiroFacade financeiroFacade = FinanceiroFacade.getInstance();

                financeiroFacade.updateContratoExame(new ContratoExame((long)dgvExame.Rows[e.RowIndex].Cells[0].Value, new Exame((long)dgvExame.CurrentRow.Cells[1].Value, dgvExame.CurrentRow.Cells[2].Value.ToString(), dgvExame.CurrentRow.Cells[8].Value.ToString(), Convert.ToBoolean(dgvExame.CurrentRow.Cells[9].Value), Convert.ToDecimal(dgvExame.CurrentRow.Cells[3].Value), (int)dgvExame.CurrentRow.Cells[10].Value, Convert.ToDecimal(dgvExame.CurrentRow.Cells[4].Value), Convert.ToBoolean(dgvExame.CurrentRow.Cells[11].Value), Convert.ToBoolean(dgvExame.CurrentRow.Cells[12].Value), string.Empty, false, null, false), contrato, Convert.ToDecimal(dgvExame.Rows[e.RowIndex].Cells[3].EditedFormattedValue), PermissionamentoFacade.usuarioAutenticado, Convert.ToDecimal(dgvExame.Rows[e.RowIndex].Cells[4].EditedFormattedValue), Convert.ToDateTime(dgvExame.Rows[e.RowIndex].Cells[5].Value), dgvExame.Rows[e.RowIndex].Cells[6].Value.ToString(), Convert.ToBoolean(dgvExame.Rows[e.RowIndex].Cells[7].Value)));
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void dgvExame_EditingControlShowing(object sender, DataGridViewEditingControlShowingEventArgs e)
        {
            if (e.Control is DataGridViewTextBoxEditingControl)
                e.Control.KeyPress += new KeyPressEventHandler(dgvExame_KeyPress);
        }

        private void dgvExame_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = ValidaCampoHelper.FormataDigitacaoNumero(sender, e, dgvExame.CurrentCell.EditedFormattedValue.ToString(), 2);
        }

        private void dgvProduto_CellBeginEdit(object sender, DataGridViewCellCancelEventArgs e)
        {
            dgvProduto.CurrentCell.Tag = dgvProduto.CurrentCell.Value;
            dgvProduto.CurrentCell.Value = String.Empty;
        }

        private void dgvProduto_CellEndEdit(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                if (String.IsNullOrEmpty(dgvProduto.CurrentCell.EditedFormattedValue.ToString()))
                    dgvProduto.CurrentCell.Value = dgvProduto.CurrentCell.Tag.ToString();

                dgvProduto.CurrentCell.Value = ValidaCampoHelper.FormataValorMonetario(dgvProduto.CurrentCell.Value.ToString());

                dgvProduto.Rows[e.RowIndex].Cells[11].Value = calculaLucro(Convert.ToDecimal(dgvProduto.Rows[e.RowIndex].Cells[3].EditedFormattedValue), Convert.ToDecimal(dgvProduto.Rows[e.RowIndex].Cells[4].EditedFormattedValue));

                /* realizando o update do contratoProduto */

                FinanceiroFacade financeiroFacade = FinanceiroFacade.getInstance();

                financeiroFacade.updateContratoProduto(new ContratoProduto((long)dgvProduto.CurrentRow.Cells[0].Value, new Produto((long)dgvProduto.CurrentRow.Cells[1].Value, dgvProduto.CurrentRow.Cells[2].Value.ToString(), dgvProduto.CurrentRow.Cells[8].Value.ToString(), Convert.ToDecimal(dgvProduto.CurrentRow.Cells[3].Value), dgvProduto.CurrentRow.Cells[9].Value.ToString(), Convert.ToDecimal(dgvProduto.CurrentRow.Cells[4].Value), (bool)dgvProduto.CurrentRow.Cells[12].Value, string.Empty), contrato, Convert.ToDecimal(dgvProduto.CurrentRow.Cells[3].EditedFormattedValue), PermissionamentoFacade.usuarioAutenticado, Convert.ToDecimal(dgvProduto.CurrentRow.Cells[4].EditedFormattedValue), Convert.ToDateTime(dgvProduto.CurrentRow.Cells[5].Value), "A", true));

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void dgvProduto_EditingControlShowing(object sender, DataGridViewEditingControlShowingEventArgs e)
        {
            if (e.Control is DataGridViewTextBoxEditingControl)
                e.Control.KeyPress += new KeyPressEventHandler(dgvProduto_KeyPress);
        }

        private void dgvProduto_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = ValidaCampoHelper.FormataDigitacaoNumero(sender, e, dgvProduto.CurrentCell.EditedFormattedValue.ToString(), 2);
        }

        private void MontaDataGridExame()
        {
            try
            {
                dgvExame.Columns.Clear();

                FinanceiroFacade financeiroFacade = FinanceiroFacade.getInstance();
                this.Cursor = Cursors.WaitCursor;
                DataSet ds = financeiroFacade.findAllContratoExameByContrato(contrato);

                dgvExame.ColumnCount = 14;

                dgvExame.Columns[0].HeaderText = "Id_contrato_exame";
                dgvExame.Columns[0].Visible = false;

                dgvExame.Columns[1].HeaderText = "Código";
                dgvExame.Columns[1].ReadOnly = true;

                dgvExame.Columns[2].HeaderText = "Exame";
                dgvExame.Columns[2].ReadOnly = true;

                dgvExame.Columns[3].HeaderText = "Preço no Contrato R$";
                dgvExame.Columns[3].ReadOnly = false;
                dgvExame.Columns[3].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
                dgvExame.Columns[3].HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleRight;
                dgvExame.Columns[3].DefaultCellStyle.BackColor = Color.LemonChiffon;

                dgvExame.Columns[4].HeaderText = "Custo no Contrato R$";
                dgvExame.Columns[4].ReadOnly = false;
                dgvExame.Columns[4].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
                dgvExame.Columns[4].HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleRight;
                dgvExame.Columns[4].DefaultCellStyle.BackColor = Color.LemonChiffon;

                dgvExame.Columns[5].HeaderText = "Data Inclusão";
                dgvExame.Columns[5].Visible = false;

                dgvExame.Columns[6].HeaderText = "Situacao";
                dgvExame.Columns[6].Visible = false;

                dgvExame.Columns[7].HeaderText = "flagAltera";
                dgvExame.Columns[7].Visible = false;

                dgvExame.Columns[8].HeaderText = "Situação Exame";
                dgvExame.Columns[8].Visible = false;

                dgvExame.Columns[9].HeaderText = "Laboratório";
                dgvExame.Columns[9].Visible = false;

                dgvExame.Columns[10].HeaderText = "Prioridade";
                dgvExame.Columns[10].Visible = false;

                dgvExame.Columns[11].HeaderText = "Externo";
                dgvExame.Columns[11].Visible = false;

                dgvExame.Columns[12].HeaderText = "Libera Documento";
                dgvExame.Columns[12].Visible = false;

                dgvExame.Columns[13].HeaderText = "Lucro Bruto %";
                dgvExame.Columns[13].ReadOnly = true;

                foreach (DataRow dataRow in ds.Tables[0].Rows)
                    dgvExame.Rows.Add(dataRow["id_contrato_exame"], dataRow["id_exame"], dataRow["descricao"], dataRow["preco_contrato"], dataRow["custo_contrato"], dataRow["data_inclusao"], dataRow["situacao"], dataRow["altera"], dataRow["situacao_exame"], dataRow["laboratorio"], dataRow["prioridade"], dataRow["externo"], dataRow["libera_documento"], dataRow["lucro"]);



            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }

        private void MontaDataGridProduto()
        {
            try
            {
                dgvProduto.Columns.Clear();

                this.Cursor = Cursors.WaitCursor;

                FinanceiroFacade financeiroFacade = FinanceiroFacade.getInstance();

                DataSet ds = financeiroFacade.findAllContratoProdutoByContrato(contrato);

                dgvProduto.ColumnCount = 13;

                dgvProduto.Columns[0].HeaderText = "Id_Produto_contrato";
                dgvProduto.Columns[0].Visible = false;

                dgvProduto.Columns[1].HeaderText = "Código";
                dgvProduto.Columns[1].ReadOnly = true;

                dgvProduto.Columns[2].HeaderText = "Produto";
                dgvProduto.Columns[2].ReadOnly = true;

                dgvProduto.Columns[3].HeaderText = "Preço no Contrato R$";
                dgvProduto.Columns[3].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
                dgvProduto.Columns[3].HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleRight;
                dgvProduto.Columns[3].DefaultCellStyle.BackColor = Color.LemonChiffon;

                dgvProduto.Columns[4].HeaderText = "Custo no Contrato R$";
                dgvProduto.Columns[4].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
                dgvProduto.Columns[4].HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleRight;
                dgvProduto.Columns[4].DefaultCellStyle.BackColor = Color.LemonChiffon;

                dgvProduto.Columns[5].HeaderText = "Data Inclusão";
                dgvProduto.Columns[5].Visible = false;

                dgvProduto.Columns[6].HeaderText = "Situacao";
                dgvProduto.Columns[6].Visible = false;

                dgvProduto.Columns[7].HeaderText = "FlagAltera";
                dgvProduto.Columns[7].Visible = false;

                dgvProduto.Columns[8].HeaderText = "Situação Produto";
                dgvProduto.Columns[8].Visible = false;

                dgvProduto.Columns[9].HeaderText = "Tipo";
                dgvProduto.Columns[9].Visible = false;

                dgvProduto.Columns[10].HeaderText = "Tipo Estudo";
                dgvProduto.Columns[10].Visible = false;

                dgvProduto.Columns[11].HeaderText = "Lucro Bruto %";
                dgvProduto.Columns[11].ReadOnly = true;

                dgvProduto.Columns[12].HeaderText = "Padrao Contrato";
                dgvProduto.Columns[12].Visible = false;
                

                foreach (DataRow dataRow in ds.Tables[0].Rows)
                    dgvProduto.Rows.Add(dataRow["id_produto_contrato"], dataRow["id_produto"], dataRow["nome"], dataRow["preco_contratado"], dataRow["custo_contrato"], dataRow["data_inclusao"], dataRow["situacao"], dataRow["altera"], dataRow["situacao_produto"], dataRow["tipo"], dataRow["tipo_estudo"], dataRow["lucro"], dataRow["padrao_contrato"]);

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }

        private List<Exame> montaListaExamesInGrid()
        {
            List<Exame> examesInGrid = new List<Exame>();

            foreach (DataGridViewRow dvRow in dgvExame.Rows)
                examesInGrid.Add(new Exame((long)dvRow.Cells[1].Value, dvRow.Cells[2].Value.ToString(), dvRow.Cells[8].Value.ToString(), Convert.ToBoolean(dvRow.Cells[9].Value), Convert.ToDecimal(dvRow.Cells[3].Value), Convert.ToInt32(dvRow.Cells[10].Value), Convert.ToDecimal(dvRow.Cells[4].Value), Convert.ToBoolean(dvRow.Cells[11].Value), Convert.ToBoolean(dvRow.Cells[12].Value), string.Empty, false, null, false));

            return examesInGrid;
        }

        private void btn_incluirProduto_Click(object sender, EventArgs e)
        {
            try
            {
                frmProdutoBusca produtoBusca = new frmProdutoBusca();
                produtoBusca.ShowDialog();

                if (produtoBusca.Produto != null)
                {
                    /* verificando se o produto selecionado não está incluído. */

                    List<Produto> produtosInGrid = montaListaProdutosInGrid();

                    if (produtosInGrid.Contains(produtoBusca.Produto))
                        throw new Exception("Produto já incluído.");

                    /* incluindo o produto no contrato do cliente */

                    FinanceiroFacade financeiroFacade = FinanceiroFacade.getInstance();

                    financeiroFacade.insertContratoProduto(new ContratoProduto(null, produtoBusca.Produto, contrato, produtoBusca.Produto.Preco, PermissionamentoFacade.usuarioAutenticado, produtoBusca.Produto.Custo, DateTime.Now, "A", true));

                    MontaDataGridProduto();

                    int index = 0;
                    foreach (DataGridViewRow dvRow in dgvProduto.Rows)
                    {
                        if ((long)dvRow.Cells[1].Value == produtoBusca.Produto.Id)
                        {
                            index = dvRow.Index;
                            break;
                        }
                    }

                    dgvProduto.CurrentCell = dgvProduto[1, index];
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private List<Produto> montaListaProdutosInGrid()
        {
            List<Produto> produtosInGrid = new List<Produto>();

            foreach (DataGridViewRow dvRow in dgvProduto.Rows)
                produtosInGrid.Add(new Produto((long)dvRow.Cells[1].Value, dvRow.Cells[2].Value.ToString(), dvRow.Cells[8].Value.ToString(), Convert.ToDecimal(dvRow.Cells[3].Value), dvRow.Cells[9].Value.ToString(), Convert.ToDecimal(dvRow.Cells[4].Value), (bool)dgvProduto.CurrentRow.Cells[12].Value, string.Empty));

            return produtosInGrid;
        }

        private string calculaLucro(decimal preco, decimal custo)
        {
            return preco == 0 || custo == 0 ? "0,00" : Convert.ToString(Math.Round((((preco / custo) - 1) * 100), 2));
        }

        private void dgvExame_DataBindingComplete(object sender, DataGridViewBindingCompleteEventArgs e)
        {
            DataGridView dgv = sender as DataGridView;
            dgv.ClearSelection();
        }

        private void dgvProduto_DataBindingComplete(object sender, DataGridViewBindingCompleteEventArgs e)
        {
            DataGridView dgv = sender as DataGridView;
            dgv.ClearSelection();
        }
    }
}
