﻿namespace SWS.View
{
    partial class frmMonitoramentoAgente
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        protected void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmMonitoramentoAgente));
            this.flpAcao = new System.Windows.Forms.FlowLayoutPanel();
            this.btnIncluir = new System.Windows.Forms.Button();
            this.lblFechar = new System.Windows.Forms.Button();
            this.lblAgente = new System.Windows.Forms.TextBox();
            this.textAgente = new System.Windows.Forms.TextBox();
            this.lblRisco = new System.Windows.Forms.TextBox();
            this.TextRisco = new System.Windows.Forms.TextBox();
            this.lblTpAvaliacao = new System.Windows.Forms.TextBox();
            this.cbTipoAvaliacao = new SWS.ComboBoxWithBorder();
            this.lblIntensidade = new System.Windows.Forms.TextBox();
            this.textIntensidade = new System.Windows.Forms.TextBox();
            this.lblLimite = new System.Windows.Forms.TextBox();
            this.textLimiteTolerancia = new System.Windows.Forms.TextBox();
            this.lblUnidade = new System.Windows.Forms.TextBox();
            this.cbUnidadeMedida = new SWS.ComboBoxWithBorder();
            this.textTecnica = new System.Windows.Forms.TextBox();
            this.lblUtilizaEpc = new System.Windows.Forms.TextBox();
            this.cbUtilizaEPC = new SWS.ComboBoxWithBorder();
            this.lblEficaciaEpc = new System.Windows.Forms.TextBox();
            this.cbEficaciaEpc = new SWS.ComboBoxWithBorder();
            this.lblUtilizaEPI = new System.Windows.Forms.TextBox();
            this.cbUtilizaEPI = new SWS.ComboBoxWithBorder();
            this.lblEficaciaEPi = new System.Windows.Forms.TextBox();
            this.cbEficaciaEPI = new SWS.ComboBoxWithBorder();
            this.lblPergunta1 = new System.Windows.Forms.Label();
            this.lblPergunta2 = new System.Windows.Forms.Label();
            this.lblPergunta3 = new System.Windows.Forms.Label();
            this.lblPergunta4 = new System.Windows.Forms.Label();
            this.grbEpiEpc = new System.Windows.Forms.GroupBox();
            this.grbEpi = new System.Windows.Forms.GroupBox();
            this.dgvEpis = new System.Windows.Forms.DataGridView();
            this.flpAcaoEpi = new System.Windows.Forms.FlowLayoutPanel();
            this.btnIncluirEpi = new System.Windows.Forms.Button();
            this.btnExcluirEpi = new System.Windows.Forms.Button();
            this.gbrPerguntasGrupo2 = new System.Windows.Forms.GroupBox();
            this.lblGrupo2Pergunta6 = new System.Windows.Forms.Label();
            this.lblGrupo2Pergunta5 = new System.Windows.Forms.Label();
            this.lblGrupo2Pergunta4 = new System.Windows.Forms.Label();
            this.lblGrupo2Pergunta3 = new System.Windows.Forms.Label();
            this.lblGrupo2Pergunta2 = new System.Windows.Forms.Label();
            this.lblGrupo2Pergunta1 = new System.Windows.Forms.Label();
            this.cbHigienizacao = new SWS.ComboBoxWithBorder();
            this.cbPeriodicidadeTroca = new SWS.ComboBoxWithBorder();
            this.cbPrazoValidade = new SWS.ComboBoxWithBorder();
            this.cbUsoIniterrupto = new SWS.ComboBoxWithBorder();
            this.cbCondicaoFuncionamento = new SWS.ComboBoxWithBorder();
            this.cbMedidaProtecao = new SWS.ComboBoxWithBorder();
            this.lblHigienizacao = new System.Windows.Forms.TextBox();
            this.lblPeriodicidadeTroca = new System.Windows.Forms.TextBox();
            this.lblPrazoValidade = new System.Windows.Forms.TextBox();
            this.lblUsoInit = new System.Windows.Forms.TextBox();
            this.lblCondicaoFuncionamento = new System.Windows.Forms.TextBox();
            this.lblMedidaProtecao = new System.Windows.Forms.TextBox();
            this.lblTecnica = new System.Windows.Forms.TextBox();
            this.textNumeroProcesso = new System.Windows.Forms.TextBox();
            this.lblNumeroProcesso = new System.Windows.Forms.TextBox();
            ((System.ComponentModel.ISupportInitialize)(this.scForm)).BeginInit();
            this.scForm.Panel1.SuspendLayout();
            this.scForm.Panel2.SuspendLayout();
            this.scForm.SuspendLayout();
            this.pnlForm.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.banner)).BeginInit();
            this.flpAcao.SuspendLayout();
            this.grbEpiEpc.SuspendLayout();
            this.grbEpi.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvEpis)).BeginInit();
            this.flpAcaoEpi.SuspendLayout();
            this.gbrPerguntasGrupo2.SuspendLayout();
            this.SuspendLayout();
            // 
            // scForm
            // 
            // 
            // scForm.Panel1
            // 
            this.scForm.Panel1.Controls.Add(this.flpAcao);
            // 
            // pnlForm
            // 
            this.pnlForm.Controls.Add(this.textNumeroProcesso);
            this.pnlForm.Controls.Add(this.lblNumeroProcesso);
            this.pnlForm.Controls.Add(this.textTecnica);
            this.pnlForm.Controls.Add(this.lblTecnica);
            this.pnlForm.Controls.Add(this.gbrPerguntasGrupo2);
            this.pnlForm.Controls.Add(this.flpAcaoEpi);
            this.pnlForm.Controls.Add(this.grbEpi);
            this.pnlForm.Controls.Add(this.grbEpiEpc);
            this.pnlForm.Controls.Add(this.cbUnidadeMedida);
            this.pnlForm.Controls.Add(this.lblUnidade);
            this.pnlForm.Controls.Add(this.textLimiteTolerancia);
            this.pnlForm.Controls.Add(this.lblLimite);
            this.pnlForm.Controls.Add(this.textIntensidade);
            this.pnlForm.Controls.Add(this.lblIntensidade);
            this.pnlForm.Controls.Add(this.cbTipoAvaliacao);
            this.pnlForm.Controls.Add(this.lblTpAvaliacao);
            this.pnlForm.Controls.Add(this.TextRisco);
            this.pnlForm.Controls.Add(this.lblRisco);
            this.pnlForm.Controls.Add(this.textAgente);
            this.pnlForm.Controls.Add(this.lblAgente);
            this.pnlForm.Location = new System.Drawing.Point(0, -2);
            this.pnlForm.Size = new System.Drawing.Size(514, 1129);
            // 
            // flpAcao
            // 
            this.flpAcao.Controls.Add(this.btnIncluir);
            this.flpAcao.Controls.Add(this.lblFechar);
            this.flpAcao.Dock = System.Windows.Forms.DockStyle.Fill;
            this.flpAcao.Location = new System.Drawing.Point(0, 0);
            this.flpAcao.Name = "flpAcao";
            this.flpAcao.Size = new System.Drawing.Size(534, 30);
            this.flpAcao.TabIndex = 0;
            // 
            // btnIncluir
            // 
            this.btnIncluir.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnIncluir.Image = global::SWS.Properties.Resources.Gravar;
            this.btnIncluir.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnIncluir.Location = new System.Drawing.Point(3, 3);
            this.btnIncluir.Name = "btnIncluir";
            this.btnIncluir.Size = new System.Drawing.Size(75, 23);
            this.btnIncluir.TabIndex = 0;
            this.btnIncluir.Text = "Gravar";
            this.btnIncluir.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnIncluir.UseVisualStyleBackColor = true;
            this.btnIncluir.Click += new System.EventHandler(this.btnIncluir_Click);
            // 
            // lblFechar
            // 
            this.lblFechar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.lblFechar.Image = global::SWS.Properties.Resources.close;
            this.lblFechar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.lblFechar.Location = new System.Drawing.Point(84, 3);
            this.lblFechar.Name = "lblFechar";
            this.lblFechar.Size = new System.Drawing.Size(75, 23);
            this.lblFechar.TabIndex = 1;
            this.lblFechar.Text = "Fechar";
            this.lblFechar.UseVisualStyleBackColor = true;
            this.lblFechar.Click += new System.EventHandler(this.lblFechar_Click);
            // 
            // lblAgente
            // 
            this.lblAgente.BackColor = System.Drawing.Color.LightGray;
            this.lblAgente.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblAgente.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblAgente.Location = new System.Drawing.Point(3, 3);
            this.lblAgente.MaxLength = 15;
            this.lblAgente.Name = "lblAgente";
            this.lblAgente.ReadOnly = true;
            this.lblAgente.Size = new System.Drawing.Size(120, 21);
            this.lblAgente.TabIndex = 33;
            this.lblAgente.TabStop = false;
            this.lblAgente.Text = "Agente";
            // 
            // textAgente
            // 
            this.textAgente.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.textAgente.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.textAgente.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textAgente.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.textAgente.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textAgente.ForeColor = System.Drawing.Color.Blue;
            this.textAgente.Location = new System.Drawing.Point(122, 3);
            this.textAgente.MaxLength = 100;
            this.textAgente.Name = "textAgente";
            this.textAgente.ReadOnly = true;
            this.textAgente.Size = new System.Drawing.Size(386, 21);
            this.textAgente.TabIndex = 39;
            this.textAgente.TabStop = false;
            // 
            // lblRisco
            // 
            this.lblRisco.BackColor = System.Drawing.Color.LightGray;
            this.lblRisco.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblRisco.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblRisco.Location = new System.Drawing.Point(3, 23);
            this.lblRisco.MaxLength = 15;
            this.lblRisco.Name = "lblRisco";
            this.lblRisco.ReadOnly = true;
            this.lblRisco.Size = new System.Drawing.Size(120, 21);
            this.lblRisco.TabIndex = 40;
            this.lblRisco.TabStop = false;
            this.lblRisco.Text = "Risco";
            // 
            // TextRisco
            // 
            this.TextRisco.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.TextRisco.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.TextRisco.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.TextRisco.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.TextRisco.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TextRisco.ForeColor = System.Drawing.Color.Blue;
            this.TextRisco.Location = new System.Drawing.Point(122, 23);
            this.TextRisco.MaxLength = 100;
            this.TextRisco.Name = "TextRisco";
            this.TextRisco.ReadOnly = true;
            this.TextRisco.Size = new System.Drawing.Size(386, 21);
            this.TextRisco.TabIndex = 41;
            this.TextRisco.TabStop = false;
            // 
            // lblTpAvaliacao
            // 
            this.lblTpAvaliacao.BackColor = System.Drawing.Color.LightGray;
            this.lblTpAvaliacao.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblTpAvaliacao.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTpAvaliacao.Location = new System.Drawing.Point(3, 43);
            this.lblTpAvaliacao.MaxLength = 15;
            this.lblTpAvaliacao.Name = "lblTpAvaliacao";
            this.lblTpAvaliacao.ReadOnly = true;
            this.lblTpAvaliacao.Size = new System.Drawing.Size(120, 21);
            this.lblTpAvaliacao.TabIndex = 42;
            this.lblTpAvaliacao.TabStop = false;
            this.lblTpAvaliacao.Text = "Tipo de Avaliação";
            // 
            // cbTipoAvaliacao
            // 
            this.cbTipoAvaliacao.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.cbTipoAvaliacao.BackColor = System.Drawing.Color.LightGray;
            this.cbTipoAvaliacao.BorderColor = System.Drawing.Color.DimGray;
            this.cbTipoAvaliacao.BorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid;
            this.cbTipoAvaliacao.FormattingEnabled = true;
            this.cbTipoAvaliacao.Location = new System.Drawing.Point(122, 43);
            this.cbTipoAvaliacao.Name = "cbTipoAvaliacao";
            this.cbTipoAvaliacao.Size = new System.Drawing.Size(386, 21);
            this.cbTipoAvaliacao.TabIndex = 75;
            this.cbTipoAvaliacao.SelectedIndexChanged += new System.EventHandler(this.cbTipoAvaliacao_SelectedIndexChanged);
            // 
            // lblIntensidade
            // 
            this.lblIntensidade.BackColor = System.Drawing.Color.LightGray;
            this.lblIntensidade.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblIntensidade.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblIntensidade.Location = new System.Drawing.Point(3, 63);
            this.lblIntensidade.MaxLength = 15;
            this.lblIntensidade.Name = "lblIntensidade";
            this.lblIntensidade.ReadOnly = true;
            this.lblIntensidade.Size = new System.Drawing.Size(120, 21);
            this.lblIntensidade.TabIndex = 76;
            this.lblIntensidade.TabStop = false;
            this.lblIntensidade.Text = "Intensidade";
            // 
            // textIntensidade
            // 
            this.textIntensidade.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.textIntensidade.BackColor = System.Drawing.Color.White;
            this.textIntensidade.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textIntensidade.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.textIntensidade.Enabled = false;
            this.textIntensidade.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textIntensidade.ForeColor = System.Drawing.Color.Blue;
            this.textIntensidade.Location = new System.Drawing.Point(122, 63);
            this.textIntensidade.MaxLength = 20;
            this.textIntensidade.Name = "textIntensidade";
            this.textIntensidade.Size = new System.Drawing.Size(386, 21);
            this.textIntensidade.TabIndex = 77;
            this.textIntensidade.TabStop = false;
            this.textIntensidade.Enter += new System.EventHandler(this.textIntensidade_Enter);
            this.textIntensidade.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.textIntensidade_KeyPress);
            this.textIntensidade.Leave += new System.EventHandler(this.textIntensidade_Leave);
            // 
            // lblLimite
            // 
            this.lblLimite.BackColor = System.Drawing.Color.LightGray;
            this.lblLimite.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblLimite.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblLimite.Location = new System.Drawing.Point(3, 83);
            this.lblLimite.MaxLength = 15;
            this.lblLimite.Name = "lblLimite";
            this.lblLimite.ReadOnly = true;
            this.lblLimite.Size = new System.Drawing.Size(120, 21);
            this.lblLimite.TabIndex = 78;
            this.lblLimite.TabStop = false;
            this.lblLimite.Text = "Limite Tolerância";
            // 
            // textLimiteTolerancia
            // 
            this.textLimiteTolerancia.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.textLimiteTolerancia.BackColor = System.Drawing.Color.White;
            this.textLimiteTolerancia.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textLimiteTolerancia.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.textLimiteTolerancia.Enabled = false;
            this.textLimiteTolerancia.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textLimiteTolerancia.ForeColor = System.Drawing.Color.Blue;
            this.textLimiteTolerancia.Location = new System.Drawing.Point(122, 83);
            this.textLimiteTolerancia.MaxLength = 20;
            this.textLimiteTolerancia.Name = "textLimiteTolerancia";
            this.textLimiteTolerancia.Size = new System.Drawing.Size(386, 21);
            this.textLimiteTolerancia.TabIndex = 79;
            this.textLimiteTolerancia.TabStop = false;
            this.textLimiteTolerancia.Enter += new System.EventHandler(this.textLimiteTolerancia_Enter);
            this.textLimiteTolerancia.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.textLimiteTolerancia_KeyPress);
            this.textLimiteTolerancia.Leave += new System.EventHandler(this.textLimiteTolerancia_Leave);
            // 
            // lblUnidade
            // 
            this.lblUnidade.BackColor = System.Drawing.Color.LightGray;
            this.lblUnidade.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblUnidade.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblUnidade.Location = new System.Drawing.Point(3, 103);
            this.lblUnidade.MaxLength = 15;
            this.lblUnidade.Name = "lblUnidade";
            this.lblUnidade.ReadOnly = true;
            this.lblUnidade.Size = new System.Drawing.Size(120, 21);
            this.lblUnidade.TabIndex = 80;
            this.lblUnidade.TabStop = false;
            this.lblUnidade.Text = "Unidade de Medida";
            // 
            // cbUnidadeMedida
            // 
            this.cbUnidadeMedida.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.cbUnidadeMedida.BackColor = System.Drawing.Color.LightGray;
            this.cbUnidadeMedida.BorderColor = System.Drawing.Color.DimGray;
            this.cbUnidadeMedida.BorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid;
            this.cbUnidadeMedida.Enabled = false;
            this.cbUnidadeMedida.FormattingEnabled = true;
            this.cbUnidadeMedida.Location = new System.Drawing.Point(122, 103);
            this.cbUnidadeMedida.Name = "cbUnidadeMedida";
            this.cbUnidadeMedida.Size = new System.Drawing.Size(386, 21);
            this.cbUnidadeMedida.TabIndex = 81;
            // 
            // textTecnica
            // 
            this.textTecnica.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textTecnica.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.textTecnica.Enabled = false;
            this.textTecnica.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textTecnica.Location = new System.Drawing.Point(122, 123);
            this.textTecnica.MaxLength = 40;
            this.textTecnica.Name = "textTecnica";
            this.textTecnica.Size = new System.Drawing.Size(386, 21);
            this.textTecnica.TabIndex = 0;
            // 
            // lblUtilizaEpc
            // 
            this.lblUtilizaEpc.BackColor = System.Drawing.Color.LightGray;
            this.lblUtilizaEpc.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblUtilizaEpc.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblUtilizaEpc.Location = new System.Drawing.Point(11, 84);
            this.lblUtilizaEpc.MaxLength = 15;
            this.lblUtilizaEpc.Name = "lblUtilizaEpc";
            this.lblUtilizaEpc.ReadOnly = true;
            this.lblUtilizaEpc.Size = new System.Drawing.Size(120, 21);
            this.lblUtilizaEpc.TabIndex = 83;
            this.lblUtilizaEpc.TabStop = false;
            this.lblUtilizaEpc.Text = "Resposta:";
            // 
            // cbUtilizaEPC
            // 
            this.cbUtilizaEPC.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.cbUtilizaEPC.BackColor = System.Drawing.Color.LightGray;
            this.cbUtilizaEPC.BorderColor = System.Drawing.Color.DimGray;
            this.cbUtilizaEPC.BorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid;
            this.cbUtilizaEPC.FormattingEnabled = true;
            this.cbUtilizaEPC.Location = new System.Drawing.Point(130, 84);
            this.cbUtilizaEPC.Name = "cbUtilizaEPC";
            this.cbUtilizaEPC.Size = new System.Drawing.Size(189, 21);
            this.cbUtilizaEPC.TabIndex = 84;
            this.cbUtilizaEPC.SelectedIndexChanged += new System.EventHandler(this.cbUtilizaEPC_SelectedIndexChanged);
            // 
            // lblEficaciaEpc
            // 
            this.lblEficaciaEpc.BackColor = System.Drawing.Color.LightGray;
            this.lblEficaciaEpc.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblEficaciaEpc.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblEficaciaEpc.Location = new System.Drawing.Point(11, 126);
            this.lblEficaciaEpc.MaxLength = 15;
            this.lblEficaciaEpc.Name = "lblEficaciaEpc";
            this.lblEficaciaEpc.ReadOnly = true;
            this.lblEficaciaEpc.Size = new System.Drawing.Size(120, 21);
            this.lblEficaciaEpc.TabIndex = 85;
            this.lblEficaciaEpc.TabStop = false;
            this.lblEficaciaEpc.Text = "Resposta:";
            // 
            // cbEficaciaEpc
            // 
            this.cbEficaciaEpc.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.cbEficaciaEpc.BackColor = System.Drawing.Color.LightGray;
            this.cbEficaciaEpc.BorderColor = System.Drawing.Color.DimGray;
            this.cbEficaciaEpc.BorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid;
            this.cbEficaciaEpc.Enabled = false;
            this.cbEficaciaEpc.FormattingEnabled = true;
            this.cbEficaciaEpc.Location = new System.Drawing.Point(130, 126);
            this.cbEficaciaEpc.Name = "cbEficaciaEpc";
            this.cbEficaciaEpc.Size = new System.Drawing.Size(189, 21);
            this.cbEficaciaEpc.TabIndex = 86;
            // 
            // lblUtilizaEPI
            // 
            this.lblUtilizaEPI.BackColor = System.Drawing.Color.LightGray;
            this.lblUtilizaEPI.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblUtilizaEPI.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblUtilizaEPI.Location = new System.Drawing.Point(11, 168);
            this.lblUtilizaEPI.MaxLength = 15;
            this.lblUtilizaEPI.Name = "lblUtilizaEPI";
            this.lblUtilizaEPI.ReadOnly = true;
            this.lblUtilizaEPI.Size = new System.Drawing.Size(120, 21);
            this.lblUtilizaEPI.TabIndex = 87;
            this.lblUtilizaEPI.TabStop = false;
            this.lblUtilizaEPI.Text = "Resposta:";
            // 
            // cbUtilizaEPI
            // 
            this.cbUtilizaEPI.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.cbUtilizaEPI.BackColor = System.Drawing.Color.LightGray;
            this.cbUtilizaEPI.BorderColor = System.Drawing.Color.DimGray;
            this.cbUtilizaEPI.BorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid;
            this.cbUtilizaEPI.FormattingEnabled = true;
            this.cbUtilizaEPI.Location = new System.Drawing.Point(130, 168);
            this.cbUtilizaEPI.Name = "cbUtilizaEPI";
            this.cbUtilizaEPI.Size = new System.Drawing.Size(189, 21);
            this.cbUtilizaEPI.TabIndex = 88;
            this.cbUtilizaEPI.SelectedIndexChanged += new System.EventHandler(this.cbUtilizaEPI_SelectedIndexChanged);
            // 
            // lblEficaciaEPi
            // 
            this.lblEficaciaEPi.BackColor = System.Drawing.Color.LightGray;
            this.lblEficaciaEPi.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblEficaciaEPi.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblEficaciaEPi.Location = new System.Drawing.Point(11, 210);
            this.lblEficaciaEPi.MaxLength = 15;
            this.lblEficaciaEPi.Name = "lblEficaciaEPi";
            this.lblEficaciaEPi.ReadOnly = true;
            this.lblEficaciaEPi.Size = new System.Drawing.Size(120, 21);
            this.lblEficaciaEPi.TabIndex = 89;
            this.lblEficaciaEPi.TabStop = false;
            this.lblEficaciaEPi.Text = "Resposta:";
            // 
            // cbEficaciaEPI
            // 
            this.cbEficaciaEPI.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.cbEficaciaEPI.BackColor = System.Drawing.Color.LightGray;
            this.cbEficaciaEPI.BorderColor = System.Drawing.Color.DimGray;
            this.cbEficaciaEPI.BorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid;
            this.cbEficaciaEPI.Enabled = false;
            this.cbEficaciaEPI.FormattingEnabled = true;
            this.cbEficaciaEPI.Location = new System.Drawing.Point(130, 210);
            this.cbEficaciaEPI.Name = "cbEficaciaEPI";
            this.cbEficaciaEPI.Size = new System.Drawing.Size(189, 21);
            this.cbEficaciaEPI.TabIndex = 90;
            // 
            // lblPergunta1
            // 
            this.lblPergunta1.AutoSize = true;
            this.lblPergunta1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPergunta1.ForeColor = System.Drawing.Color.Blue;
            this.lblPergunta1.Location = new System.Drawing.Point(8, 50);
            this.lblPergunta1.Name = "lblPergunta1";
            this.lblPergunta1.Size = new System.Drawing.Size(472, 30);
            this.lblPergunta1.TabIndex = 91;
            this.lblPergunta1.Text = "(1) - O empregador implementa medidas de proteção coletiva (EPC) para eliminar ou" +
    "\r\nreduzir a exposição dos trabalhadores ao agente nocivo?";
            // 
            // lblPergunta2
            // 
            this.lblPergunta2.AutoSize = true;
            this.lblPergunta2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPergunta2.ForeColor = System.Drawing.Color.Blue;
            this.lblPergunta2.Location = new System.Drawing.Point(8, 108);
            this.lblPergunta2.Name = "lblPergunta2";
            this.lblPergunta2.Size = new System.Drawing.Size(381, 15);
            this.lblPergunta2.TabIndex = 92;
            this.lblPergunta2.Text = "(2) - Os EPCs são eficazes na neutralização do risco ao trabalhador?";
            // 
            // lblPergunta3
            // 
            this.lblPergunta3.AutoSize = true;
            this.lblPergunta3.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPergunta3.ForeColor = System.Drawing.Color.Blue;
            this.lblPergunta3.Location = new System.Drawing.Point(8, 150);
            this.lblPergunta3.Name = "lblPergunta3";
            this.lblPergunta3.Size = new System.Drawing.Size(138, 15);
            this.lblPergunta3.TabIndex = 93;
            this.lblPergunta3.Text = "(3) - Utilização de EPIs?";
            // 
            // lblPergunta4
            // 
            this.lblPergunta4.AutoSize = true;
            this.lblPergunta4.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPergunta4.ForeColor = System.Drawing.Color.Blue;
            this.lblPergunta4.Location = new System.Drawing.Point(8, 176);
            this.lblPergunta4.Name = "lblPergunta4";
            this.lblPergunta4.Size = new System.Drawing.Size(376, 15);
            this.lblPergunta4.TabIndex = 94;
            this.lblPergunta4.Text = "(4) - Os EPIs são eficazes na neutralização do risco ao trabalhador?";
            // 
            // grbEpiEpc
            // 
            this.grbEpiEpc.Controls.Add(this.lblPergunta1);
            this.grbEpiEpc.Controls.Add(this.lblPergunta4);
            this.grbEpiEpc.Controls.Add(this.lblUtilizaEpc);
            this.grbEpiEpc.Controls.Add(this.cbEficaciaEPI);
            this.grbEpiEpc.Controls.Add(this.lblEficaciaEPi);
            this.grbEpiEpc.Controls.Add(this.lblPergunta3);
            this.grbEpiEpc.Controls.Add(this.cbUtilizaEPC);
            this.grbEpiEpc.Controls.Add(this.lblPergunta2);
            this.grbEpiEpc.Controls.Add(this.lblEficaciaEpc);
            this.grbEpiEpc.Controls.Add(this.cbUtilizaEPI);
            this.grbEpiEpc.Controls.Add(this.cbEficaciaEpc);
            this.grbEpiEpc.Controls.Add(this.lblUtilizaEPI);
            this.grbEpiEpc.ForeColor = System.Drawing.Color.Black;
            this.grbEpiEpc.Location = new System.Drawing.Point(4, 175);
            this.grbEpiEpc.Name = "grbEpiEpc";
            this.grbEpiEpc.Size = new System.Drawing.Size(507, 226);
            this.grbEpiEpc.TabIndex = 95;
            this.grbEpiEpc.TabStop = false;
            this.grbEpiEpc.Text = "Informações relativas a Equipamentos de Proteção Coletiva(EPC) e Equipamentos de " +
    "Proteção Individual (EPI).";
            // 
            // grbEpi
            // 
            this.grbEpi.Controls.Add(this.dgvEpis);
            this.grbEpi.Location = new System.Drawing.Point(4, 408);
            this.grbEpi.Name = "grbEpi";
            this.grbEpi.Size = new System.Drawing.Size(507, 154);
            this.grbEpi.TabIndex = 96;
            this.grbEpi.TabStop = false;
            this.grbEpi.Text = "Gerenciar Epis no Agente de Risco";
            // 
            // dgvEpis
            // 
            this.dgvEpis.AllowUserToAddRows = false;
            this.dgvEpis.AllowUserToDeleteRows = false;
            this.dgvEpis.AllowUserToResizeRows = false;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.Gainsboro;
            this.dgvEpis.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle1;
            this.dgvEpis.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this.dgvEpis.BackgroundColor = System.Drawing.Color.White;
            this.dgvEpis.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.Color.LightGray;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvEpis.DefaultCellStyle = dataGridViewCellStyle2;
            this.dgvEpis.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvEpis.Location = new System.Drawing.Point(3, 16);
            this.dgvEpis.Name = "dgvEpis";
            this.dgvEpis.Size = new System.Drawing.Size(501, 135);
            this.dgvEpis.TabIndex = 0;
            // 
            // flpAcaoEpi
            // 
            this.flpAcaoEpi.Controls.Add(this.btnIncluirEpi);
            this.flpAcaoEpi.Controls.Add(this.btnExcluirEpi);
            this.flpAcaoEpi.Location = new System.Drawing.Point(4, 566);
            this.flpAcaoEpi.Name = "flpAcaoEpi";
            this.flpAcaoEpi.Size = new System.Drawing.Size(504, 30);
            this.flpAcaoEpi.TabIndex = 97;
            // 
            // btnIncluirEpi
            // 
            this.btnIncluirEpi.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnIncluirEpi.Image = global::SWS.Properties.Resources.incluir;
            this.btnIncluirEpi.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnIncluirEpi.Location = new System.Drawing.Point(3, 3);
            this.btnIncluirEpi.Name = "btnIncluirEpi";
            this.btnIncluirEpi.Size = new System.Drawing.Size(75, 23);
            this.btnIncluirEpi.TabIndex = 0;
            this.btnIncluirEpi.Text = "Incluir";
            this.btnIncluirEpi.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnIncluirEpi.UseVisualStyleBackColor = true;
            this.btnIncluirEpi.Click += new System.EventHandler(this.btnIncluirEpi_Click);
            // 
            // btnExcluirEpi
            // 
            this.btnExcluirEpi.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnExcluirEpi.Image = global::SWS.Properties.Resources.lixeira;
            this.btnExcluirEpi.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnExcluirEpi.Location = new System.Drawing.Point(84, 3);
            this.btnExcluirEpi.Name = "btnExcluirEpi";
            this.btnExcluirEpi.Size = new System.Drawing.Size(75, 23);
            this.btnExcluirEpi.TabIndex = 1;
            this.btnExcluirEpi.Text = "Excluir";
            this.btnExcluirEpi.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnExcluirEpi.UseVisualStyleBackColor = true;
            this.btnExcluirEpi.Click += new System.EventHandler(this.btnExcluirEpi_Click);
            // 
            // gbrPerguntasGrupo2
            // 
            this.gbrPerguntasGrupo2.Controls.Add(this.lblGrupo2Pergunta6);
            this.gbrPerguntasGrupo2.Controls.Add(this.lblGrupo2Pergunta5);
            this.gbrPerguntasGrupo2.Controls.Add(this.lblGrupo2Pergunta4);
            this.gbrPerguntasGrupo2.Controls.Add(this.lblGrupo2Pergunta3);
            this.gbrPerguntasGrupo2.Controls.Add(this.lblGrupo2Pergunta2);
            this.gbrPerguntasGrupo2.Controls.Add(this.lblGrupo2Pergunta1);
            this.gbrPerguntasGrupo2.Controls.Add(this.cbHigienizacao);
            this.gbrPerguntasGrupo2.Controls.Add(this.cbPeriodicidadeTroca);
            this.gbrPerguntasGrupo2.Controls.Add(this.cbPrazoValidade);
            this.gbrPerguntasGrupo2.Controls.Add(this.cbUsoIniterrupto);
            this.gbrPerguntasGrupo2.Controls.Add(this.cbCondicaoFuncionamento);
            this.gbrPerguntasGrupo2.Controls.Add(this.cbMedidaProtecao);
            this.gbrPerguntasGrupo2.Controls.Add(this.lblHigienizacao);
            this.gbrPerguntasGrupo2.Controls.Add(this.lblPeriodicidadeTroca);
            this.gbrPerguntasGrupo2.Controls.Add(this.lblPrazoValidade);
            this.gbrPerguntasGrupo2.Controls.Add(this.lblUsoInit);
            this.gbrPerguntasGrupo2.Controls.Add(this.lblCondicaoFuncionamento);
            this.gbrPerguntasGrupo2.Controls.Add(this.lblMedidaProtecao);
            this.gbrPerguntasGrupo2.Location = new System.Drawing.Point(4, 606);
            this.gbrPerguntasGrupo2.Name = "gbrPerguntasGrupo2";
            this.gbrPerguntasGrupo2.Size = new System.Drawing.Size(504, 500);
            this.gbrPerguntasGrupo2.TabIndex = 98;
            this.gbrPerguntasGrupo2.TabStop = false;
            this.gbrPerguntasGrupo2.Text = "Requisitos da Norma Regulamentadora 06 - NR-06 e da Norma Regulamentadora 09 - NR" +
    "-09 pelo(s) EPI(s) informado(s).";
            // 
            // lblGrupo2Pergunta6
            // 
            this.lblGrupo2Pergunta6.AutoSize = true;
            this.lblGrupo2Pergunta6.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblGrupo2Pergunta6.ForeColor = System.Drawing.Color.Blue;
            this.lblGrupo2Pergunta6.Location = new System.Drawing.Point(8, 404);
            this.lblGrupo2Pergunta6.Name = "lblGrupo2Pergunta6";
            this.lblGrupo2Pergunta6.Size = new System.Drawing.Size(423, 30);
            this.lblGrupo2Pergunta6.TabIndex = 111;
            this.lblGrupo2Pergunta6.Text = "(6) - É observada a higienização conforme orientação do fabricante nacional\r\nou i" +
    "mportador?";
            // 
            // lblGrupo2Pergunta5
            // 
            this.lblGrupo2Pergunta5.AutoSize = true;
            this.lblGrupo2Pergunta5.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblGrupo2Pergunta5.ForeColor = System.Drawing.Color.Blue;
            this.lblGrupo2Pergunta5.Location = new System.Drawing.Point(8, 333);
            this.lblGrupo2Pergunta5.Name = "lblGrupo2Pergunta5";
            this.lblGrupo2Pergunta5.Size = new System.Drawing.Size(496, 45);
            this.lblGrupo2Pergunta5.TabIndex = 110;
            this.lblGrupo2Pergunta5.Text = "(5) - É observada a periodicidade de troca definida pelo fabricante nacional ou i" +
    "mportador\r\ne/ou programas ambientais, comprovada mediante recibo  assinado pelo " +
    "usuário\r\nem época própria?";
            // 
            // lblGrupo2Pergunta4
            // 
            this.lblGrupo2Pergunta4.AutoSize = true;
            this.lblGrupo2Pergunta4.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblGrupo2Pergunta4.ForeColor = System.Drawing.Color.Blue;
            this.lblGrupo2Pergunta4.Location = new System.Drawing.Point(11, 283);
            this.lblGrupo2Pergunta4.Name = "lblGrupo2Pergunta4";
            this.lblGrupo2Pergunta4.Size = new System.Drawing.Size(435, 15);
            this.lblGrupo2Pergunta4.TabIndex = 109;
            this.lblGrupo2Pergunta4.Text = "(4) - Foi observado o prazo de validade do CA no momento da compra do EPI?";
            // 
            // lblGrupo2Pergunta3
            // 
            this.lblGrupo2Pergunta3.AutoSize = true;
            this.lblGrupo2Pergunta3.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblGrupo2Pergunta3.ForeColor = System.Drawing.Color.Blue;
            this.lblGrupo2Pergunta3.Location = new System.Drawing.Point(8, 211);
            this.lblGrupo2Pergunta3.Name = "lblGrupo2Pergunta3";
            this.lblGrupo2Pergunta3.Size = new System.Drawing.Size(413, 45);
            this.lblGrupo2Pergunta3.TabIndex = 108;
            this.lblGrupo2Pergunta3.Text = "(3) - Foi observado o uso ininterrupto do EPI ao longo do tempo, conforme \r\nespec" +
    "ificação técnica do fabricante nacional ou importador, ajustadas às \r\ncondições " +
    "de campo?";
            // 
            // lblGrupo2Pergunta2
            // 
            this.lblGrupo2Pergunta2.AutoSize = true;
            this.lblGrupo2Pergunta2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblGrupo2Pergunta2.ForeColor = System.Drawing.Color.Blue;
            this.lblGrupo2Pergunta2.Location = new System.Drawing.Point(8, 139);
            this.lblGrupo2Pergunta2.Name = "lblGrupo2Pergunta2";
            this.lblGrupo2Pergunta2.Size = new System.Drawing.Size(486, 45);
            this.lblGrupo2Pergunta2.TabIndex = 107;
            this.lblGrupo2Pergunta2.Text = "(2) - Foram observadas as condições de funcionamento do EPI ao longo do \r\ntempo, " +
    "conforme  especificação técnica do fabricante nacional ou importador, ajustadas\r" +
    "\nàs condições de campo?";
            // 
            // lblGrupo2Pergunta1
            // 
            this.lblGrupo2Pergunta1.AutoSize = true;
            this.lblGrupo2Pergunta1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblGrupo2Pergunta1.ForeColor = System.Drawing.Color.Blue;
            this.lblGrupo2Pergunta1.Location = new System.Drawing.Point(8, 51);
            this.lblGrupo2Pergunta1.Name = "lblGrupo2Pergunta1";
            this.lblGrupo2Pergunta1.Size = new System.Drawing.Size(433, 60);
            this.lblGrupo2Pergunta1.TabIndex = 106;
            this.lblGrupo2Pergunta1.Text = resources.GetString("lblGrupo2Pergunta1.Text");
            // 
            // cbHigienizacao
            // 
            this.cbHigienizacao.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.cbHigienizacao.BackColor = System.Drawing.Color.LightGray;
            this.cbHigienizacao.BorderColor = System.Drawing.Color.DimGray;
            this.cbHigienizacao.BorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid;
            this.cbHigienizacao.FormattingEnabled = true;
            this.cbHigienizacao.Location = new System.Drawing.Point(139, 437);
            this.cbHigienizacao.Name = "cbHigienizacao";
            this.cbHigienizacao.Size = new System.Drawing.Size(180, 21);
            this.cbHigienizacao.TabIndex = 105;
            // 
            // cbPeriodicidadeTroca
            // 
            this.cbPeriodicidadeTroca.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.cbPeriodicidadeTroca.BackColor = System.Drawing.Color.LightGray;
            this.cbPeriodicidadeTroca.BorderColor = System.Drawing.Color.DimGray;
            this.cbPeriodicidadeTroca.BorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid;
            this.cbPeriodicidadeTroca.FormattingEnabled = true;
            this.cbPeriodicidadeTroca.Location = new System.Drawing.Point(139, 381);
            this.cbPeriodicidadeTroca.Name = "cbPeriodicidadeTroca";
            this.cbPeriodicidadeTroca.Size = new System.Drawing.Size(180, 21);
            this.cbPeriodicidadeTroca.TabIndex = 104;
            // 
            // cbPrazoValidade
            // 
            this.cbPrazoValidade.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.cbPrazoValidade.BackColor = System.Drawing.Color.LightGray;
            this.cbPrazoValidade.BorderColor = System.Drawing.Color.DimGray;
            this.cbPrazoValidade.BorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid;
            this.cbPrazoValidade.FormattingEnabled = true;
            this.cbPrazoValidade.Location = new System.Drawing.Point(139, 309);
            this.cbPrazoValidade.Name = "cbPrazoValidade";
            this.cbPrazoValidade.Size = new System.Drawing.Size(180, 21);
            this.cbPrazoValidade.TabIndex = 103;
            // 
            // cbUsoIniterrupto
            // 
            this.cbUsoIniterrupto.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.cbUsoIniterrupto.BackColor = System.Drawing.Color.LightGray;
            this.cbUsoIniterrupto.BorderColor = System.Drawing.Color.DimGray;
            this.cbUsoIniterrupto.BorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid;
            this.cbUsoIniterrupto.FormattingEnabled = true;
            this.cbUsoIniterrupto.Location = new System.Drawing.Point(140, 259);
            this.cbUsoIniterrupto.Name = "cbUsoIniterrupto";
            this.cbUsoIniterrupto.Size = new System.Drawing.Size(179, 21);
            this.cbUsoIniterrupto.TabIndex = 102;
            // 
            // cbCondicaoFuncionamento
            // 
            this.cbCondicaoFuncionamento.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.cbCondicaoFuncionamento.BackColor = System.Drawing.Color.LightGray;
            this.cbCondicaoFuncionamento.BorderColor = System.Drawing.Color.DimGray;
            this.cbCondicaoFuncionamento.BorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid;
            this.cbCondicaoFuncionamento.FormattingEnabled = true;
            this.cbCondicaoFuncionamento.Location = new System.Drawing.Point(140, 187);
            this.cbCondicaoFuncionamento.Name = "cbCondicaoFuncionamento";
            this.cbCondicaoFuncionamento.Size = new System.Drawing.Size(179, 21);
            this.cbCondicaoFuncionamento.TabIndex = 101;
            // 
            // cbMedidaProtecao
            // 
            this.cbMedidaProtecao.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.cbMedidaProtecao.BackColor = System.Drawing.Color.LightGray;
            this.cbMedidaProtecao.BorderColor = System.Drawing.Color.DimGray;
            this.cbMedidaProtecao.BorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid;
            this.cbMedidaProtecao.FormattingEnabled = true;
            this.cbMedidaProtecao.Location = new System.Drawing.Point(140, 115);
            this.cbMedidaProtecao.Name = "cbMedidaProtecao";
            this.cbMedidaProtecao.Size = new System.Drawing.Size(179, 21);
            this.cbMedidaProtecao.TabIndex = 100;
            // 
            // lblHigienizacao
            // 
            this.lblHigienizacao.BackColor = System.Drawing.Color.LightGray;
            this.lblHigienizacao.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblHigienizacao.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblHigienizacao.Location = new System.Drawing.Point(11, 437);
            this.lblHigienizacao.MaxLength = 15;
            this.lblHigienizacao.Name = "lblHigienizacao";
            this.lblHigienizacao.ReadOnly = true;
            this.lblHigienizacao.Size = new System.Drawing.Size(130, 21);
            this.lblHigienizacao.TabIndex = 99;
            this.lblHigienizacao.TabStop = false;
            this.lblHigienizacao.Text = "Resposta:";
            // 
            // lblPeriodicidadeTroca
            // 
            this.lblPeriodicidadeTroca.BackColor = System.Drawing.Color.LightGray;
            this.lblPeriodicidadeTroca.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblPeriodicidadeTroca.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPeriodicidadeTroca.Location = new System.Drawing.Point(11, 381);
            this.lblPeriodicidadeTroca.MaxLength = 15;
            this.lblPeriodicidadeTroca.Name = "lblPeriodicidadeTroca";
            this.lblPeriodicidadeTroca.ReadOnly = true;
            this.lblPeriodicidadeTroca.Size = new System.Drawing.Size(130, 21);
            this.lblPeriodicidadeTroca.TabIndex = 98;
            this.lblPeriodicidadeTroca.TabStop = false;
            this.lblPeriodicidadeTroca.Text = "Resposta:";
            // 
            // lblPrazoValidade
            // 
            this.lblPrazoValidade.BackColor = System.Drawing.Color.LightGray;
            this.lblPrazoValidade.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblPrazoValidade.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPrazoValidade.Location = new System.Drawing.Point(11, 309);
            this.lblPrazoValidade.MaxLength = 15;
            this.lblPrazoValidade.Name = "lblPrazoValidade";
            this.lblPrazoValidade.ReadOnly = true;
            this.lblPrazoValidade.Size = new System.Drawing.Size(130, 21);
            this.lblPrazoValidade.TabIndex = 97;
            this.lblPrazoValidade.TabStop = false;
            this.lblPrazoValidade.Text = "Resposta:";
            // 
            // lblUsoInit
            // 
            this.lblUsoInit.BackColor = System.Drawing.Color.LightGray;
            this.lblUsoInit.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblUsoInit.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblUsoInit.Location = new System.Drawing.Point(11, 259);
            this.lblUsoInit.MaxLength = 15;
            this.lblUsoInit.Name = "lblUsoInit";
            this.lblUsoInit.ReadOnly = true;
            this.lblUsoInit.Size = new System.Drawing.Size(130, 21);
            this.lblUsoInit.TabIndex = 96;
            this.lblUsoInit.TabStop = false;
            this.lblUsoInit.Text = "Resposta:";
            // 
            // lblCondicaoFuncionamento
            // 
            this.lblCondicaoFuncionamento.BackColor = System.Drawing.Color.LightGray;
            this.lblCondicaoFuncionamento.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblCondicaoFuncionamento.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCondicaoFuncionamento.Location = new System.Drawing.Point(11, 187);
            this.lblCondicaoFuncionamento.MaxLength = 15;
            this.lblCondicaoFuncionamento.Name = "lblCondicaoFuncionamento";
            this.lblCondicaoFuncionamento.ReadOnly = true;
            this.lblCondicaoFuncionamento.Size = new System.Drawing.Size(130, 21);
            this.lblCondicaoFuncionamento.TabIndex = 95;
            this.lblCondicaoFuncionamento.TabStop = false;
            this.lblCondicaoFuncionamento.Text = "Resposta:";
            // 
            // lblMedidaProtecao
            // 
            this.lblMedidaProtecao.BackColor = System.Drawing.Color.LightGray;
            this.lblMedidaProtecao.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblMedidaProtecao.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblMedidaProtecao.Location = new System.Drawing.Point(11, 115);
            this.lblMedidaProtecao.MaxLength = 15;
            this.lblMedidaProtecao.Name = "lblMedidaProtecao";
            this.lblMedidaProtecao.ReadOnly = true;
            this.lblMedidaProtecao.Size = new System.Drawing.Size(130, 21);
            this.lblMedidaProtecao.TabIndex = 94;
            this.lblMedidaProtecao.TabStop = false;
            this.lblMedidaProtecao.Text = "Resposta:";
            // 
            // lblTecnica
            // 
            this.lblTecnica.BackColor = System.Drawing.Color.LightGray;
            this.lblTecnica.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblTecnica.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTecnica.Location = new System.Drawing.Point(3, 123);
            this.lblTecnica.MaxLength = 15;
            this.lblTecnica.Name = "lblTecnica";
            this.lblTecnica.ReadOnly = true;
            this.lblTecnica.Size = new System.Drawing.Size(120, 21);
            this.lblTecnica.TabIndex = 99;
            this.lblTecnica.TabStop = false;
            this.lblTecnica.Text = "Técnica de Medição";
            // 
            // textNumeroProcesso
            // 
            this.textNumeroProcesso.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textNumeroProcesso.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.textNumeroProcesso.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textNumeroProcesso.ForeColor = System.Drawing.SystemColors.MenuHighlight;
            this.textNumeroProcesso.Location = new System.Drawing.Point(122, 143);
            this.textNumeroProcesso.MaxLength = 25;
            this.textNumeroProcesso.Name = "textNumeroProcesso";
            this.textNumeroProcesso.Size = new System.Drawing.Size(386, 21);
            this.textNumeroProcesso.TabIndex = 41;
            // 
            // lblNumeroProcesso
            // 
            this.lblNumeroProcesso.BackColor = System.Drawing.Color.LightGray;
            this.lblNumeroProcesso.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblNumeroProcesso.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNumeroProcesso.Location = new System.Drawing.Point(3, 143);
            this.lblNumeroProcesso.MaxLength = 15;
            this.lblNumeroProcesso.Name = "lblNumeroProcesso";
            this.lblNumeroProcesso.ReadOnly = true;
            this.lblNumeroProcesso.Size = new System.Drawing.Size(120, 21);
            this.lblNumeroProcesso.TabIndex = 101;
            this.lblNumeroProcesso.TabStop = false;
            this.lblNumeroProcesso.Text = "Número do Processo";
            // 
            // frmMonitoramentoAgente
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(534, 361);
            this.Name = "frmMonitoramentoAgente";
            this.Text = "MONITORAMENTO AGENTE";
            this.scForm.Panel1.ResumeLayout(false);
            this.scForm.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.scForm)).EndInit();
            this.scForm.ResumeLayout(false);
            this.pnlForm.ResumeLayout(false);
            this.pnlForm.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.banner)).EndInit();
            this.flpAcao.ResumeLayout(false);
            this.grbEpiEpc.ResumeLayout(false);
            this.grbEpiEpc.PerformLayout();
            this.grbEpi.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvEpis)).EndInit();
            this.flpAcaoEpi.ResumeLayout(false);
            this.gbrPerguntasGrupo2.ResumeLayout(false);
            this.gbrPerguntasGrupo2.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        protected System.Windows.Forms.FlowLayoutPanel flpAcao;
        protected System.Windows.Forms.Button btnIncluir;
        protected System.Windows.Forms.Button lblFechar;
        protected System.Windows.Forms.TextBox lblAgente;
        protected System.Windows.Forms.TextBox textAgente;
        protected System.Windows.Forms.TextBox lblTpAvaliacao;
        protected System.Windows.Forms.TextBox TextRisco;
        protected System.Windows.Forms.TextBox lblRisco;
        protected ComboBoxWithBorder cbTipoAvaliacao;
        protected ComboBoxWithBorder cbEficaciaEPI;
        protected System.Windows.Forms.TextBox lblEficaciaEPi;
        protected ComboBoxWithBorder cbUtilizaEPI;
        protected System.Windows.Forms.TextBox lblUtilizaEPI;
        protected ComboBoxWithBorder cbEficaciaEpc;
        protected System.Windows.Forms.TextBox lblEficaciaEpc;
        protected ComboBoxWithBorder cbUtilizaEPC;
        protected System.Windows.Forms.TextBox lblUtilizaEpc;
        protected System.Windows.Forms.TextBox textTecnica;
        protected ComboBoxWithBorder cbUnidadeMedida;
        protected System.Windows.Forms.TextBox lblUnidade;
        protected System.Windows.Forms.TextBox textLimiteTolerancia;
        protected System.Windows.Forms.TextBox lblLimite;
        protected System.Windows.Forms.TextBox textIntensidade;
        protected System.Windows.Forms.TextBox lblIntensidade;
        protected System.Windows.Forms.GroupBox grbEpiEpc;
        protected System.Windows.Forms.Label lblPergunta1;
        protected System.Windows.Forms.Label lblPergunta4;
        protected System.Windows.Forms.Label lblPergunta3;
        protected System.Windows.Forms.Label lblPergunta2;
        protected System.Windows.Forms.GroupBox gbrPerguntasGrupo2;
        protected System.Windows.Forms.FlowLayoutPanel flpAcaoEpi;
        protected System.Windows.Forms.Button btnIncluirEpi;
        protected System.Windows.Forms.Button btnExcluirEpi;
        protected System.Windows.Forms.GroupBox grbEpi;
        protected System.Windows.Forms.DataGridView dgvEpis;
        protected System.Windows.Forms.Label lblGrupo2Pergunta2;
        protected System.Windows.Forms.Label lblGrupo2Pergunta1;
        protected ComboBoxWithBorder cbHigienizacao;
        protected ComboBoxWithBorder cbPeriodicidadeTroca;
        protected ComboBoxWithBorder cbPrazoValidade;
        protected ComboBoxWithBorder cbUsoIniterrupto;
        protected ComboBoxWithBorder cbCondicaoFuncionamento;
        protected ComboBoxWithBorder cbMedidaProtecao;
        protected System.Windows.Forms.TextBox lblHigienizacao;
        protected System.Windows.Forms.TextBox lblPeriodicidadeTroca;
        protected System.Windows.Forms.TextBox lblPrazoValidade;
        protected System.Windows.Forms.TextBox lblUsoInit;
        protected System.Windows.Forms.TextBox lblCondicaoFuncionamento;
        protected System.Windows.Forms.TextBox lblMedidaProtecao;
        protected System.Windows.Forms.Label lblGrupo2Pergunta6;
        protected System.Windows.Forms.Label lblGrupo2Pergunta5;
        protected System.Windows.Forms.Label lblGrupo2Pergunta4;
        protected System.Windows.Forms.Label lblGrupo2Pergunta3;
        protected System.Windows.Forms.TextBox lblTecnica;
        protected System.Windows.Forms.TextBox textNumeroProcesso;
        protected System.Windows.Forms.TextBox lblNumeroProcesso;
    }
}