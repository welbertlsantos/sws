﻿namespace SegSis.View
{
    partial class frm_cliente_inclui
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frm_cliente_inclui));
            this.text_inscricao = new System.Windows.Forms.TextBox();
            this.text_fantasia = new System.Windows.Forms.TextBox();
            this.text_razaoSocial = new System.Windows.Forms.TextBox();
            this.text_cnpj = new System.Windows.Forms.MaskedTextBox();
            this.text_cep = new System.Windows.Forms.MaskedTextBox();
            this.lbl_razaoSocial = new System.Windows.Forms.Label();
            this.lbl_inscricao = new System.Windows.Forms.Label();
            this.lbl_cnpj = new System.Windows.Forms.Label();
            this.lbl_fantasia = new System.Windows.Forms.Label();
            this.lbl_telefone = new System.Windows.Forms.Label();
            this.lbl_endereco = new System.Windows.Forms.Label();
            this.grb_dados = new System.Windows.Forms.GroupBox();
            this.label1 = new System.Windows.Forms.Label();
            this.gb_credenciada = new System.Windows.Forms.GroupBox();
            this.text_credenciada = new System.Windows.Forms.TextBox();
            this.btn_credenciada = new System.Windows.Forms.Button();
            this.gb_Matriz = new System.Windows.Forms.GroupBox();
            this.text_matriz = new System.Windows.Forms.TextBox();
            this.btn_matiz = new System.Windows.Forms.Button();
            this.grb_tipoCliente = new System.Windows.Forms.GroupBox();
            this.cb_tipoCliente = new System.Windows.Forms.ComboBox();
            this.grb_vendedor = new System.Windows.Forms.GroupBox();
            this.text_vendedor = new System.Windows.Forms.TextBox();
            this.btn_vendedor = new System.Windows.Forms.Button();
            this.tb_paginacao = new System.Windows.Forms.TabControl();
            this.tb_endereco = new System.Windows.Forms.TabPage();
            this.text_telefone2 = new System.Windows.Forms.TextBox();
            this.lbl_uf = new System.Windows.Forms.Label();
            this.text_telefone1 = new System.Windows.Forms.TextBox();
            this.cb_uf = new System.Windows.Forms.ComboBox();
            this.lbl_fax = new System.Windows.Forms.Label();
            this.cb_cidade = new System.Windows.Forms.ComboBox();
            this.text_site = new System.Windows.Forms.TextBox();
            this.lbl_site = new System.Windows.Forms.Label();
            this.text_email = new System.Windows.Forms.TextBox();
            this.lbl_email = new System.Windows.Forms.Label();
            this.lbl_cep = new System.Windows.Forms.Label();
            this.lbl_cidade = new System.Windows.Forms.Label();
            this.text_bairro = new System.Windows.Forms.TextBox();
            this.lbl_bairro = new System.Windows.Forms.Label();
            this.text_complemento = new System.Windows.Forms.TextBox();
            this.lbl_complemento = new System.Windows.Forms.Label();
            this.text_numero = new System.Windows.Forms.TextBox();
            this.lbl_numero = new System.Windows.Forms.Label();
            this.text_endereco = new System.Windows.Forms.TextBox();
            this.tb_enderecoCob = new System.Windows.Forms.TabPage();
            this.lbl_ufCobranca = new System.Windows.Forms.Label();
            this.cb_cidadeCobranca = new System.Windows.Forms.ComboBox();
            this.cb_ufCob = new System.Windows.Forms.ComboBox();
            this.text_faxCobranca = new System.Windows.Forms.TextBox();
            this.lbl_faxCobranca = new System.Windows.Forms.Label();
            this.btn_copiar = new System.Windows.Forms.Button();
            this.text_telefoneCob = new System.Windows.Forms.TextBox();
            this.lbl_telefoneCob = new System.Windows.Forms.Label();
            this.text_cepCob = new System.Windows.Forms.MaskedTextBox();
            this.lbl_cepCob = new System.Windows.Forms.Label();
            this.lbl_cidadeCob = new System.Windows.Forms.Label();
            this.text_bairroCob = new System.Windows.Forms.TextBox();
            this.lbl_bairroCob = new System.Windows.Forms.Label();
            this.text_complementoCob = new System.Windows.Forms.TextBox();
            this.lbl_complementoCob = new System.Windows.Forms.Label();
            this.text_numeroCob = new System.Windows.Forms.TextBox();
            this.lbl_numeroCob = new System.Windows.Forms.Label();
            this.text_enderecoCob = new System.Windows.Forms.TextBox();
            this.lbl_enderecoCob = new System.Windows.Forms.Label();
            this.tb_cnae = new System.Windows.Forms.TabPage();
            this.grb_cnaePrincipal = new System.Windows.Forms.GroupBox();
            this.text_cnaePrincipal = new System.Windows.Forms.MaskedTextBox();
            this.btn_incluir = new System.Windows.Forms.Button();
            this.btn_excluiCnae = new System.Windows.Forms.Button();
            this.btn_incluiCnae = new System.Windows.Forms.Button();
            this.grd_cnae = new System.Windows.Forms.DataGridView();
            this.tb_medico = new System.Windows.Forms.TabPage();
            this.btn_excluirCoordenador = new System.Windows.Forms.Button();
            this.lbl_medico = new System.Windows.Forms.Label();
            this.btn_coordenador = new System.Windows.Forms.Button();
            this.text_nomeCoordenador = new System.Windows.Forms.TextBox();
            this.tab_logo = new System.Windows.Forms.TabPage();
            this.lbl_logo = new System.Windows.Forms.Label();
            this.pic_box_logo = new System.Windows.Forms.PictureBox();
            this.bt_abrir_file_dialog = new System.Windows.Forms.Button();
            this.tb_funcoes = new System.Windows.Forms.TabPage();
            this.btn_excluir = new System.Windows.Forms.Button();
            this.btn_incluirFuncao = new System.Windows.Forms.Button();
            this.dg_funcao = new System.Windows.Forms.DataGridView();
            this.tb_estudo = new System.Windows.Forms.TabPage();
            this.text_escopo = new System.Windows.Forms.TextBox();
            this.lbl_escopo = new System.Windows.Forms.Label();
            this.text_jornada = new System.Windows.Forms.TextBox();
            this.lbl_jornada = new System.Windows.Forms.Label();
            this.text_telefoneContato = new System.Windows.Forms.TextBox();
            this.grb_estimativa = new System.Windows.Forms.GroupBox();
            this.text_estFem = new System.Windows.Forms.TextBox();
            this.text_estMasc = new System.Windows.Forms.TextBox();
            this.lbl_estFem = new System.Windows.Forms.Label();
            this.lbl_estMasc = new System.Windows.Forms.Label();
            this.lbl_telefoneContato = new System.Windows.Forms.Label();
            this.text_documento = new System.Windows.Forms.TextBox();
            this.text_nome = new System.Windows.Forms.TextBox();
            this.lbl_responsavel = new System.Windows.Forms.Label();
            this.lbl_documento = new System.Windows.Forms.Label();
            this.lbl_cargo = new System.Windows.Forms.Label();
            this.text_cargo = new System.Windows.Forms.TextBox();
            this.tp_configuracao = new System.Windows.Forms.TabPage();
            this.chk_prestador = new System.Windows.Forms.CheckBox();
            this.text_aliquotaIss = new System.Windows.Forms.TextBox();
            this.lbl_aliquotaIss = new System.Windows.Forms.Label();
            this.chk_geraCobrancaValorLiquido = new System.Windows.Forms.CheckBox();
            this.chk_destacaIss = new System.Windows.Forms.CheckBox();
            this.chk_particular = new System.Windows.Forms.CheckBox();
            this.chk_usaContrato = new System.Windows.Forms.CheckBox();
            this.chk_vip = new System.Windows.Forms.CheckBox();
            this.grb_paginacao = new System.Windows.Forms.GroupBox();
            this.lbl_limpar = new System.Windows.Forms.Button();
            this.btn_fechar = new System.Windows.Forms.Button();
            this.btn_gravar = new System.Windows.Forms.Button();
            this.tp_clienteIncluir = new System.Windows.Forms.ToolTip(this.components);
            this.panel_banner.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox_banner)).BeginInit();
            this.panel.SuspendLayout();
            this.grb_dados.SuspendLayout();
            this.gb_credenciada.SuspendLayout();
            this.gb_Matriz.SuspendLayout();
            this.grb_tipoCliente.SuspendLayout();
            this.grb_vendedor.SuspendLayout();
            this.tb_paginacao.SuspendLayout();
            this.tb_endereco.SuspendLayout();
            this.tb_enderecoCob.SuspendLayout();
            this.tb_cnae.SuspendLayout();
            this.grb_cnaePrincipal.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grd_cnae)).BeginInit();
            this.tb_medico.SuspendLayout();
            this.tab_logo.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pic_box_logo)).BeginInit();
            this.tb_funcoes.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dg_funcao)).BeginInit();
            this.tb_estudo.SuspendLayout();
            this.grb_estimativa.SuspendLayout();
            this.tp_configuracao.SuspendLayout();
            this.grb_paginacao.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel
            // 
            this.panel.Controls.Add(this.grb_paginacao);
            this.panel.Controls.Add(this.grb_dados);
            this.panel.Controls.Add(this.tb_paginacao);
            // 
            // text_inscricao
            // 
            this.text_inscricao.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.text_inscricao.Location = new System.Drawing.Point(559, 157);
            this.text_inscricao.MaxLength = 20;
            this.text_inscricao.Name = "text_inscricao";
            this.text_inscricao.Size = new System.Drawing.Size(184, 20);
            this.text_inscricao.TabIndex = 4;
            // 
            // text_fantasia
            // 
            this.text_fantasia.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.text_fantasia.Location = new System.Drawing.Point(4, 157);
            this.text_fantasia.MaxLength = 100;
            this.text_fantasia.Name = "text_fantasia";
            this.text_fantasia.Size = new System.Drawing.Size(533, 20);
            this.text_fantasia.TabIndex = 3;
            // 
            // text_razaoSocial
            // 
            this.text_razaoSocial.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.text_razaoSocial.Location = new System.Drawing.Point(4, 118);
            this.text_razaoSocial.MaxLength = 100;
            this.text_razaoSocial.Name = "text_razaoSocial";
            this.text_razaoSocial.Size = new System.Drawing.Size(533, 20);
            this.text_razaoSocial.TabIndex = 1;
            // 
            // text_cnpj
            // 
            this.text_cnpj.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.text_cnpj.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.text_cnpj.Location = new System.Drawing.Point(559, 118);
            this.text_cnpj.Mask = "99,999,999/9999-99";
            this.text_cnpj.Name = "text_cnpj";
            this.text_cnpj.PromptChar = ' ';
            this.text_cnpj.Size = new System.Drawing.Size(184, 20);
            this.text_cnpj.TabIndex = 2;
            this.text_cnpj.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.text_cnpj.Leave += new System.EventHandler(this.text_cnpj_Leave);
            // 
            // text_cep
            // 
            this.text_cep.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.text_cep.Location = new System.Drawing.Point(589, 55);
            this.text_cep.Mask = "99,999-999";
            this.text_cep.Name = "text_cep";
            this.text_cep.PromptChar = ' ';
            this.text_cep.Size = new System.Drawing.Size(85, 20);
            this.text_cep.TabIndex = 12;
            // 
            // lbl_razaoSocial
            // 
            this.lbl_razaoSocial.AutoSize = true;
            this.lbl_razaoSocial.Location = new System.Drawing.Point(1, 102);
            this.lbl_razaoSocial.Name = "lbl_razaoSocial";
            this.lbl_razaoSocial.Size = new System.Drawing.Size(70, 13);
            this.lbl_razaoSocial.TabIndex = 7;
            this.lbl_razaoSocial.Text = "Razão Social";
            // 
            // lbl_inscricao
            // 
            this.lbl_inscricao.AutoSize = true;
            this.lbl_inscricao.Location = new System.Drawing.Point(557, 116);
            this.lbl_inscricao.Name = "lbl_inscricao";
            this.lbl_inscricao.Size = new System.Drawing.Size(94, 13);
            this.lbl_inscricao.TabIndex = 8;
            this.lbl_inscricao.Text = "Inscrição Estadual";
            // 
            // lbl_cnpj
            // 
            this.lbl_cnpj.AutoSize = true;
            this.lbl_cnpj.Location = new System.Drawing.Point(556, 102);
            this.lbl_cnpj.Name = "lbl_cnpj";
            this.lbl_cnpj.Size = new System.Drawing.Size(34, 13);
            this.lbl_cnpj.TabIndex = 9;
            this.lbl_cnpj.Text = "CNPJ";
            // 
            // lbl_fantasia
            // 
            this.lbl_fantasia.AutoSize = true;
            this.lbl_fantasia.Location = new System.Drawing.Point(1, 141);
            this.lbl_fantasia.Name = "lbl_fantasia";
            this.lbl_fantasia.Size = new System.Drawing.Size(78, 13);
            this.lbl_fantasia.TabIndex = 10;
            this.lbl_fantasia.Text = "Nome Fantasia";
            // 
            // lbl_telefone
            // 
            this.lbl_telefone.AutoSize = true;
            this.lbl_telefone.Location = new System.Drawing.Point(307, 83);
            this.lbl_telefone.Name = "lbl_telefone";
            this.lbl_telefone.Size = new System.Drawing.Size(49, 13);
            this.lbl_telefone.TabIndex = 11;
            this.lbl_telefone.Text = "Telefone";
            // 
            // lbl_endereco
            // 
            this.lbl_endereco.AutoSize = true;
            this.lbl_endereco.Location = new System.Drawing.Point(6, 3);
            this.lbl_endereco.Name = "lbl_endereco";
            this.lbl_endereco.Size = new System.Drawing.Size(53, 13);
            this.lbl_endereco.TabIndex = 12;
            this.lbl_endereco.Text = "Endereço";
            // 
            // grb_dados
            // 
            this.grb_dados.BackColor = System.Drawing.Color.White;
            this.grb_dados.Controls.Add(this.label1);
            this.grb_dados.Controls.Add(this.gb_credenciada);
            this.grb_dados.Controls.Add(this.gb_Matriz);
            this.grb_dados.Controls.Add(this.grb_tipoCliente);
            this.grb_dados.Controls.Add(this.text_razaoSocial);
            this.grb_dados.Controls.Add(this.grb_vendedor);
            this.grb_dados.Controls.Add(this.text_inscricao);
            this.grb_dados.Controls.Add(this.text_fantasia);
            this.grb_dados.Controls.Add(this.text_cnpj);
            this.grb_dados.Controls.Add(this.lbl_razaoSocial);
            this.grb_dados.Controls.Add(this.lbl_inscricao);
            this.grb_dados.Controls.Add(this.lbl_fantasia);
            this.grb_dados.Controls.Add(this.lbl_cnpj);
            this.grb_dados.Location = new System.Drawing.Point(2, 3);
            this.grb_dados.Name = "grb_dados";
            this.grb_dados.Size = new System.Drawing.Size(781, 182);
            this.grb_dados.TabIndex = 1;
            this.grb_dados.TabStop = false;
            this.grb_dados.Text = "Dados";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(557, 141);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(150, 13);
            this.label1.TabIndex = 27;
            this.label1.Text = "Inscrição Estadual / Municipal";
            // 
            // gb_credenciada
            // 
            this.gb_credenciada.Controls.Add(this.text_credenciada);
            this.gb_credenciada.Controls.Add(this.btn_credenciada);
            this.gb_credenciada.Enabled = false;
            this.gb_credenciada.Location = new System.Drawing.Point(388, 54);
            this.gb_credenciada.Name = "gb_credenciada";
            this.gb_credenciada.Size = new System.Drawing.Size(387, 38);
            this.gb_credenciada.TabIndex = 26;
            this.gb_credenciada.TabStop = false;
            this.gb_credenciada.Text = "Credenciada";
            // 
            // text_credenciada
            // 
            this.text_credenciada.BackColor = System.Drawing.Color.LightGray;
            this.text_credenciada.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.text_credenciada.Location = new System.Drawing.Point(6, 13);
            this.text_credenciada.Name = "text_credenciada";
            this.text_credenciada.Size = new System.Drawing.Size(327, 20);
            this.text_credenciada.TabIndex = 1;
            // 
            // btn_credenciada
            // 
            this.btn_credenciada.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_credenciada.Location = new System.Drawing.Point(339, 11);
            this.btn_credenciada.Name = "btn_credenciada";
            this.btn_credenciada.Size = new System.Drawing.Size(41, 23);
            this.btn_credenciada.TabIndex = 0;
            this.btn_credenciada.Text = "&Credenciada";
            this.btn_credenciada.UseVisualStyleBackColor = true;
            this.btn_credenciada.Click += new System.EventHandler(this.btn_credenciada_Click);
            // 
            // gb_Matriz
            // 
            this.gb_Matriz.Controls.Add(this.text_matriz);
            this.gb_Matriz.Controls.Add(this.btn_matiz);
            this.gb_Matriz.Enabled = false;
            this.gb_Matriz.Location = new System.Drawing.Point(4, 54);
            this.gb_Matriz.Name = "gb_Matriz";
            this.gb_Matriz.Size = new System.Drawing.Size(378, 38);
            this.gb_Matriz.TabIndex = 25;
            this.gb_Matriz.TabStop = false;
            this.gb_Matriz.Text = "Matriz";
            // 
            // text_matriz
            // 
            this.text_matriz.BackColor = System.Drawing.Color.LightGray;
            this.text_matriz.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.text_matriz.Location = new System.Drawing.Point(6, 14);
            this.text_matriz.Name = "text_matriz";
            this.text_matriz.Size = new System.Drawing.Size(319, 20);
            this.text_matriz.TabIndex = 1;
            // 
            // btn_matiz
            // 
            this.btn_matiz.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_matiz.Location = new System.Drawing.Point(331, 11);
            this.btn_matiz.Name = "btn_matiz";
            this.btn_matiz.Size = new System.Drawing.Size(41, 23);
            this.btn_matiz.TabIndex = 0;
            this.btn_matiz.Text = "&Matriz";
            this.btn_matiz.UseVisualStyleBackColor = true;
            this.btn_matiz.Click += new System.EventHandler(this.btn_matiz_Click);
            // 
            // grb_tipoCliente
            // 
            this.grb_tipoCliente.Controls.Add(this.cb_tipoCliente);
            this.grb_tipoCliente.Location = new System.Drawing.Point(5, 13);
            this.grb_tipoCliente.Name = "grb_tipoCliente";
            this.grb_tipoCliente.Size = new System.Drawing.Size(132, 39);
            this.grb_tipoCliente.TabIndex = 24;
            this.grb_tipoCliente.TabStop = false;
            this.grb_tipoCliente.Text = "Tipo Cliente";
            // 
            // cb_tipoCliente
            // 
            this.cb_tipoCliente.BackColor = System.Drawing.Color.LightGray;
            this.cb_tipoCliente.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cb_tipoCliente.FormattingEnabled = true;
            this.cb_tipoCliente.Location = new System.Drawing.Point(8, 14);
            this.cb_tipoCliente.Name = "cb_tipoCliente";
            this.cb_tipoCliente.Size = new System.Drawing.Size(115, 21);
            this.cb_tipoCliente.TabIndex = 0;
            this.cb_tipoCliente.SelectedIndexChanged += new System.EventHandler(this.cb_tipoCliente_SelectedIndexChanged);
            // 
            // grb_vendedor
            // 
            this.grb_vendedor.BackColor = System.Drawing.Color.White;
            this.grb_vendedor.Controls.Add(this.text_vendedor);
            this.grb_vendedor.Controls.Add(this.btn_vendedor);
            this.grb_vendedor.Location = new System.Drawing.Point(149, 13);
            this.grb_vendedor.Name = "grb_vendedor";
            this.grb_vendedor.Size = new System.Drawing.Size(626, 39);
            this.grb_vendedor.TabIndex = 5;
            this.grb_vendedor.TabStop = false;
            this.grb_vendedor.Text = "Vendedor";
            // 
            // text_vendedor
            // 
            this.text_vendedor.BackColor = System.Drawing.Color.LightGray;
            this.text_vendedor.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.text_vendedor.Location = new System.Drawing.Point(6, 14);
            this.text_vendedor.MaxLength = 100;
            this.text_vendedor.Name = "text_vendedor";
            this.text_vendedor.Size = new System.Drawing.Size(538, 20);
            this.text_vendedor.TabIndex = 5;
            this.text_vendedor.TabStop = false;
            // 
            // btn_vendedor
            // 
            this.btn_vendedor.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_vendedor.Location = new System.Drawing.Point(550, 12);
            this.btn_vendedor.Name = "btn_vendedor";
            this.btn_vendedor.Size = new System.Drawing.Size(70, 23);
            this.btn_vendedor.TabIndex = 5;
            this.btn_vendedor.Text = "&Selecionar";
            this.btn_vendedor.UseVisualStyleBackColor = true;
            this.btn_vendedor.Click += new System.EventHandler(this.btn_vendedor_Click);
            // 
            // tb_paginacao
            // 
            this.tb_paginacao.Controls.Add(this.tb_endereco);
            this.tb_paginacao.Controls.Add(this.tb_enderecoCob);
            this.tb_paginacao.Controls.Add(this.tb_cnae);
            this.tb_paginacao.Controls.Add(this.tb_medico);
            this.tb_paginacao.Controls.Add(this.tab_logo);
            this.tb_paginacao.Controls.Add(this.tb_funcoes);
            this.tb_paginacao.Controls.Add(this.tb_estudo);
            this.tb_paginacao.Controls.Add(this.tp_configuracao);
            this.tb_paginacao.Location = new System.Drawing.Point(3, 191);
            this.tb_paginacao.Name = "tb_paginacao";
            this.tb_paginacao.SelectedIndex = 0;
            this.tb_paginacao.Size = new System.Drawing.Size(781, 267);
            this.tb_paginacao.TabIndex = 8;
            // 
            // tb_endereco
            // 
            this.tb_endereco.BackColor = System.Drawing.Color.White;
            this.tb_endereco.Controls.Add(this.text_telefone2);
            this.tb_endereco.Controls.Add(this.lbl_uf);
            this.tb_endereco.Controls.Add(this.text_telefone1);
            this.tb_endereco.Controls.Add(this.lbl_telefone);
            this.tb_endereco.Controls.Add(this.cb_uf);
            this.tb_endereco.Controls.Add(this.lbl_fax);
            this.tb_endereco.Controls.Add(this.cb_cidade);
            this.tb_endereco.Controls.Add(this.text_site);
            this.tb_endereco.Controls.Add(this.lbl_site);
            this.tb_endereco.Controls.Add(this.text_email);
            this.tb_endereco.Controls.Add(this.lbl_email);
            this.tb_endereco.Controls.Add(this.text_cep);
            this.tb_endereco.Controls.Add(this.lbl_cep);
            this.tb_endereco.Controls.Add(this.lbl_cidade);
            this.tb_endereco.Controls.Add(this.text_bairro);
            this.tb_endereco.Controls.Add(this.lbl_bairro);
            this.tb_endereco.Controls.Add(this.text_complemento);
            this.tb_endereco.Controls.Add(this.lbl_complemento);
            this.tb_endereco.Controls.Add(this.text_numero);
            this.tb_endereco.Controls.Add(this.lbl_numero);
            this.tb_endereco.Controls.Add(this.text_endereco);
            this.tb_endereco.Controls.Add(this.lbl_endereco);
            this.tb_endereco.Location = new System.Drawing.Point(4, 22);
            this.tb_endereco.Name = "tb_endereco";
            this.tb_endereco.Padding = new System.Windows.Forms.Padding(3);
            this.tb_endereco.Size = new System.Drawing.Size(773, 241);
            this.tb_endereco.TabIndex = 0;
            this.tb_endereco.Text = "Endereço Comercial";
            // 
            // text_telefone2
            // 
            this.text_telefone2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.text_telefone2.Location = new System.Drawing.Point(515, 99);
            this.text_telefone2.MaxLength = 15;
            this.text_telefone2.Name = "text_telefone2";
            this.text_telefone2.Size = new System.Drawing.Size(159, 20);
            this.text_telefone2.TabIndex = 16;
            // 
            // lbl_uf
            // 
            this.lbl_uf.AutoSize = true;
            this.lbl_uf.Location = new System.Drawing.Point(6, 83);
            this.lbl_uf.Name = "lbl_uf";
            this.lbl_uf.Size = new System.Drawing.Size(21, 13);
            this.lbl_uf.TabIndex = 33;
            this.lbl_uf.Text = "UF";
            // 
            // text_telefone1
            // 
            this.text_telefone1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.text_telefone1.Location = new System.Drawing.Point(310, 99);
            this.text_telefone1.MaxLength = 15;
            this.text_telefone1.Name = "text_telefone1";
            this.text_telefone1.Size = new System.Drawing.Size(176, 20);
            this.text_telefone1.TabIndex = 15;
            // 
            // cb_uf
            // 
            this.cb_uf.BackColor = System.Drawing.Color.LightGray;
            this.cb_uf.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cb_uf.FormattingEnabled = true;
            this.cb_uf.Location = new System.Drawing.Point(9, 98);
            this.cb_uf.Name = "cb_uf";
            this.cb_uf.Size = new System.Drawing.Size(50, 21);
            this.cb_uf.TabIndex = 31;
            this.cb_uf.Click += new System.EventHandler(this.cb_uf_Click);
            // 
            // lbl_fax
            // 
            this.lbl_fax.AutoSize = true;
            this.lbl_fax.Location = new System.Drawing.Point(512, 83);
            this.lbl_fax.Name = "lbl_fax";
            this.lbl_fax.Size = new System.Drawing.Size(24, 13);
            this.lbl_fax.TabIndex = 26;
            this.lbl_fax.Text = "Fax";
            // 
            // cb_cidade
            // 
            this.cb_cidade.BackColor = System.Drawing.Color.LightGray;
            this.cb_cidade.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cb_cidade.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cb_cidade.FormattingEnabled = true;
            this.cb_cidade.Location = new System.Drawing.Point(76, 98);
            this.cb_cidade.Name = "cb_cidade";
            this.cb_cidade.Size = new System.Drawing.Size(216, 21);
            this.cb_cidade.TabIndex = 14;
            this.cb_cidade.Click += new System.EventHandler(this.cb_cidade_Click);
            // 
            // text_site
            // 
            this.text_site.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.text_site.Location = new System.Drawing.Point(310, 143);
            this.text_site.MaxLength = 50;
            this.text_site.Name = "text_site";
            this.text_site.Size = new System.Drawing.Size(430, 20);
            this.text_site.TabIndex = 18;
            // 
            // lbl_site
            // 
            this.lbl_site.AutoSize = true;
            this.lbl_site.Location = new System.Drawing.Point(307, 127);
            this.lbl_site.Name = "lbl_site";
            this.lbl_site.Size = new System.Drawing.Size(25, 13);
            this.lbl_site.TabIndex = 30;
            this.lbl_site.Text = "Site";
            // 
            // text_email
            // 
            this.text_email.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.text_email.Location = new System.Drawing.Point(9, 142);
            this.text_email.MaxLength = 50;
            this.text_email.Name = "text_email";
            this.text_email.Size = new System.Drawing.Size(283, 20);
            this.text_email.TabIndex = 17;
            // 
            // lbl_email
            // 
            this.lbl_email.AutoSize = true;
            this.lbl_email.Location = new System.Drawing.Point(6, 126);
            this.lbl_email.Name = "lbl_email";
            this.lbl_email.Size = new System.Drawing.Size(32, 13);
            this.lbl_email.TabIndex = 28;
            this.lbl_email.Text = "Email";
            // 
            // lbl_cep
            // 
            this.lbl_cep.AutoSize = true;
            this.lbl_cep.Location = new System.Drawing.Point(586, 39);
            this.lbl_cep.Name = "lbl_cep";
            this.lbl_cep.Size = new System.Drawing.Size(28, 13);
            this.lbl_cep.TabIndex = 22;
            this.lbl_cep.Text = "CEP";
            // 
            // lbl_cidade
            // 
            this.lbl_cidade.AutoSize = true;
            this.lbl_cidade.Location = new System.Drawing.Point(73, 83);
            this.lbl_cidade.Name = "lbl_cidade";
            this.lbl_cidade.Size = new System.Drawing.Size(40, 13);
            this.lbl_cidade.TabIndex = 20;
            this.lbl_cidade.Text = "Cidade";
            // 
            // text_bairro
            // 
            this.text_bairro.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.text_bairro.Location = new System.Drawing.Point(310, 55);
            this.text_bairro.MaxLength = 50;
            this.text_bairro.Name = "text_bairro";
            this.text_bairro.Size = new System.Drawing.Size(253, 20);
            this.text_bairro.TabIndex = 11;
            // 
            // lbl_bairro
            // 
            this.lbl_bairro.AutoSize = true;
            this.lbl_bairro.Location = new System.Drawing.Point(307, 39);
            this.lbl_bairro.Name = "lbl_bairro";
            this.lbl_bairro.Size = new System.Drawing.Size(34, 13);
            this.lbl_bairro.TabIndex = 18;
            this.lbl_bairro.Text = "Bairro";
            // 
            // text_complemento
            // 
            this.text_complemento.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.text_complemento.Location = new System.Drawing.Point(9, 55);
            this.text_complemento.MaxLength = 100;
            this.text_complemento.Name = "text_complemento";
            this.text_complemento.Size = new System.Drawing.Size(283, 20);
            this.text_complemento.TabIndex = 10;
            // 
            // lbl_complemento
            // 
            this.lbl_complemento.AutoSize = true;
            this.lbl_complemento.Location = new System.Drawing.Point(6, 39);
            this.lbl_complemento.Name = "lbl_complemento";
            this.lbl_complemento.Size = new System.Drawing.Size(71, 13);
            this.lbl_complemento.TabIndex = 16;
            this.lbl_complemento.Text = "Complemento";
            // 
            // text_numero
            // 
            this.text_numero.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.text_numero.Location = new System.Drawing.Point(589, 16);
            this.text_numero.MaxLength = 10;
            this.text_numero.Name = "text_numero";
            this.text_numero.Size = new System.Drawing.Size(151, 20);
            this.text_numero.TabIndex = 9;
            // 
            // lbl_numero
            // 
            this.lbl_numero.AutoSize = true;
            this.lbl_numero.Location = new System.Drawing.Point(586, 3);
            this.lbl_numero.Name = "lbl_numero";
            this.lbl_numero.Size = new System.Drawing.Size(44, 13);
            this.lbl_numero.TabIndex = 14;
            this.lbl_numero.Text = "Número";
            // 
            // text_endereco
            // 
            this.text_endereco.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.text_endereco.Location = new System.Drawing.Point(9, 16);
            this.text_endereco.MaxLength = 100;
            this.text_endereco.Name = "text_endereco";
            this.text_endereco.Size = new System.Drawing.Size(554, 20);
            this.text_endereco.TabIndex = 8;
            // 
            // tb_enderecoCob
            // 
            this.tb_enderecoCob.BackColor = System.Drawing.Color.White;
            this.tb_enderecoCob.Controls.Add(this.lbl_ufCobranca);
            this.tb_enderecoCob.Controls.Add(this.cb_cidadeCobranca);
            this.tb_enderecoCob.Controls.Add(this.cb_ufCob);
            this.tb_enderecoCob.Controls.Add(this.text_faxCobranca);
            this.tb_enderecoCob.Controls.Add(this.lbl_faxCobranca);
            this.tb_enderecoCob.Controls.Add(this.btn_copiar);
            this.tb_enderecoCob.Controls.Add(this.text_telefoneCob);
            this.tb_enderecoCob.Controls.Add(this.lbl_telefoneCob);
            this.tb_enderecoCob.Controls.Add(this.text_cepCob);
            this.tb_enderecoCob.Controls.Add(this.lbl_cepCob);
            this.tb_enderecoCob.Controls.Add(this.lbl_cidadeCob);
            this.tb_enderecoCob.Controls.Add(this.text_bairroCob);
            this.tb_enderecoCob.Controls.Add(this.lbl_bairroCob);
            this.tb_enderecoCob.Controls.Add(this.text_complementoCob);
            this.tb_enderecoCob.Controls.Add(this.lbl_complementoCob);
            this.tb_enderecoCob.Controls.Add(this.text_numeroCob);
            this.tb_enderecoCob.Controls.Add(this.lbl_numeroCob);
            this.tb_enderecoCob.Controls.Add(this.text_enderecoCob);
            this.tb_enderecoCob.Controls.Add(this.lbl_enderecoCob);
            this.tb_enderecoCob.Location = new System.Drawing.Point(4, 22);
            this.tb_enderecoCob.Name = "tb_enderecoCob";
            this.tb_enderecoCob.Padding = new System.Windows.Forms.Padding(3);
            this.tb_enderecoCob.Size = new System.Drawing.Size(773, 241);
            this.tb_enderecoCob.TabIndex = 3;
            this.tb_enderecoCob.Text = "Endereco de Cobrança";
            // 
            // lbl_ufCobranca
            // 
            this.lbl_ufCobranca.AutoSize = true;
            this.lbl_ufCobranca.Location = new System.Drawing.Point(6, 83);
            this.lbl_ufCobranca.Name = "lbl_ufCobranca";
            this.lbl_ufCobranca.Size = new System.Drawing.Size(21, 13);
            this.lbl_ufCobranca.TabIndex = 51;
            this.lbl_ufCobranca.Text = "UF";
            // 
            // cb_cidadeCobranca
            // 
            this.cb_cidadeCobranca.BackColor = System.Drawing.Color.LightGray;
            this.cb_cidadeCobranca.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cb_cidadeCobranca.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cb_cidadeCobranca.FormattingEnabled = true;
            this.cb_cidadeCobranca.Location = new System.Drawing.Point(75, 98);
            this.cb_cidadeCobranca.Name = "cb_cidadeCobranca";
            this.cb_cidadeCobranca.Size = new System.Drawing.Size(216, 21);
            this.cb_cidadeCobranca.TabIndex = 50;
            this.cb_cidadeCobranca.Click += new System.EventHandler(this.cb_cidadeCobranca_Click);
            // 
            // cb_ufCob
            // 
            this.cb_ufCob.BackColor = System.Drawing.Color.LightGray;
            this.cb_ufCob.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cb_ufCob.FormattingEnabled = true;
            this.cb_ufCob.Location = new System.Drawing.Point(9, 98);
            this.cb_ufCob.Name = "cb_ufCob";
            this.cb_ufCob.Size = new System.Drawing.Size(50, 21);
            this.cb_ufCob.TabIndex = 7;
            this.cb_ufCob.Click += new System.EventHandler(this.cb_ufCob_Click);
            // 
            // text_faxCobranca
            // 
            this.text_faxCobranca.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.text_faxCobranca.Location = new System.Drawing.Point(515, 98);
            this.text_faxCobranca.MaxLength = 15;
            this.text_faxCobranca.Name = "text_faxCobranca";
            this.text_faxCobranca.Size = new System.Drawing.Size(159, 20);
            this.text_faxCobranca.TabIndex = 9;
            // 
            // lbl_faxCobranca
            // 
            this.lbl_faxCobranca.AutoSize = true;
            this.lbl_faxCobranca.Location = new System.Drawing.Point(512, 83);
            this.lbl_faxCobranca.Name = "lbl_faxCobranca";
            this.lbl_faxCobranca.Size = new System.Drawing.Size(24, 13);
            this.lbl_faxCobranca.TabIndex = 49;
            this.lbl_faxCobranca.Text = "Fax";
            // 
            // btn_copiar
            // 
            this.btn_copiar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_copiar.Location = new System.Drawing.Point(9, 135);
            this.btn_copiar.Name = "btn_copiar";
            this.btn_copiar.Size = new System.Drawing.Size(76, 23);
            this.btn_copiar.TabIndex = 10;
            this.btn_copiar.Text = "Copiar";
            this.btn_copiar.UseVisualStyleBackColor = true;
            this.btn_copiar.Click += new System.EventHandler(this.btn_copiar_Click);
            // 
            // text_telefoneCob
            // 
            this.text_telefoneCob.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.text_telefoneCob.Location = new System.Drawing.Point(310, 98);
            this.text_telefoneCob.MaxLength = 15;
            this.text_telefoneCob.Name = "text_telefoneCob";
            this.text_telefoneCob.Size = new System.Drawing.Size(176, 20);
            this.text_telefoneCob.TabIndex = 8;
            // 
            // lbl_telefoneCob
            // 
            this.lbl_telefoneCob.AutoSize = true;
            this.lbl_telefoneCob.Location = new System.Drawing.Point(307, 81);
            this.lbl_telefoneCob.Name = "lbl_telefoneCob";
            this.lbl_telefoneCob.Size = new System.Drawing.Size(49, 13);
            this.lbl_telefoneCob.TabIndex = 11;
            this.lbl_telefoneCob.Text = "Telefone";
            // 
            // text_cepCob
            // 
            this.text_cepCob.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.text_cepCob.Location = new System.Drawing.Point(589, 55);
            this.text_cepCob.Mask = "99,999-999";
            this.text_cepCob.Name = "text_cepCob";
            this.text_cepCob.PromptChar = ' ';
            this.text_cepCob.Size = new System.Drawing.Size(85, 20);
            this.text_cepCob.TabIndex = 5;
            // 
            // lbl_cepCob
            // 
            this.lbl_cepCob.AutoSize = true;
            this.lbl_cepCob.Location = new System.Drawing.Point(586, 39);
            this.lbl_cepCob.Name = "lbl_cepCob";
            this.lbl_cepCob.Size = new System.Drawing.Size(28, 13);
            this.lbl_cepCob.TabIndex = 45;
            this.lbl_cepCob.Text = "CEP";
            // 
            // lbl_cidadeCob
            // 
            this.lbl_cidadeCob.AutoSize = true;
            this.lbl_cidadeCob.Location = new System.Drawing.Point(72, 83);
            this.lbl_cidadeCob.Name = "lbl_cidadeCob";
            this.lbl_cidadeCob.Size = new System.Drawing.Size(40, 13);
            this.lbl_cidadeCob.TabIndex = 44;
            this.lbl_cidadeCob.Text = "Cidade";
            // 
            // text_bairroCob
            // 
            this.text_bairroCob.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.text_bairroCob.Location = new System.Drawing.Point(310, 55);
            this.text_bairroCob.MaxLength = 50;
            this.text_bairroCob.Name = "text_bairroCob";
            this.text_bairroCob.Size = new System.Drawing.Size(253, 20);
            this.text_bairroCob.TabIndex = 4;
            // 
            // lbl_bairroCob
            // 
            this.lbl_bairroCob.AutoSize = true;
            this.lbl_bairroCob.Location = new System.Drawing.Point(307, 39);
            this.lbl_bairroCob.Name = "lbl_bairroCob";
            this.lbl_bairroCob.Size = new System.Drawing.Size(34, 13);
            this.lbl_bairroCob.TabIndex = 43;
            this.lbl_bairroCob.Text = "Bairro";
            // 
            // text_complementoCob
            // 
            this.text_complementoCob.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.text_complementoCob.Location = new System.Drawing.Point(9, 55);
            this.text_complementoCob.MaxLength = 100;
            this.text_complementoCob.Name = "text_complementoCob";
            this.text_complementoCob.Size = new System.Drawing.Size(283, 20);
            this.text_complementoCob.TabIndex = 3;
            // 
            // lbl_complementoCob
            // 
            this.lbl_complementoCob.AutoSize = true;
            this.lbl_complementoCob.Location = new System.Drawing.Point(6, 39);
            this.lbl_complementoCob.Name = "lbl_complementoCob";
            this.lbl_complementoCob.Size = new System.Drawing.Size(71, 13);
            this.lbl_complementoCob.TabIndex = 41;
            this.lbl_complementoCob.Text = "Complemento";
            // 
            // text_numeroCob
            // 
            this.text_numeroCob.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.text_numeroCob.Location = new System.Drawing.Point(589, 16);
            this.text_numeroCob.MaxLength = 10;
            this.text_numeroCob.Name = "text_numeroCob";
            this.text_numeroCob.Size = new System.Drawing.Size(138, 20);
            this.text_numeroCob.TabIndex = 2;
            // 
            // lbl_numeroCob
            // 
            this.lbl_numeroCob.AutoSize = true;
            this.lbl_numeroCob.Location = new System.Drawing.Point(586, 3);
            this.lbl_numeroCob.Name = "lbl_numeroCob";
            this.lbl_numeroCob.Size = new System.Drawing.Size(44, 13);
            this.lbl_numeroCob.TabIndex = 39;
            this.lbl_numeroCob.Text = "Número";
            // 
            // text_enderecoCob
            // 
            this.text_enderecoCob.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.text_enderecoCob.Location = new System.Drawing.Point(9, 16);
            this.text_enderecoCob.MaxLength = 100;
            this.text_enderecoCob.Name = "text_enderecoCob";
            this.text_enderecoCob.Size = new System.Drawing.Size(554, 20);
            this.text_enderecoCob.TabIndex = 1;
            // 
            // lbl_enderecoCob
            // 
            this.lbl_enderecoCob.AutoSize = true;
            this.lbl_enderecoCob.Location = new System.Drawing.Point(6, 3);
            this.lbl_enderecoCob.Name = "lbl_enderecoCob";
            this.lbl_enderecoCob.Size = new System.Drawing.Size(53, 13);
            this.lbl_enderecoCob.TabIndex = 36;
            this.lbl_enderecoCob.Text = "Endereço";
            // 
            // tb_cnae
            // 
            this.tb_cnae.BackColor = System.Drawing.Color.White;
            this.tb_cnae.Controls.Add(this.grb_cnaePrincipal);
            this.tb_cnae.Controls.Add(this.btn_excluiCnae);
            this.tb_cnae.Controls.Add(this.btn_incluiCnae);
            this.tb_cnae.Controls.Add(this.grd_cnae);
            this.tb_cnae.Location = new System.Drawing.Point(4, 22);
            this.tb_cnae.Name = "tb_cnae";
            this.tb_cnae.Padding = new System.Windows.Forms.Padding(3);
            this.tb_cnae.Size = new System.Drawing.Size(773, 241);
            this.tb_cnae.TabIndex = 1;
            this.tb_cnae.Text = "CNAE";
            // 
            // grb_cnaePrincipal
            // 
            this.grb_cnaePrincipal.Controls.Add(this.text_cnaePrincipal);
            this.grb_cnaePrincipal.Controls.Add(this.btn_incluir);
            this.grb_cnaePrincipal.Location = new System.Drawing.Point(525, 7);
            this.grb_cnaePrincipal.Name = "grb_cnaePrincipal";
            this.grb_cnaePrincipal.Size = new System.Drawing.Size(242, 56);
            this.grb_cnaePrincipal.TabIndex = 3;
            this.grb_cnaePrincipal.TabStop = false;
            this.grb_cnaePrincipal.Text = "Cnae Principal";
            // 
            // text_cnaePrincipal
            // 
            this.text_cnaePrincipal.BackColor = System.Drawing.Color.LightGray;
            this.text_cnaePrincipal.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.text_cnaePrincipal.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.text_cnaePrincipal.Location = new System.Drawing.Point(11, 19);
            this.text_cnaePrincipal.Mask = "00,00-0/00";
            this.text_cnaePrincipal.Name = "text_cnaePrincipal";
            this.text_cnaePrincipal.ReadOnly = true;
            this.text_cnaePrincipal.Size = new System.Drawing.Size(138, 20);
            this.text_cnaePrincipal.TabIndex = 6;
            this.text_cnaePrincipal.TabStop = false;
            this.text_cnaePrincipal.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // btn_incluir
            // 
            this.btn_incluir.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_incluir.Location = new System.Drawing.Point(155, 16);
            this.btn_incluir.Name = "btn_incluir";
            this.btn_incluir.Size = new System.Drawing.Size(75, 23);
            this.btn_incluir.TabIndex = 4;
            this.btn_incluir.Text = "&Principal";
            this.btn_incluir.UseVisualStyleBackColor = true;
            this.btn_incluir.Click += new System.EventHandler(this.btn_cnaePrincipal_Click);
            // 
            // btn_excluiCnae
            // 
            this.btn_excluiCnae.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_excluiCnae.Location = new System.Drawing.Point(87, 161);
            this.btn_excluiCnae.Name = "btn_excluiCnae";
            this.btn_excluiCnae.Size = new System.Drawing.Size(75, 23);
            this.btn_excluiCnae.TabIndex = 2;
            this.btn_excluiCnae.Text = "Excluir";
            this.btn_excluiCnae.UseVisualStyleBackColor = true;
            this.btn_excluiCnae.Click += new System.EventHandler(this.btn_excluiCnae_Click);
            // 
            // btn_incluiCnae
            // 
            this.btn_incluiCnae.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_incluiCnae.Location = new System.Drawing.Point(6, 161);
            this.btn_incluiCnae.Name = "btn_incluiCnae";
            this.btn_incluiCnae.Size = new System.Drawing.Size(75, 23);
            this.btn_incluiCnae.TabIndex = 1;
            this.btn_incluiCnae.Text = "Selecionar";
            this.btn_incluiCnae.UseVisualStyleBackColor = true;
            this.btn_incluiCnae.Click += new System.EventHandler(this.btn_incluiCnae_Click);
            // 
            // grd_cnae
            // 
            this.grd_cnae.AllowUserToAddRows = false;
            this.grd_cnae.AllowUserToDeleteRows = false;
            this.grd_cnae.AllowUserToOrderColumns = true;
            this.grd_cnae.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.DisplayedCells;
            this.grd_cnae.BackgroundColor = System.Drawing.Color.White;
            this.grd_cnae.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.grd_cnae.GridColor = System.Drawing.Color.Gray;
            this.grd_cnae.Location = new System.Drawing.Point(6, 6);
            this.grd_cnae.Name = "grd_cnae";
            this.grd_cnae.ReadOnly = true;
            this.grd_cnae.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.grd_cnae.Size = new System.Drawing.Size(513, 149);
            this.grd_cnae.TabIndex = 3;
            this.grd_cnae.TabStop = false;
            // 
            // tb_medico
            // 
            this.tb_medico.BackColor = System.Drawing.Color.White;
            this.tb_medico.Controls.Add(this.btn_excluirCoordenador);
            this.tb_medico.Controls.Add(this.lbl_medico);
            this.tb_medico.Controls.Add(this.btn_coordenador);
            this.tb_medico.Controls.Add(this.text_nomeCoordenador);
            this.tb_medico.Location = new System.Drawing.Point(4, 22);
            this.tb_medico.Name = "tb_medico";
            this.tb_medico.Padding = new System.Windows.Forms.Padding(3);
            this.tb_medico.Size = new System.Drawing.Size(773, 241);
            this.tb_medico.TabIndex = 4;
            this.tb_medico.Text = "Médico Coordenador";
            // 
            // btn_excluirCoordenador
            // 
            this.btn_excluirCoordenador.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_excluirCoordenador.Location = new System.Drawing.Point(85, 54);
            this.btn_excluirCoordenador.Name = "btn_excluirCoordenador";
            this.btn_excluirCoordenador.Size = new System.Drawing.Size(75, 23);
            this.btn_excluirCoordenador.TabIndex = 3;
            this.btn_excluirCoordenador.Text = "&Excluir";
            this.btn_excluirCoordenador.UseVisualStyleBackColor = true;
            this.btn_excluirCoordenador.Click += new System.EventHandler(this.btn_excluirCoordenador_Click);
            // 
            // lbl_medico
            // 
            this.lbl_medico.AutoSize = true;
            this.lbl_medico.Location = new System.Drawing.Point(6, 12);
            this.lbl_medico.Name = "lbl_medico";
            this.lbl_medico.Size = new System.Drawing.Size(106, 13);
            this.lbl_medico.TabIndex = 2;
            this.lbl_medico.Text = "Médico Coordenador";
            // 
            // btn_coordenador
            // 
            this.btn_coordenador.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_coordenador.Location = new System.Drawing.Point(6, 54);
            this.btn_coordenador.Name = "btn_coordenador";
            this.btn_coordenador.Size = new System.Drawing.Size(75, 23);
            this.btn_coordenador.TabIndex = 2;
            this.btn_coordenador.Text = "&Selecionar";
            this.btn_coordenador.UseVisualStyleBackColor = true;
            this.btn_coordenador.Click += new System.EventHandler(this.btn_coordenador_Click);
            // 
            // text_nomeCoordenador
            // 
            this.text_nomeCoordenador.BackColor = System.Drawing.Color.LightGray;
            this.text_nomeCoordenador.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.text_nomeCoordenador.Location = new System.Drawing.Point(6, 28);
            this.text_nomeCoordenador.MaxLength = 200;
            this.text_nomeCoordenador.Name = "text_nomeCoordenador";
            this.text_nomeCoordenador.ReadOnly = true;
            this.text_nomeCoordenador.Size = new System.Drawing.Size(729, 20);
            this.text_nomeCoordenador.TabIndex = 1;
            this.text_nomeCoordenador.TabStop = false;
            // 
            // tab_logo
            // 
            this.tab_logo.BackColor = System.Drawing.Color.White;
            this.tab_logo.Controls.Add(this.lbl_logo);
            this.tab_logo.Controls.Add(this.pic_box_logo);
            this.tab_logo.Controls.Add(this.bt_abrir_file_dialog);
            this.tab_logo.Location = new System.Drawing.Point(4, 22);
            this.tab_logo.Name = "tab_logo";
            this.tab_logo.Padding = new System.Windows.Forms.Padding(3);
            this.tab_logo.Size = new System.Drawing.Size(773, 241);
            this.tab_logo.TabIndex = 5;
            this.tab_logo.Text = "Logo";
            // 
            // lbl_logo
            // 
            this.lbl_logo.AutoSize = true;
            this.lbl_logo.Location = new System.Drawing.Point(129, 93);
            this.lbl_logo.Name = "lbl_logo";
            this.lbl_logo.Size = new System.Drawing.Size(157, 13);
            this.lbl_logo.TabIndex = 2;
            this.lbl_logo.Text = "Tamanho máximo: 100 x 120dpi";
            // 
            // pic_box_logo
            // 
            this.pic_box_logo.BackColor = System.Drawing.Color.White;
            this.pic_box_logo.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pic_box_logo.Location = new System.Drawing.Point(3, 6);
            this.pic_box_logo.Name = "pic_box_logo";
            this.pic_box_logo.Size = new System.Drawing.Size(120, 100);
            this.pic_box_logo.TabIndex = 0;
            this.pic_box_logo.TabStop = false;
            // 
            // bt_abrir_file_dialog
            // 
            this.bt_abrir_file_dialog.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.bt_abrir_file_dialog.Location = new System.Drawing.Point(3, 112);
            this.bt_abrir_file_dialog.Name = "bt_abrir_file_dialog";
            this.bt_abrir_file_dialog.Size = new System.Drawing.Size(77, 23);
            this.bt_abrir_file_dialog.TabIndex = 1;
            this.bt_abrir_file_dialog.Text = "&Abrir";
            this.bt_abrir_file_dialog.UseVisualStyleBackColor = true;
            this.bt_abrir_file_dialog.Click += new System.EventHandler(this.bt_abrir_file_dialog_Click);
            // 
            // tb_funcoes
            // 
            this.tb_funcoes.Controls.Add(this.btn_excluir);
            this.tb_funcoes.Controls.Add(this.btn_incluirFuncao);
            this.tb_funcoes.Controls.Add(this.dg_funcao);
            this.tb_funcoes.Location = new System.Drawing.Point(4, 22);
            this.tb_funcoes.Name = "tb_funcoes";
            this.tb_funcoes.Padding = new System.Windows.Forms.Padding(3);
            this.tb_funcoes.Size = new System.Drawing.Size(773, 241);
            this.tb_funcoes.TabIndex = 6;
            this.tb_funcoes.Text = "Funções";
            this.tb_funcoes.UseVisualStyleBackColor = true;
            // 
            // btn_excluir
            // 
            this.btn_excluir.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_excluir.Location = new System.Drawing.Point(87, 143);
            this.btn_excluir.Name = "btn_excluir";
            this.btn_excluir.Size = new System.Drawing.Size(75, 23);
            this.btn_excluir.TabIndex = 2;
            this.btn_excluir.Text = "Excluir";
            this.btn_excluir.UseVisualStyleBackColor = true;
            this.btn_excluir.Click += new System.EventHandler(this.btn_excluir_Click);
            // 
            // btn_incluirFuncao
            // 
            this.btn_incluirFuncao.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_incluirFuncao.Location = new System.Drawing.Point(6, 143);
            this.btn_incluirFuncao.Name = "btn_incluirFuncao";
            this.btn_incluirFuncao.Size = new System.Drawing.Size(75, 23);
            this.btn_incluirFuncao.TabIndex = 1;
            this.btn_incluirFuncao.Text = "&Selecionar";
            this.btn_incluirFuncao.UseVisualStyleBackColor = true;
            this.btn_incluirFuncao.Click += new System.EventHandler(this.btn_incluirFuncao_Click);
            // 
            // dg_funcao
            // 
            this.dg_funcao.AllowUserToAddRows = false;
            this.dg_funcao.AllowUserToDeleteRows = false;
            this.dg_funcao.AllowUserToResizeRows = false;
            this.dg_funcao.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this.dg_funcao.BackgroundColor = System.Drawing.Color.White;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dg_funcao.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.dg_funcao.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dg_funcao.Location = new System.Drawing.Point(6, 6);
            this.dg_funcao.Name = "dg_funcao";
            this.dg_funcao.ReadOnly = true;
            this.dg_funcao.Size = new System.Drawing.Size(761, 131);
            this.dg_funcao.TabIndex = 0;
            // 
            // tb_estudo
            // 
            this.tb_estudo.Controls.Add(this.text_escopo);
            this.tb_estudo.Controls.Add(this.lbl_escopo);
            this.tb_estudo.Controls.Add(this.text_jornada);
            this.tb_estudo.Controls.Add(this.lbl_jornada);
            this.tb_estudo.Controls.Add(this.text_telefoneContato);
            this.tb_estudo.Controls.Add(this.grb_estimativa);
            this.tb_estudo.Controls.Add(this.lbl_telefoneContato);
            this.tb_estudo.Controls.Add(this.text_documento);
            this.tb_estudo.Controls.Add(this.text_nome);
            this.tb_estudo.Controls.Add(this.lbl_responsavel);
            this.tb_estudo.Controls.Add(this.lbl_documento);
            this.tb_estudo.Controls.Add(this.lbl_cargo);
            this.tb_estudo.Controls.Add(this.text_cargo);
            this.tb_estudo.Location = new System.Drawing.Point(4, 22);
            this.tb_estudo.Name = "tb_estudo";
            this.tb_estudo.Padding = new System.Windows.Forms.Padding(3);
            this.tb_estudo.Size = new System.Drawing.Size(773, 241);
            this.tb_estudo.TabIndex = 7;
            this.tb_estudo.Text = "PPRA / PCMSO";
            this.tb_estudo.UseVisualStyleBackColor = true;
            // 
            // text_escopo
            // 
            this.text_escopo.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.text_escopo.Location = new System.Drawing.Point(4, 198);
            this.text_escopo.MaxLength = 600;
            this.text_escopo.Multiline = true;
            this.text_escopo.Name = "text_escopo";
            this.text_escopo.Size = new System.Drawing.Size(754, 35);
            this.text_escopo.TabIndex = 30;
            // 
            // lbl_escopo
            // 
            this.lbl_escopo.AutoSize = true;
            this.lbl_escopo.Location = new System.Drawing.Point(3, 182);
            this.lbl_escopo.Name = "lbl_escopo";
            this.lbl_escopo.Size = new System.Drawing.Size(105, 13);
            this.lbl_escopo.TabIndex = 31;
            this.lbl_escopo.Text = "Escopo da Atividade";
            // 
            // text_jornada
            // 
            this.text_jornada.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.text_jornada.Location = new System.Drawing.Point(6, 146);
            this.text_jornada.MaxLength = 300;
            this.text_jornada.Multiline = true;
            this.text_jornada.Name = "text_jornada";
            this.text_jornada.Size = new System.Drawing.Size(752, 33);
            this.text_jornada.TabIndex = 28;
            // 
            // lbl_jornada
            // 
            this.lbl_jornada.AutoSize = true;
            this.lbl_jornada.Location = new System.Drawing.Point(3, 130);
            this.lbl_jornada.Name = "lbl_jornada";
            this.lbl_jornada.Size = new System.Drawing.Size(105, 13);
            this.lbl_jornada.TabIndex = 29;
            this.lbl_jornada.Text = "Jornada de Trabalho";
            // 
            // text_telefoneContato
            // 
            this.text_telefoneContato.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.text_telefoneContato.Location = new System.Drawing.Point(333, 107);
            this.text_telefoneContato.MaxLength = 15;
            this.text_telefoneContato.Name = "text_telefoneContato";
            this.text_telefoneContato.Size = new System.Drawing.Size(157, 20);
            this.text_telefoneContato.TabIndex = 22;
            // 
            // grb_estimativa
            // 
            this.grb_estimativa.Controls.Add(this.text_estFem);
            this.grb_estimativa.Controls.Add(this.text_estMasc);
            this.grb_estimativa.Controls.Add(this.lbl_estFem);
            this.grb_estimativa.Controls.Add(this.lbl_estMasc);
            this.grb_estimativa.Location = new System.Drawing.Point(6, 6);
            this.grb_estimativa.Name = "grb_estimativa";
            this.grb_estimativa.Size = new System.Drawing.Size(283, 50);
            this.grb_estimativa.TabIndex = 6;
            this.grb_estimativa.TabStop = false;
            this.grb_estimativa.Text = "Estimativa Funcionários";
            // 
            // text_estFem
            // 
            this.text_estFem.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.text_estFem.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.text_estFem.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.text_estFem.Location = new System.Drawing.Point(198, 19);
            this.text_estFem.MaxLength = 4;
            this.text_estFem.Name = "text_estFem";
            this.text_estFem.Size = new System.Drawing.Size(55, 20);
            this.text_estFem.TabIndex = 7;
            this.text_estFem.Text = "0";
            this.text_estFem.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.text_estFem.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.text_estFem_KeyPress);
            // 
            // text_estMasc
            // 
            this.text_estMasc.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.text_estMasc.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.text_estMasc.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.text_estMasc.Location = new System.Drawing.Point(72, 19);
            this.text_estMasc.MaxLength = 4;
            this.text_estMasc.Name = "text_estMasc";
            this.text_estMasc.Size = new System.Drawing.Size(55, 20);
            this.text_estMasc.TabIndex = 6;
            this.text_estMasc.Text = "0";
            this.text_estMasc.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.text_estMasc.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.text_estMasc_KeyPress);
            // 
            // lbl_estFem
            // 
            this.lbl_estFem.AutoSize = true;
            this.lbl_estFem.Location = new System.Drawing.Point(140, 22);
            this.lbl_estFem.Name = "lbl_estFem";
            this.lbl_estFem.Size = new System.Drawing.Size(52, 13);
            this.lbl_estFem.TabIndex = 2;
            this.lbl_estFem.Text = "Feminino:";
            // 
            // lbl_estMasc
            // 
            this.lbl_estMasc.AutoSize = true;
            this.lbl_estMasc.Location = new System.Drawing.Point(11, 22);
            this.lbl_estMasc.Name = "lbl_estMasc";
            this.lbl_estMasc.Size = new System.Drawing.Size(58, 13);
            this.lbl_estMasc.TabIndex = 0;
            this.lbl_estMasc.Text = "Masculino:";
            // 
            // lbl_telefoneContato
            // 
            this.lbl_telefoneContato.AutoSize = true;
            this.lbl_telefoneContato.Location = new System.Drawing.Point(330, 94);
            this.lbl_telefoneContato.Name = "lbl_telefoneContato";
            this.lbl_telefoneContato.Size = new System.Drawing.Size(49, 13);
            this.lbl_telefoneContato.TabIndex = 27;
            this.lbl_telefoneContato.Text = "Telefone";
            // 
            // text_documento
            // 
            this.text_documento.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.text_documento.Location = new System.Drawing.Point(6, 107);
            this.text_documento.MaxLength = 50;
            this.text_documento.Name = "text_documento";
            this.text_documento.Size = new System.Drawing.Size(318, 20);
            this.text_documento.TabIndex = 21;
            // 
            // text_nome
            // 
            this.text_nome.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.text_nome.Location = new System.Drawing.Point(6, 71);
            this.text_nome.MaxLength = 100;
            this.text_nome.Name = "text_nome";
            this.text_nome.Size = new System.Drawing.Size(484, 20);
            this.text_nome.TabIndex = 19;
            // 
            // lbl_responsavel
            // 
            this.lbl_responsavel.AutoSize = true;
            this.lbl_responsavel.Location = new System.Drawing.Point(3, 58);
            this.lbl_responsavel.Name = "lbl_responsavel";
            this.lbl_responsavel.Size = new System.Drawing.Size(115, 13);
            this.lbl_responsavel.TabIndex = 14;
            this.lbl_responsavel.Text = "Nome do Responsável";
            // 
            // lbl_documento
            // 
            this.lbl_documento.AutoSize = true;
            this.lbl_documento.Location = new System.Drawing.Point(3, 94);
            this.lbl_documento.Name = "lbl_documento";
            this.lbl_documento.Size = new System.Drawing.Size(62, 13);
            this.lbl_documento.TabIndex = 18;
            this.lbl_documento.Text = "Documento";
            // 
            // lbl_cargo
            // 
            this.lbl_cargo.AutoSize = true;
            this.lbl_cargo.Location = new System.Drawing.Point(500, 58);
            this.lbl_cargo.Name = "lbl_cargo";
            this.lbl_cargo.Size = new System.Drawing.Size(35, 13);
            this.lbl_cargo.TabIndex = 16;
            this.lbl_cargo.Text = "Cargo";
            // 
            // text_cargo
            // 
            this.text_cargo.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.text_cargo.Location = new System.Drawing.Point(503, 71);
            this.text_cargo.MaxLength = 50;
            this.text_cargo.Name = "text_cargo";
            this.text_cargo.Size = new System.Drawing.Size(255, 20);
            this.text_cargo.TabIndex = 20;
            // 
            // tp_configuracao
            // 
            this.tp_configuracao.Controls.Add(this.chk_prestador);
            this.tp_configuracao.Controls.Add(this.text_aliquotaIss);
            this.tp_configuracao.Controls.Add(this.lbl_aliquotaIss);
            this.tp_configuracao.Controls.Add(this.chk_geraCobrancaValorLiquido);
            this.tp_configuracao.Controls.Add(this.chk_destacaIss);
            this.tp_configuracao.Controls.Add(this.chk_particular);
            this.tp_configuracao.Controls.Add(this.chk_usaContrato);
            this.tp_configuracao.Controls.Add(this.chk_vip);
            this.tp_configuracao.Location = new System.Drawing.Point(4, 22);
            this.tp_configuracao.Name = "tp_configuracao";
            this.tp_configuracao.Padding = new System.Windows.Forms.Padding(3);
            this.tp_configuracao.Size = new System.Drawing.Size(773, 241);
            this.tp_configuracao.TabIndex = 8;
            this.tp_configuracao.Text = "Configurações";
            this.tp_configuracao.UseVisualStyleBackColor = true;
            // 
            // chk_prestador
            // 
            this.chk_prestador.AutoSize = true;
            this.chk_prestador.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.chk_prestador.Location = new System.Drawing.Point(8, 76);
            this.chk_prestador.Name = "chk_prestador";
            this.chk_prestador.Size = new System.Drawing.Size(118, 17);
            this.chk_prestador.TabIndex = 31;
            this.chk_prestador.Text = "Pode ser prestador?";
            this.tp_clienteIncluir.SetToolTip(this.chk_prestador, "Se marcado, informa ao sistema que o cliente pode ser\r\nprestador de exames e com " +
                    "isso aparecerá das telas onde\r\nexiste busca por prestador.");
            this.chk_prestador.UseVisualStyleBackColor = true;
            // 
            // text_aliquotaIss
            // 
            this.text_aliquotaIss.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.text_aliquotaIss.Location = new System.Drawing.Point(8, 158);
            this.text_aliquotaIss.MaxLength = 11;
            this.text_aliquotaIss.Name = "text_aliquotaIss";
            this.text_aliquotaIss.Size = new System.Drawing.Size(74, 20);
            this.text_aliquotaIss.TabIndex = 30;
            this.text_aliquotaIss.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.tp_clienteIncluir.SetToolTip(this.text_aliquotaIss, "Aliquota de ISS informada pelo cliente.");
            this.text_aliquotaIss.Enter += new System.EventHandler(this.text_aliquotaIss_Enter);
            this.text_aliquotaIss.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.text_aliquotaIss_KeyPress);
            this.text_aliquotaIss.Leave += new System.EventHandler(this.text_aliquotaIss_Leave);
            // 
            // lbl_aliquotaIss
            // 
            this.lbl_aliquotaIss.AutoSize = true;
            this.lbl_aliquotaIss.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.lbl_aliquotaIss.Location = new System.Drawing.Point(7, 142);
            this.lbl_aliquotaIss.Name = "lbl_aliquotaIss";
            this.lbl_aliquotaIss.Size = new System.Drawing.Size(82, 13);
            this.lbl_aliquotaIss.TabIndex = 29;
            this.lbl_aliquotaIss.Text = "Alíquota de ISS";
            // 
            // chk_geraCobrancaValorLiquido
            // 
            this.chk_geraCobrancaValorLiquido.AutoSize = true;
            this.chk_geraCobrancaValorLiquido.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.chk_geraCobrancaValorLiquido.Location = new System.Drawing.Point(8, 122);
            this.chk_geraCobrancaValorLiquido.Name = "chk_geraCobrancaValorLiquido";
            this.chk_geraCobrancaValorLiquido.Size = new System.Drawing.Size(264, 17);
            this.chk_geraCobrancaValorLiquido.TabIndex = 28;
            this.chk_geraCobrancaValorLiquido.Text = "Gerar Cobrança com Valor Líquido de Nota Fiscal?";
            this.tp_clienteIncluir.SetToolTip(this.chk_geraCobrancaValorLiquido, "Esse parâmetro indica ao sistema para gerar a cobrança\r\nem cima do valor líquido " +
                    "da nota fiscal.");
            this.chk_geraCobrancaValorLiquido.UseVisualStyleBackColor = true;
            // 
            // chk_destacaIss
            // 
            this.chk_destacaIss.AutoSize = true;
            this.chk_destacaIss.Checked = true;
            this.chk_destacaIss.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chk_destacaIss.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.chk_destacaIss.Location = new System.Drawing.Point(8, 99);
            this.chk_destacaIss.Name = "chk_destacaIss";
            this.chk_destacaIss.Size = new System.Drawing.Size(155, 17);
            this.chk_destacaIss.TabIndex = 27;
            this.chk_destacaIss.Text = "Destaca ISS na nota fiscal?";
            this.tp_clienteIncluir.SetToolTip(this.chk_destacaIss, "Esse parametro faz com que o Valor do Iss seja\r\ndestacado em Nota Fiscal, permiti" +
                    "ndo que seja debitado no \r\nvalor líquido da nota fiscal.");
            this.chk_destacaIss.UseVisualStyleBackColor = true;
            this.chk_destacaIss.CheckStateChanged += new System.EventHandler(this.chk_destacaIss_CheckStateChanged);
            // 
            // chk_particular
            // 
            this.chk_particular.AutoSize = true;
            this.chk_particular.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.chk_particular.Location = new System.Drawing.Point(8, 53);
            this.chk_particular.Name = "chk_particular";
            this.chk_particular.Size = new System.Drawing.Size(107, 17);
            this.chk_particular.TabIndex = 25;
            this.chk_particular.Text = "Cliente particular?";
            this.chk_particular.UseVisualStyleBackColor = true;
            // 
            // chk_usaContrato
            // 
            this.chk_usaContrato.AutoSize = true;
            this.chk_usaContrato.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.chk_usaContrato.Location = new System.Drawing.Point(8, 30);
            this.chk_usaContrato.Name = "chk_usaContrato";
            this.chk_usaContrato.Size = new System.Drawing.Size(126, 17);
            this.chk_usaContrato.TabIndex = 24;
            this.chk_usaContrato.Text = "Cliente Usa Contrato?";
            this.chk_usaContrato.UseVisualStyleBackColor = true;
            // 
            // chk_vip
            // 
            this.chk_vip.AutoSize = true;
            this.chk_vip.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.chk_vip.Location = new System.Drawing.Point(8, 7);
            this.chk_vip.Name = "chk_vip";
            this.chk_vip.Size = new System.Drawing.Size(81, 17);
            this.chk_vip.TabIndex = 23;
            this.chk_vip.Text = "Cliente VIP?";
            this.chk_vip.UseVisualStyleBackColor = true;
            // 
            // grb_paginacao
            // 
            this.grb_paginacao.BackColor = System.Drawing.Color.White;
            this.grb_paginacao.Controls.Add(this.lbl_limpar);
            this.grb_paginacao.Controls.Add(this.btn_fechar);
            this.grb_paginacao.Controls.Add(this.btn_gravar);
            this.grb_paginacao.Location = new System.Drawing.Point(3, 464);
            this.grb_paginacao.Name = "grb_paginacao";
            this.grb_paginacao.Size = new System.Drawing.Size(781, 51);
            this.grb_paginacao.TabIndex = 23;
            this.grb_paginacao.TabStop = false;
            // 
            // lbl_limpar
            // 
            this.lbl_limpar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.lbl_limpar.Image = global::SegSis.Properties.Resources.vassoura;
            this.lbl_limpar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.lbl_limpar.Location = new System.Drawing.Point(89, 22);
            this.lbl_limpar.Name = "lbl_limpar";
            this.lbl_limpar.Size = new System.Drawing.Size(77, 23);
            this.lbl_limpar.TabIndex = 25;
            this.lbl_limpar.Text = "&Limpar";
            this.lbl_limpar.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.lbl_limpar.UseVisualStyleBackColor = true;
            this.lbl_limpar.Click += new System.EventHandler(this.lbl_limpar_Click);
            // 
            // btn_fechar
            // 
            this.btn_fechar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_fechar.Image = global::SegSis.Properties.Resources.close;
            this.btn_fechar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btn_fechar.Location = new System.Drawing.Point(172, 22);
            this.btn_fechar.Name = "btn_fechar";
            this.btn_fechar.Size = new System.Drawing.Size(77, 23);
            this.btn_fechar.TabIndex = 26;
            this.btn_fechar.Text = "&Fechar";
            this.btn_fechar.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btn_fechar.UseVisualStyleBackColor = true;
            this.btn_fechar.Click += new System.EventHandler(this.btn_fechar_Click);
            // 
            // btn_gravar
            // 
            this.btn_gravar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_gravar.Image = global::SegSis.Properties.Resources.fechar_ico;
            this.btn_gravar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btn_gravar.Location = new System.Drawing.Point(6, 22);
            this.btn_gravar.Name = "btn_gravar";
            this.btn_gravar.Size = new System.Drawing.Size(77, 23);
            this.btn_gravar.TabIndex = 24;
            this.btn_gravar.Text = "&Gravar";
            this.btn_gravar.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btn_gravar.UseVisualStyleBackColor = true;
            this.btn_gravar.Click += new System.EventHandler(this.btn_gravar_Click);
            // 
            // tp_clienteIncluir
            // 
            this.tp_clienteIncluir.ToolTipIcon = System.Windows.Forms.ToolTipIcon.Info;
            // 
            // frm_cliente_inclui
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(794, 568);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.KeyPreview = true;
            this.Name = "frm_cliente_inclui";
            this.Text = "INCLUIR CLIENTE";
            this.Load += new System.EventHandler(this.frm_cliente_inclui_Load);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.frm_cliente_inclui_KeyDown);
            this.panel_banner.ResumeLayout(false);
            this.panel_banner.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox_banner)).EndInit();
            this.panel.ResumeLayout(false);
            this.grb_dados.ResumeLayout(false);
            this.grb_dados.PerformLayout();
            this.gb_credenciada.ResumeLayout(false);
            this.gb_credenciada.PerformLayout();
            this.gb_Matriz.ResumeLayout(false);
            this.gb_Matriz.PerformLayout();
            this.grb_tipoCliente.ResumeLayout(false);
            this.grb_vendedor.ResumeLayout(false);
            this.grb_vendedor.PerformLayout();
            this.tb_paginacao.ResumeLayout(false);
            this.tb_endereco.ResumeLayout(false);
            this.tb_endereco.PerformLayout();
            this.tb_enderecoCob.ResumeLayout(false);
            this.tb_enderecoCob.PerformLayout();
            this.tb_cnae.ResumeLayout(false);
            this.grb_cnaePrincipal.ResumeLayout(false);
            this.grb_cnaePrincipal.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grd_cnae)).EndInit();
            this.tb_medico.ResumeLayout(false);
            this.tb_medico.PerformLayout();
            this.tab_logo.ResumeLayout(false);
            this.tab_logo.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pic_box_logo)).EndInit();
            this.tb_funcoes.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dg_funcao)).EndInit();
            this.tb_estudo.ResumeLayout(false);
            this.tb_estudo.PerformLayout();
            this.grb_estimativa.ResumeLayout(false);
            this.grb_estimativa.PerformLayout();
            this.tp_configuracao.ResumeLayout(false);
            this.tp_configuracao.PerformLayout();
            this.grb_paginacao.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TextBox text_inscricao;
        private System.Windows.Forms.TextBox text_fantasia;
        private System.Windows.Forms.TextBox text_razaoSocial;
        private System.Windows.Forms.MaskedTextBox text_cnpj;
        private System.Windows.Forms.MaskedTextBox text_cep;
        private System.Windows.Forms.Label lbl_razaoSocial;
        private System.Windows.Forms.Label lbl_inscricao;
        private System.Windows.Forms.Label lbl_cnpj;
        private System.Windows.Forms.Label lbl_fantasia;
        private System.Windows.Forms.Label lbl_telefone;
        private System.Windows.Forms.Label lbl_endereco;
        private System.Windows.Forms.GroupBox grb_dados;
        private System.Windows.Forms.TabControl tb_paginacao;
        private System.Windows.Forms.TabPage tb_endereco;
        private System.Windows.Forms.TabPage tb_cnae;
        private System.Windows.Forms.GroupBox grb_paginacao;
        private System.Windows.Forms.Button lbl_limpar;
        private System.Windows.Forms.Button btn_fechar;
        private System.Windows.Forms.Button btn_gravar;
        private System.Windows.Forms.Label lbl_cep;
        private System.Windows.Forms.Label lbl_cidade;
        private System.Windows.Forms.TextBox text_bairro;
        private System.Windows.Forms.Label lbl_bairro;
        private System.Windows.Forms.TextBox text_complemento;
        private System.Windows.Forms.Label lbl_complemento;
        private System.Windows.Forms.TextBox text_numero;
        private System.Windows.Forms.Label lbl_numero;
        private System.Windows.Forms.TextBox text_endereco;
        private System.Windows.Forms.ComboBox cb_cidade;
        private System.Windows.Forms.Label lbl_fax;
        private System.Windows.Forms.TextBox text_site;
        private System.Windows.Forms.Label lbl_site;
        private System.Windows.Forms.TextBox text_email;
        private System.Windows.Forms.Label lbl_email;
        private System.Windows.Forms.Button btn_vendedor;
        private System.Windows.Forms.GroupBox grb_vendedor;
        public System.Windows.Forms.TextBox text_vendedor;
        private System.Windows.Forms.TextBox text_documento;
        private System.Windows.Forms.Label lbl_documento;
        private System.Windows.Forms.TextBox text_cargo;
        private System.Windows.Forms.Label lbl_cargo;
        private System.Windows.Forms.TextBox text_nome;
        private System.Windows.Forms.Label lbl_responsavel;
        private System.Windows.Forms.Button btn_excluiCnae;
        private System.Windows.Forms.Button btn_incluiCnae;
        private System.Windows.Forms.DataGridView grd_cnae;
        private System.Windows.Forms.TextBox text_telefone2;
        private System.Windows.Forms.TextBox text_telefone1;
        private System.Windows.Forms.GroupBox grb_estimativa;
        private System.Windows.Forms.Label lbl_estFem;
        private System.Windows.Forms.Label lbl_estMasc;
        private System.Windows.Forms.GroupBox grb_cnaePrincipal;
        private System.Windows.Forms.Button btn_incluir;
        public System.Windows.Forms.MaskedTextBox text_cnaePrincipal;
        private System.Windows.Forms.TextBox text_estMasc;
        private System.Windows.Forms.TextBox text_estFem;
        private System.Windows.Forms.TabPage tb_enderecoCob;
        private System.Windows.Forms.TextBox text_telefoneCob;
        private System.Windows.Forms.Label lbl_telefoneCob;
        private System.Windows.Forms.MaskedTextBox text_cepCob;
        private System.Windows.Forms.Label lbl_cepCob;
        private System.Windows.Forms.Label lbl_cidadeCob;
        private System.Windows.Forms.TextBox text_bairroCob;
        private System.Windows.Forms.Label lbl_bairroCob;
        private System.Windows.Forms.TextBox text_complementoCob;
        private System.Windows.Forms.Label lbl_complementoCob;
        private System.Windows.Forms.TextBox text_numeroCob;
        private System.Windows.Forms.Label lbl_numeroCob;
        private System.Windows.Forms.TextBox text_enderecoCob;
        private System.Windows.Forms.Label lbl_enderecoCob;
        private System.Windows.Forms.Button btn_copiar;
        private System.Windows.Forms.TabPage tb_medico;
        private System.Windows.Forms.Button btn_coordenador;
        public System.Windows.Forms.TextBox text_nomeCoordenador;
        private System.Windows.Forms.Button btn_excluirCoordenador;
        private System.Windows.Forms.TabPage tab_logo;
        private System.Windows.Forms.PictureBox pic_box_logo;
        private System.Windows.Forms.Button bt_abrir_file_dialog;
        private System.Windows.Forms.TextBox text_faxCobranca;
        private System.Windows.Forms.Label lbl_faxCobranca;
        private System.Windows.Forms.CheckBox chk_vip;
        private System.Windows.Forms.TabPage tb_funcoes;
        private System.Windows.Forms.Button btn_excluir;
        private System.Windows.Forms.Button btn_incluirFuncao;
        private System.Windows.Forms.DataGridView dg_funcao;
        private System.Windows.Forms.TextBox text_telefoneContato;
        private System.Windows.Forms.Label lbl_telefoneContato;
        private System.Windows.Forms.CheckBox chk_usaContrato;
        private System.Windows.Forms.TabPage tb_estudo;
        private System.Windows.Forms.TextBox text_escopo;
        private System.Windows.Forms.Label lbl_escopo;
        private System.Windows.Forms.TextBox text_jornada;
        private System.Windows.Forms.Label lbl_jornada;
        private System.Windows.Forms.TabPage tp_configuracao;
        private System.Windows.Forms.Label lbl_medico;
        private System.Windows.Forms.Label lbl_logo;
        private System.Windows.Forms.GroupBox grb_tipoCliente;
        private System.Windows.Forms.ComboBox cb_tipoCliente;
        private System.Windows.Forms.GroupBox gb_credenciada;
        private System.Windows.Forms.TextBox text_credenciada;
        private System.Windows.Forms.Button btn_credenciada;
        private System.Windows.Forms.GroupBox gb_Matriz;
        private System.Windows.Forms.TextBox text_matriz;
        private System.Windows.Forms.Button btn_matiz;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.CheckBox chk_particular;
        private System.Windows.Forms.Label lbl_uf;
        private System.Windows.Forms.ComboBox cb_uf;
        private System.Windows.Forms.ComboBox cb_cidadeCobranca;
        private System.Windows.Forms.ComboBox cb_ufCob;
        private System.Windows.Forms.Label lbl_ufCobranca;
        private System.Windows.Forms.TextBox text_aliquotaIss;
        private System.Windows.Forms.ToolTip tp_clienteIncluir;
        private System.Windows.Forms.Label lbl_aliquotaIss;
        private System.Windows.Forms.CheckBox chk_geraCobrancaValorLiquido;
        private System.Windows.Forms.CheckBox chk_destacaIss;
        private System.Windows.Forms.CheckBox chk_prestador;

    }
}