﻿namespace SWS.View
{
    partial class frmRelGestaoCredenciada
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.flpAcao = new System.Windows.Forms.FlowLayoutPanel();
            this.btnImprimir = new System.Windows.Forms.Button();
            this.btnFechar = new System.Windows.Forms.Button();
            this.lblCredenciadora = new System.Windows.Forms.TextBox();
            this.textCredenciadora = new System.Windows.Forms.TextBox();
            this.btnCredenciadora = new System.Windows.Forms.Button();
            this.btnExcluirCredenciadora = new System.Windows.Forms.Button();
            this.lblCredenciada = new System.Windows.Forms.TextBox();
            this.lblPeriodoMovimento = new System.Windows.Forms.TextBox();
            this.dataInicial = new System.Windows.Forms.DateTimePicker();
            this.dataFinal = new System.Windows.Forms.DateTimePicker();
            this.lblSituacao = new System.Windows.Forms.TextBox();
            this.cbSituacao = new SWS.ComboBoxWithBorder();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.dtFinalAtendimento = new System.Windows.Forms.DateTimePicker();
            this.dtInicialAtendimento = new System.Windows.Forms.DateTimePicker();
            this.btnExcluirCredenciada = new System.Windows.Forms.Button();
            this.btnCredenciada = new System.Windows.Forms.Button();
            this.textCredenciada = new System.Windows.Forms.TextBox();
            ((System.ComponentModel.ISupportInitialize)(this.scForm)).BeginInit();
            this.scForm.Panel1.SuspendLayout();
            this.scForm.Panel2.SuspendLayout();
            this.scForm.SuspendLayout();
            this.pnlForm.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.banner)).BeginInit();
            this.flpAcao.SuspendLayout();
            this.SuspendLayout();
            // 
            // scForm
            // 
            // 
            // scForm.Panel1
            // 
            this.scForm.Panel1.Controls.Add(this.flpAcao);
            // 
            // pnlForm
            // 
            this.pnlForm.Controls.Add(this.btnExcluirCredenciada);
            this.pnlForm.Controls.Add(this.btnCredenciada);
            this.pnlForm.Controls.Add(this.textCredenciada);
            this.pnlForm.Controls.Add(this.textBox1);
            this.pnlForm.Controls.Add(this.dtFinalAtendimento);
            this.pnlForm.Controls.Add(this.dtInicialAtendimento);
            this.pnlForm.Controls.Add(this.lblPeriodoMovimento);
            this.pnlForm.Controls.Add(this.cbSituacao);
            this.pnlForm.Controls.Add(this.lblSituacao);
            this.pnlForm.Controls.Add(this.dataFinal);
            this.pnlForm.Controls.Add(this.dataInicial);
            this.pnlForm.Controls.Add(this.lblCredenciada);
            this.pnlForm.Controls.Add(this.btnExcluirCredenciadora);
            this.pnlForm.Controls.Add(this.btnCredenciadora);
            this.pnlForm.Controls.Add(this.textCredenciadora);
            this.pnlForm.Controls.Add(this.lblCredenciadora);
            // 
            // flpAcao
            // 
            this.flpAcao.Controls.Add(this.btnImprimir);
            this.flpAcao.Controls.Add(this.btnFechar);
            this.flpAcao.Dock = System.Windows.Forms.DockStyle.Fill;
            this.flpAcao.Location = new System.Drawing.Point(0, 0);
            this.flpAcao.Name = "flpAcao";
            this.flpAcao.Size = new System.Drawing.Size(534, 30);
            this.flpAcao.TabIndex = 0;
            // 
            // btnImprimir
            // 
            this.btnImprimir.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnImprimir.Image = global::SWS.Properties.Resources.impressora1;
            this.btnImprimir.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnImprimir.Location = new System.Drawing.Point(3, 3);
            this.btnImprimir.Name = "btnImprimir";
            this.btnImprimir.Size = new System.Drawing.Size(75, 23);
            this.btnImprimir.TabIndex = 2;
            this.btnImprimir.Text = "&Imprimir";
            this.btnImprimir.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnImprimir.UseVisualStyleBackColor = true;
            this.btnImprimir.Click += new System.EventHandler(this.btnImprimir_Click);
            // 
            // btnFechar
            // 
            this.btnFechar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnFechar.Image = global::SWS.Properties.Resources.close;
            this.btnFechar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnFechar.Location = new System.Drawing.Point(84, 3);
            this.btnFechar.Name = "btnFechar";
            this.btnFechar.Size = new System.Drawing.Size(75, 23);
            this.btnFechar.TabIndex = 3;
            this.btnFechar.Text = "&Fechar";
            this.btnFechar.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnFechar.UseVisualStyleBackColor = true;
            this.btnFechar.Click += new System.EventHandler(this.btnFechar_Click);
            // 
            // lblCredenciadora
            // 
            this.lblCredenciadora.BackColor = System.Drawing.Color.LightGray;
            this.lblCredenciadora.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblCredenciadora.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCredenciadora.Location = new System.Drawing.Point(9, 9);
            this.lblCredenciadora.Name = "lblCredenciadora";
            this.lblCredenciadora.ReadOnly = true;
            this.lblCredenciadora.Size = new System.Drawing.Size(96, 21);
            this.lblCredenciadora.TabIndex = 0;
            this.lblCredenciadora.TabStop = false;
            this.lblCredenciadora.Text = "Credenciadora";
            // 
            // textCredenciadora
            // 
            this.textCredenciadora.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.textCredenciadora.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textCredenciadora.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textCredenciadora.Location = new System.Drawing.Point(104, 9);
            this.textCredenciadora.Name = "textCredenciadora";
            this.textCredenciadora.ReadOnly = true;
            this.textCredenciadora.Size = new System.Drawing.Size(341, 21);
            this.textCredenciadora.TabIndex = 1;
            this.textCredenciadora.TabStop = false;
            // 
            // btnCredenciadora
            // 
            this.btnCredenciadora.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnCredenciadora.Image = global::SWS.Properties.Resources.busca;
            this.btnCredenciadora.Location = new System.Drawing.Point(444, 9);
            this.btnCredenciadora.Name = "btnCredenciadora";
            this.btnCredenciadora.Size = new System.Drawing.Size(34, 21);
            this.btnCredenciadora.TabIndex = 1;
            this.btnCredenciadora.UseVisualStyleBackColor = true;
            this.btnCredenciadora.Click += new System.EventHandler(this.btnCredenciadora_Click);
            // 
            // btnExcluirCredenciadora
            // 
            this.btnExcluirCredenciadora.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnExcluirCredenciadora.Image = global::SWS.Properties.Resources.close;
            this.btnExcluirCredenciadora.Location = new System.Drawing.Point(477, 9);
            this.btnExcluirCredenciadora.Name = "btnExcluirCredenciadora";
            this.btnExcluirCredenciadora.Size = new System.Drawing.Size(34, 21);
            this.btnExcluirCredenciadora.TabIndex = 3;
            this.btnExcluirCredenciadora.TabStop = false;
            this.btnExcluirCredenciadora.UseVisualStyleBackColor = true;
            this.btnExcluirCredenciadora.Click += new System.EventHandler(this.btnExcluirCredenciadora_Click);
            // 
            // lblCredenciada
            // 
            this.lblCredenciada.BackColor = System.Drawing.Color.LightGray;
            this.lblCredenciada.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblCredenciada.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCredenciada.Location = new System.Drawing.Point(9, 29);
            this.lblCredenciada.Name = "lblCredenciada";
            this.lblCredenciada.ReadOnly = true;
            this.lblCredenciada.Size = new System.Drawing.Size(96, 21);
            this.lblCredenciada.TabIndex = 4;
            this.lblCredenciada.TabStop = false;
            this.lblCredenciada.Text = "Credenciada(s)";
            // 
            // lblPeriodoMovimento
            // 
            this.lblPeriodoMovimento.BackColor = System.Drawing.Color.LightGray;
            this.lblPeriodoMovimento.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblPeriodoMovimento.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPeriodoMovimento.Location = new System.Drawing.Point(9, 69);
            this.lblPeriodoMovimento.Name = "lblPeriodoMovimento";
            this.lblPeriodoMovimento.ReadOnly = true;
            this.lblPeriodoMovimento.Size = new System.Drawing.Size(96, 21);
            this.lblPeriodoMovimento.TabIndex = 5;
            this.lblPeriodoMovimento.TabStop = false;
            this.lblPeriodoMovimento.Text = "Dt Movimento";
            // 
            // dataInicial
            // 
            this.dataInicial.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dataInicial.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dataInicial.Location = new System.Drawing.Point(104, 69);
            this.dataInicial.Name = "dataInicial";
            this.dataInicial.ShowCheckBox = true;
            this.dataInicial.Size = new System.Drawing.Size(120, 21);
            this.dataInicial.TabIndex = 6;
            // 
            // dataFinal
            // 
            this.dataFinal.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dataFinal.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dataFinal.Location = new System.Drawing.Point(223, 69);
            this.dataFinal.Name = "dataFinal";
            this.dataFinal.ShowCheckBox = true;
            this.dataFinal.Size = new System.Drawing.Size(120, 21);
            this.dataFinal.TabIndex = 7;
            // 
            // lblSituacao
            // 
            this.lblSituacao.BackColor = System.Drawing.Color.LightGray;
            this.lblSituacao.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblSituacao.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSituacao.Location = new System.Drawing.Point(9, 49);
            this.lblSituacao.Name = "lblSituacao";
            this.lblSituacao.ReadOnly = true;
            this.lblSituacao.Size = new System.Drawing.Size(96, 21);
            this.lblSituacao.TabIndex = 8;
            this.lblSituacao.TabStop = false;
            this.lblSituacao.Text = "Situação";
            // 
            // cbSituacao
            // 
            this.cbSituacao.BackColor = System.Drawing.Color.LightGray;
            this.cbSituacao.BorderColor = System.Drawing.Color.DimGray;
            this.cbSituacao.BorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid;
            this.cbSituacao.FormattingEnabled = true;
            this.cbSituacao.Location = new System.Drawing.Point(104, 49);
            this.cbSituacao.Name = "cbSituacao";
            this.cbSituacao.Size = new System.Drawing.Size(407, 21);
            this.cbSituacao.TabIndex = 9;
            // 
            // textBox1
            // 
            this.textBox1.BackColor = System.Drawing.Color.LightGray;
            this.textBox1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox1.Location = new System.Drawing.Point(9, 89);
            this.textBox1.Name = "textBox1";
            this.textBox1.ReadOnly = true;
            this.textBox1.Size = new System.Drawing.Size(96, 21);
            this.textBox1.TabIndex = 11;
            this.textBox1.TabStop = false;
            this.textBox1.Text = "Dt Atend.";
            // 
            // dtFinalAtendimento
            // 
            this.dtFinalAtendimento.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtFinalAtendimento.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtFinalAtendimento.Location = new System.Drawing.Point(223, 89);
            this.dtFinalAtendimento.Name = "dtFinalAtendimento";
            this.dtFinalAtendimento.ShowCheckBox = true;
            this.dtFinalAtendimento.Size = new System.Drawing.Size(120, 21);
            this.dtFinalAtendimento.TabIndex = 13;
            // 
            // dtInicialAtendimento
            // 
            this.dtInicialAtendimento.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtInicialAtendimento.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtInicialAtendimento.Location = new System.Drawing.Point(104, 89);
            this.dtInicialAtendimento.Name = "dtInicialAtendimento";
            this.dtInicialAtendimento.ShowCheckBox = true;
            this.dtInicialAtendimento.Size = new System.Drawing.Size(120, 21);
            this.dtInicialAtendimento.TabIndex = 12;
            // 
            // btnExcluirCredenciada
            // 
            this.btnExcluirCredenciada.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnExcluirCredenciada.Image = global::SWS.Properties.Resources.close;
            this.btnExcluirCredenciada.Location = new System.Drawing.Point(477, 29);
            this.btnExcluirCredenciada.Name = "btnExcluirCredenciada";
            this.btnExcluirCredenciada.Size = new System.Drawing.Size(34, 21);
            this.btnExcluirCredenciada.TabIndex = 16;
            this.btnExcluirCredenciada.TabStop = false;
            this.btnExcluirCredenciada.UseVisualStyleBackColor = true;
            this.btnExcluirCredenciada.Click += new System.EventHandler(this.btnExcluirCredenciada_Click);
            // 
            // btnCredenciada
            // 
            this.btnCredenciada.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnCredenciada.Image = global::SWS.Properties.Resources.busca;
            this.btnCredenciada.Location = new System.Drawing.Point(444, 29);
            this.btnCredenciada.Name = "btnCredenciada";
            this.btnCredenciada.Size = new System.Drawing.Size(34, 21);
            this.btnCredenciada.TabIndex = 14;
            this.btnCredenciada.UseVisualStyleBackColor = true;
            this.btnCredenciada.Click += new System.EventHandler(this.btnCredenciada_Click);
            // 
            // textCredenciada
            // 
            this.textCredenciada.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.textCredenciada.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textCredenciada.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textCredenciada.Location = new System.Drawing.Point(104, 29);
            this.textCredenciada.Name = "textCredenciada";
            this.textCredenciada.ReadOnly = true;
            this.textCredenciada.Size = new System.Drawing.Size(341, 21);
            this.textCredenciada.TabIndex = 15;
            this.textCredenciada.TabStop = false;
            // 
            // frmRelGestaoCredenciada
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(534, 361);
            this.Name = "frmRelGestaoCredenciada";
            this.Text = "GESTÃO DE CREDENCIADA";
            this.scForm.Panel1.ResumeLayout(false);
            this.scForm.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.scForm)).EndInit();
            this.scForm.ResumeLayout(false);
            this.pnlForm.ResumeLayout(false);
            this.pnlForm.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.banner)).EndInit();
            this.flpAcao.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.FlowLayoutPanel flpAcao;
        private System.Windows.Forms.Button btnImprimir;
        private System.Windows.Forms.Button btnFechar;
        private System.Windows.Forms.Button btnCredenciadora;
        private System.Windows.Forms.TextBox textCredenciadora;
        private System.Windows.Forms.TextBox lblCredenciadora;
        private System.Windows.Forms.Button btnExcluirCredenciadora;
        private System.Windows.Forms.TextBox lblCredenciada;
        private System.Windows.Forms.DateTimePicker dataFinal;
        private System.Windows.Forms.DateTimePicker dataInicial;
        private System.Windows.Forms.TextBox lblPeriodoMovimento;
        private ComboBoxWithBorder cbSituacao;
        private System.Windows.Forms.TextBox lblSituacao;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.DateTimePicker dtFinalAtendimento;
        private System.Windows.Forms.DateTimePicker dtInicialAtendimento;
        private System.Windows.Forms.Button btnExcluirCredenciada;
        private System.Windows.Forms.Button btnCredenciada;
        private System.Windows.Forms.TextBox textCredenciada;
    }
}