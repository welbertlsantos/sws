﻿namespace SWS.View
{
    partial class frmContatoIncluir
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.flpAcao = new System.Windows.Forms.FlowLayoutPanel();
            this.btGravar = new System.Windows.Forms.Button();
            this.btLimpar = new System.Windows.Forms.Button();
            this.btFechar = new System.Windows.Forms.Button();
            this.textEmail = new System.Windows.Forms.TextBox();
            this.lblEmail = new System.Windows.Forms.TextBox();
            this.textPis = new System.Windows.Forms.MaskedTextBox();
            this.lblPIS = new System.Windows.Forms.TextBox();
            this.textCelular = new System.Windows.Forms.TextBox();
            this.lblCelular = new System.Windows.Forms.TextBox();
            this.textTelefone = new System.Windows.Forms.TextBox();
            this.lblTelefone = new System.Windows.Forms.TextBox();
            this.textCEP = new System.Windows.Forms.MaskedTextBox();
            this.lblCEP = new System.Windows.Forms.TextBox();
            this.btCidade = new System.Windows.Forms.Button();
            this.textCidade = new System.Windows.Forms.TextBox();
            this.lblCidade = new System.Windows.Forms.TextBox();
            this.cbUF = new SWS.ComboBoxWithBorder();
            this.lblUf = new System.Windows.Forms.TextBox();
            this.textBairro = new System.Windows.Forms.TextBox();
            this.lblBairro = new System.Windows.Forms.TextBox();
            this.textComplemento = new System.Windows.Forms.TextBox();
            this.lblComplemento = new System.Windows.Forms.TextBox();
            this.textNumero = new System.Windows.Forms.TextBox();
            this.lblNumero = new System.Windows.Forms.TextBox();
            this.textEndereco = new System.Windows.Forms.TextBox();
            this.lblEndereco = new System.Windows.Forms.TextBox();
            this.lblDataNascimento = new System.Windows.Forms.TextBox();
            this.lblRG = new System.Windows.Forms.TextBox();
            this.textRG = new System.Windows.Forms.TextBox();
            this.textCPF = new System.Windows.Forms.MaskedTextBox();
            this.lblCPF = new System.Windows.Forms.TextBox();
            this.textNome = new System.Windows.Forms.TextBox();
            this.lblNome = new System.Windows.Forms.TextBox();
            this.cbTipoContato = new SWS.ComboBoxWithBorder();
            this.lblTipoContato = new System.Windows.Forms.TextBox();
            this.dataNascimento = new System.Windows.Forms.DateTimePicker();
            this.lblResponsavel = new System.Windows.Forms.TextBox();
            this.cbResponsavel = new SWS.ComboBoxWithBorder();
            this.cbResponsavelEstudo = new SWS.ComboBoxWithBorder();
            this.lblResponsavelEstudo = new System.Windows.Forms.TextBox();
            ((System.ComponentModel.ISupportInitialize)(this.scForm)).BeginInit();
            this.scForm.Panel1.SuspendLayout();
            this.scForm.Panel2.SuspendLayout();
            this.scForm.SuspendLayout();
            this.pnlForm.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.banner)).BeginInit();
            this.flpAcao.SuspendLayout();
            this.SuspendLayout();
            // 
            // scForm
            // 
            // 
            // scForm.Panel1
            // 
            this.scForm.Panel1.Controls.Add(this.flpAcao);
            // 
            // pnlForm
            // 
            this.pnlForm.Controls.Add(this.cbResponsavelEstudo);
            this.pnlForm.Controls.Add(this.lblResponsavelEstudo);
            this.pnlForm.Controls.Add(this.lblDataNascimento);
            this.pnlForm.Controls.Add(this.cbResponsavel);
            this.pnlForm.Controls.Add(this.lblResponsavel);
            this.pnlForm.Controls.Add(this.dataNascimento);
            this.pnlForm.Controls.Add(this.textEmail);
            this.pnlForm.Controls.Add(this.lblEmail);
            this.pnlForm.Controls.Add(this.textPis);
            this.pnlForm.Controls.Add(this.lblPIS);
            this.pnlForm.Controls.Add(this.textCelular);
            this.pnlForm.Controls.Add(this.lblCelular);
            this.pnlForm.Controls.Add(this.textTelefone);
            this.pnlForm.Controls.Add(this.lblTelefone);
            this.pnlForm.Controls.Add(this.textCEP);
            this.pnlForm.Controls.Add(this.lblCEP);
            this.pnlForm.Controls.Add(this.btCidade);
            this.pnlForm.Controls.Add(this.textCidade);
            this.pnlForm.Controls.Add(this.lblCidade);
            this.pnlForm.Controls.Add(this.cbUF);
            this.pnlForm.Controls.Add(this.lblUf);
            this.pnlForm.Controls.Add(this.textBairro);
            this.pnlForm.Controls.Add(this.lblBairro);
            this.pnlForm.Controls.Add(this.textComplemento);
            this.pnlForm.Controls.Add(this.lblComplemento);
            this.pnlForm.Controls.Add(this.textNumero);
            this.pnlForm.Controls.Add(this.lblNumero);
            this.pnlForm.Controls.Add(this.textEndereco);
            this.pnlForm.Controls.Add(this.lblEndereco);
            this.pnlForm.Controls.Add(this.lblRG);
            this.pnlForm.Controls.Add(this.textRG);
            this.pnlForm.Controls.Add(this.textCPF);
            this.pnlForm.Controls.Add(this.lblCPF);
            this.pnlForm.Controls.Add(this.textNome);
            this.pnlForm.Controls.Add(this.lblNome);
            this.pnlForm.Controls.Add(this.cbTipoContato);
            this.pnlForm.Controls.Add(this.lblTipoContato);
            this.pnlForm.Size = new System.Drawing.Size(514, 387);
            // 
            // flpAcao
            // 
            this.flpAcao.Controls.Add(this.btGravar);
            this.flpAcao.Controls.Add(this.btLimpar);
            this.flpAcao.Controls.Add(this.btFechar);
            this.flpAcao.Dock = System.Windows.Forms.DockStyle.Fill;
            this.flpAcao.Location = new System.Drawing.Point(0, 0);
            this.flpAcao.Name = "flpAcao";
            this.flpAcao.Size = new System.Drawing.Size(534, 30);
            this.flpAcao.TabIndex = 0;
            // 
            // btGravar
            // 
            this.btGravar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btGravar.Image = global::SWS.Properties.Resources.Gravar;
            this.btGravar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btGravar.Location = new System.Drawing.Point(3, 3);
            this.btGravar.Name = "btGravar";
            this.btGravar.Size = new System.Drawing.Size(75, 23);
            this.btGravar.TabIndex = 30;
            this.btGravar.TabStop = false;
            this.btGravar.Text = "&Gravar";
            this.btGravar.UseVisualStyleBackColor = true;
            this.btGravar.Click += new System.EventHandler(this.btGravar_Click);
            // 
            // btLimpar
            // 
            this.btLimpar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btLimpar.Image = global::SWS.Properties.Resources.vassoura;
            this.btLimpar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btLimpar.Location = new System.Drawing.Point(84, 3);
            this.btLimpar.Name = "btLimpar";
            this.btLimpar.Size = new System.Drawing.Size(75, 23);
            this.btLimpar.TabIndex = 32;
            this.btLimpar.TabStop = false;
            this.btLimpar.Text = "&Limpar";
            this.btLimpar.UseVisualStyleBackColor = true;
            this.btLimpar.Click += new System.EventHandler(this.btLimpar_Click);
            // 
            // btFechar
            // 
            this.btFechar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btFechar.Image = global::SWS.Properties.Resources.close;
            this.btFechar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btFechar.Location = new System.Drawing.Point(165, 3);
            this.btFechar.Name = "btFechar";
            this.btFechar.Size = new System.Drawing.Size(75, 23);
            this.btFechar.TabIndex = 31;
            this.btFechar.TabStop = false;
            this.btFechar.Text = "&Fechar";
            this.btFechar.UseVisualStyleBackColor = true;
            this.btFechar.Click += new System.EventHandler(this.btFechar_Click);
            // 
            // textEmail
            // 
            this.textEmail.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textEmail.CharacterCasing = System.Windows.Forms.CharacterCasing.Lower;
            this.textEmail.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textEmail.Location = new System.Drawing.Point(158, 348);
            this.textEmail.MaxLength = 100;
            this.textEmail.Name = "textEmail";
            this.textEmail.Size = new System.Drawing.Size(349, 21);
            this.textEmail.TabIndex = 16;
            // 
            // lblEmail
            // 
            this.lblEmail.BackColor = System.Drawing.Color.LightGray;
            this.lblEmail.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblEmail.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblEmail.Location = new System.Drawing.Point(7, 348);
            this.lblEmail.Name = "lblEmail";
            this.lblEmail.Size = new System.Drawing.Size(152, 21);
            this.lblEmail.TabIndex = 60;
            this.lblEmail.TabStop = false;
            this.lblEmail.Text = "E-Mail";
            // 
            // textPis
            // 
            this.textPis.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textPis.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textPis.Location = new System.Drawing.Point(158, 328);
            this.textPis.Mask = "999,99999,99-99";
            this.textPis.Name = "textPis";
            this.textPis.PromptChar = ' ';
            this.textPis.Size = new System.Drawing.Size(349, 21);
            this.textPis.TabIndex = 15;
            // 
            // lblPIS
            // 
            this.lblPIS.BackColor = System.Drawing.Color.LightGray;
            this.lblPIS.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblPIS.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPIS.Location = new System.Drawing.Point(7, 328);
            this.lblPIS.Name = "lblPIS";
            this.lblPIS.Size = new System.Drawing.Size(152, 21);
            this.lblPIS.TabIndex = 59;
            this.lblPIS.TabStop = false;
            this.lblPIS.Text = "PIS / PASEP";
            // 
            // textCelular
            // 
            this.textCelular.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textCelular.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.textCelular.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textCelular.Location = new System.Drawing.Point(158, 308);
            this.textCelular.MaxLength = 15;
            this.textCelular.Name = "textCelular";
            this.textCelular.Size = new System.Drawing.Size(349, 21);
            this.textCelular.TabIndex = 14;
            // 
            // lblCelular
            // 
            this.lblCelular.BackColor = System.Drawing.Color.LightGray;
            this.lblCelular.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblCelular.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCelular.Location = new System.Drawing.Point(7, 308);
            this.lblCelular.Name = "lblCelular";
            this.lblCelular.Size = new System.Drawing.Size(152, 21);
            this.lblCelular.TabIndex = 58;
            this.lblCelular.TabStop = false;
            this.lblCelular.Text = "Celular";
            // 
            // textTelefone
            // 
            this.textTelefone.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textTelefone.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.textTelefone.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textTelefone.Location = new System.Drawing.Point(158, 288);
            this.textTelefone.MaxLength = 15;
            this.textTelefone.Name = "textTelefone";
            this.textTelefone.Size = new System.Drawing.Size(349, 21);
            this.textTelefone.TabIndex = 13;
            // 
            // lblTelefone
            // 
            this.lblTelefone.BackColor = System.Drawing.Color.LightGray;
            this.lblTelefone.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblTelefone.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTelefone.Location = new System.Drawing.Point(7, 288);
            this.lblTelefone.Name = "lblTelefone";
            this.lblTelefone.Size = new System.Drawing.Size(152, 21);
            this.lblTelefone.TabIndex = 57;
            this.lblTelefone.TabStop = false;
            this.lblTelefone.Text = "Telefone";
            // 
            // textCEP
            // 
            this.textCEP.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textCEP.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textCEP.Location = new System.Drawing.Point(158, 268);
            this.textCEP.Mask = "99999-999";
            this.textCEP.Name = "textCEP";
            this.textCEP.PromptChar = ' ';
            this.textCEP.Size = new System.Drawing.Size(349, 21);
            this.textCEP.TabIndex = 12;
            // 
            // lblCEP
            // 
            this.lblCEP.BackColor = System.Drawing.Color.LightGray;
            this.lblCEP.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblCEP.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCEP.Location = new System.Drawing.Point(7, 268);
            this.lblCEP.Name = "lblCEP";
            this.lblCEP.Size = new System.Drawing.Size(152, 21);
            this.lblCEP.TabIndex = 56;
            this.lblCEP.TabStop = false;
            this.lblCEP.Text = "CEP";
            // 
            // btCidade
            // 
            this.btCidade.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btCidade.Image = global::SWS.Properties.Resources.busca;
            this.btCidade.Location = new System.Drawing.Point(473, 248);
            this.btCidade.Name = "btCidade";
            this.btCidade.Size = new System.Drawing.Size(34, 21);
            this.btCidade.TabIndex = 11;
            this.btCidade.UseVisualStyleBackColor = true;
            this.btCidade.Click += new System.EventHandler(this.btCidade_Click);
            // 
            // textCidade
            // 
            this.textCidade.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.textCidade.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textCidade.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.textCidade.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textCidade.Location = new System.Drawing.Point(158, 248);
            this.textCidade.MaxLength = 100;
            this.textCidade.Name = "textCidade";
            this.textCidade.ReadOnly = true;
            this.textCidade.Size = new System.Drawing.Size(321, 21);
            this.textCidade.TabIndex = 55;
            this.textCidade.TabStop = false;
            // 
            // lblCidade
            // 
            this.lblCidade.BackColor = System.Drawing.Color.LightGray;
            this.lblCidade.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblCidade.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCidade.Location = new System.Drawing.Point(7, 248);
            this.lblCidade.Name = "lblCidade";
            this.lblCidade.Size = new System.Drawing.Size(152, 21);
            this.lblCidade.TabIndex = 54;
            this.lblCidade.TabStop = false;
            this.lblCidade.Text = "Cidade";
            // 
            // cbUF
            // 
            this.cbUF.BorderColor = System.Drawing.Color.DimGray;
            this.cbUF.BorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid;
            this.cbUF.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbUF.FormattingEnabled = true;
            this.cbUF.Location = new System.Drawing.Point(158, 228);
            this.cbUF.Name = "cbUF";
            this.cbUF.Size = new System.Drawing.Size(349, 21);
            this.cbUF.TabIndex = 10;
            // 
            // lblUf
            // 
            this.lblUf.BackColor = System.Drawing.Color.LightGray;
            this.lblUf.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblUf.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblUf.Location = new System.Drawing.Point(7, 228);
            this.lblUf.Name = "lblUf";
            this.lblUf.Size = new System.Drawing.Size(152, 21);
            this.lblUf.TabIndex = 53;
            this.lblUf.TabStop = false;
            this.lblUf.Text = "UF";
            // 
            // textBairro
            // 
            this.textBairro.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBairro.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.textBairro.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBairro.Location = new System.Drawing.Point(158, 208);
            this.textBairro.MaxLength = 100;
            this.textBairro.Name = "textBairro";
            this.textBairro.Size = new System.Drawing.Size(349, 21);
            this.textBairro.TabIndex = 9;
            // 
            // lblBairro
            // 
            this.lblBairro.BackColor = System.Drawing.Color.LightGray;
            this.lblBairro.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblBairro.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblBairro.Location = new System.Drawing.Point(7, 208);
            this.lblBairro.Name = "lblBairro";
            this.lblBairro.Size = new System.Drawing.Size(152, 21);
            this.lblBairro.TabIndex = 50;
            this.lblBairro.TabStop = false;
            this.lblBairro.Text = "Bairro";
            // 
            // textComplemento
            // 
            this.textComplemento.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textComplemento.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.textComplemento.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textComplemento.Location = new System.Drawing.Point(158, 188);
            this.textComplemento.MaxLength = 100;
            this.textComplemento.Name = "textComplemento";
            this.textComplemento.Size = new System.Drawing.Size(349, 21);
            this.textComplemento.TabIndex = 8;
            // 
            // lblComplemento
            // 
            this.lblComplemento.BackColor = System.Drawing.Color.LightGray;
            this.lblComplemento.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblComplemento.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblComplemento.Location = new System.Drawing.Point(7, 188);
            this.lblComplemento.Name = "lblComplemento";
            this.lblComplemento.Size = new System.Drawing.Size(152, 21);
            this.lblComplemento.TabIndex = 48;
            this.lblComplemento.TabStop = false;
            this.lblComplemento.Text = "Complemento";
            // 
            // textNumero
            // 
            this.textNumero.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textNumero.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.textNumero.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textNumero.Location = new System.Drawing.Point(158, 168);
            this.textNumero.MaxLength = 15;
            this.textNumero.Name = "textNumero";
            this.textNumero.Size = new System.Drawing.Size(349, 21);
            this.textNumero.TabIndex = 7;
            // 
            // lblNumero
            // 
            this.lblNumero.BackColor = System.Drawing.Color.LightGray;
            this.lblNumero.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblNumero.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNumero.Location = new System.Drawing.Point(7, 168);
            this.lblNumero.Name = "lblNumero";
            this.lblNumero.Size = new System.Drawing.Size(152, 21);
            this.lblNumero.TabIndex = 44;
            this.lblNumero.TabStop = false;
            this.lblNumero.Text = "Numero";
            // 
            // textEndereco
            // 
            this.textEndereco.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textEndereco.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.textEndereco.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textEndereco.Location = new System.Drawing.Point(158, 148);
            this.textEndereco.MaxLength = 100;
            this.textEndereco.Name = "textEndereco";
            this.textEndereco.Size = new System.Drawing.Size(349, 21);
            this.textEndereco.TabIndex = 6;
            // 
            // lblEndereco
            // 
            this.lblEndereco.BackColor = System.Drawing.Color.LightGray;
            this.lblEndereco.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblEndereco.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblEndereco.Location = new System.Drawing.Point(7, 148);
            this.lblEndereco.Name = "lblEndereco";
            this.lblEndereco.Size = new System.Drawing.Size(152, 21);
            this.lblEndereco.TabIndex = 41;
            this.lblEndereco.TabStop = false;
            this.lblEndereco.Text = "Endereço";
            // 
            // lblDataNascimento
            // 
            this.lblDataNascimento.BackColor = System.Drawing.Color.LightGray;
            this.lblDataNascimento.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblDataNascimento.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDataNascimento.Location = new System.Drawing.Point(7, 128);
            this.lblDataNascimento.Name = "lblDataNascimento";
            this.lblDataNascimento.Size = new System.Drawing.Size(152, 21);
            this.lblDataNascimento.TabIndex = 38;
            this.lblDataNascimento.TabStop = false;
            this.lblDataNascimento.Text = "Data de Nascimento";
            // 
            // lblRG
            // 
            this.lblRG.BackColor = System.Drawing.Color.LightGray;
            this.lblRG.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblRG.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblRG.Location = new System.Drawing.Point(7, 108);
            this.lblRG.Name = "lblRG";
            this.lblRG.Size = new System.Drawing.Size(152, 21);
            this.lblRG.TabIndex = 36;
            this.lblRG.TabStop = false;
            this.lblRG.Text = "RG";
            // 
            // textRG
            // 
            this.textRG.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textRG.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.textRG.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textRG.Location = new System.Drawing.Point(158, 108);
            this.textRG.MaxLength = 15;
            this.textRG.Name = "textRG";
            this.textRG.Size = new System.Drawing.Size(349, 21);
            this.textRG.TabIndex = 4;
            // 
            // textCPF
            // 
            this.textCPF.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textCPF.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textCPF.Location = new System.Drawing.Point(158, 88);
            this.textCPF.Mask = "999,999,999-99";
            this.textCPF.Name = "textCPF";
            this.textCPF.PromptChar = ' ';
            this.textCPF.Size = new System.Drawing.Size(349, 21);
            this.textCPF.TabIndex = 3;
            // 
            // lblCPF
            // 
            this.lblCPF.BackColor = System.Drawing.Color.LightGray;
            this.lblCPF.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblCPF.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCPF.Location = new System.Drawing.Point(7, 88);
            this.lblCPF.Name = "lblCPF";
            this.lblCPF.Size = new System.Drawing.Size(152, 21);
            this.lblCPF.TabIndex = 33;
            this.lblCPF.TabStop = false;
            this.lblCPF.Text = "CPF";
            // 
            // textNome
            // 
            this.textNome.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textNome.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.textNome.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textNome.Location = new System.Drawing.Point(158, 68);
            this.textNome.MaxLength = 100;
            this.textNome.Name = "textNome";
            this.textNome.Size = new System.Drawing.Size(349, 21);
            this.textNome.TabIndex = 2;
            // 
            // lblNome
            // 
            this.lblNome.BackColor = System.Drawing.Color.LightGray;
            this.lblNome.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblNome.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNome.Location = new System.Drawing.Point(7, 68);
            this.lblNome.Name = "lblNome";
            this.lblNome.Size = new System.Drawing.Size(152, 21);
            this.lblNome.TabIndex = 31;
            this.lblNome.TabStop = false;
            this.lblNome.Text = "Nome\r\n";
            // 
            // cbTipoContato
            // 
            this.cbTipoContato.BackColor = System.Drawing.Color.LightGray;
            this.cbTipoContato.BorderColor = System.Drawing.Color.DimGray;
            this.cbTipoContato.BorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid;
            this.cbTipoContato.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbTipoContato.FormattingEnabled = true;
            this.cbTipoContato.Location = new System.Drawing.Point(158, 8);
            this.cbTipoContato.Name = "cbTipoContato";
            this.cbTipoContato.Size = new System.Drawing.Size(349, 21);
            this.cbTipoContato.TabIndex = 1;
            // 
            // lblTipoContato
            // 
            this.lblTipoContato.BackColor = System.Drawing.Color.LightGray;
            this.lblTipoContato.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblTipoContato.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTipoContato.Location = new System.Drawing.Point(7, 8);
            this.lblTipoContato.Name = "lblTipoContato";
            this.lblTipoContato.Size = new System.Drawing.Size(152, 21);
            this.lblTipoContato.TabIndex = 28;
            this.lblTipoContato.TabStop = false;
            this.lblTipoContato.Text = "Tipo de Contato";
            // 
            // dataNascimento
            // 
            this.dataNascimento.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dataNascimento.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dataNascimento.Location = new System.Drawing.Point(158, 128);
            this.dataNascimento.Name = "dataNascimento";
            this.dataNascimento.Size = new System.Drawing.Size(349, 21);
            this.dataNascimento.TabIndex = 5;
            // 
            // lblResponsavel
            // 
            this.lblResponsavel.BackColor = System.Drawing.Color.LightGray;
            this.lblResponsavel.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblResponsavel.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblResponsavel.Location = new System.Drawing.Point(7, 28);
            this.lblResponsavel.Name = "lblResponsavel";
            this.lblResponsavel.Size = new System.Drawing.Size(152, 21);
            this.lblResponsavel.TabIndex = 61;
            this.lblResponsavel.TabStop = false;
            this.lblResponsavel.Text = "Responsável Contrato";
            // 
            // cbResponsavel
            // 
            this.cbResponsavel.BackColor = System.Drawing.Color.LightGray;
            this.cbResponsavel.BorderColor = System.Drawing.Color.DimGray;
            this.cbResponsavel.BorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid;
            this.cbResponsavel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbResponsavel.FormattingEnabled = true;
            this.cbResponsavel.Location = new System.Drawing.Point(158, 28);
            this.cbResponsavel.Name = "cbResponsavel";
            this.cbResponsavel.Size = new System.Drawing.Size(349, 21);
            this.cbResponsavel.TabIndex = 2;
            // 
            // cbResponsavelEstudo
            // 
            this.cbResponsavelEstudo.BackColor = System.Drawing.Color.LightGray;
            this.cbResponsavelEstudo.BorderColor = System.Drawing.Color.DimGray;
            this.cbResponsavelEstudo.BorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid;
            this.cbResponsavelEstudo.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbResponsavelEstudo.FormattingEnabled = true;
            this.cbResponsavelEstudo.Location = new System.Drawing.Point(158, 48);
            this.cbResponsavelEstudo.Name = "cbResponsavelEstudo";
            this.cbResponsavelEstudo.Size = new System.Drawing.Size(349, 21);
            this.cbResponsavelEstudo.TabIndex = 62;
            // 
            // lblResponsavelEstudo
            // 
            this.lblResponsavelEstudo.BackColor = System.Drawing.Color.LightGray;
            this.lblResponsavelEstudo.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblResponsavelEstudo.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblResponsavelEstudo.Location = new System.Drawing.Point(7, 48);
            this.lblResponsavelEstudo.Name = "lblResponsavelEstudo";
            this.lblResponsavelEstudo.Size = new System.Drawing.Size(152, 21);
            this.lblResponsavelEstudo.TabIndex = 63;
            this.lblResponsavelEstudo.TabStop = false;
            this.lblResponsavelEstudo.Text = "Responsável PCMSO";
            // 
            // frmContatoIncluir
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(534, 361);
            this.Name = "frmContatoIncluir";
            this.Text = "INCLUIR CONTATO";
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.frmContatoIncluir_KeyDown);
            this.scForm.Panel1.ResumeLayout(false);
            this.scForm.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.scForm)).EndInit();
            this.scForm.ResumeLayout(false);
            this.pnlForm.ResumeLayout(false);
            this.pnlForm.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.banner)).EndInit();
            this.flpAcao.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.FlowLayoutPanel flpAcao;
        protected System.Windows.Forms.Button btFechar;
        protected System.Windows.Forms.Button btLimpar;
        protected System.Windows.Forms.Button btGravar;
        protected System.Windows.Forms.TextBox textEmail;
        protected System.Windows.Forms.TextBox lblEmail;
        protected System.Windows.Forms.MaskedTextBox textPis;
        protected System.Windows.Forms.TextBox lblPIS;
        protected System.Windows.Forms.TextBox textCelular;
        protected System.Windows.Forms.TextBox lblCelular;
        protected System.Windows.Forms.TextBox textTelefone;
        protected System.Windows.Forms.TextBox lblTelefone;
        protected System.Windows.Forms.MaskedTextBox textCEP;
        protected System.Windows.Forms.TextBox lblCEP;
        protected System.Windows.Forms.Button btCidade;
        protected System.Windows.Forms.TextBox textCidade;
        protected System.Windows.Forms.TextBox lblCidade;
        protected ComboBoxWithBorder cbUF;
        protected System.Windows.Forms.TextBox lblUf;
        protected System.Windows.Forms.TextBox textBairro;
        protected System.Windows.Forms.TextBox lblBairro;
        protected System.Windows.Forms.TextBox textComplemento;
        protected System.Windows.Forms.TextBox lblComplemento;
        protected System.Windows.Forms.TextBox textNumero;
        protected System.Windows.Forms.TextBox lblNumero;
        protected System.Windows.Forms.TextBox textEndereco;
        protected System.Windows.Forms.TextBox lblEndereco;
        protected System.Windows.Forms.TextBox lblDataNascimento;
        protected System.Windows.Forms.TextBox lblRG;
        protected System.Windows.Forms.TextBox textRG;
        protected System.Windows.Forms.MaskedTextBox textCPF;
        protected System.Windows.Forms.TextBox lblCPF;
        protected System.Windows.Forms.TextBox textNome;
        protected System.Windows.Forms.TextBox lblNome;
        protected ComboBoxWithBorder cbTipoContato;
        protected System.Windows.Forms.TextBox lblTipoContato;
        protected System.Windows.Forms.DateTimePicker dataNascimento;
        protected ComboBoxWithBorder cbResponsavel;
        protected System.Windows.Forms.TextBox lblResponsavel;
        protected ComboBoxWithBorder cbResponsavelEstudo;
        protected System.Windows.Forms.TextBox lblResponsavelEstudo;
    }
}