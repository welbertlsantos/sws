﻿using SWS.Entidade;
using SWS.Facade;
using SWS.View.Resources;
using SWS.View.ViewHelper;
using SWS.ViewHelper;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace SWS.View
{
    public partial class frmFuncaoInternaPrincipal : frmTemplate
    {
        FuncaoInterna funcaoInterna;
        
        public frmFuncaoInternaPrincipal()
        {
            InitializeComponent();
            validaPermissoes();
            ComboHelper.comboSituacao(cbSituacao);
        }

        private void grd_funcao_interna_RowPrePaint(object sender, DataGridViewRowPrePaintEventArgs e)
        {
            DataGridView dgv = sender as DataGridView;

            if (String.Equals(dgv.Rows[e.RowIndex].Cells[2].Value, "I"))
                dgv.Rows[e.RowIndex].DefaultCellStyle.ForeColor = Color.Red;
        }

        private void bt_incluir_Click(object sender, EventArgs e)
        {
            frmFuncaoInternaIncluir ManterFuncaoInterna = new frmFuncaoInternaIncluir();
            ManterFuncaoInterna.ShowDialog();

            if (ManterFuncaoInterna.FuncaoInterna != null)
            {
                funcaoInterna = ManterFuncaoInterna.FuncaoInterna;
                funcaoInterna = new FuncaoInterna();
                montaDataGrid();
            }
        }

        private void bt_alterar_Click(object sender, EventArgs e)
        {
            try
            {
                if (grd_funcao_interna.CurrentRow == null)
                    throw new Exception ("Selecione uma linha.");
                
                UsuarioFacade usuarioFacade = UsuarioFacade.getInstance();

                funcaoInterna = usuarioFacade.findFuncaoInternaById((Int64)grd_funcao_interna.CurrentRow.Cells[0].Value);

                if (!String.Equals(funcaoInterna.Situacao, ApplicationConstants.ATIVO))
                    throw new Exception ("Somente funções internas ativas podem ser alteradas.");

                if (String.Equals(funcaoInterna.Privado, ApplicationConstants.OFF))
                    throw new Exception("Função interna exclusiva do sistema. Não pode ser alterada.");

                frmFuncaoInternaAlterar alterarFuncaoInterna = new frmFuncaoInternaAlterar(funcaoInterna);
                alterarFuncaoInterna.ShowDialog();

                funcaoInterna = new FuncaoInterna();
                montaDataGrid();

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void bt_excluir_Click(object sender, EventArgs e)
        {
            try
            {
                if (grd_funcao_interna.CurrentRow == null)
                    throw new Exception("Selecione uma linha.");
                
                UsuarioFacade usuarioFacade = UsuarioFacade.getInstance();
                
                funcaoInterna = usuarioFacade.findFuncaoInternaById((Int64)grd_funcao_interna.CurrentRow.Cells[0].Value);

                if (MessageBox.Show("Deseja desativar a função interna selecionada?", "Confirmação", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                {
                    if (!String.Equals(funcaoInterna.Situacao, ApplicationConstants.ATIVO))
                        throw new Exception("Somente funções internas ativas podem ser desativadas.");

                    if (String.Equals(funcaoInterna.Privado, ApplicationConstants.OFF))
                        throw new Exception("Função interna exclusiva do sistema. Não pode ser desativada.");

                    funcaoInterna.Situacao = ApplicationConstants.DESATIVADO;

                    usuarioFacade.alteraFuncaoInterna(funcaoInterna);

                    MessageBox.Show("Função interna desativada com sucesso.", "Confirmação", MessageBoxButtons.OK, MessageBoxIcon.Information);

                    funcaoInterna = new FuncaoInterna();
                    montaDataGrid();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btn_reativar_Click(object sender, EventArgs e)
        {
            try
            {
                if (grd_funcao_interna.CurrentRow == null)
                    throw new Exception ("Selecione uma linha.");

                
                UsuarioFacade usuarioFacade = UsuarioFacade.getInstance();

                funcaoInterna = usuarioFacade.findFuncaoInternaById((Int64)grd_funcao_interna.CurrentRow.Cells[0].Value);

                if (MessageBox.Show("Deseja reativar a função interna selecionada?", "Confirmação", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                {
                    if (!String.Equals(funcaoInterna.Situacao, ApplicationConstants.DESATIVADO))
                        throw new Exception("Somente funções internas desativadas podem ser reativadas novamente.");

                    funcaoInterna.Situacao = ApplicationConstants.ATIVO;

                    usuarioFacade.alteraFuncaoInterna(funcaoInterna);

                    MessageBox.Show("Função interna reativada com sucesso.", "Confirmação", MessageBoxButtons.OK, MessageBoxIcon.Information);

                    funcaoInterna = new FuncaoInterna();
                    montaDataGrid();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void bt_detalhar_Click(object sender, EventArgs e)
        {
            try
            {
                if (grd_funcao_interna.CurrentRow == null)
                    throw new Exception ("Selecione uma linha.");
                
                UsuarioFacade usuarioFacade = UsuarioFacade.getInstance();

                frmFuncaoInternaDetalhar detalharFuncaoInterna = new frmFuncaoInternaDetalhar(usuarioFacade.findFuncaoInternaById((long)grd_funcao_interna.CurrentRow.Cells[0].Value));
                detalharFuncaoInterna.ShowDialog();

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção",MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void bt_limpar_Click(object sender, EventArgs e)
        {
            text_descricao.Text = String.Empty;
            ComboHelper.comboSituacao(cbSituacao);
            grd_funcao_interna.Columns.Clear();
            text_descricao.Focus();
        }

        private void bt_pesquisar_Click(object sender, EventArgs e)
        {
            try
            {
                funcaoInterna = new FuncaoInterna(null, text_descricao.Text, ((SelectItem)cbSituacao.SelectedItem).Valor, string.Empty);
                montaDataGrid();
                text_descricao.Focus();
            }
            catch (Exception Ex)
            {
                MessageBox.Show(Ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);

            }
        }

        private void bt_fechar_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void validaPermissoes()
        {
            PermissionamentoFacade permissionamentoFacade = PermissionamentoFacade.getInstance();

            bt_alterar.Enabled = permissionamentoFacade.hasPermission("FUNCAO_INTERNA", "ALTERAR");
            bt_incluir.Enabled = permissionamentoFacade.hasPermission("FUNCAO_INTERNA", "INCLUIR");
            bt_excluir.Enabled = permissionamentoFacade.hasPermission("FUNCAO_INTERNA", "EXCLUIR");
            bt_detalhar.Enabled = permissionamentoFacade.hasPermission("FUNCAO_INTERNA", "DETALHAR");
        }

        public void montaDataGrid()
        {
            try
            {
                
                UsuarioFacade usuarioFacade = UsuarioFacade.getInstance();

                DataSet ds = usuarioFacade.FindFuncaoInternaByFilter(funcaoInterna);

                grd_funcao_interna.DataSource = ds.Tables["FuncoesInternas"].DefaultView;

                grd_funcao_interna.Columns[0].HeaderText = "ID";
                grd_funcao_interna.Columns[0].Visible = false;

                grd_funcao_interna.Columns[1].HeaderText = "Descrição";
                
                grd_funcao_interna.Columns[2].HeaderText = "Situação";
                grd_funcao_interna.Columns[2].Visible = false;

                grd_funcao_interna.Columns[3].HeaderText = "Privado";
                grd_funcao_interna.Columns[3].Visible = false;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);

            }
        }

        private void grd_funcao_interna_DataBindingComplete(object sender, DataGridViewBindingCompleteEventArgs e)
        {
            DataGridView dgv = sender as DataGridView;
            dgv.ClearSelection();
        }

        private void grd_funcao_interna_CellMouseDoubleClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            try
            {
                bt_alterar.PerformClick();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }
    }
}
