﻿using SWS.Entidade;
using SWS.Facade;
using SWS.View.Resources;
using SWS.View.ViewHelper;
using SWS.ViewHelper;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace SWS.View
{
    public partial class frmSalaIncluir : frmTemplate
    {
        protected Sala sala;

        public Sala Sala
        {
            get { return sala; }
            set { sala = value; }
        }

        private List<SalaExame> examesInSala = new List<SalaExame>();

        protected List<SalaExame> ExamesInSala
        {
            get { return examesInSala; }
            set { examesInSala = value; }
        }
        
        public frmSalaIncluir()
        {
            InitializeComponent();
            ComboHelper.Boolean(cbVIP, false);
            ActiveControl = textNome;
        }

        protected void btn_fechar_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        protected void SalaIncluir_KeyDown(object sender, KeyEventArgs e)
        {
             if (e.KeyCode == Keys.Enter || e.KeyCode == Keys.Return)
                SendKeys.Send("{TAB}");
        }

        protected virtual void btn_incluir_Click(object sender, EventArgs e)
        {
            try
            {
                if (validaCampos())
                {
                    this.Cursor = Cursors.WaitCursor;

                    FilaFacade filaFacade = FilaFacade.getInstance();

                    sala = new Sala(null, textNome.Text, ApplicationConstants.ATIVO, Convert.ToInt32(textAndar.Text), Convert.ToBoolean(((SelectItem)cbVIP.SelectedItem).Valor), PermissionamentoFacade.usuarioAutenticado.Empresa, textSlug.Text, Convert.ToInt32(textQtdeChamada.Text));
                    sala.Exames = examesInSala;
                    sala = filaFacade.insertSala(sala);
                    MessageBox.Show("Sala gravada com sucesso!", "Confirmação", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    this.Close();
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }

        }

        protected virtual void btn_incluirExame_Click(object sender, EventArgs e)
        {
            try
            {
                frmExameBuscar buscarExame = new frmExameBuscar(true);
                buscarExame.ShowDialog();

                StringBuilder resultado = new StringBuilder(); 

                if (buscarExame.Exames.Count > 0)
                {
                    buscarExame.Exames.ForEach(delegate(Exame exame)
                    {
                        if (examesInSala.Exists(x => x.Exame.Id == exame.Id))
                            resultado.Append(exame.Descricao + System.Environment.NewLine);
                        else
                            examesInSala.Add(new SalaExame(null, exame, null, 0, 0, ApplicationConstants.ATIVO));

                    });

                    if (!string.IsNullOrEmpty(resultado.ToString()))
                        MessageBox.Show("O(s) exame(s): " + resultado.ToString() + "Já estavam incluído(s) na sala.", "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Warning);

                    montaGrid();
                }


            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }

        protected virtual void btn_excluirExame_Click(object sender, EventArgs e)
        {
            try
            {
                if (dgvExame.CurrentRow == null)
                    throw new Exception("Selecione uma linha.");

                if (MessageBox.Show("Deseja excluir o exame selecionado?", "Atenção", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                {
                    SalaExame salaExameRemove = examesInSala.Find(x => x.Exame.Id == (long)dgvExame.CurrentRow.Cells[1].Value);
                    examesInSala.Remove(salaExameRemove);
                    dgvExame.Rows.RemoveAt(dgvExame.CurrentRow.Index);
                    MessageBox.Show("Exame removido com sucesso.", "Confirmação", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }

        protected Boolean validaCampos()
        {
            bool retorno = true;
            
            try
            {
                if (String.IsNullOrEmpty(textNome.Text))
                {
                    textNome.Focus();
                    throw new Exception("O nome da sala é obrigatório.");
                }

                if (String.IsNullOrEmpty(textAndar.Text))
                {
                    textAndar.Focus();
                    throw new Exception(" O número do andar é obrigatório.");
                }
            }
            catch (Exception ex)
            {
                retorno = false;
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

            return retorno;
        }

        protected void text_andar_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = ValidaCampoHelper.FormataCampoNumerico(sender, e);
        }

        protected void montaGrid()
        {
            try
            {
                dgvExame.Columns.Clear();

                this.Cursor = Cursors.WaitCursor;

                dgvExame.ColumnCount = 6;

                dgvExame.Columns[0].HeaderText = "IdSalaExame";
                dgvExame.Columns[0].Visible = false;

                dgvExame.Columns[1].HeaderText = "IdExame"; //
                dgvExame.Columns[1].Visible = false;

                dgvExame.Columns[2].HeaderText = "Exame";
                dgvExame.Columns[2].ReadOnly = true;

                dgvExame.Columns[3].HeaderText = "Laboratório"; //
                dgvExame.Columns[3].ReadOnly = true;
                dgvExame.Columns[3].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;

                dgvExame.Columns[4].HeaderText = "TempoExame(min)";
                dgvExame.Columns[4].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
                dgvExame.Columns[4].DefaultCellStyle.BackColor = Color.White;

                dgvExame.Columns[5].HeaderText = "Externo"; //
                dgvExame.Columns[5].ReadOnly = true;

                examesInSala.ForEach(delegate(SalaExame salaExame)
                {
                    dgvExame.Rows.Add(salaExame.Id, salaExame.Exame.Id, salaExame.Exame.Descricao, salaExame.Exame.Laboratorio == true ? "Sim" : "Não", salaExame.TempoMedioAtendimento == null ? 0 : salaExame.TempoMedioAtendimento, salaExame.Exame.Externo == true ? "Sim" : "Não");
                });

                dgvExame.Columns[2].DisplayIndex = 2;
                dgvExame.Columns[4].DisplayIndex = 3;
                dgvExame.Columns[3].DisplayIndex = 4;
                dgvExame.Columns[4].DisplayIndex = 5;

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }

        }

        protected void dgvExame_EditingControlShowing(object sender, DataGridViewEditingControlShowingEventArgs e)
        {
            if (e.Control is DataGridViewTextBoxEditingControl)
                e.Control.KeyPress += new KeyPressEventHandler(dgvExame_KeyPress);
        }

        protected void dgvExame_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = ValidaCampoHelper.FormataCampoNumerico(sender, e);
        }

        protected virtual void dgvExame_CellLeave(object sender, DataGridViewCellEventArgs e)
        {
            if (e.ColumnIndex == 4)
            {
                if (string.IsNullOrEmpty(dgvExame.Rows[e.RowIndex].Cells[4].EditedFormattedValue.ToString()))
                    dgvExame.Rows[e.RowIndex].Cells[4].Value = 0;
            }
        }

        protected void text_andar_Leave(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(textAndar.Text.Trim()))
                textAndar.Text = textAndar.Tag.ToString();
        }

        protected void textAndar_Enter(object sender, EventArgs e)
        {
            textAndar.Tag = textAndar.Text;
            textAndar.Text = string.Empty;

        }

        private void textQtdeChamada_Enter(object sender, EventArgs e)
        {
            textQtdeChamada.Tag = textQtdeChamada.Text;
            textQtdeChamada.Text = string.Empty;
        }

        private void textQtdeChamada_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = ValidaCampoHelper.FormataCampoNumerico(sender, e);
        }

        private void textQtdeChamada_Leave(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(textQtdeChamada.Text.Trim()))
                textQtdeChamada.Text = textQtdeChamada.Tag.ToString();
        }

    }
}
