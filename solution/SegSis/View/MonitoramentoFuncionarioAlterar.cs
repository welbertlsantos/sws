﻿using SWS.Entidade;
using SWS.Facade;
using SWS.View.ViewHelper;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace SWS.View
{
    public partial class frmMonitoramentoFuncionarioAlterar : frmMonitoramentoFuncionario
    {
        protected Cliente clienteMonitoramento;
        
        public frmMonitoramentoFuncionarioAlterar(Monitoramento monitoramento) :base()
        {
            InitializeComponent();
            this.Monitoramento = monitoramento;
            this.ControlBox = false;
            montaDadosTela();
        }

        protected override void montaDadosTela()
        {
            try
            {
                textNome.Text = this.monitoramento.ClienteFuncaoFuncionario != null ? this.Monitoramento.ClienteFuncaoFuncionario.Funcionario.Nome : this.monitoramento.ClienteFuncionario.Funcionario.Nome;
                textCpf.Text = this.monitoramento.ClienteFuncaoFuncionario != null ? ValidaCampoHelper.FormataCpf(this.Monitoramento.ClienteFuncaoFuncionario.Funcionario.Cpf) : ValidaCampoHelper.FormataCpf(this.Monitoramento.ClienteFuncionario.Funcionario.Cpf);
                textCliente.Text = this.monitoramento.ClienteFuncaoFuncionario != null ? this.Monitoramento.ClienteFuncaoFuncionario.ClienteFuncao.Cliente.RazaoSocial : this.monitoramento.ClienteFuncionario.Cliente.RazaoSocial;
                textCnpj.Text = this.monitoramento.ClienteFuncaoFuncionario != null ? ValidaCampoHelper.FormataCnpj(this.Monitoramento.ClienteFuncaoFuncionario.ClienteFuncao.Cliente.Cnpj) : ValidaCampoHelper.FormataCnpj(this.monitoramento.ClienteFuncionario.Cliente.Cnpj);

                /* recuperando a função do funcionário */
                FuncionarioFacade funcionarioFacade = FuncionarioFacade.getInstance();

                funcoesFuncionarioInCliente = funcionarioFacade.findClienteFuncaoFuncionarioByFuncionarioAndSituacao(this.monitoramento.ClienteFuncaoFuncionario != null ? this.Monitoramento.ClienteFuncaoFuncionario.Funcionario : this.monitoramento.ClienteFuncionario.Funcionario, true);
                if (funcoesFuncionarioInCliente.Count == 0)
                    throw new Exception("O funcionário não está ativo em nenhuma função. O processo não pode continuar");

                clienteMonitoramento = this.monitoramento.ClienteFuncaoFuncionario != null ? this.monitoramento.ClienteFuncaoFuncionario.ClienteFuncao.Cliente : this.monitoramento.ClienteFuncionario.Cliente;

                clienteFuncaoFuncionario = funcoesFuncionarioInCliente.Find(x => x.ClienteFuncao.Cliente.Id == clienteMonitoramento.Id && x.DataDesligamento == null);
                if (clienteFuncaoFuncionario == null) throw new Exception("O funcionário não foi encontrando em nenhuma funçaõ ativada no cliente.");

                textFuncao.Text = clienteFuncaoFuncionario.ClienteFuncao.Funcao.Descricao;

                /* encontrando o pcmso do cliente para continuar */
                EstudoFacade pcmsoFacade = EstudoFacade.getInstance();
                pcmso = this.Monitoramento.Estudo;
                textPcmso.Text = pcmso.CodigoEstudo;

                /* verificando agora o ghe que o funcionário se encontra */

                ghesInPcmso = pcmsoFacade.findAllGheInPcmsoByClienteFuncao(clienteFuncaoFuncionario.ClienteFuncao, pcmso);
                if (ghesInPcmso.Count == 0) throw new Exception("A função " +
                    clienteFuncaoFuncionario.ClienteFuncao.Funcao.Descricao +
                    "[" + clienteFuncaoFuncionario.ClienteFuncao.Funcao.Id + "]" + " não foi encontrada em nenhum GHE. O processo não poderá continuar.");


                /* montando dados da tela */
                this.responsavel = this.Monitoramento.UsuarioResponsavel;
                this.textResponsavel.Text = responsavel.Nome;

                this.dataInicioMonitoramento.Checked = true;
                this.dataInicioMonitoramento.Value = monitoramento.DataInicioCondicao;

                if (monitoramento.DataFimCondicao != null &&
                    (Convert.ToDateTime(monitoramento.DataFimCondicao) != Convert.ToDateTime("01/01/0001 00:00:00")))
                {
                    this.dataFimMonitormento.Checked = true;
                    this.dataFimMonitormento.Value = (DateTime)monitoramento.DataFimCondicao;
                } 

                this.textObservacao.Text = this.Monitoramento.Observacao;

                this.grbMonitoramentoAgente.Enabled = true;
                this.flpMonitoramentoAgente.Enabled = true;

                /* desativando controles */
                this.btnExcluirResponsavel.Enabled = false;

                /* encontrando o ghe e montando a grid com informações */
                findGheSetor();

                /* encontrar o(s) gheFonte(s) que o colaborador está exposto */
                findGheFonte();

                /* encontrando os agentes / Riscos de acordo com a lista de GhesFontes encontrados */
                findGheFonteAgente();

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
                validaDados = false;
            }
        }


        protected override void btnIncluirResponsavel_Click(object sender, EventArgs e)
        {
            try
            {
                if (MessageBox.Show("Deseja alterar o usuário responsável pelo monitoramento?", "Atenção", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                {
                    frmUsuarioTecnicoSelecionar selecionarResponsavel = new frmUsuarioTecnicoSelecionar();
                    selecionarResponsavel.ShowDialog();

                    if (selecionarResponsavel.Usuario != null)
                    {
                        EsocialFacade esocialFacade = EsocialFacade.getInstance();
                        this.Monitoramento.UsuarioResponsavel = selecionarResponsavel.Usuario;
                        esocialFacade.updateMonitoramento(this.Monitoramento);
                        MessageBox.Show("Responsável alterado com sucesso", "Confirmação", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        textResponsavel.Text = this.Monitoramento.UsuarioResponsavel.Nome;
                    }

                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }


        }

        protected override void dgvRisco_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                this.monitoramentoGheFonteAgentes.Clear();
                /* encontrando o GheFonteAgente selecionado e montando a coleção de MonitoramentoGheFonteAgente */
                GheFonteAgente gheFonteAgenteSelecionado = this.gheFonteAgentes.Find(x => x.Id == (long)dgvRisco.Rows[e.RowIndex].Cells[0].Value);

                EsocialFacade EsocialFacade = EsocialFacade.getInstance();
                MonitoramentoGheFonteAgente monitoramentoGheFonteAgenteFind = EsocialFacade.findMonitoramentoGheFonteAgenteByGheFonteAgente(gheFonteAgenteSelecionado, this.monitoramento);

                if (monitoramentoGheFonteAgenteFind != null)
                {
                    this.monitoramentoGheFonteAgentes.Add(monitoramentoGheFonteAgenteFind);
                    montaGridMonitoramentoAgente();
                }
                else
                {
                    dgvMonitoramentoAgente.Columns.Clear();
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }


        }

        protected override void btnAlterarMonitoramentoAgente_Click(object sender, EventArgs e)
        {
            try
            {
                if (dgvMonitoramentoAgente.CurrentRow == null)
                    throw new Exception("Selecione uma monitoramento do agente para alterar");

                /* recuperando o monitoramento de acordo com o index da grid */

                MonitoramentoGheFonteAgente monitoramentoAgenteAlterar = monitoramentoGheFonteAgentes.Find(x => x.Id == (long)dgvMonitoramentoAgente.CurrentRow.Cells[0].Value);

                if (monitoramentoAgenteAlterar == null)
                    throw new Exception("Monitoramento para o agente não encontrado");

                /* se o agente selecionado for um não identificado, não é necessario ter monitoramento */

                if (monitoramentoAgenteAlterar != null && string.Equals(monitoramentoAgenteAlterar.GheFonteAgente.Agente.CodigoEsocial, "09.01.001"))
                    throw new Exception("Não é possível alterar o monitoramento para risco não identificado de acordo com a tabela do E-Social.");

                frmMonitoramentoAgenteAlterar alterarMonitoramento = new frmMonitoramentoAgenteAlterar(monitoramentoAgenteAlterar);
                alterarMonitoramento.ShowDialog();
                /* removendo a lista e incluindo novamente */
                monitoramentoGheFonteAgentes.Remove(monitoramentoAgenteAlterar);
                /* incluindo o novo elemento */
                monitoramentoGheFonteAgentes.Add(alterarMonitoramento.MonitoramentoGheFonteAgente);
                montaGridMonitoramentoAgente();

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        protected override void btnGravar_Click(object sender, EventArgs e)
        {
            try
            {
                if (!validacoesMonitoramento())
                {
                    EsocialFacade esocialFacade = EsocialFacade.getInstance();
                    Monitoramento monitoramentoUpdate = new Monitoramento(this.Monitoramento.Id, this.Monitoramento.MonitoramentoAnterior, clienteFuncaoFuncionario, this.Monitoramento.UsuarioResponsavel, Convert.ToDateTime(dataInicioMonitoramento.Text), textObservacao.Text, clienteMonitoramento, (DateTime)this.monitoramento.Periodo, this.Monitoramento.Estudo);

                    if (dataFimMonitormento.Checked)
                        monitoramentoUpdate.DataFimCondicao = Convert.ToDateTime(dataFimMonitormento.Text);
                    else
                        monitoramentoUpdate.DataFimCondicao = (DateTime?)null;

                    this.Monitoramento = esocialFacade.updateMonitoramento(monitoramentoUpdate);

                    MessageBox.Show("Monitoramento do funcionário alterado com sucesso.", "Confirmação", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }

        protected override void btnFechar_Click(object sender, EventArgs e)
        {
            try
            {
                /* verificar se já foi feito algum monitoramento para o agente */

                /*
                if (this.monitoramentoGheFonteAgentes.Count == 0)
                    throw new Exception("Deve existir pelo menos um agente que tenha sido monitorado.");
                */
                this.Close();
                
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
    }
}
