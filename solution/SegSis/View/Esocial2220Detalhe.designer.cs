﻿namespace SWS.View
{
    partial class frmEsocialLoteDetalhe
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            this.dg_examePendente = new System.Windows.Forms.DataGridView();
            this.btn_fechar = new System.Windows.Forms.Button();
            this.grbTotal = new System.Windows.Forms.GroupBox();
            this.lblTotalTitulo = new System.Windows.Forms.Label();
            this.textTotal = new System.Windows.Forms.TextBox();
            ((System.ComponentModel.ISupportInitialize)(this.spForm)).BeginInit();
            this.spForm.Panel1.SuspendLayout();
            this.spForm.Panel2.SuspendLayout();
            this.spForm.SuspendLayout();
            this.pnlForm.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.banner)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dg_examePendente)).BeginInit();
            this.grbTotal.SuspendLayout();
            this.SuspendLayout();
            // 
            // spForm
            // 
            // 
            // spForm.Panel1
            // 
            this.spForm.Panel1.Controls.Add(this.btn_fechar);
            // 
            // pnlForm
            // 
            this.pnlForm.Controls.Add(this.dg_examePendente);
            this.pnlForm.Controls.Add(this.grbTotal);
            // 
            // dg_examePendente
            // 
            this.dg_examePendente.AllowUserToAddRows = false;
            this.dg_examePendente.AllowUserToDeleteRows = false;
            this.dg_examePendente.AllowUserToOrderColumns = true;
            this.dg_examePendente.AllowUserToResizeColumns = false;
            this.dg_examePendente.AllowUserToResizeRows = false;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.Gainsboro;
            this.dg_examePendente.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle1;
            this.dg_examePendente.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this.dg_examePendente.BackgroundColor = System.Drawing.Color.White;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.Color.LightGray;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dg_examePendente.DefaultCellStyle = dataGridViewCellStyle2;
            this.dg_examePendente.Location = new System.Drawing.Point(3, 3);
            this.dg_examePendente.MultiSelect = false;
            this.dg_examePendente.Name = "dg_examePendente";
            this.dg_examePendente.ReadOnly = true;
            this.dg_examePendente.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.CellSelect;
            this.dg_examePendente.Size = new System.Drawing.Size(758, 399);
            this.dg_examePendente.TabIndex = 0;
            // 
            // btn_fechar
            // 
            this.btn_fechar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_fechar.Image = global::SWS.Properties.Resources.close;
            this.btn_fechar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btn_fechar.Location = new System.Drawing.Point(15, 6);
            this.btn_fechar.Name = "btn_fechar";
            this.btn_fechar.Size = new System.Drawing.Size(75, 23);
            this.btn_fechar.TabIndex = 5;
            this.btn_fechar.Text = "&Fechar";
            this.btn_fechar.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btn_fechar.UseVisualStyleBackColor = true;
            this.btn_fechar.Click += new System.EventHandler(this.btn_fechar_Click_1);
            // 
            // grbTotal
            // 
            this.grbTotal.Controls.Add(this.lblTotalTitulo);
            this.grbTotal.Controls.Add(this.textTotal);
            this.grbTotal.Location = new System.Drawing.Point(12, 408);
            this.grbTotal.Name = "grbTotal";
            this.grbTotal.Size = new System.Drawing.Size(241, 44);
            this.grbTotal.TabIndex = 38;
            this.grbTotal.TabStop = false;
            this.grbTotal.Text = "Totais";
            // 
            // lblTotalTitulo
            // 
            this.lblTotalTitulo.AutoSize = true;
            this.lblTotalTitulo.Location = new System.Drawing.Point(6, 16);
            this.lblTotalTitulo.Name = "lblTotalTitulo";
            this.lblTotalTitulo.Size = new System.Drawing.Size(146, 13);
            this.lblTotalTitulo.TabIndex = 29;
            this.lblTotalTitulo.Text = "Quantidade de atendimentos:";
            // 
            // textTotal
            // 
            this.textTotal.BackColor = System.Drawing.Color.LightBlue;
            this.textTotal.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textTotal.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textTotal.ForeColor = System.Drawing.Color.Black;
            this.textTotal.Location = new System.Drawing.Point(158, 12);
            this.textTotal.MaxLength = 10;
            this.textTotal.Name = "textTotal";
            this.textTotal.ReadOnly = true;
            this.textTotal.Size = new System.Drawing.Size(71, 23);
            this.textTotal.TabIndex = 30;
            this.textTotal.TabStop = false;
            this.textTotal.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // frmEsocialLoteDetalhe
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(784, 561);
            this.Name = "frmEsocialLoteDetalhe";
            this.Text = "E-SOCIAL EVENTO 2220 - DETALHES DO LOTE ";
            this.spForm.Panel1.ResumeLayout(false);
            this.spForm.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.spForm)).EndInit();
            this.spForm.ResumeLayout(false);
            this.pnlForm.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.banner)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dg_examePendente)).EndInit();
            this.grbTotal.ResumeLayout(false);
            this.grbTotal.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.DataGridView dg_examePendente;
        private System.Windows.Forms.Button btn_fechar;
        private System.Windows.Forms.GroupBox grbTotal;
        private System.Windows.Forms.Label lblTotalTitulo;
        private System.Windows.Forms.TextBox textTotal;
    }
}