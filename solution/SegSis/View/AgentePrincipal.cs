﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using SegSis.Facade;
using SegSis.Entidade;
using SegSis.Excecao;
using SegSis.View.ViewHelper;
using System.Text.RegularExpressions;
using SegSis.View.Resources;
using SegSis.ViewHelper;

namespace SegSis.View
{
    
    public partial class frm_agente : BaseForm
    {

        private static String Msg01 = "Selecione uma linha.";
        private static String Msg02 = "Gostaria realmente de excluir o agente.";
        private static String Msg03 = "Confirmação";
        private static String Msg04 = "Agente excluido com sucesso!";
        private static String Msg05 = "Não é possível Alterar o agente.";
        private static String Msg06 = "O agente é privado do sistema.";
        private static String Msg07 = "O agente já foi usado em um estudo.";
        private static String Msg08 = "Deseja reativar o agente selecionado?";
        private static String Msg09 = "Agente reativado com sucesso.";
        private static String Msg10 = "Somente agente que esteja ativo pode ser alterado.";
        private static String Msg11 = "Somente agente que esteja inativo pode ser reativado.";
        private static String Msg12 = "Somente agente que esteja ativo pode ser excluído.";

        public frm_agente()
        {
            InitializeComponent();
            validaPermissoes();
        }

        private void validaPermissoes()
        {
            PermissionamentoFacade permissionamentoFacade = PermissionamentoFacade.getInstance();

            bt_alterar.Enabled = permissionamentoFacade.hasPermission("AGENTE", "ALTERAR");
            bt_incluir.Enabled = permissionamentoFacade.hasPermission("AGENTE", "INCLUIR");
            bt_excluir.Enabled = permissionamentoFacade.hasPermission("AGENTE", "EXCLUIR");
            bt_detalhar.Enabled = permissionamentoFacade.hasPermission("AGENTE", "DETALHAR");
            btn_reativar.Enabled = permissionamentoFacade.hasPermission("AGENTE", "REATIVAR");
        }

        private void bt_fechar_Click(object sender, EventArgs e)
        {
            this.Close();

        }

        private void bt_limpar_Click(object sender, EventArgs e)
        {
            text_descricao.Text = String.Empty;
            rb_ativo.Checked = true;
            grd_agente.Columns.Clear();
            text_descricao.Focus();
            remontarDataGrid = false;
            carregaComboRisco(cb_tipo);
            
        }

        private void frm_agente_Load(object sender, EventArgs e)
        {
            rb_ativo.Checked = true;
            carregaComboRisco(cb_tipo);
            
        }

        private void bt_pesquisar_Click(object sender, EventArgs e)
        {
            try
            {
                montaDataGrid();
                text_descricao.Focus();
                remontarDataGrid = true;
            }
            catch (Exception Ex)
            {
                MessageBox.Show(Ex.Message);
            }
        }

        public void montaDataGrid()
        {
            PpraFacade autenticacaoFacade = PpraFacade.getInstance();

            Agente agente = new Agente(null, cb_tipo.SelectedIndex != -1 ?  new Risco(Convert.ToInt64(((SelectItem)cb_tipo.SelectedItem).Valor), (String)((SelectItem)cb_tipo.SelectedItem).Nome) : null, text_descricao.Text.Trim(), String.Empty,
                String.Empty, String.Empty, rb_ativo.Checked ? ApplicationConstants.ATIVO : rb_desativado.Checked ? ApplicationConstants.DESATIVADO : String.Empty, 
                false);

            DataSet ds = autenticacaoFacade.findAgenteByFilter(agente);

            grd_agente.DataSource = ds.Tables["Agentes"].DefaultView;

            grd_agente.Columns[0].HeaderText = "id_agente";
            grd_agente.Columns[1].HeaderText = "id_risco";
            grd_agente.Columns[2].HeaderText = "Descrição";
            grd_agente.Columns[3].HeaderText = "Trajetória";
            grd_agente.Columns[4].HeaderText = "Danos";
            grd_agente.Columns[5].HeaderText = "Limites";
            grd_agente.Columns[6].HeaderText = "Situação";
            grd_agente.Columns[7].HeaderText = "Risco";

            grd_agente.Columns[0].Visible = false;
            grd_agente.Columns[1].Visible = false;
        }

        private void bt_incluir_Click(object sender, EventArgs e)
        {
            frm_agente_incluir formAgenteIncluir = new frm_agente_incluir();
            formAgenteIncluir.ShowDialog();

            if (formAgenteIncluir.getAgente() != null)
            {
                text_descricao.Text = formAgenteIncluir.getAgente().getDescricao();
                montaDataGrid();
            }

        }

        private void bt_alterar_Click(object sender, EventArgs e)
        {
            try
            {
                this.Cursor = Cursors.WaitCursor;
                
                if (grd_agente.CurrentRow != null)
                {
                    Int64 id = (Int64)grd_agente.CurrentRow.Cells[0].Value;

                    PpraFacade ppraFacade = PpraFacade.getInstance();

                    Agente agente = ppraFacade.findAgenteById(id);

                    if (!agente.getPrivado())
                    {
                        
                        // testando para saber se o agente pode ser alterado.
                        // caso ele já tenha sido utilizado em algum estudo, ele não poderá ser mais alterado.

                        if (String.Equals(agente.getSituacao(), ApplicationConstants.ATIVO))
                        {
                            if (!ppraFacade.findGheFonteAgenteByAgente(agente))
                            {
                                frm_alterar_agente alterarAgente = new frm_alterar_agente(agente, this);
                                alterarAgente.Show();
                            }
                            else
                            {
                                throw new Exception(Msg05 + System.Environment.NewLine + Msg07);
                            }
                        }
                        else
                        {
                            throw new Exception(Msg10);
                        }
                    }
                    else
                    {
                        throw new Exception(Msg05 + System.Environment.NewLine + Msg06);
                    }
                    
                }
                else
                {
                    MessageBox.Show(Msg01);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }

        private void bt_excluir_Click(object sender, EventArgs e)
        {
            try
            {
                if (grd_agente.CurrentRow != null)
                {
                    PpraFacade autenticacaoFacade = PpraFacade.getInstance();
                    
                    if (MessageBox.Show(Msg02, Msg03, MessageBoxButtons.YesNo) == DialogResult.Yes)
                    {

                        if (String.Equals(grd_agente.CurrentRow.Cells[6].Value.ToString(), ApplicationConstants.ATIVO))
                        {
                            
                            Int32 cellValue = grd_agente.CurrentRow.Index;
                            Int64 id = (Int64)grd_agente.Rows[cellValue].Cells[0].Value;

                            autenticacaoFacade.alteraStatusAgente(id, ApplicationConstants.DESATIVADO);

                            MessageBox.Show(Msg04);

                            montaDataGrid();
                        }
                        else
                        {
                            throw new Exception(Msg12);
                        }
                    }

                }
                else
                {
                    MessageBox.Show(Msg01);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void bt_detalhar_Click(object sender, EventArgs e)
        {
            Agente agente = null;
            try
            {
                if (grd_agente.CurrentRow != null)
                {
                    PpraFacade autenticacaoFacade = PpraFacade.getInstance();
                    
                    Int64 id = (Int64)grd_agente.CurrentRow.Cells[0].Value; 
                    
                    agente = autenticacaoFacade.findAgenteById(id);
                    
                    frm_detalhar_agente detalharAgente = new frm_detalhar_agente(agente, this);
                    detalharAgente.ShowDialog();

                }
                else
                {
                    MessageBox.Show(Msg01);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void carregaComboRisco(ComboBox comboRisco)
        {
            try
            {
                PpraFacade ppraFacade = PpraFacade.getInstance();

                Risco risco = new Risco();

                comboRisco.Items.Clear();

                comboRisco.DisplayMember = "nome";
                comboRisco.ValueMember = "valor";

                foreach (DataRow rows in ppraFacade.findRisco().Tables[0].Rows)
                {
                    comboRisco.Items.Add(new SelectItem(Convert.ToString(rows[Convert.ToString("id_risco")]), Convert.ToString(rows[Convert.ToString("descricao")])));
                }

                
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void btn_reativar_Click(object sender, EventArgs e)
        {
            try
            {
                if (grd_agente.CurrentRow != null)
                {
                    PpraFacade autenticacaoFacade = PpraFacade.getInstance();

                    if (MessageBox.Show(Msg08, Msg03, MessageBoxButtons.YesNo) == DialogResult.Yes)
                    {
                        if (String.Equals(grd_agente.CurrentRow.Cells[6].Value.ToString(), ApplicationConstants.DESATIVADO))
                        {

                            Int64 id = (Int64)grd_agente.CurrentRow.Cells[0].Value;

                            autenticacaoFacade.alteraStatusAgente(id, ApplicationConstants.ATIVO);

                            MessageBox.Show(Msg09);

                            montaDataGrid();
                        }
                        else
                        {
                            throw new Exception(Msg11);
                        }
                    }

                }
                else
                {
                    MessageBox.Show(Msg01);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

    }
}
