﻿using SWS.Entidade;
using SWS.Facade;
using SWS.View.Resources;
using SWS.View.ViewHelper;
using SWS.ViewHelper;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace SWS.View
{
    public partial class frmSetores : frmTemplate
    {
        private Setor setor;

        public Setor Setor
        {
            get { return setor; }
            set { setor = value; }
        }
        
        public frmSetores()
        {
            InitializeComponent();
            ComboHelper.comboSituacao(cbSituacao);
            validaPermissoes();
        }

        private void validaPermissoes()
        {
            PermissionamentoFacade permissionamentoFacade = PermissionamentoFacade.getInstance();

            bt_alterar.Enabled = permissionamentoFacade.hasPermission("SETOR", "ALTERAR");
            bt_incluir.Enabled = permissionamentoFacade.hasPermission("SETOR", "INCLUIR");
            bt_excluir.Enabled = permissionamentoFacade.hasPermission("SETOR", "EXCLUIR");
            bt_detalhar.Enabled = permissionamentoFacade.hasPermission("SETOR", "DETALHAR");
            btn_reativar.Enabled = permissionamentoFacade.hasPermission("SETOR", "REATIVAR");
        }

        private void bt_fechar_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void bt_incluir_Click(object sender, EventArgs e)
        {
            frmSetorIncluir incluirSetor = new frmSetorIncluir();
            incluirSetor.ShowDialog();

            if (incluirSetor.Setor != null)
            {
                Setor = incluirSetor.Setor;
                textSetor.Text = Setor.Descricao;
                montaDataGrid();
            }
        }

        private void bt_alterar_Click(object sender, EventArgs e)
        {
            try
            {
                this.Cursor = Cursors.WaitCursor;

                if (grd_setor.CurrentRow == null)
                    throw new Exception("Selecione uma linha");

                EstudoFacade pcmsoFacade = EstudoFacade.getInstance();
                Setor setorAltera = pcmsoFacade.findSetorById((long)grd_setor.CurrentRow.Cells[0].Value);

                if (!string.Equals(setorAltera.Situacao, ApplicationConstants.ATIVO))
                    throw new Exception("Somente setores ativos podem ser alterados.");

                frmSetorAltera alteraSetor = new frmSetorAltera(setorAltera);
                alteraSetor.ShowDialog();

                if (alteraSetor.FlagAltera == true)
                {
                    Setor = alteraSetor.Setor;
                    textSetor.Text = Setor.Descricao;
                    montaDataGrid();
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }

        }

        private void bt_detalhar_Click(object sender, EventArgs e)
        {
            try
            {
                if (grd_setor.CurrentRow == null)
                    throw new Exception("Selecione uma linha");

                EstudoFacade pcmsoFacade = EstudoFacade.getInstance();
                Setor setorDetalhar = pcmsoFacade.findSetorById((long)grd_setor.CurrentRow.Cells[0].Value);

                frmSetorDetalhar detalharSetor = new frmSetorDetalhar(setorDetalhar);
                detalharSetor.ShowDialog();

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }

        private void bt_excluir_Click(object sender, EventArgs e)
        {
            try
            {
                this.Cursor = Cursors.WaitCursor;

                if (grd_setor.CurrentRow == null)
                    throw new Exception("Selecione uma linha.");

                EstudoFacade pcmsoFacade = EstudoFacade.getInstance();
                Setor setorExcluir = pcmsoFacade.findSetorById((long)grd_setor.CurrentRow.Cells[0].Value);

                if (!string.Equals(setorExcluir.Situacao, ApplicationConstants.ATIVO))
                    throw new Exception("Somente setores ativos podem ser excluídos.");
                setorExcluir.Situacao = ApplicationConstants.DESATIVADO;

                pcmsoFacade.updateSetor(setorExcluir);

                MessageBox.Show("Setor excluído com suceso.", "Confirmação", MessageBoxButtons.OK, MessageBoxIcon.Information);

                textSetor.Text = setorExcluir.Descricao;
                montaDataGrid();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }


        }

        private void btn_reativar_Click(object sender, EventArgs e)
        {
            try
            {
                this.Cursor = Cursors.WaitCursor;

                if (grd_setor.CurrentRow == null)
                    throw new Exception("Selecione uma linha.");

                EstudoFacade pcmsoFacade = EstudoFacade.getInstance();
                Setor setorReativar = pcmsoFacade.findSetorById((long)grd_setor.CurrentRow.Cells[0].Value);

                if (!string.Equals(setorReativar.Situacao, ApplicationConstants.DESATIVADO))
                    throw new Exception("Somente setores excluídos podem ser excluídos.");

                setorReativar.Situacao = ApplicationConstants.ATIVO;

                pcmsoFacade.updateSetor(setorReativar);

                MessageBox.Show("Setor reativado com suceso.", "Confirmação", MessageBoxButtons.OK, MessageBoxIcon.Information);

                textSetor.Text = setorReativar.Descricao;
                montaDataGrid();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }

        }

        private void bt_pesquisar_Click(object sender, EventArgs e)
        {
            montaDataGrid();
            textSetor.Focus();
        }

        public void montaDataGrid()
        {
            try
            {
                this.Cursor = Cursors.WaitCursor;

                EstudoFacade pcmsoFacade = EstudoFacade.getInstance();

                setor = new Setor(null, textSetor.Text.Trim(), ((SelectItem)cbSituacao.SelectedItem).Valor);

                DataSet ds = pcmsoFacade.findSetorByFilter(setor);

                grd_setor.DataSource = ds.Tables["Setores"].DefaultView;

                grd_setor.Columns[0].HeaderText = "ID";
                grd_setor.Columns[1].HeaderText = "Descrição";
                grd_setor.Columns[2].HeaderText = "Situação";

                grd_setor.Columns[0].Visible = false;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }

        private void grd_setor_RowPrePaint(object sender, DataGridViewRowPrePaintEventArgs e)
        {
            DataGridView dgv = sender as DataGridView;

            if (String.Equals(dgv.Rows[e.RowIndex].Cells[2].Value, ApplicationConstants.DESATIVADO))
                dgv.Rows[e.RowIndex].DefaultCellStyle.ForeColor = Color.Red;

        }

        private void grd_setor_CellMouseDoubleClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            bt_alterar.PerformClick();
        }
    }
}
