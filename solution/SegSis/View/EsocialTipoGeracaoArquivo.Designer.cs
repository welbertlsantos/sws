﻿namespace SWS.View
{
    partial class frmEsocialTipoGeracaoArquivo
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmEsocialTipoGeracaoArquivo));
            this.btnIndividual = new System.Windows.Forms.Button();
            this.btnAgrupado = new System.Windows.Forms.Button();
            this.lblIndividual = new System.Windows.Forms.Label();
            this.lblAgrupado = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // btnIndividual
            // 
            this.btnIndividual.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnIndividual.Image = global::SWS.Properties.Resources.individual;
            this.btnIndividual.Location = new System.Drawing.Point(91, 29);
            this.btnIndividual.Name = "btnIndividual";
            this.btnIndividual.Size = new System.Drawing.Size(102, 52);
            this.btnIndividual.TabIndex = 0;
            this.btnIndividual.UseVisualStyleBackColor = true;
            this.btnIndividual.Click += new System.EventHandler(this.btnIndividual_Click);
            // 
            // btnAgrupado
            // 
            this.btnAgrupado.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnAgrupado.Image = global::SWS.Properties.Resources.agrupado;
            this.btnAgrupado.Location = new System.Drawing.Point(235, 28);
            this.btnAgrupado.Name = "btnAgrupado";
            this.btnAgrupado.Size = new System.Drawing.Size(102, 52);
            this.btnAgrupado.TabIndex = 1;
            this.btnAgrupado.UseVisualStyleBackColor = true;
            this.btnAgrupado.Click += new System.EventHandler(this.btnAgrupado_Click);
            // 
            // lblIndividual
            // 
            this.lblIndividual.AutoSize = true;
            this.lblIndividual.Location = new System.Drawing.Point(118, 84);
            this.lblIndividual.Name = "lblIndividual";
            this.lblIndividual.Size = new System.Drawing.Size(52, 13);
            this.lblIndividual.TabIndex = 2;
            this.lblIndividual.Text = "&Individual";
            // 
            // lblAgrupado
            // 
            this.lblAgrupado.AutoSize = true;
            this.lblAgrupado.Location = new System.Drawing.Point(260, 83);
            this.lblAgrupado.Name = "lblAgrupado";
            this.lblAgrupado.Size = new System.Drawing.Size(53, 13);
            this.lblAgrupado.TabIndex = 3;
            this.lblAgrupado.Text = "&Agrupado";
            // 
            // frmEsocialTipoGeracaoArquivo
            // 
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(441, 120);
            this.ControlBox = false;
            this.Controls.Add(this.lblAgrupado);
            this.Controls.Add(this.lblIndividual);
            this.Controls.Add(this.btnAgrupado);
            this.Controls.Add(this.btnIndividual);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmEsocialTipoGeracaoArquivo";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "SELECIONAR O TIPO DE GERAÇÃO DO ARQUIVO";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.FlowLayoutPanel flpAcao;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button btnIndividual;
        private System.Windows.Forms.Button btnAgrupado;
        private System.Windows.Forms.Label lblIndividual;
        private System.Windows.Forms.Label lblAgrupado;
    }
}