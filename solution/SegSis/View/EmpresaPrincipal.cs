﻿using SWS.Entidade;
using SWS.Facade;
using SWS.View.Resources;
using SWS.View.ViewHelper;
using SWS.ViewHelper;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace SWS.View
{
    public partial class frmEmpresaPrincipal : frmTemplate
    {
        private Empresa empresa;
        
        public frmEmpresaPrincipal()
        {
            InitializeComponent();
            validaPermissoes();
            ComboHelper.comboSituacao(cbSituacao);
        }

        private void bt_incluir_Click(object sender, EventArgs e)
        {
            frmEmpresaIncluir formEmpresaIncluir = new frmEmpresaIncluir();
            formEmpresaIncluir.ShowDialog();

            if (formEmpresaIncluir.Empresa != null)
            {
                empresa = formEmpresaIncluir.Empresa;
                montaDataGrid();
            }
        }

        private void bt_alterar_Click(object sender, EventArgs e)
        {
            try
            {
                if (this.dgv_empresa.CurrentRow == null)
                    throw new Exception ("Selecione uma empresa");

                EmpresaFacade empresaFacade = EmpresaFacade.getInstance();

                empresa = empresaFacade.findEmpresaById((Int64)dgv_empresa.CurrentRow.Cells[0].Value);

                if (!String.Equals(empresa.Situacao, ApplicationConstants.ATIVO))
                    throw new Exception("Somente empresas ativas podem ser desativadas.");

                frmEmpresaAlterar formEmpresaAlterar = new frmEmpresaAlterar(empresa);
                formEmpresaAlterar.ShowDialog();
                montaDataGrid();

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void bt_excluir_Click(object sender, EventArgs e)
        {
            try
            {
                if (this.dgv_empresa.CurrentRow == null)
                    throw new Exception ("Selecione uma empresa.");
                
                EmpresaFacade empresaFacade = EmpresaFacade.getInstance();

                if (MessageBox.Show("Deseja desativar a empresa selecionada?", "Confirmação", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                {
                    empresa = empresaFacade.findEmpresaById((Int64)dgv_empresa.CurrentRow.Cells[0].Value);

                    if (!String.Equals(empresa.Situacao, ApplicationConstants.ATIVO))
                        throw new Exception("Somente empresas ativas podem ser desativadas.");

                    empresa.Situacao = ApplicationConstants.DESATIVADO;
                    
                    empresaFacade.updateEmpresa(empresa);

                    MessageBox.Show("Empresa desativada com sucesso.","Confirmação", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    
                    montaDataGrid();
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btn_reativar_Click(object sender, EventArgs e)
        {
            try
            {
                if (this.dgv_empresa.CurrentRow == null)
                    throw new Exception ("Selecione uma empresa.");
                
                EmpresaFacade empresaFacade = EmpresaFacade.getInstance();
                
                if (MessageBox.Show("Deseja reativar a empresa selecionada?", "Confirmação", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                {
                    empresa = empresaFacade.findEmpresaById((Int64)dgv_empresa.CurrentRow.Cells[0].Value);

                    if (!String.Equals(empresa.Situacao, ApplicationConstants.DESATIVADO))
                        throw new Exception ("Somente empresas desativadas podem ser reativadas.");

                    empresa.Situacao = ApplicationConstants.ATIVO;
                    
                    empresaFacade.updateEmpresa(empresa);

                    MessageBox.Show("Empresa reativada com sucesso.", "Confirmação", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        
                    montaDataGrid();
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void bt_detalhar_Click(object sender, EventArgs e)
        {
            try
            {
                if (this.dgv_empresa.CurrentRow == null)
                    throw new Exception("Selecione uma empresa.");

                EmpresaFacade empresaFacade = EmpresaFacade.getInstance();

                frmEmpresaDetalhar formEmpresaDetalhar = new frmEmpresaDetalhar(empresaFacade.findEmpresaById((Int64)dgv_empresa.CurrentRow.Cells[0].Value));
                
                formEmpresaDetalhar.ShowDialog();

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void bt_limpar_Click(object sender, EventArgs e)
        {
            this.text_razaoSocial.Text = String.Empty;
            this.text_cnpj.Text = String.Empty;
            this.text_razaoSocial.Focus();
            ComboHelper.comboSituacao(cbSituacao);
            this.dgv_empresa.Columns.Clear();
        }

        private void bt_pesquisar_Click(object sender, EventArgs e)
        {
            try
            {
                empresa = new Empresa(null, text_razaoSocial.Text, String.Empty, text_cnpj.Text.Trim(), String.Empty, String.Empty, String.Empty, string.Empty, String.Empty, String.Empty, String.Empty, String.Empty, String.Empty, string.Empty, String.Empty, null, ((SelectItem)cbSituacao.SelectedItem).Valor, String.Empty, null, null, false, null, string.Empty, 0, 0, 0, 0, 0, 0, 0);
                montaDataGrid();
                text_razaoSocial.Focus();
            }
            catch (Exception Ex)
            {
                MessageBox.Show(Ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void bt_fechar_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void dgv_empresa_RowPrePaint(object sender, DataGridViewRowPrePaintEventArgs e)
        {
            DataGridView dgv = sender as DataGridView;

            if (String.Equals(dgv.Rows[e.RowIndex].Cells[17].Value, "I"))
                dgv.Rows[e.RowIndex].DefaultCellStyle.ForeColor = Color.Red;

        }

        public void montaDataGrid()
        {
            try
            {
                EmpresaFacade empresaFacade = EmpresaFacade.getInstance();

                dgv_empresa.Columns.Clear();

                DataSet ds = empresaFacade.findEmpresaByFilter(empresa);

                dgv_empresa.DataSource = ds.Tables["Empresas"].DefaultView;

                dgv_empresa.Columns[0].HeaderText = "ID";
                dgv_empresa.Columns[0].Visible = false;

                dgv_empresa.Columns[1].HeaderText = "Razão Social";
                dgv_empresa.Columns[2].HeaderText = "Nome Fantasia";
                dgv_empresa.Columns[3].HeaderText = "CNPJ";
                dgv_empresa.Columns[4].HeaderText = "Inscrição Estadual";
                dgv_empresa.Columns[5].HeaderText = "Endereço";
                dgv_empresa.Columns[6].HeaderText = "Número";
                dgv_empresa.Columns[7].HeaderText = "Complemento";
                dgv_empresa.Columns[8].HeaderText = "Bairro";
                dgv_empresa.Columns[9].HeaderText = "Cidade";
                dgv_empresa.Columns[10].HeaderText = "CEP";
                dgv_empresa.Columns[11].HeaderText = "UF";
                dgv_empresa.Columns[12].HeaderText = "Telefone";
                dgv_empresa.Columns[13].HeaderText = "FAX";
                dgv_empresa.Columns[14].HeaderText = "Email";
                dgv_empresa.Columns[15].HeaderText = "Site";
                dgv_empresa.Columns[16].HeaderText = "Data Cadastro";

                dgv_empresa.Columns[17].HeaderText = "Situação";
                dgv_empresa.Columns[17].Visible = false;

                dgv_empresa.Columns[18].HeaderText = "logo";
                dgv_empresa.Columns[18].Visible = false;

                dgv_empresa.Columns[19].HeaderText = "mimetype";
                dgv_empresa.Columns[19].Visible = false;

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void validaPermissoes()
        {
            PermissionamentoFacade permissionamentoFacade = PermissionamentoFacade.getInstance();

            bt_alterar.Enabled = permissionamentoFacade.hasPermission("EMPRESA", "ALTERAR");
            bt_incluir.Enabled = permissionamentoFacade.hasPermission("EMPRESA", "INCLUIR");
            bt_excluir.Enabled = permissionamentoFacade.hasPermission("EMPRESA", "EXCLUIR");
            bt_detalhar.Enabled = permissionamentoFacade.hasPermission("EMPRESA", "DETALHAR");
            btn_reativar.Enabled = permissionamentoFacade.hasPermission("EMPRESA", "REATIVAR");
        }

        private void dgv_empresa_DataBindingComplete(object sender, DataGridViewBindingCompleteEventArgs e)
        {
            DataGridView dgv = sender as DataGridView;
            dgv.ClearSelection();
        }

        private void dgv_empresa_CellMouseDoubleClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            try
            {
                bt_alterar.PerformClick();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }

    }
}
