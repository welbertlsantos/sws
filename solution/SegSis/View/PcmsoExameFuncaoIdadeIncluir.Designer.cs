﻿namespace SegSis.View
{
    partial class PcmsoExameFuncaoIdadeIncluir
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.grd_periodicidade = new System.Windows.Forms.DataGridView();
            this.grb_periodicidade = new System.Windows.Forms.GroupBox();
            this.grb_exame = new System.Windows.Forms.GroupBox();
            this.text_idadeExame = new System.Windows.Forms.TextBox();
            this.lbl_idadeExame = new System.Windows.Forms.Label();
            this.text_descricao = new System.Windows.Forms.TextBox();
            this.lbl_descricao = new System.Windows.Forms.Label();
            this.btn_fechar = new System.Windows.Forms.Button();
            this.btn_incluir = new System.Windows.Forms.Button();
            this.btn_excluirExcecao = new System.Windows.Forms.Button();
            this.btn_incluirExcecao = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.grd_periodicidade)).BeginInit();
            this.grb_periodicidade.SuspendLayout();
            this.grb_exame.SuspendLayout();
            this.SuspendLayout();
            // 
            // grd_periodicidade
            // 
            this.grd_periodicidade.AllowUserToAddRows = false;
            this.grd_periodicidade.AllowUserToDeleteRows = false;
            this.grd_periodicidade.AllowUserToOrderColumns = true;
            this.grd_periodicidade.AllowUserToResizeRows = false;
            this.grd_periodicidade.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this.grd_periodicidade.BackgroundColor = System.Drawing.Color.White;
            this.grd_periodicidade.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.grd_periodicidade.Location = new System.Drawing.Point(9, 47);
            this.grd_periodicidade.Name = "grd_periodicidade";
            this.grd_periodicidade.Size = new System.Drawing.Size(596, 197);
            this.grd_periodicidade.TabIndex = 3;
            // 
            // grb_periodicidade
            // 
            this.grb_periodicidade.Controls.Add(this.btn_excluirExcecao);
            this.grb_periodicidade.Controls.Add(this.btn_incluirExcecao);
            this.grb_periodicidade.Controls.Add(this.text_idadeExame);
            this.grb_periodicidade.Controls.Add(this.grd_periodicidade);
            this.grb_periodicidade.Controls.Add(this.lbl_idadeExame);
            this.grb_periodicidade.Location = new System.Drawing.Point(8, 86);
            this.grb_periodicidade.Name = "grb_periodicidade";
            this.grb_periodicidade.Size = new System.Drawing.Size(695, 250);
            this.grb_periodicidade.TabIndex = 2;
            this.grb_periodicidade.TabStop = false;
            this.grb_periodicidade.Text = "Periodicidade do Exame";
            // 
            // grb_exame
            // 
            this.grb_exame.Controls.Add(this.text_descricao);
            this.grb_exame.Controls.Add(this.lbl_descricao);
            this.grb_exame.Location = new System.Drawing.Point(8, 12);
            this.grb_exame.Name = "grb_exame";
            this.grb_exame.Size = new System.Drawing.Size(695, 68);
            this.grb_exame.TabIndex = 3;
            this.grb_exame.TabStop = false;
            this.grb_exame.Text = "Exames";
            // 
            // text_idadeExame
            // 
            this.text_idadeExame.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.text_idadeExame.Enabled = false;
            this.text_idadeExame.ForeColor = System.Drawing.Color.Blue;
            this.text_idadeExame.Location = new System.Drawing.Point(190, 21);
            this.text_idadeExame.MaxLength = 3;
            this.text_idadeExame.Name = "text_idadeExame";
            this.text_idadeExame.Size = new System.Drawing.Size(37, 20);
            this.text_idadeExame.TabIndex = 2;
            this.text_idadeExame.Text = "0";
            this.text_idadeExame.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // lbl_idadeExame
            // 
            this.lbl_idadeExame.AutoSize = true;
            this.lbl_idadeExame.Location = new System.Drawing.Point(10, 23);
            this.lbl_idadeExame.Name = "lbl_idadeExame";
            this.lbl_idadeExame.Size = new System.Drawing.Size(174, 13);
            this.lbl_idadeExame.TabIndex = 2;
            this.lbl_idadeExame.Text = "Idade mínima para realizar o exame";
            // 
            // text_descricao
            // 
            this.text_descricao.BackColor = System.Drawing.Color.White;
            this.text_descricao.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.text_descricao.ForeColor = System.Drawing.SystemColors.MenuHighlight;
            this.text_descricao.Location = new System.Drawing.Point(9, 32);
            this.text_descricao.MaxLength = 100;
            this.text_descricao.Name = "text_descricao";
            this.text_descricao.ReadOnly = true;
            this.text_descricao.Size = new System.Drawing.Size(677, 20);
            this.text_descricao.TabIndex = 1;
            this.text_descricao.TabStop = false;
            // 
            // lbl_descricao
            // 
            this.lbl_descricao.AutoSize = true;
            this.lbl_descricao.Location = new System.Drawing.Point(6, 16);
            this.lbl_descricao.Name = "lbl_descricao";
            this.lbl_descricao.Size = new System.Drawing.Size(55, 13);
            this.lbl_descricao.TabIndex = 0;
            this.lbl_descricao.Text = "Descrição";
            // 
            // btn_fechar
            // 
            this.btn_fechar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_fechar.Location = new System.Drawing.Point(98, 355);
            this.btn_fechar.Name = "btn_fechar";
            this.btn_fechar.Size = new System.Drawing.Size(75, 23);
            this.btn_fechar.TabIndex = 7;
            this.btn_fechar.Text = "&Fechar";
            this.btn_fechar.UseVisualStyleBackColor = true;
            // 
            // btn_incluir
            // 
            this.btn_incluir.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_incluir.Location = new System.Drawing.Point(17, 355);
            this.btn_incluir.Name = "btn_incluir";
            this.btn_incluir.Size = new System.Drawing.Size(75, 23);
            this.btn_incluir.TabIndex = 6;
            this.btn_incluir.Text = "&Gravar";
            this.btn_incluir.UseVisualStyleBackColor = true;
            // 
            // btn_excluirExcecao
            // 
            this.btn_excluirExcecao.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_excluirExcecao.Location = new System.Drawing.Point(611, 76);
            this.btn_excluirExcecao.Name = "btn_excluirExcecao";
            this.btn_excluirExcecao.Size = new System.Drawing.Size(75, 23);
            this.btn_excluirExcecao.TabIndex = 8;
            this.btn_excluirExcecao.Text = "&Excluir";
            this.btn_excluirExcecao.UseVisualStyleBackColor = true;
            // 
            // btn_incluirExcecao
            // 
            this.btn_incluirExcecao.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_incluirExcecao.Location = new System.Drawing.Point(611, 47);
            this.btn_incluirExcecao.Name = "btn_incluirExcecao";
            this.btn_incluirExcecao.Size = new System.Drawing.Size(75, 23);
            this.btn_incluirExcecao.TabIndex = 7;
            this.btn_incluirExcecao.Text = "&Incluir";
            this.btn_incluirExcecao.UseVisualStyleBackColor = true;
            // 
            // PcmsoExameFuncaoIdadeIncluir
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(713, 399);
            this.Controls.Add(this.btn_fechar);
            this.Controls.Add(this.btn_incluir);
            this.Controls.Add(this.grb_exame);
            this.Controls.Add(this.grb_periodicidade);
            this.Name = "PcmsoExameFuncaoIdadeIncluir";
            this.Text = "INCLUIR IDADE MÍNIMA PARA EXAME POR FUNÇÃO NO PCMSO";
            ((System.ComponentModel.ISupportInitialize)(this.grd_periodicidade)).EndInit();
            this.grb_periodicidade.ResumeLayout(false);
            this.grb_periodicidade.PerformLayout();
            this.grb_exame.ResumeLayout(false);
            this.grb_exame.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.DataGridView grd_periodicidade;
        private System.Windows.Forms.GroupBox grb_periodicidade;
        public System.Windows.Forms.TextBox text_idadeExame;
        private System.Windows.Forms.Label lbl_idadeExame;
        private System.Windows.Forms.GroupBox grb_exame;
        public System.Windows.Forms.TextBox text_descricao;
        private System.Windows.Forms.Label lbl_descricao;
        private System.Windows.Forms.Button btn_fechar;
        private System.Windows.Forms.Button btn_incluir;
        private System.Windows.Forms.Button btn_excluirExcecao;
        private System.Windows.Forms.Button btn_incluirExcecao;
    }
}