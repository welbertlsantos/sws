﻿using SWS.Entidade;
using SWS.Excecao;
using SWS.Facade;
using SWS.Helper;
using SWS.View.Resources;
using SWS.View.ViewHelper;
using SWS.ViewHelper;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Security;
using System.Text;
using System.Text.RegularExpressions;
using System.Windows.Forms;

namespace SWS.View
{
    public partial class frmEmpresaIncluir : frmTemplate
    {
        protected Empresa empresa;

        public Empresa Empresa
        {
            get { return empresa; }
            set { empresa = value; }
        }

        protected Arquivo arquivo;

        protected Empresa matriz;

        public frmEmpresaIncluir()
        {
            InitializeComponent();
            ComboHelper.unidadeFederativa(cbUf);
            ComboHelper.Boolean(cbSimples, false);
            ActiveControl = text_razaoSocial;
        }

        protected virtual void btn_gravar_Click(object sender, EventArgs e)
        {
            try
            {
                this.empresaErrorProvider.Clear();

                if (validaCamposObrigatorio())
                {

                    EmpresaFacade empresaFacade = EmpresaFacade.getInstance();
                    ClienteFacade clienteFacade = ClienteFacade.getInstance();
                    
                    empresa = empresaFacade.insertEmpresa(new Empresa(null, text_razaoSocial.Text.Trim(), text_fantasia.Text.Trim(), text_cnpj.Text, text_inscricao.Text, text_endereco.Text.Trim(), text_numero.Text.Trim(), text_complemento.Text.Trim(), text_bairro.Text.Trim(), text_cep.Text, ((SelectItem)cbUf.SelectedItem).Valor, text_telefone1.Text, text_telefone2.Text, text_email.Text, text_site.Text, null, ApplicationConstants.ATIVO, arquivo != null ? arquivo.Mimetype : null, arquivo != null ? arquivo.Conteudo : null, clienteFacade.findCidadeIbgeById(Convert.ToInt64(((SelectItem)cbCidade.SelectedItem).Valor)), Convert.ToBoolean(((SelectItem)cbSimples.SelectedItem).Valor), matriz, textCodigoCnes.Text, Convert.ToDecimal(textValorMinimoImpostoFederal.Text), Convert.ToDecimal(textValorMinimoIR.Text), Convert.ToDecimal(textAliquotaPIS.Text), Convert.ToDecimal(textAliquotaCOFINS.Text), Convert.ToDecimal(textAliquotaIR.Text), Convert.ToDecimal(textAliquotaCSLL.Text), Convert.ToDecimal(textAliquotaISS.Text)));

                    MessageBox.Show("Empresa incluída com sucesso", "Confirmação", MessageBoxButtons.OK, MessageBoxIcon.Information);

                    this.Close();

                }
            }
            catch (Exception Ex)
            {
                MessageBox.Show(Ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        protected void lbl_limpar_Click(object sender, EventArgs e)
        {
            text_razaoSocial.Text = String.Empty;
            text_cnpj.Text = String.Empty;
            text_fantasia.Text = String.Empty;
            text_inscricao.Text = String.Empty;

            text_endereco.Text = String.Empty;
            text_numero.Text = String.Empty;
            text_complemento.Text = String.Empty;
            text_bairro.Text = String.Empty;
            text_cep.Text = String.Empty;
            ComboHelper.unidadeFederativa(cbUf);
            cbCidade.DataSource = null;

            text_telefone1.Text = String.Empty;
            text_telefone2.Text = String.Empty;
            text_email.Text = String.Empty;
            text_site.Text = String.Empty;
            ComboHelper.Boolean(cbSimples, false);

            this.empresaErrorProvider.Clear();
            arquivo = null;
            pic_box_logo.Image = null;
            pic_box_logo.Refresh();

            this.matriz = null;
            textMatriz.Text = string.Empty;

            text_razaoSocial.Focus();
            textCodigoCnes.Text = string.Empty;

            textValorMinimoImpostoFederal.Text = "0.00";
            textValorMinimoIR.Text = "0.00";
            textAliquotaPIS.Text = "0.00";
            textAliquotaCOFINS.Text = "0.00";
            textAliquotaIR.Text = "0.00";
            textAliquotaCSLL.Text = "0.00";

        }

        protected void btn_fechar_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        protected void bt_abrir_file_dialog_Click(object sender, EventArgs e)
        {
            OpenFileDialog clienteLogoFileDialog = new OpenFileDialog();

            clienteLogoFileDialog.Multiselect = false;
            clienteLogoFileDialog.Title = "Selecionar Logo";
            clienteLogoFileDialog.InitialDirectory = @"C:\";
            //filtra para exibir somente arquivos de imagens
            clienteLogoFileDialog.Filter = "Images(*.JPG) |*.JPG";
            clienteLogoFileDialog.CheckFileExists = true;
            clienteLogoFileDialog.CheckPathExists = true;
            clienteLogoFileDialog.FilterIndex = 2;
            clienteLogoFileDialog.RestoreDirectory = true;
            clienteLogoFileDialog.ReadOnlyChecked = true;
            clienteLogoFileDialog.ShowReadOnly = true;

            DialogResult dr = clienteLogoFileDialog.ShowDialog();

            if (dr == System.Windows.Forms.DialogResult.OK)
            {
                String nomeArquivo = clienteLogoFileDialog.FileName;
                String nomeArquivoFinal = nomeArquivo.Substring(nomeArquivo.LastIndexOf('\\') + 1);

                // cria um PictureBox
                try
                {
                    Image Imagem = Image.FromFile(nomeArquivo);
                    pic_box_logo.SizeMode = PictureBoxSizeMode.StretchImage;
                    if (Imagem.Width > 500 && Imagem.Height > 500)
                        throw new LogoException("A imagem deve ter um tamanho máximo de 500 X 500 pixels.");

                    pic_box_logo.Image = Imagem;

                    byte[] conteudo = FileHelper.CarregarArquivoImagem(nomeArquivo, 5242880);

                    arquivo = new Arquivo(null, nomeArquivoFinal, "image/jpeg", conteudo.Length, conteudo);
                }
                catch (SecurityException ex)
                {
                    // O usuário  não possui permissão para ler arquivos
                    MessageBox.Show("Erro de segurança Contate o administrador de segurança da rede.\n\n" + "Mensagem : " + ex.Message + "\n\n" + "Detalhes (enviar ao suporte):\n\n" + ex.StackTrace, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
                catch (LogoException ex)
                {
                    MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
                catch (Exception ex)
                {
                    // Não pode carregar a imagem (problemas de permissão)
                    MessageBox.Show("Não é possível exibir a imagem : " + nomeArquivoFinal + ". Você pode não ter permissão para ler o arquivo , ou " + " ele pode estar corrompido.\n\nErro reportado : " + ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error); 
                }
            }
        }

        protected void frmEmpresaIncluir_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter || e.KeyCode == Keys.Return)
                SendKeys.Send("{TAB}");

        }

        protected bool validaCamposObrigatorio()
        {
            bool retorno = true;
            try
            {
                if (string.IsNullOrEmpty(text_razaoSocial.Text))
                {
                    this.empresaErrorProvider.SetError(this.text_razaoSocial, "A Razão Social da empresa é obrigatória!");
                    retorno = false;
                }

                if (string.IsNullOrEmpty(text_cnpj.Text.Replace(".", "").Replace(",", "").Replace("-", "").Replace("/", "")))
                {
                    this.empresaErrorProvider.SetError(this.text_cnpj, "O CNPJ da empresa é obrigatório!");
                    retorno = false;
                }

                if (string.IsNullOrEmpty(text_inscricao.Text))
                {
                    this.empresaErrorProvider.SetError(this.text_inscricao, "A inscrição não pode ser nula!");
                    retorno = false;
                }

                if (Regex.IsMatch(Convert.ToString(this.text_cnpj), @"\d"))
                {
                    if (!ValidaCampoHelper.ValidaCNPJ(this.text_cnpj.Text))
                    {
                        this.empresaErrorProvider.SetError(this.text_cnpj, "CNPJ inválido. Verifique os valores digitados.");
                        retorno = false;
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            
            return retorno;

        }

        protected void cbUf_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                if (cbUf.SelectedIndex == -1)
                    throw new Exception("Selecione uma Unidade Federativa primeiro.");

                ComboHelper.iniciaComboCidade(cbCidade, ((SelectItem)cbUf.SelectedItem).Valor.ToString());

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        protected void cbCidade_Click(object sender, EventArgs e)
        {
            try
            {
                if (cbUf.SelectedIndex == 0)
                    throw new Exception("Selecione uma unidade federativa.");

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        protected virtual void btnMatriz_Click(object sender, EventArgs e)
        {
            try
            {

                /* buscando empresa matriz */

                frmEmpresaBuscar buscarEmpresa = new frmEmpresaBuscar();
                buscarEmpresa.ShowDialog();

                if (buscarEmpresa.Empresa != null)
                {
                    matriz = buscarEmpresa.Empresa;
                    textMatriz.Text = matriz.RazaoSocial;
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void textCodigoCnes_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = ValidaCampoHelper.FormataDigitacaoNumero(sender, e, textCodigoCnes.Text, 2);
        }

        private void textValorMinimoISS_Enter(object sender, EventArgs e)
        {
            textValorMinimoImpostoFederal.Tag = textValorMinimoImpostoFederal.Text;
            textValorMinimoImpostoFederal.Text = string.Empty;
        }

        private void textValorMinimoISS_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = ValidaCampoHelper.FormataDigitacaoNumero(sender, e, textValorMinimoImpostoFederal.Text, 2);
        }

        private void textValorMinimoISS_Leave(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(textValorMinimoImpostoFederal.Text))
            {
                textValorMinimoImpostoFederal.Text = textValorMinimoImpostoFederal.Tag.ToString();
            }

            textValorMinimoImpostoFederal.Text = ValidaCampoHelper.FormataValorMonetario(textValorMinimoImpostoFederal.Text);
        }

        private void textValorMinimoIR_Enter(object sender, EventArgs e)
        {
            textValorMinimoIR.Tag = textValorMinimoIR.Text;
            textValorMinimoIR.Text = string.Empty;
        }

        private void textValorMinimoIR_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = ValidaCampoHelper.FormataDigitacaoNumero(sender, e, textValorMinimoIR.Text, 2);
        }

        private void textValorMinimoIR_Leave(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(textValorMinimoIR.Text))
            {
                textValorMinimoIR.Text = textValorMinimoIR.Tag.ToString();
            }

            textValorMinimoIR.Text = ValidaCampoHelper.FormataValorMonetario(textValorMinimoIR.Text);
        }

        private void textAliquotaPIS_Enter(object sender, EventArgs e)
        {
            textAliquotaPIS.Tag = textAliquotaPIS.Text;
            textAliquotaPIS.Text = string.Empty;
        }

        private void textAliquotaPIS_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = ValidaCampoHelper.FormataDigitacaoNumero(sender, e, textAliquotaPIS.Text, 2);
        }

        private void textAliquotaPIS_Leave(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(textAliquotaPIS.Text))
            {
                textAliquotaPIS.Text = textAliquotaPIS.Tag.ToString();
            }

            textAliquotaPIS.Text = ValidaCampoHelper.FormataValorMonetario(textAliquotaPIS.Text);
        }

        private void textAliquotaCOFINS_Enter(object sender, EventArgs e)
        {
            textAliquotaCOFINS.Tag = textAliquotaCOFINS.Text;
            textAliquotaCOFINS.Text = string.Empty;
        }

        private void textAliquotaCOFINS_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = ValidaCampoHelper.FormataDigitacaoNumero(sender, e, textAliquotaCOFINS.Text, 2);
        }

        private void textAliquotaCOFINS_Leave(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(textAliquotaCOFINS.Text))
            {
                textAliquotaCOFINS.Text = textAliquotaCOFINS.Tag.ToString();
            }

            textAliquotaCOFINS.Text = ValidaCampoHelper.FormataValorMonetario(textAliquotaCOFINS.Text);
        }

        private void textAliquotaIR_Enter(object sender, EventArgs e)
        {
            textAliquotaIR.Tag = textAliquotaIR.Text;
            textAliquotaIR.Text = string.Empty;
        }

        private void textAliquotaIR_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = ValidaCampoHelper.FormataDigitacaoNumero(sender, e, textAliquotaIR.Text, 2);
        }

        private void textAliquotaIR_Leave(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(textAliquotaIR.Text))
            {
                textAliquotaIR.Text = textAliquotaIR.Tag.ToString();
            }

            textAliquotaIR.Text = ValidaCampoHelper.FormataValorMonetario(textAliquotaIR.Text);
        }

        private void textAliquotaCSLL_Enter(object sender, EventArgs e)
        {
            textAliquotaCSLL.Tag = textAliquotaCSLL.Text;
            textAliquotaCSLL.Text = string.Empty;
        }

        private void textAliquotaCSLL_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = ValidaCampoHelper.FormataDigitacaoNumero(sender, e, textAliquotaCSLL.Text, 2);
        }

        private void textAliquotaCSLL_Leave(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(textAliquotaCSLL.Text))
            {
                textAliquotaCSLL.Text = textAliquotaCSLL.Tag.ToString();
            }

            textAliquotaCSLL.Text = ValidaCampoHelper.FormataValorMonetario(textAliquotaCSLL.Text);
        }

        private void textAliquotaISS_Leave(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(textAliquotaISS.Text))
            {
                textAliquotaISS.Text = textAliquotaISS.Tag.ToString();
            }

            textAliquotaISS.Text = ValidaCampoHelper.FormataValorMonetario(textAliquotaISS.Text);
        }

        private void textAliquotaISS_Enter(object sender, EventArgs e)
        {
            textAliquotaISS.Tag = textAliquotaISS.Text;
            textAliquotaISS.Text = string.Empty;
        }

        private void textAliquotaISS_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = ValidaCampoHelper.FormataDigitacaoNumero(sender, e, textAliquotaISS.Text, 2);
        }
    }
}
