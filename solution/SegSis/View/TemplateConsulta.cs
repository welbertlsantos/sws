﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace SWS.View
{
    public partial class frmTemplateConsulta : Form
    {
        public System.Windows.Forms.SplitContainer scForm;
        public System.Windows.Forms.Panel pnlForm;
        public System.Windows.Forms.PictureBox banner;
        
        
        public frmTemplateConsulta()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmTemplateConsulta));
            this.scForm = new System.Windows.Forms.SplitContainer();
            this.banner = new System.Windows.Forms.PictureBox();
            this.pnlForm = new System.Windows.Forms.Panel();
            ((System.ComponentModel.ISupportInitialize)(this.scForm)).BeginInit();
            this.scForm.Panel2.SuspendLayout();
            this.scForm.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.banner)).BeginInit();
            this.SuspendLayout();
            // 
            // scForm
            // 
            this.scForm.Location = new System.Drawing.Point(0, 41);
            this.scForm.Name = "scForm";
            this.scForm.Orientation = System.Windows.Forms.Orientation.Horizontal;
            this.scForm.TabStop = false;
            this.scForm.IsSplitterFixed = true;
            // 
            // scForm.Panel2
            // 
            this.scForm.Panel2.AutoScroll = true;
            this.scForm.Panel2.Controls.Add(this.pnlForm);
            this.scForm.Size = new System.Drawing.Size(534, 320);
            this.scForm.SplitterDistance = 30;
            this.scForm.TabIndex = 0;
            // 
            // banner
            // 
            this.banner.Image = global::SWS.Properties.Resources.banner1;
            this.banner.Location = new System.Drawing.Point(0, 0);
            this.banner.Name = "banner";
            this.banner.Size = new System.Drawing.Size(535, 41);
            this.banner.TabIndex = 1;
            this.banner.TabStop = false;
            // 
            // pnlForm
            // 
            this.pnlForm.Location = new System.Drawing.Point(0, 0);
            this.pnlForm.Name = "pnlForm";
            this.pnlForm.Size = new System.Drawing.Size(514, 300);
            this.pnlForm.TabIndex = 0;
            // 
            // frmTemplateConsulta
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.Control;
            this.ClientSize = new System.Drawing.Size(534, 361);
            this.ControlBox = false;
            this.Controls.Add(this.banner);
            this.Controls.Add(this.scForm);
            this.KeyPreview = true;
            this.Name = "frmTemplateConsulta";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "TemplateConsulta";
            this.scForm.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.scForm)).EndInit();
            this.scForm.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.banner)).EndInit();
            this.ResumeLayout(false);
            this.Icon = ((System.Drawing.Icon)(SWS.Properties.Resources.icone));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;

        }
    }
}
