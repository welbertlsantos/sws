﻿namespace SWS.View
{
    partial class frmPerfilMontar
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.flpAcao = new System.Windows.Forms.FlowLayoutPanel();
            this.btn_fechar = new System.Windows.Forms.Button();
            this.lblPerfil = new System.Windows.Forms.TextBox();
            this.cbPerfil = new SWS.ComboBoxWithBorder();
            this.grb_dominio = new System.Windows.Forms.GroupBox();
            this.lb_dominio = new System.Windows.Forms.ListBox();
            this.grb_acao = new System.Windows.Forms.GroupBox();
            this.clb_acao = new System.Windows.Forms.CheckedListBox();
            ((System.ComponentModel.ISupportInitialize)(this.spForm)).BeginInit();
            this.spForm.Panel1.SuspendLayout();
            this.spForm.Panel2.SuspendLayout();
            this.spForm.SuspendLayout();
            this.pnlForm.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.banner)).BeginInit();
            this.flpAcao.SuspendLayout();
            this.grb_dominio.SuspendLayout();
            this.grb_acao.SuspendLayout();
            this.SuspendLayout();
            // 
            // spForm
            // 
            // 
            // spForm.Panel1
            // 
            this.spForm.Panel1.Controls.Add(this.flpAcao);
            // 
            // pnlForm
            // 
            this.pnlForm.Controls.Add(this.grb_acao);
            this.pnlForm.Controls.Add(this.grb_dominio);
            this.pnlForm.Controls.Add(this.cbPerfil);
            this.pnlForm.Controls.Add(this.lblPerfil);
            // 
            // flpAcao
            // 
            this.flpAcao.Controls.Add(this.btn_fechar);
            this.flpAcao.Dock = System.Windows.Forms.DockStyle.Fill;
            this.flpAcao.Location = new System.Drawing.Point(0, 0);
            this.flpAcao.Name = "flpAcao";
            this.flpAcao.Size = new System.Drawing.Size(784, 35);
            this.flpAcao.TabIndex = 0;
            // 
            // btn_fechar
            // 
            this.btn_fechar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_fechar.Image = global::SWS.Properties.Resources.close;
            this.btn_fechar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btn_fechar.Location = new System.Drawing.Point(3, 3);
            this.btn_fechar.Name = "btn_fechar";
            this.btn_fechar.Size = new System.Drawing.Size(75, 23);
            this.btn_fechar.TabIndex = 5;
            this.btn_fechar.Text = "&Fechar";
            this.btn_fechar.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btn_fechar.UseVisualStyleBackColor = true;
            this.btn_fechar.Click += new System.EventHandler(this.btn_fechar_Click);
            // 
            // lblPerfil
            // 
            this.lblPerfil.BackColor = System.Drawing.Color.LightGray;
            this.lblPerfil.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblPerfil.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPerfil.Location = new System.Drawing.Point(12, 10);
            this.lblPerfil.Name = "lblPerfil";
            this.lblPerfil.ReadOnly = true;
            this.lblPerfil.Size = new System.Drawing.Size(106, 21);
            this.lblPerfil.TabIndex = 0;
            this.lblPerfil.TabStop = false;
            this.lblPerfil.Text = "Perfil";
            // 
            // cbPerfil
            // 
            this.cbPerfil.BackColor = System.Drawing.Color.LightGray;
            this.cbPerfil.BorderColor = System.Drawing.Color.DimGray;
            this.cbPerfil.BorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid;
            this.cbPerfil.FormattingEnabled = true;
            this.cbPerfil.Location = new System.Drawing.Point(117, 10);
            this.cbPerfil.Name = "cbPerfil";
            this.cbPerfil.Size = new System.Drawing.Size(628, 21);
            this.cbPerfil.TabIndex = 1;
            this.cbPerfil.SelectedIndexChanged += new System.EventHandler(this.cbPerfil_SelectedIndexChanged);
            // 
            // grb_dominio
            // 
            this.grb_dominio.Controls.Add(this.lb_dominio);
            this.grb_dominio.Location = new System.Drawing.Point(12, 37);
            this.grb_dominio.Name = "grb_dominio";
            this.grb_dominio.Size = new System.Drawing.Size(365, 415);
            this.grb_dominio.TabIndex = 6;
            this.grb_dominio.TabStop = false;
            this.grb_dominio.Text = "Domínio";
            // 
            // lb_dominio
            // 
            this.lb_dominio.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lb_dominio.FormattingEnabled = true;
            this.lb_dominio.Location = new System.Drawing.Point(3, 16);
            this.lb_dominio.Name = "lb_dominio";
            this.lb_dominio.Size = new System.Drawing.Size(359, 396);
            this.lb_dominio.TabIndex = 2;
            this.lb_dominio.SelectedIndexChanged += new System.EventHandler(this.lb_dominio_SelectedIndexChanged);
            // 
            // grb_acao
            // 
            this.grb_acao.Controls.Add(this.clb_acao);
            this.grb_acao.Location = new System.Drawing.Point(383, 37);
            this.grb_acao.Name = "grb_acao";
            this.grb_acao.Size = new System.Drawing.Size(362, 412);
            this.grb_acao.TabIndex = 7;
            this.grb_acao.TabStop = false;
            this.grb_acao.Text = "Acao permitida";
            // 
            // clb_acao
            // 
            this.clb_acao.Dock = System.Windows.Forms.DockStyle.Fill;
            this.clb_acao.FormattingEnabled = true;
            this.clb_acao.Location = new System.Drawing.Point(3, 16);
            this.clb_acao.Name = "clb_acao";
            this.clb_acao.Size = new System.Drawing.Size(356, 393);
            this.clb_acao.TabIndex = 3;
            this.clb_acao.ItemCheck += new System.Windows.Forms.ItemCheckEventHandler(this.clb_acao_ItemCheck);
            // 
            // frmPerfilMontar
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(784, 561);
            this.Name = "frmPerfilMontar";
            this.Text = "MONTAR PERFIL DO USUÁRIO";
            this.spForm.Panel1.ResumeLayout(false);
            this.spForm.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.spForm)).EndInit();
            this.spForm.ResumeLayout(false);
            this.pnlForm.ResumeLayout(false);
            this.pnlForm.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.banner)).EndInit();
            this.flpAcao.ResumeLayout(false);
            this.grb_dominio.ResumeLayout(false);
            this.grb_acao.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.FlowLayoutPanel flpAcao;
        private System.Windows.Forms.Button btn_fechar;
        private ComboBoxWithBorder cbPerfil;
        private System.Windows.Forms.TextBox lblPerfil;
        private System.Windows.Forms.GroupBox grb_dominio;
        private System.Windows.Forms.ListBox lb_dominio;
        private System.Windows.Forms.GroupBox grb_acao;
        private System.Windows.Forms.CheckedListBox clb_acao;
    }
}