﻿namespace SWS.View
{
    partial class frmProdutoIncluir
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.flpACao = new System.Windows.Forms.FlowLayoutPanel();
            this.btnIncluir = new System.Windows.Forms.Button();
            this.btn_fechar = new System.Windows.Forms.Button();
            this.lblNome = new System.Windows.Forms.TextBox();
            this.lblPrecoVenda = new System.Windows.Forms.TextBox();
            this.lblPrecoCusto = new System.Windows.Forms.TextBox();
            this.textPreco = new System.Windows.Forms.TextBox();
            this.textCusto = new System.Windows.Forms.TextBox();
            this.textNome = new System.Windows.Forms.TextBox();
            this.lblPadraoContrato = new System.Windows.Forms.TextBox();
            this.cbPadraoContrato = new SWS.ComboBoxWithBorder();
            this.lblDescricao = new System.Windows.Forms.TextBox();
            this.textDescricao = new System.Windows.Forms.TextBox();
            ((System.ComponentModel.ISupportInitialize)(this.spForm)).BeginInit();
            this.spForm.Panel1.SuspendLayout();
            this.spForm.Panel2.SuspendLayout();
            this.spForm.SuspendLayout();
            this.pnlForm.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.banner)).BeginInit();
            this.flpACao.SuspendLayout();
            this.SuspendLayout();
            // 
            // spForm
            // 
            // 
            // spForm.Panel1
            // 
            this.spForm.Panel1.Controls.Add(this.flpACao);
            // 
            // pnlForm
            // 
            this.pnlForm.Controls.Add(this.textDescricao);
            this.pnlForm.Controls.Add(this.lblDescricao);
            this.pnlForm.Controls.Add(this.cbPadraoContrato);
            this.pnlForm.Controls.Add(this.lblPadraoContrato);
            this.pnlForm.Controls.Add(this.textPreco);
            this.pnlForm.Controls.Add(this.textCusto);
            this.pnlForm.Controls.Add(this.textNome);
            this.pnlForm.Controls.Add(this.lblPrecoCusto);
            this.pnlForm.Controls.Add(this.lblPrecoVenda);
            this.pnlForm.Controls.Add(this.lblNome);
            // 
            // flpACao
            // 
            this.flpACao.Controls.Add(this.btnIncluir);
            this.flpACao.Controls.Add(this.btn_fechar);
            this.flpACao.Dock = System.Windows.Forms.DockStyle.Fill;
            this.flpACao.Location = new System.Drawing.Point(0, 0);
            this.flpACao.Name = "flpACao";
            this.flpACao.Size = new System.Drawing.Size(784, 35);
            this.flpACao.TabIndex = 0;
            // 
            // btnIncluir
            // 
            this.btnIncluir.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnIncluir.Image = global::SWS.Properties.Resources.Gravar;
            this.btnIncluir.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnIncluir.Location = new System.Drawing.Point(3, 3);
            this.btnIncluir.Name = "btnIncluir";
            this.btnIncluir.Size = new System.Drawing.Size(75, 23);
            this.btnIncluir.TabIndex = 10;
            this.btnIncluir.Text = "&Gravar";
            this.btnIncluir.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnIncluir.UseVisualStyleBackColor = true;
            this.btnIncluir.Click += new System.EventHandler(this.btnIncluir_Click);
            // 
            // btn_fechar
            // 
            this.btn_fechar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_fechar.Image = global::SWS.Properties.Resources.close;
            this.btn_fechar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btn_fechar.Location = new System.Drawing.Point(84, 3);
            this.btn_fechar.Name = "btn_fechar";
            this.btn_fechar.Size = new System.Drawing.Size(75, 23);
            this.btn_fechar.TabIndex = 11;
            this.btn_fechar.Text = "&Fechar";
            this.btn_fechar.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btn_fechar.UseVisualStyleBackColor = true;
            this.btn_fechar.Click += new System.EventHandler(this.btn_fechar_Click);
            // 
            // lblNome
            // 
            this.lblNome.BackColor = System.Drawing.Color.LightGray;
            this.lblNome.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblNome.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNome.Location = new System.Drawing.Point(9, 8);
            this.lblNome.Name = "lblNome";
            this.lblNome.ReadOnly = true;
            this.lblNome.Size = new System.Drawing.Size(127, 21);
            this.lblNome.TabIndex = 0;
            this.lblNome.TabStop = false;
            this.lblNome.Text = "Nome do produto";
            // 
            // lblPrecoVenda
            // 
            this.lblPrecoVenda.BackColor = System.Drawing.Color.LightGray;
            this.lblPrecoVenda.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblPrecoVenda.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPrecoVenda.Location = new System.Drawing.Point(9, 28);
            this.lblPrecoVenda.Name = "lblPrecoVenda";
            this.lblPrecoVenda.ReadOnly = true;
            this.lblPrecoVenda.Size = new System.Drawing.Size(127, 21);
            this.lblPrecoVenda.TabIndex = 1;
            this.lblPrecoVenda.TabStop = false;
            this.lblPrecoVenda.Text = "Preço de Venda R$";
            // 
            // lblPrecoCusto
            // 
            this.lblPrecoCusto.BackColor = System.Drawing.Color.LightGray;
            this.lblPrecoCusto.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblPrecoCusto.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPrecoCusto.Location = new System.Drawing.Point(9, 47);
            this.lblPrecoCusto.Name = "lblPrecoCusto";
            this.lblPrecoCusto.ReadOnly = true;
            this.lblPrecoCusto.Size = new System.Drawing.Size(127, 21);
            this.lblPrecoCusto.TabIndex = 2;
            this.lblPrecoCusto.TabStop = false;
            this.lblPrecoCusto.Text = "Preço de Custo R$";
            // 
            // textPreco
            // 
            this.textPreco.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textPreco.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textPreco.Location = new System.Drawing.Point(135, 28);
            this.textPreco.MaxLength = 10;
            this.textPreco.Name = "textPreco";
            this.textPreco.Size = new System.Drawing.Size(591, 21);
            this.textPreco.TabIndex = 2;
            this.textPreco.Text = "0,00";
            this.textPreco.Enter += new System.EventHandler(this.textPreco_Enter);
            this.textPreco.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.textPreco_KeyPress);
            this.textPreco.Leave += new System.EventHandler(this.textPreco_Leave);
            // 
            // textCusto
            // 
            this.textCusto.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textCusto.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textCusto.Location = new System.Drawing.Point(135, 47);
            this.textCusto.MaxLength = 10;
            this.textCusto.Name = "textCusto";
            this.textCusto.Size = new System.Drawing.Size(591, 21);
            this.textCusto.TabIndex = 3;
            this.textCusto.Text = "0,00";
            this.textCusto.Enter += new System.EventHandler(this.textCusto_Enter);
            this.textCusto.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.textCusto_KeyPress);
            this.textCusto.Leave += new System.EventHandler(this.textCusto_Leave);
            // 
            // textNome
            // 
            this.textNome.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textNome.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.textNome.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textNome.Location = new System.Drawing.Point(135, 8);
            this.textNome.MaxLength = 100;
            this.textNome.Name = "textNome";
            this.textNome.Size = new System.Drawing.Size(591, 21);
            this.textNome.TabIndex = 1;
            // 
            // lblPadraoContrato
            // 
            this.lblPadraoContrato.BackColor = System.Drawing.Color.LightGray;
            this.lblPadraoContrato.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblPadraoContrato.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPadraoContrato.Location = new System.Drawing.Point(9, 67);
            this.lblPadraoContrato.Name = "lblPadraoContrato";
            this.lblPadraoContrato.ReadOnly = true;
            this.lblPadraoContrato.Size = new System.Drawing.Size(127, 21);
            this.lblPadraoContrato.TabIndex = 4;
            this.lblPadraoContrato.TabStop = false;
            this.lblPadraoContrato.Text = "Padrao contrato?";
            // 
            // cbPadraoContrato
            // 
            this.cbPadraoContrato.BackColor = System.Drawing.Color.LightGray;
            this.cbPadraoContrato.BorderColor = System.Drawing.Color.DimGray;
            this.cbPadraoContrato.BorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid;
            this.cbPadraoContrato.FormattingEnabled = true;
            this.cbPadraoContrato.Location = new System.Drawing.Point(135, 67);
            this.cbPadraoContrato.Name = "cbPadraoContrato";
            this.cbPadraoContrato.Size = new System.Drawing.Size(591, 21);
            this.cbPadraoContrato.TabIndex = 4;
            // 
            // lblDescricao
            // 
            this.lblDescricao.BackColor = System.Drawing.Color.LightGray;
            this.lblDescricao.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblDescricao.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDescricao.Location = new System.Drawing.Point(9, 87);
            this.lblDescricao.Multiline = true;
            this.lblDescricao.Name = "lblDescricao";
            this.lblDescricao.ReadOnly = true;
            this.lblDescricao.Size = new System.Drawing.Size(127, 79);
            this.lblDescricao.TabIndex = 5;
            this.lblDescricao.TabStop = false;
            this.lblDescricao.Text = "Descrição";
            // 
            // textDescricao
            // 
            this.textDescricao.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textDescricao.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.textDescricao.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textDescricao.Location = new System.Drawing.Point(135, 87);
            this.textDescricao.Multiline = true;
            this.textDescricao.Name = "textDescricao";
            this.textDescricao.Size = new System.Drawing.Size(591, 79);
            this.textDescricao.TabIndex = 5;
            // 
            // frmProdutoIncluir
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(784, 561);
            this.Name = "frmProdutoIncluir";
            this.Text = "INCLUIR PRODUTO";
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.frmProdutoIncluir_KeyDown);
            this.spForm.Panel1.ResumeLayout(false);
            this.spForm.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.spForm)).EndInit();
            this.spForm.ResumeLayout(false);
            this.pnlForm.ResumeLayout(false);
            this.pnlForm.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.banner)).EndInit();
            this.flpACao.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        protected System.Windows.Forms.FlowLayoutPanel flpACao;
        protected System.Windows.Forms.Button btnIncluir;
        protected System.Windows.Forms.Button btn_fechar;
        protected System.Windows.Forms.TextBox lblPrecoVenda;
        protected System.Windows.Forms.TextBox lblNome;
        protected System.Windows.Forms.TextBox lblPrecoCusto;
        protected System.Windows.Forms.TextBox textPreco;
        protected System.Windows.Forms.TextBox textCusto;
        protected System.Windows.Forms.TextBox textNome;
        protected System.Windows.Forms.TextBox lblPadraoContrato;
        protected ComboBoxWithBorder cbPadraoContrato;
        protected System.Windows.Forms.TextBox lblDescricao;
        protected System.Windows.Forms.TextBox textDescricao;

    }
}