﻿using SWS.Entidade;
using SWS.View.ViewHelper;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using SWS.Facade;
using System.Diagnostics;
using SWS.ViewHelper;

namespace SWS.View
{
    public partial class frmRelatorioAtendimentoSala : frmTemplateConsulta
    {

        private Sala sala;

        public Sala Sala
        {
            get { return sala; }
            set { sala = value; }
        }

        private Usuario examinador;
        
        public Usuario Examinador
        {
            get { return examinador; }
            set { examinador = value; }
        }
        
        public frmRelatorioAtendimentoSala()
        {
            InitializeComponent();
            ComboHelper.atendimentoFila(cbSituacaoAtendimento);
        }

        private void btnFechar_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnImprimir_Click(object sender, EventArgs e)
        {
            try
            {
                if (!validacaoTela())
                {
                    this.Cursor = Cursors.WaitCursor;

                    string nomeRelatorio = "ATENDIMENTO_SALA.jasper";
                    string salaSelecionada = this.Sala != null ? this.Sala.Descricao : string.Empty;
                    long idSala = this.Sala != null ? (long)this.Sala.Id : 0;
                    string usuario = this.Examinador != null ? "'" + this.Examinador.Login + "'" : string.Empty;
                    long idUsuario = this.Examinador != null ? (long)this.Examinador.Id : 0;
                    string situacao = ((SelectItem)cbSituacaoAtendimento.SelectedItem).Valor;
                    string dataInicial = this.dataInicial.Text;
                    string dataFinal = this.dataFinal.Text;


                    var process = new Process();

                    process.StartInfo.FileName = "Relatorio\\jre7\\bin\\java";
                    process.StartInfo.Arguments = " -jar Relatorio\\MiddlewareJasper.jar " + "Relatorio\\Report\\" + nomeRelatorio + " ID_SALA:" + idSala + ":INTEGER" + " SALA:" + '"' + salaSelecionada + '"' + ":STRING " + " SITUACAO:" + situacao + ":STRING " + " DATA_INICIAL:" + dataInicial + ":STRING" + " DATA_FINAL:" + dataFinal + ":STRING" + " ID_EMPRESA:" + PermissionamentoFacade.usuarioAutenticado.Empresa.Id + ":INTEGER" + " ID_USUARIO:" + idUsuario + ":INTEGER" + " USUARIO:" + usuario + ":STRING";
                    process.StartInfo.UseShellExecute = false;
                    process.StartInfo.RedirectStandardError = true;
                    process.StartInfo.RedirectStandardInput = true;
                    process.StartInfo.RedirectStandardOutput = true;
                    process.StartInfo.CreateNoWindow = true;

                    process.Start();
                    process.StandardOutput.ReadToEnd();
                    process.WaitForExit();

                }

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }

        }

        private void btSalaIncluir_Click(object sender, EventArgs e)
        {
            try
            {
                frmSalaBuscar salaBuscar = new frmSalaBuscar(PermissionamentoFacade.usuarioAutenticado.Empresa);
                salaBuscar.ShowDialog();

                if (salaBuscar.Sala != null)
                {
                    this.Sala = salaBuscar.Sala;
                    textSala.Text = Sala.Descricao;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }

        private void btSalaExcluir_Click(object sender, EventArgs e)
        {
            try
            {
                if (this.sala == null)
                    throw new Exception("Selecione primeiro uma sala.");

                this.Sala = null;
                textSala.Text = string.Empty;
                   
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }

        private void btExaminadorIncluir_Click(object sender, EventArgs e)
        {
            try
            {
                frmUsuarioSelecionar selecionarUsuario = new frmUsuarioSelecionar();
                selecionarUsuario.ShowDialog();

                if (selecionarUsuario.Usuario != null)
                {
                    this.Examinador = selecionarUsuario.Usuario;
                    this.textExaminador.Text = Examinador.Nome + " [ " + Examinador.Login + " ]";
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }

        private void btExaminadorExcluir_Click(object sender, EventArgs e)
        {
            try
            {
                if (examinador == null)
                    throw new Exception("Você deve selecionar o examidor primeiro.");

                examinador = null;
                textExaminador.Text = string.Empty;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }
        
        private bool validacaoTela ()
        {
            bool retorno = false;
            try
            {
                if (dataFinal.Checked && !dataFinal.Checked)
                    throw new Exception("Você deve selecionar o período final para pesquisa.");

                if (!dataInicial.Checked && dataFinal.Checked)
                    throw new Exception("Você deve selecionar o peróodo inicial para pesquisa.");

                if (!dataInicial.Checked && !dataFinal.Checked)
                    throw new Exception("Você deve selecionar a data inicial e a data final para pesquisa.");

                if (Convert.ToDateTime(dataFinal.Text) < Convert.ToDateTime(dataInicial.Text))
                    throw new Exception("A data final não pode ser menor que a data inicial");
                         
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
                retorno = true;
            }
            return retorno;
        }
    }
}
