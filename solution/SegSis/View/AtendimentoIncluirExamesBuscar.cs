﻿using SWS.Entidade;
using SWS.Facade;
using SWS.View.Resources;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace SWS.View
{
    public partial class frmAtendimentoIncluirExamesBuscar : frmTemplateConsulta
    {
        private Aso atendimento;
        private LinkedList<Exame> exameInAso = new LinkedList<Exame>();

        public LinkedList<Exame> ExameInAso
        {
            get { return exameInAso; }
            set { exameInAso = value; }
        }

        private Exame exame;

        public frmAtendimentoIncluirExamesBuscar(Aso atendimento)
        {
            InitializeComponent();
            this.atendimento = atendimento;
            validaPermissoes();
            ActiveControl = text_descricao;
        }

        private void btn_incluir_Click(object sender, EventArgs e)
        {
            try
            {
                AsoFacade asoFacade = AsoFacade.getInstance();
                FilaFacade filaFacade = FilaFacade.getInstance();
                LinkedList<Exame> exameInsertAso = new LinkedList<Exame>();

                /* verificando os exames que estão selecionados */

                foreach (DataGridViewRow dvRow in dvg_exame.Rows)
                    if ((bool)dvRow.Cells[9].Value == true)
                        exameInsertAso.AddLast(new Exame((Int64)dvRow.Cells[0].Value, (String)dvRow.Cells[1].Value, (String)dvRow.Cells[2].Value, (string)dvRow.Cells[3].Value == "Lab" ? true : false, (Decimal)dvRow.Cells[4].Value, (Int32)dvRow.Cells[5].Value, (Decimal)dvRow.Cells[6].Value, (String)dvRow.Cells[7].Value == "Ext" ? true : false, (Boolean)dvRow.Cells[8].Value, string.Empty, false, null, false));

                /* só incluirá os exames caso tenha algum selecionado. */
                if (exameInsertAso.Count == 0)
                    throw new Exception("Você não selecionou nenhum exame.");
                    
                asoFacade.insertExamesInASoPosGravacao(exameInsertAso, atendimento);
                MessageBox.Show("Exame(s) inserido(s) com sucesso.", "Confirmação", MessageBoxButtons.OK, MessageBoxIcon.Information);
                this.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btn_novo_Click(object sender, EventArgs e)
        {
            frmExameIncluir formExameIncluir = new frmExameIncluir();
            formExameIncluir.ShowDialog();

            if (formExameIncluir.Exame != null)
            {
                exame = formExameIncluir.Exame;
                text_descricao.Text = exame.Descricao;
                montaDataGrid();
                btn_fechar.PerformClick();
            }
        }

        private void btn_fechar_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void chk_marcaDesmarcaExame_CheckedChanged(object sender, EventArgs e)
        {
            bool situacao = chk_marcaDesmarcaExame.Checked ? true : false;
            marcaTodos(situacao);
        }

        private void btn_pesquisar_Click(object sender, EventArgs e)
        {
            try
            {
                this.dvg_exame.Columns.Clear();
                montaDataGrid();
                text_descricao.Focus();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void validaPermissoes()
        {
            PermissionamentoFacade permissionamentoFacade = PermissionamentoFacade.getInstance();
            this.btn_novo.Enabled = permissionamentoFacade.hasPermission("EXAME", "INCLUIR");
        }

        public void montaDataGrid()
        {
            try
            {
                AsoFacade asoFacade = AsoFacade.getInstance();
                this.Cursor = Cursors.WaitCursor;
                exameInAso = asoFacade.findAllExamesByAso(atendimento, new Exame(null, text_descricao.Text.Trim(), ApplicationConstants.ATIVO, false, 0, 0, 0, false, false, string.Empty, false, null, false));

                dvg_exame.ColumnCount = 9;

                dvg_exame.Columns[0].HeaderText = "Código exame";

                dvg_exame.Columns[1].HeaderText = "Exame";
                dvg_exame.Columns[1].ReadOnly = true;

                dvg_exame.Columns[2].HeaderText = "Situação";
                dvg_exame.Columns[2].Visible = false;

                dvg_exame.Columns[3].HeaderText = "Laboratorio";
                dvg_exame.Columns[3].ReadOnly = true;
                dvg_exame.Columns[3].HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleCenter;
                dvg_exame.Columns[3].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
                dvg_exame.Columns[3].MinimumWidth = 50;

                dvg_exame.Columns[4].HeaderText = "Preço do Exame";
                dvg_exame.Columns[4].Visible = false;

                dvg_exame.Columns[5].HeaderText = "Prioridade Fila";
                dvg_exame.Columns[5].Visible = false;

                dvg_exame.Columns[6].HeaderText = "Preço de Custo";
                dvg_exame.Columns[6].Visible = false;

                dvg_exame.Columns[7].HeaderText = "Externo";
                dvg_exame.Columns[7].ReadOnly = true;
                dvg_exame.Columns[7].HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleCenter;
                dvg_exame.Columns[7].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
                dvg_exame.Columns[7].MinimumWidth = 50;

                dvg_exame.Columns[8].HeaderText = "Documentação";
                dvg_exame.Columns[8].Visible = false;

                DataGridViewCheckBoxColumn check = new DataGridViewCheckBoxColumn();
                check.Name = "Selec";
                dvg_exame.Columns.Add(check);
                dvg_exame.Columns[9].DisplayIndex = 0;

                foreach (Exame exameInColecao in exameInAso)
                    dvg_exame.Rows.Add(exameInColecao.Id, exameInColecao.Descricao, exameInColecao.Situacao, (Boolean)exameInColecao.Laboratorio ? "Lab" : null, exameInColecao.Preco, exameInColecao.Prioridade, exameInColecao.Custo, exameInColecao.Externo ? "Ext" : null, exameInColecao.LiberaDocumento, false);

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }

        private void marcaTodos(Boolean situacao)
        {
            foreach (DataGridViewRow dvRow in dvg_exame.Rows)
                dvRow.Cells[9].Value = situacao;
        }

        private void frmAtendimentoIncluirExamesBuscar_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter || e.KeyCode == Keys.Return)
                SendKeys.Send("{TAB}");
        }
    }
}
