﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using SWS.Entidade;
using SWS.Facade;

namespace SWS.View
{
    public partial class frmPcmsoEstimativa : SWS.View.frmTemplateConsulta
    {
        private Estudo pcmso;

        public frmPcmsoEstimativa(Estudo pcmso)
        {
            InitializeComponent();
            this.pcmso = pcmso;
            montaDataGrid();
        }

        private void btnConfirma_Click(object sender, EventArgs e)
        {
            Boolean gravou = false;

            try
            {
                EstudoFacade pcmsoFacade = EstudoFacade.getInstance();

                this.Cursor = Cursors.WaitCursor;

                foreach (DataGridViewRow dvRow in dgEstimativa.Rows)
                {
                    if ((Boolean)dvRow.Cells[0].Value == true)
                    {
                        gravou = true;
                        /* incluindo a estimativa no estudo */

                        EstimativaEstudo estimativaEstudo = new EstimativaEstudo();
                        Estimativa estimativa = new Estimativa();
                        estimativa.Id = (long)dvRow.Cells[1].Value;
                        estimativa.Descricao = dvRow.Cells[2].Value.ToString();
                        estimativa.Situacao = dvRow.Cells[3].Value.ToString();
                        estimativaEstudo.Estimativa = estimativa;
                        estimativaEstudo.Estudo = pcmso;
                        estimativaEstudo.Quantidade = 0;

                        pcmsoFacade.insertEstimativaEstudo(estimativaEstudo);
                    }
                }

                if (!gravou)
                    throw new Exception("É necessário selecionar pelo menos um tipo de estimativa");
                else
                    this.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }

        }

        private void btnFechar_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void montaDataGrid()
        {
            try
            {
                this.dgEstimativa.Columns.Clear();
                EstudoFacade pcmsoFacade = EstudoFacade.getInstance();
                Estimativa estimativa = new Estimativa();
                estimativa.Descricao = string.Empty;

                DataSet ds = pcmsoFacade.findAllEstimativaNotInEstudo(pcmso, estimativa);

                dgEstimativa.DataSource = ds.Tables[0].DefaultView;

                dgEstimativa.Columns[0].HeaderText = "Selec";
                dgEstimativa.Columns[0].Width = 40;

                dgEstimativa.Columns[1].HeaderText = "Id_estimativa";
                dgEstimativa.Columns[1].Visible = false;

                dgEstimativa.Columns[2].HeaderText = "Tipo de Estimativa";
                dgEstimativa.Columns[2].ReadOnly = true;
                dgEstimativa.Columns[2].Width = 100;

                dgEstimativa.Columns[3].HeaderText = "Situação";
                dgEstimativa.Columns[3].Visible = false;

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }

        }
    }
}
