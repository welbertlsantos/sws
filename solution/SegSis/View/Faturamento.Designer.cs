﻿namespace SWS.View
{
    partial class frmFaturamento
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            this.flpAcao = new System.Windows.Forms.FlowLayoutPanel();
            this.btn_gravar = new System.Windows.Forms.Button();
            this.btn_fechar = new System.Windows.Forms.Button();
            this.lblCliente = new System.Windows.Forms.TextBox();
            this.lblCNPJ = new System.Windows.Forms.TextBox();
            this.lblEmpresa = new System.Windows.Forms.TextBox();
            this.lblCondicaoPagamento = new System.Windows.Forms.TextBox();
            this.lblFormaPagamento = new System.Windows.Forms.TextBox();
            this.lblDataEmissao = new System.Windows.Forms.TextBox();
            this.text_cnpj = new System.Windows.Forms.MaskedTextBox();
            this.text_razaoSocial = new System.Windows.Forms.TextBox();
            this.cb_forma = new SWS.ComboBoxWithBorder();
            this.cb_pagamento = new SWS.ComboBoxWithBorder();
            this.cb_empresa = new SWS.ComboBoxWithBorder();
            this.dt_faturamento = new System.Windows.Forms.DateTimePicker();
            this.grb_itensMovimento = new System.Windows.Forms.GroupBox();
            this.dgv_itemMovimento = new System.Windows.Forms.DataGridView();
            this.lbl_textoExclusao = new System.Windows.Forms.Label();
            this.grb_totais = new System.Windows.Forms.GroupBox();
            this.lbl_itens = new System.Windows.Forms.TextBox();
            this.lbl_totalNF = new System.Windows.Forms.TextBox();
            this.text_itensFaturados = new System.Windows.Forms.TextBox();
            this.text_totalNF = new System.Windows.Forms.TextBox();
            this.lblCentroCusto = new System.Windows.Forms.TextBox();
            this.textCentroCusto = new System.Windows.Forms.TextBox();
            ((System.ComponentModel.ISupportInitialize)(this.spForm)).BeginInit();
            this.spForm.Panel1.SuspendLayout();
            this.spForm.Panel2.SuspendLayout();
            this.spForm.SuspendLayout();
            this.pnlForm.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.banner)).BeginInit();
            this.flpAcao.SuspendLayout();
            this.grb_itensMovimento.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgv_itemMovimento)).BeginInit();
            this.grb_totais.SuspendLayout();
            this.SuspendLayout();
            // 
            // spForm
            // 
            // 
            // spForm.Panel1
            // 
            this.spForm.Panel1.Controls.Add(this.flpAcao);
            // 
            // pnlForm
            // 
            this.pnlForm.Controls.Add(this.textCentroCusto);
            this.pnlForm.Controls.Add(this.lblCentroCusto);
            this.pnlForm.Controls.Add(this.text_cnpj);
            this.pnlForm.Controls.Add(this.lblDataEmissao);
            this.pnlForm.Controls.Add(this.grb_totais);
            this.pnlForm.Controls.Add(this.lbl_textoExclusao);
            this.pnlForm.Controls.Add(this.lblFormaPagamento);
            this.pnlForm.Controls.Add(this.cb_forma);
            this.pnlForm.Controls.Add(this.lblCondicaoPagamento);
            this.pnlForm.Controls.Add(this.cb_pagamento);
            this.pnlForm.Controls.Add(this.grb_itensMovimento);
            this.pnlForm.Controls.Add(this.dt_faturamento);
            this.pnlForm.Controls.Add(this.cb_empresa);
            this.pnlForm.Controls.Add(this.text_razaoSocial);
            this.pnlForm.Controls.Add(this.lblEmpresa);
            this.pnlForm.Controls.Add(this.lblCNPJ);
            this.pnlForm.Controls.Add(this.lblCliente);
            // 
            // flpAcao
            // 
            this.flpAcao.Controls.Add(this.btn_gravar);
            this.flpAcao.Controls.Add(this.btn_fechar);
            this.flpAcao.Dock = System.Windows.Forms.DockStyle.Fill;
            this.flpAcao.Location = new System.Drawing.Point(0, 0);
            this.flpAcao.Name = "flpAcao";
            this.flpAcao.Size = new System.Drawing.Size(784, 35);
            this.flpAcao.TabIndex = 0;
            // 
            // btn_gravar
            // 
            this.btn_gravar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_gravar.Image = global::SWS.Properties.Resources.Gravar;
            this.btn_gravar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btn_gravar.Location = new System.Drawing.Point(3, 3);
            this.btn_gravar.Name = "btn_gravar";
            this.btn_gravar.Size = new System.Drawing.Size(75, 23);
            this.btn_gravar.TabIndex = 3;
            this.btn_gravar.TabStop = false;
            this.btn_gravar.Text = "&Gravar";
            this.btn_gravar.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btn_gravar.UseVisualStyleBackColor = true;
            this.btn_gravar.Click += new System.EventHandler(this.btn_gravar_Click);
            // 
            // btn_fechar
            // 
            this.btn_fechar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_fechar.Image = global::SWS.Properties.Resources.close;
            this.btn_fechar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btn_fechar.Location = new System.Drawing.Point(84, 3);
            this.btn_fechar.Name = "btn_fechar";
            this.btn_fechar.Size = new System.Drawing.Size(75, 23);
            this.btn_fechar.TabIndex = 4;
            this.btn_fechar.TabStop = false;
            this.btn_fechar.Text = "&Fechar";
            this.btn_fechar.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btn_fechar.UseVisualStyleBackColor = true;
            this.btn_fechar.Click += new System.EventHandler(this.btn_fechar_Click);
            // 
            // lblCliente
            // 
            this.lblCliente.BackColor = System.Drawing.Color.LightGray;
            this.lblCliente.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblCliente.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCliente.Location = new System.Drawing.Point(7, 7);
            this.lblCliente.Name = "lblCliente";
            this.lblCliente.ReadOnly = true;
            this.lblCliente.Size = new System.Drawing.Size(142, 21);
            this.lblCliente.TabIndex = 0;
            this.lblCliente.TabStop = false;
            this.lblCliente.Text = "Cliente";
            // 
            // lblCNPJ
            // 
            this.lblCNPJ.BackColor = System.Drawing.Color.LightGray;
            this.lblCNPJ.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblCNPJ.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCNPJ.Location = new System.Drawing.Point(7, 27);
            this.lblCNPJ.Name = "lblCNPJ";
            this.lblCNPJ.ReadOnly = true;
            this.lblCNPJ.Size = new System.Drawing.Size(142, 21);
            this.lblCNPJ.TabIndex = 1;
            this.lblCNPJ.TabStop = false;
            this.lblCNPJ.Text = "CNPJ";
            // 
            // lblEmpresa
            // 
            this.lblEmpresa.BackColor = System.Drawing.Color.LightGray;
            this.lblEmpresa.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblEmpresa.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblEmpresa.Location = new System.Drawing.Point(386, 7);
            this.lblEmpresa.Name = "lblEmpresa";
            this.lblEmpresa.ReadOnly = true;
            this.lblEmpresa.Size = new System.Drawing.Size(142, 21);
            this.lblEmpresa.TabIndex = 2;
            this.lblEmpresa.TabStop = false;
            this.lblEmpresa.Text = "Empresa";
            // 
            // lblCondicaoPagamento
            // 
            this.lblCondicaoPagamento.BackColor = System.Drawing.Color.LightGray;
            this.lblCondicaoPagamento.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblCondicaoPagamento.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCondicaoPagamento.Location = new System.Drawing.Point(386, 27);
            this.lblCondicaoPagamento.Name = "lblCondicaoPagamento";
            this.lblCondicaoPagamento.ReadOnly = true;
            this.lblCondicaoPagamento.Size = new System.Drawing.Size(142, 21);
            this.lblCondicaoPagamento.TabIndex = 3;
            this.lblCondicaoPagamento.TabStop = false;
            this.lblCondicaoPagamento.Text = "Condição de pagamento";
            // 
            // lblFormaPagamento
            // 
            this.lblFormaPagamento.BackColor = System.Drawing.Color.LightGray;
            this.lblFormaPagamento.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblFormaPagamento.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblFormaPagamento.Location = new System.Drawing.Point(386, 46);
            this.lblFormaPagamento.Name = "lblFormaPagamento";
            this.lblFormaPagamento.ReadOnly = true;
            this.lblFormaPagamento.Size = new System.Drawing.Size(142, 21);
            this.lblFormaPagamento.TabIndex = 4;
            this.lblFormaPagamento.TabStop = false;
            this.lblFormaPagamento.Text = "Forma de pagamento";
            // 
            // lblDataEmissao
            // 
            this.lblDataEmissao.BackColor = System.Drawing.Color.LightGray;
            this.lblDataEmissao.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblDataEmissao.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDataEmissao.Location = new System.Drawing.Point(7, 47);
            this.lblDataEmissao.Name = "lblDataEmissao";
            this.lblDataEmissao.ReadOnly = true;
            this.lblDataEmissao.Size = new System.Drawing.Size(142, 21);
            this.lblDataEmissao.TabIndex = 5;
            this.lblDataEmissao.TabStop = false;
            this.lblDataEmissao.Text = "Data de emissão";
            // 
            // text_cnpj
            // 
            this.text_cnpj.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.text_cnpj.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.text_cnpj.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.text_cnpj.Location = new System.Drawing.Point(148, 27);
            this.text_cnpj.Mask = "99,999,999/9999-99";
            this.text_cnpj.Name = "text_cnpj";
            this.text_cnpj.PromptChar = ' ';
            this.text_cnpj.ReadOnly = true;
            this.text_cnpj.Size = new System.Drawing.Size(230, 21);
            this.text_cnpj.TabIndex = 7;
            this.text_cnpj.TabStop = false;
            // 
            // text_razaoSocial
            // 
            this.text_razaoSocial.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.text_razaoSocial.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.text_razaoSocial.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.text_razaoSocial.Location = new System.Drawing.Point(148, 7);
            this.text_razaoSocial.MaxLength = 100;
            this.text_razaoSocial.Name = "text_razaoSocial";
            this.text_razaoSocial.ReadOnly = true;
            this.text_razaoSocial.Size = new System.Drawing.Size(230, 21);
            this.text_razaoSocial.TabIndex = 6;
            this.text_razaoSocial.TabStop = false;
            // 
            // cb_forma
            // 
            this.cb_forma.BackColor = System.Drawing.Color.LightGray;
            this.cb_forma.BorderColor = System.Drawing.Color.DimGray;
            this.cb_forma.BorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid;
            this.cb_forma.FormattingEnabled = true;
            this.cb_forma.Location = new System.Drawing.Point(527, 46);
            this.cb_forma.Name = "cb_forma";
            this.cb_forma.Size = new System.Drawing.Size(230, 21);
            this.cb_forma.TabIndex = 10;
            this.cb_forma.SelectedIndexChanged += new System.EventHandler(this.cb_forma_SelectedIndexChanged);
            // 
            // cb_pagamento
            // 
            this.cb_pagamento.BackColor = System.Drawing.Color.LightGray;
            this.cb_pagamento.BorderColor = System.Drawing.Color.DimGray;
            this.cb_pagamento.BorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid;
            this.cb_pagamento.FormattingEnabled = true;
            this.cb_pagamento.Location = new System.Drawing.Point(527, 27);
            this.cb_pagamento.Name = "cb_pagamento";
            this.cb_pagamento.Size = new System.Drawing.Size(230, 21);
            this.cb_pagamento.TabIndex = 9;
            this.cb_pagamento.SelectedIndexChanged += new System.EventHandler(this.cb_pagamento_SelectedIndexChanged);
            // 
            // cb_empresa
            // 
            this.cb_empresa.BackColor = System.Drawing.Color.LightGray;
            this.cb_empresa.BorderColor = System.Drawing.Color.DimGray;
            this.cb_empresa.BorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid;
            this.cb_empresa.FormattingEnabled = true;
            this.cb_empresa.Location = new System.Drawing.Point(527, 7);
            this.cb_empresa.Name = "cb_empresa";
            this.cb_empresa.Size = new System.Drawing.Size(230, 21);
            this.cb_empresa.TabIndex = 8;
            this.cb_empresa.SelectedIndexChanged += new System.EventHandler(this.cb_empresa_SelectedIndexChanged);
            // 
            // dt_faturamento
            // 
            this.dt_faturamento.CustomFormat = "dd/MM/yyyy HH:mm:ss";
            this.dt_faturamento.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dt_faturamento.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dt_faturamento.Location = new System.Drawing.Point(148, 47);
            this.dt_faturamento.Name = "dt_faturamento";
            this.dt_faturamento.Size = new System.Drawing.Size(230, 21);
            this.dt_faturamento.TabIndex = 11;
            // 
            // grb_itensMovimento
            // 
            this.grb_itensMovimento.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.grb_itensMovimento.Controls.Add(this.dgv_itemMovimento);
            this.grb_itensMovimento.Location = new System.Drawing.Point(7, 94);
            this.grb_itensMovimento.Name = "grb_itensMovimento";
            this.grb_itensMovimento.Size = new System.Drawing.Size(752, 287);
            this.grb_itensMovimento.TabIndex = 12;
            this.grb_itensMovimento.TabStop = false;
            this.grb_itensMovimento.Text = "Itens Movimento";
            // 
            // dgv_itemMovimento
            // 
            this.dgv_itemMovimento.AllowUserToAddRows = false;
            this.dgv_itemMovimento.AllowUserToDeleteRows = false;
            this.dgv_itemMovimento.AllowUserToOrderColumns = true;
            this.dgv_itemMovimento.AllowUserToResizeRows = false;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.Gainsboro;
            this.dgv_itemMovimento.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle1;
            this.dgv_itemMovimento.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.DisplayedCells;
            this.dgv_itemMovimento.BackgroundColor = System.Drawing.Color.White;
            this.dgv_itemMovimento.ClipboardCopyMode = System.Windows.Forms.DataGridViewClipboardCopyMode.EnableWithoutHeaderText;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgv_itemMovimento.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle2;
            this.dgv_itemMovimento.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle3.BackColor = System.Drawing.Color.LightGray;
            dataGridViewCellStyle3.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgv_itemMovimento.DefaultCellStyle = dataGridViewCellStyle3;
            this.dgv_itemMovimento.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgv_itemMovimento.Location = new System.Drawing.Point(3, 16);
            this.dgv_itemMovimento.MultiSelect = false;
            this.dgv_itemMovimento.Name = "dgv_itemMovimento";
            this.dgv_itemMovimento.ReadOnly = true;
            dataGridViewCellStyle4.Font = new System.Drawing.Font("Arial", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dgv_itemMovimento.RowsDefaultCellStyle = dataGridViewCellStyle4;
            this.dgv_itemMovimento.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgv_itemMovimento.Size = new System.Drawing.Size(746, 268);
            this.dgv_itemMovimento.TabIndex = 0;
            this.dgv_itemMovimento.ColumnAdded += new System.Windows.Forms.DataGridViewColumnEventHandler(this.dgv_itemMovimento_ColumnAdded);
            this.dgv_itemMovimento.KeyDown += new System.Windows.Forms.KeyEventHandler(this.dgv_itemMovimento_KeyDown);
            // 
            // lbl_textoExclusao
            // 
            this.lbl_textoExclusao.AutoSize = true;
            this.lbl_textoExclusao.ForeColor = System.Drawing.Color.Blue;
            this.lbl_textoExclusao.Location = new System.Drawing.Point(4, 384);
            this.lbl_textoExclusao.Name = "lbl_textoExclusao";
            this.lbl_textoExclusao.Size = new System.Drawing.Size(431, 13);
            this.lbl_textoExclusao.TabIndex = 15;
            this.lbl_textoExclusao.Text = "Para excluir algum item do movimento tecle [DELETE] em cima do item que deseja ex" +
    "cluir.\r\n";
            // 
            // grb_totais
            // 
            this.grb_totais.BackColor = System.Drawing.Color.Transparent;
            this.grb_totais.Controls.Add(this.lbl_itens);
            this.grb_totais.Controls.Add(this.lbl_totalNF);
            this.grb_totais.Controls.Add(this.text_itensFaturados);
            this.grb_totais.Controls.Add(this.text_totalNF);
            this.grb_totais.Location = new System.Drawing.Point(7, 400);
            this.grb_totais.Name = "grb_totais";
            this.grb_totais.Size = new System.Drawing.Size(752, 52);
            this.grb_totais.TabIndex = 16;
            this.grb_totais.TabStop = false;
            this.grb_totais.Text = "Totais";
            // 
            // lbl_itens
            // 
            this.lbl_itens.BackColor = System.Drawing.Color.LightGray;
            this.lbl_itens.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lbl_itens.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_itens.Location = new System.Drawing.Point(289, 19);
            this.lbl_itens.Name = "lbl_itens";
            this.lbl_itens.ReadOnly = true;
            this.lbl_itens.Size = new System.Drawing.Size(142, 21);
            this.lbl_itens.TabIndex = 5;
            this.lbl_itens.TabStop = false;
            this.lbl_itens.Text = "Total de Itens NF";
            // 
            // lbl_totalNF
            // 
            this.lbl_totalNF.BackColor = System.Drawing.Color.LightGray;
            this.lbl_totalNF.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lbl_totalNF.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_totalNF.Location = new System.Drawing.Point(10, 19);
            this.lbl_totalNF.Name = "lbl_totalNF";
            this.lbl_totalNF.ReadOnly = true;
            this.lbl_totalNF.Size = new System.Drawing.Size(142, 21);
            this.lbl_totalNF.TabIndex = 4;
            this.lbl_totalNF.TabStop = false;
            this.lbl_totalNF.Text = "Vlr da Nota Fiscal(R$)";
            // 
            // text_itensFaturados
            // 
            this.text_itensFaturados.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.text_itensFaturados.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.text_itensFaturados.Location = new System.Drawing.Point(432, 19);
            this.text_itensFaturados.Name = "text_itensFaturados";
            this.text_itensFaturados.Size = new System.Drawing.Size(100, 21);
            this.text_itensFaturados.TabIndex = 2;
            this.text_itensFaturados.TabStop = false;
            this.text_itensFaturados.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // text_totalNF
            // 
            this.text_totalNF.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.text_totalNF.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.text_totalNF.Location = new System.Drawing.Point(153, 19);
            this.text_totalNF.MaxLength = 11;
            this.text_totalNF.Name = "text_totalNF";
            this.text_totalNF.Size = new System.Drawing.Size(107, 21);
            this.text_totalNF.TabIndex = 0;
            this.text_totalNF.TabStop = false;
            this.text_totalNF.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // lblCentroCusto
            // 
            this.lblCentroCusto.BackColor = System.Drawing.Color.LightGray;
            this.lblCentroCusto.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblCentroCusto.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCentroCusto.Location = new System.Drawing.Point(7, 67);
            this.lblCentroCusto.Name = "lblCentroCusto";
            this.lblCentroCusto.ReadOnly = true;
            this.lblCentroCusto.Size = new System.Drawing.Size(142, 21);
            this.lblCentroCusto.TabIndex = 17;
            this.lblCentroCusto.TabStop = false;
            this.lblCentroCusto.Text = "Centro de Custo";
            // 
            // textCentroCusto
            // 
            this.textCentroCusto.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.textCentroCusto.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textCentroCusto.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textCentroCusto.Location = new System.Drawing.Point(148, 67);
            this.textCentroCusto.MaxLength = 50;
            this.textCentroCusto.Name = "textCentroCusto";
            this.textCentroCusto.ReadOnly = true;
            this.textCentroCusto.Size = new System.Drawing.Size(230, 21);
            this.textCentroCusto.TabIndex = 18;
            this.textCentroCusto.TabStop = false;
            // 
            // frmFaturamento
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(784, 561);
            this.Name = "frmFaturamento";
            this.Text = "FATURAMENTO";
            this.spForm.Panel1.ResumeLayout(false);
            this.spForm.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.spForm)).EndInit();
            this.spForm.ResumeLayout(false);
            this.pnlForm.ResumeLayout(false);
            this.pnlForm.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.banner)).EndInit();
            this.flpAcao.ResumeLayout(false);
            this.grb_itensMovimento.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgv_itemMovimento)).EndInit();
            this.grb_totais.ResumeLayout(false);
            this.grb_totais.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.FlowLayoutPanel flpAcao;
        private System.Windows.Forms.Button btn_fechar;
        private System.Windows.Forms.Button btn_gravar;
        private System.Windows.Forms.TextBox lblCNPJ;
        private System.Windows.Forms.TextBox lblCliente;
        private System.Windows.Forms.TextBox lblEmpresa;
        private System.Windows.Forms.TextBox lblCondicaoPagamento;
        private System.Windows.Forms.TextBox lblFormaPagamento;
        private System.Windows.Forms.TextBox lblDataEmissao;
        private System.Windows.Forms.MaskedTextBox text_cnpj;
        private System.Windows.Forms.TextBox text_razaoSocial;
        private System.Windows.Forms.DateTimePicker dt_faturamento;
        private ComboBoxWithBorder cb_forma;
        private ComboBoxWithBorder cb_pagamento;
        private ComboBoxWithBorder cb_empresa;
        private System.Windows.Forms.GroupBox grb_itensMovimento;
        private System.Windows.Forms.DataGridView dgv_itemMovimento;
        private System.Windows.Forms.Label lbl_textoExclusao;
        private System.Windows.Forms.GroupBox grb_totais;
        private System.Windows.Forms.TextBox lbl_itens;
        private System.Windows.Forms.TextBox lbl_totalNF;
        private System.Windows.Forms.TextBox text_itensFaturados;
        private System.Windows.Forms.TextBox text_totalNF;
        private System.Windows.Forms.TextBox textCentroCusto;
        private System.Windows.Forms.TextBox lblCentroCusto;
    }
}