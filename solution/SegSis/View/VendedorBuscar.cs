﻿using SWS.Entidade;
using SWS.Facade;
using SWS.View.Resources;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace SWS.View
{
    public partial class frmVendedorBuscar : frmTemplateConsulta
    {
        private Vendedor vendedor;

        public Vendedor Vendedor
        {
            get { return vendedor; }
            set { vendedor = value; }
        }
        
        public frmVendedorBuscar()
        {
            InitializeComponent();
            validaPermissoes();
        }

        private void btn_ok_Click(object sender, EventArgs e)
        {
            try
            {
                this.Cursor = Cursors.WaitCursor;

                VendedorFacade vendedorFacade = VendedorFacade.getInstance();

                if (dgvVendedor.CurrentRow == null)
                    throw new Exception("Selecione uma linha.");

                Vendedor = vendedorFacade.findVendedorById((Int64)dgvVendedor.CurrentRow.Cells[0].Value);
                this.Close();

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }

        private void btn_incluir_Click(object sender, EventArgs e)
        {
            try
            {
                this.Cursor = Cursors.WaitCursor;

                frmVendedorIncluir formVendedorIncluir = new frmVendedorIncluir();
                formVendedorIncluir.ShowDialog();

                if (formVendedorIncluir.Vendedor != null)
                {
                    Vendedor = formVendedorIncluir.Vendedor;
                    text_nome.Text = Vendedor.Nome;
                    btn_buscar.PerformClick();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }

        private void btn_buscar_Click(object sender, EventArgs e)
        {
            try
            {
                this.dgvVendedor.Columns.Clear();
                montaDataGrid();
                text_nome.Focus();
            }
            catch (Exception Ex)
            {
                MessageBox.Show(Ex.Message);
            }
        }

        private void btn_fechar_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void validaPermissoes()
        {
            PermissionamentoFacade permissionamentoFacade = PermissionamentoFacade.getInstance();

            btn_incluir.Enabled = permissionamentoFacade.hasPermission("VENDEDOR", "INCLUIR");
        }

        public void montaDataGrid()
        {
            VendedorFacade vendedorFacade = VendedorFacade.getInstance();

            DataSet ds = vendedorFacade.findVendedorByFilter(new Vendedor(null, text_nome.Text.Trim(), String.Empty, String.Empty, String.Empty, String.Empty, String.Empty, String.Empty, String.Empty, String.Empty, String.Empty, String.Empty, String.Empty, String.Empty, null, null, ApplicationConstants.ATIVO, null));

            dgvVendedor.ColumnCount = 2;

            dgvVendedor.Columns[0].HeaderText = "ID";
            dgvVendedor.Columns[0].Visible = false;

            dgvVendedor.Columns[1].HeaderText = "Nome";

            foreach (DataRow row in ds.Tables[0].Rows)
            {
                dgvVendedor.Rows.Add(Convert.ToInt64(row["id_vend"]), row["nome"].ToString());
            }


        }
    }
}
