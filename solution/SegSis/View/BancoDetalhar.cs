﻿using SWS.Entidade;
using SWS.View.ViewHelper;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace SWS.View
{
    public partial class frmBancoDetalhar : frmBancoIncluir
    {
        public frmBancoDetalhar(Banco banco) :base()
        {
            InitializeComponent();
            textNome.Text = banco.Nome;
            textCodigo.Text = banco.CodigoBanco;
            ComboHelper.Boolean(cbBoleto, false);
            ComboHelper.Boolean(cbCaixa, false);
            cbBoleto.SelectedValue = banco.Boleto == true ? "true" : "false";
            cbCaixa.SelectedValue = banco.Caixa == true ? "true" : "false";
            btnGravar.Enabled = false;
            btnLimpar.Enabled = false;
            cbBoleto.Enabled = false;
            cbCaixa.Enabled = false;
            textNome.ReadOnly = true;
            textCodigo.ReadOnly = true;

        }
    }
}
