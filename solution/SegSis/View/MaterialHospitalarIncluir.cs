﻿using SWS.Entidade;
using SWS.Facade;
using SWS.View.Resources;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace SWS.View
{
    public partial class frmMaterialHospitalarIncluir : frmTemplate
    {
        private MaterialHospitalar materiaHospitalar;

        public MaterialHospitalar MateriaHospitalar
        {
            get { return materiaHospitalar; }
            set { materiaHospitalar = value; }
        }
        
        public frmMaterialHospitalarIncluir()
        {
            InitializeComponent();
            ActiveControl = textNome;
        }

        protected virtual void btnGravar_Click(object sender, EventArgs e)
        {
            try
            {
                if (validaCamposObrigatorio())
                {
                    EstudoFacade pcmsoFacade = EstudoFacade.getInstance();

                    materiaHospitalar = pcmsoFacade.insertMaterialHospitalar(new MaterialHospitalar(null, textNome.Text, textUnidade.Text, ApplicationConstants.ATIVO));
                    MessageBox.Show("Material hospitalar incluído com sucesso.", "Confirmação", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    this.Close();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }

        protected void btnLimpar_Click(object sender, EventArgs e)
        {
            textNome.Text = string.Empty;
            textUnidade.Text = string.Empty;
            textNome.Focus();
        }

        protected void btnFechar_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        protected void frmMaterialHospitalarIncluir_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter || e.KeyCode == Keys.Return)
                SendKeys.Send("{TAB}");
        }

        protected bool validaCamposObrigatorio()
        {
            bool retorno = true;
            
            try
            {
                if (string.IsNullOrEmpty(textNome.Text))
                    throw new Exception("A Descrição é obrigatória!");

                if (string.IsNullOrEmpty(textUnidade.Text))
                    throw new Exception ("A Unidade de Medida é obrigatória!");

            }

            catch (Exception ex)
            {
                retorno = false;
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            
            return retorno;
        }

    }
}
