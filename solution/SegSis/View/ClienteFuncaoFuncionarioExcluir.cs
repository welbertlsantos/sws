﻿using SWS.Entidade;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace SWS.View
{
    public partial class frmClienteFuncaoFuncionarioExcluir : frmTemplateConsulta
    {

        private ClienteFuncaoFuncionario clienteFuncaoFuncionario;

        private bool flagAlteracao;

        public bool FlagAlteracao
        {
            get { return flagAlteracao; }
            set { flagAlteracao = value; }
        }

        public ClienteFuncaoFuncionario ClienteFuncaoFuncionario
        {
            get { return clienteFuncaoFuncionario; }
            set { clienteFuncaoFuncionario = value; }
        }

        public frmClienteFuncaoFuncionarioExcluir(ClienteFuncaoFuncionario clienteFuncaoFuncionario)
        {
            InitializeComponent();
            this.ClienteFuncaoFuncionario = clienteFuncaoFuncionario;

            if (!ClienteFuncaoFuncionario.ClienteFuncao.Cliente.Fisica)
            {
                lblRazaoSocial.Text = "Razão Social";
                lblFantasia.Text = "Nome de Fantasia";
                lblCnpj.Text = "CNPJ";
                textCnpj.Mask = "99,999,999/9999-99";
                textRazaoSocial.Text = ClienteFuncaoFuncionario.ClienteFuncao.Cliente.RazaoSocial;
                textFantasia.Text = ClienteFuncaoFuncionario.ClienteFuncao.Cliente.Fantasia;
                textCnpj.Text = ClienteFuncaoFuncionario.ClienteFuncao.Cliente.Cnpj;
            }
            else
            {
                lblRazaoSocial.Text = "Nome";
                lblFantasia.Text = "Apelido";
                lblCnpj.Text = "CPF";
                textCnpj.Mask = "999,999,999-99";
                textRazaoSocial.Text = ClienteFuncaoFuncionario.ClienteFuncao.Cliente.RazaoSocial;
                textFantasia.Text = ClienteFuncaoFuncionario.ClienteFuncao.Cliente.Fantasia;
                textCnpj.Text = ClienteFuncaoFuncionario.ClienteFuncao.Cliente.Cnpj;
            }
            dataDemissao.Checked = true;
        }

        private void btConfirmar_Click(object sender, EventArgs e)
        {
            try
            {
                if (!dataDemissao.Checked)
                    throw new Exception("A data de demissão não pode ficar em branco");

                if (clienteFuncaoFuncionario.DataCadastro > Convert.ToDateTime(dataDemissao.Text))
                    throw new Exception("A data de demissão não poderá ser menor do que a data: " + String.Format("{0:dd/MM/yyyy}", clienteFuncaoFuncionario.DataCadastro) + ".");

                clienteFuncaoFuncionario.DataDesligamento = Convert.ToDateTime(dataDemissao.Text);
                flagAlteracao = true;
                this.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        
        }

        private void btFechar_Click(object sender, EventArgs e)
        {
            try
            {
                if (!dataDemissao.Checked) throw new Exception("Você deve selecionar a data de demissão para continuar o processo");
                btConfirmar.PerformClick();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            
            
        }
    }
}
