﻿namespace SWS.View
{
    partial class frmAtendimentoGheAvulso
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.flpAcao = new System.Windows.Forms.FlowLayoutPanel();
            this.btnGravar = new System.Windows.Forms.Button();
            this.btnFechar = new System.Windows.Forms.Button();
            this.textGhe = new System.Windows.Forms.TextBox();
            this.lblGhe = new System.Windows.Forms.TextBox();
            this.textSetor = new System.Windows.Forms.TextBox();
            this.lblSetor = new System.Windows.Forms.TextBox();
            ((System.ComponentModel.ISupportInitialize)(this.scForm)).BeginInit();
            this.scForm.Panel1.SuspendLayout();
            this.scForm.Panel2.SuspendLayout();
            this.scForm.SuspendLayout();
            this.pnlForm.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.banner)).BeginInit();
            this.flpAcao.SuspendLayout();
            this.SuspendLayout();
            // 
            // scForm
            // 
            // 
            // scForm.Panel1
            // 
            this.scForm.Panel1.Controls.Add(this.flpAcao);
            // 
            // pnlForm
            // 
            this.pnlForm.Controls.Add(this.textSetor);
            this.pnlForm.Controls.Add(this.lblSetor);
            this.pnlForm.Controls.Add(this.textGhe);
            this.pnlForm.Controls.Add(this.lblGhe);
            this.pnlForm.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlForm.Size = new System.Drawing.Size(534, 286);
            // 
            // flpAcao
            // 
            this.flpAcao.Controls.Add(this.btnGravar);
            this.flpAcao.Controls.Add(this.btnFechar);
            this.flpAcao.Dock = System.Windows.Forms.DockStyle.Fill;
            this.flpAcao.Location = new System.Drawing.Point(0, 0);
            this.flpAcao.Name = "flpAcao";
            this.flpAcao.Size = new System.Drawing.Size(534, 30);
            this.flpAcao.TabIndex = 0;
            // 
            // btnGravar
            // 
            this.btnGravar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnGravar.Image = global::SWS.Properties.Resources.Gravar;
            this.btnGravar.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnGravar.Location = new System.Drawing.Point(3, 3);
            this.btnGravar.Name = "btnGravar";
            this.btnGravar.Size = new System.Drawing.Size(75, 23);
            this.btnGravar.TabIndex = 3;
            this.btnGravar.Text = "&Gravar";
            this.btnGravar.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnGravar.UseVisualStyleBackColor = true;
            this.btnGravar.Click += new System.EventHandler(this.btnGravar_Click);
            // 
            // btnFechar
            // 
            this.btnFechar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnFechar.Image = global::SWS.Properties.Resources.close;
            this.btnFechar.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnFechar.Location = new System.Drawing.Point(84, 3);
            this.btnFechar.Name = "btnFechar";
            this.btnFechar.Size = new System.Drawing.Size(75, 23);
            this.btnFechar.TabIndex = 1;
            this.btnFechar.TabStop = false;
            this.btnFechar.Text = "&Fechar";
            this.btnFechar.UseVisualStyleBackColor = true;
            this.btnFechar.Click += new System.EventHandler(this.btnFechar_Click);
            // 
            // textGhe
            // 
            this.textGhe.BackColor = System.Drawing.Color.White;
            this.textGhe.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textGhe.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.textGhe.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textGhe.ForeColor = System.Drawing.Color.Blue;
            this.textGhe.Location = new System.Drawing.Point(158, 23);
            this.textGhe.MaxLength = 100;
            this.textGhe.Name = "textGhe";
            this.textGhe.Size = new System.Drawing.Size(364, 21);
            this.textGhe.TabIndex = 1;
            // 
            // lblGhe
            // 
            this.lblGhe.BackColor = System.Drawing.Color.LightGray;
            this.lblGhe.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblGhe.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblGhe.Location = new System.Drawing.Point(12, 23);
            this.lblGhe.MaxLength = 15;
            this.lblGhe.Name = "lblGhe";
            this.lblGhe.ReadOnly = true;
            this.lblGhe.Size = new System.Drawing.Size(147, 21);
            this.lblGhe.TabIndex = 2;
            this.lblGhe.TabStop = false;
            this.lblGhe.Text = "GHE";
            // 
            // textSetor
            // 
            this.textSetor.BackColor = System.Drawing.Color.White;
            this.textSetor.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textSetor.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.textSetor.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textSetor.ForeColor = System.Drawing.Color.Blue;
            this.textSetor.Location = new System.Drawing.Point(158, 43);
            this.textSetor.MaxLength = 100;
            this.textSetor.Name = "textSetor";
            this.textSetor.Size = new System.Drawing.Size(364, 21);
            this.textSetor.TabIndex = 2;
            // 
            // lblSetor
            // 
            this.lblSetor.BackColor = System.Drawing.Color.LightGray;
            this.lblSetor.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblSetor.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSetor.Location = new System.Drawing.Point(12, 43);
            this.lblSetor.MaxLength = 15;
            this.lblSetor.Name = "lblSetor";
            this.lblSetor.ReadOnly = true;
            this.lblSetor.Size = new System.Drawing.Size(147, 21);
            this.lblSetor.TabIndex = 4;
            this.lblSetor.TabStop = false;
            this.lblSetor.Text = "Setor(es)";
            // 
            // frmAtendimentoGheAvulso
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(534, 361);
            this.Name = "frmAtendimentoGheAvulso";
            this.Text = "INCLUIR GHE AVULSO ATENDIMENTO";
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.frmAtendimentoGheAvulso_KeyDown);
            this.scForm.Panel1.ResumeLayout(false);
            this.scForm.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.scForm)).EndInit();
            this.scForm.ResumeLayout(false);
            this.pnlForm.ResumeLayout(false);
            this.pnlForm.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.banner)).EndInit();
            this.flpAcao.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        protected System.Windows.Forms.FlowLayoutPanel flpAcao;
        protected System.Windows.Forms.Button btnGravar;
        protected System.Windows.Forms.Button btnFechar;
        protected System.Windows.Forms.TextBox textSetor;
        protected System.Windows.Forms.TextBox lblSetor;
        protected System.Windows.Forms.TextBox textGhe;
        protected System.Windows.Forms.TextBox lblGhe;
    }
}