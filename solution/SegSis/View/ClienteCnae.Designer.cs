﻿namespace SegSis.View
{
    partial class frm_cliente_cnae_selecionar
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.grb_paginacao = new System.Windows.Forms.GroupBox();
            this.btn_limpar = new System.Windows.Forms.Button();
            this.btn_fechar = new System.Windows.Forms.Button();
            this.btn_ok = new System.Windows.Forms.Button();
            this.grb_gride = new System.Windows.Forms.GroupBox();
            this.grd_cnae = new System.Windows.Forms.DataGridView();
            this.grb_dados = new System.Windows.Forms.GroupBox();
            this.text_codCnae = new System.Windows.Forms.MaskedTextBox();
            this.lbl_codigo = new System.Windows.Forms.Label();
            this.btn_buscar = new System.Windows.Forms.Button();
            this.lbl_atividade = new System.Windows.Forms.Label();
            this.text_atividade = new System.Windows.Forms.TextBox();
            this.grb_paginacao.SuspendLayout();
            this.grb_gride.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grd_cnae)).BeginInit();
            this.grb_dados.SuspendLayout();
            this.SuspendLayout();
            // 
            // grb_paginacao
            // 
            this.grb_paginacao.Controls.Add(this.btn_limpar);
            this.grb_paginacao.Controls.Add(this.btn_fechar);
            this.grb_paginacao.Controls.Add(this.btn_ok);
            this.grb_paginacao.Location = new System.Drawing.Point(12, 341);
            this.grb_paginacao.Name = "grb_paginacao";
            this.grb_paginacao.Size = new System.Drawing.Size(576, 47);
            this.grb_paginacao.TabIndex = 40;
            this.grb_paginacao.TabStop = false;
            // 
            // btn_limpar
            // 
            this.btn_limpar.Location = new System.Drawing.Point(364, 18);
            this.btn_limpar.Name = "btn_limpar";
            this.btn_limpar.Size = new System.Drawing.Size(75, 23);
            this.btn_limpar.TabIndex = 37;
            this.btn_limpar.Text = "&Limpar";
            this.btn_limpar.UseVisualStyleBackColor = true;
            this.btn_limpar.Click += new System.EventHandler(this.btn_limpar_Click);
            // 
            // btn_fechar
            // 
            this.btn_fechar.Location = new System.Drawing.Point(266, 18);
            this.btn_fechar.Name = "btn_fechar";
            this.btn_fechar.Size = new System.Drawing.Size(75, 23);
            this.btn_fechar.TabIndex = 36;
            this.btn_fechar.Text = "&Fechar";
            this.btn_fechar.UseVisualStyleBackColor = true;
            this.btn_fechar.Click += new System.EventHandler(this.btn_fechar_Click);
            // 
            // btn_ok
            // 
            this.btn_ok.Location = new System.Drawing.Point(167, 18);
            this.btn_ok.Name = "btn_ok";
            this.btn_ok.Size = new System.Drawing.Size(77, 23);
            this.btn_ok.TabIndex = 35;
            this.btn_ok.Text = "&Ok";
            this.btn_ok.UseVisualStyleBackColor = true;
            this.btn_ok.Click += new System.EventHandler(this.btn_ok_Click);
            // 
            // grb_gride
            // 
            this.grb_gride.BackColor = System.Drawing.Color.Transparent;
            this.grb_gride.Controls.Add(this.grd_cnae);
            this.grb_gride.Location = new System.Drawing.Point(12, 120);
            this.grb_gride.Name = "grb_gride";
            this.grb_gride.Size = new System.Drawing.Size(576, 215);
            this.grb_gride.TabIndex = 39;
            this.grb_gride.TabStop = false;
            this.grb_gride.Text = "Cnae";
            // 
            // grd_cnae
            // 
            this.grd_cnae.AllowUserToAddRows = false;
            this.grd_cnae.AllowUserToDeleteRows = false;
            this.grd_cnae.AllowUserToOrderColumns = true;
            this.grd_cnae.AllowUserToResizeRows = false;
            this.grd_cnae.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.DisplayedCells;
            this.grd_cnae.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.grd_cnae.Location = new System.Drawing.Point(6, 19);
            this.grd_cnae.Name = "grd_cnae";
            this.grd_cnae.ReadOnly = true;
            this.grd_cnae.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.grd_cnae.Size = new System.Drawing.Size(564, 190);
            this.grd_cnae.TabIndex = 0;
            // 
            // grb_dados
            // 
            this.grb_dados.Controls.Add(this.text_codCnae);
            this.grb_dados.Controls.Add(this.lbl_codigo);
            this.grb_dados.Controls.Add(this.btn_buscar);
            this.grb_dados.Controls.Add(this.lbl_atividade);
            this.grb_dados.Controls.Add(this.text_atividade);
            this.grb_dados.Location = new System.Drawing.Point(12, 12);
            this.grb_dados.Name = "grb_dados";
            this.grb_dados.Size = new System.Drawing.Size(576, 102);
            this.grb_dados.TabIndex = 38;
            this.grb_dados.TabStop = false;
            this.grb_dados.Text = "Dados";
            // 
            // text_codCnae
            // 
            this.text_codCnae.Location = new System.Drawing.Point(74, 28);
            this.text_codCnae.Mask = "00,00-0/00";
            this.text_codCnae.Name = "text_codCnae";
            this.text_codCnae.PromptChar = ' ';
            this.text_codCnae.Size = new System.Drawing.Size(90, 20);
            this.text_codCnae.TabIndex = 21;
            // 
            // lbl_codigo
            // 
            this.lbl_codigo.AutoSize = true;
            this.lbl_codigo.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_codigo.Location = new System.Drawing.Point(13, 28);
            this.lbl_codigo.Name = "lbl_codigo";
            this.lbl_codigo.Size = new System.Drawing.Size(40, 13);
            this.lbl_codigo.TabIndex = 20;
            this.lbl_codigo.Text = "Codigo";
            // 
            // btn_buscar
            // 
            this.btn_buscar.Location = new System.Drawing.Point(484, 60);
            this.btn_buscar.Name = "btn_buscar";
            this.btn_buscar.Size = new System.Drawing.Size(65, 23);
            this.btn_buscar.TabIndex = 19;
            this.btn_buscar.Text = "&Buscar";
            this.btn_buscar.UseVisualStyleBackColor = true;
            this.btn_buscar.Click += new System.EventHandler(this.btn_buscar_Click);
            // 
            // lbl_atividade
            // 
            this.lbl_atividade.AutoSize = true;
            this.lbl_atividade.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_atividade.Location = new System.Drawing.Point(13, 64);
            this.lbl_atividade.Name = "lbl_atividade";
            this.lbl_atividade.Size = new System.Drawing.Size(51, 13);
            this.lbl_atividade.TabIndex = 7;
            this.lbl_atividade.Text = "Atividade";
            // 
            // text_atividade
            // 
            this.text_atividade.Location = new System.Drawing.Point(74, 62);
            this.text_atividade.MaxLength = 50;
            this.text_atividade.Name = "text_atividade";
            this.text_atividade.Size = new System.Drawing.Size(404, 20);
            this.text_atividade.TabIndex = 0;
            // 
            // frm_cliente_cnae_selecionar
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(600, 400);
            this.Controls.Add(this.grb_paginacao);
            this.Controls.Add(this.grb_gride);
            this.Controls.Add(this.grb_dados);
            this.Name = "frm_cliente_cnae_selecionar";
            this.Text = "Selecionar Cnae";
            this.grb_paginacao.ResumeLayout(false);
            this.grb_gride.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.grd_cnae)).EndInit();
            this.grb_dados.ResumeLayout(false);
            this.grb_dados.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox grb_paginacao;
        private System.Windows.Forms.Button btn_fechar;
        private System.Windows.Forms.Button btn_ok;
        private System.Windows.Forms.GroupBox grb_gride;
        private System.Windows.Forms.DataGridView grd_cnae;
        private System.Windows.Forms.GroupBox grb_dados;
        private System.Windows.Forms.Button btn_buscar;
        private System.Windows.Forms.TextBox text_atividade;
        private System.Windows.Forms.MaskedTextBox text_codCnae;
        private System.Windows.Forms.Label lbl_codigo;
        private System.Windows.Forms.Label lbl_atividade;
        private System.Windows.Forms.Button btn_limpar;
    }
}