﻿namespace SWS.View
{
    partial class frm_ppraIncluirOld
    {

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        protected void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle7 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle8 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle9 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle10 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle11 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frm_ppraIncluirOld));
            this.grb_paginacao = new System.Windows.Forms.GroupBox();
            this.btn_fechar = new System.Windows.Forms.Button();
            this.lbl_codPpra = new System.Windows.Forms.Label();
            this.grb_dados = new System.Windows.Forms.GroupBox();
            this.grb_grupo1 = new System.Windows.Forms.GroupBox();
            this.text_fimContr = new System.Windows.Forms.TextBox();
            this.lbl_inicioContr = new System.Windows.Forms.Label();
            this.text_inicioContr = new System.Windows.Forms.TextBox();
            this.lbl_contrato = new System.Windows.Forms.Label();
            this.text_numContrato = new System.Windows.Forms.TextBox();
            this.lbl_fimContrato = new System.Windows.Forms.Label();
            this.text_clienteContratante = new System.Windows.Forms.TextBox();
            this.btn_contratante = new System.Windows.Forms.Button();
            this.grb_cliente = new System.Windows.Forms.GroupBox();
            this.text_cliente = new System.Windows.Forms.TextBox();
            this.btn_cliente = new System.Windows.Forms.Button();
            this.grb_tecnico = new System.Windows.Forms.GroupBox();
            this.btn_tecnico = new System.Windows.Forms.Button();
            this.text_tecnico = new System.Windows.Forms.TextBox();
            this.lbl_vencimento = new System.Windows.Forms.Label();
            this.grb_grauRisco = new System.Windows.Forms.GroupBox();
            this.cb_grauRisco = new System.Windows.Forms.ComboBox();
            this.grb_data = new System.Windows.Forms.GroupBox();
            this.lbl_dataVencimento = new System.Windows.Forms.Label();
            this.lbl_dataCriacao = new System.Windows.Forms.Label();
            this.dt_criacao = new System.Windows.Forms.DateTimePicker();
            this.dt_vencimento = new System.Windows.Forms.DateTimePicker();
            this.text_codPpra = new System.Windows.Forms.MaskedTextBox();
            this.tb_dados = new System.Windows.Forms.TabControl();
            this.tb_dadosPPRA = new System.Windows.Forms.TabPage();
            this.tbConfiguracaoExtra = new System.Windows.Forms.TabControl();
            this.tpDados = new System.Windows.Forms.TabPage();
            this.lbl_endereco = new System.Windows.Forms.Label();
            this.text_localObra = new System.Windows.Forms.TextBox();
            this.lbl_UF = new System.Windows.Forms.Label();
            this.lbl_local = new System.Windows.Forms.Label();
            this.text_endereco = new System.Windows.Forms.TextBox();
            this.cb_uf = new System.Windows.Forms.ComboBox();
            this.lbl_bairro = new System.Windows.Forms.Label();
            this.text_cep = new System.Windows.Forms.MaskedTextBox();
            this.text_bairro = new System.Windows.Forms.TextBox();
            this.lbl_cep = new System.Windows.Forms.Label();
            this.lbl_obra = new System.Windows.Forms.Label();
            this.text_obra = new System.Windows.Forms.TextBox();
            this.lbl_numero = new System.Windows.Forms.Label();
            this.text_cidade = new System.Windows.Forms.TextBox();
            this.text_numero = new System.Windows.Forms.TextBox();
            this.lbl_cidade = new System.Windows.Forms.Label();
            this.lbl_complemento = new System.Windows.Forms.Label();
            this.text_complemento = new System.Windows.Forms.TextBox();
            this.tpPrevisao = new System.Windows.Forms.TabPage();
            this.btnExcluirPrevisao = new System.Windows.Forms.Button();
            this.btnIncluirPrevisao = new System.Windows.Forms.Button();
            this.dgPrevisao = new System.Windows.Forms.DataGridView();
            this.tpCNAE = new System.Windows.Forms.TabPage();
            this.tcCnae = new System.Windows.Forms.TabControl();
            this.cnaeCliente = new System.Windows.Forms.TabPage();
            this.dgvCnaeCliente = new System.Windows.Forms.DataGridView();
            this.cnaeContratante = new System.Windows.Forms.TabPage();
            this.dgvCnaeContratante = new System.Windows.Forms.DataGridView();
            this.lblCNAECliente = new System.Windows.Forms.Label();
            this.btn_salvar = new System.Windows.Forms.Button();
            this.btn_limpar = new System.Windows.Forms.Button();
            this.tb_ghe = new System.Windows.Forms.TabPage();
            this.btn_alterarGhe = new System.Windows.Forms.Button();
            this.grb_ghePPRA = new System.Windows.Forms.GroupBox();
            this.grd_ghe = new System.Windows.Forms.DataGridView();
            this.btn_incluirGHE = new System.Windows.Forms.Button();
            this.btn_excluirGHE = new System.Windows.Forms.Button();
            this.tb_gheFonte = new System.Windows.Forms.TabPage();
            this.tb_fonteGHE = new System.Windows.Forms.TabControl();
            this.Fonte = new System.Windows.Forms.TabPage();
            this.grb_gheFonte = new System.Windows.Forms.GroupBox();
            this.cb_ghe = new System.Windows.Forms.ComboBox();
            this.btn_excluirFonte = new System.Windows.Forms.Button();
            this.grb_fonte = new System.Windows.Forms.GroupBox();
            this.grd_fonte = new System.Windows.Forms.DataGridView();
            this.btn_incluirFonte = new System.Windows.Forms.Button();
            this.agente = new System.Windows.Forms.TabPage();
            this.btn_excluirAgente = new System.Windows.Forms.Button();
            this.grb_gheAgente = new System.Windows.Forms.GroupBox();
            this.cb_gheAgente = new System.Windows.Forms.ComboBox();
            this.btn_incluirAgente = new System.Windows.Forms.Button();
            this.grb_fonteAgente = new System.Windows.Forms.GroupBox();
            this.cb_fonteAgente = new System.Windows.Forms.ComboBox();
            this.grb_agente = new System.Windows.Forms.GroupBox();
            this.grd_agente = new System.Windows.Forms.DataGridView();
            this.epi = new System.Windows.Forms.TabPage();
            this.btn_excluirEPI = new System.Windows.Forms.Button();
            this.grb_epi = new System.Windows.Forms.GroupBox();
            this.grd_epi = new System.Windows.Forms.DataGridView();
            this.btn_incluirEpi = new System.Windows.Forms.Button();
            this.grb_agenteEpi = new System.Windows.Forms.GroupBox();
            this.cb_agenteEpi = new System.Windows.Forms.ComboBox();
            this.grb_fonteEpi = new System.Windows.Forms.GroupBox();
            this.cb_fonteEpi = new System.Windows.Forms.ComboBox();
            this.grb_gheEPI = new System.Windows.Forms.GroupBox();
            this.cb_gheEPI = new System.Windows.Forms.ComboBox();
            this.tb_setorGHE = new System.Windows.Forms.TabPage();
            this.tb_gheSetor = new System.Windows.Forms.TabControl();
            this.tb_setor = new System.Windows.Forms.TabPage();
            this.grb_setor = new System.Windows.Forms.GroupBox();
            this.grd_setor = new System.Windows.Forms.DataGridView();
            this.btn_excluirSetor = new System.Windows.Forms.Button();
            this.grb_gheSetor = new System.Windows.Forms.GroupBox();
            this.cb_gheSetor = new System.Windows.Forms.ComboBox();
            this.btn_incluirSetor = new System.Windows.Forms.Button();
            this.tb_funcao = new System.Windows.Forms.TabPage();
            this.btn_excluirFuncaoClientePPRA = new System.Windows.Forms.Button();
            this.btn_incluirFuncaoClientePPRA = new System.Windows.Forms.Button();
            this.grb_funcao = new System.Windows.Forms.GroupBox();
            this.grd_funcaoCliente = new System.Windows.Forms.DataGridView();
            this.tb_funcaoPPRA = new System.Windows.Forms.TabPage();
            this.btn_excluirFuncaoCliente = new System.Windows.Forms.Button();
            this.grb_funcaoCliente = new System.Windows.Forms.GroupBox();
            this.grd_setorFuncao = new System.Windows.Forms.DataGridView();
            this.btn_incluirFuncaoCliente = new System.Windows.Forms.Button();
            this.grb_setorFuncao = new System.Windows.Forms.GroupBox();
            this.cb_setorFuncao = new System.Windows.Forms.ComboBox();
            this.grb_gheFuncao = new System.Windows.Forms.GroupBox();
            this.cb_gheFuncao = new System.Windows.Forms.ComboBox();
            this.tb_cronograma = new System.Windows.Forms.TabPage();
            this.btn_excluirAtividade = new System.Windows.Forms.Button();
            this.grb_atividade = new System.Windows.Forms.GroupBox();
            this.grd_atividadeCronograma = new System.Windows.Forms.DataGridView();
            this.btn_alterarAtividade = new System.Windows.Forms.Button();
            this.btn_incluirAtividade = new System.Windows.Forms.Button();
            this.grb_comentario = new System.Windows.Forms.TabPage();
            this.btn_gravarComentario = new System.Windows.Forms.Button();
            this.text_comentario = new System.Windows.Forms.TextBox();
            this.lbl_comentario = new System.Windows.Forms.Label();
            this.tb_anexo = new System.Windows.Forms.TabPage();
            this.lblTituloAnexo = new System.Windows.Forms.Label();
            this.btn_excluirAnexo = new System.Windows.Forms.Button();
            this.btn_incluirAnexo = new System.Windows.Forms.Button();
            this.grb_anexo = new System.Windows.Forms.GroupBox();
            this.grd_anexo = new System.Windows.Forms.DataGridView();
            this.text_conteudo = new System.Windows.Forms.TextBox();
            this.IncluirPPRAErrorProvider = new System.Windows.Forms.ErrorProvider(this.components);
            this.tpPpra = new System.Windows.Forms.ToolTip(this.components);
            this.panel_banner.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox_banner)).BeginInit();
            this.panel.SuspendLayout();
            this.grb_paginacao.SuspendLayout();
            this.grb_dados.SuspendLayout();
            this.grb_grupo1.SuspendLayout();
            this.grb_cliente.SuspendLayout();
            this.grb_tecnico.SuspendLayout();
            this.grb_grauRisco.SuspendLayout();
            this.grb_data.SuspendLayout();
            this.tb_dados.SuspendLayout();
            this.tb_dadosPPRA.SuspendLayout();
            this.tbConfiguracaoExtra.SuspendLayout();
            this.tpDados.SuspendLayout();
            this.tpPrevisao.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgPrevisao)).BeginInit();
            this.tpCNAE.SuspendLayout();
            this.tcCnae.SuspendLayout();
            this.cnaeCliente.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvCnaeCliente)).BeginInit();
            this.cnaeContratante.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvCnaeContratante)).BeginInit();
            this.tb_ghe.SuspendLayout();
            this.grb_ghePPRA.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grd_ghe)).BeginInit();
            this.tb_gheFonte.SuspendLayout();
            this.tb_fonteGHE.SuspendLayout();
            this.Fonte.SuspendLayout();
            this.grb_gheFonte.SuspendLayout();
            this.grb_fonte.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grd_fonte)).BeginInit();
            this.agente.SuspendLayout();
            this.grb_gheAgente.SuspendLayout();
            this.grb_fonteAgente.SuspendLayout();
            this.grb_agente.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grd_agente)).BeginInit();
            this.epi.SuspendLayout();
            this.grb_epi.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grd_epi)).BeginInit();
            this.grb_agenteEpi.SuspendLayout();
            this.grb_fonteEpi.SuspendLayout();
            this.grb_gheEPI.SuspendLayout();
            this.tb_setorGHE.SuspendLayout();
            this.tb_gheSetor.SuspendLayout();
            this.tb_setor.SuspendLayout();
            this.grb_setor.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grd_setor)).BeginInit();
            this.grb_gheSetor.SuspendLayout();
            this.tb_funcao.SuspendLayout();
            this.grb_funcao.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grd_funcaoCliente)).BeginInit();
            this.tb_funcaoPPRA.SuspendLayout();
            this.grb_funcaoCliente.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grd_setorFuncao)).BeginInit();
            this.grb_setorFuncao.SuspendLayout();
            this.grb_gheFuncao.SuspendLayout();
            this.tb_cronograma.SuspendLayout();
            this.grb_atividade.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grd_atividadeCronograma)).BeginInit();
            this.grb_comentario.SuspendLayout();
            this.tb_anexo.SuspendLayout();
            this.grb_anexo.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grd_anexo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.IncluirPPRAErrorProvider)).BeginInit();
            this.SuspendLayout();
            // 
            // panel
            // 
            this.panel.Controls.Add(this.grb_paginacao);
            this.panel.Controls.Add(this.tb_dados);
            // 
            // grb_paginacao
            // 
            this.grb_paginacao.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.grb_paginacao.BackColor = System.Drawing.Color.White;
            this.grb_paginacao.Controls.Add(this.btn_fechar);
            this.grb_paginacao.Location = new System.Drawing.Point(2, 464);
            this.grb_paginacao.Name = "grb_paginacao";
            this.grb_paginacao.Size = new System.Drawing.Size(782, 51);
            this.grb_paginacao.TabIndex = 23;
            this.grb_paginacao.TabStop = false;
            this.grb_paginacao.Text = "Ações";
            // 
            // btn_fechar
            // 
            this.btn_fechar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_fechar.Image = global::SWS.Properties.Resources.close;
            this.btn_fechar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btn_fechar.Location = new System.Drawing.Point(14, 19);
            this.btn_fechar.Name = "btn_fechar";
            this.btn_fechar.Size = new System.Drawing.Size(77, 23);
            this.btn_fechar.TabIndex = 23;
            this.btn_fechar.Text = "&Fechar";
            this.btn_fechar.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btn_fechar.UseVisualStyleBackColor = true;
            this.btn_fechar.Click += new System.EventHandler(this.btn_fechar_Click);
            // 
            // lbl_codPpra
            // 
            this.lbl_codPpra.AutoSize = true;
            this.lbl_codPpra.Location = new System.Drawing.Point(3, 14);
            this.lbl_codPpra.Name = "lbl_codPpra";
            this.lbl_codPpra.Size = new System.Drawing.Size(72, 13);
            this.lbl_codPpra.TabIndex = 17;
            this.lbl_codPpra.Text = "Código PPRA";
            // 
            // grb_dados
            // 
            this.grb_dados.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.grb_dados.Controls.Add(this.grb_grupo1);
            this.grb_dados.Controls.Add(this.grb_cliente);
            this.grb_dados.Controls.Add(this.grb_tecnico);
            this.grb_dados.Controls.Add(this.lbl_vencimento);
            this.grb_dados.Controls.Add(this.grb_grauRisco);
            this.grb_dados.Controls.Add(this.grb_data);
            this.grb_dados.Controls.Add(this.text_codPpra);
            this.grb_dados.Controls.Add(this.lbl_codPpra);
            this.grb_dados.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.grb_dados.Location = new System.Drawing.Point(3, 6);
            this.grb_dados.Name = "grb_dados";
            this.grb_dados.Size = new System.Drawing.Size(751, 157);
            this.grb_dados.TabIndex = 0;
            this.grb_dados.TabStop = false;
            this.grb_dados.Text = "Dados";
            // 
            // grb_grupo1
            // 
            this.grb_grupo1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.grb_grupo1.Controls.Add(this.text_fimContr);
            this.grb_grupo1.Controls.Add(this.lbl_inicioContr);
            this.grb_grupo1.Controls.Add(this.text_inicioContr);
            this.grb_grupo1.Controls.Add(this.lbl_contrato);
            this.grb_grupo1.Controls.Add(this.text_numContrato);
            this.grb_grupo1.Controls.Add(this.lbl_fimContrato);
            this.grb_grupo1.Controls.Add(this.text_clienteContratante);
            this.grb_grupo1.Controls.Add(this.btn_contratante);
            this.grb_grupo1.Enabled = false;
            this.grb_grupo1.Location = new System.Drawing.Point(6, 68);
            this.grb_grupo1.Name = "grb_grupo1";
            this.grb_grupo1.Size = new System.Drawing.Size(403, 80);
            this.grb_grupo1.TabIndex = 3;
            this.grb_grupo1.TabStop = false;
            this.grb_grupo1.Text = "Cliente Contratante";
            // 
            // text_fimContr
            // 
            this.text_fimContr.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.text_fimContr.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.text_fimContr.Enabled = false;
            this.text_fimContr.ForeColor = System.Drawing.Color.White;
            this.text_fimContr.Location = new System.Drawing.Point(121, 54);
            this.text_fimContr.MaxLength = 15;
            this.text_fimContr.Name = "text_fimContr";
            this.text_fimContr.Size = new System.Drawing.Size(104, 20);
            this.text_fimContr.TabIndex = 6;
            // 
            // lbl_inicioContr
            // 
            this.lbl_inicioContr.AutoSize = true;
            this.lbl_inicioContr.Location = new System.Drawing.Point(7, 40);
            this.lbl_inicioContr.Name = "lbl_inicioContr";
            this.lbl_inicioContr.Size = new System.Drawing.Size(77, 13);
            this.lbl_inicioContr.TabIndex = 29;
            this.lbl_inicioContr.Text = "Início Contrato";
            // 
            // text_inicioContr
            // 
            this.text_inicioContr.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.text_inicioContr.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.text_inicioContr.Enabled = false;
            this.text_inicioContr.ForeColor = System.Drawing.Color.White;
            this.text_inicioContr.Location = new System.Drawing.Point(7, 54);
            this.text_inicioContr.MaxLength = 15;
            this.text_inicioContr.Name = "text_inicioContr";
            this.text_inicioContr.Size = new System.Drawing.Size(104, 20);
            this.text_inicioContr.TabIndex = 5;
            // 
            // lbl_contrato
            // 
            this.lbl_contrato.AutoSize = true;
            this.lbl_contrato.Location = new System.Drawing.Point(238, 38);
            this.lbl_contrato.Name = "lbl_contrato";
            this.lbl_contrato.Size = new System.Drawing.Size(102, 13);
            this.lbl_contrato.TabIndex = 27;
            this.lbl_contrato.Text = "Número do Contrato";
            // 
            // text_numContrato
            // 
            this.text_numContrato.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.text_numContrato.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.text_numContrato.Enabled = false;
            this.text_numContrato.ForeColor = System.Drawing.Color.White;
            this.text_numContrato.Location = new System.Drawing.Point(239, 54);
            this.text_numContrato.MaxLength = 15;
            this.text_numContrato.Name = "text_numContrato";
            this.text_numContrato.Size = new System.Drawing.Size(156, 20);
            this.text_numContrato.TabIndex = 7;
            // 
            // lbl_fimContrato
            // 
            this.lbl_fimContrato.AutoSize = true;
            this.lbl_fimContrato.Location = new System.Drawing.Point(121, 40);
            this.lbl_fimContrato.Name = "lbl_fimContrato";
            this.lbl_fimContrato.Size = new System.Drawing.Size(66, 13);
            this.lbl_fimContrato.TabIndex = 25;
            this.lbl_fimContrato.Text = "Fim Contrato";
            // 
            // text_clienteContratante
            // 
            this.text_clienteContratante.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.text_clienteContratante.BackColor = System.Drawing.Color.LightGray;
            this.text_clienteContratante.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.text_clienteContratante.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.text_clienteContratante.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.text_clienteContratante.ForeColor = System.Drawing.Color.Black;
            this.text_clienteContratante.Location = new System.Drawing.Point(7, 14);
            this.text_clienteContratante.MaxLength = 100;
            this.text_clienteContratante.Name = "text_clienteContratante";
            this.text_clienteContratante.ReadOnly = true;
            this.text_clienteContratante.Size = new System.Drawing.Size(363, 20);
            this.text_clienteContratante.TabIndex = 3;
            this.text_clienteContratante.TabStop = false;
            // 
            // btn_contratante
            // 
            this.btn_contratante.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btn_contratante.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_contratante.Image = global::SWS.Properties.Resources.busca;
            this.btn_contratante.Location = new System.Drawing.Point(367, 14);
            this.btn_contratante.Name = "btn_contratante";
            this.btn_contratante.Size = new System.Drawing.Size(28, 20);
            this.btn_contratante.TabIndex = 4;
            this.btn_contratante.UseVisualStyleBackColor = true;
            this.btn_contratante.Click += new System.EventHandler(this.btn_contratante_Click);
            // 
            // grb_cliente
            // 
            this.grb_cliente.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.grb_cliente.Controls.Add(this.text_cliente);
            this.grb_cliente.Controls.Add(this.btn_cliente);
            this.grb_cliente.Location = new System.Drawing.Point(134, 10);
            this.grb_cliente.Name = "grb_cliente";
            this.grb_cliente.Size = new System.Drawing.Size(275, 52);
            this.grb_cliente.TabIndex = 1;
            this.grb_cliente.TabStop = false;
            this.grb_cliente.Text = "Cliente";
            // 
            // text_cliente
            // 
            this.text_cliente.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.text_cliente.BackColor = System.Drawing.Color.LightGray;
            this.text_cliente.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.text_cliente.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.text_cliente.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.text_cliente.ForeColor = System.Drawing.Color.Black;
            this.text_cliente.Location = new System.Drawing.Point(6, 15);
            this.text_cliente.MaxLength = 100;
            this.text_cliente.Name = "text_cliente";
            this.text_cliente.ReadOnly = true;
            this.text_cliente.Size = new System.Drawing.Size(236, 20);
            this.text_cliente.TabIndex = 0;
            this.text_cliente.TabStop = false;
            // 
            // btn_cliente
            // 
            this.btn_cliente.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btn_cliente.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_cliente.Image = global::SWS.Properties.Resources.busca;
            this.btn_cliente.Location = new System.Drawing.Point(241, 15);
            this.btn_cliente.Name = "btn_cliente";
            this.btn_cliente.Size = new System.Drawing.Size(28, 20);
            this.btn_cliente.TabIndex = 1;
            this.btn_cliente.UseVisualStyleBackColor = true;
            this.btn_cliente.Click += new System.EventHandler(this.btn_cliente_Click);
            // 
            // grb_tecnico
            // 
            this.grb_tecnico.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.grb_tecnico.Controls.Add(this.btn_tecnico);
            this.grb_tecnico.Controls.Add(this.text_tecnico);
            this.grb_tecnico.Location = new System.Drawing.Point(415, 14);
            this.grb_tecnico.Name = "grb_tecnico";
            this.grb_tecnico.Size = new System.Drawing.Size(330, 48);
            this.grb_tecnico.TabIndex = 2;
            this.grb_tecnico.TabStop = false;
            this.grb_tecnico.Text = "Elaborador";
            // 
            // btn_tecnico
            // 
            this.btn_tecnico.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btn_tecnico.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_tecnico.Image = global::SWS.Properties.Resources.busca;
            this.btn_tecnico.Location = new System.Drawing.Point(293, 16);
            this.btn_tecnico.Name = "btn_tecnico";
            this.btn_tecnico.Size = new System.Drawing.Size(28, 20);
            this.btn_tecnico.TabIndex = 2;
            this.btn_tecnico.UseVisualStyleBackColor = true;
            this.btn_tecnico.Click += new System.EventHandler(this.btn_tecnico_Click);
            // 
            // text_tecnico
            // 
            this.text_tecnico.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.text_tecnico.BackColor = System.Drawing.Color.LightGray;
            this.text_tecnico.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.text_tecnico.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.text_tecnico.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.text_tecnico.ForeColor = System.Drawing.Color.Black;
            this.text_tecnico.Location = new System.Drawing.Point(6, 16);
            this.text_tecnico.MaxLength = 100;
            this.text_tecnico.Name = "text_tecnico";
            this.text_tecnico.ReadOnly = true;
            this.text_tecnico.Size = new System.Drawing.Size(291, 20);
            this.text_tecnico.TabIndex = 2;
            this.text_tecnico.TabStop = false;
            // 
            // lbl_vencimento
            // 
            this.lbl_vencimento.AutoSize = true;
            this.lbl_vencimento.Location = new System.Drawing.Point(581, 16);
            this.lbl_vencimento.Name = "lbl_vencimento";
            this.lbl_vencimento.Size = new System.Drawing.Size(0, 13);
            this.lbl_vencimento.TabIndex = 22;
            // 
            // grb_grauRisco
            // 
            this.grb_grauRisco.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.grb_grauRisco.BackColor = System.Drawing.Color.WhiteSmoke;
            this.grb_grauRisco.Controls.Add(this.cb_grauRisco);
            this.grb_grauRisco.Location = new System.Drawing.Point(647, 76);
            this.grb_grauRisco.Name = "grb_grauRisco";
            this.grb_grauRisco.Size = new System.Drawing.Size(98, 49);
            this.grb_grauRisco.TabIndex = 10;
            this.grb_grauRisco.TabStop = false;
            this.grb_grauRisco.Text = "Risco";
            // 
            // cb_grauRisco
            // 
            this.cb_grauRisco.BackColor = System.Drawing.Color.LightGray;
            this.cb_grauRisco.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cb_grauRisco.FormattingEnabled = true;
            this.cb_grauRisco.Location = new System.Drawing.Point(6, 20);
            this.cb_grauRisco.Name = "cb_grauRisco";
            this.cb_grauRisco.Size = new System.Drawing.Size(83, 21);
            this.cb_grauRisco.TabIndex = 11;
            // 
            // grb_data
            // 
            this.grb_data.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.grb_data.BackColor = System.Drawing.Color.WhiteSmoke;
            this.grb_data.Controls.Add(this.lbl_dataVencimento);
            this.grb_data.Controls.Add(this.lbl_dataCriacao);
            this.grb_data.Controls.Add(this.dt_criacao);
            this.grb_data.Controls.Add(this.dt_vencimento);
            this.grb_data.Location = new System.Drawing.Point(415, 72);
            this.grb_data.Name = "grb_data";
            this.grb_data.Size = new System.Drawing.Size(226, 53);
            this.grb_data.TabIndex = 8;
            this.grb_data.TabStop = false;
            // 
            // lbl_dataVencimento
            // 
            this.lbl_dataVencimento.AutoSize = true;
            this.lbl_dataVencimento.Location = new System.Drawing.Point(109, 9);
            this.lbl_dataVencimento.Name = "lbl_dataVencimento";
            this.lbl_dataVencimento.Size = new System.Drawing.Size(104, 13);
            this.lbl_dataVencimento.TabIndex = 24;
            this.lbl_dataVencimento.Text = "Data de Vencimento";
            // 
            // lbl_dataCriacao
            // 
            this.lbl_dataCriacao.AutoSize = true;
            this.lbl_dataCriacao.Location = new System.Drawing.Point(6, 9);
            this.lbl_dataCriacao.Name = "lbl_dataCriacao";
            this.lbl_dataCriacao.Size = new System.Drawing.Size(84, 13);
            this.lbl_dataCriacao.TabIndex = 23;
            this.lbl_dataCriacao.Text = "Data de Criação";
            // 
            // dt_criacao
            // 
            this.dt_criacao.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dt_criacao.Location = new System.Drawing.Point(6, 24);
            this.dt_criacao.Name = "dt_criacao";
            this.dt_criacao.ShowCheckBox = true;
            this.dt_criacao.Size = new System.Drawing.Size(100, 20);
            this.dt_criacao.TabIndex = 8;
            // 
            // dt_vencimento
            // 
            this.dt_vencimento.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dt_vencimento.Location = new System.Drawing.Point(112, 23);
            this.dt_vencimento.Name = "dt_vencimento";
            this.dt_vencimento.ShowCheckBox = true;
            this.dt_vencimento.Size = new System.Drawing.Size(104, 20);
            this.dt_vencimento.TabIndex = 9;
            // 
            // text_codPpra
            // 
            this.text_codPpra.BackColor = System.Drawing.Color.LightGray;
            this.text_codPpra.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.text_codPpra.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.text_codPpra.ForeColor = System.Drawing.Color.Black;
            this.text_codPpra.Location = new System.Drawing.Point(6, 30);
            this.text_codPpra.Mask = "0000,00,000000/00";
            this.text_codPpra.Name = "text_codPpra";
            this.text_codPpra.PromptChar = ' ';
            this.text_codPpra.ReadOnly = true;
            this.text_codPpra.Size = new System.Drawing.Size(122, 20);
            this.text_codPpra.TabIndex = 1;
            this.text_codPpra.TabStop = false;
            this.text_codPpra.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // tb_dados
            // 
            this.tb_dados.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tb_dados.Controls.Add(this.tb_dadosPPRA);
            this.tb_dados.Controls.Add(this.tb_ghe);
            this.tb_dados.Controls.Add(this.tb_gheFonte);
            this.tb_dados.Controls.Add(this.tb_setorGHE);
            this.tb_dados.Controls.Add(this.tb_cronograma);
            this.tb_dados.Controls.Add(this.grb_comentario);
            this.tb_dados.Controls.Add(this.tb_anexo);
            this.tb_dados.Location = new System.Drawing.Point(3, 3);
            this.tb_dados.Name = "tb_dados";
            this.tb_dados.SelectedIndex = 0;
            this.tb_dados.Size = new System.Drawing.Size(781, 455);
            this.tb_dados.TabIndex = 0;
            this.tb_dados.SelectedIndexChanged += new System.EventHandler(this.tb_dados_SelectedIndexChanged);
            // 
            // tb_dadosPPRA
            // 
            this.tb_dadosPPRA.BackColor = System.Drawing.Color.White;
            this.tb_dadosPPRA.Controls.Add(this.tbConfiguracaoExtra);
            this.tb_dadosPPRA.Controls.Add(this.grb_dados);
            this.tb_dadosPPRA.Controls.Add(this.btn_salvar);
            this.tb_dadosPPRA.Controls.Add(this.btn_limpar);
            this.tb_dadosPPRA.Location = new System.Drawing.Point(4, 22);
            this.tb_dadosPPRA.Name = "tb_dadosPPRA";
            this.tb_dadosPPRA.Padding = new System.Windows.Forms.Padding(3);
            this.tb_dadosPPRA.Size = new System.Drawing.Size(773, 429);
            this.tb_dadosPPRA.TabIndex = 0;
            this.tb_dadosPPRA.Text = "PPRA";
            // 
            // tbConfiguracaoExtra
            // 
            this.tbConfiguracaoExtra.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tbConfiguracaoExtra.Controls.Add(this.tpDados);
            this.tbConfiguracaoExtra.Controls.Add(this.tpPrevisao);
            this.tbConfiguracaoExtra.Controls.Add(this.tpCNAE);
            this.tbConfiguracaoExtra.Location = new System.Drawing.Point(3, 169);
            this.tbConfiguracaoExtra.Name = "tbConfiguracaoExtra";
            this.tbConfiguracaoExtra.SelectedIndex = 0;
            this.tbConfiguracaoExtra.Size = new System.Drawing.Size(764, 214);
            this.tbConfiguracaoExtra.TabIndex = 109;
            // 
            // tpDados
            // 
            this.tpDados.Controls.Add(this.lbl_endereco);
            this.tpDados.Controls.Add(this.text_localObra);
            this.tpDados.Controls.Add(this.lbl_UF);
            this.tpDados.Controls.Add(this.lbl_local);
            this.tpDados.Controls.Add(this.text_endereco);
            this.tpDados.Controls.Add(this.cb_uf);
            this.tpDados.Controls.Add(this.lbl_bairro);
            this.tpDados.Controls.Add(this.text_cep);
            this.tpDados.Controls.Add(this.text_bairro);
            this.tpDados.Controls.Add(this.lbl_cep);
            this.tpDados.Controls.Add(this.lbl_obra);
            this.tpDados.Controls.Add(this.text_obra);
            this.tpDados.Controls.Add(this.lbl_numero);
            this.tpDados.Controls.Add(this.text_cidade);
            this.tpDados.Controls.Add(this.text_numero);
            this.tpDados.Controls.Add(this.lbl_cidade);
            this.tpDados.Controls.Add(this.lbl_complemento);
            this.tpDados.Controls.Add(this.text_complemento);
            this.tpDados.Location = new System.Drawing.Point(4, 22);
            this.tpDados.Name = "tpDados";
            this.tpDados.Padding = new System.Windows.Forms.Padding(3);
            this.tpDados.Size = new System.Drawing.Size(756, 188);
            this.tpDados.TabIndex = 0;
            this.tpDados.Text = "Dados do Estudo";
            this.tpDados.UseVisualStyleBackColor = true;
            // 
            // lbl_endereco
            // 
            this.lbl_endereco.AutoSize = true;
            this.lbl_endereco.Location = new System.Drawing.Point(9, 7);
            this.lbl_endereco.Name = "lbl_endereco";
            this.lbl_endereco.Size = new System.Drawing.Size(61, 13);
            this.lbl_endereco.TabIndex = 17;
            this.lbl_endereco.Text = "Logradouro";
            // 
            // text_localObra
            // 
            this.text_localObra.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.text_localObra.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.text_localObra.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.text_localObra.Location = new System.Drawing.Point(375, 101);
            this.text_localObra.MaxLength = 255;
            this.text_localObra.Multiline = true;
            this.text_localObra.Name = "text_localObra";
            this.text_localObra.Size = new System.Drawing.Size(372, 67);
            this.text_localObra.TabIndex = 20;
            // 
            // lbl_UF
            // 
            this.lbl_UF.AutoSize = true;
            this.lbl_UF.Location = new System.Drawing.Point(610, 46);
            this.lbl_UF.Name = "lbl_UF";
            this.lbl_UF.Size = new System.Drawing.Size(21, 13);
            this.lbl_UF.TabIndex = 108;
            this.lbl_UF.Text = "UF";
            // 
            // lbl_local
            // 
            this.lbl_local.AutoSize = true;
            this.lbl_local.Location = new System.Drawing.Point(372, 85);
            this.lbl_local.Name = "lbl_local";
            this.lbl_local.Size = new System.Drawing.Size(59, 13);
            this.lbl_local.TabIndex = 30;
            this.lbl_local.Text = "Local Obra";
            // 
            // text_endereco
            // 
            this.text_endereco.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.text_endereco.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.text_endereco.Location = new System.Drawing.Point(9, 23);
            this.text_endereco.MaxLength = 100;
            this.text_endereco.Name = "text_endereco";
            this.text_endereco.Size = new System.Drawing.Size(352, 20);
            this.text_endereco.TabIndex = 12;
            // 
            // cb_uf
            // 
            this.cb_uf.BackColor = System.Drawing.Color.LightGray;
            this.cb_uf.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cb_uf.FormattingEnabled = true;
            this.cb_uf.Location = new System.Drawing.Point(610, 62);
            this.cb_uf.Name = "cb_uf";
            this.cb_uf.Size = new System.Drawing.Size(137, 21);
            this.cb_uf.TabIndex = 18;
            // 
            // lbl_bairro
            // 
            this.lbl_bairro.AutoSize = true;
            this.lbl_bairro.Location = new System.Drawing.Point(6, 46);
            this.lbl_bairro.Name = "lbl_bairro";
            this.lbl_bairro.Size = new System.Drawing.Size(34, 13);
            this.lbl_bairro.TabIndex = 26;
            this.lbl_bairro.Text = "Bairro";
            // 
            // text_cep
            // 
            this.text_cep.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.text_cep.Location = new System.Drawing.Point(535, 62);
            this.text_cep.Mask = "99,999-999";
            this.text_cep.Name = "text_cep";
            this.text_cep.PromptChar = ' ';
            this.text_cep.Size = new System.Drawing.Size(69, 20);
            this.text_cep.TabIndex = 17;
            this.text_cep.Leave += new System.EventHandler(this.text_cep_Leave);
            // 
            // text_bairro
            // 
            this.text_bairro.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.text_bairro.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.text_bairro.Location = new System.Drawing.Point(9, 62);
            this.text_bairro.MaxLength = 50;
            this.text_bairro.Name = "text_bairro";
            this.text_bairro.Size = new System.Drawing.Size(352, 20);
            this.text_bairro.TabIndex = 15;
            // 
            // lbl_cep
            // 
            this.lbl_cep.AutoSize = true;
            this.lbl_cep.Location = new System.Drawing.Point(532, 46);
            this.lbl_cep.Name = "lbl_cep";
            this.lbl_cep.Size = new System.Drawing.Size(28, 13);
            this.lbl_cep.TabIndex = 107;
            this.lbl_cep.Text = "CEP";
            // 
            // lbl_obra
            // 
            this.lbl_obra.AutoSize = true;
            this.lbl_obra.Location = new System.Drawing.Point(6, 85);
            this.lbl_obra.Name = "lbl_obra";
            this.lbl_obra.Size = new System.Drawing.Size(30, 13);
            this.lbl_obra.TabIndex = 15;
            this.lbl_obra.Text = "Obra";
            // 
            // text_obra
            // 
            this.text_obra.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.text_obra.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.text_obra.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.text_obra.Location = new System.Drawing.Point(9, 101);
            this.text_obra.MaxLength = 255;
            this.text_obra.Multiline = true;
            this.text_obra.Name = "text_obra";
            this.text_obra.Size = new System.Drawing.Size(352, 67);
            this.text_obra.TabIndex = 19;
            // 
            // lbl_numero
            // 
            this.lbl_numero.AutoSize = true;
            this.lbl_numero.Location = new System.Drawing.Point(372, 7);
            this.lbl_numero.Name = "lbl_numero";
            this.lbl_numero.Size = new System.Drawing.Size(44, 13);
            this.lbl_numero.TabIndex = 19;
            this.lbl_numero.Text = "Número";
            // 
            // text_cidade
            // 
            this.text_cidade.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.text_cidade.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.text_cidade.Location = new System.Drawing.Point(375, 62);
            this.text_cidade.MaxLength = 50;
            this.text_cidade.Name = "text_cidade";
            this.text_cidade.Size = new System.Drawing.Size(154, 20);
            this.text_cidade.TabIndex = 16;
            // 
            // text_numero
            // 
            this.text_numero.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.text_numero.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.text_numero.Location = new System.Drawing.Point(375, 23);
            this.text_numero.MaxLength = 10;
            this.text_numero.Name = "text_numero";
            this.text_numero.Size = new System.Drawing.Size(93, 20);
            this.text_numero.TabIndex = 13;
            // 
            // lbl_cidade
            // 
            this.lbl_cidade.AutoSize = true;
            this.lbl_cidade.Location = new System.Drawing.Point(372, 46);
            this.lbl_cidade.Name = "lbl_cidade";
            this.lbl_cidade.Size = new System.Drawing.Size(40, 13);
            this.lbl_cidade.TabIndex = 28;
            this.lbl_cidade.Text = "Cidade";
            // 
            // lbl_complemento
            // 
            this.lbl_complemento.AutoSize = true;
            this.lbl_complemento.Location = new System.Drawing.Point(475, 7);
            this.lbl_complemento.Name = "lbl_complemento";
            this.lbl_complemento.Size = new System.Drawing.Size(71, 13);
            this.lbl_complemento.TabIndex = 22;
            this.lbl_complemento.Text = "Complemento";
            // 
            // text_complemento
            // 
            this.text_complemento.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.text_complemento.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.text_complemento.Location = new System.Drawing.Point(478, 23);
            this.text_complemento.MaxLength = 100;
            this.text_complemento.Name = "text_complemento";
            this.text_complemento.Size = new System.Drawing.Size(269, 20);
            this.text_complemento.TabIndex = 14;
            // 
            // tpPrevisao
            // 
            this.tpPrevisao.Controls.Add(this.btnExcluirPrevisao);
            this.tpPrevisao.Controls.Add(this.btnIncluirPrevisao);
            this.tpPrevisao.Controls.Add(this.dgPrevisao);
            this.tpPrevisao.Location = new System.Drawing.Point(4, 22);
            this.tpPrevisao.Name = "tpPrevisao";
            this.tpPrevisao.Padding = new System.Windows.Forms.Padding(3);
            this.tpPrevisao.Size = new System.Drawing.Size(756, 188);
            this.tpPrevisao.TabIndex = 1;
            this.tpPrevisao.Text = "Previsão";
            this.tpPrevisao.UseVisualStyleBackColor = true;
            // 
            // btnExcluirPrevisao
            // 
            this.btnExcluirPrevisao.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnExcluirPrevisao.Image = global::SWS.Properties.Resources.lixeira;
            this.btnExcluirPrevisao.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnExcluirPrevisao.Location = new System.Drawing.Point(87, 159);
            this.btnExcluirPrevisao.Name = "btnExcluirPrevisao";
            this.btnExcluirPrevisao.Size = new System.Drawing.Size(75, 23);
            this.btnExcluirPrevisao.TabIndex = 2;
            this.btnExcluirPrevisao.Text = "Excluir";
            this.btnExcluirPrevisao.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnExcluirPrevisao.UseVisualStyleBackColor = true;
            this.btnExcluirPrevisao.Click += new System.EventHandler(this.btnExcluirPrevisao_Click);
            // 
            // btnIncluirPrevisao
            // 
            this.btnIncluirPrevisao.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnIncluirPrevisao.Image = global::SWS.Properties.Resources.icone_mais;
            this.btnIncluirPrevisao.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnIncluirPrevisao.Location = new System.Drawing.Point(6, 159);
            this.btnIncluirPrevisao.Name = "btnIncluirPrevisao";
            this.btnIncluirPrevisao.Size = new System.Drawing.Size(75, 23);
            this.btnIncluirPrevisao.TabIndex = 1;
            this.btnIncluirPrevisao.Text = "Incluir";
            this.btnIncluirPrevisao.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnIncluirPrevisao.UseVisualStyleBackColor = true;
            this.btnIncluirPrevisao.Click += new System.EventHandler(this.btnIncluirPrevisao_Click);
            // 
            // dgPrevisao
            // 
            this.dgPrevisao.AllowUserToAddRows = false;
            this.dgPrevisao.AllowUserToDeleteRows = false;
            this.dgPrevisao.AllowUserToOrderColumns = true;
            this.dgPrevisao.AllowUserToResizeRows = false;
            this.dgPrevisao.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.DisplayedCells;
            this.dgPrevisao.BackgroundColor = System.Drawing.Color.White;
            this.dgPrevisao.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.LightGray;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgPrevisao.DefaultCellStyle = dataGridViewCellStyle1;
            this.dgPrevisao.Location = new System.Drawing.Point(6, 6);
            this.dgPrevisao.Name = "dgPrevisao";
            this.dgPrevisao.RowHeadersWidth = 30;
            this.dgPrevisao.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.CellSelect;
            this.dgPrevisao.Size = new System.Drawing.Size(366, 147);
            this.dgPrevisao.TabIndex = 0;
            this.dgPrevisao.CellEndEdit += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgPrevisao_CellEndEdit);
            this.dgPrevisao.DataError += new System.Windows.Forms.DataGridViewDataErrorEventHandler(this.dgPrevisao_DataError);
            this.dgPrevisao.EditingControlShowing += new System.Windows.Forms.DataGridViewEditingControlShowingEventHandler(this.dgPrevisao_EditingControlShowing);
            this.dgPrevisao.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.dgPrevisao_KeyPress);
            // 
            // tpCNAE
            // 
            this.tpCNAE.Controls.Add(this.tcCnae);
            this.tpCNAE.Controls.Add(this.lblCNAECliente);
            this.tpCNAE.Location = new System.Drawing.Point(4, 22);
            this.tpCNAE.Name = "tpCNAE";
            this.tpCNAE.Padding = new System.Windows.Forms.Padding(3);
            this.tpCNAE.Size = new System.Drawing.Size(756, 188);
            this.tpCNAE.TabIndex = 2;
            this.tpCNAE.Text = "CNAE";
            this.tpCNAE.UseVisualStyleBackColor = true;
            // 
            // tcCnae
            // 
            this.tcCnae.Controls.Add(this.cnaeCliente);
            this.tcCnae.Controls.Add(this.cnaeContratante);
            this.tcCnae.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tcCnae.Location = new System.Drawing.Point(3, 3);
            this.tcCnae.Name = "tcCnae";
            this.tcCnae.SelectedIndex = 0;
            this.tcCnae.Size = new System.Drawing.Size(750, 182);
            this.tcCnae.TabIndex = 1;
            // 
            // cnaeCliente
            // 
            this.cnaeCliente.Controls.Add(this.dgvCnaeCliente);
            this.cnaeCliente.Location = new System.Drawing.Point(4, 22);
            this.cnaeCliente.Name = "cnaeCliente";
            this.cnaeCliente.Padding = new System.Windows.Forms.Padding(3);
            this.cnaeCliente.Size = new System.Drawing.Size(742, 156);
            this.cnaeCliente.TabIndex = 0;
            this.cnaeCliente.Text = "Cliente";
            this.cnaeCliente.UseVisualStyleBackColor = true;
            // 
            // dgvCnaeCliente
            // 
            this.dgvCnaeCliente.AllowUserToAddRows = false;
            this.dgvCnaeCliente.AllowUserToDeleteRows = false;
            this.dgvCnaeCliente.AllowUserToOrderColumns = true;
            this.dgvCnaeCliente.AllowUserToResizeRows = false;
            dataGridViewCellStyle2.BackColor = System.Drawing.Color.Gainsboro;
            this.dgvCnaeCliente.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle2;
            this.dgvCnaeCliente.BackgroundColor = System.Drawing.Color.White;
            this.dgvCnaeCliente.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle3.BackColor = System.Drawing.Color.LightGray;
            dataGridViewCellStyle3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvCnaeCliente.DefaultCellStyle = dataGridViewCellStyle3;
            this.dgvCnaeCliente.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvCnaeCliente.Location = new System.Drawing.Point(3, 3);
            this.dgvCnaeCliente.Name = "dgvCnaeCliente";
            this.dgvCnaeCliente.ReadOnly = true;
            this.dgvCnaeCliente.RowHeadersWidth = 35;
            this.dgvCnaeCliente.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvCnaeCliente.Size = new System.Drawing.Size(736, 150);
            this.dgvCnaeCliente.TabIndex = 0;
            // 
            // cnaeContratante
            // 
            this.cnaeContratante.Controls.Add(this.dgvCnaeContratante);
            this.cnaeContratante.Location = new System.Drawing.Point(4, 22);
            this.cnaeContratante.Name = "cnaeContratante";
            this.cnaeContratante.Padding = new System.Windows.Forms.Padding(3);
            this.cnaeContratante.Size = new System.Drawing.Size(742, 156);
            this.cnaeContratante.TabIndex = 1;
            this.cnaeContratante.Text = "Contratante";
            this.cnaeContratante.UseVisualStyleBackColor = true;
            // 
            // dgvCnaeContratante
            // 
            this.dgvCnaeContratante.AllowUserToAddRows = false;
            this.dgvCnaeContratante.AllowUserToDeleteRows = false;
            this.dgvCnaeContratante.AllowUserToOrderColumns = true;
            this.dgvCnaeContratante.AllowUserToResizeRows = false;
            dataGridViewCellStyle4.BackColor = System.Drawing.Color.Gainsboro;
            this.dgvCnaeContratante.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle4;
            this.dgvCnaeContratante.BackgroundColor = System.Drawing.Color.White;
            this.dgvCnaeContratante.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle5.BackColor = System.Drawing.Color.LightGray;
            dataGridViewCellStyle5.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle5.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle5.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle5.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle5.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvCnaeContratante.DefaultCellStyle = dataGridViewCellStyle5;
            this.dgvCnaeContratante.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvCnaeContratante.Location = new System.Drawing.Point(3, 3);
            this.dgvCnaeContratante.Name = "dgvCnaeContratante";
            this.dgvCnaeContratante.ReadOnly = true;
            this.dgvCnaeContratante.RowHeadersWidth = 35;
            this.dgvCnaeContratante.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvCnaeContratante.Size = new System.Drawing.Size(736, 150);
            this.dgvCnaeContratante.TabIndex = 0;
            // 
            // lblCNAECliente
            // 
            this.lblCNAECliente.AutoSize = true;
            this.lblCNAECliente.Location = new System.Drawing.Point(4, 7);
            this.lblCNAECliente.Name = "lblCNAECliente";
            this.lblCNAECliente.Size = new System.Drawing.Size(0, 13);
            this.lblCNAECliente.TabIndex = 0;
            // 
            // btn_salvar
            // 
            this.btn_salvar.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btn_salvar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_salvar.Image = global::SWS.Properties.Resources.icone_mais;
            this.btn_salvar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btn_salvar.Location = new System.Drawing.Point(9, 389);
            this.btn_salvar.Name = "btn_salvar";
            this.btn_salvar.Size = new System.Drawing.Size(77, 23);
            this.btn_salvar.TabIndex = 21;
            this.btn_salvar.Text = "&Novo";
            this.btn_salvar.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btn_salvar.UseVisualStyleBackColor = true;
            this.btn_salvar.Click += new System.EventHandler(this.btn_salvar_Click);
            // 
            // btn_limpar
            // 
            this.btn_limpar.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btn_limpar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_limpar.Image = global::SWS.Properties.Resources.vassoura;
            this.btn_limpar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btn_limpar.Location = new System.Drawing.Point(92, 389);
            this.btn_limpar.Name = "btn_limpar";
            this.btn_limpar.Size = new System.Drawing.Size(77, 23);
            this.btn_limpar.TabIndex = 22;
            this.btn_limpar.Text = "&Limpar";
            this.btn_limpar.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btn_limpar.UseVisualStyleBackColor = true;
            this.btn_limpar.Click += new System.EventHandler(this.btn_limpar_Click);
            // 
            // tb_ghe
            // 
            this.tb_ghe.Controls.Add(this.btn_alterarGhe);
            this.tb_ghe.Controls.Add(this.grb_ghePPRA);
            this.tb_ghe.Controls.Add(this.btn_incluirGHE);
            this.tb_ghe.Controls.Add(this.btn_excluirGHE);
            this.tb_ghe.Location = new System.Drawing.Point(4, 22);
            this.tb_ghe.Name = "tb_ghe";
            this.tb_ghe.Size = new System.Drawing.Size(773, 429);
            this.tb_ghe.TabIndex = 2;
            this.tb_ghe.Text = "GHE";
            this.tb_ghe.UseVisualStyleBackColor = true;
            // 
            // btn_alterarGhe
            // 
            this.btn_alterarGhe.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btn_alterarGhe.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_alterarGhe.Image = global::SWS.Properties.Resources.Alterar;
            this.btn_alterarGhe.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btn_alterarGhe.Location = new System.Drawing.Point(88, 388);
            this.btn_alterarGhe.Name = "btn_alterarGhe";
            this.btn_alterarGhe.Size = new System.Drawing.Size(77, 23);
            this.btn_alterarGhe.TabIndex = 2;
            this.btn_alterarGhe.Text = "&Alterar";
            this.btn_alterarGhe.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btn_alterarGhe.UseVisualStyleBackColor = true;
            this.btn_alterarGhe.Click += new System.EventHandler(this.btn_alterarGhe_Click);
            // 
            // grb_ghePPRA
            // 
            this.grb_ghePPRA.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.grb_ghePPRA.Controls.Add(this.grd_ghe);
            this.grb_ghePPRA.Location = new System.Drawing.Point(5, 3);
            this.grb_ghePPRA.Name = "grb_ghePPRA";
            this.grb_ghePPRA.Size = new System.Drawing.Size(765, 379);
            this.grb_ghePPRA.TabIndex = 6;
            this.grb_ghePPRA.TabStop = false;
            this.grb_ghePPRA.Text = "GHE";
            // 
            // grd_ghe
            // 
            this.grd_ghe.AllowUserToAddRows = false;
            this.grd_ghe.AllowUserToDeleteRows = false;
            this.grd_ghe.BackgroundColor = System.Drawing.Color.White;
            this.grd_ghe.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle6.BackColor = System.Drawing.Color.LightGray;
            dataGridViewCellStyle6.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle6.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle6.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle6.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle6.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.grd_ghe.DefaultCellStyle = dataGridViewCellStyle6;
            this.grd_ghe.Dock = System.Windows.Forms.DockStyle.Fill;
            this.grd_ghe.Location = new System.Drawing.Point(3, 16);
            this.grd_ghe.MultiSelect = false;
            this.grd_ghe.Name = "grd_ghe";
            this.grd_ghe.ReadOnly = true;
            this.grd_ghe.Size = new System.Drawing.Size(759, 360);
            this.grd_ghe.TabIndex = 3;
            this.grd_ghe.DataBindingComplete += new System.Windows.Forms.DataGridViewBindingCompleteEventHandler(this.grd_ghe_DataBindingComplete);
            // 
            // btn_incluirGHE
            // 
            this.btn_incluirGHE.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btn_incluirGHE.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_incluirGHE.Image = global::SWS.Properties.Resources.icone_mais;
            this.btn_incluirGHE.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btn_incluirGHE.Location = new System.Drawing.Point(5, 388);
            this.btn_incluirGHE.Name = "btn_incluirGHE";
            this.btn_incluirGHE.Size = new System.Drawing.Size(77, 23);
            this.btn_incluirGHE.TabIndex = 1;
            this.btn_incluirGHE.Text = "&Incluir";
            this.btn_incluirGHE.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btn_incluirGHE.UseVisualStyleBackColor = true;
            this.btn_incluirGHE.Click += new System.EventHandler(this.btn_incluirGHE_Click);
            // 
            // btn_excluirGHE
            // 
            this.btn_excluirGHE.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btn_excluirGHE.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_excluirGHE.Image = global::SWS.Properties.Resources.lixeira;
            this.btn_excluirGHE.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btn_excluirGHE.Location = new System.Drawing.Point(171, 388);
            this.btn_excluirGHE.Name = "btn_excluirGHE";
            this.btn_excluirGHE.Size = new System.Drawing.Size(77, 23);
            this.btn_excluirGHE.TabIndex = 3;
            this.btn_excluirGHE.Text = "&Excluir";
            this.btn_excluirGHE.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btn_excluirGHE.UseVisualStyleBackColor = true;
            this.btn_excluirGHE.Click += new System.EventHandler(this.btn_excluirGHE_Click);
            // 
            // tb_gheFonte
            // 
            this.tb_gheFonte.Controls.Add(this.tb_fonteGHE);
            this.tb_gheFonte.Location = new System.Drawing.Point(4, 22);
            this.tb_gheFonte.Name = "tb_gheFonte";
            this.tb_gheFonte.Size = new System.Drawing.Size(773, 429);
            this.tb_gheFonte.TabIndex = 3;
            this.tb_gheFonte.Text = "Fontes";
            this.tb_gheFonte.UseVisualStyleBackColor = true;
            // 
            // tb_fonteGHE
            // 
            this.tb_fonteGHE.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tb_fonteGHE.Controls.Add(this.Fonte);
            this.tb_fonteGHE.Controls.Add(this.agente);
            this.tb_fonteGHE.Controls.Add(this.epi);
            this.tb_fonteGHE.Location = new System.Drawing.Point(3, 3);
            this.tb_fonteGHE.Name = "tb_fonteGHE";
            this.tb_fonteGHE.SelectedIndex = 0;
            this.tb_fonteGHE.Size = new System.Drawing.Size(757, 421);
            this.tb_fonteGHE.TabIndex = 8;
            this.tb_fonteGHE.SelectedIndexChanged += new System.EventHandler(this.tb_fonteGHE_SelectedIndexChanged);
            // 
            // Fonte
            // 
            this.Fonte.Controls.Add(this.grb_gheFonte);
            this.Fonte.Controls.Add(this.btn_excluirFonte);
            this.Fonte.Controls.Add(this.grb_fonte);
            this.Fonte.Controls.Add(this.btn_incluirFonte);
            this.Fonte.Location = new System.Drawing.Point(4, 22);
            this.Fonte.Name = "Fonte";
            this.Fonte.Padding = new System.Windows.Forms.Padding(3);
            this.Fonte.Size = new System.Drawing.Size(749, 395);
            this.Fonte.TabIndex = 0;
            this.Fonte.Text = "Fonte";
            this.Fonte.UseVisualStyleBackColor = true;
            // 
            // grb_gheFonte
            // 
            this.grb_gheFonte.Controls.Add(this.cb_ghe);
            this.grb_gheFonte.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.grb_gheFonte.Location = new System.Drawing.Point(6, 18);
            this.grb_gheFonte.Name = "grb_gheFonte";
            this.grb_gheFonte.Size = new System.Drawing.Size(378, 54);
            this.grb_gheFonte.TabIndex = 9;
            this.grb_gheFonte.TabStop = false;
            this.grb_gheFonte.Text = "GHE";
            // 
            // cb_ghe
            // 
            this.cb_ghe.BackColor = System.Drawing.Color.LightGray;
            this.cb_ghe.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cb_ghe.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cb_ghe.FormattingEnabled = true;
            this.cb_ghe.Location = new System.Drawing.Point(6, 19);
            this.cb_ghe.Name = "cb_ghe";
            this.cb_ghe.Size = new System.Drawing.Size(366, 21);
            this.cb_ghe.TabIndex = 1;
            this.cb_ghe.SelectedIndexChanged += new System.EventHandler(this.cb_ghe_SelectedIndexChanged);
            // 
            // btn_excluirFonte
            // 
            this.btn_excluirFonte.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btn_excluirFonte.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_excluirFonte.Image = global::SWS.Properties.Resources.lixeira;
            this.btn_excluirFonte.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btn_excluirFonte.Location = new System.Drawing.Point(473, 353);
            this.btn_excluirFonte.Name = "btn_excluirFonte";
            this.btn_excluirFonte.Size = new System.Drawing.Size(77, 23);
            this.btn_excluirFonte.TabIndex = 3;
            this.btn_excluirFonte.Text = "&Excluir";
            this.btn_excluirFonte.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btn_excluirFonte.UseVisualStyleBackColor = true;
            this.btn_excluirFonte.Click += new System.EventHandler(this.btn_excluirFonte_Click);
            // 
            // grb_fonte
            // 
            this.grb_fonte.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.grb_fonte.Controls.Add(this.grd_fonte);
            this.grb_fonte.Location = new System.Drawing.Point(390, 18);
            this.grb_fonte.Name = "grb_fonte";
            this.grb_fonte.Size = new System.Drawing.Size(353, 329);
            this.grb_fonte.TabIndex = 8;
            this.grb_fonte.TabStop = false;
            this.grb_fonte.Text = "Fonte";
            // 
            // grd_fonte
            // 
            this.grd_fonte.AllowUserToAddRows = false;
            this.grd_fonte.AllowUserToDeleteRows = false;
            this.grd_fonte.AllowUserToOrderColumns = true;
            this.grd_fonte.AllowUserToResizeRows = false;
            this.grd_fonte.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this.grd_fonte.BackgroundColor = System.Drawing.Color.White;
            this.grd_fonte.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dataGridViewCellStyle7.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle7.BackColor = System.Drawing.Color.LightGray;
            dataGridViewCellStyle7.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle7.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle7.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle7.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle7.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.grd_fonte.DefaultCellStyle = dataGridViewCellStyle7;
            this.grd_fonte.Dock = System.Windows.Forms.DockStyle.Fill;
            this.grd_fonte.Location = new System.Drawing.Point(3, 16);
            this.grd_fonte.Name = "grd_fonte";
            this.grd_fonte.ReadOnly = true;
            this.grd_fonte.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.grd_fonte.Size = new System.Drawing.Size(347, 310);
            this.grd_fonte.TabIndex = 3;
            this.grd_fonte.DataBindingComplete += new System.Windows.Forms.DataGridViewBindingCompleteEventHandler(this.grd_fonte_DataBindingComplete);
            // 
            // btn_incluirFonte
            // 
            this.btn_incluirFonte.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btn_incluirFonte.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_incluirFonte.Image = global::SWS.Properties.Resources.icone_mais;
            this.btn_incluirFonte.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btn_incluirFonte.Location = new System.Drawing.Point(390, 353);
            this.btn_incluirFonte.Name = "btn_incluirFonte";
            this.btn_incluirFonte.Size = new System.Drawing.Size(77, 23);
            this.btn_incluirFonte.TabIndex = 2;
            this.btn_incluirFonte.Text = "&Incluir";
            this.btn_incluirFonte.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btn_incluirFonte.UseVisualStyleBackColor = true;
            this.btn_incluirFonte.Click += new System.EventHandler(this.btn_incluirFonte_Click);
            // 
            // agente
            // 
            this.agente.Controls.Add(this.btn_excluirAgente);
            this.agente.Controls.Add(this.grb_gheAgente);
            this.agente.Controls.Add(this.btn_incluirAgente);
            this.agente.Controls.Add(this.grb_fonteAgente);
            this.agente.Controls.Add(this.grb_agente);
            this.agente.Location = new System.Drawing.Point(4, 22);
            this.agente.Name = "agente";
            this.agente.Padding = new System.Windows.Forms.Padding(3);
            this.agente.Size = new System.Drawing.Size(749, 395);
            this.agente.TabIndex = 1;
            this.agente.Text = "Agente";
            this.agente.UseVisualStyleBackColor = true;
            // 
            // btn_excluirAgente
            // 
            this.btn_excluirAgente.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btn_excluirAgente.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_excluirAgente.Image = global::SWS.Properties.Resources.lixeira;
            this.btn_excluirAgente.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btn_excluirAgente.Location = new System.Drawing.Point(473, 357);
            this.btn_excluirAgente.Name = "btn_excluirAgente";
            this.btn_excluirAgente.Size = new System.Drawing.Size(77, 23);
            this.btn_excluirAgente.TabIndex = 4;
            this.btn_excluirAgente.Text = "&Excluir";
            this.btn_excluirAgente.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btn_excluirAgente.UseVisualStyleBackColor = true;
            this.btn_excluirAgente.Click += new System.EventHandler(this.btn_excluirAgente_Click);
            // 
            // grb_gheAgente
            // 
            this.grb_gheAgente.Controls.Add(this.cb_gheAgente);
            this.grb_gheAgente.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.grb_gheAgente.Location = new System.Drawing.Point(6, 18);
            this.grb_gheAgente.Name = "grb_gheAgente";
            this.grb_gheAgente.Size = new System.Drawing.Size(378, 51);
            this.grb_gheAgente.TabIndex = 6;
            this.grb_gheAgente.TabStop = false;
            this.grb_gheAgente.Text = "GHE";
            // 
            // cb_gheAgente
            // 
            this.cb_gheAgente.BackColor = System.Drawing.Color.LightGray;
            this.cb_gheAgente.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cb_gheAgente.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cb_gheAgente.FormattingEnabled = true;
            this.cb_gheAgente.Location = new System.Drawing.Point(6, 19);
            this.cb_gheAgente.Name = "cb_gheAgente";
            this.cb_gheAgente.Size = new System.Drawing.Size(366, 21);
            this.cb_gheAgente.TabIndex = 1;
            this.cb_gheAgente.SelectedIndexChanged += new System.EventHandler(this.cb_gheAgente_SelectedIndexChanged_1);
            // 
            // btn_incluirAgente
            // 
            this.btn_incluirAgente.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btn_incluirAgente.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_incluirAgente.Image = global::SWS.Properties.Resources.icone_mais;
            this.btn_incluirAgente.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btn_incluirAgente.Location = new System.Drawing.Point(390, 357);
            this.btn_incluirAgente.Name = "btn_incluirAgente";
            this.btn_incluirAgente.Size = new System.Drawing.Size(77, 23);
            this.btn_incluirAgente.TabIndex = 3;
            this.btn_incluirAgente.Text = "&Incluir";
            this.btn_incluirAgente.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btn_incluirAgente.UseVisualStyleBackColor = true;
            this.btn_incluirAgente.Click += new System.EventHandler(this.btn_incluirAgente_Click);
            // 
            // grb_fonteAgente
            // 
            this.grb_fonteAgente.Controls.Add(this.cb_fonteAgente);
            this.grb_fonteAgente.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.grb_fonteAgente.Location = new System.Drawing.Point(6, 78);
            this.grb_fonteAgente.Name = "grb_fonteAgente";
            this.grb_fonteAgente.Size = new System.Drawing.Size(378, 54);
            this.grb_fonteAgente.TabIndex = 4;
            this.grb_fonteAgente.TabStop = false;
            this.grb_fonteAgente.Text = "Fonte";
            // 
            // cb_fonteAgente
            // 
            this.cb_fonteAgente.BackColor = System.Drawing.Color.LightGray;
            this.cb_fonteAgente.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cb_fonteAgente.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cb_fonteAgente.FormattingEnabled = true;
            this.cb_fonteAgente.Location = new System.Drawing.Point(6, 19);
            this.cb_fonteAgente.Name = "cb_fonteAgente";
            this.cb_fonteAgente.Size = new System.Drawing.Size(366, 21);
            this.cb_fonteAgente.TabIndex = 2;
            this.cb_fonteAgente.SelectedIndexChanged += new System.EventHandler(this.cb_fonteAgente_SelectedIndexChanged);
            // 
            // grb_agente
            // 
            this.grb_agente.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.grb_agente.Controls.Add(this.grd_agente);
            this.grb_agente.Location = new System.Drawing.Point(390, 18);
            this.grb_agente.Name = "grb_agente";
            this.grb_agente.Size = new System.Drawing.Size(353, 333);
            this.grb_agente.TabIndex = 5;
            this.grb_agente.TabStop = false;
            this.grb_agente.Text = "Agente";
            // 
            // grd_agente
            // 
            this.grd_agente.AllowUserToAddRows = false;
            this.grd_agente.AllowUserToDeleteRows = false;
            this.grd_agente.AllowUserToOrderColumns = true;
            this.grd_agente.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.DisplayedCells;
            this.grd_agente.BackgroundColor = System.Drawing.Color.White;
            this.grd_agente.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dataGridViewCellStyle8.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle8.BackColor = System.Drawing.Color.LightGray;
            dataGridViewCellStyle8.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle8.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle8.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle8.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle8.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.grd_agente.DefaultCellStyle = dataGridViewCellStyle8;
            this.grd_agente.Dock = System.Windows.Forms.DockStyle.Fill;
            this.grd_agente.Location = new System.Drawing.Point(3, 16);
            this.grd_agente.Name = "grd_agente";
            this.grd_agente.ReadOnly = true;
            this.grd_agente.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.grd_agente.Size = new System.Drawing.Size(347, 314);
            this.grd_agente.TabIndex = 0;
            this.grd_agente.DataBindingComplete += new System.Windows.Forms.DataGridViewBindingCompleteEventHandler(this.grd_agente_DataBindingComplete);
            // 
            // epi
            // 
            this.epi.Controls.Add(this.btn_excluirEPI);
            this.epi.Controls.Add(this.grb_epi);
            this.epi.Controls.Add(this.btn_incluirEpi);
            this.epi.Controls.Add(this.grb_agenteEpi);
            this.epi.Controls.Add(this.grb_fonteEpi);
            this.epi.Controls.Add(this.grb_gheEPI);
            this.epi.Location = new System.Drawing.Point(4, 22);
            this.epi.Name = "epi";
            this.epi.Padding = new System.Windows.Forms.Padding(3);
            this.epi.Size = new System.Drawing.Size(749, 395);
            this.epi.TabIndex = 2;
            this.epi.Text = "EPI";
            this.epi.UseVisualStyleBackColor = true;
            // 
            // btn_excluirEPI
            // 
            this.btn_excluirEPI.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btn_excluirEPI.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_excluirEPI.Image = global::SWS.Properties.Resources.lixeira;
            this.btn_excluirEPI.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btn_excluirEPI.Location = new System.Drawing.Point(473, 354);
            this.btn_excluirEPI.Name = "btn_excluirEPI";
            this.btn_excluirEPI.Size = new System.Drawing.Size(77, 23);
            this.btn_excluirEPI.TabIndex = 5;
            this.btn_excluirEPI.Text = "&Excluir";
            this.btn_excluirEPI.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btn_excluirEPI.UseVisualStyleBackColor = true;
            this.btn_excluirEPI.Click += new System.EventHandler(this.btn_excluirEPI_Click);
            // 
            // grb_epi
            // 
            this.grb_epi.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.grb_epi.Controls.Add(this.grd_epi);
            this.grb_epi.Location = new System.Drawing.Point(390, 18);
            this.grb_epi.Name = "grb_epi";
            this.grb_epi.Size = new System.Drawing.Size(353, 330);
            this.grb_epi.TabIndex = 7;
            this.grb_epi.TabStop = false;
            this.grb_epi.Text = "EPI";
            // 
            // grd_epi
            // 
            this.grd_epi.AllowUserToAddRows = false;
            this.grd_epi.AllowUserToDeleteRows = false;
            this.grd_epi.AllowUserToOrderColumns = true;
            this.grd_epi.AllowUserToResizeRows = false;
            this.grd_epi.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this.grd_epi.BackgroundColor = System.Drawing.Color.White;
            this.grd_epi.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dataGridViewCellStyle9.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle9.BackColor = System.Drawing.Color.LightGray;
            dataGridViewCellStyle9.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle9.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle9.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle9.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle9.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.grd_epi.DefaultCellStyle = dataGridViewCellStyle9;
            this.grd_epi.Dock = System.Windows.Forms.DockStyle.Fill;
            this.grd_epi.Location = new System.Drawing.Point(3, 16);
            this.grd_epi.Name = "grd_epi";
            this.grd_epi.ReadOnly = true;
            this.grd_epi.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.grd_epi.Size = new System.Drawing.Size(347, 311);
            this.grd_epi.TabIndex = 0;
            // 
            // btn_incluirEpi
            // 
            this.btn_incluirEpi.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btn_incluirEpi.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_incluirEpi.Image = global::SWS.Properties.Resources.icone_mais;
            this.btn_incluirEpi.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btn_incluirEpi.Location = new System.Drawing.Point(390, 354);
            this.btn_incluirEpi.Name = "btn_incluirEpi";
            this.btn_incluirEpi.Size = new System.Drawing.Size(77, 23);
            this.btn_incluirEpi.TabIndex = 4;
            this.btn_incluirEpi.Text = "&Incluir";
            this.btn_incluirEpi.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btn_incluirEpi.UseVisualStyleBackColor = true;
            this.btn_incluirEpi.Click += new System.EventHandler(this.btn_incluirEpi_Click);
            // 
            // grb_agenteEpi
            // 
            this.grb_agenteEpi.Controls.Add(this.cb_agenteEpi);
            this.grb_agenteEpi.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.grb_agenteEpi.Location = new System.Drawing.Point(6, 138);
            this.grb_agenteEpi.Name = "grb_agenteEpi";
            this.grb_agenteEpi.Size = new System.Drawing.Size(378, 54);
            this.grb_agenteEpi.TabIndex = 6;
            this.grb_agenteEpi.TabStop = false;
            this.grb_agenteEpi.Text = "Agente";
            // 
            // cb_agenteEpi
            // 
            this.cb_agenteEpi.BackColor = System.Drawing.Color.LightGray;
            this.cb_agenteEpi.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cb_agenteEpi.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cb_agenteEpi.FormattingEnabled = true;
            this.cb_agenteEpi.Location = new System.Drawing.Point(6, 19);
            this.cb_agenteEpi.Name = "cb_agenteEpi";
            this.cb_agenteEpi.Size = new System.Drawing.Size(366, 21);
            this.cb_agenteEpi.TabIndex = 3;
            this.cb_agenteEpi.SelectedIndexChanged += new System.EventHandler(this.cb_agenteEpi_SelectedIndexChanged);
            // 
            // grb_fonteEpi
            // 
            this.grb_fonteEpi.Controls.Add(this.cb_fonteEpi);
            this.grb_fonteEpi.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.grb_fonteEpi.Location = new System.Drawing.Point(6, 78);
            this.grb_fonteEpi.Name = "grb_fonteEpi";
            this.grb_fonteEpi.Size = new System.Drawing.Size(378, 54);
            this.grb_fonteEpi.TabIndex = 5;
            this.grb_fonteEpi.TabStop = false;
            this.grb_fonteEpi.Text = "Fonte";
            // 
            // cb_fonteEpi
            // 
            this.cb_fonteEpi.BackColor = System.Drawing.Color.LightGray;
            this.cb_fonteEpi.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cb_fonteEpi.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cb_fonteEpi.FormattingEnabled = true;
            this.cb_fonteEpi.Location = new System.Drawing.Point(6, 19);
            this.cb_fonteEpi.Name = "cb_fonteEpi";
            this.cb_fonteEpi.Size = new System.Drawing.Size(366, 21);
            this.cb_fonteEpi.TabIndex = 2;
            this.cb_fonteEpi.SelectedIndexChanged += new System.EventHandler(this.cb_fonteEpi_SelectedIndexChanged);
            // 
            // grb_gheEPI
            // 
            this.grb_gheEPI.Controls.Add(this.cb_gheEPI);
            this.grb_gheEPI.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.grb_gheEPI.Location = new System.Drawing.Point(6, 18);
            this.grb_gheEPI.Name = "grb_gheEPI";
            this.grb_gheEPI.Size = new System.Drawing.Size(378, 54);
            this.grb_gheEPI.TabIndex = 4;
            this.grb_gheEPI.TabStop = false;
            this.grb_gheEPI.Text = "GHE";
            // 
            // cb_gheEPI
            // 
            this.cb_gheEPI.BackColor = System.Drawing.Color.LightGray;
            this.cb_gheEPI.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cb_gheEPI.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cb_gheEPI.FormattingEnabled = true;
            this.cb_gheEPI.Location = new System.Drawing.Point(6, 19);
            this.cb_gheEPI.Name = "cb_gheEPI";
            this.cb_gheEPI.Size = new System.Drawing.Size(366, 21);
            this.cb_gheEPI.TabIndex = 1;
            this.cb_gheEPI.SelectedIndexChanged += new System.EventHandler(this.cb_gheEPI_SelectedIndexChanged);
            // 
            // tb_setorGHE
            // 
            this.tb_setorGHE.Controls.Add(this.tb_gheSetor);
            this.tb_setorGHE.Location = new System.Drawing.Point(4, 22);
            this.tb_setorGHE.Name = "tb_setorGHE";
            this.tb_setorGHE.Size = new System.Drawing.Size(773, 429);
            this.tb_setorGHE.TabIndex = 4;
            this.tb_setorGHE.Text = "Setores";
            this.tb_setorGHE.UseVisualStyleBackColor = true;
            // 
            // tb_gheSetor
            // 
            this.tb_gheSetor.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tb_gheSetor.Controls.Add(this.tb_setor);
            this.tb_gheSetor.Controls.Add(this.tb_funcao);
            this.tb_gheSetor.Controls.Add(this.tb_funcaoPPRA);
            this.tb_gheSetor.Location = new System.Drawing.Point(5, 3);
            this.tb_gheSetor.Name = "tb_gheSetor";
            this.tb_gheSetor.SelectedIndex = 0;
            this.tb_gheSetor.Size = new System.Drawing.Size(749, 421);
            this.tb_gheSetor.TabIndex = 0;
            this.tb_gheSetor.SelectedIndexChanged += new System.EventHandler(this.tb_gheSetor_SelectedIndexChanged);
            // 
            // tb_setor
            // 
            this.tb_setor.Controls.Add(this.grb_setor);
            this.tb_setor.Controls.Add(this.btn_excluirSetor);
            this.tb_setor.Controls.Add(this.grb_gheSetor);
            this.tb_setor.Controls.Add(this.btn_incluirSetor);
            this.tb_setor.Location = new System.Drawing.Point(4, 22);
            this.tb_setor.Name = "tb_setor";
            this.tb_setor.Padding = new System.Windows.Forms.Padding(3);
            this.tb_setor.Size = new System.Drawing.Size(741, 395);
            this.tb_setor.TabIndex = 0;
            this.tb_setor.Text = "Setor";
            this.tb_setor.UseVisualStyleBackColor = true;
            // 
            // grb_setor
            // 
            this.grb_setor.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.grb_setor.Controls.Add(this.grd_setor);
            this.grb_setor.Location = new System.Drawing.Point(390, 18);
            this.grb_setor.Name = "grb_setor";
            this.grb_setor.Size = new System.Drawing.Size(348, 330);
            this.grb_setor.TabIndex = 9;
            this.grb_setor.TabStop = false;
            this.grb_setor.Text = "Setor";
            // 
            // grd_setor
            // 
            this.grd_setor.AllowUserToAddRows = false;
            this.grd_setor.AllowUserToDeleteRows = false;
            this.grd_setor.AllowUserToOrderColumns = true;
            this.grd_setor.AllowUserToResizeRows = false;
            this.grd_setor.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this.grd_setor.BackgroundColor = System.Drawing.Color.White;
            this.grd_setor.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dataGridViewCellStyle10.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle10.BackColor = System.Drawing.Color.LightGray;
            dataGridViewCellStyle10.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle10.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle10.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle10.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle10.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.grd_setor.DefaultCellStyle = dataGridViewCellStyle10;
            this.grd_setor.Dock = System.Windows.Forms.DockStyle.Fill;
            this.grd_setor.Location = new System.Drawing.Point(3, 16);
            this.grd_setor.Name = "grd_setor";
            this.grd_setor.ReadOnly = true;
            this.grd_setor.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.grd_setor.Size = new System.Drawing.Size(342, 311);
            this.grd_setor.TabIndex = 3;
            // 
            // btn_excluirSetor
            // 
            this.btn_excluirSetor.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btn_excluirSetor.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_excluirSetor.Image = global::SWS.Properties.Resources.lixeira;
            this.btn_excluirSetor.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btn_excluirSetor.Location = new System.Drawing.Point(473, 354);
            this.btn_excluirSetor.Name = "btn_excluirSetor";
            this.btn_excluirSetor.Size = new System.Drawing.Size(77, 23);
            this.btn_excluirSetor.TabIndex = 3;
            this.btn_excluirSetor.Text = "&Excluir";
            this.btn_excluirSetor.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btn_excluirSetor.UseVisualStyleBackColor = true;
            this.btn_excluirSetor.Click += new System.EventHandler(this.btn_excluirSetor_Click);
            // 
            // grb_gheSetor
            // 
            this.grb_gheSetor.Controls.Add(this.cb_gheSetor);
            this.grb_gheSetor.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.grb_gheSetor.Location = new System.Drawing.Point(6, 18);
            this.grb_gheSetor.Name = "grb_gheSetor";
            this.grb_gheSetor.Size = new System.Drawing.Size(378, 54);
            this.grb_gheSetor.TabIndex = 5;
            this.grb_gheSetor.TabStop = false;
            this.grb_gheSetor.Text = "GHE";
            // 
            // cb_gheSetor
            // 
            this.cb_gheSetor.BackColor = System.Drawing.Color.LightGray;
            this.cb_gheSetor.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cb_gheSetor.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cb_gheSetor.FormattingEnabled = true;
            this.cb_gheSetor.Location = new System.Drawing.Point(6, 19);
            this.cb_gheSetor.Name = "cb_gheSetor";
            this.cb_gheSetor.Size = new System.Drawing.Size(366, 21);
            this.cb_gheSetor.TabIndex = 1;
            this.cb_gheSetor.SelectedIndexChanged += new System.EventHandler(this.cb_gheSetor_SelectedIndexChanged);
            // 
            // btn_incluirSetor
            // 
            this.btn_incluirSetor.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btn_incluirSetor.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_incluirSetor.Image = global::SWS.Properties.Resources.icone_mais;
            this.btn_incluirSetor.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btn_incluirSetor.Location = new System.Drawing.Point(390, 354);
            this.btn_incluirSetor.Name = "btn_incluirSetor";
            this.btn_incluirSetor.Size = new System.Drawing.Size(77, 23);
            this.btn_incluirSetor.TabIndex = 2;
            this.btn_incluirSetor.Text = "&Incluir";
            this.btn_incluirSetor.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btn_incluirSetor.UseVisualStyleBackColor = true;
            this.btn_incluirSetor.Click += new System.EventHandler(this.btn_incluirSetor_Click);
            // 
            // tb_funcao
            // 
            this.tb_funcao.Controls.Add(this.btn_excluirFuncaoClientePPRA);
            this.tb_funcao.Controls.Add(this.btn_incluirFuncaoClientePPRA);
            this.tb_funcao.Controls.Add(this.grb_funcao);
            this.tb_funcao.Location = new System.Drawing.Point(4, 22);
            this.tb_funcao.Name = "tb_funcao";
            this.tb_funcao.Padding = new System.Windows.Forms.Padding(3);
            this.tb_funcao.Size = new System.Drawing.Size(741, 395);
            this.tb_funcao.TabIndex = 1;
            this.tb_funcao.Text = "Função do Cliente";
            this.tb_funcao.UseVisualStyleBackColor = true;
            // 
            // btn_excluirFuncaoClientePPRA
            // 
            this.btn_excluirFuncaoClientePPRA.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btn_excluirFuncaoClientePPRA.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_excluirFuncaoClientePPRA.Image = global::SWS.Properties.Resources.lixeira;
            this.btn_excluirFuncaoClientePPRA.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btn_excluirFuncaoClientePPRA.Location = new System.Drawing.Point(89, 362);
            this.btn_excluirFuncaoClientePPRA.Name = "btn_excluirFuncaoClientePPRA";
            this.btn_excluirFuncaoClientePPRA.Size = new System.Drawing.Size(77, 23);
            this.btn_excluirFuncaoClientePPRA.TabIndex = 2;
            this.btn_excluirFuncaoClientePPRA.Text = "&Excluir";
            this.btn_excluirFuncaoClientePPRA.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btn_excluirFuncaoClientePPRA.UseVisualStyleBackColor = true;
            this.btn_excluirFuncaoClientePPRA.Click += new System.EventHandler(this.btn_excluirFuncaoClientePPRA_Click);
            // 
            // btn_incluirFuncaoClientePPRA
            // 
            this.btn_incluirFuncaoClientePPRA.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btn_incluirFuncaoClientePPRA.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_incluirFuncaoClientePPRA.Image = global::SWS.Properties.Resources.icone_mais;
            this.btn_incluirFuncaoClientePPRA.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btn_incluirFuncaoClientePPRA.Location = new System.Drawing.Point(6, 362);
            this.btn_incluirFuncaoClientePPRA.Name = "btn_incluirFuncaoClientePPRA";
            this.btn_incluirFuncaoClientePPRA.Size = new System.Drawing.Size(77, 23);
            this.btn_incluirFuncaoClientePPRA.TabIndex = 1;
            this.btn_incluirFuncaoClientePPRA.Text = "&Incluir";
            this.btn_incluirFuncaoClientePPRA.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btn_incluirFuncaoClientePPRA.UseVisualStyleBackColor = true;
            this.btn_incluirFuncaoClientePPRA.Click += new System.EventHandler(this.btn_incluirFuncaoClientePPRA_Click);
            // 
            // grb_funcao
            // 
            this.grb_funcao.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.grb_funcao.Controls.Add(this.grd_funcaoCliente);
            this.grb_funcao.Location = new System.Drawing.Point(6, 6);
            this.grb_funcao.Name = "grb_funcao";
            this.grb_funcao.Size = new System.Drawing.Size(729, 350);
            this.grb_funcao.TabIndex = 20;
            this.grb_funcao.TabStop = false;
            this.grb_funcao.Text = "Função do Cliente";
            // 
            // grd_funcaoCliente
            // 
            this.grd_funcaoCliente.AllowUserToAddRows = false;
            this.grd_funcaoCliente.AllowUserToDeleteRows = false;
            this.grd_funcaoCliente.AllowUserToOrderColumns = true;
            this.grd_funcaoCliente.AllowUserToResizeRows = false;
            this.grd_funcaoCliente.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this.grd_funcaoCliente.BackgroundColor = System.Drawing.Color.White;
            this.grd_funcaoCliente.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.grd_funcaoCliente.Dock = System.Windows.Forms.DockStyle.Fill;
            this.grd_funcaoCliente.Location = new System.Drawing.Point(3, 16);
            this.grd_funcaoCliente.Name = "grd_funcaoCliente";
            this.grd_funcaoCliente.ReadOnly = true;
            this.grd_funcaoCliente.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.grd_funcaoCliente.Size = new System.Drawing.Size(723, 331);
            this.grd_funcaoCliente.TabIndex = 16;
            // 
            // tb_funcaoPPRA
            // 
            this.tb_funcaoPPRA.Controls.Add(this.btn_excluirFuncaoCliente);
            this.tb_funcaoPPRA.Controls.Add(this.grb_funcaoCliente);
            this.tb_funcaoPPRA.Controls.Add(this.btn_incluirFuncaoCliente);
            this.tb_funcaoPPRA.Controls.Add(this.grb_setorFuncao);
            this.tb_funcaoPPRA.Controls.Add(this.grb_gheFuncao);
            this.tb_funcaoPPRA.Location = new System.Drawing.Point(4, 22);
            this.tb_funcaoPPRA.Name = "tb_funcaoPPRA";
            this.tb_funcaoPPRA.Padding = new System.Windows.Forms.Padding(3);
            this.tb_funcaoPPRA.Size = new System.Drawing.Size(741, 395);
            this.tb_funcaoPPRA.TabIndex = 2;
            this.tb_funcaoPPRA.Text = "Função";
            this.tb_funcaoPPRA.UseVisualStyleBackColor = true;
            // 
            // btn_excluirFuncaoCliente
            // 
            this.btn_excluirFuncaoCliente.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btn_excluirFuncaoCliente.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_excluirFuncaoCliente.Image = global::SWS.Properties.Resources.lixeira;
            this.btn_excluirFuncaoCliente.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btn_excluirFuncaoCliente.Location = new System.Drawing.Point(473, 354);
            this.btn_excluirFuncaoCliente.Name = "btn_excluirFuncaoCliente";
            this.btn_excluirFuncaoCliente.Size = new System.Drawing.Size(77, 23);
            this.btn_excluirFuncaoCliente.TabIndex = 4;
            this.btn_excluirFuncaoCliente.Text = "&Excluir";
            this.btn_excluirFuncaoCliente.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btn_excluirFuncaoCliente.UseVisualStyleBackColor = true;
            this.btn_excluirFuncaoCliente.Click += new System.EventHandler(this.btn_excluirFuncaoCliente_Click);
            // 
            // grb_funcaoCliente
            // 
            this.grb_funcaoCliente.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.grb_funcaoCliente.Controls.Add(this.grd_setorFuncao);
            this.grb_funcaoCliente.Location = new System.Drawing.Point(390, 18);
            this.grb_funcaoCliente.Name = "grb_funcaoCliente";
            this.grb_funcaoCliente.Size = new System.Drawing.Size(326, 330);
            this.grb_funcaoCliente.TabIndex = 8;
            this.grb_funcaoCliente.TabStop = false;
            this.grb_funcaoCliente.Text = "Funcao Cliente";
            // 
            // grd_setorFuncao
            // 
            this.grd_setorFuncao.AllowUserToAddRows = false;
            this.grd_setorFuncao.AllowUserToDeleteRows = false;
            this.grd_setorFuncao.AllowUserToOrderColumns = true;
            this.grd_setorFuncao.AllowUserToResizeRows = false;
            this.grd_setorFuncao.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this.grd_setorFuncao.BackgroundColor = System.Drawing.Color.White;
            this.grd_setorFuncao.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dataGridViewCellStyle11.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle11.BackColor = System.Drawing.Color.LightGray;
            dataGridViewCellStyle11.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle11.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle11.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle11.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle11.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.grd_setorFuncao.DefaultCellStyle = dataGridViewCellStyle11;
            this.grd_setorFuncao.Dock = System.Windows.Forms.DockStyle.Fill;
            this.grd_setorFuncao.Location = new System.Drawing.Point(3, 16);
            this.grd_setorFuncao.Name = "grd_setorFuncao";
            this.grd_setorFuncao.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.grd_setorFuncao.Size = new System.Drawing.Size(320, 311);
            this.grd_setorFuncao.TabIndex = 0;
            this.grd_setorFuncao.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.grd_setorFuncao_CellClick);
            this.grd_setorFuncao.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.grd_setorFuncao_CellContentClick);
            this.grd_setorFuncao.CellFormatting += new System.Windows.Forms.DataGridViewCellFormattingEventHandler(this.grd_setorFuncao_CellFormatting);
            // 
            // btn_incluirFuncaoCliente
            // 
            this.btn_incluirFuncaoCliente.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btn_incluirFuncaoCliente.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_incluirFuncaoCliente.Image = global::SWS.Properties.Resources.icone_mais;
            this.btn_incluirFuncaoCliente.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btn_incluirFuncaoCliente.Location = new System.Drawing.Point(390, 354);
            this.btn_incluirFuncaoCliente.Name = "btn_incluirFuncaoCliente";
            this.btn_incluirFuncaoCliente.Size = new System.Drawing.Size(77, 23);
            this.btn_incluirFuncaoCliente.TabIndex = 3;
            this.btn_incluirFuncaoCliente.Text = "&Incluir";
            this.btn_incluirFuncaoCliente.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btn_incluirFuncaoCliente.UseVisualStyleBackColor = true;
            this.btn_incluirFuncaoCliente.Click += new System.EventHandler(this.btn_incluirFuncaoCliente_Click);
            // 
            // grb_setorFuncao
            // 
            this.grb_setorFuncao.Controls.Add(this.cb_setorFuncao);
            this.grb_setorFuncao.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.grb_setorFuncao.Location = new System.Drawing.Point(6, 78);
            this.grb_setorFuncao.Name = "grb_setorFuncao";
            this.grb_setorFuncao.Size = new System.Drawing.Size(378, 54);
            this.grb_setorFuncao.TabIndex = 7;
            this.grb_setorFuncao.TabStop = false;
            this.grb_setorFuncao.Text = "Setor";
            // 
            // cb_setorFuncao
            // 
            this.cb_setorFuncao.BackColor = System.Drawing.Color.LightGray;
            this.cb_setorFuncao.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cb_setorFuncao.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cb_setorFuncao.FormattingEnabled = true;
            this.cb_setorFuncao.Location = new System.Drawing.Point(6, 19);
            this.cb_setorFuncao.Name = "cb_setorFuncao";
            this.cb_setorFuncao.Size = new System.Drawing.Size(366, 21);
            this.cb_setorFuncao.TabIndex = 2;
            this.cb_setorFuncao.SelectedIndexChanged += new System.EventHandler(this.cb_setorFuncao_SelectedIndexChanged);
            // 
            // grb_gheFuncao
            // 
            this.grb_gheFuncao.Controls.Add(this.cb_gheFuncao);
            this.grb_gheFuncao.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.grb_gheFuncao.Location = new System.Drawing.Point(6, 18);
            this.grb_gheFuncao.Name = "grb_gheFuncao";
            this.grb_gheFuncao.Size = new System.Drawing.Size(378, 54);
            this.grb_gheFuncao.TabIndex = 6;
            this.grb_gheFuncao.TabStop = false;
            this.grb_gheFuncao.Text = "GHE";
            // 
            // cb_gheFuncao
            // 
            this.cb_gheFuncao.BackColor = System.Drawing.Color.LightGray;
            this.cb_gheFuncao.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cb_gheFuncao.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cb_gheFuncao.FormattingEnabled = true;
            this.cb_gheFuncao.Location = new System.Drawing.Point(6, 19);
            this.cb_gheFuncao.Name = "cb_gheFuncao";
            this.cb_gheFuncao.Size = new System.Drawing.Size(366, 21);
            this.cb_gheFuncao.TabIndex = 1;
            this.cb_gheFuncao.SelectedIndexChanged += new System.EventHandler(this.cb_gheFuncao_SelectedIndexChanged);
            // 
            // tb_cronograma
            // 
            this.tb_cronograma.BackColor = System.Drawing.Color.White;
            this.tb_cronograma.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tb_cronograma.Controls.Add(this.btn_excluirAtividade);
            this.tb_cronograma.Controls.Add(this.grb_atividade);
            this.tb_cronograma.Controls.Add(this.btn_alterarAtividade);
            this.tb_cronograma.Controls.Add(this.btn_incluirAtividade);
            this.tb_cronograma.Location = new System.Drawing.Point(4, 22);
            this.tb_cronograma.Name = "tb_cronograma";
            this.tb_cronograma.Padding = new System.Windows.Forms.Padding(3);
            this.tb_cronograma.Size = new System.Drawing.Size(773, 429);
            this.tb_cronograma.TabIndex = 1;
            this.tb_cronograma.Text = "Cronograma";
            // 
            // btn_excluirAtividade
            // 
            this.btn_excluirAtividade.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btn_excluirAtividade.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_excluirAtividade.Image = global::SWS.Properties.Resources.lixeira;
            this.btn_excluirAtividade.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btn_excluirAtividade.Location = new System.Drawing.Point(172, 398);
            this.btn_excluirAtividade.Name = "btn_excluirAtividade";
            this.btn_excluirAtividade.Size = new System.Drawing.Size(77, 23);
            this.btn_excluirAtividade.TabIndex = 5;
            this.btn_excluirAtividade.Text = "&Excluir";
            this.btn_excluirAtividade.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btn_excluirAtividade.UseVisualStyleBackColor = true;
            this.btn_excluirAtividade.Click += new System.EventHandler(this.btn_excluirAtividade_Click);
            // 
            // grb_atividade
            // 
            this.grb_atividade.Controls.Add(this.grd_atividadeCronograma);
            this.grb_atividade.Dock = System.Windows.Forms.DockStyle.Top;
            this.grb_atividade.Location = new System.Drawing.Point(3, 3);
            this.grb_atividade.Name = "grb_atividade";
            this.grb_atividade.Size = new System.Drawing.Size(765, 386);
            this.grb_atividade.TabIndex = 19;
            this.grb_atividade.TabStop = false;
            this.grb_atividade.Text = "Atividades";
            // 
            // grd_atividadeCronograma
            // 
            this.grd_atividadeCronograma.AllowUserToAddRows = false;
            this.grd_atividadeCronograma.AllowUserToDeleteRows = false;
            this.grd_atividadeCronograma.AllowUserToOrderColumns = true;
            this.grd_atividadeCronograma.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.DisplayedCells;
            this.grd_atividadeCronograma.BackgroundColor = System.Drawing.Color.White;
            this.grd_atividadeCronograma.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.grd_atividadeCronograma.Dock = System.Windows.Forms.DockStyle.Fill;
            this.grd_atividadeCronograma.Location = new System.Drawing.Point(3, 16);
            this.grd_atividadeCronograma.Name = "grd_atividadeCronograma";
            this.grd_atividadeCronograma.ReadOnly = true;
            this.grd_atividadeCronograma.Size = new System.Drawing.Size(759, 367);
            this.grd_atividadeCronograma.TabIndex = 16;
            // 
            // btn_alterarAtividade
            // 
            this.btn_alterarAtividade.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btn_alterarAtividade.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_alterarAtividade.Image = global::SWS.Properties.Resources.Alterar;
            this.btn_alterarAtividade.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btn_alterarAtividade.Location = new System.Drawing.Point(89, 398);
            this.btn_alterarAtividade.Name = "btn_alterarAtividade";
            this.btn_alterarAtividade.Size = new System.Drawing.Size(77, 23);
            this.btn_alterarAtividade.TabIndex = 4;
            this.btn_alterarAtividade.Text = "&Alterar";
            this.btn_alterarAtividade.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btn_alterarAtividade.UseVisualStyleBackColor = true;
            this.btn_alterarAtividade.Click += new System.EventHandler(this.btn_alterarAtividade_Click);
            // 
            // btn_incluirAtividade
            // 
            this.btn_incluirAtividade.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btn_incluirAtividade.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_incluirAtividade.Image = global::SWS.Properties.Resources.icone_mais;
            this.btn_incluirAtividade.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btn_incluirAtividade.Location = new System.Drawing.Point(6, 398);
            this.btn_incluirAtividade.Name = "btn_incluirAtividade";
            this.btn_incluirAtividade.Size = new System.Drawing.Size(77, 23);
            this.btn_incluirAtividade.TabIndex = 3;
            this.btn_incluirAtividade.Text = "&Incluir";
            this.btn_incluirAtividade.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btn_incluirAtividade.UseVisualStyleBackColor = true;
            this.btn_incluirAtividade.Click += new System.EventHandler(this.btn_incluirAtividade_Click);
            // 
            // grb_comentario
            // 
            this.grb_comentario.Controls.Add(this.btn_gravarComentario);
            this.grb_comentario.Controls.Add(this.text_comentario);
            this.grb_comentario.Controls.Add(this.lbl_comentario);
            this.grb_comentario.Location = new System.Drawing.Point(4, 22);
            this.grb_comentario.Name = "grb_comentario";
            this.grb_comentario.Padding = new System.Windows.Forms.Padding(3);
            this.grb_comentario.Size = new System.Drawing.Size(773, 429);
            this.grb_comentario.TabIndex = 5;
            this.grb_comentario.Text = "Comentário";
            this.grb_comentario.UseVisualStyleBackColor = true;
            // 
            // btn_gravarComentario
            // 
            this.btn_gravarComentario.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btn_gravarComentario.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_gravarComentario.Image = global::SWS.Properties.Resources.Gravar;
            this.btn_gravarComentario.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btn_gravarComentario.Location = new System.Drawing.Point(9, 400);
            this.btn_gravarComentario.Name = "btn_gravarComentario";
            this.btn_gravarComentario.Size = new System.Drawing.Size(77, 23);
            this.btn_gravarComentario.TabIndex = 2;
            this.btn_gravarComentario.Text = "&Gravar";
            this.btn_gravarComentario.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btn_gravarComentario.UseVisualStyleBackColor = true;
            this.btn_gravarComentario.Click += new System.EventHandler(this.btn_gravarComentario_Click);
            // 
            // text_comentario
            // 
            this.text_comentario.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.text_comentario.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.text_comentario.ForeColor = System.Drawing.Color.Blue;
            this.text_comentario.Location = new System.Drawing.Point(9, 19);
            this.text_comentario.MaxLength = 500;
            this.text_comentario.Multiline = true;
            this.text_comentario.Name = "text_comentario";
            this.text_comentario.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.text_comentario.Size = new System.Drawing.Size(748, 375);
            this.text_comentario.TabIndex = 1;
            // 
            // lbl_comentario
            // 
            this.lbl_comentario.AutoSize = true;
            this.lbl_comentario.Location = new System.Drawing.Point(6, 3);
            this.lbl_comentario.Name = "lbl_comentario";
            this.lbl_comentario.Size = new System.Drawing.Size(34, 13);
            this.lbl_comentario.TabIndex = 0;
            this.lbl_comentario.Text = "Texto";
            // 
            // tb_anexo
            // 
            this.tb_anexo.Controls.Add(this.lblTituloAnexo);
            this.tb_anexo.Controls.Add(this.btn_excluirAnexo);
            this.tb_anexo.Controls.Add(this.btn_incluirAnexo);
            this.tb_anexo.Controls.Add(this.grb_anexo);
            this.tb_anexo.Controls.Add(this.text_conteudo);
            this.tb_anexo.Location = new System.Drawing.Point(4, 22);
            this.tb_anexo.Name = "tb_anexo";
            this.tb_anexo.Padding = new System.Windows.Forms.Padding(3);
            this.tb_anexo.Size = new System.Drawing.Size(773, 429);
            this.tb_anexo.TabIndex = 6;
            this.tb_anexo.Text = "Anexo";
            this.tb_anexo.UseVisualStyleBackColor = true;
            // 
            // lblTituloAnexo
            // 
            this.lblTituloAnexo.AutoSize = true;
            this.lblTituloAnexo.Location = new System.Drawing.Point(6, 5);
            this.lblTituloAnexo.Name = "lblTituloAnexo";
            this.lblTituloAnexo.Size = new System.Drawing.Size(68, 13);
            this.lblTituloAnexo.TabIndex = 5;
            this.lblTituloAnexo.Text = "Título Anexo";
            // 
            // btn_excluirAnexo
            // 
            this.btn_excluirAnexo.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btn_excluirAnexo.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_excluirAnexo.Image = global::SWS.Properties.Resources.lixeira;
            this.btn_excluirAnexo.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btn_excluirAnexo.Location = new System.Drawing.Point(6, 400);
            this.btn_excluirAnexo.Name = "btn_excluirAnexo";
            this.btn_excluirAnexo.Size = new System.Drawing.Size(75, 23);
            this.btn_excluirAnexo.TabIndex = 4;
            this.btn_excluirAnexo.Text = "Excluir";
            this.btn_excluirAnexo.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btn_excluirAnexo.UseVisualStyleBackColor = true;
            this.btn_excluirAnexo.Click += new System.EventHandler(this.btn_excluirAnexo_Click);
            // 
            // btn_incluirAnexo
            // 
            this.btn_incluirAnexo.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_incluirAnexo.Image = global::SWS.Properties.Resources.fechar_ico;
            this.btn_incluirAnexo.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btn_incluirAnexo.Location = new System.Drawing.Point(9, 47);
            this.btn_incluirAnexo.Name = "btn_incluirAnexo";
            this.btn_incluirAnexo.Size = new System.Drawing.Size(77, 23);
            this.btn_incluirAnexo.TabIndex = 2;
            this.btn_incluirAnexo.Text = "Confirmar&";
            this.btn_incluirAnexo.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btn_incluirAnexo.UseVisualStyleBackColor = true;
            this.btn_incluirAnexo.Click += new System.EventHandler(this.btn_incluirAnexo_Click);
            // 
            // grb_anexo
            // 
            this.grb_anexo.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.grb_anexo.Controls.Add(this.grd_anexo);
            this.grb_anexo.Enabled = false;
            this.grb_anexo.Location = new System.Drawing.Point(3, 75);
            this.grb_anexo.Name = "grb_anexo";
            this.grb_anexo.Size = new System.Drawing.Size(764, 319);
            this.grb_anexo.TabIndex = 1;
            this.grb_anexo.TabStop = false;
            this.grb_anexo.Text = "Anexos";
            // 
            // grd_anexo
            // 
            this.grd_anexo.AllowUserToAddRows = false;
            this.grd_anexo.AllowUserToDeleteRows = false;
            this.grd_anexo.AllowUserToOrderColumns = true;
            this.grd_anexo.AllowUserToResizeRows = false;
            this.grd_anexo.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.DisplayedCells;
            this.grd_anexo.BackgroundColor = System.Drawing.Color.White;
            this.grd_anexo.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.grd_anexo.Dock = System.Windows.Forms.DockStyle.Fill;
            this.grd_anexo.Location = new System.Drawing.Point(3, 16);
            this.grd_anexo.Name = "grd_anexo";
            this.grd_anexo.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.grd_anexo.Size = new System.Drawing.Size(758, 300);
            this.grd_anexo.TabIndex = 3;
            this.grd_anexo.CellEndEdit += new System.Windows.Forms.DataGridViewCellEventHandler(this.grd_anexo_CellEndEdit);
            this.grd_anexo.EditingControlShowing += new System.Windows.Forms.DataGridViewEditingControlShowingEventHandler(this.grd_anexo_EditingControlShowing);
            // 
            // text_conteudo
            // 
            this.text_conteudo.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.text_conteudo.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.text_conteudo.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.text_conteudo.Location = new System.Drawing.Point(9, 21);
            this.text_conteudo.MaxLength = 200;
            this.text_conteudo.Name = "text_conteudo";
            this.text_conteudo.Size = new System.Drawing.Size(758, 20);
            this.text_conteudo.TabIndex = 1;
            // 
            // IncluirPPRAErrorProvider
            // 
            this.IncluirPPRAErrorProvider.ContainerControl = this;
            // 
            // frm_ppraIncluir
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(794, 568);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = true;
            this.Name = "frm_ppraIncluir";
            this.Text = "CRIAR PPRA";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.frm_ppraIncluir_Load);
            this.panel_banner.ResumeLayout(false);
            this.panel_banner.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox_banner)).EndInit();
            this.panel.ResumeLayout(false);
            this.grb_paginacao.ResumeLayout(false);
            this.grb_dados.ResumeLayout(false);
            this.grb_dados.PerformLayout();
            this.grb_grupo1.ResumeLayout(false);
            this.grb_grupo1.PerformLayout();
            this.grb_cliente.ResumeLayout(false);
            this.grb_cliente.PerformLayout();
            this.grb_tecnico.ResumeLayout(false);
            this.grb_tecnico.PerformLayout();
            this.grb_grauRisco.ResumeLayout(false);
            this.grb_data.ResumeLayout(false);
            this.grb_data.PerformLayout();
            this.tb_dados.ResumeLayout(false);
            this.tb_dadosPPRA.ResumeLayout(false);
            this.tbConfiguracaoExtra.ResumeLayout(false);
            this.tpDados.ResumeLayout(false);
            this.tpDados.PerformLayout();
            this.tpPrevisao.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgPrevisao)).EndInit();
            this.tpCNAE.ResumeLayout(false);
            this.tpCNAE.PerformLayout();
            this.tcCnae.ResumeLayout(false);
            this.cnaeCliente.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvCnaeCliente)).EndInit();
            this.cnaeContratante.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvCnaeContratante)).EndInit();
            this.tb_ghe.ResumeLayout(false);
            this.grb_ghePPRA.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.grd_ghe)).EndInit();
            this.tb_gheFonte.ResumeLayout(false);
            this.tb_fonteGHE.ResumeLayout(false);
            this.Fonte.ResumeLayout(false);
            this.grb_gheFonte.ResumeLayout(false);
            this.grb_fonte.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.grd_fonte)).EndInit();
            this.agente.ResumeLayout(false);
            this.grb_gheAgente.ResumeLayout(false);
            this.grb_fonteAgente.ResumeLayout(false);
            this.grb_agente.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.grd_agente)).EndInit();
            this.epi.ResumeLayout(false);
            this.grb_epi.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.grd_epi)).EndInit();
            this.grb_agenteEpi.ResumeLayout(false);
            this.grb_fonteEpi.ResumeLayout(false);
            this.grb_gheEPI.ResumeLayout(false);
            this.tb_setorGHE.ResumeLayout(false);
            this.tb_gheSetor.ResumeLayout(false);
            this.tb_setor.ResumeLayout(false);
            this.grb_setor.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.grd_setor)).EndInit();
            this.grb_gheSetor.ResumeLayout(false);
            this.tb_funcao.ResumeLayout(false);
            this.grb_funcao.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.grd_funcaoCliente)).EndInit();
            this.tb_funcaoPPRA.ResumeLayout(false);
            this.grb_funcaoCliente.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.grd_setorFuncao)).EndInit();
            this.grb_setorFuncao.ResumeLayout(false);
            this.grb_gheFuncao.ResumeLayout(false);
            this.tb_cronograma.ResumeLayout(false);
            this.grb_atividade.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.grd_atividadeCronograma)).EndInit();
            this.grb_comentario.ResumeLayout(false);
            this.grb_comentario.PerformLayout();
            this.tb_anexo.ResumeLayout(false);
            this.tb_anexo.PerformLayout();
            this.grb_anexo.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.grd_anexo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.IncluirPPRAErrorProvider)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        protected System.Windows.Forms.GroupBox grb_paginacao;
        protected System.Windows.Forms.Button btn_fechar;
        protected System.Windows.Forms.Label lbl_codPpra;
        protected System.Windows.Forms.GroupBox grb_dados;
        protected System.Windows.Forms.MaskedTextBox text_codPpra;
        public System.Windows.Forms.TextBox text_cliente;
        protected System.Windows.Forms.Label lbl_vencimento;
        protected System.Windows.Forms.DateTimePicker dt_vencimento;
        public System.Windows.Forms.Button btn_cliente;
        public System.Windows.Forms.GroupBox grb_grupo1;
        protected System.Windows.Forms.Label lbl_contrato;
        public System.Windows.Forms.TextBox text_numContrato;
        protected System.Windows.Forms.Label lbl_fimContrato;
        public System.Windows.Forms.TextBox text_clienteContratante;
        public System.Windows.Forms.Button btn_contratante;
        protected System.Windows.Forms.GroupBox grb_cliente;
        protected System.Windows.Forms.GroupBox grb_tecnico;
        protected System.Windows.Forms.Button btn_tecnico;
        public System.Windows.Forms.TextBox text_tecnico;
        protected System.Windows.Forms.GroupBox grb_data;
        protected System.Windows.Forms.TabControl tb_dados;
        protected System.Windows.Forms.TabPage tb_dadosPPRA;
        protected System.Windows.Forms.TabPage tb_cronograma;
        protected System.Windows.Forms.GroupBox grb_grauRisco;
        protected System.Windows.Forms.TabPage tb_ghe;
        protected System.Windows.Forms.TabPage tb_gheFonte;
        protected System.Windows.Forms.TabPage tb_setorGHE;
        protected System.Windows.Forms.Button btn_limpar;
        protected System.Windows.Forms.Button btn_excluirAtividade;
        public System.Windows.Forms.DataGridView grd_atividadeCronograma;
        protected System.Windows.Forms.Button btn_salvar;
        public System.Windows.Forms.DataGridView grd_ghe;
        protected System.Windows.Forms.Button btn_incluirGHE;
        protected System.Windows.Forms.Button btn_excluirGHE;
        protected System.Windows.Forms.Button btn_incluirAtividade;
        protected System.Windows.Forms.GroupBox grb_atividade;
        protected System.Windows.Forms.GroupBox grb_ghePPRA;
        protected System.Windows.Forms.TabControl tb_fonteGHE;
        protected System.Windows.Forms.TabPage Fonte;
        protected System.Windows.Forms.TabPage agente;
        protected System.Windows.Forms.TabPage epi;
        protected System.Windows.Forms.GroupBox grb_fonteAgente;
        protected System.Windows.Forms.ComboBox cb_fonteAgente;
        protected System.Windows.Forms.GroupBox grb_agente;
        protected System.Windows.Forms.Button btn_excluirAgente;
        protected System.Windows.Forms.Button btn_incluirAgente;
        protected System.Windows.Forms.DataGridView grd_agente;
        protected System.Windows.Forms.GroupBox grb_epi;
        protected System.Windows.Forms.Button btn_excluirEPI;
        protected System.Windows.Forms.Button btn_incluirEpi;
        protected System.Windows.Forms.DataGridView grd_epi;
        protected System.Windows.Forms.GroupBox grb_agenteEpi;
        protected System.Windows.Forms.ComboBox cb_agenteEpi;
        protected System.Windows.Forms.GroupBox grb_fonteEpi;
        protected System.Windows.Forms.ComboBox cb_fonteEpi;
        protected System.Windows.Forms.GroupBox grb_gheEPI;
        public System.Windows.Forms.ComboBox cb_gheEPI;
        protected System.Windows.Forms.GroupBox grb_gheFonte;
        public System.Windows.Forms.ComboBox cb_ghe;
        protected System.Windows.Forms.GroupBox grb_fonte;
        protected System.Windows.Forms.DataGridView grd_fonte;
        protected System.Windows.Forms.Button btn_excluirFonte;
        protected System.Windows.Forms.Button btn_incluirFonte;
        protected System.Windows.Forms.TabControl tb_gheSetor;
        protected System.Windows.Forms.TabPage tb_setor;
        protected System.Windows.Forms.GroupBox grb_setor;
        protected System.Windows.Forms.DataGridView grd_setor;
        protected System.Windows.Forms.Button btn_excluirSetor;
        protected System.Windows.Forms.Button btn_incluirSetor;
        protected System.Windows.Forms.GroupBox grb_gheSetor;
        public System.Windows.Forms.ComboBox cb_gheSetor;
        protected System.Windows.Forms.TabPage tb_funcao;
        protected System.Windows.Forms.GroupBox grb_funcao;
        protected System.Windows.Forms.Button btn_incluirFuncaoClientePPRA;
        protected System.Windows.Forms.Button btn_excluirFuncaoClientePPRA;
        protected System.Windows.Forms.DataGridView grd_funcaoCliente;
        protected System.Windows.Forms.TabPage tb_funcaoPPRA;
        protected System.Windows.Forms.GroupBox grb_funcaoCliente;
        protected System.Windows.Forms.Button btn_excluirFuncaoCliente;
        protected System.Windows.Forms.Button btn_incluirFuncaoCliente;
        public System.Windows.Forms.DataGridView grd_setorFuncao;
        protected System.Windows.Forms.GroupBox grb_setorFuncao;
        public System.Windows.Forms.ComboBox cb_setorFuncao;
        protected System.Windows.Forms.GroupBox grb_gheFuncao;
        public System.Windows.Forms.ComboBox cb_gheFuncao;
        protected System.Windows.Forms.TextBox text_localObra;
        protected System.Windows.Forms.Label lbl_local;
        protected System.Windows.Forms.ComboBox cb_uf;
        protected System.Windows.Forms.TextBox text_cidade;
        protected System.Windows.Forms.Label lbl_cidade;
        protected System.Windows.Forms.TextBox text_bairro;
        protected System.Windows.Forms.Label lbl_bairro;
        protected System.Windows.Forms.TextBox text_complemento;
        protected System.Windows.Forms.Label lbl_complemento;
        protected System.Windows.Forms.TextBox text_numero;
        protected System.Windows.Forms.Label lbl_numero;
        protected System.Windows.Forms.TextBox text_endereco;
        protected System.Windows.Forms.Label lbl_endereco;
        protected System.Windows.Forms.TextBox text_obra;
        protected System.Windows.Forms.Label lbl_obra;
        protected System.Windows.Forms.ErrorProvider IncluirPPRAErrorProvider;
        protected System.Windows.Forms.Button btn_alterarAtividade;
        protected System.Windows.Forms.Button btn_alterarGhe;
        protected System.Windows.Forms.GroupBox grb_gheAgente;
        public System.Windows.Forms.ComboBox cb_gheAgente;
        protected System.Windows.Forms.ComboBox cb_grauRisco;
        public System.Windows.Forms.TextBox text_fimContr;
        protected System.Windows.Forms.Label lbl_inicioContr;
        public System.Windows.Forms.TextBox text_inicioContr;
        protected System.Windows.Forms.Label lbl_dataVencimento;
        protected System.Windows.Forms.Label lbl_dataCriacao;
        protected System.Windows.Forms.DateTimePicker dt_criacao;
        protected System.Windows.Forms.Label lbl_cep;
        protected System.Windows.Forms.MaskedTextBox text_cep;
        protected System.Windows.Forms.TabPage grb_comentario;
        protected System.Windows.Forms.TextBox text_comentario;
        protected System.Windows.Forms.Label lbl_comentario;
        private System.ComponentModel.IContainer components;
        private System.Windows.Forms.TabPage tb_anexo;
        protected System.Windows.Forms.GroupBox grb_anexo;
        protected System.Windows.Forms.Button btn_excluirAnexo;
        protected System.Windows.Forms.DataGridView grd_anexo;
        protected System.Windows.Forms.Button btn_incluirAnexo;
        protected System.Windows.Forms.TextBox text_conteudo;
        private System.Windows.Forms.Button btn_gravarComentario;
        private System.Windows.Forms.Label lblTituloAnexo;
        protected System.Windows.Forms.Label lbl_UF;
        private System.Windows.Forms.ToolTip tpPpra;
        private System.Windows.Forms.TabPage tpDados;
        private System.Windows.Forms.TabPage tpPrevisao;
        protected System.Windows.Forms.TabControl tbConfiguracaoExtra;
        protected System.Windows.Forms.Button btnExcluirPrevisao;
        protected System.Windows.Forms.Button btnIncluirPrevisao;
        protected System.Windows.Forms.DataGridView dgPrevisao;
        private System.Windows.Forms.TabPage tpCNAE;
        private System.Windows.Forms.Label lblCNAECliente;
        private System.Windows.Forms.TabControl tcCnae;
        private System.Windows.Forms.TabPage cnaeCliente;
        private System.Windows.Forms.TabPage cnaeContratante;
        protected System.Windows.Forms.DataGridView dgvCnaeContratante;
        protected System.Windows.Forms.DataGridView dgvCnaeCliente;
    }
}