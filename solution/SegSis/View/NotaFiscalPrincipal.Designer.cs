﻿namespace SWS.View
{
    partial class frmNotaFiscalPrincipal
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmNotaFiscalPrincipal));
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            this.flpAcao = new System.Windows.Forms.FlowLayoutPanel();
            this.btnPesquisar = new System.Windows.Forms.Button();
            this.btnDetalhar = new System.Windows.Forms.Button();
            this.btnCancelar = new System.Windows.Forms.Button();
            this.btnImprimir = new System.Windows.Forms.Button();
            this.btnFechar = new System.Windows.Forms.Button();
            this.lblEmpresa = new System.Windows.Forms.TextBox();
            this.lblSituacao = new System.Windows.Forms.TextBox();
            this.lblCliente = new System.Windows.Forms.TextBox();
            this.textEmpresa = new System.Windows.Forms.TextBox();
            this.cbSituacao = new SWS.ComboBoxWithBorder();
            this.textCliente = new System.Windows.Forms.TextBox();
            this.btnEmpresa = new System.Windows.Forms.Button();
            this.btnCliente = new System.Windows.Forms.Button();
            this.btnExcluirEmpresa = new System.Windows.Forms.Button();
            this.btnExcluirCliente = new System.Windows.Forms.Button();
            this.lblNumeroNota = new System.Windows.Forms.TextBox();
            this.lblDataEmissao = new System.Windows.Forms.TextBox();
            this.lblDataCancelamento = new System.Windows.Forms.TextBox();
            this.dataEmissaoFinal = new System.Windows.Forms.DateTimePicker();
            this.dataEmissaoInicial = new System.Windows.Forms.DateTimePicker();
            this.dataCancelamentoFinal = new System.Windows.Forms.DateTimePicker();
            this.dataCancelamentoInicial = new System.Windows.Forms.DateTimePicker();
            this.textNumeroNota = new System.Windows.Forms.TextBox();
            this.grbNotas = new System.Windows.Forms.GroupBox();
            this.dgvNotaFiscal = new System.Windows.Forms.DataGridView();
            this.gbrTotais = new System.Windows.Forms.GroupBox();
            this.textValorLiquido = new System.Windows.Forms.TextBox();
            this.lblValorLiquido = new System.Windows.Forms.TextBox();
            this.textQuantidade = new System.Windows.Forms.TextBox();
            this.lblQuantidade = new System.Windows.Forms.TextBox();
            this.lblValorNF = new System.Windows.Forms.TextBox();
            this.text_valorTotal = new System.Windows.Forms.TextBox();
            ((System.ComponentModel.ISupportInitialize)(this.spForm)).BeginInit();
            this.spForm.Panel1.SuspendLayout();
            this.spForm.Panel2.SuspendLayout();
            this.spForm.SuspendLayout();
            this.pnlForm.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.banner)).BeginInit();
            this.flpAcao.SuspendLayout();
            this.grbNotas.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvNotaFiscal)).BeginInit();
            this.gbrTotais.SuspendLayout();
            this.SuspendLayout();
            // 
            // spForm
            // 
            // 
            // spForm.Panel1
            // 
            this.spForm.Panel1.Controls.Add(this.flpAcao);
            // 
            // pnlForm
            // 
            this.pnlForm.Controls.Add(this.gbrTotais);
            this.pnlForm.Controls.Add(this.grbNotas);
            this.pnlForm.Controls.Add(this.textNumeroNota);
            this.pnlForm.Controls.Add(this.lblDataCancelamento);
            this.pnlForm.Controls.Add(this.lblDataEmissao);
            this.pnlForm.Controls.Add(this.dataCancelamentoFinal);
            this.pnlForm.Controls.Add(this.dataCancelamentoInicial);
            this.pnlForm.Controls.Add(this.dataEmissaoFinal);
            this.pnlForm.Controls.Add(this.lblNumeroNota);
            this.pnlForm.Controls.Add(this.dataEmissaoInicial);
            this.pnlForm.Controls.Add(this.btnExcluirCliente);
            this.pnlForm.Controls.Add(this.btnExcluirEmpresa);
            this.pnlForm.Controls.Add(this.btnCliente);
            this.pnlForm.Controls.Add(this.btnEmpresa);
            this.pnlForm.Controls.Add(this.textCliente);
            this.pnlForm.Controls.Add(this.cbSituacao);
            this.pnlForm.Controls.Add(this.textEmpresa);
            this.pnlForm.Controls.Add(this.lblCliente);
            this.pnlForm.Controls.Add(this.lblSituacao);
            this.pnlForm.Controls.Add(this.lblEmpresa);
            // 
            // flpAcao
            // 
            this.flpAcao.Controls.Add(this.btnPesquisar);
            this.flpAcao.Controls.Add(this.btnDetalhar);
            this.flpAcao.Controls.Add(this.btnCancelar);
            this.flpAcao.Controls.Add(this.btnImprimir);
            this.flpAcao.Controls.Add(this.btnFechar);
            this.flpAcao.Dock = System.Windows.Forms.DockStyle.Fill;
            this.flpAcao.Location = new System.Drawing.Point(0, 0);
            this.flpAcao.Name = "flpAcao";
            this.flpAcao.Size = new System.Drawing.Size(784, 35);
            this.flpAcao.TabIndex = 0;
            // 
            // btnPesquisar
            // 
            this.btnPesquisar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnPesquisar.Image = global::SWS.Properties.Resources.lupa;
            this.btnPesquisar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnPesquisar.Location = new System.Drawing.Point(3, 3);
            this.btnPesquisar.Name = "btnPesquisar";
            this.btnPesquisar.Size = new System.Drawing.Size(75, 23);
            this.btnPesquisar.TabIndex = 15;
            this.btnPesquisar.Text = "&Pesquisar";
            this.btnPesquisar.UseVisualStyleBackColor = true;
            this.btnPesquisar.Click += new System.EventHandler(this.btn_pesquisar_Click);
            // 
            // btnDetalhar
            // 
            this.btnDetalhar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnDetalhar.Image = global::SWS.Properties.Resources.busca;
            this.btnDetalhar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnDetalhar.Location = new System.Drawing.Point(84, 3);
            this.btnDetalhar.Name = "btnDetalhar";
            this.btnDetalhar.Size = new System.Drawing.Size(75, 23);
            this.btnDetalhar.TabIndex = 16;
            this.btnDetalhar.Text = "&Detalhar";
            this.btnDetalhar.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnDetalhar.UseVisualStyleBackColor = true;
            this.btnDetalhar.Click += new System.EventHandler(this.btn_detalhar_Click);
            // 
            // btnCancelar
            // 
            this.btnCancelar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnCancelar.Image = global::SWS.Properties.Resources.lixeira;
            this.btnCancelar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnCancelar.Location = new System.Drawing.Point(165, 3);
            this.btnCancelar.Name = "btnCancelar";
            this.btnCancelar.Size = new System.Drawing.Size(75, 23);
            this.btnCancelar.TabIndex = 16;
            this.btnCancelar.Text = "C&ancelar";
            this.btnCancelar.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnCancelar.UseVisualStyleBackColor = true;
            this.btnCancelar.Click += new System.EventHandler(this.btn_cancelar_Click);
            // 
            // btnImprimir
            // 
            this.btnImprimir.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnImprimir.Image = ((System.Drawing.Image)(resources.GetObject("btnImprimir.Image")));
            this.btnImprimir.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnImprimir.Location = new System.Drawing.Point(246, 3);
            this.btnImprimir.Name = "btnImprimir";
            this.btnImprimir.Size = new System.Drawing.Size(75, 23);
            this.btnImprimir.TabIndex = 17;
            this.btnImprimir.Text = "&Imprimir";
            this.btnImprimir.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnImprimir.UseVisualStyleBackColor = true;
            this.btnImprimir.Click += new System.EventHandler(this.btn_imprimir_Click);
            // 
            // btnFechar
            // 
            this.btnFechar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnFechar.Image = global::SWS.Properties.Resources.close;
            this.btnFechar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnFechar.Location = new System.Drawing.Point(327, 3);
            this.btnFechar.Name = "btnFechar";
            this.btnFechar.Size = new System.Drawing.Size(75, 23);
            this.btnFechar.TabIndex = 18;
            this.btnFechar.Text = "&Fechar";
            this.btnFechar.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnFechar.UseVisualStyleBackColor = true;
            this.btnFechar.Click += new System.EventHandler(this.btn_fechar_Click);
            // 
            // lblEmpresa
            // 
            this.lblEmpresa.BackColor = System.Drawing.Color.LightGray;
            this.lblEmpresa.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblEmpresa.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblEmpresa.Location = new System.Drawing.Point(6, 6);
            this.lblEmpresa.Name = "lblEmpresa";
            this.lblEmpresa.ReadOnly = true;
            this.lblEmpresa.Size = new System.Drawing.Size(113, 21);
            this.lblEmpresa.TabIndex = 0;
            this.lblEmpresa.TabStop = false;
            this.lblEmpresa.Text = "Empresa";
            // 
            // lblSituacao
            // 
            this.lblSituacao.BackColor = System.Drawing.Color.LightGray;
            this.lblSituacao.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblSituacao.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSituacao.Location = new System.Drawing.Point(6, 26);
            this.lblSituacao.Name = "lblSituacao";
            this.lblSituacao.ReadOnly = true;
            this.lblSituacao.Size = new System.Drawing.Size(113, 21);
            this.lblSituacao.TabIndex = 1;
            this.lblSituacao.TabStop = false;
            this.lblSituacao.Text = "Situação";
            // 
            // lblCliente
            // 
            this.lblCliente.BackColor = System.Drawing.Color.LightGray;
            this.lblCliente.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblCliente.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCliente.Location = new System.Drawing.Point(6, 46);
            this.lblCliente.Name = "lblCliente";
            this.lblCliente.ReadOnly = true;
            this.lblCliente.Size = new System.Drawing.Size(113, 21);
            this.lblCliente.TabIndex = 2;
            this.lblCliente.TabStop = false;
            this.lblCliente.Text = "Cliente";
            // 
            // textEmpresa
            // 
            this.textEmpresa.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textEmpresa.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textEmpresa.Location = new System.Drawing.Point(118, 6);
            this.textEmpresa.Name = "textEmpresa";
            this.textEmpresa.Size = new System.Drawing.Size(223, 21);
            this.textEmpresa.TabIndex = 3;
            this.textEmpresa.TabStop = false;
            // 
            // cbSituacao
            // 
            this.cbSituacao.BackColor = System.Drawing.Color.LightGray;
            this.cbSituacao.BorderColor = System.Drawing.Color.DimGray;
            this.cbSituacao.BorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid;
            this.cbSituacao.FormattingEnabled = true;
            this.cbSituacao.Location = new System.Drawing.Point(118, 26);
            this.cbSituacao.Name = "cbSituacao";
            this.cbSituacao.Size = new System.Drawing.Size(289, 21);
            this.cbSituacao.TabIndex = 4;
            // 
            // textCliente
            // 
            this.textCliente.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textCliente.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textCliente.Location = new System.Drawing.Point(118, 46);
            this.textCliente.Name = "textCliente";
            this.textCliente.Size = new System.Drawing.Size(223, 21);
            this.textCliente.TabIndex = 5;
            this.textCliente.TabStop = false;
            // 
            // btnEmpresa
            // 
            this.btnEmpresa.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnEmpresa.Image = global::SWS.Properties.Resources.busca;
            this.btnEmpresa.Location = new System.Drawing.Point(340, 6);
            this.btnEmpresa.Name = "btnEmpresa";
            this.btnEmpresa.Size = new System.Drawing.Size(34, 21);
            this.btnEmpresa.TabIndex = 6;
            this.btnEmpresa.UseVisualStyleBackColor = true;
            this.btnEmpresa.Click += new System.EventHandler(this.btnEmpresa_Click);
            // 
            // btnCliente
            // 
            this.btnCliente.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnCliente.Image = global::SWS.Properties.Resources.busca;
            this.btnCliente.Location = new System.Drawing.Point(340, 46);
            this.btnCliente.Name = "btnCliente";
            this.btnCliente.Size = new System.Drawing.Size(34, 21);
            this.btnCliente.TabIndex = 7;
            this.btnCliente.UseVisualStyleBackColor = true;
            this.btnCliente.Click += new System.EventHandler(this.btnCliente_Click);
            // 
            // btnExcluirEmpresa
            // 
            this.btnExcluirEmpresa.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnExcluirEmpresa.Image = global::SWS.Properties.Resources.close;
            this.btnExcluirEmpresa.Location = new System.Drawing.Point(373, 6);
            this.btnExcluirEmpresa.Name = "btnExcluirEmpresa";
            this.btnExcluirEmpresa.Size = new System.Drawing.Size(34, 21);
            this.btnExcluirEmpresa.TabIndex = 8;
            this.btnExcluirEmpresa.UseVisualStyleBackColor = true;
            this.btnExcluirEmpresa.Click += new System.EventHandler(this.btnExcluirEmpresa_Click);
            // 
            // btnExcluirCliente
            // 
            this.btnExcluirCliente.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnExcluirCliente.Image = global::SWS.Properties.Resources.close;
            this.btnExcluirCliente.Location = new System.Drawing.Point(373, 46);
            this.btnExcluirCliente.Name = "btnExcluirCliente";
            this.btnExcluirCliente.Size = new System.Drawing.Size(34, 21);
            this.btnExcluirCliente.TabIndex = 9;
            this.btnExcluirCliente.UseVisualStyleBackColor = true;
            this.btnExcluirCliente.Click += new System.EventHandler(this.btnExcluirCliente_Click);
            // 
            // lblNumeroNota
            // 
            this.lblNumeroNota.BackColor = System.Drawing.Color.LightGray;
            this.lblNumeroNota.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblNumeroNota.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNumeroNota.Location = new System.Drawing.Point(413, 6);
            this.lblNumeroNota.MaxLength = 10;
            this.lblNumeroNota.Name = "lblNumeroNota";
            this.lblNumeroNota.ReadOnly = true;
            this.lblNumeroNota.Size = new System.Drawing.Size(113, 21);
            this.lblNumeroNota.TabIndex = 10;
            this.lblNumeroNota.Text = "Número Nota";
            // 
            // lblDataEmissao
            // 
            this.lblDataEmissao.BackColor = System.Drawing.Color.LightGray;
            this.lblDataEmissao.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblDataEmissao.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDataEmissao.Location = new System.Drawing.Point(413, 26);
            this.lblDataEmissao.MaxLength = 10;
            this.lblDataEmissao.Name = "lblDataEmissao";
            this.lblDataEmissao.ReadOnly = true;
            this.lblDataEmissao.Size = new System.Drawing.Size(113, 21);
            this.lblDataEmissao.TabIndex = 11;
            this.lblDataEmissao.Text = "Dt Emissão";
            // 
            // lblDataCancelamento
            // 
            this.lblDataCancelamento.BackColor = System.Drawing.Color.LightGray;
            this.lblDataCancelamento.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblDataCancelamento.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDataCancelamento.Location = new System.Drawing.Point(413, 46);
            this.lblDataCancelamento.MaxLength = 10;
            this.lblDataCancelamento.Name = "lblDataCancelamento";
            this.lblDataCancelamento.ReadOnly = true;
            this.lblDataCancelamento.Size = new System.Drawing.Size(113, 21);
            this.lblDataCancelamento.TabIndex = 12;
            this.lblDataCancelamento.Text = "Dt Cancelamento";
            // 
            // dataEmissaoFinal
            // 
            this.dataEmissaoFinal.Checked = false;
            this.dataEmissaoFinal.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dataEmissaoFinal.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dataEmissaoFinal.Location = new System.Drawing.Point(641, 26);
            this.dataEmissaoFinal.Name = "dataEmissaoFinal";
            this.dataEmissaoFinal.ShowCheckBox = true;
            this.dataEmissaoFinal.Size = new System.Drawing.Size(117, 21);
            this.dataEmissaoFinal.TabIndex = 4;
            // 
            // dataEmissaoInicial
            // 
            this.dataEmissaoInicial.Checked = false;
            this.dataEmissaoInicial.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dataEmissaoInicial.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dataEmissaoInicial.Location = new System.Drawing.Point(525, 26);
            this.dataEmissaoInicial.Name = "dataEmissaoInicial";
            this.dataEmissaoInicial.ShowCheckBox = true;
            this.dataEmissaoInicial.Size = new System.Drawing.Size(117, 21);
            this.dataEmissaoInicial.TabIndex = 3;
            // 
            // dataCancelamentoFinal
            // 
            this.dataCancelamentoFinal.Checked = false;
            this.dataCancelamentoFinal.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dataCancelamentoFinal.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dataCancelamentoFinal.Location = new System.Drawing.Point(641, 46);
            this.dataCancelamentoFinal.Name = "dataCancelamentoFinal";
            this.dataCancelamentoFinal.ShowCheckBox = true;
            this.dataCancelamentoFinal.Size = new System.Drawing.Size(117, 21);
            this.dataCancelamentoFinal.TabIndex = 14;
            // 
            // dataCancelamentoInicial
            // 
            this.dataCancelamentoInicial.Checked = false;
            this.dataCancelamentoInicial.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dataCancelamentoInicial.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dataCancelamentoInicial.Location = new System.Drawing.Point(525, 46);
            this.dataCancelamentoInicial.Name = "dataCancelamentoInicial";
            this.dataCancelamentoInicial.ShowCheckBox = true;
            this.dataCancelamentoInicial.Size = new System.Drawing.Size(117, 21);
            this.dataCancelamentoInicial.TabIndex = 13;
            // 
            // textNumeroNota
            // 
            this.textNumeroNota.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textNumeroNota.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textNumeroNota.Location = new System.Drawing.Point(525, 6);
            this.textNumeroNota.MaxLength = 20;
            this.textNumeroNota.Name = "textNumeroNota";
            this.textNumeroNota.Size = new System.Drawing.Size(233, 21);
            this.textNumeroNota.TabIndex = 1;
            this.textNumeroNota.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.textNumeroNota_KeyPress);
            // 
            // grbNotas
            // 
            this.grbNotas.Controls.Add(this.dgvNotaFiscal);
            this.grbNotas.Location = new System.Drawing.Point(3, 73);
            this.grbNotas.Name = "grbNotas";
            this.grbNotas.Size = new System.Drawing.Size(758, 292);
            this.grbNotas.TabIndex = 15;
            this.grbNotas.TabStop = false;
            this.grbNotas.Text = "Notas";
            // 
            // dgvNotaFiscal
            // 
            this.dgvNotaFiscal.AllowUserToAddRows = false;
            this.dgvNotaFiscal.AllowUserToDeleteRows = false;
            this.dgvNotaFiscal.AllowUserToOrderColumns = true;
            this.dgvNotaFiscal.AllowUserToResizeRows = false;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.Gainsboro;
            this.dgvNotaFiscal.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle1;
            this.dgvNotaFiscal.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.DisplayedCells;
            this.dgvNotaFiscal.BackgroundColor = System.Drawing.Color.White;
            this.dgvNotaFiscal.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.Color.LightGray;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvNotaFiscal.DefaultCellStyle = dataGridViewCellStyle2;
            this.dgvNotaFiscal.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvNotaFiscal.Location = new System.Drawing.Point(3, 16);
            this.dgvNotaFiscal.MultiSelect = false;
            this.dgvNotaFiscal.Name = "dgvNotaFiscal";
            this.dgvNotaFiscal.ReadOnly = true;
            this.dgvNotaFiscal.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvNotaFiscal.Size = new System.Drawing.Size(752, 273);
            this.dgvNotaFiscal.TabIndex = 0;
            this.dgvNotaFiscal.RowPrePaint += new System.Windows.Forms.DataGridViewRowPrePaintEventHandler(this.dgNotaFiscal_RowPrePaint);
            // 
            // gbrTotais
            // 
            this.gbrTotais.Controls.Add(this.textValorLiquido);
            this.gbrTotais.Controls.Add(this.lblValorLiquido);
            this.gbrTotais.Controls.Add(this.textQuantidade);
            this.gbrTotais.Controls.Add(this.lblQuantidade);
            this.gbrTotais.Controls.Add(this.lblValorNF);
            this.gbrTotais.Controls.Add(this.text_valorTotal);
            this.gbrTotais.Location = new System.Drawing.Point(3, 368);
            this.gbrTotais.Name = "gbrTotais";
            this.gbrTotais.Size = new System.Drawing.Size(758, 88);
            this.gbrTotais.TabIndex = 16;
            this.gbrTotais.TabStop = false;
            this.gbrTotais.Text = "Totais da pesquisa";
            // 
            // textValorLiquido
            // 
            this.textValorLiquido.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.textValorLiquido.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textValorLiquido.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textValorLiquido.ForeColor = System.Drawing.Color.Blue;
            this.textValorLiquido.Location = new System.Drawing.Point(124, 55);
            this.textValorLiquido.MaxLength = 255;
            this.textValorLiquido.Name = "textValorLiquido";
            this.textValorLiquido.ReadOnly = true;
            this.textValorLiquido.Size = new System.Drawing.Size(127, 21);
            this.textValorLiquido.TabIndex = 44;
            this.textValorLiquido.TabStop = false;
            this.textValorLiquido.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // lblValorLiquido
            // 
            this.lblValorLiquido.BackColor = System.Drawing.Color.LightGray;
            this.lblValorLiquido.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblValorLiquido.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblValorLiquido.Location = new System.Drawing.Point(6, 55);
            this.lblValorLiquido.Name = "lblValorLiquido";
            this.lblValorLiquido.Size = new System.Drawing.Size(119, 21);
            this.lblValorLiquido.TabIndex = 43;
            this.lblValorLiquido.TabStop = false;
            this.lblValorLiquido.Text = "Valor Líquido (R$)";
            // 
            // textQuantidade
            // 
            this.textQuantidade.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.textQuantidade.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textQuantidade.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textQuantidade.ForeColor = System.Drawing.Color.Blue;
            this.textQuantidade.Location = new System.Drawing.Point(124, 15);
            this.textQuantidade.MaxLength = 10;
            this.textQuantidade.Name = "textQuantidade";
            this.textQuantidade.ReadOnly = true;
            this.textQuantidade.Size = new System.Drawing.Size(127, 21);
            this.textQuantidade.TabIndex = 42;
            this.textQuantidade.TabStop = false;
            this.textQuantidade.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // lblQuantidade
            // 
            this.lblQuantidade.BackColor = System.Drawing.Color.LightGray;
            this.lblQuantidade.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblQuantidade.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblQuantidade.Location = new System.Drawing.Point(6, 15);
            this.lblQuantidade.Name = "lblQuantidade";
            this.lblQuantidade.Size = new System.Drawing.Size(119, 21);
            this.lblQuantidade.TabIndex = 41;
            this.lblQuantidade.TabStop = false;
            this.lblQuantidade.Text = "Quantidade NF";
            // 
            // lblValorNF
            // 
            this.lblValorNF.BackColor = System.Drawing.Color.LightGray;
            this.lblValorNF.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblValorNF.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblValorNF.Location = new System.Drawing.Point(6, 35);
            this.lblValorNF.Name = "lblValorNF";
            this.lblValorNF.Size = new System.Drawing.Size(119, 21);
            this.lblValorNF.TabIndex = 40;
            this.lblValorNF.TabStop = false;
            this.lblValorNF.Text = "Valor Total NF (R$)";
            // 
            // text_valorTotal
            // 
            this.text_valorTotal.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.text_valorTotal.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.text_valorTotal.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.text_valorTotal.ForeColor = System.Drawing.Color.Blue;
            this.text_valorTotal.Location = new System.Drawing.Point(124, 35);
            this.text_valorTotal.MaxLength = 255;
            this.text_valorTotal.Name = "text_valorTotal";
            this.text_valorTotal.ReadOnly = true;
            this.text_valorTotal.Size = new System.Drawing.Size(127, 21);
            this.text_valorTotal.TabIndex = 39;
            this.text_valorTotal.TabStop = false;
            this.text_valorTotal.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // frmNotaFiscalPrincipal
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(784, 561);
            this.Name = "frmNotaFiscalPrincipal";
            this.Text = "GERENCIAR NOTAS FISCAIS";
            this.spForm.Panel1.ResumeLayout(false);
            this.spForm.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.spForm)).EndInit();
            this.spForm.ResumeLayout(false);
            this.pnlForm.ResumeLayout(false);
            this.pnlForm.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.banner)).EndInit();
            this.flpAcao.ResumeLayout(false);
            this.grbNotas.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvNotaFiscal)).EndInit();
            this.gbrTotais.ResumeLayout(false);
            this.gbrTotais.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.FlowLayoutPanel flpAcao;
        private System.Windows.Forms.Button btnPesquisar;
        private System.Windows.Forms.Button btnDetalhar;
        private System.Windows.Forms.Button btnCancelar;
        private System.Windows.Forms.Button btnImprimir;
        private System.Windows.Forms.Button btnFechar;
        private System.Windows.Forms.TextBox lblSituacao;
        private System.Windows.Forms.TextBox lblEmpresa;
        private System.Windows.Forms.TextBox lblCliente;
        private System.Windows.Forms.TextBox lblNumeroNota;
        private System.Windows.Forms.Button btnExcluirCliente;
        private System.Windows.Forms.Button btnExcluirEmpresa;
        private System.Windows.Forms.Button btnCliente;
        private System.Windows.Forms.Button btnEmpresa;
        private System.Windows.Forms.TextBox textCliente;
        private ComboBoxWithBorder cbSituacao;
        private System.Windows.Forms.TextBox textEmpresa;
        private System.Windows.Forms.TextBox lblDataCancelamento;
        private System.Windows.Forms.TextBox lblDataEmissao;
        private System.Windows.Forms.DateTimePicker dataEmissaoFinal;
        private System.Windows.Forms.DateTimePicker dataEmissaoInicial;
        private System.Windows.Forms.DateTimePicker dataCancelamentoFinal;
        private System.Windows.Forms.DateTimePicker dataCancelamentoInicial;
        private System.Windows.Forms.TextBox textNumeroNota;
        private System.Windows.Forms.GroupBox grbNotas;
        private System.Windows.Forms.DataGridView dgvNotaFiscal;
        private System.Windows.Forms.GroupBox gbrTotais;
        private System.Windows.Forms.TextBox textQuantidade;
        private System.Windows.Forms.TextBox lblQuantidade;
        private System.Windows.Forms.TextBox lblValorNF;
        private System.Windows.Forms.TextBox text_valorTotal;
        private System.Windows.Forms.TextBox textValorLiquido;
        private System.Windows.Forms.TextBox lblValorLiquido;
    }
}