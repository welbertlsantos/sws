﻿namespace SegSis.View
{
    partial class frm_FuncaoComentarioBuscar
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frm_FuncaoComentarioBuscar));
            this.grbAtividade = new System.Windows.Forms.GroupBox();
            this.dgAtividade = new System.Windows.Forms.DataGridView();
            this.grbAcao = new System.Windows.Forms.GroupBox();
            this.btnFechar = new System.Windows.Forms.Button();
            this.btnNovo = new System.Windows.Forms.Button();
            this.btnGravar = new System.Windows.Forms.Button();
            this.grbAtividade.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgAtividade)).BeginInit();
            this.grbAcao.SuspendLayout();
            this.SuspendLayout();
            // 
            // grbAtividade
            // 
            this.grbAtividade.Controls.Add(this.dgAtividade);
            this.grbAtividade.Location = new System.Drawing.Point(12, 13);
            this.grbAtividade.Name = "grbAtividade";
            this.grbAtividade.Size = new System.Drawing.Size(576, 309);
            this.grbAtividade.TabIndex = 0;
            this.grbAtividade.TabStop = false;
            this.grbAtividade.Text = "Atividade";
            // 
            // dgAtividade
            // 
            this.dgAtividade.AllowUserToAddRows = false;
            this.dgAtividade.AllowUserToDeleteRows = false;
            this.dgAtividade.AllowUserToOrderColumns = true;
            this.dgAtividade.AllowUserToResizeRows = false;
            this.dgAtividade.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.DisplayedCells;
            this.dgAtividade.BackgroundColor = System.Drawing.Color.White;
            this.dgAtividade.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.Silver;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgAtividade.DefaultCellStyle = dataGridViewCellStyle1;
            this.dgAtividade.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgAtividade.Location = new System.Drawing.Point(3, 16);
            this.dgAtividade.MultiSelect = false;
            this.dgAtividade.Name = "dgAtividade";
            this.dgAtividade.ReadOnly = true;
            this.dgAtividade.Size = new System.Drawing.Size(570, 290);
            this.dgAtividade.TabIndex = 0;
            // 
            // grbAcao
            // 
            this.grbAcao.Controls.Add(this.btnFechar);
            this.grbAcao.Controls.Add(this.btnNovo);
            this.grbAcao.Controls.Add(this.btnGravar);
            this.grbAcao.Location = new System.Drawing.Point(12, 340);
            this.grbAcao.Name = "grbAcao";
            this.grbAcao.Size = new System.Drawing.Size(576, 48);
            this.grbAcao.TabIndex = 1;
            this.grbAcao.TabStop = false;
            this.grbAcao.Text = "Ação";
            // 
            // btnFechar
            // 
            this.btnFechar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnFechar.Image = global::SegSis.Properties.Resources.close;
            this.btnFechar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnFechar.Location = new System.Drawing.Point(169, 17);
            this.btnFechar.Name = "btnFechar";
            this.btnFechar.Size = new System.Drawing.Size(75, 23);
            this.btnFechar.TabIndex = 2;
            this.btnFechar.Text = "&Fechar";
            this.btnFechar.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnFechar.UseVisualStyleBackColor = true;
            this.btnFechar.Click += new System.EventHandler(this.btnFechar_Click);
            // 
            // btnNovo
            // 
            this.btnNovo.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnNovo.Image = global::SegSis.Properties.Resources.icone_mais;
            this.btnNovo.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnNovo.Location = new System.Drawing.Point(88, 17);
            this.btnNovo.Name = "btnNovo";
            this.btnNovo.Size = new System.Drawing.Size(75, 23);
            this.btnNovo.TabIndex = 1;
            this.btnNovo.Text = "&Novo";
            this.btnNovo.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnNovo.UseVisualStyleBackColor = true;
            this.btnNovo.Click += new System.EventHandler(this.btnNovo_Click);
            // 
            // btnGravar
            // 
            this.btnGravar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnGravar.Image = global::SegSis.Properties.Resources.Gravar;
            this.btnGravar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnGravar.Location = new System.Drawing.Point(7, 17);
            this.btnGravar.Name = "btnGravar";
            this.btnGravar.Size = new System.Drawing.Size(75, 23);
            this.btnGravar.TabIndex = 0;
            this.btnGravar.Text = "&Gravar";
            this.btnGravar.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnGravar.UseVisualStyleBackColor = true;
            this.btnGravar.Click += new System.EventHandler(this.btnGravar_Click);
            // 
            // frm_FuncaoComentarioBuscar
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(600, 400);
            this.Controls.Add(this.grbAcao);
            this.Controls.Add(this.grbAtividade);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "frm_FuncaoComentarioBuscar";
            this.Text = "CONSULTAR ATIVIDADES";
            this.grbAtividade.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgAtividade)).EndInit();
            this.grbAcao.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox grbAtividade;
        private System.Windows.Forms.DataGridView dgAtividade;
        private System.Windows.Forms.GroupBox grbAcao;
        private System.Windows.Forms.Button btnFechar;
        private System.Windows.Forms.Button btnNovo;
        private System.Windows.Forms.Button btnGravar;
    }
}