﻿using SWS.Entidade;
using SWS.Facade;
using SWS.View.Resources;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using SWS.ViewHelper;
using SWS.Resources;
using SWS.View.ViewHelper;

namespace SWS.View
{
    public partial class frmClienteFuncaoPrincipal : frmTemplate
    {
        private Cliente cliente;

        public Cliente Cliente
        {
            get { return cliente; }
            set { cliente = value; }
        }
        
        public frmClienteFuncaoPrincipal()
        {
            InitializeComponent();
            ActiveControl = btnCliente;
            validaPermissoes();
        }

        private void btn_incluir_Click(object sender, EventArgs e)
        {
            try
            {
                if (cliente == null)
                    throw new Exception("Selecione o cliente primeiro.");

                frmFuncaoPesquisa incluirFuncao = new frmFuncaoPesquisa();
                incluirFuncao.ShowDialog();

                if (incluirFuncao.Funcoes.Count > 0)
                {
                    /* verificando se existe alguma função na coleção que já esteja presente no cadastro do cliente */

                    foreach (DataGridViewRow dvRow in dgvFuncao.Rows)
                        if (incluirFuncao.Funcoes.Exists(x => x.Id == (long)dvRow.Cells[2].Value))
                            throw new Exception("Já existe a função : " + dvRow.Cells[3].Value.ToString() + " Cadastrada no cliente. Refaça a sua seleção");

                    EstudoFacade pcmsoFacade = EstudoFacade.getInstance();
                    incluirFuncao.Funcoes.ForEach(delegate(Funcao funcao)
                    {
                        pcmsoFacade.insertClienteFuncao(new ClienteFuncao(null, cliente, funcao, ApplicationConstants.ATIVO, DateTime.Now, null));
                    });
                    
                    MessageBox.Show("Função incluída no cliente com sucesso.", "Confirmação", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    montaGrid();
                }


            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }

        private void btn_excluir_Click(object sender, EventArgs e)
        {
            try
            {
                if (dgvFuncao.CurrentRow == null)
                    throw new Exception("Selecione uma linha.");

                if (MessageBox.Show("Deseja excluir a função selecionada?", "Atenção", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                {
                    EstudoFacade pcmsoFacade = EstudoFacade.getInstance();
                    FuncionarioFacade funcionarioFacade = FuncionarioFacade.getInstance();

                    ClienteFuncao clienteFuncaoExcluir = pcmsoFacade.findClienteFuncaoById((long)dgvFuncao.CurrentRow.Cells[0].Value);

                    /* verificar se o função está sendo usada por algum funcionário e informar 
                     * os funcionários que estão ligados a essa função */

                    List<ClienteFuncaoFuncionario> listClienteFuncaoFuncionario = funcionarioFacade.findAllByClienteFuncao(clienteFuncaoExcluir);

                    if (listClienteFuncaoFuncionario.Count > 0)
                    {
                        StringBuilder funcionarios = new StringBuilder();

                        foreach (ClienteFuncaoFuncionario clienteFuncaoFuncionario in listClienteFuncaoFuncionario)
                        {
                            funcionarios.Append(clienteFuncaoFuncionario.Funcionario.Nome + " [ " + ValidaCampoHelper.FormataCpf(clienteFuncaoFuncionario.Funcionario.Cpf) + " ]" + System.Environment.NewLine);
                        }

                        throw new Exception("Não é permitido excluir a função com funcionario ativo na função. Resolva esse problema primeiro. " + System.Environment.NewLine + System.Environment.NewLine + funcionarios.ToString());

                    }

                    clienteFuncaoExcluir.Situacao = ApplicationConstants.DESATIVADO;
                    clienteFuncaoExcluir.DataExclusao = new DateTime();
                    pcmsoFacade.updateClienteFuncao(clienteFuncaoExcluir);
                    MessageBox.Show("Funçaõ excluída com cliente com sucesso.", "Confirmação", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    montaGrid();
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btn_fechar_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void validaPermissoes()
        {
            PermissionamentoFacade permissionamentoFacade = PermissionamentoFacade.getInstance();

            this.btnIncluir.Enabled = permissionamentoFacade.hasPermission("FUNCAOCLIENTE", "INCLUIR");
            this.btnExcluir.Enabled = permissionamentoFacade.hasPermission("FUNCAOCLIENTE", "EXCLUIR");
        }

        private void btnCliente_Click(object sender, EventArgs e)
        {
            try
            {
                frmClienteSelecionar formCliente = new frmClienteSelecionar(null, null, false, null, null, null, null, null, true);
                formCliente.ShowDialog();

                if (formCliente.Cliente != null)
                {
                    cliente = formCliente.Cliente;
                    textCliente.Text = cliente.RazaoSocial;
                    montaGrid();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }

        public void montaGrid()
        {
            try
            {
                EstudoFacade pcmsoFacade = EstudoFacade.getInstance();
                this.Cursor = Cursors.WaitCursor;

                DataSet ds = pcmsoFacade.findAllClienteFuncao(cliente);

                dgvFuncao.Columns.Clear();

                dgvFuncao.DataSource = ds.Tables["Funcoes"].DefaultView;

                dgvFuncao.Columns[0].HeaderText = "id_seg_cliente_funcao";
                dgvFuncao.Columns[0].Visible = false;

                dgvFuncao.Columns[1].HeaderText = "id_cliente";
                dgvFuncao.Columns[1].Visible = false;

                dgvFuncao.Columns[2].HeaderText = "id_funcao";
                dgvFuncao.Columns[2].Visible = false;

                dgvFuncao.Columns[3].HeaderText = "Descrição";

                dgvFuncao.Columns[4].HeaderText = "CBO";

            }
            catch (Exception Ex)
            {
                MessageBox.Show(Ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }


        }
    }
}
