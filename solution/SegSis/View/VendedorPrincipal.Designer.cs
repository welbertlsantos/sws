﻿namespace SWS.View
{
    partial class frmVendedorPrincipal
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            this.flpAcao = new System.Windows.Forms.FlowLayoutPanel();
            this.btnIncluir = new System.Windows.Forms.Button();
            this.btnAlterar = new System.Windows.Forms.Button();
            this.btnExcluir = new System.Windows.Forms.Button();
            this.btnPesquisar = new System.Windows.Forms.Button();
            this.btnReativar = new System.Windows.Forms.Button();
            this.btnDetalhar = new System.Windows.Forms.Button();
            this.btnFechar = new System.Windows.Forms.Button();
            this.textNome = new System.Windows.Forms.TextBox();
            this.lblNome = new System.Windows.Forms.TextBox();
            this.textCpf = new System.Windows.Forms.MaskedTextBox();
            this.lblCPF = new System.Windows.Forms.TextBox();
            this.lblSituacao = new System.Windows.Forms.TextBox();
            this.cbSituacao = new SWS.ComboBoxWithBorder();
            this.lblUf = new System.Windows.Forms.TextBox();
            this.cbUf = new SWS.ComboBoxWithBorder();
            this.lblCidade = new System.Windows.Forms.TextBox();
            this.btnExcluirCidade = new System.Windows.Forms.Button();
            this.btnCidade = new System.Windows.Forms.Button();
            this.textCidade = new System.Windows.Forms.TextBox();
            this.grbGride = new System.Windows.Forms.GroupBox();
            this.dgvVendedores = new System.Windows.Forms.DataGridView();
            ((System.ComponentModel.ISupportInitialize)(this.spForm)).BeginInit();
            this.spForm.Panel1.SuspendLayout();
            this.spForm.Panel2.SuspendLayout();
            this.spForm.SuspendLayout();
            this.pnlForm.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.banner)).BeginInit();
            this.flpAcao.SuspendLayout();
            this.grbGride.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvVendedores)).BeginInit();
            this.SuspendLayout();
            // 
            // spForm
            // 
            // 
            // spForm.Panel1
            // 
            this.spForm.Panel1.Controls.Add(this.flpAcao);
            // 
            // pnlForm
            // 
            this.pnlForm.Controls.Add(this.grbGride);
            this.pnlForm.Controls.Add(this.btnExcluirCidade);
            this.pnlForm.Controls.Add(this.btnCidade);
            this.pnlForm.Controls.Add(this.textCidade);
            this.pnlForm.Controls.Add(this.lblCidade);
            this.pnlForm.Controls.Add(this.cbUf);
            this.pnlForm.Controls.Add(this.lblUf);
            this.pnlForm.Controls.Add(this.cbSituacao);
            this.pnlForm.Controls.Add(this.lblSituacao);
            this.pnlForm.Controls.Add(this.textCpf);
            this.pnlForm.Controls.Add(this.lblCPF);
            this.pnlForm.Controls.Add(this.textNome);
            this.pnlForm.Controls.Add(this.lblNome);
            // 
            // flpAcao
            // 
            this.flpAcao.Controls.Add(this.btnIncluir);
            this.flpAcao.Controls.Add(this.btnAlterar);
            this.flpAcao.Controls.Add(this.btnExcluir);
            this.flpAcao.Controls.Add(this.btnPesquisar);
            this.flpAcao.Controls.Add(this.btnReativar);
            this.flpAcao.Controls.Add(this.btnDetalhar);
            this.flpAcao.Controls.Add(this.btnFechar);
            this.flpAcao.Dock = System.Windows.Forms.DockStyle.Fill;
            this.flpAcao.Location = new System.Drawing.Point(0, 0);
            this.flpAcao.Name = "flpAcao";
            this.flpAcao.Size = new System.Drawing.Size(784, 35);
            this.flpAcao.TabIndex = 1;
            // 
            // btnIncluir
            // 
            this.btnIncluir.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnIncluir.Image = global::SWS.Properties.Resources.icone_mais;
            this.btnIncluir.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnIncluir.Location = new System.Drawing.Point(3, 3);
            this.btnIncluir.Name = "btnIncluir";
            this.btnIncluir.Size = new System.Drawing.Size(71, 22);
            this.btnIncluir.TabIndex = 1;
            this.btnIncluir.Text = "&Incluir";
            this.btnIncluir.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnIncluir.UseVisualStyleBackColor = true;
            this.btnIncluir.Click += new System.EventHandler(this.btnIncluir_Click);
            // 
            // btnAlterar
            // 
            this.btnAlterar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnAlterar.Image = global::SWS.Properties.Resources.Alterar;
            this.btnAlterar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnAlterar.Location = new System.Drawing.Point(80, 3);
            this.btnAlterar.Name = "btnAlterar";
            this.btnAlterar.Size = new System.Drawing.Size(71, 22);
            this.btnAlterar.TabIndex = 2;
            this.btnAlterar.Text = "&Alterar";
            this.btnAlterar.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnAlterar.UseVisualStyleBackColor = true;
            this.btnAlterar.Click += new System.EventHandler(this.btnAlterar_Click);
            // 
            // btnExcluir
            // 
            this.btnExcluir.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnExcluir.Image = global::SWS.Properties.Resources.lixeira;
            this.btnExcluir.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnExcluir.Location = new System.Drawing.Point(157, 3);
            this.btnExcluir.Name = "btnExcluir";
            this.btnExcluir.Size = new System.Drawing.Size(71, 22);
            this.btnExcluir.TabIndex = 3;
            this.btnExcluir.Text = "&Excluir";
            this.btnExcluir.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnExcluir.UseVisualStyleBackColor = true;
            this.btnExcluir.Click += new System.EventHandler(this.btnExcluir_Click);
            // 
            // btnPesquisar
            // 
            this.btnPesquisar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnPesquisar.Image = global::SWS.Properties.Resources.lupa;
            this.btnPesquisar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnPesquisar.Location = new System.Drawing.Point(234, 3);
            this.btnPesquisar.Name = "btnPesquisar";
            this.btnPesquisar.Size = new System.Drawing.Size(71, 22);
            this.btnPesquisar.TabIndex = 4;
            this.btnPesquisar.Text = "Pesquisar";
            this.btnPesquisar.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnPesquisar.UseVisualStyleBackColor = true;
            this.btnPesquisar.Click += new System.EventHandler(this.btnPesquisar_Click);
            // 
            // btnReativar
            // 
            this.btnReativar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnReativar.Image = global::SWS.Properties.Resources.devolucao;
            this.btnReativar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnReativar.Location = new System.Drawing.Point(311, 3);
            this.btnReativar.Name = "btnReativar";
            this.btnReativar.Size = new System.Drawing.Size(71, 22);
            this.btnReativar.TabIndex = 5;
            this.btnReativar.Text = "&Reativar";
            this.btnReativar.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnReativar.UseVisualStyleBackColor = true;
            this.btnReativar.Click += new System.EventHandler(this.btnReativar_Click);
            // 
            // btnDetalhar
            // 
            this.btnDetalhar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnDetalhar.Image = global::SWS.Properties.Resources.lupa;
            this.btnDetalhar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnDetalhar.Location = new System.Drawing.Point(388, 3);
            this.btnDetalhar.Name = "btnDetalhar";
            this.btnDetalhar.Size = new System.Drawing.Size(71, 22);
            this.btnDetalhar.TabIndex = 6;
            this.btnDetalhar.Text = "&Detalhar";
            this.btnDetalhar.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnDetalhar.UseVisualStyleBackColor = true;
            this.btnDetalhar.Click += new System.EventHandler(this.btnDetalhar_Click);
            // 
            // btnFechar
            // 
            this.btnFechar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnFechar.Image = global::SWS.Properties.Resources.close;
            this.btnFechar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnFechar.Location = new System.Drawing.Point(465, 3);
            this.btnFechar.Name = "btnFechar";
            this.btnFechar.Size = new System.Drawing.Size(71, 22);
            this.btnFechar.TabIndex = 7;
            this.btnFechar.Text = "&Fechar";
            this.btnFechar.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnFechar.UseVisualStyleBackColor = true;
            this.btnFechar.Click += new System.EventHandler(this.btnFechar_Click);
            // 
            // textNome
            // 
            this.textNome.BackColor = System.Drawing.Color.White;
            this.textNome.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textNome.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.textNome.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textNome.Location = new System.Drawing.Point(140, 3);
            this.textNome.MaxLength = 15;
            this.textNome.Name = "textNome";
            this.textNome.Size = new System.Drawing.Size(621, 21);
            this.textNome.TabIndex = 1;
            // 
            // lblNome
            // 
            this.lblNome.BackColor = System.Drawing.Color.LightGray;
            this.lblNome.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblNome.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNome.Location = new System.Drawing.Point(3, 3);
            this.lblNome.Name = "lblNome";
            this.lblNome.ReadOnly = true;
            this.lblNome.Size = new System.Drawing.Size(138, 21);
            this.lblNome.TabIndex = 4;
            this.lblNome.TabStop = false;
            this.lblNome.Text = "Nome";
            // 
            // textCpf
            // 
            this.textCpf.BackColor = System.Drawing.Color.White;
            this.textCpf.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textCpf.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textCpf.Location = new System.Drawing.Point(140, 23);
            this.textCpf.Mask = "999,999,999-99";
            this.textCpf.Name = "textCpf";
            this.textCpf.PromptChar = ' ';
            this.textCpf.Size = new System.Drawing.Size(621, 21);
            this.textCpf.TabIndex = 2;
            // 
            // lblCPF
            // 
            this.lblCPF.BackColor = System.Drawing.Color.LightGray;
            this.lblCPF.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblCPF.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCPF.Location = new System.Drawing.Point(3, 23);
            this.lblCPF.Name = "lblCPF";
            this.lblCPF.ReadOnly = true;
            this.lblCPF.Size = new System.Drawing.Size(138, 21);
            this.lblCPF.TabIndex = 6;
            this.lblCPF.TabStop = false;
            this.lblCPF.Text = "CPF";
            // 
            // lblSituacao
            // 
            this.lblSituacao.BackColor = System.Drawing.Color.LightGray;
            this.lblSituacao.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblSituacao.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSituacao.Location = new System.Drawing.Point(3, 43);
            this.lblSituacao.Name = "lblSituacao";
            this.lblSituacao.ReadOnly = true;
            this.lblSituacao.Size = new System.Drawing.Size(138, 21);
            this.lblSituacao.TabIndex = 8;
            this.lblSituacao.TabStop = false;
            this.lblSituacao.Text = "Situação";
            // 
            // cbSituacao
            // 
            this.cbSituacao.BackColor = System.Drawing.Color.LightGray;
            this.cbSituacao.BorderColor = System.Drawing.Color.DimGray;
            this.cbSituacao.BorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid;
            this.cbSituacao.FormattingEnabled = true;
            this.cbSituacao.Location = new System.Drawing.Point(140, 43);
            this.cbSituacao.Name = "cbSituacao";
            this.cbSituacao.Size = new System.Drawing.Size(621, 21);
            this.cbSituacao.TabIndex = 3;
            // 
            // lblUf
            // 
            this.lblUf.BackColor = System.Drawing.Color.LightGray;
            this.lblUf.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblUf.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblUf.Location = new System.Drawing.Point(3, 63);
            this.lblUf.Name = "lblUf";
            this.lblUf.ReadOnly = true;
            this.lblUf.Size = new System.Drawing.Size(138, 21);
            this.lblUf.TabIndex = 10;
            this.lblUf.TabStop = false;
            this.lblUf.Text = "UF";
            // 
            // cbUf
            // 
            this.cbUf.BackColor = System.Drawing.Color.LightGray;
            this.cbUf.BorderColor = System.Drawing.Color.DimGray;
            this.cbUf.BorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid;
            this.cbUf.FormattingEnabled = true;
            this.cbUf.Location = new System.Drawing.Point(140, 63);
            this.cbUf.Name = "cbUf";
            this.cbUf.Size = new System.Drawing.Size(621, 21);
            this.cbUf.TabIndex = 4;
            // 
            // lblCidade
            // 
            this.lblCidade.BackColor = System.Drawing.Color.LightGray;
            this.lblCidade.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblCidade.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCidade.Location = new System.Drawing.Point(3, 83);
            this.lblCidade.Name = "lblCidade";
            this.lblCidade.ReadOnly = true;
            this.lblCidade.Size = new System.Drawing.Size(138, 21);
            this.lblCidade.TabIndex = 12;
            this.lblCidade.TabStop = false;
            this.lblCidade.Text = "Cidade";
            // 
            // btnExcluirCidade
            // 
            this.btnExcluirCidade.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnExcluirCidade.Image = global::SWS.Properties.Resources.close;
            this.btnExcluirCidade.Location = new System.Drawing.Point(732, 83);
            this.btnExcluirCidade.Name = "btnExcluirCidade";
            this.btnExcluirCidade.Size = new System.Drawing.Size(30, 21);
            this.btnExcluirCidade.TabIndex = 39;
            this.btnExcluirCidade.TabStop = false;
            this.btnExcluirCidade.UseVisualStyleBackColor = true;
            this.btnExcluirCidade.Click += new System.EventHandler(this.btnExcluirCidade_Click);
            // 
            // btnCidade
            // 
            this.btnCidade.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnCidade.Image = global::SWS.Properties.Resources.busca;
            this.btnCidade.Location = new System.Drawing.Point(703, 83);
            this.btnCidade.Name = "btnCidade";
            this.btnCidade.Size = new System.Drawing.Size(30, 21);
            this.btnCidade.TabIndex = 5;
            this.btnCidade.UseVisualStyleBackColor = true;
            this.btnCidade.Click += new System.EventHandler(this.btnCidade_Click);
            // 
            // textCidade
            // 
            this.textCidade.BackColor = System.Drawing.SystemColors.Control;
            this.textCidade.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textCidade.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textCidade.Location = new System.Drawing.Point(140, 83);
            this.textCidade.MaxLength = 100;
            this.textCidade.Name = "textCidade";
            this.textCidade.ReadOnly = true;
            this.textCidade.Size = new System.Drawing.Size(567, 21);
            this.textCidade.TabIndex = 38;
            this.textCidade.TabStop = false;
            // 
            // grbGride
            // 
            this.grbGride.BackColor = System.Drawing.Color.White;
            this.grbGride.Controls.Add(this.dgvVendedores);
            this.grbGride.Location = new System.Drawing.Point(3, 110);
            this.grbGride.Name = "grbGride";
            this.grbGride.Size = new System.Drawing.Size(758, 342);
            this.grbGride.TabIndex = 40;
            this.grbGride.TabStop = false;
            this.grbGride.Text = "Vendedores";
            // 
            // dgvVendedores
            // 
            this.dgvVendedores.AllowUserToAddRows = false;
            this.dgvVendedores.AllowUserToDeleteRows = false;
            this.dgvVendedores.AllowUserToOrderColumns = true;
            this.dgvVendedores.AllowUserToResizeRows = false;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.Gainsboro;
            this.dgvVendedores.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle1;
            this.dgvVendedores.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this.dgvVendedores.BackgroundColor = System.Drawing.Color.White;
            this.dgvVendedores.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.Color.LightGray;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvVendedores.DefaultCellStyle = dataGridViewCellStyle2;
            this.dgvVendedores.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvVendedores.Location = new System.Drawing.Point(3, 16);
            this.dgvVendedores.MultiSelect = false;
            this.dgvVendedores.Name = "dgvVendedores";
            this.dgvVendedores.ReadOnly = true;
            this.dgvVendedores.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvVendedores.Size = new System.Drawing.Size(752, 323);
            this.dgvVendedores.TabIndex = 6;
            this.dgvVendedores.RowPrePaint += new System.Windows.Forms.DataGridViewRowPrePaintEventHandler(this.dgvVendedores_RowPrePaint);
            this.dgvVendedores.DoubleClick += new System.EventHandler(this.dgvVendedores_DoubleClick);
            // 
            // frmVendedorPrincipal
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(784, 561);
            this.Name = "frmVendedorPrincipal";
            this.Text = "GERENCIAR VENDEDOR";
            this.spForm.Panel1.ResumeLayout(false);
            this.spForm.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.spForm)).EndInit();
            this.spForm.ResumeLayout(false);
            this.pnlForm.ResumeLayout(false);
            this.pnlForm.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.banner)).EndInit();
            this.flpAcao.ResumeLayout(false);
            this.grbGride.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvVendedores)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.FlowLayoutPanel flpAcao;
        private System.Windows.Forms.Button btnIncluir;
        private System.Windows.Forms.Button btnAlterar;
        private System.Windows.Forms.Button btnExcluir;
        private System.Windows.Forms.Button btnReativar;
        private System.Windows.Forms.Button btnDetalhar;
        private System.Windows.Forms.Button btnFechar;
        private System.Windows.Forms.Button btnPesquisar;
        private System.Windows.Forms.TextBox textNome;
        private System.Windows.Forms.TextBox lblNome;
        private System.Windows.Forms.TextBox lblSituacao;
        private System.Windows.Forms.MaskedTextBox textCpf;
        private System.Windows.Forms.TextBox lblCPF;
        private System.Windows.Forms.TextBox lblCidade;
        private ComboBoxWithBorder cbUf;
        private System.Windows.Forms.TextBox lblUf;
        private ComboBoxWithBorder cbSituacao;
        private System.Windows.Forms.Button btnExcluirCidade;
        private System.Windows.Forms.Button btnCidade;
        public System.Windows.Forms.TextBox textCidade;
        private System.Windows.Forms.GroupBox grbGride;
        private System.Windows.Forms.DataGridView dgvVendedores;
    }
}