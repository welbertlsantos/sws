﻿using SWS.Entidade;
using SWS.Facade;
using SWS.View.Resources;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace SWS.View
{
    public partial class frmAgentesBuscar : frmTemplateConsulta
    {
        private Agente agente;

        public Agente Agente
        {
            get { return agente; }
            set { agente = value; }
        }
        
        public frmAgentesBuscar()
        {
            InitializeComponent();
            validaPermissao();
            ActiveControl = textAgente;
        }

        private void validaPermissao()
        {
            PermissionamentoFacade permissionamento = PermissionamentoFacade.getInstance();
            btnNovo.Enabled = permissionamento.hasPermission("AGENTE", "INCLUIR");
            
        }

        private void btnConfirmar_Click(object sender, EventArgs e)
        {
            try
            {
                if (dgvAgente.CurrentRow == null)
                    throw new Exception("Selecione uma linha");

                EstudoFacade pcmsoFacade = EstudoFacade.getInstance();
                Agente = pcmsoFacade.findAgenteById((long)dgvAgente.CurrentRow.Cells[0].Value);
                this.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }


        }

        private void btnNovo_Click(object sender, EventArgs e)
        {
            try
            {
                frmAgentesIncluir incluirAgentes = new frmAgentesIncluir();
                incluirAgentes.ShowDialog();

                if (incluirAgentes.Agente != null)
                {
                    Agente = incluirAgentes.Agente;
                    this.Close();
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }

        private void btnFechar_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnAgente_Click(object sender, EventArgs e)
        {
            try
            {
                montaDataGrid(new Agente(null, null, textAgente.Text.Trim(), string.Empty, string.Empty, string.Empty
                    , ApplicationConstants.ATIVO, false, string.Empty, string.Empty, string.Empty));
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }


        }

        private void montaDataGrid(Agente agente)
        {
            try
            {
                EstudoFacade pcmsoFacade = EstudoFacade.getInstance();
                dgvAgente.Columns.Clear();
                
                DataSet ds = pcmsoFacade.findAgenteByFilter(agente);
                dgvAgente.DataSource = ds.Tables[0].DefaultView;

                dgvAgente.Columns[0].HeaderText = "idAgente";
                dgvAgente.Columns[0].Visible = false;

                dgvAgente.Columns[1].HeaderText = "idRisco";
                dgvAgente.Columns[1].Visible = false;

                dgvAgente.Columns[2].HeaderText = "Agente";
                dgvAgente.Columns[2].Width = 300;

                dgvAgente.Columns[3].HeaderText = "Trajetória";
                dgvAgente.Columns[3].Visible = false;

                dgvAgente.Columns[4].HeaderText = "Danos";
                dgvAgente.Columns[4].Visible = false;

                dgvAgente.Columns[5].HeaderText = "Limite";
                dgvAgente.Columns[5].Visible = false;

                dgvAgente.Columns[6].HeaderText = "Situação";
                dgvAgente.Columns[6].Visible = false;

                dgvAgente.Columns[7].HeaderText = "Risco";
                dgvAgente.Columns[7].Width = 100;

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void frmAgentesBuscar_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter || e.KeyCode == Keys.Return)
                SendKeys.Send("{TAB}");
        }
    }
}
