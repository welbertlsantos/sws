﻿namespace SWS.View
{
    partial class frmRelCobranca
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmRelCobranca));
            this.grbCliente = new System.Windows.Forms.GroupBox();
            this.btnCliente = new System.Windows.Forms.Button();
            this.textCliente = new System.Windows.Forms.TextBox();
            this.grbSituacao = new System.Windows.Forms.GroupBox();
            this.rbSituacaoCancelada = new System.Windows.Forms.RadioButton();
            this.rbSituacaoBaixada = new System.Windows.Forms.RadioButton();
            this.rbSituacaoAberta = new System.Windows.Forms.RadioButton();
            this.grbPeriodo = new System.Windows.Forms.GroupBox();
            this.grbBaixa = new System.Windows.Forms.GroupBox();
            this.dataBaixaFinal = new System.Windows.Forms.DateTimePicker();
            this.lblBaixa = new System.Windows.Forms.Label();
            this.dataBaixaInicial = new System.Windows.Forms.DateTimePicker();
            this.grbCancelamento = new System.Windows.Forms.GroupBox();
            this.dataCancelamentoFinal = new System.Windows.Forms.DateTimePicker();
            this.lblCancelamento = new System.Windows.Forms.Label();
            this.dataCancelamentoInicial = new System.Windows.Forms.DateTimePicker();
            this.grbVencimento = new System.Windows.Forms.GroupBox();
            this.chkVencimento = new System.Windows.Forms.CheckBox();
            this.dataVencimentoFinal = new System.Windows.Forms.DateTimePicker();
            this.lblVencimento = new System.Windows.Forms.Label();
            this.dataVencimentoInicial = new System.Windows.Forms.DateTimePicker();
            this.grbEmissao = new System.Windows.Forms.GroupBox();
            this.chkEmissao = new System.Windows.Forms.CheckBox();
            this.dataEmissaoFinal = new System.Windows.Forms.DateTimePicker();
            this.lblEmissao = new System.Windows.Forms.Label();
            this.dataEmissaoInicial = new System.Windows.Forms.DateTimePicker();
            this.grbTipo = new System.Windows.Forms.GroupBox();
            this.rbDetalhado = new System.Windows.Forms.RadioButton();
            this.rbSimplificado = new System.Windows.Forms.RadioButton();
            this.grbAcao = new System.Windows.Forms.GroupBox();
            this.btnFechar = new System.Windows.Forms.Button();
            this.btnImprimir = new System.Windows.Forms.Button();
            this.grbCliente.SuspendLayout();
            this.grbSituacao.SuspendLayout();
            this.grbPeriodo.SuspendLayout();
            this.grbBaixa.SuspendLayout();
            this.grbCancelamento.SuspendLayout();
            this.grbVencimento.SuspendLayout();
            this.grbEmissao.SuspendLayout();
            this.grbTipo.SuspendLayout();
            this.grbAcao.SuspendLayout();
            this.SuspendLayout();
            // 
            // grbCliente
            // 
            this.grbCliente.Controls.Add(this.btnCliente);
            this.grbCliente.Controls.Add(this.textCliente);
            this.grbCliente.Location = new System.Drawing.Point(5, 12);
            this.grbCliente.Name = "grbCliente";
            this.grbCliente.Size = new System.Drawing.Size(583, 49);
            this.grbCliente.TabIndex = 0;
            this.grbCliente.TabStop = false;
            this.grbCliente.Text = "Cliente";
            // 
            // btnCliente
            // 
            this.btnCliente.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnCliente.Image = global::SWS.Properties.Resources.lupa;
            this.btnCliente.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnCliente.Location = new System.Drawing.Point(483, 16);
            this.btnCliente.Name = "btnCliente";
            this.btnCliente.Size = new System.Drawing.Size(75, 23);
            this.btnCliente.TabIndex = 1;
            this.btnCliente.Text = "&Buscar";
            this.btnCliente.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnCliente.UseVisualStyleBackColor = true;
            this.btnCliente.Click += new System.EventHandler(this.btnCliente_Click);
            // 
            // textCliente
            // 
            this.textCliente.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textCliente.Location = new System.Drawing.Point(7, 19);
            this.textCliente.Name = "textCliente";
            this.textCliente.ReadOnly = true;
            this.textCliente.Size = new System.Drawing.Size(470, 20);
            this.textCliente.TabIndex = 0;
            this.textCliente.TabStop = false;
            // 
            // grbSituacao
            // 
            this.grbSituacao.Controls.Add(this.rbSituacaoCancelada);
            this.grbSituacao.Controls.Add(this.rbSituacaoBaixada);
            this.grbSituacao.Controls.Add(this.rbSituacaoAberta);
            this.grbSituacao.Location = new System.Drawing.Point(5, 210);
            this.grbSituacao.Name = "grbSituacao";
            this.grbSituacao.Size = new System.Drawing.Size(123, 100);
            this.grbSituacao.TabIndex = 1;
            this.grbSituacao.TabStop = false;
            this.grbSituacao.Text = "Situação";
            // 
            // rbSituacaoCancelada
            // 
            this.rbSituacaoCancelada.AutoSize = true;
            this.rbSituacaoCancelada.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.rbSituacaoCancelada.Location = new System.Drawing.Point(8, 66);
            this.rbSituacaoCancelada.Name = "rbSituacaoCancelada";
            this.rbSituacaoCancelada.Size = new System.Drawing.Size(75, 17);
            this.rbSituacaoCancelada.TabIndex = 2;
            this.rbSituacaoCancelada.Text = "Cancelada";
            this.rbSituacaoCancelada.UseVisualStyleBackColor = true;
            this.rbSituacaoCancelada.CheckedChanged += new System.EventHandler(this.rbSituacaoCancelada_CheckedChanged);
            // 
            // rbSituacaoBaixada
            // 
            this.rbSituacaoBaixada.AutoSize = true;
            this.rbSituacaoBaixada.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.rbSituacaoBaixada.Location = new System.Drawing.Point(8, 43);
            this.rbSituacaoBaixada.Name = "rbSituacaoBaixada";
            this.rbSituacaoBaixada.Size = new System.Drawing.Size(62, 17);
            this.rbSituacaoBaixada.TabIndex = 1;
            this.rbSituacaoBaixada.Text = "Baixada";
            this.rbSituacaoBaixada.UseVisualStyleBackColor = true;
            this.rbSituacaoBaixada.CheckedChanged += new System.EventHandler(this.rbSituacaoBaixada_CheckedChanged);
            // 
            // rbSituacaoAberta
            // 
            this.rbSituacaoAberta.AutoSize = true;
            this.rbSituacaoAberta.Checked = true;
            this.rbSituacaoAberta.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.rbSituacaoAberta.Location = new System.Drawing.Point(8, 20);
            this.rbSituacaoAberta.Name = "rbSituacaoAberta";
            this.rbSituacaoAberta.Size = new System.Drawing.Size(72, 17);
            this.rbSituacaoAberta.TabIndex = 0;
            this.rbSituacaoAberta.TabStop = true;
            this.rbSituacaoAberta.Text = "Em aberto";
            this.rbSituacaoAberta.UseVisualStyleBackColor = true;
            this.rbSituacaoAberta.CheckedChanged += new System.EventHandler(this.rbSituacaoAberta_CheckedChanged);
            // 
            // grbPeriodo
            // 
            this.grbPeriodo.Controls.Add(this.grbBaixa);
            this.grbPeriodo.Controls.Add(this.grbCancelamento);
            this.grbPeriodo.Controls.Add(this.grbVencimento);
            this.grbPeriodo.Controls.Add(this.grbEmissao);
            this.grbPeriodo.Location = new System.Drawing.Point(5, 67);
            this.grbPeriodo.Name = "grbPeriodo";
            this.grbPeriodo.Size = new System.Drawing.Size(518, 137);
            this.grbPeriodo.TabIndex = 2;
            this.grbPeriodo.TabStop = false;
            this.grbPeriodo.Text = "Período";
            // 
            // grbBaixa
            // 
            this.grbBaixa.Controls.Add(this.dataBaixaFinal);
            this.grbBaixa.Controls.Add(this.lblBaixa);
            this.grbBaixa.Controls.Add(this.dataBaixaInicial);
            this.grbBaixa.Enabled = false;
            this.grbBaixa.Location = new System.Drawing.Point(270, 76);
            this.grbBaixa.Name = "grbBaixa";
            this.grbBaixa.Size = new System.Drawing.Size(241, 51);
            this.grbBaixa.TabIndex = 5;
            this.grbBaixa.TabStop = false;
            this.grbBaixa.Text = "Baixa";
            // 
            // dataBaixaFinal
            // 
            this.dataBaixaFinal.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dataBaixaFinal.Location = new System.Drawing.Point(133, 19);
            this.dataBaixaFinal.Name = "dataBaixaFinal";
            this.dataBaixaFinal.Size = new System.Drawing.Size(96, 20);
            this.dataBaixaFinal.TabIndex = 2;
            // 
            // lblBaixa
            // 
            this.lblBaixa.AutoSize = true;
            this.lblBaixa.Location = new System.Drawing.Point(105, 23);
            this.lblBaixa.Name = "lblBaixa";
            this.lblBaixa.Size = new System.Drawing.Size(22, 13);
            this.lblBaixa.TabIndex = 1;
            this.lblBaixa.Text = "até";
            // 
            // dataBaixaInicial
            // 
            this.dataBaixaInicial.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dataBaixaInicial.Location = new System.Drawing.Point(7, 20);
            this.dataBaixaInicial.Name = "dataBaixaInicial";
            this.dataBaixaInicial.Size = new System.Drawing.Size(96, 20);
            this.dataBaixaInicial.TabIndex = 0;
            // 
            // grbCancelamento
            // 
            this.grbCancelamento.Controls.Add(this.dataCancelamentoFinal);
            this.grbCancelamento.Controls.Add(this.lblCancelamento);
            this.grbCancelamento.Controls.Add(this.dataCancelamentoInicial);
            this.grbCancelamento.Enabled = false;
            this.grbCancelamento.Location = new System.Drawing.Point(270, 19);
            this.grbCancelamento.Name = "grbCancelamento";
            this.grbCancelamento.Size = new System.Drawing.Size(241, 51);
            this.grbCancelamento.TabIndex = 4;
            this.grbCancelamento.TabStop = false;
            this.grbCancelamento.Text = "Cancelamento";
            // 
            // dataCancelamentoFinal
            // 
            this.dataCancelamentoFinal.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dataCancelamentoFinal.Location = new System.Drawing.Point(133, 19);
            this.dataCancelamentoFinal.Name = "dataCancelamentoFinal";
            this.dataCancelamentoFinal.Size = new System.Drawing.Size(96, 20);
            this.dataCancelamentoFinal.TabIndex = 2;
            // 
            // lblCancelamento
            // 
            this.lblCancelamento.AutoSize = true;
            this.lblCancelamento.Location = new System.Drawing.Point(105, 23);
            this.lblCancelamento.Name = "lblCancelamento";
            this.lblCancelamento.Size = new System.Drawing.Size(22, 13);
            this.lblCancelamento.TabIndex = 1;
            this.lblCancelamento.Text = "até";
            // 
            // dataCancelamentoInicial
            // 
            this.dataCancelamentoInicial.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dataCancelamentoInicial.Location = new System.Drawing.Point(7, 20);
            this.dataCancelamentoInicial.Name = "dataCancelamentoInicial";
            this.dataCancelamentoInicial.Size = new System.Drawing.Size(96, 20);
            this.dataCancelamentoInicial.TabIndex = 0;
            // 
            // grbVencimento
            // 
            this.grbVencimento.Controls.Add(this.chkVencimento);
            this.grbVencimento.Controls.Add(this.dataVencimentoFinal);
            this.grbVencimento.Controls.Add(this.lblVencimento);
            this.grbVencimento.Controls.Add(this.dataVencimentoInicial);
            this.grbVencimento.Location = new System.Drawing.Point(6, 76);
            this.grbVencimento.Name = "grbVencimento";
            this.grbVencimento.Size = new System.Drawing.Size(256, 51);
            this.grbVencimento.TabIndex = 3;
            this.grbVencimento.TabStop = false;
            this.grbVencimento.Text = "Vencimento";
            // 
            // chkVencimento
            // 
            this.chkVencimento.AutoSize = true;
            this.chkVencimento.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.chkVencimento.Location = new System.Drawing.Point(6, 23);
            this.chkVencimento.Name = "chkVencimento";
            this.chkVencimento.Size = new System.Drawing.Size(12, 11);
            this.chkVencimento.TabIndex = 4;
            this.chkVencimento.UseVisualStyleBackColor = true;
            this.chkVencimento.CheckedChanged += new System.EventHandler(this.chkVencimento_CheckedChanged);
            // 
            // dataVencimentoFinal
            // 
            this.dataVencimentoFinal.Enabled = false;
            this.dataVencimentoFinal.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dataVencimentoFinal.Location = new System.Drawing.Point(147, 19);
            this.dataVencimentoFinal.Name = "dataVencimentoFinal";
            this.dataVencimentoFinal.Size = new System.Drawing.Size(96, 20);
            this.dataVencimentoFinal.TabIndex = 2;
            // 
            // lblVencimento
            // 
            this.lblVencimento.AutoSize = true;
            this.lblVencimento.Location = new System.Drawing.Point(119, 25);
            this.lblVencimento.Name = "lblVencimento";
            this.lblVencimento.Size = new System.Drawing.Size(22, 13);
            this.lblVencimento.TabIndex = 1;
            this.lblVencimento.Text = "até";
            // 
            // dataVencimentoInicial
            // 
            this.dataVencimentoInicial.Enabled = false;
            this.dataVencimentoInicial.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dataVencimentoInicial.Location = new System.Drawing.Point(21, 20);
            this.dataVencimentoInicial.Name = "dataVencimentoInicial";
            this.dataVencimentoInicial.Size = new System.Drawing.Size(96, 20);
            this.dataVencimentoInicial.TabIndex = 0;
            // 
            // grbEmissao
            // 
            this.grbEmissao.Controls.Add(this.chkEmissao);
            this.grbEmissao.Controls.Add(this.dataEmissaoFinal);
            this.grbEmissao.Controls.Add(this.lblEmissao);
            this.grbEmissao.Controls.Add(this.dataEmissaoInicial);
            this.grbEmissao.Location = new System.Drawing.Point(6, 19);
            this.grbEmissao.Name = "grbEmissao";
            this.grbEmissao.Size = new System.Drawing.Size(256, 51);
            this.grbEmissao.TabIndex = 0;
            this.grbEmissao.TabStop = false;
            this.grbEmissao.Text = "Emissão";
            // 
            // chkEmissao
            // 
            this.chkEmissao.AutoSize = true;
            this.chkEmissao.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.chkEmissao.Location = new System.Drawing.Point(6, 24);
            this.chkEmissao.Name = "chkEmissao";
            this.chkEmissao.Size = new System.Drawing.Size(12, 11);
            this.chkEmissao.TabIndex = 3;
            this.chkEmissao.UseVisualStyleBackColor = true;
            this.chkEmissao.CheckedChanged += new System.EventHandler(this.chkEmissao_CheckedChanged);
            // 
            // dataEmissaoFinal
            // 
            this.dataEmissaoFinal.Enabled = false;
            this.dataEmissaoFinal.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dataEmissaoFinal.Location = new System.Drawing.Point(147, 19);
            this.dataEmissaoFinal.Name = "dataEmissaoFinal";
            this.dataEmissaoFinal.Size = new System.Drawing.Size(96, 20);
            this.dataEmissaoFinal.TabIndex = 2;
            // 
            // lblEmissao
            // 
            this.lblEmissao.AutoSize = true;
            this.lblEmissao.Location = new System.Drawing.Point(119, 23);
            this.lblEmissao.Name = "lblEmissao";
            this.lblEmissao.Size = new System.Drawing.Size(22, 13);
            this.lblEmissao.TabIndex = 1;
            this.lblEmissao.Text = "até";
            // 
            // dataEmissaoInicial
            // 
            this.dataEmissaoInicial.Enabled = false;
            this.dataEmissaoInicial.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dataEmissaoInicial.Location = new System.Drawing.Point(21, 20);
            this.dataEmissaoInicial.Name = "dataEmissaoInicial";
            this.dataEmissaoInicial.Size = new System.Drawing.Size(96, 20);
            this.dataEmissaoInicial.TabIndex = 0;
            // 
            // grbTipo
            // 
            this.grbTipo.Controls.Add(this.rbDetalhado);
            this.grbTipo.Controls.Add(this.rbSimplificado);
            this.grbTipo.Location = new System.Drawing.Point(144, 210);
            this.grbTipo.Name = "grbTipo";
            this.grbTipo.Size = new System.Drawing.Size(123, 100);
            this.grbTipo.TabIndex = 3;
            this.grbTipo.TabStop = false;
            this.grbTipo.Text = "Tipo de Relatório";
            // 
            // rbDetalhado
            // 
            this.rbDetalhado.AutoSize = true;
            this.rbDetalhado.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.rbDetalhado.Location = new System.Drawing.Point(8, 43);
            this.rbDetalhado.Name = "rbDetalhado";
            this.rbDetalhado.Size = new System.Drawing.Size(73, 17);
            this.rbDetalhado.TabIndex = 1;
            this.rbDetalhado.Text = "Detalhado";
            this.rbDetalhado.UseVisualStyleBackColor = true;
            // 
            // rbSimplificado
            // 
            this.rbSimplificado.AutoSize = true;
            this.rbSimplificado.Checked = true;
            this.rbSimplificado.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.rbSimplificado.Location = new System.Drawing.Point(8, 20);
            this.rbSimplificado.Name = "rbSimplificado";
            this.rbSimplificado.Size = new System.Drawing.Size(80, 17);
            this.rbSimplificado.TabIndex = 0;
            this.rbSimplificado.TabStop = true;
            this.rbSimplificado.Text = "Simplificado";
            this.rbSimplificado.UseVisualStyleBackColor = true;
            // 
            // grbAcao
            // 
            this.grbAcao.Controls.Add(this.btnFechar);
            this.grbAcao.Controls.Add(this.btnImprimir);
            this.grbAcao.Location = new System.Drawing.Point(5, 341);
            this.grbAcao.Name = "grbAcao";
            this.grbAcao.Size = new System.Drawing.Size(583, 47);
            this.grbAcao.TabIndex = 4;
            this.grbAcao.TabStop = false;
            this.grbAcao.Text = "Ação";
            // 
            // btnFechar
            // 
            this.btnFechar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnFechar.Image = global::SWS.Properties.Resources.close;
            this.btnFechar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnFechar.Location = new System.Drawing.Point(89, 18);
            this.btnFechar.Name = "btnFechar";
            this.btnFechar.Size = new System.Drawing.Size(75, 23);
            this.btnFechar.TabIndex = 1;
            this.btnFechar.Text = "&Fechar";
            this.btnFechar.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnFechar.UseVisualStyleBackColor = true;
            this.btnFechar.Click += new System.EventHandler(this.btnFechar_Click);
            // 
            // btnImprimir
            // 
            this.btnImprimir.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnImprimir.Image = global::SWS.Properties.Resources.impressora1;
            this.btnImprimir.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnImprimir.Location = new System.Drawing.Point(8, 18);
            this.btnImprimir.Name = "btnImprimir";
            this.btnImprimir.Size = new System.Drawing.Size(75, 23);
            this.btnImprimir.TabIndex = 0;
            this.btnImprimir.Text = "&Imprimir";
            this.btnImprimir.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnImprimir.UseVisualStyleBackColor = true;
            this.btnImprimir.Click += new System.EventHandler(this.btnImprimir_Click);
            // 
            // frmRelCobranca
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(600, 400);
            this.ControlBox = false;
            this.Controls.Add(this.grbAcao);
            this.Controls.Add(this.grbTipo);
            this.Controls.Add(this.grbPeriodo);
            this.Controls.Add(this.grbSituacao);
            this.Controls.Add(this.grbCliente);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "frmRelCobranca";
            this.Text = "RELATÓRIO DE COBRANÇA";
            this.grbCliente.ResumeLayout(false);
            this.grbCliente.PerformLayout();
            this.grbSituacao.ResumeLayout(false);
            this.grbSituacao.PerformLayout();
            this.grbPeriodo.ResumeLayout(false);
            this.grbBaixa.ResumeLayout(false);
            this.grbBaixa.PerformLayout();
            this.grbCancelamento.ResumeLayout(false);
            this.grbCancelamento.PerformLayout();
            this.grbVencimento.ResumeLayout(false);
            this.grbVencimento.PerformLayout();
            this.grbEmissao.ResumeLayout(false);
            this.grbEmissao.PerformLayout();
            this.grbTipo.ResumeLayout(false);
            this.grbTipo.PerformLayout();
            this.grbAcao.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox grbCliente;
        private System.Windows.Forms.TextBox textCliente;
        private System.Windows.Forms.Button btnCliente;
        private System.Windows.Forms.GroupBox grbSituacao;
        private System.Windows.Forms.RadioButton rbSituacaoCancelada;
        private System.Windows.Forms.RadioButton rbSituacaoBaixada;
        private System.Windows.Forms.RadioButton rbSituacaoAberta;
        private System.Windows.Forms.GroupBox grbPeriodo;
        private System.Windows.Forms.GroupBox grbBaixa;
        private System.Windows.Forms.DateTimePicker dataBaixaFinal;
        private System.Windows.Forms.Label lblBaixa;
        private System.Windows.Forms.DateTimePicker dataBaixaInicial;
        private System.Windows.Forms.GroupBox grbCancelamento;
        private System.Windows.Forms.DateTimePicker dataCancelamentoFinal;
        private System.Windows.Forms.Label lblCancelamento;
        private System.Windows.Forms.DateTimePicker dataCancelamentoInicial;
        private System.Windows.Forms.GroupBox grbVencimento;
        private System.Windows.Forms.DateTimePicker dataVencimentoFinal;
        private System.Windows.Forms.Label lblVencimento;
        private System.Windows.Forms.DateTimePicker dataVencimentoInicial;
        private System.Windows.Forms.GroupBox grbEmissao;
        private System.Windows.Forms.DateTimePicker dataEmissaoFinal;
        private System.Windows.Forms.Label lblEmissao;
        private System.Windows.Forms.DateTimePicker dataEmissaoInicial;
        private System.Windows.Forms.GroupBox grbTipo;
        private System.Windows.Forms.RadioButton rbDetalhado;
        private System.Windows.Forms.RadioButton rbSimplificado;
        private System.Windows.Forms.GroupBox grbAcao;
        private System.Windows.Forms.Button btnFechar;
        private System.Windows.Forms.Button btnImprimir;
        private System.Windows.Forms.CheckBox chkVencimento;
        private System.Windows.Forms.CheckBox chkEmissao;
    }
}