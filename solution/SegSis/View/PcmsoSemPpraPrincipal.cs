﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using SegSis.Entidade;
using SegSis.Facade;
using SegSis.View.Resources;
using SegSis.Excecao;

namespace SegSis.View
{
    public partial class frm_PcmsoSemPpraPrincipal : BaseForm
    {
        public Int64 idCliente; // valores alimentados pelos forms de consulta
        public Int64 idUsuarioEngenheiro; // valores alimentados pelos forms de consulta
        public Int64 idUsuarioTecno; // valores alimentados pelos forms de consulta

        public static String Msg02 = " Selecione uma linha!";
        public static String Msg03 = " Deseja realmente excluir esse PCMSO?";
        public static String Msg04 = " Atenção ";
        public static String Msg05 = " Confirmação ";
        public static String Msg06 = " PCMSO excluído com sucesso!";
        public static String Msg07 = " Revisão concluída com sucesso !";
        public static String Msg08 = " Deseja finalizar esse PCMSO?";
        public static String Msg09 = " PCMSO finalizado com sucesso.";
        public static String Msg10 = " Deseja enviar esse estudo para análise?";
        public static String Msg11 = " PCMSO em análise, deseja enviá-lo para Construção? ";
        public static String Msg12 = " PCMSO enviado para analise com sucesso. ";
        public static String Msg13 = " PCMSO enviado novamente para construção com sucesso. ";

        private Estudo pcmso = null;

        public String comentario = null;

        public frm_PcmsoSemPpraPrincipal()
        {
            InitializeComponent();

            InitializeComponent();
            rb_todos.Checked = true;
            ck_dataCriacao.Checked = false;
            validaPermissoes();
        }

        public void setEstudo(Estudo pcmso)
        {
            this.pcmso = pcmso;
        }

        public Estudo getEstudo()
        {
            return this.pcmso;
        }

        public void setElaborador(Usuario elaborador)
        {
            this.pcmso.setEngenheiro(elaborador);
        }

        private void validaPermissoes()
        {
            PermissionamentoFacade permissionamentoFacade = PermissionamentoFacade.getInstance();

            bt_alterar.Enabled = permissionamentoFacade.hasPermission("PPRA", "ALTERAR");
            bt_incluir.Enabled = permissionamentoFacade.hasPermission("PPRA", "INCLUIR");
            bt_excluir.Enabled = permissionamentoFacade.hasPermission("PPRA", "EXCLUIR");
            bt_imprimir.Enabled = permissionamentoFacade.hasPermission("PPRA", "IMPRIMIR");
            btn_enviarAnalise.Enabled = permissionamentoFacade.hasPermission("PPRA", "ENVIAR");
            btn_finalizar.Enabled = permissionamentoFacade.hasPermission("PPRA", "FINALIZAR");
            btn_revisao.Enabled = permissionamentoFacade.hasPermission("PPRA", "REVISAR");
            btn_corrigir.Enabled = permissionamentoFacade.hasPermission("PPRA", "CORRIGIR");

        }

        private void bt_fechar_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void ck_dataCadastro_CheckedChanged(object sender, EventArgs e)
        {
            if (ck_dataCriacao.Checked)
            {
                dtCriacao_inicial.Enabled = true;
                dtCriacao_final.Enabled = true;
            }
            else
            {
                dtCriacao_inicial.Enabled = false;
                dtCriacao_final.Enabled = false;
            }
        }

        private void bt_limpar_Click(object sender, EventArgs e)
        {
            text_codPpra.Text = String.Empty;
            rb_todos.Checked = true;
            text_cliente.Text = String.Empty;
            text_cliente.Text = String.Empty;
            idCliente = 0; // zerando o codigo do cliente.
            text_engenheiro.Text = String.Empty;
            idUsuarioEngenheiro = 0; // zerando o código do usuário engenheiro.
            text_tecno.Text = String.Empty;
            idUsuarioTecno = 0; // zerando o código de usúário técnico.
            ck_dataCriacao.Checked = false;
            grd_pcmso.Columns.Clear();
        }

        private void bt_pesquisar_Click(object sender, EventArgs e)
        {
            try
            {
                this.pcmsoErrorProvider.Clear();

                this.Cursor = Cursors.WaitCursor;

                if (validaCampos())
                {
                    montaDataGrid();
                    text_codPpra.Focus();
                    remontarDataGrid = true;
                }
            }
            catch (Exception Ex)
            {
                MessageBox.Show(Ex.Message);
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }

        private Boolean validaCampos()
        {
            Boolean retorno = Convert.ToBoolean(true);

            if (Convert.ToDateTime(this.dtCriacao_final.Text) < Convert.ToDateTime(this.dtCriacao_inicial.Text))
            {
                this.pcmsoErrorProvider.SetError(this.dtCriacao_final, "A data final deve ser maior que a inicial!");
                retorno = Convert.ToBoolean(false);
            }

            return retorno;

        }

        public void montaDataGrid()
        {
            PcmsoFacade pcmsoFacade = PcmsoFacade.getInstance();

            this.Cursor = Cursors.WaitCursor;

            Estudo pcmso = new Estudo();

            String situacaoPcmso = null;
            DateTime? dataCriacaoFinal = null;

            VendedorCliente vendedorCliente = new VendedorCliente();
            Cliente cliente = new Cliente();

            Usuario usuarioTecno = new Usuario();
            Usuario usuarioEngenheiro = new Usuario();

            pcmso.setCodEstudo(text_codPpra.Text);

            // preparando o objeto vendedorCliente e adicionando ao estudo.
            cliente.setRazaoSocial(text_cliente.Text);
            cliente.setId(idCliente);

            vendedorCliente.setCliente(cliente);
            pcmso.setVendedorCliente(vendedorCliente);

            // preparando o objeto Usuario com o papel de tecnico.

            usuarioTecno.setId(idUsuarioTecno);
            usuarioTecno.setNome(text_tecno.Text);
            pcmso.setTecno(usuarioTecno);

            // preparando o objeto Usuario com o papel de engenheiro.
            usuarioEngenheiro.setId(idUsuarioEngenheiro);
            usuarioEngenheiro.setNome(text_engenheiro.Text);
            pcmso.setEngenheiro(usuarioEngenheiro);

            if (rb_aguardando.Checked)
            {
                situacaoPcmso = ApplicationConstants.ATIVO;
            }
            else if (rb_fechado.Checked)
            {
                situacaoPcmso = ApplicationConstants.FECHADO;
            }

            if (rb_construcao.Checked)
            {
                situacaoPcmso = ApplicationConstants.CONSTRUCAO;
            }

            pcmso.setSituacaoEstudo(situacaoPcmso); // situacao do estudo

            if (ck_dataCriacao.Checked)
            {
                dataCriacaoFinal = Convert.ToDateTime(dtCriacao_final.Text);
                pcmso.setDataCriacao(Convert.ToDateTime(dtCriacao_inicial.Text)); // data da criacao

            }
            else
            {
                pcmso.setDataCriacao(null); // limpando data de criação
            }

            DataSet ds = pcmsoFacade.findPcmsoByFilter(pcmso, dataCriacaoFinal);

            grd_pcmso.DataSource = ds.Tables["Estudos"].DefaultView;

            grd_pcmso.Columns[0].HeaderText = "ID";
            grd_pcmso.Columns[1].HeaderText = "Código PCMSO";
            grd_pcmso.Columns[2].HeaderText = "Data da Criação";
            grd_pcmso.Columns[3].HeaderText = "Data de Vencimento";
            grd_pcmso.Columns[4].HeaderText = "Data de Fechamento";
            grd_pcmso.Columns[5].HeaderText = "Situação";
            grd_pcmso.Columns[6].HeaderText = "Cliente";

            grd_pcmso.Columns[0].Visible = false;

            for (int i = 0; i < 6; i++)
            {
                grd_pcmso.Columns[i].DisplayIndex = i + 1;
            }

            grd_pcmso.Columns[6].DisplayIndex = 0;

            this.Cursor = Cursors.Default;
        }

        private void btn_clienteLocalizar_Click(object sender, EventArgs e)
        {
            frm_clienteSelecionarAtivos formClientesAtivos = new frm_clienteSelecionarAtivos(this);
            formClientesAtivos.ShowDialog();
        }

        private void bt_tecnico_Click(object sender, EventArgs e)
        {
            frm_ppraUsuarioTecnicoSelecionar formUsuarioFuncaoInternaSelecionar = new frm_ppraUsuarioTecnicoSelecionar(this);
            formUsuarioFuncaoInternaSelecionar.ShowDialog();
        }

        private void bt_engenheiro_Click(object sender, EventArgs e)
        {
            frm_PcmsoMedicoSelecionar formPcmsoMedicoSelecionar = new frm_PcmsoMedicoSelecionar(this);
            formPcmsoMedicoSelecionar.ShowDialog();
        }

        private void bt_incluir_Click(object sender, EventArgs e)
        {
            frm_PcmsoSemPpraIncluir formPcmsoSemPpraIncluir = new frm_PcmsoSemPpraIncluir(this);
            formPcmsoSemPpraIncluir.ShowDialog();
        }

        private void bt_alterar_Click(object sender, EventArgs e)
        {
            try
            {
                this.Cursor = Cursors.WaitCursor;

                if (grd_pcmso.CurrentRow != null)
                {
                    Int32 cellValue = grd_pcmso.CurrentRow.Index;

                    Int64 id = (Int64)grd_pcmso.Rows[cellValue].Cells[0].Value;

                    PcmsoFacade pcmsoFacade = PcmsoFacade.getInstance();

                    pcmso = pcmsoFacade.findById(id);

                    // somente será possível alterar um estudo em contrucao.

                    if (pcmso.getSituacaoEstudo() != ApplicationConstants.CONSTRUCAO)
                    {
                        throw new PcmsoException(PcmsoException.msg12);
                    }

                    //frm_ppraAlterar formPpraAlterar = new frm_ppraAlterar(pcmso, this);
                    //formPpraAlterar.ShowDialog();
                }
                else
                {
                    MessageBox.Show(Msg02);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }

        private void bt_imprimir_Click(object sender, EventArgs e)
        {
            PpraFacade ppraFacade = PpraFacade.getInstance();
            PcmsoFacade pcmsoFacade = PcmsoFacade.getInstance();
            RelatorioFacade relatorioFacade = RelatorioFacade.getInstance();

            try
            {
                if (grd_pcmso.CurrentRow != null)
                {
                    this.Cursor = Cursors.WaitCursor;

                    Int32 cellValue = grd_pcmso.CurrentRow.Index;

                    Int64 id = (Int64)grd_pcmso.Rows[cellValue].Cells[0].Value;

                    Estudo estudo = pcmsoFacade.findById(id);

                    ppraFacade.montaQuadroVersao(id);

                    System.Diagnostics.Process.Start("java", "-jar Relatorio\\MiddlewareJasper.jar " + "Relatorio\\Report\\" + relatorioFacade.findNomeRelatorioById(estudo.getIdRelatorio()) + " ID_PPRA:" + id + ":Integer");

                }
                else
                {
                    MessageBox.Show(Msg02);
                }
            }
            catch (Exception ex)
            {
                this.Cursor = Cursors.Default;
                MessageBox.Show(ex.Message);
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }

        private void btn_revisao_Click(object sender, EventArgs e)
        {
            PcmsoFacade pcmsoFacade = PcmsoFacade.getInstance();

            try
            {
                if (grd_pcmso.CurrentRow != null)
                {
                    this.Cursor = Cursors.WaitCursor;

                    Int32 cellValue = grd_pcmso.CurrentRow.Index;

                    Int64 id = (Int64)grd_pcmso.Rows[cellValue].Cells[0].Value;

                    if (!pcmsoFacade.verificaPodeReplicarByRevisao(id))
                    {
                        throw new PcmsoException(PcmsoException.msg14);
                    }

                    if (!pcmsoFacade.verificaPodeReplicarByPcmso(id))
                    {
                        throw new PcmsoException(PcmsoException.msg13);
                    }

                    // iniciando tela de comentario para a revisão

                    frm_ppraReplicar formPpraReplicar = new frm_ppraReplicar(this);
                    formPpraReplicar.ShowDialog();

                    pcmsoFacade.replicarPcmso(id, comentario, false);

                    MessageBox.Show(Msg07, Msg05);
                    montaDataGrid();
                }
                else
                {
                    MessageBox.Show(Msg02);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }

        private void bt_excluir_Click(object sender, EventArgs e)
        {
            try
            {
                if (grd_pcmso.CurrentRow != null)
                {
                    PcmsoFacade pcmsoFacade = PcmsoFacade.getInstance();

                    this.Cursor = Cursors.WaitCursor;

                    if (MessageBox.Show(Msg03, Msg05, MessageBoxButtons.YesNo) == DialogResult.Yes)
                    {
                        Int32 cellValue = grd_pcmso.CurrentRow.Index;
                        Int64 id = (Int64)grd_pcmso.Rows[cellValue].Cells[0].Value;
                        String situacao = (string)grd_pcmso.Rows[cellValue].Cells[5].Value;

                        if (situacao == ApplicationConstants.FECHADO)
                        {
                            throw new PcmsoException(PcmsoException.msg15);
                        }
                        else
                        {
                            pcmsoFacade.excluiPcmso(id);
                            MessageBox.Show(Msg06, Msg05);

                            montaDataGrid();
                        }
                    }
                }
                else
                {
                    MessageBox.Show(Msg02);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }

        private void btn_finalizar_Click(object sender, EventArgs e)
        {
            try
            {
                if (grd_pcmso.CurrentRow != null)
                {
                    PcmsoFacade pcmsoFacade = PcmsoFacade.getInstance();

                    this.Cursor = Cursors.WaitCursor;

                    if (MessageBox.Show(Msg08, Msg04, MessageBoxButtons.YesNo) == DialogResult.Yes)
                    {
                        Int32 cellValue = grd_pcmso.CurrentRow.Index;
                        Int64 id = (Int64)grd_pcmso.Rows[cellValue].Cells[0].Value;
                        String situacao = (string)grd_pcmso.Rows[cellValue].Cells[5].Value;

                        if (situacao != ApplicationConstants.ATIVO)
                        {
                            throw new PcmsoException(PcmsoException.msg16);
                        }
                        else
                        {
                            pcmso = pcmsoFacade.findById(id);

                            pcmsoFacade.mudaEstadoPcmso(pcmso, ApplicationConstants.FECHADO);

                            MessageBox.Show(Msg09, Msg05);

                            montaDataGrid();

                        }
                    }
                }
                else
                {
                    MessageBox.Show(Msg02);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);

            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }

        private void btn_enviarAnalise_Click(object sender, EventArgs e)
        {
            try
            {

                if (grd_pcmso.CurrentRow != null)
                {
                    this.Cursor = Cursors.WaitCursor;

                    PcmsoFacade pcmsoFacade = PcmsoFacade.getInstance();

                    Int32 cellValue = grd_pcmso.CurrentRow.Index;
                    Int64 id = (Int64)grd_pcmso.Rows[cellValue].Cells[0].Value;
                    String situacao = (string)grd_pcmso.Rows[cellValue].Cells[5].Value;

                    if (situacao.Equals(ApplicationConstants.ATIVO))
                    {
                        if (MessageBox.Show(Msg11, Msg04, MessageBoxButtons.YesNo) == DialogResult.Yes)
                        {
                            pcmso = pcmsoFacade.findById(id);

                            pcmsoFacade.mudaEstadoPcmso(pcmso, ApplicationConstants.CONSTRUCAO);

                            MessageBox.Show(Msg13, Msg05);
                            montaDataGrid();
                        }
                    }
                    else
                    {
                        if (situacao.Equals(ApplicationConstants.CONSTRUCAO))
                        {
                            if (MessageBox.Show(Msg10, Msg04, MessageBoxButtons.YesNo) == DialogResult.Yes)
                            {
                                pcmso = pcmsoFacade.findById(id);

                                frm_PpraElaborador formPpraElaborador = new frm_PpraElaborador(this, pcmso);
                                formPpraElaborador.ShowDialog();

                                if (pcmso.getEngenheiro() != null)
                                {
                                    pcmsoFacade.mudaEstadoPcmso(pcmso, ApplicationConstants.ATIVO);

                                    MessageBox.Show(Msg12, Msg05);
                                    montaDataGrid();
                                }
                            }
                        }
                    }
                }
                else
                {
                    MessageBox.Show(Msg02);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }

        private void grd_ppra_RowPrePaint(object sender, DataGridViewRowPrePaintEventArgs e)
        {
            DataGridView dvg = sender as DataGridView;
            String valor = dvg.Rows[e.RowIndex].Cells[5].Value.ToString();

            if (String.Equals(valor, ApplicationConstants.FECHADO))
            {
                dvg.Rows[e.RowIndex].DefaultCellStyle.ForeColor = Color.Green;
            }
            if (String.Equals(valor, ApplicationConstants.ATIVO))
            {
                dvg.Rows[e.RowIndex].DefaultCellStyle.ForeColor = Color.Blue;
            }
        }

        private void btn_corrigir_Click(object sender, EventArgs e)
        {
            PcmsoFacade pcmsoFacade = PcmsoFacade.getInstance();

            try
            {
                if (grd_pcmso.CurrentRow != null)
                {
                    this.Cursor = Cursors.WaitCursor;

                    Int32 cellValue = grd_pcmso.CurrentRow.Index;

                    Int64 id = (Int64)grd_pcmso.Rows[cellValue].Cells[0].Value;

                    if (!pcmsoFacade.verificaPodeReplicarByRevisao(id))
                    {
                        throw new PcmsoException(PcmsoException.msg14);
                    }

                    if (!pcmsoFacade.verificaPodeReplicarByPcmso(id))
                    {
                        throw new PcmsoException(PcmsoException.msg13);
                    }

                    // iniciando tela de comentario para a revisão

                    frm_ppraReplicar formPpraReplicar = new frm_ppraReplicar(this);
                    formPpraReplicar.ShowDialog();

                    pcmsoFacade.replicarPcmso(id, comentario, true);

                    MessageBox.Show(Msg07, Msg05);
                    montaDataGrid();
                }
                else
                {
                    MessageBox.Show(Msg02);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }
    }
}
