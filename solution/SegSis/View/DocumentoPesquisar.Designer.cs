﻿namespace SWS.View
{
    partial class frm_DocumentoPesquisar
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frm_DocumentoPesquisar));
            this.grbFiltro = new System.Windows.Forms.GroupBox();
            this.btn_pesquisar = new System.Windows.Forms.Button();
            this.lblDocumento = new System.Windows.Forms.Label();
            this.textDocumento = new System.Windows.Forms.TextBox();
            this.grbDocumentos = new System.Windows.Forms.GroupBox();
            this.dgDocumento = new System.Windows.Forms.DataGridView();
            this.grbAcao = new System.Windows.Forms.GroupBox();
            this.btnFechar = new System.Windows.Forms.Button();
            this.btnConfirmar = new System.Windows.Forms.Button();
            this.grbFiltro.SuspendLayout();
            this.grbDocumentos.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgDocumento)).BeginInit();
            this.grbAcao.SuspendLayout();
            this.SuspendLayout();
            // 
            // grbFiltro
            // 
            this.grbFiltro.Controls.Add(this.btn_pesquisar);
            this.grbFiltro.Controls.Add(this.lblDocumento);
            this.grbFiltro.Controls.Add(this.textDocumento);
            this.grbFiltro.Location = new System.Drawing.Point(3, 4);
            this.grbFiltro.Name = "grbFiltro";
            this.grbFiltro.Size = new System.Drawing.Size(592, 95);
            this.grbFiltro.TabIndex = 0;
            this.grbFiltro.TabStop = false;
            this.grbFiltro.Text = "Filtro";
            // 
            // btn_pesquisar
            // 
            this.btn_pesquisar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_pesquisar.Image = global::SWS.Properties.Resources.lupa;
            this.btn_pesquisar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btn_pesquisar.Location = new System.Drawing.Point(9, 59);
            this.btn_pesquisar.Name = "btn_pesquisar";
            this.btn_pesquisar.Size = new System.Drawing.Size(75, 23);
            this.btn_pesquisar.TabIndex = 2;
            this.btn_pesquisar.Text = "&Buscar";
            this.btn_pesquisar.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btn_pesquisar.UseVisualStyleBackColor = true;
            this.btn_pesquisar.Click += new System.EventHandler(this.btn_pesquisar_Click);
            // 
            // lblDocumento
            // 
            this.lblDocumento.AutoSize = true;
            this.lblDocumento.Location = new System.Drawing.Point(9, 17);
            this.lblDocumento.Name = "lblDocumento";
            this.lblDocumento.Size = new System.Drawing.Size(62, 13);
            this.lblDocumento.TabIndex = 1;
            this.lblDocumento.Text = "Documento";
            // 
            // textDocumento
            // 
            this.textDocumento.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textDocumento.Location = new System.Drawing.Point(9, 33);
            this.textDocumento.MaxLength = 200;
            this.textDocumento.Name = "textDocumento";
            this.textDocumento.Size = new System.Drawing.Size(576, 20);
            this.textDocumento.TabIndex = 0;
            // 
            // grbDocumentos
            // 
            this.grbDocumentos.Controls.Add(this.dgDocumento);
            this.grbDocumentos.Location = new System.Drawing.Point(3, 106);
            this.grbDocumentos.Name = "grbDocumentos";
            this.grbDocumentos.Size = new System.Drawing.Size(592, 238);
            this.grbDocumentos.TabIndex = 1;
            this.grbDocumentos.TabStop = false;
            this.grbDocumentos.Text = "Documentos";
            // 
            // dgDocumento
            // 
            this.dgDocumento.AllowUserToAddRows = false;
            this.dgDocumento.AllowUserToDeleteRows = false;
            this.dgDocumento.AllowUserToOrderColumns = true;
            this.dgDocumento.AllowUserToResizeRows = false;
            this.dgDocumento.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.DisplayedCells;
            this.dgDocumento.BackgroundColor = System.Drawing.Color.White;
            this.dgDocumento.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgDocumento.DefaultCellStyle = dataGridViewCellStyle1;
            this.dgDocumento.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgDocumento.Location = new System.Drawing.Point(3, 16);
            this.dgDocumento.Name = "dgDocumento";
            this.dgDocumento.Size = new System.Drawing.Size(586, 219);
            this.dgDocumento.TabIndex = 0;
            // 
            // grbAcao
            // 
            this.grbAcao.Controls.Add(this.btnFechar);
            this.grbAcao.Controls.Add(this.btnConfirmar);
            this.grbAcao.Location = new System.Drawing.Point(3, 347);
            this.grbAcao.Name = "grbAcao";
            this.grbAcao.Size = new System.Drawing.Size(589, 48);
            this.grbAcao.TabIndex = 2;
            this.grbAcao.TabStop = false;
            this.grbAcao.Text = "Ações";
            // 
            // btnFechar
            // 
            this.btnFechar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnFechar.Image = global::SWS.Properties.Resources.close;
            this.btnFechar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnFechar.Location = new System.Drawing.Point(90, 18);
            this.btnFechar.Name = "btnFechar";
            this.btnFechar.Size = new System.Drawing.Size(75, 23);
            this.btnFechar.TabIndex = 1;
            this.btnFechar.Text = "Fechar";
            this.btnFechar.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnFechar.UseVisualStyleBackColor = true;
            this.btnFechar.Click += new System.EventHandler(this.btnFechar_Click);
            // 
            // btnConfirmar
            // 
            this.btnConfirmar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnConfirmar.Image = global::SWS.Properties.Resources.fechar_ico;
            this.btnConfirmar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnConfirmar.Location = new System.Drawing.Point(9, 18);
            this.btnConfirmar.Name = "btnConfirmar";
            this.btnConfirmar.Size = new System.Drawing.Size(75, 23);
            this.btnConfirmar.TabIndex = 0;
            this.btnConfirmar.Text = "Confirmar";
            this.btnConfirmar.UseVisualStyleBackColor = true;
            this.btnConfirmar.Click += new System.EventHandler(this.btnConfirmar_Click);
            // 
            // frm_DocumentoPesquisar
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(600, 400);
            this.ControlBox = false;
            this.Controls.Add(this.grbAcao);
            this.Controls.Add(this.grbDocumentos);
            this.Controls.Add(this.grbFiltro);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "frm_DocumentoPesquisar";
            this.Text = "PESQUISAR DOCUMENTOS";
            this.grbFiltro.ResumeLayout(false);
            this.grbFiltro.PerformLayout();
            this.grbDocumentos.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgDocumento)).EndInit();
            this.grbAcao.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox grbFiltro;
        private System.Windows.Forms.Button btn_pesquisar;
        private System.Windows.Forms.Label lblDocumento;
        private System.Windows.Forms.TextBox textDocumento;
        private System.Windows.Forms.GroupBox grbDocumentos;
        private System.Windows.Forms.DataGridView dgDocumento;
        private System.Windows.Forms.GroupBox grbAcao;
        private System.Windows.Forms.Button btnFechar;
        private System.Windows.Forms.Button btnConfirmar;
    }
}