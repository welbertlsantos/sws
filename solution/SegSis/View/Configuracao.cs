﻿using SWS.Entidade;
using SWS.Facade;
using SWS.View.ViewHelper;
using SWS.ViewHelper;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace SWS.View
{
    public partial class frmConfiguracao : frmTemplate
    {
        List<Configuracao> configuracoes;
        Produto aso2Via;
        Produto asoInclusaoExames;
        Produto asoTranscrito;

        Exame classificacaoRisco;
                
        public frmConfiguracao()
        {
            InitializeComponent();
            
            try
            {
                PermissionamentoFacade permissionamentoFacade = PermissionamentoFacade.getInstance();
                FinanceiroFacade financeiroFacade = FinanceiroFacade.getInstance();
                EstudoFacade estudoFacade = EstudoFacade.getInstance();

                configuracoes = permissionamentoFacade.findAllConfiguracaoList();
                ComboHelper.Boolean(cbGeraMovimentoExame, false);
                ComboHelper.Boolean(cbGravaProdutoAutomatico, false);
                ComboHelper.Boolean(cbSSL, false);
                ComboHelper.Boolean(cbUtilizaFilial, false);
                ComboHelper.Boolean(cbImprimeSala, false);
                ComboHelper.Boolean(cbPcmsoVencido, true);
                ComboHelper.Boolean(cbOrdenacaoAtendimento, false);
                ComboHelper.Boolean(cbCancelaExameAtendimentoFinalizado, true);
                ComboHelper.Boolean(cbAtendimentoBloqueado, false);
                ComboHelper.Boolean(cbMedicoExaminador, false);
                ComboHelper.Boolean(cbConverterEsocial2220, false);
                /* montando dados na tela */


                textDiretorio.Text = configuracoes[0].Valor;
                textNumeroInicialNF.Text = financeiroFacade.findLastNumeroNfe().ToString();
                textJurosProrrogacao.Text = ValidaCampoHelper.FormataValorMonetario(configuracoes[9].Valor.Replace(".", ","));
                textQtdeTermica.Text = configuracoes[10].Valor;
                textNomeEmpresaTermica.Text = configuracoes[11].Valor;
                textIPServidorTermica.Text = configuracoes[12].Valor;
                textNomeImpressoraTermica.Text = configuracoes[13].Valor;
                textSerialCliente.Text = configuracoes[14].Valor;
                cbGeraMovimentoExame.SelectedValue = Convert.ToBoolean(configuracoes[15].Valor) == true ? "true" : "false";
                cbGravaProdutoAutomatico.SelectedValue = Convert.ToBoolean(configuracoes[18].Valor) == true ? "true" : "false";


                if (!string.IsNullOrEmpty(configuracoes[19].Valor))
                    aso2Via = financeiroFacade.findProdutoById(Convert.ToInt64(configuracoes[19].Valor));

                if (!string.IsNullOrEmpty(configuracoes[20].Valor))
                    asoTranscrito = financeiroFacade.findProdutoById(Convert.ToInt64(configuracoes[20].Valor));

                if (!string.IsNullOrEmpty(configuracoes[21].Valor))
                    asoInclusaoExames = financeiroFacade.findProdutoById(Convert.ToInt64(configuracoes[21].Valor));

                if (!string.IsNullOrEmpty(configuracoes[50].Valor))
                {
                    this.classificacaoRisco = estudoFacade.findExameById(Convert.ToInt64(configuracoes[50].Valor));
                    this.textAnaliseRisco.Text = Convert.ToString(this.classificacaoRisco.Id + " - " + this.classificacaoRisco.Descricao);
                }

                textAso2Via.Text = Convert.ToString(aso2Via.Id + " - " + aso2Via.Nome);
                textAsoTranscrito.Text = Convert.ToString(asoTranscrito.Id + " - " + asoTranscrito.Nome);
                textAsoInclusaoExame.Text = Convert.ToString(asoInclusaoExames.Id + " - " + asoInclusaoExames.Nome);
                

                textCidade.Text = configuracoes[22].Valor;
                textNomeEmpresa.Text = configuracoes[23].Valor;
                textEndereco.Text = configuracoes[24].Valor;
                textNumero.Text = configuracoes[25].Valor;
                textComplemento.Text = configuracoes[26].Valor;
                textBairro.Text = configuracoes[27].Valor;
                textUF.Text = configuracoes[28].Valor;
                textCEP.Text = configuracoes[29].Valor;
                textCNPJ.Text = configuracoes[30].Valor;
                textInscricao.Text = configuracoes[31].Valor;
                textEmail.Text = configuracoes[32].Valor;
                textServidorSMTP.Text = configuracoes[33].Valor;
                textNumeroPorta.Text = configuracoes[34].Valor;
                cbSSL.SelectedValue = Convert.ToBoolean(configuracoes[35].Valor) == true ? "true" : "false";
                textEmailEnvio.Text = configuracoes[36].Valor;
                textSenhaEmail.Text = configuracoes[37].Valor;
                cbUtilizaFilial.SelectedValue = Convert.ToBoolean(configuracoes[38].Valor) == true ? "true" : "false";
                textDiasPesquisa.Text = configuracoes[39].Valor;
                cbImprimeSala.SelectedValue = Convert.ToBoolean(configuracoes[40].Valor) == true ? "true" : "false";
                cbPcmsoVencido.SelectedValue = Convert.ToBoolean(configuracoes[41].Valor) == true ? "true" : "false";
                cbOrdenacaoAtendimento.SelectedValue = Convert.ToBoolean(configuracoes[42].Valor) == true ? "true" : "false";
                textVersaoSistema.Text = configuracoes[43].Valor.ToString();
                cbCancelaExameAtendimentoFinalizado.SelectedValue = Convert.ToBoolean(configuracoes[44].Valor) == true ? "true" : "false";
                cbAtendimentoBloqueado.SelectedValue = Convert.ToBoolean(configuracoes[45].Valor) == true ? "true" : "false";
                cbMedicoExaminador.SelectedValue = Convert.ToBoolean(configuracoes[46].Valor) == true ? "true" : "false";
                textNumeroLicencas.Text = configuracoes[47].Valor;
                cbConverterEsocial2220.SelectedValue = Convert.ToBoolean(configuracoes[48].Valor) == true ? "true" : "false";
                textLocalntegracao.Text = configuracoes[49].Valor;

                /* setando para avisos de alteração nas propriedades */
            }
            
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void textNumeroInicialNF_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = ValidaCampoHelper.FormataCampoNumerico(sender, e);
        }

        private void textQtdeTermica_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = ValidaCampoHelper.FormataCampoNumerico(sender, e);
        }

        private void textNumeroPorta_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = ValidaCampoHelper.FormataCampoNumerico(sender, e);
        }

        private void btnFechar_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void textJurosProrrogacao_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = ValidaCampoHelper.FormataDigitacaoNumero(sender, e, textJurosProrrogacao.Text, 2);
        }

        private void frmConfiguracao_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter || e.KeyCode == Keys.Return)
                SendKeys.Send("{TAB}");
        }

        private void textIPServidorTermica_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = ValidaCampoHelper.ValidaEntradaIP(sender, e);
        }

        private void textDiretorio_Leave(object sender, EventArgs e)
        {
            try
            {

                if (string.IsNullOrEmpty(textDiretorio.Text))
                    throw new Exception("Campo obrigatório");
                
                PermissionamentoFacade permissionamentoFacade = PermissionamentoFacade.getInstance();
                permissionamentoFacade.updateConfiguracao(new Configuracao(configuracoes[0].Id, configuracoes[0].Descricao, textDiretorio.Text));

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
                textDiretorio.Focus();

            }
        }

        private void textNumeroInicialNF_Enter(object sender, EventArgs e)
        {
            textNumeroInicialNF.Tag = textNumeroInicialNF.Text;
            textNumeroInicialNF.SelectAll();
        }

        private void textNumeroInicialNF_Leave(object sender, EventArgs e)
        {
            try
            {
                FinanceiroFacade financeiroFacade = FinanceiroFacade.getInstance();

                /* Verificando se o valor digitado poderá ser utilizado na sequence de nota fiscal  */

                if (financeiroFacade.findLastNumeroNfe() > Convert.ToInt64(textNumeroInicialNF.Text))
                    throw new Exception("O número informado é menor do que o último valor gravado. Utilize outro número.");

                if (string.Equals(textNumeroInicialNF.Text, "0"))
                    throw new Exception("O número informado não poderá ser zero.");

                financeiroFacade.restartSequenceNumeroNfe(Convert.ToInt64(textNumeroInicialNF.Text));

                PermissionamentoFacade permissionamentoFacade = PermissionamentoFacade.getInstance();
                permissionamentoFacade.updateConfiguracao(new Configuracao(configuracoes[8].Id, configuracoes[8].Descricao, textNumeroInicialNF.Text));

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
                textNumeroInicialNF.Focus();

            }
        }
        
        private void textJurosProrrogacao_Enter(object sender, EventArgs e)
        {
            textJurosProrrogacao.Tag = textJurosProrrogacao.Text;
            textJurosProrrogacao.SelectAll();
        }

        private void textJurosProrrogacao_Leave(object sender, EventArgs e)
        {
            try
            {
                if (string.IsNullOrEmpty(textJurosProrrogacao.Text))
                    textJurosProrrogacao.Text = textJurosProrrogacao.Tag.ToString();

                PermissionamentoFacade permissionamentoFacade = PermissionamentoFacade.getInstance();
                permissionamentoFacade.updateConfiguracao(new Configuracao(configuracoes[9].Id, configuracoes[9].Descricao, ValidaCampoHelper.formataValorMonetarioBanco(ValidaCampoHelper.FormataValorMonetario(textJurosProrrogacao.Text))));

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
                textJurosProrrogacao.Focus();

            }
        }

        private void textQtdeTermica_Enter(object sender, EventArgs e)
        {
            textQtdeTermica.Tag = textQtdeTermica.Text;
            textQtdeTermica.SelectAll();
        }

        private void textQtdeTermica_Leave(object sender, EventArgs e)
        {
            try
            {
                if (string.IsNullOrEmpty(textQtdeTermica.Text))
                    textQtdeTermica.Text = textQtdeTermica.Tag.ToString();

                PermissionamentoFacade permissionamentoFacade = PermissionamentoFacade.getInstance();
                permissionamentoFacade.updateConfiguracao(new Configuracao(configuracoes[10].Id, configuracoes[10].Descricao, textQtdeTermica.Text));

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
                textQtdeTermica.Focus();

            }
        }

        private void textNomeEmpresaTermica_Enter(object sender, EventArgs e)
        {
            textNomeEmpresaTermica.Tag = textNomeEmpresaTermica.Text;
            textNomeEmpresaTermica.SelectAll();
        }

        private void textNomeEmpresaTermica_Leave(object sender, EventArgs e)
        {
            try
            {
                if (string.IsNullOrEmpty(textNomeEmpresaTermica.Text))
                    textNomeEmpresaTermica.Text = textNomeEmpresaTermica.Tag.ToString();

                PermissionamentoFacade permissionamentoFacade = PermissionamentoFacade.getInstance();
                permissionamentoFacade.updateConfiguracao(new Configuracao(configuracoes[11].Id, configuracoes[11].Descricao, textNomeEmpresaTermica.Text));

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
                textNomeEmpresaTermica.Focus();

            }
        }

        private void textIPServidorTermica_Enter_1(object sender, EventArgs e)
        {
            textIPServidorTermica.Tag = textIPServidorTermica.Text;
            textIPServidorTermica.SelectAll();
        }

        private void textIPServidorTermica_Leave_1(object sender, EventArgs e)
        {

            try
            {
                if (string.IsNullOrEmpty(textIPServidorTermica.Text))
                    textIPServidorTermica.Text = textIPServidorTermica.Tag.ToString();

                /* validando IP digitado */
                if (!ValidaCampoHelper.ValidaIP(textIPServidorTermica.Text.Trim()))
                    throw new Exception("IP inválido. Reveja os valores digitados.");

                PermissionamentoFacade permissionamentoFacade = PermissionamentoFacade.getInstance();
                permissionamentoFacade.updateConfiguracao(new Configuracao(configuracoes[12].Id, configuracoes[12].Descricao, textIPServidorTermica.Text));

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
                textIPServidorTermica.Focus();

            }

        }

        private void textNomeImpressoraTermica_Enter(object sender, EventArgs e)
        {
            textNomeImpressoraTermica.Tag = textNomeImpressoraTermica.Text;
            textNomeImpressoraTermica.SelectAll();

        }

        private void textNomeImpressoraTermica_Leave(object sender, EventArgs e)
        {
            try
            {
                if (string.IsNullOrEmpty(textNomeImpressoraTermica.Text))
                    textNomeImpressoraTermica.Text = textNomeImpressoraTermica.Tag.ToString();

                PermissionamentoFacade permissionamentoFacade = PermissionamentoFacade.getInstance();
                permissionamentoFacade.updateConfiguracao(new Configuracao(configuracoes[13].Id, configuracoes[13].Descricao, textNomeImpressoraTermica.Text));

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
                textNomeImpressoraTermica.Focus();

            }
        }

        private void cbGeraMovimentoExame_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                PermissionamentoFacade permissionamentoFacade = PermissionamentoFacade.getInstance();
                permissionamentoFacade.updateConfiguracao(new Configuracao(configuracoes[15].Id, configuracoes[15].Descricao, ((SelectItem)cbGeraMovimentoExame.SelectedItem).Valor));
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
                cbGeraMovimentoExame.Focus();
            }
        }

        private void cbGravaProdutoAutomatico_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                 PermissionamentoFacade permissionamentoFacade = PermissionamentoFacade.getInstance();
                 permissionamentoFacade.updateConfiguracao(new Configuracao(configuracoes[18].Id, configuracoes[18].Descricao, ((SelectItem)cbGravaProdutoAutomatico.SelectedItem).Valor));
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
                cbGravaProdutoAutomatico.Focus();
                
            }
        }

        private void btnAso2Via_Click(object sender, EventArgs e)
        {
            try
            {
                frmProdutoBusca produtoBusca = new frmProdutoBusca();
                produtoBusca.ShowDialog();

                if (produtoBusca.Produto != null)
                {
                    aso2Via = produtoBusca.Produto;
                    textAso2Via.Text = Convert.ToString(aso2Via.Id + " - " + aso2Via.Nome);
                    PermissionamentoFacade permissionamentoFacade = PermissionamentoFacade.getInstance();
                    permissionamentoFacade.updateConfiguracao(new Configuracao(configuracoes[19].Id, configuracoes[19].Descricao, aso2Via.Id.ToString()));
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
                btnAso2Via.Focus();
            }
        }

        private void btnAsoTranscrito_Click(object sender, EventArgs e)
        {
            try
            {
                frmProdutoBusca produtoBusca = new frmProdutoBusca();
                produtoBusca.ShowDialog();

                if (produtoBusca.Produto != null)
                {
                    asoTranscrito = produtoBusca.Produto;
                    textAsoTranscrito.Text = Convert.ToString(asoTranscrito.Id + " - " + asoTranscrito.Nome);
                    PermissionamentoFacade permissionamentoFacade = PermissionamentoFacade.getInstance();
                    permissionamentoFacade.updateConfiguracao(new Configuracao(configuracoes[20].Id, configuracoes[20].Descricao, asoTranscrito.Id.ToString()));
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
                btnAso2Via.Focus();
            }
        }

        private void btnAsoInclusaoExame_Click(object sender, EventArgs e)
        {
            try
            {
                frmProdutoBusca produtoBusca = new frmProdutoBusca();
                produtoBusca.ShowDialog();

                if (produtoBusca.Produto != null)
                {
                    asoInclusaoExames = produtoBusca.Produto;
                    textAsoInclusaoExame.Text = Convert.ToString(asoInclusaoExames.Id + " - " + asoInclusaoExames.Nome);
                    PermissionamentoFacade permissionamentoFacade = PermissionamentoFacade.getInstance();
                    permissionamentoFacade.updateConfiguracao(new Configuracao(configuracoes[21].Id, configuracoes[21].Descricao, asoInclusaoExames.Id.ToString()));
                    
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
                btnAso2Via.Focus();
            }
        }

        private void textCidade_Enter(object sender, EventArgs e)
        {
            textCidade.Tag = textCidade.Text;
            textCidade.SelectAll();
        }

        private void textCidade_Leave(object sender, EventArgs e)
        {
            try
            {
                if (string.IsNullOrEmpty(textCidade.Text))
                    textCidade.Text = textCidade.Tag.ToString();

                PermissionamentoFacade permissionamentoFacade = PermissionamentoFacade.getInstance();
                permissionamentoFacade.updateConfiguracao(new Configuracao(configuracoes[22].Id, configuracoes[22].Descricao, textCidade.Text));

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
                textCidade.Focus();

            }
        }

        private void textNomeEmpresa_Enter(object sender, EventArgs e)
        {
            textNomeEmpresa.Tag = textNomeEmpresa.Text;
            textNomeEmpresa.SelectAll();
        }

        private void textNomeEmpresa_Leave(object sender, EventArgs e)
        {
            try
            {
                if (string.IsNullOrEmpty(textNomeEmpresa.Text))
                    textNomeEmpresa.Text = textNomeEmpresa.Tag.ToString();

                PermissionamentoFacade permissionamentoFacade = PermissionamentoFacade.getInstance();
                permissionamentoFacade.updateConfiguracao(new Configuracao(configuracoes[23].Id, configuracoes[23].Descricao, textNomeEmpresa.Text));

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
                textNomeEmpresa.Focus();

            }
        }

        private void textEndereco_Enter(object sender, EventArgs e)
        {
            textEndereco.Tag = textEndereco.Text;
            textEndereco.SelectAll();
        }

        private void textEndereco_Leave(object sender, EventArgs e)
        {
            try
            {
                if (string.IsNullOrEmpty(textEndereco.Text))
                    textEndereco.Text = textEndereco.Tag.ToString();

                PermissionamentoFacade permissionamentoFacade = PermissionamentoFacade.getInstance();
                permissionamentoFacade.updateConfiguracao(new Configuracao(configuracoes[24].Id, configuracoes[24].Descricao, textEndereco.Text));

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
                textEndereco.Focus();

            }
        }

        private void textNumero_Enter(object sender, EventArgs e)
        {
            textNumero.Tag = textNumero.Text;
            textNumero.SelectAll();
        }

        private void textNumero_Leave(object sender, EventArgs e)
        {
            try
            {
                if (string.IsNullOrEmpty(textNumero.Text))
                    textNumero.Text = textNumero.Tag.ToString();

                PermissionamentoFacade permissionamentoFacade = PermissionamentoFacade.getInstance();
                permissionamentoFacade.updateConfiguracao(new Configuracao(configuracoes[25].Id, configuracoes[25].Descricao, textNumero.Text));

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
                textNumero.Focus();

            }
        }

        private void textComplemento_Leave(object sender, EventArgs e)
        {
            try
            {
                PermissionamentoFacade permissionamentoFacade = PermissionamentoFacade.getInstance();
                permissionamentoFacade.updateConfiguracao(new Configuracao(configuracoes[26].Id, configuracoes[26].Descricao, textComplemento.Text));

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
                textComplemento.Focus();

            }
        }

        private void textBairro_Enter(object sender, EventArgs e)
        {
            textBairro.Tag = textBairro.Text;
            textBairro.SelectAll();
        }

        private void textBairro_Leave(object sender, EventArgs e)
        {
            try
            {
                if (string.IsNullOrEmpty(textBairro.Text))
                    textBairro.Text = textBairro.Tag.ToString();

                PermissionamentoFacade permissionamentoFacade = PermissionamentoFacade.getInstance();
                permissionamentoFacade.updateConfiguracao(new Configuracao(configuracoes[27].Id, configuracoes[27].Descricao, textBairro.Text));

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
                textBairro.Focus();

            }
        }

        private void textUF_Enter(object sender, EventArgs e)
        {
            textUF.Tag = textUF.Text;
            textUF.SelectAll();
        }

        private void textUF_Leave(object sender, EventArgs e)
        {
            try
            {
                if (string.IsNullOrEmpty(textUF.Text))
                    textUF.Text = textUF.Tag.ToString();

                PermissionamentoFacade permissionamentoFacade = PermissionamentoFacade.getInstance();
                permissionamentoFacade.updateConfiguracao(new Configuracao(configuracoes[28].Id, configuracoes[28].Descricao, textUF.Text));

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
                textUF.Focus();

            }
        }

        private void textCEP_Enter(object sender, EventArgs e)
        {
            textCEP.Tag = textCEP.Text.Replace(".", "").Replace("-", "").Trim();
            textCEP.SelectionStart = 0;
        }

        private void textCEP_Leave(object sender, EventArgs e)
        {
            try
            {
                if (string.IsNullOrEmpty(textCEP.Text.Replace(".","").Replace("-","").Trim()))
                    textCEP.Text = textCEP.Tag.ToString();

                PermissionamentoFacade permissionamentoFacade = PermissionamentoFacade.getInstance();
                permissionamentoFacade.updateConfiguracao(new Configuracao(configuracoes[29].Id, configuracoes[29].Descricao, textCEP.Text.Replace(".","").Replace("-","").Trim()));

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
                textCEP.Focus();

            }
        }

        private void textCNPJ_Enter(object sender, EventArgs e)
        {
            textCNPJ.Tag = textCNPJ.Text.Replace(".", "").Replace("-", "").Replace("/", "").Trim();
            textCNPJ.SelectionStart = 0;

        }

        private void textCNPJ_Leave(object sender, EventArgs e)
        {
            try
            {
                if (string.IsNullOrEmpty(textCNPJ.Text.Replace(".","").Replace("-","").Replace("/","").Trim()))
                    textCNPJ.Text = textCNPJ.Tag.ToString();

                PermissionamentoFacade permissionamentoFacade = PermissionamentoFacade.getInstance();
                permissionamentoFacade.updateConfiguracao(new Configuracao(configuracoes[30].Id, configuracoes[30].Descricao, textCNPJ.Text));

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
                textCNPJ.Focus();

            }
        }

        private void textInscricao_Enter(object sender, EventArgs e)
        {
            textInscricao.Tag = textInscricao.Text;
            textInscricao.SelectAll();
        }

        private void textInscricao_Leave(object sender, EventArgs e)
        {
            try
            {
                PermissionamentoFacade permissionamentoFacade = PermissionamentoFacade.getInstance();
                permissionamentoFacade.updateConfiguracao(new Configuracao(configuracoes[31].Id, configuracoes[31].Descricao, textInscricao.Text));

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
                textInscricao.Focus();

            }
        }

        private void textEmail_Enter(object sender, EventArgs e)
        {
            textEmail.Tag = textEmail.Text;
            textEmail.SelectAll();
        }

        private void textEmail_Leave(object sender, EventArgs e)
        {
            try
            {
                if (string.IsNullOrEmpty(textEmail.Text))
                    textEmail.Text = textEmail.Tag.ToString();

                PermissionamentoFacade permissionamentoFacade = PermissionamentoFacade.getInstance();
                permissionamentoFacade.updateConfiguracao(new Configuracao(configuracoes[32].Id, configuracoes[32].Descricao, textEmail.Text));

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
                textEmail.Focus();

            }
        }

        private void textServidorSMTP_Enter(object sender, EventArgs e)
        {
            textServidorSMTP.Tag = textServidorSMTP.Text;
            textServidorSMTP.SelectAll();
        }

        private void textServidorSMTP_Leave(object sender, EventArgs e)
        {
            try
            {
                if (string.IsNullOrEmpty(textServidorSMTP.Text))
                    textServidorSMTP.Text = textServidorSMTP.Tag.ToString();

                PermissionamentoFacade permissionamentoFacade = PermissionamentoFacade.getInstance();
                permissionamentoFacade.updateConfiguracao(new Configuracao(configuracoes[33].Id, configuracoes[33].Descricao, textServidorSMTP.Text));

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
                textServidorSMTP.Focus();

            }
        }

        private void textNumeroPorta_Enter(object sender, EventArgs e)
        {
            textNumeroPorta.Tag = textNumeroPorta.Text;
            textNumeroPorta.SelectAll();
        }

        private void textNumeroPorta_Leave(object sender, EventArgs e)
        {
            try
            {
                if (string.IsNullOrEmpty(textNumeroPorta.Text))
                    textNumeroPorta.Text = textNumeroPorta.Tag.ToString();

                PermissionamentoFacade permissionamentoFacade = PermissionamentoFacade.getInstance();
                permissionamentoFacade.updateConfiguracao(new Configuracao(configuracoes[34].Id, configuracoes[34].Descricao, textNumeroPorta.Text));

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
                textNumeroPorta.Focus();

            }
        }

        private void cbSSL_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                 PermissionamentoFacade permissionamentoFacade = PermissionamentoFacade.getInstance();
                 permissionamentoFacade.updateConfiguracao(new Configuracao(configuracoes[35].Id, configuracoes[35].Descricao, ((SelectItem)cbSSL.SelectedItem).Valor));
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
                cbSSL.Focus();

            }
        }

        private void textDiretorio_Enter(object sender, EventArgs e)
        {
            textDiretorio.SelectAll();
        }

        private void textComplemento_Enter(object sender, EventArgs e)
        {
            textComplemento.Tag = textComplemento.Text;
            textComplemento.SelectAll();
        }

        private void textEmailEnvio_Enter(object sender, EventArgs e)
        {
            textEmailEnvio.Tag = textEmailEnvio.Text;
            textEmailEnvio.SelectAll();
        }

        private void textEmailEnvio_Leave(object sender, EventArgs e)
        {
            try
            {
                if (string.IsNullOrEmpty(textEmailEnvio.Text))
                    textEmailEnvio.Text = textEmailEnvio.Tag.ToString();

                PermissionamentoFacade permissionamentoFacade = PermissionamentoFacade.getInstance();
                permissionamentoFacade.updateConfiguracao(new Configuracao(configuracoes[36].Id, configuracoes[36].Descricao, textEmailEnvio.Text));

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
                textEmailEnvio.Focus();

            }
        }

        private void textSenhaEmail_Enter(object sender, EventArgs e)
        {
            textSenhaEmail.Tag = textSenhaEmail.Text;
            textSenhaEmail.SelectAll();
        }

        private void textSenhaEmail_Leave(object sender, EventArgs e)
        {
            try
            {
                if (string.IsNullOrEmpty(textSenhaEmail.Text))
                    textSenhaEmail.Text = textSenhaEmail.Tag.ToString();

                PermissionamentoFacade permissionamentoFacade = PermissionamentoFacade.getInstance();
                permissionamentoFacade.updateConfiguracao(new Configuracao(configuracoes[37].Id, configuracoes[37].Descricao, textSenhaEmail.Text));

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
                textSenhaEmail.Focus();

            }
        }

        private void cbUtilizaFilial_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                PermissionamentoFacade permissionamentoFacade = PermissionamentoFacade.getInstance();
                permissionamentoFacade.updateConfiguracao(new Configuracao(configuracoes[38].Id, configuracoes[38].Descricao, ((SelectItem)cbUtilizaFilial.SelectedItem).Valor));
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
                cbUtilizaFilial.Focus();

            }
        }

        private void textDiasPesquisa_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = ValidaCampoHelper.FormataDigitacaoNumero(sender, e, textDiasPesquisa.Text, 2);
        }

        private void textDiasPesquisa_Leave(object sender, EventArgs e)
        {
            try
            {
                if (string.IsNullOrEmpty(textDiasPesquisa.Text))
                    textDiasPesquisa.Text = textDiasPesquisa.Tag.ToString();

                PermissionamentoFacade permissionamentoFacade = PermissionamentoFacade.getInstance();
                permissionamentoFacade.updateConfiguracao(new Configuracao(configuracoes[39].Id, configuracoes[39].Descricao, textDiasPesquisa.Text));

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
                textDiasPesquisa.Focus();

            }
        }

        private void textDiasPesquisa_Enter(object sender, EventArgs e)
        {
            textDiasPesquisa.Tag = textDiasPesquisa.Text;
            textDiasPesquisa.SelectAll();
        }

        private void cbImprimeSala_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                PermissionamentoFacade permissionamentoFacade = PermissionamentoFacade.getInstance();
                permissionamentoFacade.updateConfiguracao(new Configuracao(configuracoes[40].Id, configuracoes[40].Descricao, ((SelectItem)this.cbImprimeSala.SelectedItem).Valor));
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
                cbImprimeSala.Focus();

            }
        }

        private void cbPcmsoVencido_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                PermissionamentoFacade permissionamentoFacade = PermissionamentoFacade.getInstance();
                permissionamentoFacade.updateConfiguracao(new Configuracao(configuracoes[41].Id, configuracoes[41].Descricao, ((SelectItem)this.cbPcmsoVencido.SelectedItem).Valor));
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
                cbPcmsoVencido.Focus();
            }
        }

        private void cbOrdenacaoAtendimento_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                PermissionamentoFacade permissionamentoFacade = PermissionamentoFacade.getInstance();
                permissionamentoFacade.updateConfiguracao(new Configuracao(configuracoes[42].Id, configuracoes[42].Descricao, ((SelectItem)this.cbOrdenacaoAtendimento.SelectedItem).Valor));
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
                cbOrdenacaoAtendimento.Focus();
            }
        }

        private void cbCancelaExameAtendimentoFinalizado_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                PermissionamentoFacade permissionamentoFacade = PermissionamentoFacade.getInstance();
                permissionamentoFacade.updateConfiguracao(new Configuracao(configuracoes[44].Id, configuracoes[44].Descricao, ((SelectItem)this.cbCancelaExameAtendimentoFinalizado.SelectedItem).Valor));
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
                cbCancelaExameAtendimentoFinalizado.Focus();
            }
        }

        private void cbAtendimentoBloqueado_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                PermissionamentoFacade permissionamentoFacade = PermissionamentoFacade.getInstance();
                permissionamentoFacade.updateConfiguracao(new Configuracao(configuracoes[45].Id, configuracoes[45].Descricao, ((SelectItem)this.cbAtendimentoBloqueado.SelectedItem).Valor));
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
                cbAtendimentoBloqueado.Focus();
            }

        }

        private void cbMedicoExaminador_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                PermissionamentoFacade permissionamentoFacade = PermissionamentoFacade.getInstance();
                permissionamentoFacade.updateConfiguracao(new Configuracao(configuracoes[46].Id, configuracoes[46].Descricao, ((SelectItem)this.cbMedicoExaminador.SelectedItem).Valor));
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
                cbMedicoExaminador.Focus();
            }
        }

        private void cbConverterEsocial2220_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                PermissionamentoFacade permissionamentoFacade = PermissionamentoFacade.getInstance();
                permissionamentoFacade.updateConfiguracao(new Configuracao(configuracoes[48].Id, configuracoes[48].Descricao, ((SelectItem)this.cbConverterEsocial2220.SelectedItem).Valor));
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
                cbConverterEsocial2220.Focus();
            }
        }

        private void textLocalntegracao_Enter(object sender, EventArgs e)
        {
            textLocalntegracao.Tag = textLocalntegracao.Text;
            textLocalntegracao.SelectAll();
        }

        private void textLocalntegracao_Leave(object sender, EventArgs e)
        {
            try
            {
                if (string.IsNullOrEmpty(textLocalntegracao.Text))
                    textLocalntegracao.Text = textLocalntegracao.Tag.ToString();

                PermissionamentoFacade permissionamentoFacade = PermissionamentoFacade.getInstance();
                permissionamentoFacade.updateConfiguracao(new Configuracao(configuracoes[49].Id, configuracoes[49].Descricao, textLocalntegracao.Text));

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
                textLocalntegracao.Focus();

            }
        }

        private void btnAnaliseRisco_Click(object sender, EventArgs e)
        {
            try
            {
                frmExameBuscar buscarExame = new frmExameBuscar(false);
                buscarExame.ShowDialog();

                if (buscarExame.Exame != null)
                {
                    classificacaoRisco = buscarExame.Exame;
                    this.textAnaliseRisco.Text = Convert.ToString(this.classificacaoRisco.Id + " - " + this.classificacaoRisco.Descricao);
                    PermissionamentoFacade permissionamentoFacade = PermissionamentoFacade.getInstance();
                    permissionamentoFacade.updateConfiguracao(new Configuracao(configuracoes[50].Id, configuracoes[50].Descricao, this.classificacaoRisco.Id.ToString()));

                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
                btnAso2Via.Focus();
            }

        }
    }
}
