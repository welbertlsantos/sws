﻿namespace SWS.View
{
    partial class frm_PpraEpiIncluir
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frm_PpraEpiIncluir));
            this.grb_dados = new System.Windows.Forms.GroupBox();
            this.text_finalidade = new System.Windows.Forms.TextBox();
            this.bt_buscar = new System.Windows.Forms.Button();
            this.lbl_nome = new System.Windows.Forms.Label();
            this.text_descricao = new System.Windows.Forms.TextBox();
            this.grb_epi = new System.Windows.Forms.GroupBox();
            this.grb_tipoEpi = new System.Windows.Forms.GroupBox();
            this.rb_epc = new System.Windows.Forms.RadioButton();
            this.rb_epi = new System.Windows.Forms.RadioButton();
            this.rb_medidaControle = new System.Windows.Forms.RadioButton();
            this.grp_tipoEpi = new System.Windows.Forms.GroupBox();
            this.cb_tipoEpi = new System.Windows.Forms.ComboBox();
            this.grb_paginacao = new System.Windows.Forms.GroupBox();
            this.btn_fechar = new System.Windows.Forms.Button();
            this.btn_ok = new System.Windows.Forms.Button();
            this.grb_dados.SuspendLayout();
            this.grb_epi.SuspendLayout();
            this.grb_tipoEpi.SuspendLayout();
            this.grp_tipoEpi.SuspendLayout();
            this.grb_paginacao.SuspendLayout();
            this.SuspendLayout();
            // 
            // grb_dados
            // 
            this.grb_dados.Controls.Add(this.text_finalidade);
            this.grb_dados.Controls.Add(this.bt_buscar);
            this.grb_dados.Controls.Add(this.lbl_nome);
            this.grb_dados.Controls.Add(this.text_descricao);
            this.grb_dados.Location = new System.Drawing.Point(3, 2);
            this.grb_dados.Name = "grb_dados";
            this.grb_dados.Size = new System.Drawing.Size(594, 139);
            this.grb_dados.TabIndex = 35;
            this.grb_dados.TabStop = false;
            this.grb_dados.Text = "Dados";
            // 
            // text_finalidade
            // 
            this.text_finalidade.BackColor = System.Drawing.Color.White;
            this.text_finalidade.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.text_finalidade.ForeColor = System.Drawing.Color.Blue;
            this.text_finalidade.Location = new System.Drawing.Point(6, 58);
            this.text_finalidade.MaxLength = 255;
            this.text_finalidade.Multiline = true;
            this.text_finalidade.Name = "text_finalidade";
            this.text_finalidade.ReadOnly = true;
            this.text_finalidade.Size = new System.Drawing.Size(579, 40);
            this.text_finalidade.TabIndex = 0;
            this.text_finalidade.TabStop = false;
            // 
            // bt_buscar
            // 
            this.bt_buscar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.bt_buscar.Image = global::SWS.Properties.Resources.lupa;
            this.bt_buscar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.bt_buscar.Location = new System.Drawing.Point(6, 104);
            this.bt_buscar.Name = "bt_buscar";
            this.bt_buscar.Size = new System.Drawing.Size(77, 23);
            this.bt_buscar.TabIndex = 1;
            this.bt_buscar.Text = "&Buscar";
            this.bt_buscar.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.bt_buscar.UseVisualStyleBackColor = true;
            this.bt_buscar.Click += new System.EventHandler(this.bt_buscar_Click);
            // 
            // lbl_nome
            // 
            this.lbl_nome.AutoSize = true;
            this.lbl_nome.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_nome.Location = new System.Drawing.Point(3, 16);
            this.lbl_nome.Name = "lbl_nome";
            this.lbl_nome.Size = new System.Drawing.Size(55, 13);
            this.lbl_nome.TabIndex = 7;
            this.lbl_nome.Text = "Descrição";
            // 
            // text_descricao
            // 
            this.text_descricao.BackColor = System.Drawing.Color.White;
            this.text_descricao.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.text_descricao.ForeColor = System.Drawing.Color.Blue;
            this.text_descricao.Location = new System.Drawing.Point(6, 32);
            this.text_descricao.MaxLength = 100;
            this.text_descricao.Name = "text_descricao";
            this.text_descricao.ReadOnly = true;
            this.text_descricao.Size = new System.Drawing.Size(579, 20);
            this.text_descricao.TabIndex = 0;
            this.text_descricao.TabStop = false;
            // 
            // grb_epi
            // 
            this.grb_epi.Controls.Add(this.grb_tipoEpi);
            this.grb_epi.Controls.Add(this.grp_tipoEpi);
            this.grb_epi.Location = new System.Drawing.Point(3, 147);
            this.grb_epi.Name = "grb_epi";
            this.grb_epi.Size = new System.Drawing.Size(594, 194);
            this.grb_epi.TabIndex = 36;
            this.grb_epi.TabStop = false;
            this.grb_epi.Text = "Aplicação";
            // 
            // grb_tipoEpi
            // 
            this.grb_tipoEpi.Controls.Add(this.rb_epc);
            this.grb_tipoEpi.Controls.Add(this.rb_epi);
            this.grb_tipoEpi.Controls.Add(this.rb_medidaControle);
            this.grb_tipoEpi.Location = new System.Drawing.Point(201, 19);
            this.grb_tipoEpi.Name = "grb_tipoEpi";
            this.grb_tipoEpi.Size = new System.Drawing.Size(237, 102);
            this.grb_tipoEpi.TabIndex = 2;
            this.grb_tipoEpi.TabStop = false;
            // 
            // rb_epc
            // 
            this.rb_epc.AutoSize = true;
            this.rb_epc.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.rb_epc.Location = new System.Drawing.Point(6, 65);
            this.rb_epc.Name = "rb_epc";
            this.rb_epc.Size = new System.Drawing.Size(188, 17);
            this.rb_epc.TabIndex = 5;
            this.rb_epc.TabStop = true;
            this.rb_epc.Text = "Equipamento de Proteçao Coletiva";
            this.rb_epc.UseVisualStyleBackColor = true;
            // 
            // rb_epi
            // 
            this.rb_epi.AutoSize = true;
            this.rb_epi.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.rb_epi.Location = new System.Drawing.Point(6, 41);
            this.rb_epi.Name = "rb_epi";
            this.rb_epi.Size = new System.Drawing.Size(195, 17);
            this.rb_epi.TabIndex = 4;
            this.rb_epi.TabStop = true;
            this.rb_epi.Text = "Equipamento de Proteção Individual";
            this.rb_epi.UseVisualStyleBackColor = true;
            // 
            // rb_medidaControle
            // 
            this.rb_medidaControle.AutoSize = true;
            this.rb_medidaControle.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.rb_medidaControle.Location = new System.Drawing.Point(6, 18);
            this.rb_medidaControle.Name = "rb_medidaControle";
            this.rb_medidaControle.Size = new System.Drawing.Size(116, 17);
            this.rb_medidaControle.TabIndex = 3;
            this.rb_medidaControle.TabStop = true;
            this.rb_medidaControle.Text = "Medida de Controle";
            this.rb_medidaControle.UseVisualStyleBackColor = true;
            this.rb_medidaControle.CheckedChanged += new System.EventHandler(this.rb_medidaControle_CheckedChanged);
            // 
            // grp_tipoEpi
            // 
            this.grp_tipoEpi.Controls.Add(this.cb_tipoEpi);
            this.grp_tipoEpi.Location = new System.Drawing.Point(6, 19);
            this.grp_tipoEpi.Name = "grp_tipoEpi";
            this.grp_tipoEpi.Size = new System.Drawing.Size(163, 60);
            this.grp_tipoEpi.TabIndex = 1;
            this.grp_tipoEpi.TabStop = false;
            // 
            // cb_tipoEpi
            // 
            this.cb_tipoEpi.BackColor = System.Drawing.Color.White;
            this.cb_tipoEpi.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cb_tipoEpi.FormattingEnabled = true;
            this.cb_tipoEpi.Location = new System.Drawing.Point(6, 19);
            this.cb_tipoEpi.Name = "cb_tipoEpi";
            this.cb_tipoEpi.Size = new System.Drawing.Size(151, 21);
            this.cb_tipoEpi.TabIndex = 2;
            // 
            // grb_paginacao
            // 
            this.grb_paginacao.Controls.Add(this.btn_fechar);
            this.grb_paginacao.Controls.Add(this.btn_ok);
            this.grb_paginacao.Location = new System.Drawing.Point(3, 347);
            this.grb_paginacao.Name = "grb_paginacao";
            this.grb_paginacao.Size = new System.Drawing.Size(594, 51);
            this.grb_paginacao.TabIndex = 37;
            this.grb_paginacao.TabStop = false;
            // 
            // btn_fechar
            // 
            this.btn_fechar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_fechar.Image = global::SWS.Properties.Resources.close;
            this.btn_fechar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btn_fechar.Location = new System.Drawing.Point(92, 19);
            this.btn_fechar.Name = "btn_fechar";
            this.btn_fechar.Size = new System.Drawing.Size(77, 23);
            this.btn_fechar.TabIndex = 7;
            this.btn_fechar.Text = "&Fechar";
            this.btn_fechar.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btn_fechar.UseVisualStyleBackColor = true;
            this.btn_fechar.Click += new System.EventHandler(this.btn_fechar_Click);
            // 
            // btn_ok
            // 
            this.btn_ok.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_ok.Image = global::SWS.Properties.Resources.fechar_ico;
            this.btn_ok.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btn_ok.Location = new System.Drawing.Point(9, 19);
            this.btn_ok.Name = "btn_ok";
            this.btn_ok.Size = new System.Drawing.Size(77, 23);
            this.btn_ok.TabIndex = 6;
            this.btn_ok.Text = "&OK";
            this.btn_ok.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btn_ok.UseVisualStyleBackColor = true;
            this.btn_ok.Click += new System.EventHandler(this.btn_ok_Click);
            // 
            // frm_PpraEpiIncluir
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(600, 400);
            this.ControlBox = false;
            this.Controls.Add(this.grb_paginacao);
            this.Controls.Add(this.grb_epi);
            this.Controls.Add(this.grb_dados);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "frm_PpraEpiIncluir";
            this.Text = "PPRA - EPI";
            this.grb_dados.ResumeLayout(false);
            this.grb_dados.PerformLayout();
            this.grb_epi.ResumeLayout(false);
            this.grb_tipoEpi.ResumeLayout(false);
            this.grb_tipoEpi.PerformLayout();
            this.grp_tipoEpi.ResumeLayout(false);
            this.grb_paginacao.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox grb_dados;
        private System.Windows.Forms.Button bt_buscar;
        private System.Windows.Forms.Label lbl_nome;
        public System.Windows.Forms.TextBox text_descricao;
        public System.Windows.Forms.TextBox text_finalidade;
        private System.Windows.Forms.GroupBox grb_epi;
        private System.Windows.Forms.GroupBox grb_paginacao;
        private System.Windows.Forms.Button btn_fechar;
        private System.Windows.Forms.Button btn_ok;
        private System.Windows.Forms.GroupBox grp_tipoEpi;
        private System.Windows.Forms.ComboBox cb_tipoEpi;
        private System.Windows.Forms.GroupBox grb_tipoEpi;
        private System.Windows.Forms.RadioButton rb_epc;
        private System.Windows.Forms.RadioButton rb_epi;
        private System.Windows.Forms.RadioButton rb_medidaControle;
    }
}