﻿using SWS.Entidade;
using SWS.View.ViewHelper;
using SWS.ViewHelper;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace SWS.View
{
    public partial class frmContatoAlterar : frmContatoIncluir
    {
        public frmContatoAlterar(Contato contato) : base()
        {
            InitializeComponent();
            this.contato = contato;

            /* populando dados do contato*/
            ComboHelper.findTipoContato(cbTipoContato);
            cbTipoContato.SelectedValue = contato.TipoContato.Id.ToString();

            textNome.Text = contato.Nome;
            textCPF.Text = contato.Cpf;
            textRG.Text = contato.Rg;
            if (contato.DataNascimento != null)
            {
                dataNascimento.Checked = true;
                dataNascimento.Text = Convert.ToString(contato.DataNascimento);
            }

            textEndereco.Text = contato.Endereco;
            textNumero.Text = contato.Numero;
            textComplemento.Text = contato.Complemento;
            textBairro.Text = contato.Bairro;
            ComboHelper.unidadeFederativa(cbUF);
            cbUF.SelectedValue = contato.Uf;
            if (contato.Cidade != null)
            {
                cidade = contato.Cidade;
                textCidade.Text = contato.Cidade.Nome;
            }

            textCEP.Text = contato.Cep;
            textTelefone.Text = contato.Telefone;
            textCelular.Text = contato.Celular;
            textPis.Text = contato.PisPasep;
            textEmail.Text = contato.Email;

            cbResponsavel.SelectedValue = contato.ResponsavelContrato == true ? "true" : "false";
            
        }

        protected override void btGravar_Click(object sender, EventArgs e)
        {
            try
            {
                if (validaCampos())
                {
                    contato = new Contato(contato.Id, cidade, new TipoContato(Convert.ToInt64(((SelectItem)cbTipoContato.SelectedItem).Valor), ((SelectItem)cbTipoContato.SelectedItem).Nome), null, textNome.Text.Trim(), textCPF.Text, textRG.Text.Trim(), dataNascimento.Checked ? (DateTime?)Convert.ToDateTime(dataNascimento.Text) : null, textEndereco.Text.Trim(), textNumero.Text.Trim(), textComplemento.Text.Trim(), textBairro.Text.Trim(), ((SelectItem)cbUF.SelectedItem).Valor, textCEP.Text, textTelefone.Text.Trim(), textCelular.Text.Trim(), textPis.Text.Trim(), textEmail.Text.Trim(), Convert.ToBoolean(((SelectItem)cbResponsavel.SelectedItem).Valor), Convert.ToBoolean(((SelectItem)cbResponsavelEstudo.SelectedItem).Valor)); 
                    this.Close();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atençao", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
    }
}
