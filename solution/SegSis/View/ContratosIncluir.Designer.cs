﻿namespace SWS.View
{
    partial class frmContratosIncluir
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmContratosIncluir));
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            this.flpAcao = new System.Windows.Forms.FlowLayoutPanel();
            this.btnGravar = new System.Windows.Forms.Button();
            this.btnFechar = new System.Windows.Forms.Button();
            this.lblCodigo = new System.Windows.Forms.TextBox();
            this.textCodigo = new System.Windows.Forms.MaskedTextBox();
            this.lblDataElaboracao = new System.Windows.Forms.TextBox();
            this.dataElaboracao = new System.Windows.Forms.DateTimePicker();
            this.lblDataInicio = new System.Windows.Forms.TextBox();
            this.dataInicio = new System.Windows.Forms.DateTimePicker();
            this.lblDataTermino = new System.Windows.Forms.TextBox();
            this.dataTermino = new System.Windows.Forms.DateTimePicker();
            this.lblCliente = new System.Windows.Forms.TextBox();
            this.textCliente = new System.Windows.Forms.TextBox();
            this.btnCliente = new System.Windows.Forms.Button();
            this.btnPrestador = new System.Windows.Forms.Button();
            this.textPrestador = new System.Windows.Forms.TextBox();
            this.lblPrestador = new System.Windows.Forms.TextBox();
            this.grbExame = new System.Windows.Forms.GroupBox();
            this.dgvExame = new System.Windows.Forms.DataGridView();
            this.btnExcluirExame = new System.Windows.Forms.Button();
            this.btnIncluirExame = new System.Windows.Forms.Button();
            this.btnExcluirProduto = new System.Windows.Forms.Button();
            this.btnIncluirProduto = new System.Windows.Forms.Button();
            this.grbProdutos = new System.Windows.Forms.GroupBox();
            this.dgvProduto = new System.Windows.Forms.DataGridView();
            this.lblBloqueio = new System.Windows.Forms.TextBox();
            this.lblTempoBloqueio = new System.Windows.Forms.TextBox();
            this.lblMensalidade = new System.Windows.Forms.TextBox();
            this.lblCobrancaAutomatica = new System.Windows.Forms.TextBox();
            this.lblValorMensal = new System.Windows.Forms.TextBox();
            this.lblNumeroVidas = new System.Windows.Forms.TextBox();
            this.lblValorNumeroVidas = new System.Windows.Forms.TextBox();
            this.lblDiaCobranca = new System.Windows.Forms.TextBox();
            this.grbComentario = new System.Windows.Forms.GroupBox();
            this.textComentario = new System.Windows.Forms.TextBox();
            this.cbBloqueio = new SWS.ComboBoxWithBorder();
            this.cbValorMensal = new SWS.ComboBoxWithBorder();
            this.cbNumeroVidas = new SWS.ComboBoxWithBorder();
            this.cbCobrancaAutomatica = new SWS.ComboBoxWithBorder();
            this.textDiasBloqueio = new System.Windows.Forms.TextBox();
            this.textValorMensalidade = new System.Windows.Forms.TextBox();
            this.textValorNumeroVidas = new System.Windows.Forms.TextBox();
            this.textDiaCobranca = new System.Windows.Forms.TextBox();
            this.flpProduto = new System.Windows.Forms.FlowLayoutPanel();
            this.flpExame = new System.Windows.Forms.FlowLayoutPanel();
            this.lblEmpresa = new System.Windows.Forms.TextBox();
            this.lblCnjEmpresa = new System.Windows.Forms.TextBox();
            this.textEmpresa = new System.Windows.Forms.TextBox();
            this.textCNPJ = new System.Windows.Forms.TextBox();
            this.lblFantasia = new System.Windows.Forms.TextBox();
            this.textNomeFantasia = new System.Windows.Forms.TextBox();
            ((System.ComponentModel.ISupportInitialize)(this.spForm)).BeginInit();
            this.spForm.Panel1.SuspendLayout();
            this.spForm.Panel2.SuspendLayout();
            this.spForm.SuspendLayout();
            this.pnlForm.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.banner)).BeginInit();
            this.flpAcao.SuspendLayout();
            this.grbExame.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvExame)).BeginInit();
            this.grbProdutos.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvProduto)).BeginInit();
            this.grbComentario.SuspendLayout();
            this.flpProduto.SuspendLayout();
            this.flpExame.SuspendLayout();
            this.SuspendLayout();
            // 
            // spForm
            // 
            // 
            // spForm.Panel1
            // 
            this.spForm.Panel1.Controls.Add(this.flpAcao);
            // 
            // pnlForm
            // 
            this.pnlForm.Controls.Add(this.textNomeFantasia);
            this.pnlForm.Controls.Add(this.lblFantasia);
            this.pnlForm.Controls.Add(this.textCNPJ);
            this.pnlForm.Controls.Add(this.textEmpresa);
            this.pnlForm.Controls.Add(this.lblCnjEmpresa);
            this.pnlForm.Controls.Add(this.lblEmpresa);
            this.pnlForm.Controls.Add(this.flpExame);
            this.pnlForm.Controls.Add(this.flpProduto);
            this.pnlForm.Controls.Add(this.lblDataInicio);
            this.pnlForm.Controls.Add(this.lblDataElaboracao);
            this.pnlForm.Controls.Add(this.textDiaCobranca);
            this.pnlForm.Controls.Add(this.textValorNumeroVidas);
            this.pnlForm.Controls.Add(this.textValorMensalidade);
            this.pnlForm.Controls.Add(this.textDiasBloqueio);
            this.pnlForm.Controls.Add(this.cbCobrancaAutomatica);
            this.pnlForm.Controls.Add(this.cbNumeroVidas);
            this.pnlForm.Controls.Add(this.cbValorMensal);
            this.pnlForm.Controls.Add(this.cbBloqueio);
            this.pnlForm.Controls.Add(this.grbComentario);
            this.pnlForm.Controls.Add(this.lblDiaCobranca);
            this.pnlForm.Controls.Add(this.lblValorNumeroVidas);
            this.pnlForm.Controls.Add(this.lblNumeroVidas);
            this.pnlForm.Controls.Add(this.lblValorMensal);
            this.pnlForm.Controls.Add(this.lblCobrancaAutomatica);
            this.pnlForm.Controls.Add(this.lblMensalidade);
            this.pnlForm.Controls.Add(this.lblTempoBloqueio);
            this.pnlForm.Controls.Add(this.lblBloqueio);
            this.pnlForm.Controls.Add(this.grbProdutos);
            this.pnlForm.Controls.Add(this.grbExame);
            this.pnlForm.Controls.Add(this.btnPrestador);
            this.pnlForm.Controls.Add(this.textPrestador);
            this.pnlForm.Controls.Add(this.lblPrestador);
            this.pnlForm.Controls.Add(this.textCodigo);
            this.pnlForm.Controls.Add(this.btnCliente);
            this.pnlForm.Controls.Add(this.textCliente);
            this.pnlForm.Controls.Add(this.lblCliente);
            this.pnlForm.Controls.Add(this.lblDataTermino);
            this.pnlForm.Controls.Add(this.dataInicio);
            this.pnlForm.Controls.Add(this.dataTermino);
            this.pnlForm.Controls.Add(this.dataElaboracao);
            this.pnlForm.Controls.Add(this.lblCodigo);
            this.pnlForm.Size = new System.Drawing.Size(764, 1051);
            // 
            // flpAcao
            // 
            this.flpAcao.Controls.Add(this.btnGravar);
            this.flpAcao.Controls.Add(this.btnFechar);
            this.flpAcao.Dock = System.Windows.Forms.DockStyle.Fill;
            this.flpAcao.Location = new System.Drawing.Point(0, 0);
            this.flpAcao.Name = "flpAcao";
            this.flpAcao.Size = new System.Drawing.Size(784, 35);
            this.flpAcao.TabIndex = 0;
            // 
            // btnGravar
            // 
            this.btnGravar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnGravar.Image = global::SWS.Properties.Resources.Gravar;
            this.btnGravar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnGravar.Location = new System.Drawing.Point(3, 3);
            this.btnGravar.Name = "btnGravar";
            this.btnGravar.Size = new System.Drawing.Size(75, 23);
            this.btnGravar.TabIndex = 26;
            this.btnGravar.TabStop = false;
            this.btnGravar.Text = "&Gravar";
            this.btnGravar.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnGravar.UseVisualStyleBackColor = true;
            this.btnGravar.Click += new System.EventHandler(this.btnGravar_Click);
            // 
            // btnFechar
            // 
            this.btnFechar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnFechar.Image = global::SWS.Properties.Resources.close;
            this.btnFechar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnFechar.Location = new System.Drawing.Point(84, 3);
            this.btnFechar.Name = "btnFechar";
            this.btnFechar.Size = new System.Drawing.Size(75, 23);
            this.btnFechar.TabIndex = 28;
            this.btnFechar.TabStop = false;
            this.btnFechar.Text = "&Fechar";
            this.btnFechar.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnFechar.UseVisualStyleBackColor = true;
            this.btnFechar.Click += new System.EventHandler(this.btnFechar_Click);
            // 
            // lblCodigo
            // 
            this.lblCodigo.BackColor = System.Drawing.Color.LightGray;
            this.lblCodigo.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblCodigo.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCodigo.Location = new System.Drawing.Point(12, 70);
            this.lblCodigo.Name = "lblCodigo";
            this.lblCodigo.ReadOnly = true;
            this.lblCodigo.Size = new System.Drawing.Size(129, 21);
            this.lblCodigo.TabIndex = 0;
            this.lblCodigo.TabStop = false;
            this.lblCodigo.Text = "Código do contrato";
            // 
            // textCodigo
            // 
            this.textCodigo.BackColor = System.Drawing.Color.LightGray;
            this.textCodigo.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textCodigo.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textCodigo.Location = new System.Drawing.Point(140, 70);
            this.textCodigo.Mask = "9999,99,999999-99";
            this.textCodigo.Name = "textCodigo";
            this.textCodigo.PromptChar = ' ';
            this.textCodigo.ReadOnly = true;
            this.textCodigo.Size = new System.Drawing.Size(595, 21);
            this.textCodigo.TabIndex = 1;
            // 
            // lblDataElaboracao
            // 
            this.lblDataElaboracao.BackColor = System.Drawing.Color.LightGray;
            this.lblDataElaboracao.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblDataElaboracao.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDataElaboracao.Location = new System.Drawing.Point(12, 90);
            this.lblDataElaboracao.Name = "lblDataElaboracao";
            this.lblDataElaboracao.ReadOnly = true;
            this.lblDataElaboracao.Size = new System.Drawing.Size(129, 21);
            this.lblDataElaboracao.TabIndex = 2;
            this.lblDataElaboracao.TabStop = false;
            this.lblDataElaboracao.Text = "Data de elaboração";
            // 
            // dataElaboracao
            // 
            this.dataElaboracao.Checked = false;
            this.dataElaboracao.CustomFormat = "dd/MM/yyyy HH:mm:ss";
            this.dataElaboracao.Enabled = false;
            this.dataElaboracao.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dataElaboracao.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dataElaboracao.Location = new System.Drawing.Point(140, 90);
            this.dataElaboracao.Name = "dataElaboracao";
            this.dataElaboracao.ShowCheckBox = true;
            this.dataElaboracao.Size = new System.Drawing.Size(595, 21);
            this.dataElaboracao.TabIndex = 3;
            // 
            // lblDataInicio
            // 
            this.lblDataInicio.BackColor = System.Drawing.Color.LightGray;
            this.lblDataInicio.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblDataInicio.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDataInicio.Location = new System.Drawing.Point(12, 110);
            this.lblDataInicio.Name = "lblDataInicio";
            this.lblDataInicio.ReadOnly = true;
            this.lblDataInicio.Size = new System.Drawing.Size(129, 21);
            this.lblDataInicio.TabIndex = 4;
            this.lblDataInicio.TabStop = false;
            this.lblDataInicio.Text = "Data de início";
            // 
            // dataInicio
            // 
            this.dataInicio.Checked = false;
            this.dataInicio.CustomFormat = "dd/MM/yyyy HH:mm:ss";
            this.dataInicio.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dataInicio.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dataInicio.Location = new System.Drawing.Point(140, 110);
            this.dataInicio.Name = "dataInicio";
            this.dataInicio.ShowCheckBox = true;
            this.dataInicio.Size = new System.Drawing.Size(595, 21);
            this.dataInicio.TabIndex = 5;
            // 
            // lblDataTermino
            // 
            this.lblDataTermino.BackColor = System.Drawing.Color.LightGray;
            this.lblDataTermino.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblDataTermino.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDataTermino.Location = new System.Drawing.Point(12, 130);
            this.lblDataTermino.Name = "lblDataTermino";
            this.lblDataTermino.ReadOnly = true;
            this.lblDataTermino.Size = new System.Drawing.Size(129, 21);
            this.lblDataTermino.TabIndex = 6;
            this.lblDataTermino.TabStop = false;
            this.lblDataTermino.Text = "Data de término";
            // 
            // dataTermino
            // 
            this.dataTermino.Checked = false;
            this.dataTermino.CustomFormat = "dd/MM/yyyy HH:mm:ss";
            this.dataTermino.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dataTermino.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dataTermino.Location = new System.Drawing.Point(140, 130);
            this.dataTermino.Name = "dataTermino";
            this.dataTermino.ShowCheckBox = true;
            this.dataTermino.Size = new System.Drawing.Size(595, 21);
            this.dataTermino.TabIndex = 7;
            // 
            // lblCliente
            // 
            this.lblCliente.BackColor = System.Drawing.Color.LightGray;
            this.lblCliente.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblCliente.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCliente.Location = new System.Drawing.Point(12, 150);
            this.lblCliente.Name = "lblCliente";
            this.lblCliente.ReadOnly = true;
            this.lblCliente.Size = new System.Drawing.Size(129, 21);
            this.lblCliente.TabIndex = 9;
            this.lblCliente.TabStop = false;
            this.lblCliente.Text = "Cliente";
            // 
            // textCliente
            // 
            this.textCliente.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.textCliente.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textCliente.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textCliente.Location = new System.Drawing.Point(140, 150);
            this.textCliente.Name = "textCliente";
            this.textCliente.ReadOnly = true;
            this.textCliente.Size = new System.Drawing.Size(565, 21);
            this.textCliente.TabIndex = 10;
            // 
            // btnCliente
            // 
            this.btnCliente.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnCliente.Image = global::SWS.Properties.Resources.busca;
            this.btnCliente.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnCliente.Location = new System.Drawing.Point(703, 150);
            this.btnCliente.Name = "btnCliente";
            this.btnCliente.Size = new System.Drawing.Size(32, 21);
            this.btnCliente.TabIndex = 11;
            this.btnCliente.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnCliente.UseVisualStyleBackColor = true;
            this.btnCliente.Click += new System.EventHandler(this.btnCliente_Click);
            // 
            // btnPrestador
            // 
            this.btnPrestador.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnPrestador.Image = global::SWS.Properties.Resources.busca;
            this.btnPrestador.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnPrestador.Location = new System.Drawing.Point(703, 170);
            this.btnPrestador.Name = "btnPrestador";
            this.btnPrestador.Size = new System.Drawing.Size(32, 21);
            this.btnPrestador.TabIndex = 14;
            this.btnPrestador.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnPrestador.UseVisualStyleBackColor = true;
            this.btnPrestador.Click += new System.EventHandler(this.btnPrestador_Click);
            // 
            // textPrestador
            // 
            this.textPrestador.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.textPrestador.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textPrestador.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textPrestador.Location = new System.Drawing.Point(140, 170);
            this.textPrestador.Name = "textPrestador";
            this.textPrestador.ReadOnly = true;
            this.textPrestador.Size = new System.Drawing.Size(565, 21);
            this.textPrestador.TabIndex = 13;
            // 
            // lblPrestador
            // 
            this.lblPrestador.BackColor = System.Drawing.Color.LightGray;
            this.lblPrestador.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblPrestador.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPrestador.Location = new System.Drawing.Point(12, 170);
            this.lblPrestador.Name = "lblPrestador";
            this.lblPrestador.ReadOnly = true;
            this.lblPrestador.Size = new System.Drawing.Size(129, 21);
            this.lblPrestador.TabIndex = 12;
            this.lblPrestador.TabStop = false;
            this.lblPrestador.Text = "Prestador de Serviços";
            // 
            // grbExame
            // 
            this.grbExame.Controls.Add(this.dgvExame);
            this.grbExame.Location = new System.Drawing.Point(12, 197);
            this.grbExame.Name = "grbExame";
            this.grbExame.Size = new System.Drawing.Size(723, 237);
            this.grbExame.TabIndex = 15;
            this.grbExame.TabStop = false;
            this.grbExame.Text = "Exames";
            // 
            // dgvExame
            // 
            this.dgvExame.AllowUserToAddRows = false;
            this.dgvExame.AllowUserToDeleteRows = false;
            this.dgvExame.AllowUserToOrderColumns = true;
            this.dgvExame.AllowUserToResizeRows = false;
            this.dgvExame.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this.dgvExame.BackgroundColor = System.Drawing.Color.White;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.Color.LightGray;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvExame.DefaultCellStyle = dataGridViewCellStyle2;
            this.dgvExame.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvExame.Location = new System.Drawing.Point(3, 16);
            this.dgvExame.MultiSelect = false;
            this.dgvExame.Name = "dgvExame";
            this.dgvExame.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvExame.Size = new System.Drawing.Size(717, 218);
            this.dgvExame.TabIndex = 7;
            this.dgvExame.TabStop = false;
            this.dgvExame.CellBeginEdit += new System.Windows.Forms.DataGridViewCellCancelEventHandler(this.dgvExame_CellBeginEdit);
            this.dgvExame.CellEndEdit += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvExame_CellEndEdit);
            this.dgvExame.DataBindingComplete += new System.Windows.Forms.DataGridViewBindingCompleteEventHandler(this.dgvExame_DataBindingComplete);
            this.dgvExame.EditingControlShowing += new System.Windows.Forms.DataGridViewEditingControlShowingEventHandler(this.dgvExame_EditingControlShowing);
            this.dgvExame.RowPrePaint += new System.Windows.Forms.DataGridViewRowPrePaintEventHandler(this.dgvExame_RowPrePaint);
            this.dgvExame.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.dgvExame_KeyPress);
            // 
            // btnExcluirExame
            // 
            this.btnExcluirExame.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnExcluirExame.Image = global::SWS.Properties.Resources.lixeira;
            this.btnExcluirExame.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnExcluirExame.Location = new System.Drawing.Point(84, 3);
            this.btnExcluirExame.Name = "btnExcluirExame";
            this.btnExcluirExame.Size = new System.Drawing.Size(75, 23);
            this.btnExcluirExame.TabIndex = 17;
            this.btnExcluirExame.Text = "&Excluir";
            this.btnExcluirExame.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnExcluirExame.UseVisualStyleBackColor = true;
            this.btnExcluirExame.Click += new System.EventHandler(this.btnExcluirExame_Click);
            // 
            // btnIncluirExame
            // 
            this.btnIncluirExame.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnIncluirExame.Image = ((System.Drawing.Image)(resources.GetObject("btnIncluirExame.Image")));
            this.btnIncluirExame.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnIncluirExame.Location = new System.Drawing.Point(3, 3);
            this.btnIncluirExame.Name = "btnIncluirExame";
            this.btnIncluirExame.Size = new System.Drawing.Size(75, 23);
            this.btnIncluirExame.TabIndex = 16;
            this.btnIncluirExame.Text = "&Incluir";
            this.btnIncluirExame.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnIncluirExame.UseVisualStyleBackColor = true;
            this.btnIncluirExame.Click += new System.EventHandler(this.btnIncluirExame_Click);
            // 
            // btnExcluirProduto
            // 
            this.btnExcluirProduto.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnExcluirProduto.Image = ((System.Drawing.Image)(resources.GetObject("btnExcluirProduto.Image")));
            this.btnExcluirProduto.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnExcluirProduto.Location = new System.Drawing.Point(84, 3);
            this.btnExcluirProduto.Name = "btnExcluirProduto";
            this.btnExcluirProduto.Size = new System.Drawing.Size(75, 23);
            this.btnExcluirProduto.TabIndex = 20;
            this.btnExcluirProduto.Text = "E&xcluir";
            this.btnExcluirProduto.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnExcluirProduto.UseVisualStyleBackColor = true;
            this.btnExcluirProduto.Click += new System.EventHandler(this.btnExcluirProduto_Click);
            // 
            // btnIncluirProduto
            // 
            this.btnIncluirProduto.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnIncluirProduto.Image = ((System.Drawing.Image)(resources.GetObject("btnIncluirProduto.Image")));
            this.btnIncluirProduto.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnIncluirProduto.Location = new System.Drawing.Point(3, 3);
            this.btnIncluirProduto.Name = "btnIncluirProduto";
            this.btnIncluirProduto.Size = new System.Drawing.Size(75, 23);
            this.btnIncluirProduto.TabIndex = 19;
            this.btnIncluirProduto.Text = "I&ncluir";
            this.btnIncluirProduto.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnIncluirProduto.UseVisualStyleBackColor = true;
            this.btnIncluirProduto.Click += new System.EventHandler(this.btnIncluirProduto_Click);
            // 
            // grbProdutos
            // 
            this.grbProdutos.Controls.Add(this.dgvProduto);
            this.grbProdutos.Location = new System.Drawing.Point(12, 477);
            this.grbProdutos.Name = "grbProdutos";
            this.grbProdutos.Size = new System.Drawing.Size(723, 237);
            this.grbProdutos.TabIndex = 18;
            this.grbProdutos.TabStop = false;
            this.grbProdutos.Text = "Produtos";
            // 
            // dgvProduto
            // 
            this.dgvProduto.AllowUserToAddRows = false;
            this.dgvProduto.AllowUserToDeleteRows = false;
            this.dgvProduto.AllowUserToOrderColumns = true;
            this.dgvProduto.AllowUserToResizeRows = false;
            this.dgvProduto.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this.dgvProduto.BackgroundColor = System.Drawing.Color.White;
            this.dgvProduto.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.LightGray;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvProduto.DefaultCellStyle = dataGridViewCellStyle1;
            this.dgvProduto.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvProduto.Location = new System.Drawing.Point(3, 16);
            this.dgvProduto.MultiSelect = false;
            this.dgvProduto.Name = "dgvProduto";
            this.dgvProduto.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvProduto.Size = new System.Drawing.Size(717, 218);
            this.dgvProduto.TabIndex = 10;
            this.dgvProduto.TabStop = false;
            this.dgvProduto.CellBeginEdit += new System.Windows.Forms.DataGridViewCellCancelEventHandler(this.dgvProduto_CellBeginEdit);
            this.dgvProduto.CellEndEdit += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvProduto_CellEndEdit);
            this.dgvProduto.DataBindingComplete += new System.Windows.Forms.DataGridViewBindingCompleteEventHandler(this.dgvProduto_DataBindingComplete);
            this.dgvProduto.EditingControlShowing += new System.Windows.Forms.DataGridViewEditingControlShowingEventHandler(this.dgvProduto_EditingControlShowing);
            this.dgvProduto.RowPrePaint += new System.Windows.Forms.DataGridViewRowPrePaintEventHandler(this.dgvProduto_RowPrePaint);
            this.dgvProduto.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.dgvProduto_KeyPress);
            // 
            // lblBloqueio
            // 
            this.lblBloqueio.BackColor = System.Drawing.Color.LightGray;
            this.lblBloqueio.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblBloqueio.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblBloqueio.Location = new System.Drawing.Point(12, 752);
            this.lblBloqueio.Name = "lblBloqueio";
            this.lblBloqueio.ReadOnly = true;
            this.lblBloqueio.Size = new System.Drawing.Size(189, 21);
            this.lblBloqueio.TabIndex = 21;
            this.lblBloqueio.TabStop = false;
            this.lblBloqueio.Text = "Bloqueio por inadimplência";
            // 
            // lblTempoBloqueio
            // 
            this.lblTempoBloqueio.BackColor = System.Drawing.Color.LightGray;
            this.lblTempoBloqueio.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblTempoBloqueio.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTempoBloqueio.Location = new System.Drawing.Point(12, 771);
            this.lblTempoBloqueio.Name = "lblTempoBloqueio";
            this.lblTempoBloqueio.ReadOnly = true;
            this.lblTempoBloqueio.Size = new System.Drawing.Size(189, 21);
            this.lblTempoBloqueio.TabIndex = 22;
            this.lblTempoBloqueio.TabStop = false;
            this.lblTempoBloqueio.Text = "Dias para bloquear";
            // 
            // lblMensalidade
            // 
            this.lblMensalidade.BackColor = System.Drawing.Color.LightGray;
            this.lblMensalidade.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblMensalidade.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblMensalidade.Location = new System.Drawing.Point(12, 791);
            this.lblMensalidade.Name = "lblMensalidade";
            this.lblMensalidade.ReadOnly = true;
            this.lblMensalidade.Size = new System.Drawing.Size(189, 21);
            this.lblMensalidade.TabIndex = 23;
            this.lblMensalidade.TabStop = false;
            this.lblMensalidade.Text = "Gera valor mensal";
            // 
            // lblCobrancaAutomatica
            // 
            this.lblCobrancaAutomatica.BackColor = System.Drawing.Color.LightGray;
            this.lblCobrancaAutomatica.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblCobrancaAutomatica.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCobrancaAutomatica.Location = new System.Drawing.Point(12, 871);
            this.lblCobrancaAutomatica.Name = "lblCobrancaAutomatica";
            this.lblCobrancaAutomatica.ReadOnly = true;
            this.lblCobrancaAutomatica.Size = new System.Drawing.Size(189, 21);
            this.lblCobrancaAutomatica.TabIndex = 24;
            this.lblCobrancaAutomatica.TabStop = false;
            this.lblCobrancaAutomatica.Text = "Gera cobrança automatica";
            // 
            // lblValorMensal
            // 
            this.lblValorMensal.BackColor = System.Drawing.Color.LightGray;
            this.lblValorMensal.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblValorMensal.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblValorMensal.Location = new System.Drawing.Point(12, 811);
            this.lblValorMensal.Name = "lblValorMensal";
            this.lblValorMensal.ReadOnly = true;
            this.lblValorMensal.Size = new System.Drawing.Size(189, 21);
            this.lblValorMensal.TabIndex = 25;
            this.lblValorMensal.TabStop = false;
            this.lblValorMensal.Text = "Valor da Mensalidade R$";
            // 
            // lblNumeroVidas
            // 
            this.lblNumeroVidas.BackColor = System.Drawing.Color.LightGray;
            this.lblNumeroVidas.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblNumeroVidas.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNumeroVidas.Location = new System.Drawing.Point(12, 831);
            this.lblNumeroVidas.Name = "lblNumeroVidas";
            this.lblNumeroVidas.ReadOnly = true;
            this.lblNumeroVidas.Size = new System.Drawing.Size(189, 21);
            this.lblNumeroVidas.TabIndex = 26;
            this.lblNumeroVidas.TabStop = false;
            this.lblNumeroVidas.Text = "Cobrança por número de vidas";
            // 
            // lblValorNumeroVidas
            // 
            this.lblValorNumeroVidas.BackColor = System.Drawing.Color.LightGray;
            this.lblValorNumeroVidas.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblValorNumeroVidas.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblValorNumeroVidas.Location = new System.Drawing.Point(12, 851);
            this.lblValorNumeroVidas.Name = "lblValorNumeroVidas";
            this.lblValorNumeroVidas.ReadOnly = true;
            this.lblValorNumeroVidas.Size = new System.Drawing.Size(189, 21);
            this.lblValorNumeroVidas.TabIndex = 27;
            this.lblValorNumeroVidas.TabStop = false;
            this.lblValorNumeroVidas.Text = "Valor R$ / número vidas";
            // 
            // lblDiaCobranca
            // 
            this.lblDiaCobranca.BackColor = System.Drawing.Color.LightGray;
            this.lblDiaCobranca.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblDiaCobranca.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDiaCobranca.Location = new System.Drawing.Point(12, 891);
            this.lblDiaCobranca.Name = "lblDiaCobranca";
            this.lblDiaCobranca.ReadOnly = true;
            this.lblDiaCobranca.Size = new System.Drawing.Size(189, 21);
            this.lblDiaCobranca.TabIndex = 28;
            this.lblDiaCobranca.TabStop = false;
            this.lblDiaCobranca.Text = "Dia para gerar a cobrança";
            // 
            // grbComentario
            // 
            this.grbComentario.BackColor = System.Drawing.Color.White;
            this.grbComentario.Controls.Add(this.textComentario);
            this.grbComentario.Location = new System.Drawing.Point(12, 917);
            this.grbComentario.Name = "grbComentario";
            this.grbComentario.Size = new System.Drawing.Size(723, 107);
            this.grbComentario.TabIndex = 29;
            this.grbComentario.TabStop = false;
            this.grbComentario.Text = "Comentário";
            // 
            // textComentario
            // 
            this.textComentario.BackColor = System.Drawing.Color.LightSteelBlue;
            this.textComentario.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.textComentario.Dock = System.Windows.Forms.DockStyle.Fill;
            this.textComentario.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textComentario.Location = new System.Drawing.Point(3, 16);
            this.textComentario.Multiline = true;
            this.textComentario.Name = "textComentario";
            this.textComentario.Size = new System.Drawing.Size(717, 88);
            this.textComentario.TabIndex = 22;
            this.textComentario.Enter += new System.EventHandler(this.textComentario_Enter);
            this.textComentario.Leave += new System.EventHandler(this.textComentario_Leave);
            // 
            // cbBloqueio
            // 
            this.cbBloqueio.BackColor = System.Drawing.Color.LightGray;
            this.cbBloqueio.BorderColor = System.Drawing.Color.DimGray;
            this.cbBloqueio.BorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid;
            this.cbBloqueio.FormattingEnabled = true;
            this.cbBloqueio.Location = new System.Drawing.Point(200, 752);
            this.cbBloqueio.Name = "cbBloqueio";
            this.cbBloqueio.Size = new System.Drawing.Size(535, 21);
            this.cbBloqueio.TabIndex = 30;
            this.cbBloqueio.SelectedIndexChanged += new System.EventHandler(this.cbBloqueio_SelectedIndexChanged);
            // 
            // cbValorMensal
            // 
            this.cbValorMensal.BackColor = System.Drawing.Color.LightGray;
            this.cbValorMensal.BorderColor = System.Drawing.Color.DimGray;
            this.cbValorMensal.BorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid;
            this.cbValorMensal.FormattingEnabled = true;
            this.cbValorMensal.Location = new System.Drawing.Point(200, 791);
            this.cbValorMensal.Name = "cbValorMensal";
            this.cbValorMensal.Size = new System.Drawing.Size(535, 21);
            this.cbValorMensal.TabIndex = 31;
            this.cbValorMensal.SelectedIndexChanged += new System.EventHandler(this.cbValorMensal_SelectedIndexChanged);
            // 
            // cbNumeroVidas
            // 
            this.cbNumeroVidas.BackColor = System.Drawing.Color.LightGray;
            this.cbNumeroVidas.BorderColor = System.Drawing.Color.DimGray;
            this.cbNumeroVidas.BorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid;
            this.cbNumeroVidas.FormattingEnabled = true;
            this.cbNumeroVidas.Location = new System.Drawing.Point(200, 831);
            this.cbNumeroVidas.Name = "cbNumeroVidas";
            this.cbNumeroVidas.Size = new System.Drawing.Size(535, 21);
            this.cbNumeroVidas.TabIndex = 32;
            this.cbNumeroVidas.SelectedIndexChanged += new System.EventHandler(this.cbNumeroVidas_SelectedIndexChanged);
            // 
            // cbCobrancaAutomatica
            // 
            this.cbCobrancaAutomatica.BackColor = System.Drawing.Color.LightGray;
            this.cbCobrancaAutomatica.BorderColor = System.Drawing.Color.DimGray;
            this.cbCobrancaAutomatica.BorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid;
            this.cbCobrancaAutomatica.FormattingEnabled = true;
            this.cbCobrancaAutomatica.Location = new System.Drawing.Point(200, 871);
            this.cbCobrancaAutomatica.Name = "cbCobrancaAutomatica";
            this.cbCobrancaAutomatica.Size = new System.Drawing.Size(535, 21);
            this.cbCobrancaAutomatica.TabIndex = 33;
            this.cbCobrancaAutomatica.SelectedIndexChanged += new System.EventHandler(this.cbCobrancaAutomatica_SelectedIndexChanged);
            // 
            // textDiasBloqueio
            // 
            this.textDiasBloqueio.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textDiasBloqueio.Enabled = false;
            this.textDiasBloqueio.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textDiasBloqueio.Location = new System.Drawing.Point(200, 771);
            this.textDiasBloqueio.MaxLength = 2;
            this.textDiasBloqueio.Name = "textDiasBloqueio";
            this.textDiasBloqueio.Size = new System.Drawing.Size(535, 21);
            this.textDiasBloqueio.TabIndex = 34;
            this.textDiasBloqueio.Enter += new System.EventHandler(this.textDiasBloqueio_Enter);
            this.textDiasBloqueio.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.textDiasBloqueio_KeyPress);
            this.textDiasBloqueio.Leave += new System.EventHandler(this.textDiasBloqueio_Leave);
            // 
            // textValorMensalidade
            // 
            this.textValorMensalidade.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textValorMensalidade.Enabled = false;
            this.textValorMensalidade.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textValorMensalidade.Location = new System.Drawing.Point(200, 811);
            this.textValorMensalidade.MaxLength = 15;
            this.textValorMensalidade.Name = "textValorMensalidade";
            this.textValorMensalidade.Size = new System.Drawing.Size(535, 21);
            this.textValorMensalidade.TabIndex = 35;
            this.textValorMensalidade.Enter += new System.EventHandler(this.textValorMensalidade_Enter);
            this.textValorMensalidade.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.textValorMensalidade_KeyPress);
            this.textValorMensalidade.Leave += new System.EventHandler(this.textValorMensalidade_Leave);
            // 
            // textValorNumeroVidas
            // 
            this.textValorNumeroVidas.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textValorNumeroVidas.Enabled = false;
            this.textValorNumeroVidas.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textValorNumeroVidas.Location = new System.Drawing.Point(200, 851);
            this.textValorNumeroVidas.MaxLength = 15;
            this.textValorNumeroVidas.Name = "textValorNumeroVidas";
            this.textValorNumeroVidas.Size = new System.Drawing.Size(535, 21);
            this.textValorNumeroVidas.TabIndex = 36;
            this.textValorNumeroVidas.Enter += new System.EventHandler(this.textValorNumeroVidas_Enter);
            this.textValorNumeroVidas.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.textValorNumeroVidas_KeyPress);
            this.textValorNumeroVidas.Leave += new System.EventHandler(this.textValorNumeroVidas_Leave);
            // 
            // textDiaCobranca
            // 
            this.textDiaCobranca.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textDiaCobranca.Enabled = false;
            this.textDiaCobranca.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textDiaCobranca.Location = new System.Drawing.Point(200, 891);
            this.textDiaCobranca.MaxLength = 2;
            this.textDiaCobranca.Name = "textDiaCobranca";
            this.textDiaCobranca.Size = new System.Drawing.Size(535, 21);
            this.textDiaCobranca.TabIndex = 37;
            this.textDiaCobranca.Enter += new System.EventHandler(this.textDiaCobranca_Enter);
            this.textDiaCobranca.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.textDiaCobranca_KeyPress);
            this.textDiaCobranca.Leave += new System.EventHandler(this.textDiaCobranca_Leave);
            // 
            // flpProduto
            // 
            this.flpProduto.Controls.Add(this.btnIncluirProduto);
            this.flpProduto.Controls.Add(this.btnExcluirProduto);
            this.flpProduto.Location = new System.Drawing.Point(12, 720);
            this.flpProduto.Name = "flpProduto";
            this.flpProduto.Size = new System.Drawing.Size(723, 29);
            this.flpProduto.TabIndex = 38;
            // 
            // flpExame
            // 
            this.flpExame.Controls.Add(this.btnIncluirExame);
            this.flpExame.Controls.Add(this.btnExcluirExame);
            this.flpExame.Location = new System.Drawing.Point(12, 440);
            this.flpExame.Name = "flpExame";
            this.flpExame.Size = new System.Drawing.Size(723, 31);
            this.flpExame.TabIndex = 39;
            // 
            // lblEmpresa
            // 
            this.lblEmpresa.BackColor = System.Drawing.Color.LightGray;
            this.lblEmpresa.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblEmpresa.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblEmpresa.Location = new System.Drawing.Point(12, 10);
            this.lblEmpresa.Name = "lblEmpresa";
            this.lblEmpresa.ReadOnly = true;
            this.lblEmpresa.Size = new System.Drawing.Size(129, 21);
            this.lblEmpresa.TabIndex = 40;
            this.lblEmpresa.TabStop = false;
            this.lblEmpresa.Text = "Empresa";
            // 
            // lblCnjEmpresa
            // 
            this.lblCnjEmpresa.BackColor = System.Drawing.Color.LightGray;
            this.lblCnjEmpresa.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblCnjEmpresa.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCnjEmpresa.Location = new System.Drawing.Point(12, 50);
            this.lblCnjEmpresa.Name = "lblCnjEmpresa";
            this.lblCnjEmpresa.ReadOnly = true;
            this.lblCnjEmpresa.Size = new System.Drawing.Size(129, 21);
            this.lblCnjEmpresa.TabIndex = 41;
            this.lblCnjEmpresa.TabStop = false;
            this.lblCnjEmpresa.Text = "CNPJ";
            // 
            // textEmpresa
            // 
            this.textEmpresa.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.textEmpresa.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textEmpresa.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textEmpresa.Location = new System.Drawing.Point(140, 10);
            this.textEmpresa.Name = "textEmpresa";
            this.textEmpresa.ReadOnly = true;
            this.textEmpresa.Size = new System.Drawing.Size(595, 21);
            this.textEmpresa.TabIndex = 42;
            // 
            // textCNPJ
            // 
            this.textCNPJ.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.textCNPJ.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textCNPJ.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textCNPJ.Location = new System.Drawing.Point(140, 50);
            this.textCNPJ.Name = "textCNPJ";
            this.textCNPJ.ReadOnly = true;
            this.textCNPJ.Size = new System.Drawing.Size(595, 21);
            this.textCNPJ.TabIndex = 43;
            // 
            // lblFantasia
            // 
            this.lblFantasia.BackColor = System.Drawing.Color.LightGray;
            this.lblFantasia.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblFantasia.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblFantasia.Location = new System.Drawing.Point(12, 30);
            this.lblFantasia.Name = "lblFantasia";
            this.lblFantasia.ReadOnly = true;
            this.lblFantasia.Size = new System.Drawing.Size(129, 21);
            this.lblFantasia.TabIndex = 44;
            this.lblFantasia.TabStop = false;
            this.lblFantasia.Text = "Nome de Fantasia";
            // 
            // textNomeFantasia
            // 
            this.textNomeFantasia.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.textNomeFantasia.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textNomeFantasia.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textNomeFantasia.Location = new System.Drawing.Point(140, 30);
            this.textNomeFantasia.Name = "textNomeFantasia";
            this.textNomeFantasia.ReadOnly = true;
            this.textNomeFantasia.Size = new System.Drawing.Size(595, 21);
            this.textNomeFantasia.TabIndex = 45;
            // 
            // frmContratosIncluir
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(784, 561);
            this.Name = "frmContratosIncluir";
            this.Text = "INCLUIR CONTRATO";
            this.Load += new System.EventHandler(this.frmContratosIncluir_Load);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.frmContratosIncluir_KeyDown);
            this.spForm.Panel1.ResumeLayout(false);
            this.spForm.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.spForm)).EndInit();
            this.spForm.ResumeLayout(false);
            this.pnlForm.ResumeLayout(false);
            this.pnlForm.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.banner)).EndInit();
            this.flpAcao.ResumeLayout(false);
            this.grbExame.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvExame)).EndInit();
            this.grbProdutos.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvProduto)).EndInit();
            this.grbComentario.ResumeLayout(false);
            this.grbComentario.PerformLayout();
            this.flpProduto.ResumeLayout(false);
            this.flpExame.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        protected System.Windows.Forms.FlowLayoutPanel flpAcao;
        protected System.Windows.Forms.Button btnFechar;
        protected System.Windows.Forms.Button btnGravar;
        protected System.Windows.Forms.TextBox lblCodigo;
        protected System.Windows.Forms.MaskedTextBox textCodigo;
        protected System.Windows.Forms.TextBox lblDataElaboracao;
        protected System.Windows.Forms.DateTimePicker dataElaboracao;
        protected System.Windows.Forms.DateTimePicker dataInicio;
        protected System.Windows.Forms.TextBox lblDataInicio;
        protected System.Windows.Forms.TextBox lblDataTermino;
        protected System.Windows.Forms.DateTimePicker dataTermino;
        protected System.Windows.Forms.TextBox lblCliente;
        protected System.Windows.Forms.TextBox textCliente;
        protected System.Windows.Forms.Button btnCliente;
        protected System.Windows.Forms.Button btnPrestador;
        protected System.Windows.Forms.TextBox textPrestador;
        protected System.Windows.Forms.TextBox lblPrestador;
        protected System.Windows.Forms.Button btnExcluirExame;
        protected System.Windows.Forms.Button btnIncluirExame;
        protected System.Windows.Forms.Button btnExcluirProduto;
        protected System.Windows.Forms.Button btnIncluirProduto;
        protected System.Windows.Forms.TextBox lblTempoBloqueio;
        protected System.Windows.Forms.TextBox lblBloqueio;
        protected System.Windows.Forms.TextBox lblMensalidade;
        protected System.Windows.Forms.TextBox lblValorMensal;
        protected System.Windows.Forms.TextBox lblCobrancaAutomatica;
        protected System.Windows.Forms.TextBox lblNumeroVidas;
        protected System.Windows.Forms.TextBox lblValorNumeroVidas;
        protected System.Windows.Forms.TextBox lblDiaCobranca;
        protected ComboBoxWithBorder cbBloqueio;
        protected System.Windows.Forms.GroupBox grbComentario;
        protected System.Windows.Forms.TextBox textComentario;
        protected ComboBoxWithBorder cbCobrancaAutomatica;
        protected ComboBoxWithBorder cbNumeroVidas;
        protected ComboBoxWithBorder cbValorMensal;
        protected System.Windows.Forms.TextBox textDiasBloqueio;
        protected System.Windows.Forms.TextBox textValorMensalidade;
        protected System.Windows.Forms.TextBox textValorNumeroVidas;
        protected System.Windows.Forms.TextBox textDiaCobranca;
        protected System.Windows.Forms.DataGridView dgvExame;
        protected System.Windows.Forms.DataGridView dgvProduto;
        protected System.Windows.Forms.GroupBox grbExame;
        protected System.Windows.Forms.GroupBox grbProdutos;
        protected System.Windows.Forms.TextBox textCNPJ;
        protected System.Windows.Forms.TextBox textEmpresa;
        protected System.Windows.Forms.TextBox lblCnjEmpresa;
        protected System.Windows.Forms.TextBox lblEmpresa;
        private System.Windows.Forms.FlowLayoutPanel flpExame;
        private System.Windows.Forms.FlowLayoutPanel flpProduto;
        protected System.Windows.Forms.TextBox textNomeFantasia;
        protected System.Windows.Forms.TextBox lblFantasia;
    }
}