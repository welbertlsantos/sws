﻿namespace SWS.View
{
    partial class frmContratoCopiar
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            this.flpAcao = new System.Windows.Forms.FlowLayoutPanel();
            this.btnFechar = new System.Windows.Forms.Button();
            this.btnExcluirCliente = new System.Windows.Forms.Button();
            this.btnCliente = new System.Windows.Forms.Button();
            this.textCliente = new System.Windows.Forms.TextBox();
            this.lblCliente = new System.Windows.Forms.TextBox();
            this.textCodigo = new System.Windows.Forms.MaskedTextBox();
            this.lblCodigo = new System.Windows.Forms.TextBox();
            this.grbContrato = new System.Windows.Forms.GroupBox();
            this.dgvContrato = new System.Windows.Forms.DataGridView();
            this.flpAcaoBuscar = new System.Windows.Forms.FlowLayoutPanel();
            this.btnBuscar = new System.Windows.Forms.Button();
            this.btnExcluirClienteDuplicado = new System.Windows.Forms.Button();
            this.btnClienteDuplicado = new System.Windows.Forms.Button();
            this.textClienteDuplicado = new System.Windows.Forms.TextBox();
            this.lblClienteDuplicado = new System.Windows.Forms.TextBox();
            this.flowLayoutPanel1 = new System.Windows.Forms.FlowLayoutPanel();
            this.btnCopiar = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.scForm)).BeginInit();
            this.scForm.Panel1.SuspendLayout();
            this.scForm.Panel2.SuspendLayout();
            this.scForm.SuspendLayout();
            this.pnlForm.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.banner)).BeginInit();
            this.flpAcao.SuspendLayout();
            this.grbContrato.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvContrato)).BeginInit();
            this.flpAcaoBuscar.SuspendLayout();
            this.flowLayoutPanel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // scForm
            // 
            // 
            // scForm.Panel1
            // 
            this.scForm.Panel1.Controls.Add(this.flpAcao);
            this.scForm.Size = new System.Drawing.Size(784, 527);
            this.scForm.SplitterDistance = 49;
            // 
            // pnlForm
            // 
            this.pnlForm.Controls.Add(this.flowLayoutPanel1);
            this.pnlForm.Controls.Add(this.btnExcluirClienteDuplicado);
            this.pnlForm.Controls.Add(this.btnClienteDuplicado);
            this.pnlForm.Controls.Add(this.textClienteDuplicado);
            this.pnlForm.Controls.Add(this.lblClienteDuplicado);
            this.pnlForm.Controls.Add(this.flpAcaoBuscar);
            this.pnlForm.Controls.Add(this.grbContrato);
            this.pnlForm.Controls.Add(this.textCodigo);
            this.pnlForm.Controls.Add(this.lblCodigo);
            this.pnlForm.Controls.Add(this.btnExcluirCliente);
            this.pnlForm.Controls.Add(this.btnCliente);
            this.pnlForm.Controls.Add(this.textCliente);
            this.pnlForm.Controls.Add(this.lblCliente);
            this.pnlForm.Size = new System.Drawing.Size(766, 530);
            // 
            // banner
            // 
            this.banner.Size = new System.Drawing.Size(784, 41);
            // 
            // flpAcao
            // 
            this.flpAcao.Controls.Add(this.btnFechar);
            this.flpAcao.Dock = System.Windows.Forms.DockStyle.Fill;
            this.flpAcao.Location = new System.Drawing.Point(0, 0);
            this.flpAcao.Name = "flpAcao";
            this.flpAcao.Size = new System.Drawing.Size(784, 49);
            this.flpAcao.TabIndex = 0;
            // 
            // btnFechar
            // 
            this.btnFechar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnFechar.Image = global::SWS.Properties.Resources.close;
            this.btnFechar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnFechar.Location = new System.Drawing.Point(3, 3);
            this.btnFechar.Name = "btnFechar";
            this.btnFechar.Size = new System.Drawing.Size(70, 23);
            this.btnFechar.TabIndex = 16;
            this.btnFechar.TabStop = false;
            this.btnFechar.Text = "&Fechar";
            this.btnFechar.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnFechar.UseVisualStyleBackColor = true;
            this.btnFechar.Click += new System.EventHandler(this.btnFechar_Click);
            // 
            // btnExcluirCliente
            // 
            this.btnExcluirCliente.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnExcluirCliente.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnExcluirCliente.Image = global::SWS.Properties.Resources.close;
            this.btnExcluirCliente.Location = new System.Drawing.Point(729, 3);
            this.btnExcluirCliente.Name = "btnExcluirCliente";
            this.btnExcluirCliente.Size = new System.Drawing.Size(27, 21);
            this.btnExcluirCliente.TabIndex = 34;
            this.btnExcluirCliente.TabStop = false;
            this.btnExcluirCliente.TextAlign = System.Drawing.ContentAlignment.TopRight;
            this.btnExcluirCliente.UseVisualStyleBackColor = true;
            this.btnExcluirCliente.Click += new System.EventHandler(this.btnExcluirCliente_Click);
            // 
            // btnCliente
            // 
            this.btnCliente.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnCliente.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCliente.Image = global::SWS.Properties.Resources.busca;
            this.btnCliente.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnCliente.Location = new System.Drawing.Point(703, 3);
            this.btnCliente.Name = "btnCliente";
            this.btnCliente.Size = new System.Drawing.Size(27, 21);
            this.btnCliente.TabIndex = 1;
            this.btnCliente.TextAlign = System.Drawing.ContentAlignment.TopRight;
            this.btnCliente.UseVisualStyleBackColor = true;
            this.btnCliente.Click += new System.EventHandler(this.btnCliente_Click);
            // 
            // textCliente
            // 
            this.textCliente.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.textCliente.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textCliente.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textCliente.Location = new System.Drawing.Point(119, 3);
            this.textCliente.Name = "textCliente";
            this.textCliente.ReadOnly = true;
            this.textCliente.Size = new System.Drawing.Size(587, 21);
            this.textCliente.TabIndex = 33;
            this.textCliente.TabStop = false;
            // 
            // lblCliente
            // 
            this.lblCliente.BackColor = System.Drawing.Color.LightGray;
            this.lblCliente.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblCliente.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCliente.Location = new System.Drawing.Point(3, 3);
            this.lblCliente.Name = "lblCliente";
            this.lblCliente.ReadOnly = true;
            this.lblCliente.Size = new System.Drawing.Size(117, 21);
            this.lblCliente.TabIndex = 31;
            this.lblCliente.TabStop = false;
            this.lblCliente.Text = "Cliente";
            // 
            // textCodigo
            // 
            this.textCodigo.BackColor = System.Drawing.Color.White;
            this.textCodigo.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textCodigo.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textCodigo.ForeColor = System.Drawing.Color.Blue;
            this.textCodigo.Location = new System.Drawing.Point(119, 23);
            this.textCodigo.Mask = "0000,00,000000/00";
            this.textCodigo.Name = "textCodigo";
            this.textCodigo.PromptChar = ' ';
            this.textCodigo.Size = new System.Drawing.Size(235, 21);
            this.textCodigo.TabIndex = 2;
            // 
            // lblCodigo
            // 
            this.lblCodigo.BackColor = System.Drawing.Color.LightGray;
            this.lblCodigo.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblCodigo.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCodigo.Location = new System.Drawing.Point(3, 23);
            this.lblCodigo.Name = "lblCodigo";
            this.lblCodigo.ReadOnly = true;
            this.lblCodigo.Size = new System.Drawing.Size(117, 21);
            this.lblCodigo.TabIndex = 35;
            this.lblCodigo.TabStop = false;
            this.lblCodigo.Text = "Código";
            // 
            // grbContrato
            // 
            this.grbContrato.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.grbContrato.Controls.Add(this.dgvContrato);
            this.grbContrato.Location = new System.Drawing.Point(3, 90);
            this.grbContrato.Name = "grbContrato";
            this.grbContrato.Size = new System.Drawing.Size(753, 197);
            this.grbContrato.TabIndex = 37;
            this.grbContrato.TabStop = false;
            this.grbContrato.Text = "Contratos";
            // 
            // dgvContrato
            // 
            this.dgvContrato.AllowUserToAddRows = false;
            this.dgvContrato.AllowUserToDeleteRows = false;
            this.dgvContrato.AllowUserToOrderColumns = true;
            this.dgvContrato.AllowUserToResizeRows = false;
            this.dgvContrato.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this.dgvContrato.BackgroundColor = System.Drawing.Color.White;
            this.dgvContrato.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle3.BackColor = System.Drawing.Color.LightGray;
            dataGridViewCellStyle3.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvContrato.DefaultCellStyle = dataGridViewCellStyle3;
            this.dgvContrato.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvContrato.Location = new System.Drawing.Point(3, 16);
            this.dgvContrato.MultiSelect = false;
            this.dgvContrato.Name = "dgvContrato";
            this.dgvContrato.ReadOnly = true;
            this.dgvContrato.RowHeadersWidth = 30;
            this.dgvContrato.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvContrato.Size = new System.Drawing.Size(747, 178);
            this.dgvContrato.TabIndex = 4;
            // 
            // flpAcaoBuscar
            // 
            this.flpAcaoBuscar.Controls.Add(this.btnBuscar);
            this.flpAcaoBuscar.Location = new System.Drawing.Point(3, 51);
            this.flpAcaoBuscar.Name = "flpAcaoBuscar";
            this.flpAcaoBuscar.Size = new System.Drawing.Size(753, 33);
            this.flpAcaoBuscar.TabIndex = 3;
            this.flpAcaoBuscar.TabStop = true;
            // 
            // btnBuscar
            // 
            this.btnBuscar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnBuscar.Image = global::SWS.Properties.Resources.lupa;
            this.btnBuscar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnBuscar.Location = new System.Drawing.Point(3, 3);
            this.btnBuscar.Name = "btnBuscar";
            this.btnBuscar.Size = new System.Drawing.Size(70, 23);
            this.btnBuscar.TabIndex = 3;
            this.btnBuscar.Text = "&Buscar";
            this.btnBuscar.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnBuscar.UseVisualStyleBackColor = true;
            this.btnBuscar.Click += new System.EventHandler(this.btnBuscar_Click);
            // 
            // btnExcluirClienteDuplicado
            // 
            this.btnExcluirClienteDuplicado.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnExcluirClienteDuplicado.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnExcluirClienteDuplicado.Image = global::SWS.Properties.Resources.close;
            this.btnExcluirClienteDuplicado.Location = new System.Drawing.Point(732, 293);
            this.btnExcluirClienteDuplicado.Name = "btnExcluirClienteDuplicado";
            this.btnExcluirClienteDuplicado.Size = new System.Drawing.Size(27, 21);
            this.btnExcluirClienteDuplicado.TabIndex = 42;
            this.btnExcluirClienteDuplicado.TabStop = false;
            this.btnExcluirClienteDuplicado.TextAlign = System.Drawing.ContentAlignment.TopRight;
            this.btnExcluirClienteDuplicado.UseVisualStyleBackColor = true;
            this.btnExcluirClienteDuplicado.Click += new System.EventHandler(this.btnExcluirClienteDuplicado_Click);
            // 
            // btnClienteDuplicado
            // 
            this.btnClienteDuplicado.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnClienteDuplicado.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnClienteDuplicado.Image = global::SWS.Properties.Resources.busca;
            this.btnClienteDuplicado.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnClienteDuplicado.Location = new System.Drawing.Point(706, 293);
            this.btnClienteDuplicado.Name = "btnClienteDuplicado";
            this.btnClienteDuplicado.Size = new System.Drawing.Size(27, 21);
            this.btnClienteDuplicado.TabIndex = 5;
            this.btnClienteDuplicado.TextAlign = System.Drawing.ContentAlignment.TopRight;
            this.btnClienteDuplicado.UseVisualStyleBackColor = true;
            this.btnClienteDuplicado.Click += new System.EventHandler(this.btnClienteDuplicado_Click);
            // 
            // textClienteDuplicado
            // 
            this.textClienteDuplicado.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.textClienteDuplicado.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textClienteDuplicado.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textClienteDuplicado.Location = new System.Drawing.Point(122, 293);
            this.textClienteDuplicado.Name = "textClienteDuplicado";
            this.textClienteDuplicado.ReadOnly = true;
            this.textClienteDuplicado.Size = new System.Drawing.Size(587, 21);
            this.textClienteDuplicado.TabIndex = 41;
            this.textClienteDuplicado.TabStop = false;
            // 
            // lblClienteDuplicado
            // 
            this.lblClienteDuplicado.BackColor = System.Drawing.Color.LightGray;
            this.lblClienteDuplicado.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblClienteDuplicado.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblClienteDuplicado.Location = new System.Drawing.Point(6, 293);
            this.lblClienteDuplicado.Name = "lblClienteDuplicado";
            this.lblClienteDuplicado.ReadOnly = true;
            this.lblClienteDuplicado.Size = new System.Drawing.Size(117, 21);
            this.lblClienteDuplicado.TabIndex = 39;
            this.lblClienteDuplicado.TabStop = false;
            this.lblClienteDuplicado.Text = "Cliente para copiar";
            // 
            // flowLayoutPanel1
            // 
            this.flowLayoutPanel1.Controls.Add(this.btnCopiar);
            this.flowLayoutPanel1.Location = new System.Drawing.Point(7, 323);
            this.flowLayoutPanel1.Name = "flowLayoutPanel1";
            this.flowLayoutPanel1.Size = new System.Drawing.Size(753, 33);
            this.flowLayoutPanel1.TabIndex = 6;
            this.flowLayoutPanel1.TabStop = true;
            // 
            // btnCopiar
            // 
            this.btnCopiar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnCopiar.Image = global::SWS.Properties.Resources.copiar;
            this.btnCopiar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnCopiar.Location = new System.Drawing.Point(3, 3);
            this.btnCopiar.Name = "btnCopiar";
            this.btnCopiar.Size = new System.Drawing.Size(70, 23);
            this.btnCopiar.TabIndex = 6;
            this.btnCopiar.Text = "&Copiar";
            this.btnCopiar.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnCopiar.UseVisualStyleBackColor = true;
            this.btnCopiar.Click += new System.EventHandler(this.btnCopiar_Click);
            // 
            // frmContratoCopiar
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(784, 561);
            this.Name = "frmContratoCopiar";
            this.Text = "COPIAR CONTRATO";
            this.scForm.Panel1.ResumeLayout(false);
            this.scForm.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.scForm)).EndInit();
            this.scForm.ResumeLayout(false);
            this.pnlForm.ResumeLayout(false);
            this.pnlForm.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.banner)).EndInit();
            this.flpAcao.ResumeLayout(false);
            this.grbContrato.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvContrato)).EndInit();
            this.flpAcaoBuscar.ResumeLayout(false);
            this.flowLayoutPanel1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.FlowLayoutPanel flpAcao;
        private System.Windows.Forms.Button btnFechar;
        private System.Windows.Forms.Button btnExcluirCliente;
        private System.Windows.Forms.Button btnCliente;
        private System.Windows.Forms.TextBox textCliente;
        private System.Windows.Forms.TextBox lblCliente;
        private System.Windows.Forms.MaskedTextBox textCodigo;
        private System.Windows.Forms.TextBox lblCodigo;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel1;
        private System.Windows.Forms.Button btnCopiar;
        private System.Windows.Forms.Button btnExcluirClienteDuplicado;
        private System.Windows.Forms.Button btnClienteDuplicado;
        private System.Windows.Forms.TextBox textClienteDuplicado;
        private System.Windows.Forms.TextBox lblClienteDuplicado;
        private System.Windows.Forms.FlowLayoutPanel flpAcaoBuscar;
        private System.Windows.Forms.Button btnBuscar;
        private System.Windows.Forms.GroupBox grbContrato;
        private System.Windows.Forms.DataGridView dgvContrato;
    }
}