﻿using SWS.Entidade;
using SWS.Facade;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace SWS.View
{
    public partial class frmComplementoAgenteEsocial : frmTemplateConsulta
    {
        private ComplementoAgenteEsocial complementoAgenteEsocial;

        public ComplementoAgenteEsocial ComplementoAgenteEsocial
        {
            get { return complementoAgenteEsocial; }
            set { complementoAgenteEsocial = value; }
        }

        private Risco risco;
        
        public frmComplementoAgenteEsocial()
        {
            InitializeComponent();
            montaDataGrid();
            ActiveControl = textProcedimento;

        }

        private void btnConfirmar_Click(object sender, EventArgs e)
        {
            try
            {
                if (this.dgvProcedimento.CurrentRow == null)
                    throw new Exception("selecione uma  linha.");

                ComplementoAgenteEsocial = new ComplementoAgenteEsocial((long)dgvProcedimento.CurrentRow.Cells[0].Value, (string)dgvProcedimento.CurrentRow.Cells[1].Value, (string)dgvProcedimento.CurrentRow.Cells[2].Value);
                this.Close();

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }

        private void btnFechar_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnProcedimento_Click(object sender, EventArgs e)
        {
            montaDataGrid();
        }

        private void montaDataGrid()
        {
            try
            {
                this.Cursor = Cursors.WaitCursor;

                EsocialFacade esocialFacade = EsocialFacade.getInstance();

                this.dgvProcedimento.Columns.Clear();
                this.dgvProcedimento.ColumnCount = 3;
                this.dgvProcedimento.Columns[0].HeaderText = "id";
                this.dgvProcedimento.Columns[0].Visible = false;
                this.dgvProcedimento.Columns[1].HeaderText = "Codigo";
                this.dgvProcedimento.Columns[2].HeaderText = "Descrição Agente";

                foreach (ComplementoAgenteEsocial complementoAgenteEsocial in esocialFacade.findAllComplementoAgenteEsocialByFilter(new ComplementoAgenteEsocial(null, null, textProcedimento.Text)))
                    dgvProcedimento.Rows.Add(complementoAgenteEsocial.Id, complementoAgenteEsocial.Codigo, complementoAgenteEsocial.Descricao);


            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }


        }
    }
}
