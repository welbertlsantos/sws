﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SegSis.Resources
{
    class ConfigurationConstans
    {
        public static String DIRETORIO = "DIRETORIO_PADRAO_APLICACAO";
        public static String ISS_PADRAO = "ALIQUOTA_SERVICO";
        public static String PIS = "PIS_PERCENTUAL";
        public static String COFINS = "COFINS_PERCENTUAL";
        public static String IR = "IR_PERCENTUAL";
        public static String CSSL = "CSLL_PERCENTUAL";
        public static String PERCENTUAL_JUROS_MENSAL_PRORROGACAO_CRC = "PERCENTUAL_JUROS_MENSAL_PRORROGACAO_CRC";
        public static String QTD_VIAS_IMPRESSORA_TERMICA = "QTD_VIAS_IMPRESSORA_TERMICA";
        public static String EMPRESA_IMPRESSORA_TERMICA = "EMPRESA_IMPRESSORA_TERMICA";
        public static String SERVIDOR_IMPRESSORA_TERMICA = "SERVIDOR_IMPRESSORA_TERMICA";
        public static String NOME_IMPRESSORA_TERMICA = "NOME_IMPRESSORA_TERMICA";
        public static String SERIAL_CLIENTE = "SERIAL_CLIENTE";
        public static String GERA_MOVIMENTO_FINALIZACAO_EXAME = "GERA_MOVIMENTO_FINALIZACAO_EXAME";
        public static String VALOR_MINIMO_ISS = "VALOR_MINIMO_ISS";
        public static String VALOR_MINIO_IR = "VALOR_MINIO_IR";
        public static String GRAVA_PRODUTO_AUTOMATICO = "GRAVA_PRODUTO_AUTOMATICO";
        public static String ASO_2_VIA = "ASO_2_VIA";
        public static String ASO_TRANSCRITO = "ASO_TRANSCRITO";
        public static String ASO_INCLUSAO_EXAMES = "ASO_INCLUSAO_EXAMES";
        public static String SERVIDOR_SMTP = "SERVIDOR_SMTP";
        public static String NUMERO_PORTA = "NUMERO_PORTA";
        public static String SSL = "SSL";
        public static String EMAIL_ENVIO = "EMAIL_ENVIO";
        public static String SENHA_EMAIL = "SENHA_EMAIL";

    }
}
