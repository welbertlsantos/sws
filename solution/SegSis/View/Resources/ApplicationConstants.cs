﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SegSis.View.Resources
{
    class ApplicationConstants
    {
        public static String ATIVO = "A";
        public static String DESATIVADO = "I";
        public static String ON = "1";
        public static String OFF = "0";
        public static String FECHADO = "F";
        public static String CONSTRUCAO = "C";
        public static String REJEITADO = "R";
        public static String PRINCIPAL = "S";
        public static String SEGUNDARIO = "N";
        public static String NAO_APLICA = "NA";
        public static String OBRIGATORIO = "O";
        public static String EVENTUAL = "E";
        public static String MEDIDA_CONTROLE = "MC";
        public static String EPC = "EPC";
        public static String EPI = "EPI";
        public static String PENDENTE = "P";
        public static String BOLETO = "BOLETO";

        public static String ASO = "AS";
        public static String PCMSO = "PC";
        public static String PPRA = "PP";


        public static Double QTD_ITENS_QUADRO_GHE = 8;
        public static Int32 QTD_ITENS_QUADRO_REVISAO = 16;
        public static Int32 QTD_ITENS_QUADRO_CRONOGRAMA = 15;
        public static Int32 QTD_ITENS_QUADRO_PRINCIPAIS_MAQUINAS = 34;
        public static Int32 QTD_ITENS_QUADRO_EPC = 31;
        
        public static Double QTD_ITENS_QUADRO_EXAME = 9;
        public static Double QTD_ITENS_QUADRO_MEDICO = 53;
        public static Double QTD_ITENS_QUADRO_MATERIAL_HOSPITALAR = 35;
        public static Double QTD_ITENS_QUADRO_HOSPITAL = 3;

        public static String EXAME_EXTRA = "EE";
        public static String EXAME_PCMSO = "EP";

        public static String ATENDER_EXAME = "AE";
        public static String FINALIZAR_EXAME = "FE";
        public static String DEVOLVER_EXAME = "DE";
        public static String CANCELAR_EXAME = "CE";
        public static String ESTORNAR_FINALIZACAO = "EF";

        public static String RELATORIO_PPRA = "PP";
        public static String RELATORIO_PCMSO = "PC";

        public static String APTO = "APTO";
        public static String INAPTO = "INAPTO";

        public static String BAIXADO = "B";
        public static String ABERTA = "A";
        public static String DESDOBRADA = "D";
        public static String CANCELADA = "C";

        public static String NORMAL = "N";
        public static String ALTERADO = "A";

        public static String LAUDOAPTO = "A";
        public static String LAUDOINAPTO = "I";

        public static String UNIDADE = "U";
        public static String CREDENCIADA = "C";

        public static String EXAME = "E";
        public static String PRODUTO = "P";

        public static String ORDENACAOCURVAABC = "A";
        public static String ORDENACAODESCRICAO = "D";

        public static String ASO_INCLUSAO_EXAMS = "IE";
        public static String ASO_TRANSCRITO = "AT";

        public static String ATENDIMENTO = "ASO";

        /* protocolo */

        public static String DELETEPROTOCOLO = "DELETE";
        public static String FINALIZAPROTOCOLO = "FINALIZA";

        public static String DOCUMENTO = "D";
        public static String DOCUMENTO_PROTOCOLO = "DP";
        

        /* pesquisa cliente */

        public static String RAZAO_SOCIAL = "R";
        public static String NOME_FANTASIA = "F";
        public static String CNPJ = "C";

        /* relatorio laudo atendimentos */

        public static String LAUDADO = "L";
        public static String NAO_LAUDADO = "NL";
        public static String TODOS = "T";
        public static String ATENDIMENTO_RELATORIO = "A";
        public static String ATENDIMENTO_EXAME_RELATORIO = "AE";
        public static String SINTETICO = "S";
        public static String ANALITICO = "A";
        public static String ORDEM_CLIENTE = "C";
        public static String EMISSAO = "E";
        public static String VENCIMENTO = "V";
        public static String ESTATISTICO = "E";
        public static String ATENDIDO = "A";
        public static String NAO_ATENDIDO = "P";
        public static String TIPO_PERIODICO = "PER";
        public static String TIPO_ADMISSIONAL = "ADM";
        public static String TIPO_DEMISSIONAL = "DEM";
        public static String TIPO_RETORNO_TRABALHO = "RET";
        public static String TIPO_MUDANCA_FUNCAO = "MUD";
        public static String TIPO_OUTROS = "OUT";

        /* laudo prontuario */

        public static String LAUDO1 = "APTO PARA A FUNÇÃO";
        public static String LAUDO2 = "INAPTO PARA A FUNÇÃO";
        public static String LAUDO3 = "APTO PARA O RETORNO AO TRABALHO";
        public static String LAUDO4 = "APTO PARA A FUNÇÃO COM RECOMENDAÇÕES (PCA), (HAS) E (DM) ";
        public static String LAUDO5 = "ENCAMINHADO PARA O INSS";
        public static String LAUDO6 = "APTO AO RETORNO AO TRABALHO COM RESTRIÇÕES";

        /* contrato comercial */

        public static String CONTRATO_GRAVADO = "G";
        public static String CONTRATO_FECHADO = "F";
        public static String CONTRATO_ENCERRADO = "E";
        public static String CONTRATO_CANCELADO = "C";
        public static String PROPOSTA = "P";
        public static String CONTRATO = "C";

        public static String BUSCA_CODIGO_CONTRATO = "T";
        public static String BUSCA_CNPJ = "C";
        public static String ADITIVO = "A";

        public static String FUNCIONARIO_NOME = "N";
        public static String FUNCIONARIO_CPF = "C";
        public static String FUNCIONARIO_RG = "R";


    }
}
