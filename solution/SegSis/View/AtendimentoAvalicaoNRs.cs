﻿using SWS.View.ViewHelper;
using SWS.ViewHelper;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace SWS.View
{
    public partial class frmAtendimentoAvalicaoNRs : frmTemplateConsulta
    {
        private bool altura;

        public bool Altura
        {
            get { return altura; }
            set { altura = value; }
        }
        private bool confinado;

        public bool Confinado
        {
            get { return confinado; }
            set { confinado = value; }
        }

        private bool eletricidade;

        public bool Eletricidade
        {
            get { return eletricidade; }
            set { eletricidade = value; }
        }
        
        public frmAtendimentoAvalicaoNRs(bool altura, bool confinado, bool eletricidade)
        {
            InitializeComponent();
            ActiveControl = cbAltura;

            this.Altura = altura;
            this.Confinado = confinado;
            this.Eletricidade = eletricidade;

            ComboHelper.Boolean(cbAltura, false);
            ComboHelper.Boolean(cbConfinado, false);
            ComboHelper.Boolean(cbEletricidade, false);

            cbAltura.SelectedValue = altura == true ? "true" : "false";
            cbConfinado.SelectedValue = confinado == true ? "true" : "false";
            cbEletricidade.SelectedValue = eletricidade == true ? "true" : "false";
        }

        private void frmAtendimentoAvalicaoNRs_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter || e.KeyCode == Keys.Return)
                SendKeys.Send("{TAB}");
        }

        private void cbConfinado_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (Convert.ToBoolean(((SelectItem)cbConfinado.SelectedItem).Valor))
                confinado = true;
            else
                confinado = false;
        }

        private void cbAltura_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (Convert.ToBoolean(((SelectItem)cbAltura.SelectedItem).Valor))
                altura = true;
            else
                altura = false;
        }

        private void btnFechar_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnConfirma_Click(object sender, EventArgs e)
        {
            try
            {
                Altura = string.Equals(((SelectItem)cbAltura.SelectedItem).Valor, "true") ? true : false;
                Confinado = string.Equals(((SelectItem)cbConfinado.SelectedItem).Valor, "true") ? true : false;
                Eletricidade = string.Equals(((SelectItem)cbEletricidade.SelectedItem).Valor, "true") ? true : false;
                this.Close();
               
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void cbEletricidade_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (Convert.ToBoolean(((SelectItem)cbEletricidade.SelectedItem).Valor))
                eletricidade = true;
            else
                eletricidade = false;
        }
    }
}
