﻿namespace SWS.View
{
    partial class frmMenu
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmMenu));
            this.Tre_Principal = new System.Windows.Forms.TreeView();
            this.PrincipalImage = new System.Windows.Forms.ImageList(this.components);
            this.lbl_usuario = new System.Windows.Forms.Label();
            this.btn_sair = new System.Windows.Forms.Button();
            this.btn_alterarSenha = new System.Windows.Forms.Button();
            this.tHorario = new System.Windows.Forms.Timer(this.components);
            this.pcBoxEmail = new System.Windows.Forms.PictureBox();
            this.lblEmail = new System.Windows.Forms.Label();
            this.btEnviarEmail = new System.Windows.Forms.Button();
            this.btConsultar = new System.Windows.Forms.Button();
            this.pnlEmail = new System.Windows.Forms.Panel();
            this.flpAcaoMenu = new System.Windows.Forms.FlowLayoutPanel();
            this.statusSistema = new System.Windows.Forms.StatusStrip();
            this.lblVersao = new System.Windows.Forms.ToolStripStatusLabel();
            this.lblHora = new System.Windows.Forms.ToolStripStatusLabel();
            this.lblUsuario = new System.Windows.Forms.ToolStripStatusLabel();
            this.lblEmpresa = new System.Windows.Forms.ToolStripStatusLabel();
            this.spMenu = new System.Windows.Forms.SplitContainer();
            this.panel_banner.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox_banner)).BeginInit();
            this.panel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pcBoxEmail)).BeginInit();
            this.pnlEmail.SuspendLayout();
            this.flpAcaoMenu.SuspendLayout();
            this.statusSistema.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.spMenu)).BeginInit();
            this.spMenu.Panel1.SuspendLayout();
            this.spMenu.Panel2.SuspendLayout();
            this.spMenu.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel_banner
            // 
            this.panel_banner.Size = new System.Drawing.Size(801, 42);
            // 
            // panel
            // 
            this.panel.Controls.Add(this.spMenu);
            this.panel.Controls.Add(this.statusSistema);
            this.panel.Controls.Add(this.lbl_usuario);
            // 
            // Tre_Principal
            // 
            this.Tre_Principal.BackColor = System.Drawing.SystemColors.Control;
            this.Tre_Principal.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Tre_Principal.FullRowSelect = true;
            this.Tre_Principal.ImageIndex = 0;
            this.Tre_Principal.ImageList = this.PrincipalImage;
            this.Tre_Principal.Location = new System.Drawing.Point(0, 0);
            this.Tre_Principal.Name = "Tre_Principal";
            this.Tre_Principal.SelectedImageIndex = 0;
            this.Tre_Principal.Size = new System.Drawing.Size(262, 496);
            this.Tre_Principal.TabIndex = 0;
            this.Tre_Principal.DoubleClick += new System.EventHandler(this.Tre_Principal_DoubleClick);
            this.Tre_Principal.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.Tre_Principal_KeyPress);
            // 
            // PrincipalImage
            // 
            this.PrincipalImage.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("PrincipalImage.ImageStream")));
            this.PrincipalImage.TransparentColor = System.Drawing.Color.Transparent;
            this.PrincipalImage.Images.SetKeyName(0, "view-tree-icon.png");
            this.PrincipalImage.Images.SetKeyName(1, "view-tree-icon_no.png");
            // 
            // lbl_usuario
            // 
            this.lbl_usuario.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lbl_usuario.Location = new System.Drawing.Point(499, 7);
            this.lbl_usuario.Name = "lbl_usuario";
            this.lbl_usuario.Size = new System.Drawing.Size(276, 119);
            this.lbl_usuario.TabIndex = 3;
            this.lbl_usuario.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // btn_sair
            // 
            this.btn_sair.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btn_sair.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_sair.Image = global::SWS.Properties.Resources.close;
            this.btn_sair.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btn_sair.Location = new System.Drawing.Point(184, 3);
            this.btn_sair.Name = "btn_sair";
            this.btn_sair.Size = new System.Drawing.Size(86, 28);
            this.btn_sair.TabIndex = 9;
            this.btn_sair.Text = "&Sair";
            this.btn_sair.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btn_sair.UseVisualStyleBackColor = true;
            this.btn_sair.Click += new System.EventHandler(this.btn_sair_Click);
            // 
            // btn_alterarSenha
            // 
            this.btn_alterarSenha.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btn_alterarSenha.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btn_alterarSenha.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_alterarSenha.Image = global::SWS.Properties.Resources.password1;
            this.btn_alterarSenha.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btn_alterarSenha.Location = new System.Drawing.Point(92, 3);
            this.btn_alterarSenha.Name = "btn_alterarSenha";
            this.btn_alterarSenha.Size = new System.Drawing.Size(86, 28);
            this.btn_alterarSenha.TabIndex = 10;
            this.btn_alterarSenha.Text = "&Alterar Senha";
            this.btn_alterarSenha.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btn_alterarSenha.UseVisualStyleBackColor = true;
            this.btn_alterarSenha.Click += new System.EventHandler(this.btn_alterarSenha_Click);
            // 
            // tHorario
            // 
            this.tHorario.Enabled = true;
            this.tHorario.Interval = 2;
            this.tHorario.Tick += new System.EventHandler(this.tHorario_Tick);
            // 
            // pcBoxEmail
            // 
            this.pcBoxEmail.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.pcBoxEmail.Image = global::SWS.Properties.Resources.emailPrincipal;
            this.pcBoxEmail.Location = new System.Drawing.Point(33, 3);
            this.pcBoxEmail.Name = "pcBoxEmail";
            this.pcBoxEmail.Size = new System.Drawing.Size(75, 67);
            this.pcBoxEmail.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pcBoxEmail.TabIndex = 11;
            this.pcBoxEmail.TabStop = false;
            // 
            // lblEmail
            // 
            this.lblEmail.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.lblEmail.AutoSize = true;
            this.lblEmail.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblEmail.Location = new System.Drawing.Point(114, 0);
            this.lblEmail.Name = "lblEmail";
            this.lblEmail.Size = new System.Drawing.Size(152, 20);
            this.lblEmail.TabIndex = 12;
            this.lblEmail.Text = "Gerenciar E-mails";
            // 
            // btEnviarEmail
            // 
            this.btEnviarEmail.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btEnviarEmail.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btEnviarEmail.Image = global::SWS.Properties.Resources.email;
            this.btEnviarEmail.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btEnviarEmail.Location = new System.Drawing.Point(118, 23);
            this.btEnviarEmail.Name = "btEnviarEmail";
            this.btEnviarEmail.Size = new System.Drawing.Size(75, 23);
            this.btEnviarEmail.TabIndex = 13;
            this.btEnviarEmail.Text = "Enviar";
            this.btEnviarEmail.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btEnviarEmail.UseVisualStyleBackColor = true;
            this.btEnviarEmail.Click += new System.EventHandler(this.btEnviarEmail_Click);
            // 
            // btConsultar
            // 
            this.btConsultar.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btConsultar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btConsultar.Image = global::SWS.Properties.Resources.busca;
            this.btConsultar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btConsultar.Location = new System.Drawing.Point(195, 23);
            this.btConsultar.Name = "btConsultar";
            this.btConsultar.Size = new System.Drawing.Size(75, 23);
            this.btConsultar.TabIndex = 14;
            this.btConsultar.Text = "Consultar";
            this.btConsultar.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btConsultar.UseVisualStyleBackColor = true;
            this.btConsultar.Click += new System.EventHandler(this.btConsultar_Click);
            // 
            // pnlEmail
            // 
            this.pnlEmail.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.pnlEmail.BackColor = System.Drawing.SystemColors.Control;
            this.pnlEmail.Controls.Add(this.lblEmail);
            this.pnlEmail.Controls.Add(this.btConsultar);
            this.pnlEmail.Controls.Add(this.pcBoxEmail);
            this.pnlEmail.Controls.Add(this.btEnviarEmail);
            this.pnlEmail.Location = new System.Drawing.Point(242, 344);
            this.pnlEmail.Name = "pnlEmail";
            this.pnlEmail.Size = new System.Drawing.Size(273, 100);
            this.pnlEmail.TabIndex = 15;
            // 
            // flpAcaoMenu
            // 
            this.flpAcaoMenu.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.flpAcaoMenu.BackColor = System.Drawing.SystemColors.Control;
            this.flpAcaoMenu.Controls.Add(this.btn_sair);
            this.flpAcaoMenu.Controls.Add(this.btn_alterarSenha);
            this.flpAcaoMenu.Location = new System.Drawing.Point(242, 450);
            this.flpAcaoMenu.Name = "flpAcaoMenu";
            this.flpAcaoMenu.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.flpAcaoMenu.Size = new System.Drawing.Size(273, 37);
            this.flpAcaoMenu.TabIndex = 16;
            // 
            // statusSistema
            // 
            this.statusSistema.BackColor = System.Drawing.Color.DarkGray;
            this.statusSistema.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.lblVersao,
            this.lblHora,
            this.lblUsuario,
            this.lblEmpresa});
            this.statusSistema.Location = new System.Drawing.Point(0, 496);
            this.statusSistema.Name = "statusSistema";
            this.statusSistema.RenderMode = System.Windows.Forms.ToolStripRenderMode.Professional;
            this.statusSistema.Size = new System.Drawing.Size(788, 22);
            this.statusSistema.TabIndex = 17;
            this.statusSistema.Text = "Status do Sistema";
            // 
            // lblVersao
            // 
            this.lblVersao.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblVersao.ForeColor = System.Drawing.Color.MidnightBlue;
            this.lblVersao.Name = "lblVersao";
            this.lblVersao.Size = new System.Drawing.Size(145, 17);
            this.lblVersao.Text = "toolStripStatusLabel1";
            // 
            // lblHora
            // 
            this.lblHora.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblHora.Name = "lblHora";
            this.lblHora.Size = new System.Drawing.Size(124, 17);
            this.lblHora.Text = "toolStripStatusLabel1";
            // 
            // lblUsuario
            // 
            this.lblUsuario.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblUsuario.Name = "lblUsuario";
            this.lblUsuario.Size = new System.Drawing.Size(145, 17);
            this.lblUsuario.Text = "toolStripStatusLabel1";
            // 
            // lblEmpresa
            // 
            this.lblEmpresa.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblEmpresa.Name = "lblEmpresa";
            this.lblEmpresa.Size = new System.Drawing.Size(145, 17);
            this.lblEmpresa.Text = "toolStripStatusLabel1";
            // 
            // spMenu
            // 
            this.spMenu.BackColor = System.Drawing.SystemColors.Control;
            this.spMenu.Dock = System.Windows.Forms.DockStyle.Fill;
            this.spMenu.Location = new System.Drawing.Point(0, 0);
            this.spMenu.Name = "spMenu";
            // 
            // spMenu.Panel1
            // 
            this.spMenu.Panel1.Controls.Add(this.Tre_Principal);
            // 
            // spMenu.Panel2
            // 
            this.spMenu.Panel2.BackColor = System.Drawing.SystemColors.Control;
            this.spMenu.Panel2.Controls.Add(this.pnlEmail);
            this.spMenu.Panel2.Controls.Add(this.flpAcaoMenu);
            this.spMenu.Size = new System.Drawing.Size(788, 496);
            this.spMenu.SplitterDistance = 262;
            this.spMenu.TabIndex = 18;
            // 
            // frmMenu
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.LightGray;
            this.ClientSize = new System.Drawing.Size(794, 568);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.KeyPreview = true;
            this.MaximizeBox = true;
            this.Name = "frmMenu";
            this.Text = "MENU PRINCIPAL";
            this.Activated += new System.EventHandler(this.validarLogado);
            this.Load += new System.EventHandler(this.Frm_Principal_Load);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Frm_Principal_KeyDown);
            this.panel_banner.ResumeLayout(false);
            this.panel_banner.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox_banner)).EndInit();
            this.panel.ResumeLayout(false);
            this.panel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pcBoxEmail)).EndInit();
            this.pnlEmail.ResumeLayout(false);
            this.pnlEmail.PerformLayout();
            this.flpAcaoMenu.ResumeLayout(false);
            this.statusSistema.ResumeLayout(false);
            this.statusSistema.PerformLayout();
            this.spMenu.Panel1.ResumeLayout(false);
            this.spMenu.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.spMenu)).EndInit();
            this.spMenu.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TreeView Tre_Principal;
        private System.Windows.Forms.Label lbl_usuario;
        private System.Windows.Forms.Button btn_sair;
        private System.Windows.Forms.Button btn_alterarSenha;
        private System.Windows.Forms.ImageList PrincipalImage;
        private System.Windows.Forms.Timer tHorario;
        private System.Windows.Forms.Button btConsultar;
        private System.Windows.Forms.Button btEnviarEmail;
        private System.Windows.Forms.Label lblEmail;
        private System.Windows.Forms.PictureBox pcBoxEmail;
        private System.Windows.Forms.Panel pnlEmail;
        private System.Windows.Forms.SplitContainer spMenu;
        private System.Windows.Forms.FlowLayoutPanel flpAcaoMenu;
        private System.Windows.Forms.StatusStrip statusSistema;
        private System.Windows.Forms.ToolStripStatusLabel lblUsuario;
        private System.Windows.Forms.ToolStripStatusLabel lblEmpresa;
        private System.Windows.Forms.ToolStripStatusLabel lblVersao;
        private System.Windows.Forms.ToolStripStatusLabel lblHora;


    }
}