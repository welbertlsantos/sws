﻿using SWS.Entidade;
using SWS.Facade;
using SWS.View.Resources;
using SWS.View.ViewHelper;
using SWS.ViewHelper;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Net.Mail;
using System.Text;
using System.Windows.Forms;

namespace SWS.View
{
    public partial class frmProntuario : frmTemplate
    {

        private Prontuario prontuario;
        private bool exibirConclusao;

        public Prontuario Prontuario
        {
            get { return prontuario; }
            set { prontuario = value; }
        }


        public frmProntuario(Prontuario prontuario, bool conclusao)
        {
            InitializeComponent();
            this.prontuario = prontuario;
            exibirConclusao = conclusao;

            validaPermissao();

            montaDataGridBloco1();
            /*colocando o focus para digitacao da pressão do colaborador */
            textPressao.Focus();

            /* montando bloco 2 */

            textPressao.Text = prontuario.Pressao;
            textFrequencia.Text = prontuario.Frequencia;
            textPeso.Text = Convert.ToString(prontuario.Peso);
            textAltura.Text = Convert.ToString(prontuario.Altura);
            calculaIMC();
            ComboHelper.cbLassegue(cbLassegue);
            cbLassegue.SelectedValue = String.IsNullOrEmpty(prontuario.Lassegue) ? String.Empty : prontuario.Lassegue;

            montaDataGridBloco3();
            montaDataGridBloco4();
            textComentarioAparelho.Text = prontuario.TextComentarioAparelho;

            if (String.Equals(prontuario.Aso.ClienteFuncaoFuncionario.Funcionario.Sexo, "F"))
            {
                grbBloco5.Enabled = true;
                /*preenchendo valores do bloco feminino */

                textParto.Text = Convert.ToString(prontuario.Parto);
                ComboHelper.cbContraceptivo(cbContraceptivo);
                cbContraceptivo.SelectedValue = String.IsNullOrEmpty(prontuario.Metodo) ? String.Empty : prontuario.Metodo;
                if (prontuario.DataUltMenstruacao != null)
                {
                    this.dataUltimaMenstruacao.Checked = true;
                    dataUltimaMenstruacao.Text = Convert.ToString(prontuario.DataUltMenstruacao);
                }
                
            }
            else
                grbBloco5.Enabled = false;

            montaDataGridBloco6();

            /* preenchendo bloco 7 */

            ComboHelper.Boolean(cbPrevidenciaAfastado, false);
            ComboHelper.Boolean(cbPrevidenciaRecurso, false);
            
            cbPrevidenciaAfastado.SelectedValue = prontuario.PreviAfastado ? "true" : "false";
            cbPrevidenciaRecurso.SelectedValue = prontuario.PreviRecurso ? "true" : "false";
            if (prontuario.DataRecurso != null)
            {
                this.dataAfastamento.Checked = true;
                dataAfastamento.Text = Convert.ToString(prontuario.DataRecurso);
            }
            
            textCausa.Text = prontuario.Causa;

            montaDataGridBloco8();

            /* preenchendo bloco 9 */
            if (!String.IsNullOrEmpty(prontuario.LaudoComentario) && exibirConclusao == true)
                textComentario.Text = prontuario.LaudoComentario;
        }

        private void btn_gravar_Click(object sender, EventArgs e)
        {
            try
            {
                AsoFacade asoFacade = AsoFacade.getInstance();

                this.Cursor = Cursors.WaitCursor;

                /* iniciando gravacao bloco2 */

                prontuario.Pressao = textPressao.Text;
                prontuario.Frequencia = textFrequencia.Text;
                prontuario.Peso = String.IsNullOrEmpty(textPeso.Text.Trim()) ? (Decimal?)null : Convert.ToDecimal(textPeso.Text);
                prontuario.Altura = String.IsNullOrEmpty(textAltura.Text.TrimEnd()) ? (Decimal?)null : Convert.ToDecimal(textAltura.Text);
                prontuario.Lassegue = cbLassegue.SelectedIndex == -1 ? String.Empty : Convert.ToString(((SelectItem)cbLassegue.SelectedItem).Valor);

                /* iniciando gravacao bloco3 */

                prontuario.Bebida = (Boolean)dgBloco3.Rows[0].Cells[2].Value;
                prontuario.Fumo = (Boolean)dgBloco3.Rows[1].Cells[2].Value;
                prontuario.Diarreia = (Boolean)dgBloco3.Rows[2].Cells[2].Value;
                prontuario.Tosse = (Boolean)dgBloco3.Rows[3].Cells[2].Value;
                prontuario.Afastado = (Boolean)dgBloco3.Rows[4].Cells[2].Value;
                prontuario.Convulsao = (Boolean)dgBloco3.Rows[5].Cells[2].Value;
                prontuario.Hepatite = (Boolean)dgBloco3.Rows[6].Cells[2].Value;
                prontuario.Acidente = (Boolean)dgBloco3.Rows[7].Cells[2].Value;
                prontuario.Tuberculose = (Boolean)dgBloco3.Rows[8].Cells[2].Value;
                prontuario.Antitetanica = (Boolean)dgBloco3.Rows[9].Cells[2].Value;
                prontuario.AtividadeFisica = (Boolean)dgBloco3.Rows[10].Cells[2].Value;
                prontuario.MedoAltura = (Boolean)dgBloco3.Rows[11].Cells[2].Value;
                prontuario.DoresJuntas = (Boolean)dgBloco3.Rows[12].Cells[2].Value;
                prontuario.Palpitacao = (Boolean)dgBloco3.Rows[13].Cells[2].Value;
                prontuario.Tontura = (Boolean)dgBloco3.Rows[14].Cells[2].Value;
                prontuario.Azia = (Boolean)dgBloco3.Rows[15].Cells[2].Value;
                prontuario.Desmaio = (Boolean)dgBloco3.Rows[16].Cells[2].Value;
                prontuario.DoresCabeca = (Boolean)dgBloco3.Rows[17].Cells[2].Value;
                prontuario.Epilepsia = (Boolean)dgBloco3.Rows[18].Cells[2].Value;
                prontuario.Insonia = (Boolean)dgBloco3.Rows[19].Cells[2].Value;
                prontuario.ProblemaAudicao = (Boolean)dgBloco3.Rows[20].Cells[2].Value;
                prontuario.Tendinite = (Boolean)dgBloco3.Rows[21].Cells[2].Value;
                prontuario.Fratura = (Boolean)dgBloco3.Rows[22].Cells[2].Value;
                prontuario.Alergia = (Boolean)dgBloco3.Rows[23].Cells[2].Value;
                prontuario.Asma = (Boolean)dgBloco3.Rows[24].Cells[2].Value;
                prontuario.Diabete = (Boolean)dgBloco3.Rows[25].Cells[2].Value;
                prontuario.Hernia = (Boolean)dgBloco3.Rows[26].Cells[2].Value;
                prontuario.ManchasPele = (Boolean)dgBloco3.Rows[27].Cells[2].Value;
                prontuario.PressaoAlta = (Boolean)dgBloco3.Rows[28].Cells[2].Value;
                prontuario.ProblemaVisao = (Boolean)dgBloco3.Rows[29].Cells[2].Value;
                prontuario.ProblemaColuna = (Boolean)dgBloco3.Rows[30].Cells[2].Value;
                prontuario.Rinite = (Boolean)dgBloco3.Rows[31].Cells[2].Value;
                prontuario.Varize = (Boolean)dgBloco3.Rows[32].Cells[2].Value;
                prontuario.Oculos = (Boolean)dgBloco3.Rows[33].Cells[2].Value;


                /* iniciando gravacao bloco4 */

                String laudoMucosa = (Boolean)dgBloco4.Rows[0].Cells[2].Value == true ? ApplicationConstants.NORMAL : (Boolean)dgBloco4.Rows[0].Cells[3].Value == true ? ApplicationConstants.ALTERADO : String.Empty;

                prontuario.AparelhoMucosa = laudoMucosa;
                prontuario.AparelhoMucosaObs = String.IsNullOrEmpty((String)dgBloco4.Rows[0].Cells[4].Value) ? String.Empty : (String)dgBloco4.Rows[0].Cells[4].Value;

                String laudoVascular = (Boolean)dgBloco4.Rows[1].Cells[2].Value == true ? ApplicationConstants.NORMAL : (Boolean)dgBloco4.Rows[1].Cells[3].Value == true ? ApplicationConstants.ALTERADO : String.Empty;

                prontuario.AparelhoVascular = laudoVascular;
                prontuario.AparelhoVascularObs = String.IsNullOrEmpty((String)dgBloco4.Rows[1].Cells[4].Value) ? String.Empty : (String)dgBloco4.Rows[1].Cells[4].Value;

                String laudoRespiratorio = (Boolean)dgBloco4.Rows[2].Cells[2].Value == true ? ApplicationConstants.NORMAL : (Boolean)dgBloco4.Rows[2].Cells[3].Value == true ? ApplicationConstants.ALTERADO : String.Empty;

                prontuario.AparelhoRespiratorio = laudoRespiratorio;
                prontuario.AparelhoRespiratorioObs = String.IsNullOrEmpty((String)dgBloco4.Rows[2].Cells[4].Value) ? String.Empty : (String)dgBloco4.Rows[2].Cells[4].Value;

                String laudoOlhoDir = (Boolean)dgBloco4.Rows[3].Cells[2].Value == true ? ApplicationConstants.NORMAL : (Boolean)dgBloco4.Rows[3].Cells[3].Value == true ? ApplicationConstants.ALTERADO : String.Empty;

                prontuario.AparelhoOlhoDir = laudoOlhoDir;
                prontuario.AparelhoOlhoDirObs = String.IsNullOrEmpty((String)dgBloco4.Rows[3].Cells[4].Value) ? String.Empty : (String)dgBloco4.Rows[3].Cells[4].Value;

                String laudoOlhoEsq = (Boolean)dgBloco4.Rows[4].Cells[2].Value == true ? ApplicationConstants.NORMAL : (Boolean)dgBloco4.Rows[4].Cells[3].Value == true ? ApplicationConstants.ALTERADO : String.Empty;

                prontuario.AparelhoOlhoEsq = laudoOlhoEsq;
                prontuario.AparelhoOlhoEsqObs = String.IsNullOrEmpty((String)dgBloco4.Rows[4].Cells[4].Value) ? String.Empty : (String)dgBloco4.Rows[4].Cells[4].Value;

                String laudoProtese = (Boolean)dgBloco4.Rows[5].Cells[2].Value == true ? ApplicationConstants.NORMAL : (Boolean)dgBloco4.Rows[5].Cells[3].Value == true ? ApplicationConstants.ALTERADO : String.Empty;

                prontuario.AparelhoProtese = laudoProtese;
                prontuario.AparelhoProteseObs = String.IsNullOrEmpty((String)dgBloco4.Rows[5].Cells[4].Value) ? String.Empty : (String)dgBloco4.Rows[5].Cells[4].Value;

                String laudoGanglio = (Boolean)dgBloco4.Rows[6].Cells[2].Value == true ? ApplicationConstants.NORMAL : (Boolean)dgBloco4.Rows[6].Cells[3].Value == true ? ApplicationConstants.ALTERADO : String.Empty;

                prontuario.AparelhoGanglio = laudoGanglio;
                prontuario.AparelhoGanglioObs = String.IsNullOrEmpty((String)dgBloco4.Rows[6].Cells[4].Value) ? String.Empty : (String)dgBloco4.Rows[6].Cells[4].Value;

                String laudoAbdomen = (Boolean)dgBloco4.Rows[7].Cells[2].Value == true ? ApplicationConstants.NORMAL : (Boolean)dgBloco4.Rows[7].Cells[3].Value == true ? ApplicationConstants.ALTERADO : String.Empty;

                prontuario.AparelhoAbdomen = laudoAbdomen;
                prontuario.AparelhoAbdomenObs = String.IsNullOrEmpty((String)dgBloco4.Rows[7].Cells[4].Value) ? String.Empty : (String)dgBloco4.Rows[7].Cells[4].Value;

                String laudoUrina = (Boolean)dgBloco4.Rows[8].Cells[2].Value == true ? ApplicationConstants.NORMAL : (Boolean)dgBloco4.Rows[8].Cells[3].Value == true ? ApplicationConstants.ALTERADO : String.Empty;

                prontuario.AparelhoUrina = laudoUrina;
                prontuario.AparelhoUrinaObs = String.IsNullOrEmpty((String)dgBloco4.Rows[8].Cells[4].Value) ? String.Empty : (String)dgBloco4.Rows[8].Cells[4].Value;

                String laudoMembro = (Boolean)dgBloco4.Rows[9].Cells[2].Value == true ? ApplicationConstants.NORMAL : (Boolean)dgBloco4.Rows[9].Cells[3].Value == true ? ApplicationConstants.ALTERADO : String.Empty;

                prontuario.AparelhoMembro = laudoMembro;
                prontuario.AparelhoMembroObs = String.IsNullOrEmpty((String)dgBloco4.Rows[9].Cells[4].Value) ? String.Empty : (String)dgBloco4.Rows[9].Cells[4].Value;

                String laudoColuna = (Boolean)dgBloco4.Rows[10].Cells[2].Value == true ? ApplicationConstants.NORMAL : (Boolean)dgBloco4.Rows[10].Cells[3].Value == true ? ApplicationConstants.ALTERADO : String.Empty;

                prontuario.AparelhoColuna = laudoColuna;
                prontuario.AparelhoColunaObs = String.IsNullOrEmpty((String)dgBloco4.Rows[10].Cells[4].Value) ? String.Empty : (String)dgBloco4.Rows[10].Cells[4].Value;

                String laudoEquilibrio = (Boolean)dgBloco4.Rows[11].Cells[2].Value == true ? ApplicationConstants.NORMAL : (Boolean)dgBloco4.Rows[11].Cells[3].Value == true ? ApplicationConstants.ALTERADO : String.Empty;

                prontuario.AparelhoEquilibrio = laudoEquilibrio;
                prontuario.AparelhoEquilibrioObs = String.IsNullOrEmpty((String)dgBloco4.Rows[11].Cells[4].Value) ? String.Empty : (String)dgBloco4.Rows[11].Cells[4].Value;

                String laudoForca = (Boolean)dgBloco4.Rows[12].Cells[2].Value == true ? ApplicationConstants.NORMAL : (Boolean)dgBloco4.Rows[12].Cells[3].Value == true ? ApplicationConstants.ALTERADO : String.Empty;

                prontuario.AparelhoForca = laudoForca;
                prontuario.AparelhoForcaObs = String.IsNullOrEmpty((String)dgBloco4.Rows[12].Cells[4].Value) ? String.Empty : (String)dgBloco4.Rows[12].Cells[4].Value;

                String laudoMotora = (Boolean)dgBloco4.Rows[13].Cells[2].Value == true ? ApplicationConstants.NORMAL : (Boolean)dgBloco4.Rows[13].Cells[3].Value == true ? ApplicationConstants.ALTERADO : String.Empty;

                prontuario.AparelhoMotor = laudoMotora;
                prontuario.AparelhoMotorObs = String.IsNullOrEmpty((String)dgBloco4.Rows[13].Cells[4].Value) ? String.Empty : (String)dgBloco4.Rows[13].Cells[4].Value;

                prontuario.TextComentarioAparelho = textComentarioAparelho.Text.ToUpper();

                /* iniciando gravacao bloco 5 */

                prontuario.Parto = String.IsNullOrEmpty(textParto.Text) ? 0 : Convert.ToInt32(textParto.Text);
                prontuario.Metodo = cbContraceptivo.SelectedIndex == -1 ? String.Empty : Convert.ToString(((SelectItem)cbContraceptivo.SelectedItem).Valor);
                prontuario.DataUltMenstruacao = this.dataUltimaMenstruacao.Checked ? Convert.ToDateTime(dataUltimaMenstruacao.Text) : (DateTime?)null;

                /* iniciando gravacao bloco 7 */

                prontuario.PreviAfastado = Convert.ToBoolean(((SelectItem)cbPrevidenciaAfastado.SelectedItem).Valor);
                prontuario.PreviRecurso = Convert.ToBoolean(((SelectItem)cbPrevidenciaRecurso.SelectedItem).Valor);
                prontuario.DataRecurso = this.dataAfastamento.Checked ? Convert.ToDateTime(dataAfastamento.Text) : (DateTime?)null;
                prontuario.Causa = textCausa.Text.ToUpper();

                /* iniciando gravacao bloco8 */

                String conclusao = (Boolean)dgBloco8.Rows[0].Cells[1].Value == true ? ApplicationConstants.LAUDO1 :
                    (Boolean)dgBloco8.Rows[1].Cells[1].Value == true ? ApplicationConstants.LAUDO2 :
                        (Boolean)dgBloco8.Rows[2].Cells[1].Value == true ? ApplicationConstants.LAUDO3 :
                        (Boolean)dgBloco8.Rows[3].Cells[1].Value == true ? ApplicationConstants.LAUDO4 :
                        (Boolean)dgBloco8.Rows[4].Cells[1].Value == true ? ApplicationConstants.LAUDO5 :
                        (Boolean)dgBloco8.Rows[5].Cells[1].Value == true ? ApplicationConstants.LAUDO6 :
                        String.Empty;


                prontuario.Laudo = conclusao;
                prontuario.LaudoComentario = textComentario.Text.ToUpper();

                /* incluindo usuário que gravou o prontuário */

                prontuario.Usuario = PermissionamentoFacade.usuarioAutenticado;

                /* enviando prontuário para gravacao */

                if (prontuario.Id == null)
                {
                    prontuario = asoFacade.insertProntuario(prontuario);
                    MessageBox.Show("Prontuário gravado com sucesso.", "Confirmação", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    PermissionamentoFacade permissionamento = PermissionamentoFacade.getInstance();
                    btnEmail.Enabled = permissionamento.hasPermission("PRONTUARIO", "ENVIAR_EMAIL") && true;
                }
                else
                {
                    PermissionamentoFacade permissionamentoFacade = PermissionamentoFacade.getInstance();

                    /* somente será permitido que o usuário que tenha permissão de alterar o prontuario possa gravar as alteracoes " */

                    if (!permissionamentoFacade.hasPermission("PRONTUARIO", "ALTERAR"))
                        throw new Exception("Você não tem permissão para alterar o prontuário.");
                     
                    prontuario = asoFacade.updateProntuario(prontuario);
                    MessageBox.Show("Prontuário gravado com sucesso.", "Confirmação", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }

        private void btnImprimir_Click(object sender, EventArgs e)
        {
            try
            {
                this.Cursor = Cursors.WaitCursor;

                String nomeRelatorio = "PRONTUARIO_ELETRONICO.JASPER";

                if (prontuario.Id == null)
                    throw new Exception("Somente podem ser impressos prontuários já gravados.");

                var process = new Process();

                process.StartInfo.FileName = "Relatorio\\jre7\\bin\\java";
                process.StartInfo.Arguments = " -jar Relatorio\\MiddlewareJasper.jar " + "Relatorio\\Report\\" + nomeRelatorio + " ID_PRONTUARIO:" + prontuario.Id + ":Integer";
                process.StartInfo.UseShellExecute = false;
                process.StartInfo.RedirectStandardError = true;
                process.StartInfo.RedirectStandardInput = true;
                process.StartInfo.RedirectStandardOutput = true;
                process.StartInfo.CreateNoWindow = true;
                process.Start();
                process.StandardOutput.ReadToEnd();

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }

        private void btnEmail_Click(object sender, EventArgs e)
        {
            try
            {
                frmEmailEnviar formEmail = new frmEmailEnviar();
                formEmail.ShowDialog();

                /* Inicinado gravação do e-mail na base */

                if (formEmail.Email != null)
                {
                    /* usuário envio o e-mail e foi gravado com sucesso */
                    if (formEmail.Retorno)
                    {
                        Email email = new Email(null, prontuario, formEmail.Email.To.ToString().ToLower(), formEmail.Email.Subject,
                            formEmail.Email.Body, String.Empty, formEmail.Email.Priority.ToString(), DateTime.Now, PermissionamentoFacade.usuarioAutenticado);

                        /* verificando se tem anexos e gravando no email. */

                        if (formEmail.Email.Attachments.Count > 0)
                        {
                            foreach (Attachment anexo in formEmail.Email.Attachments)
                                email.Anexo += anexo.Name + "; ";
                        }

                        AsoFacade asoFacade = AsoFacade.getInstance();
                        asoFacade.insertEmail(email);

                    }

                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btnVerEmail_Click(object sender, EventArgs e)
        {
            try
            {
                if (prontuario.Id != null)
                {
                    AsoFacade asoFacade = AsoFacade.getInstance();

                    List<Email> emails = asoFacade.findEmailByProntuario(prontuario);

                    frmEmailConsultar emailEnviado = new frmEmailConsultar(emails);
                    emailEnviado.ShowDialog();

                }

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btn_fechar_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Gostaria de salvar o prontuário antes de fechá-lo?", "Atenção", MessageBoxButtons.YesNo) == DialogResult.Yes)
            {
                btn_gravar.PerformClick();
                this.Close();
            }
            else
                this.Close();
        }

        private void dgBloco3_ColumnAdded(object sender, DataGridViewColumnEventArgs e)
        {
            e.Column.SortMode = DataGridViewColumnSortMode.NotSortable;
        }

        private void dgBloco4_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            if ((sender as DataGridView).CurrentCell is DataGridViewCheckBoxCell)
            {
                if (Convert.ToBoolean(((sender as DataGridView).CurrentCell as DataGridViewCheckBoxCell).EditedFormattedValue))
                    (sender as DataGridView).Rows[e.RowIndex].Cells[2].Value = false;
                else
                    (sender as DataGridView).Rows[e.RowIndex].Cells[2].Value = true;

            }
        }

        private void dgBloco4_ColumnAdded(object sender, DataGridViewColumnEventArgs e)
        {
            e.Column.SortMode = DataGridViewColumnSortMode.NotSortable;
        }

        private void dgBloco6_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            /* verificando se o usuário tem permissão para alteração */

            PermissionamentoFacade permissionamento = PermissionamentoFacade.getInstance();
            AsoFacade asoFacade = AsoFacade.getInstance();
            String resultado = String.Empty;

            if (permissionamento.hasPermission("PRONTUARIO", "LAUDAR"))
            {
                /* verificando que célula disparou a informação */

                if (e.ColumnIndex == 3)
                {
                    /* o usuario selecionou normal então será verificado se o alterado está selecionado.
                     * caso esteja marcado, então o alterado será desmarcado. */

                    if (Convert.ToBoolean(dgBloco6.Rows[e.RowIndex].Cells[4].Value) == true)
                        dgBloco6.Rows[e.RowIndex].Cells[4].Value = false;

                }

                if (e.ColumnIndex == 4)
                {
                    /* o usuario selecionou alterado então será verificado se o normal está selecionado.
                     * caso esteja marcado, então o alterado será desmarcado. */

                    if (Convert.ToBoolean(dgBloco6.Rows[e.RowIndex].Cells[3].Value) == true)
                        dgBloco6.Rows[e.RowIndex].Cells[3].Value = false;

                }

            }
        }

        private void dgBloco6_CellEnter(object sender, DataGridViewCellEventArgs e)
        {
            if ((Boolean)dgBloco6.Rows[e.RowIndex].Cells[3].Value == false &&
                (Boolean)dgBloco6.Rows[e.RowIndex].Cells[4].Value == false)
                dgBloco6.Rows[e.RowIndex].Cells[5].ReadOnly = true;
            else
                dgBloco6.Rows[e.RowIndex].Cells[5].ReadOnly = false;
        }

        private void dgBloco6_CellLeave(object sender, DataGridViewCellEventArgs e)
        {
            PermissionamentoFacade permissionamento = PermissionamentoFacade.getInstance();
            AsoFacade asoFacade = AsoFacade.getInstance();
            String resultado = String.Empty;

            if (permissionamento.hasPermission("PRONTUARIO", "LAUDAR"))
            {

                /* verificando que situacao está aplicado ao exame. Normal ou alterado */

                if ((Boolean)dgBloco6.Rows[e.RowIndex].Cells[3].Value == false && (Boolean)dgBloco6.Rows[e.RowIndex].Cells[4].Value == false)
                    resultado = String.Empty;
                else if ((Boolean)dgBloco6.Rows[e.RowIndex].Cells[3].Value == true)
                    resultado = ApplicationConstants.NORMAL;
                else if (((Boolean)dgBloco6.Rows[e.RowIndex].Cells[4].Value == true))
                    resultado = ApplicationConstants.ALTERADO;


                /* montando o objeto para gravacao no banco */

                if (String.Equals(dgBloco6.Rows[e.RowIndex].Cells[2].Value, ApplicationConstants.EXAME_PCMSO))
                    asoFacade.updateGheFonteAgenteExameAsoLaudoAso(new GheFonteAgenteExameAso((Int64)dgBloco6.Rows[e.RowIndex].Cells[0].Value, null, null, null, null, false, false, String.Empty, false, null, null, null, null, false, null, resultado, Convert.ToString(dgBloco6.Rows[e.RowIndex].Cells[5].Value).ToUpper(), false, false, null, null, false, false, null, null, false, true));
                else
                    asoFacade.updateClienteFuncaoExameAsoLaudoAso(new ClienteFuncaoExameASo((Int64)dgBloco6.Rows[e.RowIndex].Cells[0].Value, null, null, null, null, false, false, String.Empty, false, null, null, null, null, false, null, resultado, Convert.ToString(dgBloco6.Rows[e.RowIndex].Cells[5].Value).ToUpper(), false, null, null, null, null, true));

            }
            
        }

        private void dgBloco6_CurrentCellDirtyStateChanged(object sender, EventArgs e)
        {
            /* verificando se o usuário tem permissão para alteração */

            PermissionamentoFacade permissionamento = PermissionamentoFacade.getInstance();

            if (permissionamento.hasPermission("PRONTUARIO", "LAUDAR"))
            {
                if (this.dgBloco6.IsCurrentCellDirty)
                    dgBloco6.CommitEdit(DataGridViewDataErrorContexts.Commit);
            }
        }

        private void dgBloco6_EditingControlShowing(object sender, DataGridViewEditingControlShowingEventArgs e)
        {
            PermissionamentoFacade permissionamento = PermissionamentoFacade.getInstance();

            if (permissionamento.hasPermission("PRONTUARIO", "LAUDAR"))
            {
                if (dgBloco6.CurrentCell.ColumnIndex == 5)
                {
                    if (e.Control is TextBox)
                        ((TextBox)e.Control).CharacterCasing = CharacterCasing.Upper;
                }
            }
        }

        private void dgBloco8_CellValueChanged(object sender, DataGridViewCellEventArgs e)
        {
            if ((sender as DataGridView).CurrentCell is DataGridViewCheckBoxCell)
            {
                if (Convert.ToBoolean(((sender as DataGridView).CurrentCell as DataGridViewCheckBoxCell).Value))
                {
                    foreach (DataGridViewRow row in (sender as DataGridView).Rows)
                    {
                        if (row.Index != (sender as DataGridView).CurrentCell.RowIndex && Convert.ToBoolean(row.Cells[e.ColumnIndex].Value) == true)
                            row.Cells[e.ColumnIndex].Value = false;
                    }
                }
            }
        }

        private void validaPermissao()
        {
            PermissionamentoFacade permissionamentoFacade = PermissionamentoFacade.getInstance();

            btnImprimir.Enabled = permissionamentoFacade.hasPermission("PRONTUARIO", "IMPRIMIR");
            btnEmail.Enabled = (permissionamentoFacade.hasPermission("PRONTUARIO", "ENVIAR_EMAIL") && prontuario.Id != null);

        }

        private void montaDataGridBloco1()
        {
            /* montando lista que será usada para montar a grid */

            AsoFacade asoFacade = AsoFacade.getInstance();

            dgBloco1.Columns.Clear();
            dgBloco1.ColumnCount = 2;

            dgBloco1.Columns[0].HeaderText = "Dados";
            dgBloco1.Columns[0].DefaultCellStyle.BackColor = Color.LightGray;

            dgBloco1.Columns[1].HeaderText = "Valores";
            dgBloco1.Columns[1].DefaultCellStyle.BackColor = Color.LightBlue;

            /* adicionando linhas */

            dgBloco1.Rows.Add("Tipo de Atendimento", prontuario.Aso.Periodicidade.Descricao);
            dgBloco1.Rows.Add("Código de atendimento", ValidaCampoHelper.RetornaCodigoAtendimentoFormatado(prontuario.Aso.Codigo));
            dgBloco1.Rows.Add("Nome do Colaborador", prontuario.Aso.ClienteFuncaoFuncionario.Funcionario.Nome);
            dgBloco1.Rows.Add("RG", prontuario.Aso.ClienteFuncaoFuncionario.Funcionario.Rg);
            dgBloco1.Rows.Add("CPF", ValidaCampoHelper.FormataCpf(prontuario.Aso.ClienteFuncaoFuncionario.Funcionario.Cpf));
            dgBloco1.Rows.Add("Cliente", prontuario.Aso.Cliente.RazaoSocial);
            dgBloco1.Rows.Add(prontuario.Aso.ClienteFuncaoFuncionario.ClienteFuncao.Cliente.Cnpj.Length == 14  ? "CNPJ" : "CPF", prontuario.Aso.ClienteFuncaoFuncionario.ClienteFuncao.Cliente.Cnpj.Length == 14 ? ValidaCampoHelper.FormataCnpj(prontuario.Aso.Cliente.Cnpj) : ValidaCampoHelper.FormataCpf(prontuario.Aso.ClienteFuncaoFuncionario.ClienteFuncao.Cliente.Cnpj));
            dgBloco1.Rows.Add("Setor", asoFacade.findDescricaoSetoresByAso(prontuario.Aso).Count > 0 ? asoFacade.findDescricaoSetoresByAso(prontuario.Aso).ElementAt(0) : String.Empty);
            dgBloco1.Rows.Add("Função", prontuario.Aso.ClienteFuncaoFuncionario.ClienteFuncao.Funcao.Descricao);
            dgBloco1.Rows.Add("Matricula", prontuario.Aso.ClienteFuncaoFuncionario.Matricula);
            dgBloco1.Rows.Add("Sexo", String.Equals(prontuario.Aso.ClienteFuncaoFuncionario.Funcionario.Sexo, "M") ? "MASCULINO" : "FEMININO");
            dgBloco1.Rows.Add("Data de Nascimento", String.Format("{0:dd/MM/yyyy}", prontuario.Aso.ClienteFuncaoFuncionario.Funcionario.DataNascimento));
            dgBloco1.Rows.Add("Idade", ValidaCampoHelper.CalculaIdade(prontuario.Aso.ClienteFuncaoFuncionario.Funcionario.DataNascimento));
            dgBloco1.Rows.Add("Tipo Sanguíneo", prontuario.Aso.ClienteFuncaoFuncionario.Funcionario.TipoSangue + prontuario.Aso.ClienteFuncaoFuncionario.Funcionario.FatorRh);

        }

        private void calculaIMC()
        {
            /* método para calcular o imc do colaborador, somente se tiver peso e altura preenchido */

            if (!String.IsNullOrEmpty(textPeso.Text) && Convert.ToDecimal(textPeso.Text) != 0 && !String.IsNullOrEmpty(textAltura.Text) && Convert.ToDecimal(textAltura.Text) != 0)
                textIMC.Text = Convert.ToString(Math.Round(Convert.ToDecimal(textPeso.Text) / (Convert.ToDecimal(textAltura.Text) * Convert.ToDecimal(textAltura.Text)), 2));
            else
                textIMC.Text = String.Empty;
        }

        private void montaDataGridBloco3()
        {
            /* criando lista de perguntas */

            dgBloco3.Columns.Clear();
            dgBloco3.ColumnCount = 2;

            dgBloco3.Columns[0].HeaderText = "Item";
            dgBloco3.Columns[0].DefaultCellStyle.BackColor = Color.LightGray;
            dgBloco3.Columns[0].ReadOnly = true;
            dgBloco3.Columns[0].Width = 40;

            dgBloco3.Columns[1].HeaderText = "Pergunta";
            dgBloco3.Columns[1].DefaultCellStyle.BackColor = Color.LightGray;
            dgBloco3.Columns[1].ReadOnly = false;
            dgBloco3.Columns[1].Width = 300;

            DataGridViewCheckBoxColumn check = new DataGridViewCheckBoxColumn();
            check.Name = "Selecionar";
            dgBloco3.Columns.Add(check);
            dgBloco3.Columns[2].Width = 70;
            dgBloco3.Columns[2].HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleCenter;

            /* preenchendo a tabela lista */

            dgBloco3.Rows.Add(1, "Consome bebida alcóolica regularmente?", prontuario.Bebida);
            dgBloco3.Rows.Add(2, "É fumante?", prontuario.Fumo);
            dgBloco3.Rows.Add(3, "Está com diarreia ou constipação?", prontuario.Diarreia);
            dgBloco3.Rows.Add(4, "Está com tosse?", prontuario.Tosse);
            dgBloco3.Rows.Add(5, "Já esteve afastado do trabalho nos últimos 12 meses?", prontuario.Afastado);
            dgBloco3.Rows.Add(6, "Já teve convulsões?", prontuario.Convulsao);
            dgBloco3.Rows.Add(7, "Já teve hepatite?", prontuario.Hepatite);
            dgBloco3.Rows.Add(8, "Já teve ou sofreu algum acidente no trabalho?", prontuario.Acidente);
            dgBloco3.Rows.Add(9, "Já teve tuberculose?", prontuario.Tuberculose);
            dgBloco3.Rows.Add(10, "Já tomou vacina antitetânica?", prontuario.Antitetanica);
            dgBloco3.Rows.Add(11, "Pratica atividade física regularmente?", prontuario.AtividadeFisica);
            dgBloco3.Rows.Add(12, "Tem medo(fobia) de altura?", prontuario.MedoAltura);
            dgBloco3.Rows.Add(13, "Sente dores nas juntas e articulações?", prontuario.DoresJuntas);
            dgBloco3.Rows.Add(14, "Sente palpitação ou dor no coração?", prontuario.Palpitacao);
            dgBloco3.Rows.Add(15, "Sente tonturas ou zumbidos?", prontuario.Tontura);
            dgBloco3.Rows.Add(16, "Sofre de azia?", prontuario.Azia);
            dgBloco3.Rows.Add(17, "Sofre de desmaios?", prontuario.Desmaio);
            dgBloco3.Rows.Add(18, "Sofre de dores de cabeça com frequência?", prontuario.DoresCabeca);
            dgBloco3.Rows.Add(19, "Sofre de epilepsia?", prontuario.Epilepsia);
            dgBloco3.Rows.Add(20, "Sofre de Insônia?", prontuario.Insonia);
            dgBloco3.Rows.Add(21, "Sofre de problemas auditivos (diminuição da audição)?", prontuario.ProblemaAudicao);
            dgBloco3.Rows.Add(22, "Sofre de tendinite e ou bursite?", prontuario.Tendinite);
            dgBloco3.Rows.Add(23, "Sofreu alguma fratura ou luxação", prontuario.Fratura);
            dgBloco3.Rows.Add(24, "Tem alguma alergia?", prontuario.Alergia);
            dgBloco3.Rows.Add(25, "Tem asma e ou bronquite?", prontuario.Asma);
            dgBloco3.Rows.Add(26, "Tem diabetes?", prontuario.Diabete);
            dgBloco3.Rows.Add(27, "Tem hérnias?", prontuario.Hernia);
            dgBloco3.Rows.Add(28, "Tem manchas na pele?", prontuario.ManchasPele);
            dgBloco3.Rows.Add(29, "Tem problemas de pressão alta?", prontuario.PressaoAlta);
            dgBloco3.Rows.Add(30, "Tem problemas de visão (alteração na visão)?", prontuario.ProblemaVisao);
            dgBloco3.Rows.Add(31, "Tem problemas na coluna?", prontuario.ProblemaColuna);
            dgBloco3.Rows.Add(32, "Tem rinite e ou sinusite?", prontuario.Rinite);
            dgBloco3.Rows.Add(33, "Tem varizes ou edemas?", prontuario.Varize);
            dgBloco3.Rows.Add(34, "Usa óculos?", prontuario.Oculos);

        }

        private void montaDataGridBloco4()
        {
            dgBloco4.Columns.Clear();
            dgBloco4.ColumnCount = 2;

            dgBloco4.Columns[0].HeaderText = "Item";
            dgBloco4.Columns[0].DefaultCellStyle.BackColor = Color.LightGray;
            dgBloco4.Columns[0].ReadOnly = true;
            dgBloco4.Columns[0].Width = 40;

            dgBloco4.Columns[1].HeaderText = "Aparelho";
            dgBloco4.Columns[1].DefaultCellStyle.BackColor = Color.LightGray;
            dgBloco4.Columns[1].ReadOnly = true;
            dgBloco4.Columns[1].Width = 300;

            DataGridViewCheckBoxColumn normal = new DataGridViewCheckBoxColumn();
            normal.Name = "Normal";
            dgBloco4.Columns.Add(normal);
            dgBloco4.Columns[2].HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleCenter;
            dgBloco4.Columns[2].Width = 50;
            dgBloco4.Columns[2].Visible = false;

            DataGridViewCheckBoxColumn alterado = new DataGridViewCheckBoxColumn();
            alterado.Name = "Alterado";
            dgBloco4.Columns.Add(alterado);
            dgBloco4.Columns[3].HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleCenter;
            dgBloco4.Columns[3].Width = 50;

            DataGridViewTextBoxColumn observacao = new DataGridViewTextBoxColumn();
            observacao.Name = "Observações ";
            dgBloco4.Columns.Add(observacao);

            dgBloco4.Columns[4].Width = 400;
            ((DataGridViewTextBoxColumn)dgBloco4.Columns[4]).MaxInputLength = 100;



            /* montando a grid */

            Boolean mucosaNormal = false;
            Boolean mucosaAlterado = false;
            Boolean cardiovascularNormal = false;
            Boolean cardioAlterado = false;
            Boolean respiratorioNormal = false;
            Boolean respiratorioAlterado = false;
            Boolean olhoDirNormal = false;
            Boolean OlhoDirAlterado = false;
            Boolean olhoEsqNormal = false;
            Boolean OlhoEsqAlterado = false;
            Boolean proteseNormal = false;
            Boolean proteseAlterado = false;
            Boolean ganglioNormal = false;
            Boolean ganglioAlterado = false;
            Boolean abdomenNormal = false;
            Boolean abdomenAlterado = false;
            Boolean urinaNormal = false;
            Boolean urinaAlterado = false;
            Boolean membroNormal = false;
            Boolean membroAlterado = false;
            Boolean colunaNormal = false;
            Boolean colunaAlterado = false;
            Boolean equilibrioNormal = false;
            Boolean equilibrioAlterado = false;
            Boolean forcaNormal = false;
            Boolean forcAlterado = false;
            Boolean forcaMotora = false;
            Boolean forcAMotora = false;


            if (prontuario.Id != null)
            {
                mucosaNormal = String.Equals(prontuario.AparelhoMucosa, ApplicationConstants.NORMAL) ? true : false;
                mucosaAlterado = String.Equals(prontuario.AparelhoMucosa, ApplicationConstants.ALTERADO) ? true : false;

                cardiovascularNormal = String.Equals(prontuario.AparelhoVascular, ApplicationConstants.NORMAL) ? true : false;
                cardioAlterado = String.Equals(prontuario.AparelhoVascular, ApplicationConstants.ALTERADO) ? true : false;

                respiratorioNormal = String.Equals(prontuario.AparelhoRespiratorio, ApplicationConstants.NORMAL) ? true : false;
                respiratorioAlterado = String.Equals(prontuario.AparelhoRespiratorio, ApplicationConstants.ALTERADO) ? true : false;

                olhoDirNormal = String.Equals(prontuario.AparelhoOlhoDir, ApplicationConstants.NORMAL) ? true : false;
                OlhoDirAlterado = String.Equals(prontuario.AparelhoOlhoDir, ApplicationConstants.ALTERADO) ? true : false;

                olhoEsqNormal = String.Equals(prontuario.AparelhoOlhoEsq, ApplicationConstants.NORMAL) ? true : false;
                OlhoEsqAlterado = String.Equals(prontuario.AparelhoOlhoEsq, ApplicationConstants.ALTERADO) ? true : false;

                proteseNormal = String.Equals(prontuario.AparelhoProtese, ApplicationConstants.NORMAL) ? true : false;
                proteseAlterado = String.Equals(prontuario.AparelhoProtese, ApplicationConstants.ALTERADO) ? true : false;

                ganglioNormal = String.Equals(prontuario.AparelhoGanglio, ApplicationConstants.NORMAL) ? true : false;
                ganglioAlterado = String.Equals(prontuario.AparelhoGanglio, ApplicationConstants.ALTERADO) ? true : false;

                abdomenNormal = String.Equals(prontuario.AparelhoAbdomen, ApplicationConstants.NORMAL) ? true : false;
                abdomenAlterado = String.Equals(prontuario.AparelhoAbdomen, ApplicationConstants.ALTERADO) ? true : false;

                urinaNormal = String.Equals(prontuario.AparelhoUrina, ApplicationConstants.NORMAL) ? true : false;
                urinaAlterado = String.Equals(prontuario.AparelhoUrina, ApplicationConstants.ALTERADO) ? true : false;

                membroNormal = String.Equals(prontuario.AparelhoMembro, ApplicationConstants.NORMAL) ? true : false;
                membroAlterado = String.Equals(prontuario.AparelhoMembro, ApplicationConstants.ALTERADO) ? true : false;

                colunaNormal = String.Equals(prontuario.AparelhoColuna, ApplicationConstants.NORMAL) ? true : false;
                colunaAlterado = String.Equals(prontuario.AparelhoColuna, ApplicationConstants.ALTERADO) ? true : false;

                equilibrioNormal = String.Equals(prontuario.AparelhoEquilibrio, ApplicationConstants.NORMAL) ? true : false;
                equilibrioAlterado = String.Equals(prontuario.AparelhoEquilibrio, ApplicationConstants.ALTERADO) ? true : false;

                forcaNormal = String.Equals(prontuario.AparelhoForca, ApplicationConstants.NORMAL) ? true : false;
                forcAlterado = String.Equals(prontuario.AparelhoForca, ApplicationConstants.ALTERADO) ? true : false;

                forcaMotora = String.Equals(prontuario.AparelhoMotor, ApplicationConstants.NORMAL) ? true : false;
                forcAMotora = String.Equals(prontuario.AparelhoMotor, ApplicationConstants.ALTERADO) ? true : false;

            }
            else
            {
                mucosaNormal = true;
                cardiovascularNormal = true;
                respiratorioNormal = true;
                olhoDirNormal = true;
                olhoEsqNormal = true;
                proteseNormal = true;
                ganglioNormal = true;
                abdomenNormal = true;
                urinaNormal = true;
                membroNormal = true;
                colunaNormal = true;
                equilibrioNormal = true;
                forcaNormal = true;
                forcaMotora = true;
            }


            dgBloco4.Rows.Add(35, "Mucosa", mucosaNormal, mucosaAlterado, prontuario.AparelhoMucosaObs);
            dgBloco4.Rows.Add(36, "Aparelho cardiovascular", cardiovascularNormal, cardioAlterado, prontuario.AparelhoVascularObs);
            dgBloco4.Rows.Add(37, "Aparelho respiratório", respiratorioNormal, respiratorioAlterado, prontuario.AparelhoRespiratorioObs);
            dgBloco4.Rows.Add(38, "Acuidade visual olho direito", olhoDirNormal, OlhoDirAlterado, prontuario.AparelhoOlhoDirObs);
            dgBloco4.Rows.Add(39, "Acuidade visual olho esquerdo", olhoEsqNormal, OlhoEsqAlterado, prontuario.AparelhoOlhoEsqObs);
            dgBloco4.Rows.Add(40, "Prótese dentária", proteseNormal, proteseAlterado, prontuario.AparelhoProteseObs);
            dgBloco4.Rows.Add(41, "Gânglios", ganglioNormal, ganglioAlterado, prontuario.AparelhoGanglioObs);
            dgBloco4.Rows.Add(42, "Aparelho gastrointestinal (abdômen)", abdomenNormal, abdomenAlterado, prontuario.AparelhoAbdomenObs);
            dgBloco4.Rows.Add(43, "Aparelho gênito urinário", urinaNormal, urinaAlterado, prontuario.AparelhoUrinaObs);
            dgBloco4.Rows.Add(44, "Sistema Osteomuscular e membros", membroNormal, membroAlterado, prontuario.AparelhoMembroObs);
            dgBloco4.Rows.Add(45, "Coluna: C T L S", colunaNormal, colunaAlterado, prontuario.AparelhoColunaObs);
            dgBloco4.Rows.Add(46, "Equilíbrio / Rom Berg", equilibrioNormal, equilibrioAlterado, prontuario.AparelhoEquilibrioObs);
            dgBloco4.Rows.Add(47, "Força muscular", forcaNormal, forcAlterado, prontuario.AparelhoForcaObs);
            dgBloco4.Rows.Add(48, "Coordenação motora", forcaMotora, forcAMotora, prontuario.AparelhoMotorObs);

        }

        private void montaDataGridBloco6()
        {

            /* verificando parâmetro de usuário. 
             * caso o usuário possa laudar no prontuário então ele
             * poderá alterar as informações na grid */

            PermissionamentoFacade permissionamento = PermissionamentoFacade.getInstance();

            dgBloco6.ReadOnly = permissionamento.hasPermission("PRONTUARIO", "LAUDAR") ? false : true;

            dgBloco6.Columns.Clear();

            dgBloco6.ColumnCount = 3;

            dgBloco6.Columns[0].HeaderText = "id";
            dgBloco6.Columns[0].Visible = false;

            dgBloco6.Columns[1].HeaderText = "Exame";
            dgBloco6.Columns[1].DefaultCellStyle.BackColor = Color.LightGray;
            dgBloco6.Columns[1].ReadOnly = true;

            dgBloco6.Columns[2].HeaderText = "TipoExame";
            dgBloco6.Columns[2].Visible = false;

            DataGridViewCheckBoxColumn checkNormal = new DataGridViewCheckBoxColumn();
            checkNormal.Name = "Normal";
            dgBloco6.Columns.Add(checkNormal);
            dgBloco6.Columns["Normal"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
            dgBloco6.Columns["Normal"].HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleCenter;

            DataGridViewCheckBoxColumn checkAlterado = new DataGridViewCheckBoxColumn();
            checkAlterado.Name = "Alterado";
            dgBloco6.Columns.Add(checkAlterado);
            dgBloco6.Columns["Alterado"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
            dgBloco6.Columns["Alterado"].HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleCenter;

            DataGridViewTextBoxColumn textColumn = new DataGridViewTextBoxColumn();
            textColumn.Name = "observacao";
            textColumn.HeaderText = "Observação";
            dgBloco6.Columns.Add(textColumn);

            /* preenchendo a grid com valores da colecao GheFonteAgenteExameAso */
            foreach (GheFonteAgenteExameAso gfaea in prontuario.Aso.ExamePcmso)
            {
                Boolean normal = String.Equals(gfaea.Resultado, ApplicationConstants.NORMAL) ? true : false;
                Boolean alterado = String.Equals(gfaea.Resultado, ApplicationConstants.ALTERADO) ? true : false;

                dgBloco6.Rows.Add(gfaea.Id, gfaea.GheFonteAgenteExame.Exame.Descricao, ApplicationConstants.EXAME_PCMSO, normal, alterado, gfaea.Observacao);
            }

            /* preenchendo a grid com valores da colecao clienteFuncaoExameAso */
            foreach (ClienteFuncaoExameASo cfea in prontuario.Aso.ExameExtra)
            {
                Boolean normal = String.Equals(cfea.Resultado, ApplicationConstants.NORMAL) ? true : false;
                Boolean alterado = String.Equals(cfea.Resultado, ApplicationConstants.ALTERADO) ? true : false;

                dgBloco6.Rows.Add(cfea.Id, cfea.ClienteFuncaoExame.Exame.Descricao, ApplicationConstants.EXAME_EXTRA, normal, alterado, cfea.ObservacaoResultado);

            }

            dgBloco6.Sort(dgBloco6.Columns[0], ListSortDirection.Ascending);

        }

        private void montaDataGridBloco8()
        {
            dgBloco8.Columns.Clear();

            dgBloco8.ColumnCount = 1;

            dgBloco8.Columns[0].HeaderText = "Conclusão";
            dgBloco8.Columns[0].Width = 300;
            dgBloco8.Columns[0].DefaultCellStyle.BackColor = Color.LightGray;

            DataGridViewCheckBoxColumn check = new DataGridViewCheckBoxColumn();
            check.Name = "Selecionar";
            dgBloco8.Columns.Add(check);
            dgBloco8.Columns[1].HeaderText = "Selecionar";
            dgBloco8.Columns[1].Width = 70;
            dgBloco8.Columns[1].HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleCenter;

            /* incluindo os itens da tabela */

            if (exibirConclusao)
            {

                dgBloco8.Rows.Add("Apto para a função", String.Equals(prontuario.Laudo, ApplicationConstants.LAUDO1) ? true : false);
                dgBloco8.Rows.Add("Inapto para a função", String.Equals(prontuario.Laudo, ApplicationConstants.LAUDO2) ? true : false);
                dgBloco8.Rows.Add("Apto para o retorno ao trabalho", String.Equals(prontuario.Laudo, ApplicationConstants.LAUDO3) ? true : false);
                dgBloco8.Rows.Add("Apto para a função com recomendações (PCA), (HAS), (DM)", String.Equals(prontuario.Laudo, ApplicationConstants.LAUDO4) ? true : false);
                dgBloco8.Rows.Add("Encaminhado ao INSS", String.Equals(prontuario.Laudo, ApplicationConstants.LAUDO5) ? true : false);
                dgBloco8.Rows.Add("Apto ao retorno ao trabalho com restrições", String.Equals(prontuario.Laudo, ApplicationConstants.LAUDO6) ? true : false);
            }
            else
            {
                dgBloco8.Rows.Add("Apto para a função", false);
                dgBloco8.Rows.Add("Inapto para a função", false);
                dgBloco8.Rows.Add("Apto para o retorno ao trabalho", false);
                dgBloco8.Rows.Add("Apto para a função com recomendações (PCA), (HAS), (DM)", false);
                dgBloco8.Rows.Add("Encaminhado ao INSS", false);
                dgBloco8.Rows.Add("Apto ao retorno ao trabalho com restrições", false);
            }
        }

        private void btnTabelaIMC_Click(object sender, EventArgs e)
        {
            frmProntuarioIMC formIMC = new frmProntuarioIMC();
            formIMC.ShowDialog();
        }

        private void frmProntuario_KeyDown(object sender, KeyEventArgs e)
        {
            if ((e.KeyCode == Keys.Enter) || (e.KeyCode == Keys.Return))
                SendKeys.Send("{TAB}");
        }

        private void textPeso_Enter(object sender, EventArgs e)
        {
            textPeso.Tag = textPeso.Text;
            textPeso.Text = String.Empty;
        }

        private void textPeso_Leave(object sender, EventArgs e)
        {
            if (String.IsNullOrEmpty(textPeso.Text))
                textPeso.Text = textPeso.Tag.ToString();

            calculaIMC();
        }

        private void textPeso_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = ValidaCampoHelper.FormataDigitacaoNumero(sender, e, textPeso.Text, 2);
        }

        private void textAltura_Enter(object sender, EventArgs e)
        {
            textAltura.Tag = textAltura.Text;
            textAltura.Text = String.Empty;
        }

        private void textAltura_Leave(object sender, EventArgs e)
        {
            if (String.IsNullOrEmpty(textAltura.Text))
                textAltura.Text = textAltura.Tag.ToString();

            calculaIMC();
        }

        private void textAltura_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = ValidaCampoHelper.FormataDigitacaoNumero(sender, e, textAltura.Text, 2);
        }

        private void btnContraceptivoOutros_Click(object sender, EventArgs e)
        {
            if (String.Equals(((SelectItem)(cbContraceptivo.SelectedItem)).Valor, "OUTROS"))
            {
                frmProntuarioOutrosMetodos formOutros = new frmProntuarioOutrosMetodos(prontuario.MetodoObs);
                formOutros.ShowDialog();

                prontuario.MetodoObs = formOutros.Outros;

            }
        }

        private void dgBloco8_CurrentCellDirtyStateChanged(object sender, EventArgs e)
        {
            if (this.dgBloco8.IsCurrentCellDirty)
                dgBloco8.CommitEdit(DataGridViewDataErrorContexts.Commit);
        }

        private void dgBloco4_CellMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            if (e.ColumnIndex == 4)
            {
                if ((Boolean)dgBloco4.Rows[e.RowIndex].Cells[2].Value == true ||
                    (Boolean)dgBloco4.Rows[e.RowIndex].Cells[3].Value == true)
                    dgBloco4.Rows[e.RowIndex].Cells[4].ReadOnly = false;

                else
                    dgBloco4.Rows[e.RowIndex].Cells[4].ReadOnly = true;
            }
        }

        private void cbPrevidenciaAfastado_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (((SelectItem)cbPrevidenciaAfastado.SelectedItem).Valor == "true")
            {
                dataAfastamento.Enabled = true;
                textCausa.Enabled = true;
            }
            else
            {
                dataAfastamento.Enabled = false;
                textCausa.Enabled = false;
            }
        }
        
    }
}
