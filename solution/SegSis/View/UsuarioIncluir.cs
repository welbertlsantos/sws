﻿using SWS.Entidade;
using SWS.Excecao;
using SWS.Facade;
using SWS.Helper;
using SWS.View.Resources;
using SWS.View.ViewHelper;
using SWS.ViewHelper;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Security;
using System.Text;
using System.Windows.Forms;

namespace SWS.View
{
    public partial class frmUsuarioIncluir : frmTemplate
    {
        protected Usuario usuario;

        public Usuario Usuario
        {
            get { return usuario; }
            set { usuario = value; }
        }

        protected Arquivo assinatura;

        public Arquivo Assinatura
        {
            get { return assinatura; }
            set { assinatura = value; }
        }

        protected HashSet<Perfil> perfisSelecionados = new HashSet<Perfil>();

        public HashSet<Perfil> PerfisSelecionados
        {
            get { return perfisSelecionados; }
            set { perfisSelecionados = value; }
        }

        protected FuncaoInterna funcaoInterna;
        protected Medico coordenador;
        protected Empresa empresa;
        protected List<UsuarioCliente> usuarioClientes = new List<UsuarioCliente>();
        
        public frmUsuarioIncluir()
        {
            InitializeComponent();
            ComboHelper.Boolean(cbDocumentacao, false);
            ComboHelper.Boolean(cbExterno, false);
            ComboHelper.identidadeOrgaoEmissor(cbEmissorCertificado);
            ComboHelper.unidadeFederativa(cbUfEmissor);
        }

        protected virtual void bt_gravar_Click(object sender, EventArgs e)
        {
            this.incluirUsuarioErrorProvider.Clear();

            try
            {
                if (validaCamposObrigatorios())
                {

                    /* validando o CPF do usuário */
                    if (!ValidaCampoHelper.ValidaCPF(textCpf.Text))
                        throw new Exception("CPF é inválido.");

                    UsuarioFacade usuarioFacade = UsuarioFacade.getInstance();
                    PermissionamentoFacade permissionamentoFacade = PermissionamentoFacade.getInstance();

                    int numeroUsuariosAtivos = usuarioFacade.listaUsuariosAtivos().Tables[0].Rows.Count;
                    Dictionary<string, string> configuracoes = permissionamentoFacade.findAllConfiguracao();
                    string configNumeroLicencas;

                    configuracoes.TryGetValue("NUMERO_LICENCAS", out configNumeroLicencas);

                    int numeroLicencas;
                    int.TryParse(configNumeroLicencas,out numeroLicencas);

                    if (numeroUsuariosAtivos >= numeroLicencas)
                    {
                        throw new Exception("Não é possível cadastrar novos Usuários. Número de licenças excedido."); 
                    }

                    usuario = new Usuario(null, textNome.Text.Trim(), textCpf.Text, textIdentidade.Text.Trim(), textNumeroRegistro.Text.Trim(), Convert.ToDateTime(dataAdmissao.Value), ApplicationConstants.ATIVO, textLogin.Text.Trim(), Util.getMD5Hash("padrao"), textEmail.Text.Trim(), coordenador, empresa, textMte.Text.Trim(), Convert.ToBoolean(((SelectItem)cbDocumentacao.SelectedItem).Valor), assinatura == null ? null : assinatura.Conteudo, assinatura == null ? null : assinatura.Mimetype, Convert.ToBoolean(((SelectItem)cbExterno.SelectedItem).Valor), textOrgaoEmissor.Text, ((SelectItem)cbUfEmissor.SelectedItem).Valor);

                    usuario.Perfil = perfisSelecionados;
                    usuario.FuncaoInterna = funcaoInterna;
                    usuario.UsuarioCliente = usuarioClientes;
                    
                    usuario = usuarioFacade.novoUsuario(usuario);

                    MessageBox.Show("Usuário incluído com sucesso", "Confirmação", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    
                    this.Close();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        protected void btnLimpar_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Deseja limpar todo o conteúdo da tela?", "Atenção", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
            {
                this.incluirUsuarioErrorProvider.Clear();
                textNome.Text = String.Empty;
                textCpf.Text = String.Empty;
                textIdentidade.Text = String.Empty;
                textEmail.Text = String.Empty;
                textLogin.Text = String.Empty;
                textNumeroRegistro.Text = String.Empty;
                empresa = null;
                textEmpresa.Text = String.Empty;
                coordenador = null;
                textCoordenador.Text = String.Empty;
                dataAdmissao.Checked = false;
                funcaoInterna = null;
                textFuncaoInterna.Text = String.Empty;
                ComboHelper.Boolean(this.cbDocumentacao, false);
                textMte.Text = String.Empty;
                assinatura = null;
                pbAssinatura.Refresh();
                dgvPerfil.Columns.Clear();
                perfisSelecionados.Clear();
                ComboHelper.Boolean(this.cbExterno, false);
                textNome.Focus();
                ComboHelper.identidadeOrgaoEmissor(cbEmissorCertificado);
            }
        }
         
        protected void btn_fechar_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        protected void btnEmpresa_Click(object sender, EventArgs e)
        {
            frmEmpresaBuscar formUsuarioEmpresa = new frmEmpresaBuscar();
            formUsuarioEmpresa.ShowDialog();

            if (formUsuarioEmpresa.Empresa != null)
            {
                empresa = formUsuarioEmpresa.Empresa;
                textEmpresa.Text = empresa.RazaoSocial;
            }
        }

        protected void btnFuncaoInterna_Click(object sender, EventArgs e)
        {
            frmFuncaoInternaBusca formFuncaoInterna = new frmFuncaoInternaBusca();
            formFuncaoInterna.ShowDialog();

            if (formFuncaoInterna.FuncaoInterna != null)
            {
                funcaoInterna = formFuncaoInterna.FuncaoInterna;
                textFuncaoInterna.Text = funcaoInterna.Descricao;

                /* verificando se a função selecionada é de médico. */

                btnMedico.Enabled = funcaoInterna.Id == 4 ? true : false;
            }
        }

        protected void btnMedico_Click(object sender, EventArgs e)
        {
            frmMedicoBuscar formUsuarioMedicoIncluir = new frmMedicoBuscar();
            formUsuarioMedicoIncluir.ShowDialog();

            if (formUsuarioMedicoIncluir.Medico != null)
            {
                coordenador = formUsuarioMedicoIncluir.Medico;
                textCoordenador.Text = coordenador.Nome.ToUpper();
            }
        }

        protected void btnAssinatura_Click(object sender, EventArgs e)
        {
            OpenFileDialog assinaturaDialog = new OpenFileDialog();

            assinaturaDialog.Multiselect = false;
            assinaturaDialog.Title = "Selecionar a imagem da assinatura";
            assinaturaDialog.InitialDirectory = @"C:\";

            //filtra para exibir somente arquivos de imagens
            assinaturaDialog.Filter = "Imagens(*.jpg;*.bmp;*.png)|*.jpg;*.bmp;*.png";
            assinaturaDialog.CheckFileExists = true;
            assinaturaDialog.CheckPathExists = true;
            assinaturaDialog.FilterIndex = 2;
            assinaturaDialog.RestoreDirectory = true;
            assinaturaDialog.ReadOnlyChecked = true;
            assinaturaDialog.ShowReadOnly = true;

            DialogResult dr = assinaturaDialog.ShowDialog();

            if (dr == System.Windows.Forms.DialogResult.OK)
            {
                String nomeArquivo = assinaturaDialog.FileName;
                String nomeArquivoFinal = nomeArquivo.Substring(nomeArquivo.LastIndexOf('\\') + 1);
                String extensaoArquivo = nomeArquivo.Substring(nomeArquivo.LastIndexOf('.') + 1);

                try
                {
                    Image Imagem = Image.FromFile(nomeArquivo);
                    pbAssinatura.SizeMode = PictureBoxSizeMode.StretchImage;
                    if (Imagem.Width > 500 && Imagem.Height > 500)
                        throw new LogoException("A imagem deve ter um tamanho máximo de 500 X 500 pixels.");

                    pbAssinatura.Image = Imagem;

                    byte[] conteudo = FileHelper.CarregarArquivoImagem(nomeArquivo, 5242880);

                    assinatura = new Arquivo(null, nomeArquivoFinal, "image/" + extensaoArquivo.ToUpper(), conteudo.Length, conteudo);
                }
                catch (SecurityException ex)
                {
                    // O usuário  não possui permissão para ler arquivos
                    MessageBox.Show("Erro de segurança Contate o administrador de segurança da rede.\n\n" + "Mensagem : " + ex.Message + "\n\n" + "Detalhes (enviar ao suporte):\n\n" + ex.StackTrace, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
                catch (LogoException ex)
                {
                    MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
                catch (Exception ex)
                {
                    // Não pode carregar a imagem (problemas de permissão)
                    MessageBox.Show("Não é possível exibir a imagem : " + nomeArquivoFinal + ". Você pode não ter permissão para ler o arquivo , ou " + " ele pode estar corrompido.\n\nErro reportado : " + ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
        }

        protected virtual void btn_incluir_perfil_Click(object sender, EventArgs e)
        {
            frmPerfilSelecionar incluirPerfil = new frmPerfilSelecionar();
            incluirPerfil.ShowDialog();

            if (incluirPerfil.Perfil.Count > 0)
            {
                this.perfisSelecionados.UnionWith(incluirPerfil.Perfil);
            }
            montaPerfil();
        }

        protected virtual void btn_excluir_perfil_Click(object sender, EventArgs e)
        {
            try
            {
                if (dgvPerfil.CurrentRow == null)
                    throw new Exception("Selecione um perfil.");

                Perfil perfil = new Perfil(Convert.ToInt64(dgvPerfil.CurrentRow.Cells[0].Value), dgvPerfil.CurrentRow.Cells[1].Value.ToString(),"A");

                perfisSelecionados.Remove(perfil);
                dgvPerfil.Rows.RemoveAt(dgvPerfil.CurrentRow.Index);

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

            montaPerfil();
        }

        protected bool validaCamposObrigatorios()
        {
            bool retorno = true;

            try
            {
                if (string.IsNullOrEmpty(textNome.Text.Trim()))
                {
                    this.incluirUsuarioErrorProvider.SetError(this.textNome, "O nome é obrigatório.");
                    retorno = false;
                }
                if (string.IsNullOrEmpty(textCpf.Text.Replace(".", "").Replace("-", "")))
                {
                    this.incluirUsuarioErrorProvider.SetError(this.textCpf, "CPF é obrigatório.");
                    retorno = false;
                }
                if (string.IsNullOrEmpty(textLogin.Text.Trim()))
                {
                    this.incluirUsuarioErrorProvider.SetError(this.textLogin, "Login é obrigatório.");
                    retorno = false;
                }

                /* somente para usuários internos */
                if (Convert.ToBoolean(((SelectItem)cbExterno.SelectedItem).Valor) == false && !dataAdmissao.Checked)
                {
                    this.incluirUsuarioErrorProvider.SetError(this.dataAdmissao, "Data de admissão do usuário é obrigatório.");
                    retorno = false;
                }
                
                /* somente para usuários internos */
                if (Convert.ToBoolean(((SelectItem)cbExterno.SelectedItem).Valor) == false && funcaoInterna == null)
                {
                    this.incluirUsuarioErrorProvider.SetError(this.btnFuncaoInterna, "O usuário deve estar relacionado a uma função.");
                    retorno = false;
                }
                
                /* somente para usuários internos */
                if (Convert.ToBoolean(((SelectItem)cbExterno.SelectedItem).Valor) == false && empresa == null)
                {
                    this.incluirUsuarioErrorProvider.SetError(this.btnEmpresa, "Obrigatório informar a empresa onde o usuário está relacionado.");
                    retorno = false;
                }

                /* somente para usuários internos */
                /* checando para saber se o usuário foi selecionado como médico do trabalho e n
                 * não foi selecionado um médico do sistema para ele */
                if (Convert.ToBoolean(((SelectItem)cbExterno.SelectedItem).Valor) == false && funcaoInterna != null && funcaoInterna.Id == 4)
                    if (coordenador == null)
                    {
                        retorno = false;
                        throw new MedicoException(MedicoException.msg3);
                    }

                /* somente para usuarios externos */
                if (Convert.ToBoolean(((SelectItem)cbExterno.SelectedItem).Valor) == true && dgvCliente.RowCount == 0)
                {
                    retorno = false;
                    throw new Exception("Usuario externo é obrigatório a seleção de pelo menos um cliente.");
                    
                }

                if (string.Equals(((SelectItem)cbDocumentacao.SelectedItem).Valor, "true"))
                {
                    if (cbEmissorCertificado.SelectedIndex == 0)
                    {
                        this.incluirUsuarioErrorProvider.SetError(cbEmissorCertificado, "Você deve selecionar o emissor do certificado em caso de usuario que elabora documentação");
                        retorno = false;
                    }
                }
                
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

            return retorno;
        }

        protected virtual void montaPerfil()
        {
            try
            {
                PermissionamentoFacade permissionamentoFacade = PermissionamentoFacade.getInstance();
                
                dgvPerfil.Rows.Clear();

                dgvPerfil.ColumnCount = 2;

                dgvPerfil.Columns[0].Name = "ID";
                dgvPerfil.Columns[0].Visible = false;

                dgvPerfil.Columns[1].Name = "Descrição";

                foreach (Perfil perfil in this.perfisSelecionados)
                    this.dgvPerfil.Rows.Add(perfil.Id, perfil.Descricao);

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        protected void cbDocumentacao_SelectedIndexChanged(object sender, EventArgs e)
        {
            textMte.Enabled = Convert.ToBoolean(((SelectItem)cbDocumentacao.SelectedItem).Valor) == true ? true : false;
            btnAssinatura.Enabled = Convert.ToBoolean(((SelectItem)cbDocumentacao.SelectedItem).Valor) == true ? true : false;
            
        }

        protected void frmUsuarioIncluir_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter || e.KeyCode == Keys.Return)
                SendKeys.Send("{TAB}");

        }

        protected virtual void btIncluirCliente_Click(object sender, EventArgs e)
        {
            try
            {
                frmClienteSelecionar selecionarCliente = new frmClienteSelecionar(null, null, null, null, null, null, null, null, true);
                selecionarCliente.ShowDialog();

                if (selecionarCliente.Cliente != null)
                {
                    /* verificando se o cliente já foi inserido na coleção */
                    UsuarioCliente usuarioClienteProcuraLista = usuarioClientes.Find(x => x.Cliente.Id == selecionarCliente.Cliente.Id);
                    if (usuarioClienteProcuraLista != null)
                        throw new Exception("Cliente já inserido.");

                    usuarioClientes.Add(new UsuarioCliente(null, selecionarCliente.Cliente, null, null));
                    montaGridCliente();
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }

        protected virtual void montaGridCliente()
        {
            try
            {
                dgvCliente.Columns.Clear();
                dgvCliente.ColumnCount = 3;

                dgvCliente.Columns[0].HeaderText = "id_cliente";
                dgvCliente.Columns[0].Visible = false;

                dgvCliente.Columns[1].HeaderText = "Cliente";
                dgvCliente.Columns[1].AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCells;
                dgvCliente.Columns[2].HeaderText = "CNPJ";

                usuarioClientes.ForEach(delegate(UsuarioCliente usuarioCliente)
                {
                    dgvCliente.Rows.Add(usuarioCliente.Cliente.Id, usuarioCliente.Cliente.RazaoSocial, usuarioCliente.Cliente.Cnpj);
                });

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        protected virtual void btExcluirCliente_Click(object sender, EventArgs e)
        {
            try
            {
                if (dgvCliente.CurrentRow == null)
                    throw new Exception("Selecione uma linha");

                UsuarioCliente usuarioClienteRemove = usuarioClientes.Find(x => x.Cliente.Id == (long)dgvCliente.CurrentRow.Cells[0].Value);
                usuarioClientes.Remove(usuarioClienteRemove);
                montaGridCliente();

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        protected void cbEmissorCertificado_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (string.Equals(((SelectItem)cbEmissorCertificado.SelectedItem).Valor, "9"))
            {
                textOrgaoEmissor.Text = string.Empty;
                textOrgaoEmissor.Enabled = true;

            }
            else
            {
                textOrgaoEmissor.Text = string.Empty;
                textOrgaoEmissor.Enabled = false;
                if (string.Equals(((SelectItem)cbEmissorCertificado.SelectedItem).Valor, "1"))
                {
                    textOrgaoEmissor.Text = "CRM";
                }
                else
                {
                    textOrgaoEmissor.Text = "CREA";
                }

            }
        }

    }
}
