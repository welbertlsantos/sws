﻿using SWS.Entidade;
using SWS.View.ViewHelper;
using SWS.ViewHelper;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace SWS.View
{
    public partial class frmClienteFuncaoFuncionarioIncluir : frmTemplateConsulta
    {
        protected ClienteFuncaoFuncionario clienteFuncaoFuncionario;
        
        public ClienteFuncaoFuncionario ClienteFuncaoFuncionario
        {
            get { return clienteFuncaoFuncionario; }
            set { clienteFuncaoFuncionario = value; }
        }

        private bool vip = false;

        public bool Vip
        {
            get { return vip; }
            set { vip = value; }
        }

        protected ClienteFuncao clienteFuncao = null;

        public frmClienteFuncaoFuncionarioIncluir(ClienteFuncao clienteFuncao)
        {
            InitializeComponent();
            this.clienteFuncao = clienteFuncao;

            /* preenchendo dado do cliente na tela */
            if (!clienteFuncao.Cliente.Fisica)
            {
                lblRazaoSocial.Text = "Razão Social";
                lblFantasia.Text = "Nome de Fantasia";
                lblCnpj.Text = "CNPJ";
                textCnpj.Mask = "99,999,999/9999-99";
                textRazaoSocial.Text = clienteFuncao.Cliente.RazaoSocial;
                textFantasia.Text = clienteFuncao.Cliente.Fantasia;
                textCnpj.Text = clienteFuncao.Cliente.Cnpj;
            }
            else
            {
                lblRazaoSocial.Text = "Nome";
                lblFantasia.Text = "Apelido";
                lblCnpj.Text = "CPF";
                textCnpj.Mask = "999,999,999-99";
                textRazaoSocial.Text = clienteFuncao.Cliente.RazaoSocial;
                textFantasia.Text = clienteFuncao.Cliente.Fantasia;
                textCnpj.Text = clienteFuncao.Cliente.Cnpj;
            }
            
            ComboHelper.brPdh(cbBrPdh);
            ComboHelper.Boolean(cbVinculo, true);
            ComboHelper.Boolean(cbEstagiario, false);
            ComboHelper.Boolean(cbVip, false);

            ActiveControl = textRegime;

        }

        protected virtual void btConfirmar_Click(object sender, EventArgs e)
        {
            try
            {
                if (!validaTela())
                    throw new Exception("Veja os dados digitados");

                /* gravando o cliente Funcao funcionaário */

                ClienteFuncaoFuncionario = new ClienteFuncaoFuncionario(null, clienteFuncao, null, "A", Convert.ToDateTime(dataAdmissao.Text), null, textMatricula.Text, ((SelectItem)cbVip.SelectedItem).Valor == "true" ? true : false, ((SelectItem)cbBrPdh.SelectedItem).Valor.ToString(), textRegime.Text, ((SelectItem)cbEstagiario.SelectedItem).Valor == "true" ? true : false, ((SelectItem)cbVinculo.SelectedItem).Valor == "true" ? true : false);

                this.Close();

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        protected void btFechar_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        protected bool validaTela()
        {
            bool result = true;
            try
            {
                if (!dataAdmissao.Checked)
                    throw new Exception("A data de admissão é obrigatória");
                    
                /* validando para saber se o campo matrícula é obrigatório */
                if (Convert.ToBoolean(((SelectItem)cbVinculo.SelectedItem).Valor) == true && string.IsNullOrEmpty(textMatricula.Text))
                    throw new Exception("Funcionário marcado como vínculo de trabalho com a empresa. Nesse caso a matrícula é obrigatória e deverá ser informada a mesma matrícula que ele foi registrado.");

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
                result = false;
            }

            return result;
        }

        private void cbVip_SelectedIndexChanged(object sender, EventArgs e)
        {
            Vip = Convert.ToBoolean(((SelectItem)cbVip.SelectedItem).Valor);
        }
    }
}
