﻿using SWS.Entidade;
using SWS.Facade;
using SWS.Helper;
using SWS.View.Resources;
using SWS.View.ViewHelper;
using SWS.ViewHelper;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace SWS.View
{
    public partial class frmUsuarioAlterar : frmUsuarioIncluir
    {
        HashSet<UsuarioPerfil> perfisUsuario = new HashSet<UsuarioPerfil>();
        
        public frmUsuarioAlterar(Usuario usuario) :base()
        {
            InitializeComponent();
            this.usuario = usuario;

            textNome.Text = usuario.Nome;
            textCpf.Text = usuario.Cpf;
            textIdentidade.Text = usuario.Rg;
            textEmail.Text = usuario.Email;
            textLogin.Text = usuario.Login.ToLower();
            textLogin.ReadOnly = true;
            textNumeroRegistro.Text = usuario.DocumentoExtra;
            empresa = usuario.Empresa;
            if (empresa != null)
                textEmpresa.Text = usuario.Empresa.RazaoSocial;
            
            coordenador = usuario.Medico;
            textCoordenador.Text = usuario.Medico == null ? String.Empty : usuario.Medico.Nome;
            dataAdmissao.Checked = true;
            dataAdmissao.Text = usuario.DataAdmissao.ToString();
            funcaoInterna = usuario.FuncaoInterna;
            if (funcaoInterna != null)
                textFuncaoInterna.Text = usuario.FuncaoInterna.Descricao;
            cbDocumentacao.SelectedValue = usuario.ElaboraDocumento == true ? "true" : "false";
            cbExterno.SelectedValue = usuario.Externo == true ? "true" : "false";
            textMte.Text = usuario.Mte;
            cbEmissorCertificado.SelectedValue = string.Equals(usuario.OrgaoEmissor, "CRM") ? "1" : string.Equals(usuario.OrgaoEmissor, "CREA") ? "4" : "9";
            textNumeroRegistro.Text = usuario.DocumentoExtra;
            textOrgaoEmissor.Text = usuario.OrgaoEmissor;
            cbUfEmissor.SelectedValue = usuario.UfOrgaoEmissor;


            /* preenchendo informações do arquivo */

            if (usuario.Assinatura != null)
            {
                assinatura = new Arquivo(null, String.Empty, usuario.Mimetype, 0, usuario.Assinatura);
                pbAssinatura.Image = FileHelper.byteArrayToImage(usuario.Assinatura);
            }

            montaPerfil();

            /* desabilitando controles */
            btnEmpresa.Enabled = false;
            btnMedico.Enabled = false;
            btnFuncaoInterna.Enabled = false;

            montaGridCliente();

        }

        protected override void bt_gravar_Click(object sender, EventArgs e)
        {
            this.incluirUsuarioErrorProvider.Clear();

            try
            {
                if (validaCamposObrigatorios())
                {
                    /* validando o CPF do usuário */
                    if (!ValidaCampoHelper.ValidaCPF(textCpf.Text))
                        throw new Exception("CPF é inválido.");

                    UsuarioFacade usuarioFacade = UsuarioFacade.getInstance();

                    usuario = new Usuario(usuario.Id, textNome.Text.Trim(), textCpf.Text, textIdentidade.Text.Trim(), textNumeroRegistro.Text.Trim(), Convert.ToDateTime(dataAdmissao.Value), ApplicationConstants.ATIVO, textLogin.Text.Trim(), usuario.Senha, textEmail.Text.Trim(), coordenador, empresa, textMte.Text.Trim(), Convert.ToBoolean(((SelectItem)this.cbDocumentacao.SelectedItem).Valor), assinatura == null ? null : assinatura.Conteudo, assinatura == null ? null : assinatura.Mimetype, Convert.ToBoolean(((SelectItem)cbExterno.SelectedItem).Valor), textOrgaoEmissor.Text, ((SelectItem)cbUfEmissor.SelectedItem).Valor);
                    
                    usuarioFacade.alterarUsuario(usuario, false);

                    MessageBox.Show("Usuário alterado com sucesso", "Confirmação", MessageBoxButtons.OK, MessageBoxIcon.Information);

                    this.Close();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        protected override void btn_incluir_perfil_Click(object sender, EventArgs e)
        {
            try
            {
                frmPerfilSelecionar incluirPerfil = new frmPerfilSelecionar();
                incluirPerfil.ShowDialog();

                PermissionamentoFacade permissionamentoFacade = PermissionamentoFacade.getInstance();

                if (incluirPerfil.Perfil.Count > 0)
                {
                    HashSet<Perfil> perfis = incluirPerfil.Perfil;
                    List<Perfil> perfilInsert = new List<Perfil>();
                    HashSet<UsuarioPerfil> usuarioIncluirPerfil = new HashSet<UsuarioPerfil>();

                    /* excluindo perfis já incluído */
                    foreach (Perfil perfil in perfis)
                    {
                        if (perfisUsuario.ToList().Exists(x => x.Perfil.Id != perfil.Id))
                            perfilInsert.Add(perfil);
                    }

                    foreach (Perfil perfil in perfilInsert)
                    {
                        UsuarioPerfil usuarioPerfil = new UsuarioPerfil();
                        usuarioPerfil.Perfil = perfil;
                        usuarioPerfil.Usuario = usuario;
                        usuarioPerfil.DataInicio = DateTime.Now;
                        perfisUsuario.Add(usuarioPerfil);
                        usuarioIncluirPerfil.Add(usuarioPerfil);
                    }
                    permissionamentoFacade.insertUsuarioPerfil(usuarioIncluirPerfil);
                }

                montaPerfil();

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }

        protected override void btn_excluir_perfil_Click(object sender, EventArgs e)
        {
            try
            {
                if (dgvPerfil.CurrentRow == null)
                    throw new Exception("Selecione um perfil.");

                Perfil perfilExcluir = new Perfil();
                perfilExcluir.Id = (long)dgvPerfil.CurrentRow.Cells[0].Value;

                UsuarioPerfil usuarioPerfilExcluir = new UsuarioPerfil();
                usuarioPerfilExcluir.Perfil = perfilExcluir;
                usuarioPerfilExcluir.Usuario = usuario;

                PermissionamentoFacade permissionamentoFacade = PermissionamentoFacade.getInstance();

                permissionamentoFacade.deleteUsuarioPerfil(usuarioPerfilExcluir);

                montaPerfil();

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        protected override void montaPerfil()
        {
            try
            {
                PermissionamentoFacade permissionamentoFacade = PermissionamentoFacade.getInstance();

                dgvPerfil.Columns.Clear();
                dgvPerfil.ColumnCount = 4;

                dgvPerfil.Columns[0].HeaderText = "Id Perfil";
                dgvPerfil.Columns[0].Visible = false;

                dgvPerfil.Columns[1].HeaderText = "Id Usuario";
                dgvPerfil.Columns[1].Visible = false;

                dgvPerfil.Columns[2].HeaderText = "Perfil";

                dgvPerfil.Columns[3].HeaderText = "Data Início";
                dgvPerfil.Columns[3].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
                dgvPerfil.Columns[3].HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleCenter;

                DataSet ds = permissionamentoFacade.findAllPerfilByUsuario(usuario);

                foreach (DataRow row in ds.Tables[0].Rows)
                {
                    UsuarioPerfil usuarioPerfil = new UsuarioPerfil();
                    usuarioPerfil.Usuario = usuario;
                    Perfil perfil = new Perfil();
                    perfil.Id = (long)row["ID_PERFIL"];
                    perfil.Descricao = row["DESCRICAO"].ToString();
                    perfil.Situacao = ApplicationConstants.ATIVO;
                    usuarioPerfil.Perfil = perfil;
                    usuarioPerfil.DataInicio = Convert.ToDateTime(row["DATA_INICIO"]);

                    dgvPerfil.Rows.Add(Convert.ToInt64(row["id_perfil"]), Convert.ToInt64(row["id_usuario"]), row["descricao"].ToString(), Convert.ToDateTime(row["data_inicio"]).ToShortDateString());
                    perfisUsuario.Add(usuarioPerfil);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        protected override void montaGridCliente()
        {
            try
            {
                dgvCliente.Columns.Clear();
                dgvCliente.ColumnCount = 4;

                dgvCliente.Columns[0].HeaderText = "id";
                dgvCliente.Columns[0].Visible = false;

                dgvCliente.Columns[1].HeaderText = "id_cliente";
                dgvCliente.Columns[1].Visible = false;

                dgvCliente.Columns[2].HeaderText = "Cliente";
                dgvCliente.Columns[2].AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCells;
                
                dgvCliente.Columns[3].HeaderText = "CNPJ";

                usuario.UsuarioCliente.ForEach(delegate(UsuarioCliente usuarioCliente)
                {
                    dgvCliente.Rows.Add(usuarioCliente.Id, usuarioCliente.Cliente.Id, usuarioCliente.Cliente.RazaoSocial, usuarioCliente.Cliente.Cnpj);
                });

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        protected override void btIncluirCliente_Click(object sender, EventArgs e)
        {
            try
            {
                frmClienteSelecionar selecionarCliente = new frmClienteSelecionar(null, null, null, null, null, null, null, null, true);
                selecionarCliente.ShowDialog();

                if (selecionarCliente.Cliente != null)
                {
                    /* verificando se o cliente não está incluído na coleção */
                    
                    UsuarioCliente usuarioClienteProcuraLista = usuario.UsuarioCliente.Find(x => x.Cliente.Id == selecionarCliente.Cliente.Id);
                    if (usuarioClienteProcuraLista != null)
                        throw new Exception("Cliente já inserido.");
                    
                    /* incluindo o usuario cliente no relacionamento */
                    UsuarioFacade usuarioFacade = UsuarioFacade.getInstance();
                    usuario.UsuarioCliente.Add(usuarioFacade.insertUsuarioCliente(new UsuarioCliente(null, selecionarCliente.Cliente, usuario, null)));
                    montaGridCliente();
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }


        protected override void btExcluirCliente_Click(object sender, EventArgs e)
        {
            try
            {
                if (dgvCliente.CurrentRow == null)
                    throw new Exception("Selecione uma linha");

                UsuarioFacade usuarioFacade = UsuarioFacade.getInstance();
                UsuarioCliente usuarioClienteRemovido;

                if (MessageBox.Show("Deseja excluir o cliente selecionado?", "Atenção", MessageBoxButtons.YesNo, MessageBoxIcon.Question ) == DialogResult.Yes)
                {
                    usuarioClienteRemovido = usuario.UsuarioCliente.Find(x => x.Id == (long)dgvCliente.CurrentRow.Cells[0].Value);
                    usuarioFacade.deleteUsuarioCliente(usuarioClienteRemovido);
                    usuario.UsuarioCliente.Remove(usuarioClienteRemovido);
                    montaGridCliente();
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
    }
}
