﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using SWS.Facade;
using SWS.Entidade;

namespace SWS.View
{
    public partial class frm_PpraElaborador : BaseFormConsulta
    {
        private static String msg1 = "Selecione uma linha.";

        Usuario elaborador = null;

        public Usuario getElaborador()
        {
            return this.elaborador;
        }

        public frm_PpraElaborador()
        {
            InitializeComponent();
            MontaDataGrid();
        }

        private void btn_fechar_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btn_gravar_Click(object sender, EventArgs e)
        {
            try
            {
                UsuarioFacade usuarioFacade = UsuarioFacade.getInstance();

                if (dgv_elaborador.CurrentRow != null)
                {
                    Int64 id = (Int64)this.dgv_elaborador.CurrentRow.Cells[0].Value;

                    elaborador = usuarioFacade.findUsuarioById(id);
                    
                    this.Close();
                }
                else
                {
                    throw new Exception(msg1);
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        public void MontaDataGrid()
        {
            try
            {
                UsuarioFacade usuarioFacade = UsuarioFacade.getInstance();

                this.dgv_elaborador.Columns.Clear();

                this.dgv_elaborador.ColumnCount = 9;

                this.dgv_elaborador.Columns[0].HeaderText = "idUsuario";
                this.dgv_elaborador.Columns[1].HeaderText = "Nome";
                this.dgv_elaborador.Columns[2].HeaderText = "CPF";
                this.dgv_elaborador.Columns[3].HeaderText = "RG";
                this.dgv_elaborador.Columns[4].HeaderText = "Documento Extra";
                this.dgv_elaborador.Columns[5].HeaderText = "Data_de_admissão";
                this.dgv_elaborador.Columns[6].HeaderText = "Situação";
                this.dgv_elaborador.Columns[7].HeaderText = "Login";
                this.dgv_elaborador.Columns[8].HeaderText = "Id_médico";

                this.dgv_elaborador.Columns[0].Visible = false;
                this.dgv_elaborador.Columns[2].Visible = false;
                this.dgv_elaborador.Columns[3].Visible = false;
                this.dgv_elaborador.Columns[4].Visible = false;
                this.dgv_elaborador.Columns[5].Visible = false;
                this.dgv_elaborador.Columns[6].Visible = false;
                this.dgv_elaborador.Columns[7].Visible = false;
                this.dgv_elaborador.Columns[8].Visible = false;

                foreach (Usuario usuario in usuarioFacade.findUsuarioByFuncaoInterna(1, 2))
                {
                    if (usuario.Medico != null)
                    {
                        this.dgv_elaborador.Rows.Add(usuario.Id, usuario.Nome, usuario.Cpf,
                            usuario.Rg, usuario.DocumentoExtra, usuario.DataAdmissao,
                            usuario.Situacao, usuario.Login, usuario.Medico.Id);
                    }
                    else
                    {
                        this.dgv_elaborador.Rows.Add(usuario.Id, usuario.Nome, usuario.Cpf,
                            usuario.Rg, usuario.DocumentoExtra, usuario.DataAdmissao,
                            usuario.Situacao, usuario.Login, null);
                    }

                }

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
    }
}
