﻿using SWS.Entidade;
using SWS.Facade;
using SWS.View.Resources;
using SWS.View.ViewHelper;
using SWS.ViewHelper;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace SWS.View
{
    public partial class frmProtocolosPrincipal : frmTemplate
    {
        private Cliente cliente;
        private long? idItem;
        private string tipoItem;
        

        public frmProtocolosPrincipal()
        {
            InitializeComponent();
            ComboHelper.situacao(cbSituacao);
            dataCancelamentoInicial.Text = Convert.ToString(DateTime.Now.AddDays(-30));
            dataGravacaoInicial.Text = Convert.ToString(DateTime.Now.AddDays(-30));
            validaPermissao();
        }

        private void btn_fechar_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        public void validaPermissao()
        {
            PermissionamentoFacade permissionamento = PermissionamentoFacade.getInstance();

            this.btn_incluir.Enabled = permissionamento.hasPermission("PROTOCOLO", "INCLUIR");
            this.btn_alterar.Enabled = permissionamento.hasPermission("PROTOCOLO", "ALTERAR");
            this.btn_excluir.Enabled = permissionamento.hasPermission("PROTOCOLO", "EXCLUIR");
            this.btn_imprimir.Enabled = permissionamento.hasPermission("PROTOCOLO", "IMPRIMIR");

        }

        private void btCliente_Click(object sender, EventArgs e)
        {
            frmClienteSelecionar formCliente = new frmClienteSelecionar(null, null, null, null, null, null, null, null, null);
            formCliente.ShowDialog();

            if (formCliente.Cliente != null)
            {
                cliente = formCliente.Cliente;
                textCliente.Text = cliente.RazaoSocial;
            }
        }

        private void btnExcluirCliente_Click(object sender, EventArgs e)
        {
            try
            {
                if (cliente == null)
                    throw new Exception("Você deve primeiro selecionar o cliente");

                cliente = null;
                textCliente.Text = string.Empty;
                    
            }
            catch(Exception ex) 
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btn_pesquisar_Click(object sender, EventArgs e)
        {
            if (validaDados())
            {
                montaDadaGrid();
                this.textCodigo.Focus();
            }
                
        }

        public void montaDadaGrid()
        {
            try
            {
                this.Cursor = Cursors.WaitCursor;

                ProtocoloFacade protocoloFacade = ProtocoloFacade.getInstance();

                DateTime? dataGravacaoInicial = this.dataGravacaoInicial.Checked ? Convert.ToDateTime(this.dataGravacaoInicial.Text + " 00:00:00") : (DateTime?)null;
                DateTime? dataGravacaoFinal = this.dataGravacaoFinal.Checked ? Convert.ToDateTime(this.dataGravacaoInicial.Text + " 23:59:59") : (DateTime?)null;

                DateTime? dataCancelamentoInicial = this.dataCancelamentoInicial.Checked ? Convert.ToDateTime(this.dataCancelamentoInicial.Text + " 00:00:00") : (DateTime?)null;
                DateTime? dataCancelamentoFinal = this.dataCancelamentoFinal.Checked ? Convert.ToDateTime(this.dataCancelamentoFinal.Text + " 23:59:59") : (DateTime?)null;

                Protocolo protocolo = new Protocolo(null, this.textCodigo.Text, String.Empty,
                    ((SelectItem)cbSituacao.SelectedItem).Valor, dataGravacaoInicial, null, dataCancelamentoInicial,
                    null, string.Empty, null, null, String.Empty, String.Empty, cliente);

                dgvProtocolo.Columns.Clear();

                DataSet ds = protocoloFacade.findByFilter(protocolo, dataGravacaoFinal, dataCancelamentoFinal, tipoItem, idItem);

                dgvProtocolo.DataSource = ds.Tables["Protocolos"].DefaultView;

                /* valores da grid */

                dgvProtocolo.Columns[0].HeaderText = "Id_protocolo";
                dgvProtocolo.Columns[0].Visible = false;

                dgvProtocolo.Columns[1].HeaderText = "Número";
                dgvProtocolo.Columns[1].DisplayIndex = 1;

                dgvProtocolo.Columns[2].HeaderText = "Documento";
                dgvProtocolo.Columns[2].DisplayIndex = 5;

                dgvProtocolo.Columns[3].HeaderText = "Situação";
                dgvProtocolo.Columns[3].DisplayIndex = 6;

                dgvProtocolo.Columns[4].HeaderText = "Data de Gravação";
                dgvProtocolo.Columns[4].DisplayIndex = 2;

                dgvProtocolo.Columns[5].HeaderText = "Usuário que gravou";
                dgvProtocolo.Columns[5].DisplayIndex = 3;

                dgvProtocolo.Columns[6].HeaderText = "Data de Cancelamento";
                dgvProtocolo.Columns[6].DisplayIndex = 8;

                dgvProtocolo.Columns[7].HeaderText = "Usuário que Cancelou";
                dgvProtocolo.Columns[7].DisplayIndex = 7;

                dgvProtocolo.Columns[8].HeaderText = "Meio de Envio";
                dgvProtocolo.Columns[8].DisplayIndex = 4;

                dgvProtocolo.Columns[9].HeaderText = "Usuario que Finalizou";
                dgvProtocolo.Columns[9].DisplayIndex = 8;

                dgvProtocolo.Columns[10].HeaderText = "Data de Finalização";
                dgvProtocolo.Columns[10].DisplayIndex = 9;

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }

        private Boolean validaDados()
        {
            Boolean retorno = true;
            try
            {
                if (this.dataGravacaoInicial.Checked && !this.dataGravacaoFinal.Checked)
                    throw new Exception("Você deve marcar a data final de gravação para validar a pesquisar");

                if (this.dataGravacaoFinal.Checked && !this.dataGravacaoInicial.Checked)
                    throw new Exception("Você deve marcar a data inicial de gravaçao para validar a pesquisa");

                if (this.dataGravacaoInicial.Checked && this.dataGravacaoFinal.Checked)
                    if (Convert.ToDateTime(this.dataGravacaoFinal.Text) < Convert.ToDateTime(this.dataGravacaoInicial.Text))
                        throw new Exception("A data de gravação inicial não pode ser maior que a data de gravação final. Refaça sua pesquisa");

                if (this.dataCancelamentoInicial.Checked && !this.dataCancelamentoFinal.Checked)
                    throw new Exception("Você deve marcar a data final de cancelamento para validar a pesquisar");

                if (this.dataCancelamentoFinal.Checked && !this.dataCancelamentoInicial.Checked)
                    throw new Exception("Você deve marcar a data inicial de cancelamento para validar a pesquisa");

                if (this.dataCancelamentoInicial.Checked && this.dataCancelamentoFinal.Checked)
                    if (Convert.ToDateTime(this.dataCancelamentoFinal.Text) < Convert.ToDateTime(this.dataCancelamentoInicial.Text))
                        throw new Exception("A data de cancelamenot inicial não pode ser maior que a data de cancelamento final. Refaça sua pesquisa");

            }
            catch (Exception ex)
            {
                retorno = false;   
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

            return retorno;
        }

        private void btnLimpar_Click(object sender, EventArgs e)
        {
            this.textCodigo.Text = string.Empty;
            ComboHelper.situacao(cbSituacao);
            this.dataGravacaoInicial.Checked = false;
            this.dataGravacaoFinal.Checked = false;
            this.dataCancelamentoInicial.Checked = false;
            this.dataCancelamentoFinal.Checked = false;
            this.dgvProtocolo.Columns.Clear();

            /* limpando objetos usados */

            this.textCliente.Text = string.Empty;
            cliente = null;
            idItem = null;
            tipoItem = string.Empty;
        }

        private void btn_incluir_Click(object sender, EventArgs e)
        {

            frmProtocoloIncluir protocoloIncluir = new frmProtocoloIncluir();
            protocoloIncluir.ShowDialog();

            if (protocoloIncluir.Protocolo != null)
            {
                textCodigo.Text = protocoloIncluir.Protocolo.Numero;
                montaDadaGrid();
            }   
        }

        private void btn_alterar_Click(object sender, EventArgs e)
        {
            try
            {
                this.Cursor = Cursors.WaitCursor;

                if (this.dgvProtocolo.CurrentRow == null)
                    throw new Exception("Você deve selecionar uma linha");

                ProtocoloFacade protocoloFacade = ProtocoloFacade.getInstance();
                Protocolo protocoloAlteracao = protocoloFacade.findById((long)dgvProtocolo.CurrentRow.Cells[0].Value);

                /* verificando a situação do protocolo. Somente será permitido a alteração caso ele esteja em construção */

                if (!String.Equals(protocoloAlteracao.Situacao, ApplicationConstants.CONSTRUCAO))
                    throw new Exception("Somente protocolos em contrução podem ser alterados.");

                /* TODO - acertar a parte do protocolo incluir */
                frmprotocoloAlterar alterarProtocolo = new frmprotocoloAlterar(protocoloAlteracao);
                alterarProtocolo.ShowDialog();

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }

        private void btnFinalizar_Click(object sender, EventArgs e)
        {
            try
            {
                this.Cursor = Cursors.WaitCursor;

                if (this.dgvProtocolo.CurrentRow == null)
                    throw new Exception("Você deve selecionar uma linha");

                ProtocoloFacade protocoloFacade = ProtocoloFacade.getInstance();
                Protocolo protocoloFinalizar = protocoloFacade.findById((long)dgvProtocolo.CurrentRow.Cells[0].Value);

                if (!String.Equals(protocoloFinalizar.Situacao, ApplicationConstants.CONSTRUCAO))
                    throw new Exception("Somente protocolos em contrução podem ser finalizados");

                DataSet ds = protocoloFacade.findAllItensByProtocolo(protocoloFinalizar);

                if (ds.Tables[0].Rows.Count == 0)
                    throw new Exception("O protocolo não tem nenhum item incluido. Reveja o protocolo para finalização");

                frmProtocoloFinalizar finalizarProtocolo = new frmProtocoloFinalizar(protocoloFinalizar);
                finalizarProtocolo.ShowDialog();

                if (finalizarProtocolo.Finalizado)
                {
                    MessageBox.Show("Protocolo Finalizado com sucesso.", "Confirmação", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    textCodigo.Text = protocoloFinalizar.Numero;
                    montaDadaGrid();

                }

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }

        private void btn_excluir_Click(object sender, EventArgs e)
        {
            try
            {
                this.Cursor = Cursors.WaitCursor;

                if (this.dgvProtocolo.CurrentRow == null)
                    throw new Exception("Você deve selecionar uma linha.");

                ProtocoloFacade protocoloFacade = ProtocoloFacade.getInstance();
                Protocolo protocoloExcluir = protocoloFacade.findById((long)dgvProtocolo.CurrentRow.Cells[0].Value);

                if (String.Equals(protocoloExcluir.Situacao, ApplicationConstants.DESATIVADO))
                    throw new Exception("Protocoló já excluído.");

                /* dataset com todos os itens do protocolo */
                DataSet ds = protocoloFacade.findAllItensByProtocolo(protocoloExcluir);

                /* setando para o protocolo informações de cancelamento */
                protocoloExcluir.DataCancelamento = DateTime.Now;
                protocoloExcluir.UsuarioCancelou = PermissionamentoFacade.usuarioAutenticado;

                /* solicitando que o usuário grave informações de cancelamento */
                /* TODO -> Acertar problemas de protocolo observação */
                frmProtoloObservacao incluirObservacao = new frmProtoloObservacao(protocoloExcluir, ApplicationConstants.DELETEPROTOCOLO);
                incluirObservacao.ShowDialog();

                /* verificando se foi selecionado alguma observação */
                if (String.IsNullOrEmpty(incluirObservacao.Protocolo.MotivoCancelamento))
                    throw new Exception("Você deve informar o motivo da exclusão.");

                protocoloExcluir = incluirObservacao.Protocolo;
                protocoloFacade.cancelaProtocolo(protocoloExcluir, ds);
                MessageBox.Show("Protocolo Cancelado com sucesso.", "Confirmação", MessageBoxButtons.OK, MessageBoxIcon.Information);
                montaDadaGrid();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }

        private void btn_imprimir_Click(object sender, EventArgs e)
        {
            try
            {
                this.Cursor = Cursors.WaitCursor;

                if (this.dgvProtocolo.CurrentRow == null)
                    throw new Exception("Você deve selecionar uma linha.");

                ProtocoloFacade protocoloFacade = ProtocoloFacade.getInstance();
                Protocolo protocoloImprimir = protocoloFacade.findById((long)dgvProtocolo.CurrentRow.Cells[0].Value);

                if (!String.Equals(protocoloImprimir.Situacao, ApplicationConstants.FECHADO))
                    throw new Exception("Somente protocolos finalizados podem ser impressos.");

                var process = new Process();
                process.StartInfo.FileName = "Relatorio\\jre7\\bin\\java";
                process.StartInfo.Arguments = " -jar Relatorio\\MiddlewareJasper.jar " + "Relatorio\\Report\\" + "PROTOCOLO.JASPER" + " ID_PROTOCOLO:" + protocoloImprimir.Id + ":Integer";
                process.StartInfo.UseShellExecute = false;
                process.StartInfo.RedirectStandardError = true;
                process.StartInfo.RedirectStandardInput = true;
                process.StartInfo.RedirectStandardOutput = true;
                process.StartInfo.CreateNoWindow = true;
                process.Start();
                process.StandardOutput.ReadToEnd();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                Cursor = Cursors.Default;
            }
        }

        private void dgvProtocolo_CellMouseDoubleClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            try
            {
                if (!String.Equals(ApplicationConstants.FECHADO, dgvProtocolo.CurrentRow.Cells[3].Value.ToString()))
                    throw new Exception("Somente é permitido ver as observações de protocolos já finalizados.");

                ProtocoloFacade protocoloFacade = ProtocoloFacade.getInstance();
 
                /* TODO -> acertar observação do protocolo */
                frmProtoloObservacao formObservacao = new frmProtoloObservacao(protocoloFacade.findById((long)dgvProtocolo.CurrentRow.Cells[0].Value), ApplicationConstants.FINALIZAPROTOCOLO);
                formObservacao.ShowDialog();

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            
        }

        private void dgvProtocolo_RowPrePaint(object sender, DataGridViewRowPrePaintEventArgs e)
        {
            DataGridView dgv = sender as DataGridView;

            String valor = dgv.Rows[e.RowIndex].Cells[3].Value.ToString();

            if (String.Equals(valor, ApplicationConstants.FECHADO))
                dgv.Rows[e.RowIndex].DefaultCellStyle.ForeColor = Color.DarkGreen;
            else if (String.Equals(valor, ApplicationConstants.DESATIVADO))
                dgv.Rows[e.RowIndex].DefaultCellStyle.ForeColor = Color.Red;
        }
    }
}
