﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Drawing;

namespace SWS.View
{
    public partial class BaseForm : Form
    {

        protected System.Windows.Forms.Panel panel_banner;
        protected System.Windows.Forms.PictureBox pictureBox_banner;
        protected System.Windows.Forms.Panel panel;
        protected Boolean remontarDataGrid = false;
        
        public BaseForm() 
        {

            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(BaseForm));
            this.panel = new System.Windows.Forms.Panel();
            this.panel_banner = new System.Windows.Forms.Panel();
            this.pictureBox_banner = new System.Windows.Forms.PictureBox();
            this.panel_banner.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox_banner)).BeginInit();
            this.SuspendLayout();
            // 
            // panel
            // 
            this.panel.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.panel.BackColor = System.Drawing.Color.White;
            this.panel.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel.Location = new System.Drawing.Point(0, 43);
            this.panel.Name = "panel";
            this.panel.Size = new System.Drawing.Size(790, 520);
            this.panel.TabIndex = 1;
            // 
            // panel_banner
            // 
            this.panel_banner.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.panel_banner.BackColor = System.Drawing.SystemColors.Control;
            this.panel_banner.BackgroundImage = global::SWS.Properties.Resources.banner2;
            this.panel_banner.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.panel_banner.Controls.Add(this.pictureBox_banner);
            this.panel_banner.Location = new System.Drawing.Point(0, 0);
            this.panel_banner.Name = "panel_banner";
            this.panel_banner.Size = new System.Drawing.Size(790, 42);
            this.panel_banner.TabIndex = 0;
            // 
            // pictureBox_banner
            // 
            this.pictureBox_banner.BackColor = System.Drawing.Color.Transparent;
            this.pictureBox_banner.Image = global::SWS.Properties.Resources.logo;
            this.pictureBox_banner.Location = new System.Drawing.Point(0, 0);
            this.pictureBox_banner.Name = "pictureBox_banner";
            this.pictureBox_banner.Size = new System.Drawing.Size(790, 42);
            this.pictureBox_banner.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.pictureBox_banner.TabIndex = 1;
            this.pictureBox_banner.TabStop = false;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(826, 600);
            this.MaximizeBox = false;
            this.Controls.Add(this.panel);
            this.Controls.Add(this.panel_banner);
            //this.Name = "Form1";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            //this.Text = "form1";
            this.panel_banner.ResumeLayout(false);
            this.panel_banner.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox_banner)).EndInit();
            this.ResumeLayout(false);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(SWS.Properties.Resources.icone));
            
        }   

        private void BaseForm_Load(object sender, EventArgs e)
        {
            
        }

        private void InitializeComponent()
        {
            
        }

        public Boolean getRemontarDataGrid()
        {
            return remontarDataGrid;
        }
    }
}
