﻿namespace SWS.View
{
    partial class frm_PcmsoSemPpraIncluir
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle8 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle9 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle10 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle11 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle12 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle13 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle14 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frm_PcmsoSemPpraIncluir));
            this.tb_dados = new System.Windows.Forms.TabControl();
            this.tb_dadosPPRA = new System.Windows.Forms.TabPage();
            this.tpConfiguracaoExtra = new System.Windows.Forms.TabControl();
            this.tbDados = new System.Windows.Forms.TabPage();
            this.cb_uf = new System.Windows.Forms.ComboBox();
            this.lblUf = new System.Windows.Forms.Label();
            this.lbl_endereco = new System.Windows.Forms.Label();
            this.text_endereco = new System.Windows.Forms.TextBox();
            this.text_cep = new System.Windows.Forms.MaskedTextBox();
            this.lbl_cep = new System.Windows.Forms.Label();
            this.lbl_bairro = new System.Windows.Forms.Label();
            this.text_bairro = new System.Windows.Forms.TextBox();
            this.text_complemento = new System.Windows.Forms.TextBox();
            this.text_localObra = new System.Windows.Forms.TextBox();
            this.lbl_complemento = new System.Windows.Forms.Label();
            this.lbl_obra = new System.Windows.Forms.Label();
            this.lbl_localObra = new System.Windows.Forms.Label();
            this.text_obra = new System.Windows.Forms.TextBox();
            this.text_cidade = new System.Windows.Forms.TextBox();
            this.lbl_numero = new System.Windows.Forms.Label();
            this.lbl_cidade = new System.Windows.Forms.Label();
            this.text_numero = new System.Windows.Forms.TextBox();
            this.tbPrevisao = new System.Windows.Forms.TabPage();
            this.btnExcluirPrevisao = new System.Windows.Forms.Button();
            this.btnIncluirPrevisao = new System.Windows.Forms.Button();
            this.dgPrevisao = new System.Windows.Forms.DataGridView();
            this.grb_dados = new System.Windows.Forms.GroupBox();
            this.grb_contratante_contrato = new System.Windows.Forms.GroupBox();
            this.text_fimContr = new System.Windows.Forms.TextBox();
            this.lbl_inicioContr = new System.Windows.Forms.Label();
            this.text_inicioContr = new System.Windows.Forms.TextBox();
            this.lbl_contrato = new System.Windows.Forms.Label();
            this.text_numContrato = new System.Windows.Forms.TextBox();
            this.lbl_fimContrato = new System.Windows.Forms.Label();
            this.text_clienteContratante = new System.Windows.Forms.TextBox();
            this.btn_contratante = new System.Windows.Forms.Button();
            this.grb_cliente = new System.Windows.Forms.GroupBox();
            this.text_cliente = new System.Windows.Forms.TextBox();
            this.lbl_cliente = new System.Windows.Forms.Label();
            this.btn_cliente = new System.Windows.Forms.Button();
            this.grb_tecnico = new System.Windows.Forms.GroupBox();
            this.lbl_tecno = new System.Windows.Forms.Label();
            this.btn_tecnico = new System.Windows.Forms.Button();
            this.text_tecnico = new System.Windows.Forms.TextBox();
            this.lbl_vencimento = new System.Windows.Forms.Label();
            this.grb_grauRisco = new System.Windows.Forms.GroupBox();
            this.cb_grauRisco = new System.Windows.Forms.ComboBox();
            this.grb_data = new System.Windows.Forms.GroupBox();
            this.lbl_dataVencimento = new System.Windows.Forms.Label();
            this.lbl_dataCriacao = new System.Windows.Forms.Label();
            this.dt_criacao = new System.Windows.Forms.DateTimePicker();
            this.dt_vencimento = new System.Windows.Forms.DateTimePicker();
            this.text_codPpra = new System.Windows.Forms.MaskedTextBox();
            this.lbl_codPpra = new System.Windows.Forms.Label();
            this.btn_salvar = new System.Windows.Forms.Button();
            this.btn_limpar = new System.Windows.Forms.Button();
            this.tb_ghe = new System.Windows.Forms.TabPage();
            this.btn_alterarGhe = new System.Windows.Forms.Button();
            this.grb_ghePPRA = new System.Windows.Forms.GroupBox();
            this.grd_ghe = new System.Windows.Forms.DataGridView();
            this.btn_incluirGHE = new System.Windows.Forms.Button();
            this.btn_excluirGHE = new System.Windows.Forms.Button();
            this.tb_gheFonte = new System.Windows.Forms.TabPage();
            this.tb_fonteGHE = new System.Windows.Forms.TabControl();
            this.Fonte = new System.Windows.Forms.TabPage();
            this.grb_gheFonte = new System.Windows.Forms.GroupBox();
            this.cb_ghe = new System.Windows.Forms.ComboBox();
            this.btn_excluirFonte = new System.Windows.Forms.Button();
            this.grb_fonte = new System.Windows.Forms.GroupBox();
            this.grd_fonte = new System.Windows.Forms.DataGridView();
            this.btn_incluirFonte = new System.Windows.Forms.Button();
            this.agente = new System.Windows.Forms.TabPage();
            this.btn_excluirAgente = new System.Windows.Forms.Button();
            this.grb_gheAgente = new System.Windows.Forms.GroupBox();
            this.cb_gheAgente = new System.Windows.Forms.ComboBox();
            this.btn_incluirAgente = new System.Windows.Forms.Button();
            this.grb_fonteAgente = new System.Windows.Forms.GroupBox();
            this.cb_fonteAgente = new System.Windows.Forms.ComboBox();
            this.grb_agente = new System.Windows.Forms.GroupBox();
            this.grd_agente = new System.Windows.Forms.DataGridView();
            this.tb_setorGHE = new System.Windows.Forms.TabPage();
            this.tb_gheSetor = new System.Windows.Forms.TabControl();
            this.tb_setor = new System.Windows.Forms.TabPage();
            this.grb_setor = new System.Windows.Forms.GroupBox();
            this.grd_setor = new System.Windows.Forms.DataGridView();
            this.btn_excluirSetor = new System.Windows.Forms.Button();
            this.grb_gheSetor = new System.Windows.Forms.GroupBox();
            this.cb_gheSetor = new System.Windows.Forms.ComboBox();
            this.btn_incluirSetor = new System.Windows.Forms.Button();
            this.tb_funcao = new System.Windows.Forms.TabPage();
            this.btn_excluirFuncaoClientePPRA = new System.Windows.Forms.Button();
            this.btn_incluirFuncaoClientePPRA = new System.Windows.Forms.Button();
            this.grb_funcao = new System.Windows.Forms.GroupBox();
            this.grd_funcaoCliente = new System.Windows.Forms.DataGridView();
            this.tb_funcaoPPRA = new System.Windows.Forms.TabPage();
            this.btn_excluirFuncaoCliente = new System.Windows.Forms.Button();
            this.grb_funcaoCliente = new System.Windows.Forms.GroupBox();
            this.grd_setorFuncao = new System.Windows.Forms.DataGridView();
            this.btn_incluirFuncaoCliente = new System.Windows.Forms.Button();
            this.grb_setorFuncao = new System.Windows.Forms.GroupBox();
            this.cb_setorFuncao = new System.Windows.Forms.ComboBox();
            this.grb_gheFuncao = new System.Windows.Forms.GroupBox();
            this.cb_gheFuncao = new System.Windows.Forms.ComboBox();
            this.tb_cronograma = new System.Windows.Forms.TabPage();
            this.btn_excluirAtividade = new System.Windows.Forms.Button();
            this.btn_alterarAtividade = new System.Windows.Forms.Button();
            this.btn_incluirAtividade = new System.Windows.Forms.Button();
            this.grb_atividade = new System.Windows.Forms.GroupBox();
            this.grd_atividadeCronograma = new System.Windows.Forms.DataGridView();
            this.grb_comentario = new System.Windows.Forms.TabPage();
            this.btn_gravarComentario = new System.Windows.Forms.Button();
            this.text_comentario = new System.Windows.Forms.TextBox();
            this.lbl_comentario = new System.Windows.Forms.Label();
            this.tb_anexo = new System.Windows.Forms.TabPage();
            this.btn_excluirAnexo = new System.Windows.Forms.Button();
            this.lblDescricaoAnexo = new System.Windows.Forms.Label();
            this.grb_anexo = new System.Windows.Forms.GroupBox();
            this.grd_anexo = new System.Windows.Forms.DataGridView();
            this.text_conteudo = new System.Windows.Forms.TextBox();
            this.btn_incluirAnexo = new System.Windows.Forms.Button();
            this.pd_pcmsoExame = new System.Windows.Forms.TabPage();
            this.btn_exame_alterar = new System.Windows.Forms.Button();
            this.grb_periodicidade = new System.Windows.Forms.GroupBox();
            this.grd_periodicidade = new System.Windows.Forms.DataGridView();
            this.grb_exames = new System.Windows.Forms.GroupBox();
            this.grd_exame = new System.Windows.Forms.DataGridView();
            this.btn_excluir = new System.Windows.Forms.Button();
            this.grb_risco = new System.Windows.Forms.GroupBox();
            this.grd_risco = new System.Windows.Forms.DataGridView();
            this.btn_incluir = new System.Windows.Forms.Button();
            this.grb_funcoesExame = new System.Windows.Forms.GroupBox();
            this.grd_funcao = new System.Windows.Forms.DataGridView();
            this.grb_gheExame = new System.Windows.Forms.GroupBox();
            this.grd_gheExame = new System.Windows.Forms.DataGridView();
            this.tb_hospitais = new System.Windows.Forms.TabPage();
            this.btn_excluirMedico = new System.Windows.Forms.Button();
            this.btn_excluirHospital = new System.Windows.Forms.Button();
            this.btn_incluirMedico = new System.Windows.Forms.Button();
            this.grb_medico = new System.Windows.Forms.GroupBox();
            this.grd_medico = new System.Windows.Forms.DataGridView();
            this.btn_incluirHospital = new System.Windows.Forms.Button();
            this.grb_hospital = new System.Windows.Forms.GroupBox();
            this.grd_hospital = new System.Windows.Forms.DataGridView();
            this.tb_equipamento = new System.Windows.Forms.TabPage();
            this.btn_excluirMaterial = new System.Windows.Forms.Button();
            this.grb_material_salvar = new System.Windows.Forms.GroupBox();
            this.btn_salvarMaterial = new System.Windows.Forms.Button();
            this.btn_incluirMaterial = new System.Windows.Forms.Button();
            this.grb_material = new System.Windows.Forms.GroupBox();
            this.grd_material = new System.Windows.Forms.DataGridView();
            this.btn_fechar = new System.Windows.Forms.Button();
            this.grb_paginacao = new System.Windows.Forms.GroupBox();
            this.pcmsoIncluirErrorProvider = new System.Windows.Forms.ErrorProvider(this.components);
            this.panel_banner.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox_banner)).BeginInit();
            this.panel.SuspendLayout();
            this.tb_dados.SuspendLayout();
            this.tb_dadosPPRA.SuspendLayout();
            this.tpConfiguracaoExtra.SuspendLayout();
            this.tbDados.SuspendLayout();
            this.tbPrevisao.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgPrevisao)).BeginInit();
            this.grb_dados.SuspendLayout();
            this.grb_contratante_contrato.SuspendLayout();
            this.grb_cliente.SuspendLayout();
            this.grb_tecnico.SuspendLayout();
            this.grb_grauRisco.SuspendLayout();
            this.grb_data.SuspendLayout();
            this.tb_ghe.SuspendLayout();
            this.grb_ghePPRA.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grd_ghe)).BeginInit();
            this.tb_gheFonte.SuspendLayout();
            this.tb_fonteGHE.SuspendLayout();
            this.Fonte.SuspendLayout();
            this.grb_gheFonte.SuspendLayout();
            this.grb_fonte.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grd_fonte)).BeginInit();
            this.agente.SuspendLayout();
            this.grb_gheAgente.SuspendLayout();
            this.grb_fonteAgente.SuspendLayout();
            this.grb_agente.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grd_agente)).BeginInit();
            this.tb_setorGHE.SuspendLayout();
            this.tb_gheSetor.SuspendLayout();
            this.tb_setor.SuspendLayout();
            this.grb_setor.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grd_setor)).BeginInit();
            this.grb_gheSetor.SuspendLayout();
            this.tb_funcao.SuspendLayout();
            this.grb_funcao.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grd_funcaoCliente)).BeginInit();
            this.tb_funcaoPPRA.SuspendLayout();
            this.grb_funcaoCliente.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grd_setorFuncao)).BeginInit();
            this.grb_setorFuncao.SuspendLayout();
            this.grb_gheFuncao.SuspendLayout();
            this.tb_cronograma.SuspendLayout();
            this.grb_atividade.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grd_atividadeCronograma)).BeginInit();
            this.grb_comentario.SuspendLayout();
            this.tb_anexo.SuspendLayout();
            this.grb_anexo.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grd_anexo)).BeginInit();
            this.pd_pcmsoExame.SuspendLayout();
            this.grb_periodicidade.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grd_periodicidade)).BeginInit();
            this.grb_exames.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grd_exame)).BeginInit();
            this.grb_risco.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grd_risco)).BeginInit();
            this.grb_funcoesExame.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grd_funcao)).BeginInit();
            this.grb_gheExame.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grd_gheExame)).BeginInit();
            this.tb_hospitais.SuspendLayout();
            this.grb_medico.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grd_medico)).BeginInit();
            this.grb_hospital.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grd_hospital)).BeginInit();
            this.tb_equipamento.SuspendLayout();
            this.grb_material_salvar.SuspendLayout();
            this.grb_material.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grd_material)).BeginInit();
            this.grb_paginacao.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pcmsoIncluirErrorProvider)).BeginInit();
            this.SuspendLayout();
            // 
            // panel
            // 
            this.panel.Controls.Add(this.grb_paginacao);
            this.panel.Controls.Add(this.tb_dados);
            // 
            // tb_dados
            // 
            this.tb_dados.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tb_dados.Controls.Add(this.tb_dadosPPRA);
            this.tb_dados.Controls.Add(this.tb_ghe);
            this.tb_dados.Controls.Add(this.tb_gheFonte);
            this.tb_dados.Controls.Add(this.tb_setorGHE);
            this.tb_dados.Controls.Add(this.tb_cronograma);
            this.tb_dados.Controls.Add(this.grb_comentario);
            this.tb_dados.Controls.Add(this.tb_anexo);
            this.tb_dados.Controls.Add(this.pd_pcmsoExame);
            this.tb_dados.Controls.Add(this.tb_hospitais);
            this.tb_dados.Controls.Add(this.tb_equipamento);
            this.tb_dados.Location = new System.Drawing.Point(3, 3);
            this.tb_dados.Name = "tb_dados";
            this.tb_dados.SelectedIndex = 0;
            this.tb_dados.Size = new System.Drawing.Size(781, 455);
            this.tb_dados.TabIndex = 1;
            this.tb_dados.SelectedIndexChanged += new System.EventHandler(this.tb_dados_SelectedIndexChanged);
            // 
            // tb_dadosPPRA
            // 
            this.tb_dadosPPRA.BackColor = System.Drawing.Color.White;
            this.tb_dadosPPRA.Controls.Add(this.tpConfiguracaoExtra);
            this.tb_dadosPPRA.Controls.Add(this.grb_dados);
            this.tb_dadosPPRA.Controls.Add(this.btn_salvar);
            this.tb_dadosPPRA.Controls.Add(this.btn_limpar);
            this.tb_dadosPPRA.Location = new System.Drawing.Point(4, 22);
            this.tb_dadosPPRA.Name = "tb_dadosPPRA";
            this.tb_dadosPPRA.Padding = new System.Windows.Forms.Padding(3);
            this.tb_dadosPPRA.Size = new System.Drawing.Size(773, 429);
            this.tb_dadosPPRA.TabIndex = 0;
            this.tb_dadosPPRA.Text = "PCMSO";
            // 
            // tpConfiguracaoExtra
            // 
            this.tpConfiguracaoExtra.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tpConfiguracaoExtra.Controls.Add(this.tbDados);
            this.tpConfiguracaoExtra.Controls.Add(this.tbPrevisao);
            this.tpConfiguracaoExtra.Location = new System.Drawing.Point(7, 169);
            this.tpConfiguracaoExtra.Name = "tpConfiguracaoExtra";
            this.tpConfiguracaoExtra.SelectedIndex = 0;
            this.tpConfiguracaoExtra.Size = new System.Drawing.Size(764, 214);
            this.tpConfiguracaoExtra.TabIndex = 108;
            // 
            // tbDados
            // 
            this.tbDados.Controls.Add(this.cb_uf);
            this.tbDados.Controls.Add(this.lblUf);
            this.tbDados.Controls.Add(this.lbl_endereco);
            this.tbDados.Controls.Add(this.text_endereco);
            this.tbDados.Controls.Add(this.text_cep);
            this.tbDados.Controls.Add(this.lbl_cep);
            this.tbDados.Controls.Add(this.lbl_bairro);
            this.tbDados.Controls.Add(this.text_bairro);
            this.tbDados.Controls.Add(this.text_complemento);
            this.tbDados.Controls.Add(this.text_localObra);
            this.tbDados.Controls.Add(this.lbl_complemento);
            this.tbDados.Controls.Add(this.lbl_obra);
            this.tbDados.Controls.Add(this.lbl_localObra);
            this.tbDados.Controls.Add(this.text_obra);
            this.tbDados.Controls.Add(this.text_cidade);
            this.tbDados.Controls.Add(this.lbl_numero);
            this.tbDados.Controls.Add(this.lbl_cidade);
            this.tbDados.Controls.Add(this.text_numero);
            this.tbDados.Location = new System.Drawing.Point(4, 22);
            this.tbDados.Name = "tbDados";
            this.tbDados.Padding = new System.Windows.Forms.Padding(3);
            this.tbDados.Size = new System.Drawing.Size(756, 188);
            this.tbDados.TabIndex = 0;
            this.tbDados.Text = "Dados Extras";
            this.tbDados.UseVisualStyleBackColor = true;
            // 
            // cb_uf
            // 
            this.cb_uf.BackColor = System.Drawing.Color.LightGray;
            this.cb_uf.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cb_uf.FormattingEnabled = true;
            this.cb_uf.Location = new System.Drawing.Point(649, 58);
            this.cb_uf.Name = "cb_uf";
            this.cb_uf.Size = new System.Drawing.Size(100, 21);
            this.cb_uf.TabIndex = 18;
            // 
            // lblUf
            // 
            this.lblUf.AutoSize = true;
            this.lblUf.Location = new System.Drawing.Point(648, 42);
            this.lblUf.Name = "lblUf";
            this.lblUf.Size = new System.Drawing.Size(21, 13);
            this.lblUf.TabIndex = 108;
            this.lblUf.Text = "UF";
            // 
            // lbl_endereco
            // 
            this.lbl_endereco.AutoSize = true;
            this.lbl_endereco.Location = new System.Drawing.Point(6, 3);
            this.lbl_endereco.Name = "lbl_endereco";
            this.lbl_endereco.Size = new System.Drawing.Size(61, 13);
            this.lbl_endereco.TabIndex = 17;
            this.lbl_endereco.Text = "Logradouro";
            // 
            // text_endereco
            // 
            this.text_endereco.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.text_endereco.Location = new System.Drawing.Point(6, 19);
            this.text_endereco.MaxLength = 100;
            this.text_endereco.Name = "text_endereco";
            this.text_endereco.Size = new System.Drawing.Size(361, 20);
            this.text_endereco.TabIndex = 12;
            // 
            // text_cep
            // 
            this.text_cep.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.text_cep.Location = new System.Drawing.Point(573, 58);
            this.text_cep.Mask = "99,999-999";
            this.text_cep.Name = "text_cep";
            this.text_cep.PromptChar = ' ';
            this.text_cep.Size = new System.Drawing.Size(69, 20);
            this.text_cep.TabIndex = 17;
            // 
            // lbl_cep
            // 
            this.lbl_cep.AutoSize = true;
            this.lbl_cep.Location = new System.Drawing.Point(573, 42);
            this.lbl_cep.Name = "lbl_cep";
            this.lbl_cep.Size = new System.Drawing.Size(28, 13);
            this.lbl_cep.TabIndex = 107;
            this.lbl_cep.Text = "CEP";
            // 
            // lbl_bairro
            // 
            this.lbl_bairro.AutoSize = true;
            this.lbl_bairro.Location = new System.Drawing.Point(3, 42);
            this.lbl_bairro.Name = "lbl_bairro";
            this.lbl_bairro.Size = new System.Drawing.Size(34, 13);
            this.lbl_bairro.TabIndex = 26;
            this.lbl_bairro.Text = "Bairro";
            // 
            // text_bairro
            // 
            this.text_bairro.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.text_bairro.Location = new System.Drawing.Point(6, 58);
            this.text_bairro.MaxLength = 50;
            this.text_bairro.Name = "text_bairro";
            this.text_bairro.Size = new System.Drawing.Size(361, 20);
            this.text_bairro.TabIndex = 15;
            // 
            // text_complemento
            // 
            this.text_complemento.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.text_complemento.Location = new System.Drawing.Point(480, 19);
            this.text_complemento.MaxLength = 100;
            this.text_complemento.Name = "text_complemento";
            this.text_complemento.Size = new System.Drawing.Size(269, 20);
            this.text_complemento.TabIndex = 14;
            // 
            // text_localObra
            // 
            this.text_localObra.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.text_localObra.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.text_localObra.Location = new System.Drawing.Point(376, 97);
            this.text_localObra.MaxLength = 255;
            this.text_localObra.Multiline = true;
            this.text_localObra.Name = "text_localObra";
            this.text_localObra.Size = new System.Drawing.Size(373, 79);
            this.text_localObra.TabIndex = 20;
            // 
            // lbl_complemento
            // 
            this.lbl_complemento.AutoSize = true;
            this.lbl_complemento.Location = new System.Drawing.Point(477, 3);
            this.lbl_complemento.Name = "lbl_complemento";
            this.lbl_complemento.Size = new System.Drawing.Size(71, 13);
            this.lbl_complemento.TabIndex = 22;
            this.lbl_complemento.Text = "Complemento";
            // 
            // lbl_obra
            // 
            this.lbl_obra.AutoSize = true;
            this.lbl_obra.Location = new System.Drawing.Point(3, 81);
            this.lbl_obra.Name = "lbl_obra";
            this.lbl_obra.Size = new System.Drawing.Size(30, 13);
            this.lbl_obra.TabIndex = 15;
            this.lbl_obra.Text = "Obra";
            // 
            // lbl_localObra
            // 
            this.lbl_localObra.AutoSize = true;
            this.lbl_localObra.Location = new System.Drawing.Point(373, 81);
            this.lbl_localObra.Name = "lbl_localObra";
            this.lbl_localObra.Size = new System.Drawing.Size(59, 13);
            this.lbl_localObra.TabIndex = 30;
            this.lbl_localObra.Text = "Local Obra";
            // 
            // text_obra
            // 
            this.text_obra.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.text_obra.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.text_obra.Location = new System.Drawing.Point(6, 97);
            this.text_obra.MaxLength = 255;
            this.text_obra.Multiline = true;
            this.text_obra.Name = "text_obra";
            this.text_obra.Size = new System.Drawing.Size(361, 79);
            this.text_obra.TabIndex = 19;
            // 
            // text_cidade
            // 
            this.text_cidade.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.text_cidade.Location = new System.Drawing.Point(376, 58);
            this.text_cidade.MaxLength = 50;
            this.text_cidade.Name = "text_cidade";
            this.text_cidade.Size = new System.Drawing.Size(191, 20);
            this.text_cidade.TabIndex = 16;
            // 
            // lbl_numero
            // 
            this.lbl_numero.AutoSize = true;
            this.lbl_numero.Location = new System.Drawing.Point(373, 3);
            this.lbl_numero.Name = "lbl_numero";
            this.lbl_numero.Size = new System.Drawing.Size(44, 13);
            this.lbl_numero.TabIndex = 19;
            this.lbl_numero.Text = "Número";
            // 
            // lbl_cidade
            // 
            this.lbl_cidade.AutoSize = true;
            this.lbl_cidade.Location = new System.Drawing.Point(373, 42);
            this.lbl_cidade.Name = "lbl_cidade";
            this.lbl_cidade.Size = new System.Drawing.Size(40, 13);
            this.lbl_cidade.TabIndex = 28;
            this.lbl_cidade.Text = "Cidade";
            // 
            // text_numero
            // 
            this.text_numero.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.text_numero.Location = new System.Drawing.Point(376, 19);
            this.text_numero.MaxLength = 10;
            this.text_numero.Name = "text_numero";
            this.text_numero.Size = new System.Drawing.Size(93, 20);
            this.text_numero.TabIndex = 13;
            // 
            // tbPrevisao
            // 
            this.tbPrevisao.Controls.Add(this.btnExcluirPrevisao);
            this.tbPrevisao.Controls.Add(this.btnIncluirPrevisao);
            this.tbPrevisao.Controls.Add(this.dgPrevisao);
            this.tbPrevisao.Location = new System.Drawing.Point(4, 22);
            this.tbPrevisao.Name = "tbPrevisao";
            this.tbPrevisao.Padding = new System.Windows.Forms.Padding(3);
            this.tbPrevisao.Size = new System.Drawing.Size(756, 188);
            this.tbPrevisao.TabIndex = 1;
            this.tbPrevisao.Text = "Previsão";
            this.tbPrevisao.UseVisualStyleBackColor = true;
            // 
            // btnExcluirPrevisao
            // 
            this.btnExcluirPrevisao.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnExcluirPrevisao.Image = global::SWS.Properties.Resources.lixeira;
            this.btnExcluirPrevisao.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnExcluirPrevisao.Location = new System.Drawing.Point(87, 159);
            this.btnExcluirPrevisao.Name = "btnExcluirPrevisao";
            this.btnExcluirPrevisao.Size = new System.Drawing.Size(75, 23);
            this.btnExcluirPrevisao.TabIndex = 5;
            this.btnExcluirPrevisao.Text = "Excluir";
            this.btnExcluirPrevisao.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnExcluirPrevisao.UseVisualStyleBackColor = true;
            this.btnExcluirPrevisao.Click += new System.EventHandler(this.btnExcluirPrevisao_Click);
            // 
            // btnIncluirPrevisao
            // 
            this.btnIncluirPrevisao.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnIncluirPrevisao.Image = global::SWS.Properties.Resources.icone_mais;
            this.btnIncluirPrevisao.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnIncluirPrevisao.Location = new System.Drawing.Point(6, 159);
            this.btnIncluirPrevisao.Name = "btnIncluirPrevisao";
            this.btnIncluirPrevisao.Size = new System.Drawing.Size(75, 23);
            this.btnIncluirPrevisao.TabIndex = 4;
            this.btnIncluirPrevisao.Text = "Incluir";
            this.btnIncluirPrevisao.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnIncluirPrevisao.UseVisualStyleBackColor = true;
            this.btnIncluirPrevisao.Click += new System.EventHandler(this.btnIncluirPrevisao_Click);
            // 
            // dgPrevisao
            // 
            this.dgPrevisao.AllowUserToAddRows = false;
            this.dgPrevisao.AllowUserToDeleteRows = false;
            this.dgPrevisao.AllowUserToOrderColumns = true;
            this.dgPrevisao.AllowUserToResizeRows = false;
            this.dgPrevisao.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.DisplayedCells;
            this.dgPrevisao.BackgroundColor = System.Drawing.Color.White;
            this.dgPrevisao.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dataGridViewCellStyle8.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle8.BackColor = System.Drawing.Color.LightGray;
            dataGridViewCellStyle8.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle8.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle8.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle8.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle8.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgPrevisao.DefaultCellStyle = dataGridViewCellStyle8;
            this.dgPrevisao.Location = new System.Drawing.Point(6, 6);
            this.dgPrevisao.Name = "dgPrevisao";
            this.dgPrevisao.RowHeadersWidth = 30;
            this.dgPrevisao.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.CellSelect;
            this.dgPrevisao.Size = new System.Drawing.Size(366, 147);
            this.dgPrevisao.TabIndex = 3;
            this.dgPrevisao.CellEndEdit += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgPrevisao_CellEndEdit);
            this.dgPrevisao.DataError += new System.Windows.Forms.DataGridViewDataErrorEventHandler(this.dgPrevisao_DataError);
            this.dgPrevisao.EditingControlShowing += new System.Windows.Forms.DataGridViewEditingControlShowingEventHandler(this.dgPrevisao_EditingControlShowing);
            this.dgPrevisao.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.dgPrevisao_KeyPress);
            // 
            // grb_dados
            // 
            this.grb_dados.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.grb_dados.Controls.Add(this.grb_contratante_contrato);
            this.grb_dados.Controls.Add(this.grb_cliente);
            this.grb_dados.Controls.Add(this.grb_tecnico);
            this.grb_dados.Controls.Add(this.lbl_vencimento);
            this.grb_dados.Controls.Add(this.grb_grauRisco);
            this.grb_dados.Controls.Add(this.grb_data);
            this.grb_dados.Controls.Add(this.text_codPpra);
            this.grb_dados.Controls.Add(this.lbl_codPpra);
            this.grb_dados.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.grb_dados.Location = new System.Drawing.Point(3, 6);
            this.grb_dados.Name = "grb_dados";
            this.grb_dados.Size = new System.Drawing.Size(764, 157);
            this.grb_dados.TabIndex = 0;
            this.grb_dados.TabStop = false;
            this.grb_dados.Text = "Dados";
            // 
            // grb_contratante_contrato
            // 
            this.grb_contratante_contrato.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.grb_contratante_contrato.Controls.Add(this.text_fimContr);
            this.grb_contratante_contrato.Controls.Add(this.lbl_inicioContr);
            this.grb_contratante_contrato.Controls.Add(this.text_inicioContr);
            this.grb_contratante_contrato.Controls.Add(this.lbl_contrato);
            this.grb_contratante_contrato.Controls.Add(this.text_numContrato);
            this.grb_contratante_contrato.Controls.Add(this.lbl_fimContrato);
            this.grb_contratante_contrato.Controls.Add(this.text_clienteContratante);
            this.grb_contratante_contrato.Controls.Add(this.btn_contratante);
            this.grb_contratante_contrato.Enabled = false;
            this.grb_contratante_contrato.Location = new System.Drawing.Point(6, 68);
            this.grb_contratante_contrato.Name = "grb_contratante_contrato";
            this.grb_contratante_contrato.Size = new System.Drawing.Size(403, 80);
            this.grb_contratante_contrato.TabIndex = 3;
            this.grb_contratante_contrato.TabStop = false;
            this.grb_contratante_contrato.Text = "Cliente Contratante";
            // 
            // text_fimContr
            // 
            this.text_fimContr.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.text_fimContr.Enabled = false;
            this.text_fimContr.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.text_fimContr.Location = new System.Drawing.Point(121, 54);
            this.text_fimContr.MaxLength = 15;
            this.text_fimContr.Name = "text_fimContr";
            this.text_fimContr.Size = new System.Drawing.Size(104, 20);
            this.text_fimContr.TabIndex = 6;
            // 
            // lbl_inicioContr
            // 
            this.lbl_inicioContr.AutoSize = true;
            this.lbl_inicioContr.Location = new System.Drawing.Point(7, 40);
            this.lbl_inicioContr.Name = "lbl_inicioContr";
            this.lbl_inicioContr.Size = new System.Drawing.Size(77, 13);
            this.lbl_inicioContr.TabIndex = 29;
            this.lbl_inicioContr.Text = "Início Contrato";
            // 
            // text_inicioContr
            // 
            this.text_inicioContr.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.text_inicioContr.Enabled = false;
            this.text_inicioContr.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.text_inicioContr.Location = new System.Drawing.Point(7, 54);
            this.text_inicioContr.MaxLength = 15;
            this.text_inicioContr.Name = "text_inicioContr";
            this.text_inicioContr.Size = new System.Drawing.Size(104, 20);
            this.text_inicioContr.TabIndex = 5;
            // 
            // lbl_contrato
            // 
            this.lbl_contrato.AutoSize = true;
            this.lbl_contrato.Location = new System.Drawing.Point(238, 38);
            this.lbl_contrato.Name = "lbl_contrato";
            this.lbl_contrato.Size = new System.Drawing.Size(102, 13);
            this.lbl_contrato.TabIndex = 27;
            this.lbl_contrato.Text = "Número do Contrato";
            // 
            // text_numContrato
            // 
            this.text_numContrato.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.text_numContrato.Enabled = false;
            this.text_numContrato.Location = new System.Drawing.Point(239, 54);
            this.text_numContrato.MaxLength = 15;
            this.text_numContrato.Name = "text_numContrato";
            this.text_numContrato.Size = new System.Drawing.Size(156, 20);
            this.text_numContrato.TabIndex = 7;
            // 
            // lbl_fimContrato
            // 
            this.lbl_fimContrato.AutoSize = true;
            this.lbl_fimContrato.Location = new System.Drawing.Point(121, 40);
            this.lbl_fimContrato.Name = "lbl_fimContrato";
            this.lbl_fimContrato.Size = new System.Drawing.Size(66, 13);
            this.lbl_fimContrato.TabIndex = 25;
            this.lbl_fimContrato.Text = "Fim Contrato";
            // 
            // text_clienteContratante
            // 
            this.text_clienteContratante.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.text_clienteContratante.BackColor = System.Drawing.Color.LightGray;
            this.text_clienteContratante.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.text_clienteContratante.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.text_clienteContratante.ForeColor = System.Drawing.Color.Blue;
            this.text_clienteContratante.Location = new System.Drawing.Point(7, 14);
            this.text_clienteContratante.MaxLength = 100;
            this.text_clienteContratante.Name = "text_clienteContratante";
            this.text_clienteContratante.ReadOnly = true;
            this.text_clienteContratante.Size = new System.Drawing.Size(333, 20);
            this.text_clienteContratante.TabIndex = 3;
            this.text_clienteContratante.TabStop = false;
            // 
            // btn_contratante
            // 
            this.btn_contratante.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btn_contratante.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_contratante.Location = new System.Drawing.Point(346, 11);
            this.btn_contratante.Name = "btn_contratante";
            this.btn_contratante.Size = new System.Drawing.Size(49, 23);
            this.btn_contratante.TabIndex = 4;
            this.btn_contratante.Text = "&Contr.";
            this.btn_contratante.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btn_contratante.UseVisualStyleBackColor = true;
            this.btn_contratante.Click += new System.EventHandler(this.btn_contratante_Click);
            // 
            // grb_cliente
            // 
            this.grb_cliente.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.grb_cliente.Controls.Add(this.text_cliente);
            this.grb_cliente.Controls.Add(this.lbl_cliente);
            this.grb_cliente.Controls.Add(this.btn_cliente);
            this.grb_cliente.Location = new System.Drawing.Point(134, 10);
            this.grb_cliente.Name = "grb_cliente";
            this.grb_cliente.Size = new System.Drawing.Size(275, 52);
            this.grb_cliente.TabIndex = 1;
            this.grb_cliente.TabStop = false;
            // 
            // text_cliente
            // 
            this.text_cliente.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.text_cliente.BackColor = System.Drawing.Color.LightGray;
            this.text_cliente.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.text_cliente.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.text_cliente.ForeColor = System.Drawing.Color.Blue;
            this.text_cliente.Location = new System.Drawing.Point(6, 15);
            this.text_cliente.MaxLength = 100;
            this.text_cliente.Name = "text_cliente";
            this.text_cliente.ReadOnly = true;
            this.text_cliente.Size = new System.Drawing.Size(206, 20);
            this.text_cliente.TabIndex = 0;
            this.text_cliente.TabStop = false;
            // 
            // lbl_cliente
            // 
            this.lbl_cliente.AutoSize = true;
            this.lbl_cliente.Location = new System.Drawing.Point(3, -1);
            this.lbl_cliente.Name = "lbl_cliente";
            this.lbl_cliente.Size = new System.Drawing.Size(39, 13);
            this.lbl_cliente.TabIndex = 18;
            this.lbl_cliente.Text = "Cliente";
            // 
            // btn_cliente
            // 
            this.btn_cliente.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btn_cliente.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_cliente.Location = new System.Drawing.Point(218, 12);
            this.btn_cliente.Name = "btn_cliente";
            this.btn_cliente.Size = new System.Drawing.Size(49, 23);
            this.btn_cliente.TabIndex = 1;
            this.btn_cliente.Text = "&Cliente";
            this.btn_cliente.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btn_cliente.UseVisualStyleBackColor = true;
            this.btn_cliente.Click += new System.EventHandler(this.btn_cliente_Click);
            // 
            // grb_tecnico
            // 
            this.grb_tecnico.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.grb_tecnico.Controls.Add(this.lbl_tecno);
            this.grb_tecnico.Controls.Add(this.btn_tecnico);
            this.grb_tecnico.Controls.Add(this.text_tecnico);
            this.grb_tecnico.Location = new System.Drawing.Point(415, 14);
            this.grb_tecnico.Name = "grb_tecnico";
            this.grb_tecnico.Size = new System.Drawing.Size(343, 48);
            this.grb_tecnico.TabIndex = 2;
            this.grb_tecnico.TabStop = false;
            // 
            // lbl_tecno
            // 
            this.lbl_tecno.AutoSize = true;
            this.lbl_tecno.Location = new System.Drawing.Point(6, 0);
            this.lbl_tecno.Name = "lbl_tecno";
            this.lbl_tecno.Size = new System.Drawing.Size(131, 13);
            this.lbl_tecno.TabIndex = 24;
            this.lbl_tecno.Text = "Elaborador do Documento";
            // 
            // btn_tecnico
            // 
            this.btn_tecnico.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btn_tecnico.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_tecnico.Location = new System.Drawing.Point(261, 13);
            this.btn_tecnico.Name = "btn_tecnico";
            this.btn_tecnico.Size = new System.Drawing.Size(75, 23);
            this.btn_tecnico.TabIndex = 2;
            this.btn_tecnico.Text = "&Elaborador";
            this.btn_tecnico.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btn_tecnico.UseVisualStyleBackColor = true;
            this.btn_tecnico.Click += new System.EventHandler(this.btn_tecnico_Click);
            // 
            // text_tecnico
            // 
            this.text_tecnico.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.text_tecnico.BackColor = System.Drawing.Color.LightGray;
            this.text_tecnico.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.text_tecnico.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.text_tecnico.ForeColor = System.Drawing.Color.Blue;
            this.text_tecnico.Location = new System.Drawing.Point(6, 16);
            this.text_tecnico.MaxLength = 100;
            this.text_tecnico.Name = "text_tecnico";
            this.text_tecnico.ReadOnly = true;
            this.text_tecnico.Size = new System.Drawing.Size(249, 20);
            this.text_tecnico.TabIndex = 2;
            this.text_tecnico.TabStop = false;
            // 
            // lbl_vencimento
            // 
            this.lbl_vencimento.AutoSize = true;
            this.lbl_vencimento.Location = new System.Drawing.Point(581, 16);
            this.lbl_vencimento.Name = "lbl_vencimento";
            this.lbl_vencimento.Size = new System.Drawing.Size(0, 13);
            this.lbl_vencimento.TabIndex = 22;
            // 
            // grb_grauRisco
            // 
            this.grb_grauRisco.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.grb_grauRisco.BackColor = System.Drawing.Color.White;
            this.grb_grauRisco.Controls.Add(this.cb_grauRisco);
            this.grb_grauRisco.Location = new System.Drawing.Point(647, 76);
            this.grb_grauRisco.Name = "grb_grauRisco";
            this.grb_grauRisco.Size = new System.Drawing.Size(111, 49);
            this.grb_grauRisco.TabIndex = 10;
            this.grb_grauRisco.TabStop = false;
            this.grb_grauRisco.Text = "Risco";
            // 
            // cb_grauRisco
            // 
            this.cb_grauRisco.BackColor = System.Drawing.Color.Silver;
            this.cb_grauRisco.Dock = System.Windows.Forms.DockStyle.Fill;
            this.cb_grauRisco.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cb_grauRisco.FormattingEnabled = true;
            this.cb_grauRisco.Location = new System.Drawing.Point(3, 16);
            this.cb_grauRisco.Name = "cb_grauRisco";
            this.cb_grauRisco.Size = new System.Drawing.Size(105, 21);
            this.cb_grauRisco.TabIndex = 11;
            // 
            // grb_data
            // 
            this.grb_data.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.grb_data.BackColor = System.Drawing.Color.WhiteSmoke;
            this.grb_data.Controls.Add(this.lbl_dataVencimento);
            this.grb_data.Controls.Add(this.lbl_dataCriacao);
            this.grb_data.Controls.Add(this.dt_criacao);
            this.grb_data.Controls.Add(this.dt_vencimento);
            this.grb_data.Location = new System.Drawing.Point(415, 72);
            this.grb_data.Name = "grb_data";
            this.grb_data.Size = new System.Drawing.Size(226, 53);
            this.grb_data.TabIndex = 8;
            this.grb_data.TabStop = false;
            // 
            // lbl_dataVencimento
            // 
            this.lbl_dataVencimento.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lbl_dataVencimento.AutoSize = true;
            this.lbl_dataVencimento.Location = new System.Drawing.Point(109, 9);
            this.lbl_dataVencimento.Name = "lbl_dataVencimento";
            this.lbl_dataVencimento.Size = new System.Drawing.Size(104, 13);
            this.lbl_dataVencimento.TabIndex = 24;
            this.lbl_dataVencimento.Text = "Data de Vencimento";
            // 
            // lbl_dataCriacao
            // 
            this.lbl_dataCriacao.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lbl_dataCriacao.AutoSize = true;
            this.lbl_dataCriacao.Location = new System.Drawing.Point(6, 9);
            this.lbl_dataCriacao.Name = "lbl_dataCriacao";
            this.lbl_dataCriacao.Size = new System.Drawing.Size(84, 13);
            this.lbl_dataCriacao.TabIndex = 23;
            this.lbl_dataCriacao.Text = "Data de Criação";
            // 
            // dt_criacao
            // 
            this.dt_criacao.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.dt_criacao.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dt_criacao.Location = new System.Drawing.Point(6, 24);
            this.dt_criacao.Name = "dt_criacao";
            this.dt_criacao.ShowCheckBox = true;
            this.dt_criacao.Size = new System.Drawing.Size(100, 20);
            this.dt_criacao.TabIndex = 8;
            // 
            // dt_vencimento
            // 
            this.dt_vencimento.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.dt_vencimento.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dt_vencimento.Location = new System.Drawing.Point(112, 23);
            this.dt_vencimento.Name = "dt_vencimento";
            this.dt_vencimento.ShowCheckBox = true;
            this.dt_vencimento.Size = new System.Drawing.Size(104, 20);
            this.dt_vencimento.TabIndex = 9;
            // 
            // text_codPpra
            // 
            this.text_codPpra.BackColor = System.Drawing.Color.LightGray;
            this.text_codPpra.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.text_codPpra.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.text_codPpra.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.text_codPpra.Location = new System.Drawing.Point(6, 30);
            this.text_codPpra.Mask = "0000,00,000000/00";
            this.text_codPpra.Name = "text_codPpra";
            this.text_codPpra.PromptChar = ' ';
            this.text_codPpra.ReadOnly = true;
            this.text_codPpra.Size = new System.Drawing.Size(122, 20);
            this.text_codPpra.TabIndex = 1;
            this.text_codPpra.TabStop = false;
            this.text_codPpra.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // lbl_codPpra
            // 
            this.lbl_codPpra.AutoSize = true;
            this.lbl_codPpra.Location = new System.Drawing.Point(3, 14);
            this.lbl_codPpra.Name = "lbl_codPpra";
            this.lbl_codPpra.Size = new System.Drawing.Size(81, 13);
            this.lbl_codPpra.TabIndex = 17;
            this.lbl_codPpra.Text = "Código PCMSO";
            // 
            // btn_salvar
            // 
            this.btn_salvar.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btn_salvar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_salvar.Image = global::SWS.Properties.Resources.icone_mais;
            this.btn_salvar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btn_salvar.Location = new System.Drawing.Point(6, 400);
            this.btn_salvar.Name = "btn_salvar";
            this.btn_salvar.Size = new System.Drawing.Size(77, 23);
            this.btn_salvar.TabIndex = 21;
            this.btn_salvar.Text = "&Novo";
            this.btn_salvar.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btn_salvar.UseVisualStyleBackColor = true;
            this.btn_salvar.Click += new System.EventHandler(this.btn_salvar_Click);
            // 
            // btn_limpar
            // 
            this.btn_limpar.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btn_limpar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_limpar.Image = global::SWS.Properties.Resources.vassoura;
            this.btn_limpar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btn_limpar.Location = new System.Drawing.Point(89, 400);
            this.btn_limpar.Name = "btn_limpar";
            this.btn_limpar.Size = new System.Drawing.Size(77, 23);
            this.btn_limpar.TabIndex = 22;
            this.btn_limpar.Text = "&Limpar";
            this.btn_limpar.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btn_limpar.UseVisualStyleBackColor = true;
            this.btn_limpar.Click += new System.EventHandler(this.btn_limpar_Click);
            // 
            // tb_ghe
            // 
            this.tb_ghe.Controls.Add(this.btn_alterarGhe);
            this.tb_ghe.Controls.Add(this.grb_ghePPRA);
            this.tb_ghe.Controls.Add(this.btn_incluirGHE);
            this.tb_ghe.Controls.Add(this.btn_excluirGHE);
            this.tb_ghe.Location = new System.Drawing.Point(4, 22);
            this.tb_ghe.Name = "tb_ghe";
            this.tb_ghe.Size = new System.Drawing.Size(773, 429);
            this.tb_ghe.TabIndex = 2;
            this.tb_ghe.Text = "GHE";
            this.tb_ghe.UseVisualStyleBackColor = true;
            // 
            // btn_alterarGhe
            // 
            this.btn_alterarGhe.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btn_alterarGhe.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_alterarGhe.Image = global::SWS.Properties.Resources.Alterar;
            this.btn_alterarGhe.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btn_alterarGhe.Location = new System.Drawing.Point(88, 392);
            this.btn_alterarGhe.Name = "btn_alterarGhe";
            this.btn_alterarGhe.Size = new System.Drawing.Size(77, 23);
            this.btn_alterarGhe.TabIndex = 2;
            this.btn_alterarGhe.Text = "&Alterar";
            this.btn_alterarGhe.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btn_alterarGhe.UseVisualStyleBackColor = true;
            this.btn_alterarGhe.Click += new System.EventHandler(this.btn_alterarGhe_Click);
            // 
            // grb_ghePPRA
            // 
            this.grb_ghePPRA.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.grb_ghePPRA.Controls.Add(this.grd_ghe);
            this.grb_ghePPRA.Location = new System.Drawing.Point(5, 3);
            this.grb_ghePPRA.Name = "grb_ghePPRA";
            this.grb_ghePPRA.Size = new System.Drawing.Size(765, 383);
            this.grb_ghePPRA.TabIndex = 6;
            this.grb_ghePPRA.TabStop = false;
            this.grb_ghePPRA.Text = "GHE";
            // 
            // grd_ghe
            // 
            this.grd_ghe.AllowUserToAddRows = false;
            this.grd_ghe.AllowUserToDeleteRows = false;
            this.grd_ghe.BackgroundColor = System.Drawing.Color.White;
            this.grd_ghe.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.grd_ghe.Dock = System.Windows.Forms.DockStyle.Fill;
            this.grd_ghe.Location = new System.Drawing.Point(3, 16);
            this.grd_ghe.MultiSelect = false;
            this.grd_ghe.Name = "grd_ghe";
            this.grd_ghe.ReadOnly = true;
            this.grd_ghe.Size = new System.Drawing.Size(759, 364);
            this.grd_ghe.TabIndex = 3;
            // 
            // btn_incluirGHE
            // 
            this.btn_incluirGHE.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btn_incluirGHE.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_incluirGHE.Image = global::SWS.Properties.Resources.icone_mais;
            this.btn_incluirGHE.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btn_incluirGHE.Location = new System.Drawing.Point(5, 392);
            this.btn_incluirGHE.Name = "btn_incluirGHE";
            this.btn_incluirGHE.Size = new System.Drawing.Size(77, 23);
            this.btn_incluirGHE.TabIndex = 1;
            this.btn_incluirGHE.Text = "&Incluir";
            this.btn_incluirGHE.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btn_incluirGHE.UseVisualStyleBackColor = true;
            this.btn_incluirGHE.Click += new System.EventHandler(this.btn_incluirGHE_Click);
            // 
            // btn_excluirGHE
            // 
            this.btn_excluirGHE.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btn_excluirGHE.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_excluirGHE.Image = global::SWS.Properties.Resources.lixeira;
            this.btn_excluirGHE.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btn_excluirGHE.Location = new System.Drawing.Point(171, 392);
            this.btn_excluirGHE.Name = "btn_excluirGHE";
            this.btn_excluirGHE.Size = new System.Drawing.Size(77, 23);
            this.btn_excluirGHE.TabIndex = 3;
            this.btn_excluirGHE.Text = "&Excluir";
            this.btn_excluirGHE.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btn_excluirGHE.UseVisualStyleBackColor = true;
            this.btn_excluirGHE.Click += new System.EventHandler(this.btn_excluirGHE_Click);
            // 
            // tb_gheFonte
            // 
            this.tb_gheFonte.Controls.Add(this.tb_fonteGHE);
            this.tb_gheFonte.Location = new System.Drawing.Point(4, 22);
            this.tb_gheFonte.Name = "tb_gheFonte";
            this.tb_gheFonte.Size = new System.Drawing.Size(773, 429);
            this.tb_gheFonte.TabIndex = 3;
            this.tb_gheFonte.Text = "Fontes";
            this.tb_gheFonte.UseVisualStyleBackColor = true;
            // 
            // tb_fonteGHE
            // 
            this.tb_fonteGHE.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tb_fonteGHE.Controls.Add(this.Fonte);
            this.tb_fonteGHE.Controls.Add(this.agente);
            this.tb_fonteGHE.Location = new System.Drawing.Point(3, 3);
            this.tb_fonteGHE.Name = "tb_fonteGHE";
            this.tb_fonteGHE.SelectedIndex = 0;
            this.tb_fonteGHE.Size = new System.Drawing.Size(757, 421);
            this.tb_fonteGHE.TabIndex = 8;
            this.tb_fonteGHE.SelectedIndexChanged += new System.EventHandler(this.tb_fonteGHE_SelectedIndexChanged);
            // 
            // Fonte
            // 
            this.Fonte.Controls.Add(this.grb_gheFonte);
            this.Fonte.Controls.Add(this.btn_excluirFonte);
            this.Fonte.Controls.Add(this.grb_fonte);
            this.Fonte.Controls.Add(this.btn_incluirFonte);
            this.Fonte.Location = new System.Drawing.Point(4, 22);
            this.Fonte.Name = "Fonte";
            this.Fonte.Padding = new System.Windows.Forms.Padding(3);
            this.Fonte.Size = new System.Drawing.Size(749, 395);
            this.Fonte.TabIndex = 0;
            this.Fonte.Text = "Fonte";
            this.Fonte.UseVisualStyleBackColor = true;
            // 
            // grb_gheFonte
            // 
            this.grb_gheFonte.Controls.Add(this.cb_ghe);
            this.grb_gheFonte.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.grb_gheFonte.Location = new System.Drawing.Point(6, 18);
            this.grb_gheFonte.Name = "grb_gheFonte";
            this.grb_gheFonte.Size = new System.Drawing.Size(378, 54);
            this.grb_gheFonte.TabIndex = 9;
            this.grb_gheFonte.TabStop = false;
            this.grb_gheFonte.Text = "GHE";
            // 
            // cb_ghe
            // 
            this.cb_ghe.BackColor = System.Drawing.Color.LightGray;
            this.cb_ghe.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cb_ghe.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cb_ghe.FormattingEnabled = true;
            this.cb_ghe.Location = new System.Drawing.Point(6, 19);
            this.cb_ghe.Name = "cb_ghe";
            this.cb_ghe.Size = new System.Drawing.Size(366, 21);
            this.cb_ghe.TabIndex = 1;
            this.cb_ghe.SelectedIndexChanged += new System.EventHandler(this.cb_ghe_SelectedIndexChanged);
            // 
            // btn_excluirFonte
            // 
            this.btn_excluirFonte.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btn_excluirFonte.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_excluirFonte.Image = global::SWS.Properties.Resources.lixeira;
            this.btn_excluirFonte.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btn_excluirFonte.Location = new System.Drawing.Point(476, 353);
            this.btn_excluirFonte.Name = "btn_excluirFonte";
            this.btn_excluirFonte.Size = new System.Drawing.Size(77, 23);
            this.btn_excluirFonte.TabIndex = 3;
            this.btn_excluirFonte.Text = "&Excluir";
            this.btn_excluirFonte.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btn_excluirFonte.UseVisualStyleBackColor = true;
            this.btn_excluirFonte.Click += new System.EventHandler(this.btn_excluirFonte_Click);
            // 
            // grb_fonte
            // 
            this.grb_fonte.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.grb_fonte.Controls.Add(this.grd_fonte);
            this.grb_fonte.Location = new System.Drawing.Point(390, 18);
            this.grb_fonte.Name = "grb_fonte";
            this.grb_fonte.Size = new System.Drawing.Size(353, 332);
            this.grb_fonte.TabIndex = 8;
            this.grb_fonte.TabStop = false;
            this.grb_fonte.Text = "Fonte";
            // 
            // grd_fonte
            // 
            this.grd_fonte.AllowUserToAddRows = false;
            this.grd_fonte.AllowUserToDeleteRows = false;
            this.grd_fonte.AllowUserToOrderColumns = true;
            this.grd_fonte.AllowUserToResizeColumns = false;
            this.grd_fonte.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this.grd_fonte.BackgroundColor = System.Drawing.Color.White;
            this.grd_fonte.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dataGridViewCellStyle9.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle9.BackColor = System.Drawing.Color.LightGray;
            dataGridViewCellStyle9.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle9.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle9.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle9.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle9.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.grd_fonte.DefaultCellStyle = dataGridViewCellStyle9;
            this.grd_fonte.Dock = System.Windows.Forms.DockStyle.Fill;
            this.grd_fonte.Location = new System.Drawing.Point(3, 16);
            this.grd_fonte.Name = "grd_fonte";
            this.grd_fonte.ReadOnly = true;
            this.grd_fonte.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.grd_fonte.Size = new System.Drawing.Size(347, 313);
            this.grd_fonte.TabIndex = 3;
            // 
            // btn_incluirFonte
            // 
            this.btn_incluirFonte.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btn_incluirFonte.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_incluirFonte.Image = global::SWS.Properties.Resources.icone_mais;
            this.btn_incluirFonte.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btn_incluirFonte.Location = new System.Drawing.Point(393, 353);
            this.btn_incluirFonte.Name = "btn_incluirFonte";
            this.btn_incluirFonte.Size = new System.Drawing.Size(77, 23);
            this.btn_incluirFonte.TabIndex = 2;
            this.btn_incluirFonte.Text = "&Incluir";
            this.btn_incluirFonte.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btn_incluirFonte.UseVisualStyleBackColor = true;
            this.btn_incluirFonte.Click += new System.EventHandler(this.btn_incluirFonte_Click);
            // 
            // agente
            // 
            this.agente.Controls.Add(this.btn_excluirAgente);
            this.agente.Controls.Add(this.grb_gheAgente);
            this.agente.Controls.Add(this.btn_incluirAgente);
            this.agente.Controls.Add(this.grb_fonteAgente);
            this.agente.Controls.Add(this.grb_agente);
            this.agente.Location = new System.Drawing.Point(4, 22);
            this.agente.Name = "agente";
            this.agente.Padding = new System.Windows.Forms.Padding(3);
            this.agente.Size = new System.Drawing.Size(749, 395);
            this.agente.TabIndex = 1;
            this.agente.Text = "Agente";
            this.agente.UseVisualStyleBackColor = true;
            // 
            // btn_excluirAgente
            // 
            this.btn_excluirAgente.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btn_excluirAgente.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_excluirAgente.Image = global::SWS.Properties.Resources.lixeira;
            this.btn_excluirAgente.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btn_excluirAgente.Location = new System.Drawing.Point(476, 353);
            this.btn_excluirAgente.Name = "btn_excluirAgente";
            this.btn_excluirAgente.Size = new System.Drawing.Size(77, 23);
            this.btn_excluirAgente.TabIndex = 4;
            this.btn_excluirAgente.Text = "&Excluir";
            this.btn_excluirAgente.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btn_excluirAgente.UseVisualStyleBackColor = true;
            this.btn_excluirAgente.Click += new System.EventHandler(this.btn_excluirAgente_Click);
            // 
            // grb_gheAgente
            // 
            this.grb_gheAgente.Controls.Add(this.cb_gheAgente);
            this.grb_gheAgente.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.grb_gheAgente.Location = new System.Drawing.Point(6, 18);
            this.grb_gheAgente.Name = "grb_gheAgente";
            this.grb_gheAgente.Size = new System.Drawing.Size(378, 51);
            this.grb_gheAgente.TabIndex = 6;
            this.grb_gheAgente.TabStop = false;
            this.grb_gheAgente.Text = "GHE";
            // 
            // cb_gheAgente
            // 
            this.cb_gheAgente.BackColor = System.Drawing.Color.LightGray;
            this.cb_gheAgente.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cb_gheAgente.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cb_gheAgente.FormattingEnabled = true;
            this.cb_gheAgente.Location = new System.Drawing.Point(6, 19);
            this.cb_gheAgente.Name = "cb_gheAgente";
            this.cb_gheAgente.Size = new System.Drawing.Size(366, 21);
            this.cb_gheAgente.TabIndex = 1;
            this.cb_gheAgente.SelectedIndexChanged += new System.EventHandler(this.cb_gheAgente_SelectedIndexChanged);
            // 
            // btn_incluirAgente
            // 
            this.btn_incluirAgente.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btn_incluirAgente.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_incluirAgente.Image = global::SWS.Properties.Resources.icone_mais;
            this.btn_incluirAgente.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btn_incluirAgente.Location = new System.Drawing.Point(393, 353);
            this.btn_incluirAgente.Name = "btn_incluirAgente";
            this.btn_incluirAgente.Size = new System.Drawing.Size(77, 23);
            this.btn_incluirAgente.TabIndex = 3;
            this.btn_incluirAgente.Text = "&Incluir";
            this.btn_incluirAgente.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btn_incluirAgente.UseVisualStyleBackColor = true;
            this.btn_incluirAgente.Click += new System.EventHandler(this.btn_incluirAgente_Click);
            // 
            // grb_fonteAgente
            // 
            this.grb_fonteAgente.Controls.Add(this.cb_fonteAgente);
            this.grb_fonteAgente.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.grb_fonteAgente.Location = new System.Drawing.Point(6, 78);
            this.grb_fonteAgente.Name = "grb_fonteAgente";
            this.grb_fonteAgente.Size = new System.Drawing.Size(378, 54);
            this.grb_fonteAgente.TabIndex = 4;
            this.grb_fonteAgente.TabStop = false;
            this.grb_fonteAgente.Text = "Fonte";
            // 
            // cb_fonteAgente
            // 
            this.cb_fonteAgente.BackColor = System.Drawing.Color.LightGray;
            this.cb_fonteAgente.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cb_fonteAgente.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cb_fonteAgente.FormattingEnabled = true;
            this.cb_fonteAgente.Location = new System.Drawing.Point(6, 19);
            this.cb_fonteAgente.Name = "cb_fonteAgente";
            this.cb_fonteAgente.Size = new System.Drawing.Size(366, 21);
            this.cb_fonteAgente.TabIndex = 2;
            this.cb_fonteAgente.SelectedIndexChanged += new System.EventHandler(this.cb_fonteAgente_SelectedIndexChanged);
            // 
            // grb_agente
            // 
            this.grb_agente.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.grb_agente.Controls.Add(this.grd_agente);
            this.grb_agente.Location = new System.Drawing.Point(390, 18);
            this.grb_agente.Name = "grb_agente";
            this.grb_agente.Size = new System.Drawing.Size(353, 332);
            this.grb_agente.TabIndex = 5;
            this.grb_agente.TabStop = false;
            this.grb_agente.Text = "Agente";
            // 
            // grd_agente
            // 
            this.grd_agente.AllowUserToAddRows = false;
            this.grd_agente.AllowUserToDeleteRows = false;
            this.grd_agente.AllowUserToOrderColumns = true;
            this.grd_agente.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.DisplayedCells;
            this.grd_agente.BackgroundColor = System.Drawing.Color.White;
            this.grd_agente.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dataGridViewCellStyle10.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle10.BackColor = System.Drawing.Color.LightGray;
            dataGridViewCellStyle10.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle10.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle10.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle10.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle10.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.grd_agente.DefaultCellStyle = dataGridViewCellStyle10;
            this.grd_agente.Dock = System.Windows.Forms.DockStyle.Fill;
            this.grd_agente.Location = new System.Drawing.Point(3, 16);
            this.grd_agente.Name = "grd_agente";
            this.grd_agente.ReadOnly = true;
            this.grd_agente.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.grd_agente.Size = new System.Drawing.Size(347, 313);
            this.grd_agente.TabIndex = 0;
            // 
            // tb_setorGHE
            // 
            this.tb_setorGHE.Controls.Add(this.tb_gheSetor);
            this.tb_setorGHE.Location = new System.Drawing.Point(4, 22);
            this.tb_setorGHE.Name = "tb_setorGHE";
            this.tb_setorGHE.Size = new System.Drawing.Size(773, 429);
            this.tb_setorGHE.TabIndex = 4;
            this.tb_setorGHE.Text = "Setores";
            this.tb_setorGHE.UseVisualStyleBackColor = true;
            // 
            // tb_gheSetor
            // 
            this.tb_gheSetor.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tb_gheSetor.Controls.Add(this.tb_setor);
            this.tb_gheSetor.Controls.Add(this.tb_funcao);
            this.tb_gheSetor.Controls.Add(this.tb_funcaoPPRA);
            this.tb_gheSetor.Location = new System.Drawing.Point(3, 3);
            this.tb_gheSetor.Name = "tb_gheSetor";
            this.tb_gheSetor.SelectedIndex = 0;
            this.tb_gheSetor.Size = new System.Drawing.Size(757, 421);
            this.tb_gheSetor.TabIndex = 0;
            this.tb_gheSetor.SelectedIndexChanged += new System.EventHandler(this.tb_gheSetor_SelectedIndexChanged);
            // 
            // tb_setor
            // 
            this.tb_setor.Controls.Add(this.grb_setor);
            this.tb_setor.Controls.Add(this.btn_excluirSetor);
            this.tb_setor.Controls.Add(this.grb_gheSetor);
            this.tb_setor.Controls.Add(this.btn_incluirSetor);
            this.tb_setor.Location = new System.Drawing.Point(4, 22);
            this.tb_setor.Name = "tb_setor";
            this.tb_setor.Padding = new System.Windows.Forms.Padding(3);
            this.tb_setor.Size = new System.Drawing.Size(749, 395);
            this.tb_setor.TabIndex = 0;
            this.tb_setor.Text = "Setor";
            this.tb_setor.UseVisualStyleBackColor = true;
            // 
            // grb_setor
            // 
            this.grb_setor.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.grb_setor.Controls.Add(this.grd_setor);
            this.grb_setor.Location = new System.Drawing.Point(390, 18);
            this.grb_setor.Name = "grb_setor";
            this.grb_setor.Size = new System.Drawing.Size(353, 332);
            this.grb_setor.TabIndex = 9;
            this.grb_setor.TabStop = false;
            this.grb_setor.Text = "Setor";
            // 
            // grd_setor
            // 
            this.grd_setor.AllowUserToAddRows = false;
            this.grd_setor.AllowUserToDeleteRows = false;
            this.grd_setor.AllowUserToOrderColumns = true;
            this.grd_setor.AllowUserToResizeRows = false;
            this.grd_setor.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this.grd_setor.BackgroundColor = System.Drawing.Color.White;
            this.grd_setor.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dataGridViewCellStyle11.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle11.BackColor = System.Drawing.Color.LightGray;
            dataGridViewCellStyle11.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle11.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle11.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle11.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle11.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.grd_setor.DefaultCellStyle = dataGridViewCellStyle11;
            this.grd_setor.Dock = System.Windows.Forms.DockStyle.Fill;
            this.grd_setor.Location = new System.Drawing.Point(3, 16);
            this.grd_setor.Name = "grd_setor";
            this.grd_setor.ReadOnly = true;
            this.grd_setor.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.grd_setor.Size = new System.Drawing.Size(347, 313);
            this.grd_setor.TabIndex = 3;
            // 
            // btn_excluirSetor
            // 
            this.btn_excluirSetor.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btn_excluirSetor.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_excluirSetor.Image = global::SWS.Properties.Resources.lixeira;
            this.btn_excluirSetor.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btn_excluirSetor.Location = new System.Drawing.Point(476, 356);
            this.btn_excluirSetor.Name = "btn_excluirSetor";
            this.btn_excluirSetor.Size = new System.Drawing.Size(77, 23);
            this.btn_excluirSetor.TabIndex = 3;
            this.btn_excluirSetor.Text = "&Excluir";
            this.btn_excluirSetor.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btn_excluirSetor.UseVisualStyleBackColor = true;
            this.btn_excluirSetor.Click += new System.EventHandler(this.btn_excluirSetor_Click);
            // 
            // grb_gheSetor
            // 
            this.grb_gheSetor.Controls.Add(this.cb_gheSetor);
            this.grb_gheSetor.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.grb_gheSetor.Location = new System.Drawing.Point(6, 18);
            this.grb_gheSetor.Name = "grb_gheSetor";
            this.grb_gheSetor.Size = new System.Drawing.Size(378, 54);
            this.grb_gheSetor.TabIndex = 5;
            this.grb_gheSetor.TabStop = false;
            this.grb_gheSetor.Text = "GHE";
            // 
            // cb_gheSetor
            // 
            this.cb_gheSetor.BackColor = System.Drawing.Color.LightGray;
            this.cb_gheSetor.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cb_gheSetor.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cb_gheSetor.FormattingEnabled = true;
            this.cb_gheSetor.Location = new System.Drawing.Point(6, 19);
            this.cb_gheSetor.Name = "cb_gheSetor";
            this.cb_gheSetor.Size = new System.Drawing.Size(366, 21);
            this.cb_gheSetor.TabIndex = 1;
            this.cb_gheSetor.SelectedIndexChanged += new System.EventHandler(this.cb_gheSetor_SelectedIndexChanged);
            // 
            // btn_incluirSetor
            // 
            this.btn_incluirSetor.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btn_incluirSetor.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_incluirSetor.Image = global::SWS.Properties.Resources.icone_mais;
            this.btn_incluirSetor.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btn_incluirSetor.Location = new System.Drawing.Point(393, 356);
            this.btn_incluirSetor.Name = "btn_incluirSetor";
            this.btn_incluirSetor.Size = new System.Drawing.Size(77, 23);
            this.btn_incluirSetor.TabIndex = 2;
            this.btn_incluirSetor.Text = "&Incluir";
            this.btn_incluirSetor.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btn_incluirSetor.UseVisualStyleBackColor = true;
            this.btn_incluirSetor.Click += new System.EventHandler(this.btn_incluirSetor_Click);
            // 
            // tb_funcao
            // 
            this.tb_funcao.Controls.Add(this.btn_excluirFuncaoClientePPRA);
            this.tb_funcao.Controls.Add(this.btn_incluirFuncaoClientePPRA);
            this.tb_funcao.Controls.Add(this.grb_funcao);
            this.tb_funcao.Location = new System.Drawing.Point(4, 22);
            this.tb_funcao.Name = "tb_funcao";
            this.tb_funcao.Padding = new System.Windows.Forms.Padding(3);
            this.tb_funcao.Size = new System.Drawing.Size(749, 395);
            this.tb_funcao.TabIndex = 1;
            this.tb_funcao.Text = "Função do Cliente";
            this.tb_funcao.UseVisualStyleBackColor = true;
            // 
            // btn_excluirFuncaoClientePPRA
            // 
            this.btn_excluirFuncaoClientePPRA.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btn_excluirFuncaoClientePPRA.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_excluirFuncaoClientePPRA.Image = global::SWS.Properties.Resources.lixeira;
            this.btn_excluirFuncaoClientePPRA.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btn_excluirFuncaoClientePPRA.Location = new System.Drawing.Point(95, 366);
            this.btn_excluirFuncaoClientePPRA.Name = "btn_excluirFuncaoClientePPRA";
            this.btn_excluirFuncaoClientePPRA.Size = new System.Drawing.Size(77, 23);
            this.btn_excluirFuncaoClientePPRA.TabIndex = 2;
            this.btn_excluirFuncaoClientePPRA.Text = "&Excluir";
            this.btn_excluirFuncaoClientePPRA.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btn_excluirFuncaoClientePPRA.UseVisualStyleBackColor = true;
            this.btn_excluirFuncaoClientePPRA.Click += new System.EventHandler(this.btn_excluirFuncaoClientePPRA_Click);
            // 
            // btn_incluirFuncaoClientePPRA
            // 
            this.btn_incluirFuncaoClientePPRA.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btn_incluirFuncaoClientePPRA.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_incluirFuncaoClientePPRA.Image = global::SWS.Properties.Resources.icone_mais;
            this.btn_incluirFuncaoClientePPRA.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btn_incluirFuncaoClientePPRA.Location = new System.Drawing.Point(12, 366);
            this.btn_incluirFuncaoClientePPRA.Name = "btn_incluirFuncaoClientePPRA";
            this.btn_incluirFuncaoClientePPRA.Size = new System.Drawing.Size(77, 23);
            this.btn_incluirFuncaoClientePPRA.TabIndex = 1;
            this.btn_incluirFuncaoClientePPRA.Text = "&Incluir";
            this.btn_incluirFuncaoClientePPRA.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btn_incluirFuncaoClientePPRA.UseVisualStyleBackColor = true;
            this.btn_incluirFuncaoClientePPRA.Click += new System.EventHandler(this.btn_incluirFuncaoClientePPRA_Click);
            // 
            // grb_funcao
            // 
            this.grb_funcao.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.grb_funcao.Controls.Add(this.grd_funcaoCliente);
            this.grb_funcao.Location = new System.Drawing.Point(6, 6);
            this.grb_funcao.Name = "grb_funcao";
            this.grb_funcao.Size = new System.Drawing.Size(729, 354);
            this.grb_funcao.TabIndex = 20;
            this.grb_funcao.TabStop = false;
            this.grb_funcao.Text = "Função do Cliente";
            // 
            // grd_funcaoCliente
            // 
            this.grd_funcaoCliente.AllowUserToAddRows = false;
            this.grd_funcaoCliente.AllowUserToDeleteRows = false;
            this.grd_funcaoCliente.AllowUserToOrderColumns = true;
            this.grd_funcaoCliente.AllowUserToResizeRows = false;
            this.grd_funcaoCliente.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this.grd_funcaoCliente.BackgroundColor = System.Drawing.Color.White;
            this.grd_funcaoCliente.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.grd_funcaoCliente.Dock = System.Windows.Forms.DockStyle.Fill;
            this.grd_funcaoCliente.Location = new System.Drawing.Point(3, 16);
            this.grd_funcaoCliente.Name = "grd_funcaoCliente";
            this.grd_funcaoCliente.ReadOnly = true;
            this.grd_funcaoCliente.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.grd_funcaoCliente.Size = new System.Drawing.Size(723, 335);
            this.grd_funcaoCliente.TabIndex = 16;
            // 
            // tb_funcaoPPRA
            // 
            this.tb_funcaoPPRA.Controls.Add(this.btn_excluirFuncaoCliente);
            this.tb_funcaoPPRA.Controls.Add(this.grb_funcaoCliente);
            this.tb_funcaoPPRA.Controls.Add(this.btn_incluirFuncaoCliente);
            this.tb_funcaoPPRA.Controls.Add(this.grb_setorFuncao);
            this.tb_funcaoPPRA.Controls.Add(this.grb_gheFuncao);
            this.tb_funcaoPPRA.Location = new System.Drawing.Point(4, 22);
            this.tb_funcaoPPRA.Name = "tb_funcaoPPRA";
            this.tb_funcaoPPRA.Padding = new System.Windows.Forms.Padding(3);
            this.tb_funcaoPPRA.Size = new System.Drawing.Size(749, 395);
            this.tb_funcaoPPRA.TabIndex = 2;
            this.tb_funcaoPPRA.Text = "Função";
            this.tb_funcaoPPRA.UseVisualStyleBackColor = true;
            // 
            // btn_excluirFuncaoCliente
            // 
            this.btn_excluirFuncaoCliente.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btn_excluirFuncaoCliente.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_excluirFuncaoCliente.Image = global::SWS.Properties.Resources.lixeira;
            this.btn_excluirFuncaoCliente.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btn_excluirFuncaoCliente.Location = new System.Drawing.Point(476, 353);
            this.btn_excluirFuncaoCliente.Name = "btn_excluirFuncaoCliente";
            this.btn_excluirFuncaoCliente.Size = new System.Drawing.Size(77, 23);
            this.btn_excluirFuncaoCliente.TabIndex = 4;
            this.btn_excluirFuncaoCliente.Text = "&Excluir";
            this.btn_excluirFuncaoCliente.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btn_excluirFuncaoCliente.UseVisualStyleBackColor = true;
            this.btn_excluirFuncaoCliente.Click += new System.EventHandler(this.btn_excluirFuncaoCliente_Click);
            // 
            // grb_funcaoCliente
            // 
            this.grb_funcaoCliente.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.grb_funcaoCliente.Controls.Add(this.grd_setorFuncao);
            this.grb_funcaoCliente.Location = new System.Drawing.Point(390, 18);
            this.grb_funcaoCliente.Name = "grb_funcaoCliente";
            this.grb_funcaoCliente.Size = new System.Drawing.Size(353, 332);
            this.grb_funcaoCliente.TabIndex = 8;
            this.grb_funcaoCliente.TabStop = false;
            this.grb_funcaoCliente.Text = "Funcao Cliente";
            // 
            // grd_setorFuncao
            // 
            this.grd_setorFuncao.AllowUserToAddRows = false;
            this.grd_setorFuncao.AllowUserToDeleteRows = false;
            this.grd_setorFuncao.AllowUserToOrderColumns = true;
            this.grd_setorFuncao.AllowUserToResizeRows = false;
            this.grd_setorFuncao.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this.grd_setorFuncao.BackgroundColor = System.Drawing.Color.White;
            this.grd_setorFuncao.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dataGridViewCellStyle12.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle12.BackColor = System.Drawing.Color.LightGray;
            dataGridViewCellStyle12.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle12.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle12.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle12.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle12.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.grd_setorFuncao.DefaultCellStyle = dataGridViewCellStyle12;
            this.grd_setorFuncao.Dock = System.Windows.Forms.DockStyle.Fill;
            this.grd_setorFuncao.Location = new System.Drawing.Point(3, 16);
            this.grd_setorFuncao.Name = "grd_setorFuncao";
            this.grd_setorFuncao.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.grd_setorFuncao.Size = new System.Drawing.Size(347, 313);
            this.grd_setorFuncao.TabIndex = 0;
            this.grd_setorFuncao.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.grd_setorFuncao_CellClick);
            this.grd_setorFuncao.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.grd_setorFuncao_CellContentClick);
            this.grd_setorFuncao.CellFormatting += new System.Windows.Forms.DataGridViewCellFormattingEventHandler(this.grd_setorFuncao_CellFormatting);
            // 
            // btn_incluirFuncaoCliente
            // 
            this.btn_incluirFuncaoCliente.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btn_incluirFuncaoCliente.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_incluirFuncaoCliente.Image = global::SWS.Properties.Resources.icone_mais;
            this.btn_incluirFuncaoCliente.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btn_incluirFuncaoCliente.Location = new System.Drawing.Point(393, 353);
            this.btn_incluirFuncaoCliente.Name = "btn_incluirFuncaoCliente";
            this.btn_incluirFuncaoCliente.Size = new System.Drawing.Size(77, 23);
            this.btn_incluirFuncaoCliente.TabIndex = 3;
            this.btn_incluirFuncaoCliente.Text = "&Incluir";
            this.btn_incluirFuncaoCliente.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btn_incluirFuncaoCliente.UseVisualStyleBackColor = true;
            this.btn_incluirFuncaoCliente.Click += new System.EventHandler(this.btn_incluirFuncaoCliente_Click);
            // 
            // grb_setorFuncao
            // 
            this.grb_setorFuncao.Controls.Add(this.cb_setorFuncao);
            this.grb_setorFuncao.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.grb_setorFuncao.Location = new System.Drawing.Point(6, 78);
            this.grb_setorFuncao.Name = "grb_setorFuncao";
            this.grb_setorFuncao.Size = new System.Drawing.Size(378, 54);
            this.grb_setorFuncao.TabIndex = 7;
            this.grb_setorFuncao.TabStop = false;
            this.grb_setorFuncao.Text = "Setor";
            // 
            // cb_setorFuncao
            // 
            this.cb_setorFuncao.BackColor = System.Drawing.Color.LightGray;
            this.cb_setorFuncao.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cb_setorFuncao.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cb_setorFuncao.FormattingEnabled = true;
            this.cb_setorFuncao.Location = new System.Drawing.Point(6, 19);
            this.cb_setorFuncao.Name = "cb_setorFuncao";
            this.cb_setorFuncao.Size = new System.Drawing.Size(366, 21);
            this.cb_setorFuncao.TabIndex = 2;
            this.cb_setorFuncao.SelectedIndexChanged += new System.EventHandler(this.cb_setorFuncao_SelectedIndexChanged);
            // 
            // grb_gheFuncao
            // 
            this.grb_gheFuncao.Controls.Add(this.cb_gheFuncao);
            this.grb_gheFuncao.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.grb_gheFuncao.Location = new System.Drawing.Point(6, 18);
            this.grb_gheFuncao.Name = "grb_gheFuncao";
            this.grb_gheFuncao.Size = new System.Drawing.Size(378, 54);
            this.grb_gheFuncao.TabIndex = 6;
            this.grb_gheFuncao.TabStop = false;
            this.grb_gheFuncao.Text = "GHE";
            // 
            // cb_gheFuncao
            // 
            this.cb_gheFuncao.BackColor = System.Drawing.Color.LightGray;
            this.cb_gheFuncao.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cb_gheFuncao.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cb_gheFuncao.FormattingEnabled = true;
            this.cb_gheFuncao.Location = new System.Drawing.Point(6, 19);
            this.cb_gheFuncao.Name = "cb_gheFuncao";
            this.cb_gheFuncao.Size = new System.Drawing.Size(366, 21);
            this.cb_gheFuncao.TabIndex = 1;
            this.cb_gheFuncao.SelectedIndexChanged += new System.EventHandler(this.cb_gheFuncao_SelectedIndexChanged);
            // 
            // tb_cronograma
            // 
            this.tb_cronograma.BackColor = System.Drawing.Color.White;
            this.tb_cronograma.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tb_cronograma.Controls.Add(this.btn_excluirAtividade);
            this.tb_cronograma.Controls.Add(this.btn_alterarAtividade);
            this.tb_cronograma.Controls.Add(this.btn_incluirAtividade);
            this.tb_cronograma.Controls.Add(this.grb_atividade);
            this.tb_cronograma.Location = new System.Drawing.Point(4, 22);
            this.tb_cronograma.Name = "tb_cronograma";
            this.tb_cronograma.Padding = new System.Windows.Forms.Padding(3);
            this.tb_cronograma.Size = new System.Drawing.Size(773, 429);
            this.tb_cronograma.TabIndex = 1;
            this.tb_cronograma.Text = "Cronograma";
            // 
            // btn_excluirAtividade
            // 
            this.btn_excluirAtividade.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btn_excluirAtividade.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_excluirAtividade.Image = global::SWS.Properties.Resources.lixeira;
            this.btn_excluirAtividade.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btn_excluirAtividade.Location = new System.Drawing.Point(175, 387);
            this.btn_excluirAtividade.Name = "btn_excluirAtividade";
            this.btn_excluirAtividade.Size = new System.Drawing.Size(77, 23);
            this.btn_excluirAtividade.TabIndex = 5;
            this.btn_excluirAtividade.Text = "&Excluir";
            this.btn_excluirAtividade.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btn_excluirAtividade.UseVisualStyleBackColor = true;
            this.btn_excluirAtividade.Click += new System.EventHandler(this.btn_excluirAtividade_Click);
            // 
            // btn_alterarAtividade
            // 
            this.btn_alterarAtividade.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btn_alterarAtividade.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_alterarAtividade.Image = global::SWS.Properties.Resources.Alterar;
            this.btn_alterarAtividade.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btn_alterarAtividade.Location = new System.Drawing.Point(92, 387);
            this.btn_alterarAtividade.Name = "btn_alterarAtividade";
            this.btn_alterarAtividade.Size = new System.Drawing.Size(77, 23);
            this.btn_alterarAtividade.TabIndex = 4;
            this.btn_alterarAtividade.Text = "&Alterar";
            this.btn_alterarAtividade.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btn_alterarAtividade.UseVisualStyleBackColor = true;
            this.btn_alterarAtividade.Click += new System.EventHandler(this.btn_alterarAtividade_Click);
            // 
            // btn_incluirAtividade
            // 
            this.btn_incluirAtividade.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btn_incluirAtividade.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_incluirAtividade.Image = global::SWS.Properties.Resources.icone_mais;
            this.btn_incluirAtividade.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btn_incluirAtividade.Location = new System.Drawing.Point(9, 387);
            this.btn_incluirAtividade.Name = "btn_incluirAtividade";
            this.btn_incluirAtividade.Size = new System.Drawing.Size(77, 23);
            this.btn_incluirAtividade.TabIndex = 3;
            this.btn_incluirAtividade.Text = "&Incluir";
            this.btn_incluirAtividade.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btn_incluirAtividade.UseVisualStyleBackColor = true;
            this.btn_incluirAtividade.Click += new System.EventHandler(this.btn_incluirAtividade_Click);
            // 
            // grb_atividade
            // 
            this.grb_atividade.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.grb_atividade.Controls.Add(this.grd_atividadeCronograma);
            this.grb_atividade.Location = new System.Drawing.Point(6, 6);
            this.grb_atividade.Name = "grb_atividade";
            this.grb_atividade.Size = new System.Drawing.Size(759, 375);
            this.grb_atividade.TabIndex = 19;
            this.grb_atividade.TabStop = false;
            this.grb_atividade.Text = "Atividades";
            // 
            // grd_atividadeCronograma
            // 
            this.grd_atividadeCronograma.AllowUserToAddRows = false;
            this.grd_atividadeCronograma.AllowUserToDeleteRows = false;
            this.grd_atividadeCronograma.AllowUserToOrderColumns = true;
            this.grd_atividadeCronograma.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.DisplayedCells;
            this.grd_atividadeCronograma.BackgroundColor = System.Drawing.Color.White;
            this.grd_atividadeCronograma.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.grd_atividadeCronograma.Dock = System.Windows.Forms.DockStyle.Fill;
            this.grd_atividadeCronograma.Location = new System.Drawing.Point(3, 16);
            this.grd_atividadeCronograma.Name = "grd_atividadeCronograma";
            this.grd_atividadeCronograma.ReadOnly = true;
            this.grd_atividadeCronograma.Size = new System.Drawing.Size(753, 356);
            this.grd_atividadeCronograma.TabIndex = 16;
            // 
            // grb_comentario
            // 
            this.grb_comentario.Controls.Add(this.btn_gravarComentario);
            this.grb_comentario.Controls.Add(this.text_comentario);
            this.grb_comentario.Controls.Add(this.lbl_comentario);
            this.grb_comentario.Location = new System.Drawing.Point(4, 22);
            this.grb_comentario.Name = "grb_comentario";
            this.grb_comentario.Padding = new System.Windows.Forms.Padding(3);
            this.grb_comentario.Size = new System.Drawing.Size(773, 429);
            this.grb_comentario.TabIndex = 5;
            this.grb_comentario.Text = "Comentário";
            this.grb_comentario.UseVisualStyleBackColor = true;
            // 
            // btn_gravarComentario
            // 
            this.btn_gravarComentario.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btn_gravarComentario.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_gravarComentario.Image = global::SWS.Properties.Resources.Gravar;
            this.btn_gravarComentario.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btn_gravarComentario.Location = new System.Drawing.Point(9, 400);
            this.btn_gravarComentario.Name = "btn_gravarComentario";
            this.btn_gravarComentario.Size = new System.Drawing.Size(77, 23);
            this.btn_gravarComentario.TabIndex = 2;
            this.btn_gravarComentario.Text = "&Gravar";
            this.btn_gravarComentario.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btn_gravarComentario.UseVisualStyleBackColor = true;
            this.btn_gravarComentario.Click += new System.EventHandler(this.btn_gravarComentario_Click);
            // 
            // text_comentario
            // 
            this.text_comentario.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.text_comentario.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.text_comentario.ForeColor = System.Drawing.Color.Blue;
            this.text_comentario.Location = new System.Drawing.Point(9, 19);
            this.text_comentario.MaxLength = 500;
            this.text_comentario.Multiline = true;
            this.text_comentario.Name = "text_comentario";
            this.text_comentario.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.text_comentario.Size = new System.Drawing.Size(748, 375);
            this.text_comentario.TabIndex = 1;
            // 
            // lbl_comentario
            // 
            this.lbl_comentario.AutoSize = true;
            this.lbl_comentario.Location = new System.Drawing.Point(6, 3);
            this.lbl_comentario.Name = "lbl_comentario";
            this.lbl_comentario.Size = new System.Drawing.Size(34, 13);
            this.lbl_comentario.TabIndex = 0;
            this.lbl_comentario.Text = "Texto";
            // 
            // tb_anexo
            // 
            this.tb_anexo.Controls.Add(this.btn_excluirAnexo);
            this.tb_anexo.Controls.Add(this.lblDescricaoAnexo);
            this.tb_anexo.Controls.Add(this.grb_anexo);
            this.tb_anexo.Controls.Add(this.text_conteudo);
            this.tb_anexo.Controls.Add(this.btn_incluirAnexo);
            this.tb_anexo.Location = new System.Drawing.Point(4, 22);
            this.tb_anexo.Name = "tb_anexo";
            this.tb_anexo.Padding = new System.Windows.Forms.Padding(3);
            this.tb_anexo.Size = new System.Drawing.Size(773, 429);
            this.tb_anexo.TabIndex = 6;
            this.tb_anexo.Text = "Anexo";
            this.tb_anexo.UseVisualStyleBackColor = true;
            // 
            // btn_excluirAnexo
            // 
            this.btn_excluirAnexo.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btn_excluirAnexo.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_excluirAnexo.Image = global::SWS.Properties.Resources.lixeira;
            this.btn_excluirAnexo.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btn_excluirAnexo.Location = new System.Drawing.Point(6, 390);
            this.btn_excluirAnexo.Name = "btn_excluirAnexo";
            this.btn_excluirAnexo.Size = new System.Drawing.Size(77, 23);
            this.btn_excluirAnexo.TabIndex = 4;
            this.btn_excluirAnexo.Text = "Excluir";
            this.btn_excluirAnexo.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btn_excluirAnexo.UseVisualStyleBackColor = true;
            this.btn_excluirAnexo.Click += new System.EventHandler(this.btn_excluirAnexo_Click);
            // 
            // lblDescricaoAnexo
            // 
            this.lblDescricaoAnexo.AutoSize = true;
            this.lblDescricaoAnexo.Location = new System.Drawing.Point(6, 4);
            this.lblDescricaoAnexo.Name = "lblDescricaoAnexo";
            this.lblDescricaoAnexo.Size = new System.Drawing.Size(55, 13);
            this.lblDescricaoAnexo.TabIndex = 3;
            this.lblDescricaoAnexo.Text = "Descrição";
            // 
            // grb_anexo
            // 
            this.grb_anexo.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.grb_anexo.Controls.Add(this.grd_anexo);
            this.grb_anexo.Enabled = false;
            this.grb_anexo.Location = new System.Drawing.Point(3, 59);
            this.grb_anexo.Name = "grb_anexo";
            this.grb_anexo.Size = new System.Drawing.Size(760, 325);
            this.grb_anexo.TabIndex = 1;
            this.grb_anexo.TabStop = false;
            this.grb_anexo.Text = "Anexos";
            // 
            // grd_anexo
            // 
            this.grd_anexo.AllowUserToAddRows = false;
            this.grd_anexo.AllowUserToDeleteRows = false;
            this.grd_anexo.AllowUserToOrderColumns = true;
            this.grd_anexo.AllowUserToResizeRows = false;
            this.grd_anexo.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.DisplayedCells;
            this.grd_anexo.BackgroundColor = System.Drawing.Color.White;
            this.grd_anexo.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.grd_anexo.Dock = System.Windows.Forms.DockStyle.Fill;
            this.grd_anexo.Location = new System.Drawing.Point(3, 16);
            this.grd_anexo.Name = "grd_anexo";
            this.grd_anexo.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.grd_anexo.Size = new System.Drawing.Size(754, 306);
            this.grd_anexo.TabIndex = 3;
            // 
            // text_conteudo
            // 
            this.text_conteudo.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.text_conteudo.Location = new System.Drawing.Point(6, 21);
            this.text_conteudo.MaxLength = 200;
            this.text_conteudo.Multiline = true;
            this.text_conteudo.Name = "text_conteudo";
            this.text_conteudo.Size = new System.Drawing.Size(657, 23);
            this.text_conteudo.TabIndex = 1;
            // 
            // btn_incluirAnexo
            // 
            this.btn_incluirAnexo.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_incluirAnexo.Image = global::SWS.Properties.Resources.fechar_ico;
            this.btn_incluirAnexo.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btn_incluirAnexo.Location = new System.Drawing.Point(669, 21);
            this.btn_incluirAnexo.Name = "btn_incluirAnexo";
            this.btn_incluirAnexo.Size = new System.Drawing.Size(77, 23);
            this.btn_incluirAnexo.TabIndex = 2;
            this.btn_incluirAnexo.Text = "Incluir";
            this.btn_incluirAnexo.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btn_incluirAnexo.UseVisualStyleBackColor = true;
            this.btn_incluirAnexo.Click += new System.EventHandler(this.btn_incluirAnexo_Click);
            // 
            // pd_pcmsoExame
            // 
            this.pd_pcmsoExame.Controls.Add(this.btn_exame_alterar);
            this.pd_pcmsoExame.Controls.Add(this.grb_periodicidade);
            this.pd_pcmsoExame.Controls.Add(this.grb_exames);
            this.pd_pcmsoExame.Controls.Add(this.btn_excluir);
            this.pd_pcmsoExame.Controls.Add(this.grb_risco);
            this.pd_pcmsoExame.Controls.Add(this.btn_incluir);
            this.pd_pcmsoExame.Controls.Add(this.grb_funcoesExame);
            this.pd_pcmsoExame.Controls.Add(this.grb_gheExame);
            this.pd_pcmsoExame.Location = new System.Drawing.Point(4, 22);
            this.pd_pcmsoExame.Name = "pd_pcmsoExame";
            this.pd_pcmsoExame.Padding = new System.Windows.Forms.Padding(3);
            this.pd_pcmsoExame.Size = new System.Drawing.Size(773, 429);
            this.pd_pcmsoExame.TabIndex = 7;
            this.pd_pcmsoExame.Text = "Exames";
            this.pd_pcmsoExame.UseVisualStyleBackColor = true;
            // 
            // btn_exame_alterar
            // 
            this.btn_exame_alterar.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btn_exame_alterar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_exame_alterar.Image = global::SWS.Properties.Resources.Alterar;
            this.btn_exame_alterar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btn_exame_alterar.Location = new System.Drawing.Point(94, 399);
            this.btn_exame_alterar.Name = "btn_exame_alterar";
            this.btn_exame_alterar.Size = new System.Drawing.Size(77, 23);
            this.btn_exame_alterar.TabIndex = 7;
            this.btn_exame_alterar.Text = "&Alterar";
            this.btn_exame_alterar.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btn_exame_alterar.UseVisualStyleBackColor = true;
            this.btn_exame_alterar.Click += new System.EventHandler(this.btn_exame_alterar_Click);
            // 
            // grb_periodicidade
            // 
            this.grb_periodicidade.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.grb_periodicidade.Controls.Add(this.grd_periodicidade);
            this.grb_periodicidade.Location = new System.Drawing.Point(403, 276);
            this.grb_periodicidade.Name = "grb_periodicidade";
            this.grb_periodicidade.Size = new System.Drawing.Size(362, 120);
            this.grb_periodicidade.TabIndex = 9;
            this.grb_periodicidade.TabStop = false;
            this.grb_periodicidade.Text = "Periodicidade";
            // 
            // grd_periodicidade
            // 
            this.grd_periodicidade.AllowUserToAddRows = false;
            this.grd_periodicidade.AllowUserToDeleteRows = false;
            this.grd_periodicidade.AllowUserToResizeRows = false;
            this.grd_periodicidade.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this.grd_periodicidade.BackgroundColor = System.Drawing.Color.White;
            this.grd_periodicidade.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.grd_periodicidade.Dock = System.Windows.Forms.DockStyle.Fill;
            this.grd_periodicidade.Location = new System.Drawing.Point(3, 16);
            this.grd_periodicidade.MultiSelect = false;
            this.grd_periodicidade.Name = "grd_periodicidade";
            this.grd_periodicidade.ReadOnly = true;
            this.grd_periodicidade.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.grd_periodicidade.Size = new System.Drawing.Size(356, 101);
            this.grd_periodicidade.TabIndex = 7;
            // 
            // grb_exames
            // 
            this.grb_exames.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.grb_exames.Controls.Add(this.grd_exame);
            this.grb_exames.Location = new System.Drawing.Point(8, 276);
            this.grb_exames.Name = "grb_exames";
            this.grb_exames.Size = new System.Drawing.Size(390, 120);
            this.grb_exames.TabIndex = 8;
            this.grb_exames.TabStop = false;
            this.grb_exames.Text = "Exames";
            // 
            // grd_exame
            // 
            this.grd_exame.AllowUserToAddRows = false;
            this.grd_exame.AllowUserToDeleteRows = false;
            this.grd_exame.AllowUserToOrderColumns = true;
            this.grd_exame.AllowUserToResizeRows = false;
            this.grd_exame.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.DisplayedCells;
            this.grd_exame.BackgroundColor = System.Drawing.Color.GhostWhite;
            this.grd_exame.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.grd_exame.Dock = System.Windows.Forms.DockStyle.Fill;
            this.grd_exame.Location = new System.Drawing.Point(3, 16);
            this.grd_exame.Name = "grd_exame";
            this.grd_exame.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.CellSelect;
            this.grd_exame.Size = new System.Drawing.Size(384, 101);
            this.grd_exame.TabIndex = 4;
            this.grd_exame.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.grd_exame_CellClick);
            this.grd_exame.CellEndEdit += new System.Windows.Forms.DataGridViewCellEventHandler(this.grd_exame_CellEndEdit);
            this.grd_exame.EditingControlShowing += new System.Windows.Forms.DataGridViewEditingControlShowingEventHandler(this.grd_exame_EditingControlShowing);
            this.grd_exame.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.grd_exame_KeyPress);
            // 
            // btn_excluir
            // 
            this.btn_excluir.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btn_excluir.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_excluir.Image = global::SWS.Properties.Resources.lixeira;
            this.btn_excluir.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btn_excluir.Location = new System.Drawing.Point(177, 399);
            this.btn_excluir.Name = "btn_excluir";
            this.btn_excluir.Size = new System.Drawing.Size(77, 23);
            this.btn_excluir.TabIndex = 6;
            this.btn_excluir.Text = "&Excluir";
            this.btn_excluir.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btn_excluir.UseVisualStyleBackColor = true;
            this.btn_excluir.Click += new System.EventHandler(this.btn_excluir_Click);
            // 
            // grb_risco
            // 
            this.grb_risco.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.grb_risco.Controls.Add(this.grd_risco);
            this.grb_risco.Location = new System.Drawing.Point(8, 144);
            this.grb_risco.Name = "grb_risco";
            this.grb_risco.Size = new System.Drawing.Size(758, 132);
            this.grb_risco.TabIndex = 7;
            this.grb_risco.TabStop = false;
            this.grb_risco.Text = "Riscos";
            // 
            // grd_risco
            // 
            this.grd_risco.AllowUserToAddRows = false;
            this.grd_risco.AllowUserToDeleteRows = false;
            this.grd_risco.AllowUserToOrderColumns = true;
            this.grd_risco.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this.grd_risco.BackgroundColor = System.Drawing.Color.GhostWhite;
            this.grd_risco.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.grd_risco.Dock = System.Windows.Forms.DockStyle.Fill;
            this.grd_risco.Location = new System.Drawing.Point(3, 16);
            this.grd_risco.MultiSelect = false;
            this.grd_risco.Name = "grd_risco";
            this.grd_risco.ReadOnly = true;
            this.grd_risco.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.grd_risco.Size = new System.Drawing.Size(752, 113);
            this.grd_risco.TabIndex = 3;
            this.grd_risco.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.grd_risco_CellClick);
            // 
            // btn_incluir
            // 
            this.btn_incluir.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btn_incluir.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_incluir.Image = global::SWS.Properties.Resources.fechar_ico;
            this.btn_incluir.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btn_incluir.Location = new System.Drawing.Point(11, 399);
            this.btn_incluir.Name = "btn_incluir";
            this.btn_incluir.Size = new System.Drawing.Size(77, 23);
            this.btn_incluir.TabIndex = 5;
            this.btn_incluir.Text = "&Incluir";
            this.btn_incluir.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btn_incluir.UseVisualStyleBackColor = true;
            this.btn_incluir.Click += new System.EventHandler(this.btn_incluir_Click);
            // 
            // grb_funcoesExame
            // 
            this.grb_funcoesExame.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.grb_funcoesExame.Controls.Add(this.grd_funcao);
            this.grb_funcoesExame.Location = new System.Drawing.Point(394, 6);
            this.grb_funcoesExame.Name = "grb_funcoesExame";
            this.grb_funcoesExame.Size = new System.Drawing.Size(371, 132);
            this.grb_funcoesExame.TabIndex = 6;
            this.grb_funcoesExame.TabStop = false;
            this.grb_funcoesExame.Text = "Funções";
            // 
            // grd_funcao
            // 
            this.grd_funcao.AllowUserToAddRows = false;
            this.grd_funcao.AllowUserToDeleteRows = false;
            this.grd_funcao.AllowUserToOrderColumns = true;
            this.grd_funcao.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this.grd_funcao.BackgroundColor = System.Drawing.Color.White;
            this.grd_funcao.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dataGridViewCellStyle13.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle13.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle13.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle13.ForeColor = System.Drawing.Color.Blue;
            dataGridViewCellStyle13.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle13.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle13.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.grd_funcao.DefaultCellStyle = dataGridViewCellStyle13;
            this.grd_funcao.Dock = System.Windows.Forms.DockStyle.Fill;
            this.grd_funcao.Location = new System.Drawing.Point(3, 16);
            this.grd_funcao.MultiSelect = false;
            this.grd_funcao.Name = "grd_funcao";
            this.grd_funcao.ReadOnly = true;
            this.grd_funcao.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.grd_funcao.Size = new System.Drawing.Size(365, 113);
            this.grd_funcao.TabIndex = 0;
            this.grd_funcao.TabStop = false;
            // 
            // grb_gheExame
            // 
            this.grb_gheExame.Controls.Add(this.grd_gheExame);
            this.grb_gheExame.Location = new System.Drawing.Point(7, 6);
            this.grb_gheExame.Name = "grb_gheExame";
            this.grb_gheExame.Size = new System.Drawing.Size(381, 132);
            this.grb_gheExame.TabIndex = 5;
            this.grb_gheExame.TabStop = false;
            this.grb_gheExame.Text = "GHE";
            // 
            // grd_gheExame
            // 
            this.grd_gheExame.AllowUserToAddRows = false;
            this.grd_gheExame.AllowUserToDeleteRows = false;
            this.grd_gheExame.AllowUserToOrderColumns = true;
            this.grd_gheExame.AllowUserToResizeRows = false;
            this.grd_gheExame.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this.grd_gheExame.BackgroundColor = System.Drawing.Color.White;
            dataGridViewCellStyle14.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle14.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle14.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle14.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle14.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle14.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle14.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.grd_gheExame.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle14;
            this.grd_gheExame.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.grd_gheExame.Dock = System.Windows.Forms.DockStyle.Fill;
            this.grd_gheExame.Location = new System.Drawing.Point(3, 16);
            this.grd_gheExame.MultiSelect = false;
            this.grd_gheExame.Name = "grd_gheExame";
            this.grd_gheExame.ReadOnly = true;
            this.grd_gheExame.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.grd_gheExame.Size = new System.Drawing.Size(375, 113);
            this.grd_gheExame.TabIndex = 2;
            this.grd_gheExame.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.grd_ghe_CellClick);
            // 
            // tb_hospitais
            // 
            this.tb_hospitais.Controls.Add(this.btn_excluirMedico);
            this.tb_hospitais.Controls.Add(this.btn_excluirHospital);
            this.tb_hospitais.Controls.Add(this.btn_incluirMedico);
            this.tb_hospitais.Controls.Add(this.grb_medico);
            this.tb_hospitais.Controls.Add(this.btn_incluirHospital);
            this.tb_hospitais.Controls.Add(this.grb_hospital);
            this.tb_hospitais.Location = new System.Drawing.Point(4, 22);
            this.tb_hospitais.Name = "tb_hospitais";
            this.tb_hospitais.Padding = new System.Windows.Forms.Padding(3);
            this.tb_hospitais.Size = new System.Drawing.Size(773, 429);
            this.tb_hospitais.TabIndex = 8;
            this.tb_hospitais.Text = "Médicos e Hospitais";
            this.tb_hospitais.UseVisualStyleBackColor = true;
            // 
            // btn_excluirMedico
            // 
            this.btn_excluirMedico.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btn_excluirMedico.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_excluirMedico.Image = ((System.Drawing.Image)(resources.GetObject("btn_excluirMedico.Image")));
            this.btn_excluirMedico.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btn_excluirMedico.Location = new System.Drawing.Point(91, 394);
            this.btn_excluirMedico.Name = "btn_excluirMedico";
            this.btn_excluirMedico.Size = new System.Drawing.Size(77, 23);
            this.btn_excluirMedico.TabIndex = 6;
            this.btn_excluirMedico.Text = "&Excluir";
            this.btn_excluirMedico.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btn_excluirMedico.UseVisualStyleBackColor = true;
            this.btn_excluirMedico.Click += new System.EventHandler(this.btn_excluirMedico_Click);
            // 
            // btn_excluirHospital
            // 
            this.btn_excluirHospital.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_excluirHospital.Image = ((System.Drawing.Image)(resources.GetObject("btn_excluirHospital.Image")));
            this.btn_excluirHospital.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btn_excluirHospital.Location = new System.Drawing.Point(91, 193);
            this.btn_excluirHospital.Name = "btn_excluirHospital";
            this.btn_excluirHospital.Size = new System.Drawing.Size(77, 23);
            this.btn_excluirHospital.TabIndex = 3;
            this.btn_excluirHospital.Text = "&Excluir";
            this.btn_excluirHospital.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btn_excluirHospital.UseVisualStyleBackColor = true;
            this.btn_excluirHospital.Click += new System.EventHandler(this.btn_excluirHospital_Click);
            // 
            // btn_incluirMedico
            // 
            this.btn_incluirMedico.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btn_incluirMedico.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_incluirMedico.Image = ((System.Drawing.Image)(resources.GetObject("btn_incluirMedico.Image")));
            this.btn_incluirMedico.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btn_incluirMedico.Location = new System.Drawing.Point(7, 394);
            this.btn_incluirMedico.Name = "btn_incluirMedico";
            this.btn_incluirMedico.Size = new System.Drawing.Size(77, 23);
            this.btn_incluirMedico.TabIndex = 5;
            this.btn_incluirMedico.Text = "&Incluir";
            this.btn_incluirMedico.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btn_incluirMedico.UseVisualStyleBackColor = true;
            this.btn_incluirMedico.Click += new System.EventHandler(this.btn_incluirMedico_Click);
            // 
            // grb_medico
            // 
            this.grb_medico.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.grb_medico.Controls.Add(this.grd_medico);
            this.grb_medico.Location = new System.Drawing.Point(7, 222);
            this.grb_medico.Name = "grb_medico";
            this.grb_medico.Size = new System.Drawing.Size(760, 166);
            this.grb_medico.TabIndex = 3;
            this.grb_medico.TabStop = false;
            this.grb_medico.Text = "Médicos";
            // 
            // grd_medico
            // 
            this.grd_medico.AllowUserToAddRows = false;
            this.grd_medico.AllowUserToDeleteRows = false;
            this.grd_medico.AllowUserToOrderColumns = true;
            this.grd_medico.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.DisplayedCells;
            this.grd_medico.BackgroundColor = System.Drawing.Color.White;
            this.grd_medico.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.grd_medico.Dock = System.Windows.Forms.DockStyle.Fill;
            this.grd_medico.Location = new System.Drawing.Point(3, 16);
            this.grd_medico.MultiSelect = false;
            this.grd_medico.Name = "grd_medico";
            this.grd_medico.ReadOnly = true;
            this.grd_medico.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.grd_medico.Size = new System.Drawing.Size(754, 147);
            this.grd_medico.TabIndex = 4;
            // 
            // btn_incluirHospital
            // 
            this.btn_incluirHospital.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_incluirHospital.Image = ((System.Drawing.Image)(resources.GetObject("btn_incluirHospital.Image")));
            this.btn_incluirHospital.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btn_incluirHospital.Location = new System.Drawing.Point(7, 193);
            this.btn_incluirHospital.Name = "btn_incluirHospital";
            this.btn_incluirHospital.Size = new System.Drawing.Size(77, 23);
            this.btn_incluirHospital.TabIndex = 2;
            this.btn_incluirHospital.Text = "&Incluir";
            this.btn_incluirHospital.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btn_incluirHospital.UseVisualStyleBackColor = true;
            this.btn_incluirHospital.Click += new System.EventHandler(this.btn_incluirHospital_Click);
            // 
            // grb_hospital
            // 
            this.grb_hospital.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.grb_hospital.Controls.Add(this.grd_hospital);
            this.grb_hospital.Location = new System.Drawing.Point(7, 6);
            this.grb_hospital.Name = "grb_hospital";
            this.grb_hospital.Size = new System.Drawing.Size(760, 181);
            this.grb_hospital.TabIndex = 2;
            this.grb_hospital.TabStop = false;
            this.grb_hospital.Text = "Hospitais";
            // 
            // grd_hospital
            // 
            this.grd_hospital.AllowUserToAddRows = false;
            this.grd_hospital.AllowUserToDeleteRows = false;
            this.grd_hospital.AllowUserToOrderColumns = true;
            this.grd_hospital.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.DisplayedCells;
            this.grd_hospital.BackgroundColor = System.Drawing.Color.White;
            this.grd_hospital.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.grd_hospital.Dock = System.Windows.Forms.DockStyle.Fill;
            this.grd_hospital.Location = new System.Drawing.Point(3, 16);
            this.grd_hospital.MultiSelect = false;
            this.grd_hospital.Name = "grd_hospital";
            this.grd_hospital.ReadOnly = true;
            this.grd_hospital.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.grd_hospital.Size = new System.Drawing.Size(754, 162);
            this.grd_hospital.TabIndex = 1;
            // 
            // tb_equipamento
            // 
            this.tb_equipamento.Controls.Add(this.btn_excluirMaterial);
            this.tb_equipamento.Controls.Add(this.grb_material_salvar);
            this.tb_equipamento.Controls.Add(this.btn_incluirMaterial);
            this.tb_equipamento.Controls.Add(this.grb_material);
            this.tb_equipamento.Location = new System.Drawing.Point(4, 22);
            this.tb_equipamento.Name = "tb_equipamento";
            this.tb_equipamento.Padding = new System.Windows.Forms.Padding(3);
            this.tb_equipamento.Size = new System.Drawing.Size(773, 429);
            this.tb_equipamento.TabIndex = 9;
            this.tb_equipamento.Text = "Materiais Hospitalares";
            this.tb_equipamento.UseVisualStyleBackColor = true;
            // 
            // btn_excluirMaterial
            // 
            this.btn_excluirMaterial.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btn_excluirMaterial.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_excluirMaterial.Image = global::SWS.Properties.Resources.lixeira;
            this.btn_excluirMaterial.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btn_excluirMaterial.Location = new System.Drawing.Point(93, 325);
            this.btn_excluirMaterial.Name = "btn_excluirMaterial";
            this.btn_excluirMaterial.Size = new System.Drawing.Size(77, 23);
            this.btn_excluirMaterial.TabIndex = 2;
            this.btn_excluirMaterial.Text = "&Excluir";
            this.btn_excluirMaterial.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btn_excluirMaterial.UseVisualStyleBackColor = true;
            this.btn_excluirMaterial.Click += new System.EventHandler(this.btn_excluirMaterial_Click);
            // 
            // grb_material_salvar
            // 
            this.grb_material_salvar.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.grb_material_salvar.Controls.Add(this.btn_salvarMaterial);
            this.grb_material_salvar.Location = new System.Drawing.Point(7, 354);
            this.grb_material_salvar.Name = "grb_material_salvar";
            this.grb_material_salvar.Size = new System.Drawing.Size(760, 50);
            this.grb_material_salvar.TabIndex = 6;
            this.grb_material_salvar.TabStop = false;
            // 
            // btn_salvarMaterial
            // 
            this.btn_salvarMaterial.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btn_salvarMaterial.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_salvarMaterial.Image = global::SWS.Properties.Resources.Gravar;
            this.btn_salvarMaterial.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btn_salvarMaterial.Location = new System.Drawing.Point(5, 19);
            this.btn_salvarMaterial.Name = "btn_salvarMaterial";
            this.btn_salvarMaterial.Size = new System.Drawing.Size(77, 23);
            this.btn_salvarMaterial.TabIndex = 4;
            this.btn_salvarMaterial.Text = "&Salvar";
            this.btn_salvarMaterial.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btn_salvarMaterial.UseVisualStyleBackColor = true;
            this.btn_salvarMaterial.Click += new System.EventHandler(this.btn_salvarMaterial_Click);
            // 
            // btn_incluirMaterial
            // 
            this.btn_incluirMaterial.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btn_incluirMaterial.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_incluirMaterial.Image = global::SWS.Properties.Resources.icone_mais;
            this.btn_incluirMaterial.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btn_incluirMaterial.Location = new System.Drawing.Point(10, 325);
            this.btn_incluirMaterial.Name = "btn_incluirMaterial";
            this.btn_incluirMaterial.Size = new System.Drawing.Size(77, 23);
            this.btn_incluirMaterial.TabIndex = 1;
            this.btn_incluirMaterial.Text = "&Incluir";
            this.btn_incluirMaterial.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btn_incluirMaterial.UseVisualStyleBackColor = true;
            this.btn_incluirMaterial.Click += new System.EventHandler(this.btn_incluirMaterial_Click);
            // 
            // grb_material
            // 
            this.grb_material.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.grb_material.Controls.Add(this.grd_material);
            this.grb_material.Location = new System.Drawing.Point(6, 6);
            this.grb_material.Name = "grb_material";
            this.grb_material.Size = new System.Drawing.Size(761, 313);
            this.grb_material.TabIndex = 5;
            this.grb_material.TabStop = false;
            this.grb_material.Text = "Materiais Hospitalares";
            // 
            // grd_material
            // 
            this.grd_material.AllowUserToAddRows = false;
            this.grd_material.AllowUserToDeleteRows = false;
            this.grd_material.AllowUserToOrderColumns = true;
            this.grd_material.AllowUserToResizeRows = false;
            this.grd_material.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this.grd_material.BackgroundColor = System.Drawing.Color.White;
            this.grd_material.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.grd_material.Dock = System.Windows.Forms.DockStyle.Fill;
            this.grd_material.Location = new System.Drawing.Point(3, 16);
            this.grd_material.MultiSelect = false;
            this.grd_material.Name = "grd_material";
            this.grd_material.Size = new System.Drawing.Size(755, 294);
            this.grd_material.TabIndex = 3;
            this.grd_material.EditingControlShowing += new System.Windows.Forms.DataGridViewEditingControlShowingEventHandler(this.grd_material_EditingControlShowing);
            this.grd_material.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.grd_material_KeyPress);
            // 
            // btn_fechar
            // 
            this.btn_fechar.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btn_fechar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_fechar.Image = global::SWS.Properties.Resources.close;
            this.btn_fechar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btn_fechar.Location = new System.Drawing.Point(13, 19);
            this.btn_fechar.Name = "btn_fechar";
            this.btn_fechar.Size = new System.Drawing.Size(77, 23);
            this.btn_fechar.TabIndex = 23;
            this.btn_fechar.Text = "&Fechar";
            this.btn_fechar.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btn_fechar.UseVisualStyleBackColor = true;
            this.btn_fechar.Click += new System.EventHandler(this.btn_fechar_Click);
            // 
            // grb_paginacao
            // 
            this.grb_paginacao.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.grb_paginacao.BackColor = System.Drawing.Color.White;
            this.grb_paginacao.Controls.Add(this.btn_fechar);
            this.grb_paginacao.Location = new System.Drawing.Point(2, 460);
            this.grb_paginacao.Name = "grb_paginacao";
            this.grb_paginacao.Size = new System.Drawing.Size(782, 51);
            this.grb_paginacao.TabIndex = 24;
            this.grb_paginacao.TabStop = false;
            // 
            // pcmsoIncluirErrorProvider
            // 
            this.pcmsoIncluirErrorProvider.ContainerControl = this;
            // 
            // frm_PcmsoSemPpraIncluir
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(794, 568);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = true;
            this.Name = "frm_PcmsoSemPpraIncluir";
            this.Text = "MANTER PCMSO";
            this.Load += new System.EventHandler(this.PcmsoSemPpraIncluir_Load);
            this.panel_banner.ResumeLayout(false);
            this.panel_banner.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox_banner)).EndInit();
            this.panel.ResumeLayout(false);
            this.tb_dados.ResumeLayout(false);
            this.tb_dadosPPRA.ResumeLayout(false);
            this.tpConfiguracaoExtra.ResumeLayout(false);
            this.tbDados.ResumeLayout(false);
            this.tbDados.PerformLayout();
            this.tbPrevisao.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgPrevisao)).EndInit();
            this.grb_dados.ResumeLayout(false);
            this.grb_dados.PerformLayout();
            this.grb_contratante_contrato.ResumeLayout(false);
            this.grb_contratante_contrato.PerformLayout();
            this.grb_cliente.ResumeLayout(false);
            this.grb_cliente.PerformLayout();
            this.grb_tecnico.ResumeLayout(false);
            this.grb_tecnico.PerformLayout();
            this.grb_grauRisco.ResumeLayout(false);
            this.grb_data.ResumeLayout(false);
            this.grb_data.PerformLayout();
            this.tb_ghe.ResumeLayout(false);
            this.grb_ghePPRA.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.grd_ghe)).EndInit();
            this.tb_gheFonte.ResumeLayout(false);
            this.tb_fonteGHE.ResumeLayout(false);
            this.Fonte.ResumeLayout(false);
            this.grb_gheFonte.ResumeLayout(false);
            this.grb_fonte.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.grd_fonte)).EndInit();
            this.agente.ResumeLayout(false);
            this.grb_gheAgente.ResumeLayout(false);
            this.grb_fonteAgente.ResumeLayout(false);
            this.grb_agente.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.grd_agente)).EndInit();
            this.tb_setorGHE.ResumeLayout(false);
            this.tb_gheSetor.ResumeLayout(false);
            this.tb_setor.ResumeLayout(false);
            this.grb_setor.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.grd_setor)).EndInit();
            this.grb_gheSetor.ResumeLayout(false);
            this.tb_funcao.ResumeLayout(false);
            this.grb_funcao.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.grd_funcaoCliente)).EndInit();
            this.tb_funcaoPPRA.ResumeLayout(false);
            this.grb_funcaoCliente.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.grd_setorFuncao)).EndInit();
            this.grb_setorFuncao.ResumeLayout(false);
            this.grb_gheFuncao.ResumeLayout(false);
            this.tb_cronograma.ResumeLayout(false);
            this.grb_atividade.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.grd_atividadeCronograma)).EndInit();
            this.grb_comentario.ResumeLayout(false);
            this.grb_comentario.PerformLayout();
            this.tb_anexo.ResumeLayout(false);
            this.tb_anexo.PerformLayout();
            this.grb_anexo.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.grd_anexo)).EndInit();
            this.pd_pcmsoExame.ResumeLayout(false);
            this.grb_periodicidade.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.grd_periodicidade)).EndInit();
            this.grb_exames.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.grd_exame)).EndInit();
            this.grb_risco.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.grd_risco)).EndInit();
            this.grb_funcoesExame.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.grd_funcao)).EndInit();
            this.grb_gheExame.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.grd_gheExame)).EndInit();
            this.tb_hospitais.ResumeLayout(false);
            this.grb_medico.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.grd_medico)).EndInit();
            this.grb_hospital.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.grd_hospital)).EndInit();
            this.tb_equipamento.ResumeLayout(false);
            this.grb_material_salvar.ResumeLayout(false);
            this.grb_material.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.grd_material)).EndInit();
            this.grb_paginacao.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pcmsoIncluirErrorProvider)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        protected System.Windows.Forms.TabControl tb_dados;
        protected System.Windows.Forms.TabPage tb_dadosPPRA;
        protected System.Windows.Forms.ComboBox cb_uf;
        protected System.Windows.Forms.MaskedTextBox text_cep;
        protected System.Windows.Forms.Label lbl_cep;
        protected System.Windows.Forms.TextBox text_localObra;
        protected System.Windows.Forms.Label lbl_localObra;
        protected System.Windows.Forms.TextBox text_cidade;
        protected System.Windows.Forms.Label lbl_cidade;
        protected System.Windows.Forms.TextBox text_bairro;
        protected System.Windows.Forms.Label lbl_bairro;
        protected System.Windows.Forms.TextBox text_complemento;
        protected System.Windows.Forms.Label lbl_complemento;
        protected System.Windows.Forms.TextBox text_numero;
        protected System.Windows.Forms.Label lbl_numero;
        protected System.Windows.Forms.TextBox text_endereco;
        protected System.Windows.Forms.Label lbl_endereco;
        protected System.Windows.Forms.TextBox text_obra;
        protected System.Windows.Forms.Label lbl_obra;
        protected System.Windows.Forms.GroupBox grb_dados;
        public System.Windows.Forms.GroupBox grb_contratante_contrato;
        public System.Windows.Forms.TextBox text_fimContr;
        protected System.Windows.Forms.Label lbl_inicioContr;
        public System.Windows.Forms.TextBox text_inicioContr;
        protected System.Windows.Forms.Label lbl_contrato;
        public System.Windows.Forms.TextBox text_numContrato;
        protected System.Windows.Forms.Label lbl_fimContrato;
        public System.Windows.Forms.TextBox text_clienteContratante;
        public System.Windows.Forms.Button btn_contratante;
        protected System.Windows.Forms.GroupBox grb_cliente;
        public System.Windows.Forms.TextBox text_cliente;
        protected System.Windows.Forms.Label lbl_cliente;
        public System.Windows.Forms.Button btn_cliente;
        protected System.Windows.Forms.GroupBox grb_tecnico;
        protected System.Windows.Forms.Label lbl_tecno;
        protected System.Windows.Forms.Button btn_tecnico;
        public System.Windows.Forms.TextBox text_tecnico;
        protected System.Windows.Forms.Label lbl_vencimento;
        protected System.Windows.Forms.GroupBox grb_grauRisco;
        protected System.Windows.Forms.ComboBox cb_grauRisco;
        protected System.Windows.Forms.GroupBox grb_data;
        protected System.Windows.Forms.Label lbl_dataVencimento;
        protected System.Windows.Forms.Label lbl_dataCriacao;
        protected System.Windows.Forms.DateTimePicker dt_criacao;
        protected System.Windows.Forms.DateTimePicker dt_vencimento;
        protected System.Windows.Forms.MaskedTextBox text_codPpra;
        protected System.Windows.Forms.Label lbl_codPpra;
        protected System.Windows.Forms.Button btn_salvar;
        protected System.Windows.Forms.Button btn_limpar;
        protected System.Windows.Forms.TabPage tb_ghe;
        protected System.Windows.Forms.GroupBox grb_ghePPRA;
        protected System.Windows.Forms.Button btn_alterarGhe;
        public System.Windows.Forms.DataGridView grd_ghe;
        protected System.Windows.Forms.Button btn_excluirGHE;
        protected System.Windows.Forms.Button btn_incluirGHE;
        protected System.Windows.Forms.TabPage tb_gheFonte;
        protected System.Windows.Forms.TabControl tb_fonteGHE;
        protected System.Windows.Forms.TabPage Fonte;
        protected System.Windows.Forms.GroupBox grb_gheFonte;
        public System.Windows.Forms.ComboBox cb_ghe;
        protected System.Windows.Forms.GroupBox grb_fonte;
        protected System.Windows.Forms.DataGridView grd_fonte;
        protected System.Windows.Forms.Button btn_excluirFonte;
        protected System.Windows.Forms.Button btn_incluirFonte;
        protected System.Windows.Forms.TabPage agente;
        protected System.Windows.Forms.GroupBox grb_gheAgente;
        public System.Windows.Forms.ComboBox cb_gheAgente;
        protected System.Windows.Forms.GroupBox grb_fonteAgente;
        protected System.Windows.Forms.ComboBox cb_fonteAgente;
        protected System.Windows.Forms.GroupBox grb_agente;
        protected System.Windows.Forms.Button btn_excluirAgente;
        protected System.Windows.Forms.Button btn_incluirAgente;
        protected System.Windows.Forms.DataGridView grd_agente;
        protected System.Windows.Forms.TabPage tb_setorGHE;
        protected System.Windows.Forms.TabControl tb_gheSetor;
        protected System.Windows.Forms.TabPage tb_setor;
        protected System.Windows.Forms.GroupBox grb_setor;
        protected System.Windows.Forms.DataGridView grd_setor;
        protected System.Windows.Forms.Button btn_excluirSetor;
        protected System.Windows.Forms.Button btn_incluirSetor;
        protected System.Windows.Forms.GroupBox grb_gheSetor;
        public System.Windows.Forms.ComboBox cb_gheSetor;
        protected System.Windows.Forms.TabPage tb_funcao;
        protected System.Windows.Forms.GroupBox grb_funcao;
        protected System.Windows.Forms.Button btn_incluirFuncaoClientePPRA;
        protected System.Windows.Forms.Button btn_excluirFuncaoClientePPRA;
        protected System.Windows.Forms.DataGridView grd_funcaoCliente;
        protected System.Windows.Forms.TabPage tb_funcaoPPRA;
        protected System.Windows.Forms.GroupBox grb_funcaoCliente;
        protected System.Windows.Forms.Button btn_excluirFuncaoCliente;
        protected System.Windows.Forms.Button btn_incluirFuncaoCliente;
        public System.Windows.Forms.DataGridView grd_setorFuncao;
        protected System.Windows.Forms.GroupBox grb_setorFuncao;
        public System.Windows.Forms.ComboBox cb_setorFuncao;
        protected System.Windows.Forms.GroupBox grb_gheFuncao;
        public System.Windows.Forms.ComboBox cb_gheFuncao;
        protected System.Windows.Forms.TabPage tb_cronograma;
        protected System.Windows.Forms.Button btn_excluirAtividade;
        protected System.Windows.Forms.Button btn_alterarAtividade;
        protected System.Windows.Forms.Button btn_incluirAtividade;
        protected System.Windows.Forms.GroupBox grb_atividade;
        public System.Windows.Forms.DataGridView grd_atividadeCronograma;
        protected System.Windows.Forms.TabPage grb_comentario;
        private System.Windows.Forms.Button btn_gravarComentario;
        protected System.Windows.Forms.TextBox text_comentario;
        protected System.Windows.Forms.Label lbl_comentario;
        private System.Windows.Forms.TabPage tb_anexo;
        protected System.Windows.Forms.GroupBox grb_anexo;
        protected System.Windows.Forms.Button btn_excluirAnexo;
        protected System.Windows.Forms.DataGridView grd_anexo;
        protected System.Windows.Forms.Button btn_incluirAnexo;
        protected System.Windows.Forms.TextBox text_conteudo;
        protected System.Windows.Forms.GroupBox grb_paginacao;
        protected System.Windows.Forms.Button btn_fechar;
        private System.Windows.Forms.ErrorProvider pcmsoIncluirErrorProvider;
        private System.Windows.Forms.TabPage pd_pcmsoExame;
        private System.Windows.Forms.GroupBox grb_periodicidade;
        private System.Windows.Forms.DataGridView grd_periodicidade;
        private System.Windows.Forms.GroupBox grb_exames;
        private System.Windows.Forms.Button btn_excluir;
        private System.Windows.Forms.Button btn_incluir;
        private System.Windows.Forms.DataGridView grd_exame;
        private System.Windows.Forms.GroupBox grb_risco;
        private System.Windows.Forms.DataGridView grd_risco;
        private System.Windows.Forms.GroupBox grb_funcoesExame;
        private System.Windows.Forms.DataGridView grd_funcao;
        private System.Windows.Forms.GroupBox grb_gheExame;
        private System.Windows.Forms.DataGridView grd_gheExame;
        private System.Windows.Forms.TabPage tb_hospitais;
        private System.Windows.Forms.TabPage tb_equipamento;
        private System.Windows.Forms.GroupBox grb_medico;
        private System.Windows.Forms.Button btn_excluirMedico;
        private System.Windows.Forms.Button btn_incluirMedico;
        protected System.Windows.Forms.DataGridView grd_medico;
        private System.Windows.Forms.GroupBox grb_hospital;
        private System.Windows.Forms.Button btn_excluirHospital;
        private System.Windows.Forms.Button btn_incluirHospital;
        protected System.Windows.Forms.DataGridView grd_hospital;
        private System.Windows.Forms.GroupBox grb_material_salvar;
        private System.Windows.Forms.Button btn_salvarMaterial;
        private System.Windows.Forms.GroupBox grb_material;
        private System.Windows.Forms.Button btn_excluirMaterial;
        private System.Windows.Forms.Button btn_incluirMaterial;
        private System.Windows.Forms.DataGridView grd_material;
        private System.Windows.Forms.Button btn_exame_alterar;
        private System.Windows.Forms.Label lblDescricaoAnexo;
        private System.Windows.Forms.TabControl tpConfiguracaoExtra;
        private System.Windows.Forms.TabPage tbDados;
        private System.Windows.Forms.TabPage tbPrevisao;
        protected System.Windows.Forms.Label lblUf;
        protected System.Windows.Forms.Button btnExcluirPrevisao;
        protected System.Windows.Forms.Button btnIncluirPrevisao;
        protected System.Windows.Forms.DataGridView dgPrevisao;
    }
}