﻿namespace SegSis.View
{
    partial class frm_FuncionarioAlterarClienteFuncao
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frm_FuncionarioAlterarClienteFuncao));
            this.grb_paginacao = new System.Windows.Forms.GroupBox();
            this.btn_novo = new System.Windows.Forms.Button();
            this.btn_fechar = new System.Windows.Forms.Button();
            this.btn_ok = new System.Windows.Forms.Button();
            this.grb_funcao = new System.Windows.Forms.GroupBox();
            this.grd_funcao = new System.Windows.Forms.DataGridView();
            this.grb_filtro = new System.Windows.Forms.GroupBox();
            this.btn_pesquisar = new System.Windows.Forms.Button();
            this.text_cbo = new System.Windows.Forms.TextBox();
            this.lbl_cbo = new System.Windows.Forms.Label();
            this.lbl_descricao = new System.Windows.Forms.Label();
            this.text_descricao = new System.Windows.Forms.TextBox();
            this.grb_paginacao.SuspendLayout();
            this.grb_funcao.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grd_funcao)).BeginInit();
            this.grb_filtro.SuspendLayout();
            this.SuspendLayout();
            // 
            // grb_paginacao
            // 
            this.grb_paginacao.Controls.Add(this.btn_novo);
            this.grb_paginacao.Controls.Add(this.btn_fechar);
            this.grb_paginacao.Controls.Add(this.btn_ok);
            this.grb_paginacao.Location = new System.Drawing.Point(13, 343);
            this.grb_paginacao.Name = "grb_paginacao";
            this.grb_paginacao.Size = new System.Drawing.Size(575, 49);
            this.grb_paginacao.TabIndex = 8;
            this.grb_paginacao.TabStop = false;
            // 
            // btn_novo
            // 
            this.btn_novo.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_novo.Image = global::SegSis.Properties.Resources.icone_mais;
            this.btn_novo.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btn_novo.Location = new System.Drawing.Point(90, 19);
            this.btn_novo.Name = "btn_novo";
            this.btn_novo.Size = new System.Drawing.Size(75, 23);
            this.btn_novo.TabIndex = 6;
            this.btn_novo.Text = "&Novo";
            this.btn_novo.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btn_novo.UseVisualStyleBackColor = true;
            this.btn_novo.Click += new System.EventHandler(this.btn_novo_Click);
            // 
            // btn_fechar
            // 
            this.btn_fechar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_fechar.Image = global::SegSis.Properties.Resources.close;
            this.btn_fechar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btn_fechar.Location = new System.Drawing.Point(171, 19);
            this.btn_fechar.Name = "btn_fechar";
            this.btn_fechar.Size = new System.Drawing.Size(75, 23);
            this.btn_fechar.TabIndex = 7;
            this.btn_fechar.Text = "&Fechar";
            this.btn_fechar.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btn_fechar.UseVisualStyleBackColor = false;
            this.btn_fechar.Click += new System.EventHandler(this.btn_fechar_Click);
            // 
            // btn_ok
            // 
            this.btn_ok.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_ok.Image = global::SegSis.Properties.Resources.fechar_ico;
            this.btn_ok.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btn_ok.Location = new System.Drawing.Point(9, 19);
            this.btn_ok.Name = "btn_ok";
            this.btn_ok.Size = new System.Drawing.Size(75, 23);
            this.btn_ok.TabIndex = 5;
            this.btn_ok.Text = "&OK";
            this.btn_ok.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btn_ok.UseVisualStyleBackColor = true;
            this.btn_ok.Click += new System.EventHandler(this.btn_ok_Click);
            // 
            // grb_funcao
            // 
            this.grb_funcao.Controls.Add(this.grd_funcao);
            this.grb_funcao.Location = new System.Drawing.Point(12, 142);
            this.grb_funcao.Name = "grb_funcao";
            this.grb_funcao.Size = new System.Drawing.Size(576, 178);
            this.grb_funcao.TabIndex = 7;
            this.grb_funcao.TabStop = false;
            this.grb_funcao.Text = "Funções";
            // 
            // grd_funcao
            // 
            this.grd_funcao.AllowUserToAddRows = false;
            this.grd_funcao.AllowUserToDeleteRows = false;
            this.grd_funcao.AllowUserToOrderColumns = true;
            this.grd_funcao.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this.grd_funcao.BackgroundColor = System.Drawing.Color.White;
            this.grd_funcao.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.grd_funcao.Location = new System.Drawing.Point(10, 20);
            this.grd_funcao.MultiSelect = false;
            this.grd_funcao.Name = "grd_funcao";
            this.grd_funcao.ReadOnly = true;
            this.grd_funcao.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.grd_funcao.Size = new System.Drawing.Size(560, 135);
            this.grd_funcao.TabIndex = 4;
            // 
            // grb_filtro
            // 
            this.grb_filtro.Controls.Add(this.btn_pesquisar);
            this.grb_filtro.Controls.Add(this.text_cbo);
            this.grb_filtro.Controls.Add(this.lbl_cbo);
            this.grb_filtro.Controls.Add(this.lbl_descricao);
            this.grb_filtro.Controls.Add(this.text_descricao);
            this.grb_filtro.Location = new System.Drawing.Point(12, 9);
            this.grb_filtro.Name = "grb_filtro";
            this.grb_filtro.Size = new System.Drawing.Size(575, 127);
            this.grb_filtro.TabIndex = 6;
            this.grb_filtro.TabStop = false;
            this.grb_filtro.Text = "Filtro";
            // 
            // btn_pesquisar
            // 
            this.btn_pesquisar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_pesquisar.Image = global::SegSis.Properties.Resources.lupa;
            this.btn_pesquisar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btn_pesquisar.Location = new System.Drawing.Point(10, 97);
            this.btn_pesquisar.Name = "btn_pesquisar";
            this.btn_pesquisar.Size = new System.Drawing.Size(75, 23);
            this.btn_pesquisar.TabIndex = 3;
            this.btn_pesquisar.Text = "Pesquisar";
            this.btn_pesquisar.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btn_pesquisar.UseVisualStyleBackColor = true;
            this.btn_pesquisar.Click += new System.EventHandler(this.btn_pesquisar_Click);
            // 
            // text_cbo
            // 
            this.text_cbo.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.text_cbo.Location = new System.Drawing.Point(10, 71);
            this.text_cbo.MaxLength = 6;
            this.text_cbo.Name = "text_cbo";
            this.text_cbo.Size = new System.Drawing.Size(100, 20);
            this.text_cbo.TabIndex = 2;
            this.text_cbo.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.text_cbo_KeyPress);
            // 
            // lbl_cbo
            // 
            this.lbl_cbo.AutoSize = true;
            this.lbl_cbo.Location = new System.Drawing.Point(7, 55);
            this.lbl_cbo.Name = "lbl_cbo";
            this.lbl_cbo.Size = new System.Drawing.Size(29, 13);
            this.lbl_cbo.TabIndex = 3;
            this.lbl_cbo.Text = "CBO";
            // 
            // lbl_descricao
            // 
            this.lbl_descricao.AutoSize = true;
            this.lbl_descricao.Location = new System.Drawing.Point(7, 16);
            this.lbl_descricao.Name = "lbl_descricao";
            this.lbl_descricao.Size = new System.Drawing.Size(55, 13);
            this.lbl_descricao.TabIndex = 2;
            this.lbl_descricao.Text = "Descrição";
            // 
            // text_descricao
            // 
            this.text_descricao.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.text_descricao.Location = new System.Drawing.Point(10, 32);
            this.text_descricao.MaxLength = 100;
            this.text_descricao.Name = "text_descricao";
            this.text_descricao.Size = new System.Drawing.Size(559, 20);
            this.text_descricao.TabIndex = 1;
            // 
            // frm_FuncionarioAlterarClienteFuncao
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(600, 400);
            this.ControlBox = false;
            this.Controls.Add(this.grb_paginacao);
            this.Controls.Add(this.grb_funcao);
            this.Controls.Add(this.grb_filtro);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "frm_FuncionarioAlterarClienteFuncao";
            this.Text = "INCLUIR FUNÇÃO";
            this.grb_paginacao.ResumeLayout(false);
            this.grb_funcao.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.grd_funcao)).EndInit();
            this.grb_filtro.ResumeLayout(false);
            this.grb_filtro.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox grb_paginacao;
        private System.Windows.Forms.Button btn_fechar;
        private System.Windows.Forms.Button btn_ok;
        private System.Windows.Forms.GroupBox grb_funcao;
        private System.Windows.Forms.DataGridView grd_funcao;
        private System.Windows.Forms.GroupBox grb_filtro;
        private System.Windows.Forms.Button btn_pesquisar;
        private System.Windows.Forms.TextBox text_cbo;
        private System.Windows.Forms.Label lbl_cbo;
        private System.Windows.Forms.Label lbl_descricao;
        private System.Windows.Forms.TextBox text_descricao;
        private System.Windows.Forms.Button btn_novo;
    }
}