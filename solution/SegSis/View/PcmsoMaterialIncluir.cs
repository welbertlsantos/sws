﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using SWS.Facade;
using SWS.Excecao;
using SWS.Entidade;


namespace SWS.View
{
    public partial class frm_pcmoMaterialIncluir : BaseFormConsulta
    {
        public List<MaterialHospitalar> listaMaterialHospitalar = new List<MaterialHospitalar>() { };

        frm_PcmsoExames formPcmsoExame;
        frm_PcmsoSemPpraIncluir formPcmsoSemPpraIncluir;

        MaterialHospitalar materialHospitalar = null;

        public frm_pcmoMaterialIncluir(frm_PcmsoExames formPcmsoExame)
        {
            InitializeComponent();
            this.formPcmsoExame = formPcmsoExame;
            validaPermissoes();
        }

        public frm_pcmoMaterialIncluir(frm_PcmsoSemPpraIncluir formPcmsoSemPpraIncluir)
        {
            InitializeComponent();
            this.formPcmsoSemPpraIncluir = formPcmsoSemPpraIncluir;
            validaPermissoes();
        }

        private void validaPermissoes()
        {
            PermissionamentoFacade permissionamentoFacade = PermissionamentoFacade.getInstance();

            this.btn_novo.Enabled = permissionamentoFacade.hasPermission("MATERIAL_HOSPITALAR", "INCLUIR");
        }

        private void btn_buscar_Click(object sender, EventArgs e)
        {
            try
            {
                MontaDataGrid();
                text_descricao.Focus();

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }

        }

        private void btn_incluir_Click(object sender, EventArgs e)
        {
            try
            {
                PcmsoFacade pcmsoFacade = PcmsoFacade.getInstance();

                MaterialHospitalarEstudo materialHospitalarEstudo;

                foreach (DataGridViewRow dvRom in grd_material.Rows)
                {

                    if (dvRom.Cells[4].Value != null)
                    {
                        
                        materialHospitalar = new MaterialHospitalar((Int64)dvRom.Cells[0].Value,(string)dvRom.Cells[1].Value,
                            (string)dvRom.Cells[2].Value, (string)dvRom.Cells[3].Value);

                        if (formPcmsoExame != null)
                        {
                            materialHospitalarEstudo = new MaterialHospitalarEstudo(formPcmsoExame.pcmso, materialHospitalar, (Int32)0);
                        }
                        else
                        {
                            materialHospitalarEstudo = new MaterialHospitalarEstudo(formPcmsoSemPpraIncluir.pcmso, materialHospitalar, (Int32)0);
                        }

                        pcmsoFacade.inserirMaterialHospitalarEstudo(materialHospitalarEstudo);

                    }

                }

                if (formPcmsoExame != null)
                {
                    formPcmsoExame.MontaDataGridMaterialHospitalar();
                }
                else
                {
                    formPcmsoSemPpraIncluir.MontaDataGridMaterialHospitalar();
                }

                this.Close();


            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }

        }

        private void btn_novo_Click(object sender, EventArgs e)
        {
            frmMaterialHospitalarIncluir incluirMaterial = new frmMaterialHospitalarIncluir();
            incluirMaterial.ShowDialog();

            if (incluirMaterial.MateriaHospitalar != null)
            {
                materialHospitalar = incluirMaterial.MateriaHospitalar;
                text_descricao.Text = materialHospitalar.Descricao;
                MontaDataGrid();
            }

        }

        private void btn_fechar_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        public void MontaDataGrid()
        {
            try
            {
                PcmsoFacade pcmsoFacade = PcmsoFacade.getInstance();
                
                grd_material.Columns.Clear();

                // montando dados do objeto material hospitalar

                MaterialHospitalar materialHospitalarProcurado = new MaterialHospitalar(null, text_descricao.Text, null, null);

                if (formPcmsoExame != null)
                {
                    listaMaterialHospitalar = pcmsoFacade.findAllMaterialHospitalarAtivoByPcmso(materialHospitalarProcurado, this.formPcmsoExame.pcmso);
                }
                else
                {
                    listaMaterialHospitalar = pcmsoFacade.findAllMaterialHospitalarAtivoByPcmso(materialHospitalarProcurado, this.formPcmsoSemPpraIncluir.pcmso);
                }

                // montando  grid usando a lista.

                grd_material.ColumnCount = 4;

                grd_material.Columns[0].Name = "IdMaterialHospitalar";
                grd_material.Columns[0].Visible = false;

                grd_material.Columns[1].Name = "Descrição";
                grd_material.Columns[1].ReadOnly = true;

                grd_material.Columns[2].Name = "Unidade";
                grd_material.Columns[2].ReadOnly = true;

                grd_material.Columns[3].Name = "Situação";
                grd_material.Columns[3].ReadOnly = true;
                grd_material.Columns[3].Visible = false;


                DataGridViewCheckBoxColumn chk = new DataGridViewCheckBoxColumn();
                grd_material.Columns.Add(chk);
                chk.HeaderText = "Selec";
                chk.Name = "controleCheck";
                
                // reordenando a lista para o checkbox vir como primeiro item

                for (int i = 0; i < 4; i++)
                {
                    grd_material.Columns[i].DisplayIndex = i + 1;
                }

                grd_material.Columns[4].DisplayIndex = 0;


                // interagindo na lista para montar a grid.

                foreach (MaterialHospitalar materialHospitalar in listaMaterialHospitalar)
                {

                    this.grd_material.Rows.Add(materialHospitalar.Id, materialHospitalar.Descricao, 
                            materialHospitalar.Unidade, materialHospitalar.Situacao);
 
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }

        }

    }
}
