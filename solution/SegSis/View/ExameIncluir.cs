﻿using SWS.Entidade;
using SWS.Facade;
using SWS.View.Resources;
using SWS.View.ViewHelper;
using SWS.ViewHelper;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace SWS.View
{
    public partial class frmExameIncluir : frmTemplate
    {
        protected Exame exame;

        public Exame Exame
        {
            get { return exame; }
            set { exame = value; }
        }

        private List<MedicoExame> medicoExame = new List<MedicoExame>();

        protected List<MedicoExame> MedicoExame
        {
            get { return medicoExame; }
            set { medicoExame = value; }
        }

        private RelatorioExame relatorioExameSelecionado;

        protected RelatorioExame RelatorioExameSelecionado
        {
            get { return relatorioExameSelecionado; }
            set { relatorioExameSelecionado = value; }
        }

        protected Relatorio relatorio;
        
        public frmExameIncluir()
        {
            InitializeComponent();
            ComboHelper.Boolean(cbLaboratorio, false);
            ComboHelper.Boolean(cbExterno, false);
            ComboHelper.Boolean(cbDocumento, false);
            ComboHelper.Boolean(cbExameComplementar, false);
            ComboHelper.periodoVencimentoCombo(cbPeriodoVencimento);
            ComboHelper.Boolean(cbPadraoContrato, false);
            ActiveControl = text_descricao;
        }

        protected void text_precoCusto_Enter(object sender, EventArgs e)
        {
            text_precoCusto.Tag = text_precoCusto.Text;
            text_precoCusto.Text = string.Empty;
        }

        protected void text_precoCusto_Leave(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(text_precoCusto.Text))
                text_precoCusto.Text = text_precoCusto.Tag.ToString();

            text_precoCusto.Text = ValidaCampoHelper.FormataValorMonetario(text_precoCusto.Text);
        }

        protected void text_precoCusto_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = ValidaCampoHelper.FormataDigitacaoNumero(sender, e, text_precoCusto.Text, 2);
        }

        protected void text_preco_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = ValidaCampoHelper.FormataDigitacaoNumero(sender, e, text_preco.Text, 2);
        }

        protected void text_preco_Leave(object sender, EventArgs e)
        {
            if (String.IsNullOrEmpty(text_preco.Text))
                text_preco.Text = text_preco.Tag.ToString();

            text_preco.Text = ValidaCampoHelper.FormataValorMonetario(text_preco.Text);
        }

        protected void text_preco_Enter(object sender, EventArgs e)
        {
            text_preco.Tag = text_preco.Text;
            text_preco.Text = string.Empty;
        }

        protected void text_prioridade_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = ValidaCampoHelper.FormataCampoNumerico(sender, e);
        }

        protected virtual void btn_gravar_Click(object sender, EventArgs e)
        {
            try
            {
                if (validaCamposObrigatorio())
                {
                    EstudoFacade pcmsoFacade = EstudoFacade.getInstance();
                    
                    exame = new Exame(null, text_descricao.Text.Trim(), string.Empty, Convert.ToBoolean(((SelectItem)cbLaboratorio.SelectedItem).Valor), Convert.ToDecimal(text_preco.Text), Convert.ToInt32(text_prioridade.Text), Convert.ToDecimal(text_precoCusto.Text), Convert.ToBoolean(((SelectItem)cbExterno.SelectedItem).Valor), Convert.ToBoolean(((SelectItem)cbDocumento.SelectedItem).Valor), textCodigoTuss.Text, Convert.ToBoolean(((SelectItem)cbExameComplementar.SelectedItem).Valor), Convert.ToInt32(((SelectItem)cbPeriodoVencimento.SelectedItem).Valor), Convert.ToBoolean(((SelectItem)cbPadraoContrato.SelectedItem).Valor));
                    exame.MedicoExame = MedicoExame;
                    exame.RelatorioExame = relatorio != null ? new RelatorioExame(null, exame, relatorio, ApplicationConstants.ATIVO, DateTime.Now) : null;
                    pcmsoFacade.insertExame(exame);

                    MessageBox.Show("Exame incluído com sucesso.", "Confirmação", MessageBoxButtons.OK, MessageBoxIcon.Information);

                    this.Close();
                }
            }

            catch (Exception Ex)
            {
                MessageBox.Show(Ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        protected void btn_limpar_Click(object sender, EventArgs e)
        {
            text_descricao.Text = string.Empty;
            text_descricao.Focus();
            text_preco.Text = "0,00";
            text_precoCusto.Text = "0,00";
            text_prioridade.Text = "0";
            ComboHelper.Boolean(cbLaboratorio, false);
            ComboHelper.Boolean(cbExterno, false);
            ComboHelper.Boolean(cbDocumento, false);
            textCodigoTuss.Text = string.Empty;
            ComboHelper.Boolean(cbExameComplementar, false);
        }

        protected void btn_fechar_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        protected bool validaCamposObrigatorio()
        {
            bool retorno = true;
            try
            {
                if (string.IsNullOrEmpty(text_descricao.Text.Trim()))
                    throw new Exception("A Descrição é obrigatória!");

                if (string.IsNullOrEmpty(text_prioridade.Text))
                    throw new Exception("A Prioridade é obrigatória!");
            }
            catch (Exception ex)
            {
                retorno = false;
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

            return retorno;
        }

        private void frmExameIncluir_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter || e.KeyCode == Keys.Return)
                SendKeys.Send("{TAB}");
        }

        protected void MontaGridMedicoExame()
        {
            try
            {
                this.dgvMedico.Columns.Clear();
                this.dgvMedico.ColumnCount = 5;

                this.dgvMedico.Columns[0].HeaderText = "id";
                this.dgvMedico.Columns[0].Visible = false;

                this.dgvMedico.Columns[1].HeaderText = "id_medico";
                this.dgvMedico.Columns[1].Visible = false;

                this.dgvMedico.Columns[2].HeaderText = "Médico";

                this.dgvMedico.Columns[3].HeaderText = "CRM";

                this.dgvMedico.Columns[4].HeaderText = "UF-CRM";

                this.MedicoExame.ForEach(delegate(MedicoExame medicoExame)
                {
                    this.dgvMedico.Rows.Add(null, (long)medicoExame.Medico.Id, medicoExame.Medico.Nome, medicoExame.Medico.Crm, medicoExame.Medico.UfCrm);
                });

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        
        }

        protected virtual void btIncluirMedico_Click(object sender, EventArgs e)
        {
            try
            {
                frmMedicoBuscar medicoBuscar = new frmMedicoBuscar();
                medicoBuscar.ShowDialog();

                if (medicoBuscar.Medico != null)
                {
                    /* verificando se o médico já foi inserido na coleção */
                    MedicoExame medicoExameBusca = MedicoExame.Find(x => x.Medico.Id == medicoBuscar.Medico.Id);

                    if (medicoExameBusca != null)
                        throw new Exception("Médico já inserido na coleção.");

                    MedicoExame.Add(new MedicoExame(null, null, medicoBuscar.Medico, null));
                    MontaGridMedicoExame();
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }

        protected virtual void btExcluirMedico_Click(object sender, EventArgs e)
        {
            try
            {
                MedicoExame medicoExameExcluir = MedicoExame.Find(x => x.Medico.Id == (long)dgvMedico.CurrentRow.Cells[1].Value);
                MedicoExame.Remove(medicoExameExcluir);
                dgvMedico.Rows.RemoveAt((int)dgvMedico.CurrentRow.Index);

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }

        protected void textCodigoTuss_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = ValidaCampoHelper.FormataDigitacaoNumero(sender, e, textCodigoTuss.Text, 2);
        }

        protected virtual void btnRelatorio_Click(object sender, EventArgs e)
        {
            try
            {
                frmExameRelatorio selecionarRelatorio = new frmExameRelatorio();
                selecionarRelatorio.ShowDialog();

                if (selecionarRelatorio.RelatorioSelecionado != null)
                {
                    relatorio = selecionarRelatorio.RelatorioSelecionado;
                    textRelatorio.Text = relatorio.NmRelatorio;
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }


        }

        private void btnProcedimentoEsocial_Click(object sender, EventArgs e)
        {
            frmProcedimentoEsocialBuscar buscarProcedimento = new frmProcedimentoEsocialBuscar();
            buscarProcedimento.ShowDialog();

            if (buscarProcedimento.ProcedimentoEsocial != null) 
            {
                textCodigoTuss.Text = buscarProcedimento.ProcedimentoEsocial.Codigo;
            }
        }

    }
}
