﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using SegSis.Facade;
using SegSis.Entidade;
using SegSis.Excecao;
using SegSis.View.ViewHelper;
using System.Text.RegularExpressions;
using SegSis.View.Resources;

namespace SegSis.View
{
    public partial class frm_clienteVendedorSelecionar_alterar : BaseFormConsulta
    {
        
        Vendedor vendedor = null;

        public Vendedor getVendedor()
        {
            return this.vendedor;
        }
        
        private static String Msg01 = "Seleciona uma linha";

        public frm_clienteVendedorSelecionar_alterar()
        {
            InitializeComponent();
            validaPermissoes();
        }

        private void validaPermissoes()
        {
            PermissionamentoFacade permissionamentoFacade = PermissionamentoFacade.getInstance();

            btn_incluir.Enabled = permissionamentoFacade.hasPermission("VENDEDOR", "INCLUIR");
        }

        private void btn_buscar_Click(object sender, EventArgs e)
        {
            try
            {
                this.grd_vendedor.Columns.Clear();
                montaDataGrid();
                text_nome.Focus();
            }
            catch (Exception Ex)
            {
                MessageBox.Show(Ex.Message);
            }
        }

        private void btn_incluir_Click(object sender, EventArgs e)
        {
            try
            {
                this.Cursor = Cursors.WaitCursor;

                frm_vendedor_incluir formVendedorIncluir = new frm_vendedor_incluir();
                formVendedorIncluir.ShowDialog();

                if (formVendedorIncluir.getVendedor() != null)
                {
                    vendedor = formVendedorIncluir.getVendedor();
                    text_nome.Text = vendedor.getNome();
                    btn_ok.PerformClick();

                }
                
                this.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }

        private void btn_ok_Click(object sender, EventArgs e)
        {
            try
            {
                if (grd_vendedor.CurrentRow != null)
                {

                    VendedorFacade vendedorFacade = VendedorFacade.getInstance();
                    vendedor = vendedorFacade.findVendedorById((Int64)grd_vendedor.CurrentRow.Cells[0].Value);
                    
                    this.Close();

                }
                else
                {
                    MessageBox.Show(Msg01);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void btn_fechar_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        public void montaDataGrid()
        {
            try
            {
                VendedorFacade vendedorFacade = VendedorFacade.getInstance();

                DataSet ds = vendedorFacade.findVendedorByName(new Vendedor(null, text_nome.Text));

                grd_vendedor.DataSource = ds.Tables["Vendedores"].DefaultView;

                grd_vendedor.Columns[0].HeaderText = "ID";
                grd_vendedor.Columns[0].Visible = false;

                grd_vendedor.Columns[1].HeaderText = "Nome";

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }

        }
    }
}
