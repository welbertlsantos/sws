﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using SWS.Facade;
using SWS.Entidade;
using SWS.Excecao;
using SWS.View.ViewHelper;
using System.Text.RegularExpressions;
using SWS.View.Resources;

namespace SWS.View
{
    public partial class frm_ppraUsuarioTecnicoIncluir : BaseFormConsulta
    {

        private static String Msg01 = " Selecione um linha";
        private static string Msg02 = " Atenção";

        Usuario usuario = null;
        public Usuario getUsuario()
        {
            return this.usuario;
        }
        
        public frm_ppraUsuarioTecnicoIncluir()
        {
            InitializeComponent();
            validaPermissoes();
        }

        private void validaPermissoes()
        {
            PermissionamentoFacade permissionamentoFacade = PermissionamentoFacade.getInstance();
            this.btn_novo.Enabled = permissionamentoFacade.hasPermission("USUARIO", "INCLUIR");
        }

        private void btn_buscar_Click(object sender, EventArgs e)
        {
            try
            {
                this.grd_usuario.Columns.Clear();
                this.Cursor = Cursors.WaitCursor;
                montaDataGrid();
                text_nome.Focus();
            }
            catch (Exception Ex)
            {
                MessageBox.Show(Ex.Message);
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }

        public void montaDataGrid()
        {
            UsuarioFacade usuarioFacade = UsuarioFacade.getInstance();

            DataSet ds = usuarioFacade.findUsuarioElaboraDocumento(new Usuario(null, text_nome.Text.Trim().ToUpper(), string.Empty, String.Empty, String.Empty, null, String.Empty, String.Empty, String.Empty, String.Empty, null, null,String.Empty, false, null, String.Empty, false, false, string.Empty, string.Empty));

            grd_usuario.DataSource = ds.Tables["Usuarios"].DefaultView;
            
            grd_usuario.Columns[0].HeaderText = "ID";
            grd_usuario.Columns[0].Visible = false;

            grd_usuario.Columns[1].HeaderText = "Nome do Usuário";

        }

        private void btn_fechar_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btn_ok_Click(object sender, EventArgs e)
        {
            UsuarioFacade usuarioFacede = UsuarioFacade.getInstance();

            try
            {
                if (grd_usuario.CurrentRow != null)
                {

                    Int64 id = (Int64)grd_usuario.CurrentRow.Cells[0].Value; 
                    usuario = usuarioFacede.findUsuarioPpraById(id);
                    this.Close();
                }
                else
                {
                    MessageBox.Show(Msg01, Msg02);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
        }

        private void btn_novo_Click(object sender, EventArgs e)
        {
            frmUsuarioIncluir formUsuarioIncluir = new frmUsuarioIncluir();
            formUsuarioIncluir.ShowDialog();

            if (formUsuarioIncluir.Usuario != null)
            {
                text_nome.Text = formUsuarioIncluir.Usuario.Nome.ToUpper();
                btn_buscar.PerformClick();
            }
        }
    }
}
