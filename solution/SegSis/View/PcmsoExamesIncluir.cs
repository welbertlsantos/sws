﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using SWS.Entidade;
using SWS.Facade;
using SWS.View.ViewHelper;
using SWS.View.Resources;
using SWS.ViewHelper;

namespace SWS.View
{
    public partial class frmPcmsoExamesIncluir : frmTemplateConsulta
    {
        protected static String msg1 = "Selecione um Exame.";
        protected static String msg2 = "Não é possível gravar um Exame no PCMSO sem uma Periodicidade.";
        protected static String msg3 = "Selecione uma Função.";
        protected static String msg5 = "O campo idade não pode ser vazio. Para não limitar a idade para o exame use 0.";

        protected GheFonteAgenteExame gheFonteAgenteExame;

        public GheFonteAgenteExame GheFonteAgenteExame
        {
            get { return gheFonteAgenteExame; }
            set { gheFonteAgenteExame = value; }
        }

        protected Dictionary<Int64, Dictionary<Funcao, Int32>> excecaoIdadeFuncao = new Dictionary<Int64, Dictionary<Funcao, Int32>>();

        protected GheFonteAgente gheFonteAgente;

        protected Exame exame;

        public frmPcmsoExamesIncluir(GheFonteAgente gheFonteAgente) :base()
        {
            InitializeComponent();
            this.gheFonteAgente = gheFonteAgente;

            /* preenchendo informações na tela */
            textGhe.Text = gheFonteAgente.GheFonte.Ghe.Descricao;
            ComboHelper.Boolean(cbAltura, false);
            ComboHelper.Boolean(cbEspaco, false);
            ComboHelper.Boolean(cbEletricidade, false);
            ActiveControl = btExame;
          
        }

        public frmPcmsoExamesIncluir() :base()
        {
            InitializeComponent();
        }

        protected virtual void btIncluir_Click(object sender, EventArgs e)
        {
            try
            {
                EstudoFacade pcmsoFacade = EstudoFacade.getInstance();
                this.Cursor = Cursors.WaitCursor;

                if (validaDadosTela())
                {
                    gheFonteAgenteExame = pcmsoFacade.insertGheFonteAgenteExame(new GheFonteAgenteExame(null, exame, gheFonteAgente, Convert.ToInt32(textIdade.Text), String.Empty, ((SelectItem)cbEspaco.SelectedItem).Valor == "true" ? true : false, ((SelectItem)cbAltura.SelectedItem).Valor == "true" ? true : false, ((SelectItem)cbEletricidade.SelectedItem).Valor == "true" ? true: false));

                    foreach (DataGridViewRow dvRow in dgvPeriodicidade.Rows)
                    {
                        if ((Boolean)dvRow.Cells[2].Value == true)
                            pcmsoFacade.insertGheFonteAgenteExamePeriodicidade(new GheFonteAgenteExamePeriodicidade(new Periodicidade((Int64)dvRow.Cells[0].Value, dvRow.Cells[1].Value.ToString(), null), gheFonteAgenteExame, String.Empty, dvRow.Cells[3].Value == string.Empty ? (int?)null : Convert.ToInt32(dvRow.Cells[3].Value)));
                    }

                    foreach (DataGridViewRow dvRow in dgvExcecao.Rows)
                    {
                        foreach (GheSetorClienteFuncao gheSetorClienteFuncao in pcmsoFacade.findGheSetorClienteFuncaoByGheAndFuncao(gheFonteAgente.GheFonte.Ghe, new Funcao((Int64)dvRow.Cells[0].Value, dvRow.Cells[1].Value.ToString(), String.Empty, String.Empty, String.Empty)))
                        {
                            pcmsoFacade.insertGheSetorClienteFuncaoExame(new GheSetorClienteFuncaoExame(gheSetorClienteFuncao, exame, dvRow.Cells[2].Value == null ? 0 : Convert.ToInt32(dvRow.Cells[2].Value), gheFonteAgenteExame));
                        }
                    }

                    this.Close();
                }

                //Significa uma alteração
                /*
                else
                {
                    //Alterar a idade geral do exame no ghe
                    gheFonteAgenteExameAlterado.setIdadeExame(Convert.ToInt32(text_idadeExame.Text));
                    estudoFacade.updateGheFonteAgenteExame(gheFonteAgenteExameAlterado);

                    //Limpar a listagem de exceções de idade por função e criar com a seleção.
                    estudoFacade.deleteGheSetorClienteFuncaoExame(ghe, exame);

                    //Gravando a nova lista de exceções
                    Funcao funcao = null;
                    //Incluindo exceções de idade para o exame por função
                    foreach (DataGridViewRow dvRow in grd_excecao_exames.Rows)
                    {
                        funcao = new Funcao();
                        funcao.setIdFuncao((Int64)dvRow.Cells[0].Value);
                        funcao.setDescricao((String)dvRow.Cells[1].Value);

                        GheSetorClienteFuncaoExame gheSetorClienteFuncaoExame = null;

                        //Buscando gheSetorClienteFuncao aplicados a função independente do setor
                        foreach (GheSetorClienteFuncao gheSetorClienteFuncao in estudoFacade.findGheSetorClienteFuncaoByGheAndFuncao(ghe, funcao))
                        {
                            gheSetorClienteFuncaoExame = new GheSetorClienteFuncaoExame(gheSetorClienteFuncao, exame, (Int32)dvRow.Cells[2].Value, gheFonteAgenteExameAlterado);

                            estudoFacade.incluiGheSetorClienteFuncaoExame(gheSetorClienteFuncaoExame);
                        }
                    }

                    this.Close();
                }
                 * */
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }

        protected void btExame_Click(object sender, EventArgs e)
        {
            try
            {
                frmExameBuscar buscarExame = new frmExameBuscar();
                buscarExame.ShowDialog();

                if (buscarExame.Exame != null)
                {
                    exame = buscarExame.Exame;
                    textExame.Text = exame.Descricao;
                    montaGridPeriodicidade();
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        protected void btFechar_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        protected void btIncluirExcecao_Click(object sender, EventArgs e)
        {
            try
            {
                if (exame == null)
                    throw new Exception(msg1);

                frmPcmsoFuncoesGhe funcaoGhe = new frmPcmsoFuncoesGhe(gheFonteAgente.GheFonte.Ghe, exame);
                funcaoGhe.ShowDialog();

                if (funcaoGhe.ExcecaoIdadeFuncao.Count > 0)
                {
                    excecaoIdadeFuncao = funcaoGhe.ExcecaoIdadeFuncao;
                    montaGridExcecaoExamesPorFuncao();
                }
                
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }

        protected void btExcluirExcecao_Click(object sender, EventArgs e)
        {
            try
            {
                EstudoFacade pcmsoFacade = EstudoFacade.getInstance();

                if (dgvExcecao.CurrentRow == null)
                    throw new Exception (msg3);
                
                excecaoIdadeFuncao.Remove((Int64)dgvExcecao.CurrentRow.Cells[0].Value);
                
                pcmsoFacade.deleteGheSetorClienteFuncaoExame(gheFonteAgente.GheFonte.Ghe, exame, new Funcao((Int64)dgvExcecao.CurrentRow.Cells[0].Value, string.Empty, string.Empty, string.Empty, string.Empty));
                montaGridExcecaoExamesPorFuncao();
                
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        protected void montaGridExcecaoExamesPorFuncao()
        {
            List<Int64> list = new List<Int64>(excecaoIdadeFuncao.Keys);

            KeyValuePair<Funcao, Int32> funcaoExcecao;

            dgvExcecao.Columns.Clear();

            dgvExcecao.ColumnCount = 3;

            dgvExcecao.Columns[0].Name = "ID";
            dgvExcecao.Columns[0].Visible = false;

            dgvExcecao.Columns[1].Name = "Função";
            dgvExcecao.Columns[2].Name = "Idade";

            Int32 rowindex = 0;

            Funcao func = null;

            foreach (KeyValuePair<Int64, Dictionary<Funcao, Int32>> dict in excecaoIdadeFuncao)
            {
                funcaoExcecao = dict.Value.First();

                func = funcaoExcecao.Key;

                this.dgvExcecao.Rows.Insert(rowindex, func.Id, func.Descricao, funcaoExcecao.Value);

                rowindex++;
            }
        }

        protected void montaGridPeriodicidade()
        {
            try
            {
                dgvPeriodicidade.Columns.Clear();

                EstudoFacade pcmsoFacade = EstudoFacade.getInstance();
                DataSet ds = pcmsoFacade.findAllPeriodicidade();

                //dgvPeriodicidade.DataSource = ds.Tables["Periodicidades"].DefaultView;

                dgvPeriodicidade.ColumnCount = 2;

                dgvPeriodicidade.Columns[0].HeaderText = "Id";
                dgvPeriodicidade.Columns[0].Visible = false;

                dgvPeriodicidade.Columns[1].HeaderText = "Descrição";
                dgvPeriodicidade.Columns[1].ReadOnly = true;

                DataGridViewCheckBoxColumn check = new DataGridViewCheckBoxColumn();
                check.Name = "Selecione";
                dgvPeriodicidade.Columns.Add(check);
                dgvPeriodicidade.Columns[2].HeaderText = "Selecione";
                dgvPeriodicidade.Columns[2].Width = 30;

                /* incluindo atributo periodo de vencimento */

                DataGridViewComboBoxColumn periodoVencimento = new DataGridViewComboBoxColumn();
                dgvPeriodicidade.Columns.Add(periodoVencimento);
                ComboHelper.periodoVencimentoCombo(periodoVencimento);
                dgvPeriodicidade.Columns[3].HeaderText = "Vencimento";
                dgvPeriodicidade.Columns[3].DefaultCellStyle.BackColor = Color.LightYellow;
                dgvPeriodicidade.Columns[3].Width = 150;

                foreach (DataRow dvRow in ds.Tables[0].Rows)
                    dgvPeriodicidade.Rows.Add(Convert.ToInt64(dvRow["id_periodicidade"]), dvRow["descricao"].ToString(), false, exame.PeriodoVencimento == null ? string.Empty : exame.PeriodoVencimento.ToString());

                /* reordenando a grid */
                dgvPeriodicidade.Columns[1].DisplayIndex = 2;
                dgvPeriodicidade.Columns[2].DisplayIndex = 1;
                dgvPeriodicidade.Columns[3].DisplayIndex = 3;

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        protected void textIdade_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = ValidaCampoHelper.FormataCampoNumerico(sender, e);
        }

        protected Boolean validaDadosTela()
        {
            Boolean result = true;
            try
            {
                /* validando para saber se foi selecionado um exame */
                if (exame == null)
                    throw new Exception(msg1);

                /* validando idade mínima */

                if (String.IsNullOrEmpty(textIdade.Text))
                    throw new Exception(msg5);

                foreach (DataGridViewRow dvRow in dgvExcecao.Rows)
                {
                    if (String.IsNullOrEmpty(dvRow.Cells[2].Value.ToString()))
                        throw new Exception(msg5);
                }
                 
                /* validando se foi selecionado pelo menos uma periodicidade */
                Boolean valida = false;
                foreach (DataGridViewRow dvRow in dgvPeriodicidade.Rows)
                {
                    if ((Boolean)dvRow.Cells[2].Value == true)
                        valida = true;
                }

                if (!valida)
                    throw new Exception(msg2);

            }
            catch (Exception ex)
            {
                result = false;
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            return result;
        }

        protected void frmPcmsoExamesIncluir_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.KeyCode == Keys.F4)
                    btIncluir.PerformClick();

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        

        
    }
}
