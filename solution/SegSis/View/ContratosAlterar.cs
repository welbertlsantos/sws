﻿using SWS.Entidade;
using SWS.Facade;
using SWS.View.ViewHelper;
using SWS.ViewHelper;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace SWS.View
{
    public partial class frmContratosAlterar : frmContratosIncluir
    {
        public frmContratosAlterar(Contrato contrato) :base()
        {
            InitializeComponent();
            this.contrato = contrato;

            /* preenchendo informações de tela */

            textCodigo.Text = contrato.CodigoContrato;
            dataElaboracao.Text = contrato.DataElaboracao.ToString();
            dataInicio.Checked = contrato.DataInicio != null ? true : false;
            dataInicio.Text = contrato.DataInicio != null ? contrato.DataInicio.ToString() : string.Empty;
            dataTermino.Checked = contrato.DataFim != null ? true : false;
            dataTermino.Text = contrato.DataFim != null ? contrato.DataFim.ToString() : string.Empty;
            this.cliente = contrato.Cliente;
            textCliente.Text = contrato.Cliente.RazaoSocial;
            btnCliente.Enabled = false;
            this.prestador = contrato.Prestador;
            textPrestador.Text = contrato.Prestador != null ? prestador.RazaoSocial : string.Empty;
            btnPrestador.Enabled = contrato.Prestador != null ? false : true;

            /* Verificando se for aditivo e acertamento os parâmetros */
            if (contrato.ContratoPai != null)
            {
                btnPrestador.Enabled = false;
                dataInicio.Enabled = false;
                dataTermino.Enabled = false;
            }

            MontaDataGridExame();
            MontaDataGridProduto();

            /* montando condições do contrato */

            cbBloqueio.SelectedValue = contrato.BloqueiaInadimplencia == true ? "true" : "false";
            textDiasBloqueio.Text = contrato.DiasInadimplenciaBloqueio == null ? string.Empty : contrato.DiasInadimplenciaBloqueio.ToString();

            cbValorMensal.SelectedValue = contrato.GeraMensalidade == true ? "true" : "false";
            textValorMensalidade.Text = contrato.ValorMensalidade.ToString();

            cbNumeroVidas.SelectedValue = contrato.GeraNumeroVidas == true ? "true" : "false";
            textValorNumeroVidas.Text = contrato.ValorNumeroVidas.ToString();

            cbCobrancaAutomatica.SelectedValue = contrato.CobrancaAutomatica == true ? "true" : "false";
            textDiaCobranca.Text = contrato.DataCobranca.ToString();

            textComentario.Text = contrato.Observacao;

            textEmpresa.Text = contrato.Empresa != null ? contrato.Empresa.RazaoSocial : string.Empty;
            textNomeFantasia.Text = contrato.Empresa != null ? contrato.Empresa.Fantasia : string.Empty;
            textCNPJ.Text = contrato.Empresa != null ? ValidaCampoHelper.FormataCnpj(contrato.Empresa.Cnpj) : string.Empty;
        }

        protected override void MontaDataGridExame()
        {
            try
            {
                if (contrato != null)
                {
                    this.Cursor = Cursors.WaitCursor;

                    dgvExame.Columns.Clear();

                    FinanceiroFacade financeiroFacade = FinanceiroFacade.getInstance();

                    DataSet ds = financeiroFacade.findAllContratoExameByContrato(contrato);

                    dgvExame.ColumnCount = 15;

                    dgvExame.Columns[0].HeaderText = "Id_contrato_exame";
                    dgvExame.Columns[0].Visible = false;

                    dgvExame.Columns[1].HeaderText = "Código";
                    dgvExame.Columns[1].ReadOnly = true;

                    dgvExame.Columns[2].HeaderText = "Exame";
                    dgvExame.Columns[2].ReadOnly = true;

                    dgvExame.Columns[3].HeaderText = "Preço no Contrato R$";
                    dgvExame.Columns[3].ReadOnly = false;
                    dgvExame.Columns[3].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
                    dgvExame.Columns[3].HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleRight;
                    dgvExame.Columns[3].DefaultCellStyle.BackColor = Color.LemonChiffon;

                    dgvExame.Columns[4].HeaderText = "Custo no Contrato R$";
                    dgvExame.Columns[4].ReadOnly = false;
                    dgvExame.Columns[4].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
                    dgvExame.Columns[4].HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleRight;
                    dgvExame.Columns[4].DefaultCellStyle.BackColor = Color.LemonChiffon;

                    dgvExame.Columns[5].HeaderText = "Data Inclusão";
                    dgvExame.Columns[5].Visible = false;

                    dgvExame.Columns[6].HeaderText = "Situacao";
                    dgvExame.Columns[6].Visible = false;

                    dgvExame.Columns[7].HeaderText = "flagAltera";
                    dgvExame.Columns[7].Visible = false;

                    dgvExame.Columns[8].HeaderText = "Situação Exame";
                    dgvExame.Columns[8].Visible = false;

                    dgvExame.Columns[9].HeaderText = "Laboratório";
                    dgvExame.Columns[9].Visible = false;

                    dgvExame.Columns[10].HeaderText = "Prioridade";
                    dgvExame.Columns[10].Visible = false;

                    dgvExame.Columns[11].HeaderText = "Externo";
                    dgvExame.Columns[11].Visible = false;

                    dgvExame.Columns[12].HeaderText = "Libera Documento";
                    dgvExame.Columns[12].Visible = false;

                    dgvExame.Columns[13].HeaderText = "Lucro Bruto %";
                    dgvExame.Columns[13].ReadOnly = true;
                    dgvExame.Columns[13].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
                    dgvExame.Columns[13].HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleRight;

                    dgvExame.Columns[14].HeaderText = "Padrao Contrato";
                    dgvExame.Columns[14].Visible = false;

                    foreach (DataRow dataRow in ds.Tables[0].Rows)
                        dgvExame.Rows.Add(dataRow["id_contrato_exame"], dataRow["id_exame"], dataRow["descricao"], dataRow["preco_contrato"], dataRow["custo_contrato"], dataRow["data_inclusao"], dataRow["situacao"], dataRow["altera"], dataRow["situacao_exame"], dataRow["laboratorio"], dataRow["prioridade"], dataRow["externo"], dataRow["libera_documento"], dataRow["lucro"], dataRow["padrao_contrato"]);

                    if (contrato.ContratoPai != null)
                        foreach (DataGridViewRow row in dgvExame.Rows)
                            row.ReadOnly = Convert.ToBoolean(row.Cells[7].Value) == true ? false : true;
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }

        protected override void MontaDataGridProduto()
        {
            try
            {
                if (contrato != null)
                {
                    dgvProduto.Columns.Clear();

                    FinanceiroFacade financeiroFacade = FinanceiroFacade.getInstance();

                    DataSet ds = financeiroFacade.findAllContratoProdutoByContrato(contrato);

                    dgvProduto.ColumnCount = 13;

                    dgvProduto.Columns[0].HeaderText = "Id_Produto_contrato";
                    dgvProduto.Columns[0].Visible = false;

                    dgvProduto.Columns[1].HeaderText = "Código";
                    dgvProduto.Columns[1].ReadOnly = true;

                    dgvProduto.Columns[2].HeaderText = "Produto";
                    dgvProduto.Columns[2].ReadOnly = true;

                    dgvProduto.Columns[3].HeaderText = "Preço no Contrato R$";
                    dgvProduto.Columns[3].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
                    dgvProduto.Columns[3].HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleRight;
                    dgvProduto.Columns[3].DefaultCellStyle.BackColor = Color.LemonChiffon;

                    dgvProduto.Columns[4].HeaderText = "Custo no Contrato R$";
                    dgvProduto.Columns[4].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
                    dgvProduto.Columns[4].HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleRight;
                    dgvProduto.Columns[4].DefaultCellStyle.BackColor = Color.LemonChiffon;

                    dgvProduto.Columns[5].HeaderText = "Data Inclusão";
                    dgvProduto.Columns[5].Visible = false;

                    dgvProduto.Columns[6].HeaderText = "Situacao";
                    dgvProduto.Columns[6].Visible = false;

                    dgvProduto.Columns[7].HeaderText = "FlagAltera";
                    dgvProduto.Columns[7].Visible = false;

                    dgvProduto.Columns[8].HeaderText = "Situação Produto";
                    dgvProduto.Columns[8].Visible = false;

                    dgvProduto.Columns[9].HeaderText = "Tipo";
                    dgvProduto.Columns[9].Visible = false;

                    dgvProduto.Columns[10].HeaderText = "Tipo Estudo";
                    dgvProduto.Columns[10].Visible = false;

                    dgvProduto.Columns[11].HeaderText = "Lucro Bruto %";
                    dgvProduto.Columns[11].ReadOnly = true;
                    dgvProduto.Columns[11].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
                    dgvProduto.Columns[11].HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleRight;

                    dgvProduto.Columns[12].HeaderText = "Padrão Contrato";
                    dgvProduto.Columns[12].Visible = false;

                    foreach (DataRow dataRow in ds.Tables[0].Rows)
                        dgvProduto.Rows.Add(dataRow["id_produto_contrato"], dataRow["id_produto"], dataRow["nome"], dataRow["preco_contratado"], dataRow["custo_contrato"], dataRow["data_inclusao"], dataRow["situacao"], dataRow["altera"], dataRow["situacao_produto"], dataRow["tipo"], dataRow["tipo_estudo"], dataRow["lucro"], dataRow["Padrao_Contrato"]);

                    if (contrato.ContratoPai != null)
                        foreach (DataGridViewRow row in dgvProduto.Rows)
                            row.ReadOnly = Convert.ToBoolean(row.Cells[7].Value) == true ? false : true;
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        protected override void btnIncluirExame_Click(object sender, EventArgs e)
        {
            try
            {
                frmExameBuscar exameBuscar = new frmExameBuscar();
                exameBuscar.ShowDialog();

                if (exameBuscar.Exame != null)
                {
                    /* verificando se o exame não está presente na grid */

                    List<Exame> examesInGrid = montaListaExamesInGrid();

                    if (examesInGrid.Contains(exameBuscar.Exame))
                        throw new Exception("Exame já incluído.");

                    /* incluindo o exame no contrato do cliente */

                    FinanceiroFacade financeiroFacade = FinanceiroFacade.getInstance();

                    financeiroFacade.insertContratoExame(new ContratoExame(null, exameBuscar.Exame, contrato, exameBuscar.Exame.Preco, PermissionamentoFacade.usuarioAutenticado, exameBuscar.Exame.Custo, DateTime.Now, "A", true));
                    MontaDataGridExame();

                    /* posicionando a selecao no exame que foi incluído.*/
                    int index = 0;
                    foreach (DataGridViewRow dvRow in dgvExame.Rows)
                        if ((long)dvRow.Cells[1].Value == exameBuscar.Exame.Id)
                        {
                            index = dvRow.Index;
                            break;
                        }

                    dgvExame.CurrentCell = dgvExame[1, index];

                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        protected override List<Exame> montaListaExamesInGrid()
        {
            List<Exame> examesInGrid = new List<Exame>();
            
            foreach (DataGridViewRow dvRow in dgvExame.Rows)
                examesInGrid.Add(new Exame((long)dvRow.Cells[1].Value, dvRow.Cells[2].Value.ToString(), dvRow.Cells[8].Value.ToString(), Convert.ToBoolean(dvRow.Cells[9].Value), Convert.ToDecimal(dvRow.Cells[3].Value), Convert.ToInt32(dvRow.Cells[10].Value), Convert.ToDecimal(dvRow.Cells[4].Value), Convert.ToBoolean(dvRow.Cells[11].Value), Convert.ToBoolean(dvRow.Cells[12].Value), string.Empty, false, null, (bool)dvRow.Cells[14].Value));

            return examesInGrid;
        }

        protected override List<Produto> montaListaProdutosInGrid()
        {
            List<Produto> produtosInGrid = new List<Produto>();
            
            foreach (DataGridViewRow dvRow in dgvProduto.Rows)
                produtosInGrid.Add(new Produto((long)dvRow.Cells[1].Value, dvRow.Cells[2].Value.ToString(), dvRow.Cells[8].Value.ToString(), Convert.ToDecimal(dvRow.Cells[3].Value), dvRow.Cells[9].Value.ToString(), Convert.ToDecimal(dvRow.Cells[4].Value), (bool)dvRow.Cells[12].Value, string.Empty));

            return produtosInGrid;
        }

        protected override void btnExcluirExame_Click(object sender, EventArgs e)
        {
            try
            {

                if (dgvExame.CurrentRow == null)
                    throw new Exception("Selecione um exame.");

                /* verificando se o exame pode ser excluído */
                if (!Convert.ToBoolean(dgvExame.CurrentRow.Cells[7].Value))
                    throw new Exception("Você não poderá excluir dados do contrato pai. Somente incluir novos exames.");

                /* excluindo o exame do contrato do cliente */

                FinanceiroFacade financeiroFacade = FinanceiroFacade.getInstance();

                financeiroFacade.deleteContratoExame(new ContratoExame((long)dgvExame.CurrentRow.Cells[0].Value, new Exame((long)dgvExame.CurrentRow.Cells[1].Value, dgvExame.CurrentRow.Cells[2].Value.ToString(), dgvExame.CurrentRow.Cells[8].Value.ToString(), Convert.ToBoolean(dgvExame.CurrentRow.Cells[9].Value), Convert.ToDecimal(dgvExame.CurrentRow.Cells[3].Value), (int)dgvExame.CurrentRow.Cells[10].Value, Convert.ToDecimal(dgvExame.CurrentRow.Cells[4].Value), Convert.ToBoolean(dgvExame.CurrentRow.Cells[11].Value), Convert.ToBoolean(dgvExame.CurrentRow.Cells[12].Value), string.Empty, false, null, false), contrato, Convert.ToDecimal(dgvExame.CurrentRow.Cells[3].Value), PermissionamentoFacade.usuarioAutenticado, Convert.ToDecimal(dgvExame.CurrentRow.Cells[4].Value), Convert.ToDateTime(dgvExame.CurrentRow.Cells[5].Value), dgvExame.CurrentRow.Cells[6].Value.ToString(), Convert.ToBoolean(dgvExame.CurrentRow.Cells[7].Value)));

                MessageBox.Show("Exame excluído com sucesso.", "Confirmação", MessageBoxButtons.OK, MessageBoxIcon.Information);

                MontaDataGridExame();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        protected override void dgvExame_CellEndEdit(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                if (String.IsNullOrEmpty(dgvExame.CurrentCell.EditedFormattedValue.ToString()))
                    dgvExame.CurrentCell.Value = dgvExame.CurrentCell.Tag.ToString();

                dgvExame.CurrentCell.Value = ValidaCampoHelper.FormataValorMonetario(dgvExame.CurrentCell.Value.ToString());

                dgvExame.Rows[e.RowIndex].Cells[13].Value = calculaLucro(Convert.ToDecimal(dgvExame.Rows[e.RowIndex].Cells[3].EditedFormattedValue), Convert.ToDecimal(dgvExame.Rows[e.RowIndex].Cells[4].EditedFormattedValue));

                /* realizando update do contratoExame */

                FinanceiroFacade financeiroFacade = FinanceiroFacade.getInstance();

                financeiroFacade.updateContratoExame(new ContratoExame((long)dgvExame.Rows[e.RowIndex].Cells[0].Value, new Exame((long)dgvExame.CurrentRow.Cells[1].Value, dgvExame.CurrentRow.Cells[2].Value.ToString(), dgvExame.CurrentRow.Cells[8].Value.ToString(), Convert.ToBoolean(dgvExame.CurrentRow.Cells[9].Value), Convert.ToDecimal(dgvExame.CurrentRow.Cells[3].Value), (int)dgvExame.CurrentRow.Cells[10].Value, Convert.ToDecimal(dgvExame.CurrentRow.Cells[4].Value), Convert.ToBoolean(dgvExame.CurrentRow.Cells[11].Value), Convert.ToBoolean(dgvExame.CurrentRow.Cells[12].Value), string.Empty, false, null, false), contrato, Convert.ToDecimal(dgvExame.Rows[e.RowIndex].Cells[3].EditedFormattedValue), PermissionamentoFacade.usuarioAutenticado, Convert.ToDecimal(dgvExame.Rows[e.RowIndex].Cells[4].EditedFormattedValue), Convert.ToDateTime(dgvExame.Rows[e.RowIndex].Cells[5].Value), dgvExame.Rows[e.RowIndex].Cells[6].Value.ToString(), Convert.ToBoolean(dgvExame.Rows[e.RowIndex].Cells[7].Value)));
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }

        protected override void btnIncluirProduto_Click(object sender, EventArgs e)
        {
            try
            {

                frmProdutoBusca produtoBusca = new frmProdutoBusca();
                produtoBusca.ShowDialog();

                if (produtoBusca.Produto != null)
                {
                    /* verificando se o produto selecionado não está incluído. */

                    List<Produto> produtosInGrid = montaListaProdutosInGrid();

                    if (produtosInGrid.Contains(produtoBusca.Produto))
                        throw new Exception("Produto já incluído.");

                    /* incluindo o produto no contrato do cliente */

                    FinanceiroFacade financeiroFacade = FinanceiroFacade.getInstance();

                    financeiroFacade.insertContratoProduto(new ContratoProduto(null, produtoBusca.Produto, contrato, produtoBusca.Produto.Preco, PermissionamentoFacade.usuarioAutenticado, produtoBusca.Produto.Custo, DateTime.Now, "A", true));

                    MontaDataGridProduto();

                    /* posicionando a selecao no exame que foi incluído.*/
                    int index = 0;
                    foreach (DataGridViewRow dvRow in dgvProduto.Rows)
                        if ((long)dvRow.Cells[1].Value == produtoBusca.Produto.Id)
                        {
                            index = dvRow.Index;
                            break;
                        }

                    dgvProduto.CurrentCell = dgvExame[1, index];


                }

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        protected override void btnExcluirProduto_Click(object sender, EventArgs e)
        {
            try
            {

                if (dgvProduto.CurrentRow == null)
                    throw new Exception("Selecione um produto.");

                if (!Convert.ToBoolean(dgvProduto.CurrentRow.Cells[7].Value))
                    throw new Exception("Você não poderá excluir dados do contrato pai. Somente incluir novos produtos.");

                /* excluindo o produto do contrato do cliente */

                FinanceiroFacade financeiroFacade = FinanceiroFacade.getInstance();

                financeiroFacade.deleteContratoProduto(new ContratoProduto((long)dgvProduto.CurrentRow.Cells[0].Value, new Produto((long)dgvProduto.CurrentRow.Cells[1].Value, dgvProduto.CurrentRow.Cells[2].Value.ToString(), dgvProduto.CurrentRow.Cells[8].Value.ToString(), Convert.ToDecimal(dgvProduto.CurrentRow.Cells[3].Value), dgvProduto.CurrentRow.Cells[9].Value.ToString(), Convert.ToDecimal(dgvProduto.CurrentRow.Cells[4].Value), Convert.ToBoolean(dgvProduto.CurrentRow.Cells[12].Value), string.Empty), contrato, Convert.ToDecimal(dgvProduto.CurrentRow.Cells[3].Value), PermissionamentoFacade.usuarioAutenticado, Convert.ToDecimal(dgvProduto.CurrentRow.Cells[4].Value), Convert.ToDateTime(dgvProduto.CurrentRow.Cells[5].Value), "I", true));

                MontaDataGridProduto();

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        protected override void dgvProduto_CellEndEdit(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                if (String.IsNullOrEmpty(dgvProduto.CurrentCell.EditedFormattedValue.ToString()))
                    dgvProduto.CurrentCell.Value = dgvProduto.CurrentCell.Tag.ToString();

                dgvProduto.CurrentCell.Value = ValidaCampoHelper.FormataValorMonetario(dgvProduto.CurrentCell.Value.ToString());

                dgvProduto.Rows[e.RowIndex].Cells[11].Value = calculaLucro(Convert.ToDecimal(dgvProduto.Rows[e.RowIndex].Cells[3].EditedFormattedValue), Convert.ToDecimal(dgvProduto.Rows[e.RowIndex].Cells[4].EditedFormattedValue));

                /* realizando o update do contratoProduto */

                FinanceiroFacade financeiroFacade = FinanceiroFacade.getInstance();

                financeiroFacade.updateContratoProduto(new ContratoProduto((long)dgvProduto.CurrentRow.Cells[0].Value, new Produto((long)dgvProduto.CurrentRow.Cells[1].Value, dgvProduto.CurrentRow.Cells[2].Value.ToString(), dgvProduto.CurrentRow.Cells[8].Value.ToString(), Convert.ToDecimal(dgvProduto.CurrentRow.Cells[3].Value), dgvProduto.CurrentRow.Cells[9].Value.ToString(), Convert.ToDecimal(dgvProduto.CurrentRow.Cells[4].Value), (bool)dgvProduto.CurrentRow.Cells[12].Value, string.Empty), contrato, Convert.ToDecimal(dgvProduto.CurrentRow.Cells[3].EditedFormattedValue), PermissionamentoFacade.usuarioAutenticado, Convert.ToDecimal(dgvProduto.CurrentRow.Cells[4].EditedFormattedValue), Convert.ToDateTime(dgvProduto.CurrentRow.Cells[5].Value), "A", true));

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        protected override bool verificaValoresZerados()
        {
            bool valida = true;
            StringBuilder mensagem = new StringBuilder();

            try
            {
                foreach (DataGridViewRow dvRow in dgvExame.Rows)
                {

                    if (Convert.ToDecimal(dvRow.Cells[3].Value) == Convert.ToDecimal(0))
                    {
                        valida = false;
                        mensagem.Append("Tem exames com preço zerado. Verifique." + System.Environment.NewLine);
                    }

                    if (Convert.ToDecimal(dvRow.Cells[4].Value) == Convert.ToDecimal(0))
                    {
                        valida = false;
                        mensagem.Append("Tem exames com custo zerado. Verifique." + System.Environment.NewLine);
                    }
                }

                foreach (DataGridViewRow dvRow in dgvProduto.Rows)
                {
                    if (Convert.ToDecimal(dvRow.Cells[3].Value) == Convert.ToDecimal(0))
                    {
                        valida = false;
                        mensagem.Append("Tem produtos com preço zerado. Verifique." + System.Environment.NewLine);
                    }

                    if (Convert.ToDecimal(dvRow.Cells[4].Value) == Convert.ToDecimal(0))
                    {
                        valida = false;
                        mensagem.Append("Tem produtos com custo zerado. Verifique.");
                    }
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

            return valida;
        }

        protected override void btnPrestador_Click(object sender, EventArgs e)
        {
            try
            {
                if (MessageBox.Show("Deseja inserir um prestador de serviços ao contrato. Um novo contrato será criado. Deseja continuar?", "Confirmação", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                {
                    frmClienteSelecionar formPrestador = new frmClienteSelecionar(true, false, false, null, null, null, null, false, true);
                    formPrestador.ShowDialog();

                    if (formPrestador.Cliente != null)
                    {
                        prestador = formPrestador.Cliente;
                        textPrestador.Text = prestador.RazaoSocial;

                        FinanceiroFacade financeiroFacade = FinanceiroFacade.getInstance();

                        /* montando coleções de exames */

                        List<ContratoExame> examesInContratoPrestador = new List<ContratoExame>();

                        foreach (DataGridViewRow dvRow in dgvExame.Rows)
                            examesInContratoPrestador.Add(new ContratoExame(null, new Exame(Convert.ToInt64(dvRow.Cells[1].Value)), null, Convert.ToDecimal(dvRow.Cells[3].Value), PermissionamentoFacade.usuarioAutenticado, Convert.ToDecimal(dvRow.Cells[4].Value), DateTime.Now, "A", true));

                        /* montando coleções de produtos */

                        List<ContratoProduto> produtoInContratoPrestador = new List<ContratoProduto>();

                        foreach (DataGridViewRow dvRow in dgvProduto.Rows)
                            produtoInContratoPrestador.Add(new ContratoProduto(null, new Produto(Convert.ToInt64(dvRow.Cells[1].Value), dvRow.Cells[2].Value.ToString(), dvRow.Cells[8].Value.ToString(), Convert.ToDecimal(dvRow.Cells[3].Value), dvRow.Cells[9].Value.ToString(), Convert.ToDecimal(dvRow.Cells[4].Value), (bool)dvRow.Cells[12].Value, string.Empty), null, Convert.ToDecimal(dvRow.Cells[3].Value), PermissionamentoFacade.usuarioAutenticado, Convert.ToDecimal(dvRow.Cells[4].Value), DateTime.Now, "A", true));

                        contrato.Prestador = prestador;
                        Contrato contratoPrestador = financeiroFacade.insertContratoPrestador(contrato, examesInContratoPrestador, produtoInContratoPrestador);

                        MessageBox.Show("Novo contrato com o prestador incluído com sucesso." + System.Environment.NewLine + "Anote o código do novo contrato: " + ValidaCampoHelper.RetornaCodigoEstudoFormatado(contratoPrestador.CodigoContrato), "Confirmação", MessageBoxButtons.OK, MessageBoxIcon.Information);

                        this.Close();
                    }

                }

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        protected override void btnGravar_Click(object sender, EventArgs e)
        {
            try
            {
                if (validaDados())
                {
                    FinanceiroFacade financeiroFacade = FinanceiroFacade.getInstance();

                    /* verificando situação de exames zerados */
                    if (!verificaValoresZerados())
                    {
                        if (MessageBox.Show("Existem valores zerados no contrato, deseja continuar mesmo assim?", "Confirmação", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.No)
                            throw new Exception("Altere os valores da grid que estão com zero");
                    }

                    contrato = financeiroFacade.updateContrato(new Contrato(contrato.Id, contrato.CodigoContrato, contrato.Cliente, contrato.UsuarioCriador, contrato.DataElaboracao, dataTermino.Checked ? Convert.ToDateTime(dataTermino.Text) : (DateTime?)null, dataInicio.Checked ? Convert.ToDateTime(dataInicio.Text) : (DateTime?)null, Contrato.Situacao, textComentario.Text, contrato.UsuarioFinalizou, contrato.UsuarioCancelamento, contrato.MotivoCancelamento, contrato.Prestador, contrato.ContratoAutomatico, contrato.DataCancelamento, contrato.ContratoPai, Convert.ToBoolean(((SelectItem)cbBloqueio.SelectedItem).Valor), Convert.ToBoolean(((SelectItem)cbValorMensal.SelectedItem).Valor), Convert.ToBoolean(((SelectItem)cbNumeroVidas.SelectedItem).Valor), string.IsNullOrEmpty(textDiaCobranca.Text) ? (int?)null : Convert.ToInt32(textDiaCobranca.Text), Convert.ToBoolean(((SelectItem)cbCobrancaAutomatica.SelectedItem).Valor), string.IsNullOrEmpty(textValorMensalidade.Text) ? (decimal?)null : Convert.ToDecimal(textValorMensalidade.Text), string.IsNullOrEmpty(textValorNumeroVidas.Text) ? (decimal?)null : Convert.ToDecimal(textValorNumeroVidas.Text), string.IsNullOrEmpty(textDiasBloqueio.Text) ? (int?)null : Convert.ToInt32(textDiasBloqueio.Text), contrato.ContratoRaiz, contrato.ClienteProposta, contrato.Proposta, contrato.PropostaFormaPagamento, contrato.UsuarioEncerrou, contrato.DataEncerramento, contrato.MotivoEncerramento, contrato.Empresa));

                    MessageBox.Show("Contrato alterado com sucesso.", "Confirmação", MessageBoxButtons.OK, MessageBoxIcon.Information);

                    this.Close();
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }

        protected override void dgvExame_RowPrePaint(object sender, DataGridViewRowPrePaintEventArgs e)
        {
            DataGridView dgv = sender as DataGridView;

            if (Convert.ToBoolean(dgv.Rows[e.RowIndex].Cells[7].Value))
                dgv.Rows[e.RowIndex].DefaultCellStyle.ForeColor = Color.Blue;

        }

        protected override void dgvProduto_RowPrePaint(object sender, DataGridViewRowPrePaintEventArgs e)
        {
            DataGridView dgv = sender as DataGridView;

            if (Convert.ToBoolean(dgv.Rows[e.RowIndex].Cells[7].Value))
                dgv.Rows[e.RowIndex].DefaultCellStyle.ForeColor = Color.Blue;
        }

    }
}
