﻿using SWS.Entidade;
using SWS.Facade;
using SWS.Resources;
using SWS.View.Resources;
using SWS.View.ViewHelper;
using SWS.ViewHelper;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace SWS.View
{
    public partial class frmFaturamento : frmTemplate
    {

        private Cliente cliente;
        private List<Movimento> movimentos;
        private Empresa empresa;
        private Plano plano;
        private PlanoForma planoForma;
        private Decimal valorTotal;
        private Int32 quantidadeItens;
        private Nfe nfe;
        private CentroCusto centroCusto;
        
        private decimal valorISS = 0;
        private decimal valorPIS = 0;
        private decimal valorCOFINS = 0;
        private decimal valorCSLL = 0;
        private decimal valorIR = 0;

        public Nfe Nfe
        {
            get { return nfe; }
            set { nfe = value; }
        }
        private Conta conta = null;
        
        public frmFaturamento(Cliente cliente, List<Movimento> movimentos, Decimal valorTotal)
        {
            InitializeComponent();
            this.cliente = cliente;
            this.movimentos = movimentos;
            carregaComboEmpresa(cb_empresa);
            empresa = PermissionamentoFacade.usuarioAutenticado.Empresa;
            cb_empresa.Text = empresa.RazaoSocial;
            carregaComboPagamento(cb_pagamento);

            // setando a data de hoje para a data de emissao
            this.dt_faturamento.Text = Convert.ToString(DateTime.Now);

            // atualizado totalizadores.

            this.valorTotal = valorTotal;
            quantidadeItens = movimentos.Count;

            // preenchendo campos informativos.

            if (cliente.Fisica)
            {
                lblCNPJ.Text = "CPF";
                text_razaoSocial.Text = cliente.RazaoSocial;
                text_cnpj.Mask = "999,999,999-99";
                text_cnpj.Text = cliente.Cnpj;
            }
            else
            {
                lblCNPJ.Text = "CNPJ";
                text_razaoSocial.Text = cliente.RazaoSocial;
                text_cnpj.Mask = "99,999,999/9999-99";
                text_cnpj.Text = cliente.Cnpj;
            }

            montaGrid();

            /* avaliando a nota fiscal em relação ao centro de custo */

            ValidaCentroCusto(movimentos);
                
        }

        private void ValidaCentroCusto(List<Movimento> movimentos)
        {
            HashSet<CentroCusto> centroCustos = new HashSet<CentroCusto>();

            movimentos.ForEach(delegate(Movimento movimento)
            {
                if (movimento.CentroCusto != null)
                    centroCustos.Add(movimento.CentroCusto);
            });

            if (centroCustos.Count == 1)
            {
                centroCusto = centroCustos.ElementAt(0);
                textCentroCusto.Text = centroCusto.Descricao;
            }
            else if (centroCustos.Count >= 2)
            {
                MessageBox.Show("Cliente utiliza centro de custo e você escolheu não faturar pelo centro de custo.", "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }

        private void btn_gravar_Click(object sender, EventArgs e)
        {
            try
            {
                if (validaDados())
                {
                    FinanceiroFacade financeiroFacade = FinanceiroFacade.getInstance();

                    if (empresa.SimplesNacional)
                    {
                        /* empresas no simples nacional não tem destaque de impostos federais. Somente municipais */
                        /* verificar imposto federal */
                        validaCalculoISS();
                    }
                    else
                    {
                        /* empresa não está no simples nacional. Haverá possíveis impostos federais e municipais. */

                        if (!cliente.SimplesNacional)
                        {
                            /* verificar se tem cálculo de imposto municipal e federal */
                            validaCalculoISS();
                            if (validaCalculoImpostoFederal())
                                validaCalculoIR();
                        }
                        else
                        {
                            validaCalculoISS();
                        }
                    }

                    Decimal valorLiquido = Convert.ToDecimal(valorTotal - ( valorISS + valorCOFINS + valorCSLL + valorIR + valorPIS));

                    nfe = new Nfe(null, PermissionamentoFacade.usuarioAutenticado, empresa, null, valorTotal, Convert.ToDateTime(dt_faturamento.Text), valorPIS, valorCOFINS, valorTotal, Convert.ToDecimal(empresa.AliquotaISS), valorIR, valorISS, valorCSLL, cliente, DateTime.Now, null, null, null, ApplicationConstants.FECHADO, planoForma, valorLiquido, centroCusto);
                    nfe.ListaMovimento = movimentos;

                     /* solicitando ao usuario que informe a conta que irá o boleto caso tenha forma de pagamento boleto.*/

                    if (String.Equals(nfe.PlanoForma.Forma.Descricao.ToUpper(), ApplicationConstants.BOLETO))
                    {
                        frmFaturamentoConta contas = new frmFaturamentoConta();
                        contas.ShowDialog();

                        if (contas.Conta != null)
                        {
                            this.Cursor = Cursors.WaitCursor;
                            nfe = financeiroFacade.gravaNotaFiscal(nfe, contas.Conta, PermissionamentoFacade.usuarioAutenticado);
                            MessageBox.Show("Nota fiscal gravada com sucesso.", "Confirmação", MessageBoxButtons.OK, MessageBoxIcon.Information);
                            
                            /* gerando a impressao da nota fiscal */

                            ImprimirNota(nfe);

                            this.Close();
                        }
                    }
                    else
                    {
                        this.Cursor = Cursors.WaitCursor;
                        nfe = financeiroFacade.gravaNotaFiscal(nfe, conta, PermissionamentoFacade.usuarioAutenticado);
                        MessageBox.Show("Nota fiscal gravada com sucesso.", "Confirmação", MessageBoxButtons.OK, MessageBoxIcon.Information);

                        /* gerando nota para a impressão */
                        ImprimirNota(nfe);
                       
                        this.Close();
                    }
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }

        private static void ImprimirNota(Nfe nfe)
        {
            var process = new Process();

            process.StartInfo.FileName = "Relatorio\\jre7\\bin\\java";
            process.StartInfo.Arguments = " -jar Relatorio\\MiddlewareJasper.jar " + "Relatorio\\Report\\" + "NOTAFISCAL.JASPER" + " ID_NFE:" + nfe.Id + ":INTEGER";
            process.StartInfo.UseShellExecute = false;
            process.StartInfo.RedirectStandardError = true;
            process.StartInfo.RedirectStandardInput = true;
            process.StartInfo.RedirectStandardOutput = true;
            process.StartInfo.CreateNoWindow = true;

            process.Start();
            process.StandardOutput.ReadToEnd();
            process.WaitForExit();
        }

        private void btn_fechar_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void carregaComboEmpresa(ComboBox combo)
        {
            try
            {
                EmpresaFacade empresaFacade = EmpresaFacade.getInstance();

                combo.DisplayMember = "nome";
                combo.ValueMember = "valor";
                combo.DropDownStyle = ComboBoxStyle.DropDownList;

                foreach (DataRow rows in empresaFacade.findEmpresaByFilter(new Empresa(null, string.Empty,string.Empty, string.Empty, string.Empty, string.Empty, string.Empty, string.Empty, string.Empty, string.Empty, string.Empty, string.Empty, string.Empty, string.Empty, string.Empty, null, ApplicationConstants.ATIVO, string.Empty, null, null, false, null, string.Empty, 0,0 ,0, 0, 0, 0, 0)).Tables[0].Rows)
                    combo.Items.Add(new SelectItem(Convert.ToString(rows["id_empresa"]), Convert.ToString(rows["razao_social"])));

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void carregaComboPagamento(ComboBox combo)
        {
            try
            {
                FinanceiroFacade financeiroFacade = FinanceiroFacade.getInstance();

                combo.DisplayMember = "nome";
                combo.ValueMember = "valor";
                combo.DropDownStyle = ComboBoxStyle.DropDownList;

                foreach (DataRow rows in financeiroFacade.findPlanoByFilter(new Plano(null, string.Empty, ApplicationConstants.ATIVO, false)).Tables[0].Rows)
                    combo.Items.Add(new SelectItem(Convert.ToString(rows["id_plano"]), Convert.ToString(rows["descricao"])));

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void cb_empresa_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                if (cb_empresa.SelectedIndex != -1)
                {
                    EmpresaFacade empresaFacade = EmpresaFacade.getInstance();
                    empresa = empresaFacade.findEmpresaById(Convert.ToInt64(((SelectItem)cb_empresa.SelectedItem).Valor));
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void carregaComboForma(ComboBox combo)
        {
            try
            {
                FinanceiroFacade financeiroFacade = FinanceiroFacade.getInstance();

                combo.DisplayMember = "nome";
                combo.ValueMember = "valor";
                combo.DropDownStyle = ComboBoxStyle.DropDownList;

                foreach (DataRow rows in financeiroFacade.findFormaByPlano(plano).Tables[0].Rows)
                    combo.Items.Add(new SelectItem(Convert.ToString(rows["id_plano_forma"]), Convert.ToString(rows["descricao"])));

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void cb_forma_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                if (cb_forma.SelectedIndex != -1)
                {
                    FinanceiroFacade financeiroFacade = FinanceiroFacade.getInstance();
                    planoForma = financeiroFacade.findPlanoFormaById(Convert.ToInt64(((SelectItem)cb_forma.SelectedItem).Valor));
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void cb_pagamento_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                if (cb_pagamento.SelectedIndex != -1)
                {
                    cb_forma.Items.Clear();

                    FinanceiroFacade financeiroFacade = FinanceiroFacade.getInstance();
                    plano = financeiroFacade.findPlanoById(Convert.ToInt64(((SelectItem)cb_pagamento.SelectedItem).Valor));

                    carregaComboForma(cb_forma);

                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void dgv_itemMovimento_ColumnAdded(object sender, DataGridViewColumnEventArgs e)
        {
            e.Column.SortMode = DataGridViewColumnSortMode.NotSortable;
        }

        private void dgv_itemMovimento_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if (dgv_itemMovimento.CurrentRow == null)
                    throw new Exception("Selecione uma linha");
                
                if (e.KeyCode == Keys.Delete)
                {
                    if (MessageBox.Show("Deseja excluir esse item?", "Confirmação", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                    {
                        movimentos.RemoveAt(dgv_itemMovimento.CurrentRow.Index);
                        ValidaCentroCusto(movimentos);
                        atualizaTotalizadores((Decimal)dgv_itemMovimento.CurrentRow.Cells[2].Value);
                        dgv_itemMovimento.Rows.RemoveAt(dgv_itemMovimento.CurrentRow.Index);

                        MessageBox.Show("Item excluído.", "Confirmação", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }

                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void atualizaTotalizadores(Decimal valorSelecionado)
        {

            // atualizando quantidade de itens.
            quantidadeItens--;
            text_itensFaturados.Text = Convert.ToString(quantidadeItens);

            // atualizando total nf
            valorTotal -= valorSelecionado;
            text_totalNF.Text = ValidaCampoHelper.FormataValorMonetario(Convert.ToString(valorTotal));

        }

        public void mostraTotal()
        {
            text_itensFaturados.Text = Convert.ToString(quantidadeItens);
            text_totalNF.Text = ValidaCampoHelper.FormataValorMonetario(Convert.ToString(valorTotal));
        }

        public void montaGrid()
        {
            try
            {
                this.Cursor = Cursors.WaitCursor;

                this.dgv_itemMovimento.Columns.Clear();

                this.dgv_itemMovimento.ColumnCount = 16;

                this.dgv_itemMovimento.Columns[0].HeaderText = "ID Movimento";
                this.dgv_itemMovimento.Columns[0].Visible = false;

                this.dgv_itemMovimento.Columns[1].HeaderText = "Data Inclusão";
                this.dgv_itemMovimento.Columns[1].DisplayIndex = 6;

                this.dgv_itemMovimento.Columns[2].HeaderText = "Preço";
                this.dgv_itemMovimento.Columns[2].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
                this.dgv_itemMovimento.Columns[2].HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleRight;
                this.dgv_itemMovimento.Columns[2].DisplayIndex = 4;

                this.dgv_itemMovimento.Columns[3].HeaderText = "Quantidade";
                this.dgv_itemMovimento.Columns[3].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
                this.dgv_itemMovimento.Columns[3].HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleRight;
                this.dgv_itemMovimento.Columns[3].DisplayIndex = 5;

                this.dgv_itemMovimento.Columns[4].HeaderText = "ID_ghe_fonte_agente_exame_aso";
                this.dgv_itemMovimento.Columns[4].Visible = false;

                this.dgv_itemMovimento.Columns[5].HeaderText = "ID_cliente_função_exame_aso";
                this.dgv_itemMovimento.Columns[5].Visible = false;

                this.dgv_itemMovimento.Columns[6].HeaderText = "ID_exame";
                this.dgv_itemMovimento.Columns[6].Visible = false;

                this.dgv_itemMovimento.Columns[7].HeaderText = "Nome do Exame";
                this.dgv_itemMovimento.Columns[7].DisplayIndex = 2;

                this.dgv_itemMovimento.Columns[8].HeaderText = "ID_produto_contrato";
                this.dgv_itemMovimento.Columns[8].Visible = false;

                this.dgv_itemMovimento.Columns[9].HeaderText = "ID_produto";
                this.dgv_itemMovimento.Columns[9].Visible = false;

                this.dgv_itemMovimento.Columns[10].HeaderText = "Produto";
                this.dgv_itemMovimento.Columns[10].DisplayIndex = 3;

                this.dgv_itemMovimento.Columns[11].HeaderText = "Id_usuario";
                this.dgv_itemMovimento.Columns[11].Visible = false;

                this.dgv_itemMovimento.Columns[12].HeaderText = "Usuário";
                this.dgv_itemMovimento.Columns[12].DisplayIndex = 7;

                this.dgv_itemMovimento.Columns[13].HeaderText = "Nome do Funcionário";
                this.dgv_itemMovimento.Columns[13].DisplayIndex = 0;

                this.dgv_itemMovimento.Columns[14].HeaderText = "Código do Atendimento";
                this.dgv_itemMovimento.Columns[14].DisplayIndex = 1;

                this.dgv_itemMovimento.Columns[15].HeaderText = "Centro de Custo";
                this.dgv_itemMovimento.Columns[15].DisplayIndex = 6;

                foreach (Movimento movimento in movimentos)
                {
                    if (movimento.GheFonteAgenteExameAso != null)
                        dgv_itemMovimento.Rows.Add(movimento.Id, movimento.DataInclusao, movimento.PrecoUnit, movimento.Quantidade, movimento.GheFonteAgenteExameAso.Id, null, movimento.GheFonteAgenteExameAso.GheFonteAgenteExame.Exame.Id, movimento.GheFonteAgenteExameAso.GheFonteAgenteExame.Exame.Descricao, null, null, null, movimento.Usuario.Id, movimento.Usuario.Login, movimento.Aso.ClienteFuncaoFuncionario.Funcionario.Nome, movimento.Aso.Codigo, movimento.CentroCusto != null ? movimento.CentroCusto.Descricao : string.Empty);

                    else if (movimento.ClienteFuncaoExameAso != null)
                        dgv_itemMovimento.Rows.Add(movimento.Id, movimento.DataInclusao, movimento.PrecoUnit, movimento.Quantidade, null, movimento.ClienteFuncaoExameAso.Id, movimento.ClienteFuncaoExameAso.ClienteFuncaoExame.Exame.Id, movimento.ClienteFuncaoExameAso.ClienteFuncaoExame.Exame.Descricao, null, null, null, movimento.Usuario.Id, movimento.Usuario.Login, movimento.Aso.ClienteFuncaoFuncionario.Funcionario.Nome, movimento.Aso.Codigo, movimento.CentroCusto != null ? movimento.CentroCusto.Descricao : string.Empty);

                    else if (movimento.ContratoProduto != null)
                        dgv_itemMovimento.Rows.Add(movimento.Id, movimento.DataInclusao, movimento.PrecoUnit, movimento.Quantidade, null, null, null, null, movimento.ContratoProduto.Id, movimento.ContratoProduto.Produto.Id, movimento.ContratoProduto.Produto.Nome, movimento.Usuario.Id, movimento.Usuario.Login, null, null, movimento.CentroCusto != null ? movimento.CentroCusto.Descricao : string.Empty);

                    mostraTotal();
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }

        private bool validaDados()
        {
            bool retorno = true;
            try
            {

                if (cb_empresa.SelectedIndex == -1)
                    throw new Exception( "A empresa é obrigatória.");

                if (cb_pagamento.SelectedIndex == -1)
                    throw new Exception("Condição de pagamento é obrigatório.");

                if (movimentos.Count == 0)
                    throw new Exception("Não existe movimento para ser faturado.");

                if (cb_forma.SelectedIndex == -1)
                    throw new Exception("A forma de pagamento não foi selecionada.");

            }
            catch (Exception ex)
            {
                retorno = false;
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            
            return retorno;
        }

        private void validaCalculoISS()
        {
            if (cliente.CidadeIbge.Id == empresa.Cidade.Id)
                valorISS = valorTotal * (empresa.AliquotaISS / 100);
        }

        private bool validaCalculoImpostoFederal()
        {
            bool realizaCalculoImposto = false;

            if (valorTotal >= empresa.ValorMinimoImpostoFederal)
            {
                valorPIS = valorTotal * (empresa.AliquotaPIS / 100);
                valorCOFINS = valorTotal * (empresa.AliquotaCOFINS / 100);
                valorCSLL = valorTotal * (empresa.AliquotaCSLL / 100);
                realizaCalculoImposto = true;
            }
            return realizaCalculoImposto;
        }

        private void validaCalculoIR()
        {
            if (valorTotal >= empresa.ValorMinimoIR)
                valorIR = valorTotal * (empresa.AliquotaIR / 100);
        }

    }
}
