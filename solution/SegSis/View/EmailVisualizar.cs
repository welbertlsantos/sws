﻿using SWS.Entidade;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace SWS.View
{
    public partial class frmEmailVisualizar : frmTemplateConsulta
    {
        public frmEmailVisualizar(Email email)
        {
            InitializeComponent();
            textEmail.Text = email.Para;
            textEmail.ReadOnly = true;

            textPrioridade.Text = String.Equals(email.Prioridade, "Normal") ? "NORMAL" : String.Equals(email.Prioridade, "Low") ? "BAIXA" : "ALTA";
            textPrioridade.ReadOnly = true;

            textAssunto.Text = email.Assunto;
            textAssunto.ReadOnly = true;

            textMensagem.Text = email.Mensagem;
            textMensagem.ReadOnly = true;

            textAnexo.Text = email.Anexo;
            textAnexo.ReadOnly = true;
        }

        private void btnFechar_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
