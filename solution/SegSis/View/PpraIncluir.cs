﻿using SWS.Entidade;
using SWS.View.ViewHelper;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace SWS.View
{
    public partial class frmPpraIncluir : frmTemplate
    {
        protected Estudo ppra;

        public Estudo Ppra
        {
            get { return ppra; }
            set { ppra = value; }
        }

        protected Cliente cliente;

        public Cliente Cliente
        {
            get { return cliente; }
            set { cliente = value; }
        }

        private ClienteCnae clienteCnaeIsCliente;

        protected ClienteCnae ClienteCnaeIsCliente
        {
            get { return clienteCnaeIsCliente; }
            set { clienteCnaeIsCliente = value; }
        }
        
        public frmPpraIncluir()
        {
            InitializeComponent();
            ActiveControl = btnIncluirCliente;
            ComboHelper.carregaComboRisco(cbGrauRisco);
            controleItensTela(false);
        }

        private void btn_fechar_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void frmPpraIncluir_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter || e.KeyCode == Keys.Return)
                SendKeys.Send("{TAB}");
        }

        private void controleItensTela(bool situacao)
        {
            dgPrevisao.Enabled = situacao;
            flpPrevisao.Enabled = situacao;
            dgvGhe.Enabled = situacao;
            flpGhe.Enabled = situacao;
            dgvFonte.Enabled = situacao;
            flpFonte.Enabled = situacao;
            dgvAgente.Enabled = situacao;
            flpAgente.Enabled = situacao;
            dgvEpi.Enabled = situacao;
            flpEpi.Enabled = situacao;
            dgvSetor.Enabled = situacao;
            flpSetor.Enabled = situacao;
            dgvFuncao.Enabled = situacao;
            flpFuncao.Enabled = situacao;
            dgvAtividade.Enabled = situacao;
            flpAtividade.Enabled = situacao;
            textAnexo.Enabled = situacao;
            btnIncluirAnexo.Enabled = situacao;
            dgvAnexo.Enabled = situacao;
        }

        private bool validaInclusaoNovoEstudo()
        {
            bool retorno = false;
            try
            {


            }
            catch (Exception ex)
            {
                retorno = true;
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            return retorno;
        }

        private void btnNovoEstudo_Click(object sender, EventArgs e)
        {
            try
            {
                if (!validaInclusaoNovoEstudo())
                {
                    controleItensTela(true);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {

            }

        }

        private void btnIncluirCliente_Click(object sender, EventArgs e)
        {
            try
            {
                frmClienteSelecionar selecionarCliente = new frmClienteSelecionar(null, null, null, null, null, null, null, null, true);
                selecionarCliente.ShowDialog();

                if (selecionarCliente.Cliente != null)
                {
                    this.Cliente = selecionarCliente.Cliente;
                    textCliente.Text = cliente.RazaoSocial + " - " + (this.Cliente.tipoCliente() == 1 ? ValidaCampoHelper.FormataCnpj(this.Cliente.Cnpj) : ValidaCampoHelper.FormataCpf(this.Cliente.Cnpj));
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }

        private void btnExcluirCliente_Click(object sender, EventArgs e)
        {
            try
            {
                if (this.Cliente == null)
                    throw new Exception("Você deve selecionar primeiro o cliente.");

                this.Cliente = null;
                this.textCliente.Text = string.Empty;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }

        private void btnIncluirCnae_Click(object sender, EventArgs e)
        {
            try
            {
                if (this.Cliente == null)
                    throw new Exception("Selecione primeiro o cliente para depois selecionar o CNAE que será utilizado no estudo");

                frmCnaeClienteSelecionar selecionarCnae = new frmCnaeClienteSelecionar(this.Cliente);
                selecionarCnae.ShowDialog();

                if (selecionarCnae.ClienteCnae != null)
                {
                    this.ClienteCnaeIsCliente = selecionarCnae.ClienteCnae;
                    this.textCnae.Text = ValidaCampoHelper.RetornaCnaeFormatado(this.ClienteCnaeIsCliente.Cnae.CodCnae);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }

        private void btnExcluirCnae_Click(object sender, EventArgs e)
        {
            try
            {
                if (this.ClienteCnaeIsCliente == null)
                    throw new Exception("Você deve selecionar primeiro o CNAE para depois excluí-lo.");

                this.ClienteCnaeIsCliente = null;
                textCnae.Text = string.Empty;

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }
        
    }
}
