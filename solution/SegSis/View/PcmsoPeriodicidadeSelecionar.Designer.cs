﻿namespace SegSis.View
{
    partial class frm_PcmsoPeriodicidadeSelecionar
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frm_PcmsoPeriodicidadeSelecionar));
            this.grd_periodicidade = new System.Windows.Forms.DataGridView();
            this.btn_buscar = new System.Windows.Forms.Button();
            this.grb_gride = new System.Windows.Forms.GroupBox();
            this.grb_dados = new System.Windows.Forms.GroupBox();
            this.lbl_atividade = new System.Windows.Forms.Label();
            this.text_descricao = new System.Windows.Forms.TextBox();
            this.btn_fechar = new System.Windows.Forms.Button();
            this.btn_ok = new System.Windows.Forms.Button();
            this.grb_paginacao = new System.Windows.Forms.GroupBox();
            ((System.ComponentModel.ISupportInitialize)(this.grd_periodicidade)).BeginInit();
            this.grb_gride.SuspendLayout();
            this.grb_dados.SuspendLayout();
            this.grb_paginacao.SuspendLayout();
            this.SuspendLayout();
            // 
            // grd_periodicidade
            // 
            this.grd_periodicidade.AllowUserToAddRows = false;
            this.grd_periodicidade.AllowUserToDeleteRows = false;
            this.grd_periodicidade.AllowUserToOrderColumns = true;
            this.grd_periodicidade.AllowUserToResizeRows = false;
            this.grd_periodicidade.BackgroundColor = System.Drawing.Color.White;
            this.grd_periodicidade.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.grd_periodicidade.Location = new System.Drawing.Point(6, 19);
            this.grd_periodicidade.Name = "grd_periodicidade";
            this.grd_periodicidade.ReadOnly = true;
            this.grd_periodicidade.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.grd_periodicidade.Size = new System.Drawing.Size(581, 228);
            this.grd_periodicidade.TabIndex = 3;
            // 
            // btn_buscar
            // 
            this.btn_buscar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_buscar.Image = global::SegSis.Properties.Resources.lupa;
            this.btn_buscar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btn_buscar.Location = new System.Drawing.Point(6, 58);
            this.btn_buscar.Name = "btn_buscar";
            this.btn_buscar.Size = new System.Drawing.Size(77, 23);
            this.btn_buscar.TabIndex = 2;
            this.btn_buscar.Text = "&Buscar";
            this.btn_buscar.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btn_buscar.UseVisualStyleBackColor = true;
            this.btn_buscar.Click += new System.EventHandler(this.btn_buscar_Click);
            // 
            // grb_gride
            // 
            this.grb_gride.BackColor = System.Drawing.Color.Transparent;
            this.grb_gride.Controls.Add(this.grd_periodicidade);
            this.grb_gride.Location = new System.Drawing.Point(4, 91);
            this.grb_gride.Name = "grb_gride";
            this.grb_gride.Size = new System.Drawing.Size(593, 253);
            this.grb_gride.TabIndex = 54;
            this.grb_gride.TabStop = false;
            this.grb_gride.Text = "Periodicidades";
            // 
            // grb_dados
            // 
            this.grb_dados.Controls.Add(this.btn_buscar);
            this.grb_dados.Controls.Add(this.lbl_atividade);
            this.grb_dados.Controls.Add(this.text_descricao);
            this.grb_dados.Location = new System.Drawing.Point(4, 4);
            this.grb_dados.Name = "grb_dados";
            this.grb_dados.Size = new System.Drawing.Size(593, 88);
            this.grb_dados.TabIndex = 53;
            this.grb_dados.TabStop = false;
            this.grb_dados.Text = "Dados";
            // 
            // lbl_atividade
            // 
            this.lbl_atividade.AutoSize = true;
            this.lbl_atividade.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_atividade.Location = new System.Drawing.Point(3, 16);
            this.lbl_atividade.Name = "lbl_atividade";
            this.lbl_atividade.Size = new System.Drawing.Size(55, 13);
            this.lbl_atividade.TabIndex = 7;
            this.lbl_atividade.Text = "Descricao";
            // 
            // text_descricao
            // 
            this.text_descricao.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.text_descricao.Location = new System.Drawing.Point(6, 32);
            this.text_descricao.MaxLength = 100;
            this.text_descricao.Name = "text_descricao";
            this.text_descricao.Size = new System.Drawing.Size(578, 20);
            this.text_descricao.TabIndex = 1;
            // 
            // btn_fechar
            // 
            this.btn_fechar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_fechar.Image = global::SegSis.Properties.Resources.close;
            this.btn_fechar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btn_fechar.Location = new System.Drawing.Point(89, 16);
            this.btn_fechar.Name = "btn_fechar";
            this.btn_fechar.Size = new System.Drawing.Size(75, 23);
            this.btn_fechar.TabIndex = 6;
            this.btn_fechar.Text = "&Fechar";
            this.btn_fechar.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btn_fechar.UseVisualStyleBackColor = true;
            this.btn_fechar.Click += new System.EventHandler(this.btn_fechar_Click);
            // 
            // btn_ok
            // 
            this.btn_ok.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_ok.Image = global::SegSis.Properties.Resources.fechar_ico;
            this.btn_ok.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btn_ok.Location = new System.Drawing.Point(6, 16);
            this.btn_ok.Name = "btn_ok";
            this.btn_ok.Size = new System.Drawing.Size(77, 23);
            this.btn_ok.TabIndex = 4;
            this.btn_ok.Text = "&Ok";
            this.btn_ok.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btn_ok.UseVisualStyleBackColor = true;
            this.btn_ok.Click += new System.EventHandler(this.btn_ok_Click);
            // 
            // grb_paginacao
            // 
            this.grb_paginacao.Controls.Add(this.btn_fechar);
            this.grb_paginacao.Controls.Add(this.btn_ok);
            this.grb_paginacao.Location = new System.Drawing.Point(4, 350);
            this.grb_paginacao.Name = "grb_paginacao";
            this.grb_paginacao.Size = new System.Drawing.Size(593, 47);
            this.grb_paginacao.TabIndex = 55;
            this.grb_paginacao.TabStop = false;
            // 
            // frm_PcmsoPeriodicidadeSelecionar
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(600, 400);
            this.ControlBox = false;
            this.Controls.Add(this.grb_gride);
            this.Controls.Add(this.grb_dados);
            this.Controls.Add(this.grb_paginacao);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "frm_PcmsoPeriodicidadeSelecionar";
            this.Text = "SELECIONAR PERIODICIDADE";
            ((System.ComponentModel.ISupportInitialize)(this.grd_periodicidade)).EndInit();
            this.grb_gride.ResumeLayout(false);
            this.grb_dados.ResumeLayout(false);
            this.grb_dados.PerformLayout();
            this.grb_paginacao.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.DataGridView grd_periodicidade;
        private System.Windows.Forms.Button btn_buscar;
        private System.Windows.Forms.GroupBox grb_gride;
        private System.Windows.Forms.GroupBox grb_dados;
        private System.Windows.Forms.Button btn_fechar;
        private System.Windows.Forms.Button btn_ok;
        private System.Windows.Forms.GroupBox grb_paginacao;
        private System.Windows.Forms.Label lbl_atividade;
        private System.Windows.Forms.TextBox text_descricao;
    }
}