﻿using SWS.Entidade;
using SWS.Facade;
using SWS.View.ViewHelper;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace SWS.View
{
    public partial class frmEpiAlterar : frmEpiIncluir
    {
        public frmEpiAlterar(Epi epi)
        {
            InitializeComponent();
            this.epi = epi;

            text_descricao.Text = epi.Descricao;
            text_finalidade.Text = epi.Finalidade;
            text_ca.Text = epi.Ca;
        }

        protected override void btn_gravar_Click(object sender, EventArgs e)
        {
            try
            {
                if (this.validaCamposObrigatorio())
                {
                    EstudoFacade PcmsoFacade = EstudoFacade.getInstance();

                    epi = PcmsoFacade.updateEpi(new Epi(epi.Id, text_descricao.Text.Trim(), text_finalidade.Text.Trim(), text_ca.Text.Trim(), epi.Situacao));

                    MessageBox.Show("EPI alterado com sucesso.", "Confirmação", MessageBoxButtons.OK, MessageBoxIcon.Information);

                    this.Close();
                }
            }

            catch (Exception Ex)
            {
                MessageBox.Show(Ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

    }
}
