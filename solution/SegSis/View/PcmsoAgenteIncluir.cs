﻿using SWS.Entidade;
using SWS.Facade;
using SWS.View.Resources;
using SWS.View.ViewHelper;
using SWS.ViewHelper;
using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SWS.View
{
    public partial class frmPcmsoAgenteIncluir : frmTemplateConsulta
    {
        protected GheFonte gheFonte;

        protected GheFonteAgente gheFonteAgente;
        protected Agente agente;
        protected GradSoma gradSoma;
        protected GradExposicao gradExposicao;
        protected GradEfeito gradEfeito;

        public GheFonteAgente GheFonteAgente
        {
            get { return gheFonteAgente; }
            set { gheFonteAgente = value; }
        }
        
        public frmPcmsoAgenteIncluir(GheFonte gheFonte)
        {
            InitializeComponent();
            ActiveControl = btnAgenteIncluir;
            this.gheFonte = gheFonte;
            statusControl(false);
            
        }
        
        public frmPcmsoAgenteIncluir ()
        {
            InitializeComponent();
        }

        protected void btnAgenteIncluir_Click(object sender, EventArgs e)
        {
            frmAgentesBuscar buscarAgente = new frmAgentesBuscar();
            buscarAgente.ShowDialog();

            if (buscarAgente.Agente != null)
            {
                agente = buscarAgente.Agente;
                textAgente.Text = agente.Descricao;
                statusControl(true);
                montaComboExposicao(cbExposicao);
                montaComboEfeito(cbEfeito);
                ComboHelper.tempoExposicao(cbTempoExposicao);
            }
        }

        protected void btnAgenteExcluir_Click(object sender, EventArgs e)
        {
            try
            {
                if (agente == null)
                    throw new Exception("Você deve selecionar primeiro o agente");

                agente = null;
                textAgente.Text = string.Empty;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }

        protected virtual void btnGravar_Click(object sender, EventArgs e)
        {
            try
            {
                if (validaCamposTela())
                {
                    EstudoFacade pcmsoFacade = EstudoFacade.getInstance();
                    
                    this.GheFonteAgente = new GheFonteAgente();
                    GheFonteAgente.Agente = agente;
                    GheFonteAgente.GheFonte = gheFonte;
                    GheFonteAgente.GradEfeito = gradEfeito;
                    GheFonteAgente.GradExposicao = gradExposicao;
                    GheFonteAgente.GradSoma = gradSoma;
                    GheFonteAgente.Situacao = ApplicationConstants.ATIVO;
                    GheFonteAgente.TempoExposicao = ((SelectItem)cbTempoExposicao.SelectedItem).Valor;

                    GheFonteAgente = pcmsoFacade.insertGheFonteAgente(GheFonteAgente);
                    MessageBox.Show("Agente incluído com sucesso", "Confirmação", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    this.Close();
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }

        protected void btnFechar_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        protected void montaComboExposicao(ComboBox combo)
        {
            try
            {
                EstudoFacade pcmsoFacade = EstudoFacade.getInstance();
                ArrayList arrNew = new ArrayList();

                DataSet ds = pcmsoFacade.findAllGradExposicao();

                foreach (DataRow rows in ds.Tables[0].Rows)
                {
                    arrNew.Add(new SelectItem(
                        rows["ID_GRAD_EXPOSICAO"].ToString(),
                        rows["CATEGORIA"].ToString()
                        ));
                }

                combo.DataSource = arrNew;
                combo.DisplayMember = "Nome";
                combo.ValueMember = "Valor";
                combo.DropDownStyle = ComboBoxStyle.DropDownList;

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            
        }

        protected void montaComboEfeito(ComboBox combo)
        {
            try
            {
                EstudoFacade pcmsoFacade = EstudoFacade.getInstance();
                DataSet ds = pcmsoFacade.findAllGradEfeito();
                ArrayList arrNew = new ArrayList();

                foreach (DataRow rows in ds.Tables[0].Rows)
                {
                    arrNew.Add(new SelectItem(
                        rows["ID_GRAD_EFEITO"].ToString(),
                        rows["CATEGORIA"].ToString()
                        ));
                }

                combo.DataSource = arrNew;
                combo.DisplayMember = "Nome";
                combo.ValueMember = "Valor";
                combo.DropDownStyle = ComboBoxStyle.DropDownList;

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }

        protected void statusControl(bool status)
        {
            cbEfeito.Enabled = status;
            cbExposicao.Enabled = status;
            cbTempoExposicao.Enabled = status;
            grbMedicaControle.Enabled = status;
        }

        protected void cbExposicao_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                if (cbEfeito.SelectedIndex != -1)
                    CalculaGradacaoSoma();

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }

        protected void cbEfeito_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                if (cbExposicao.SelectedIndex != -1)
                    CalculaGradacaoSoma();

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }

        protected void CalculaGradacaoSoma()
        {
            try
            {
                EstudoFacade pcmsoFacade = EstudoFacade.getInstance();

                if (cbExposicao.SelectedIndex == -1)
                    throw new Exception("Selecione primeiro a exposição ao agente");

                if (cbEfeito.SelectedIndex == -1)
                    throw new Exception("Selecione primeiro o efeito a saúde.");

                gradExposicao = new GradExposicao();
                gradExposicao.Id = Convert.ToInt64(((SelectItem)cbExposicao.SelectedItem).Valor);
                gradExposicao = pcmsoFacade.findGradExposicaoById(gradExposicao);

                gradEfeito = new GradEfeito();
                gradEfeito.Id = Convert.ToInt64(((SelectItem)cbEfeito.SelectedItem).Valor);
                gradEfeito = pcmsoFacade.findGradEfeitoById(gradEfeito);

                gradSoma = new GradSoma();
                gradSoma.Faixa = (Convert.ToInt32(gradExposicao.Valor) + Convert.ToInt32(gradEfeito.Valor));
                
                gradSoma = pcmsoFacade.findGradSomaByGradSoma(gradSoma);
                montaQuadroExplicativo();
                
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }

        protected void montaQuadroExplicativo()
        {
            try
            {
                lbMedicaControle.Items.Clear();
                lbMedicaControle.Items.Add("Exposicao" + " ( " + gradExposicao.Valor + " ) " + System.Environment.NewLine);
                lbMedicaControle.Items.Add(gradExposicao.Descricao + System.Environment.NewLine);
                lbMedicaControle.Items.Add("Efeito" + " ( " + gradEfeito.Valor + " ) " + System.Environment.NewLine);
                lbMedicaControle.Items.Add(gradEfeito.Descricao + System.Environment.NewLine);
                lbMedicaControle.Items.Add("Resultado:" + System.Environment.NewLine);
                lbMedicaControle.Items.Add("( " + gradSoma.Faixa + " ) - " + gradSoma.Descricao);

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }

        protected bool validaCamposTela()
        {
            bool resultado = true;
            try
            {
                if (cbExposicao.SelectedIndex == -1)
                    throw new Exception("Você deve selecionar a exposição ao agente");

                if (cbEfeito.SelectedIndex == -1)
                    throw new Exception("Você deve selecionar o efeito a saúde.");

                if (cbTempoExposicao.SelectedIndex == -1)
                    throw new Exception("Você deve selecionar o tempo de exposição ao agente");

                if (lbMedicaControle.Items.Count == 0)
                    throw new Exception("Nenhuma médida de controle foi apresentada.");

                if (agente == null)
                    throw new Exception("Você deve selecionar o agente.");
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
                resultado = false;
            }
            return resultado;
        }

    }
}
