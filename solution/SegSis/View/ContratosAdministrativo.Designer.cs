﻿namespace SWS.View
{
    partial class frmContratosAdministrativo
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmContratosAdministrativo));
            this.flpAcao = new System.Windows.Forms.FlowLayoutPanel();
            this.btn_fechar = new System.Windows.Forms.Button();
            this.lblCliente = new System.Windows.Forms.TextBox();
            this.btn_cliente = new System.Windows.Forms.Button();
            this.text_cliente = new System.Windows.Forms.TextBox();
            this.grb_exames = new System.Windows.Forms.GroupBox();
            this.dgvExame = new System.Windows.Forms.DataGridView();
            this.btn_incluirExame = new System.Windows.Forms.Button();
            this.btnExcluirExame = new System.Windows.Forms.Button();
            this.grb_produtosInternos = new System.Windows.Forms.GroupBox();
            this.dgvProduto = new System.Windows.Forms.DataGridView();
            this.btn_incluirProduto = new System.Windows.Forms.Button();
            this.btnExcluirProduto = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.spForm)).BeginInit();
            this.spForm.Panel1.SuspendLayout();
            this.spForm.Panel2.SuspendLayout();
            this.spForm.SuspendLayout();
            this.pnlForm.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.banner)).BeginInit();
            this.flpAcao.SuspendLayout();
            this.grb_exames.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvExame)).BeginInit();
            this.grb_produtosInternos.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvProduto)).BeginInit();
            this.SuspendLayout();
            // 
            // spForm
            // 
            // 
            // spForm.Panel1
            // 
            this.spForm.Panel1.Controls.Add(this.flpAcao);
            // 
            // pnlForm
            // 
            this.pnlForm.Controls.Add(this.btnExcluirProduto);
            this.pnlForm.Controls.Add(this.btn_incluirProduto);
            this.pnlForm.Controls.Add(this.grb_produtosInternos);
            this.pnlForm.Controls.Add(this.btnExcluirExame);
            this.pnlForm.Controls.Add(this.btn_incluirExame);
            this.pnlForm.Controls.Add(this.grb_exames);
            this.pnlForm.Controls.Add(this.btn_cliente);
            this.pnlForm.Controls.Add(this.text_cliente);
            this.pnlForm.Controls.Add(this.lblCliente);
            // 
            // flpAcao
            // 
            this.flpAcao.Controls.Add(this.btn_fechar);
            this.flpAcao.Dock = System.Windows.Forms.DockStyle.Fill;
            this.flpAcao.Location = new System.Drawing.Point(0, 0);
            this.flpAcao.Name = "flpAcao";
            this.flpAcao.Size = new System.Drawing.Size(784, 35);
            this.flpAcao.TabIndex = 0;
            // 
            // btn_fechar
            // 
            this.btn_fechar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_fechar.Image = global::SWS.Properties.Resources.close;
            this.btn_fechar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btn_fechar.Location = new System.Drawing.Point(3, 3);
            this.btn_fechar.Name = "btn_fechar";
            this.btn_fechar.Size = new System.Drawing.Size(75, 23);
            this.btn_fechar.TabIndex = 11;
            this.btn_fechar.TabStop = false;
            this.btn_fechar.Text = "&Fechar";
            this.btn_fechar.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btn_fechar.UseVisualStyleBackColor = true;
            this.btn_fechar.Click += new System.EventHandler(this.btn_fechar_Click);
            // 
            // lblCliente
            // 
            this.lblCliente.BackColor = System.Drawing.Color.LightGray;
            this.lblCliente.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblCliente.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCliente.Location = new System.Drawing.Point(9, 8);
            this.lblCliente.Name = "lblCliente";
            this.lblCliente.ReadOnly = true;
            this.lblCliente.Size = new System.Drawing.Size(103, 21);
            this.lblCliente.TabIndex = 0;
            this.lblCliente.TabStop = false;
            this.lblCliente.Text = "Cliente";
            // 
            // btn_cliente
            // 
            this.btn_cliente.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btn_cliente.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_cliente.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_cliente.Image = global::SWS.Properties.Resources.busca;
            this.btn_cliente.Location = new System.Drawing.Point(713, 8);
            this.btn_cliente.Name = "btn_cliente";
            this.btn_cliente.Size = new System.Drawing.Size(34, 21);
            this.btn_cliente.TabIndex = 3;
            this.btn_cliente.UseVisualStyleBackColor = true;
            this.btn_cliente.Click += new System.EventHandler(this.btn_cliente_Click);
            // 
            // text_cliente
            // 
            this.text_cliente.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.text_cliente.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.text_cliente.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.text_cliente.Location = new System.Drawing.Point(111, 8);
            this.text_cliente.MaxLength = 100;
            this.text_cliente.Name = "text_cliente";
            this.text_cliente.Size = new System.Drawing.Size(604, 21);
            this.text_cliente.TabIndex = 2;
            this.text_cliente.TabStop = false;
            // 
            // grb_exames
            // 
            this.grb_exames.Controls.Add(this.dgvExame);
            this.grb_exames.Location = new System.Drawing.Point(9, 35);
            this.grb_exames.Name = "grb_exames";
            this.grb_exames.Size = new System.Drawing.Size(741, 158);
            this.grb_exames.TabIndex = 6;
            this.grb_exames.TabStop = false;
            this.grb_exames.Text = "Exames";
            // 
            // dgvExame
            // 
            this.dgvExame.AllowUserToAddRows = false;
            this.dgvExame.AllowUserToDeleteRows = false;
            this.dgvExame.AllowUserToOrderColumns = true;
            this.dgvExame.AllowUserToResizeRows = false;
            this.dgvExame.BackgroundColor = System.Drawing.Color.White;
            this.dgvExame.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.Color.LightGray;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvExame.DefaultCellStyle = dataGridViewCellStyle2;
            this.dgvExame.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvExame.Location = new System.Drawing.Point(3, 16);
            this.dgvExame.MultiSelect = false;
            this.dgvExame.Name = "dgvExame";
            this.dgvExame.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvExame.Size = new System.Drawing.Size(735, 139);
            this.dgvExame.TabIndex = 5;
            this.dgvExame.CellBeginEdit += new System.Windows.Forms.DataGridViewCellCancelEventHandler(this.dgvExame_CellBeginEdit);
            this.dgvExame.CellEndEdit += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvExame_CellEndEdit);
            this.dgvExame.DataBindingComplete += new System.Windows.Forms.DataGridViewBindingCompleteEventHandler(this.dgvExame_DataBindingComplete);
            this.dgvExame.EditingControlShowing += new System.Windows.Forms.DataGridViewEditingControlShowingEventHandler(this.dgvExame_EditingControlShowing);
            this.dgvExame.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.dgvExame_KeyPress);
            // 
            // btn_incluirExame
            // 
            this.btn_incluirExame.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btn_incluirExame.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_incluirExame.Image = ((System.Drawing.Image)(resources.GetObject("btn_incluirExame.Image")));
            this.btn_incluirExame.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btn_incluirExame.Location = new System.Drawing.Point(9, 196);
            this.btn_incluirExame.Name = "btn_incluirExame";
            this.btn_incluirExame.Size = new System.Drawing.Size(75, 23);
            this.btn_incluirExame.TabIndex = 7;
            this.btn_incluirExame.Text = "&Incluir";
            this.btn_incluirExame.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btn_incluirExame.UseVisualStyleBackColor = true;
            this.btn_incluirExame.Click += new System.EventHandler(this.btn_incluirExame_Click);
            // 
            // btnExcluirExame
            // 
            this.btnExcluirExame.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnExcluirExame.Image = global::SWS.Properties.Resources.lixeira;
            this.btnExcluirExame.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnExcluirExame.Location = new System.Drawing.Point(90, 196);
            this.btnExcluirExame.Name = "btnExcluirExame";
            this.btnExcluirExame.Size = new System.Drawing.Size(75, 23);
            this.btnExcluirExame.TabIndex = 8;
            this.btnExcluirExame.Text = "&Exluir";
            this.btnExcluirExame.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnExcluirExame.UseVisualStyleBackColor = true;
            this.btnExcluirExame.Click += new System.EventHandler(this.btnExcluirExame_Click);
            // 
            // grb_produtosInternos
            // 
            this.grb_produtosInternos.Controls.Add(this.dgvProduto);
            this.grb_produtosInternos.Location = new System.Drawing.Point(9, 225);
            this.grb_produtosInternos.Name = "grb_produtosInternos";
            this.grb_produtosInternos.Size = new System.Drawing.Size(741, 173);
            this.grb_produtosInternos.TabIndex = 9;
            this.grb_produtosInternos.TabStop = false;
            this.grb_produtosInternos.Text = "Produtos Internos";
            // 
            // dgvProduto
            // 
            this.dgvProduto.AllowUserToAddRows = false;
            this.dgvProduto.AllowUserToDeleteRows = false;
            this.dgvProduto.AllowUserToOrderColumns = true;
            this.dgvProduto.BackgroundColor = System.Drawing.Color.White;
            this.dgvProduto.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.LightGray;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvProduto.DefaultCellStyle = dataGridViewCellStyle1;
            this.dgvProduto.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvProduto.Location = new System.Drawing.Point(3, 16);
            this.dgvProduto.MultiSelect = false;
            this.dgvProduto.Name = "dgvProduto";
            this.dgvProduto.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvProduto.Size = new System.Drawing.Size(735, 154);
            this.dgvProduto.TabIndex = 2;
            this.dgvProduto.CellBeginEdit += new System.Windows.Forms.DataGridViewCellCancelEventHandler(this.dgvProduto_CellBeginEdit);
            this.dgvProduto.CellEndEdit += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvProduto_CellEndEdit);
            this.dgvProduto.DataBindingComplete += new System.Windows.Forms.DataGridViewBindingCompleteEventHandler(this.dgvProduto_DataBindingComplete);
            this.dgvProduto.EditingControlShowing += new System.Windows.Forms.DataGridViewEditingControlShowingEventHandler(this.dgvProduto_EditingControlShowing);
            this.dgvProduto.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.dgvProduto_KeyPress);
            // 
            // btn_incluirProduto
            // 
            this.btn_incluirProduto.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btn_incluirProduto.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_incluirProduto.Image = ((System.Drawing.Image)(resources.GetObject("btn_incluirProduto.Image")));
            this.btn_incluirProduto.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btn_incluirProduto.Location = new System.Drawing.Point(9, 404);
            this.btn_incluirProduto.Name = "btn_incluirProduto";
            this.btn_incluirProduto.Size = new System.Drawing.Size(75, 23);
            this.btn_incluirProduto.TabIndex = 4;
            this.btn_incluirProduto.Text = "&Incluir";
            this.btn_incluirProduto.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btn_incluirProduto.UseVisualStyleBackColor = true;
            this.btn_incluirProduto.Click += new System.EventHandler(this.btn_incluirProduto_Click);
            // 
            // btnExcluirProduto
            // 
            this.btnExcluirProduto.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnExcluirProduto.Image = global::SWS.Properties.Resources.lixeira;
            this.btnExcluirProduto.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnExcluirProduto.Location = new System.Drawing.Point(90, 404);
            this.btnExcluirProduto.Name = "btnExcluirProduto";
            this.btnExcluirProduto.Size = new System.Drawing.Size(75, 23);
            this.btnExcluirProduto.TabIndex = 10;
            this.btnExcluirProduto.Text = "&Excluir";
            this.btnExcluirProduto.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnExcluirProduto.UseVisualStyleBackColor = true;
            this.btnExcluirProduto.Click += new System.EventHandler(this.btnExcluirProduto_Click);
            // 
            // frmContratosAdministrativo
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(784, 561);
            this.Name = "frmContratosAdministrativo";
            this.Text = "GERENCIAMENTO DE CONTRATOS ADMINISTRATIVOS";
            this.spForm.Panel1.ResumeLayout(false);
            this.spForm.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.spForm)).EndInit();
            this.spForm.ResumeLayout(false);
            this.pnlForm.ResumeLayout(false);
            this.pnlForm.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.banner)).EndInit();
            this.flpAcao.ResumeLayout(false);
            this.grb_exames.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvExame)).EndInit();
            this.grb_produtosInternos.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvProduto)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.FlowLayoutPanel flpAcao;
        private System.Windows.Forms.Button btn_fechar;
        private System.Windows.Forms.TextBox lblCliente;
        private System.Windows.Forms.Button btn_cliente;
        private System.Windows.Forms.TextBox text_cliente;
        public System.Windows.Forms.GroupBox grb_exames;
        private System.Windows.Forms.DataGridView dgvExame;
        private System.Windows.Forms.Button btn_incluirExame;
        private System.Windows.Forms.Button btnExcluirExame;
        public System.Windows.Forms.GroupBox grb_produtosInternos;
        private System.Windows.Forms.DataGridView dgvProduto;
        private System.Windows.Forms.Button btnExcluirProduto;
        private System.Windows.Forms.Button btn_incluirProduto;
    }
}