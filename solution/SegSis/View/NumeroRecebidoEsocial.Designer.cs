﻿namespace SWS.View
{
    partial class frmNumeroRecebidoEsocial
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.flpAcao = new System.Windows.Forms.FlowLayoutPanel();
            this.btIGravar = new System.Windows.Forms.Button();
            this.btnFechar = new System.Windows.Forms.Button();
            this.textRecibo = new System.Windows.Forms.TextBox();
            ((System.ComponentModel.ISupportInitialize)(this.scForm)).BeginInit();
            this.scForm.Panel1.SuspendLayout();
            this.scForm.Panel2.SuspendLayout();
            this.scForm.SuspendLayout();
            this.pnlForm.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.banner)).BeginInit();
            this.flpAcao.SuspendLayout();
            this.SuspendLayout();
            // 
            // scForm
            // 
            // 
            // scForm.Panel1
            // 
            this.scForm.Panel1.Controls.Add(this.flpAcao);
            // 
            // pnlForm
            // 
            this.pnlForm.Controls.Add(this.textRecibo);
            // 
            // flpAcao
            // 
            this.flpAcao.Controls.Add(this.btIGravar);
            this.flpAcao.Controls.Add(this.btnFechar);
            this.flpAcao.Dock = System.Windows.Forms.DockStyle.Fill;
            this.flpAcao.Location = new System.Drawing.Point(0, 0);
            this.flpAcao.Name = "flpAcao";
            this.flpAcao.Size = new System.Drawing.Size(534, 30);
            this.flpAcao.TabIndex = 0;
            // 
            // btIGravar
            // 
            this.btIGravar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btIGravar.Image = global::SWS.Properties.Resources.Gravar;
            this.btIGravar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btIGravar.Location = new System.Drawing.Point(3, 3);
            this.btIGravar.Name = "btIGravar";
            this.btIGravar.Size = new System.Drawing.Size(71, 22);
            this.btIGravar.TabIndex = 29;
            this.btIGravar.TabStop = false;
            this.btIGravar.Text = "&Gravar";
            this.btIGravar.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btIGravar.UseVisualStyleBackColor = true;
            this.btIGravar.Click += new System.EventHandler(this.btIGravar_Click);
            // 
            // btnFechar
            // 
            this.btnFechar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnFechar.Image = global::SWS.Properties.Resources.close;
            this.btnFechar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnFechar.Location = new System.Drawing.Point(80, 3);
            this.btnFechar.Name = "btnFechar";
            this.btnFechar.Size = new System.Drawing.Size(71, 22);
            this.btnFechar.TabIndex = 30;
            this.btnFechar.TabStop = false;
            this.btnFechar.Text = "&Fechar";
            this.btnFechar.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnFechar.UseVisualStyleBackColor = true;
            this.btnFechar.Click += new System.EventHandler(this.btnFechar_Click);
            // 
            // textRecibo
            // 
            this.textRecibo.Dock = System.Windows.Forms.DockStyle.Fill;
            this.textRecibo.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textRecibo.ForeColor = System.Drawing.Color.Blue;
            this.textRecibo.Location = new System.Drawing.Point(0, 0);
            this.textRecibo.MaxLength = 23;
            this.textRecibo.Multiline = true;
            this.textRecibo.Name = "textRecibo";
            this.textRecibo.Size = new System.Drawing.Size(514, 300);
            this.textRecibo.TabIndex = 0;
            this.textRecibo.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.textRecibo_KeyPress);
            // 
            // frmNumeroRecebidoEsocial
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(534, 361);
            this.Name = "frmNumeroRecebidoEsocial";
            this.Text = "NÚMERO DE RECIBO DO E-SOCIAL";
            this.scForm.Panel1.ResumeLayout(false);
            this.scForm.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.scForm)).EndInit();
            this.scForm.ResumeLayout(false);
            this.pnlForm.ResumeLayout(false);
            this.pnlForm.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.banner)).EndInit();
            this.flpAcao.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.FlowLayoutPanel flpAcao;
        private System.Windows.Forms.Button btIGravar;
        private System.Windows.Forms.Button btnFechar;
        private System.Windows.Forms.TextBox textRecibo;
    }
}