﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace SWS.View
{
    public partial class BaseFormConsultaAuxiliar : Form
    {
        public BaseFormConsultaAuxiliar()
        {
            this.StartPosition = FormStartPosition.CenterScreen;
            this.AutoSize = false;
            this.MaximizeBox = false;
            this.MinimizeBox = true;
            this.FormBorderStyle = FormBorderStyle.FixedSingle;
            InitializeComponent();

        }
    }
}
