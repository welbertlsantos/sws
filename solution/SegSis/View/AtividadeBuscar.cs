﻿using SWS.Entidade;
using SWS.Facade;
using SWS.View.Resources;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SWS.View
{
    public partial class frmAtividadeBuscar : frmTemplateConsulta
    {
        private Atividade atividade;

        public Atividade Atividade
        {
            get { return atividade; }
            set { atividade = value; }
        }

        public frmAtividadeBuscar()
        {
            InitializeComponent();
            ActiveControl = textAtividade;
        }

        private void btnAtividadeBuscar_Click(object sender, EventArgs e)
        {
            montaGridAtividade();
        }

        private void montaGridAtividade()
        {
            try
            {
                Atividade atividade = new Atividade();
                atividade.Descricao = textAtividade.Text.Trim();
                atividade.Situacao = ApplicationConstants.ATIVO;

                EstudoFacade pcmsoFacade = EstudoFacade.getInstance();

                DataSet ds = pcmsoFacade.findAtividadeAtivosByDescricao(atividade);

                dgvAtividade.Columns.Clear();

                dgvAtividade.ColumnCount = 2;

                dgvAtividade.Columns[0].HeaderText = "IdAtividade";
                dgvAtividade.Columns[0].Visible = false;
                dgvAtividade.Columns[1].HeaderText = "Atividade";

                foreach (DataRow row in ds.Tables[0].Rows)
                    dgvAtividade.Rows.Add(
                        row["ID_ATIVIDADE"],
                        row["DESCRICAO"].ToString());

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }

        private void btnConfirmar_Click(object sender, EventArgs e)
        {
            try
            {
                if (dgvAtividade.CurrentRow == null)
                    throw new Exception("Selecione uma atividade.");

                Atividade = new Atividade();
                Atividade.Id = (long)dgvAtividade.CurrentRow.Cells[0].Value;
                Atividade.Descricao = dgvAtividade.CurrentRow.Cells[1].Value.ToString();
                Atividade.Situacao = ApplicationConstants.ATIVO;

                this.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }

        private void btnFechar_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnNovo_Click(object sender, EventArgs e)
        {
            frmAtividadeIncluir incluirNovaAtividade = new frmAtividadeIncluir();
            incluirNovaAtividade.ShowDialog();

            if (incluirNovaAtividade.Atividade != null)
            {
                Atividade = incluirNovaAtividade.Atividade;
                this.Close();                
            }
        }
    }
}
