﻿using SWS.Entidade;
using SWS.Facade;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace SWS.View
{
    public partial class frmProcedimentoEsocialBuscar : frmTemplateConsulta
    {
        private ProcedimentoEsocial procedimentoEsocial;

        public ProcedimentoEsocial ProcedimentoEsocial
        {
            get { return procedimentoEsocial; }
            set { procedimentoEsocial = value; }
        }
        
        public frmProcedimentoEsocialBuscar()
        {
            InitializeComponent();
            montaDataGrid();
            ActiveControl = textProcedimento;
        }

        private void btnConfirmar_Click(object sender, EventArgs e)
        {
            try
            {
                if (this.dgvProcedimento.CurrentRow == null)
                    throw new Exception("selecione uma  linha.");

                ProcedimentoEsocial = new ProcedimentoEsocial((long)dgvProcedimento.CurrentRow.Cells[0].Value, (string)dgvProcedimento.CurrentRow.Cells[1].Value, (string)dgvProcedimento.CurrentRow.Cells[2].Value);
                this.Close();

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }


        }

        private void btnFechar_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void montaDataGrid()
        {
            try
            {
                this.Cursor = Cursors.WaitCursor;

                EsocialFacade esocialFacade = EsocialFacade.getInstance();

                this.dgvProcedimento.Columns.Clear();
                this.dgvProcedimento.ColumnCount = 3;
                this.dgvProcedimento.Columns[0].HeaderText = "id";
                this.dgvProcedimento.Columns[0].Visible = false;
                this.dgvProcedimento.Columns[1].HeaderText = "Codigo";
                this.dgvProcedimento.Columns[2].HeaderText = "Procedimento";

                foreach (ProcedimentoEsocial procedimento in esocialFacade.findAllProcedimentosByFilter(textProcedimento.Text.Trim()))
                    dgvProcedimento.Rows.Add(procedimento.Id, procedimento.Codigo, procedimento.Procedimento);
                

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }


        }

        private void btnProcedimento_Click(object sender, EventArgs e)
        {
            montaDataGrid();
        }

        
    }
}
