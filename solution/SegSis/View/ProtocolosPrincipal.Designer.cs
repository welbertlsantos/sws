﻿namespace SWS.View
{
    partial class frmProtocolosPrincipal
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            this.flowLayoutPanel1 = new System.Windows.Forms.FlowLayoutPanel();
            this.btn_pesquisar = new System.Windows.Forms.Button();
            this.btnLimpar = new System.Windows.Forms.Button();
            this.btn_incluir = new System.Windows.Forms.Button();
            this.btn_alterar = new System.Windows.Forms.Button();
            this.btnFinalizar = new System.Windows.Forms.Button();
            this.btn_excluir = new System.Windows.Forms.Button();
            this.btn_imprimir = new System.Windows.Forms.Button();
            this.btn_fechar = new System.Windows.Forms.Button();
            this.textCodigo = new System.Windows.Forms.MaskedTextBox();
            this.lblCodigo = new System.Windows.Forms.TextBox();
            this.cbSituacao = new SWS.ComboBoxWithBorder();
            this.lblSituacao = new System.Windows.Forms.TextBox();
            this.lblDataGravacao = new System.Windows.Forms.TextBox();
            this.dataGravacaoFinal = new System.Windows.Forms.DateTimePicker();
            this.dataGravacaoInicial = new System.Windows.Forms.DateTimePicker();
            this.lblDataCancelamento = new System.Windows.Forms.TextBox();
            this.dataCancelamentoInicial = new System.Windows.Forms.DateTimePicker();
            this.dataCancelamentoFinal = new System.Windows.Forms.DateTimePicker();
            this.btnExcluirCliente = new System.Windows.Forms.Button();
            this.btCliente = new System.Windows.Forms.Button();
            this.textCliente = new System.Windows.Forms.TextBox();
            this.lblCliente = new System.Windows.Forms.TextBox();
            this.grbAtendimento = new System.Windows.Forms.GroupBox();
            this.dgvProtocolo = new System.Windows.Forms.DataGridView();
            this.grbLegenda = new System.Windows.Forms.GroupBox();
            this.lblTextConstrucao = new System.Windows.Forms.Label();
            this.lblLegendaConstrucao = new System.Windows.Forms.Label();
            this.lblAtendimentoFinalizado = new System.Windows.Forms.Label();
            this.lblLegendaFinalizado = new System.Windows.Forms.Label();
            this.lblAsoCancelado = new System.Windows.Forms.Label();
            this.lblLegendaCancelado = new System.Windows.Forms.Label();
            this.lblObservacao = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.spForm)).BeginInit();
            this.spForm.Panel1.SuspendLayout();
            this.spForm.Panel2.SuspendLayout();
            this.spForm.SuspendLayout();
            this.pnlForm.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.banner)).BeginInit();
            this.flowLayoutPanel1.SuspendLayout();
            this.grbAtendimento.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvProtocolo)).BeginInit();
            this.grbLegenda.SuspendLayout();
            this.SuspendLayout();
            // 
            // spForm
            // 
            // 
            // spForm.Panel1
            // 
            this.spForm.Panel1.Controls.Add(this.flowLayoutPanel1);
            // 
            // pnlForm
            // 
            this.pnlForm.Controls.Add(this.lblObservacao);
            this.pnlForm.Controls.Add(this.grbLegenda);
            this.pnlForm.Controls.Add(this.btnExcluirCliente);
            this.pnlForm.Controls.Add(this.btCliente);
            this.pnlForm.Controls.Add(this.textCliente);
            this.pnlForm.Controls.Add(this.lblCliente);
            this.pnlForm.Controls.Add(this.grbAtendimento);
            this.pnlForm.Controls.Add(this.lblDataCancelamento);
            this.pnlForm.Controls.Add(this.dataCancelamentoInicial);
            this.pnlForm.Controls.Add(this.dataCancelamentoFinal);
            this.pnlForm.Controls.Add(this.lblDataGravacao);
            this.pnlForm.Controls.Add(this.dataGravacaoFinal);
            this.pnlForm.Controls.Add(this.dataGravacaoInicial);
            this.pnlForm.Controls.Add(this.cbSituacao);
            this.pnlForm.Controls.Add(this.lblSituacao);
            this.pnlForm.Controls.Add(this.textCodigo);
            this.pnlForm.Controls.Add(this.lblCodigo);
            // 
            // flowLayoutPanel1
            // 
            this.flowLayoutPanel1.Controls.Add(this.btn_pesquisar);
            this.flowLayoutPanel1.Controls.Add(this.btnLimpar);
            this.flowLayoutPanel1.Controls.Add(this.btn_incluir);
            this.flowLayoutPanel1.Controls.Add(this.btn_alterar);
            this.flowLayoutPanel1.Controls.Add(this.btnFinalizar);
            this.flowLayoutPanel1.Controls.Add(this.btn_excluir);
            this.flowLayoutPanel1.Controls.Add(this.btn_imprimir);
            this.flowLayoutPanel1.Controls.Add(this.btn_fechar);
            this.flowLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.flowLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.flowLayoutPanel1.Name = "flowLayoutPanel1";
            this.flowLayoutPanel1.Size = new System.Drawing.Size(784, 35);
            this.flowLayoutPanel1.TabIndex = 0;
            // 
            // btn_pesquisar
            // 
            this.btn_pesquisar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_pesquisar.Image = global::SWS.Properties.Resources.lupa;
            this.btn_pesquisar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btn_pesquisar.Location = new System.Drawing.Point(3, 3);
            this.btn_pesquisar.Name = "btn_pesquisar";
            this.btn_pesquisar.Size = new System.Drawing.Size(75, 23);
            this.btn_pesquisar.TabIndex = 1;
            this.btn_pesquisar.Text = "&Pesquisar";
            this.btn_pesquisar.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btn_pesquisar.UseVisualStyleBackColor = true;
            this.btn_pesquisar.Click += new System.EventHandler(this.btn_pesquisar_Click);
            // 
            // btnLimpar
            // 
            this.btnLimpar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnLimpar.Image = global::SWS.Properties.Resources.vassoura;
            this.btnLimpar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnLimpar.Location = new System.Drawing.Point(84, 3);
            this.btnLimpar.Name = "btnLimpar";
            this.btnLimpar.Size = new System.Drawing.Size(75, 23);
            this.btnLimpar.TabIndex = 25;
            this.btnLimpar.TabStop = false;
            this.btnLimpar.Text = "&Limpar";
            this.btnLimpar.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnLimpar.UseVisualStyleBackColor = true;
            this.btnLimpar.Click += new System.EventHandler(this.btnLimpar_Click);
            // 
            // btn_incluir
            // 
            this.btn_incluir.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_incluir.Image = global::SWS.Properties.Resources.icone_mais;
            this.btn_incluir.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btn_incluir.Location = new System.Drawing.Point(165, 3);
            this.btn_incluir.Name = "btn_incluir";
            this.btn_incluir.Size = new System.Drawing.Size(75, 23);
            this.btn_incluir.TabIndex = 2;
            this.btn_incluir.Text = "&Novo";
            this.btn_incluir.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btn_incluir.UseVisualStyleBackColor = true;
            this.btn_incluir.Click += new System.EventHandler(this.btn_incluir_Click);
            // 
            // btn_alterar
            // 
            this.btn_alterar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_alterar.Image = global::SWS.Properties.Resources.Alterar;
            this.btn_alterar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btn_alterar.Location = new System.Drawing.Point(246, 3);
            this.btn_alterar.Name = "btn_alterar";
            this.btn_alterar.Size = new System.Drawing.Size(75, 23);
            this.btn_alterar.TabIndex = 3;
            this.btn_alterar.Text = "&Alterar";
            this.btn_alterar.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btn_alterar.UseVisualStyleBackColor = true;
            this.btn_alterar.Click += new System.EventHandler(this.btn_alterar_Click);
            // 
            // btnFinalizar
            // 
            this.btnFinalizar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnFinalizar.Image = global::SWS.Properties.Resources.fechar_ico;
            this.btnFinalizar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnFinalizar.Location = new System.Drawing.Point(327, 3);
            this.btnFinalizar.Name = "btnFinalizar";
            this.btnFinalizar.Size = new System.Drawing.Size(75, 23);
            this.btnFinalizar.TabIndex = 4;
            this.btnFinalizar.Text = "&Finalizar";
            this.btnFinalizar.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnFinalizar.UseVisualStyleBackColor = true;
            this.btnFinalizar.Click += new System.EventHandler(this.btnFinalizar_Click);
            // 
            // btn_excluir
            // 
            this.btn_excluir.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_excluir.Image = global::SWS.Properties.Resources.Icone_cancelar;
            this.btn_excluir.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btn_excluir.Location = new System.Drawing.Point(408, 3);
            this.btn_excluir.Name = "btn_excluir";
            this.btn_excluir.Size = new System.Drawing.Size(75, 23);
            this.btn_excluir.TabIndex = 5;
            this.btn_excluir.Text = "&Excluir";
            this.btn_excluir.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btn_excluir.UseVisualStyleBackColor = true;
            this.btn_excluir.Click += new System.EventHandler(this.btn_excluir_Click);
            // 
            // btn_imprimir
            // 
            this.btn_imprimir.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_imprimir.Image = global::SWS.Properties.Resources.impressora1;
            this.btn_imprimir.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btn_imprimir.Location = new System.Drawing.Point(489, 3);
            this.btn_imprimir.Name = "btn_imprimir";
            this.btn_imprimir.Size = new System.Drawing.Size(75, 23);
            this.btn_imprimir.TabIndex = 6;
            this.btn_imprimir.Text = "&Imprimir";
            this.btn_imprimir.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btn_imprimir.UseVisualStyleBackColor = true;
            this.btn_imprimir.Click += new System.EventHandler(this.btn_imprimir_Click);
            // 
            // btn_fechar
            // 
            this.btn_fechar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_fechar.Image = global::SWS.Properties.Resources.close;
            this.btn_fechar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btn_fechar.Location = new System.Drawing.Point(570, 3);
            this.btn_fechar.Name = "btn_fechar";
            this.btn_fechar.Size = new System.Drawing.Size(75, 23);
            this.btn_fechar.TabIndex = 23;
            this.btn_fechar.TabStop = false;
            this.btn_fechar.Text = "&Fechar";
            this.btn_fechar.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btn_fechar.UseVisualStyleBackColor = true;
            this.btn_fechar.Click += new System.EventHandler(this.btn_fechar_Click);
            // 
            // textCodigo
            // 
            this.textCodigo.BackColor = System.Drawing.Color.White;
            this.textCodigo.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textCodigo.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textCodigo.Location = new System.Drawing.Point(140, 3);
            this.textCodigo.Mask = "9999,99,999999";
            this.textCodigo.Name = "textCodigo";
            this.textCodigo.PromptChar = ' ';
            this.textCodigo.Size = new System.Drawing.Size(213, 21);
            this.textCodigo.TabIndex = 1;
            // 
            // lblCodigo
            // 
            this.lblCodigo.BackColor = System.Drawing.Color.LightGray;
            this.lblCodigo.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblCodigo.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCodigo.Location = new System.Drawing.Point(3, 3);
            this.lblCodigo.Name = "lblCodigo";
            this.lblCodigo.ReadOnly = true;
            this.lblCodigo.Size = new System.Drawing.Size(138, 21);
            this.lblCodigo.TabIndex = 2;
            this.lblCodigo.TabStop = false;
            this.lblCodigo.Text = "Código";
            // 
            // cbSituacao
            // 
            this.cbSituacao.BackColor = System.Drawing.Color.LightGray;
            this.cbSituacao.BorderColor = System.Drawing.Color.DimGray;
            this.cbSituacao.BorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid;
            this.cbSituacao.FormattingEnabled = true;
            this.cbSituacao.Location = new System.Drawing.Point(140, 23);
            this.cbSituacao.Name = "cbSituacao";
            this.cbSituacao.Size = new System.Drawing.Size(213, 21);
            this.cbSituacao.TabIndex = 2;
            // 
            // lblSituacao
            // 
            this.lblSituacao.BackColor = System.Drawing.Color.LightGray;
            this.lblSituacao.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblSituacao.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSituacao.Location = new System.Drawing.Point(3, 23);
            this.lblSituacao.Name = "lblSituacao";
            this.lblSituacao.ReadOnly = true;
            this.lblSituacao.Size = new System.Drawing.Size(138, 21);
            this.lblSituacao.TabIndex = 10;
            this.lblSituacao.TabStop = false;
            this.lblSituacao.Text = "Situação";
            // 
            // lblDataGravacao
            // 
            this.lblDataGravacao.BackColor = System.Drawing.Color.LightGray;
            this.lblDataGravacao.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblDataGravacao.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDataGravacao.Location = new System.Drawing.Point(426, 23);
            this.lblDataGravacao.Name = "lblDataGravacao";
            this.lblDataGravacao.ReadOnly = true;
            this.lblDataGravacao.Size = new System.Drawing.Size(120, 21);
            this.lblDataGravacao.TabIndex = 14;
            this.lblDataGravacao.TabStop = false;
            this.lblDataGravacao.Text = "Gravação";
            // 
            // dataGravacaoFinal
            // 
            this.dataGravacaoFinal.Checked = false;
            this.dataGravacaoFinal.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dataGravacaoFinal.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dataGravacaoFinal.Location = new System.Drawing.Point(651, 23);
            this.dataGravacaoFinal.Name = "dataGravacaoFinal";
            this.dataGravacaoFinal.ShowCheckBox = true;
            this.dataGravacaoFinal.Size = new System.Drawing.Size(107, 21);
            this.dataGravacaoFinal.TabIndex = 8;
            // 
            // dataGravacaoInicial
            // 
            this.dataGravacaoInicial.Checked = false;
            this.dataGravacaoInicial.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dataGravacaoInicial.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dataGravacaoInicial.Location = new System.Drawing.Point(545, 23);
            this.dataGravacaoInicial.Name = "dataGravacaoInicial";
            this.dataGravacaoInicial.ShowCheckBox = true;
            this.dataGravacaoInicial.Size = new System.Drawing.Size(107, 21);
            this.dataGravacaoInicial.TabIndex = 7;
            // 
            // lblDataCancelamento
            // 
            this.lblDataCancelamento.BackColor = System.Drawing.Color.LightGray;
            this.lblDataCancelamento.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblDataCancelamento.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDataCancelamento.Location = new System.Drawing.Point(426, 3);
            this.lblDataCancelamento.Name = "lblDataCancelamento";
            this.lblDataCancelamento.ReadOnly = true;
            this.lblDataCancelamento.Size = new System.Drawing.Size(120, 21);
            this.lblDataCancelamento.TabIndex = 15;
            this.lblDataCancelamento.TabStop = false;
            this.lblDataCancelamento.Text = "Cancelamento";
            // 
            // dataCancelamentoInicial
            // 
            this.dataCancelamentoInicial.Checked = false;
            this.dataCancelamentoInicial.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dataCancelamentoInicial.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dataCancelamentoInicial.Location = new System.Drawing.Point(545, 3);
            this.dataCancelamentoInicial.Name = "dataCancelamentoInicial";
            this.dataCancelamentoInicial.ShowCheckBox = true;
            this.dataCancelamentoInicial.Size = new System.Drawing.Size(107, 21);
            this.dataCancelamentoInicial.TabIndex = 5;
            // 
            // dataCancelamentoFinal
            // 
            this.dataCancelamentoFinal.Checked = false;
            this.dataCancelamentoFinal.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dataCancelamentoFinal.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dataCancelamentoFinal.Location = new System.Drawing.Point(651, 3);
            this.dataCancelamentoFinal.Name = "dataCancelamentoFinal";
            this.dataCancelamentoFinal.ShowCheckBox = true;
            this.dataCancelamentoFinal.Size = new System.Drawing.Size(107, 21);
            this.dataCancelamentoFinal.TabIndex = 6;
            // 
            // btnExcluirCliente
            // 
            this.btnExcluirCliente.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnExcluirCliente.Image = global::SWS.Properties.Resources.close;
            this.btnExcluirCliente.Location = new System.Drawing.Point(381, 43);
            this.btnExcluirCliente.Name = "btnExcluirCliente";
            this.btnExcluirCliente.Size = new System.Drawing.Size(30, 21);
            this.btnExcluirCliente.TabIndex = 40;
            this.btnExcluirCliente.TabStop = false;
            this.btnExcluirCliente.UseVisualStyleBackColor = true;
            this.btnExcluirCliente.Click += new System.EventHandler(this.btnExcluirCliente_Click);
            // 
            // btCliente
            // 
            this.btCliente.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btCliente.Image = global::SWS.Properties.Resources.busca;
            this.btCliente.Location = new System.Drawing.Point(352, 43);
            this.btCliente.Name = "btCliente";
            this.btCliente.Size = new System.Drawing.Size(30, 21);
            this.btCliente.TabIndex = 3;
            this.btCliente.UseVisualStyleBackColor = true;
            this.btCliente.Click += new System.EventHandler(this.btCliente_Click);
            // 
            // textCliente
            // 
            this.textCliente.BackColor = System.Drawing.SystemColors.Control;
            this.textCliente.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textCliente.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textCliente.Location = new System.Drawing.Point(140, 43);
            this.textCliente.MaxLength = 100;
            this.textCliente.Name = "textCliente";
            this.textCliente.ReadOnly = true;
            this.textCliente.Size = new System.Drawing.Size(213, 21);
            this.textCliente.TabIndex = 39;
            this.textCliente.TabStop = false;
            // 
            // lblCliente
            // 
            this.lblCliente.BackColor = System.Drawing.Color.LightGray;
            this.lblCliente.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblCliente.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCliente.Location = new System.Drawing.Point(3, 43);
            this.lblCliente.Name = "lblCliente";
            this.lblCliente.ReadOnly = true;
            this.lblCliente.Size = new System.Drawing.Size(138, 21);
            this.lblCliente.TabIndex = 37;
            this.lblCliente.TabStop = false;
            this.lblCliente.Text = "Cliente";
            // 
            // grbAtendimento
            // 
            this.grbAtendimento.Controls.Add(this.dgvProtocolo);
            this.grbAtendimento.Location = new System.Drawing.Point(3, 90);
            this.grbAtendimento.Name = "grbAtendimento";
            this.grbAtendimento.Size = new System.Drawing.Size(761, 318);
            this.grbAtendimento.TabIndex = 9;
            this.grbAtendimento.TabStop = false;
            this.grbAtendimento.Text = "Atendimentos";
            // 
            // dgvProtocolo
            // 
            this.dgvProtocolo.AllowUserToAddRows = false;
            this.dgvProtocolo.AllowUserToDeleteRows = false;
            this.dgvProtocolo.AllowUserToOrderColumns = true;
            this.dgvProtocolo.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this.dgvProtocolo.BackgroundColor = System.Drawing.Color.White;
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle3.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle3.Font = new System.Drawing.Font("Arial", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvProtocolo.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle3;
            this.dgvProtocolo.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle4.BackColor = System.Drawing.Color.LightGray;
            dataGridViewCellStyle4.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle4.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle4.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle4.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle4.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvProtocolo.DefaultCellStyle = dataGridViewCellStyle4;
            this.dgvProtocolo.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvProtocolo.Location = new System.Drawing.Point(3, 16);
            this.dgvProtocolo.MultiSelect = false;
            this.dgvProtocolo.Name = "dgvProtocolo";
            this.dgvProtocolo.ReadOnly = true;
            this.dgvProtocolo.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvProtocolo.Size = new System.Drawing.Size(755, 299);
            this.dgvProtocolo.TabIndex = 9;
            this.dgvProtocolo.CellMouseDoubleClick += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.dgvProtocolo_CellMouseDoubleClick);
            this.dgvProtocolo.RowPrePaint += new System.Windows.Forms.DataGridViewRowPrePaintEventHandler(this.dgvProtocolo_RowPrePaint);
            // 
            // grbLegenda
            // 
            this.grbLegenda.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.grbLegenda.Controls.Add(this.lblTextConstrucao);
            this.grbLegenda.Controls.Add(this.lblLegendaConstrucao);
            this.grbLegenda.Controls.Add(this.lblAtendimentoFinalizado);
            this.grbLegenda.Controls.Add(this.lblLegendaFinalizado);
            this.grbLegenda.Controls.Add(this.lblAsoCancelado);
            this.grbLegenda.Controls.Add(this.lblLegendaCancelado);
            this.grbLegenda.Location = new System.Drawing.Point(6, 414);
            this.grbLegenda.Name = "grbLegenda";
            this.grbLegenda.Size = new System.Drawing.Size(405, 47);
            this.grbLegenda.TabIndex = 45;
            this.grbLegenda.TabStop = false;
            this.grbLegenda.Text = "Legenda";
            // 
            // lblTextConstrucao
            // 
            this.lblTextConstrucao.AutoSize = true;
            this.lblTextConstrucao.Location = new System.Drawing.Point(28, 20);
            this.lblTextConstrucao.Name = "lblTextConstrucao";
            this.lblTextConstrucao.Size = new System.Drawing.Size(61, 13);
            this.lblTextConstrucao.TabIndex = 37;
            this.lblTextConstrucao.Text = "Construção";
            // 
            // lblLegendaConstrucao
            // 
            this.lblLegendaConstrucao.AutoSize = true;
            this.lblLegendaConstrucao.BackColor = System.Drawing.Color.Black;
            this.lblLegendaConstrucao.Location = new System.Drawing.Point(9, 20);
            this.lblLegendaConstrucao.Name = "lblLegendaConstrucao";
            this.lblLegendaConstrucao.Size = new System.Drawing.Size(13, 13);
            this.lblLegendaConstrucao.TabIndex = 36;
            this.lblLegendaConstrucao.Text = "  ";
            // 
            // lblAtendimentoFinalizado
            // 
            this.lblAtendimentoFinalizado.AutoSize = true;
            this.lblAtendimentoFinalizado.Location = new System.Drawing.Point(111, 20);
            this.lblAtendimentoFinalizado.Name = "lblAtendimentoFinalizado";
            this.lblAtendimentoFinalizado.Size = new System.Drawing.Size(54, 13);
            this.lblAtendimentoFinalizado.TabIndex = 35;
            this.lblAtendimentoFinalizado.Text = "Finalizado";
            // 
            // lblLegendaFinalizado
            // 
            this.lblLegendaFinalizado.AutoSize = true;
            this.lblLegendaFinalizado.BackColor = System.Drawing.Color.Green;
            this.lblLegendaFinalizado.Location = new System.Drawing.Point(92, 20);
            this.lblLegendaFinalizado.Name = "lblLegendaFinalizado";
            this.lblLegendaFinalizado.Size = new System.Drawing.Size(13, 13);
            this.lblLegendaFinalizado.TabIndex = 34;
            this.lblLegendaFinalizado.Text = "  ";
            // 
            // lblAsoCancelado
            // 
            this.lblAsoCancelado.AutoSize = true;
            this.lblAsoCancelado.Location = new System.Drawing.Point(195, 20);
            this.lblAsoCancelado.Name = "lblAsoCancelado";
            this.lblAsoCancelado.Size = new System.Drawing.Size(58, 13);
            this.lblAsoCancelado.TabIndex = 27;
            this.lblAsoCancelado.Text = "Cancelado";
            // 
            // lblLegendaCancelado
            // 
            this.lblLegendaCancelado.AutoSize = true;
            this.lblLegendaCancelado.BackColor = System.Drawing.Color.Red;
            this.lblLegendaCancelado.Location = new System.Drawing.Point(176, 20);
            this.lblLegendaCancelado.Name = "lblLegendaCancelado";
            this.lblLegendaCancelado.Size = new System.Drawing.Size(13, 13);
            this.lblLegendaCancelado.TabIndex = 31;
            this.lblLegendaCancelado.Text = "  ";
            // 
            // lblObservacao
            // 
            this.lblObservacao.Location = new System.Drawing.Point(440, 421);
            this.lblObservacao.Name = "lblObservacao";
            this.lblObservacao.Size = new System.Drawing.Size(321, 40);
            this.lblObservacao.TabIndex = 46;
            this.lblObservacao.Text = "Duplo clique no protocolo selecionado para visualizar\r\na observação gravada. Some" +
    "nte para protocolos já finalizados.";
            this.lblObservacao.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // frmProtocolosPrincipal
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(784, 561);
            this.Name = "frmProtocolosPrincipal";
            this.Text = "GERENCIAR PROTOCOLOS";
            this.spForm.Panel1.ResumeLayout(false);
            this.spForm.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.spForm)).EndInit();
            this.spForm.ResumeLayout(false);
            this.pnlForm.ResumeLayout(false);
            this.pnlForm.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.banner)).EndInit();
            this.flowLayoutPanel1.ResumeLayout(false);
            this.grbAtendimento.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvProtocolo)).EndInit();
            this.grbLegenda.ResumeLayout(false);
            this.grbLegenda.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel1;
        private System.Windows.Forms.Button btnFinalizar;
        private System.Windows.Forms.Button btn_fechar;
        private System.Windows.Forms.Button btn_imprimir;
        private System.Windows.Forms.Button btn_excluir;
        private System.Windows.Forms.Button btn_alterar;
        private System.Windows.Forms.Button btn_incluir;
        public System.Windows.Forms.MaskedTextBox textCodigo;
        private System.Windows.Forms.TextBox lblCodigo;
        private ComboBoxWithBorder cbSituacao;
        private System.Windows.Forms.TextBox lblSituacao;
        private System.Windows.Forms.TextBox lblDataGravacao;
        private System.Windows.Forms.DateTimePicker dataGravacaoFinal;
        private System.Windows.Forms.DateTimePicker dataGravacaoInicial;
        private System.Windows.Forms.TextBox lblDataCancelamento;
        private System.Windows.Forms.DateTimePicker dataCancelamentoInicial;
        private System.Windows.Forms.DateTimePicker dataCancelamentoFinal;
        private System.Windows.Forms.Button btnExcluirCliente;
        private System.Windows.Forms.Button btCliente;
        public System.Windows.Forms.TextBox textCliente;
        private System.Windows.Forms.TextBox lblCliente;
        private System.Windows.Forms.Button btn_pesquisar;
        private System.Windows.Forms.Button btnLimpar;
        private System.Windows.Forms.GroupBox grbAtendimento;
        public System.Windows.Forms.DataGridView dgvProtocolo;
        private System.Windows.Forms.GroupBox grbLegenda;
        private System.Windows.Forms.Label lblTextConstrucao;
        private System.Windows.Forms.Label lblLegendaConstrucao;
        private System.Windows.Forms.Label lblAtendimentoFinalizado;
        private System.Windows.Forms.Label lblLegendaFinalizado;
        private System.Windows.Forms.Label lblAsoCancelado;
        private System.Windows.Forms.Label lblLegendaCancelado;
        private System.Windows.Forms.Label lblObservacao;
    }
}