﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using SWS.Facade;
using SWS.Entidade;
using SWS.View.Resources;

namespace SWS.View
{
    public partial class frm_ClienteFuncaoIncluirFuncao : BaseFormConsulta
    {
        frm_ppraIncluirOld formPpraIncluir;
        frm_PcmsoSemPpraIncluir formPcmsoSemPpraIncluir;
        
        public static String Msg01 = "Selecione uma linha";
        public static String Msg02 = "Atenção";

        Funcao funcaoSelecionada = null;
        ClienteFuncao clienteFuncao = null;

        Int16 formSelecionado = 0;
        
        Cliente clienteProcurado;
        
        public frm_ClienteFuncaoIncluirFuncao(frm_ppraIncluirOld formPpraIncluir)
        {
            InitializeComponent();
            this.formPpraIncluir = formPpraIncluir;
            clienteProcurado = formPpraIncluir.Ppra.VendedorCliente.getCliente();
            validaPermissoes();

            formSelecionado = 1;
        }

        public frm_ClienteFuncaoIncluirFuncao(frm_PcmsoSemPpraIncluir formPcmsoSemPpraIncluir)
        {
            InitializeComponent();
            this.formPcmsoSemPpraIncluir = formPcmsoSemPpraIncluir;
            clienteProcurado = formPcmsoSemPpraIncluir.pcmso.VendedorCliente.getCliente();
            validaPermissoes();

            formSelecionado = 2;
        }

        private void validaPermissoes()
        {
            PermissionamentoFacade permissionamentoFacade = PermissionamentoFacade.getInstance();

            this.btn_novo.Enabled = permissionamentoFacade.hasPermission("FUNCAO", "INCLUIR");
        }
        

        private void btn_buscar_Click(object sender, EventArgs e)
        {
            try
            {
                this.Cursor = Cursors.WaitCursor;
                montaDataGrid();
                text_descricao.Focus();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }

        private void btn_incluir_Click(object sender, EventArgs e)
        {
            try
            {
                PpraFacade ppraFacade = PpraFacade.getInstance();

                

                if (grd_funcao.CurrentRow != null)
                {
                    foreach (DataGridViewRow dvRow in grd_funcao.SelectedRows)
                    {
                        Int32 cellValue = dvRow.Index;

                        Int64 id = (Int64)grd_funcao.Rows[cellValue].Cells[0].Value;
                        String descricao = (String)grd_funcao.Rows[cellValue].Cells[1].Value;
                        String cbo = (String)grd_funcao.Rows[cellValue].Cells[2].Value;

                        funcaoSelecionada = new Funcao(id, descricao, cbo, ApplicationConstants.ATIVO,null);

                        clienteFuncao = new ClienteFuncao(null, clienteProcurado, funcaoSelecionada, ApplicationConstants.ATIVO, null, null);

                        ppraFacade.insertClienteFuncao(clienteFuncao);

                    }

                    if (formSelecionado == 1)
                    {
                        formPpraIncluir.montaGridFuncao();
                        formPpraIncluir.grd_setorFuncao.Columns.Clear();
                    }
                    
                    if (formSelecionado == 2)
                    {
                        formPcmsoSemPpraIncluir.montaGridFuncao();
                        formPcmsoSemPpraIncluir.grd_setorFuncao.Columns.Clear();
                    }

                    this.Close();
                }
                else
                {
                    MessageBox.Show(Msg01, Msg02);
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void btn_novo_Click(object sender, EventArgs e)
        {
            try
            {
                this.Cursor = Cursors.WaitCursor;

                PpraFacade ppraFacade = PpraFacade.getInstance();

                frmFuncaoIncluir formFuncaoInclui = new frmFuncaoIncluir();
                formFuncaoInclui.ShowDialog();

                funcaoSelecionada = formFuncaoInclui.Funcao;

                if (funcaoSelecionada != null)
                {
                    clienteFuncao = new ClienteFuncao(null, clienteProcurado, funcaoSelecionada, ApplicationConstants.ATIVO, null, null);
                    ppraFacade.insertClienteFuncao(clienteFuncao);
                    montaDataGrid();
                }
                
                this.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }

        }

        private void btn_fechar_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void textBox1_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!Char.IsDigit(e.KeyChar) && e.KeyChar != (char)8)
            {
                e.Handled = true;

            }
        }


        public void montaDataGrid()
        {

            PpraFacade ppraFacade = PpraFacade.getInstance();

            Funcao funcao = new Funcao();

            grd_funcao.Columns.Clear();

            funcao.Descricao = text_descricao.Text.ToUpper();
            funcao.CodCbo = text_cbo.Text;

            DataSet ds = ppraFacade.findAllFuncaoAtivaNotInClient(funcao,clienteProcurado);
                
            grd_funcao.DataSource = ds.Tables["Funcoes"].DefaultView;

            grd_funcao.Columns[0].HeaderText = "id_funcao";
            grd_funcao.Columns[0].Visible = false;

            grd_funcao.Columns[1].HeaderText = "Descrição";
            
            grd_funcao.Columns[2].HeaderText = "Cod_CBO";
            
            grd_funcao.Columns[3].HeaderText = "Situação";
            grd_funcao.Columns[3].Visible = false;

            grd_funcao.Columns[4].HeaderText = "Comentário";
            grd_funcao.Columns[4].Visible = false;

            grd_funcao.Columns[5].HeaderText = "Selec";
            grd_funcao.Columns[5].Visible = false;
            
        }

    }
}
