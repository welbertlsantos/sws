﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using SWS.Facade;
using SWS.Entidade;
using SWS.Excecao;
using SWS.View.ViewHelper;
using System.Text.RegularExpressions;
using SWS.View.Resources;

namespace SWS.View
{
    public partial class frm_ppraUsuarioEngenheiroSelecionar : BaseFormConsulta
    {

        private static String Msg01 = " Selecione um linha";
        private static string Msg02 = " Atenção";

        Usuario engenheiro = null;

        public Usuario getEngenheiro()
        {
            return this.engenheiro;
        }
        
        public frm_ppraUsuarioEngenheiroSelecionar()
        {
            InitializeComponent();
        }

        private void btn_fechar_Click(object sender, EventArgs e)
        {
            this.Close();

        }

        private void btn_buscar_Click(object sender, EventArgs e)
        {
            try
            {
                this.grd_usuario.Columns.Clear();
                this.Cursor = Cursors.WaitCursor;
                montaDataGrid();
                text_nome.Focus();
            }
            catch (Exception Ex)
            {
                MessageBox.Show(Ex.Message);
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }

        public void montaDataGrid()
        {
            Int64 idFuncaoProcurada = 1; // id funcao interna técnico de seguranca.

            UsuarioFacade usuarioFacade = UsuarioFacade.getInstance();

            DataSet ds = usuarioFacade.findUsuarioFuncaoAtivoByFilter(new Usuario(null, text_nome.Text.Trim(), String.Empty, String.Empty, String.Empty, null, String.Empty, String.Empty, String.Empty, String.Empty, null,null, String.Empty, false, null,String.Empty, false, false, string.Empty, string.Empty), idFuncaoProcurada);

            grd_usuario.DataSource = ds.Tables["Usuarios"].DefaultView;
            
            grd_usuario.Columns[0].HeaderText = "ID";
            grd_usuario.Columns[0].Visible = false;

            grd_usuario.Columns[1].HeaderText = "Nome do Usuário";
        }

        private void btn_ok_Click(object sender, EventArgs e)
        {
            try
            {
                if (grd_usuario.CurrentRow != null)
                {
                    UsuarioFacade usuarioFacade = UsuarioFacade.getInstance();
                    
                    Int64 id = (Int64)grd_usuario.CurrentRow.Cells[0].Value; // id do item selecionado.

                    engenheiro = usuarioFacade.findUsuarioById(id);

                    this.Close();

                }
                else
                {
                    MessageBox.Show(Msg01, Msg02);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

    }
}
