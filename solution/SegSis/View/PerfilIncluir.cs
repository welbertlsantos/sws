﻿using SWS.Entidade;
using SWS.Facade;
using SWS.View.Resources;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace SWS.View
{
    public partial class frmPerfilIncluir : frmTemplate
    {
        private Perfil perfil;

        public Perfil Perfil
        {
            get { return perfil; }
            set { perfil = value; }
        }
        
        public frmPerfilIncluir()
        {
            InitializeComponent();
            ActiveControl = textPerfil;
        }

        protected virtual void btnGravar_Click(object sender, EventArgs e)
        {
            try
            {
                if (ValidaCampos())
                {
                    UsuarioFacade usuarioFacade = UsuarioFacade.getInstance();
                    perfil = usuarioFacade.incluirPerfil(new Perfil(null, textPerfil.Text, ApplicationConstants.ATIVO));
                    MessageBox.Show("Perfil incluído com sucesso.", "Confirmação", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    this.Close();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        protected void btnFechar_Click(object sender, EventArgs e)
        {
            this.Close();
        }


        protected bool ValidaCampos()
        { 
            bool result = true;

            try
            {
                if (string.IsNullOrEmpty(textPerfil.Text.Trim()))
                    throw new Exception("Campo nome do perfil é obrigatório.");

            }
            catch (Exception ex)
            {
                result = false;
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            return result;
        }

        protected void frmPerfilIncluir_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter || e.KeyCode == Keys.Return)
                SendKeys.Send("{TAB}");

        }
    }
}
