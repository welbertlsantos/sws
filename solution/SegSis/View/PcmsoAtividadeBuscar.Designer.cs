﻿namespace SWS.View
{
    partial class frm_PcmsoAtividadeBuscar
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frm_PcmsoAtividadeBuscar));
            this.grb_dado = new System.Windows.Forms.GroupBox();
            this.lbl_descricao = new System.Windows.Forms.Label();
            this.btn_buscar = new System.Windows.Forms.Button();
            this.text_descricao = new System.Windows.Forms.TextBox();
            this.grb_atividade = new System.Windows.Forms.GroupBox();
            this.grd_atividade = new System.Windows.Forms.DataGridView();
            this.grb_paginacao = new System.Windows.Forms.GroupBox();
            this.btn_fechar = new System.Windows.Forms.Button();
            this.btn_novo = new System.Windows.Forms.Button();
            this.btn_incluir = new System.Windows.Forms.Button();
            this.grb_dado.SuspendLayout();
            this.grb_atividade.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grd_atividade)).BeginInit();
            this.grb_paginacao.SuspendLayout();
            this.SuspendLayout();
            // 
            // grb_dado
            // 
            this.grb_dado.Controls.Add(this.lbl_descricao);
            this.grb_dado.Controls.Add(this.btn_buscar);
            this.grb_dado.Controls.Add(this.text_descricao);
            this.grb_dado.Location = new System.Drawing.Point(13, 13);
            this.grb_dado.Name = "grb_dado";
            this.grb_dado.Size = new System.Drawing.Size(575, 92);
            this.grb_dado.TabIndex = 0;
            this.grb_dado.TabStop = false;
            this.grb_dado.Text = "Filtro";
            // 
            // lbl_descricao
            // 
            this.lbl_descricao.AutoSize = true;
            this.lbl_descricao.Location = new System.Drawing.Point(6, 16);
            this.lbl_descricao.Name = "lbl_descricao";
            this.lbl_descricao.Size = new System.Drawing.Size(55, 13);
            this.lbl_descricao.TabIndex = 3;
            this.lbl_descricao.Text = "Descrição";
            // 
            // btn_buscar
            // 
            this.btn_buscar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_buscar.Image = global::SWS.Properties.Resources.lupa;
            this.btn_buscar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btn_buscar.Location = new System.Drawing.Point(6, 58);
            this.btn_buscar.Name = "btn_buscar";
            this.btn_buscar.Size = new System.Drawing.Size(75, 23);
            this.btn_buscar.TabIndex = 2;
            this.btn_buscar.Text = "&Buscar";
            this.btn_buscar.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btn_buscar.UseVisualStyleBackColor = true;
            this.btn_buscar.Click += new System.EventHandler(this.btn_buscar_Click);
            // 
            // text_descricao
            // 
            this.text_descricao.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.text_descricao.Location = new System.Drawing.Point(6, 32);
            this.text_descricao.MaxLength = 100;
            this.text_descricao.Name = "text_descricao";
            this.text_descricao.Size = new System.Drawing.Size(563, 20);
            this.text_descricao.TabIndex = 1;
            // 
            // grb_atividade
            // 
            this.grb_atividade.Controls.Add(this.grd_atividade);
            this.grb_atividade.Location = new System.Drawing.Point(13, 111);
            this.grb_atividade.Name = "grb_atividade";
            this.grb_atividade.Size = new System.Drawing.Size(575, 228);
            this.grb_atividade.TabIndex = 1;
            this.grb_atividade.TabStop = false;
            this.grb_atividade.Text = "Atividades";
            // 
            // grd_atividade
            // 
            this.grd_atividade.AllowUserToAddRows = false;
            this.grd_atividade.AllowUserToDeleteRows = false;
            this.grd_atividade.AllowUserToOrderColumns = true;
            this.grd_atividade.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this.grd_atividade.BackgroundColor = System.Drawing.Color.White;
            this.grd_atividade.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.grd_atividade.Location = new System.Drawing.Point(6, 20);
            this.grd_atividade.MultiSelect = false;
            this.grd_atividade.Name = "grd_atividade";
            this.grd_atividade.ReadOnly = true;
            this.grd_atividade.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.grd_atividade.Size = new System.Drawing.Size(563, 202);
            this.grd_atividade.TabIndex = 3;
            // 
            // grb_paginacao
            // 
            this.grb_paginacao.Controls.Add(this.btn_fechar);
            this.grb_paginacao.Controls.Add(this.btn_novo);
            this.grb_paginacao.Controls.Add(this.btn_incluir);
            this.grb_paginacao.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.grb_paginacao.Location = new System.Drawing.Point(12, 345);
            this.grb_paginacao.Name = "grb_paginacao";
            this.grb_paginacao.Size = new System.Drawing.Size(575, 44);
            this.grb_paginacao.TabIndex = 4;
            this.grb_paginacao.TabStop = false;
            // 
            // btn_fechar
            // 
            this.btn_fechar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_fechar.Image = global::SWS.Properties.Resources.close;
            this.btn_fechar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btn_fechar.Location = new System.Drawing.Point(170, 15);
            this.btn_fechar.Name = "btn_fechar";
            this.btn_fechar.Size = new System.Drawing.Size(75, 23);
            this.btn_fechar.TabIndex = 6;
            this.btn_fechar.Text = "&Fechar";
            this.btn_fechar.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btn_fechar.UseVisualStyleBackColor = true;
            this.btn_fechar.Click += new System.EventHandler(this.btn_fechar_Click);
            // 
            // btn_novo
            // 
            this.btn_novo.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_novo.Image = global::SWS.Properties.Resources.icone_mais;
            this.btn_novo.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btn_novo.Location = new System.Drawing.Point(88, 15);
            this.btn_novo.Name = "btn_novo";
            this.btn_novo.Size = new System.Drawing.Size(75, 23);
            this.btn_novo.TabIndex = 5;
            this.btn_novo.Text = "&Novo";
            this.btn_novo.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btn_novo.UseVisualStyleBackColor = true;
            this.btn_novo.Click += new System.EventHandler(this.btn_novo_Click);
            // 
            // btn_incluir
            // 
            this.btn_incluir.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_incluir.Image = global::SWS.Properties.Resources.fechar_ico;
            this.btn_incluir.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btn_incluir.Location = new System.Drawing.Point(6, 15);
            this.btn_incluir.Name = "btn_incluir";
            this.btn_incluir.Size = new System.Drawing.Size(75, 23);
            this.btn_incluir.TabIndex = 4;
            this.btn_incluir.Text = "&Incluir";
            this.btn_incluir.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btn_incluir.UseVisualStyleBackColor = true;
            this.btn_incluir.Click += new System.EventHandler(this.btn_incluir_Click);
            // 
            // frm_PcmsoAtividadeBuscar
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(600, 400);
            this.ControlBox = false;
            this.Controls.Add(this.grb_paginacao);
            this.Controls.Add(this.grb_atividade);
            this.Controls.Add(this.grb_dado);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "frm_PcmsoAtividadeBuscar";
            this.Text = "BUSCAR ATIVIDADE";
            this.grb_dado.ResumeLayout(false);
            this.grb_dado.PerformLayout();
            this.grb_atividade.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.grd_atividade)).EndInit();
            this.grb_paginacao.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox grb_dado;
        private System.Windows.Forms.Button btn_buscar;
        private System.Windows.Forms.TextBox text_descricao;
        private System.Windows.Forms.GroupBox grb_atividade;
        private System.Windows.Forms.Label lbl_descricao;
        private System.Windows.Forms.DataGridView grd_atividade;
        private System.Windows.Forms.GroupBox grb_paginacao;
        private System.Windows.Forms.Button btn_fechar;
        private System.Windows.Forms.Button btn_novo;
        private System.Windows.Forms.Button btn_incluir;
    }
}