﻿using SWS.Entidade;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace SWS.View
{
    public partial class frmHospitalDetalhar : frmHospitalIncluir
    {
        public frmHospitalDetalhar(Hospital hospital) :base()
        {
            InitializeComponent();
            
            textNome.Text = hospital.Nome;
            textEndereco.Text = hospital.Endereco;
            textNumero.Text = hospital.Numero;
            textComplemento.Text = hospital.Complemento;
            textBairro.Text = hospital.Bairro;
            textCep.Text = hospital.Cep;
            cbUf.SelectedValue = hospital.Uf;
            textCidade.Text = hospital.Cidade != null ? hospital.Cidade.Nome : string.Empty;
            Cidade = hospital.Cidade;
            textTelefone.Text = hospital.Telefone1;
            textTelefone2.Text = hospital.Telefone2;
            textObservacao.Text = hospital.Observacao;
            cbUf.Enabled = false;
            btnGravar.Enabled = false;
            btnLimpar.Enabled = false;

        }
    }
}
