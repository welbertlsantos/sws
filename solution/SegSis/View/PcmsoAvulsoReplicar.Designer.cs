﻿namespace SWS.View
{
    partial class frmPcmsoAvulsoReplicar
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.flpAcao = new System.Windows.Forms.FlowLayoutPanel();
            this.btnReplicar = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.lblCliente = new System.Windows.Forms.TextBox();
            this.lblContratante = new System.Windows.Forms.TextBox();
            this.textCliente = new System.Windows.Forms.TextBox();
            this.textContratante = new System.Windows.Forms.TextBox();
            this.btnContratante = new System.Windows.Forms.Button();
            this.btnCliente = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.scForm)).BeginInit();
            this.scForm.Panel1.SuspendLayout();
            this.scForm.Panel2.SuspendLayout();
            this.scForm.SuspendLayout();
            this.pnlForm.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.banner)).BeginInit();
            this.flpAcao.SuspendLayout();
            this.SuspendLayout();
            // 
            // scForm
            // 
            // 
            // scForm.Panel1
            // 
            this.scForm.Panel1.Controls.Add(this.flpAcao);
            // 
            // pnlForm
            // 
            this.pnlForm.Controls.Add(this.btnCliente);
            this.pnlForm.Controls.Add(this.btnContratante);
            this.pnlForm.Controls.Add(this.textContratante);
            this.pnlForm.Controls.Add(this.textCliente);
            this.pnlForm.Controls.Add(this.lblContratante);
            this.pnlForm.Controls.Add(this.lblCliente);
            // 
            // flpAcao
            // 
            this.flpAcao.Controls.Add(this.btnReplicar);
            this.flpAcao.Controls.Add(this.button2);
            this.flpAcao.Dock = System.Windows.Forms.DockStyle.Fill;
            this.flpAcao.Location = new System.Drawing.Point(0, 0);
            this.flpAcao.Name = "flpAcao";
            this.flpAcao.Size = new System.Drawing.Size(534, 30);
            this.flpAcao.TabIndex = 0;
            // 
            // btnReplicar
            // 
            this.btnReplicar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnReplicar.Image = global::SWS.Properties.Resources.copiar;
            this.btnReplicar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnReplicar.Location = new System.Drawing.Point(3, 3);
            this.btnReplicar.Name = "btnReplicar";
            this.btnReplicar.Size = new System.Drawing.Size(75, 23);
            this.btnReplicar.TabIndex = 3;
            this.btnReplicar.Text = "&Replicar";
            this.btnReplicar.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnReplicar.UseVisualStyleBackColor = true;
            this.btnReplicar.Click += new System.EventHandler(this.btnReplicar_Click);
            // 
            // button2
            // 
            this.button2.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button2.Image = global::SWS.Properties.Resources.close;
            this.button2.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.button2.Location = new System.Drawing.Point(84, 3);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(75, 23);
            this.button2.TabIndex = 4;
            this.button2.Text = "&Fechar";
            this.button2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // lblCliente
            // 
            this.lblCliente.BackColor = System.Drawing.Color.LightGray;
            this.lblCliente.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblCliente.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCliente.Location = new System.Drawing.Point(12, 13);
            this.lblCliente.Name = "lblCliente";
            this.lblCliente.ReadOnly = true;
            this.lblCliente.Size = new System.Drawing.Size(135, 21);
            this.lblCliente.TabIndex = 0;
            this.lblCliente.TabStop = false;
            this.lblCliente.Text = "Cliente";
            // 
            // lblContratante
            // 
            this.lblContratante.BackColor = System.Drawing.Color.LightGray;
            this.lblContratante.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblContratante.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblContratante.Location = new System.Drawing.Point(12, 33);
            this.lblContratante.Name = "lblContratante";
            this.lblContratante.ReadOnly = true;
            this.lblContratante.Size = new System.Drawing.Size(135, 21);
            this.lblContratante.TabIndex = 1;
            this.lblContratante.TabStop = false;
            this.lblContratante.Text = "Contratante ou Unidade";
            // 
            // textCliente
            // 
            this.textCliente.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.textCliente.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textCliente.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textCliente.Location = new System.Drawing.Point(146, 13);
            this.textCliente.Name = "textCliente";
            this.textCliente.ReadOnly = true;
            this.textCliente.Size = new System.Drawing.Size(327, 21);
            this.textCliente.TabIndex = 2;
            this.textCliente.TabStop = false;
            // 
            // textContratante
            // 
            this.textContratante.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.textContratante.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textContratante.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textContratante.Location = new System.Drawing.Point(146, 33);
            this.textContratante.Name = "textContratante";
            this.textContratante.ReadOnly = true;
            this.textContratante.Size = new System.Drawing.Size(327, 21);
            this.textContratante.TabIndex = 3;
            this.textContratante.TabStop = false;
            // 
            // btnContratante
            // 
            this.btnContratante.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnContratante.Image = global::SWS.Properties.Resources.busca;
            this.btnContratante.Location = new System.Drawing.Point(472, 33);
            this.btnContratante.Name = "btnContratante";
            this.btnContratante.Size = new System.Drawing.Size(34, 21);
            this.btnContratante.TabIndex = 2;
            this.btnContratante.UseVisualStyleBackColor = true;
            this.btnContratante.Click += new System.EventHandler(this.btnContratante_Click);
            // 
            // btnCliente
            // 
            this.btnCliente.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnCliente.Image = global::SWS.Properties.Resources.busca;
            this.btnCliente.Location = new System.Drawing.Point(472, 13);
            this.btnCliente.Name = "btnCliente";
            this.btnCliente.Size = new System.Drawing.Size(34, 21);
            this.btnCliente.TabIndex = 1;
            this.btnCliente.UseVisualStyleBackColor = true;
            this.btnCliente.Click += new System.EventHandler(this.btnCliente_Click);
            // 
            // frmPcmsoAvulsoReplicar
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(534, 361);
            this.Name = "frmPcmsoAvulsoReplicar";
            this.Text = "REPLICAR PCMSO AVULSO";
            this.scForm.Panel1.ResumeLayout(false);
            this.scForm.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.scForm)).EndInit();
            this.scForm.ResumeLayout(false);
            this.pnlForm.ResumeLayout(false);
            this.pnlForm.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.banner)).EndInit();
            this.flpAcao.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.FlowLayoutPanel flpAcao;
        private System.Windows.Forms.Button btnReplicar;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button btnContratante;
        private System.Windows.Forms.TextBox textContratante;
        private System.Windows.Forms.TextBox textCliente;
        private System.Windows.Forms.TextBox lblContratante;
        private System.Windows.Forms.TextBox lblCliente;
        private System.Windows.Forms.Button btnCliente;
    }
}