﻿using SWS.Entidade;
using SWS.Facade;
using SWS.View.ViewHelper;
using SWS.ViewHelper;
using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SWS.View
{
    public partial class frmRelatorioAnual : frmTemplateConsulta
    {
        private Cliente cliente;
        private Cliente unidade;
        private RelatorioAnual relatorioAnual;
        private LinkedList<RelatorioAnualItem> itemRelatorio = new LinkedList<RelatorioAnualItem>();
        private LinkedList<Setor> setorInRelatorio = new LinkedList<Setor>();
        private Medico coordenador;
        private CentroCusto centroCusto;
        
        public frmRelatorioAnual()
        {
            InitializeComponent();
            ComboHelper.Boolean(cbSomenteMatriz, false);
        }

        private void btCliente_Click(object sender, EventArgs e)
        {
            try
            {
                frmClienteSelecionar formCliente = new frmClienteSelecionar(null, null, false, null, null, null, null, false, null);
                formCliente.ShowDialog();

                if (formCliente.Cliente != null)
                {
                    ClienteFacade clienteFacade = ClienteFacade.getInstance();
                    this.cliente = formCliente.Cliente;
                    this.textCliente.Text = cliente.RazaoSocial;

                    /* zerando informações de unidade */
                    this.unidade = null;

                    /* verificando se o cliente tem alguma unidade para habilitar os controles */
                    bool clienteHaveUnit = clienteFacade.findClienteHaveUnidade(this.cliente);
                    this.btnUnidade.Enabled = clienteHaveUnit;
                    this.btnExcluirUnidade.Enabled = clienteHaveUnit;

                    /* verificando se o cliente tem centro de custo */
                    if (this.cliente.UsaCentroCusto == true)
                    {
                        cbCentroCusto.Enabled = this.cliente.UsaCentroCusto;
                        carregaCentroCusto(this.cliente.CentroCusto, cbCentroCusto);
                    }
                        
                }
                else
                {
                    this.cliente = null;
                    this.textCliente.Text = String.Empty;
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
        }

        public void carregaCentroCusto(List<CentroCusto> centroCustos, ComboBox cb)
        {
            try
            {
                ArrayList arr = new ArrayList();
                /* incluindo a primeira informação do combo */

                arr.Add(new SelectItem("0", "Todos os centros de custos"));

                centroCustos.OrderBy(item => item.Descricao).ToList().ForEach(delegate(CentroCusto cc)
                {
                    arr.Add(new SelectItem(cc.Id.ToString(), cc.Descricao));
                });

                cb.DataSource = arr;

                cb.DisplayMember = "Nome";
                cb.ValueMember = "Valor";

                cb.DropDownStyle = ComboBoxStyle.DropDownList;

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private bool validaDados()
        {

            bool retorno = true;

            try
            {
                if (!this.dataEmissaoInicial.Checked && this.dataEmissaoFinal.Checked)
                    throw new Exception("Você deve marcar o período inicial");


                if (this.dataEmissaoInicial.Checked && !this.dataEmissaoFinal.Checked)
                    throw new Exception("Você deve marcar o período Final");

                if (!this.dataEmissaoInicial.Checked && !this.dataEmissaoFinal.Checked)
                    throw new Exception("Você deve marcar os períodos.");


                if (Convert.ToDateTime(this.dataEmissaoInicial.Text) > Convert.ToDateTime(this.dataEmissaoFinal.Text))
                    throw new Exception("A data inicial não pode ser maior que a data final.");

            }
            catch (Exception ex)
            {
                retorno = false;
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }

            return retorno;
        }

        private void btnImprimir_Click(object sender, EventArgs e)
        {
            try
            {
                this.Cursor = Cursors.WaitCursor;

                EstudoFacade pcmsoFacade = EstudoFacade.getInstance();

                /* validando se o cliente foi selecionado */
                if (cliente == null)
                    throw new Exception("Por favor, selecione o cliente.");

                /* validando informacao da data */
                if (validaDados())
                {
                    bool matriz = Convert.ToBoolean(((SelectItem)cbSomenteMatriz.SelectedItem).Valor);

                    /* setando valor para centro de custo se selecionado */
                    if (this.cliente.UsaCentroCusto && cbCentroCusto.SelectedIndex > 0)
                    {
                        ClienteFacade clienteFacade = ClienteFacade.getInstance();
                        this.centroCusto = clienteFacade.findCentroCustoById(Convert.ToInt64(((SelectItem)cbCentroCusto.SelectedItem).Valor));
                    }
                    
                    
                    lbAcompanhamento.Items.Clear();
                    lbAcompanhamento.Items.Add("Criando o relatório anual..");
                    System.Threading.Thread.Sleep(2000);
                    Refresh();

                    /* montando objeto RelatorioAnual */

                    relatorioAnual = new RelatorioAnual(null,
                        PermissionamentoFacade.usuarioAutenticado,
                        null,
                        cliente,
                        Convert.ToDateTime(this.dataEmissaoInicial.Text + " 00:00:00"),
                        Convert.ToDateTime(this.dataEmissaoFinal.Text + " 23:59:59"),
                        DateTime.Now);

                    /* verificando se existe exames não laudados no período.
                     * somente exames do pcmso serão validados nesse relatório. */
                    lbAcompanhamento.Items.Add("Verificando se existem exames não laudados no período...");
                    System.Threading.Thread.Sleep(2000);
                    Refresh();

                    Boolean continua = false;

                    if (pcmsoFacade.verificaExisteExameNaoLaudadoNoAtendimentoPorCliente(cliente, Convert.ToDateTime(this.dataEmissaoInicial.Text + " 00:00:00"),
                        Convert.ToDateTime(this.dataEmissaoFinal.Text + " 23:59:59"), unidade, matriz, centroCusto))
                    {

                        if (MessageBox.Show("Existe exames não laudados no período solicitado. Verifique o atendimento através do relatório de atendimento laudo. Deseja continuar com o relatório?" , "Atenção", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                            continua = true;
                    }
                    else
                        continua = true;

                    if (continua)
                    {
                        lbAcompanhamento.Items.Add("Recuperando todos os setores do pcmso...");
                        System.Threading.Thread.Sleep(2000);
                        Refresh();

                        /* recuperando os setores que farão parte da análise do relatório anual. */
                        setorInRelatorio = pcmsoFacade.verificaSetoresInRelatorio(cliente, Convert.ToDateTime(this.dataEmissaoInicial.Text + " 00:00:00"),
                        Convert.ToDateTime(this.dataEmissaoFinal.Text + " 23:59:59"), unidade, matriz, centroCusto);

                        lbAcompanhamento.Items.Add("Analisando exames já laudados...");
                        System.Threading.Thread.Sleep(2000);
                        Refresh();

                        lbAcompanhamento.Items.Add("Calculando a previsão para o próximo ano...");
                        System.Threading.Thread.Sleep(2000);
                        Refresh();


                        /* montando itens do relatório */

                        relatorioAnual = pcmsoFacade.montaItemRelatorio(setorInRelatorio, relatorioAnual, unidade, matriz, centroCusto);

                        lbAcompanhamento.Items.Add("Selecionando o coordenador responsável pelo relatório ... ");
                        System.Threading.Thread.Sleep(2000);
                        Refresh();


                        /* gravando o coordenador que irá assinar o relatório */

                        frmSelecionarCoordenador selecionarCoordenador = new frmSelecionarCoordenador();
                        selecionarCoordenador.ShowDialog();

                        if (selecionarCoordenador.Coordenador == null)
                            throw new Exception("Você deve selecionar o coordenador");


                        Usuario usuario = selecionarCoordenador.Coordenador;
                        coordenador = usuario.Medico;

                        /* atribuindo o médico coordenador ao relatório */

                        relatorioAnual.MedicoCoordenador = coordenador;

                        /* gravando relatório no banco e preparando para a impressão */

                        relatorioAnual = pcmsoFacade.insertRelatorioAnual(relatorioAnual);

                        lbAcompanhamento.Items.Add("Relatório montado. Preparando para impressão...");
                        System.Threading.Thread.Sleep(2000);
                        Refresh();
                        this.Cursor = Cursors.WaitCursor;

                        var process = new Process();

                        String unidadeRelatorio = unidade != null ? unidade.RazaoSocial : String.Empty;
                        string centroCustoDescricao = this.centroCusto != null ? this.centroCusto.Descricao : "EMPTY";

                        process.StartInfo.FileName = "Relatorio\\jre7\\bin\\java";
                        process.StartInfo.Arguments = " -jar Relatorio\\MiddlewareJasper.jar " + "Relatorio\\Report\\" + "RELATORIO_ANUAL.JASPER" + " ID_RELATORIO_ANUAL:" + relatorioAnual.Id + ":Integer" + " RAZAO_UNIDADE:" + '"' + unidadeRelatorio + '"' + ":STRING" + " CENTRO_CUSTO:" + '"' + centroCustoDescricao + '"' + ":STRING" ;
                        process.StartInfo.UseShellExecute = false;
                        process.StartInfo.RedirectStandardError = true;
                        process.StartInfo.RedirectStandardInput = true;
                        process.StartInfo.RedirectStandardOutput = true;
                        process.StartInfo.CreateNoWindow = true;
                        process.Start();
                        process.StandardOutput.ReadToEnd();
                        process.WaitForExit();
                        this.Close();

                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                lbAcompanhamento.Items.Clear();
            }
            finally
            {
                this.Cursor = Cursors.Default;
                lbAcompanhamento.Items.Clear();
            }
        }

        private void btnUnidade_Click(object sender, EventArgs e)
        {
            try
            {
                frmUnidadeBuscar pesquisarUnidade = new frmUnidadeBuscar(cliente);
                pesquisarUnidade.ShowDialog();


                if (pesquisarUnidade.Unidade != null)
                {
                    unidade = pesquisarUnidade.Unidade;
                    textUnidade.Text = unidade.RazaoSocial;
                    cbSomenteMatriz.SelectedValue = "false";
                }
                else
                {
                    unidade = null;
                    textUnidade.Text = String.Empty;
                    cbSomenteMatriz.SelectedValue = "false";
                }

                cbSomenteMatriz.Enabled = unidade != null ? false : true;


            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
        }

        private void btnFechar_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnExcluirCliente_Click(object sender, EventArgs e)
        {
            try
            {
                if (cliente == null)
                    throw new Exception("Você deve primeiro selecionar o cliente");

                this.cliente = null;
                this.textCliente.Text = string.Empty;

                this.unidade = null;
                this.textUnidade.Text = string.Empty;
                this.btnUnidade.Enabled = false;
                this.btnExcluirUnidade.Enabled = false;

                this.cbCentroCusto.DataSource = null;
                this.centroCusto = null;
                this.cbCentroCusto.Enabled = false;

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
              
        }

        private void btnExcluirUnidade_Click(object sender, EventArgs e)
        {
            try
            {

                if (unidade == null)
                    throw new Exception("Você deve primeiro selecionar a unidade do cliente");

                unidade = null;
                textUnidade.Text = string.Empty;

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }

        }

        private void cbCentroCusto_SelectedIndexChanged(object sender, EventArgs e)
        {
        }

    }
}
