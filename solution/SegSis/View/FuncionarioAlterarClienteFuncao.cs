﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using SegSis.Facade;
using SegSis.Entidade;
using SegSis.View.ViewHelper;
using SegSis.View.Resources;

namespace SegSis.View
{
    public partial class frm_FuncionarioAlterarClienteFuncao : BaseFormConsulta
    {
        
        private ClienteFuncaoFuncionario clienteFuncaoFuncionario = null;

        public static String msg1 = "Selecione uma linha.";
        public static String msg2 = "Atençao";

        Cliente cliente = null;

        Funcionario funcionario = null;

        public frm_FuncionarioAlterarClienteFuncao(Cliente cliente, Funcionario funcionario)
        {
            InitializeComponent();
            this.cliente = cliente;
            this.funcionario = funcionario;
        }

        public ClienteFuncaoFuncionario getClienteFuncaoFuncionario()
        {
            return this.clienteFuncaoFuncionario;
        }

        private void btn_fechar_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btn_pesquisar_Click(object sender, EventArgs e)
        {
            try
            {
                grd_funcao.Columns.Clear();
                montaDataGrid();
                text_descricao.Focus();

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        public void montaDataGrid()
        {
            try
            {
                this.Cursor = Cursors.WaitCursor;

                ClienteFacade clienteFacade = ClienteFacade.getInstance();

                Funcao funcao = new Funcao();

                funcao.setDescricao(text_descricao.Text.ToUpper());
                funcao.setCodCbo(text_cbo.Text);

                ClienteFuncao clienteFuncao = new ClienteFuncao();

                clienteFuncao.setCliente(cliente);
                clienteFuncao.setFuncao(funcao);

                DataSet ds = clienteFacade.findFuncaoByClienteFuncao(clienteFuncao);

                grd_funcao.DataSource = ds.Tables["Funcoes"].DefaultView;

                grd_funcao.Columns[0].HeaderText = "id_seg_cliente_funcao";
                grd_funcao.Columns[0].Visible = false;

                grd_funcao.Columns[1].HeaderText = "Id_funcao";
                grd_funcao.Columns[1].Visible = false;
                
                grd_funcao.Columns[2].HeaderText = "Descrição";
                grd_funcao.Columns[3].HeaderText = "Codigo CBO";
            
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }

        }

        private void btn_ok_Click(object sender, EventArgs e)
        {
            try
            {
                this.Cursor = Cursors.WaitCursor;
                
                if (grd_funcao.CurrentRow != null)
                {
                    Int64 id = (Int64)grd_funcao.CurrentRow.Cells[0].Value;

                    PpraFacade ppraFacade = PpraFacade.getInstance();

                    clienteFuncaoFuncionario = new ClienteFuncaoFuncionario(null, ppraFacade.findClienteFuncaoById(id),
                        funcionario, ApplicationConstants.ATIVO,DateTime.Now, null,string.Empty, false);

                    frm_FuncionarioAlterarClienteFuncaoFuncionario formFuncionarioAlterarClienteFuncaoFuncionario = new frm_FuncionarioAlterarClienteFuncaoFuncionario(clienteFuncaoFuncionario);
                    formFuncionarioAlterarClienteFuncaoFuncionario.ShowDialog();

                    if (formFuncionarioAlterarClienteFuncaoFuncionario.getClienteFuncaoFuncionario() != null)
                    {
                        clienteFuncaoFuncionario = formFuncionarioAlterarClienteFuncaoFuncionario.getClienteFuncaoFuncionario();
                        this.Close();
                    }
                    
                }
                else
                {
                    MessageBox.Show(msg1, msg2);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }

        }

        private void text_cbo_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = ValidaCampoHelper.FormataCampoNumerico(sender, e);
        }

        private void btn_novo_Click(object sender, EventArgs e)
        {
            
            frm_FuncionarioAlterarClienteFuncaoIncluirFuncao formFuncionarioAlterarIncluirFuncao = new frm_FuncionarioAlterarClienteFuncaoIncluirFuncao(cliente);
            formFuncionarioAlterarIncluirFuncao.ShowDialog();

            if (formFuncionarioAlterarIncluirFuncao.getClienteFuncao() != null)
            {

                clienteFuncaoFuncionario = new ClienteFuncaoFuncionario(null, formFuncionarioAlterarIncluirFuncao.getClienteFuncao(), funcionario,
                    ApplicationConstants.ATIVO, DateTime.Now, null, string.Empty, false);

                frm_FuncionarioAlterarClienteFuncaoFuncionario fomrFuncionarioAlterarClienteFuncaoFuncionario = new frm_FuncionarioAlterarClienteFuncaoFuncionario(clienteFuncaoFuncionario);
                fomrFuncionarioAlterarClienteFuncaoFuncionario.ShowDialog();

                if (fomrFuncionarioAlterarClienteFuncaoFuncionario.getClienteFuncaoFuncionario() != null)
                {
                    clienteFuncaoFuncionario = fomrFuncionarioAlterarClienteFuncaoFuncionario.getClienteFuncaoFuncionario();
                    this.Close();
                }
            }

            this.Close();
        }
    }
}
