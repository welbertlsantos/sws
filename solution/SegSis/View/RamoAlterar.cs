﻿using SWS.Entidade;
using SWS.Facade;
using SWS.View.Resources;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace SWS.View
{
    public partial class frmRamoAlterar : frmRamoIncluir
    {
        public frmRamoAlterar(Ramo ramo) : base()
        {
            InitializeComponent();

        }

        protected override void btGravar_Click(object sender, EventArgs e)
        {
            try
            {
                if (validaDadosTela())
                {
                    ClienteFacade clienteFacade = ClienteFacade.getInstance();

                    ramo = clienteFacade.updateRamo(new Ramo(ramo.Id, textDescricao.Text, ApplicationConstants.ATIVO));

                    MessageBox.Show("Ramo Alterado com sucesso.", "Confirmação", MessageBoxButtons.OK, MessageBoxIcon.Information);

                    this.Close();

                }

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
    }
}
