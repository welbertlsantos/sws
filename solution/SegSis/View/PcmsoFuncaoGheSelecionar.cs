﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using SWS.Facade;
using SWS.Entidade;

namespace SWS.View
{
    public partial class frm_PcmsoFuncaoGheSelecionar : BaseFormConsulta
    {
        static String Msg01 = "Selecione uma linha";
        
        Ghe ghe;
        Cliente cliente;
        Exame exame;
        Funcao funcao;
        public frm_PcmsoExameIncluir formPcmsoExameIncluir;

        public frm_PcmsoFuncaoGheSelecionar(frm_PcmsoExameIncluir formPcmsoExameIncluir, Cliente cliente, Ghe ghe, Exame exame)
        {
            InitializeComponent();
            this.formPcmsoExameIncluir = formPcmsoExameIncluir;
            this.cliente = cliente;
            this.ghe = ghe;
            this.exame = exame;

            inicializa();
        }

        private void inicializa()
        {
            try
            {
                this.text_cliente.Text = cliente.RazaoSocial;
                this.text_ghe.Text = ghe.Descricao;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void textBox1_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!Char.IsDigit(e.KeyChar) && e.KeyChar != (char)8)
            {
                e.Handled = true;
            }
        }
        
        //Buscar as funções que não foram inseridas no ghe ainda.
        public void montaDataGrid()
        {
            PcmsoFacade pcmsoFacade = PcmsoFacade.getInstance();
            ClienteFacade clienteFacade = ClienteFacade.getInstance();

            Funcao funcao = new Funcao();

            grd_funcao.Columns.Clear();

            funcao.Descricao = text_descricao.Text.ToUpper();
            funcao.CodCbo = text_cbo.Text;

            //Buscar as setor função não vinculadas nas exceções cadastrada para todo o ghe
            DataSet ds = pcmsoFacade.findAllFuncaoNotInGheSetorClienteFuncaoExame(exame, ghe, funcao);
                
            grd_funcao.DataSource = ds.Tables["Funcoes"].DefaultView;

            grd_funcao.Columns[0].HeaderText = "id_funcao";
            grd_funcao.Columns[1].HeaderText = "Descrição";
            
            grd_funcao.Columns[0].Visible = false;
        }

        private void btn_buscar_Click(object sender, EventArgs e)
        {
            try
            {
                this.Cursor = Cursors.WaitCursor;

                montaDataGrid();
                text_descricao.Focus();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }

        private void btn_incluir_Click(object sender, EventArgs e)
        {
            try
            {
                PpraFacade estudoFacade = PpraFacade.getInstance();


                if (this.grd_funcao.CurrentRow != null)
                {
                    Int64 id = (Int64)grd_funcao.CurrentRow.Cells[0].Value;

                    funcao = estudoFacade.findFuncaoById(id);

                    frm_GheSetorClienteFuncaoExameIdade formGheSetorClienteFuncaoExameIdade = new frm_GheSetorClienteFuncaoExameIdade(this, ghe, cliente,funcao,exame);
                    formGheSetorClienteFuncaoExameIdade.ShowDialog();
                }
                else
                {
                    MessageBox.Show(Msg01);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void btn_fechar_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btn_limpar_Click(object sender, EventArgs e)
        {
            text_cbo.Text = string.Empty;
            text_descricao.Text = string.Empty;

            grd_funcao.Columns.Clear();
        }
    }
}
