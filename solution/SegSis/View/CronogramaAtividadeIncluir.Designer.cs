﻿namespace SWS.View
{
    partial class frmCronogramaAtividadeIncluir
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmCronogramaAtividadeIncluir));
            this.grb_paginacao = new System.Windows.Forms.GroupBox();
            this.btn_limpar = new System.Windows.Forms.Button();
            this.btn_incluir = new System.Windows.Forms.Button();
            this.btn_fechar = new System.Windows.Forms.Button();
            this.grb_dados = new System.Windows.Forms.GroupBox();
            this.grb_ano = new System.Windows.Forms.GroupBox();
            this.text_ano = new System.Windows.Forms.TextBox();
            this.text_evidencia = new System.Windows.Forms.TextBox();
            this.grb_mes = new System.Windows.Forms.GroupBox();
            this.cb_mes = new System.Windows.Forms.ComboBox();
            this.text_publico = new System.Windows.Forms.TextBox();
            this.lbl_evidencia = new System.Windows.Forms.Label();
            this.lbl_publico = new System.Windows.Forms.Label();
            this.grb_atividade = new System.Windows.Forms.GroupBox();
            this.text_atividade = new System.Windows.Forms.TextBox();
            this.btn_atividade = new System.Windows.Forms.Button();
            this.IncluirAtividaePPRAErrorProvider = new System.Windows.Forms.ErrorProvider(this.components);
            this.grb_paginacao.SuspendLayout();
            this.grb_dados.SuspendLayout();
            this.grb_ano.SuspendLayout();
            this.grb_mes.SuspendLayout();
            this.grb_atividade.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.IncluirAtividaePPRAErrorProvider)).BeginInit();
            this.SuspendLayout();
            // 
            // grb_paginacao
            // 
            this.grb_paginacao.Controls.Add(this.btn_limpar);
            this.grb_paginacao.Controls.Add(this.btn_incluir);
            this.grb_paginacao.Controls.Add(this.btn_fechar);
            this.grb_paginacao.Location = new System.Drawing.Point(6, 341);
            this.grb_paginacao.Name = "grb_paginacao";
            this.grb_paginacao.Size = new System.Drawing.Size(587, 55);
            this.grb_paginacao.TabIndex = 6;
            this.grb_paginacao.TabStop = false;
            // 
            // btn_limpar
            // 
            this.btn_limpar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_limpar.Image = global::SWS.Properties.Resources.vassoura;
            this.btn_limpar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btn_limpar.Location = new System.Drawing.Point(87, 19);
            this.btn_limpar.Name = "btn_limpar";
            this.btn_limpar.Size = new System.Drawing.Size(75, 23);
            this.btn_limpar.TabIndex = 7;
            this.btn_limpar.Text = "&Limpar";
            this.btn_limpar.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btn_limpar.UseVisualStyleBackColor = true;
            this.btn_limpar.Click += new System.EventHandler(this.btn_limpar_Click);
            // 
            // btn_incluir
            // 
            this.btn_incluir.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_incluir.Image = global::SWS.Properties.Resources.fechar_ico;
            this.btn_incluir.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btn_incluir.Location = new System.Drawing.Point(6, 19);
            this.btn_incluir.Name = "btn_incluir";
            this.btn_incluir.Size = new System.Drawing.Size(75, 23);
            this.btn_incluir.TabIndex = 6;
            this.btn_incluir.Text = "&Incluir";
            this.btn_incluir.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btn_incluir.UseVisualStyleBackColor = true;
            this.btn_incluir.Click += new System.EventHandler(this.btn_incluir_Click);
            // 
            // btn_fechar
            // 
            this.btn_fechar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_fechar.Image = global::SWS.Properties.Resources.close;
            this.btn_fechar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btn_fechar.Location = new System.Drawing.Point(168, 19);
            this.btn_fechar.Name = "btn_fechar";
            this.btn_fechar.Size = new System.Drawing.Size(75, 23);
            this.btn_fechar.TabIndex = 8;
            this.btn_fechar.Text = "&Fechar";
            this.btn_fechar.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btn_fechar.UseVisualStyleBackColor = true;
            this.btn_fechar.Click += new System.EventHandler(this.btn_fechar_Click);
            // 
            // grb_dados
            // 
            this.grb_dados.BackColor = System.Drawing.Color.White;
            this.grb_dados.Controls.Add(this.grb_ano);
            this.grb_dados.Controls.Add(this.text_evidencia);
            this.grb_dados.Controls.Add(this.grb_mes);
            this.grb_dados.Controls.Add(this.text_publico);
            this.grb_dados.Controls.Add(this.lbl_evidencia);
            this.grb_dados.Controls.Add(this.lbl_publico);
            this.grb_dados.Location = new System.Drawing.Point(6, 59);
            this.grb_dados.Name = "grb_dados";
            this.grb_dados.Size = new System.Drawing.Size(587, 276);
            this.grb_dados.TabIndex = 2;
            this.grb_dados.TabStop = false;
            this.grb_dados.Text = "Dados";
            // 
            // grb_ano
            // 
            this.grb_ano.BackColor = System.Drawing.Color.White;
            this.grb_ano.Controls.Add(this.text_ano);
            this.grb_ano.Location = new System.Drawing.Point(222, 19);
            this.grb_ano.Name = "grb_ano";
            this.grb_ano.Size = new System.Drawing.Size(118, 50);
            this.grb_ano.TabIndex = 3;
            this.grb_ano.TabStop = false;
            this.grb_ano.Text = "Ano de Realização";
            // 
            // text_ano
            // 
            this.text_ano.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.text_ano.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.text_ano.Location = new System.Drawing.Point(28, 19);
            this.text_ano.MaxLength = 4;
            this.text_ano.Name = "text_ano";
            this.text_ano.Size = new System.Drawing.Size(60, 20);
            this.text_ano.TabIndex = 3;
            this.text_ano.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.text_ano.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.text_ano_KeyPress);
            // 
            // text_evidencia
            // 
            this.text_evidencia.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.text_evidencia.Location = new System.Drawing.Point(6, 168);
            this.text_evidencia.MaxLength = 250;
            this.text_evidencia.Multiline = true;
            this.text_evidencia.Name = "text_evidencia";
            this.text_evidencia.Size = new System.Drawing.Size(559, 90);
            this.text_evidencia.TabIndex = 5;
            // 
            // grb_mes
            // 
            this.grb_mes.BackColor = System.Drawing.Color.White;
            this.grb_mes.Controls.Add(this.cb_mes);
            this.grb_mes.Location = new System.Drawing.Point(6, 19);
            this.grb_mes.Name = "grb_mes";
            this.grb_mes.Size = new System.Drawing.Size(210, 50);
            this.grb_mes.TabIndex = 2;
            this.grb_mes.TabStop = false;
            this.grb_mes.Text = "Mês de Realização";
            // 
            // cb_mes
            // 
            this.cb_mes.BackColor = System.Drawing.Color.LightGray;
            this.cb_mes.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cb_mes.FormattingEnabled = true;
            this.cb_mes.Location = new System.Drawing.Point(10, 19);
            this.cb_mes.Name = "cb_mes";
            this.cb_mes.Size = new System.Drawing.Size(188, 21);
            this.cb_mes.TabIndex = 2;
            // 
            // text_publico
            // 
            this.text_publico.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.text_publico.Location = new System.Drawing.Point(6, 88);
            this.text_publico.MaxLength = 250;
            this.text_publico.Multiline = true;
            this.text_publico.Name = "text_publico";
            this.text_publico.Size = new System.Drawing.Size(559, 61);
            this.text_publico.TabIndex = 4;
            // 
            // lbl_evidencia
            // 
            this.lbl_evidencia.AutoSize = true;
            this.lbl_evidencia.Location = new System.Drawing.Point(6, 152);
            this.lbl_evidencia.Name = "lbl_evidencia";
            this.lbl_evidencia.Size = new System.Drawing.Size(104, 13);
            this.lbl_evidencia.TabIndex = 6;
            this.lbl_evidencia.Text = "Evidencia / Registro";
            // 
            // lbl_publico
            // 
            this.lbl_publico.AutoSize = true;
            this.lbl_publico.Location = new System.Drawing.Point(6, 72);
            this.lbl_publico.Name = "lbl_publico";
            this.lbl_publico.Size = new System.Drawing.Size(66, 13);
            this.lbl_publico.TabIndex = 5;
            this.lbl_publico.Text = "Publico Alvo";
            // 
            // grb_atividade
            // 
            this.grb_atividade.BackColor = System.Drawing.Color.White;
            this.grb_atividade.Controls.Add(this.text_atividade);
            this.grb_atividade.Controls.Add(this.btn_atividade);
            this.grb_atividade.Location = new System.Drawing.Point(6, 1);
            this.grb_atividade.Name = "grb_atividade";
            this.grb_atividade.Size = new System.Drawing.Size(587, 52);
            this.grb_atividade.TabIndex = 1;
            this.grb_atividade.TabStop = false;
            this.grb_atividade.Text = "Atividades";
            // 
            // text_atividade
            // 
            this.text_atividade.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.text_atividade.Location = new System.Drawing.Point(6, 19);
            this.text_atividade.MaxLength = 100;
            this.text_atividade.Name = "text_atividade";
            this.text_atividade.ReadOnly = true;
            this.text_atividade.Size = new System.Drawing.Size(536, 20);
            this.text_atividade.TabIndex = 0;
            this.text_atividade.TabStop = false;
            // 
            // btn_atividade
            // 
            this.btn_atividade.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_atividade.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_atividade.Image = global::SWS.Properties.Resources.busca;
            this.btn_atividade.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btn_atividade.Location = new System.Drawing.Point(548, 19);
            this.btn_atividade.Name = "btn_atividade";
            this.btn_atividade.Size = new System.Drawing.Size(33, 20);
            this.btn_atividade.TabIndex = 1;
            this.btn_atividade.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btn_atividade.UseVisualStyleBackColor = true;
            this.btn_atividade.Click += new System.EventHandler(this.btn_atividade_Click);
            // 
            // IncluirAtividaePPRAErrorProvider
            // 
            this.IncluirAtividaePPRAErrorProvider.ContainerControl = this;
            // 
            // frmCronogramaAtividadeIncluir
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(600, 400);
            this.Controls.Add(this.grb_dados);
            this.Controls.Add(this.grb_paginacao);
            this.Controls.Add(this.grb_atividade);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "frmCronogramaAtividadeIncluir";
            this.Text = "INCLUIR ATIVIDADE";
            this.grb_paginacao.ResumeLayout(false);
            this.grb_dados.ResumeLayout(false);
            this.grb_dados.PerformLayout();
            this.grb_ano.ResumeLayout(false);
            this.grb_ano.PerformLayout();
            this.grb_mes.ResumeLayout(false);
            this.grb_atividade.ResumeLayout(false);
            this.grb_atividade.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.IncluirAtividaePPRAErrorProvider)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btn_atividade;
        public System.Windows.Forms.TextBox text_atividade;
        private System.Windows.Forms.GroupBox grb_atividade;
        private System.Windows.Forms.Label lbl_evidencia;
        private System.Windows.Forms.Label lbl_publico;
        private System.Windows.Forms.GroupBox grb_dados;
        private System.Windows.Forms.ComboBox cb_mes;
        private System.Windows.Forms.TextBox text_publico;
        private System.Windows.Forms.GroupBox grb_mes;
        private System.Windows.Forms.GroupBox grb_ano;
        private System.Windows.Forms.TextBox text_evidencia;
        private System.Windows.Forms.Button btn_fechar;
        private System.Windows.Forms.Button btn_incluir;
        private System.Windows.Forms.GroupBox grb_paginacao;
        private System.Windows.Forms.Button btn_limpar;
        private System.Windows.Forms.TextBox text_ano;
        private System.Windows.Forms.ErrorProvider IncluirAtividaePPRAErrorProvider;
    }
}