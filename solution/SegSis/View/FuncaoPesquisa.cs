﻿using SWS.Entidade;
using SWS.Facade;
using SWS.View.Resources;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace SWS.View
{
    public partial class frmFuncaoPesquisa : frmTemplateConsulta
    {
        private Funcao funcao;

        private List<Funcao> funcoes = new List<Funcao>();

        public List<Funcao> Funcoes
        {
            get { return funcoes; }
            set { funcoes = value; }
        }

        private bool status = false;
        
        public frmFuncaoPesquisa()
        {
            InitializeComponent();
            ActiveControl = textFuncao;
        }

        private void btConfirmar_Click(object sender, EventArgs e)
        {
            try
            {
                if (dgvFuncao.CurrentRow == null)
                    throw new Exception("Selecione uma linha.");

                foreach (DataGridViewRow dvRow in dgvFuncao.Rows)
                    if ((bool)dvRow.Cells[4].Value)
                        funcoes.Add(new Funcao((Int64)dvRow.Cells[0].Value, dvRow.Cells[1].Value.ToString(), dvRow.Cells[2].Value.ToString(), dvRow.Cells[3].Value.ToString(), string.Empty));
                
                this.Close();

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btIncluirFuncao_Click(object sender, EventArgs e)
        {
            frmFuncaoIncluir incluirFuncao = new frmFuncaoIncluir();
            incluirFuncao.ShowDialog();

            if (incluirFuncao.Funcao != null)
            {
                funcoes.Add(incluirFuncao.Funcao);
                this.Close();
            }
        }

        private void btFechar_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btBuscar_Click(object sender, EventArgs e)
        {
            funcao = new Funcao(null, textFuncao.Text, String.Empty, ApplicationConstants.ATIVO, String.Empty);
            montaGridFuncoes();
        }

        private void montaGridFuncoes()
        {
            try
            {
                this.Cursor = Cursors.WaitCursor;

                dgvFuncao.Columns.Clear();
                dgvFuncao.ColumnCount = 4;

                dgvFuncao.Columns[0].HeaderText = "Código Função";
                dgvFuncao.Columns[0].ReadOnly = true;
                dgvFuncao.Columns[0].Width = 50;

                dgvFuncao.Columns[1].HeaderText = "Descrição";
                dgvFuncao.Columns[1].ReadOnly = true;
                dgvFuncao.Columns[1].Width = 350;

                dgvFuncao.Columns[2].HeaderText = "Codigo CBO";
                dgvFuncao.Columns[2].ReadOnly = true;

                dgvFuncao.Columns[3].HeaderText = "Situação";
                dgvFuncao.Columns[3].Visible = false;

                DataGridViewCheckBoxColumn check = new DataGridViewCheckBoxColumn();
                check.Name = "Selec";
                dgvFuncao.Columns.Add(check);
                dgvFuncao.Columns[4].DisplayIndex = 0;
                dgvFuncao.Columns[4].Width = 40;

                EstudoFacade pcmsoFacade = EstudoFacade.getInstance();

                DataSet ds = pcmsoFacade.findFuncaoByFilter(funcao);

                foreach (DataRow row in ds.Tables[0].Rows)
                    dgvFuncao.Rows.Add(Convert.ToInt64(row["id_funcao"]), row["descricao"].ToString(), row["cod_cbo"].ToString(), row["situacao"].ToString(), false);

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }

        private void frmFuncaoPesquisa_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.KeyCode == Keys.F2)
                {
                    if (dgvFuncao.Rows.Count > 0)
                    {
                        foreach (DataGridViewRow dvRow in dgvFuncao.Rows)
                        {
                            dvRow.Cells[4].Value = !status;
                        }

                        status = (bool)dgvFuncao.Rows[0].Cells[4].Value;
                    }
                   
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }
    }
}
