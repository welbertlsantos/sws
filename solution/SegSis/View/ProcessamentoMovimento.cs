﻿using SWS.Entidade;
using SWS.Facade;
using SWS.View.Resources;
using SWS.View.ViewHelper;
using SWS.ViewHelper;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Threading;

namespace SWS.View
{
    public partial class frmProcessamentoMovimento : frmTemplate
    {
        public frmProcessamentoMovimento()
        {
            InitializeComponent();

            ComboHelper.comboMes(cbMes);
            ComboHelper.comboAno(cbAno);

            cbMes.SelectedValue = DateTime.Now.Month.ToString();
            cbAno.SelectedValue = DateTime.Now.Year.ToString();

            ActiveControl = btnCliente;
        }

        private Cliente cliente;

        public Cliente Cliente
        {
            get { return cliente; }
            set { cliente = value; }
        }

        private DateTime dataInicial;
        private DateTime dataFinal;

        private List<Aso> atendimentos = new List<Aso>();
        private List<Movimento> movimentosLancados = new List<Movimento>();
        private List<Movimento> movimentoIncluir = new List<Movimento>();

        private void btFechar_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnProcessar_Click(object sender, EventArgs e)
        {
            try
            {
                if (this.Cliente == null)
                    throw new Exception("Você deve selecionar um cliente para continuar o processamento.");

                this.Cursor = Cursors.WaitCursor;

                this.listProcessamento.Items.Clear();
                Refresh();

                this.movimentosLancados.Clear();
                this.movimentoIncluir.Clear();
                

                int ano = Convert.ToInt32(((SelectItem)cbAno.SelectedItem).Valor);
                int mes = Convert.ToInt32(((SelectItem)cbMes.SelectedItem).Valor);
                int dia = DateTime.DaysInMonth(ano, mes);

                DateTime dataFinalSelecionada = new DateTime(ano, mes, dia);
                DateTime dataInicialSelecionada = new DateTime(ano, mes, 1);

                this.dataInicial = Convert.ToDateTime(dataInicialSelecionada.ToShortDateString() + " 00:00:00");
                this.dataFinal = Convert.ToDateTime(dataFinalSelecionada.ToShortDateString() + " 23:59:59");

                AsoFacade asoFacade = AsoFacade.getInstance();
                FinanceiroFacade financeiroFacade = FinanceiroFacade.getInstance();

                /* recuperando os atendimentos do cliente no período selecionado. */

                this.atendimentos = asoFacade.findAllAtendimentosInSituacaoByClienteAndEmpresaAndPeriodo(this.cliente, ApplicationConstants.FECHADO, this.dataInicial, this.dataFinal, false, PermissionamentoFacade.usuarioAutenticado.Empresa);

                this.listProcessamento.Items.Add("Total de atendimentos do cliente no período: " + this.atendimentos.Count);
                Refresh();
                
                foreach (Aso atendimento in atendimentos)
                {
                    List<Movimento> movimentoNoAtendimento = new List<Movimento>();
                    List<Exame> examesSemContrato = new List<Exame>();

                    /* descobrindo se todos os exames apurados no atendimento estão no movimento */
                    this.movimentosLancados = financeiroFacade.findAllLancamentoByAtendimento(atendimento);

                    foreach (GheFonteAgenteExameAso gheFonteAgenteExameAso in atendimento.ExamePcmso)
                    {
                        if (!this.movimentosLancados.Exists(x => x.GheFonteAgenteExameAso != null && x.GheFonteAgenteExameAso.Id == gheFonteAgenteExameAso.Id))
                        {
                            /* esse movimento deverá ser incluído */
                            /* devemos encontrar o contrato do cliente vigente nessa data */
                            Contrato contrato = financeiroFacade.findContratoVigenteByData((DateTime)atendimento.DataFinalizacao, atendimento.Cliente, null);

                            /* caso o contrato do cliente não existe então será utilizado o contrato do sistema */
                            Contrato contratoSistema = null;
                            if (contrato == null)
                            {
                                contratoSistema = financeiroFacade.findContratoSistemaByCliente(atendimento.Cliente);
                            }

                            /* devemos encontrar o contrato exame do cliente */
                            ContratoExame contratoExame = null;
                            if (contrato != null)
                            {
                                contratoExame = financeiroFacade.findContratoExameByContratoandExame(contrato, gheFonteAgenteExameAso.GheFonteAgenteExame.Exame);
                            }
                            else
                            {
                                contratoExame = financeiroFacade.findContratoExameByContratoandExame(contratoSistema, gheFonteAgenteExameAso.GheFonteAgenteExame.Exame);
                            }


                            /* TODO: verificar se caso não tenha o contratoExame temos que armazenar isso e informar o usuário */

                            if (contratoExame == null)
                            {
                                /* incluir um contrato exame */
                                contratoExame = new ContratoExame();
                                contratoExame.Altera = false;
                                contratoExame.Contrato = contratoSistema == null ? contrato : contratoSistema;
                                contratoExame.CustoContrato = gheFonteAgenteExameAso.GheFonteAgenteExame.Exame.Custo;
                                contratoExame.DataInclusao = DateTime.Now;
                                contratoExame.Exame = gheFonteAgenteExameAso.GheFonteAgenteExame.Exame;
                                contratoExame.PrecoContratado = gheFonteAgenteExameAso.GheFonteAgenteExame.Exame.Preco;
                                contratoExame.Situacao = ApplicationConstants.ATIVO;
                                contratoExame.Usuario = PermissionamentoFacade.usuarioAutenticado;

                                financeiroFacade.insertContratoExame(contratoExame);
                            }

                            /* devemos montar o movimento para a inclusão posterior */
                            Movimento movimento = new Movimento();
                            movimento.Aso = atendimento;
                            movimento.CentroCusto = atendimento.CentroCusto;
                            movimento.Cliente = atendimento.Cliente;
                            movimento.ContratoExame = contratoExame;
                            movimento.CustoUnitario = contratoExame.CustoContrato;
                            movimento.DataGravacao = DateTime.Now;
                            movimento.DataInclusao = atendimento.DataFinalizacao;
                            movimento.Empresa = PermissionamentoFacade.usuarioAutenticado.Empresa;
                            movimento.Estudo = atendimento.Estudo;
                            movimento.GheFonteAgenteExameAso = gheFonteAgenteExameAso;
                            movimento.PrecoUnit = contratoExame.PrecoContratado;
                            movimento.Quantidade = 1;
                            movimento.Situacao = ApplicationConstants.PENDENTE;
                            movimento.Unidade = null;
                            movimento.Usuario = PermissionamentoFacade.usuarioAutenticado;
                            movimento.ValorComissao = 0;

                            this.movimentoIncluir.Add(movimento);
                            movimentoNoAtendimento.Add(movimento);
                        }
                    }

                    foreach (ClienteFuncaoExameASo clienteFuncaoExameAso in atendimento.ExameExtra)
                    {
                        if (!this.movimentosLancados.Exists(x => x.ClienteFuncaoExameAso != null && x.ClienteFuncaoExameAso.Id == clienteFuncaoExameAso.Id))
                        {
                            /* esse movimento deverá ser incluído */
                            /* devemos encontrar o contrato do cliente vigente nessa data */
                            Contrato contrato = financeiroFacade.findContratoVigenteByData((DateTime)atendimento.DataFinalizacao, atendimento.Cliente, null);

                            /* caso o contrato do cliente não existe então será utilizado o contrato do sistema */
                            Contrato contratoSistema = null;
                            if (contrato == null)
                            {
                                contratoSistema = financeiroFacade.findContratoSistemaByCliente(atendimento.Cliente);
                            }

                            /* devemos encontrar o contrato exame do cliente */
                            ContratoExame contratoExame = null;
                            if (contrato != null)
                            {
                                contratoExame = financeiroFacade.findContratoExameByContratoandExame(contrato, clienteFuncaoExameAso.ClienteFuncaoExame.Exame);
                            }
                            else
                            {
                                contratoExame = financeiroFacade.findContratoExameByContratoandExame(contratoSistema, clienteFuncaoExameAso.ClienteFuncaoExame.Exame);
                            }

                            /* TODO: verificar se caso não tenha o contratoExame temos que armazenar isso e informar o usuário */

                            if (contratoExame == null)
                            {
                                /* incluir um contrato exame */
                                contratoExame = new ContratoExame();
                                contratoExame.Altera = false;
                                contratoExame.Contrato = contratoSistema == null ? contrato : contratoSistema;
                                contratoExame.CustoContrato = clienteFuncaoExameAso.ClienteFuncaoExame.Exame.Custo;
                                contratoExame.DataInclusao = DateTime.Now;
                                contratoExame.Exame = clienteFuncaoExameAso.ClienteFuncaoExame.Exame;
                                contratoExame.PrecoContratado = clienteFuncaoExameAso.ClienteFuncaoExame.Exame.Preco;
                                contratoExame.Situacao = ApplicationConstants.ATIVO;
                                contratoExame.Usuario = PermissionamentoFacade.usuarioAutenticado;

                                financeiroFacade.insertContratoExame(contratoExame);
                            }

                            /* devemos montar o movimento para a inclusão posterior */
                            Movimento movimento = new Movimento();
                            movimento.Aso = atendimento;
                            movimento.CentroCusto = atendimento.CentroCusto;
                            movimento.Cliente = atendimento.Cliente;
                            movimento.ContratoExame = contratoExame;
                            movimento.CustoUnitario = contratoExame.CustoContrato;
                            movimento.DataGravacao = DateTime.Now;
                            movimento.DataInclusao = atendimento.DataFinalizacao;
                            movimento.Empresa = PermissionamentoFacade.usuarioAutenticado.Empresa;
                            movimento.Estudo = atendimento.Estudo;
                            movimento.ClienteFuncaoExameAso = clienteFuncaoExameAso;
                            movimento.PrecoUnit = contratoExame.PrecoContratado;
                            movimento.Quantidade = 1;
                            movimento.Situacao = ApplicationConstants.PENDENTE;
                            movimento.Unidade = null;
                            movimento.Usuario = PermissionamentoFacade.usuarioAutenticado;
                            movimento.ValorComissao = 0;

                            this.movimentoIncluir.Add(movimento);
                            movimentoNoAtendimento.Add(movimento);
                        }
                    }

                    /* incluindo informação pro usuário */
                    if (movimentoNoAtendimento.Count > 0)
                    {
                        this.listProcessamento.Items.Add("Foram encontratos: " + movimentoNoAtendimento.Count + " exames do atendimento: " + atendimento.Codigo + " que não foram incluídos no movimento. ");
                    }
                    Refresh();
                }

                /* realizando a inclusão no movimento para os atendimentos não inclusos */

                if (this.movimentoIncluir.Count > 0)
                {
                    this.listProcessamento.Items.Add("");
                    this.listProcessamento.Items.Add("Preparando para realizar a inclusão no movimento.....");
                    Thread.Sleep(3000);
                    Refresh();
                    foreach (Movimento movimento in this.movimentoIncluir)
                    {
                        financeiroFacade.insertMovimento(movimento);
                    }

                    MessageBox.Show("Conclusão do processo de gravação do movimento terminado com sucesso.", "Confirmação", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    
                }
                else
                    throw new Exception("Não foi localizado nenhum lançamento para ser incluído.");

                

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
            
        }

        private void btnCliente_Click(object sender, EventArgs e)
        {
            try
            {
                frmClienteSelecionar clienteProcessamento = new frmClienteSelecionar(null, null, null, null, null, null, null, null, null);
                clienteProcessamento.ShowDialog();

                if (clienteProcessamento.Cliente != null)
                {
                    this.Cliente = clienteProcessamento.Cliente;
                    textCliente.Text = this.Cliente.RazaoSocial + " " + this.Cliente.Fantasia;
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            

        }

        private void btnExcluirCliente_Click(object sender, EventArgs e)
        {
            try
            {
                if (this.Cliente == null)
                    throw new Exception("Selecione primeiro um cliente.");

                this.Cliente = null;
                textCliente.Text = string.Empty;
                this.listProcessamento.Items.Clear();
                ActiveControl = btnCliente;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }
    }
}
