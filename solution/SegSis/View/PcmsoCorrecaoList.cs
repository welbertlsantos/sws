﻿using SWS.Entidade;
using SWS.Facade;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SWS.View
{
    public partial class frmPcmsoCorrecaoList : frmTemplateConsulta
    {
        private Estudo pcmso;
        
        public frmPcmsoCorrecaoList(Estudo pcmso)
        {
            InitializeComponent();
            this.pcmso = pcmso;
            montaGrid();
            
        }

        private void btnFechar_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        public void montaGrid()
        {
            try
            {
                LogFacade logFacade = LogFacade.getInstance();

                DataSet ds = logFacade.findAllCorrecaoEstudoByEstudo(pcmso);

                dgvCorrecao.ColumnCount = 6;
                
                dgvCorrecao.Columns[0].HeaderText = "Id";
                dgvCorrecao.Columns[0].Visible = false;
                dgvCorrecao.Columns[1].HeaderText = "Id Usuario";
                dgvCorrecao.Columns[1].Visible = false;
                dgvCorrecao.Columns[2].HeaderText = "Usuario";
                dgvCorrecao.Columns[2].DefaultCellStyle.ForeColor = Color.Red;
                dgvCorrecao.Columns[3].HeaderText = "Id Estudo";
                dgvCorrecao.Columns[3].Visible = false;
                dgvCorrecao.Columns[4].HeaderText = "Histórico";
                dgvCorrecao.Columns[5].HeaderText = "Data Gravação";


                foreach (DataRow row in ds.Tables["Correcoes"].Rows)
                {
                    dgvCorrecao.Rows.Add(
                        row["ID_CORRECAO_ESTUDO"],
                        row["ID_USUARIO"],
                        row["NOME"].ToString(),
                        row["ID_ESTUDO"],
                        row["HISTORICO"].ToString(),
                        String.Format("{0:dd/MM/yyyy}", row["DATA_CORRECAO"])
                        );
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }
    }
}
