﻿namespace SWS.View
{
    partial class frmPcmsoPrincipal
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            this.flpAcao = new System.Windows.Forms.FlowLayoutPanel();
            this.btnPesquisar = new System.Windows.Forms.Button();
            this.btnIncluir = new System.Windows.Forms.Button();
            this.btnAlterar = new System.Windows.Forms.Button();
            this.btnCorrigir = new System.Windows.Forms.Button();
            this.btnRevisionar = new System.Windows.Forms.Button();
            this.btnFinalizar = new System.Windows.Forms.Button();
            this.btnEnviar = new System.Windows.Forms.Button();
            this.btnImprimir = new System.Windows.Forms.Button();
            this.btnExcluir = new System.Windows.Forms.Button();
            this.btnReativar = new System.Windows.Forms.Button();
            this.btnConverter = new System.Windows.Forms.Button();
            this.btnFechar = new System.Windows.Forms.Button();
            this.textCodigo = new System.Windows.Forms.MaskedTextBox();
            this.lblCodigo = new System.Windows.Forms.TextBox();
            this.btnExcluirCliente = new System.Windows.Forms.Button();
            this.btnCliente = new System.Windows.Forms.Button();
            this.textCliente = new System.Windows.Forms.TextBox();
            this.lblCliente = new System.Windows.Forms.TextBox();
            this.lblDataElaboracao = new System.Windows.Forms.TextBox();
            this.dataInicial = new System.Windows.Forms.DateTimePicker();
            this.dataFinal = new System.Windows.Forms.DateTimePicker();
            this.cbSituacao = new SWS.ComboBoxWithBorder();
            this.lblSituacao = new System.Windows.Forms.TextBox();
            this.btnExcluirResponsavel = new System.Windows.Forms.Button();
            this.btnResponsavel = new System.Windows.Forms.Button();
            this.textResponsavel = new System.Windows.Forms.TextBox();
            this.lblResponsavel = new System.Windows.Forms.TextBox();
            this.btnExcluirElaborador = new System.Windows.Forms.Button();
            this.btnElaborador = new System.Windows.Forms.Button();
            this.textUsuarioElaborador = new System.Windows.Forms.TextBox();
            this.lblElaborador = new System.Windows.Forms.TextBox();
            this.grb_pcmso = new System.Windows.Forms.GroupBox();
            this.dgvPcmso = new System.Windows.Forms.DataGridView();
            this.lblTextoInformativoCorrecao = new System.Windows.Forms.Label();
            this.grbLegenda = new System.Windows.Forms.GroupBox();
            this.lblTextConstrucao = new System.Windows.Forms.Label();
            this.lblLegendaConstrucao = new System.Windows.Forms.Label();
            this.lblTextCancelado = new System.Windows.Forms.Label();
            this.lblStatusCancelado = new System.Windows.Forms.Label();
            this.textLegendaAnalise = new System.Windows.Forms.Label();
            this.lblLegendaAnalise = new System.Windows.Forms.Label();
            this.lblTextoFechado = new System.Windows.Forms.Label();
            this.lblLegendaFechado = new System.Windows.Forms.Label();
            this.tpPcmsoGerenciar = new System.Windows.Forms.ToolTip(this.components);
            this.btnReplicar = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.spForm)).BeginInit();
            this.spForm.Panel1.SuspendLayout();
            this.spForm.Panel2.SuspendLayout();
            this.spForm.SuspendLayout();
            this.pnlForm.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.banner)).BeginInit();
            this.flpAcao.SuspendLayout();
            this.grb_pcmso.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvPcmso)).BeginInit();
            this.grbLegenda.SuspendLayout();
            this.SuspendLayout();
            // 
            // spForm
            // 
            this.spForm.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            // 
            // spForm.Panel1
            // 
            this.spForm.Panel1.Controls.Add(this.flpAcao);
            this.spForm.Panel1MinSize = 50;
            this.spForm.SplitterDistance = 60;
            // 
            // pnlForm
            // 
            this.pnlForm.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.pnlForm.Controls.Add(this.lblTextoInformativoCorrecao);
            this.pnlForm.Controls.Add(this.grb_pcmso);
            this.pnlForm.Controls.Add(this.grbLegenda);
            this.pnlForm.Controls.Add(this.btnExcluirElaborador);
            this.pnlForm.Controls.Add(this.btnElaborador);
            this.pnlForm.Controls.Add(this.textUsuarioElaborador);
            this.pnlForm.Controls.Add(this.lblElaborador);
            this.pnlForm.Controls.Add(this.btnExcluirResponsavel);
            this.pnlForm.Controls.Add(this.btnResponsavel);
            this.pnlForm.Controls.Add(this.textResponsavel);
            this.pnlForm.Controls.Add(this.lblResponsavel);
            this.pnlForm.Controls.Add(this.cbSituacao);
            this.pnlForm.Controls.Add(this.lblSituacao);
            this.pnlForm.Controls.Add(this.lblDataElaboracao);
            this.pnlForm.Controls.Add(this.dataInicial);
            this.pnlForm.Controls.Add(this.dataFinal);
            this.pnlForm.Controls.Add(this.btnExcluirCliente);
            this.pnlForm.Controls.Add(this.btnCliente);
            this.pnlForm.Controls.Add(this.textCliente);
            this.pnlForm.Controls.Add(this.lblCliente);
            this.pnlForm.Controls.Add(this.textCodigo);
            this.pnlForm.Controls.Add(this.lblCodigo);
            this.pnlForm.Location = new System.Drawing.Point(0, 3);
            this.pnlForm.Size = new System.Drawing.Size(784, 436);
            // 
            // banner
            // 
            this.banner.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            // 
            // flpAcao
            // 
            this.flpAcao.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.flpAcao.Controls.Add(this.btnPesquisar);
            this.flpAcao.Controls.Add(this.btnIncluir);
            this.flpAcao.Controls.Add(this.btnAlterar);
            this.flpAcao.Controls.Add(this.btnCorrigir);
            this.flpAcao.Controls.Add(this.btnRevisionar);
            this.flpAcao.Controls.Add(this.btnFinalizar);
            this.flpAcao.Controls.Add(this.btnEnviar);
            this.flpAcao.Controls.Add(this.btnImprimir);
            this.flpAcao.Controls.Add(this.btnExcluir);
            this.flpAcao.Controls.Add(this.btnReativar);
            this.flpAcao.Controls.Add(this.btnConverter);
            this.flpAcao.Controls.Add(this.btnReplicar);
            this.flpAcao.Controls.Add(this.btnFechar);
            this.flpAcao.Location = new System.Drawing.Point(0, 0);
            this.flpAcao.Name = "flpAcao";
            this.flpAcao.Size = new System.Drawing.Size(784, 61);
            this.flpAcao.TabIndex = 0;
            // 
            // btnPesquisar
            // 
            this.btnPesquisar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnPesquisar.Image = global::SWS.Properties.Resources.lupa;
            this.btnPesquisar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnPesquisar.Location = new System.Drawing.Point(3, 3);
            this.btnPesquisar.Name = "btnPesquisar";
            this.btnPesquisar.Size = new System.Drawing.Size(75, 23);
            this.btnPesquisar.TabIndex = 34;
            this.btnPesquisar.Text = "&Pesquisar";
            this.btnPesquisar.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.tpPcmsoGerenciar.SetToolTip(this.btnPesquisar, "Pesquisar os PCMSOs de acordo com os filtros selecionados.");
            this.btnPesquisar.UseVisualStyleBackColor = true;
            this.btnPesquisar.Click += new System.EventHandler(this.btnPesquisar_Click);
            // 
            // btnIncluir
            // 
            this.btnIncluir.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnIncluir.Image = global::SWS.Properties.Resources.icone_mais;
            this.btnIncluir.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnIncluir.Location = new System.Drawing.Point(84, 3);
            this.btnIncluir.Name = "btnIncluir";
            this.btnIncluir.Size = new System.Drawing.Size(75, 23);
            this.btnIncluir.TabIndex = 33;
            this.btnIncluir.Text = "&Incluir";
            this.btnIncluir.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.tpPcmsoGerenciar.SetToolTip(this.btnIncluir, "incluir um novo PCMSO (normal ou avulso) no sistema.");
            this.btnIncluir.UseVisualStyleBackColor = true;
            this.btnIncluir.Click += new System.EventHandler(this.btnIncluir_Click);
            // 
            // btnAlterar
            // 
            this.btnAlterar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnAlterar.Image = global::SWS.Properties.Resources.Alterar;
            this.btnAlterar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnAlterar.Location = new System.Drawing.Point(165, 3);
            this.btnAlterar.Name = "btnAlterar";
            this.btnAlterar.Size = new System.Drawing.Size(75, 23);
            this.btnAlterar.TabIndex = 25;
            this.btnAlterar.Text = "&Alterar";
            this.btnAlterar.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.tpPcmsoGerenciar.SetToolTip(this.btnAlterar, "alterar O PCMSO selecionado.");
            this.btnAlterar.UseVisualStyleBackColor = true;
            this.btnAlterar.Click += new System.EventHandler(this.btnAlterar_Click);
            // 
            // btnCorrigir
            // 
            this.btnCorrigir.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnCorrigir.Image = global::SWS.Properties.Resources.crayon_icon_clip_art_p;
            this.btnCorrigir.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnCorrigir.Location = new System.Drawing.Point(246, 3);
            this.btnCorrigir.Name = "btnCorrigir";
            this.btnCorrigir.Size = new System.Drawing.Size(75, 23);
            this.btnCorrigir.TabIndex = 32;
            this.btnCorrigir.Text = "&Corrigir";
            this.btnCorrigir.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.tpPcmsoGerenciar.SetToolTip(this.btnCorrigir, "Essa função é usada para corrigir o pcmso sem necessidade\r\nde incluir uma revisão" +
        ". Porém esse procedimento deve ser \r\nfeito na mesma hora. ");
            this.btnCorrigir.UseVisualStyleBackColor = true;
            this.btnCorrigir.Click += new System.EventHandler(this.btnCorrigir_Click);
            // 
            // btnRevisionar
            // 
            this.btnRevisionar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnRevisionar.Image = global::SWS.Properties.Resources.devolucao;
            this.btnRevisionar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnRevisionar.Location = new System.Drawing.Point(327, 3);
            this.btnRevisionar.Name = "btnRevisionar";
            this.btnRevisionar.Size = new System.Drawing.Size(75, 23);
            this.btnRevisionar.TabIndex = 30;
            this.btnRevisionar.Text = "Revi&sionar";
            this.btnRevisionar.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.tpPcmsoGerenciar.SetToolTip(this.btnRevisionar, "Essa função cria uma nova revisão do PCMSO e será \r\ngerenciada pelo quadro de rev" +
        "isões.");
            this.btnRevisionar.UseVisualStyleBackColor = true;
            this.btnRevisionar.Click += new System.EventHandler(this.btnRevisionar_Click);
            // 
            // btnFinalizar
            // 
            this.btnFinalizar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnFinalizar.Image = global::SWS.Properties.Resources.fechar_ico;
            this.btnFinalizar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnFinalizar.Location = new System.Drawing.Point(408, 3);
            this.btnFinalizar.Name = "btnFinalizar";
            this.btnFinalizar.Size = new System.Drawing.Size(75, 23);
            this.btnFinalizar.TabIndex = 29;
            this.btnFinalizar.Text = "&Finalizar";
            this.btnFinalizar.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.tpPcmsoGerenciar.SetToolTip(this.btnFinalizar, "Finaliza um PCMSO que esteja em estado de aprovação.");
            this.btnFinalizar.UseVisualStyleBackColor = true;
            this.btnFinalizar.Click += new System.EventHandler(this.btnFinalizar_Click);
            // 
            // btnEnviar
            // 
            this.btnEnviar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnEnviar.Image = global::SWS.Properties.Resources.enviar_mensagem_318_10100;
            this.btnEnviar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnEnviar.Location = new System.Drawing.Point(489, 3);
            this.btnEnviar.Name = "btnEnviar";
            this.btnEnviar.Size = new System.Drawing.Size(75, 23);
            this.btnEnviar.TabIndex = 28;
            this.btnEnviar.Text = "En&viar";
            this.btnEnviar.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.tpPcmsoGerenciar.SetToolTip(this.btnEnviar, "Envia para análise do coordenador ou retorna para ");
            this.btnEnviar.UseVisualStyleBackColor = true;
            this.btnEnviar.Click += new System.EventHandler(this.btnEnviar_Click);
            // 
            // btnImprimir
            // 
            this.btnImprimir.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnImprimir.Image = global::SWS.Properties.Resources.impressora1;
            this.btnImprimir.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnImprimir.Location = new System.Drawing.Point(570, 3);
            this.btnImprimir.Name = "btnImprimir";
            this.btnImprimir.Size = new System.Drawing.Size(75, 23);
            this.btnImprimir.TabIndex = 27;
            this.btnImprimir.Text = "Imp&rimir";
            this.btnImprimir.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnImprimir.UseVisualStyleBackColor = true;
            this.btnImprimir.Click += new System.EventHandler(this.btnImprimir_Click);
            // 
            // btnExcluir
            // 
            this.btnExcluir.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnExcluir.Image = global::SWS.Properties.Resources.lixeira;
            this.btnExcluir.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnExcluir.Location = new System.Drawing.Point(651, 3);
            this.btnExcluir.Name = "btnExcluir";
            this.btnExcluir.Size = new System.Drawing.Size(75, 23);
            this.btnExcluir.TabIndex = 26;
            this.btnExcluir.Text = "&Excluir";
            this.btnExcluir.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnExcluir.UseVisualStyleBackColor = true;
            this.btnExcluir.Click += new System.EventHandler(this.btnExcluir_Click);
            // 
            // btnReativar
            // 
            this.btnReativar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnReativar.Image = global::SWS.Properties.Resources.go_back_98505;
            this.btnReativar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnReativar.Location = new System.Drawing.Point(3, 32);
            this.btnReativar.Name = "btnReativar";
            this.btnReativar.Size = new System.Drawing.Size(75, 23);
            this.btnReativar.TabIndex = 35;
            this.btnReativar.Text = "&Reativar";
            this.btnReativar.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnReativar.UseVisualStyleBackColor = true;
            this.btnReativar.Click += new System.EventHandler(this.btnReativar_Click);
            // 
            // btnConverter
            // 
            this.btnConverter.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnConverter.Image = global::SWS.Properties.Resources.protocolo;
            this.btnConverter.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnConverter.Location = new System.Drawing.Point(84, 32);
            this.btnConverter.Name = "btnConverter";
            this.btnConverter.Size = new System.Drawing.Size(75, 23);
            this.btnConverter.TabIndex = 36;
            this.btnConverter.Text = "&Converter";
            this.btnConverter.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnConverter.UseVisualStyleBackColor = true;
            this.btnConverter.Click += new System.EventHandler(this.btnConverter_Click);
            // 
            // btnFechar
            // 
            this.btnFechar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnFechar.Image = global::SWS.Properties.Resources.close;
            this.btnFechar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnFechar.Location = new System.Drawing.Point(246, 32);
            this.btnFechar.Name = "btnFechar";
            this.btnFechar.Size = new System.Drawing.Size(75, 23);
            this.btnFechar.TabIndex = 31;
            this.btnFechar.Text = "Fec&har";
            this.btnFechar.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnFechar.UseVisualStyleBackColor = true;
            this.btnFechar.Click += new System.EventHandler(this.btnFechar_Click);
            // 
            // textCodigo
            // 
            this.textCodigo.BackColor = System.Drawing.Color.White;
            this.textCodigo.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textCodigo.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textCodigo.Location = new System.Drawing.Point(140, 3);
            this.textCodigo.Mask = "0000,00,000000/00";
            this.textCodigo.Name = "textCodigo";
            this.textCodigo.PromptChar = ' ';
            this.textCodigo.Size = new System.Drawing.Size(213, 21);
            this.textCodigo.TabIndex = 41;
            // 
            // lblCodigo
            // 
            this.lblCodigo.BackColor = System.Drawing.Color.LightGray;
            this.lblCodigo.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblCodigo.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCodigo.Location = new System.Drawing.Point(3, 3);
            this.lblCodigo.Name = "lblCodigo";
            this.lblCodigo.ReadOnly = true;
            this.lblCodigo.Size = new System.Drawing.Size(138, 21);
            this.lblCodigo.TabIndex = 40;
            this.lblCodigo.TabStop = false;
            this.lblCodigo.Text = "Código do Estudo";
            // 
            // btnExcluirCliente
            // 
            this.btnExcluirCliente.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnExcluirCliente.Image = global::SWS.Properties.Resources.close;
            this.btnExcluirCliente.Location = new System.Drawing.Point(731, 3);
            this.btnExcluirCliente.Name = "btnExcluirCliente";
            this.btnExcluirCliente.Size = new System.Drawing.Size(30, 21);
            this.btnExcluirCliente.TabIndex = 55;
            this.btnExcluirCliente.UseVisualStyleBackColor = true;
            this.btnExcluirCliente.Click += new System.EventHandler(this.btnExcluirCliente_Click);
            // 
            // btnCliente
            // 
            this.btnCliente.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnCliente.Image = global::SWS.Properties.Resources.busca;
            this.btnCliente.Location = new System.Drawing.Point(702, 3);
            this.btnCliente.Name = "btnCliente";
            this.btnCliente.Size = new System.Drawing.Size(30, 21);
            this.btnCliente.TabIndex = 53;
            this.btnCliente.UseVisualStyleBackColor = true;
            this.btnCliente.Click += new System.EventHandler(this.btnCliente_Click);
            // 
            // textCliente
            // 
            this.textCliente.BackColor = System.Drawing.SystemColors.Control;
            this.textCliente.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textCliente.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textCliente.Location = new System.Drawing.Point(494, 3);
            this.textCliente.MaxLength = 100;
            this.textCliente.Name = "textCliente";
            this.textCliente.ReadOnly = true;
            this.textCliente.Size = new System.Drawing.Size(210, 21);
            this.textCliente.TabIndex = 54;
            this.textCliente.TabStop = false;
            // 
            // lblCliente
            // 
            this.lblCliente.BackColor = System.Drawing.Color.LightGray;
            this.lblCliente.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblCliente.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCliente.Location = new System.Drawing.Point(357, 3);
            this.lblCliente.Name = "lblCliente";
            this.lblCliente.ReadOnly = true;
            this.lblCliente.Size = new System.Drawing.Size(138, 21);
            this.lblCliente.TabIndex = 52;
            this.lblCliente.TabStop = false;
            this.lblCliente.Text = "Cliente";
            // 
            // lblDataElaboracao
            // 
            this.lblDataElaboracao.BackColor = System.Drawing.Color.LightGray;
            this.lblDataElaboracao.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblDataElaboracao.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDataElaboracao.Location = new System.Drawing.Point(3, 23);
            this.lblDataElaboracao.Name = "lblDataElaboracao";
            this.lblDataElaboracao.ReadOnly = true;
            this.lblDataElaboracao.Size = new System.Drawing.Size(138, 21);
            this.lblDataElaboracao.TabIndex = 56;
            this.lblDataElaboracao.TabStop = false;
            this.lblDataElaboracao.Text = "Data de criação";
            // 
            // dataInicial
            // 
            this.dataInicial.Checked = false;
            this.dataInicial.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dataInicial.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dataInicial.Location = new System.Drawing.Point(140, 23);
            this.dataInicial.Name = "dataInicial";
            this.dataInicial.ShowCheckBox = true;
            this.dataInicial.Size = new System.Drawing.Size(107, 21);
            this.dataInicial.TabIndex = 57;
            // 
            // dataFinal
            // 
            this.dataFinal.Checked = false;
            this.dataFinal.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dataFinal.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dataFinal.Location = new System.Drawing.Point(246, 23);
            this.dataFinal.Name = "dataFinal";
            this.dataFinal.ShowCheckBox = true;
            this.dataFinal.Size = new System.Drawing.Size(107, 21);
            this.dataFinal.TabIndex = 58;
            // 
            // cbSituacao
            // 
            this.cbSituacao.BackColor = System.Drawing.Color.LightGray;
            this.cbSituacao.BorderColor = System.Drawing.Color.DimGray;
            this.cbSituacao.BorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid;
            this.cbSituacao.FormattingEnabled = true;
            this.cbSituacao.Location = new System.Drawing.Point(140, 43);
            this.cbSituacao.Name = "cbSituacao";
            this.cbSituacao.Size = new System.Drawing.Size(213, 21);
            this.cbSituacao.TabIndex = 59;
            // 
            // lblSituacao
            // 
            this.lblSituacao.BackColor = System.Drawing.Color.LightGray;
            this.lblSituacao.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblSituacao.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSituacao.Location = new System.Drawing.Point(3, 43);
            this.lblSituacao.Name = "lblSituacao";
            this.lblSituacao.ReadOnly = true;
            this.lblSituacao.Size = new System.Drawing.Size(138, 21);
            this.lblSituacao.TabIndex = 60;
            this.lblSituacao.TabStop = false;
            this.lblSituacao.Text = "Situação";
            // 
            // btnExcluirResponsavel
            // 
            this.btnExcluirResponsavel.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnExcluirResponsavel.Image = global::SWS.Properties.Resources.close;
            this.btnExcluirResponsavel.Location = new System.Drawing.Point(731, 23);
            this.btnExcluirResponsavel.Name = "btnExcluirResponsavel";
            this.btnExcluirResponsavel.Size = new System.Drawing.Size(30, 21);
            this.btnExcluirResponsavel.TabIndex = 64;
            this.btnExcluirResponsavel.UseVisualStyleBackColor = true;
            this.btnExcluirResponsavel.Click += new System.EventHandler(this.btnExcluirResponsavel_Click);
            // 
            // btnResponsavel
            // 
            this.btnResponsavel.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnResponsavel.Image = global::SWS.Properties.Resources.busca;
            this.btnResponsavel.Location = new System.Drawing.Point(702, 23);
            this.btnResponsavel.Name = "btnResponsavel";
            this.btnResponsavel.Size = new System.Drawing.Size(30, 21);
            this.btnResponsavel.TabIndex = 62;
            this.btnResponsavel.UseVisualStyleBackColor = true;
            this.btnResponsavel.Click += new System.EventHandler(this.btnResponsavel_Click);
            // 
            // textResponsavel
            // 
            this.textResponsavel.BackColor = System.Drawing.SystemColors.Control;
            this.textResponsavel.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textResponsavel.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textResponsavel.Location = new System.Drawing.Point(494, 23);
            this.textResponsavel.MaxLength = 100;
            this.textResponsavel.Name = "textResponsavel";
            this.textResponsavel.ReadOnly = true;
            this.textResponsavel.Size = new System.Drawing.Size(210, 21);
            this.textResponsavel.TabIndex = 63;
            this.textResponsavel.TabStop = false;
            // 
            // lblResponsavel
            // 
            this.lblResponsavel.BackColor = System.Drawing.Color.LightGray;
            this.lblResponsavel.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblResponsavel.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblResponsavel.Location = new System.Drawing.Point(357, 23);
            this.lblResponsavel.Name = "lblResponsavel";
            this.lblResponsavel.ReadOnly = true;
            this.lblResponsavel.Size = new System.Drawing.Size(138, 21);
            this.lblResponsavel.TabIndex = 61;
            this.lblResponsavel.TabStop = false;
            this.lblResponsavel.Text = "Responsável";
            // 
            // btnExcluirElaborador
            // 
            this.btnExcluirElaborador.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnExcluirElaborador.Image = global::SWS.Properties.Resources.close;
            this.btnExcluirElaborador.Location = new System.Drawing.Point(731, 43);
            this.btnExcluirElaborador.Name = "btnExcluirElaborador";
            this.btnExcluirElaborador.Size = new System.Drawing.Size(30, 21);
            this.btnExcluirElaborador.TabIndex = 68;
            this.btnExcluirElaborador.UseVisualStyleBackColor = true;
            this.btnExcluirElaborador.Click += new System.EventHandler(this.btnExcluirElaborador_Click);
            // 
            // btnElaborador
            // 
            this.btnElaborador.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnElaborador.Image = global::SWS.Properties.Resources.busca;
            this.btnElaborador.Location = new System.Drawing.Point(702, 43);
            this.btnElaborador.Name = "btnElaborador";
            this.btnElaborador.Size = new System.Drawing.Size(30, 21);
            this.btnElaborador.TabIndex = 66;
            this.btnElaborador.UseVisualStyleBackColor = true;
            this.btnElaborador.Click += new System.EventHandler(this.btnElaborador_Click);
            // 
            // textUsuarioElaborador
            // 
            this.textUsuarioElaborador.BackColor = System.Drawing.SystemColors.Control;
            this.textUsuarioElaborador.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textUsuarioElaborador.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textUsuarioElaborador.Location = new System.Drawing.Point(494, 43);
            this.textUsuarioElaborador.MaxLength = 100;
            this.textUsuarioElaborador.Name = "textUsuarioElaborador";
            this.textUsuarioElaborador.ReadOnly = true;
            this.textUsuarioElaborador.Size = new System.Drawing.Size(210, 21);
            this.textUsuarioElaborador.TabIndex = 67;
            this.textUsuarioElaborador.TabStop = false;
            // 
            // lblElaborador
            // 
            this.lblElaborador.BackColor = System.Drawing.Color.LightGray;
            this.lblElaborador.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblElaborador.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblElaborador.Location = new System.Drawing.Point(357, 43);
            this.lblElaborador.Name = "lblElaborador";
            this.lblElaborador.ReadOnly = true;
            this.lblElaborador.Size = new System.Drawing.Size(138, 21);
            this.lblElaborador.TabIndex = 65;
            this.lblElaborador.TabStop = false;
            this.lblElaborador.Text = "Elaborador";
            // 
            // grb_pcmso
            // 
            this.grb_pcmso.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.grb_pcmso.Controls.Add(this.dgvPcmso);
            this.grb_pcmso.Location = new System.Drawing.Point(3, 70);
            this.grb_pcmso.Name = "grb_pcmso";
            this.grb_pcmso.Size = new System.Drawing.Size(769, 269);
            this.grb_pcmso.TabIndex = 69;
            this.grb_pcmso.TabStop = false;
            this.grb_pcmso.Text = "PCMSO";
            // 
            // dgvPcmso
            // 
            this.dgvPcmso.AllowUserToAddRows = false;
            this.dgvPcmso.AllowUserToDeleteRows = false;
            this.dgvPcmso.AllowUserToOrderColumns = true;
            this.dgvPcmso.AllowUserToResizeRows = false;
            this.dgvPcmso.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this.dgvPcmso.BackgroundColor = System.Drawing.Color.White;
            this.dgvPcmso.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.LightGray;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvPcmso.DefaultCellStyle = dataGridViewCellStyle1;
            this.dgvPcmso.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvPcmso.Location = new System.Drawing.Point(3, 16);
            this.dgvPcmso.MultiSelect = false;
            this.dgvPcmso.Name = "dgvPcmso";
            this.dgvPcmso.ReadOnly = true;
            this.dgvPcmso.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvPcmso.Size = new System.Drawing.Size(763, 250);
            this.dgvPcmso.TabIndex = 14;
            this.dgvPcmso.CellContentDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvPcmso_CellContentDoubleClick);
            this.dgvPcmso.RowPrePaint += new System.Windows.Forms.DataGridViewRowPrePaintEventHandler(this.dgvPcmso_RowPrePaint);
            // 
            // lblTextoInformativoCorrecao
            // 
            this.lblTextoInformativoCorrecao.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.lblTextoInformativoCorrecao.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTextoInformativoCorrecao.ForeColor = System.Drawing.Color.Red;
            this.lblTextoInformativoCorrecao.Location = new System.Drawing.Point(494, 352);
            this.lblTextoInformativoCorrecao.Name = "lblTextoInformativoCorrecao";
            this.lblTextoInformativoCorrecao.Size = new System.Drawing.Size(278, 37);
            this.lblTextoInformativoCorrecao.TabIndex = 107;
            this.lblTextoInformativoCorrecao.Text = "Duplo clique no estudo selecionado para visualizar os históricos\r\nde correções.";
            // 
            // grbLegenda
            // 
            this.grbLegenda.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.grbLegenda.Controls.Add(this.lblTextConstrucao);
            this.grbLegenda.Controls.Add(this.lblLegendaConstrucao);
            this.grbLegenda.Controls.Add(this.lblTextCancelado);
            this.grbLegenda.Controls.Add(this.lblStatusCancelado);
            this.grbLegenda.Controls.Add(this.textLegendaAnalise);
            this.grbLegenda.Controls.Add(this.lblLegendaAnalise);
            this.grbLegenda.Controls.Add(this.lblTextoFechado);
            this.grbLegenda.Controls.Add(this.lblLegendaFechado);
            this.grbLegenda.Location = new System.Drawing.Point(6, 345);
            this.grbLegenda.Name = "grbLegenda";
            this.grbLegenda.Size = new System.Drawing.Size(376, 44);
            this.grbLegenda.TabIndex = 106;
            this.grbLegenda.TabStop = false;
            this.grbLegenda.Text = "Legenda";
            // 
            // lblTextConstrucao
            // 
            this.lblTextConstrucao.AutoSize = true;
            this.lblTextConstrucao.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTextConstrucao.Location = new System.Drawing.Point(289, 18);
            this.lblTextConstrucao.Name = "lblTextConstrucao";
            this.lblTextConstrucao.Size = new System.Drawing.Size(69, 15);
            this.lblTextConstrucao.TabIndex = 7;
            this.lblTextConstrucao.Text = "Construção";
            // 
            // lblLegendaConstrucao
            // 
            this.lblLegendaConstrucao.AutoSize = true;
            this.lblLegendaConstrucao.BackColor = System.Drawing.Color.Black;
            this.lblLegendaConstrucao.ForeColor = System.Drawing.Color.Black;
            this.lblLegendaConstrucao.Location = new System.Drawing.Point(270, 20);
            this.lblLegendaConstrucao.Name = "lblLegendaConstrucao";
            this.lblLegendaConstrucao.Size = new System.Drawing.Size(13, 13);
            this.lblLegendaConstrucao.TabIndex = 6;
            this.lblLegendaConstrucao.Text = "  ";
            // 
            // lblTextCancelado
            // 
            this.lblTextCancelado.AutoSize = true;
            this.lblTextCancelado.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTextCancelado.Location = new System.Drawing.Point(210, 18);
            this.lblTextCancelado.Name = "lblTextCancelado";
            this.lblTextCancelado.Size = new System.Drawing.Size(54, 15);
            this.lblTextCancelado.TabIndex = 5;
            this.lblTextCancelado.Text = "Excluído";
            // 
            // lblStatusCancelado
            // 
            this.lblStatusCancelado.AutoSize = true;
            this.lblStatusCancelado.BackColor = System.Drawing.Color.Red;
            this.lblStatusCancelado.ForeColor = System.Drawing.Color.Black;
            this.lblStatusCancelado.Location = new System.Drawing.Point(194, 20);
            this.lblStatusCancelado.Name = "lblStatusCancelado";
            this.lblStatusCancelado.Size = new System.Drawing.Size(13, 13);
            this.lblStatusCancelado.TabIndex = 4;
            this.lblStatusCancelado.Text = "  ";
            // 
            // textLegendaAnalise
            // 
            this.textLegendaAnalise.AutoSize = true;
            this.textLegendaAnalise.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textLegendaAnalise.Location = new System.Drawing.Point(119, 18);
            this.textLegendaAnalise.Name = "textLegendaAnalise";
            this.textLegendaAnalise.Size = new System.Drawing.Size(69, 15);
            this.textLegendaAnalise.TabIndex = 3;
            this.textLegendaAnalise.Text = "Em análise";
            // 
            // lblLegendaAnalise
            // 
            this.lblLegendaAnalise.AutoSize = true;
            this.lblLegendaAnalise.BackColor = System.Drawing.Color.Blue;
            this.lblLegendaAnalise.ForeColor = System.Drawing.Color.Black;
            this.lblLegendaAnalise.Location = new System.Drawing.Point(100, 20);
            this.lblLegendaAnalise.Name = "lblLegendaAnalise";
            this.lblLegendaAnalise.Size = new System.Drawing.Size(13, 13);
            this.lblLegendaAnalise.TabIndex = 2;
            this.lblLegendaAnalise.Text = "  ";
            // 
            // lblTextoFechado
            // 
            this.lblTextoFechado.AutoSize = true;
            this.lblTextoFechado.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTextoFechado.Location = new System.Drawing.Point(30, 18);
            this.lblTextoFechado.Name = "lblTextoFechado";
            this.lblTextoFechado.Size = new System.Drawing.Size(64, 15);
            this.lblTextoFechado.TabIndex = 1;
            this.lblTextoFechado.Text = "Finalizado";
            // 
            // lblLegendaFechado
            // 
            this.lblLegendaFechado.AutoSize = true;
            this.lblLegendaFechado.BackColor = System.Drawing.Color.Green;
            this.lblLegendaFechado.ForeColor = System.Drawing.Color.Black;
            this.lblLegendaFechado.Location = new System.Drawing.Point(11, 20);
            this.lblLegendaFechado.Name = "lblLegendaFechado";
            this.lblLegendaFechado.Size = new System.Drawing.Size(13, 13);
            this.lblLegendaFechado.TabIndex = 0;
            this.lblLegendaFechado.Text = "  ";
            // 
            // tpPcmsoGerenciar
            // 
            this.tpPcmsoGerenciar.BackColor = System.Drawing.Color.White;
            this.tpPcmsoGerenciar.ForeColor = System.Drawing.Color.Blue;
            this.tpPcmsoGerenciar.IsBalloon = true;
            this.tpPcmsoGerenciar.ToolTipIcon = System.Windows.Forms.ToolTipIcon.Info;
            // 
            // btnReplicar
            // 
            this.btnReplicar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnReplicar.Image = global::SWS.Properties.Resources.icones_2;
            this.btnReplicar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnReplicar.Location = new System.Drawing.Point(165, 32);
            this.btnReplicar.Name = "btnReplicar";
            this.btnReplicar.Size = new System.Drawing.Size(75, 23);
            this.btnReplicar.TabIndex = 37;
            this.btnReplicar.Text = "Replicar";
            this.btnReplicar.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnReplicar.UseVisualStyleBackColor = true;
            this.btnReplicar.Click += new System.EventHandler(this.btnReplicar_Click);
            // 
            // frmPcmsoPrincipal
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(784, 561);
            this.MaximizeBox = true;
            this.Name = "frmPcmsoPrincipal";
            this.Text = "GERENCIAR PCMSO";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.spForm.Panel1.ResumeLayout(false);
            this.spForm.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.spForm)).EndInit();
            this.spForm.ResumeLayout(false);
            this.pnlForm.ResumeLayout(false);
            this.pnlForm.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.banner)).EndInit();
            this.flpAcao.ResumeLayout(false);
            this.grb_pcmso.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvPcmso)).EndInit();
            this.grbLegenda.ResumeLayout(false);
            this.grbLegenda.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.FlowLayoutPanel flpAcao;
        private System.Windows.Forms.Button btnIncluir;
        private System.Windows.Forms.Button btnCorrigir;
        private System.Windows.Forms.Button btnFechar;
        private System.Windows.Forms.Button btnRevisionar;
        private System.Windows.Forms.Button btnFinalizar;
        private System.Windows.Forms.Button btnEnviar;
        private System.Windows.Forms.Button btnImprimir;
        private System.Windows.Forms.Button btnExcluir;
        private System.Windows.Forms.Button btnAlterar;
        public System.Windows.Forms.MaskedTextBox textCodigo;
        private System.Windows.Forms.TextBox lblCodigo;
        private System.Windows.Forms.Button btnExcluirCliente;
        private System.Windows.Forms.Button btnCliente;
        public System.Windows.Forms.TextBox textCliente;
        private System.Windows.Forms.TextBox lblCliente;
        private System.Windows.Forms.TextBox lblDataElaboracao;
        private System.Windows.Forms.DateTimePicker dataInicial;
        private System.Windows.Forms.DateTimePicker dataFinal;
        private ComboBoxWithBorder cbSituacao;
        private System.Windows.Forms.TextBox lblSituacao;
        private System.Windows.Forms.Button btnExcluirResponsavel;
        private System.Windows.Forms.Button btnResponsavel;
        public System.Windows.Forms.TextBox textResponsavel;
        private System.Windows.Forms.TextBox lblResponsavel;
        private System.Windows.Forms.Button btnExcluirElaborador;
        private System.Windows.Forms.Button btnElaborador;
        public System.Windows.Forms.TextBox textUsuarioElaborador;
        private System.Windows.Forms.TextBox lblElaborador;
        private System.Windows.Forms.GroupBox grb_pcmso;
        private System.Windows.Forms.DataGridView dgvPcmso;
        private System.Windows.Forms.Label lblTextoInformativoCorrecao;
        private System.Windows.Forms.GroupBox grbLegenda;
        private System.Windows.Forms.Label textLegendaAnalise;
        private System.Windows.Forms.Label lblLegendaAnalise;
        private System.Windows.Forms.Label lblTextoFechado;
        private System.Windows.Forms.Label lblLegendaFechado;
        private System.Windows.Forms.Button btnPesquisar;
        private System.Windows.Forms.Button btnReativar;
        private System.Windows.Forms.ToolTip tpPcmsoGerenciar;
        private System.Windows.Forms.Button btnConverter;
        private System.Windows.Forms.Label lblTextConstrucao;
        private System.Windows.Forms.Label lblLegendaConstrucao;
        private System.Windows.Forms.Label lblTextCancelado;
        private System.Windows.Forms.Label lblStatusCancelado;
        private System.Windows.Forms.Button btnReplicar;
    }
}