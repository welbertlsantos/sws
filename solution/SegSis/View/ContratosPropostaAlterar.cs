﻿using SWS.Entidade;
using SWS.Facade;
using SWS.View.ViewHelper;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace SWS.View
{
    public partial class frmContratosPropostaAlterar : frmContratosPropostaIncluir
    {
        public frmContratosPropostaAlterar(Contrato proposta) :base()
        {
            InitializeComponent();
            this.proposta = proposta;

            textCodigo.Text = proposta.CodigoContrato;
            dataElaboracao.Text = proposta.DataElaboracao.ToString();
            dataVencimento.Text = proposta.DataFim.ToString();
            cliente = proposta.Cliente;
            textCliente.Text = proposta.Cliente.RazaoSocial;
            if (cliente != null)
                btnCliente.Enabled = false;

            clienteProposta = proposta.ClienteProposta;
            textPreCliente.Text = proposta.ClienteProposta.RazaoSocial;
            if (clienteProposta != null)
                btnPreCliente.Enabled = false;

            MontaDataGridExame();
            MontaDataGridProduto();
            textComentario.Text = proposta.Observacao;
            textFormaPagamento.Text = proposta.PropostaFormaPagamento;
        }

        protected override void MontaDataGridExame()
        {
            try
            {
                dgvExame.Columns.Clear();

                FinanceiroFacade financeiroFacade = FinanceiroFacade.getInstance();

                DataSet ds = financeiroFacade.findAllContratoExameByContrato(proposta);

                dgvExame.ColumnCount = 13;

                dgvExame.Columns[0].HeaderText = "Id_contrato_exame";
                dgvExame.Columns[0].Visible = false;

                dgvExame.Columns[1].HeaderText = "Código";
                dgvExame.Columns[1].ReadOnly = true;

                dgvExame.Columns[2].HeaderText = "Exame";
                dgvExame.Columns[2].ReadOnly = true;

                dgvExame.Columns[3].HeaderText = "Preço no Contrato R$";
                dgvExame.Columns[3].ReadOnly = false;
                dgvExame.Columns[3].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
                dgvExame.Columns[3].HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleRight;
                dgvExame.Columns[3].DefaultCellStyle.BackColor = Color.LemonChiffon;

                dgvExame.Columns[4].HeaderText = "Custo no Contrato R$";
                dgvExame.Columns[4].ReadOnly = false;
                dgvExame.Columns[4].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
                dgvExame.Columns[4].HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleRight;
                dgvExame.Columns[4].DefaultCellStyle.BackColor = Color.LemonChiffon;

                dgvExame.Columns[5].HeaderText = "Data Inclusão";
                dgvExame.Columns[5].Visible = false;

                dgvExame.Columns[6].HeaderText = "Situacao";
                dgvExame.Columns[6].Visible = false;

                dgvExame.Columns[7].HeaderText = "flagAltera";
                dgvExame.Columns[7].Visible = false;

                dgvExame.Columns[8].HeaderText = "Situação Exame";
                dgvExame.Columns[8].Visible = false;

                dgvExame.Columns[9].HeaderText = "Laboratório";
                dgvExame.Columns[9].Visible = false;

                dgvExame.Columns[10].HeaderText = "Prioridade";
                dgvExame.Columns[10].Visible = false;

                dgvExame.Columns[11].HeaderText = "Externo";
                dgvExame.Columns[11].Visible = false;

                dgvExame.Columns[12].HeaderText = "Libera Documento";
                dgvExame.Columns[12].Visible = false;

                foreach (DataRow dataRow in ds.Tables[0].Rows)
                    dgvExame.Rows.Add(dataRow["id_contrato_exame"], dataRow["id_exame"], dataRow["descricao"], dataRow["preco_contrato"], dataRow["custo_contrato"], dataRow["data_inclusao"], dataRow["situacao"], dataRow["altera"], dataRow["situacao_exame"], dataRow["laboratorio"], dataRow["prioridade"], dataRow["externo"], dataRow["libera_documento"]);

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        protected override void MontaDataGridProduto()
        {
            try
            {
                dgvProduto.Columns.Clear();

                FinanceiroFacade financeiroFacade = FinanceiroFacade.getInstance();

                DataSet ds = financeiroFacade.findAllContratoProdutoByContrato(proposta);

                dgvProduto.ColumnCount = 12;

                dgvProduto.Columns[0].HeaderText = "Id_Produto_contrato";
                dgvProduto.Columns[0].Visible = false;

                dgvProduto.Columns[1].HeaderText = "Código";
                dgvProduto.Columns[1].ReadOnly = true;

                dgvProduto.Columns[2].HeaderText = "Produto";
                dgvProduto.Columns[2].ReadOnly = true;

                dgvProduto.Columns[3].HeaderText = "Preço no Contrato R$";
                dgvProduto.Columns[3].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
                dgvProduto.Columns[3].HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleRight;
                dgvProduto.Columns[3].DefaultCellStyle.BackColor = Color.LemonChiffon;

                dgvProduto.Columns[4].HeaderText = "Custo no Contrato R$";
                dgvProduto.Columns[4].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
                dgvProduto.Columns[4].HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleRight;
                dgvProduto.Columns[4].DefaultCellStyle.BackColor = Color.LemonChiffon;

                dgvProduto.Columns[5].HeaderText = "Data Inclusão";
                dgvProduto.Columns[5].Visible = false;

                dgvProduto.Columns[6].HeaderText = "Situacao";
                dgvProduto.Columns[6].Visible = false;

                dgvProduto.Columns[7].HeaderText = "FlagAltera";
                dgvProduto.Columns[7].Visible = false;

                dgvProduto.Columns[8].HeaderText = "Situação Produto";
                dgvProduto.Columns[8].Visible = false;

                dgvProduto.Columns[9].HeaderText = "Tipo";
                dgvProduto.Columns[9].Visible = false;

                dgvProduto.Columns[10].HeaderText = "Tipo Estudo";
                dgvProduto.Columns[10].Visible = false;

                dgvProduto.Columns[11].HeaderText = "Padrao Contrato";
                dgvProduto.Columns[11].Visible = false;

                foreach (DataRow dataRow in ds.Tables[0].Rows)
                    dgvProduto.Rows.Add(dataRow["id_produto_contrato"], dataRow["id_produto"], dataRow["nome"], dataRow["preco_contratado"], dataRow["custo_contrato"], dataRow["data_inclusao"], dataRow["situacao"], dataRow["altera"], dataRow["situacao_produto"], dataRow["tipo"], dataRow["tipo_estudo"], dataRow["padrao_contrato"]);


            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        protected override void btnIncluirExame_Click(object sender, EventArgs e)
        {
            try
            {
                frmExameBuscar exameBuscar = new frmExameBuscar();
                exameBuscar.ShowDialog();

                if (exameBuscar.Exame != null)
                {
                    /* verificando se o exame não está presente na grid */

                    List<Exame> examesInGrid = montaListaExamesInGrid();

                    if (examesInGrid.Contains(exameBuscar.Exame))
                        throw new Exception("Exame já incluído.");

                    /* incluindo o exame no contrato do cliente */

                    FinanceiroFacade financeiroFacade = FinanceiroFacade.getInstance();

                    financeiroFacade.insertContratoExame(new ContratoExame(null, exameBuscar.Exame, proposta, exameBuscar.Exame.Preco, PermissionamentoFacade.usuarioAutenticado, exameBuscar.Exame.Custo, DateTime.Now, "A", true));
                    MontaDataGridExame();

                    /* posicionando a seleção na linha inserida */
                    int index = 0;
                    foreach (DataGridViewRow dvRow in dgvExame.Rows)
                        if ((long)dvRow.Cells[1].Value == exameBuscar.Exame.Id)
                        {
                            index = dvRow.Index;
                            break;
                        }

                    dgvExame.CurrentCell = dgvExame[1, index];
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        protected override List<Exame> montaListaExamesInGrid()
        {
            List<Exame> examesInGrid = new List<Exame>();

            foreach (DataGridViewRow dvRow in dgvExame.Rows)
                examesInGrid.Add(new Exame((long)dvRow.Cells[1].Value, dvRow.Cells[2].Value.ToString(), dvRow.Cells[8].Value.ToString(), Convert.ToBoolean(dvRow.Cells[9].Value), Convert.ToDecimal(dvRow.Cells[3].Value), Convert.ToInt32(dvRow.Cells[10].Value), Convert.ToDecimal(dvRow.Cells[4].Value), Convert.ToBoolean(dvRow.Cells[11].Value), Convert.ToBoolean(dvRow.Cells[12].Value), string.Empty, false, null, false));

            return examesInGrid;
        }

        protected override List<Produto> montaListaProdutosInGrid()
        {
            List<Produto> produtosInGrid = new List<Produto>();

            foreach (DataGridViewRow dvRow in dgvProduto.Rows)
                produtosInGrid.Add(new Produto((long)dvRow.Cells[1].Value, dvRow.Cells[2].Value.ToString(), dvRow.Cells[8].Value.ToString(), Convert.ToDecimal(dvRow.Cells[3].Value), dvRow.Cells[9].Value.ToString(), Convert.ToDecimal(dvRow.Cells[4].Value), (bool)dvRow.Cells[11].Value, string.Empty));

            return produtosInGrid;
        }

        protected override void btnExcluirExame_Click(object sender, EventArgs e)
        {
            try
            {

                if (dgvExame.CurrentRow == null)
                    throw new Exception("Selecione um exame.");

                /* excluindo o exame do contrato do cliente */

                FinanceiroFacade financeiroFacade = FinanceiroFacade.getInstance();

                financeiroFacade.deleteContratoExame(new ContratoExame((long)dgvExame.CurrentRow.Cells[0].Value, new Exame((long)dgvExame.CurrentRow.Cells[1].Value, dgvExame.CurrentRow.Cells[2].Value.ToString(), dgvExame.CurrentRow.Cells[8].Value.ToString(), Convert.ToBoolean(dgvExame.CurrentRow.Cells[9].Value), Convert.ToDecimal(dgvExame.CurrentRow.Cells[3].Value), (int)dgvExame.CurrentRow.Cells[10].Value, Convert.ToDecimal(dgvExame.CurrentRow.Cells[4].Value), Convert.ToBoolean(dgvExame.CurrentRow.Cells[11].Value), Convert.ToBoolean(dgvExame.CurrentRow.Cells[12].Value), string.Empty, false, null, false), proposta, Convert.ToDecimal(dgvExame.CurrentRow.Cells[3].Value), PermissionamentoFacade.usuarioAutenticado, Convert.ToDecimal(dgvExame.CurrentRow.Cells[4].Value), Convert.ToDateTime(dgvExame.CurrentRow.Cells[5].Value), dgvExame.CurrentRow.Cells[6].Value.ToString(), Convert.ToBoolean(dgvExame.CurrentRow.Cells[7].Value)));

                MessageBox.Show("Exame excluído com sucesso.", "Confirmação", MessageBoxButtons.OK, MessageBoxIcon.Information);

                MontaDataGridExame();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        protected override void dgvExame_CellEndEdit(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                if (String.IsNullOrEmpty(dgvExame.CurrentCell.EditedFormattedValue.ToString()))
                    dgvExame.CurrentCell.Value = dgvExame.CurrentCell.Tag.ToString();

                dgvExame.CurrentCell.Value = ValidaCampoHelper.FormataValorMonetario(dgvExame.CurrentCell.Value.ToString());

                /* realizando update do contratoExame */

                FinanceiroFacade financeiroFacade = FinanceiroFacade.getInstance();

                financeiroFacade.updateContratoExame(new ContratoExame((long)dgvExame.Rows[e.RowIndex].Cells[0].Value, new Exame((long)dgvExame.CurrentRow.Cells[1].Value, dgvExame.CurrentRow.Cells[2].Value.ToString(), dgvExame.CurrentRow.Cells[8].Value.ToString(), Convert.ToBoolean(dgvExame.CurrentRow.Cells[9].Value), Convert.ToDecimal(dgvExame.CurrentRow.Cells[3].Value), (int)dgvExame.CurrentRow.Cells[10].Value, Convert.ToDecimal(dgvExame.CurrentRow.Cells[4].Value), Convert.ToBoolean(dgvExame.CurrentRow.Cells[11].Value), Convert.ToBoolean(dgvExame.CurrentRow.Cells[12].Value), string.Empty, false, null, false), proposta, Convert.ToDecimal(dgvExame.Rows[e.RowIndex].Cells[3].EditedFormattedValue), PermissionamentoFacade.usuarioAutenticado, Convert.ToDecimal(dgvExame.Rows[e.RowIndex].Cells[4].EditedFormattedValue), Convert.ToDateTime(dgvExame.Rows[e.RowIndex].Cells[5].Value), dgvExame.Rows[e.RowIndex].Cells[6].Value.ToString(), Convert.ToBoolean(dgvExame.Rows[e.RowIndex].Cells[7].Value)));
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }

        protected override void btnIncluirProduto_Click(object sender, EventArgs e)
        {
            try
            {

                frmProdutoBusca produtoBusca = new frmProdutoBusca();
                produtoBusca.ShowDialog();

                if (produtoBusca.Produto != null)
                {
                    /* verificando se o produto selecionado não está incluído. */

                    List<Produto> produtosInGrid = montaListaProdutosInGrid();

                    if (produtosInGrid.Contains(produtoBusca.Produto))
                        throw new Exception("Produto já incluído.");

                    /* incluindo o produto no contrato do cliente */

                    FinanceiroFacade financeiroFacade = FinanceiroFacade.getInstance();

                    financeiroFacade.insertContratoProduto(new ContratoProduto(null, produtoBusca.Produto, proposta, produtoBusca.Produto.Preco, PermissionamentoFacade.usuarioAutenticado, produtoBusca.Produto.Custo, DateTime.Now, "A", true));

                    MontaDataGridProduto();

                    /* posicionando a seleção na linha inserida */
                    int index = 0;
                    foreach (DataGridViewRow dvRow in dgvProduto.Rows)
                        if ((long)dvRow.Cells[1].Value == produtoBusca.Produto.Id)
                        {
                            index = dvRow.Index;
                            break;
                        }

                    dgvProduto.CurrentCell = dgvProduto[1, index];

                }

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        protected override void btnExcluirProduto_Click(object sender, EventArgs e)
        {
            try
            {

                if (dgvProduto.CurrentRow == null)
                    throw new Exception("Selecione um produto.");

                if (!Convert.ToBoolean(dgvProduto.CurrentRow.Cells[7].Value))
                    throw new Exception("Você não poderá excluir dados do contrato pai. Somente incluir novos produtos.");

                /* excluindo o produto do contrato do cliente */

                FinanceiroFacade financeiroFacade = FinanceiroFacade.getInstance();

                financeiroFacade.deleteContratoProduto(new ContratoProduto((long)dgvProduto.CurrentRow.Cells[0].Value, new Produto((long)dgvProduto.CurrentRow.Cells[1].Value, dgvProduto.CurrentRow.Cells[2].Value.ToString(), dgvProduto.CurrentRow.Cells[8].Value.ToString(), Convert.ToDecimal(dgvProduto.CurrentRow.Cells[3].Value), dgvProduto.CurrentRow.Cells[9].Value.ToString(), Convert.ToDecimal(dgvProduto.CurrentRow.Cells[4].Value), (bool)dgvProduto.CurrentRow.Cells[11].Value, string.Empty), proposta, Convert.ToDecimal(dgvProduto.CurrentRow.Cells[3].Value), PermissionamentoFacade.usuarioAutenticado, Convert.ToDecimal(dgvProduto.CurrentRow.Cells[4].Value), Convert.ToDateTime(dgvProduto.CurrentRow.Cells[5].Value), "I", true));

                MontaDataGridProduto();

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        protected override void dgvProduto_CellEndEdit(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                if (String.IsNullOrEmpty(dgvProduto.CurrentCell.EditedFormattedValue.ToString()))
                    dgvProduto.CurrentCell.Value = dgvProduto.CurrentCell.Tag.ToString();

                dgvProduto.CurrentCell.Value = ValidaCampoHelper.FormataValorMonetario(dgvProduto.CurrentCell.Value.ToString());

                /* realizando o update do contratoProduto */

                FinanceiroFacade financeiroFacade = FinanceiroFacade.getInstance();

                financeiroFacade.updateContratoProduto(new ContratoProduto((long)dgvProduto.CurrentRow.Cells[0].Value, new Produto((long)dgvProduto.CurrentRow.Cells[1].Value, dgvProduto.CurrentRow.Cells[2].Value.ToString(), dgvProduto.CurrentRow.Cells[8].Value.ToString(), Convert.ToDecimal(dgvProduto.CurrentRow.Cells[3].Value), dgvProduto.CurrentRow.Cells[9].Value.ToString(), Convert.ToDecimal(dgvProduto.CurrentRow.Cells[4].Value), (bool)dgvProduto.CurrentRow.Cells[11].Value, string.Empty), proposta, Convert.ToDecimal(dgvProduto.CurrentRow.Cells[3].EditedFormattedValue), PermissionamentoFacade.usuarioAutenticado, Convert.ToDecimal(dgvProduto.CurrentRow.Cells[4].EditedFormattedValue), Convert.ToDateTime(dgvProduto.CurrentRow.Cells[5].Value), "A", true));

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        protected override void btnGravar_Click(object sender, EventArgs e)
        {
            try
            {
                if (validaDados())
                {
                    FinanceiroFacade financeiroFacade = FinanceiroFacade.getInstance();

                   proposta = financeiroFacade.updateContrato(new Contrato(proposta.Id, proposta.CodigoContrato, proposta.Cliente, proposta.UsuarioCriador, proposta.DataElaboracao, dataVencimento.Checked ? Convert.ToDateTime(dataVencimento.Text) : (DateTime?)null, proposta.DataInicio, proposta.Situacao, textComentario.Text, proposta.UsuarioFinalizou, proposta.UsuarioCancelamento, proposta.MotivoCancelamento, proposta.Prestador, proposta.ContratoAutomatico, proposta.DataCancelamento, proposta.ContratoPai, false, false, false, null, false, null, null, null , proposta.ContratoRaiz, proposta.ClienteProposta, proposta.Proposta, proposta.PropostaFormaPagamento, proposta.UsuarioEncerrou, proposta.DataEncerramento, proposta.MotivoEncerramento, PermissionamentoFacade.usuarioAutenticado.Empresa));

                    MessageBox.Show("Proposta alterada com sucesso.", "Confirmação", MessageBoxButtons.OK, MessageBoxIcon.Information);

                    this.Close();
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }
    }
}
