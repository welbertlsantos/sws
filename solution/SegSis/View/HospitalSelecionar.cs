﻿using SWS.Entidade;
using SWS.Facade;
using SWS.View.Resources;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace SWS.View
{
    public partial class frmHospitalSelecionar : frmTemplateConsulta
    {
        private Hospital hospital;

        public Hospital Hospital
        {
            get { return hospital; }
            set { hospital = value; }
        }
        
        public frmHospitalSelecionar()
        {
            InitializeComponent();
            ValidaPermissao();
            ActiveControl = textHospital;

        }

        private void btnConfirmar_Click(object sender, EventArgs e)
        {
            try
            {
                if (dgvHospital.CurrentRow == null)
                    throw new Exception("Selecione uma linha.");

                EstudoFacade pcmsoFacade = EstudoFacade.getInstance();

                hospital = pcmsoFacade.findHospitalById((long)dgvHospital.CurrentRow.Cells[0].Value);
                this.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }


        }

        private void btnNovo_Click(object sender, EventArgs e)
        {
            try
            {
                frmHospitalIncluir incluirHospital = new frmHospitalIncluir();
                incluirHospital.ShowDialog();

                if (incluirHospital.Hospital != null)
                {
                    Hospital = incluirHospital.Hospital;
                    this.Close();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }

        private void btnFechar_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void ValidaPermissao()
        {
            PermissionamentoFacade permissionamentoFacade = PermissionamentoFacade.getInstance();
            btnNovo.Enabled = permissionamentoFacade.hasPermission("HOSPITAL", "INCLUIR");
        }

        private void MontaDataGrid()
        {
            try
            {
                dgvHospital.Columns.Clear();

                this.Cursor = Cursors.WaitCursor;

                EstudoFacade pcmsoFacade = EstudoFacade.getInstance();

                DataSet ds = pcmsoFacade.findHospitalByFilter(new Hospital(null, textHospital.Text, string.Empty, string.Empty, string.Empty, string.Empty, string.Empty, string.Empty, string.Empty, string.Empty, string.Empty, ApplicationConstants.ATIVO, null));

                dgvHospital.DataSource = ds.Tables[0].DefaultView;

                dgvHospital.Columns[0].HeaderText = "id";
                dgvHospital.Columns[0].Visible = false;

                dgvHospital.Columns[1].HeaderText = "Nome";

                dgvHospital.Columns[2].HeaderText = "Endereço";
                dgvHospital.Columns[2].Visible = false;

                dgvHospital.Columns[3].HeaderText = "Número";
                dgvHospital.Columns[3].Visible = false;

                dgvHospital.Columns[4].HeaderText = "Complemento";
                dgvHospital.Columns[4].Visible = false;

                dgvHospital.Columns[5].HeaderText = "Bairro";
                dgvHospital.Columns[5].Visible = false;

                dgvHospital.Columns[6].HeaderText = "Cidade";
                dgvHospital.Columns[6].Visible = false;

                dgvHospital.Columns[7].HeaderText = "CEP";
                dgvHospital.Columns[7].Visible = false;

                dgvHospital.Columns[8].HeaderText = "UF";
                dgvHospital.Columns[8].Visible = false;

                dgvHospital.Columns[9].HeaderText = "Telefone";
                dgvHospital.Columns[9].Visible = false;

                dgvHospital.Columns[10].HeaderText = "Telefone";
                dgvHospital.Columns[10].Visible = false;

                dgvHospital.Columns[11].HeaderText = "Observação";
                dgvHospital.Columns[11].Visible = false;

                dgvHospital.Columns[12].HeaderText = "Situação";
                dgvHospital.Columns[12].Visible = false;


            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }

        }

        private void btnBusca_Click(object sender, EventArgs e)
        {
            try
            {
                MontaDataGrid();
                textHospital.Focus();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }

        private void dgvHospital_CellMouseDoubleClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            try
            {
                btnConfirmar.PerformClick();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }
    }
}
