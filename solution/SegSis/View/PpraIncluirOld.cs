﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using SWS.Facade;
using SWS.Entidade;
using SWS.Excecao;
using SWS.View.ViewHelper;
using System.Text.RegularExpressions;
using SWS.View.Resources;
using SWS.ViewHelper;


namespace SWS.View
{
    public partial class frm_ppraIncluirOld : BaseForm
    {
        protected Estudo ppra;

        public Estudo Ppra
        {
            get { return ppra; }
            set { ppra = value; }
        }

        // relacionado ao cronograma do estudo
        protected Cronograma cronograma;

        protected HashSet<CronogramaAtividade> listCronogramaAtividade = new HashSet<CronogramaAtividade>();

        // relacionado aos ghes do estudo
        protected Ghe ghe;

        protected GheFonte gheFonte;

        // relacinando agente ao ghe fonte
        protected GheFonteAgente gheFonteAgente;

        // relacionado ao epi do ghe_fonte_agente
        protected GheFonteAgenteEpi gheFonteAgenteEpi;

        // relacionado ao GheSetor do estudo
        protected GheSetor gheSetor;

        // relacionado ao GheSetorFuncao do estudo
        protected GheSetorClienteFuncao gheSetorClienteFuncao;

        protected Cliente contratante;

        protected VendedorCliente vendedorCliente;

        protected Usuario tecno;

        public static String Msg01 = " Confirmação ";
        public static String Msg02 = " PPRA ";
        public static String Msg03 = " gravado com sucesso ";
        public static String Msg04 = " PPRA alterado com sucesso";
        public static String Msg05 = " Cronograma do PPRA incluído com sucesso";
        public static String Msg06 = " Não é possível incluir cronograma sem descrição";
        public static String Msg07 = " Selecione uma linha!";
        public static String Msg08 = " Atenção";
        public static String Msg09 = " Cronograma do PPRA alterado com sucesso";
        public static String Msg10 = " Deseja excluir esta atividade do cronograma? ";
        public static String Msg11 = " Atividade excluída do cronograma com sucesso ";
        public static String Msg12 = " GHE incluído com sucesso";
        public static String Msg13 = " Não é possível criar um PPRA novo sem um cliente ";

        public static String Msg14 = " Ghe Alterado com sucesso";
        public static String Msg15 = " Deseja realmente excluir o GHE? ";
        public static String Msg16 = " Essa ação excluirá todos os relacionamentos.";
        public static String Msg17 = " GHE excluído com sucesso ";
        public static String Msg18 = " Deseja realmente excluir a Fonte?";
        public static String Msg19 = " Essa acao excluirá a(s) fonte(s) do GHE ";
        public static String Msg20 = " Fonte excluída com sucesso ";
        public static String Msg21 = " Selecione um GHE";
        public static String Msg22 = " Selecione uma fonte";
        public static String Msg23 = " Deseja excluir o agente?";
        public static String Msg24 = " Agente(s) excluídos com sucesso";

        public static String Msg25 = " Deseja realmente excluir o Epi?";
        public static String Msg26 = " Epi Excluído com sucesso!";

        public static String Msg27 = " Deseja realmente excluir o setor? ";
        public static String Msg28 = " Essa acao excluirá o(s) setor(es) do GHE";
        public static String Msg29 = " Setor(es) excluído(s) com sucesso";

        public static String Msg30 = " Deseja realmente excluir a função do cliente? ";
        public static String Msg31 = " Essa ação desativará a função ";
        public static String Msg32 = " Função(ões) excluído(s) com sucesso";
        public static String Msg33 = " Selecione um setor";
        public static String Msg34 = " Deseja realmente excluir a função do Setor? ";
        public static String Msg35 = " Função do Setor excluída com sucesso";

        public static String msg36 = "Deseja excluir esse conteúdo?";
        public static String msg37 = "Conteúdo excluido com sucesso";
        public static String msg39 = "O contratante não pode ser o mesmo cliente do estudo.";

        public static String msg40 = "Clique no botão para selecionar a atividade da função.";

        public static String msg41 = "Precisa gravar primeiro o estudo.";
        public static String msg42 = "Número maior do que o permitido para o campo. ";

        public static String msg43 = "Caso a função participe de alguma atividade em espaço confinado, marque essa caixa ";
        public static String msg44 = "Caso a função participe de alguma atividade em altura, marque essa caixa ";

        public frm_ppraIncluirOld()
        {
            InitializeComponent();
        }

        protected void btn_fechar_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        protected void btn_cliente_Click(object sender, EventArgs e)
        {
            /* revisao 9.2 */
            frmClienteSelecionar selecionarCliente = new frmClienteSelecionar(null, null, false, null, null, null, null, false, true);
            selecionarCliente.ShowDialog();

            if (selecionarCliente.Cliente != null)
            {
                vendedorCliente = selecionarCliente.Cliente.VendedorCliente.Find(x => string.Equals(x.getSituacao(), ApplicationConstants.ATIVO));

                text_cliente.Text = vendedorCliente.getCliente().RazaoSocial;

                grb_grupo1.Enabled = true;
                btn_contratante.Enabled = true;
                btn_cliente.Enabled = true;

                /* montando grid e cnae */
                montaGridCnaeCliente();
            }

        }

        protected void btn_tecnico_Click(object sender, EventArgs e)
        {
            frm_ppraUsuarioTecnicoIncluir formUsuarioTecnicoIncluirPpra = new frm_ppraUsuarioTecnicoIncluir();
            formUsuarioTecnicoIncluirPpra.ShowDialog();

            if (formUsuarioTecnicoIncluirPpra.getUsuario() != null)
            {
                tecno = formUsuarioTecnicoIncluirPpra.getUsuario();
                text_tecnico.Text = formUsuarioTecnicoIncluirPpra.getUsuario().Nome.ToUpper();
            }

        }

        protected void btn_contratante_Click(object sender, EventArgs e)
        {
            try
            {
                //zerando informações do contratante
                text_numContrato.Text = String.Empty;
                text_inicioContr.Text = String.Empty;
                text_fimContr.Text = String.Empty;

                frmClienteSelecionar formCliente = new frmClienteSelecionar(null, null, null, null, null, null, null, false, true);
                formCliente.ShowDialog();

                if (formCliente.Cliente != null)
                {
                    if (formCliente.Cliente.Id == vendedorCliente.getCliente().Id)
                        throw new Exception("O contratante não pode ser o mesmo cliente do estudo.");

                    contratante = formCliente.Cliente;
                    text_clienteContratante.Text = contratante.RazaoSocial;
                    text_clienteContratante.Enabled = true;
                    text_inicioContr.Enabled = true;
                    text_fimContr.Enabled = true;
                    text_numContrato.Enabled = true;

                    /* montando grid de cnae */
                    montaGridCnaeContratante();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        public void frm_ppraIncluir_Load(object sender, EventArgs e)
        {
            ComboHelper.unidadeFederativa(cb_uf);
            ComboHelper.grauRisco(cb_grauRisco);
            dt_criacao.Checked = false;
            dt_vencimento.Checked = false;
            enableCommand(false);
        }

        protected virtual void btn_salvar_Click(object sender, EventArgs e)
        {
            try
            {
                this.Cursor = Cursors.WaitCursor;

                if (ppra == null)
                {
                    if (vendedorCliente == null)
                        throw new PpraException(PpraException.msg);

                    PpraFacade ppraFacade = PpraFacade.getInstance();
                    RelatorioFacade relatorioFacade = RelatorioFacade.getInstance();

                    /* chamada do metodo para incluir o estudo */
                    ppra = ppraFacade.incluirPpra(new Estudo(null, relatorioFacade.findUltimaVersaoByTipo(ApplicationConstants.RELATORIO_PPRA), string.Empty, new Cronograma(null, "CRONOGRAMA DE ATIVIDADES"), vendedorCliente, null, tecno, contratante, Convert.ToDateTime(dt_criacao.Text), Convert.ToDateTime(dt_vencimento.Text), null, ApplicationConstants.CONSTRUCAO, false, text_fimContr.Text, text_obra.Text, text_localObra.Text, text_endereco.Text, text_numero.Text, text_complemento.Text, text_bairro.Text, text_cidade.Text, ((SelectItem)cb_uf.SelectedItem).Valor, text_numContrato.Text, ((SelectItem)cb_grauRisco.SelectedItem).Valor, vendedorCliente.getCliente().RazaoSocial, vendedorCliente.getCliente().Cnpj, vendedorCliente.getCliente().Inscricao, vendedorCliente.getCliente().EstFem, vendedorCliente.getCliente().EstMasc, vendedorCliente.getCliente().Jornada, vendedorCliente.getCliente().Endereco, vendedorCliente.getCliente().Numero, vendedorCliente.getCliente().Complemento, vendedorCliente.getCliente().Bairro, vendedorCliente.getCliente().CidadeIbge.Nome, vendedorCliente.getCliente().Email, vendedorCliente.getCliente().Cep, vendedorCliente.getCliente().Uf, vendedorCliente.getCliente().Telefone1, vendedorCliente.getCliente().Telefone2, contratante != null ? contratante.RazaoSocial : string.Empty, contratante != null ? contratante.Cnpj : string.Empty, contratante != null ? contratante.Endereco : string.Empty, contratante != null ? contratante.Numero : string.Empty, contratante != null ? contratante.Complemento : string.Empty, contratante != null ? contratante.Bairro : string.Empty, contratante != null ? contratante.CidadeIbge.Nome : string.Empty, contratante != null ? contratante.Cep : string.Empty, contratante != null ? contratante.Uf : string.Empty, contratante != null ? contratante.Inscricao : string.Empty, text_inicioContr.Text, text_cep.Text, vendedorCliente.getCliente().Fantasia, text_comentario.Text, false, null));

                    MessageBox.Show("PPRA" + ppra.CodigoEstudo + " gravado com sucesso", "Confirmação");
                    text_codPpra.Text = Convert.ToString(ppra.CodigoEstudo);

                    // desabilitando botão impedindo a gravação
                    enableCommand(true);

                    // Mundando nome do botão novo
                    btn_salvar.Text = "&Gravar";

                }
                else
                {
                    PpraFacade ppraFacade = PpraFacade.getInstance();

                    //setando valores para o estudo.

                    // parametros do contratante.
                    ppra.DataInicioContrato = text_inicioContr.Text;
                    ppra.DataFimContrato = text_fimContr.Text;
                    ppra.NumContrato = text_numContrato.Text;

                    // parametros da criacao
                    ppra.DataCriacao = Convert.ToDateTime(dt_criacao.Text);
                    ppra.DataVencimento = Convert.ToDateTime(dt_vencimento.Text);

                    // parametros do risco
                    ppra.GrauRisco = ((SelectItem)cb_grauRisco.SelectedItem).Valor;

                    // parametros do endereco
                    ppra.Endereco = text_endereco.Text;
                    ppra.Numero = text_numero.Text;
                    ppra.Complemento = text_complemento.Text;
                    ppra.Bairro = text_bairro.Text;
                    ppra.Cidade = text_cidade.Text;
                    ppra.Uf = Convert.ToString(cb_uf.SelectedValue);
                    ppra.Obra = text_obra.Text;
                    ppra.LocalObra = text_localObra.Text;
                    ppra.Situacao = ppra.Situacao;

                    ppra.Cep = text_cep.Text;
                    ppra.Comentario = text_comentario.Text;

                    // chamada no metodo para alterar o estudo em tempo e execucao da inclusão do estudo.
                    ppra = ppraFacade.alteraPpra(ppra);

                    MessageBox.Show("PPRA alterado com sucesso", "Confirmação", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }

        protected void enableCommand(Boolean abilitaComando)
        {
            // cronograma
            grb_atividade.Enabled = abilitaComando;
            btn_incluirAtividade.Enabled = abilitaComando;
            btn_excluirAtividade.Enabled = abilitaComando;
            btn_alterarAtividade.Enabled = abilitaComando;

            // GHE
            grb_ghePPRA.Enabled = abilitaComando;
            btn_incluirGHE.Enabled = abilitaComando;
            btn_alterarGhe.Enabled = abilitaComando;
            btn_excluirGHE.Enabled = abilitaComando;

            //fontes
            grb_gheFonte.Enabled = abilitaComando;
            grb_fonte.Enabled = abilitaComando;
            btn_incluirFonte.Enabled = abilitaComando;
            btn_excluirFonte.Enabled = abilitaComando;

            //agentes
            grb_gheAgente.Enabled = abilitaComando;
            grb_fonteAgente.Enabled = abilitaComando;
            grb_agente.Enabled = abilitaComando;
            btn_incluirAgente.Enabled = abilitaComando;
            btn_excluirAgente.Enabled = abilitaComando;

            //EPI
            grb_gheEPI.Enabled = abilitaComando;
            grb_fonteEpi.Enabled = abilitaComando;
            grb_agenteEpi.Enabled = abilitaComando;
            grb_epi.Enabled = abilitaComando;
            btn_incluirEpi.Enabled = abilitaComando;
            btn_excluirEPI.Enabled = abilitaComando;

            //Setores
            grb_gheSetor.Enabled = abilitaComando;
            grb_setor.Enabled = abilitaComando;
            btn_incluirSetor.Enabled = abilitaComando;
            btn_excluirSetor.Enabled = abilitaComando;

            //funcaoCliente
            grb_funcao.Enabled = abilitaComando;
            btn_incluirFuncaoClientePPRA.Enabled = abilitaComando;
            btn_excluirFuncaoClientePPRA.Enabled = abilitaComando;

            //funcao
            grb_gheFuncao.Enabled = abilitaComando;
            grb_setorFuncao.Enabled = abilitaComando;
            grb_funcaoCliente.Enabled = abilitaComando;
            btn_incluirFuncaoCliente.Enabled = abilitaComando;
            btn_excluirFuncaoCliente.Enabled = abilitaComando;

            //Anexo
            grb_anexo.Enabled = abilitaComando;
            btn_incluirAnexo.Enabled = abilitaComando;
            btn_excluirAnexo.Enabled = abilitaComando;
            text_conteudo.Enabled = abilitaComando;

            /* Comentários */
            grb_comentario.Enabled = abilitaComando;
            btn_gravarComentario.Enabled = abilitaComando;

            /*previsão */
            dgPrevisao.Enabled = abilitaComando;
            btnIncluirPrevisao.Enabled = abilitaComando;
            btnExcluirPrevisao.Enabled = abilitaComando;

            /*cnae */
            this.dgvCnaeCliente.Enabled = abilitaComando;
            this.dgvCnaeContratante.Enabled = abilitaComando;

        }

        protected void btn_limpar_Click(object sender, EventArgs e)
        {
            text_endereco.Text = String.Empty;
            text_numero.Text = String.Empty;
            text_complemento.Text = String.Empty;
            text_bairro.Text = String.Empty;
            text_cidade.Text = String.Empty;
            ComboHelper.unidadeFederativa(cb_uf);
            text_obra.Text = String.Empty;
            text_localObra.Text = String.Empty;
            tecno = null;
            text_tecnico.Text = String.Empty;
            contratante = null;
            text_clienteContratante.Text = String.Empty;
            text_numContrato.Text = String.Empty;

        }

        protected void btn_incluirAtividade_Click(object sender, EventArgs e)
        {
            try
            {
                frmCronogramaAtividadeIncluir atividadeCronograma = new frmCronogramaAtividadeIncluir(ppra.Cronograma);
                atividadeCronograma.ShowDialog();

                if (atividadeCronograma.CronogramaAtividade != null)
                {
                    PcmsoFacade pcmsoFacade = PcmsoFacade.getInstance();
                    listCronogramaAtividade.Add(pcmsoFacade.incluirCronogramaAtividade(atividadeCronograma.CronogramaAtividade));
                }

                MontarGrid();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }

        public void MontarGrid()
        {
            grd_atividadeCronograma.Columns.Clear();

            grd_atividadeCronograma.ColumnCount = 7;

            grd_atividadeCronograma.Columns[0].Name = "IdAtividade";
            grd_atividadeCronograma.Columns[0].Visible = false;

            grd_atividadeCronograma.Columns[1].Name = "IdCronograma";
            grd_atividadeCronograma.Columns[1].Visible = false;

            grd_atividadeCronograma.Columns[2].Name = "Atividade";
            grd_atividadeCronograma.Columns[3].Name = "Mês Realizado";
            grd_atividadeCronograma.Columns[4].Name = "Ano Realizado";
            grd_atividadeCronograma.Columns[5].Name = "Público Alvo";
            grd_atividadeCronograma.Columns[6].Name = "Evidência / Registro";

            foreach (CronogramaAtividade cronogramaAtividade in listCronogramaAtividade)
            {
                grd_atividadeCronograma.Rows.Add(cronogramaAtividade.Atividade.Id, cronogramaAtividade.Cronograma.Id, cronogramaAtividade.Atividade.Descricao, cronogramaAtividade.MesRealizado, cronogramaAtividade.AnoRealizado, cronogramaAtividade.PublicoAlvo, cronogramaAtividade.Evidencia);
            }
        }

        protected void btn_alterarAtividade_Click(object sender, EventArgs e)
        {
            try
            {
                if (grd_atividadeCronograma.CurrentRow == null)
                    throw new Exception("Selecione uma atividade.");

                frmCronogramaAtividadeAlterar alterarAtividadeCronograma = new frmCronogramaAtividadeAlterar(listCronogramaAtividade.First(x => x.Atividade.Id == (Int64)grd_atividadeCronograma.CurrentRow.Cells[0].Value));
                alterarAtividadeCronograma.ShowDialog();
                MontarGrid();

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        protected void btn_excluirAtividade_Click(object sender, EventArgs e)
        {
            try
            {
                if (grd_atividadeCronograma.CurrentRow == null)
                    throw new Exception("Selecione uma atividade.");


                if (MessageBox.Show("Deseja excluir a atividade selecionada?", "Confirmação", MessageBoxButtons.YesNo) == DialogResult.Yes)
                {
                    PcmsoFacade pcmsoFacade = PcmsoFacade.getInstance();

                    CronogramaAtividade cronogramaAtividadeExcluir = listCronogramaAtividade.First(x => x.Atividade.Id == (Int64)grd_atividadeCronograma.CurrentRow.Cells[0].Value);

                    pcmsoFacade.excluiCronogramaAtividade(cronogramaAtividadeExcluir);
                    listCronogramaAtividade.Remove(cronogramaAtividadeExcluir);

                    MessageBox.Show("Atividade excluída com sucesso.", "Confirmação", MessageBoxButtons.OK, MessageBoxIcon.Information);

                    MontarGrid();
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        protected void btn_incluirGHE_Click(object sender, EventArgs e)
        {
            frm_GheIncluir formGheIncluir = new frm_GheIncluir(ppra);
            formGheIncluir.ShowDialog();

            if (formGheIncluir.Ghe != null)
            {
                PpraFacade ppraFacade = PpraFacade.getInstance();

                ghe = formGheIncluir.Ghe;
                montaGridGhe();

                iniciaComboGhe(cb_ghe);
                iniciaComboGhe(cb_gheSetor);
                iniciaComboGhe(cb_gheFuncao);
                iniciaComboGhe(cb_gheAgente);
                iniciaComboGhe(cb_gheEPI);

            }
        }

        public void montaGridGhe()
        {
            try
            {
                grd_ghe.Columns.Clear();

                PpraFacade ppraFacade = PpraFacade.getInstance();

                DataSet ds = ppraFacade.findGhePpra(ppra);

                grd_ghe.DataSource = ds.Tables["Ghes"].DefaultView;
                grd_ghe.AutoResizeColumn(2);

                grd_ghe.Columns[0].HeaderText = "IdGhe";
                grd_ghe.Columns[0].Visible = false;

                grd_ghe.Columns[1].HeaderText = "IdPpra";
                grd_ghe.Columns[1].Visible = false;

                grd_ghe.Columns[2].HeaderText = "Descrição";

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        protected void btn_alterarGhe_Click(object sender, EventArgs e)
        {
            try
            {
                if (grd_ghe.CurrentRow == null)
                    throw new Exception("Selecione um GHE para alterar.");

                PpraFacade ppraFacade = PpraFacade.getInstance();

                frm_GheAlterar formGheAlterar = new frm_GheAlterar(ppraFacade.findGheById((Int64)grd_ghe.CurrentRow.Cells[0].Value));
                formGheAlterar.ShowDialog();

                ghe = formGheAlterar.Ghe;
                montaGridGhe();

                iniciaComboGhe(cb_ghe);
                iniciaComboGhe(cb_gheSetor);
                iniciaComboGhe(cb_gheFuncao);

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }

        protected void btn_excluirGHE_Click(object sender, EventArgs e)
        {
            try
            {
                if (grd_ghe.CurrentRow == null)
                    throw new Exception("Selecione o ghe para exclusão.");


                if (MessageBox.Show("Deseja excluir o ghe Selecionado? Essa ação excluirá todos os relacionamentos com esse GHE", "Atenção", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                {
                    PpraFacade ppraFacade = PpraFacade.getInstance();

                    ppraFacade.deleteGheById((Int64)grd_ghe.CurrentRow.Cells[0].Value, ppra);

                    montaGridGhe();

                    iniciaComboGhe(cb_ghe);
                    iniciaComboGhe(cb_gheSetor);
                    iniciaComboGhe(cb_gheFuncao);
                    iniciaComboGhe(cb_gheAgente);
                    iniciaComboGhe(cb_gheEPI);
                    grd_fonte.Columns.Clear();
                    grd_setor.Columns.Clear();

                    MessageBox.Show("Ghe excluído com sucesso.", "Confirmação", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }

        public void iniciaComboGhe(ComboBox combo)
        {
            PpraFacade ppraFacade = PpraFacade.getInstance();

            combo.Items.Clear();

            foreach (DataRow rows in ppraFacade.findGhePpra(ppra).Tables[0].Rows)
                combo.Items.Add(new SelectItem(rows["id_ghe"].ToString(), rows["descricao"].ToString()));

            combo.ValueMember = "nome";
        }

        protected void btn_incluirFonte_Click(object sender, EventArgs e)
        {
            try
            {
                if (cb_ghe.SelectedItem == null)
                    throw new Exception("Selecione um GHE.");

                frm_ppraGheFonteIncluir formPpraGheFonteIncluir = new frm_ppraGheFonteIncluir(ghe);
                formPpraGheFonteIncluir.ShowDialog();

                montaGridFonte();

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        protected void cb_ghe_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                PpraFacade ppraFacade = PpraFacade.getInstance();
                ghe = ppraFacade.findGheById(Convert.ToInt64(((SelectItem)cb_ghe.SelectedItem).Valor));
                montaGridFonte();
            }

            catch (Exception ex)
            {
                MessageBox.Show(ex.Message,  "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        public void montaGridFonte()
        {
            try
            {
                grd_fonte.Columns.Clear();

                PpraFacade ppraFacade = PpraFacade.getInstance();

                DataSet ds = ppraFacade.findAllFonteByGheDataSet(ghe);

                grd_fonte.DataSource = ds.Tables[0].DefaultView;

                grd_fonte.Columns[0].HeaderText = "id_ghe_fonte";
                grd_fonte.Columns[0].Visible = false;

                grd_fonte.Columns[1].HeaderText = "id_fonte";
                grd_fonte.Columns[1].Visible = false;

                grd_fonte.Columns[2].HeaderText = "Fonte";
                grd_fonte.Columns[2].DefaultCellStyle.ForeColor = Color.Red;

                grd_fonte.Columns[3].HeaderText = "Id_ghe";
                grd_fonte.Columns[3].Visible = false;

                grd_fonte.Columns[4].HeaderText = "Ghe";
                grd_fonte.Columns[4].Visible = false;

                grd_fonte.Columns[5].HeaderText = "Principal?";

            }
            catch (Exception Ex)
            {
                MessageBox.Show(Ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        protected void btn_excluirFonte_Click(object sender, EventArgs e)
        {
            try
            {
                PpraFacade ppraFacade = PpraFacade.getInstance();

                if (grd_fonte.CurrentRow == null)
                    throw new Exception("Selecione uma fonte para exclusão.");

                if (MessageBox.Show("Deseja realmente excluir a Fonte selecionada? Essa acao excluirá a(s) fonte(s) do GHE", "Confirmação", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                {
                    foreach (DataGridViewRow dvRow in grd_fonte.SelectedRows)
                        ppraFacade.deleteGheFonteById(new GheFonte((Int64)dvRow.Cells[0].Value, null, null));

                    MessageBox.Show("Fonte(s) excluídas com sucesso.", "Confirmação", MessageBoxButtons.OK, MessageBoxIcon.Information);

                    montaGridFonte();
                }

            }
            catch (Exception Ex)
            {
                MessageBox.Show(Ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
        }

        protected void tb_fonteGHE_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (tb_fonteGHE.SelectedIndex == 1)
            {
                iniciaComboGhe(cb_gheAgente);
                cb_fonteAgente.Items.Clear();
                grd_agente.Columns.Clear();

            }

            if (tb_fonteGHE.SelectedIndex == 2)
            {
                iniciaComboGhe(cb_gheEPI);
                cb_fonteEpi.Items.Clear();
                cb_agenteEpi.Items.Clear();
                grd_epi.Columns.Clear();
            }


        }

        protected void iniciaComboGheFonte(ComboBox combo, Boolean selecao)
        {
            PpraFacade ppraFacade = PpraFacade.getInstance();

            SelectItem item = null;
            String valor;
            String descricao;

            combo.Items.Clear();

            if (selecao == true)
            {
                foreach (DataRow rows in ppraFacade.findAllFonteByGheInEpi(ghe).Tables[0].Rows)
                {
                    descricao = Convert.ToString(rows[Convert.ToString("descricao")]);
                    valor = Convert.ToString(rows[Convert.ToString("id_fonte")]);

                    item = new SelectItem(valor, descricao);

                    combo.Items.Add(item);
                }
            }
            else
            {
                foreach (DataRow rows in ppraFacade.findAllFonteByGheDataSet(ghe).Tables[0].Rows)
                {

                    String principal = (Boolean)rows["Principal"] ? " - (PRINCIPAL)" : String.Empty;

                    descricao = Convert.ToString(rows[Convert.ToString("descricao")])
                        + principal;

                    valor = Convert.ToString(rows[Convert.ToString("id_fonte")]);
                    item = new SelectItem(valor, descricao);

                    combo.Items.Add(item);
                }
            }

            combo.ValueMember = "nome";
        }

        protected void btn_incluirAgente_Click(object sender, EventArgs e)
        {
            try
            {
                if (((SelectItem)cb_fonteAgente.SelectedItem) == null)
                    throw new Exception("Selecione uma fonte.");

                frm_ppraAgenteIncluir formPpraAgenteIncluir = new frm_ppraAgenteIncluir(gheFonte);
                formPpraAgenteIncluir.ShowDialog();

                montaGridAgente();


            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        protected void cb_gheAgente_SelectedIndexChanged_1(object sender, EventArgs e)
        {
            try
            {
                PpraFacade ppraFacade = PpraFacade.getInstance();
                ghe = ppraFacade.findGheById(Convert.ToInt64(((SelectItem)cb_gheAgente.SelectedItem).Valor));
                
                grd_agente.Columns.Clear();
                
                iniciaComboGheFonte(cb_fonteAgente, false);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        protected void cb_fonteAgente_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                if (((SelectItem)cb_gheAgente.SelectedItem) == null)
                    throw new Exception("Selecione um ghe primeiro.");
                
                PpraFacade ppraFacade = PpraFacade.getInstance();
                
                gheFonte = ppraFacade.findGheFonteByGheFonte(new GheFonte(null, ppraFacade.findFonteById(Convert.ToInt64(((SelectItem)cb_fonteAgente.SelectedItem).Valor)), ghe));
                
                montaGridAgente();


            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        public void montaGridAgente()
        {
            try
            {
                PpraFacade ppraFacade = PpraFacade.getInstance();
                this.Cursor = Cursors.WaitCursor;

                grd_agente.Columns.Clear();

                DataSet ds = ppraFacade.findAllAgenteByGheFonte(gheFonte);

                grd_agente.DataSource = ds.Tables[0].DefaultView;

                grd_agente.Columns[0].HeaderText = "Id_ghe_fonte_agente";
                grd_agente.Columns[0].Visible = false;

                grd_agente.Columns[1].HeaderText = "Id_agente";
                grd_agente.Columns[1].Visible = false;

                grd_agente.Columns[2].HeaderText = "Agente";
                grd_agente.Columns[2].DefaultCellStyle.ForeColor = Color.Red;

                grd_agente.Columns[3].HeaderText = "Id GheFonte";
                grd_agente.Columns[3].Visible = false;

                grd_agente.Columns[4].HeaderText = "Risco";
                grd_agente.Columns[4].DefaultCellStyle.ForeColor = Color.Blue;

            }
            catch (Exception Ex)
            {
                MessageBox.Show(Ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }

        protected void btn_excluirAgente_Click(object sender, EventArgs e)
        {
            try
            {
                PpraFacade ppraFacade = PpraFacade.getInstance();

                if (grd_agente.CurrentRow != null)
                {

                    if (MessageBox.Show(Msg23 + Msg16, Msg01, MessageBoxButtons.YesNo) == DialogResult.Yes)
                    {
                        foreach (DataGridViewRow dvRow in grd_agente.SelectedRows)
                        {
                            ppraFacade.deleteGheFonteAgenteById((Int64)dvRow.Cells[0].Value);
                        }

                        MessageBox.Show(Msg24, Msg01);
                        montaGridAgente();
                    }
                }
                else
                {
                    MessageBox.Show(Msg07, Msg08);
                }
            }
            catch (Exception Ex)
            {
                MessageBox.Show(Ex.Message);
            }
        }

        protected void cb_gheEPI_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                SelectItem itemSelecionado = (SelectItem)cb_gheEPI.SelectedItem;
                grd_epi.Columns.Clear();

                // criando o objeto GHE
                ghe.Id = Convert.ToInt64(itemSelecionado.Valor);
                ghe.Descricao = Convert.ToString(itemSelecionado.Nome);

                // Excluindo itens selecionados do combo epi
                cb_agenteEpi.Items.Clear();

                iniciaComboGheFonte(cb_fonteEpi, true);

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atençao", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
        }


        protected void cb_fonteEpi_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                PpraFacade ppraFacade = PpraFacade.getInstance();
                SelectItem gheSelecionado = (SelectItem)cb_gheEPI.SelectedItem;
                Ghe ghe = new Ghe(Convert.ToInt64(gheSelecionado.Valor), ppra, Convert.ToString(gheSelecionado.Nome), 0, string.Empty, string.Empty);
                Fonte fonte = new Fonte();

                
                grd_epi.Columns.Clear();
                // zerando combo agente selecionado
                cb_agenteEpi.Items.Clear();

                SelectItem fonteSelecioando = (SelectItem)cb_fonteEpi.SelectedItem;

                fonte.Id = Convert.ToInt64(fonteSelecioando.Valor);
                fonte.Descricao = Convert.ToString(fonteSelecioando.Nome);

                gheFonte.setFonte(fonte);
                gheFonte.setGhe(ghe);

                gheFonte = ppraFacade.findGheFonteByGheFonteInEpi(gheFonte);

                iniciaComboGheFonteAgente(cb_agenteEpi);

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atençao", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
        }

        protected void cb_agenteEpi_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                PpraFacade ppraFacade = PpraFacade.getInstance();
                SelectItem gheSelecionado = (SelectItem)cb_gheEPI.SelectedItem;
                Ghe ghe = new Ghe(Convert.ToInt64(gheSelecionado.Valor), ppra, Convert.ToString(gheSelecionado.Nome), 0 , string.Empty, string.Empty);
                Fonte fonte = new Fonte();
                Agente agente = new Agente();

                // preenchendo objeto GHE.
                
                // preenchendo objeto Fonte
                SelectItem fonteSelecionada = (SelectItem)cb_fonteEpi.SelectedItem;
                fonte.Id = Convert.ToInt64(fonteSelecionada.Valor);
                fonte.Descricao = Convert.ToString(fonteSelecionada.Nome);

                // preenchendo objeto Agente
                SelectItem agenteSelecionado = (SelectItem)cb_agenteEpi.SelectedItem;
                agente.Id = Convert.ToInt64(agenteSelecionado.Valor);
                agente.Descricao = Convert.ToString(agenteSelecionado.Nome);

                // objeto ghefonte da classe ppraIncluir.
                gheFonte.setFonte(fonte);
                gheFonte.setGhe(ghe);

                // objeto gheFonteAgente da classe ppraIncluir
                gheFonteAgente = new GheFonteAgente(null, null, null, null, gheFonte, agente, string.Empty, ApplicationConstants.ATIVO);

                /* buscando o gheFonteAgente dado um GheFonte e um Agente */

                gheFonteAgente = ppraFacade.findGheFonteAgenteByGheFonteAndAgente(gheFonte, agente);

                if (gheFonteAgente != null)
                    montaGridEpi();


            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atençao", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
        }

        protected void btn_incluirEpi_Click(object sender, EventArgs e)
        {
            try
            {

                /* verificando se o usuario selecionou o ghe */
                if (cb_gheEPI.SelectedIndex == -1)
                {
                    throw new GheException(GheException.msg);
                }

                /* verificando se o usuario selecionou a fonte */
                if (cb_fonteEpi.SelectedIndex == -1)
                {
                    throw new FonteException(FonteException.msg);
                }

                /* verificando se o usuario selecionou o agente */
                if (cb_agenteEpi.SelectedIndex == -1)
                {
                    throw new AgenteException(AgenteException.msg1);
                }

                frm_PpraEpiIncluir formPpraEpiIncluir = new frm_PpraEpiIncluir(gheFonteAgente);
                formPpraEpiIncluir.ShowDialog();

                montaGridEpi();


            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
        }

        protected void btn_excluirEPI_Click(object sender, EventArgs e)
        {
            try
            {
                PpraFacade ppraFacade = PpraFacade.getInstance();

                if (grd_epi.CurrentRow == null)
                    throw new Exception("Selecione uma linha.");

                if (MessageBox.Show(Msg25, Msg01, MessageBoxButtons.YesNo) == DialogResult.Yes)
                {
                    foreach (DataGridViewRow dvRow in grd_epi.SelectedRows)
                    {
                        ppraFacade.deleteGheFonteAgenteEpi((Int64)dvRow.Cells[0].Value);
                    }

                    montaGridEpi();

                    MessageBox.Show(Msg26, Msg01);

                    montaGridAgente();

                }

            }
            catch (Exception Ex)
            {
                MessageBox.Show(Ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }

        }

        protected void iniciaComboGheFonteAgente(ComboBox combo)
        {
            PpraFacade ppraFacade = PpraFacade.getInstance();

            SelectItem item = null;
            String valor;
            String descricao;

            combo.Items.Clear();

            foreach (DataRow rows in ppraFacade.findAllAgenteByGheFonte(gheFonte).Tables[0].Rows)
            {

                descricao = Convert.ToString(rows[Convert.ToString("agente")]) + " - " + Convert.ToString(rows["risco"]);
                valor = Convert.ToString(rows[Convert.ToString("id_agente")]);

                item = new SelectItem(valor, descricao);

                combo.Items.Add(item);
            }

            combo.ValueMember = "nome";
        }

        public void montaGridEpi()
        {
            try
            {
                PpraFacade ppraFacade = PpraFacade.getInstance();

                grd_epi.Columns.Clear();

                DataSet ds = ppraFacade.findAllGheFonteAgenteEpiByGHeFonteAgente(gheFonteAgente);

                grd_epi.DataSource = ds.Tables[0].DefaultView;

                grd_epi.Columns[0].HeaderText = "id_ghe_fonte_agente_epi";
                grd_epi.Columns[0].Visible = false;

                grd_epi.Columns[1].HeaderText = "Id_Ghe_fonte_Agente";
                grd_epi.Columns[1].Visible = false;

                grd_epi.Columns[2].HeaderText = "Id EPI";
                grd_epi.Columns[2].Visible = false;

                grd_epi.Columns[3].HeaderText = "EPI";

                grd_epi.Columns[4].HeaderText = "Tipo";

                grd_epi.Columns[5].HeaderText = "Finalidade";
                grd_epi.Columns[5].Visible = false;

                grd_epi.Columns[6].HeaderText = "Controle A";
                grd_epi.Columns[6].Visible = false;

                grd_epi.Columns[7].HeaderText = "Situação";
                grd_epi.Columns[7].Visible = false;

                grd_epi.Columns[8].HeaderText = "Classificação";
                grd_epi.Columns[8].DefaultCellStyle.ForeColor = Color.Red;

            }
            catch (Exception Ex)
            {
                MessageBox.Show(Ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
        }

        protected void cb_gheSetor_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                SelectItem itemSelecionado = (SelectItem)cb_gheSetor.SelectedItem;

                // criando o objeto GHE
                ghe = new Ghe(Convert.ToInt64(itemSelecionado.Valor), this.ppra, Convert.ToString(itemSelecionado.Nome), 0, ApplicationConstants.ATIVO, string.Empty);
                montaGridGheSetor();

            }

            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        public void montaGridGheSetor()
        {
            try
            {
                PpraFacade ppraFacade = PpraFacade.getInstance();

                grd_setor.Columns.Clear();

                DataSet ds = ppraFacade.findAllSetorByGhe(ghe);

                grd_setor.DataSource = ds.Tables[0].DefaultView;

                grd_setor.Columns[0].HeaderText = "id_ghe_Setor";
                grd_setor.Columns[0].Visible = false;

                grd_setor.Columns[1].HeaderText = "id_setor";
                grd_setor.Columns[1].Visible = false;

                grd_setor.Columns[2].HeaderText = "Setor";

                grd_setor.Columns[3].HeaderText = "Id_ghe";
                grd_setor.Columns[3].Visible = false;

                grd_setor.Columns[4].HeaderText = "GHE";
                grd_setor.Columns[4].Visible = false;

            }
            catch (Exception Ex)
            {
                MessageBox.Show(Ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
        }

        protected void btn_incluirSetor_Click(object sender, EventArgs e)
        {
            try
            {
                if (cb_gheSetor.SelectedIndex == -1)
                    throw new Exception("Selecione um ghe.");

                frm_PpraSetor formSetor = new frm_PpraSetor(ghe);
                formSetor.ShowDialog();

                montaGridGheSetor();

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        protected void btn_excluirSetor_Click(object sender, EventArgs e)
        {
            try
            {
                PpraFacade ppraFacade = PpraFacade.getInstance();

                GheSetor gheSetorExcluir = new GheSetor();

                if (grd_setor.CurrentRow != null)
                {

                    if (MessageBox.Show(Msg27 + Msg28, Msg01, MessageBoxButtons.YesNo) == DialogResult.Yes)
                    {
                        foreach (DataGridViewRow dvRow in grd_setor.SelectedRows)
                        {
                            ppraFacade.deleteGheSetor(new GheSetor((Int64)dvRow.Cells[0].Value, new Setor((long)dvRow.Cells[1].Value, (String)dvRow.Cells[2].Value, String.Empty), new Ghe((Int64)dvRow.Cells[3].Value, ppra, (String)dvRow.Cells[4].Value, 0, string.Empty, string.Empty), ApplicationConstants.DESATIVADO));
                        }

                        MessageBox.Show(Msg29, Msg01);

                        montaGridGheSetor();
                    }
                }
                else
                {
                    MessageBox.Show(Msg07, Msg08);
                }
            }
            catch (Exception Ex)
            {
                MessageBox.Show(Ex.Message);
            }
        }

        protected void btn_incluirFuncaoClientePPRA_Click(object sender, EventArgs e)
        {
            try
            {
                if (!String.IsNullOrEmpty(text_cliente.Text))
                {
                    frm_ClienteFuncaoIncluirFuncao formPpraClienteFuncao = new frm_ClienteFuncaoIncluirFuncao(this);
                    formPpraClienteFuncao.ShowDialog();
                }
                else
                {
                    throw new ClienteException(ClienteException.msg1);

                }

            }
            catch (Exception Ex)
            {
                MessageBox.Show(Ex.Message);
            }

        }

        public void montaGridFuncao()
        {
            try
            {
                PpraFacade ppraFacade = PpraFacade.getInstance();

                DataSet ds = ppraFacade.findAllClienteFuncao(ppra.VendedorCliente.getCliente());

                // montando a gride.

                grd_funcaoCliente.DataSource = ds.Tables["Funcoes"].DefaultView;

                grd_funcaoCliente.Columns[0].HeaderText = "id_seg_cliente_funcao";
                grd_funcaoCliente.Columns[1].HeaderText = "id_cliente";
                grd_funcaoCliente.Columns[2].HeaderText = "id_funcao";
                grd_funcaoCliente.Columns[3].HeaderText = "Descrição";
                grd_funcaoCliente.Columns[4].HeaderText = "CBO";

                grd_funcaoCliente.Columns[0].Visible = false;
                grd_funcaoCliente.Columns[1].Visible = false;
                grd_funcaoCliente.Columns[2].Visible = false;


            }
            catch (Exception Ex)
            {
                MessageBox.Show(Ex.Message);
            }

        }

        protected void tb_gheSetor_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (tb_gheSetor.SelectedIndex == 1 && !String.IsNullOrEmpty(text_cliente.Text))
            {
                montaGridFuncao();

            }

        }

        protected void btn_excluirFuncaoClientePPRA_Click(object sender, EventArgs e)
        {

            try
            {
                if (!String.IsNullOrEmpty(text_cliente.Text))
                {
                    PpraFacade ppraFacade = PpraFacade.getInstance();

                    if (grd_funcaoCliente.CurrentRow != null)
                    {

                        if (MessageBox.Show(Msg30 + Msg31, Msg01, MessageBoxButtons.YesNo) == DialogResult.Yes)
                        {
                            foreach (DataGridViewRow dvRow in grd_funcaoCliente.SelectedRows)
                            {
                                Int32 cellValue = dvRow.Index;

                                ClienteFuncao clientefuncao = ppraFacade.findClienteFuncaoById((Int64)grd_funcaoCliente.Rows[cellValue].Cells[0].Value);
                                clientefuncao.Situacao = ApplicationConstants.DESATIVADO;
                                clientefuncao.DataExclusao = DateTime.Now;
                                ppraFacade.updateClienteFuncao(clientefuncao);
                            }

                            MessageBox.Show(Msg32, Msg01);
                            montaGridFuncao();
                            grd_setorFuncao.Columns.Clear();


                        }
                    }
                    else
                    {
                        MessageBox.Show(Msg07, Msg08);
                    }

                }
                else
                {
                    throw new ClienteException(ClienteException.msg1);

                }

            }
            catch (Exception Ex)
            {
                MessageBox.Show(Ex.Message);
            }
        }

        protected void cb_gheFuncao_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                SelectItem itemSelecionado = (SelectItem)cb_gheFuncao.SelectedItem;
                grd_setorFuncao.Columns.Clear();

                // criando o objeto GHE
                ghe = new Ghe(Convert.ToInt64(itemSelecionado.Valor), ppra, Convert.ToString(itemSelecionado.Nome), 0, ApplicationConstants.ATIVO, string.Empty);
                iniciaComboSetorFuncao(cb_setorFuncao);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        protected void iniciaComboSetorFuncao(ComboBox combo)
        {
            PpraFacade ppraFacade = PpraFacade.getInstance();

            SelectItem item = null;
            String valor;
            String descricao;

            combo.Items.Clear();

            foreach (DataRow rows in ppraFacade.findAllSetorByGhe(ghe).Tables[0].Rows)
            {
                descricao = Convert.ToString(rows[Convert.ToString("SETOR")]);
                valor = Convert.ToString(rows[Convert.ToString("id_setor")]);

                item = new SelectItem(valor, descricao);

                combo.Items.Add(item);
            }

            combo.ValueMember = "nome";
        }

        protected void cb_setorFuncao_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                PpraFacade ppraFacade = PpraFacade.getInstance();

                Setor setor = new Setor();

                SelectItem gheSelecionado = (SelectItem)cb_gheFuncao.SelectedItem;
                ghe = new Ghe(Convert.ToInt64(gheSelecionado.Valor), ppra, Convert.ToString(gheSelecionado.Nome), 0, ApplicationConstants.ATIVO, string.Empty);

                SelectItem setorSelecioando = (SelectItem)cb_setorFuncao.SelectedItem;
                setor = new Setor(Convert.ToInt64(setorSelecioando.Valor), Convert.ToString(setorSelecioando.Nome), ApplicationConstants.ATIVO); 

                gheSetor = new GheSetor(null, setor, ghe, string.Empty);

                /* preenchendo objeto gheSetor dado um Ghe e um Setor */

                gheSetor = ppraFacade.findAllGheSetorByGheAndSetor(ghe, setor).First(x => x.Setor.Id == setor.Id && x.Ghe.Id == ghe.Id);

                if (gheSetor != null)
                {
                    gheSetorClienteFuncao = new GheSetorClienteFuncao(null, null, gheSetor, false, false, ApplicationConstants.ATIVO, null, false);
                    montaGridClienteFuncao();
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }

        }

        public void montaGridClienteFuncao()
        {
            try
            {
                PpraFacade ppraFacade = PpraFacade.getInstance();

                grd_setorFuncao.Columns.Clear();

                DataSet ds = ppraFacade.findAllGheSetorClienteFuncaoByGheSetor(gheSetor);

                grd_setorFuncao.DataSource = ds.Tables[0].DefaultView;

                grd_setorFuncao.Columns[0].HeaderText = "id_ghe_setor_cliente_funcao";
                grd_setorFuncao.Columns[0].Visible = false;

                grd_setorFuncao.Columns[1].HeaderText = "id_ghe_setor";
                grd_setorFuncao.Columns[1].Visible = false;

                grd_setorFuncao.Columns[2].HeaderText = "id_funcao";
                grd_setorFuncao.Columns[2].Visible = false;

                grd_setorFuncao.Columns[3].HeaderText = "Função";
                grd_setorFuncao.Columns[3].DisplayIndex = 1;
                grd_setorFuncao.Columns[3].ReadOnly = true;

                grd_setorFuncao.Columns[4].HeaderText = "Descrição da Atividade";
                grd_setorFuncao.Columns[4].DisplayIndex = 5;
                grd_setorFuncao.Columns[4].ReadOnly = true;

                grd_setorFuncao.Columns[5].HeaderText = "Confinado";
                grd_setorFuncao.Columns[5].DisplayIndex = 2;

                grd_setorFuncao.Columns[6].HeaderText = "Altura";
                grd_setorFuncao.Columns[6].DisplayIndex = 3;

                grd_setorFuncao.Columns[7].HeaderText = "id_seg_cliente_funcao";
                grd_setorFuncao.Columns[7].Visible = false;

                grd_setorFuncao.Columns[8].HeaderText = "id_comentario_funcao";
                grd_setorFuncao.Columns[8].Visible = false;

                /* incluindo botão na grid para selecionar a atividade da função */
                DataGridViewButtonColumn btn = new DataGridViewButtonColumn();
                grd_setorFuncao.Columns.Add(btn);

                btn.HeaderText = "Atividade";
                btn.Text = "...";
                btn.Name = "btn_prestador";
                btn.UseColumnTextForButtonValue = true;
                grd_setorFuncao.Columns[9].DisplayIndex = 4;
                grd_setorFuncao.Columns[9].ReadOnly = true;

            }
            catch (Exception Ex)
            {
                MessageBox.Show(Ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
        }

        protected void btn_incluirFuncaoCliente_Click(object sender, EventArgs e)
        {
            try
            {
                if (cb_setorFuncao.SelectedIndex == -1)
                {
                    throw new Exception(Msg33);
                }

                frm_ppraGheSetorClienteFuncao formGheSetorClienteFuncao = new frm_ppraGheSetorClienteFuncao(gheSetor, ppra.VendedorCliente.getCliente());
                formGheSetorClienteFuncao.ShowDialog();

                montaGridClienteFuncao();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
        }

        protected void btn_excluirFuncaoCliente_Click(object sender, EventArgs e)
        {
            try
            {
                PpraFacade ppraFacade = PpraFacade.getInstance();

                if (grd_setorFuncao.CurrentRow != null)
                {

                    if (MessageBox.Show(Msg34, Msg08, MessageBoxButtons.YesNo) == DialogResult.Yes)
                    {
                        foreach (DataGridViewRow dvRow in grd_setorFuncao.SelectedRows)
                        {
                            ppraFacade.excluirGheSetorClienteFuncao(new GheSetorClienteFuncao((Int64)dvRow.Cells[0].Value, null, gheSetor, false, false, ApplicationConstants.DESATIVADO, null, false));
                        }

                        MessageBox.Show(Msg35, Msg01);

                        montaGridClienteFuncao();
                    }
                }
                else
                {
                    MessageBox.Show(Msg07, Msg08);
                }
            }
            catch (Exception Ex)
            {
                MessageBox.Show(Ex.Message);
            }
        }

        private void btn_incluirAnexo_Click(object sender, EventArgs e)
        {
            try
            {
                //Validando dados


                if (!String.IsNullOrEmpty(text_conteudo.Text))
                {
                    //Incluindo o anexo no banco de dados

                    PpraFacade ppraFacade = PpraFacade.getInstance();

                    Anexo anexo = new Anexo(null, ppra, Convert.ToString(text_conteudo.Text.ToUpper()));

                    ppraFacade.incluiAnexo(anexo);

                    MontaGridAnexo();

                    text_conteudo.Text = String.Empty;
                    text_conteudo.Focus();

                }
                else
                {
                    throw new AnexoException(AnexoException.msg);
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void btn_excluirAnexo_Click(object sender, EventArgs e)
        {
            try
            {
                if (grd_anexo.CurrentRow != null)
                {
                    PpraFacade ppraFacade = PpraFacade.getInstance();

                    if (MessageBox.Show(msg36, Msg08, MessageBoxButtons.YesNo) == DialogResult.Yes)
                    {
                        Int32 cellValue = grd_anexo.CurrentRow.Index;

                        Int64 id = (Int64)grd_anexo.Rows[cellValue].Cells[0].Value;

                        Anexo anexo = new Anexo();
                        anexo.setId(id);

                        ppraFacade.excluiAnexo(anexo);

                        MessageBox.Show(msg37, Msg01);

                        MontaGridAnexo();
                    }
                }
                else
                {
                    MessageBox.Show(Msg07, Msg08);
                }


            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        public void MontaGridAnexo()
        {
            try
            {
                PpraFacade ppraFacade = PpraFacade.getInstance();

                grd_anexo.Columns.Clear();

                DataSet ds = ppraFacade.findAllAnexoByEstudo(ppra);

                grd_anexo.DataSource = ds.Tables["Anexos"].DefaultView;

                grd_anexo.Columns[0].HeaderText = "Id";
                grd_anexo.Columns[1].HeaderText = "Id_Estudo";
                grd_anexo.Columns[2].HeaderText = "Conteudo";

                grd_anexo.Columns[0].Visible = false;
                grd_anexo.Columns[1].Visible = false;

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void tb_dados_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (tb_dados.SelectedIndex == 6)
            {
                text_conteudo.Focus();
            }
        }

        private void grd_anexo_EditingControlShowing(object sender, DataGridViewEditingControlShowingEventArgs e)
        {
            if (grd_anexo.CurrentCell.ColumnIndex == 2)
            {
                if (e.Control is TextBox)
                {
                    ((TextBox)e.Control).CharacterCasing = CharacterCasing.Upper;
                }
            }
        }

        private void grd_anexo_CellEndEdit(object sender, DataGridViewCellEventArgs e)
        {
            try
            {

                if (grd_anexo.CurrentRow != null)
                {
                    // montando objeto da edição.

                    PpraFacade ppraFacade = PpraFacade.getInstance();

                    Int32 cellValue = grd_anexo.CurrentRow.Index;

                    Int64 id = (Int64)grd_anexo.Rows[cellValue].Cells[0].Value;
                    String conteudo = (String)grd_anexo.Rows[cellValue].Cells[2].Value;

                    Anexo anexo = new Anexo(id, ppra, conteudo);

                    ppraFacade.alteraAnexo(anexo);
                    MontaGridAnexo();
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                MontaGridAnexo();

            }
        }

        private void btn_gravarComentario_Click(object sender, EventArgs e)
        {
            SalvarPpra();
        }

        private void SalvarPpra()
        {
            try
            {
                if (ppra.Id == null)
                {

                    if (String.IsNullOrEmpty(text_cliente.Text))
                    {
                        throw new PpraException(PpraException.msg);
                    }

                    PpraFacade ppraFacade = PpraFacade.getInstance();

                    //setando valores para o estudo.

                    // parametros do contratante.
                    ppra.DataInicioContrato = Convert.ToString(text_inicioContr.Text);
                    ppra.DataFimContrato = Convert.ToString(text_fimContr.Text);
                    ppra.NumContrato = text_numContrato.Text;

                    // parametros da criacao
                    ppra.DataCriacao = Convert.ToDateTime(dt_criacao.Text);
                    ppra.DataVencimento = Convert.ToDateTime(dt_vencimento.Text);

                    // parametros do risco
                    ppra.GrauRisco = Convert.ToString(cb_grauRisco.SelectedValue);

                    // parametros do endereco
                    ppra.Endereco = text_endereco.Text;
                    ppra.Numero = text_numero.Text;
                    ppra.Complemento = text_complemento.Text;
                    ppra.Bairro = text_bairro.Text;
                    ppra.Cidade = text_cidade.Text;
                    ppra.Uf = Convert.ToString(cb_uf.SelectedValue);
                    ppra.Obra = text_obra.Text;
                    ppra.LocalObra = text_localObra.Text;
                    ppra.Situacao = ApplicationConstants.CONSTRUCAO;
                    ppra.Cep = text_cep.Text;
                    ppra.Comentario = text_comentario.Text;

                    // chamada do metodo para incluir o estudo
                    ppra = ppraFacade.incluirPpra(ppra);

                    MessageBox.Show(Msg02 + ppra.CodigoEstudo + "Gravado com sucesso", "Confirmação");
                    text_codPpra.Text = Convert.ToString(ppra.CodigoEstudo);

                    // desabilitando botão impedindo a gravação

                    enableCommand(true);

                    // Mundando nome do botão novo

                    btn_salvar.Text = "&Gravar";

                }
                else
                {
                    PpraFacade ppraFacade = PpraFacade.getInstance();

                    //setando valores para o estudo.

                    // parametros do contratante.
                    ppra.DataInicioContrato = Convert.ToString(text_inicioContr.Text);
                    ppra.DataFimContrato = Convert.ToString(text_fimContr.Text);
                    ppra.NumContrato = text_numContrato.Text;

                    // parametros da criacao
                    ppra.DataCriacao = Convert.ToDateTime(dt_criacao.Text);
                    ppra.DataVencimento = Convert.ToDateTime(dt_vencimento.Text);

                    // parametros do risco
                    ppra.GrauRisco = Convert.ToString(cb_grauRisco.SelectedValue);

                    // parametros do endereco
                    ppra.Endereco = text_endereco.Text;
                    ppra.Numero = text_numero.Text;
                    ppra.Complemento = text_complemento.Text;
                    ppra.Bairro = text_bairro.Text;
                    ppra.Cidade = text_cidade.Text;
                    ppra.Uf = Convert.ToString(cb_uf.SelectedValue);
                    ppra.Obra = text_obra.Text;
                    ppra.LocalObra = text_localObra.Text;
                    ppra.Situacao = ApplicationConstants.CONSTRUCAO;
                    ppra.Cep = text_cep.Text;
                    ppra.Comentario = text_comentario.Text;

                    // chamada no metodo para alterar o estudo em tempo e execucao da inclusão do estudo.
                    ppra = ppraFacade.alteraPpra(ppra);

                    MessageBox.Show(Msg04, Msg01);
                }
            }
            catch (ExameException ex)
            {
                MessageBox.Show(ex.Message);
            }

        }

        private void grd_setorFuncao_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.ColumnIndex == 9)
            {
                PpraFacade ppraFacade = PpraFacade.getInstance();
                PcmsoFacade pcmsoFacade = PcmsoFacade.getInstance();

                frmFuncaoComentario buscarComentario = new frmFuncaoComentario(
                    ppraFacade.findFuncaoById((Int64)grd_setorFuncao.Rows[e.RowIndex].Cells[2].Value));
                buscarComentario.ShowDialog();

                if (buscarComentario.ComentarioFuncao != null)
                {
                    gheSetorClienteFuncao = ppraFacade.findGheSetorClienteFuncaoById((Int64)grd_setorFuncao.Rows[e.RowIndex].Cells[0].Value);
                    gheSetorClienteFuncao.ComentarioFuncao = buscarComentario.ComentarioFuncao;
                    pcmsoFacade.updateGheSetorClienteFuncao(gheSetorClienteFuncao);

                }

                montaGridClienteFuncao();
            }

        }

        private void grd_setorFuncao_CellFormatting(object sender, DataGridViewCellFormattingEventArgs e)
        {
            if (e.ColumnIndex == 7)
            {
                DataGridViewCell cell = this.grd_setorFuncao.Rows[e.RowIndex].Cells[e.ColumnIndex];

                cell.ToolTipText = msg40;
            }
            else if (e.ColumnIndex == 5)
            {
                DataGridViewCell cell = this.grd_setorFuncao.Rows[e.RowIndex].Cells[e.ColumnIndex];

                cell.ToolTipText = msg43;
            }
            else if (e.ColumnIndex == 6)
            {
                DataGridViewCell cell = this.grd_setorFuncao.Rows[e.RowIndex].Cells[e.ColumnIndex];

                cell.ToolTipText = msg44;

            }

        }

        private void btnIncluirPrevisao_Click(object sender, EventArgs e)
        {
            try
            {
                /* somente poderá ser incluído caso o estudo já tenha sido gravado */
                if (ppra.Id == null)
                {
                    throw new Exception(msg41);
                }

                frmPcmsoEstimativa formEstimativa = new frmPcmsoEstimativa(ppra);
                formEstimativa.ShowDialog();

                montaGridEstimativa();

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
        }

        public void montaGridEstimativa()
        {
            this.dgPrevisao.Columns.Clear();

            PpraFacade ppraFacade = PpraFacade.getInstance();

            DataSet ds = ppraFacade.findAllEstimativaInEstudo(ppra);

            dgPrevisao.DataSource = ds.Tables[0].DefaultView;

            dgPrevisao.Columns[0].HeaderText = "Id_Estimativa_estudo";
            dgPrevisao.Columns[0].Visible = false;

            dgPrevisao.Columns[1].HeaderText = "Id_Estimativa";
            dgPrevisao.Columns[1].Visible = false;

            dgPrevisao.Columns[2].HeaderText = "Id Estudo";
            dgPrevisao.Columns[2].Visible = false;

            dgPrevisao.Columns[3].HeaderText = "Estimativa";
            dgPrevisao.Columns[3].ReadOnly = true;

            dgPrevisao.Columns[4].HeaderText = "Qtde";
            dgPrevisao.Columns[4].DefaultCellStyle.BackColor = Color.White;

        }

        private void dgPrevisao_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!Char.IsDigit(e.KeyChar) && e.KeyChar != (char)8)
            {
                e.Handled = true;

            }
        }

        private void dgPrevisao_EditingControlShowing(object sender, DataGridViewEditingControlShowingEventArgs e)
        {
            if (e.Control is DataGridViewTextBoxEditingControl)
            {
                e.Control.KeyPress += new KeyPressEventHandler(dgPrevisao_KeyPress);

            }
        }

        private void dgPrevisao_CellEndEdit(object sender, DataGridViewCellEventArgs e)
        {
            try
            {

                /* validando o valor digitado caso o usuário não tenha digitado nenhuma informação o campo
                 * obrigatoriamente ficará com zero */

                if (String.IsNullOrEmpty(dgPrevisao.Rows[e.RowIndex].Cells[e.ColumnIndex].EditedFormattedValue.ToString().Trim()))
                {
                    this.dgPrevisao.Rows[e.RowIndex].Cells[e.ColumnIndex].Value = 0;
                }

                /* gravando a informação no banco de dados */

                PpraFacade ppraFacade = PpraFacade.getInstance();


                ppraFacade.updateEstimaEstudo(new EstimativaEstudo(Convert.ToInt64(dgPrevisao.Rows[e.RowIndex].Cells[0].Value),
                                ppra, new Estimativa(Convert.ToInt64(dgPrevisao.Rows[e.RowIndex].Cells[1].Value), (String)dgPrevisao.Rows[e.RowIndex].Cells[3].Value, String.Empty),
                                Convert.ToInt32(dgPrevisao.Rows[e.RowIndex].Cells[4].EditedFormattedValue)));


            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
        }

        private void dgPrevisao_DataError(object sender, DataGridViewDataErrorEventArgs e)
        {
            try
            {
                /*validando valor digitado para não dar estouro de inteiro32 */

                int number = Int32.Parse((String)dgPrevisao.Rows[e.RowIndex].Cells[e.ColumnIndex].EditedFormattedValue);
            }

            catch (OverflowException)
            {
                MessageBox.Show(msg42, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
        }

        private void btnExcluirPrevisao_Click(object sender, EventArgs e)
        {
            try
            {
                if (dgPrevisao.CurrentRow == null)
                {
                    throw new Exception(Msg07);
                }

                PpraFacade ppraFacade = PpraFacade.getInstance();

                ppraFacade.deleteEstimativaEstudo(new EstimativaEstudo(Convert.ToInt64(dgPrevisao.CurrentRow.Cells[0].Value),
                                ppra, new Estimativa(Convert.ToInt64(dgPrevisao.CurrentRow.Cells[1].Value), (String)dgPrevisao.CurrentRow.Cells[3].Value, String.Empty),
                                Convert.ToInt32(dgPrevisao.CurrentRow.Cells[4].EditedFormattedValue)));

                montaGridEstimativa();


            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
        }

        private void grd_setorFuncao_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.ColumnIndex == 5 || e.ColumnIndex == 6)
            {
                PcmsoFacade pcmsoFacade = PcmsoFacade.getInstance();

                grd_setorFuncao.EndEdit();

                pcmsoFacade.updateGheSetorClienteFuncao(new GheSetorClienteFuncao((Int64)grd_setorFuncao.Rows[e.RowIndex].Cells[0].Value,
                    null, null,
                    (Boolean)grd_setorFuncao.Rows[e.RowIndex].Cells[5].Value,
                    (Boolean)grd_setorFuncao.Rows[e.RowIndex].Cells[6].Value, ApplicationConstants.ATIVO, (Int64?)grd_setorFuncao.Rows[e.RowIndex].Cells[8].Value == null ? null : new ComentarioFuncao((Int64)grd_setorFuncao.Rows[e.RowIndex].Cells[8].Value, null, grd_setorFuncao.Rows[e.RowIndex].Cells[4].Value.ToString(), string.Empty), (bool)grd_setorFuncao.Rows[e.RowIndex].Cells[9].Value));
            }
        }


        protected virtual void montaGridCnaeCliente()
        {
            try
            {
                ClienteFacade clienteFacade = ClienteFacade.getInstance();

                dgvCnaeCliente.Columns.Clear();

                DataSet ds = clienteFacade.findAllCnaeByCliente(vendedorCliente.getCliente());

                dgvCnaeCliente.DataSource = ds.Tables[0].DefaultView;

                dgvCnaeCliente.Columns[0].HeaderText = "id_cnae";
                dgvCnaeCliente.Columns[0].Visible = false;
                dgvCnaeCliente.Columns[1].HeaderText = "codigo";
                dgvCnaeCliente.Columns[2].HeaderText = "atividade";
                dgvCnaeCliente.Columns[3].HeaderText = "grau_de_risco";
                dgvCnaeCliente.Columns[4].HeaderText = "grupo_cipa";
                dgvCnaeCliente.Columns[5].HeaderText = "situação";
                dgvCnaeCliente.Columns[5].Visible = false;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }

        protected virtual void montaGridCnaeContratante()
        {
            try
            {
                ClienteFacade clienteFacade = ClienteFacade.getInstance();

                dgvCnaeContratante.Columns.Clear();

                DataSet ds = clienteFacade.findAllCnaeByCliente(contratante);

                dgvCnaeContratante.DataSource = ds.Tables[0].DefaultView;

                dgvCnaeContratante.Columns[0].HeaderText = "id_cnae";
                dgvCnaeContratante.Columns[0].Visible = false;
                dgvCnaeContratante.Columns[1].HeaderText = "codigo";
                dgvCnaeContratante.Columns[2].HeaderText = "atividade";
                dgvCnaeContratante.Columns[3].HeaderText = "grau_de_risco";
                dgvCnaeContratante.Columns[4].HeaderText = "grupo_cipa";
                dgvCnaeContratante.Columns[5].HeaderText = "situação";
                dgvCnaeContratante.Columns[5].Visible = false;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }

        private void text_cep_Leave(object sender, EventArgs e)
        {
            try
            {
                /* validando o cep digitado */
                if (!ValidaCampoHelper.ValidaCepDigitado(text_cep.Text))
                    throw new Exception("CEP inválido");
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
                text_cep.Focus();
            }
        }

        private void grd_ghe_DataBindingComplete(object sender, DataGridViewBindingCompleteEventArgs e)
        {
            DataGridView dgv = sender as DataGridView;
            dgv.ClearSelection();
        }

        private void grd_fonte_DataBindingComplete(object sender, DataGridViewBindingCompleteEventArgs e)
        {
            DataGridView dgv = sender as DataGridView;
            dgv.ClearSelection();
        }

        private void grd_agente_DataBindingComplete(object sender, DataGridViewBindingCompleteEventArgs e)
        {
            DataGridView dgv = sender as DataGridView;
            dgv.ClearSelection();
        }
    }
}