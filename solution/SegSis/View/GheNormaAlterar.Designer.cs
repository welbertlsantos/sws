﻿namespace SWS.View
{
    partial class frm_GheNormaAlterar
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frm_GheNormaAlterar));
            this.grb_paginacao = new System.Windows.Forms.GroupBox();
            this.btn_limpar = new System.Windows.Forms.Button();
            this.btn_novo = new System.Windows.Forms.Button();
            this.btn_fechar = new System.Windows.Forms.Button();
            this.btn_OK = new System.Windows.Forms.Button();
            this.grb_norma = new System.Windows.Forms.GroupBox();
            this.grd_norma = new System.Windows.Forms.DataGridView();
            this.grb_dados = new System.Windows.Forms.GroupBox();
            this.btn_buscar = new System.Windows.Forms.Button();
            this.lbl_descricao = new System.Windows.Forms.Label();
            this.text_descricao = new System.Windows.Forms.TextBox();
            this.grb_paginacao.SuspendLayout();
            this.grb_norma.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grd_norma)).BeginInit();
            this.grb_dados.SuspendLayout();
            this.SuspendLayout();
            // 
            // grb_paginacao
            // 
            this.grb_paginacao.Controls.Add(this.btn_limpar);
            this.grb_paginacao.Controls.Add(this.btn_novo);
            this.grb_paginacao.Controls.Add(this.btn_fechar);
            this.grb_paginacao.Controls.Add(this.btn_OK);
            this.grb_paginacao.Location = new System.Drawing.Point(12, 337);
            this.grb_paginacao.Name = "grb_paginacao";
            this.grb_paginacao.Size = new System.Drawing.Size(576, 51);
            this.grb_paginacao.TabIndex = 5;
            this.grb_paginacao.TabStop = false;
            // 
            // btn_limpar
            // 
            this.btn_limpar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_limpar.Image = global::SWS.Properties.Resources.vassoura;
            this.btn_limpar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btn_limpar.Location = new System.Drawing.Point(171, 19);
            this.btn_limpar.Name = "btn_limpar";
            this.btn_limpar.Size = new System.Drawing.Size(75, 23);
            this.btn_limpar.TabIndex = 6;
            this.btn_limpar.Text = "&Limpar";
            this.btn_limpar.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btn_limpar.UseVisualStyleBackColor = true;
            this.btn_limpar.Click += new System.EventHandler(this.btn_limpar_Click);
            // 
            // btn_novo
            // 
            this.btn_novo.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_novo.Image = global::SWS.Properties.Resources.icone_mais;
            this.btn_novo.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btn_novo.Location = new System.Drawing.Point(90, 19);
            this.btn_novo.Name = "btn_novo";
            this.btn_novo.Size = new System.Drawing.Size(75, 23);
            this.btn_novo.TabIndex = 5;
            this.btn_novo.Text = "&Novo";
            this.btn_novo.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btn_novo.UseVisualStyleBackColor = true;
            this.btn_novo.Click += new System.EventHandler(this.btn_novo_Click);
            // 
            // btn_fechar
            // 
            this.btn_fechar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_fechar.Image = global::SWS.Properties.Resources.close;
            this.btn_fechar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btn_fechar.Location = new System.Drawing.Point(252, 19);
            this.btn_fechar.Name = "btn_fechar";
            this.btn_fechar.Size = new System.Drawing.Size(75, 23);
            this.btn_fechar.TabIndex = 7;
            this.btn_fechar.Text = "&Fechar";
            this.btn_fechar.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btn_fechar.UseVisualStyleBackColor = true;
            this.btn_fechar.Click += new System.EventHandler(this.btn_fechar_Click);
            // 
            // btn_OK
            // 
            this.btn_OK.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_OK.Image = global::SWS.Properties.Resources.fechar_ico;
            this.btn_OK.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btn_OK.Location = new System.Drawing.Point(9, 19);
            this.btn_OK.Name = "btn_OK";
            this.btn_OK.Size = new System.Drawing.Size(75, 23);
            this.btn_OK.TabIndex = 4;
            this.btn_OK.Text = "&OK";
            this.btn_OK.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btn_OK.UseVisualStyleBackColor = true;
            this.btn_OK.Click += new System.EventHandler(this.btn_OK_Click);
            // 
            // grb_norma
            // 
            this.grb_norma.Controls.Add(this.grd_norma);
            this.grb_norma.Location = new System.Drawing.Point(12, 109);
            this.grb_norma.Name = "grb_norma";
            this.grb_norma.Size = new System.Drawing.Size(576, 222);
            this.grb_norma.TabIndex = 4;
            this.grb_norma.TabStop = false;
            this.grb_norma.Text = "Notas GHE";
            // 
            // grd_norma
            // 
            this.grd_norma.AllowUserToAddRows = false;
            this.grd_norma.AllowUserToDeleteRows = false;
            this.grd_norma.AllowUserToOrderColumns = true;
            this.grd_norma.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this.grd_norma.BackgroundColor = System.Drawing.Color.White;
            this.grd_norma.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.grd_norma.Location = new System.Drawing.Point(7, 20);
            this.grd_norma.Name = "grd_norma";
            this.grd_norma.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.grd_norma.Size = new System.Drawing.Size(563, 196);
            this.grd_norma.TabIndex = 3;
            // 
            // grb_dados
            // 
            this.grb_dados.Controls.Add(this.btn_buscar);
            this.grb_dados.Controls.Add(this.lbl_descricao);
            this.grb_dados.Controls.Add(this.text_descricao);
            this.grb_dados.Location = new System.Drawing.Point(12, 12);
            this.grb_dados.Name = "grb_dados";
            this.grb_dados.Size = new System.Drawing.Size(575, 91);
            this.grb_dados.TabIndex = 3;
            this.grb_dados.TabStop = false;
            this.grb_dados.Text = "Dados";
            // 
            // btn_buscar
            // 
            this.btn_buscar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_buscar.Image = global::SWS.Properties.Resources.lupa;
            this.btn_buscar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btn_buscar.Location = new System.Drawing.Point(9, 58);
            this.btn_buscar.Name = "btn_buscar";
            this.btn_buscar.Size = new System.Drawing.Size(75, 23);
            this.btn_buscar.TabIndex = 2;
            this.btn_buscar.Text = "&Buscar";
            this.btn_buscar.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btn_buscar.UseVisualStyleBackColor = true;
            this.btn_buscar.Click += new System.EventHandler(this.btn_buscar_Click);
            // 
            // lbl_descricao
            // 
            this.lbl_descricao.AutoSize = true;
            this.lbl_descricao.Location = new System.Drawing.Point(6, 16);
            this.lbl_descricao.Name = "lbl_descricao";
            this.lbl_descricao.Size = new System.Drawing.Size(35, 13);
            this.lbl_descricao.TabIndex = 1;
            this.lbl_descricao.Text = "Título";
            // 
            // text_descricao
            // 
            this.text_descricao.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.text_descricao.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.text_descricao.Location = new System.Drawing.Point(9, 32);
            this.text_descricao.MaxLength = 100;
            this.text_descricao.Name = "text_descricao";
            this.text_descricao.Size = new System.Drawing.Size(560, 20);
            this.text_descricao.TabIndex = 1;
            // 
            // frm_GheNormaAlterar
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(600, 400);
            this.Controls.Add(this.grb_paginacao);
            this.Controls.Add(this.grb_norma);
            this.Controls.Add(this.grb_dados);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "frm_GheNormaAlterar";
            this.Text = "ALTERAR NOTA GHE";
            this.grb_paginacao.ResumeLayout(false);
            this.grb_norma.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.grd_norma)).EndInit();
            this.grb_dados.ResumeLayout(false);
            this.grb_dados.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox grb_paginacao;
        private System.Windows.Forms.Button btn_novo;
        private System.Windows.Forms.Button btn_fechar;
        private System.Windows.Forms.Button btn_OK;
        private System.Windows.Forms.GroupBox grb_norma;
        private System.Windows.Forms.DataGridView grd_norma;
        private System.Windows.Forms.GroupBox grb_dados;
        private System.Windows.Forms.Button btn_limpar;
        private System.Windows.Forms.Button btn_buscar;
        private System.Windows.Forms.Label lbl_descricao;
        private System.Windows.Forms.TextBox text_descricao;
    }
}