﻿namespace SWS.View
{
    partial class frmAtendimentoAnexo
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            this.flpAcao = new System.Windows.Forms.FlowLayoutPanel();
            this.btnIncluir = new System.Windows.Forms.Button();
            this.btnExcluir = new System.Windows.Forms.Button();
            this.btnFechar = new System.Windows.Forms.Button();
            this.lblCodigoAtendimento = new System.Windows.Forms.TextBox();
            this.lblTipoAtendimento = new System.Windows.Forms.TextBox();
            this.lblDataAtendimento = new System.Windows.Forms.TextBox();
            this.lblNomeColaborador = new System.Windows.Forms.TextBox();
            this.lblRGColaborador = new System.Windows.Forms.TextBox();
            this.lblCpfColaborador = new System.Windows.Forms.TextBox();
            this.lblCliente = new System.Windows.Forms.TextBox();
            this.lblCnpjCliente = new System.Windows.Forms.TextBox();
            this.lblSetor = new System.Windows.Forms.TextBox();
            this.lblFuncao = new System.Windows.Forms.TextBox();
            this.textFuncao = new System.Windows.Forms.TextBox();
            this.textSetor = new System.Windows.Forms.TextBox();
            this.textCNPJ = new System.Windows.Forms.TextBox();
            this.textEmpresa = new System.Windows.Forms.TextBox();
            this.textDataAtendimento = new System.Windows.Forms.TextBox();
            this.textCpf = new System.Windows.Forms.TextBox();
            this.textRg = new System.Windows.Forms.TextBox();
            this.textTipoAtendimento = new System.Windows.Forms.TextBox();
            this.textNomeColaborador = new System.Windows.Forms.TextBox();
            this.textCodigoAtendimento = new System.Windows.Forms.TextBox();
            this.grbAnexo = new System.Windows.Forms.GroupBox();
            this.dgvArquivoAnexo = new System.Windows.Forms.DataGridView();
            ((System.ComponentModel.ISupportInitialize)(this.spForm)).BeginInit();
            this.spForm.Panel1.SuspendLayout();
            this.spForm.Panel2.SuspendLayout();
            this.spForm.SuspendLayout();
            this.pnlForm.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.banner)).BeginInit();
            this.flpAcao.SuspendLayout();
            this.grbAnexo.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvArquivoAnexo)).BeginInit();
            this.SuspendLayout();
            // 
            // spForm
            // 
            // 
            // spForm.Panel1
            // 
            this.spForm.Panel1.Controls.Add(this.flpAcao);
            // 
            // pnlForm
            // 
            this.pnlForm.Controls.Add(this.grbAnexo);
            this.pnlForm.Controls.Add(this.textFuncao);
            this.pnlForm.Controls.Add(this.textSetor);
            this.pnlForm.Controls.Add(this.textCNPJ);
            this.pnlForm.Controls.Add(this.textEmpresa);
            this.pnlForm.Controls.Add(this.textDataAtendimento);
            this.pnlForm.Controls.Add(this.textCpf);
            this.pnlForm.Controls.Add(this.textRg);
            this.pnlForm.Controls.Add(this.textTipoAtendimento);
            this.pnlForm.Controls.Add(this.textNomeColaborador);
            this.pnlForm.Controls.Add(this.textCodigoAtendimento);
            this.pnlForm.Controls.Add(this.lblFuncao);
            this.pnlForm.Controls.Add(this.lblSetor);
            this.pnlForm.Controls.Add(this.lblCnpjCliente);
            this.pnlForm.Controls.Add(this.lblCliente);
            this.pnlForm.Controls.Add(this.lblCpfColaborador);
            this.pnlForm.Controls.Add(this.lblRGColaborador);
            this.pnlForm.Controls.Add(this.lblNomeColaborador);
            this.pnlForm.Controls.Add(this.lblDataAtendimento);
            this.pnlForm.Controls.Add(this.lblTipoAtendimento);
            this.pnlForm.Controls.Add(this.lblCodigoAtendimento);
            // 
            // flpAcao
            // 
            this.flpAcao.Controls.Add(this.btnIncluir);
            this.flpAcao.Controls.Add(this.btnExcluir);
            this.flpAcao.Controls.Add(this.btnFechar);
            this.flpAcao.Dock = System.Windows.Forms.DockStyle.Fill;
            this.flpAcao.Location = new System.Drawing.Point(0, 0);
            this.flpAcao.Name = "flpAcao";
            this.flpAcao.Size = new System.Drawing.Size(784, 35);
            this.flpAcao.TabIndex = 0;
            // 
            // btnIncluir
            // 
            this.btnIncluir.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnIncluir.Image = global::SWS.Properties.Resources.icone_mais;
            this.btnIncluir.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnIncluir.Location = new System.Drawing.Point(3, 3);
            this.btnIncluir.Name = "btnIncluir";
            this.btnIncluir.Size = new System.Drawing.Size(75, 23);
            this.btnIncluir.TabIndex = 10;
            this.btnIncluir.Text = "&Incluir";
            this.btnIncluir.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnIncluir.UseVisualStyleBackColor = true;
            this.btnIncluir.Click += new System.EventHandler(this.btnIncluir_Click);
            // 
            // btnExcluir
            // 
            this.btnExcluir.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnExcluir.Image = global::SWS.Properties.Resources.lixeira;
            this.btnExcluir.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnExcluir.Location = new System.Drawing.Point(84, 3);
            this.btnExcluir.Name = "btnExcluir";
            this.btnExcluir.Size = new System.Drawing.Size(75, 23);
            this.btnExcluir.TabIndex = 11;
            this.btnExcluir.Text = "&Excluir";
            this.btnExcluir.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnExcluir.UseVisualStyleBackColor = true;
            this.btnExcluir.Click += new System.EventHandler(this.btnExcluir_Click);
            // 
            // btnFechar
            // 
            this.btnFechar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnFechar.Image = global::SWS.Properties.Resources.close;
            this.btnFechar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnFechar.Location = new System.Drawing.Point(165, 3);
            this.btnFechar.Name = "btnFechar";
            this.btnFechar.Size = new System.Drawing.Size(75, 23);
            this.btnFechar.TabIndex = 12;
            this.btnFechar.Text = "&Fechar";
            this.btnFechar.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnFechar.UseVisualStyleBackColor = true;
            this.btnFechar.Click += new System.EventHandler(this.btnFechar_Click);
            // 
            // lblCodigoAtendimento
            // 
            this.lblCodigoAtendimento.BackColor = System.Drawing.Color.LightGray;
            this.lblCodigoAtendimento.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblCodigoAtendimento.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCodigoAtendimento.Location = new System.Drawing.Point(9, 8);
            this.lblCodigoAtendimento.Name = "lblCodigoAtendimento";
            this.lblCodigoAtendimento.ReadOnly = true;
            this.lblCodigoAtendimento.Size = new System.Drawing.Size(133, 21);
            this.lblCodigoAtendimento.TabIndex = 0;
            this.lblCodigoAtendimento.TabStop = false;
            this.lblCodigoAtendimento.Text = "Código Atendimento";
            // 
            // lblTipoAtendimento
            // 
            this.lblTipoAtendimento.BackColor = System.Drawing.Color.LightGray;
            this.lblTipoAtendimento.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblTipoAtendimento.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTipoAtendimento.Location = new System.Drawing.Point(9, 28);
            this.lblTipoAtendimento.Name = "lblTipoAtendimento";
            this.lblTipoAtendimento.ReadOnly = true;
            this.lblTipoAtendimento.Size = new System.Drawing.Size(133, 21);
            this.lblTipoAtendimento.TabIndex = 1;
            this.lblTipoAtendimento.TabStop = false;
            this.lblTipoAtendimento.Text = "Tipo de Atendimento";
            // 
            // lblDataAtendimento
            // 
            this.lblDataAtendimento.BackColor = System.Drawing.Color.LightGray;
            this.lblDataAtendimento.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblDataAtendimento.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDataAtendimento.Location = new System.Drawing.Point(9, 48);
            this.lblDataAtendimento.Name = "lblDataAtendimento";
            this.lblDataAtendimento.ReadOnly = true;
            this.lblDataAtendimento.Size = new System.Drawing.Size(133, 21);
            this.lblDataAtendimento.TabIndex = 2;
            this.lblDataAtendimento.TabStop = false;
            this.lblDataAtendimento.Text = "Data do Atendimento";
            // 
            // lblNomeColaborador
            // 
            this.lblNomeColaborador.BackColor = System.Drawing.Color.LightGray;
            this.lblNomeColaborador.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblNomeColaborador.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNomeColaborador.Location = new System.Drawing.Point(9, 68);
            this.lblNomeColaborador.Name = "lblNomeColaborador";
            this.lblNomeColaborador.ReadOnly = true;
            this.lblNomeColaborador.Size = new System.Drawing.Size(133, 21);
            this.lblNomeColaborador.TabIndex = 3;
            this.lblNomeColaborador.TabStop = false;
            this.lblNomeColaborador.Text = "Nome do Colaborador";
            // 
            // lblRGColaborador
            // 
            this.lblRGColaborador.BackColor = System.Drawing.Color.LightGray;
            this.lblRGColaborador.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblRGColaborador.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblRGColaborador.Location = new System.Drawing.Point(9, 88);
            this.lblRGColaborador.Name = "lblRGColaborador";
            this.lblRGColaborador.ReadOnly = true;
            this.lblRGColaborador.Size = new System.Drawing.Size(133, 21);
            this.lblRGColaborador.TabIndex = 4;
            this.lblRGColaborador.TabStop = false;
            this.lblRGColaborador.Text = "RG";
            // 
            // lblCpfColaborador
            // 
            this.lblCpfColaborador.BackColor = System.Drawing.Color.LightGray;
            this.lblCpfColaborador.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblCpfColaborador.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCpfColaborador.Location = new System.Drawing.Point(9, 108);
            this.lblCpfColaborador.Name = "lblCpfColaborador";
            this.lblCpfColaborador.ReadOnly = true;
            this.lblCpfColaborador.Size = new System.Drawing.Size(133, 21);
            this.lblCpfColaborador.TabIndex = 5;
            this.lblCpfColaborador.TabStop = false;
            this.lblCpfColaborador.Text = "CPF";
            // 
            // lblCliente
            // 
            this.lblCliente.BackColor = System.Drawing.Color.LightGray;
            this.lblCliente.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblCliente.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCliente.Location = new System.Drawing.Point(9, 128);
            this.lblCliente.Name = "lblCliente";
            this.lblCliente.ReadOnly = true;
            this.lblCliente.Size = new System.Drawing.Size(133, 21);
            this.lblCliente.TabIndex = 6;
            this.lblCliente.TabStop = false;
            this.lblCliente.Text = "Cliente";
            // 
            // lblCnpjCliente
            // 
            this.lblCnpjCliente.BackColor = System.Drawing.Color.LightGray;
            this.lblCnpjCliente.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblCnpjCliente.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCnpjCliente.Location = new System.Drawing.Point(9, 148);
            this.lblCnpjCliente.Name = "lblCnpjCliente";
            this.lblCnpjCliente.ReadOnly = true;
            this.lblCnpjCliente.Size = new System.Drawing.Size(133, 21);
            this.lblCnpjCliente.TabIndex = 7;
            this.lblCnpjCliente.TabStop = false;
            this.lblCnpjCliente.Text = "CNPJ";
            // 
            // lblSetor
            // 
            this.lblSetor.BackColor = System.Drawing.Color.LightGray;
            this.lblSetor.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblSetor.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSetor.Location = new System.Drawing.Point(9, 168);
            this.lblSetor.Name = "lblSetor";
            this.lblSetor.ReadOnly = true;
            this.lblSetor.Size = new System.Drawing.Size(133, 21);
            this.lblSetor.TabIndex = 8;
            this.lblSetor.TabStop = false;
            this.lblSetor.Text = "Setor";
            // 
            // lblFuncao
            // 
            this.lblFuncao.BackColor = System.Drawing.Color.LightGray;
            this.lblFuncao.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblFuncao.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblFuncao.Location = new System.Drawing.Point(9, 188);
            this.lblFuncao.Name = "lblFuncao";
            this.lblFuncao.ReadOnly = true;
            this.lblFuncao.Size = new System.Drawing.Size(133, 21);
            this.lblFuncao.TabIndex = 9;
            this.lblFuncao.TabStop = false;
            this.lblFuncao.Text = "Função";
            // 
            // textFuncao
            // 
            this.textFuncao.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.textFuncao.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textFuncao.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textFuncao.Location = new System.Drawing.Point(141, 188);
            this.textFuncao.MaxLength = 100;
            this.textFuncao.Name = "textFuncao";
            this.textFuncao.ReadOnly = true;
            this.textFuncao.Size = new System.Drawing.Size(609, 21);
            this.textFuncao.TabIndex = 19;
            this.textFuncao.TabStop = false;
            // 
            // textSetor
            // 
            this.textSetor.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.textSetor.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textSetor.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textSetor.Location = new System.Drawing.Point(141, 168);
            this.textSetor.MaxLength = 100;
            this.textSetor.Name = "textSetor";
            this.textSetor.ReadOnly = true;
            this.textSetor.Size = new System.Drawing.Size(609, 21);
            this.textSetor.TabIndex = 18;
            this.textSetor.TabStop = false;
            // 
            // textCNPJ
            // 
            this.textCNPJ.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.textCNPJ.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textCNPJ.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textCNPJ.Location = new System.Drawing.Point(141, 148);
            this.textCNPJ.MaxLength = 100;
            this.textCNPJ.Name = "textCNPJ";
            this.textCNPJ.ReadOnly = true;
            this.textCNPJ.Size = new System.Drawing.Size(609, 21);
            this.textCNPJ.TabIndex = 17;
            this.textCNPJ.TabStop = false;
            // 
            // textEmpresa
            // 
            this.textEmpresa.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.textEmpresa.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textEmpresa.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textEmpresa.Location = new System.Drawing.Point(141, 128);
            this.textEmpresa.MaxLength = 100;
            this.textEmpresa.Name = "textEmpresa";
            this.textEmpresa.ReadOnly = true;
            this.textEmpresa.Size = new System.Drawing.Size(609, 21);
            this.textEmpresa.TabIndex = 16;
            this.textEmpresa.TabStop = false;
            // 
            // textDataAtendimento
            // 
            this.textDataAtendimento.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.textDataAtendimento.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textDataAtendimento.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textDataAtendimento.Location = new System.Drawing.Point(141, 48);
            this.textDataAtendimento.MaxLength = 100;
            this.textDataAtendimento.Name = "textDataAtendimento";
            this.textDataAtendimento.ReadOnly = true;
            this.textDataAtendimento.Size = new System.Drawing.Size(609, 21);
            this.textDataAtendimento.TabIndex = 15;
            this.textDataAtendimento.TabStop = false;
            // 
            // textCpf
            // 
            this.textCpf.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.textCpf.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textCpf.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textCpf.Location = new System.Drawing.Point(141, 108);
            this.textCpf.MaxLength = 100;
            this.textCpf.Name = "textCpf";
            this.textCpf.ReadOnly = true;
            this.textCpf.Size = new System.Drawing.Size(609, 21);
            this.textCpf.TabIndex = 14;
            this.textCpf.TabStop = false;
            // 
            // textRg
            // 
            this.textRg.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.textRg.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textRg.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textRg.Location = new System.Drawing.Point(141, 88);
            this.textRg.MaxLength = 100;
            this.textRg.Name = "textRg";
            this.textRg.ReadOnly = true;
            this.textRg.Size = new System.Drawing.Size(609, 21);
            this.textRg.TabIndex = 13;
            this.textRg.TabStop = false;
            // 
            // textTipoAtendimento
            // 
            this.textTipoAtendimento.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.textTipoAtendimento.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textTipoAtendimento.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textTipoAtendimento.Location = new System.Drawing.Point(141, 28);
            this.textTipoAtendimento.MaxLength = 100;
            this.textTipoAtendimento.Name = "textTipoAtendimento";
            this.textTipoAtendimento.ReadOnly = true;
            this.textTipoAtendimento.Size = new System.Drawing.Size(609, 21);
            this.textTipoAtendimento.TabIndex = 12;
            this.textTipoAtendimento.TabStop = false;
            // 
            // textNomeColaborador
            // 
            this.textNomeColaborador.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.textNomeColaborador.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textNomeColaborador.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textNomeColaborador.Location = new System.Drawing.Point(141, 68);
            this.textNomeColaborador.MaxLength = 100;
            this.textNomeColaborador.Name = "textNomeColaborador";
            this.textNomeColaborador.ReadOnly = true;
            this.textNomeColaborador.Size = new System.Drawing.Size(609, 21);
            this.textNomeColaborador.TabIndex = 11;
            this.textNomeColaborador.TabStop = false;
            // 
            // textCodigoAtendimento
            // 
            this.textCodigoAtendimento.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.textCodigoAtendimento.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textCodigoAtendimento.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textCodigoAtendimento.Location = new System.Drawing.Point(141, 8);
            this.textCodigoAtendimento.Name = "textCodigoAtendimento";
            this.textCodigoAtendimento.ReadOnly = true;
            this.textCodigoAtendimento.Size = new System.Drawing.Size(609, 21);
            this.textCodigoAtendimento.TabIndex = 10;
            this.textCodigoAtendimento.TabStop = false;
            // 
            // grbAnexo
            // 
            this.grbAnexo.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.grbAnexo.Controls.Add(this.dgvArquivoAnexo);
            this.grbAnexo.Location = new System.Drawing.Point(7, 216);
            this.grbAnexo.Name = "grbAnexo";
            this.grbAnexo.Size = new System.Drawing.Size(743, 236);
            this.grbAnexo.TabIndex = 20;
            this.grbAnexo.TabStop = false;
            this.grbAnexo.Text = "Anexos";
            // 
            // dgvArquivoAnexo
            // 
            this.dgvArquivoAnexo.AllowUserToAddRows = false;
            this.dgvArquivoAnexo.AllowUserToDeleteRows = false;
            this.dgvArquivoAnexo.AllowUserToOrderColumns = true;
            this.dgvArquivoAnexo.AllowUserToResizeRows = false;
            dataGridViewCellStyle3.BackColor = System.Drawing.Color.Gainsboro;
            this.dgvArquivoAnexo.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle3;
            this.dgvArquivoAnexo.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.DisplayedCells;
            this.dgvArquivoAnexo.BackgroundColor = System.Drawing.Color.White;
            this.dgvArquivoAnexo.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle4.BackColor = System.Drawing.Color.LightGray;
            dataGridViewCellStyle4.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle4.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle4.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle4.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle4.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvArquivoAnexo.DefaultCellStyle = dataGridViewCellStyle4;
            this.dgvArquivoAnexo.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvArquivoAnexo.Location = new System.Drawing.Point(3, 16);
            this.dgvArquivoAnexo.MultiSelect = false;
            this.dgvArquivoAnexo.Name = "dgvArquivoAnexo";
            this.dgvArquivoAnexo.ReadOnly = true;
            this.dgvArquivoAnexo.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvArquivoAnexo.Size = new System.Drawing.Size(737, 217);
            this.dgvArquivoAnexo.TabIndex = 3;
            this.dgvArquivoAnexo.CellDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvArquivoAnexo_CellDoubleClick);
            // 
            // frmAtendimentoAnexo
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(784, 561);
            this.Name = "frmAtendimentoAnexo";
            this.Text = "GERENCIAR ANEXOS NO ATENDIMENTO";
            this.spForm.Panel1.ResumeLayout(false);
            this.spForm.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.spForm)).EndInit();
            this.spForm.ResumeLayout(false);
            this.pnlForm.ResumeLayout(false);
            this.pnlForm.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.banner)).EndInit();
            this.flpAcao.ResumeLayout(false);
            this.grbAnexo.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvArquivoAnexo)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.FlowLayoutPanel flpAcao;
        private System.Windows.Forms.Button btnIncluir;
        private System.Windows.Forms.Button btnExcluir;
        private System.Windows.Forms.Button btnFechar;
        private System.Windows.Forms.TextBox lblCodigoAtendimento;
        private System.Windows.Forms.TextBox lblTipoAtendimento;
        private System.Windows.Forms.TextBox lblDataAtendimento;
        private System.Windows.Forms.TextBox lblNomeColaborador;
        private System.Windows.Forms.TextBox lblRGColaborador;
        private System.Windows.Forms.TextBox lblCpfColaborador;
        private System.Windows.Forms.TextBox lblCliente;
        private System.Windows.Forms.TextBox lblCnpjCliente;
        private System.Windows.Forms.TextBox lblSetor;
        private System.Windows.Forms.TextBox lblFuncao;
        private System.Windows.Forms.TextBox textFuncao;
        private System.Windows.Forms.TextBox textSetor;
        private System.Windows.Forms.TextBox textCNPJ;
        private System.Windows.Forms.TextBox textEmpresa;
        private System.Windows.Forms.TextBox textDataAtendimento;
        private System.Windows.Forms.TextBox textCpf;
        private System.Windows.Forms.TextBox textRg;
        private System.Windows.Forms.TextBox textTipoAtendimento;
        private System.Windows.Forms.TextBox textNomeColaborador;
        private System.Windows.Forms.TextBox textCodigoAtendimento;
        private System.Windows.Forms.GroupBox grbAnexo;
        private System.Windows.Forms.DataGridView dgvArquivoAnexo;
    }
}