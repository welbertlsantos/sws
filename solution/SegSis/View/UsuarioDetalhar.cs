﻿using SWS.Entidade;
using SWS.Facade;
using SWS.Helper;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace SWS.View
{
    public partial class frmUsuarioDetalhar : frmUsuarioIncluir
    {
        public frmUsuarioDetalhar(Usuario usuario) : base()
        {
            InitializeComponent();

            this.usuario = usuario;

            textNome.Text = usuario.Nome;
            textCpf.Text = usuario.Cpf;
            textIdentidade.Text = usuario.Rg;
            textEmail.Text = usuario.Email;
            textLogin.Text = usuario.Login.ToLower();
            textLogin.ReadOnly = true;
            textNumeroRegistro.Text = usuario.DocumentoExtra;
            empresa = usuario.Empresa;
            if (empresa != null)
                textEmpresa.Text = usuario.Empresa.RazaoSocial;
            
            coordenador = usuario.Medico;
            textCoordenador.Text = usuario.Medico == null ? String.Empty : usuario.Medico.Nome;
            dataAdmissao.Checked = true;
            dataAdmissao.Text = usuario.DataAdmissao.ToString();
            funcaoInterna = usuario.FuncaoInterna;
            if (funcaoInterna != null)
                textFuncaoInterna.Text = usuario.FuncaoInterna.Descricao;
            
            cbDocumentacao.SelectedValue = usuario.ElaboraDocumento == true ? "true" : "false";
            cbExterno.SelectedValue = usuario.Externo == true ? "true" : "false";
            textMte.Text = usuario.Mte;

            /* preenchendo informações do arquivo */

            if (usuario.Assinatura != null)
            {
                assinatura = new Arquivo(null, String.Empty, usuario.Mimetype, 0, usuario.Assinatura);
                pbAssinatura.Image = FileHelper.byteArrayToImage(usuario.Assinatura);
            }

            montaPerfil();

            /* desabilitando controles */
            btnEmpresa.Enabled = false;
            btnMedico.Enabled = false;
            btnFuncaoInterna.Enabled = false;

            textNome.ReadOnly = true;
            textCpf.ReadOnly = true;
            textIdentidade.ReadOnly = true;
            textEmail.ReadOnly = true;
            textLogin.ReadOnly = true;
            textNumeroRegistro.ReadOnly = true;
            textDataAdmissao.Enabled = false;
            dataAdmissao.Enabled = false;
            this.cbDocumentacao.Enabled = false;
            textMte.Enabled = false;
            btnAssinatura.Enabled = false;
            btn_incluir_perfil.Enabled = false;
            btn_excluir_perfil.Enabled = false;
            bt_gravar.Enabled = false;
            btnLimpar.Enabled = false;
            this.cbExterno.Enabled = false;

            btIncluirCliente.Enabled = false;
            btExcluirCliente.Enabled = false;
            montaGridCliente();
            dgvCliente.Enabled = false;
            
        }

        protected override void montaPerfil()
        {
            try
            {
                PermissionamentoFacade permissionamentoFacade = PermissionamentoFacade.getInstance();

                dgvPerfil.Columns.Clear();
                dgvPerfil.ColumnCount = 4;

                dgvPerfil.Columns[0].HeaderText = "Id Perfil";
                dgvPerfil.Columns[0].Visible = false;

                dgvPerfil.Columns[1].HeaderText = "Id Usuario";
                dgvPerfil.Columns[1].Visible = false;

                dgvPerfil.Columns[2].HeaderText = "Perfil";

                dgvPerfil.Columns[3].HeaderText = "Data Início";
                dgvPerfil.Columns[3].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
                dgvPerfil.Columns[3].HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleCenter;

                DataSet ds = permissionamentoFacade.findAllPerfilByUsuario(usuario);

                foreach (DataRow row in ds.Tables[0].Rows)
                    dgvPerfil.Rows.Add(Convert.ToInt64(row["id_perfil"]), Convert.ToInt64(row["id_usuario"]), row["descricao"].ToString(), Convert.ToDateTime(row["data_inicio"]).ToShortDateString());

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        protected override void montaGridCliente()
        {
            try
            {
                dgvCliente.Columns.Clear();
                dgvCliente.ColumnCount = 4;

                dgvCliente.Columns[0].HeaderText = "id";
                dgvCliente.Columns[0].Visible = false;

                dgvCliente.Columns[1].HeaderText = "id_cliente";
                dgvCliente.Columns[1].Visible = false;

                dgvCliente.Columns[2].HeaderText = "Cliente";
                dgvCliente.Columns[2].AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCells;

                dgvCliente.Columns[3].HeaderText = "CNPJ";

                usuario.UsuarioCliente.ForEach(delegate(UsuarioCliente usuarioCliente)
                {
                    dgvCliente.Rows.Add(usuarioCliente.Id, usuarioCliente.Cliente.Id, usuarioCliente.Cliente.RazaoSocial, usuarioCliente.Cliente.Cnpj);
                });

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
    }
}
