﻿using SWS.Entidade;
using SWS.Facade;
using SWS.View.Resources;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace SWS.View
{
    public partial class frmPlanoAlterar : frmPlanoIncluir
    {
        public frmPlanoAlterar(Plano plano) :base()
        {
            InitializeComponent();
            this.Plano = plano;
            textDescricao.Text = plano.Descricao;
            formas = plano.Formas;
            parcelas = plano.Parcelas;
            montaDataGridParcelas();
            montaDataGridForma();
        }

        protected override void btnGravar_Click(object sender, EventArgs e)
        {
            try
            {
                if (ValidaCampos())
                {
                    FinanceiroFacade financeiroFacade = FinanceiroFacade.getInstance();

                    Plano = financeiroFacade.updatePlano(new Plano(Plano.Id, textDescricao.Text, ApplicationConstants.ATIVO, false));
                    MessageBox.Show("Plano alterado com sucesso.", "Confirmação", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    this.Close();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }

        protected override void btn_incluirParcela_Click(object sender, EventArgs e)
        {
            try
            {
                FinanceiroFacade financeiroFacade = FinanceiroFacade.getInstance();
                
                frmParcelaIncluir incluirParcela = new frmParcelaIncluir();
                incluirParcela.ShowDialog();

                if (incluirParcela.Parcela != null)
                {
                    /*verificando se não existe uma parcela cadastrada com esse dia. */
                    if (parcelas.Exists(x => x.Dias == incluirParcela.Parcela.Dias))
                        throw new Exception("Já existe essa parcela cadastrada.");

                    Parcela parcelaIncluir = incluirParcela.Parcela;
                    parcelaIncluir.Plano = Plano;

                    parcelas.Add(financeiroFacade.insertParcela(parcelaIncluir));
                    montaDataGridParcelas();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }

        protected override void btn_excluirParcela_Click(object sender, EventArgs e)
        {
            try
            {
                if (dgvParcela.CurrentRow == null)
                    throw new Exception("Selecione uma linha.");

                Parcela parcelaExcluir = parcelas.Find(x => x.Id == (long)dgvParcela.CurrentRow.Cells[0].Value);

                FinanceiroFacade financeiroFacade = FinanceiroFacade.getInstance();
                financeiroFacade.deleteParcela(parcelaExcluir);
                parcelas.RemoveAll(x => x.Id == (long)dgvParcela.CurrentRow.Cells[0].Value);
                montaDataGridParcelas();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }

        protected override void btn_incluirForma_Click(object sender, EventArgs e)
        {
            try
            {
                frmPesquisarForma selecionarForma = new frmPesquisarForma();
                selecionarForma.ShowDialog();

                if (selecionarForma.Formas.Count > 0)
                {
                    /* verificando se já existe alguma forma já incluída no plano */
                    foreach (DataGridViewRow dvRow in dgvForma.Rows)
                        if (selecionarForma.Formas.Exists(x => x.Id == (long)dvRow.Cells[1].Value))
                            throw new Exception("Já existe a forma de recebimento: " + dvRow.Cells[2].Value + " cadastrada para o plano. Refaça a sua pesquisa.");

                    FinanceiroFacade financeiroFacade = FinanceiroFacade.getInstance();

                    selecionarForma.Formas.ForEach(delegate(Forma forma)
                    {
                        formas.Add(financeiroFacade.insertPlanoForma(new PlanoForma(null, forma, Plano, ApplicationConstants.ATIVO)));
                    });

                    montaDataGridForma();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }

        protected override void btn_excluir_Click(object sender, EventArgs e)
        {
            try
            {
                if (dgvForma.CurrentRow == null)
                    throw new Exception("Selecione uma linha.");

                PlanoForma planoFormaExcluir = formas.Find(x => x.Id == (long)dgvForma.CurrentRow.Cells[0].Value);
                planoFormaExcluir.Situacao = ApplicationConstants.DESATIVADO;

                FinanceiroFacade financeiroFacade = FinanceiroFacade.getInstance();
                financeiroFacade.updatePlanoForma(planoFormaExcluir);
                formas.RemoveAll(x => x.Id == (long)dgvForma.CurrentRow.Cells[0].Value);
                montaDataGridForma();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }

        protected override void btnAlterar_Click(object sender, EventArgs e)
        {
            try
            {
                if (dgvParcela.CurrentRow == null)
                    throw new Exception("Selecione uma linha.");

                Parcela parcelaAlterar = parcelas.Find(x => x.Id == (long)dgvParcela.CurrentRow.Cells[0].Value);

                frmParcelaAlterar alterarParcela = new frmParcelaAlterar(parcelaAlterar);
                alterarParcela.ShowDialog();

                FinanceiroFacade financeiroFacade = FinanceiroFacade.getInstance();
                financeiroFacade.updateParcela(alterarParcela.Parcela);
                
                parcelas.RemoveAll(x => x.Id == parcelaAlterar.Id);

                parcelas.Add(alterarParcela.Parcela);
                montaDataGridParcelas();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

    }
}
