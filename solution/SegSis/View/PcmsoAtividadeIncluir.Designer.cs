﻿namespace SWS.View
{
    partial class frmPcmsoAtividadeIncluir
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.flpAcao = new System.Windows.Forms.FlowLayoutPanel();
            this.btnGravar = new System.Windows.Forms.Button();
            this.btnFechar = new System.Windows.Forms.Button();
            this.btnAtividadeExcluir = new System.Windows.Forms.Button();
            this.btnAtividadeIncluir = new System.Windows.Forms.Button();
            this.textAtividade = new System.Windows.Forms.TextBox();
            this.lblAtividade = new System.Windows.Forms.TextBox();
            this.lblRealizacao = new System.Windows.Forms.TextBox();
            this.cbMesRealizacao = new SWS.ComboBoxWithBorder();
            this.cbAnoRealizacao = new SWS.ComboBoxWithBorder();
            this.lblAlvo = new System.Windows.Forms.TextBox();
            this.textPublico = new System.Windows.Forms.TextBox();
            this.lblEvidencia = new System.Windows.Forms.TextBox();
            this.textRegistro = new System.Windows.Forms.TextBox();
            this.lblObrigatorioAtividade = new System.Windows.Forms.Label();
            this.lblObrigatorioRealizacao = new System.Windows.Forms.Label();
            this.lblObrigatorioPublico = new System.Windows.Forms.Label();
            this.lblObrigatorioRegistro = new System.Windows.Forms.Label();
            this.lblInformacaoObrigatorio = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.scForm)).BeginInit();
            this.scForm.Panel1.SuspendLayout();
            this.scForm.Panel2.SuspendLayout();
            this.scForm.SuspendLayout();
            this.pnlForm.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.banner)).BeginInit();
            this.flpAcao.SuspendLayout();
            this.SuspendLayout();
            // 
            // scForm
            // 
            // 
            // scForm.Panel1
            // 
            this.scForm.Panel1.Controls.Add(this.flpAcao);
            // 
            // pnlForm
            // 
            this.pnlForm.Controls.Add(this.lblInformacaoObrigatorio);
            this.pnlForm.Controls.Add(this.lblObrigatorioRegistro);
            this.pnlForm.Controls.Add(this.lblObrigatorioPublico);
            this.pnlForm.Controls.Add(this.lblObrigatorioAtividade);
            this.pnlForm.Controls.Add(this.textRegistro);
            this.pnlForm.Controls.Add(this.lblEvidencia);
            this.pnlForm.Controls.Add(this.textPublico);
            this.pnlForm.Controls.Add(this.lblAlvo);
            this.pnlForm.Controls.Add(this.cbAnoRealizacao);
            this.pnlForm.Controls.Add(this.cbMesRealizacao);
            this.pnlForm.Controls.Add(this.lblRealizacao);
            this.pnlForm.Controls.Add(this.btnAtividadeExcluir);
            this.pnlForm.Controls.Add(this.btnAtividadeIncluir);
            this.pnlForm.Controls.Add(this.textAtividade);
            this.pnlForm.Controls.Add(this.lblAtividade);
            this.pnlForm.Controls.Add(this.lblObrigatorioRealizacao);
            // 
            // flpAcao
            // 
            this.flpAcao.Controls.Add(this.btnGravar);
            this.flpAcao.Controls.Add(this.btnFechar);
            this.flpAcao.Dock = System.Windows.Forms.DockStyle.Fill;
            this.flpAcao.Location = new System.Drawing.Point(0, 0);
            this.flpAcao.Name = "flpAcao";
            this.flpAcao.Size = new System.Drawing.Size(534, 30);
            this.flpAcao.TabIndex = 0;
            // 
            // btnGravar
            // 
            this.btnGravar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnGravar.Image = global::SWS.Properties.Resources.Gravar;
            this.btnGravar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnGravar.Location = new System.Drawing.Point(3, 3);
            this.btnGravar.Name = "btnGravar";
            this.btnGravar.Size = new System.Drawing.Size(77, 23);
            this.btnGravar.TabIndex = 6;
            this.btnGravar.Text = "&Gravar";
            this.btnGravar.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnGravar.UseVisualStyleBackColor = true;
            this.btnGravar.Click += new System.EventHandler(this.btnGravar_Click);
            // 
            // btnFechar
            // 
            this.btnFechar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnFechar.Image = global::SWS.Properties.Resources.close;
            this.btnFechar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnFechar.Location = new System.Drawing.Point(86, 3);
            this.btnFechar.Name = "btnFechar";
            this.btnFechar.Size = new System.Drawing.Size(75, 23);
            this.btnFechar.TabIndex = 14;
            this.btnFechar.TabStop = false;
            this.btnFechar.Text = "&Fechar";
            this.btnFechar.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnFechar.UseVisualStyleBackColor = true;
            this.btnFechar.Click += new System.EventHandler(this.btnFechar_Click);
            // 
            // btnAtividadeExcluir
            // 
            this.btnAtividadeExcluir.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnAtividadeExcluir.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnAtividadeExcluir.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAtividadeExcluir.Image = global::SWS.Properties.Resources.close;
            this.btnAtividadeExcluir.Location = new System.Drawing.Point(454, 3);
            this.btnAtividadeExcluir.Name = "btnAtividadeExcluir";
            this.btnAtividadeExcluir.Size = new System.Drawing.Size(34, 21);
            this.btnAtividadeExcluir.TabIndex = 74;
            this.btnAtividadeExcluir.TabStop = false;
            this.btnAtividadeExcluir.UseVisualStyleBackColor = true;
            this.btnAtividadeExcluir.Click += new System.EventHandler(this.btnAtividadeExcluir_Click);
            // 
            // btnAtividadeIncluir
            // 
            this.btnAtividadeIncluir.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnAtividadeIncluir.Image = global::SWS.Properties.Resources.busca;
            this.btnAtividadeIncluir.Location = new System.Drawing.Point(421, 3);
            this.btnAtividadeIncluir.Name = "btnAtividadeIncluir";
            this.btnAtividadeIncluir.Size = new System.Drawing.Size(34, 21);
            this.btnAtividadeIncluir.TabIndex = 1;
            this.btnAtividadeIncluir.UseVisualStyleBackColor = true;
            this.btnAtividadeIncluir.Click += new System.EventHandler(this.btnAtividadeIncluir_Click);
            // 
            // textAtividade
            // 
            this.textAtividade.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.textAtividade.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textAtividade.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.textAtividade.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textAtividade.Location = new System.Drawing.Point(139, 3);
            this.textAtividade.MaxLength = 100;
            this.textAtividade.Name = "textAtividade";
            this.textAtividade.ReadOnly = true;
            this.textAtividade.Size = new System.Drawing.Size(283, 21);
            this.textAtividade.TabIndex = 72;
            this.textAtividade.TabStop = false;
            // 
            // lblAtividade
            // 
            this.lblAtividade.BackColor = System.Drawing.Color.Silver;
            this.lblAtividade.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblAtividade.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblAtividade.Location = new System.Drawing.Point(3, 3);
            this.lblAtividade.MaxLength = 100;
            this.lblAtividade.Name = "lblAtividade";
            this.lblAtividade.ReadOnly = true;
            this.lblAtividade.Size = new System.Drawing.Size(137, 21);
            this.lblAtividade.TabIndex = 73;
            this.lblAtividade.TabStop = false;
            this.lblAtividade.Text = "Atividade";
            // 
            // lblRealizacao
            // 
            this.lblRealizacao.BackColor = System.Drawing.Color.Silver;
            this.lblRealizacao.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblRealizacao.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblRealizacao.Location = new System.Drawing.Point(3, 23);
            this.lblRealizacao.MaxLength = 100;
            this.lblRealizacao.Name = "lblRealizacao";
            this.lblRealizacao.ReadOnly = true;
            this.lblRealizacao.Size = new System.Drawing.Size(137, 21);
            this.lblRealizacao.TabIndex = 75;
            this.lblRealizacao.TabStop = false;
            this.lblRealizacao.Text = "Realizacao";
            // 
            // cbMesRealizacao
            // 
            this.cbMesRealizacao.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.cbMesRealizacao.BackColor = System.Drawing.Color.White;
            this.cbMesRealizacao.BorderColor = System.Drawing.Color.DimGray;
            this.cbMesRealizacao.BorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid;
            this.cbMesRealizacao.FormattingEnabled = true;
            this.cbMesRealizacao.Location = new System.Drawing.Point(139, 23);
            this.cbMesRealizacao.Name = "cbMesRealizacao";
            this.cbMesRealizacao.Size = new System.Drawing.Size(126, 21);
            this.cbMesRealizacao.TabIndex = 2;
            // 
            // cbAnoRealizacao
            // 
            this.cbAnoRealizacao.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.cbAnoRealizacao.BackColor = System.Drawing.Color.White;
            this.cbAnoRealizacao.BorderColor = System.Drawing.Color.DimGray;
            this.cbAnoRealizacao.BorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid;
            this.cbAnoRealizacao.FormattingEnabled = true;
            this.cbAnoRealizacao.Location = new System.Drawing.Point(264, 23);
            this.cbAnoRealizacao.Name = "cbAnoRealizacao";
            this.cbAnoRealizacao.Size = new System.Drawing.Size(82, 21);
            this.cbAnoRealizacao.TabIndex = 3;
            // 
            // lblAlvo
            // 
            this.lblAlvo.BackColor = System.Drawing.Color.Silver;
            this.lblAlvo.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblAlvo.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblAlvo.Location = new System.Drawing.Point(3, 43);
            this.lblAlvo.MaxLength = 100;
            this.lblAlvo.Name = "lblAlvo";
            this.lblAlvo.ReadOnly = true;
            this.lblAlvo.Size = new System.Drawing.Size(137, 21);
            this.lblAlvo.TabIndex = 86;
            this.lblAlvo.TabStop = false;
            this.lblAlvo.Text = "Público Alvo";
            // 
            // textPublico
            // 
            this.textPublico.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textPublico.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.textPublico.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textPublico.Location = new System.Drawing.Point(139, 43);
            this.textPublico.MaxLength = 250;
            this.textPublico.Name = "textPublico";
            this.textPublico.Size = new System.Drawing.Size(349, 21);
            this.textPublico.TabIndex = 4;
            // 
            // lblEvidencia
            // 
            this.lblEvidencia.BackColor = System.Drawing.Color.Silver;
            this.lblEvidencia.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblEvidencia.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblEvidencia.Location = new System.Drawing.Point(3, 63);
            this.lblEvidencia.MaxLength = 100;
            this.lblEvidencia.Name = "lblEvidencia";
            this.lblEvidencia.ReadOnly = true;
            this.lblEvidencia.Size = new System.Drawing.Size(137, 21);
            this.lblEvidencia.TabIndex = 88;
            this.lblEvidencia.TabStop = false;
            this.lblEvidencia.Text = "Registro";
            // 
            // textRegistro
            // 
            this.textRegistro.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textRegistro.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.textRegistro.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textRegistro.Location = new System.Drawing.Point(139, 63);
            this.textRegistro.MaxLength = 250;
            this.textRegistro.Name = "textRegistro";
            this.textRegistro.Size = new System.Drawing.Size(349, 21);
            this.textRegistro.TabIndex = 5;
            // 
            // lblObrigatorioAtividade
            // 
            this.lblObrigatorioAtividade.AutoSize = true;
            this.lblObrigatorioAtividade.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblObrigatorioAtividade.ForeColor = System.Drawing.Color.Red;
            this.lblObrigatorioAtividade.Location = new System.Drawing.Point(494, 4);
            this.lblObrigatorioAtividade.Name = "lblObrigatorioAtividade";
            this.lblObrigatorioAtividade.Size = new System.Drawing.Size(15, 20);
            this.lblObrigatorioAtividade.TabIndex = 93;
            this.lblObrigatorioAtividade.Text = "*";
            // 
            // lblObrigatorioRealizacao
            // 
            this.lblObrigatorioRealizacao.AutoSize = true;
            this.lblObrigatorioRealizacao.BackColor = System.Drawing.Color.Transparent;
            this.lblObrigatorioRealizacao.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblObrigatorioRealizacao.ForeColor = System.Drawing.Color.Red;
            this.lblObrigatorioRealizacao.Location = new System.Drawing.Point(351, 26);
            this.lblObrigatorioRealizacao.Name = "lblObrigatorioRealizacao";
            this.lblObrigatorioRealizacao.Size = new System.Drawing.Size(15, 20);
            this.lblObrigatorioRealizacao.TabIndex = 94;
            this.lblObrigatorioRealizacao.Text = "*";
            // 
            // lblObrigatorioPublico
            // 
            this.lblObrigatorioPublico.AutoSize = true;
            this.lblObrigatorioPublico.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblObrigatorioPublico.ForeColor = System.Drawing.Color.Red;
            this.lblObrigatorioPublico.Location = new System.Drawing.Point(494, 44);
            this.lblObrigatorioPublico.Name = "lblObrigatorioPublico";
            this.lblObrigatorioPublico.Size = new System.Drawing.Size(15, 20);
            this.lblObrigatorioPublico.TabIndex = 95;
            this.lblObrigatorioPublico.Text = "*";
            // 
            // lblObrigatorioRegistro
            // 
            this.lblObrigatorioRegistro.AutoSize = true;
            this.lblObrigatorioRegistro.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblObrigatorioRegistro.ForeColor = System.Drawing.Color.Red;
            this.lblObrigatorioRegistro.Location = new System.Drawing.Point(494, 64);
            this.lblObrigatorioRegistro.Name = "lblObrigatorioRegistro";
            this.lblObrigatorioRegistro.Size = new System.Drawing.Size(15, 20);
            this.lblObrigatorioRegistro.TabIndex = 96;
            this.lblObrigatorioRegistro.Text = "*";
            // 
            // lblInformacaoObrigatorio
            // 
            this.lblInformacaoObrigatorio.AutoSize = true;
            this.lblInformacaoObrigatorio.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblInformacaoObrigatorio.ForeColor = System.Drawing.Color.Blue;
            this.lblInformacaoObrigatorio.Location = new System.Drawing.Point(3, 96);
            this.lblInformacaoObrigatorio.Name = "lblInformacaoObrigatorio";
            this.lblInformacaoObrigatorio.Size = new System.Drawing.Size(252, 15);
            this.lblInformacaoObrigatorio.TabIndex = 97;
            this.lblInformacaoObrigatorio.Text = "Campos marcados com [ * ] são obrigatórios.";
            // 
            // frmPcmsoAtividadeIncluir
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(534, 361);
            this.Name = "frmPcmsoAtividadeIncluir";
            this.Text = "INCLUIR ATIVIDADES";
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.frmPcmsoAtividadeIncluir_KeyDown);
            this.scForm.Panel1.ResumeLayout(false);
            this.scForm.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.scForm)).EndInit();
            this.scForm.ResumeLayout(false);
            this.pnlForm.ResumeLayout(false);
            this.pnlForm.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.banner)).EndInit();
            this.flpAcao.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        protected System.Windows.Forms.FlowLayoutPanel flpAcao;
        protected System.Windows.Forms.Button btnGravar;
        protected System.Windows.Forms.Button btnFechar;
        protected System.Windows.Forms.Button btnAtividadeExcluir;
        protected System.Windows.Forms.Button btnAtividadeIncluir;
        protected System.Windows.Forms.TextBox textAtividade;
        protected System.Windows.Forms.TextBox lblAtividade;
        protected System.Windows.Forms.TextBox lblRealizacao;
        protected ComboBoxWithBorder cbAnoRealizacao;
        protected ComboBoxWithBorder cbMesRealizacao;
        protected System.Windows.Forms.TextBox lblAlvo;
        protected System.Windows.Forms.TextBox textRegistro;
        protected System.Windows.Forms.TextBox lblEvidencia;
        protected System.Windows.Forms.TextBox textPublico;
        protected System.Windows.Forms.Label lblObrigatorioRegistro;
        protected System.Windows.Forms.Label lblObrigatorioPublico;
        protected System.Windows.Forms.Label lblObrigatorioRealizacao;
        protected System.Windows.Forms.Label lblObrigatorioAtividade;
        protected System.Windows.Forms.Label lblInformacaoObrigatorio;
    }
}