﻿using SWS.Entidade;
using SWS.Excecao;
using SWS.Facade;
using SWS.Helper;
using SWS.View.ViewHelper;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Security;
using System.Text;
using System.Windows.Forms;

namespace SWS.View
{
    public partial class frmAtendimentoAnexo : frmTemplate
    {

        private Aso aso;
        
        public frmAtendimentoAnexo(Aso aso)
        {
            this.aso = aso;
            InitializeComponent();
            montaDadosTela();
            montaGrid();
            validaPermissoes();
        }

        private void btnIncluir_Click(object sender, EventArgs e)
        {
            OpenFileDialog clienteFileDialog = new OpenFileDialog();

            clienteFileDialog.Multiselect = false;
            clienteFileDialog.Title = "Selecionar Anexo";
            clienteFileDialog.InitialDirectory = @"C:\";
            //filtra para exibir somente arquivos de imagens
            //clienteLogoFileDialog.Filter = "Images(*.JPG) |*.JPG";
            clienteFileDialog.CheckFileExists = true;
            clienteFileDialog.CheckPathExists = true;
            clienteFileDialog.FilterIndex = 2;
            clienteFileDialog.RestoreDirectory = true;
            clienteFileDialog.ReadOnlyChecked = true;
            clienteFileDialog.ShowReadOnly = true;

            DialogResult dr = clienteFileDialog.ShowDialog();

            if (dr == System.Windows.Forms.DialogResult.OK)
            {
                String nomeArquivo = clienteFileDialog.FileName;
                String nomeArquivoFinal = nomeArquivo.Substring(nomeArquivo.LastIndexOf('\\') + 1);

                try
                {
                    this.Cursor = Cursors.WaitCursor;

                    AsoFacade asoFacade = AsoFacade.getInstance();

                    byte[] conteudo = System.IO.File.ReadAllBytes(nomeArquivo);

                    if (conteudo.Length > 5242880)
                        throw new ArquivoException(ArquivoException.msg1);

                    ArquivoAnexo arquivoAnexo = new ArquivoAnexo(null, nomeArquivoFinal, Path.GetExtension(clienteFileDialog.FileName), conteudo.Length, conteudo, aso);

                    //salvar arquivo
                    asoFacade.insertArquivoAnexo(arquivoAnexo);

                    this.montaGrid();
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
        }

        private void btnExcluir_Click(object sender, EventArgs e)
        {
            try
            {
                if (dgvArquivoAnexo.CurrentRow == null)
                    throw new Exception("Selecione uma linha.");
                
                if (MessageBox.Show("Deseja excluir o arquivo selecionado?", "Atenção", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                {
 
                    AsoFacade asoFacade = AsoFacade.getInstance();
                    
                    this.Cursor = Cursors.WaitCursor;
                    ArquivoAnexo arquivoAnexo = asoFacade.findArquivoAnexoById((long)dgvArquivoAnexo.CurrentRow.Cells[0].Value);
                    asoFacade.deleteArquivoAnexo(arquivoAnexo);
                    MessageBox.Show("Arquivo excluido com sucesso.", "Confirmação", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    montaGrid();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btnFechar_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void montaDadosTela()
        {
            try
            {
                AsoFacade asoFacade = AsoFacade.getInstance();

                textCodigoAtendimento.Text = ValidaCampoHelper.RetornaCodigoAtendimentoFormatado(aso.Codigo);
                textTipoAtendimento.Text = aso.Periodicidade.Descricao;
                textNomeColaborador.Text = aso.ClienteFuncaoFuncionario.Funcionario.Nome;
                textRg.Text = aso.ClienteFuncaoFuncionario.Funcionario.Rg;
                textCpf.Text = ValidaCampoHelper.FormataCpf(aso.ClienteFuncaoFuncionario.Funcionario.Cpf);
                textEmpresa.Text = aso.Cliente.RazaoSocial;
                textCNPJ.Text = !aso.ClienteFuncaoFuncionario.ClienteFuncao.Cliente.Fisica ? ValidaCampoHelper.FormataCnpj(aso.Cliente.Cnpj) : ValidaCampoHelper.FormataCpf(aso.Cliente.Cnpj);
                textSetor.Text = asoFacade.findDescricaoSetoresByAso(aso).Count == 0 ? String.Empty : asoFacade.findDescricaoSetoresByAso(aso).ElementAt(0);
                textFuncao.Text = aso.ClienteFuncaoFuncionario.ClienteFuncao.Funcao.Descricao;
                textDataAtendimento.Text = String.Format("{0:dd/MM/yyyy HH:mm:ss}", aso.DataGravacao);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void validaPermissoes()
        {
            PermissionamentoFacade permissionamentoFacade = PermissionamentoFacade.getInstance();

            this.btnIncluir.Enabled = permissionamentoFacade.hasPermission("ANEXO", "INCLUIR");
            this.btnExcluir.Enabled = permissionamentoFacade.hasPermission("ANEXO", "EXCLUIR");

        }

        public void montaGrid()
        {
            try
            {
                this.Cursor = Cursors.WaitCursor;

                AsoFacade asoFacade = AsoFacade.getInstance();

                DataSet ds = asoFacade.findArquivoAnexoByAso(aso);

                dgvArquivoAnexo.DataSource = ds.Tables["Anexos"].DefaultView;

                dgvArquivoAnexo.Columns[0].HeaderText = "id";
                dgvArquivoAnexo.Columns[0].Visible = false;

                dgvArquivoAnexo.Columns[1].HeaderText = "Descrição";

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }

        private void dgvArquivoAnexo_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                AsoFacade asoFacade = AsoFacade.getInstance();

                ArquivoAnexo arquivoAnexo = asoFacade.findArquivoAnexoById((long)dgvArquivoAnexo.Rows[e.RowIndex].Cells[0].Value);
                FileHelper.OpenInAnotherApp(arquivoAnexo.Conteudo, arquivoAnexo.Descricao);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
    }
}
