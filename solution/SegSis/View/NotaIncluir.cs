﻿using SWS.Entidade;
using SWS.Facade;
using SWS.View.Resources;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace SWS.View
{
    public partial class frmNotaIncluir : frmTemplate
    {

        private Nota nota;

        public Nota Nota
        {
            get { return nota; }
            set { nota = value; }
        }
        
        public frmNotaIncluir()
        {
            InitializeComponent();
            ActiveControl = textTitulo;
        }

        protected virtual void btnGravar_Click(object sender, EventArgs e)
        {
            try
            {
                if (validaCamposObrigatorio())
                {
                    EstudoFacade pcmsoFacade = EstudoFacade.getInstance();
                    nota = pcmsoFacade.insertNota(new Nota(null, textTitulo.Text, ApplicationConstants.ATIVO, textConteudo.Text));
                    MessageBox.Show("Nota incluída com sucesso.", "Confirmação", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    this.Close();

                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }

        protected void btnFechar_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        protected bool validaCamposObrigatorio()
        {
            bool retorno = true;

            try
            {
                if (string.IsNullOrEmpty(textTitulo.Text))
                    throw new Exception(" O título é obrigatório!");

                if (string.IsNullOrEmpty(textConteudo.Text))
                    throw new Exception(" O conteúdo é obrigatório!");
            }
            catch (Exception ex)
            {
                retorno = false;
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            
            return retorno;
        }

        protected void frmNotaIncluir_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter || e.KeyCode == Keys.Return)
                SendKeys.Send("{TAB}");
        }
    }
}
