﻿using SWS.Entidade;
using SWS.Facade;
using SWS.View.Resources;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace SWS.View
{
    public partial class frmExamesExtrasClienteFuncao : frmTemplateConsulta
    {
        private Cliente cliente;
        private ClienteFuncao clienteFuncao;

        public ClienteFuncao ClienteFuncao
        {
            get { return clienteFuncao; }
            set { clienteFuncao = value; }
        }
        
        public frmExamesExtrasClienteFuncao(Cliente cliente)
        {
            InitializeComponent();
            this.cliente = cliente;
            montaGridFuncoes();
        }

        private void montaGridFuncoes()
        {
            try
            {
                if (cliente == null)
                    throw new Exception("Necessário ter um cliente para pesquisa.");
                
                this.Cursor = Cursors.WaitCursor;

                dgvFuncao.Columns.Clear();

                EstudoFacade pcmsoFacade = EstudoFacade.getInstance();

                DataSet ds = pcmsoFacade.findAllClienteFuncao(cliente);

                dgvFuncao.DataSource = ds.Tables[0].DefaultView;

                dgvFuncao.Columns[0].HeaderText = "id_seg_cliente_funcao";
                dgvFuncao.Columns[0].Visible = false;

                dgvFuncao.Columns[1].HeaderText = "id_cliente";
                dgvFuncao.Columns[1].Visible = false;

                dgvFuncao.Columns[2].HeaderText = "id_funcao";
                dgvFuncao.Columns[2].Visible = false;

                dgvFuncao.Columns[3].HeaderText = "Função";

                dgvFuncao.Columns[4].HeaderText = "Código CBO";


            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }

        private void btConfirmar_Click(object sender, EventArgs e)
        {
            try
            {
                if (dgvFuncao.CurrentRow == null)
                    throw new Exception("Selecione uma função");
                
                EstudoFacade pcmsoFacade = EstudoFacade.getInstance();
                
                clienteFuncao = pcmsoFacade.findClienteFuncaoById((long)dgvFuncao.CurrentRow.Cells[0].Value);

                this.Close();

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btFechar_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btIncluirFuncao_Click(object sender, EventArgs e)
        {
            try
            {
                frmFuncaoPesquisa incluirFuncao = new frmFuncaoPesquisa();
                incluirFuncao.ShowDialog();

                if (incluirFuncao.Funcoes.Count > 0)
                {
                    /*verificando se não existe nenhuma função selecionada já cadastrada */

                    foreach (DataGridViewRow dvRow in dgvFuncao.Rows)
                        if (incluirFuncao.Funcoes.Exists(x => x.Id == (long)dvRow.Cells[2].Value))
                            throw new Exception("Já existe a função: " + dvRow.Cells[3].Value + " cadastrada no cliente. Refaça a seleção.");
                    
                    EstudoFacade pcmsoFacade = EstudoFacade.getInstance();

                    incluirFuncao.Funcoes.ForEach(delegate(Funcao funcao)
                    {
                        pcmsoFacade.insertClienteFuncao(new ClienteFuncao(null, cliente, funcao, ApplicationConstants.ATIVO, DateTime.Now, null));
                    });
                    
                    montaGridFuncoes();
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void dgvFuncao_CellMouseDoubleClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            try
            {
                btConfirmar.PerformClick();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }
    }
}
