﻿using SWS.Entidade;
using SWS.Facade;
using SWS.View.Resources;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SWS.View
{
    public partial class frmPcmsoMaterial : frmTemplateConsulta
    {
        Estudo pcmso;
        
        
        public frmPcmsoMaterial(Estudo pcmso)
        {
            InitializeComponent();
            this.pcmso = pcmso;
            montaGridMaterial();
        }

        private void btnFechar_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnGravar_Click(object sender, EventArgs e)
        {
            try
            {
                bool flagSelecao = false;
                EstudoFacade pcmsoFacade = EstudoFacade.getInstance();
                List<MaterialHospitalarEstudo> materiaisInEstudo = new List<MaterialHospitalarEstudo>();
                materiaisInEstudo = pcmsoFacade.findAllMateriaHospitalarByEstudo(pcmso).ToList();
                List<MaterialHospitalar> materiaisJaIncluido = new List<MaterialHospitalar>();

                foreach (DataGridViewRow row in dgvMateriais.Rows)
                {
                    if (Convert.ToBoolean(row.Cells[3].Value))
                    {
                        flagSelecao = true;
                        MaterialHospitalarEstudo materialEstudo = new MaterialHospitalarEstudo();
                        MaterialHospitalar material = new MaterialHospitalar();
                        material.Id = (long)row.Cells[0].Value;
                        material.Descricao = row.Cells[1].Value.ToString();

                        /* verificando para saber se não existe o material já incluído no estudo */

                        if (materiaisInEstudo.Exists(x => x.MaterialHospitalar.Id == material.Id))
                        {
                            materiaisJaIncluido.Add(material);
                            continue;
                        }

                        materialEstudo.Estudo = pcmso;
                        materialEstudo.MaterialHospitalar = material;
                        materialEstudo.Quantidade = 1;
                        pcmsoFacade.insertMaterialHospitalarEstudo(materialEstudo);
                    }
                }

                if (!flagSelecao)
                    throw new Exception("Você deve selecionar ao menos um material.");

                StringBuilder query = new StringBuilder();
                int i = 1;
                if (materiaisJaIncluido.Count > 0)
                {
                    foreach (MaterialHospitalar materialHospitalar in materiaisJaIncluido)
                    {
                        query.Append(materialHospitalar.Descricao);
                        if (i < materiaisJaIncluido.Count)
                        {
                            query.Append(", ");
                        }
                        else
                        {
                            query.Append(".");
                        }
                    }

                    MessageBox.Show("Os seguintes materiais já estão incluídos no estudo:" + System.Environment.NewLine + query.ToString(),
                        "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
                this.Close();

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }

        private void montaGridMaterial()
        {
            try
            {
                dgvMateriais.Columns.Clear();

                dgvMateriais.ColumnCount = 3;

                EstudoFacade pcmsoFacade = EstudoFacade.getInstance();

                MaterialHospitalar materiais = new MaterialHospitalar();
                materiais.Situacao = ApplicationConstants.ATIVO;
                materiais.Descricao = string.Empty;
                materiais.Unidade = string.Empty;
                
                DataSet ds = pcmsoFacade.findMaterilHospitalarByFilter(materiais);

                dgvMateriais.Columns[0].HeaderText = "IdMaterial";
                dgvMateriais.Columns[0].Visible = false;
                dgvMateriais.Columns[1].HeaderText = "Material";
                dgvMateriais.Columns[1].ReadOnly = true;
                dgvMateriais.Columns[2].HeaderText = "Unidade";
                dgvMateriais.Columns[2].ReadOnly = true;

                DataGridViewCheckBoxColumn selec = new DataGridViewCheckBoxColumn();
                selec.Name = "Selec";
                dgvMateriais.Columns.Add(selec);
                dgvMateriais.Columns[3].DisplayIndex = 0;

                foreach (DataRow row in ds.Tables[0].Rows)
                    dgvMateriais.Rows.Add(
                        row["ID_MATERIAL_HOSPITALAR"],
                        row["DESCRICAO"].ToString(),
                        row["UNIDADE"].ToString(),
                        true
                        );
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }

        private void btnNovo_Click(object sender, EventArgs e)
        {
            frmMaterialHospitalarIncluir incluirMaterial = new frmMaterialHospitalarIncluir();
            incluirMaterial.ShowDialog();

            if (incluirMaterial.MateriaHospitalar != null)
                montaGridMaterial();
        }
    }
}
