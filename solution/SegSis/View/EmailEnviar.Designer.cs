﻿namespace SWS.View
{
    partial class frmEmailEnviar
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.flpAcao = new System.Windows.Forms.FlowLayoutPanel();
            this.btnEnviar = new System.Windows.Forms.Button();
            this.btnAnexo = new System.Windows.Forms.Button();
            this.btnCancelar = new System.Windows.Forms.Button();
            this.lblPara = new System.Windows.Forms.TextBox();
            this.lblAssunto = new System.Windows.Forms.TextBox();
            this.lblPrioridade = new System.Windows.Forms.TextBox();
            this.textAssunto = new System.Windows.Forms.TextBox();
            this.textEmail = new System.Windows.Forms.TextBox();
            this.cbPrioridade = new SWS.ComboBoxWithBorder();
            this.grbMensagem = new System.Windows.Forms.GroupBox();
            this.textMensagem = new System.Windows.Forms.TextBox();
            this.lblListaAnexo = new System.Windows.Forms.Label();
            this.ofdEmail = new System.Windows.Forms.OpenFileDialog();
            this.tp = new System.Windows.Forms.ToolTip(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.scForm)).BeginInit();
            this.scForm.Panel1.SuspendLayout();
            this.scForm.Panel2.SuspendLayout();
            this.scForm.SuspendLayout();
            this.pnlForm.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.banner)).BeginInit();
            this.flpAcao.SuspendLayout();
            this.grbMensagem.SuspendLayout();
            this.SuspendLayout();
            // 
            // scForm
            // 
            // 
            // scForm.Panel1
            // 
            this.scForm.Panel1.Controls.Add(this.flpAcao);
            // 
            // pnlForm
            // 
            this.pnlForm.Controls.Add(this.lblListaAnexo);
            this.pnlForm.Controls.Add(this.grbMensagem);
            this.pnlForm.Controls.Add(this.cbPrioridade);
            this.pnlForm.Controls.Add(this.textAssunto);
            this.pnlForm.Controls.Add(this.textEmail);
            this.pnlForm.Controls.Add(this.lblPrioridade);
            this.pnlForm.Controls.Add(this.lblAssunto);
            this.pnlForm.Controls.Add(this.lblPara);
            // 
            // flpAcao
            // 
            this.flpAcao.Controls.Add(this.btnEnviar);
            this.flpAcao.Controls.Add(this.btnAnexo);
            this.flpAcao.Controls.Add(this.btnCancelar);
            this.flpAcao.Dock = System.Windows.Forms.DockStyle.Fill;
            this.flpAcao.Location = new System.Drawing.Point(0, 0);
            this.flpAcao.Name = "flpAcao";
            this.flpAcao.Size = new System.Drawing.Size(534, 30);
            this.flpAcao.TabIndex = 0;
            // 
            // btnEnviar
            // 
            this.btnEnviar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnEnviar.Image = global::SWS.Properties.Resources.enviar_mensagem_318_10100;
            this.btnEnviar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnEnviar.Location = new System.Drawing.Point(3, 3);
            this.btnEnviar.Name = "btnEnviar";
            this.btnEnviar.Size = new System.Drawing.Size(75, 23);
            this.btnEnviar.TabIndex = 7;
            this.btnEnviar.TabStop = false;
            this.btnEnviar.Text = "&Enviar";
            this.btnEnviar.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnEnviar.UseVisualStyleBackColor = true;
            this.btnEnviar.Click += new System.EventHandler(this.btnEnviar_Click);
            // 
            // btnAnexo
            // 
            this.btnAnexo.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnAnexo.Image = global::SWS.Properties.Resources.clip;
            this.btnAnexo.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnAnexo.Location = new System.Drawing.Point(84, 3);
            this.btnAnexo.Name = "btnAnexo";
            this.btnAnexo.Size = new System.Drawing.Size(75, 23);
            this.btnAnexo.TabIndex = 8;
            this.btnAnexo.TabStop = false;
            this.btnAnexo.Text = "&Anexo";
            this.btnAnexo.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnAnexo.UseVisualStyleBackColor = true;
            this.btnAnexo.Click += new System.EventHandler(this.btnAnexo_Click);
            // 
            // btnCancelar
            // 
            this.btnCancelar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnCancelar.Image = global::SWS.Properties.Resources.close;
            this.btnCancelar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnCancelar.Location = new System.Drawing.Point(165, 3);
            this.btnCancelar.Name = "btnCancelar";
            this.btnCancelar.Size = new System.Drawing.Size(75, 23);
            this.btnCancelar.TabIndex = 9;
            this.btnCancelar.TabStop = false;
            this.btnCancelar.Text = "&Cancelar";
            this.btnCancelar.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnCancelar.UseVisualStyleBackColor = true;
            this.btnCancelar.Click += new System.EventHandler(this.btnCancelar_Click);
            // 
            // lblPara
            // 
            this.lblPara.BackColor = System.Drawing.Color.LightGray;
            this.lblPara.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblPara.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPara.Location = new System.Drawing.Point(7, 8);
            this.lblPara.Name = "lblPara";
            this.lblPara.ReadOnly = true;
            this.lblPara.Size = new System.Drawing.Size(100, 21);
            this.lblPara.TabIndex = 0;
            this.lblPara.TabStop = false;
            this.lblPara.Text = "Para";
            // 
            // lblAssunto
            // 
            this.lblAssunto.BackColor = System.Drawing.Color.LightGray;
            this.lblAssunto.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblAssunto.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblAssunto.Location = new System.Drawing.Point(7, 28);
            this.lblAssunto.Name = "lblAssunto";
            this.lblAssunto.ReadOnly = true;
            this.lblAssunto.Size = new System.Drawing.Size(100, 21);
            this.lblAssunto.TabIndex = 1;
            this.lblAssunto.TabStop = false;
            this.lblAssunto.Text = "Assunto";
            // 
            // lblPrioridade
            // 
            this.lblPrioridade.BackColor = System.Drawing.Color.LightGray;
            this.lblPrioridade.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblPrioridade.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPrioridade.Location = new System.Drawing.Point(7, 48);
            this.lblPrioridade.Name = "lblPrioridade";
            this.lblPrioridade.ReadOnly = true;
            this.lblPrioridade.Size = new System.Drawing.Size(100, 21);
            this.lblPrioridade.TabIndex = 2;
            this.lblPrioridade.TabStop = false;
            this.lblPrioridade.Text = "Prioridade";
            // 
            // textAssunto
            // 
            this.textAssunto.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textAssunto.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textAssunto.Location = new System.Drawing.Point(106, 28);
            this.textAssunto.Name = "textAssunto";
            this.textAssunto.Size = new System.Drawing.Size(400, 21);
            this.textAssunto.TabIndex = 2;
            // 
            // textEmail
            // 
            this.textEmail.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textEmail.CharacterCasing = System.Windows.Forms.CharacterCasing.Lower;
            this.textEmail.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textEmail.ForeColor = System.Drawing.Color.Blue;
            this.textEmail.Location = new System.Drawing.Point(106, 8);
            this.textEmail.MaxLength = 100;
            this.textEmail.Name = "textEmail";
            this.textEmail.Size = new System.Drawing.Size(400, 21);
            this.textEmail.TabIndex = 1;
            this.tp.SetToolTip(this.textEmail, "Para enviar emails para mais um email os e-mails");
            // 
            // cbPrioridade
            // 
            this.cbPrioridade.BackColor = System.Drawing.Color.LightGray;
            this.cbPrioridade.BorderColor = System.Drawing.Color.DimGray;
            this.cbPrioridade.BorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid;
            this.cbPrioridade.FormattingEnabled = true;
            this.cbPrioridade.Location = new System.Drawing.Point(106, 48);
            this.cbPrioridade.Name = "cbPrioridade";
            this.cbPrioridade.Size = new System.Drawing.Size(400, 21);
            this.cbPrioridade.TabIndex = 3;
            // 
            // grbMensagem
            // 
            this.grbMensagem.Controls.Add(this.textMensagem);
            this.grbMensagem.Location = new System.Drawing.Point(7, 75);
            this.grbMensagem.Name = "grbMensagem";
            this.grbMensagem.Size = new System.Drawing.Size(504, 186);
            this.grbMensagem.TabIndex = 8;
            this.grbMensagem.TabStop = false;
            this.grbMensagem.Text = "Mensagem";
            // 
            // textMensagem
            // 
            this.textMensagem.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textMensagem.Dock = System.Windows.Forms.DockStyle.Fill;
            this.textMensagem.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textMensagem.Location = new System.Drawing.Point(3, 16);
            this.textMensagem.Multiline = true;
            this.textMensagem.Name = "textMensagem";
            this.textMensagem.Size = new System.Drawing.Size(498, 167);
            this.textMensagem.TabIndex = 4;
            // 
            // lblListaAnexo
            // 
            this.lblListaAnexo.AutoSize = true;
            this.lblListaAnexo.ForeColor = System.Drawing.Color.Red;
            this.lblListaAnexo.Location = new System.Drawing.Point(12, 264);
            this.lblListaAnexo.Name = "lblListaAnexo";
            this.lblListaAnexo.Size = new System.Drawing.Size(51, 13);
            this.lblListaAnexo.TabIndex = 9;
            this.lblListaAnexo.Text = "Anexo(s):";
            // 
            // ofdEmail
            // 
            this.ofdEmail.FileName = "incluirAnexo";
            this.ofdEmail.Multiselect = true;
            // 
            // frmEmailEnviar
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(534, 361);
            this.Name = "frmEmailEnviar";
            this.Text = "ENVIAR E-MAIL";
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.frmEmailEnviar_KeyDown);
            this.scForm.Panel1.ResumeLayout(false);
            this.scForm.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.scForm)).EndInit();
            this.scForm.ResumeLayout(false);
            this.pnlForm.ResumeLayout(false);
            this.pnlForm.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.banner)).EndInit();
            this.flpAcao.ResumeLayout(false);
            this.grbMensagem.ResumeLayout(false);
            this.grbMensagem.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.FlowLayoutPanel flpAcao;
        private System.Windows.Forms.Button btnCancelar;
        private System.Windows.Forms.Button btnAnexo;
        private System.Windows.Forms.Button btnEnviar;
        private System.Windows.Forms.TextBox lblPara;
        private System.Windows.Forms.TextBox lblPrioridade;
        private System.Windows.Forms.TextBox lblAssunto;
        private ComboBoxWithBorder cbPrioridade;
        private System.Windows.Forms.TextBox textAssunto;
        private System.Windows.Forms.TextBox textEmail;
        private System.Windows.Forms.GroupBox grbMensagem;
        private System.Windows.Forms.TextBox textMensagem;
        private System.Windows.Forms.Label lblListaAnexo;
        private System.Windows.Forms.OpenFileDialog ofdEmail;
        private System.Windows.Forms.ToolTip tp;
    }
}