﻿namespace SWS.View
{
    partial class frm_ProdutoTipoEstudoOld
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frm_ProdutoTipoEstudoOld));
            this.grb_produto = new System.Windows.Forms.GroupBox();
            this.btn_fechar = new System.Windows.Forms.Button();
            this.btn_confirma = new System.Windows.Forms.Button();
            this.dg_produto = new System.Windows.Forms.DataGridView();
            this.grb_produto.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dg_produto)).BeginInit();
            this.SuspendLayout();
            // 
            // grb_produto
            // 
            this.grb_produto.BackColor = System.Drawing.Color.White;
            this.grb_produto.Controls.Add(this.btn_fechar);
            this.grb_produto.Controls.Add(this.btn_confirma);
            this.grb_produto.Controls.Add(this.dg_produto);
            this.grb_produto.Dock = System.Windows.Forms.DockStyle.Fill;
            this.grb_produto.Location = new System.Drawing.Point(0, 0);
            this.grb_produto.Name = "grb_produto";
            this.grb_produto.Size = new System.Drawing.Size(284, 262);
            this.grb_produto.TabIndex = 0;
            this.grb_produto.TabStop = false;
            this.grb_produto.Text = "Produtos";
            // 
            // btn_fechar
            // 
            this.btn_fechar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_fechar.Image = global::SWS.Properties.Resources.close;
            this.btn_fechar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btn_fechar.Location = new System.Drawing.Point(89, 227);
            this.btn_fechar.Name = "btn_fechar";
            this.btn_fechar.Size = new System.Drawing.Size(75, 23);
            this.btn_fechar.TabIndex = 2;
            this.btn_fechar.Text = "&Fechar";
            this.btn_fechar.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btn_fechar.UseVisualStyleBackColor = true;
            this.btn_fechar.Click += new System.EventHandler(this.btn_fechar_Click);
            // 
            // btn_confirma
            // 
            this.btn_confirma.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_confirma.Image = global::SWS.Properties.Resources.fechar_ico;
            this.btn_confirma.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btn_confirma.Location = new System.Drawing.Point(7, 227);
            this.btn_confirma.Name = "btn_confirma";
            this.btn_confirma.Size = new System.Drawing.Size(75, 23);
            this.btn_confirma.TabIndex = 1;
            this.btn_confirma.Text = "&Confirma";
            this.btn_confirma.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btn_confirma.UseVisualStyleBackColor = true;
            this.btn_confirma.Click += new System.EventHandler(this.btn_confirma_Click);
            // 
            // dg_produto
            // 
            this.dg_produto.AllowUserToAddRows = false;
            this.dg_produto.AllowUserToDeleteRows = false;
            this.dg_produto.AllowUserToOrderColumns = true;
            this.dg_produto.AllowUserToResizeRows = false;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.Gainsboro;
            this.dg_produto.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle1;
            this.dg_produto.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.DisplayedCells;
            this.dg_produto.BackgroundColor = System.Drawing.Color.White;
            this.dg_produto.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.Color.LightGray;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dg_produto.DefaultCellStyle = dataGridViewCellStyle2;
            this.dg_produto.Location = new System.Drawing.Point(6, 19);
            this.dg_produto.Name = "dg_produto";
            this.dg_produto.ReadOnly = true;
            this.dg_produto.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dg_produto.Size = new System.Drawing.Size(272, 201);
            this.dg_produto.TabIndex = 0;
            this.dg_produto.CellMouseDoubleClick += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.dg_produto_CellMouseDoubleClick);
            // 
            // frm_ProdutoTipoEstudo
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(284, 262);
            this.ControlBox = false;
            this.Controls.Add(this.grb_produto);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "frm_ProdutoTipoEstudo";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "SELECIONAR PRODUTO";
            this.Load += new System.EventHandler(this.frm_ProdutoTipoEstudo_Load);
            this.grb_produto.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dg_produto)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox grb_produto;
        private System.Windows.Forms.Button btn_fechar;
        private System.Windows.Forms.Button btn_confirma;
        private System.Windows.Forms.DataGridView dg_produto;
    }
}