﻿namespace SegSis.View
{
    partial class frm_atividade
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.pnl_atividade = new System.Windows.Forms.Panel();
            this.pnl_paginacao = new System.Windows.Forms.Panel();
            this.btn_inclui = new System.Windows.Forms.Button();
            this.btn_altera = new System.Windows.Forms.Button();
            this.btn_exclui = new System.Windows.Forms.Button();
            this.btn_detalha = new System.Windows.Forms.Button();
            this.grb_gride = new System.Windows.Forms.GroupBox();
            this.dg_atividade = new System.Windows.Forms.DataGridView();
            this.grb_filtro = new System.Windows.Forms.GroupBox();
            this.btn_limpar = new System.Windows.Forms.Button();
            this.btn_pesquisar = new System.Windows.Forms.Button();
            this.grb_situacao = new System.Windows.Forms.GroupBox();
            this.rb_todos = new System.Windows.Forms.RadioButton();
            this.rb_desativada = new System.Windows.Forms.RadioButton();
            this.rb_ativo = new System.Windows.Forms.RadioButton();
            this.text_descricao = new System.Windows.Forms.TextBox();
            this.lbl_descricao = new System.Windows.Forms.Label();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.pnl_atividade.SuspendLayout();
            this.pnl_paginacao.SuspendLayout();
            this.grb_gride.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dg_atividade)).BeginInit();
            this.grb_filtro.SuspendLayout();
            this.grb_situacao.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // pnl_atividade
            // 
            this.pnl_atividade.BackColor = System.Drawing.Color.SteelBlue;
            this.pnl_atividade.Controls.Add(this.pnl_paginacao);
            this.pnl_atividade.Controls.Add(this.grb_gride);
            this.pnl_atividade.Controls.Add(this.grb_filtro);
            this.pnl_atividade.Location = new System.Drawing.Point(-2, 37);
            this.pnl_atividade.Name = "pnl_atividade";
            this.pnl_atividade.Size = new System.Drawing.Size(774, 526);
            this.pnl_atividade.TabIndex = 0;
            // 
            // pnl_paginacao
            // 
            this.pnl_paginacao.Controls.Add(this.btn_inclui);
            this.pnl_paginacao.Controls.Add(this.btn_altera);
            this.pnl_paginacao.Controls.Add(this.btn_exclui);
            this.pnl_paginacao.Controls.Add(this.btn_detalha);
            this.pnl_paginacao.Location = new System.Drawing.Point(6, 466);
            this.pnl_paginacao.Name = "pnl_paginacao";
            this.pnl_paginacao.Size = new System.Drawing.Size(751, 52);
            this.pnl_paginacao.TabIndex = 2;
            // 
            // btn_inclui
            // 
            this.btn_inclui.Location = new System.Drawing.Point(171, 18);
            this.btn_inclui.Name = "btn_inclui";
            this.btn_inclui.Size = new System.Drawing.Size(75, 23);
            this.btn_inclui.TabIndex = 16;
            this.btn_inclui.Text = "&Incluir";
            this.btn_inclui.UseVisualStyleBackColor = true;
            // 
            // btn_altera
            // 
            this.btn_altera.Location = new System.Drawing.Point(271, 18);
            this.btn_altera.Name = "btn_altera";
            this.btn_altera.Size = new System.Drawing.Size(75, 23);
            this.btn_altera.TabIndex = 15;
            this.btn_altera.Text = "&Alterar";
            this.btn_altera.UseVisualStyleBackColor = true;
            // 
            // btn_exclui
            // 
            this.btn_exclui.Location = new System.Drawing.Point(373, 18);
            this.btn_exclui.Name = "btn_exclui";
            this.btn_exclui.Size = new System.Drawing.Size(75, 23);
            this.btn_exclui.TabIndex = 18;
            this.btn_exclui.Text = "&Excluir";
            this.btn_exclui.UseVisualStyleBackColor = true;
            // 
            // btn_detalha
            // 
            this.btn_detalha.Location = new System.Drawing.Point(466, 18);
            this.btn_detalha.Name = "btn_detalha";
            this.btn_detalha.Size = new System.Drawing.Size(75, 23);
            this.btn_detalha.TabIndex = 17;
            this.btn_detalha.Text = "&Detalhar";
            this.btn_detalha.UseVisualStyleBackColor = true;
            // 
            // grb_gride
            // 
            this.grb_gride.Controls.Add(this.dg_atividade);
            this.grb_gride.Location = new System.Drawing.Point(6, 126);
            this.grb_gride.Name = "grb_gride";
            this.grb_gride.Size = new System.Drawing.Size(751, 334);
            this.grb_gride.TabIndex = 1;
            this.grb_gride.TabStop = false;
            // 
            // dg_atividade
            // 
            this.dg_atividade.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dg_atividade.Location = new System.Drawing.Point(9, 19);
            this.dg_atividade.Name = "dg_atividade";
            this.dg_atividade.Size = new System.Drawing.Size(730, 271);
            this.dg_atividade.TabIndex = 0;
            // 
            // grb_filtro
            // 
            this.grb_filtro.Controls.Add(this.btn_limpar);
            this.grb_filtro.Controls.Add(this.btn_pesquisar);
            this.grb_filtro.Controls.Add(this.grb_situacao);
            this.grb_filtro.Controls.Add(this.text_descricao);
            this.grb_filtro.Controls.Add(this.lbl_descricao);
            this.grb_filtro.Location = new System.Drawing.Point(6, 3);
            this.grb_filtro.Name = "grb_filtro";
            this.grb_filtro.Size = new System.Drawing.Size(751, 117);
            this.grb_filtro.TabIndex = 0;
            this.grb_filtro.TabStop = false;
            // 
            // btn_limpar
            // 
            this.btn_limpar.Location = new System.Drawing.Point(356, 77);
            this.btn_limpar.Name = "btn_limpar";
            this.btn_limpar.Size = new System.Drawing.Size(75, 23);
            this.btn_limpar.TabIndex = 4;
            this.btn_limpar.Text = "&Limpar";
            this.btn_limpar.UseVisualStyleBackColor = true;
            // 
            // btn_pesquisar
            // 
            this.btn_pesquisar.Location = new System.Drawing.Point(247, 77);
            this.btn_pesquisar.Name = "btn_pesquisar";
            this.btn_pesquisar.Size = new System.Drawing.Size(75, 23);
            this.btn_pesquisar.TabIndex = 3;
            this.btn_pesquisar.Text = "&Pesquisar";
            this.btn_pesquisar.UseVisualStyleBackColor = true;
            // 
            // grb_situacao
            // 
            this.grb_situacao.Controls.Add(this.rb_todos);
            this.grb_situacao.Controls.Add(this.rb_desativada);
            this.grb_situacao.Controls.Add(this.rb_ativo);
            this.grb_situacao.Location = new System.Drawing.Point(520, 16);
            this.grb_situacao.Name = "grb_situacao";
            this.grb_situacao.Size = new System.Drawing.Size(219, 46);
            this.grb_situacao.TabIndex = 2;
            this.grb_situacao.TabStop = false;
            // 
            // rb_todos
            // 
            this.rb_todos.AutoSize = true;
            this.rb_todos.Checked = true;
            this.rb_todos.Location = new System.Drawing.Point(158, 17);
            this.rb_todos.Name = "rb_todos";
            this.rb_todos.Size = new System.Drawing.Size(55, 17);
            this.rb_todos.TabIndex = 2;
            this.rb_todos.TabStop = true;
            this.rb_todos.Text = "Todos";
            this.rb_todos.UseVisualStyleBackColor = true;
            // 
            // rb_desativada
            // 
            this.rb_desativada.AutoSize = true;
            this.rb_desativada.Location = new System.Drawing.Point(82, 17);
            this.rb_desativada.Name = "rb_desativada";
            this.rb_desativada.Size = new System.Drawing.Size(79, 17);
            this.rb_desativada.TabIndex = 1;
            this.rb_desativada.Text = "Desativada";
            this.rb_desativada.UseVisualStyleBackColor = true;
            // 
            // rb_ativo
            // 
            this.rb_ativo.AutoSize = true;
            this.rb_ativo.Location = new System.Drawing.Point(14, 17);
            this.rb_ativo.Name = "rb_ativo";
            this.rb_ativo.Size = new System.Drawing.Size(61, 17);
            this.rb_ativo.TabIndex = 0;
            this.rb_ativo.Text = "Ativada";
            this.rb_ativo.UseVisualStyleBackColor = true;
            // 
            // text_descricao
            // 
            this.text_descricao.Location = new System.Drawing.Point(9, 33);
            this.text_descricao.MaxLength = 50;
            this.text_descricao.Name = "text_descricao";
            this.text_descricao.Size = new System.Drawing.Size(505, 20);
            this.text_descricao.TabIndex = 1;
            // 
            // lbl_descricao
            // 
            this.lbl_descricao.AutoSize = true;
            this.lbl_descricao.Location = new System.Drawing.Point(6, 16);
            this.lbl_descricao.Name = "lbl_descricao";
            this.lbl_descricao.Size = new System.Drawing.Size(55, 13);
            this.lbl_descricao.TabIndex = 0;
            this.lbl_descricao.Text = "Descrição";
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = global::SegSis.View.Properties.Resources.banner1;
            this.pictureBox1.Location = new System.Drawing.Point(-2, -3);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(797, 37);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 3;
            this.pictureBox1.TabStop = false;
            // 
            // frm_atividade
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(784, 562);
            this.Controls.Add(this.pnl_atividade);
            this.Controls.Add(this.pictureBox1);
            this.Name = "frm_atividade";
            this.Text = "Atividade";
            this.pnl_atividade.ResumeLayout(false);
            this.pnl_paginacao.ResumeLayout(false);
            this.grb_gride.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dg_atividade)).EndInit();
            this.grb_filtro.ResumeLayout(false);
            this.grb_filtro.PerformLayout();
            this.grb_situacao.ResumeLayout(false);
            this.grb_situacao.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel pnl_atividade;
        private System.Windows.Forms.GroupBox grb_filtro;
        private System.Windows.Forms.TextBox text_descricao;
        private System.Windows.Forms.Label lbl_descricao;
        private System.Windows.Forms.GroupBox grb_situacao;
        private System.Windows.Forms.RadioButton rb_todos;
        private System.Windows.Forms.RadioButton rb_desativada;
        private System.Windows.Forms.RadioButton rb_ativo;
        private System.Windows.Forms.Button btn_pesquisar;
        private System.Windows.Forms.Button btn_limpar;
        private System.Windows.Forms.GroupBox grb_gride;
        private System.Windows.Forms.DataGridView dg_atividade;
        private System.Windows.Forms.Panel pnl_paginacao;
        private System.Windows.Forms.Button btn_inclui;
        private System.Windows.Forms.Button btn_altera;
        private System.Windows.Forms.Button btn_exclui;
        private System.Windows.Forms.Button btn_detalha;
        private System.Windows.Forms.PictureBox pictureBox1;
    }
}