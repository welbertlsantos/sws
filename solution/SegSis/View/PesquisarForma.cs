﻿using SWS.Entidade;
using SWS.Facade;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace SWS.View
{
    public partial class frmPesquisarForma : frmTemplateConsulta
    {
        private List<Forma> formas = new List<Forma>();

        public List<Forma> Formas
        {
            get { return formas; }
            set { formas = value; }
        }

        private bool status = false;
        
        public frmPesquisarForma()
        {
            InitializeComponent();
            ActiveControl = dgvForma;
            MontaDataGrid();
        }

        private void MontaDataGrid()
        {
            try
            {
                dgvForma.Columns.Clear();
                dgvForma.ColumnCount = 2;

                dgvForma.Columns[0].HeaderText = "Id";
                dgvForma.Columns[0].Visible = false;

                dgvForma.Columns[1].HeaderText = "Forma";
                dgvForma.Columns[1].ReadOnly = true;

                DataGridViewCheckBoxColumn check = new DataGridViewCheckBoxColumn();
                check.Name = "Selecionar";
                dgvForma.Columns.Add(check);
                dgvForma.Columns[2].DisplayIndex = 0;

                FinanceiroFacade financeiroFacade = FinanceiroFacade.getInstance();
                financeiroFacade.findAllFormas().ForEach(delegate(Forma forma)
                {
                    dgvForma.Rows.Add(forma.Id, forma.Descricao, false);
                });

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }

        private void frmPesquisarForma_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.F2)
            {
                if (dgvForma.Rows.Count > 0)
                {
                    foreach (DataGridViewRow dvRow in dgvForma.Rows)
                    {
                        dvRow.Cells[2].Value = !status;
                    }

                    status = (bool)dgvForma.Rows[0].Cells[2].Value;
                }
            }
        }

        private void btnGravar_Click(object sender, EventArgs e)
        {
            try
            {
                foreach (DataGridViewRow dvRow in dgvForma.Rows)
                    if ((bool)dvRow.Cells[2].Value)
                        formas.Add(new Forma((long)dvRow.Cells[0].Value, dvRow.Cells[1].Value.ToString()));

                this.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btnFechar_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
