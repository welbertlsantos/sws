﻿using SWS.Entidade;
using SWS.Facade;
using SWS.View.ViewHelper;
using SWS.ViewHelper;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace SWS.View
{
    public partial class frmMonitoramentoAgenteAlterar : frmMonitoramentoAgente
    {
        protected bool flagAltera = false;
        
        public frmMonitoramentoAgenteAlterar(MonitoramentoGheFonteAgente monitoramentogheFonteAgente) : base()
        {
            InitializeComponent();
            /* montando dados da tela */

            this.MonitoramentoGheFonteAgente = monitoramentogheFonteAgente;
            this.gheFonteAgente = monitoramentoGheFonteAgente.GheFonteAgente;

            ComboHelper.tipoAvaliacao(cbTipoAvaliacao);
            ComboHelper.unidadeMedicaoAgente(cbUnidadeMedida);
            cbTipoAvaliacao.SelectedValue = this.MonitoramentoGheFonteAgente.TipoAvaliacao;
            textIntensidade.Text = this.MonitoramentoGheFonteAgente.IntensidadeConcentracao.ToString();
            textLimiteTolerancia.Text = this.MonitoramentoGheFonteAgente.LimiteTolerancia.ToString();
            cbUnidadeMedida.SelectedValue = this.monitoramentoGheFonteAgente.UnidadeMedicao;
            textTecnica.Text = this.monitoramentoGheFonteAgente.TecnicaMedicao;
             
            ComboHelper.utilizaEPC(cbUtilizaEPC);
            ComboHelper.utilizaEPI(cbUtilizaEPI);
            ComboHelper.booleandoEsocial(cbEficaciaEpc);
            ComboHelper.booleandoEsocial(cbEficaciaEPI);

            ComboHelper.booleanoEsocialSemDefault(cbMedidaProtecao);
            ComboHelper.booleanoEsocialSemDefault(cbCondicaoFuncionamento);
            ComboHelper.booleanoEsocialSemDefault(cbUsoIniterrupto);
            ComboHelper.booleanoEsocialSemDefault(cbPrazoValidade);
            ComboHelper.booleanoEsocialSemDefault(cbPeriodicidadeTroca);
            ComboHelper.booleanoEsocialSemDefault(cbHigienizacao);
            

            textAgente.Text = this.MonitoramentoGheFonteAgente.GheFonteAgente.Agente.Descricao;
            TextRisco.Text = this.MonitoramentoGheFonteAgente.GheFonteAgente.Agente.Risco.Descricao;
            
            cbUtilizaEPC.SelectedValue = this.monitoramentoGheFonteAgente.UtilizaEpc;
            cbEficaciaEpc.SelectedValue = this.monitoramentoGheFonteAgente.EficaciaEpc;
            cbUtilizaEPI.SelectedValue = this.monitoramentoGheFonteAgente.UtilizaEpi;
            cbEficaciaEPI.SelectedValue = this.monitoramentoGheFonteAgente.EficaciaEpi;

            cbMedidaProtecao.SelectedValue = this.monitoramentoGheFonteAgente.MedidaProtecao;
            cbCondicaoFuncionamento.SelectedValue = this.monitoramentoGheFonteAgente.CondicaoFuncionamento;
            cbUsoIniterrupto.SelectedValue = this.monitoramentoGheFonteAgente.UsoIniterrupto;
            cbPrazoValidade.SelectedValue = this.monitoramentoGheFonteAgente.PrazoValidade;
            cbPeriodicidadeTroca.SelectedValue = this.monitoramentoGheFonteAgente.PeriodicidadeTroca;
            cbHigienizacao.SelectedValue = this.monitoramentoGheFonteAgente.Higienizacao;

            /* somente será liberado os grupos de perguntas 2 e inclusão de epi se utilizaEPI = 2 */

            if (!string.Equals(this.monitoramentoGheFonteAgente.UtilizaEpi, "2"))
            {
                flpAcaoEpi.Enabled = false;
                gbrPerguntasGrupo2.Enabled = false;
            }
            else
            {
                flpAcaoEpi.Enabled = true;
                gbrPerguntasGrupo2.Enabled = true;
            }

            textNumeroProcesso.Text = this.monitoramentoGheFonteAgente.NumeroProcessoJudicial;

            /* buscando colecao de epis */
            EsocialFacade esocialFacade = EsocialFacade.getInstance();
            this.epis = esocialFacade.findAllEpiMonitoramentoByMonitoramentoGheFonteAgente(this.monitoramentoGheFonteAgente);
            montaGridEpis();
        }

        public frmMonitoramentoAgenteAlterar()
        {
            InitializeComponent();
        }

        protected override void btnIncluir_Click(object sender, EventArgs e)
        {
            try
            {
                /* validando campos obrigatórios */
                if (!validaCamposTela())
                {
                    EsocialFacade esocialFacade = EsocialFacade.getInstance();

                    MonitoramentoGheFonteAgente = esocialFacade.updateMonitoramentoGheFonteAgente(new MonitoramentoGheFonteAgente(
                        MonitoramentoGheFonteAgente.Id,
                        gheFonteAgente,
                        MonitoramentoGheFonteAgente.Monitoramento,
                        cbTipoAvaliacao.SelectedIndex == -1 ? string.Empty : ((SelectItem)cbTipoAvaliacao.SelectedItem).Valor,
                        string.IsNullOrEmpty(textIntensidade.Text.Trim()) ? Convert.ToDecimal(0) : Convert.ToDecimal(textIntensidade.Text),
                        string.IsNullOrEmpty(textLimiteTolerancia.Text.Trim()) ? Convert.ToDecimal(0) : Convert.ToDecimal(textLimiteTolerancia.Text),
                        cbUnidadeMedida.SelectedIndex == -1 ? string.Empty : ((SelectItem)cbUnidadeMedida.SelectedItem).Valor,
                        textTecnica.Text.Trim(),
                        ((SelectItem)cbUtilizaEPC.SelectedItem).Valor,
                        string.Equals(((SelectItem)cbUtilizaEPC.SelectedItem).Valor, "2") ? ((SelectItem)cbEficaciaEpc.SelectedItem).Valor : string.Empty,
                        ((SelectItem)cbUtilizaEPI.SelectedItem).Valor,
                        string.Equals(((SelectItem)cbUtilizaEPI.SelectedItem).Valor, "2") ? ((SelectItem)cbEficaciaEPI.SelectedItem).Valor : string.Empty,
                        ((SelectItem)cbMedidaProtecao.SelectedItem).Valor, 
                        ((SelectItem)cbCondicaoFuncionamento.SelectedItem).Valor,
                        ((SelectItem)cbUsoIniterrupto.SelectedItem).Valor,
                        ((SelectItem)cbPrazoValidade.SelectedItem).Valor,
                        ((SelectItem)cbPeriodicidadeTroca.SelectedItem).Valor,
                        ((SelectItem)cbHigienizacao.SelectedItem).Valor, 
                        textNumeroProcesso.Text));

                    MessageBox.Show("Monitoramento do agente alterado com sucesso.", "Confirmação", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    this.Close();

                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }

        protected override void btnIncluirEpi_Click(object sender, EventArgs e)
        {
            try
            {
                frmEpiBuscar buscarEpi = new frmEpiBuscar();
                buscarEpi.ShowDialog();

                if (buscarEpi.Epi != null)
                {
                    /* verificando se o epi não está na lista */
                    if (epis.Exists(x => x.Epi.Id == buscarEpi.Epi.Id))
                        throw new Exception("Epi já incluido.");

                    /* incluir o epi no agente monitorado */
                    EsocialFacade esocialFacade = EsocialFacade.getInstance();
                    epis.Add(esocialFacade.insertEpiMonitoramento(new EpiMonitoramento(null, buscarEpi.Epi, this.MonitoramentoGheFonteAgente)));
                    montaGridEpis();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }

        protected override void btnExcluirEpi_Click(object sender, EventArgs e)
        {
            try
            {
                if (dgvEpis.CurrentRow == null)
                    throw new Exception("Selecione um EPI para excluir");

                if (MessageBox.Show("Deseja excluir o EPI selecionado?", "Atenção", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                {
                    EpiMonitoramento epiMonitoramentoExcluir = epis.Find(x => x.Id == (long)dgvEpis.CurrentRow.Cells[0].Value);
                    EsocialFacade esocialFacade = EsocialFacade.getInstance();
                    esocialFacade.deleteEpiMonitoramento(epiMonitoramentoExcluir);
                    epis.RemoveAt(dgvEpis.CurrentRow.Index);
                    MessageBox.Show("Epi excluído com sucesso", "Confirmação", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    montaGridEpis();
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }

        private void cbUtilizaEPI_SelectedIndexChanged_1(object sender, EventArgs e)
        {
            if (((SelectItem)cbUtilizaEPI.SelectedItem).Valor == "2")
            {
                cbEficaciaEPI.Enabled = true;
                /* habilitando demais controles e carregando informação necessária */
                flpAcaoEpi.Enabled = true;
                gbrPerguntasGrupo2.Enabled = true;

            }
            else
            {
                if (flagAltera && epis.Count > 0)
                {

                    if (MessageBox.Show("Você está informando que não será utilizado EPI. Nesse caso os epis serão excluídos e as perguntas do grupo 2 também. Deseja continuar?", "Confirmação", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                    {
                        /* excluíndo os epis na lista */
                        foreach (EpiMonitoramento epiMonitoramento in epis)
                        {
                            EsocialFacade esocialFacade = EsocialFacade.getInstance();
                            esocialFacade.deleteEpiMonitoramento(epiMonitoramento);
                        }

                        epis.Clear();
                        montaGridEpis();

                        /* carregando novamente as perguntas */
                        ComboHelper.booleanoEsocialSemDefault(cbMedidaProtecao);
                        ComboHelper.booleanoEsocialSemDefault(cbCondicaoFuncionamento);
                        ComboHelper.booleanoEsocialSemDefault(cbUsoIniterrupto);
                        ComboHelper.booleanoEsocialSemDefault(cbPrazoValidade);
                        ComboHelper.booleanoEsocialSemDefault(cbPeriodicidadeTroca);
                        ComboHelper.booleanoEsocialSemDefault(cbHigienizacao);
                        flagAltera = true;

                    }
                }


                cbEficaciaEPI.Enabled = false;

                flpAcaoEpi.Enabled = false;
                gbrPerguntasGrupo2.Enabled = false;
            }

            flagAltera = true;
            
        }
    }
}
