﻿using SWS.Entidade;
using SWS.Facade;
using SWS.View.Resources;
using SWS.View.ViewHelper;
using SWS.ViewHelper;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace SWS.View
{
    public partial class frmNotaPrincipal : frmTemplate
    {
        private Nota nota;

        public frmNotaPrincipal()
        {
            InitializeComponent();
            validaPermissoes();
            ActiveControl = textNota;
            ComboHelper.comboSituacao(cbSituacao);
        }

        protected void frmNotaIncluir_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter || e.KeyCode == Keys.Return)
                SendKeys.Send("{TAB}");
        }

        private void btnIncluir_Click(object sender, EventArgs e)
        {
            try
            {
                frmNotaIncluir incluirNota = new frmNotaIncluir();
                incluirNota.ShowDialog();

                if (incluirNota.Nota != null)
                {
                    nota = new Nota();
                    montaDataGrid();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }

        private void btnAlterar_Click(object sender, EventArgs e)
        {
            try
            {
                if (dgvNota.CurrentRow == null)
                    throw new Exception("Selecione uma linha.");

                EstudoFacade pcmsoFacade = EstudoFacade.getInstance();

                Nota notaAlterar = pcmsoFacade.findNotaById((long)dgvNota.CurrentRow.Cells[0].Value);

                if (!string.Equals(notaAlterar.Situacao, ApplicationConstants.ATIVO))
                    throw new Exception("Somente notas ativas podem ser alteradas.");

                frmNotaAlterar alterarNota = new frmNotaAlterar(notaAlterar);
                alterarNota.ShowDialog();
                nota = new Nota();
                montaDataGrid();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }

        private void btnPesquisar_Click(object sender, EventArgs e)
        {
            try
            {
                nota = new Nota(null, textNota.Text, ((SelectItem)cbSituacao.SelectedItem).Valor, string.Empty);
                montaDataGrid();
                textNota.Focus();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }

        private void btnReativar_Click(object sender, EventArgs e)
        {
            try
            {
                if (dgvNota.CurrentRow == null)
                    throw new Exception("Selecione uma linha.");

                EstudoFacade pcmsoFacade = EstudoFacade.getInstance();

                Nota notaReativar = pcmsoFacade.findNotaById((long)dgvNota.CurrentRow.Cells[0].Value);

                if (!string.Equals(notaReativar.Situacao, ApplicationConstants.DESATIVADO))
                    throw new Exception("Somente notas inativas podem ser reativadas.");

                if (MessageBox.Show("Deseja reativar a nota selecionada?", "Atenção", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                {
                    notaReativar.Situacao = ApplicationConstants.ATIVO;
                    pcmsoFacade.updateNota(notaReativar);
                    MessageBox.Show("Nota do GHE reativada com sucesso.", "Confirmação", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    nota = new Nota();
                    montaDataGrid();
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }

        private void btnDetalhar_Click(object sender, EventArgs e)
        {
            try
            {
                if (dgvNota.CurrentRow == null)
                    throw new Exception("Selecione uma linha.");

                EstudoFacade pcmsoFacade = EstudoFacade.getInstance();

                frmNotaDetalhar notaDetalhar = new frmNotaDetalhar(pcmsoFacade.findNotaById((long)dgvNota.CurrentRow.Cells[0].Value));
                notaDetalhar.ShowDialog();

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }

        private void btnExcluir_Click(object sender, EventArgs e)
        {
            try
            {
                if (dgvNota.CurrentRow == null)
                    throw new Exception("Selecione uma linha.");

                EstudoFacade pcmsoFacade = EstudoFacade.getInstance();

                Nota notaExcluir = pcmsoFacade.findNotaById((long)dgvNota.CurrentRow.Cells[0].Value);

                if (!string.Equals(notaExcluir.Situacao, ApplicationConstants.ATIVO))
                    throw new Exception("Somente notas ativas podem ser excluídas.");

                if (MessageBox.Show("Deseja excluir a nota selecionada?", "Atenção", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                {
                    notaExcluir.Situacao = ApplicationConstants.DESATIVADO;
                    pcmsoFacade.updateNota(notaExcluir);
                    MessageBox.Show("Nota do GHE excluída com sucesso.", "Confirmação", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    nota = new Nota();
                    montaDataGrid();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }

        private void btnFechar_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void validaPermissoes()
        {
            PermissionamentoFacade permissionamentoFacade = PermissionamentoFacade.getInstance();

            btnAlterar.Enabled = permissionamentoFacade.hasPermission("NORMA", "ALTERAR");
            btnIncluir.Enabled = permissionamentoFacade.hasPermission("NORMA", "INCLUIR");
            btnExcluir.Enabled = permissionamentoFacade.hasPermission("NORMA", "EXCLUIR");
            btnDetalhar.Enabled = permissionamentoFacade.hasPermission("NORMA", "DETALHAR");
            btnReativar.Enabled = permissionamentoFacade.hasPermission("NORMA", "REATIVAR");
        }

        public void montaDataGrid()
        {
            EstudoFacade pcmsoFacade = EstudoFacade.getInstance();


            DataSet ds = pcmsoFacade.findNotaByFilter(nota);

            dgvNota.DataSource = ds.Tables[0].DefaultView;

            dgvNota.Columns[0].HeaderText = "ID";
            dgvNota.Columns[0].Visible = false;

            dgvNota.Columns[1].HeaderText = "Título";

            dgvNota.Columns[2].HeaderText = "Situação";
            dgvNota.Columns[2].Visible = false;
            
            dgvNota.Columns[3].HeaderText = "Conteúdo";
            dgvNota.Columns[3].Visible = false;

        }

        private void dgvNota_RowPrePaint(object sender, DataGridViewRowPrePaintEventArgs e)
        {
            DataGridView dgv = sender as DataGridView;

            if (string.Equals(dgv.Rows[e.RowIndex].Cells[2].Value, ApplicationConstants.DESATIVADO))
                dgv.Rows[e.RowIndex].DefaultCellStyle.ForeColor = Color.Red;
        }

        private void dgvNota_CellMouseDoubleClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            try
            {
                btnAlterar.PerformClick();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }
    }
}
