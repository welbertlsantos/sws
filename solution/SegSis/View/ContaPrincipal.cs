﻿using SWS.Entidade;
using SWS.Facade;
using SWS.View.Resources;
using SWS.View.ViewHelper;
using SWS.ViewHelper;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace SWS.View
{
    public partial class frmContaPrincipal : frmTemplate
    {
        private Conta conta;
        private Banco banco;
        
        public frmContaPrincipal()
        {
            InitializeComponent();
            ComboHelper.comboSituacao(cbSituacao);
        }

        private void btnIncluir_Click(object sender, EventArgs e)
        {
            try
            {
                frmContaIncluir incluirConta = new frmContaIncluir();
                incluirConta.ShowDialog();

                if (incluirConta.Conta != null)
                {
                    conta = new Conta(null, banco, string.Empty, string.Empty, string.Empty, string.Empty, string.Empty, null, string.Empty, string.Empty, string.Empty, string.Empty, string.Empty, false, string.Empty, false);
                    montaDataGrid();
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btnAlterar_Click(object sender, EventArgs e)
        {
            try
            {
                if (dgvConta.CurrentRow == null)
                    throw new Exception("Selecione uma linha.");

                FinanceiroFacade financeiroFacade = FinanceiroFacade.getInstance();

                Conta contaAlterar = financeiroFacade.findContaById((long)dgvConta.CurrentRow.Cells[0].Value);

                if (!string.Equals(contaAlterar.Situacao, ApplicationConstants.ATIVO))
                    throw new Exception("Somente contas ativas podem ser alteradas.");

                if (contaAlterar.Privado)
                    throw new Exception("Conta exclúsiva do sistema. Não pode ser alterada.");

                frmContaAlterar alterarConta = new frmContaAlterar(contaAlterar);
                alterarConta.ShowDialog();
                conta = new Conta(null, banco, string.Empty, string.Empty, string.Empty, string.Empty, string.Empty, null, string.Empty, string.Empty, string.Empty, string.Empty, string.Empty, false, string.Empty, false);
                montaDataGrid();

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btnPesquisar_Click(object sender, EventArgs e)
        {
            try
            {
                textNome.Focus();
                conta = new Conta(null, banco, textNome.Text, string.Empty, string.Empty, string.Empty, string.Empty, null, string.Empty, string.Empty, string.Empty, string.Empty, string.Empty, false, ((SelectItem)cbSituacao.SelectedItem).Valor, false);
                montaDataGrid();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btnReativar_Click(object sender, EventArgs e)
        {
            try
            {
                if (dgvConta.CurrentRow == null)
                    throw new Exception("Selecione uma linha.");

                FinanceiroFacade financeiroFacade = FinanceiroFacade.getInstance();

                Conta contaReativar = financeiroFacade.findContaById((long)dgvConta.CurrentRow.Cells[0].Value);

                if (!string.Equals(contaReativar.Situacao, ApplicationConstants.DESATIVADO))
                    throw new Exception("Somente contas inativas podem ser reativadas.");

                if (MessageBox.Show("Deseja reativar a conta selecionada?", "Atenção", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                {
                    contaReativar.Situacao = ApplicationConstants.ATIVO;
                    financeiroFacade.updateConta(contaReativar);
                    MessageBox.Show("Conta reativada com sucesso.", "Confirmação", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    conta = new Conta(null, banco, string.Empty, string.Empty, string.Empty, string.Empty, string.Empty, null, string.Empty, string.Empty, string.Empty, string.Empty, string.Empty, false, string.Empty, false);
                    montaDataGrid();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btnDetalhar_Click(object sender, EventArgs e)
        {
            try
            {
                if (dgvConta.CurrentRow == null)
                    throw new Exception("Selecione uma linha.");

                FinanceiroFacade financeiroFacade = FinanceiroFacade.getInstance();

                Conta contaDetalhar = financeiroFacade.findContaById((long)dgvConta.CurrentRow.Cells[0].Value);

                frmContaDetalhar detalharConta = new frmContaDetalhar(contaDetalhar);
                detalharConta.ShowDialog();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btnExcluir_Click(object sender, EventArgs e)
        {
            try
            {
                if (dgvConta.CurrentRow == null)
                    throw new Exception("Selecione uma linha.");

                FinanceiroFacade financeiroFacade = FinanceiroFacade.getInstance();

                Conta contaExcluir = financeiroFacade.findContaById((long)dgvConta.CurrentRow.Cells[0].Value);

                if (contaExcluir.Privado)
                    throw new Exception("Conta exclusiva do sistema. Não pode ser excluída.");

                if (!string.Equals(contaExcluir.Situacao, ApplicationConstants.ATIVO))
                    throw new Exception("Somente contas ativas podem ser excluídas.");

                if (MessageBox.Show("Deseja excluir a conta selecionada?", "Atenção", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                {
                    contaExcluir.Situacao = ApplicationConstants.DESATIVADO;
                    financeiroFacade.updateConta(contaExcluir);
                    MessageBox.Show("Conta excluída com sucesso.", "Confirmação", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    conta = new Conta(null, banco, string.Empty, string.Empty, string.Empty, string.Empty, string.Empty, null, string.Empty, string.Empty, string.Empty, string.Empty, string.Empty, false, string.Empty, false);
                    montaDataGrid();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btnFechar_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnBanco_Click(object sender, EventArgs e)
        {
            try
            {
                frmBancoSelecionar bancoSelecionar = new frmBancoSelecionar(null, null);
                bancoSelecionar.ShowDialog();

                if (bancoSelecionar.Banco != null)
                {
                    banco = bancoSelecionar.Banco;
                    textBanco.Text = banco.Nome;
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }

        public void montaDataGrid()
        {
            try
            {
                this.Cursor = Cursors.WaitCursor;

                String situacao = String.Empty;

                FinanceiroFacade financeiroFacade = FinanceiroFacade.getInstance();

                dgvConta.Columns.Clear();

                DataSet ds = financeiroFacade.findContaByFilter(conta);

                dgvConta.DataSource = ds.Tables[0].DefaultView;

                dgvConta.Columns[0].HeaderText = "idConta";
                dgvConta.Columns[0].Visible = false;

                dgvConta.Columns[1].HeaderText = "Nome";

                dgvConta.Columns[2].HeaderText = "Agência";

                dgvConta.Columns[3].HeaderText = "Dígito Agência";

                dgvConta.Columns[4].HeaderText = "Conta";

                dgvConta.Columns[5].HeaderText = "Dígito Conta";

                dgvConta.Columns[6].HeaderText = "Próximo Numero";
                dgvConta.Columns[6].Visible = false;

                dgvConta.Columns[7].HeaderText = "Número de Remessa";
                dgvConta.Columns[7].Visible = false;

                dgvConta.Columns[8].HeaderText = "Carteira";
                dgvConta.Columns[8].Visible = false;

                dgvConta.Columns[9].HeaderText = "Convênio";
                dgvConta.Columns[9].Visible = false;

                dgvConta.Columns[10].HeaderText = "Variaçao da Carteira";
                dgvConta.Columns[10].Visible = false;

                dgvConta.Columns[11].HeaderText = "Código Cedente Banco";
                dgvConta.Columns[11].Visible = false;

                dgvConta.Columns[12].HeaderText = "Cobrança Registrada?";
                dgvConta.Columns[12].Width = 50;

                dgvConta.Columns[13].HeaderText = "Situação";
                dgvConta.Columns[13].Visible = false;

                dgvConta.Columns[14].HeaderText = "IdBanco";
                dgvConta.Columns[14].Visible = false;

                dgvConta.Columns[15].HeaderText = "Banco";

                dgvConta.Columns[16].HeaderText = "Código Banco";

                dgvConta.Columns[17].HeaderText = "Caixa?";

                dgvConta.Columns[18].HeaderText = "Situação";
                dgvConta.Columns[18].Visible = false;

                dgvConta.Columns[19].HeaderText = "Conta Privado";
                dgvConta.Columns[19].Visible = false;

                dgvConta.Columns[20].HeaderText = "Banco Privado";
                dgvConta.Columns[20].Visible = false;

                dgvConta.Columns[21].HeaderText = "Emite Boleto?";
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }

        private void dgvConta_RowPrePaint(object sender, DataGridViewRowPrePaintEventArgs e)
        {
            DataGridView dvg = sender as DataGridView;

            if (String.Equals(dvg.Rows[e.RowIndex].Cells[13].Value.ToString(), ApplicationConstants.DESATIVADO))
                dvg.Rows[e.RowIndex].DefaultCellStyle.ForeColor = Color.Red;
        }

        private void dgvConta_CellMouseDoubleClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            try
            {
                btnAlterar.PerformClick();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }
    }
}
