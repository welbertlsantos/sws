﻿using SWS.Entidade;
using SWS.Facade;
using SWS.View.Resources;
using SWS.View.ViewHelper;
using SWS.ViewHelper;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace SWS.View
{
    public partial class frmFuncionarioAlterar : frmFuncionarioIncluir
    {
        public frmFuncionarioAlterar(Funcionario funcionario)
        {
            InitializeComponent();
            this.funcionario = funcionario;

            /* preenchendo dados da tela */

            textNome.Text = funcionario.Nome;
            textCpf.Text = funcionario.Cpf;
            textRg.Text = funcionario.Rg;
            dataNascimento.Checked = true;
            dataNascimento.ShowCheckBox = false;
            dataNascimento.Text = Convert.ToString(funcionario.DataNascimento);
            textIdade.Text = ValidaCampoHelper.CalculaIdade(funcionario.DataNascimento).ToString();
            ComboHelper.startSexoAlterar(cbSexo);
            cbSexo.SelectedValue = funcionario.Sexo;
            textEmail.Text = funcionario.Email;
            ComboHelper.TipoSague(cbTipoSanguineo);
            cbTipoSanguineo.SelectedValue = funcionario.TipoSangue;
            ComboHelper.FatorRH(cbFatorRh);
            cbFatorRh.SelectedValue = funcionario.FatorRh;
            textPeso.Text = funcionario.Peso.ToString();
            textAltura.Text = funcionario.Altura.ToString();
            textCtps.Text = funcionario.Ctps;
            textPis.Text = funcionario.PisPasep;

            textLogradouro.Text = funcionario.Endereco;
            textNumero.Text = funcionario.Numero;
            textComplemento.Text = funcionario.Complemento;
            textBairro.Text = funcionario.Bairro;
            textCEP.Text = funcionario.Cep;
            ComboHelper.unidadeFederativa(cbUF);
            cbUF.SelectedValue = funcionario.Uf;
            cidade = funcionario.Cidade;
            if (cidade != null)
                textCidade.Text = cidade.Nome;
            textTelefone.Text = funcionario.Telefone1;
            textCelular.Text = funcionario.Telefone2;
            cbPcd.SelectedValue = funcionario.Pcd ? "true" : "false";

            textOrgaoEmissor.Text = funcionario.OrgaoEmissor;
            cbUfEmissor.SelectedValue = funcionario.UfEmissor;

            textSerieCtps.Text = funcionario.Serie;
            cbUfEmissorCtps.SelectedValue = funcionario.UfEmissorCtps;

            /* montando coleção de clientes que o funcionário foi cadastrado */

            FuncionarioFacade funcionarioFacade = FuncionarioFacade.getInstance();

            /* montando coleção das funções que o funcionário foi cadastrado */
            clienteFuncaoFuncionarioList = funcionarioFacade.findClienteFuncaoFuncionarioByFuncionarioAndSituacao(funcionario, false);
            montaGridCliente();
        }

        protected override void btIncluirCliente_Click(object sender, EventArgs e)
        {
            try
            {
                Cliente clienteSelecionado = null;
                ClienteFuncao clienteFuncaoSelecionado = null;
                ClienteFuncaoFuncionario clienteFuncaoFuncionarioSelecionado = null;
                FuncionarioFacade funcionarioFacade = FuncionarioFacade.getInstance();

                if (MessageBox.Show("Deseja incluir um cliente para esse funcionário?", "Confirmação", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                {
                    frmClienteSelecionar selecionarCliente = new frmClienteSelecionar(null, null, false, null, null, null, null, null, true);
                    selecionarCliente.ShowDialog();

                    if (selecionarCliente.Cliente != null)
                    {
                        clienteSelecionado = selecionarCliente.Cliente;

                        /* verificando se o cliente já está na coleção. Caso esteja então ele será desligado da função anterior */

                        if (clienteFuncaoFuncionarioList.Exists(x => x.ClienteFuncao.Cliente.Id == clienteSelecionado.Id && x.DataDesligamento == null))
                        {
                            /* desligando o funcionário do clienteFuncaoFuncionario incluíndo a data de demissão para hoje */
                            clienteFuncaoFuncionarioSelecionado = clienteFuncaoFuncionarioList.Find(x => x.ClienteFuncao.Cliente.Id == clienteSelecionado.Id && x.DataDesligamento == null);

                            if (clienteFuncaoFuncionarioSelecionado == null)
                                throw new Exception("Problemas no relacionamento do cliente. Solicite suporte");

                            //clienteFuncaoFuncionarioSelecionado.DataDesligamento = DateTime.Now;
                            funcionarioFacade.updateClienteFuncaoFuncionario(clienteFuncaoFuncionarioSelecionado);

                        }

                        /* incluindo a nova função */

                        frmClienteFuncaoSeleciona selecionarClienteFuncao = new frmClienteFuncaoSeleciona(clienteSelecionado);
                        selecionarClienteFuncao.ShowDialog();

                        if (selecionarClienteFuncao.ClienteFuncao != null)
                        {
                            clienteFuncaoSelecionado = selecionarClienteFuncao.ClienteFuncao;

                            /* incluindo dados do clienteFuncaoFuncionario */

                            frmClienteFuncaoFuncionarioIncluir selecionarClienteFuncaoFuncionario = new frmClienteFuncaoFuncionarioIncluir(clienteFuncaoSelecionado);
                            selecionarClienteFuncaoFuncionario.ShowDialog();

                            clienteFuncaoFuncionarioSelecionado = selecionarClienteFuncaoFuncionario.ClienteFuncaoFuncionario;

                            if (clienteFuncaoFuncionarioList.Exists(clienteFuncaoFuncionario => clienteFuncaoFuncionario.ClienteFuncao.Id == clienteFuncaoFuncionarioSelecionado.ClienteFuncao.Id && clienteFuncaoFuncionario.Situacao == "A"))
                            {
                                throw new Exception("Funcionário já possui essa função nessa empresa.");
                            }
                            else
                            {
                                clienteFuncaoFuncionarioSelecionado.Funcionario = funcionario;
                                clienteFuncaoFuncionarioList.Add(funcionarioFacade.incluirClienteFuncaoFuncionario(clienteFuncaoFuncionarioSelecionado));

                                montaGridCliente();
                                montaGridFuncao();
                                MessageBox.Show("Cadastramento realizado com sucesso.", "Confirmação", MessageBoxButtons.OK, MessageBoxIcon.Information);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }

        protected override void btAlterarCliente_Click(object sender, EventArgs e)
        {
            try
            {
                if (dgvCliente.CurrentRow == null && dgvFuncao.CurrentRow == null)
                    throw new Exception("Selecione uma linha.");

                ClienteFuncaoFuncionario clienteFuncaoFuncionarioAlterar = clienteFuncaoFuncionarioList.Find(x => x.Id == (long)dgvFuncao.CurrentRow.Cells[0].Value);

                frmClienteFuncaoFuncionarioAlterar alterarFuncionarioCliente = new frmClienteFuncaoFuncionarioAlterar(clienteFuncaoFuncionarioAlterar);
                alterarFuncionarioCliente.ShowDialog();

                if (alterarFuncionarioCliente.FlagAlteracao == true)
                {
                    /* gravando alterações do clienteFuncionario */
                    FuncionarioFacade funcionarioFacade = FuncionarioFacade.getInstance();

                    ClienteFuncaoFuncionario clienteFuncaoFuncionarioAltera = alterarFuncionarioCliente.ClienteFuncaoFuncionario;
                    funcionarioFacade.updateClienteFuncaoFuncionario(clienteFuncaoFuncionarioAltera);
                    MessageBox.Show("Dados do funcionário alterado com sucesso.", "Confirmação", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    montaGridCliente();
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }

        protected override void btExcluirCliente_Click(object sender, EventArgs e)
        {
            try
            {
                if (dgvCliente.CurrentRow == null)
                    throw new Exception("Você deve selecionar o cliente primeiro.");

                long idCliente = (long)dgvCliente.CurrentRow.Cells[1].Value;
                long idFuncao = (long)dgvFuncao.CurrentRow.Cells[1].Value;

                ClienteFuncaoFuncionario clienteFuncaoFuncionarioExcluir = clienteFuncaoFuncionarioList.Find(x => x.Id == (long)dgvFuncao.CurrentRow.Cells[0].Value );

                if (clienteFuncaoFuncionarioExcluir.DataDesligamento != null)
                    throw new Exception("A função ligada ao cliente já foi desativada. Não é permitido exclusão.");

                if (MessageBox.Show("Você deseja remover o cliente selecionado.", "Confirmação", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                {
                    
                    /* solicitando ao usuário que coloque a data de demissão do funcionário na empresa. A mesma será gravada no clienteFuncionario que foi excluído. */

                    frmClienteFuncaoFuncionarioExcluir funcionarioExcluir = new frmClienteFuncaoFuncionarioExcluir(clienteFuncaoFuncionarioExcluir);
                    funcionarioExcluir.ShowDialog();

                    if (funcionarioExcluir.ClienteFuncaoFuncionario.DataDesligamento != null)
                    {
                        FuncionarioFacade funcionarioFacade = FuncionarioFacade.getInstance();
                        /* excluindo a função que está relacionada ao clienteFuncionário */
                        clienteFuncaoFuncionarioExcluir.Situacao = "I";
                        funcionarioFacade.updateClienteFuncaoFuncionario(clienteFuncaoFuncionarioExcluir);
                        clienteFuncaoFuncionarioList.Remove(clienteFuncaoFuncionarioExcluir);
                        MessageBox.Show("Funcionário desativado com sucesso.", "Confirmação", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        montaGridCliente();
                        montaGridFuncao();
                    }

                }

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        protected override void btGravar_Click(object sender, EventArgs e)
        {
            try
            {
                if (!validaDadosTela())
                    throw new Exception("Verificar dados digitados.");

                /* preparando os dados do funcionário */

                funcionario = new Funcionario(funcionario.Id, textNome.Text.Trim(), textCpf.Text, textRg.Text, textLogradouro.Text.Trim(), textNumero.Text.Trim(), textComplemento.Text.Trim(), textBairro.Text.Trim(), cidade, (((SelectItem)cbUF.SelectedItem).Valor).ToString(), textTelefone.Text.Trim(), textCelular.Text.Trim(), textEmail.Text, (((SelectItem)cbTipoSanguineo.SelectedItem).Valor).ToString(), (((SelectItem)cbFatorRh.SelectedItem).Valor).ToString(), Convert.ToDateTime(dataNascimento.Text), String.IsNullOrEmpty(textPeso.Text) ? 0 : Convert.ToInt32(textPeso.Text), String.IsNullOrEmpty(textAltura.Text) ? 0 : Convert.ToDecimal(textAltura.Text), ApplicationConstants.ATIVO, textCEP.Text, (((SelectItem)cbSexo.SelectedItem).Valor).ToString(), textCtps.Text.Trim(), textPis.Text.Trim(), Convert.ToBoolean(((SelectItem)cbPcd.SelectedItem).Valor), textOrgaoEmissor.Text, ((SelectItem)cbUfEmissor.SelectedItem).Valor, textSerieCtps.Text.Trim(), ((SelectItem)cbUfEmissorCtps.SelectedItem).Valor); 
                /* verificando se o clienteFuncionario informado é estagiário. Nesse caso 
                 * a obrigação do cadastro do pis não e necessário */

                int quantidadeEstagiario = 0;

                clienteFuncaoFuncionarioList.ForEach(delegate(ClienteFuncaoFuncionario clienteFuncionarioProcura)
                {
                    if (clienteFuncionarioProcura.Estagiario == true)
                    {
                        quantidadeEstagiario++;
                    }
                });

                if ((quantidadeEstagiario != clienteFuncaoFuncionarioList.Count) && string.IsNullOrEmpty(funcionario.PisPasep))
                    throw new Exception("O campo Pis é obrigatório porque o funcionário está relacionado a um cliente");


                funcionario.ClienteFuncaoFuncionario = clienteFuncaoFuncionarioList;
                FuncionarioFacade funcionarioFacade = FuncionarioFacade.getInstance();

                funcionario = funcionarioFacade.updateFuncionario(funcionario);

                MessageBox.Show("Funcionário alterado com sucesso", "Confirmação", MessageBoxButtons.OK, MessageBoxIcon.Information);

                this.Close();

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        protected override void dgvFuncao_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                if (e.ColumnIndex == 7)
                {
                    /* alterando o cliente funcao funcionario */
                    FuncionarioFacade funcionarioFacade = FuncionarioFacade.getInstance();

                    clienteFuncaoFuncionarioList.Find(x => x.Id == (long)dgvFuncao.CurrentRow.Cells[0].Value).Vip = (bool)dgvFuncao.Rows[e.RowIndex].Cells[7].EditedFormattedValue;
                    funcionarioFacade.updateClienteFuncaoFuncionario(clienteFuncaoFuncionarioList.Find(x => x.Id == (long)dgvFuncao.CurrentRow.Cells[0].Value));
                    montaGridFuncao();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }
    }
}
