﻿using SWS.Entidade;
using SWS.Facade;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace SWS.View
{
    public partial class frmCidadePesquisa : frmTemplateConsulta
    {
        private CidadeIbge cidade;

        public CidadeIbge Cidade
        {
            get { return cidade; }
            set { cidade = value; }
        }

        private string uf;

        public frmCidadePesquisa(string uf)
        {
            InitializeComponent();
            this.uf = uf;
            ActiveControl = textCidade;
        }

        private void btnIncluir_Click(object sender, EventArgs e)
        {
            try
            {
                if (dgvCidade.CurrentRow == null)
                    throw new Exception("Selecione uma linha.");

                cidade = new CidadeIbge((Int64)dgvCidade.CurrentRow.Cells[0].Value, (String)dgvCidade.CurrentRow.Cells[1].Value, (String)dgvCidade.CurrentRow.Cells[2].Value, (String)dgvCidade.CurrentRow.Cells[3].Value); 
                this.Close();

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btnPesquisa_Click(object sender, EventArgs e)
        {
            montadataGrid();
        }

        private void btnFechar_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void montadataGrid()
        {
            try
            {
                this.Cursor = Cursors.WaitCursor;

                ClienteFacade clienteFacade = ClienteFacade.getInstance();

                dgvCidade.Columns.Clear();

                DataSet ds = clienteFacade.findCidadesByUf(uf, String.IsNullOrEmpty(textCidade.Text.Trim()) ? null : new CidadeIbge(null, String.Empty, textCidade.Text.ToUpper(), String.Empty));

                dgvCidade.DataSource = ds.Tables["Cidades"].DefaultView;

                dgvCidade.Columns[0].HeaderText = "id";
                dgvCidade.Columns[0].Visible = false;

                dgvCidade.Columns[1].HeaderText = "CódigoIBGE";
                dgvCidade.Columns[1].DisplayIndex = 2;

                dgvCidade.Columns[2].HeaderText = "Nome";
                dgvCidade.Columns[2].DisplayIndex = 1;

                dgvCidade.Columns[3].HeaderText = "UF";
                dgvCidade.Columns[3].DisplayIndex = 3;

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }

        }

        private void dgvCidade_CellMouseDoubleClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            try
            {
                btnIncluir.PerformClick();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }
    }
}
