﻿using SWS.Entidade;
using SWS.Excecao;
using SWS.Facade;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Xml;
using System.Xml.Serialization;
using Ionic.Zip;
using SWS.Helper;
using SWS.Resources;
using SWS.View.ViewHelper;



namespace SWS.View
{
    public partial class frmEsocial2220Principal : frmTemplate
    {
        private Cliente cliente;
        private string diretorioTemporario;
        private string diretorioExportacao;
        private string[] arquivos;
        private string tipoGeracaoArquivo;

        public string TipoGeracaoArquivo
        {
            get { return tipoGeracaoArquivo; }
            set { tipoGeracaoArquivo = value; }
        }


        private void validaPermissoes()
        {
            PermissionamentoFacade permissionamentoFacade = PermissionamentoFacade.getInstance();

            btnIncluir.Enabled = permissionamentoFacade.hasPermission("ESOCIAL-2220", "INCLUIR");
            btnPesquisa.Enabled = permissionamentoFacade.hasPermission("ESOCIAL-2220", "PESQUISAR");
            btnExcluir.Enabled = permissionamentoFacade.hasPermission("ESOCIAL-2220", "EXCLUIR");
            btnExportar.Enabled = permissionamentoFacade.hasPermission("ESOCIAL-2220", "EXPORTAR");
        }

        public frmEsocial2220Principal()
        {
            InitializeComponent();
            validaPermissoes();
        }

        private void btnIncluir_Click(object sender, EventArgs e)
        {
            frmEsocial2220Lote esocial2220LoteAsoForm = new frmEsocial2220Lote();
            esocial2220LoteAsoForm.ShowDialog();
        }

        private void btnPesquisa_Click(object sender, EventArgs e)
        {
            try
            {

                /*fazendo as validações de tela para a pesquisa.
                 *se o usuario selecionou o cliente, ou o cliente e a função, ou não
                 *preencheu nenhum atributo do funcionário ou do aso então será obrigatório
                 *usar o período para o filtro.
                 */

                if (!this.periodoCriacaoFinal.Checked && !this.periodoCriacaoInicial.Checked)
                    throw new Exception("É obrigatório selecionar um período.");

                /* verificando para saber se as datas estão em formato crescente e com lógica */

                if (periodoCriacaoInicial.Checked)
                {
                    if (!periodoCriacaoFinal.Checked)
                        throw new Exception("Você deve marcar também a data de emissão final para a pesquisa");

                    if (Convert.ToDateTime(periodoCriacaoInicial.Text) > Convert.ToDateTime(periodoCriacaoFinal.Text))
                        throw new Exception("A data de emissão final não pode ser maior que a data de emissão final.");
                }

                montaDataGrid();

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btnExportar_Click(object sender, EventArgs e)
        {
            try
            {
                /* chamando a tela para verificar o tipo de exportação */
                frmEsocialTipoGeracaoArquivo tipoExportacao = new frmEsocialTipoGeracaoArquivo();
                tipoExportacao.ShowDialog();

                if (!string.IsNullOrEmpty(tipoExportacao.TipoArquivo))
                {
                   TipoGeracaoArquivo = tipoExportacao.TipoArquivo;
                    if (string.Equals(TipoGeracaoArquivo, "INDIVIDUAL"))
                    {
                        geracaoIndividual();
                    }
                    else
                        geracaoAgrupado();

                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            

        }

        private void btnFechar_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btCliente_Click(object sender, EventArgs e)
        {
            try
            {
                frmClienteSelecionar selecionarCliente = new frmClienteSelecionar(null, null, false, null, null, null, null, null, null);
                selecionarCliente.ShowDialog();

                if (selecionarCliente.Cliente != null)
                {
                    cliente = selecionarCliente.Cliente;
                    this.textCliente.Text = this.cliente.RazaoSocial + " - " + ((int)this.cliente.tipoCliente() == 1 ? ValidaCampoHelper.FormataCnpj(this.cliente.Cnpj) : ValidaCampoHelper.FormataCpf(this.cliente.Cnpj));
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }

        private void btnExcluirCliente_Click(object sender, EventArgs e)
        {
            try
            {
                if (cliente == null)
                    throw new Exception("Você deve selecionar primeiro o cliente.");

                cliente = null;
                textCliente.Text = string.Empty;

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }

        public void montaDataGrid()
        {
            try
            {
                this.Cursor = Cursors.WaitCursor;

                this.dgvLote.Columns.Clear();
                textTotal.Text = String.Empty;

                EsocialFacade esocialFacade = EsocialFacade.getInstance();

                LoteEsocial loteEsocial = new LoteEsocial();
                loteEsocial.cliente = cliente;
                loteEsocial.Tipo = "2220";

                DataSet ds = esocialFacade.findEsocialByFilter(loteEsocial, Convert.ToDateTime(periodoCriacaoInicial.Text + " 00:00:00"), Convert.ToDateTime(periodoCriacaoFinal.Text + " 23:59:59"));

                dgvLote.DataSource = ds.Tables["Lotes"].DefaultView;

                // montando ids das colunas

                dgvLote.Columns[0].HeaderText = "id_esocial_lote";
                dgvLote.Columns[0].Visible = false;

                dgvLote.Columns[1].HeaderText = "Código";
                dgvLote.Columns[2].HeaderText = "Razão Social";
                dgvLote.Columns[3].HeaderText = "CNPJ";
                dgvLote.Columns[4].HeaderText = "Tipo";
                dgvLote.Columns[5].HeaderText = "Data Criação";
                dgvLote.Columns[6].HeaderText = "Data de Movimento";

                textTotal.Text = dgvLote.RowCount.ToString();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }

        private void dgvLote_DoubleClick(object sender, EventArgs e)
        {
            try
            {
                if (dgvLote.CurrentRow == null)
                    throw new Exception("Selecione uma linha");

                this.Cursor = Cursors.WaitCursor;

                LoteEsocial loteEsocial = new LoteEsocial();
                loteEsocial.Id = (Int64)dgvLote.CurrentRow.Cells[0].Value;
                EsocialFacade esocialFacade = EsocialFacade.getInstance();
                DataSet atendimentos = esocialFacade.findAtendimentosByLote(loteEsocial);

                //chamar o form para exibir os atendimentos pertencentes ao lote

                frmEsocialLoteDetalhe frmEsocialLoteDetalhe = new frmEsocialLoteDetalhe(atendimentos);
                frmEsocialLoteDetalhe.ShowDialog();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }

        private T Deserialize<T>(string input) where T : class
        {
            XmlSerializer ser = new XmlSerializer(typeof(T));

            XmlWriterSettings settings = new XmlWriterSettings()
            {
                Encoding = new UnicodeEncoding(false, false),
                Indent = false,
                OmitXmlDeclaration = false
            };

            using (StringReader sr = new StringReader(input))
            {
                return (T)ser.Deserialize(sr);
            }
        }

        private string Serialize<T>(T ObjectToSerialize)
        {
            XmlSerializer xmlSerializer = new XmlSerializer(ObjectToSerialize.GetType());
            var xmlnsEmpty = new XmlSerializerNamespaces();
            
            if (string.Equals(tipoGeracaoArquivo, "INDIVIDUAL"))
            {
                xmlnsEmpty.Add("", "http://www.esocial.gov.br/schema/evt/evtMonit/v_S_01_02_00");
            }
            else
                xmlnsEmpty.Add("", "");
            

            using (Utf8StringWriter textWriter = new Utf8StringWriter())
            {
                xmlSerializer.Serialize(textWriter, ObjectToSerialize, xmlnsEmpty);
                return textWriter.ToString().Replace("utf-8", "UTF-8");
            }
        }

        public sealed class Utf8StringWriter : StringWriter
        {
            public override Encoding Encoding
            {
                get
                {
                    return Encoding.UTF8;
                }
            }
        }

        public string GetTemporaryDirectory()
        {
            string tempFolder = Path.GetTempFileName();
            File.Delete(tempFolder);
            Directory.CreateDirectory(tempFolder);

            return tempFolder;
        }

        private void btnExcluir_Click(object sender, EventArgs e)
        {
            try
            {
                if (dgvLote.CurrentRow == null)
                    throw new Exception("Selecione um lote para excluir.");

                EsocialFacade esocialFacade = EsocialFacade.getInstance();

                if (MessageBox.Show("Deseja excluir o lote E-Social gerado?", "Confirmação", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                {
                    LoteEsocial esocialLoteExcluir = new LoteEsocial();
                    esocialLoteExcluir.Id = (long)dgvLote.CurrentRow.Cells[0].Value;
                    esocialFacade.deleteEsocialLote(esocialLoteExcluir);
                    MessageBox.Show("Lote Esocial excluído com sucesso.", "Confirmação", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    montaDataGrid();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }

        private void geracaoIndividual()
        {
            SWS.Modelo.modelo2220.ESocial esocial = new SWS.Modelo.modelo2220.ESocial();
            SWS.Modelo.modelo2220.EvtMonit evtMonit = new SWS.Modelo.modelo2220.EvtMonit();
            SWS.Modelo.modelo2220.IdeEvento ideEvento = new SWS.Modelo.modelo2220.IdeEvento();
            SWS.Modelo.modelo2220.IdeEmpregador ideEmpregador = new SWS.Modelo.modelo2220.IdeEmpregador();
            SWS.Modelo.modelo2220.IdeVinculo ideVinculo = new SWS.Modelo.modelo2220.IdeVinculo();
            SWS.Modelo.modelo2220.ExMedOcup exMedOcup = new Modelo.modelo2220.ExMedOcup();
            SWS.Modelo.modelo2220.Aso aso = new SWS.Modelo.modelo2220.Aso();
            SWS.Modelo.modelo2220.Exame exame = new SWS.Modelo.modelo2220.Exame();
            SWS.Modelo.modelo2220.Medico medico = new SWS.Modelo.modelo2220.Medico();
            SWS.Modelo.modelo2220.RespMonit respMonit = new SWS.Modelo.modelo2220.RespMonit();

            EsocialFacade esocialFacade = EsocialFacade.getInstance();
            AsoFacade asoFacade = AsoFacade.getInstance();
            PermissionamentoFacade permissionamentoFacade = PermissionamentoFacade.getInstance();

            bool gerarExameTipoString = false;

            try
            {
                if (dgvLote.CurrentRow == null)
                    throw new Exception("Selecione uma linha");

                diretorioTemporario = GetTemporaryDirectory();

                LoteEsocial loteEsocial = esocialFacade.findLoteEsocialById((long)dgvLote.CurrentRow.Cells[0].Value);

                List<String> cpfDuplicadoInLote = esocialFacade.cpfDuplicadosNoLote(loteEsocial);
                List<Aso> atendimentos = esocialFacade.findAtendimentosByLoteList(loteEsocial);
                Dictionary<string, LinkedList<Aso>> atendimentosDuplicados = new Dictionary<string, LinkedList<Aso>>();

                /* verificando se será gerado o arquivo com exportação para o esocial exames do tipo string ou inteiro */

                if (MessageBox.Show("Deseja gerar o arquivo com os códigos de exames em formato de string. Exemplo: 0180 ?", "Atenção", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button1 ) == DialogResult.Yes)
                {
                    gerarExameTipoString = true;
                }


                if (cpfDuplicadoInLote.Count > 0)
                {
                    foreach (Aso atendimento in atendimentos)
                    {
                        if (cpfDuplicadoInLote.Contains(atendimento.Funcionario.Cpf))
                        {

                            /* verificando se já existe essa chave no dicionario. Só irá continuar se não 
                             * existir a chave */
                            if (!atendimentosDuplicados.ContainsKey(atendimento.Funcionario.Cpf))
                            {
                                LinkedList<Aso> atendimentosDoMesmoCpf = new LinkedList<Aso>();

                                /* percorrendo a lista de atendimento para pegar todos os atendimentos desse cpf */
                                foreach (Aso atendimentoFuncionario in atendimentos)
                                {
                                    if (atendimentoFuncionario.Funcionario.Cpf == atendimento.Funcionario.Cpf)
                                        atendimentosDoMesmoCpf.AddLast(atendimentoFuncionario);

                                }

                                atendimentosDuplicados.Add(atendimento.Funcionario.Cpf, atendimentosDoMesmoCpf);
                            }

                        }

                    }
                }

                /* verificando os atendimentos duplicados na lista */

                this.Cursor = Cursors.WaitCursor;
                DateTime sequenciaData = DateTime.Now;
                try
                {
                    foreach (Aso atendimento in esocialFacade.findAtendimentosByLoteList(loteEsocial))
                    {
                        if (!atendimentosDuplicados.ContainsKey(atendimento.Funcionario.Cpf))
                        {
                            using (StreamWriter writer = new StreamWriter(diretorioTemporario + "//" + atendimento.Funcionario.Nome.Replace(" ", "_") + "_" + atendimento.Codigo + ".xml", false, Encoding.UTF8))
                            {
                                sequenciaData = sequenciaData.AddSeconds(1);
                                /* só adiciona elementos que não estão duplicados */

                                // Responsável pelo monitoramento

                                if (atendimento.Coordenador != null)
                                {
                                    respMonit.CpfResp = atendimento.Coordenador.Cpf.PadLeft(11, '0');
                                    respMonit.NmResp = atendimento.Coordenador.Nome;
                                    respMonit.NrCRM = atendimento.Coordenador.Crm;
                                    respMonit.UfCRM = atendimento.Coordenador.UfCrm;
                                }

                                //Empregador

                                ideEmpregador.TpInsc = atendimento.ClienteFuncaoFuncionario.ClienteFuncao.Cliente.tipoCliente().ToString();
                                /* somente para clientes tipo governo que deve ser colocado os 14 dígitos do cpf ou cnpj */
                                ideEmpregador.NrInsc = atendimento.ClienteFuncaoFuncionario.ClienteFuncao.Cliente.Cnpj.Length == 14
                                    ? atendimento.ClienteFuncaoFuncionario.ClienteFuncao.Cliente.Cnpj.Substring(0, 8)
                                    : atendimento.ClienteFuncaoFuncionario.ClienteFuncao.Cliente.Cnpj;


                                // Medico

                                if (atendimento.Examinador != null)
                                {
                                    medico.NmMed = atendimento.Examinador.Nome;
                                    medico.NrCRM = atendimento.Examinador.Crm;
                                    medico.UfCRM = atendimento.Examinador.UfCrm;
                                }

                                // ideVinculo

                                ideVinculo.CpfTrab = atendimento.ClienteFuncaoFuncionario.Funcionario.Cpf;
                                ideVinculo.Matricula = atendimento.ClienteFuncaoFuncionario.Matricula;
                                // ideVinculo.CodCateg = "101"; Não será preenchido até ser criado o vinculo com a tabela 1 do esocial;

                                // Exames
                                /* exames com caracteres especiais */
                                string[] examesEspeciais = { "0583", "0998", "0999", "1128", "1230", "1992", "1993", "1994", "1995", "1996", "1997", "1998", "1999", "9999" };

                                /* recuperando os exames do atendimento */
                                List<SWS.Modelo.modelo2220.Exame> exames = new List<SWS.Modelo.modelo2220.Exame>();
                                SWS.Modelo.modelo2220.Exame exameTemp;

                                /* recuperando a configuração para saber se deve ser aplicado na geração do arquivo */
                                string convertEsocialProcedimento;
                                PermissionamentoFacade.mapaConfiguracoes.TryGetValue(SWS.Resources.ConfigurationConstans.CONVERT_ESOCIAL_2220_PROCEDIMENTO, out convertEsocialProcedimento);

                                foreach (ClienteFuncaoExameASo exameExtra in asoFacade.findAllExameExtraByAsoInSituacao(atendimento, true, true, false, null, false))
                                {
                                    exameTemp = new Modelo.modelo2220.Exame();
                                    exameTemp.DtExm = String.Format("{0:yyyy-MM-dd}", exameExtra.DataExame);
                                    

                                    if (!Convert.ToBoolean(gerarExameTipoString))
                                    {
                                        exameTemp.ProcRealizado = Convert.ToInt32(exameExtra.ClienteFuncaoExame.Exame.CodigoTuss).ToString();
                                    }
                                    else
                                    {
                                        exameTemp.ProcRealizado = exameExtra.ClienteFuncaoExame.Exame.CodigoTuss;
                                    }
                                    
                                    
                                    if (Array.IndexOf(examesEspeciais, exameExtra.ClienteFuncaoExame.Exame.CodigoTuss) > -1)
                                    {
                                        exameTemp.ObsProc = exameExtra.ClienteFuncaoExame.Exame.Descricao;
                                    }

                                    /* TODO incluir inicialmente todos com 1. Avaliar como melhorar essa lógica */
                                    exameTemp.OrdExame = "1";

                                    /* TODO NÃO informar o resultado porque o funcionário ainda não teria autorizado o envio das informaçoes */
                                    if (!string.IsNullOrEmpty(exameExtra.Resultado))
                                        exameTemp.IndResult = string.Equals(exameExtra.Resultado, "N") ? "1" : string.Equals(exameExtra.Resultado, "A") ? "2" : string.Equals(exameExtra.Resultado, "E") ? "3" : "4";
                                    exames.Add(exameTemp);
                                }

                                foreach (GheFonteAgenteExameAso examesPcmso in asoFacade.findAllExamePcmsoByAsoInSituacao(atendimento, true, true, false, null, false))
                                {
                                    exameTemp = new Modelo.modelo2220.Exame();
                                    exameTemp.DtExm = String.Format("{0:yyyy-MM-dd}", examesPcmso.DataExame);

                                    if (!Convert.ToBoolean(gerarExameTipoString))
                                    {
                                        exameTemp.ProcRealizado = Convert.ToInt32(examesPcmso.GheFonteAgenteExame.Exame.CodigoTuss).ToString();
                                    }
                                    else
                                    {
                                        exameTemp.ProcRealizado = examesPcmso.GheFonteAgenteExame.Exame.CodigoTuss;
                                    }

                                    
                                    if (Array.IndexOf(examesEspeciais, examesPcmso.GheFonteAgenteExame.Exame.CodigoTuss) > -1)
                                    {
                                        exameTemp.ObsProc = examesPcmso.GheFonteAgenteExame.Exame.Descricao;
                                    }

                                    /* TODO incluir inicialmente todos com 1. Avaliar como melhorar essa lógica */
                                    exameTemp.OrdExame = "1";

                                    /* TODO NÃO informar o resultado porque o funcionário ainda não teria autorizado o envio das informaçoes */
                                    if (!string.IsNullOrEmpty(examesPcmso.Resultado))
                                        exameTemp.IndResult = string.Equals(examesPcmso.Resultado, "N") ? "1" : string.Equals(examesPcmso.Resultado, "A") ? "2" : string.Equals(examesPcmso.Resultado, "E") ? "3" : "4";
                                    exames.Add(exameTemp);
                                }

                                // ASO

                                aso.DtAso = String.Format("{0:yyyy-MM-dd}", atendimento.DataElaboracao);
                                /* TODO NÃO INFORMAR O CAMPO SITUAÇÃO DO ASO. Aguardar para saber como será informado isso com a autorização do colaborador */
                                if (!string.IsNullOrEmpty(atendimento.Conclusao))
                                    aso.ResAso = string.Equals(atendimento.Conclusao, "A") ? "1" : "2";

                                aso.Exame = exames;
                                if (atendimento.Examinador != null)
                                {
                                    aso.Medico = medico;
                                }



                                // Exame medico Ocupacional

                                exMedOcup.TpExameOcup = atendimento.Periodicidade.tipoPeriodicidadeEsocial().ToString();
                                exMedOcup.ASo = aso;
                                if (atendimento.Coordenador != null)
                                    exMedOcup.RespMonit = respMonit;

                                // ideEvento


                                /* indica que tipo de envio é realizado 1 para original e 2 para retificacao */
                                ideEvento.IndRetif = "1";
                                /* numero do recibo para o caso de retificacao */
                                // ideEvento.NrRecibo = "00";
                                /* tipo de ambiente */
                                ideEvento.TpAmb = "1";
                                /* tipo de emissão */
                                ideEvento.ProcEmi = "1";
                                /* versão do sistema */
                                Dictionary<string, string> configuracao = new Dictionary<string, string>();
                                string versao;
                                configuracao = permissionamentoFacade.findAllConfiguracao();
                                configuracao.TryGetValue(ConfigurationConstans.VERSAO_SISTEMA, out versao);
                                ideEvento.VerProc = versao;

                                // Evento de monitoramento

                                /* verificando se existe algum atendimento duplicado para o colaborador no lote */
                                /* A identificação única do evento (Id) é composta por 36
                                    caracteres, conforme o que segue:
                                    IDTNNNNNNNNNNNNNNAAAAMMDDHHMMSSQQQQQ
                                    ID - Texto Fixo "ID";
                                    T - Tipo de Inscrição do Empregador (1 - CNPJ; 2 - CPF);
                                    NNNNNNNNNNNNNN - Número do CNPJ ou CPF do
                                    empregador - Completar com zeros à direita. No caso de
                                    pessoas jurídicas, o CNPJ informado deve conter 8 ou 14
                                    posições de acordo com o enquadramento do contribuinte
                                    para preenchimento do campo {ideEmpregador/nrInsc} do
                                    evento S-1000, completando-se com zeros à direita, se
                                    necessário.
                                    AAAAMMDD - Ano, mês e dia da geração do evento;
                                    HHMMSS - Hora, minuto e segundo da geração do evento;
                                    QQQQQ - Número sequencial da chave. Incrementar
                                    somente quando ocorrer geração de eventos na mesma
                                    data/hora, completando com zeros à esquerda.
                                    OBS.: No caso de pessoas jurídicas, o CNPJ informado
                                    deverá conter 8 ou 14 posições de acordo com o
                                    enquadramento do contribuinte para preenchimento do
                                    campo {ideEmpregador/nrInsc} do evento S-1000,
                                    completando-se com zeros à direita, se necessário." */
                                StringBuilder stringId = new StringBuilder();
                                stringId.Append("ID");
                                stringId.Append(atendimento.ClienteFuncaoFuncionario.ClienteFuncao.Cliente.tipoCliente().ToString());

                                if (atendimento.ClienteFuncaoFuncionario.ClienteFuncao.Cliente.Cnpj.Length == 14)
                                {
                                    stringId.Append(atendimento.ClienteFuncaoFuncionario.ClienteFuncao.Cliente.Cnpj.Substring(0, 8).PadRight(14, '0'));
                                }
                                else
                                {
                                    stringId.Append(atendimento.ClienteFuncaoFuncionario.ClienteFuncao.Cliente.Cnpj.PadRight(14, '0'));
                                }


                                stringId.Append(String.Format("{0:yyyyMMddHHmmss}", sequenciaData));
                                stringId.Append("1".PadLeft(5, '0'));

                                evtMonit.Id = stringId.ToString();
                                evtMonit.IdeEvento = ideEvento;
                                evtMonit.IdeEmpregador = ideEmpregador;
                                evtMonit.IdeVinculo = ideVinculo;
                                evtMonit.ExMedOcup = exMedOcup;


                                // E-Social

                                esocial.EvtMonit = evtMonit;

                                String xml = Serialize(esocial);

                                writer.Write(xml);
                                writer.Flush();
                                /* zerando variávies auxiliares */
                                exames.Clear();
                            }
                        }

                    }

                    /* TODO inicio da criação dos atendimentos que estão duplicados */

                    foreach (KeyValuePair<string, LinkedList<Aso>> item in atendimentosDuplicados)
                    {
                        int iDic = 0;
                        /* interação entre a lista de atendimento duplicados */
                        foreach (Aso atendimentoColaborador in item.Value)
                        {
                            using (StreamWriter writer = new StreamWriter(diretorioTemporario + "//" + atendimentoColaborador.Funcionario.Nome + "_" + atendimentoColaborador.Codigo + ".xml", false, Encoding.UTF8))
                            {
                                sequenciaData = sequenciaData.AddSeconds(1);
                                /* só adiciona elementos que não estão duplicados */

                                // Responsável pelo monitoramento

                                if (atendimentoColaborador.Coordenador != null)
                                {
                                    respMonit.CpfResp = atendimentoColaborador.Coordenador.Cpf.PadLeft(11, '0');
                                    respMonit.NmResp = atendimentoColaborador.Coordenador.Nome;
                                    respMonit.NrCRM = atendimentoColaborador.Coordenador.Crm;
                                    respMonit.UfCRM = atendimentoColaborador.Coordenador.UfCrm;
                                }

                                //Empregador

                                ideEmpregador.TpInsc = atendimentoColaborador.ClienteFuncaoFuncionario.ClienteFuncao.Cliente.tipoCliente().ToString();

                                ideEmpregador.NrInsc = atendimentoColaborador.ClienteFuncaoFuncionario.ClienteFuncao.Cliente.Cnpj.Length == 14
                                    ? atendimentoColaborador.ClienteFuncaoFuncionario.ClienteFuncao.Cliente.Cnpj.Substring(0, 8)
                                    : atendimentoColaborador.ClienteFuncaoFuncionario.ClienteFuncao.Cliente.Cnpj;

                                // Medico

                                if (atendimentoColaborador.Examinador != null)
                                {
                                    medico.NmMed = atendimentoColaborador.Examinador.Nome;
                                    medico.NrCRM = atendimentoColaborador.Examinador.Crm;
                                    medico.UfCRM = atendimentoColaborador.Examinador.UfCrm;
                                }

                                // ideVinculo

                                ideVinculo.CpfTrab = atendimentoColaborador.ClienteFuncaoFuncionario.Funcionario.Cpf;
                                ideVinculo.Matricula = atendimentoColaborador.ClienteFuncaoFuncionario.Matricula;
                                // ideVinculo.CodCateg = "101"; Não será preenchido até ser criado o vinculo com a tabela 1 do esocial;

                                // Exame
                                /* exames com caracteres especiais */
                                string[] examesEspeciais = { "0583", "0998", "0999", "1128", "1230", "1992", "1993", "1994", "1995", "1996", "1997", "1998", "1999", "9999" };

                                /* recuperando os exames do atendimento */
                                List<SWS.Modelo.modelo2220.Exame> exames = new List<SWS.Modelo.modelo2220.Exame>();
                                SWS.Modelo.modelo2220.Exame exameTemp;

                                /* recuperando a configuração para saber se deve ser aplicado na geração do arquivo */
                                string convertEsocialProcedimento;
                                PermissionamentoFacade.mapaConfiguracoes.TryGetValue(SWS.Resources.ConfigurationConstans.CONVERT_ESOCIAL_2220_PROCEDIMENTO, out convertEsocialProcedimento);

                                foreach (ClienteFuncaoExameASo exameExtra in asoFacade.findAllExameExtraByAsoInSituacao(atendimentoColaborador, true, true, false, null, false))
                                {
                                    exameTemp = new Modelo.modelo2220.Exame();
                                    exameTemp.DtExm = String.Format("{0:yyyy-MM-dd}", exameExtra.DataExame);

                                    if (!Convert.ToBoolean(gerarExameTipoString))
                                    {
                                        exameTemp.ProcRealizado = Convert.ToInt32(exameExtra.ClienteFuncaoExame.Exame.CodigoTuss).ToString();
                                    }
                                    else
                                    {
                                        exameTemp.ProcRealizado = exameExtra.ClienteFuncaoExame.Exame.CodigoTuss;
                                    }

                                    
                                    if (Array.IndexOf(examesEspeciais, exameExtra.ClienteFuncaoExame.Exame.CodigoTuss) > -1)
                                    {
                                        exameTemp.ObsProc = exameExtra.ClienteFuncaoExame.Exame.Descricao;
                                    }

                                    /* TODO incluir inicialmente todos com 1. Avaliar como melhorar essa lógica */
                                    exameTemp.OrdExame = "1";

                                    /* TODO NÃO informar o resultado porque o funcionário ainda não teria autorizado o envio das informaçoes */
                                    if (!string.IsNullOrEmpty(exameExtra.Resultado))
                                        exameTemp.IndResult = string.Equals(exameExtra.Resultado, "N") ? "1" : string.Equals(exameExtra.Resultado, "A") ? "2" : string.Equals(exameExtra.Resultado, "E") ? "3" : "4";
                                    exames.Add(exameTemp);
                                }

                                foreach (GheFonteAgenteExameAso examesPcmso in asoFacade.findAllExamePcmsoByAsoInSituacao(atendimentoColaborador, true, true, false, null, false))
                                {
                                    exameTemp = new Modelo.modelo2220.Exame();
                                    exameTemp.DtExm = String.Format("{0:yyyy-MM-dd}", examesPcmso.DataExame);

                                    if (!Convert.ToBoolean(gerarExameTipoString))
                                    {
                                        exameTemp.ProcRealizado = Convert.ToInt32(examesPcmso.GheFonteAgenteExame.Exame.CodigoTuss).ToString();
                                    }
                                    else
                                    {
                                        exameTemp.ProcRealizado = examesPcmso.GheFonteAgenteExame.Exame.CodigoTuss;
                                    }

                                    
                                    if (Array.IndexOf(examesEspeciais, examesPcmso.GheFonteAgenteExame.Exame.CodigoTuss) > -1)
                                    {
                                        exameTemp.ObsProc = examesPcmso.GheFonteAgenteExame.Exame.Descricao;
                                    }

                                    /* TODO incluir inicialmente todos com 1. Avaliar como melhorar essa lógica */
                                    exameTemp.OrdExame = "1";
                                    /* TODO NÃO informar o resultado porque o funcionário ainda não teria autorizado o envio das informaçoes */
                                    if (!string.IsNullOrEmpty(examesPcmso.Resultado))
                                        exameTemp.IndResult = string.Equals(examesPcmso.Resultado, "N") ? "1" : string.Equals(examesPcmso.Resultado, "A") ? "2" : string.Equals(examesPcmso.Resultado, "E") ? "3" : "4";
                                    exames.Add(exameTemp);
                                }

                                // ASO

                                aso.DtAso = String.Format("{0:yyyy-MM-dd}", atendimentoColaborador.DataElaboracao);
                                /* TODO NÃO INFORMAR O CAMPO SITUAÇÃO DO ASO. Aguardar para saber como será informado isso com a autorização do colaborador */
                                if (!string.IsNullOrEmpty(atendimentoColaborador.Conclusao))
                                    aso.ResAso = string.Equals(atendimentoColaborador.Conclusao, "A") ? "1" : "2";
                                /* incluindo a coleção no aso */
                                aso.Exame = exames;
                                if (atendimentoColaborador.Examinador != null)
                                {
                                    aso.Medico = medico;
                                }

                                // Exame medico Ocupacional

                                exMedOcup.TpExameOcup = atendimentoColaborador.Periodicidade.tipoPeriodicidadeEsocial().ToString();
                                exMedOcup.ASo = aso;
                                if (atendimentoColaborador.Coordenador != null)
                                    exMedOcup.RespMonit = respMonit;

                                // ideEvento


                                /* indica que tipo de envio é realizado 1 para original e 2 para retificacao */
                                ideEvento.IndRetif = "1";
                                /* numero do recibo para o caso de retificacao */
                                // ideEvento.NrRecibo = "00";
                                /* tipo de ambiente */
                                ideEvento.TpAmb = "1";
                                /* tipo de emissão */
                                ideEvento.ProcEmi = "1";
                                /* versão do sistema */
                                Dictionary<string, string> configuracao = new Dictionary<string, string>();
                                string versao;
                                configuracao = permissionamentoFacade.findAllConfiguracao();
                                configuracao.TryGetValue(ConfigurationConstans.VERSAO_SISTEMA, out versao);
                                ideEvento.VerProc = versao;

                                // Evento de monitoramento

                                /* verificando se existe algum atendimento duplicado para o colaborador no lote */
                                /* A identificação única do evento (Id) é composta por 36
                                    caracteres, conforme o que segue:
                                    IDTNNNNNNNNNNNNNNAAAAMMDDHHMMSSQQQQQ
                                    ID - Texto Fixo "ID";
                                    T - Tipo de Inscrição do Empregador (1 - CNPJ; 2 - CPF);
                                    NNNNNNNNNNNNNN - Número do CNPJ ou CPF do
                                    empregador - Completar com zeros à direita. No caso de
                                    pessoas jurídicas, o CNPJ informado deve conter 8 ou 14
                                    posições de acordo com o enquadramento do contribuinte
                                    para preenchimento do campo {ideEmpregador/nrInsc} do
                                    evento S-1000, completando-se com zeros à direita, se
                                    necessário.
                                    AAAAMMDD - Ano, mês e dia da geração do evento;
                                    HHMMSS - Hora, minuto e segundo da geração do evento;
                                    QQQQQ - Número sequencial da chave. Incrementar
                                    somente quando ocorrer geração de eventos na mesma
                                    data/hora, completando com zeros à esquerda.
                                    OBS.: No caso de pessoas jurídicas, o CNPJ informado
                                    deverá conter 8 ou 14 posições de acordo com o
                                    enquadramento do contribuinte para preenchimento do
                                    campo {ideEmpregador/nrInsc} do evento S-1000,
                                    completando-se com zeros à direita, se necessário." */
                                StringBuilder stringId = new StringBuilder();
                                stringId.Append("ID");
                                stringId.Append(atendimentoColaborador.ClienteFuncaoFuncionario.ClienteFuncao.Cliente.tipoCliente().ToString());

                                if (atendimentoColaborador.ClienteFuncaoFuncionario.ClienteFuncao.Cliente.Cnpj.Length == 14)
                                {
                                    stringId.Append(atendimentoColaborador.ClienteFuncaoFuncionario.ClienteFuncao.Cliente.Cnpj.Substring(0, 8).PadRight(14, '0'));
                                }
                                else
                                {
                                    stringId.Append(atendimentoColaborador.ClienteFuncaoFuncionario.ClienteFuncao.Cliente.Cnpj.PadRight(14, '0'));
                                }

                                stringId.Append(String.Format("{0:yyyyMMddHHmmss}", sequenciaData));
                                stringId.Append((iDic + 1).ToString().PadLeft(5, '0'));

                                evtMonit.Id = stringId.ToString();
                                evtMonit.IdeEvento = ideEvento;
                                evtMonit.IdeEmpregador = ideEmpregador;
                                evtMonit.IdeVinculo = ideVinculo;
                                evtMonit.ExMedOcup = exMedOcup;


                                // E-Social

                                esocial.EvtMonit = evtMonit;

                                String xml = Serialize(esocial);

                                writer.Write(xml);
                                writer.Flush();
                                /* zerando variávies auxiliares */
                                iDic++;
                                exames.Clear();
                            }
                        }
                    }

                    /* gerar arquivo de saída do zip */
                    FolderBrowserDialog fbd1 = new FolderBrowserDialog();
                    fbd1.Description = "Selecione uma pasta onde o arquivo de exportação do E-Social evento 2220 será criado.";
                    fbd1.RootFolder = Environment.SpecialFolder.MyComputer;
                    fbd1.ShowNewFolderButton = true;

                    DialogResult dr = fbd1.ShowDialog();
                    //Exibe a caixa de diálogo
                    if (dr == System.Windows.Forms.DialogResult.OK)
                    {
                        //Exibe a pasta selecionada
                        diretorioExportacao = fbd1.SelectedPath;
                    }

                    /* lista todos os arquivos do diretorio temporário */
                    arquivos = Directory.GetFiles(diretorioTemporario);

                    /* setando o local de destino dos arquivos */
                    string localDestino = diretorioExportacao + "\\" + dgvLote.CurrentRow.Cells[1].Value.ToString() + ".zip";

                    /* criando arquivo zip */
                    SWS.Helper.FileHelper.criarArquivoZip(arquivos, localDestino);

                    MessageBox.Show("Arquivo gerado com sucesso", "Confirmação", MessageBoxButtons.OK, MessageBoxIcon.Information);

                }
                catch (Exception ex)
                {
                    MessageBox.Show("Não foi possível salvar o seu arquivo. Informe o erro: " + ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
                finally
                {
                    this.Cursor = Cursors.Default;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }

        }

        private void geracaoAgrupado()
        {
            SWS.Modelo.modelo2220Agrupado.Evento evento = new Modelo.modelo2220Agrupado.Evento();
            SWS.Modelo.modelo2220Agrupado.Eventos eventos = new Modelo.modelo2220Agrupado.Eventos();
            
            eventos.evento = new List<Modelo.modelo2220Agrupado.Evento>();
            
            SWS.Modelo.modelo2220Agrupado.ESocial esocial = new SWS.Modelo.modelo2220Agrupado.ESocial();
            SWS.Modelo.modelo2220Agrupado.EvtMonit evtMonit = new SWS.Modelo.modelo2220Agrupado.EvtMonit();
            SWS.Modelo.modelo2220Agrupado.IdeEvento ideEvento = new SWS.Modelo.modelo2220Agrupado.IdeEvento();
            SWS.Modelo.modelo2220Agrupado.IdeEmpregador ideEmpregador = new SWS.Modelo.modelo2220Agrupado.IdeEmpregador();
            SWS.Modelo.modelo2220Agrupado.IdeVinculo ideVinculo = new SWS.Modelo.modelo2220Agrupado.IdeVinculo();
            SWS.Modelo.modelo2220Agrupado.ExMedOcup exMedOcup = new Modelo.modelo2220Agrupado.ExMedOcup();
            SWS.Modelo.modelo2220Agrupado.Aso aso = new SWS.Modelo.modelo2220Agrupado.Aso();
            SWS.Modelo.modelo2220Agrupado.Exame exame = new SWS.Modelo.modelo2220Agrupado.Exame();
            SWS.Modelo.modelo2220Agrupado.Medico medico = new SWS.Modelo.modelo2220Agrupado.Medico();
            SWS.Modelo.modelo2220Agrupado.RespMonit respMonit = new SWS.Modelo.modelo2220Agrupado.RespMonit();

            EsocialFacade esocialFacade = EsocialFacade.getInstance();
            AsoFacade asoFacade = AsoFacade.getInstance();
            PermissionamentoFacade permissionamentoFacade = PermissionamentoFacade.getInstance();

            bool gerarExameTipoString = false;

            
            try
            {

                if (dgvLote.CurrentRow == null)
                    throw new Exception("Selecione uma linha");

                diretorioTemporario = GetTemporaryDirectory();

                LoteEsocial loteEsocial = esocialFacade.findLoteEsocialById((long)dgvLote.CurrentRow.Cells[0].Value);

                List<Aso> atendimentos = esocialFacade.findAtendimentosByLoteList(loteEsocial);

                this.Cursor = Cursors.WaitCursor;
                DateTime sequenciaData = DateTime.Now;

                /* verificando se será gerado o arquivo com exportação para o esocial exames do tipo string ou inteiro */

                if (MessageBox.Show("Deseja gerar o arquivo com os códigos de exames em formato de string. Exemplo: 0180 ?", "Atenção", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button1) == DialogResult.Yes)
                {
                    gerarExameTipoString = true;
                }

                try
                {
                    using (StreamWriter writer = new StreamWriter(diretorioTemporario + "//" + loteEsocial.cliente.RazaoSocial + "_" + loteEsocial.codigo + ".xml", false, Encoding.UTF8))
                    {
                        List<Aso> asos = esocialFacade.findAtendimentosByLoteList(loteEsocial);
                        foreach (Aso atendimento in asos)
                        {

                            evento = new Modelo.modelo2220Agrupado.Evento();
                            ideVinculo = new Modelo.modelo2220Agrupado.IdeVinculo();
                            aso = new Modelo.modelo2220Agrupado.Aso();
                            exMedOcup = new Modelo.modelo2220Agrupado.ExMedOcup();
                            evtMonit = new Modelo.modelo2220Agrupado.EvtMonit();
                            esocial = new Modelo.modelo2220Agrupado.ESocial();

                            sequenciaData = sequenciaData.AddSeconds(1);

                            // Responsável pelo monitoramento
                            if (atendimento.Coordenador != null)
                            {
                                respMonit.CpfResp = atendimento.Coordenador.Cpf.PadLeft(11, '0');
                                respMonit.NmResp = atendimento.Coordenador.Nome;
                                respMonit.NrCRM = atendimento.Coordenador.Crm;
                                respMonit.UfCRM = atendimento.Coordenador.UfCrm;
                            }

                            //Empregador
                            ideEmpregador.TpInsc = atendimento.ClienteFuncaoFuncionario.ClienteFuncao.Cliente.tipoCliente().ToString();
                            /* somente para clientes tipo governo que deve ser colocado os 14 dígitos do cpf ou cnpj */
                            ideEmpregador.NrInsc = atendimento.ClienteFuncaoFuncionario.ClienteFuncao.Cliente.Cnpj.Length == 14
                                ? atendimento.ClienteFuncaoFuncionario.ClienteFuncao.Cliente.Cnpj.Substring(0, 8)
                                : atendimento.ClienteFuncaoFuncionario.ClienteFuncao.Cliente.Cnpj;


                            // Medico
                            if (atendimento.Examinador != null)
                            {
                                medico.NmMed = atendimento.Examinador.Nome;
                                medico.NrCRM = atendimento.Examinador.Crm;
                                medico.UfCRM = atendimento.Examinador.UfCrm;
                            }

                            // ideVinculo
                            ideVinculo.CpfTrab = atendimento.ClienteFuncaoFuncionario.Funcionario.Cpf;
                            ideVinculo.Matricula = atendimento.ClienteFuncaoFuncionario.Matricula;
                            // ideVinculo.CodCateg = "101"; Não será preenchido até ser criado o vinculo com a tabela 1 do esocial;

                            // Exame
                            /* exames com caracteres especiais */
                            string[] examesEspeciais = { "0583", "0998", "0999", "1128", "1230", "1992", "1993", "1994", "1995", "1996", "1997", "1998", "1999", "9999" };

                            /* recuperando os exames do atendimento */
                            List<SWS.Modelo.modelo2220Agrupado.Exame> exames = new List<SWS.Modelo.modelo2220Agrupado.Exame>();
                            SWS.Modelo.modelo2220Agrupado.Exame exameTemp;

                            /* recuperando a configuração para saber se deve ser aplicado na geração do arquivo */
                            string convertEsocialProcedimento;
                            PermissionamentoFacade.mapaConfiguracoes.TryGetValue(SWS.Resources.ConfigurationConstans.CONVERT_ESOCIAL_2220_PROCEDIMENTO, out convertEsocialProcedimento);

                            foreach (ClienteFuncaoExameASo exameExtra in asoFacade.findAllExameExtraByAsoInSituacao(atendimento, true, true, false, null, false))
                            {
                                exameTemp = new Modelo.modelo2220Agrupado.Exame();
                                exameTemp.DtExm = String.Format("{0:yyyy-MM-dd}", exameExtra.DataExame);

                                if (!Convert.ToBoolean(gerarExameTipoString))
                                {
                                    exameTemp.ProcRealizado = Convert.ToInt32(exameExtra.ClienteFuncaoExame.Exame.CodigoTuss).ToString();
                                }
                                else
                                {
                                    exameTemp.ProcRealizado = exameExtra.ClienteFuncaoExame.Exame.CodigoTuss;
                                }

                                
                                if (Array.IndexOf(examesEspeciais, exameExtra.ClienteFuncaoExame.Exame.CodigoTuss) > -1)
                                {
                                    exameTemp.ObsProc = exameExtra.ClienteFuncaoExame.Exame.Descricao;
                                }

                                /* TODO incluir inicialmente todos com 1. Avaliar como melhorar essa lógica */
                                exameTemp.OrdExame = "1";

                                /* TODO NÃO informar o resultado porque o funcionário ainda não teria autorizado o envio das informaçoes */
                                if (!string.IsNullOrEmpty(exameExtra.Resultado))
                                    exameTemp.IndResult = string.Equals(exameExtra.Resultado, "N") ? "1" : string.Equals(exameExtra.Resultado, "A") ? "2" : string.Equals(exameExtra.Resultado, "E") ? "3" : "4";
                                exames.Add(exameTemp);
                            }

                            foreach (GheFonteAgenteExameAso examesPcmso in asoFacade.findAllExamePcmsoByAsoInSituacao(atendimento, true, true, false, null, false))
                            {
                                exameTemp = new Modelo.modelo2220Agrupado.Exame();
                                exameTemp.DtExm = String.Format("{0:yyyy-MM-dd}", examesPcmso.DataExame);

                                if (!Convert.ToBoolean(gerarExameTipoString))
                                {
                                    exameTemp.ProcRealizado = Convert.ToInt32(examesPcmso.GheFonteAgenteExame.Exame.CodigoTuss).ToString();
                                }
                                else
                                {
                                    exameTemp.ProcRealizado = examesPcmso.GheFonteAgenteExame.Exame.CodigoTuss;
                                }

                                
                                if (Array.IndexOf(examesEspeciais, examesPcmso.GheFonteAgenteExame.Exame.CodigoTuss) > -1)
                                {
                                    exameTemp.ObsProc = examesPcmso.GheFonteAgenteExame.Exame.Descricao;
                                }

                                /* TODO incluir inicialmente todos com 1. Avaliar como melhorar essa lógica */
                                exameTemp.OrdExame = "1";

                                /* TODO NÃO informar o resultado porque o funcionário ainda não teria autorizado o envio das informaçoes */
                                if (!string.IsNullOrEmpty(examesPcmso.Resultado))
                                    exameTemp.IndResult = string.Equals(examesPcmso.Resultado, "N") ? "1" : string.Equals(examesPcmso.Resultado, "A") ? "2" : string.Equals(examesPcmso.Resultado, "E") ? "3" : "4";
                                exames.Add(exameTemp);
                            }

                            // ASO

                            aso.DtAso = String.Format("{0:yyyy-MM-dd}", atendimento.DataElaboracao);
                            /* TODO NÃO INFORMAR O CAMPO SITUAÇÃO DO ASO. Aguardar para saber como será informado isso com a autorização do colaborador */
                            if (!string.IsNullOrEmpty(atendimento.Conclusao))
                                aso.ResAso = string.Equals(atendimento.Conclusao, "A") ? "1" : "2";

                            aso.Exame = exames;
                            if (atendimento.Examinador != null)
                            {
                                aso.Medico = medico;
                            }

                            // Exame medico Ocupacional

                            exMedOcup.TpExameOcup = atendimento.Periodicidade.tipoPeriodicidadeEsocial().ToString();
                            exMedOcup.ASo = aso;
                            if (atendimento.Coordenador != null)
                                exMedOcup.RespMonit = respMonit;

                            // ideEvento
                            /* indica que tipo de envio é realizado 1 para original e 2 para retificacao */
                            ideEvento.IndRetif = "1";
                            /* numero do recibo para o caso de retificacao */
                            // ideEvento.NrRecibo = "00";
                            /* tipo de ambiente */
                            ideEvento.TpAmb = "1";
                            /* tipo de emissão */
                            ideEvento.ProcEmi = "1";
                            /* versão do sistema */
                            Dictionary<string, string> configuracao = new Dictionary<string, string>();
                            string versao;
                            configuracao = permissionamentoFacade.findAllConfiguracao();
                            configuracao.TryGetValue(ConfigurationConstans.VERSAO_SISTEMA, out versao);
                            ideEvento.VerProc = versao;

                            // Evento de monitoramento
                            StringBuilder stringId = new StringBuilder();
                            stringId.Append("ID");
                            stringId.Append(atendimento.ClienteFuncaoFuncionario.ClienteFuncao.Cliente.tipoCliente().ToString());

                            if (atendimento.ClienteFuncaoFuncionario.ClienteFuncao.Cliente.Cnpj.Length == 14)
                            {
                                stringId.Append(atendimento.ClienteFuncaoFuncionario.ClienteFuncao.Cliente.Cnpj.Substring(0, 8).PadRight(14, '0'));
                            }
                            else
                            {
                                stringId.Append(atendimento.ClienteFuncaoFuncionario.ClienteFuncao.Cliente.Cnpj.PadRight(14, '0'));
                            }

                            stringId.Append(String.Format("{0:yyyyMMddHHmmss}", sequenciaData));
                            stringId.Append("1".PadLeft(5, '0'));

                            evtMonit.Id = stringId.ToString();
                            evtMonit.IdeEvento = ideEvento;
                            evtMonit.IdeEmpregador = ideEmpregador;
                            evtMonit.IdeVinculo = ideVinculo;
                            evtMonit.ExMedOcup = exMedOcup;


                            // E-Social
                            esocial.EvtMonit = evtMonit;
                            /* alteracao para grupo de atendimento */
                            evento.eSocial = esocial;
                            evento.Id = evtMonit.Id;
                            eventos.evento.Add(evento);

                        }

                        /* gravando a coleção */
                        String xml = Serialize(eventos);

                        writer.Write(xml);
                        writer.Flush();
                    }

                    /* gerar arquivo de saída do zip */
                    FolderBrowserDialog fbd1 = new FolderBrowserDialog();
                    fbd1.Description = "Selecione uma pasta onde o arquivo de exportação do E-Social evento 2220 será criado.";
                    fbd1.RootFolder = Environment.SpecialFolder.MyComputer;
                    fbd1.ShowNewFolderButton = true;

                    DialogResult dr = fbd1.ShowDialog();
                    //Exibe a caixa de diálogo
                    if (dr == System.Windows.Forms.DialogResult.OK)
                    {
                        //Exibe a pasta selecionada
                        diretorioExportacao = fbd1.SelectedPath;
                    }

                    /* lista todos os arquivos do diretorio temporário */
                    arquivos = Directory.GetFiles(diretorioTemporario);

                    /* setando o local de destino dos arquivos */
                    string localDestino = diretorioExportacao + "\\" + dgvLote.CurrentRow.Cells[1].Value.ToString() + ".zip";

                    /* criando arquivo zip */
                    SWS.Helper.FileHelper.criarArquivoZip(arquivos, localDestino);

                    MessageBox.Show("Arquivo gerado com sucesso", "Confirmação", MessageBoxButtons.OK, MessageBoxIcon.Information);

                }
                catch (Exception ex)
                {
                    MessageBox.Show("Não foi possível salvar o seu arquivo. Informe o erro: " + ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
                finally
                {
                    this.Cursor = Cursors.Default;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }


    }
}
