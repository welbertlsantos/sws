﻿using SWS.Entidade;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace SWS.View
{
    public partial class frmAtendimentoGheAvulsoAlterar : frmAtendimentoGheAvulso
    {
        public frmAtendimentoGheAvulsoAlterar(GheSetorAvulso gheSetorAvulso)
        {
            InitializeComponent();
            this.textGhe.Text = gheSetorAvulso.Ghe;
            this.textSetor.Text = gheSetorAvulso.Setor;
            this.GheSetor = gheSetorAvulso;
        }

        protected override void btnGravar_Click(object sender, EventArgs e)
        {
            try
            {
                if (!validaCampo())
                {
                    GheSetor.Ghe = textGhe.Text.Trim();
                    GheSetor.Setor = textSetor.Text.Trim();
                    this.Close();
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }
    }
}
