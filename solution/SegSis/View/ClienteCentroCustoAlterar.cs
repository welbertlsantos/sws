﻿using SWS.Entidade;
using SWS.View.Resources;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace SWS.View
{
    public partial class frmClienteCentroCustoAlterar : frmClienteCentroCustoIncluir
    {
        public frmClienteCentroCustoAlterar(CentroCusto centroCusto) :base()
        {
            InitializeComponent();
            this.CentroCusto = centroCusto;
            textCentroCusto.Text = CentroCusto.Descricao;
        }

        protected override void btnGravar_Click(object sender, EventArgs e)
        {
            try
            {
                if (string.IsNullOrEmpty(textCentroCusto.Text.Trim()))
                    throw new Exception("O campo centro de custo não pode ser vazio.");

                CentroCusto.Descricao = textCentroCusto.Text;
                this.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }


    }
}
