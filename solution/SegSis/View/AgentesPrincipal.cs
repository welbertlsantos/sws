﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using SWS.Facade;
using SWS.View.ViewHelper;
using SWS.ViewHelper;
using SWS.Entidade;
using SWS.View.Resources;

namespace SWS.View
{
    public partial class frmAgentesPrincipal : frmTemplate
    {
        private Agente agente;

        public frmAgentesPrincipal()
        {
            InitializeComponent();
            validaPermissoes();
            ComboHelper.carregaComboRisco(cbTipo);
            ComboHelper.comboSituacao(cbSituacao);
            ActiveControl = textAgente;
        }

        private void validaPermissoes()
        {
            PermissionamentoFacade permissionamentoFacade = PermissionamentoFacade.getInstance();

            bt_alterar.Enabled = permissionamentoFacade.hasPermission("AGENTE", "ALTERAR");
            bt_incluir.Enabled = permissionamentoFacade.hasPermission("AGENTE", "INCLUIR");
            bt_excluir.Enabled = permissionamentoFacade.hasPermission("AGENTE", "EXCLUIR");
            bt_detalhar.Enabled = permissionamentoFacade.hasPermission("AGENTE", "DETALHAR");
            btn_reativar.Enabled = permissionamentoFacade.hasPermission("AGENTE", "REATIVAR");
        }

        private void bt_fechar_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void bt_limpar_Click(object sender, EventArgs e)
        {
            this.textAgente.Text = String.Empty;
            ComboHelper.carregaComboRisco(cbTipo);
            ComboHelper.comboSituacao(cbSituacao);
            this.dgvAgente.Columns.Clear();
        }

        private void bt_pesquisar_Click(object sender, EventArgs e)
        {
            try
            {
                agente = new Agente(null, cbTipo.SelectedIndex != 0 ? new Risco(Convert.ToInt64(((SelectItem)cbTipo.SelectedItem).Valor), (String)((SelectItem)cbTipo.SelectedItem).Nome) : null, textAgente.Text.Trim(), String.Empty, String.Empty, String.Empty, ((SelectItem)cbSituacao.SelectedItem).Valor, false, string.Empty, string.Empty, string.Empty);
                montaDataGrid();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        public void montaDataGrid()
        {
            try
            {

                EstudoFacade pcmsoFacade = EstudoFacade.getInstance();
                
                DataSet ds = pcmsoFacade.findAgenteByFilter(agente);

                dgvAgente.DataSource = ds.Tables["Agentes"].DefaultView;

                dgvAgente.Columns[0].HeaderText = "id_agente";
                dgvAgente.Columns[0].Visible = false;

                dgvAgente.Columns[1].HeaderText = "id_risco";
                dgvAgente.Columns[1].Visible = false;

                dgvAgente.Columns[2].HeaderText = "Descrição";
                dgvAgente.Columns[3].HeaderText = "Trajetória";
                dgvAgente.Columns[4].HeaderText = "Danos";
                dgvAgente.Columns[5].HeaderText = "Limites";
                dgvAgente.Columns[6].HeaderText = "Situação";
                dgvAgente.Columns[6].Visible = false;
                dgvAgente.Columns[7].HeaderText = "Risco";
                dgvAgente.Columns[7].DisplayIndex = 3;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error); 
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }

        private void bt_incluir_Click(object sender, EventArgs e)
        {
            frmAgentesIncluir formAgenteIncluir = new frmAgentesIncluir();
            formAgenteIncluir.ShowDialog();

            if (formAgenteIncluir.Agente != null)
            {
                agente = new Agente();  
                montaDataGrid();
            }
        }

        private void bt_alterar_Click(object sender, EventArgs e)
        {
            try
            {
                if (dgvAgente.CurrentRow == null)
                    throw new Exception("Selecione uma linha.");

                EstudoFacade pcmsoFacade = EstudoFacade.getInstance();
                
                agente = pcmsoFacade.findAgenteById((Int64)dgvAgente.CurrentRow.Cells[0].Value);

                if (agente.Privado)
                    throw new Exception(("Não é possível Alterar o agente. O agente é privado do sistema."));
                    
                if (!String.Equals(agente.Situacao, ApplicationConstants.ATIVO))
                    throw new Exception("Somente agente que esteja ativo pode ser alterado.");

                if (pcmsoFacade.findGheFonteAgenteByAgente(agente))
                    throw new Exception("Não é possível Alterar o agente. O agente já foi usado em um estudo.");

                frmAgentesAlterar alterarAgente = new frmAgentesAlterar(agente);
                alterarAgente.ShowDialog();
                
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
       }

        private void bt_excluir_Click(object sender, EventArgs e)
        {
            try
            {
                if (dgvAgente.CurrentRow == null)
                    throw new Exception("Selecione uma linha.");
                
                EstudoFacade pcmsoFacade = EstudoFacade.getInstance();

                if (MessageBox.Show("Gostaria realmente de excluir o agente.", "Confirmação", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                {

                    if (!String.Equals(dgvAgente.CurrentRow.Cells[6].Value.ToString(), ApplicationConstants.ATIVO))
                        throw new Exception("Somente agente que esteja ativo pode ser excluído.");

                    agente = pcmsoFacade.findAgenteById((Int64)dgvAgente.CurrentRow.Cells[0].Value);
                    agente.Situacao = ApplicationConstants.DESATIVADO;

                    pcmsoFacade.updateAgente(agente);

                    MessageBox.Show("Agente excluido com sucesso!", "Confirmação", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    agente = new Agente();
                    montaDataGrid();
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void bt_detalhar_Click(object sender, EventArgs e)
        {
            try
            {
                if (dgvAgente.CurrentRow == null)
                    throw new Exception("Selecione uma linha.");
                
                EstudoFacade pcmsoFacade = EstudoFacade.getInstance();

                agente = pcmsoFacade.findAgenteById((Int64)dgvAgente.CurrentRow.Cells[0].Value);

                frmAgentesDetalhar detalharAgente = new frmAgentesDetalhar(agente);
                detalharAgente.ShowDialog();

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btn_reativar_Click(object sender, EventArgs e)
        {
            try
            {
                if (dgvAgente.CurrentRow == null)
                    throw new Exception("Selecione uma linha.");
                
                EstudoFacade pcmsoFacade = EstudoFacade.getInstance();

                if (MessageBox.Show("Deseja reativar o agente selecionado?", "Confirmação", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                {
                    if (!String.Equals(dgvAgente.CurrentRow.Cells[6].Value.ToString(), ApplicationConstants.DESATIVADO))
                        throw new Exception("Somente agente que esteja inativo pode ser reativado.");
                    
                    agente = pcmsoFacade.findAgenteById((Int64)dgvAgente.CurrentRow.Cells[0].Value);
                    agente.Situacao = ApplicationConstants.ATIVO;

                    pcmsoFacade.updateAgente(agente);

                    MessageBox.Show("Agente reativado com sucesso.", "Confirmação", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    agente = new Agente();
                    montaDataGrid();
                    
                }
                
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void dgvAgente_RowPrePaint(object sender, DataGridViewRowPrePaintEventArgs e)
        {
            DataGridView dgv = sender as DataGridView;

            if (String.Equals(dgv.Rows[e.RowIndex].Cells[6].Value, ApplicationConstants.DESATIVADO))
                dgv.Rows[e.RowIndex].DefaultCellStyle.ForeColor = Color.Red;
        }

        private void dgvAgente_CellMouseDoubleClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            try
            {
                bt_alterar.PerformClick();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }



    }
}
