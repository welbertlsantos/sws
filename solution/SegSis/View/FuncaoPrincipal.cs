﻿using SWS.Entidade;
using SWS.Facade;
using SWS.View.Resources;
using SWS.View.ViewHelper;
using SWS.ViewHelper;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace SWS.View
{
    public partial class frmFuncaoPrincipal : frmTemplate
    {
        Funcao funcao;
        
        public frmFuncaoPrincipal()
        {
            InitializeComponent();
            ComboHelper.comboSituacao(cbSituacao);
            ActiveControl = textFuncao;
            validaPermissoes();
            
        }

        private void btnIncluir_Click(object sender, EventArgs e)
        {
            try
            {
                frmFuncaoIncluir incluirFuncao = new frmFuncaoIncluir();
                incluirFuncao.ShowDialog();

                if (incluirFuncao.Funcao != null)
                {
                    funcao = new Funcao();
                    montaDataGrid();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }


        }

        private void btnAlterar_Click(object sender, EventArgs e)
        {
            try
            {
                if (dgvFuncao.CurrentRow == null)
                    throw new Exception("Selecione uma linha.");

                EstudoFacade pcmsoFacade = EstudoFacade.getInstance();
                Funcao funcaoAlterar = pcmsoFacade.findFuncaoById((long)dgvFuncao.CurrentRow.Cells[0].Value);

                frmFuncaoAlterar alterarFuncao = new frmFuncaoAlterar(funcaoAlterar);
                alterarFuncao.ShowDialog();

                funcao = new Funcao();
                montaDataGrid();

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }

        private void btnPesquisar_Click(object sender, EventArgs e)
        {
            try
            {
                funcao = new Funcao(null, textFuncao.Text, string.Empty, ((SelectItem)cbSituacao.SelectedItem).Valor, string.Empty);
                montaDataGrid();
                textFuncao.Focus();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }

        private void btnReativar_Click(object sender, EventArgs e)
        {
            try
            {
                if (dgvFuncao.CurrentRow == null)
                    throw new Exception("Selecione uma linha.");

                EstudoFacade pcmsoFacade = EstudoFacade.getInstance();
                Funcao funcaoReativar = pcmsoFacade.findFuncaoById((long)dgvFuncao.CurrentRow.Cells[0].Value);

                if (!string.Equals(funcaoReativar.Situacao, ApplicationConstants.DESATIVADO))
                    throw new Exception("Somente funções inativas podem ser reativadas.");

                if (MessageBox.Show("Deseja reativar a função selecionada?", "Atenção", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                {
                    funcaoReativar.Situacao = ApplicationConstants.ATIVO;
                    pcmsoFacade.updateFuncao(funcaoReativar);
                    MessageBox.Show("Função reativada com sucesso.", "Confirmação", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    funcao = new Funcao();
                    montaDataGrid();
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }

        private void btnDetalhar_Click(object sender, EventArgs e)
        {
            try
            {
                if (dgvFuncao.CurrentRow == null)
                    throw new Exception("Selecione uma linha.");

                EstudoFacade pcmsoFacade = EstudoFacade.getInstance();

                frmFuncaoDetalhar detalharFuncao = new frmFuncaoDetalhar(pcmsoFacade.findFuncaoById((long)dgvFuncao.CurrentRow.Cells[0].Value));
                detalharFuncao.ShowDialog();

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }

        private void btnExcluir_Click(object sender, EventArgs e)
        {
            try
            {
                if (dgvFuncao.CurrentRow == null)
                    throw new Exception("Selecione uma linha.");

                EstudoFacade pcmsoFacade = EstudoFacade.getInstance();
                Funcao funcaoExcluir = pcmsoFacade.findFuncaoById((long)dgvFuncao.CurrentRow.Cells[0].Value);

                if (!string.Equals(funcaoExcluir.Situacao, ApplicationConstants.ATIVO))
                    throw new Exception("Somente funções ativas podem ser excluídas.");

                if (MessageBox.Show("Deseja excluir a função selecionada?", "Atenção", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                {
                    funcaoExcluir.Situacao = ApplicationConstants.DESATIVADO;
                    pcmsoFacade.updateFuncao(funcaoExcluir);
                    MessageBox.Show("Função excluída com sucesso.", "Confirmação", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    funcao = new Funcao();
                    montaDataGrid();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }

        private void btnFechar_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void dgvFuncao_RowPrePaint(object sender, DataGridViewRowPrePaintEventArgs e)
        {
            DataGridView dgv = sender as DataGridView;

            if (string.Equals(dgv.Rows[e.RowIndex].Cells[3].Value, ApplicationConstants.DESATIVADO))
                dgv.Rows[e.RowIndex].DefaultCellStyle.ForeColor = Color.Red;

        }

        private void validaPermissoes()
        {
            PermissionamentoFacade permissionamentoFacade = PermissionamentoFacade.getInstance();

            btnAlterar.Enabled = permissionamentoFacade.hasPermission("FUNCAO", "ALTERAR");
            btnIncluir.Enabled = permissionamentoFacade.hasPermission("FUNCAO", "INCLUIR");
            btnExcluir.Enabled = permissionamentoFacade.hasPermission("FUNCAO", "EXCLUIR");
            btnDetalhar.Enabled = permissionamentoFacade.hasPermission("FUNCAO", "DETALHAR");
            btnReativar.Enabled = permissionamentoFacade.hasPermission("FUNCAO", "REATIVAR");
        }


        public void montaDataGrid()
        {

            try
            {
                this.Cursor = Cursors.WaitCursor;

                EstudoFacade pcmsoFacade = EstudoFacade.getInstance();

                DataSet ds = pcmsoFacade.findFuncaoByFilter(funcao);

                dgvFuncao.DataSource = ds.Tables["funcoes"].DefaultView;

                dgvFuncao.Columns[0].HeaderText = "ID";
                dgvFuncao.Columns[0].Visible = false;
                dgvFuncao.Columns[1].HeaderText = "Descrição";
                dgvFuncao.Columns[2].HeaderText = "Cod CBO";
                dgvFuncao.Columns[3].HeaderText = "Situação";
                dgvFuncao.Columns[3].Visible = false;

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }

        private void dgvFuncao_CellMouseDoubleClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            try
            {
                btnAlterar.PerformClick();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }
    }
}
