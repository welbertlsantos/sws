﻿using SWS.Entidade;
using SWS.Facade;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace SWS.View
{
    public partial class frmEmpresaBuscar : frmTemplateConsulta
    {
        private Empresa empresa;

        public Empresa Empresa
        {
            get { return empresa; }
            set { empresa = value; }
        }
        
        public frmEmpresaBuscar()
        {
            InitializeComponent();
            validaPermissoes();
            ActiveControl = text_razaoSocial;
        }

        private void btn_ok_Click(object sender, EventArgs e)
        {
            try
            {
                EmpresaFacade empresaFacade = EmpresaFacade.getInstance();

                if (dgv_empresa.CurrentRow == null)
                    throw new Exception("Selecione uma empresa.");
                
                empresa = empresaFacade.findEmpresaById((Int64)dgv_empresa.CurrentRow.Cells[0].Value);
                
                this.Close();

            }
            catch (Exception Ex)
            {
                MessageBox.Show(Ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btn_novo_Click(object sender, EventArgs e)
        {
            try
            {
                frmEmpresaIncluir incluirEmpresa = new frmEmpresaIncluir();
                incluirEmpresa.ShowDialog();

                if (incluirEmpresa.Empresa != null)
                {
                    empresa = incluirEmpresa.Empresa;
                    this.Close();
                }
            }
            catch (Exception Ex)
            {
                MessageBox.Show(Ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btn_buscar_Click(object sender, EventArgs e)
        {
            try
            {
                this.dgv_empresa.Columns.Clear();
                montaDataGrid();
                text_razaoSocial.Focus();
            }
            catch (Exception Ex)
            {
                MessageBox.Show(Ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btn_fechar_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void validaPermissoes()
        {
            PermissionamentoFacade permissionamentoFacade = PermissionamentoFacade.getInstance();

            this.btn_novo.Enabled = permissionamentoFacade.hasPermission("EMPRESA", "INCLUIR");
        }

        public void montaDataGrid()
        {
            try
            {
                EmpresaFacade empresaFacade = EmpresaFacade.getInstance();

                DataSet ds = empresaFacade.findEmpresaByFilter(new Empresa(null, text_razaoSocial.Text.Trim(), string.Empty, string.Empty, string.Empty, string.Empty, string.Empty, string.Empty, string.Empty, string.Empty, string.Empty, string.Empty, string.Empty, string.Empty, string.Empty, null, "A", string.Empty, null, null, false, null, string.Empty, 0, 0, 0, 0, 0, 0, 0));

                this.dgv_empresa.DataSource = ds.Tables["Empresas"].DefaultView;

                dgv_empresa.Columns[0].HeaderText = "ID";
                dgv_empresa.Columns[0].Visible = false;

                dgv_empresa.Columns[1].HeaderText = "Razão Social";
                dgv_empresa.Columns[1].ReadOnly = true;

                dgv_empresa.Columns[2].HeaderText = "Nome Fantasia";
                dgv_empresa.Columns[2].Visible = false;

                dgv_empresa.Columns[3].HeaderText = "CNPJ";
                dgv_empresa.Columns[3].ReadOnly = true;

                dgv_empresa.Columns[4].HeaderText = "Inscrição Estadual";
                dgv_empresa.Columns[4].Visible = false;

                dgv_empresa.Columns[5].HeaderText = "Endereço";
                dgv_empresa.Columns[5].Visible = false;

                dgv_empresa.Columns[6].HeaderText = "Número";
                dgv_empresa.Columns[6].Visible = false;

                dgv_empresa.Columns[7].HeaderText = "Complemento";
                dgv_empresa.Columns[7].Visible = false;

                dgv_empresa.Columns[8].HeaderText = "Bairro";
                dgv_empresa.Columns[8].Visible = false;

                dgv_empresa.Columns[9].HeaderText = "Cidade";
                dgv_empresa.Columns[9].Visible = false;

                dgv_empresa.Columns[10].HeaderText = "CEP";
                dgv_empresa.Columns[10].Visible = false;

                dgv_empresa.Columns[11].HeaderText = "UF";
                dgv_empresa.Columns[11].Visible = false;

                dgv_empresa.Columns[12].HeaderText = "Telefone";
                dgv_empresa.Columns[12].Visible = false;

                dgv_empresa.Columns[13].HeaderText = "FAX";
                dgv_empresa.Columns[13].Visible = false;

                dgv_empresa.Columns[14].HeaderText = "Email";
                dgv_empresa.Columns[14].Visible = false;

                dgv_empresa.Columns[15].HeaderText = "Site";
                dgv_empresa.Columns[15].Visible = false;

                dgv_empresa.Columns[16].HeaderText = "Data Cadastro";
                dgv_empresa.Columns[16].Visible = false;

                dgv_empresa.Columns[17].HeaderText = "Situação";
                dgv_empresa.Columns[17].Visible = false;

                dgv_empresa.Columns[18].HeaderText = "MimeType";
                dgv_empresa.Columns[18].Visible = false;

                dgv_empresa.Columns[19].HeaderText = "Logomarca";
                dgv_empresa.Columns[19].Visible = false;

                dgv_empresa.Columns[20].HeaderText = "Simples Nacional?";
                dgv_empresa.Columns[20].Visible = false;
            }
            catch (Exception Ex)
            {
                MessageBox.Show(Ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
    }
}
