﻿namespace SWS.View
{
    partial class frmFilaAtendimento
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            this.flpAcao = new System.Windows.Forms.FlowLayoutPanel();
            this.btnAtender = new System.Windows.Forms.Button();
            this.btnPesquisar = new System.Windows.Forms.Button();
            this.btnLimpar = new System.Windows.Forms.Button();
            this.btnProntuario = new System.Windows.Forms.Button();
            this.btnLaudar = new System.Windows.Forms.Button();
            this.btnFechar = new System.Windows.Forms.Button();
            this.lblSala = new System.Windows.Forms.TextBox();
            this.cbSala = new SWS.ComboBoxWithBorder();
            this.lblPeriodo = new System.Windows.Forms.TextBox();
            this.dataInicial = new System.Windows.Forms.DateTimePicker();
            this.dataFinal = new System.Windows.Forms.DateTimePicker();
            this.lblAtendimento = new System.Windows.Forms.TextBox();
            this.textCodigoAtendimento = new System.Windows.Forms.MaskedTextBox();
            this.btnAtendimento = new System.Windows.Forms.Button();
            this.grb_listaExame = new System.Windows.Forms.GroupBox();
            this.dgvExames = new System.Windows.Forms.DataGridView();
            this.grb_filaAtendimento = new System.Windows.Forms.GroupBox();
            this.dgvFilaAtendimento = new System.Windows.Forms.DataGridView();
            this.pnlTotal = new System.Windows.Forms.Panel();
            this.textTotalAtendimento = new System.Windows.Forms.TextBox();
            this.lblTotalAtendimento = new System.Windows.Forms.Label();
            this.cbSituacao = new SWS.ComboBoxWithBorder();
            this.lblStatus = new System.Windows.Forms.TextBox();
            ((System.ComponentModel.ISupportInitialize)(this.spForm)).BeginInit();
            this.spForm.Panel1.SuspendLayout();
            this.spForm.Panel2.SuspendLayout();
            this.spForm.SuspendLayout();
            this.pnlForm.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.banner)).BeginInit();
            this.flpAcao.SuspendLayout();
            this.grb_listaExame.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvExames)).BeginInit();
            this.grb_filaAtendimento.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvFilaAtendimento)).BeginInit();
            this.pnlTotal.SuspendLayout();
            this.SuspendLayout();
            // 
            // spForm
            // 
            this.spForm.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            // 
            // spForm.Panel1
            // 
            this.spForm.Panel1.Controls.Add(this.flpAcao);
            // 
            // pnlForm
            // 
            this.pnlForm.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.pnlForm.Controls.Add(this.cbSituacao);
            this.pnlForm.Controls.Add(this.lblStatus);
            this.pnlForm.Controls.Add(this.pnlTotal);
            this.pnlForm.Controls.Add(this.grb_filaAtendimento);
            this.pnlForm.Controls.Add(this.grb_listaExame);
            this.pnlForm.Controls.Add(this.btnAtendimento);
            this.pnlForm.Controls.Add(this.textCodigoAtendimento);
            this.pnlForm.Controls.Add(this.lblAtendimento);
            this.pnlForm.Controls.Add(this.lblPeriodo);
            this.pnlForm.Controls.Add(this.cbSala);
            this.pnlForm.Controls.Add(this.dataFinal);
            this.pnlForm.Controls.Add(this.dataInicial);
            this.pnlForm.Controls.Add(this.lblSala);
            this.pnlForm.Size = new System.Drawing.Size(781, 464);
            // 
            // banner
            // 
            this.banner.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            // 
            // flpAcao
            // 
            this.flpAcao.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.flpAcao.Controls.Add(this.btnAtender);
            this.flpAcao.Controls.Add(this.btnPesquisar);
            this.flpAcao.Controls.Add(this.btnLimpar);
            this.flpAcao.Controls.Add(this.btnProntuario);
            this.flpAcao.Controls.Add(this.btnLaudar);
            this.flpAcao.Controls.Add(this.btnFechar);
            this.flpAcao.Location = new System.Drawing.Point(0, 0);
            this.flpAcao.Name = "flpAcao";
            this.flpAcao.Size = new System.Drawing.Size(784, 35);
            this.flpAcao.TabIndex = 0;
            // 
            // btnAtender
            // 
            this.btnAtender.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnAtender.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnAtender.Image = global::SWS.Properties.Resources.atender__Copy_;
            this.btnAtender.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnAtender.Location = new System.Drawing.Point(3, 3);
            this.btnAtender.Name = "btnAtender";
            this.btnAtender.Size = new System.Drawing.Size(78, 23);
            this.btnAtender.TabIndex = 15;
            this.btnAtender.Text = "&Atender";
            this.btnAtender.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnAtender.UseVisualStyleBackColor = true;
            this.btnAtender.Click += new System.EventHandler(this.btnAtender_Click);
            // 
            // btnPesquisar
            // 
            this.btnPesquisar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnPesquisar.Image = global::SWS.Properties.Resources.busca;
            this.btnPesquisar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnPesquisar.Location = new System.Drawing.Point(87, 3);
            this.btnPesquisar.Name = "btnPesquisar";
            this.btnPesquisar.Size = new System.Drawing.Size(75, 23);
            this.btnPesquisar.TabIndex = 13;
            this.btnPesquisar.Text = "&Pesquisar";
            this.btnPesquisar.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnPesquisar.UseVisualStyleBackColor = true;
            this.btnPesquisar.Click += new System.EventHandler(this.btnPesquisar_Click);
            // 
            // btnLimpar
            // 
            this.btnLimpar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnLimpar.Image = global::SWS.Properties.Resources.vassoura;
            this.btnLimpar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnLimpar.Location = new System.Drawing.Point(168, 3);
            this.btnLimpar.Name = "btnLimpar";
            this.btnLimpar.Size = new System.Drawing.Size(75, 23);
            this.btnLimpar.TabIndex = 14;
            this.btnLimpar.Text = "&Limpar";
            this.btnLimpar.UseVisualStyleBackColor = true;
            this.btnLimpar.Click += new System.EventHandler(this.btnLimpar_Click);
            // 
            // btnProntuario
            // 
            this.btnProntuario.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnProntuario.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnProntuario.Image = global::SWS.Properties.Resources.icones_2;
            this.btnProntuario.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnProntuario.Location = new System.Drawing.Point(249, 3);
            this.btnProntuario.Name = "btnProntuario";
            this.btnProntuario.Size = new System.Drawing.Size(75, 23);
            this.btnProntuario.TabIndex = 17;
            this.btnProntuario.Text = "&Prontuário";
            this.btnProntuario.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnProntuario.UseVisualStyleBackColor = true;
            this.btnProntuario.Click += new System.EventHandler(this.btnProntuario_Click);
            // 
            // btnLaudar
            // 
            this.btnLaudar.Enabled = false;
            this.btnLaudar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnLaudar.Image = global::SWS.Properties.Resources.prontuario;
            this.btnLaudar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnLaudar.Location = new System.Drawing.Point(330, 3);
            this.btnLaudar.Name = "btnLaudar";
            this.btnLaudar.Size = new System.Drawing.Size(75, 23);
            this.btnLaudar.TabIndex = 18;
            this.btnLaudar.Text = "&Laudar";
            this.btnLaudar.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnLaudar.UseVisualStyleBackColor = true;
            this.btnLaudar.Click += new System.EventHandler(this.btnLaudar_Click);
            // 
            // btnFechar
            // 
            this.btnFechar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnFechar.Image = global::SWS.Properties.Resources.close;
            this.btnFechar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnFechar.Location = new System.Drawing.Point(411, 3);
            this.btnFechar.Name = "btnFechar";
            this.btnFechar.Size = new System.Drawing.Size(75, 23);
            this.btnFechar.TabIndex = 16;
            this.btnFechar.Text = "&Fechar";
            this.btnFechar.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnFechar.UseVisualStyleBackColor = true;
            this.btnFechar.Click += new System.EventHandler(this.btnFechar_Click);
            // 
            // lblSala
            // 
            this.lblSala.BackColor = System.Drawing.Color.LightGray;
            this.lblSala.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblSala.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSala.Location = new System.Drawing.Point(3, 3);
            this.lblSala.Name = "lblSala";
            this.lblSala.ReadOnly = true;
            this.lblSala.Size = new System.Drawing.Size(137, 21);
            this.lblSala.TabIndex = 0;
            this.lblSala.TabStop = false;
            this.lblSala.Text = "Sala de atendimento";
            // 
            // cbSala
            // 
            this.cbSala.BackColor = System.Drawing.Color.LightGray;
            this.cbSala.BorderColor = System.Drawing.Color.DimGray;
            this.cbSala.BorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid;
            this.cbSala.FormattingEnabled = true;
            this.cbSala.Location = new System.Drawing.Point(139, 3);
            this.cbSala.Name = "cbSala";
            this.cbSala.Size = new System.Drawing.Size(213, 21);
            this.cbSala.TabIndex = 1;
            this.cbSala.SelectedValueChanged += new System.EventHandler(this.cbSala_SelectedValueChanged);
            // 
            // lblPeriodo
            // 
            this.lblPeriodo.BackColor = System.Drawing.Color.LightGray;
            this.lblPeriodo.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblPeriodo.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPeriodo.Location = new System.Drawing.Point(3, 43);
            this.lblPeriodo.Name = "lblPeriodo";
            this.lblPeriodo.ReadOnly = true;
            this.lblPeriodo.Size = new System.Drawing.Size(137, 21);
            this.lblPeriodo.TabIndex = 2;
            this.lblPeriodo.TabStop = false;
            this.lblPeriodo.Text = "Período";
            // 
            // dataInicial
            // 
            this.dataInicial.Checked = false;
            this.dataInicial.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dataInicial.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dataInicial.Location = new System.Drawing.Point(139, 43);
            this.dataInicial.Name = "dataInicial";
            this.dataInicial.ShowCheckBox = true;
            this.dataInicial.Size = new System.Drawing.Size(107, 21);
            this.dataInicial.TabIndex = 2;
            // 
            // dataFinal
            // 
            this.dataFinal.Checked = false;
            this.dataFinal.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dataFinal.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dataFinal.Location = new System.Drawing.Point(245, 43);
            this.dataFinal.Name = "dataFinal";
            this.dataFinal.ShowCheckBox = true;
            this.dataFinal.Size = new System.Drawing.Size(107, 21);
            this.dataFinal.TabIndex = 3;
            // 
            // lblAtendimento
            // 
            this.lblAtendimento.BackColor = System.Drawing.Color.LightGray;
            this.lblAtendimento.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblAtendimento.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblAtendimento.Location = new System.Drawing.Point(3, 63);
            this.lblAtendimento.Name = "lblAtendimento";
            this.lblAtendimento.ReadOnly = true;
            this.lblAtendimento.Size = new System.Drawing.Size(137, 21);
            this.lblAtendimento.TabIndex = 9;
            this.lblAtendimento.TabStop = false;
            this.lblAtendimento.Text = "Atendimento";
            // 
            // textCodigoAtendimento
            // 
            this.textCodigoAtendimento.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textCodigoAtendimento.CutCopyMaskFormat = System.Windows.Forms.MaskFormat.ExcludePromptAndLiterals;
            this.textCodigoAtendimento.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textCodigoAtendimento.ForeColor = System.Drawing.Color.Blue;
            this.textCodigoAtendimento.Location = new System.Drawing.Point(139, 63);
            this.textCodigoAtendimento.Mask = "9999,99,999999";
            this.textCodigoAtendimento.Name = "textCodigoAtendimento";
            this.textCodigoAtendimento.PromptChar = ' ';
            this.textCodigoAtendimento.Size = new System.Drawing.Size(181, 21);
            this.textCodigoAtendimento.TabIndex = 10;
            // 
            // btnAtendimento
            // 
            this.btnAtendimento.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnAtendimento.Image = global::SWS.Properties.Resources.busca;
            this.btnAtendimento.Location = new System.Drawing.Point(318, 63);
            this.btnAtendimento.Name = "btnAtendimento";
            this.btnAtendimento.Size = new System.Drawing.Size(34, 21);
            this.btnAtendimento.TabIndex = 11;
            this.btnAtendimento.UseVisualStyleBackColor = true;
            this.btnAtendimento.Click += new System.EventHandler(this.btnAtendimento_Click);
            // 
            // grb_listaExame
            // 
            this.grb_listaExame.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.grb_listaExame.Controls.Add(this.dgvExames);
            this.grb_listaExame.Location = new System.Drawing.Point(3, 90);
            this.grb_listaExame.Name = "grb_listaExame";
            this.grb_listaExame.Size = new System.Drawing.Size(775, 116);
            this.grb_listaExame.TabIndex = 12;
            this.grb_listaExame.TabStop = false;
            this.grb_listaExame.Text = "Lista de Exames";
            // 
            // dgvExames
            // 
            this.dgvExames.AllowUserToAddRows = false;
            this.dgvExames.AllowUserToDeleteRows = false;
            this.dgvExames.AllowUserToOrderColumns = true;
            this.dgvExames.AllowUserToResizeColumns = false;
            this.dgvExames.AllowUserToResizeRows = false;
            dataGridViewCellStyle3.BackColor = System.Drawing.Color.Gainsboro;
            this.dgvExames.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle3;
            this.dgvExames.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this.dgvExames.BackgroundColor = System.Drawing.Color.White;
            this.dgvExames.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle4.BackColor = System.Drawing.Color.LightGray;
            dataGridViewCellStyle4.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle4.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle4.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle4.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle4.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvExames.DefaultCellStyle = dataGridViewCellStyle4;
            this.dgvExames.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvExames.Location = new System.Drawing.Point(3, 16);
            this.dgvExames.MultiSelect = false;
            this.dgvExames.Name = "dgvExames";
            this.dgvExames.ReadOnly = true;
            this.dgvExames.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvExames.Size = new System.Drawing.Size(769, 97);
            this.dgvExames.TabIndex = 7;
            this.dgvExames.TabStop = false;
            // 
            // grb_filaAtendimento
            // 
            this.grb_filaAtendimento.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.grb_filaAtendimento.Controls.Add(this.dgvFilaAtendimento);
            this.grb_filaAtendimento.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.grb_filaAtendimento.Location = new System.Drawing.Point(3, 208);
            this.grb_filaAtendimento.Name = "grb_filaAtendimento";
            this.grb_filaAtendimento.Size = new System.Drawing.Size(778, 197);
            this.grb_filaAtendimento.TabIndex = 5;
            this.grb_filaAtendimento.TabStop = false;
            this.grb_filaAtendimento.Text = "Fila de Atendimento";
            // 
            // dgvFilaAtendimento
            // 
            this.dgvFilaAtendimento.AllowDrop = true;
            this.dgvFilaAtendimento.AllowUserToAddRows = false;
            this.dgvFilaAtendimento.AllowUserToDeleteRows = false;
            this.dgvFilaAtendimento.AllowUserToResizeRows = false;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.Gainsboro;
            this.dgvFilaAtendimento.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle1;
            this.dgvFilaAtendimento.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this.dgvFilaAtendimento.BackgroundColor = System.Drawing.Color.White;
            this.dgvFilaAtendimento.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.Color.LightGray;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvFilaAtendimento.DefaultCellStyle = dataGridViewCellStyle2;
            this.dgvFilaAtendimento.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvFilaAtendimento.Location = new System.Drawing.Point(3, 16);
            this.dgvFilaAtendimento.MultiSelect = false;
            this.dgvFilaAtendimento.Name = "dgvFilaAtendimento";
            this.dgvFilaAtendimento.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvFilaAtendimento.Size = new System.Drawing.Size(772, 178);
            this.dgvFilaAtendimento.TabIndex = 0;
            this.dgvFilaAtendimento.CellContentDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvFilaAtendimento_CellContentDoubleClick);
            this.dgvFilaAtendimento.DataBindingComplete += new System.Windows.Forms.DataGridViewBindingCompleteEventHandler(this.dgvFilaAtendimento_DataBindingComplete);
            this.dgvFilaAtendimento.RowPrePaint += new System.Windows.Forms.DataGridViewRowPrePaintEventHandler(this.dgvFilaAtendimento_RowPrePaint);
            // 
            // pnlTotal
            // 
            this.pnlTotal.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.pnlTotal.BackColor = System.Drawing.Color.LightGray;
            this.pnlTotal.Controls.Add(this.textTotalAtendimento);
            this.pnlTotal.Controls.Add(this.lblTotalAtendimento);
            this.pnlTotal.Location = new System.Drawing.Point(3, 414);
            this.pnlTotal.Name = "pnlTotal";
            this.pnlTotal.Size = new System.Drawing.Size(778, 35);
            this.pnlTotal.TabIndex = 13;
            // 
            // textTotalAtendimento
            // 
            this.textTotalAtendimento.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.textTotalAtendimento.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textTotalAtendimento.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textTotalAtendimento.Location = new System.Drawing.Point(128, 7);
            this.textTotalAtendimento.Name = "textTotalAtendimento";
            this.textTotalAtendimento.ReadOnly = true;
            this.textTotalAtendimento.Size = new System.Drawing.Size(100, 21);
            this.textTotalAtendimento.TabIndex = 1;
            this.textTotalAtendimento.TabStop = false;
            // 
            // lblTotalAtendimento
            // 
            this.lblTotalAtendimento.AutoSize = true;
            this.lblTotalAtendimento.Location = new System.Drawing.Point(9, 11);
            this.lblTotalAtendimento.Name = "lblTotalAtendimento";
            this.lblTotalAtendimento.Size = new System.Drawing.Size(113, 13);
            this.lblTotalAtendimento.TabIndex = 0;
            this.lblTotalAtendimento.Text = "Total de Atendimentos";
            // 
            // cbSituacao
            // 
            this.cbSituacao.BackColor = System.Drawing.Color.LightGray;
            this.cbSituacao.BorderColor = System.Drawing.Color.DimGray;
            this.cbSituacao.BorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid;
            this.cbSituacao.FormattingEnabled = true;
            this.cbSituacao.Location = new System.Drawing.Point(139, 23);
            this.cbSituacao.Name = "cbSituacao";
            this.cbSituacao.Size = new System.Drawing.Size(213, 21);
            this.cbSituacao.TabIndex = 15;
            this.cbSituacao.SelectedIndexChanged += new System.EventHandler(this.cbSituacao_SelectedIndexChanged);
            // 
            // lblStatus
            // 
            this.lblStatus.BackColor = System.Drawing.Color.LightGray;
            this.lblStatus.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblStatus.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblStatus.Location = new System.Drawing.Point(3, 23);
            this.lblStatus.Name = "lblStatus";
            this.lblStatus.ReadOnly = true;
            this.lblStatus.Size = new System.Drawing.Size(137, 21);
            this.lblStatus.TabIndex = 14;
            this.lblStatus.TabStop = false;
            this.lblStatus.Text = "Situação Atendimento";
            // 
            // frmFilaAtendimento
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(784, 561);
            this.Name = "frmFilaAtendimento";
            this.Text = "FILA DE ATENDIMENTO";
            this.Load += new System.EventHandler(this.frmFilaDeAtendimento_Load);
            this.spForm.Panel1.ResumeLayout(false);
            this.spForm.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.spForm)).EndInit();
            this.spForm.ResumeLayout(false);
            this.pnlForm.ResumeLayout(false);
            this.pnlForm.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.banner)).EndInit();
            this.flpAcao.ResumeLayout(false);
            this.grb_listaExame.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvExames)).EndInit();
            this.grb_filaAtendimento.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvFilaAtendimento)).EndInit();
            this.pnlTotal.ResumeLayout(false);
            this.pnlTotal.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.FlowLayoutPanel flpAcao;
        private System.Windows.Forms.Button btnLimpar;
        private System.Windows.Forms.Button btnPesquisar;
        private System.Windows.Forms.Button btnProntuario;
        private System.Windows.Forms.Button btnAtender;
        private System.Windows.Forms.Button btnFechar;
        private ComboBoxWithBorder cbSala;
        private System.Windows.Forms.TextBox lblSala;
        private System.Windows.Forms.TextBox lblPeriodo;
        private System.Windows.Forms.DateTimePicker dataFinal;
        private System.Windows.Forms.DateTimePicker dataInicial;
        private System.Windows.Forms.TextBox lblAtendimento;
        private System.Windows.Forms.MaskedTextBox textCodigoAtendimento;
        private System.Windows.Forms.Button btnAtendimento;
        private System.Windows.Forms.GroupBox grb_listaExame;
        private System.Windows.Forms.DataGridView dgvExames;
        private System.Windows.Forms.GroupBox grb_filaAtendimento;
        private System.Windows.Forms.DataGridView dgvFilaAtendimento;
        private System.Windows.Forms.Panel pnlTotal;
        private ComboBoxWithBorder cbSituacao;
        private System.Windows.Forms.TextBox lblStatus;
        private System.Windows.Forms.Label lblTotalAtendimento;
        private System.Windows.Forms.TextBox textTotalAtendimento;
        private System.Windows.Forms.Button btnLaudar;
    }
}