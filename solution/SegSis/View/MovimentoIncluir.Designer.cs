﻿namespace SWS.View
{
    partial class frmMovimentoIncluir
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            this.flpAcao = new System.Windows.Forms.FlowLayoutPanel();
            this.btn_confirmar = new System.Windows.Forms.Button();
            this.btn_fechar = new System.Windows.Forms.Button();
            this.lblCliente = new System.Windows.Forms.TextBox();
            this.textCliente = new System.Windows.Forms.TextBox();
            this.btnCliente = new System.Windows.Forms.Button();
            this.lblUnidade = new System.Windows.Forms.TextBox();
            this.btnUnidade = new System.Windows.Forms.Button();
            this.textUnidade = new System.Windows.Forms.TextBox();
            this.lblDataInclusao = new System.Windows.Forms.TextBox();
            this.dataInclusao = new System.Windows.Forms.DateTimePicker();
            this.lblProduto = new System.Windows.Forms.TextBox();
            this.textProduto = new System.Windows.Forms.TextBox();
            this.btnIncluirProduto = new System.Windows.Forms.Button();
            this.lblPreco = new System.Windows.Forms.TextBox();
            this.textPreco = new System.Windows.Forms.TextBox();
            this.lblQuantidade = new System.Windows.Forms.TextBox();
            this.textQuantidade = new System.Windows.Forms.TextBox();
            this.btnGravar = new System.Windows.Forms.Button();
            this.grbItens = new System.Windows.Forms.GroupBox();
            this.dgvMovimento = new System.Windows.Forms.DataGridView();
            this.grb_totais = new System.Windows.Forms.GroupBox();
            this.lblTotal = new System.Windows.Forms.TextBox();
            this.lblQuantidadeItem = new System.Windows.Forms.TextBox();
            this.textTotalItens = new System.Windows.Forms.TextBox();
            this.textQuantidadeItens = new System.Windows.Forms.TextBox();
            this.lblCentroCusto = new System.Windows.Forms.TextBox();
            this.textCentroCusto = new System.Windows.Forms.TextBox();
            this.btnCentroCusto = new System.Windows.Forms.Button();
            this.btnCredenciada = new System.Windows.Forms.Button();
            this.textCredenciada = new System.Windows.Forms.TextBox();
            this.lblCredenciada = new System.Windows.Forms.TextBox();
            ((System.ComponentModel.ISupportInitialize)(this.scForm)).BeginInit();
            this.scForm.Panel1.SuspendLayout();
            this.scForm.Panel2.SuspendLayout();
            this.scForm.SuspendLayout();
            this.pnlForm.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.banner)).BeginInit();
            this.flpAcao.SuspendLayout();
            this.grbItens.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvMovimento)).BeginInit();
            this.grb_totais.SuspendLayout();
            this.SuspendLayout();
            // 
            // scForm
            // 
            // 
            // scForm.Panel1
            // 
            this.scForm.Panel1.Controls.Add(this.flpAcao);
            this.scForm.Size = new System.Drawing.Size(785, 520);
            this.scForm.SplitterDistance = 48;
            // 
            // pnlForm
            // 
            this.pnlForm.Controls.Add(this.btnCredenciada);
            this.pnlForm.Controls.Add(this.textCredenciada);
            this.pnlForm.Controls.Add(this.lblCredenciada);
            this.pnlForm.Controls.Add(this.lblDataInclusao);
            this.pnlForm.Controls.Add(this.btnCentroCusto);
            this.pnlForm.Controls.Add(this.textCentroCusto);
            this.pnlForm.Controls.Add(this.lblCentroCusto);
            this.pnlForm.Controls.Add(this.grb_totais);
            this.pnlForm.Controls.Add(this.grbItens);
            this.pnlForm.Controls.Add(this.btnGravar);
            this.pnlForm.Controls.Add(this.textQuantidade);
            this.pnlForm.Controls.Add(this.lblQuantidade);
            this.pnlForm.Controls.Add(this.textPreco);
            this.pnlForm.Controls.Add(this.lblPreco);
            this.pnlForm.Controls.Add(this.btnIncluirProduto);
            this.pnlForm.Controls.Add(this.textProduto);
            this.pnlForm.Controls.Add(this.lblProduto);
            this.pnlForm.Controls.Add(this.btnUnidade);
            this.pnlForm.Controls.Add(this.textUnidade);
            this.pnlForm.Controls.Add(this.dataInclusao);
            this.pnlForm.Controls.Add(this.lblUnidade);
            this.pnlForm.Controls.Add(this.btnCliente);
            this.pnlForm.Controls.Add(this.textCliente);
            this.pnlForm.Controls.Add(this.lblCliente);
            this.pnlForm.Size = new System.Drawing.Size(772, 462);
            // 
            // banner
            // 
            this.banner.Size = new System.Drawing.Size(785, 41);
            // 
            // flpAcao
            // 
            this.flpAcao.Controls.Add(this.btn_confirmar);
            this.flpAcao.Controls.Add(this.btn_fechar);
            this.flpAcao.Dock = System.Windows.Forms.DockStyle.Fill;
            this.flpAcao.Location = new System.Drawing.Point(0, 0);
            this.flpAcao.Name = "flpAcao";
            this.flpAcao.Size = new System.Drawing.Size(785, 48);
            this.flpAcao.TabIndex = 0;
            // 
            // btn_confirmar
            // 
            this.btn_confirmar.Cursor = System.Windows.Forms.Cursors.Default;
            this.btn_confirmar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_confirmar.Image = global::SWS.Properties.Resources.Gravar;
            this.btn_confirmar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btn_confirmar.Location = new System.Drawing.Point(3, 3);
            this.btn_confirmar.Name = "btn_confirmar";
            this.btn_confirmar.Size = new System.Drawing.Size(75, 23);
            this.btn_confirmar.TabIndex = 8;
            this.btn_confirmar.TabStop = false;
            this.btn_confirmar.Text = "&Confirma";
            this.btn_confirmar.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btn_confirmar.UseVisualStyleBackColor = true;
            this.btn_confirmar.Click += new System.EventHandler(this.btn_confirmar_Click);
            // 
            // btn_fechar
            // 
            this.btn_fechar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_fechar.Image = global::SWS.Properties.Resources.close;
            this.btn_fechar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btn_fechar.Location = new System.Drawing.Point(84, 3);
            this.btn_fechar.Name = "btn_fechar";
            this.btn_fechar.Size = new System.Drawing.Size(75, 23);
            this.btn_fechar.TabIndex = 9;
            this.btn_fechar.TabStop = false;
            this.btn_fechar.Text = "&Fechar";
            this.btn_fechar.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btn_fechar.UseVisualStyleBackColor = true;
            this.btn_fechar.Click += new System.EventHandler(this.btn_fechar_Click);
            // 
            // lblCliente
            // 
            this.lblCliente.BackColor = System.Drawing.Color.LightGray;
            this.lblCliente.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblCliente.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCliente.Location = new System.Drawing.Point(10, 7);
            this.lblCliente.Name = "lblCliente";
            this.lblCliente.ReadOnly = true;
            this.lblCliente.Size = new System.Drawing.Size(100, 21);
            this.lblCliente.TabIndex = 0;
            this.lblCliente.TabStop = false;
            this.lblCliente.Text = "Cliente/Credenciada";
            // 
            // textCliente
            // 
            this.textCliente.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.textCliente.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textCliente.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textCliente.Location = new System.Drawing.Point(109, 7);
            this.textCliente.MaxLength = 100;
            this.textCliente.Name = "textCliente";
            this.textCliente.ReadOnly = true;
            this.textCliente.Size = new System.Drawing.Size(615, 21);
            this.textCliente.TabIndex = 1;
            this.textCliente.TabStop = false;
            // 
            // btnCliente
            // 
            this.btnCliente.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnCliente.Image = global::SWS.Properties.Resources.busca;
            this.btnCliente.Location = new System.Drawing.Point(722, 7);
            this.btnCliente.Name = "btnCliente";
            this.btnCliente.Size = new System.Drawing.Size(33, 21);
            this.btnCliente.TabIndex = 1;
            this.btnCliente.UseVisualStyleBackColor = true;
            this.btnCliente.Click += new System.EventHandler(this.btn_cliente_Click);
            // 
            // lblUnidade
            // 
            this.lblUnidade.BackColor = System.Drawing.Color.LightGray;
            this.lblUnidade.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblUnidade.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblUnidade.Location = new System.Drawing.Point(10, 27);
            this.lblUnidade.Name = "lblUnidade";
            this.lblUnidade.ReadOnly = true;
            this.lblUnidade.Size = new System.Drawing.Size(100, 21);
            this.lblUnidade.TabIndex = 3;
            this.lblUnidade.TabStop = false;
            this.lblUnidade.Text = "Unidade";
            // 
            // btnUnidade
            // 
            this.btnUnidade.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnUnidade.Image = global::SWS.Properties.Resources.busca;
            this.btnUnidade.Location = new System.Drawing.Point(722, 27);
            this.btnUnidade.Name = "btnUnidade";
            this.btnUnidade.Size = new System.Drawing.Size(33, 21);
            this.btnUnidade.TabIndex = 2;
            this.btnUnidade.UseVisualStyleBackColor = true;
            this.btnUnidade.Click += new System.EventHandler(this.btn_unidade_Click);
            // 
            // textUnidade
            // 
            this.textUnidade.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.textUnidade.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textUnidade.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textUnidade.Location = new System.Drawing.Point(109, 27);
            this.textUnidade.MaxLength = 100;
            this.textUnidade.Name = "textUnidade";
            this.textUnidade.ReadOnly = true;
            this.textUnidade.Size = new System.Drawing.Size(615, 21);
            this.textUnidade.TabIndex = 4;
            this.textUnidade.TabStop = false;
            // 
            // lblDataInclusao
            // 
            this.lblDataInclusao.BackColor = System.Drawing.Color.LightGray;
            this.lblDataInclusao.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblDataInclusao.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDataInclusao.Location = new System.Drawing.Point(10, 87);
            this.lblDataInclusao.Name = "lblDataInclusao";
            this.lblDataInclusao.ReadOnly = true;
            this.lblDataInclusao.Size = new System.Drawing.Size(100, 21);
            this.lblDataInclusao.TabIndex = 6;
            this.lblDataInclusao.TabStop = false;
            this.lblDataInclusao.Text = "Data de Inclusão";
            // 
            // dataInclusao
            // 
            this.dataInclusao.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dataInclusao.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dataInclusao.Location = new System.Drawing.Point(109, 87);
            this.dataInclusao.Name = "dataInclusao";
            this.dataInclusao.Size = new System.Drawing.Size(100, 21);
            this.dataInclusao.TabIndex = 7;
            // 
            // lblProduto
            // 
            this.lblProduto.BackColor = System.Drawing.Color.LightGray;
            this.lblProduto.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblProduto.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblProduto.Location = new System.Drawing.Point(10, 123);
            this.lblProduto.Name = "lblProduto";
            this.lblProduto.ReadOnly = true;
            this.lblProduto.Size = new System.Drawing.Size(491, 21);
            this.lblProduto.TabIndex = 8;
            this.lblProduto.TabStop = false;
            this.lblProduto.Text = "Produto";
            // 
            // textProduto
            // 
            this.textProduto.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.textProduto.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textProduto.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textProduto.Location = new System.Drawing.Point(10, 143);
            this.textProduto.MaxLength = 100;
            this.textProduto.Name = "textProduto";
            this.textProduto.ReadOnly = true;
            this.textProduto.Size = new System.Drawing.Size(462, 21);
            this.textProduto.TabIndex = 9;
            this.textProduto.TabStop = false;
            // 
            // btnIncluirProduto
            // 
            this.btnIncluirProduto.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnIncluirProduto.Image = global::SWS.Properties.Resources.busca;
            this.btnIncluirProduto.Location = new System.Drawing.Point(468, 143);
            this.btnIncluirProduto.Name = "btnIncluirProduto";
            this.btnIncluirProduto.Size = new System.Drawing.Size(33, 21);
            this.btnIncluirProduto.TabIndex = 3;
            this.btnIncluirProduto.UseVisualStyleBackColor = true;
            this.btnIncluirProduto.Click += new System.EventHandler(this.btn_incluirProduto_Click);
            // 
            // lblPreco
            // 
            this.lblPreco.BackColor = System.Drawing.Color.LightGray;
            this.lblPreco.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblPreco.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPreco.Location = new System.Drawing.Point(499, 123);
            this.lblPreco.Name = "lblPreco";
            this.lblPreco.ReadOnly = true;
            this.lblPreco.Size = new System.Drawing.Size(100, 21);
            this.lblPreco.TabIndex = 11;
            this.lblPreco.TabStop = false;
            this.lblPreco.Text = "Preço";
            this.lblPreco.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // textPreco
            // 
            this.textPreco.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.textPreco.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textPreco.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textPreco.Location = new System.Drawing.Point(499, 143);
            this.textPreco.MaxLength = 100;
            this.textPreco.Name = "textPreco";
            this.textPreco.ReadOnly = true;
            this.textPreco.Size = new System.Drawing.Size(100, 21);
            this.textPreco.TabIndex = 12;
            this.textPreco.TabStop = false;
            this.textPreco.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // lblQuantidade
            // 
            this.lblQuantidade.BackColor = System.Drawing.Color.LightGray;
            this.lblQuantidade.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblQuantidade.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblQuantidade.Location = new System.Drawing.Point(598, 123);
            this.lblQuantidade.Name = "lblQuantidade";
            this.lblQuantidade.ReadOnly = true;
            this.lblQuantidade.Size = new System.Drawing.Size(102, 21);
            this.lblQuantidade.TabIndex = 13;
            this.lblQuantidade.TabStop = false;
            this.lblQuantidade.Text = "Quantidade";
            this.lblQuantidade.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // textQuantidade
            // 
            this.textQuantidade.BackColor = System.Drawing.SystemColors.Window;
            this.textQuantidade.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textQuantidade.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textQuantidade.Location = new System.Drawing.Point(598, 143);
            this.textQuantidade.MaxLength = 100;
            this.textQuantidade.Name = "textQuantidade";
            this.textQuantidade.Size = new System.Drawing.Size(102, 21);
            this.textQuantidade.TabIndex = 4;
            this.textQuantidade.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.textQuantidade.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.text_quantidade_KeyPress);
            // 
            // btnGravar
            // 
            this.btnGravar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnGravar.Image = global::SWS.Properties.Resources.fechar_ico;
            this.btnGravar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnGravar.Location = new System.Drawing.Point(699, 123);
            this.btnGravar.Name = "btnGravar";
            this.btnGravar.Size = new System.Drawing.Size(56, 41);
            this.btnGravar.TabIndex = 5;
            this.btnGravar.Text = "OK";
            this.btnGravar.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnGravar.UseVisualStyleBackColor = true;
            this.btnGravar.Click += new System.EventHandler(this.btn_OK_Click);
            // 
            // grbItens
            // 
            this.grbItens.Controls.Add(this.dgvMovimento);
            this.grbItens.Location = new System.Drawing.Point(10, 170);
            this.grbItens.Name = "grbItens";
            this.grbItens.Size = new System.Drawing.Size(745, 240);
            this.grbItens.TabIndex = 16;
            this.grbItens.TabStop = false;
            this.grbItens.Text = "Itens";
            // 
            // dgvMovimento
            // 
            this.dgvMovimento.AllowUserToAddRows = false;
            this.dgvMovimento.AllowUserToDeleteRows = false;
            this.dgvMovimento.AllowUserToOrderColumns = true;
            this.dgvMovimento.AllowUserToResizeRows = false;
            dataGridViewCellStyle4.BackColor = System.Drawing.Color.Gainsboro;
            this.dgvMovimento.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle4;
            this.dgvMovimento.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.DisplayedCells;
            this.dgvMovimento.BackgroundColor = System.Drawing.Color.White;
            dataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle5.BackColor = System.Drawing.Color.LightGray;
            dataGridViewCellStyle5.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle5.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle5.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle5.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle5.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvMovimento.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle5;
            this.dgvMovimento.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle6.BackColor = System.Drawing.Color.LightGray;
            dataGridViewCellStyle6.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle6.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle6.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle6.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle6.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvMovimento.DefaultCellStyle = dataGridViewCellStyle6;
            this.dgvMovimento.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvMovimento.Location = new System.Drawing.Point(3, 16);
            this.dgvMovimento.MultiSelect = false;
            this.dgvMovimento.Name = "dgvMovimento";
            this.dgvMovimento.ReadOnly = true;
            this.dgvMovimento.RowHeadersWidth = 30;
            this.dgvMovimento.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvMovimento.Size = new System.Drawing.Size(739, 221);
            this.dgvMovimento.TabIndex = 6;
            this.dgvMovimento.KeyDown += new System.Windows.Forms.KeyEventHandler(this.dgv_movimento_KeyDown);
            // 
            // grb_totais
            // 
            this.grb_totais.Controls.Add(this.lblTotal);
            this.grb_totais.Controls.Add(this.lblQuantidadeItem);
            this.grb_totais.Controls.Add(this.textTotalItens);
            this.grb_totais.Controls.Add(this.textQuantidadeItens);
            this.grb_totais.Location = new System.Drawing.Point(10, 416);
            this.grb_totais.Name = "grb_totais";
            this.grb_totais.Size = new System.Drawing.Size(494, 40);
            this.grb_totais.TabIndex = 17;
            this.grb_totais.TabStop = false;
            this.grb_totais.Text = "Totais";
            // 
            // lblTotal
            // 
            this.lblTotal.BackColor = System.Drawing.Color.LightGray;
            this.lblTotal.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblTotal.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTotal.Location = new System.Drawing.Point(224, 12);
            this.lblTotal.Name = "lblTotal";
            this.lblTotal.ReadOnly = true;
            this.lblTotal.Size = new System.Drawing.Size(89, 21);
            this.lblTotal.TabIndex = 10;
            this.lblTotal.TabStop = false;
            this.lblTotal.Text = "Total R$";
            // 
            // lblQuantidadeItem
            // 
            this.lblQuantidadeItem.BackColor = System.Drawing.Color.LightGray;
            this.lblQuantidadeItem.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblQuantidadeItem.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblQuantidadeItem.Location = new System.Drawing.Point(11, 12);
            this.lblQuantidadeItem.Name = "lblQuantidadeItem";
            this.lblQuantidadeItem.ReadOnly = true;
            this.lblQuantidadeItem.Size = new System.Drawing.Size(89, 21);
            this.lblQuantidadeItem.TabIndex = 9;
            this.lblQuantidadeItem.TabStop = false;
            this.lblQuantidadeItem.Text = "Quant. Itens";
            // 
            // textTotalItens
            // 
            this.textTotalItens.BackColor = System.Drawing.Color.White;
            this.textTotalItens.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textTotalItens.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textTotalItens.Location = new System.Drawing.Point(312, 12);
            this.textTotalItens.Name = "textTotalItens";
            this.textTotalItens.Size = new System.Drawing.Size(100, 21);
            this.textTotalItens.TabIndex = 3;
            this.textTotalItens.TabStop = false;
            this.textTotalItens.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // textQuantidadeItens
            // 
            this.textQuantidadeItens.BackColor = System.Drawing.Color.White;
            this.textQuantidadeItens.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textQuantidadeItens.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textQuantidadeItens.ForeColor = System.Drawing.Color.Navy;
            this.textQuantidadeItens.Location = new System.Drawing.Point(99, 12);
            this.textQuantidadeItens.Name = "textQuantidadeItens";
            this.textQuantidadeItens.ReadOnly = true;
            this.textQuantidadeItens.Size = new System.Drawing.Size(100, 21);
            this.textQuantidadeItens.TabIndex = 1;
            this.textQuantidadeItens.TabStop = false;
            this.textQuantidadeItens.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // lblCentroCusto
            // 
            this.lblCentroCusto.BackColor = System.Drawing.Color.LightGray;
            this.lblCentroCusto.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblCentroCusto.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCentroCusto.Location = new System.Drawing.Point(10, 47);
            this.lblCentroCusto.Name = "lblCentroCusto";
            this.lblCentroCusto.ReadOnly = true;
            this.lblCentroCusto.Size = new System.Drawing.Size(100, 21);
            this.lblCentroCusto.TabIndex = 18;
            this.lblCentroCusto.TabStop = false;
            this.lblCentroCusto.Text = "Centro de Custo";
            // 
            // textCentroCusto
            // 
            this.textCentroCusto.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.textCentroCusto.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textCentroCusto.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textCentroCusto.Location = new System.Drawing.Point(109, 47);
            this.textCentroCusto.MaxLength = 100;
            this.textCentroCusto.Name = "textCentroCusto";
            this.textCentroCusto.ReadOnly = true;
            this.textCentroCusto.Size = new System.Drawing.Size(615, 21);
            this.textCentroCusto.TabIndex = 19;
            this.textCentroCusto.TabStop = false;
            // 
            // btnCentroCusto
            // 
            this.btnCentroCusto.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnCentroCusto.Image = global::SWS.Properties.Resources.busca;
            this.btnCentroCusto.Location = new System.Drawing.Point(722, 47);
            this.btnCentroCusto.Name = "btnCentroCusto";
            this.btnCentroCusto.Size = new System.Drawing.Size(33, 21);
            this.btnCentroCusto.TabIndex = 20;
            this.btnCentroCusto.UseVisualStyleBackColor = true;
            this.btnCentroCusto.Click += new System.EventHandler(this.btnCentroCusto_Click);
            // 
            // btnCredenciada
            // 
            this.btnCredenciada.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnCredenciada.Image = global::SWS.Properties.Resources.busca;
            this.btnCredenciada.Location = new System.Drawing.Point(722, 67);
            this.btnCredenciada.Name = "btnCredenciada";
            this.btnCredenciada.Size = new System.Drawing.Size(33, 21);
            this.btnCredenciada.TabIndex = 23;
            this.btnCredenciada.UseVisualStyleBackColor = true;
            this.btnCredenciada.Click += new System.EventHandler(this.btnCredenciada_Click);
            // 
            // textCredenciada
            // 
            this.textCredenciada.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.textCredenciada.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textCredenciada.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textCredenciada.Location = new System.Drawing.Point(109, 67);
            this.textCredenciada.MaxLength = 100;
            this.textCredenciada.Name = "textCredenciada";
            this.textCredenciada.ReadOnly = true;
            this.textCredenciada.Size = new System.Drawing.Size(615, 21);
            this.textCredenciada.TabIndex = 22;
            this.textCredenciada.TabStop = false;
            // 
            // lblCredenciada
            // 
            this.lblCredenciada.BackColor = System.Drawing.Color.LightGray;
            this.lblCredenciada.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblCredenciada.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCredenciada.Location = new System.Drawing.Point(10, 67);
            this.lblCredenciada.Name = "lblCredenciada";
            this.lblCredenciada.ReadOnly = true;
            this.lblCredenciada.Size = new System.Drawing.Size(100, 21);
            this.lblCredenciada.TabIndex = 21;
            this.lblCredenciada.TabStop = false;
            this.lblCredenciada.Text = "Credenciada";
            // 
            // frmMovimentoIncluir
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(784, 561);
            this.Name = "frmMovimentoIncluir";
            this.Text = "INCLUIR ITEM AVULSO NO MOVIMENTO";
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.frmMovimentoIncluir_KeyDown);
            this.scForm.Panel1.ResumeLayout(false);
            this.scForm.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.scForm)).EndInit();
            this.scForm.ResumeLayout(false);
            this.pnlForm.ResumeLayout(false);
            this.pnlForm.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.banner)).EndInit();
            this.flpAcao.ResumeLayout(false);
            this.grbItens.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvMovimento)).EndInit();
            this.grb_totais.ResumeLayout(false);
            this.grb_totais.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.FlowLayoutPanel flpAcao;
        private System.Windows.Forms.Button btn_fechar;
        private System.Windows.Forms.Button btn_confirmar;
        private System.Windows.Forms.TextBox lblCliente;
        private System.Windows.Forms.TextBox textCliente;
        private System.Windows.Forms.Button btnCliente;
        private System.Windows.Forms.TextBox lblUnidade;
        private System.Windows.Forms.Button btnUnidade;
        private System.Windows.Forms.TextBox textUnidade;
        private System.Windows.Forms.TextBox lblDataInclusao;
        private System.Windows.Forms.DateTimePicker dataInclusao;
        private System.Windows.Forms.TextBox lblProduto;
        private System.Windows.Forms.TextBox textProduto;
        private System.Windows.Forms.TextBox lblPreco;
        private System.Windows.Forms.Button btnIncluirProduto;
        private System.Windows.Forms.TextBox textPreco;
        private System.Windows.Forms.TextBox lblQuantidade;
        private System.Windows.Forms.TextBox textQuantidade;
        private System.Windows.Forms.Button btnGravar;
        private System.Windows.Forms.GroupBox grbItens;
        private System.Windows.Forms.DataGridView dgvMovimento;
        private System.Windows.Forms.GroupBox grb_totais;
        private System.Windows.Forms.TextBox lblTotal;
        private System.Windows.Forms.TextBox lblQuantidadeItem;
        private System.Windows.Forms.TextBox textTotalItens;
        private System.Windows.Forms.TextBox textQuantidadeItens;
        private System.Windows.Forms.Button btnCentroCusto;
        private System.Windows.Forms.TextBox textCentroCusto;
        private System.Windows.Forms.TextBox lblCentroCusto;
        private System.Windows.Forms.Button btnCredenciada;
        private System.Windows.Forms.TextBox textCredenciada;
        private System.Windows.Forms.TextBox lblCredenciada;
    }
}