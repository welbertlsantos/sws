﻿namespace SWS.View
{
    partial class frmPcmsoFuncoesGhe
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            this.flpAcao = new System.Windows.Forms.FlowLayoutPanel();
            this.btnIncluir = new System.Windows.Forms.Button();
            this.btnBuscar = new System.Windows.Forms.Button();
            this.btnLimpar = new System.Windows.Forms.Button();
            this.btnFechar = new System.Windows.Forms.Button();
            this.textGhe = new System.Windows.Forms.TextBox();
            this.lblGhe = new System.Windows.Forms.TextBox();
            this.lblFuncao = new System.Windows.Forms.TextBox();
            this.textFuncao = new System.Windows.Forms.TextBox();
            this.grbFuncao = new System.Windows.Forms.GroupBox();
            this.dgvFuncao = new System.Windows.Forms.DataGridView();
            this.llbExame = new System.Windows.Forms.TextBox();
            this.textExame = new System.Windows.Forms.TextBox();
            ((System.ComponentModel.ISupportInitialize)(this.scForm)).BeginInit();
            this.scForm.Panel1.SuspendLayout();
            this.scForm.Panel2.SuspendLayout();
            this.scForm.SuspendLayout();
            this.pnlForm.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.banner)).BeginInit();
            this.flpAcao.SuspendLayout();
            this.grbFuncao.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvFuncao)).BeginInit();
            this.SuspendLayout();
            // 
            // scForm
            // 
            // 
            // scForm.Panel1
            // 
            this.scForm.Panel1.Controls.Add(this.flpAcao);
            // 
            // pnlForm
            // 
            this.pnlForm.Controls.Add(this.textExame);
            this.pnlForm.Controls.Add(this.llbExame);
            this.pnlForm.Controls.Add(this.grbFuncao);
            this.pnlForm.Controls.Add(this.textFuncao);
            this.pnlForm.Controls.Add(this.lblFuncao);
            this.pnlForm.Controls.Add(this.lblGhe);
            this.pnlForm.Controls.Add(this.textGhe);
            this.pnlForm.TabIndex = 1;
            this.pnlForm.TabStop = true;
            // 
            // flpAcao
            // 
            this.flpAcao.Controls.Add(this.btnIncluir);
            this.flpAcao.Controls.Add(this.btnBuscar);
            this.flpAcao.Controls.Add(this.btnLimpar);
            this.flpAcao.Controls.Add(this.btnFechar);
            this.flpAcao.Dock = System.Windows.Forms.DockStyle.Fill;
            this.flpAcao.Location = new System.Drawing.Point(0, 0);
            this.flpAcao.Name = "flpAcao";
            this.flpAcao.Size = new System.Drawing.Size(534, 30);
            this.flpAcao.TabIndex = 0;
            // 
            // btnIncluir
            // 
            this.btnIncluir.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnIncluir.Image = global::SWS.Properties.Resources.fechar_ico;
            this.btnIncluir.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnIncluir.Location = new System.Drawing.Point(3, 3);
            this.btnIncluir.Name = "btnIncluir";
            this.btnIncluir.Size = new System.Drawing.Size(75, 23);
            this.btnIncluir.TabIndex = 12;
            this.btnIncluir.TabStop = false;
            this.btnIncluir.Text = "&Incluir";
            this.btnIncluir.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnIncluir.UseVisualStyleBackColor = true;
            this.btnIncluir.Click += new System.EventHandler(this.btnIncluir_Click);
            // 
            // btnBuscar
            // 
            this.btnBuscar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnBuscar.Image = global::SWS.Properties.Resources.busca;
            this.btnBuscar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnBuscar.Location = new System.Drawing.Point(84, 3);
            this.btnBuscar.Name = "btnBuscar";
            this.btnBuscar.Size = new System.Drawing.Size(75, 23);
            this.btnBuscar.TabIndex = 14;
            this.btnBuscar.Text = "&Buscar";
            this.btnBuscar.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnBuscar.UseVisualStyleBackColor = true;
            this.btnBuscar.Click += new System.EventHandler(this.btnBuscar_Click);
            // 
            // btnLimpar
            // 
            this.btnLimpar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnLimpar.Image = global::SWS.Properties.Resources.vassoura;
            this.btnLimpar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnLimpar.Location = new System.Drawing.Point(165, 3);
            this.btnLimpar.Name = "btnLimpar";
            this.btnLimpar.Size = new System.Drawing.Size(75, 23);
            this.btnLimpar.TabIndex = 11;
            this.btnLimpar.TabStop = false;
            this.btnLimpar.Text = "&Limpar";
            this.btnLimpar.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnLimpar.UseVisualStyleBackColor = true;
            this.btnLimpar.Click += new System.EventHandler(this.btnLimpar_Click);
            // 
            // btnFechar
            // 
            this.btnFechar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnFechar.Image = global::SWS.Properties.Resources.close;
            this.btnFechar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnFechar.Location = new System.Drawing.Point(246, 3);
            this.btnFechar.Name = "btnFechar";
            this.btnFechar.Size = new System.Drawing.Size(75, 23);
            this.btnFechar.TabIndex = 13;
            this.btnFechar.TabStop = false;
            this.btnFechar.Text = "&Fechar";
            this.btnFechar.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnFechar.UseVisualStyleBackColor = true;
            this.btnFechar.Click += new System.EventHandler(this.btnFechar_Click);
            // 
            // textGhe
            // 
            this.textGhe.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.textGhe.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textGhe.Enabled = false;
            this.textGhe.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textGhe.ForeColor = System.Drawing.Color.Black;
            this.textGhe.Location = new System.Drawing.Point(142, 13);
            this.textGhe.MaxLength = 100;
            this.textGhe.Name = "textGhe";
            this.textGhe.ReadOnly = true;
            this.textGhe.Size = new System.Drawing.Size(350, 21);
            this.textGhe.TabIndex = 10;
            this.textGhe.TabStop = false;
            // 
            // lblGhe
            // 
            this.lblGhe.BackColor = System.Drawing.Color.Silver;
            this.lblGhe.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblGhe.Enabled = false;
            this.lblGhe.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblGhe.ForeColor = System.Drawing.Color.Black;
            this.lblGhe.Location = new System.Drawing.Point(12, 13);
            this.lblGhe.MaxLength = 100;
            this.lblGhe.Name = "lblGhe";
            this.lblGhe.ReadOnly = true;
            this.lblGhe.Size = new System.Drawing.Size(131, 21);
            this.lblGhe.TabIndex = 11;
            this.lblGhe.TabStop = false;
            this.lblGhe.Text = "GHE";
            // 
            // lblFuncao
            // 
            this.lblFuncao.BackColor = System.Drawing.Color.Silver;
            this.lblFuncao.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblFuncao.Enabled = false;
            this.lblFuncao.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblFuncao.ForeColor = System.Drawing.Color.Black;
            this.lblFuncao.Location = new System.Drawing.Point(12, 53);
            this.lblFuncao.MaxLength = 100;
            this.lblFuncao.Name = "lblFuncao";
            this.lblFuncao.ReadOnly = true;
            this.lblFuncao.Size = new System.Drawing.Size(131, 21);
            this.lblFuncao.TabIndex = 12;
            this.lblFuncao.TabStop = false;
            this.lblFuncao.Text = "Função";
            // 
            // textFuncao
            // 
            this.textFuncao.BackColor = System.Drawing.Color.White;
            this.textFuncao.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textFuncao.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.textFuncao.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textFuncao.ForeColor = System.Drawing.Color.Black;
            this.textFuncao.Location = new System.Drawing.Point(142, 53);
            this.textFuncao.MaxLength = 100;
            this.textFuncao.Name = "textFuncao";
            this.textFuncao.Size = new System.Drawing.Size(350, 21);
            this.textFuncao.TabIndex = 1;
            // 
            // grbFuncao
            // 
            this.grbFuncao.Controls.Add(this.dgvFuncao);
            this.grbFuncao.Location = new System.Drawing.Point(12, 80);
            this.grbFuncao.Name = "grbFuncao";
            this.grbFuncao.Size = new System.Drawing.Size(482, 194);
            this.grbFuncao.TabIndex = 14;
            this.grbFuncao.TabStop = false;
            this.grbFuncao.Text = "Lista de Funções do GHE";
            // 
            // dgvFuncao
            // 
            this.dgvFuncao.AllowUserToAddRows = false;
            this.dgvFuncao.AllowUserToDeleteRows = false;
            this.dgvFuncao.AllowUserToOrderColumns = true;
            this.dgvFuncao.AllowUserToResizeRows = false;
            this.dgvFuncao.BackgroundColor = System.Drawing.Color.White;
            this.dgvFuncao.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.LightGray;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvFuncao.DefaultCellStyle = dataGridViewCellStyle1;
            this.dgvFuncao.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvFuncao.Location = new System.Drawing.Point(3, 16);
            this.dgvFuncao.MultiSelect = false;
            this.dgvFuncao.Name = "dgvFuncao";
            this.dgvFuncao.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvFuncao.Size = new System.Drawing.Size(476, 175);
            this.dgvFuncao.TabIndex = 4;
            this.dgvFuncao.DataError += new System.Windows.Forms.DataGridViewDataErrorEventHandler(this.dgvFuncao_DataError);
            this.dgvFuncao.EditingControlShowing += new System.Windows.Forms.DataGridViewEditingControlShowingEventHandler(this.dgvFuncao_EditingControlShowing);
            this.dgvFuncao.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.dgvFuncao_KeyPress);
            // 
            // llbExame
            // 
            this.llbExame.BackColor = System.Drawing.Color.Silver;
            this.llbExame.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.llbExame.Enabled = false;
            this.llbExame.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.llbExame.ForeColor = System.Drawing.Color.Black;
            this.llbExame.Location = new System.Drawing.Point(12, 33);
            this.llbExame.MaxLength = 100;
            this.llbExame.Name = "llbExame";
            this.llbExame.ReadOnly = true;
            this.llbExame.Size = new System.Drawing.Size(131, 21);
            this.llbExame.TabIndex = 15;
            this.llbExame.TabStop = false;
            this.llbExame.Text = "Exame";
            // 
            // textExame
            // 
            this.textExame.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.textExame.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textExame.Enabled = false;
            this.textExame.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textExame.ForeColor = System.Drawing.Color.Black;
            this.textExame.Location = new System.Drawing.Point(142, 33);
            this.textExame.MaxLength = 100;
            this.textExame.Name = "textExame";
            this.textExame.ReadOnly = true;
            this.textExame.Size = new System.Drawing.Size(350, 21);
            this.textExame.TabIndex = 16;
            this.textExame.TabStop = false;
            // 
            // frmPcmsoFuncoesGhe
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.ClientSize = new System.Drawing.Size(534, 361);
            this.Name = "frmPcmsoFuncoesGhe";
            this.Text = "SELECIONAR FUNÇÃO";
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.frmPcmsoFuncoesGhe_KeyDown);
            this.scForm.Panel1.ResumeLayout(false);
            this.scForm.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.scForm)).EndInit();
            this.scForm.ResumeLayout(false);
            this.pnlForm.ResumeLayout(false);
            this.pnlForm.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.banner)).EndInit();
            this.flpAcao.ResumeLayout(false);
            this.grbFuncao.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvFuncao)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btnLimpar;
        private System.Windows.Forms.Button btnIncluir;
        private System.Windows.Forms.Button btnFechar;
        private System.Windows.Forms.TextBox lblGhe;
        private System.Windows.Forms.TextBox textGhe;
        private System.Windows.Forms.TextBox lblFuncao;
        private System.Windows.Forms.TextBox textFuncao;
        private System.Windows.Forms.Button btnBuscar;
        private System.Windows.Forms.GroupBox grbFuncao;
        private System.Windows.Forms.DataGridView dgvFuncao;
        private System.Windows.Forms.TextBox textExame;
        private System.Windows.Forms.TextBox llbExame;
        protected System.Windows.Forms.FlowLayoutPanel flpAcao;
    }
}
