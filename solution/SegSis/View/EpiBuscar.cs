﻿using SWS.Entidade;
using SWS.Facade;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace SWS.View
{
    public partial class frmEpiBuscar : frmTemplateConsulta
    {
        private Epi epi;

        public Epi Epi
        {
            get { return epi; }
            set { epi = value; }
        }
        
        
        public frmEpiBuscar()
        {
            InitializeComponent();
        }

        private void btnFechar_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnPesquisar_Click(object sender, EventArgs e)
        {
            try
            {
                montaGrid();
                textEPI.Focus();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }

        private void montaGrid() 
        {
            try
            {
                dgvEpi.Columns.Clear();
                this.Cursor = Cursors.WaitCursor;

                EstudoFacade pcmsoFacade = EstudoFacade.getInstance();

                DataSet ds = pcmsoFacade.findEpiByFilter(new Epi(null, textEPI.Text, string.Empty, string.Empty, "A"));

                dgvEpi.DataSource = ds.Tables["Epis"].DefaultView;

                dgvEpi.Columns[0].HeaderText = "id";
                dgvEpi.Columns[0].Visible = false;

                dgvEpi.Columns[1].HeaderText = "Descrição";
                dgvEpi.Columns[2].HeaderText = "Finalidade";
                dgvEpi.Columns[2].Visible = false;
                dgvEpi.Columns[3].HeaderText = "CA";
                dgvEpi.Columns[4].HeaderText = "Situação";
                dgvEpi.Columns[4].Visible = false;

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }


        }

        private void btnSelecionar_Click(object sender, EventArgs e)
        {
            try
            {
                if (dgvEpi.CurrentRow == null)
                    throw new Exception("Selecione uma linha");

                EstudoFacade pcmsoFacace = EstudoFacade.getInstance();

                this.Epi = pcmsoFacace.findEpiById((long)dgvEpi.CurrentRow.Cells[0].Value);
                Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }
    }
}
