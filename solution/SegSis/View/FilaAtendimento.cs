﻿using SWS.Entidade;
using SWS.Facade;
using SWS.View.Resources;
using SWS.View.ViewHelper;
using SWS.ViewHelper;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace SWS.View
{
    public partial class frmFilaAtendimento : frmTemplate
    {

        private HashSet<Exame> filtroExames = new HashSet<Exame>();

        private Dictionary<String, LinkedList<ItemFilaAtendimento>> atendimento = new Dictionary<String, LinkedList<ItemFilaAtendimento>>();
        private LinkedList<ItemFilaAtendimento> listaAtendimentos = new LinkedList<ItemFilaAtendimento>();
        private ItemFilaAtendimento atendimentosInGrid = null;

        private Sala salaAtendimento;
        
        public frmFilaAtendimento()
        {
            InitializeComponent();
            btnAtender.Enabled = false;
            validaPermissoes();
            ActiveControl = cbSala;
            MaximizeBox = true;
            WindowState = FormWindowState.Maximized;
            ComboHelper.atendimentoFila(cbSituacao);
        }

        private void btnAtender_Click(object sender, EventArgs e)
        {
            try
            {
                this.Cursor = Cursors.WaitCursor;

                FilaFacade filaFacade = FilaFacade.getInstance();
                AsoFacade AsoFacade = AsoFacade.getInstance();
                LinkedList<ItemFilaAtendimento> listaAtendimento = null;
                LinkedList<ItemFilaAtendimento> listaTodosAtendimento = new LinkedList<ItemFilaAtendimento>();
                List<string> nomesSendoAtendidos = new List<string>();
                StringBuilder exames = new StringBuilder(); //colecao de todos os exames por atendimento
                /* verificado se o usuário não selecionou nenhum tipo de atendimento */
                bool result = false;
                int qtdeResult = 0;
                foreach (DataGridViewRow dvRow in dgvFilaAtendimento.Rows)
                {
                    if ((bool)dvRow.Cells[14].Value)
                    {
                        result = true;
                        qtdeResult ++;
                        
                        atendimento.TryGetValue((String)dvRow.Cells[0].Value, out listaAtendimento);

                        AsoFacade asoFacade = AsoFacade.getInstance();
                        Aso aso = filaFacade.findAsoByItemFilaAtendimento(listaAtendimento.First());
                        List<AtendimentoExame> exameEmAtendimentoEmOutraSala = asoFacade.FindAllExamesByAsoByBySalaInService(aso, salaAtendimento);

                        if (exameEmAtendimentoEmOutraSala.Count > 0)
                        {
                            nomesSendoAtendidos.Add(dvRow.Cells[1].Value.ToString() + " - " + exameEmAtendimentoEmOutraSala.ElementAt(0).Sala.Descricao);
                        }
                        else
                        {
                            listaAtendimento.ToList().ForEach(delegate(ItemFilaAtendimento item)
                            {
                                listaTodosAtendimento.AddFirst(item);
                            });
                        }
                    }
                }
                if (!result) throw new Exception("Selecione um atendimento.");
                else
                {
                    /* verificando se foi selecionado um numero maior de quantidade permitida por sala */

                    if (salaAtendimento.QtdeChamada < qtdeResult)
                        throw new Exception("Você selecionou uma quantidade de atendimento maior do que a sala permite. Refaça sua lista ");

                    if (nomesSendoAtendidos.Count > 0)
                    {
                        StringBuilder nomesEmAtendimento = new StringBuilder();
                        nomesSendoAtendidos.ForEach(delegate(string nome)
                        {
                            nomesEmAtendimento.Append(nome + System.Environment.NewLine);
                        });

                        MessageBox.Show("O(s) calaborador(es) abaixo estão em atendimento nas respectivas salas: " + System.Environment.NewLine + nomesEmAtendimento.ToString(), "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }
                    if (listaTodosAtendimento.Count > 0)
                    {
                        /* incluindo o atendimento na tabela de acompanhamentoAtendimento se a seleção for pendente */
                        if (string.Equals(((SelectItem)cbSituacao.SelectedItem).Valor, "P"))
                        {
                            /* agrupando todos os atendimentos selecionados e para cada primeiro elemento será adicionado
                             * apenas uma unica vez na tabela seg_atendimento. Com isso o colaborador será chamado apenas uma vez
                             * por sala. */
                            List<ItemFilaAtendimento> itemOrdenado = listaTodosAtendimento.OrderBy(item => item.SenhaAtendimento).ToList();
                            var query = itemOrdenado.ToList().GroupBy(item => item.CodigoAtendimento);
                            
                            foreach(var item in query)
                            {
                                filaFacade.insertAcompanhamentoAtendimento(new AcompanhamentoAtendimento(null, item.First().Nome, filaFacade.findSalaById(Convert.ToInt64(((SelectItem)cbSala.SelectedItem).Valor)).SlugSala, filaFacade.findSalaById(Convert.ToInt64(((SelectItem)cbSala.SelectedItem).Valor)).Andar, item.First().Empresa, item.First().Rg, DateTime.Now, item.First().SenhaAtendimento.ToString(), false, item.First().Cpf, item.First().DataAtendido, (long)AsoFacade.findAsoByCodigo(item.First().CodigoAtendimento).Id, item.First().CodigoAtendimento, item.First().Exame.Descricao, (long)PermissionamentoFacade.usuarioAutenticado.Empresa.Id, (string)PermissionamentoFacade.usuarioAutenticado.Empresa.Fantasia));
                            }
                        }
                        
                        frmFilaAtendimentoExames formFilaAtendimentoExames = new frmFilaAtendimentoExames(listaTodosAtendimento, salaAtendimento);
                        formFilaAtendimentoExames.ShowDialog();
                        montaGridFilaAtendimento();
                    }
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }

        private void btnPesquisar_Click(object sender, EventArgs e)
        {
            try
            {
                if (ValidaTela())
                {
                    textCodigoAtendimento.Focus();
                    montaGridFilaAtendimento();
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btnLimpar_Click(object sender, EventArgs e)
        {
            try
            {

                filtroExames.Clear();
                cbSala.Items.Clear();
                carregaComboSalas(cbSala);
                dataInicial.Value = DateTime.Now;
                dataFinal.Value = DateTime.Now;
                textCodigoAtendimento.Text = string.Empty;
                montaGridFiltroExame();

                this.dgvFilaAtendimento.Columns.Clear();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btnProntuario_Click(object sender, EventArgs e)
        {
            try
            {
                this.Cursor = Cursors.WaitCursor;
                AsoFacade asoFacade = AsoFacade.getInstance();
                Aso atendimento = null;
                bool selecionado = false;

                foreach (DataGridViewRow dvRow in dgvFilaAtendimento.Rows)
                {
                    if ((bool)dvRow.Cells[14].Value)
                    {

                        /* verificando se o usuário não marcou mais de um atendimento para prontuariazar */
                        selecionado = true;
                        string codigoAtendimentoMarcado = dvRow.Cells[0].Value.ToString();

                        foreach (DataGridViewRow dvRowIn in dgvFilaAtendimento.Rows)
                        {
                            if ((bool)dvRowIn.Cells[14].Value == true && !string.Equals(dvRowIn.Cells[0].Value, codigoAtendimentoMarcado))
                                throw new Exception("Não pode laudar mais de um atendimento por vez.");
                        }

                        atendimento = asoFacade.findAsoByCodigo((string)dvRow.Cells[0].Value);

                        if (atendimento == null)
                            throw new Exception("Atendimento não encontrado.");
                    }
                }

                if (!selecionado)
                    throw new Exception("Selecione um atendimento.");

                Prontuario prontuario = asoFacade.findProntuarioByAso(atendimento);

                if (prontuario != null)
                {
                    frmProntuario formProntuario = new frmProntuario(prontuario, true);
                    formProntuario.ShowDialog();
                }
                else
                {

                    /* procurando o último prontuário do colaborador */
                    Prontuario lasProntuario = asoFacade.findLastProntuarioByFuncionario(atendimento.ClienteFuncaoFuncionario.Funcionario);

                    if (lasProntuario != null)
                    {

                        if (MessageBox.Show("Deseja carregar o último prontuário do colaborador?", "Atenção", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                        {
                            lasProntuario.Aso = atendimento;
                            frmProntuario formProntuario = new frmProntuario(lasProntuario, false);
                            formProntuario.ShowDialog();
                        }
                    }
                    else
                    {
                        prontuario = new Prontuario();
                        prontuario.Aso = atendimento;
                        frmProntuario formProntuario = new frmProntuario(prontuario, false);
                        formProntuario.ShowDialog();
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }

        private void btnFechar_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnAtendimento_Click(object sender, EventArgs e)
        {
            try
            {
                this.Cursor = Cursors.WaitCursor;
                int linha = 0;
                Boolean codigoEncontrado = false;

                foreach (DataGridViewRow dvRow in dgvFilaAtendimento.Rows)
                {
                    if (String.Equals(dvRow.Cells[0].Value, textCodigoAtendimento.Text.Replace(".", "")))
                    {
                        linha = dvRow.Index;
                        codigoEncontrado = true;
                        break;
                    }
                }

                /* atribuindo ao controle dataGrid a linha encontrada e 
                 * posicionado a sua selecao.
                 */
                if (codigoEncontrado)
                {
                    dgvFilaAtendimento.CurrentCell = dgvFilaAtendimento.Rows[linha].Cells[11];
                    dgvFilaAtendimento.Rows[linha].Selected = true;
                    textCodigoAtendimento.Text = String.Empty;
                    textCodigoAtendimento.Focus();
                }
                else
                {
                    MessageBox.Show("Código de atendimento não encontrado", "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    textCodigoAtendimento.Focus();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }

        private void cbSala_SelectedValueChanged(object sender, EventArgs e)
        {
            try
            {
                FilaFacade filaFacade = FilaFacade.getInstance();
                salaAtendimento = filaFacade.findSalaById(Convert.ToInt64(((SelectItem)cbSala.SelectedItem).Valor));

                filtroExames = filaFacade.buscaExamesBySala(salaAtendimento);
                montaGridFiltroExame();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void validaPermissoes()
        {
            PermissionamentoFacade permissionamentoFacade = PermissionamentoFacade.getInstance();

            this.btnAtender.Enabled = permissionamentoFacade.hasPermission("FILA_ATENDIMENTO", "FINALIZAR");
            this.btnProntuario.Enabled = permissionamentoFacade.hasPermission("FILA_ATENDIMENTO", "PRONTUARIZAR");
        }

        private void carregaComboSalas(ComboBox combo)
        {
            try
            {
                FilaFacade filaFacade = FilaFacade.getInstance();

                foreach (DataRow rows in filaFacade.findSalaByFilter(new Sala(null, string.Empty, ApplicationConstants.ATIVO, 0, false, PermissionamentoFacade.usuarioAutenticado.Empresa, string.Empty, 0)).Tables[0].Rows)
                    combo.Items.Add(new SelectItem(Convert.ToString(rows[Convert.ToString("id_sala")]), Convert.ToString(rows[Convert.ToString("descricao")])));

                combo.DisplayMember = "Nome";
                combo.DropDownStyle = ComboBoxStyle.DropDownList;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void desabilitaSort(DataGridView dataGridView)
        {
            foreach (DataGridViewColumn column in dataGridView.Columns)
                column.SortMode = DataGridViewColumnSortMode.NotSortable;
        }

        public void montaGridFilaAtendimento()
        {
            try
            {
                this.Cursor = Cursors.WaitCursor;
                textTotalAtendimento.Text = string.Empty;
                listaAtendimentos.Clear();

                FilaFacade filaFacade = FilaFacade.getInstance();

                atendimento.Clear();

                dgvFilaAtendimento.Columns.Clear();

                dgvFilaAtendimento.ColumnCount = 14;

                dgvFilaAtendimento.Columns[0].HeaderText = "CodAtendimento";
                dgvFilaAtendimento.Columns[0].ReadOnly = true;

                dgvFilaAtendimento.Columns[1].HeaderText = "Nome";
                dgvFilaAtendimento.Columns[1].ReadOnly = true;

                dgvFilaAtendimento.Columns[2].HeaderText = "Data Nascimento";
                dgvFilaAtendimento.Columns[2].ReadOnly = true;

                dgvFilaAtendimento.Columns[3].HeaderText = "CPF";
                dgvFilaAtendimento.Columns[3].ReadOnly = true;

                dgvFilaAtendimento.Columns[4].HeaderText = "Função";
                dgvFilaAtendimento.Columns[4].ReadOnly = true;

                dgvFilaAtendimento.Columns[5].HeaderText = "Empresa";
                dgvFilaAtendimento.Columns[5].ReadOnly = true;

                dgvFilaAtendimento.Columns[6].HeaderText = "RG";
                dgvFilaAtendimento.Columns[6].ReadOnly = true;

                dgvFilaAtendimento.Columns[7].HeaderText = "Peso";
                dgvFilaAtendimento.Columns[7].ReadOnly = true;

                dgvFilaAtendimento.Columns[8].HeaderText = "Altura";
                dgvFilaAtendimento.Columns[8].ReadOnly = true;

                dgvFilaAtendimento.Columns[9].HeaderText = "Periodicidade";
                dgvFilaAtendimento.Columns[9].ReadOnly = true;

                dgvFilaAtendimento.Columns[10].HeaderText = "IMC";
                dgvFilaAtendimento.Columns[10].ReadOnly = true;

                dgvFilaAtendimento.Columns[11].HeaderText = "Prestador";
                dgvFilaAtendimento.Columns[11].ReadOnly = true;

                dgvFilaAtendimento.Columns[12].HeaderText = "Prioridade";
                dgvFilaAtendimento.Columns[12].Visible = false;

                dgvFilaAtendimento.Columns[13].HeaderText = "Id";
                dgvFilaAtendimento.Columns[13].Visible = false;

                /* incluíndo possibilidade para incluir multiplos atendimentos. */

                DataGridViewCheckBoxColumn selecionar = new DataGridViewCheckBoxColumn();
                selecionar.Name = "Check";

                dgvFilaAtendimento.Columns.Add(selecionar);
                dgvFilaAtendimento.Columns[14].DisplayIndex = 0;

                DataGridViewTextBoxColumn senhaAtendimento = new DataGridViewTextBoxColumn();
                senhaAtendimento.Name = "Senha";
                dgvFilaAtendimento.Columns.Add(senhaAtendimento);
                dgvFilaAtendimento.Columns[15].DisplayIndex = 1;

                if (string.Equals(((SelectItem)cbSituacao.SelectedItem).Valor, "P"))
                {
                    /* exames pendentes */
                    
                    /* Agrupando exames por atendimento através do código do atendimento */

                    listaAtendimentos = filaFacade.buscaExamesNaoFinalizados(Convert.ToDateTime(dataInicial.Text + " 00:00:00"), Convert.ToDateTime(dataFinal.Text + " 23:59:59"), salaAtendimento);

                    listaAtendimentos.ToList().ForEach(delegate(ItemFilaAtendimento item)
                    {
                        atendimento.TryGetValue(item.CodigoAtendimento, out listaAtendimentos);
                        if (listaAtendimentos == null || listaAtendimentos.Count == 0)
                        {
                            listaAtendimentos = new LinkedList<ItemFilaAtendimento>();
                            listaAtendimentos.AddFirst(item);
                            atendimento.Add(item.CodigoAtendimento, listaAtendimentos);
                        }
                        else
                        {
                            listaAtendimentos.AddFirst(item);
                        }
                    });

                    atendimento.Keys.ToList().ForEach(delegate(string codigoAtendimento)
                    {
                        atendimento.TryGetValue(codigoAtendimento, out listaAtendimentos);
                        if (listaAtendimentos != null && listaAtendimentos.Count > 0)
                        {
                            atendimentosInGrid = listaAtendimentos.First();
                            dgvFilaAtendimento.Rows.Add(atendimentosInGrid.CodigoAtendimento, atendimentosInGrid.Nome, atendimentosInGrid.DataNascimento, atendimentosInGrid.Cpf, atendimentosInGrid.Funcao, atendimentosInGrid.Empresa, atendimentosInGrid.Rg, atendimentosInGrid.Peso, atendimentosInGrid.Altura, atendimentosInGrid.Periodicidade, atendimentosInGrid.Imc, atendimentosInGrid.Prestador, atendimentosInGrid.Prioridade, atendimentosInGrid.IdItem, false, atendimentosInGrid.SenhaAtendimento);
                        }
                    });

                }
                else
                {
                    /* exames realizados */

                    listaAtendimentos = filaFacade.buscaExamesFinalizados(Convert.ToDateTime(dataInicial.Text + " 00:00:00"), Convert.ToDateTime(dataFinal.Text + " 23:59:59"), salaAtendimento);

                    listaAtendimentos.ToList().ForEach(delegate(ItemFilaAtendimento item)
                    {
                        atendimento.TryGetValue(item.CodigoAtendimento, out listaAtendimentos);
                        if (listaAtendimentos == null || listaAtendimentos.Count == 0)
                        {
                            listaAtendimentos = new LinkedList<ItemFilaAtendimento>();
                            listaAtendimentos.AddFirst(item);
                            atendimento.Add(item.CodigoAtendimento, listaAtendimentos);
                        }
                        else
                        {
                            listaAtendimentos.AddFirst(item);
                        }
                    });

                    atendimento.Keys.ToList().ForEach(delegate(string codigoAtendimento)
                    {
                        atendimento.TryGetValue(codigoAtendimento, out listaAtendimentos);
                        if (listaAtendimentos != null && listaAtendimentos.Count > 0)
                        {
                            atendimentosInGrid = listaAtendimentos.First();
                            dgvFilaAtendimento.Rows.Add(atendimentosInGrid.CodigoAtendimento, atendimentosInGrid.Nome, atendimentosInGrid.DataNascimento, atendimentosInGrid.Cpf, atendimentosInGrid.Funcao, atendimentosInGrid.Empresa, atendimentosInGrid.Rg, atendimentosInGrid.Peso, atendimentosInGrid.Altura, atendimentosInGrid.Periodicidade, atendimentosInGrid.Imc, atendimentosInGrid.Prestador, atendimentosInGrid.Prioridade, atendimentosInGrid.IdItem, false, atendimentosInGrid.SenhaAtendimento);
                        }
                    });


                }
                textTotalAtendimento.Text = dgvFilaAtendimento.Rows.Count.ToString();
                desabilitaSort(dgvFilaAtendimento);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }

        public void montaGridFiltroExame()
        {
            dgvExames.Columns.Clear();

            dgvExames.ColumnCount = 5;

            dgvExames.Columns[0].HeaderText = "IdExame";
            dgvExames.Columns[0].Visible = false;

            dgvExames.Columns[1].HeaderText = "Descrição";
            
            dgvExames.Columns[2].HeaderText = "Situação";
            dgvExames.Columns[2].Visible = false;

            dgvExames.Columns[3].HeaderText = "Laboratório";
            dgvExames.Columns[3].Visible = false;

            dgvExames.Columns[4].HeaderText = "Preço";
            dgvExames.Columns[4].Visible = false;

            filtroExames.ToList().ForEach(delegate(Exame exame)
            {
                dgvExames.Rows.Add(exame.Id, exame.Descricao, exame.Situacao, exame.Laboratorio, exame.Preco.ToString());
            });

        }

        private bool ValidaTela()
        {
            bool retorno = true;
            try
            {
                if (salaAtendimento == null)
                    throw new Exception("Você deve selecionar uma sala para a pesquisa.");

                if (!dataInicial.Checked && !dataFinal.Checked)
                    throw new Exception("Você deve selecionar uma data para pesquisa.");

                if (dataInicial.Checked && !dataFinal.Checked)
                    throw new Exception("Você deve selecionar a data final para a pesquisa.");

                if (dataFinal.Checked && !dataInicial.Checked)
                    throw new Exception("Você deve selecionar a data inicial para a pesquisa.");

                if (Convert.ToDateTime(dataInicial.Text + " 00:00:00") > Convert.ToDateTime(dataFinal.Text + " 23:59:59"))
                    throw new Exception("A data inicial não pode ser maior do que a data final.");

            }
            catch (Exception ex)
            {
                retorno = false;
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            return retorno;
        }

        private void dgvFilaAtendimento_RowPrePaint(object sender, DataGridViewRowPrePaintEventArgs e)
        {
            DataGridView dgv = sender as DataGridView;

            if (Convert.ToBoolean(dgv.Rows[e.RowIndex].Cells[12].Value))
                dgv.Rows[e.RowIndex].DefaultCellStyle.BackColor = Color.ForestGreen;
        }

        private void dgvFilaAtendimento_CellContentDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            btnAtender.PerformClick();
        }

        private void frmFilaDeAtendimento_Load(object sender, EventArgs e)
        {
            carregaComboSalas(cbSala);
        }

        private void dgvFilaAtendimento_DataBindingComplete(object sender, DataGridViewBindingCompleteEventArgs e)
        {
            DataGridView dgv = sender as DataGridView;
            dgv.ClearSelection();
        }

        private void cbSituacao_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (string.Equals(((SelectItem)cbSituacao.SelectedItem).Valor, "P"))
            {
                dgvFilaAtendimento.Columns.Clear();
                btnPesquisar.PerformClick();
                btnAtender.Enabled = true;
                btnLaudar.Enabled = false;
            }
            else
            {
                dgvFilaAtendimento.Columns.Clear();
                btnPesquisar.PerformClick();
                btnAtender.Enabled = false;
                btnLaudar.Enabled = true;
            }
        }

        private void btnLaudar_Click(object sender, EventArgs e)
        {
            try
            {
                this.Cursor = Cursors.WaitCursor;
                AsoFacade asoFacade = AsoFacade.getInstance();
                Aso aso = null;
                bool selecionado = false;

                LinkedList<ItemFilaAtendimento> listaAtendido = new LinkedList<ItemFilaAtendimento>();

                foreach (DataGridViewRow dvRow in dgvFilaAtendimento.Rows)
                {
                    if ((bool)dvRow.Cells[14].Value)
                    {
                        selecionado = true;

                        string codigoAtendimentoMarcado = dvRow.Cells[0].Value.ToString();

                        foreach (DataGridViewRow dvRowIn in dgvFilaAtendimento.Rows)
                        {
                            if ((bool)dvRowIn.Cells[14].Value == true && !string.Equals(dvRowIn.Cells[0].Value, codigoAtendimentoMarcado))
                                throw new Exception("Não pode laudar mais de um atendimento por vez.");
                        }
                        
                        if (listaAtendido != null && listaAtendido.Count > 0)
                        {
                            listaAtendido.AddFirst(this.listaAtendimentos.First(x => x.IdItem == (long)dvRow.Cells[14].Value));
                        }
                        else
                        {
                            atendimento.TryGetValue((String)dvRow.Cells[0].Value, out listaAtendido);
                        }
                        
                        aso = asoFacade.findAsoByCodigo((string)dvRow.Cells[0].Value);
                    }
                }


                if (!selecionado)
                    throw new Exception("Selecione um atendimento.");

                frmAtendimentoLaudar laudarExame = new frmAtendimentoLaudar(aso, listaAtendido, false);
                laudarExame.ShowDialog();

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }

        
    }
}
