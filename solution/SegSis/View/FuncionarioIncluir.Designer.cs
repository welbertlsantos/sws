﻿namespace SWS.View
{
    partial class frmFuncionarioIncluir
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmFuncionarioIncluir));
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            this.flpAcao = new System.Windows.Forms.FlowLayoutPanel();
            this.btGravar = new System.Windows.Forms.Button();
            this.btImprimir = new System.Windows.Forms.Button();
            this.btLimpar = new System.Windows.Forms.Button();
            this.btFechar = new System.Windows.Forms.Button();
            this.textIdade = new System.Windows.Forms.TextBox();
            this.lblIdade = new System.Windows.Forms.TextBox();
            this.textCtps = new System.Windows.Forms.TextBox();
            this.lblCtps = new System.Windows.Forms.TextBox();
            this.textAltura = new System.Windows.Forms.TextBox();
            this.lblAltura = new System.Windows.Forms.TextBox();
            this.textPeso = new System.Windows.Forms.TextBox();
            this.lblPeso = new System.Windows.Forms.TextBox();
            this.cbFatorRh = new SWS.ComboBoxWithBorder();
            this.lblFatorRh = new System.Windows.Forms.TextBox();
            this.cbTipoSanguineo = new SWS.ComboBoxWithBorder();
            this.lblTipoSanguineo = new System.Windows.Forms.TextBox();
            this.textEmail = new System.Windows.Forms.TextBox();
            this.lblEmail = new System.Windows.Forms.TextBox();
            this.cbSexo = new SWS.ComboBoxWithBorder();
            this.lblSexo = new System.Windows.Forms.TextBox();
            this.lblDataNascimento = new System.Windows.Forms.TextBox();
            this.textRg = new System.Windows.Forms.TextBox();
            this.dataNascimento = new System.Windows.Forms.DateTimePicker();
            this.lblRg = new System.Windows.Forms.TextBox();
            this.textCpf = new System.Windows.Forms.MaskedTextBox();
            this.lblCpf = new System.Windows.Forms.TextBox();
            this.textNome = new System.Windows.Forms.TextBox();
            this.lblNome = new System.Windows.Forms.TextBox();
            this.textTelefone = new System.Windows.Forms.TextBox();
            this.btAlterarCliente = new System.Windows.Forms.Button();
            this.btExcluirCliente = new System.Windows.Forms.Button();
            this.btIncluirCliente = new System.Windows.Forms.Button();
            this.grbCliente = new System.Windows.Forms.GroupBox();
            this.dgvCliente = new System.Windows.Forms.DataGridView();
            this.grbFuncao = new System.Windows.Forms.GroupBox();
            this.dgvFuncao = new System.Windows.Forms.DataGridView();
            this.epFuncionarioIncluir = new System.Windows.Forms.ErrorProvider(this.components);
            this.tPFuncionarioInclusao = new System.Windows.Forms.ToolTip(this.components);
            this.lblOrgaoEmissor = new System.Windows.Forms.TextBox();
            this.textOrgaoEmissor = new System.Windows.Forms.TextBox();
            this.lblUfEmissor = new System.Windows.Forms.TextBox();
            this.cbUfEmissor = new SWS.ComboBoxWithBorder();
            this.flpAcaoCliente = new System.Windows.Forms.FlowLayoutPanel();
            this.btCidadeComercial = new System.Windows.Forms.Button();
            this.lblPis = new System.Windows.Forms.TextBox();
            this.textPis = new System.Windows.Forms.MaskedTextBox();
            this.cbPcd = new SWS.ComboBoxWithBorder();
            this.lblPcd = new System.Windows.Forms.TextBox();
            this.lblLogradouro = new System.Windows.Forms.TextBox();
            this.textLogradouro = new System.Windows.Forms.TextBox();
            this.textNumero = new System.Windows.Forms.TextBox();
            this.lblNumero = new System.Windows.Forms.TextBox();
            this.lblComplemento = new System.Windows.Forms.TextBox();
            this.textComplemento = new System.Windows.Forms.TextBox();
            this.textBairro = new System.Windows.Forms.TextBox();
            this.lblBairro = new System.Windows.Forms.TextBox();
            this.lblCEP = new System.Windows.Forms.TextBox();
            this.textCEP = new System.Windows.Forms.MaskedTextBox();
            this.cbUF = new SWS.ComboBoxWithBorder();
            this.lblUF = new System.Windows.Forms.TextBox();
            this.lblCidade = new System.Windows.Forms.TextBox();
            this.textCidade = new System.Windows.Forms.TextBox();
            this.lblTelefoneComercial = new System.Windows.Forms.TextBox();
            this.textCelular = new System.Windows.Forms.TextBox();
            this.lblCelular = new System.Windows.Forms.TextBox();
            this.lblSerieCtps = new System.Windows.Forms.TextBox();
            this.lblUfEmissorCtps = new System.Windows.Forms.TextBox();
            this.textSerieCtps = new System.Windows.Forms.TextBox();
            this.cbUfEmissorCtps = new SWS.ComboBoxWithBorder();
            ((System.ComponentModel.ISupportInitialize)(this.spForm)).BeginInit();
            this.spForm.Panel1.SuspendLayout();
            this.spForm.Panel2.SuspendLayout();
            this.spForm.SuspendLayout();
            this.pnlForm.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.banner)).BeginInit();
            this.flpAcao.SuspendLayout();
            this.grbCliente.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvCliente)).BeginInit();
            this.grbFuncao.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvFuncao)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.epFuncionarioIncluir)).BeginInit();
            this.flpAcaoCliente.SuspendLayout();
            this.SuspendLayout();
            // 
            // spForm
            // 
            // 
            // spForm.Panel1
            // 
            this.spForm.Panel1.Controls.Add(this.flpAcao);
            // 
            // pnlForm
            // 
            this.pnlForm.Controls.Add(this.cbUfEmissorCtps);
            this.pnlForm.Controls.Add(this.textSerieCtps);
            this.pnlForm.Controls.Add(this.lblUfEmissorCtps);
            this.pnlForm.Controls.Add(this.lblSerieCtps);
            this.pnlForm.Controls.Add(this.flpAcaoCliente);
            this.pnlForm.Controls.Add(this.cbUfEmissor);
            this.pnlForm.Controls.Add(this.lblUfEmissor);
            this.pnlForm.Controls.Add(this.textOrgaoEmissor);
            this.pnlForm.Controls.Add(this.lblOrgaoEmissor);
            this.pnlForm.Controls.Add(this.cbPcd);
            this.pnlForm.Controls.Add(this.lblPcd);
            this.pnlForm.Controls.Add(this.grbFuncao);
            this.pnlForm.Controls.Add(this.grbCliente);
            this.pnlForm.Controls.Add(this.btCidadeComercial);
            this.pnlForm.Controls.Add(this.textCelular);
            this.pnlForm.Controls.Add(this.lblCelular);
            this.pnlForm.Controls.Add(this.lblTelefoneComercial);
            this.pnlForm.Controls.Add(this.textTelefone);
            this.pnlForm.Controls.Add(this.lblCidade);
            this.pnlForm.Controls.Add(this.textCidade);
            this.pnlForm.Controls.Add(this.textCEP);
            this.pnlForm.Controls.Add(this.lblUF);
            this.pnlForm.Controls.Add(this.lblCEP);
            this.pnlForm.Controls.Add(this.lblBairro);
            this.pnlForm.Controls.Add(this.lblComplemento);
            this.pnlForm.Controls.Add(this.lblNumero);
            this.pnlForm.Controls.Add(this.lblLogradouro);
            this.pnlForm.Controls.Add(this.textLogradouro);
            this.pnlForm.Controls.Add(this.textNumero);
            this.pnlForm.Controls.Add(this.textComplemento);
            this.pnlForm.Controls.Add(this.textBairro);
            this.pnlForm.Controls.Add(this.cbUF);
            this.pnlForm.Controls.Add(this.textIdade);
            this.pnlForm.Controls.Add(this.lblIdade);
            this.pnlForm.Controls.Add(this.textPis);
            this.pnlForm.Controls.Add(this.lblPis);
            this.pnlForm.Controls.Add(this.textCtps);
            this.pnlForm.Controls.Add(this.lblCtps);
            this.pnlForm.Controls.Add(this.textAltura);
            this.pnlForm.Controls.Add(this.lblAltura);
            this.pnlForm.Controls.Add(this.textPeso);
            this.pnlForm.Controls.Add(this.lblPeso);
            this.pnlForm.Controls.Add(this.cbFatorRh);
            this.pnlForm.Controls.Add(this.lblFatorRh);
            this.pnlForm.Controls.Add(this.cbTipoSanguineo);
            this.pnlForm.Controls.Add(this.lblTipoSanguineo);
            this.pnlForm.Controls.Add(this.textEmail);
            this.pnlForm.Controls.Add(this.lblEmail);
            this.pnlForm.Controls.Add(this.cbSexo);
            this.pnlForm.Controls.Add(this.lblSexo);
            this.pnlForm.Controls.Add(this.lblDataNascimento);
            this.pnlForm.Controls.Add(this.textRg);
            this.pnlForm.Controls.Add(this.dataNascimento);
            this.pnlForm.Controls.Add(this.lblRg);
            this.pnlForm.Controls.Add(this.textCpf);
            this.pnlForm.Controls.Add(this.lblCpf);
            this.pnlForm.Controls.Add(this.textNome);
            this.pnlForm.Controls.Add(this.lblNome);
            this.pnlForm.Size = new System.Drawing.Size(764, 1000);
            // 
            // flpAcao
            // 
            this.flpAcao.Controls.Add(this.btGravar);
            this.flpAcao.Controls.Add(this.btImprimir);
            this.flpAcao.Controls.Add(this.btLimpar);
            this.flpAcao.Controls.Add(this.btFechar);
            this.flpAcao.Dock = System.Windows.Forms.DockStyle.Fill;
            this.flpAcao.Location = new System.Drawing.Point(0, 0);
            this.flpAcao.Name = "flpAcao";
            this.flpAcao.Size = new System.Drawing.Size(784, 35);
            this.flpAcao.TabIndex = 0;
            // 
            // btGravar
            // 
            this.btGravar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btGravar.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btGravar.Image = global::SWS.Properties.Resources.Gravar;
            this.btGravar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btGravar.Location = new System.Drawing.Point(3, 3);
            this.btGravar.Name = "btGravar";
            this.btGravar.Size = new System.Drawing.Size(77, 23);
            this.btGravar.TabIndex = 35;
            this.btGravar.TabStop = false;
            this.btGravar.Text = "&Gravar";
            this.btGravar.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btGravar.UseVisualStyleBackColor = true;
            this.btGravar.Click += new System.EventHandler(this.btGravar_Click);
            // 
            // btImprimir
            // 
            this.btImprimir.Enabled = false;
            this.btImprimir.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btImprimir.Image = global::SWS.Properties.Resources.impressora1;
            this.btImprimir.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btImprimir.Location = new System.Drawing.Point(86, 3);
            this.btImprimir.Name = "btImprimir";
            this.btImprimir.Size = new System.Drawing.Size(77, 23);
            this.btImprimir.TabIndex = 36;
            this.btImprimir.TabStop = false;
            this.btImprimir.Text = "&Imprimir";
            this.btImprimir.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btImprimir.UseVisualStyleBackColor = true;
            this.btImprimir.Click += new System.EventHandler(this.btImprimir_Click);
            // 
            // btLimpar
            // 
            this.btLimpar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btLimpar.Image = global::SWS.Properties.Resources.vassoura;
            this.btLimpar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btLimpar.Location = new System.Drawing.Point(169, 3);
            this.btLimpar.Name = "btLimpar";
            this.btLimpar.Size = new System.Drawing.Size(77, 23);
            this.btLimpar.TabIndex = 38;
            this.btLimpar.TabStop = false;
            this.btLimpar.Text = "&Limpar";
            this.btLimpar.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btLimpar.UseVisualStyleBackColor = true;
            this.btLimpar.Click += new System.EventHandler(this.btLimpar_Click);
            // 
            // btFechar
            // 
            this.btFechar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btFechar.Image = global::SWS.Properties.Resources.close;
            this.btFechar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btFechar.Location = new System.Drawing.Point(252, 3);
            this.btFechar.Name = "btFechar";
            this.btFechar.Size = new System.Drawing.Size(77, 23);
            this.btFechar.TabIndex = 37;
            this.btFechar.TabStop = false;
            this.btFechar.Text = "&Fechar";
            this.btFechar.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btFechar.UseVisualStyleBackColor = true;
            this.btFechar.Click += new System.EventHandler(this.btFechar_Click);
            // 
            // textIdade
            // 
            this.textIdade.BackColor = System.Drawing.Color.White;
            this.textIdade.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textIdade.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.textIdade.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textIdade.ForeColor = System.Drawing.Color.Blue;
            this.textIdade.Location = new System.Drawing.Point(209, 128);
            this.textIdade.MaxLength = 3;
            this.textIdade.Name = "textIdade";
            this.textIdade.ReadOnly = true;
            this.textIdade.Size = new System.Drawing.Size(530, 21);
            this.textIdade.TabIndex = 7;
            // 
            // lblIdade
            // 
            this.lblIdade.BackColor = System.Drawing.Color.LightGray;
            this.lblIdade.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblIdade.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblIdade.Location = new System.Drawing.Point(10, 128);
            this.lblIdade.Name = "lblIdade";
            this.lblIdade.ReadOnly = true;
            this.lblIdade.Size = new System.Drawing.Size(200, 21);
            this.lblIdade.TabIndex = 95;
            this.lblIdade.TabStop = false;
            this.lblIdade.Text = "Idade";
            // 
            // textCtps
            // 
            this.textCtps.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textCtps.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textCtps.ForeColor = System.Drawing.Color.Black;
            this.textCtps.Location = new System.Drawing.Point(209, 268);
            this.textCtps.MaxLength = 15;
            this.textCtps.Name = "textCtps";
            this.textCtps.Size = new System.Drawing.Size(530, 21);
            this.textCtps.TabIndex = 14;
            // 
            // lblCtps
            // 
            this.lblCtps.BackColor = System.Drawing.Color.LightGray;
            this.lblCtps.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblCtps.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCtps.Location = new System.Drawing.Point(10, 268);
            this.lblCtps.Name = "lblCtps";
            this.lblCtps.ReadOnly = true;
            this.lblCtps.Size = new System.Drawing.Size(200, 21);
            this.lblCtps.TabIndex = 93;
            this.lblCtps.TabStop = false;
            this.lblCtps.Text = "CTPS";
            // 
            // textAltura
            // 
            this.textAltura.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textAltura.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.textAltura.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textAltura.Location = new System.Drawing.Point(209, 248);
            this.textAltura.MaxLength = 10;
            this.textAltura.Name = "textAltura";
            this.textAltura.Size = new System.Drawing.Size(530, 21);
            this.textAltura.TabIndex = 13;
            this.textAltura.Enter += new System.EventHandler(this.textAltura_Enter);
            this.textAltura.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.textAltura_KeyPress);
            this.textAltura.Leave += new System.EventHandler(this.textAltura_Leave);
            // 
            // lblAltura
            // 
            this.lblAltura.BackColor = System.Drawing.Color.LightGray;
            this.lblAltura.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblAltura.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblAltura.Location = new System.Drawing.Point(10, 248);
            this.lblAltura.Name = "lblAltura";
            this.lblAltura.ReadOnly = true;
            this.lblAltura.Size = new System.Drawing.Size(200, 21);
            this.lblAltura.TabIndex = 92;
            this.lblAltura.TabStop = false;
            this.lblAltura.Text = "Altura";
            // 
            // textPeso
            // 
            this.textPeso.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textPeso.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.textPeso.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textPeso.Location = new System.Drawing.Point(209, 228);
            this.textPeso.MaxLength = 100;
            this.textPeso.Name = "textPeso";
            this.textPeso.Size = new System.Drawing.Size(530, 21);
            this.textPeso.TabIndex = 12;
            this.textPeso.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.textPeso_KeyPress);
            // 
            // lblPeso
            // 
            this.lblPeso.BackColor = System.Drawing.Color.LightGray;
            this.lblPeso.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblPeso.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPeso.Location = new System.Drawing.Point(10, 228);
            this.lblPeso.Name = "lblPeso";
            this.lblPeso.ReadOnly = true;
            this.lblPeso.Size = new System.Drawing.Size(200, 21);
            this.lblPeso.TabIndex = 91;
            this.lblPeso.TabStop = false;
            this.lblPeso.Text = "Peso";
            // 
            // cbFatorRh
            // 
            this.cbFatorRh.BackColor = System.Drawing.Color.LightGray;
            this.cbFatorRh.BorderColor = System.Drawing.Color.DimGray;
            this.cbFatorRh.BorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid;
            this.cbFatorRh.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbFatorRh.FormattingEnabled = true;
            this.cbFatorRh.Location = new System.Drawing.Point(209, 208);
            this.cbFatorRh.Name = "cbFatorRh";
            this.cbFatorRh.Size = new System.Drawing.Size(530, 21);
            this.cbFatorRh.TabIndex = 11;
            // 
            // lblFatorRh
            // 
            this.lblFatorRh.BackColor = System.Drawing.Color.LightGray;
            this.lblFatorRh.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblFatorRh.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblFatorRh.Location = new System.Drawing.Point(10, 208);
            this.lblFatorRh.Name = "lblFatorRh";
            this.lblFatorRh.ReadOnly = true;
            this.lblFatorRh.Size = new System.Drawing.Size(200, 21);
            this.lblFatorRh.TabIndex = 90;
            this.lblFatorRh.TabStop = false;
            this.lblFatorRh.Text = "Fator RH";
            // 
            // cbTipoSanguineo
            // 
            this.cbTipoSanguineo.BackColor = System.Drawing.Color.LightGray;
            this.cbTipoSanguineo.BorderColor = System.Drawing.Color.DimGray;
            this.cbTipoSanguineo.BorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid;
            this.cbTipoSanguineo.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbTipoSanguineo.FormattingEnabled = true;
            this.cbTipoSanguineo.Location = new System.Drawing.Point(209, 188);
            this.cbTipoSanguineo.Name = "cbTipoSanguineo";
            this.cbTipoSanguineo.Size = new System.Drawing.Size(530, 21);
            this.cbTipoSanguineo.TabIndex = 10;
            // 
            // lblTipoSanguineo
            // 
            this.lblTipoSanguineo.BackColor = System.Drawing.Color.LightGray;
            this.lblTipoSanguineo.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblTipoSanguineo.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTipoSanguineo.Location = new System.Drawing.Point(10, 188);
            this.lblTipoSanguineo.Name = "lblTipoSanguineo";
            this.lblTipoSanguineo.ReadOnly = true;
            this.lblTipoSanguineo.Size = new System.Drawing.Size(200, 21);
            this.lblTipoSanguineo.TabIndex = 89;
            this.lblTipoSanguineo.TabStop = false;
            this.lblTipoSanguineo.Text = "Tipo sanguíneo";
            // 
            // textEmail
            // 
            this.textEmail.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textEmail.CharacterCasing = System.Windows.Forms.CharacterCasing.Lower;
            this.textEmail.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textEmail.Location = new System.Drawing.Point(209, 168);
            this.textEmail.MaxLength = 50;
            this.textEmail.Name = "textEmail";
            this.textEmail.Size = new System.Drawing.Size(530, 21);
            this.textEmail.TabIndex = 9;
            this.textEmail.Leave += new System.EventHandler(this.textEmail_Leave);
            // 
            // lblEmail
            // 
            this.lblEmail.BackColor = System.Drawing.Color.LightGray;
            this.lblEmail.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblEmail.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblEmail.Location = new System.Drawing.Point(10, 168);
            this.lblEmail.Name = "lblEmail";
            this.lblEmail.ReadOnly = true;
            this.lblEmail.Size = new System.Drawing.Size(200, 21);
            this.lblEmail.TabIndex = 88;
            this.lblEmail.TabStop = false;
            this.lblEmail.Text = "E-mail";
            // 
            // cbSexo
            // 
            this.cbSexo.BackColor = System.Drawing.Color.LightGray;
            this.cbSexo.BorderColor = System.Drawing.Color.DimGray;
            this.cbSexo.BorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid;
            this.cbSexo.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbSexo.FormattingEnabled = true;
            this.cbSexo.Location = new System.Drawing.Point(209, 148);
            this.cbSexo.Name = "cbSexo";
            this.cbSexo.Size = new System.Drawing.Size(530, 21);
            this.cbSexo.TabIndex = 8;
            // 
            // lblSexo
            // 
            this.lblSexo.BackColor = System.Drawing.Color.LightGray;
            this.lblSexo.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblSexo.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSexo.Location = new System.Drawing.Point(10, 148);
            this.lblSexo.Name = "lblSexo";
            this.lblSexo.ReadOnly = true;
            this.lblSexo.Size = new System.Drawing.Size(200, 21);
            this.lblSexo.TabIndex = 87;
            this.lblSexo.TabStop = false;
            this.lblSexo.Text = "Sexo";
            // 
            // lblDataNascimento
            // 
            this.lblDataNascimento.BackColor = System.Drawing.Color.LightGray;
            this.lblDataNascimento.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblDataNascimento.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDataNascimento.Location = new System.Drawing.Point(10, 108);
            this.lblDataNascimento.Name = "lblDataNascimento";
            this.lblDataNascimento.ReadOnly = true;
            this.lblDataNascimento.Size = new System.Drawing.Size(200, 21);
            this.lblDataNascimento.TabIndex = 86;
            this.lblDataNascimento.TabStop = false;
            this.lblDataNascimento.Text = "Data de nascimento";
            // 
            // textRg
            // 
            this.textRg.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textRg.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.textRg.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textRg.Location = new System.Drawing.Point(209, 48);
            this.textRg.MaxLength = 15;
            this.textRg.Name = "textRg";
            this.textRg.Size = new System.Drawing.Size(530, 21);
            this.textRg.TabIndex = 3;
            // 
            // dataNascimento
            // 
            this.dataNascimento.Checked = false;
            this.dataNascimento.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dataNascimento.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dataNascimento.Location = new System.Drawing.Point(209, 108);
            this.dataNascimento.Name = "dataNascimento";
            this.dataNascimento.ShowCheckBox = true;
            this.dataNascimento.Size = new System.Drawing.Size(530, 21);
            this.dataNascimento.TabIndex = 6;
            this.dataNascimento.ValueChanged += new System.EventHandler(this.dataNascimento_ValueChanged);
            // 
            // lblRg
            // 
            this.lblRg.BackColor = System.Drawing.Color.LightGray;
            this.lblRg.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblRg.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblRg.Location = new System.Drawing.Point(10, 48);
            this.lblRg.Name = "lblRg";
            this.lblRg.ReadOnly = true;
            this.lblRg.Size = new System.Drawing.Size(200, 21);
            this.lblRg.TabIndex = 85;
            this.lblRg.TabStop = false;
            this.lblRg.Text = "Documento de Identidade";
            // 
            // textCpf
            // 
            this.textCpf.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textCpf.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textCpf.ForeColor = System.Drawing.Color.MediumBlue;
            this.textCpf.Location = new System.Drawing.Point(209, 28);
            this.textCpf.Mask = "000,000,000-00";
            this.textCpf.Name = "textCpf";
            this.textCpf.PromptChar = ' ';
            this.textCpf.Size = new System.Drawing.Size(530, 21);
            this.textCpf.TabIndex = 2;
            this.textCpf.Leave += new System.EventHandler(this.textCpf_Leave);
            // 
            // lblCpf
            // 
            this.lblCpf.BackColor = System.Drawing.Color.LightGray;
            this.lblCpf.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblCpf.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCpf.Location = new System.Drawing.Point(10, 28);
            this.lblCpf.Name = "lblCpf";
            this.lblCpf.ReadOnly = true;
            this.lblCpf.Size = new System.Drawing.Size(200, 21);
            this.lblCpf.TabIndex = 84;
            this.lblCpf.TabStop = false;
            this.lblCpf.Text = "CPF";
            // 
            // textNome
            // 
            this.textNome.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textNome.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.textNome.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textNome.Location = new System.Drawing.Point(209, 8);
            this.textNome.MaxLength = 100;
            this.textNome.Name = "textNome";
            this.textNome.Size = new System.Drawing.Size(530, 21);
            this.textNome.TabIndex = 1;
            // 
            // lblNome
            // 
            this.lblNome.BackColor = System.Drawing.Color.LightGray;
            this.lblNome.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblNome.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNome.Location = new System.Drawing.Point(10, 8);
            this.lblNome.Name = "lblNome";
            this.lblNome.ReadOnly = true;
            this.lblNome.Size = new System.Drawing.Size(200, 21);
            this.lblNome.TabIndex = 83;
            this.lblNome.TabStop = false;
            this.lblNome.Text = "Nome";
            // 
            // textTelefone
            // 
            this.textTelefone.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textTelefone.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.textTelefone.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textTelefone.Location = new System.Drawing.Point(209, 508);
            this.textTelefone.MaxLength = 15;
            this.textTelefone.Name = "textTelefone";
            this.textTelefone.Size = new System.Drawing.Size(530, 21);
            this.textTelefone.TabIndex = 26;
            // 
            // btAlterarCliente
            // 
            this.btAlterarCliente.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btAlterarCliente.Image = global::SWS.Properties.Resources.Alterar;
            this.btAlterarCliente.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btAlterarCliente.Location = new System.Drawing.Point(84, 3);
            this.btAlterarCliente.Name = "btAlterarCliente";
            this.btAlterarCliente.Size = new System.Drawing.Size(75, 23);
            this.btAlterarCliente.TabIndex = 29;
            this.btAlterarCliente.Text = "&Alterar";
            this.btAlterarCliente.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btAlterarCliente.UseVisualStyleBackColor = true;
            this.btAlterarCliente.Click += new System.EventHandler(this.btAlterarCliente_Click);
            // 
            // btExcluirCliente
            // 
            this.btExcluirCliente.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btExcluirCliente.Image = global::SWS.Properties.Resources.lixeira;
            this.btExcluirCliente.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btExcluirCliente.Location = new System.Drawing.Point(165, 3);
            this.btExcluirCliente.Name = "btExcluirCliente";
            this.btExcluirCliente.Size = new System.Drawing.Size(75, 23);
            this.btExcluirCliente.TabIndex = 30;
            this.btExcluirCliente.Text = "&Excluir";
            this.btExcluirCliente.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btExcluirCliente.UseVisualStyleBackColor = true;
            this.btExcluirCliente.Click += new System.EventHandler(this.btExcluirCliente_Click);
            // 
            // btIncluirCliente
            // 
            this.btIncluirCliente.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btIncluirCliente.Image = ((System.Drawing.Image)(resources.GetObject("btIncluirCliente.Image")));
            this.btIncluirCliente.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btIncluirCliente.Location = new System.Drawing.Point(3, 3);
            this.btIncluirCliente.Name = "btIncluirCliente";
            this.btIncluirCliente.Size = new System.Drawing.Size(75, 23);
            this.btIncluirCliente.TabIndex = 28;
            this.btIncluirCliente.Text = "&Incluir";
            this.btIncluirCliente.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btIncluirCliente.UseVisualStyleBackColor = true;
            this.btIncluirCliente.Click += new System.EventHandler(this.btIncluirCliente_Click);
            // 
            // grbCliente
            // 
            this.grbCliente.Controls.Add(this.dgvCliente);
            this.grbCliente.Location = new System.Drawing.Point(10, 557);
            this.grbCliente.Name = "grbCliente";
            this.grbCliente.Size = new System.Drawing.Size(729, 155);
            this.grbCliente.TabIndex = 116;
            this.grbCliente.TabStop = false;
            this.grbCliente.Text = "Clientes";
            // 
            // dgvCliente
            // 
            this.dgvCliente.AllowUserToAddRows = false;
            this.dgvCliente.AllowUserToDeleteRows = false;
            this.dgvCliente.AllowUserToOrderColumns = true;
            this.dgvCliente.AllowUserToResizeRows = false;
            this.dgvCliente.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.DisplayedCells;
            this.dgvCliente.BackgroundColor = System.Drawing.Color.White;
            this.dgvCliente.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.Color.LightGray;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvCliente.DefaultCellStyle = dataGridViewCellStyle2;
            this.dgvCliente.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvCliente.Location = new System.Drawing.Point(3, 16);
            this.dgvCliente.MultiSelect = false;
            this.dgvCliente.Name = "dgvCliente";
            this.dgvCliente.ReadOnly = true;
            this.dgvCliente.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvCliente.Size = new System.Drawing.Size(723, 136);
            this.dgvCliente.TabIndex = 0;
            this.dgvCliente.TabStop = false;
            this.dgvCliente.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvCliente_CellClick);
            this.dgvCliente.CellDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvCliente_CellDoubleClick);
            this.dgvCliente.CellFormatting += new System.Windows.Forms.DataGridViewCellFormattingEventHandler(this.dgvCliente_CellFormatting);
            // 
            // grbFuncao
            // 
            this.grbFuncao.Controls.Add(this.dgvFuncao);
            this.grbFuncao.Location = new System.Drawing.Point(10, 756);
            this.grbFuncao.Name = "grbFuncao";
            this.grbFuncao.Size = new System.Drawing.Size(729, 229);
            this.grbFuncao.TabIndex = 120;
            this.grbFuncao.TabStop = false;
            this.grbFuncao.Text = "Funções / Cargos";
            // 
            // dgvFuncao
            // 
            this.dgvFuncao.AllowUserToAddRows = false;
            this.dgvFuncao.AllowUserToDeleteRows = false;
            this.dgvFuncao.AllowUserToOrderColumns = true;
            this.dgvFuncao.AllowUserToResizeRows = false;
            this.dgvFuncao.BackgroundColor = System.Drawing.Color.White;
            this.dgvFuncao.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.LightGray;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvFuncao.DefaultCellStyle = dataGridViewCellStyle1;
            this.dgvFuncao.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvFuncao.Location = new System.Drawing.Point(3, 16);
            this.dgvFuncao.MultiSelect = false;
            this.dgvFuncao.Name = "dgvFuncao";
            this.dgvFuncao.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvFuncao.Size = new System.Drawing.Size(723, 210);
            this.dgvFuncao.TabIndex = 0;
            this.dgvFuncao.TabStop = false;
            this.dgvFuncao.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvFuncao_CellContentClick);
            this.dgvFuncao.RowPrePaint += new System.Windows.Forms.DataGridViewRowPrePaintEventHandler(this.dgvFuncao_RowPrePaint);
            // 
            // epFuncionarioIncluir
            // 
            this.epFuncionarioIncluir.ContainerControl = this;
            // 
            // lblOrgaoEmissor
            // 
            this.lblOrgaoEmissor.BackColor = System.Drawing.Color.LightGray;
            this.lblOrgaoEmissor.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblOrgaoEmissor.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblOrgaoEmissor.Location = new System.Drawing.Point(10, 68);
            this.lblOrgaoEmissor.Name = "lblOrgaoEmissor";
            this.lblOrgaoEmissor.ReadOnly = true;
            this.lblOrgaoEmissor.Size = new System.Drawing.Size(200, 21);
            this.lblOrgaoEmissor.TabIndex = 122;
            this.lblOrgaoEmissor.TabStop = false;
            this.lblOrgaoEmissor.Text = "Orgão Emissor";
            // 
            // textOrgaoEmissor
            // 
            this.textOrgaoEmissor.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textOrgaoEmissor.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.textOrgaoEmissor.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textOrgaoEmissor.Location = new System.Drawing.Point(209, 68);
            this.textOrgaoEmissor.MaxLength = 15;
            this.textOrgaoEmissor.Name = "textOrgaoEmissor";
            this.textOrgaoEmissor.Size = new System.Drawing.Size(530, 21);
            this.textOrgaoEmissor.TabIndex = 4;
            // 
            // lblUfEmissor
            // 
            this.lblUfEmissor.BackColor = System.Drawing.Color.LightGray;
            this.lblUfEmissor.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblUfEmissor.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblUfEmissor.Location = new System.Drawing.Point(10, 88);
            this.lblUfEmissor.Name = "lblUfEmissor";
            this.lblUfEmissor.ReadOnly = true;
            this.lblUfEmissor.Size = new System.Drawing.Size(200, 21);
            this.lblUfEmissor.TabIndex = 124;
            this.lblUfEmissor.TabStop = false;
            this.lblUfEmissor.Text = "UF Emissor";
            // 
            // cbUfEmissor
            // 
            this.cbUfEmissor.BackColor = System.Drawing.Color.LightGray;
            this.cbUfEmissor.BorderColor = System.Drawing.Color.DimGray;
            this.cbUfEmissor.BorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid;
            this.cbUfEmissor.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbUfEmissor.FormattingEnabled = true;
            this.cbUfEmissor.Location = new System.Drawing.Point(209, 88);
            this.cbUfEmissor.Name = "cbUfEmissor";
            this.cbUfEmissor.Size = new System.Drawing.Size(530, 21);
            this.cbUfEmissor.TabIndex = 5;
            // 
            // flpAcaoCliente
            // 
            this.flpAcaoCliente.Controls.Add(this.btIncluirCliente);
            this.flpAcaoCliente.Controls.Add(this.btAlterarCliente);
            this.flpAcaoCliente.Controls.Add(this.btExcluirCliente);
            this.flpAcaoCliente.Location = new System.Drawing.Point(10, 717);
            this.flpAcaoCliente.Name = "flpAcaoCliente";
            this.flpAcaoCliente.Size = new System.Drawing.Size(726, 36);
            this.flpAcaoCliente.TabIndex = 125;
            // 
            // btCidadeComercial
            // 
            this.btCidadeComercial.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btCidadeComercial.Image = global::SWS.Properties.Resources.busca;
            this.btCidadeComercial.Location = new System.Drawing.Point(705, 488);
            this.btCidadeComercial.Name = "btCidadeComercial";
            this.btCidadeComercial.Size = new System.Drawing.Size(34, 21);
            this.btCidadeComercial.TabIndex = 25;
            this.btCidadeComercial.UseVisualStyleBackColor = true;
            this.btCidadeComercial.Click += new System.EventHandler(this.btCidadeComercial_Click);
            // 
            // lblPis
            // 
            this.lblPis.BackColor = System.Drawing.Color.LightGray;
            this.lblPis.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblPis.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPis.Location = new System.Drawing.Point(10, 328);
            this.lblPis.Name = "lblPis";
            this.lblPis.ReadOnly = true;
            this.lblPis.Size = new System.Drawing.Size(200, 21);
            this.lblPis.TabIndex = 94;
            this.lblPis.TabStop = false;
            this.lblPis.Text = "PIS / PASEP";
            // 
            // textPis
            // 
            this.textPis.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textPis.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textPis.ForeColor = System.Drawing.Color.MediumBlue;
            this.textPis.Location = new System.Drawing.Point(209, 328);
            this.textPis.Mask = "999,99999,99-9";
            this.textPis.Name = "textPis";
            this.textPis.PromptChar = ' ';
            this.textPis.Size = new System.Drawing.Size(530, 21);
            this.textPis.TabIndex = 17;
            // 
            // cbPcd
            // 
            this.cbPcd.BackColor = System.Drawing.Color.LightGray;
            this.cbPcd.BorderColor = System.Drawing.Color.DimGray;
            this.cbPcd.BorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid;
            this.cbPcd.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbPcd.FormattingEnabled = true;
            this.cbPcd.Location = new System.Drawing.Point(209, 348);
            this.cbPcd.Name = "cbPcd";
            this.cbPcd.Size = new System.Drawing.Size(530, 21);
            this.cbPcd.TabIndex = 18;
            // 
            // lblPcd
            // 
            this.lblPcd.BackColor = System.Drawing.Color.LightGray;
            this.lblPcd.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblPcd.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPcd.Location = new System.Drawing.Point(10, 348);
            this.lblPcd.Name = "lblPcd";
            this.lblPcd.ReadOnly = true;
            this.lblPcd.Size = new System.Drawing.Size(200, 21);
            this.lblPcd.TabIndex = 121;
            this.lblPcd.TabStop = false;
            this.lblPcd.Text = "Portador de Alguma deficiência?";
            // 
            // lblLogradouro
            // 
            this.lblLogradouro.BackColor = System.Drawing.Color.LightGray;
            this.lblLogradouro.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblLogradouro.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblLogradouro.Location = new System.Drawing.Point(10, 368);
            this.lblLogradouro.Name = "lblLogradouro";
            this.lblLogradouro.ReadOnly = true;
            this.lblLogradouro.Size = new System.Drawing.Size(200, 21);
            this.lblLogradouro.TabIndex = 105;
            this.lblLogradouro.TabStop = false;
            this.lblLogradouro.Text = "Logradouro";
            // 
            // textLogradouro
            // 
            this.textLogradouro.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textLogradouro.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.textLogradouro.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textLogradouro.Location = new System.Drawing.Point(209, 368);
            this.textLogradouro.MaxLength = 100;
            this.textLogradouro.Name = "textLogradouro";
            this.textLogradouro.Size = new System.Drawing.Size(530, 21);
            this.textLogradouro.TabIndex = 19;
            // 
            // textNumero
            // 
            this.textNumero.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textNumero.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.textNumero.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textNumero.Location = new System.Drawing.Point(209, 388);
            this.textNumero.MaxLength = 10;
            this.textNumero.Name = "textNumero";
            this.textNumero.Size = new System.Drawing.Size(530, 21);
            this.textNumero.TabIndex = 20;
            // 
            // lblNumero
            // 
            this.lblNumero.BackColor = System.Drawing.Color.LightGray;
            this.lblNumero.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblNumero.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNumero.Location = new System.Drawing.Point(10, 388);
            this.lblNumero.Name = "lblNumero";
            this.lblNumero.ReadOnly = true;
            this.lblNumero.Size = new System.Drawing.Size(200, 21);
            this.lblNumero.TabIndex = 106;
            this.lblNumero.TabStop = false;
            this.lblNumero.Text = "Número";
            // 
            // lblComplemento
            // 
            this.lblComplemento.BackColor = System.Drawing.Color.LightGray;
            this.lblComplemento.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblComplemento.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblComplemento.Location = new System.Drawing.Point(10, 408);
            this.lblComplemento.Name = "lblComplemento";
            this.lblComplemento.ReadOnly = true;
            this.lblComplemento.Size = new System.Drawing.Size(200, 21);
            this.lblComplemento.TabIndex = 107;
            this.lblComplemento.TabStop = false;
            this.lblComplemento.Text = "Complemento";
            // 
            // textComplemento
            // 
            this.textComplemento.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textComplemento.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.textComplemento.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textComplemento.Location = new System.Drawing.Point(209, 408);
            this.textComplemento.MaxLength = 100;
            this.textComplemento.Name = "textComplemento";
            this.textComplemento.Size = new System.Drawing.Size(530, 21);
            this.textComplemento.TabIndex = 21;
            // 
            // textBairro
            // 
            this.textBairro.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBairro.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.textBairro.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBairro.Location = new System.Drawing.Point(209, 428);
            this.textBairro.MaxLength = 50;
            this.textBairro.Name = "textBairro";
            this.textBairro.Size = new System.Drawing.Size(530, 21);
            this.textBairro.TabIndex = 22;
            // 
            // lblBairro
            // 
            this.lblBairro.BackColor = System.Drawing.Color.LightGray;
            this.lblBairro.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblBairro.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblBairro.Location = new System.Drawing.Point(10, 428);
            this.lblBairro.Name = "lblBairro";
            this.lblBairro.ReadOnly = true;
            this.lblBairro.Size = new System.Drawing.Size(200, 21);
            this.lblBairro.TabIndex = 108;
            this.lblBairro.TabStop = false;
            this.lblBairro.Text = "Bairro";
            // 
            // lblCEP
            // 
            this.lblCEP.BackColor = System.Drawing.Color.LightGray;
            this.lblCEP.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblCEP.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCEP.Location = new System.Drawing.Point(10, 448);
            this.lblCEP.Name = "lblCEP";
            this.lblCEP.ReadOnly = true;
            this.lblCEP.Size = new System.Drawing.Size(200, 21);
            this.lblCEP.TabIndex = 109;
            this.lblCEP.TabStop = false;
            this.lblCEP.Text = "CEP";
            // 
            // textCEP
            // 
            this.textCEP.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textCEP.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textCEP.Location = new System.Drawing.Point(209, 448);
            this.textCEP.Mask = "99,999-999";
            this.textCEP.Name = "textCEP";
            this.textCEP.PromptChar = ' ';
            this.textCEP.Size = new System.Drawing.Size(530, 21);
            this.textCEP.TabIndex = 23;
            // 
            // cbUF
            // 
            this.cbUF.BackColor = System.Drawing.Color.LightGray;
            this.cbUF.BorderColor = System.Drawing.Color.DimGray;
            this.cbUF.BorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid;
            this.cbUF.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbUF.FormattingEnabled = true;
            this.cbUF.Location = new System.Drawing.Point(209, 468);
            this.cbUF.Name = "cbUF";
            this.cbUF.Size = new System.Drawing.Size(530, 21);
            this.cbUF.TabIndex = 24;
            // 
            // lblUF
            // 
            this.lblUF.BackColor = System.Drawing.Color.LightGray;
            this.lblUF.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblUF.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblUF.Location = new System.Drawing.Point(10, 468);
            this.lblUF.Name = "lblUF";
            this.lblUF.ReadOnly = true;
            this.lblUF.Size = new System.Drawing.Size(200, 21);
            this.lblUF.TabIndex = 110;
            this.lblUF.TabStop = false;
            this.lblUF.Text = "Unidade Federativa";
            // 
            // lblCidade
            // 
            this.lblCidade.BackColor = System.Drawing.Color.LightGray;
            this.lblCidade.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblCidade.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCidade.Location = new System.Drawing.Point(10, 488);
            this.lblCidade.Name = "lblCidade";
            this.lblCidade.ReadOnly = true;
            this.lblCidade.Size = new System.Drawing.Size(200, 21);
            this.lblCidade.TabIndex = 112;
            this.lblCidade.TabStop = false;
            this.lblCidade.Text = "Cidade";
            // 
            // textCidade
            // 
            this.textCidade.BackColor = System.Drawing.Color.LightGray;
            this.textCidade.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textCidade.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textCidade.Location = new System.Drawing.Point(209, 488);
            this.textCidade.Name = "textCidade";
            this.textCidade.ReadOnly = true;
            this.textCidade.Size = new System.Drawing.Size(497, 21);
            this.textCidade.TabIndex = 111;
            this.textCidade.TabStop = false;
            // 
            // lblTelefoneComercial
            // 
            this.lblTelefoneComercial.BackColor = System.Drawing.Color.LightGray;
            this.lblTelefoneComercial.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblTelefoneComercial.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTelefoneComercial.Location = new System.Drawing.Point(10, 508);
            this.lblTelefoneComercial.Name = "lblTelefoneComercial";
            this.lblTelefoneComercial.ReadOnly = true;
            this.lblTelefoneComercial.Size = new System.Drawing.Size(200, 21);
            this.lblTelefoneComercial.TabIndex = 113;
            this.lblTelefoneComercial.TabStop = false;
            this.lblTelefoneComercial.Text = "Telefone";
            // 
            // textCelular
            // 
            this.textCelular.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textCelular.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.textCelular.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textCelular.Location = new System.Drawing.Point(209, 528);
            this.textCelular.MaxLength = 15;
            this.textCelular.Name = "textCelular";
            this.textCelular.Size = new System.Drawing.Size(530, 21);
            this.textCelular.TabIndex = 27;
            // 
            // lblCelular
            // 
            this.lblCelular.BackColor = System.Drawing.Color.LightGray;
            this.lblCelular.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblCelular.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCelular.Location = new System.Drawing.Point(10, 528);
            this.lblCelular.Name = "lblCelular";
            this.lblCelular.ReadOnly = true;
            this.lblCelular.Size = new System.Drawing.Size(200, 21);
            this.lblCelular.TabIndex = 114;
            this.lblCelular.TabStop = false;
            this.lblCelular.Text = "Celular";
            // 
            // lblSerieCtps
            // 
            this.lblSerieCtps.BackColor = System.Drawing.Color.LightGray;
            this.lblSerieCtps.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblSerieCtps.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSerieCtps.Location = new System.Drawing.Point(10, 288);
            this.lblSerieCtps.Name = "lblSerieCtps";
            this.lblSerieCtps.ReadOnly = true;
            this.lblSerieCtps.Size = new System.Drawing.Size(200, 21);
            this.lblSerieCtps.TabIndex = 126;
            this.lblSerieCtps.TabStop = false;
            this.lblSerieCtps.Text = "Serie";
            // 
            // lblUfEmissorCtps
            // 
            this.lblUfEmissorCtps.BackColor = System.Drawing.Color.LightGray;
            this.lblUfEmissorCtps.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblUfEmissorCtps.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblUfEmissorCtps.Location = new System.Drawing.Point(10, 308);
            this.lblUfEmissorCtps.Name = "lblUfEmissorCtps";
            this.lblUfEmissorCtps.ReadOnly = true;
            this.lblUfEmissorCtps.Size = new System.Drawing.Size(200, 21);
            this.lblUfEmissorCtps.TabIndex = 127;
            this.lblUfEmissorCtps.TabStop = false;
            this.lblUfEmissorCtps.Text = "UF Emissor";
            // 
            // textSerieCtps
            // 
            this.textSerieCtps.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textSerieCtps.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.textSerieCtps.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textSerieCtps.ForeColor = System.Drawing.Color.Black;
            this.textSerieCtps.Location = new System.Drawing.Point(209, 288);
            this.textSerieCtps.MaxLength = 15;
            this.textSerieCtps.Name = "textSerieCtps";
            this.textSerieCtps.Size = new System.Drawing.Size(530, 21);
            this.textSerieCtps.TabIndex = 15;
            // 
            // cbUfEmissorCtps
            // 
            this.cbUfEmissorCtps.BackColor = System.Drawing.Color.LightGray;
            this.cbUfEmissorCtps.BorderColor = System.Drawing.Color.DimGray;
            this.cbUfEmissorCtps.BorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid;
            this.cbUfEmissorCtps.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbUfEmissorCtps.FormattingEnabled = true;
            this.cbUfEmissorCtps.Location = new System.Drawing.Point(209, 308);
            this.cbUfEmissorCtps.Name = "cbUfEmissorCtps";
            this.cbUfEmissorCtps.Size = new System.Drawing.Size(530, 21);
            this.cbUfEmissorCtps.TabIndex = 16;
            // 
            // frmFuncionarioIncluir
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(784, 561);
            this.Name = "frmFuncionarioIncluir";
            this.Text = "INCLUIR FUNCIONÁRIO";
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.FuncionarioIncluir_KeyDown);
            this.spForm.Panel1.ResumeLayout(false);
            this.spForm.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.spForm)).EndInit();
            this.spForm.ResumeLayout(false);
            this.pnlForm.ResumeLayout(false);
            this.pnlForm.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.banner)).EndInit();
            this.flpAcao.ResumeLayout(false);
            this.grbCliente.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvCliente)).EndInit();
            this.grbFuncao.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvFuncao)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.epFuncionarioIncluir)).EndInit();
            this.flpAcaoCliente.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.FlowLayoutPanel flpAcao;
        protected System.Windows.Forms.Button btGravar;
        protected System.Windows.Forms.Button btImprimir;
        protected System.Windows.Forms.Button btLimpar;
        protected System.Windows.Forms.Button btFechar;
        protected System.Windows.Forms.TextBox textIdade;
        protected System.Windows.Forms.TextBox lblIdade;
        protected System.Windows.Forms.TextBox textCtps;
        protected System.Windows.Forms.TextBox lblCtps;
        protected System.Windows.Forms.TextBox textAltura;
        protected System.Windows.Forms.TextBox lblAltura;
        protected System.Windows.Forms.TextBox textPeso;
        protected System.Windows.Forms.TextBox lblPeso;
        protected ComboBoxWithBorder cbFatorRh;
        protected System.Windows.Forms.TextBox lblFatorRh;
        protected ComboBoxWithBorder cbTipoSanguineo;
        protected System.Windows.Forms.TextBox lblTipoSanguineo;
        protected System.Windows.Forms.TextBox textEmail;
        protected System.Windows.Forms.TextBox lblEmail;
        protected ComboBoxWithBorder cbSexo;
        protected System.Windows.Forms.TextBox lblSexo;
        protected System.Windows.Forms.TextBox lblDataNascimento;
        protected System.Windows.Forms.TextBox textRg;
        protected System.Windows.Forms.DateTimePicker dataNascimento;
        protected System.Windows.Forms.TextBox lblRg;
        protected System.Windows.Forms.MaskedTextBox textCpf;
        protected System.Windows.Forms.TextBox lblCpf;
        protected System.Windows.Forms.TextBox textNome;
        protected System.Windows.Forms.TextBox lblNome;
        protected System.Windows.Forms.TextBox textTelefone;
        protected System.Windows.Forms.Button btAlterarCliente;
        protected System.Windows.Forms.Button btExcluirCliente;
        protected System.Windows.Forms.Button btIncluirCliente;
        protected System.Windows.Forms.GroupBox grbCliente;
        protected System.Windows.Forms.DataGridView dgvCliente;
        protected System.Windows.Forms.GroupBox grbFuncao;
        protected System.Windows.Forms.DataGridView dgvFuncao;
        protected System.Windows.Forms.ErrorProvider epFuncionarioIncluir;
        protected System.Windows.Forms.ToolTip tPFuncionarioInclusao;
        protected ComboBoxWithBorder cbUfEmissor;
        protected System.Windows.Forms.TextBox lblUfEmissor;
        protected System.Windows.Forms.TextBox textOrgaoEmissor;
        protected System.Windows.Forms.TextBox lblOrgaoEmissor;
        protected ComboBoxWithBorder cbUfEmissorCtps;
        protected System.Windows.Forms.TextBox textSerieCtps;
        protected System.Windows.Forms.TextBox lblUfEmissorCtps;
        protected System.Windows.Forms.TextBox lblSerieCtps;
        protected System.Windows.Forms.FlowLayoutPanel flpAcaoCliente;
        protected ComboBoxWithBorder cbPcd;
        protected System.Windows.Forms.TextBox lblPcd;
        protected System.Windows.Forms.Button btCidadeComercial;
        protected System.Windows.Forms.TextBox textCelular;
        protected System.Windows.Forms.TextBox lblCelular;
        protected System.Windows.Forms.TextBox lblTelefoneComercial;
        protected System.Windows.Forms.TextBox lblCidade;
        protected System.Windows.Forms.TextBox textCidade;
        protected System.Windows.Forms.MaskedTextBox textCEP;
        protected System.Windows.Forms.TextBox lblUF;
        protected System.Windows.Forms.TextBox lblCEP;
        protected System.Windows.Forms.TextBox lblBairro;
        protected System.Windows.Forms.TextBox lblComplemento;
        protected System.Windows.Forms.TextBox lblNumero;
        protected System.Windows.Forms.TextBox lblLogradouro;
        protected System.Windows.Forms.TextBox textLogradouro;
        protected System.Windows.Forms.TextBox textNumero;
        protected System.Windows.Forms.TextBox textComplemento;
        protected System.Windows.Forms.TextBox textBairro;
        protected ComboBoxWithBorder cbUF;
        protected System.Windows.Forms.MaskedTextBox textPis;
        protected System.Windows.Forms.TextBox lblPis;
    }
}