﻿using SWS.Entidade;
using SWS.Facade;
using SWS.View.Resources;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace SWS.View
{
    public partial class frmProtocoloIncluir : frmTemplate
    {
        protected Protocolo protocolo;
        protected Cliente cliente;

        public Protocolo Protocolo
        {
            get { return protocolo; }
            set { protocolo = value; }
        }


        public frmProtocoloIncluir()
        {
            InitializeComponent();

        }

        protected void btnProtocolo_Click(object sender, EventArgs e)
        {
            try
            {
                if (protocolo == null)
                    throw new Exception("Você deve criar primeiro o protocolo.");

                frmProtocoloIncluirItens incluirItens = new frmProtocoloIncluirItens(protocolo, cliente);
                incluirItens.ShowDialog();

                if (incluirItens.Protocolo.Atendimento != null || incluirItens.Protocolo.GheFonteAgenteExameAsos.Count > 0 || incluirItens.Protocolo.ClienteFuncaoExameAsos.Count > 0 || incluirItens.Protocolo.Documentos.Count > 0)
                    montaDataGrid();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            
        }

        protected void btnFechar_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        protected void btCliente_Click(object sender, EventArgs e)
        {
            frmClienteSelecionar formCliente = new frmClienteSelecionar(null, null, null, null, null, null, null, null, null);
            formCliente.ShowDialog();

            if (formCliente.Cliente != null)
            {
                cliente = formCliente.Cliente;
                textCliente.Text = cliente.RazaoSocial;
            }

        }

        protected void btnExcluirCliente_Click(object sender, EventArgs e)
        {
            try
            {
                if (cliente == null)
                    throw new Exception("Você deve primeiro selecionar o cliente");

                cliente = null;
                textCliente.Text = string.Empty;

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        protected void btnExcluir_Click(object sender, EventArgs e)
        {
            try
            {
                if (dgItens.Rows.Count == 0)
                    throw new Exception("Protocolo não possui item(ns) para excluir.");
                
                ProtocoloFacade protocoloFacade = ProtocoloFacade.getInstance();
                bool exclusao = false;
                foreach (DataGridViewRow dvRow in dgItens.Rows)
                {
                    if ((bool)dvRow.Cells[11].Value == true)
                    {
                        /* verificando que item deve ser excluído. A informação encontra-se no campo [Tipo] */
                        exclusao = true;
                        string tipo = dvRow.Cells[10].Value.ToString();
                        switch (tipo)
                        {
                            case "EP":
                                protocoloFacade.deleteItemProtocolo((Int64)dvRow.Cells[0].Value, ApplicationConstants.EXAME_PCMSO);
                                break;

                            case "EE":
                                protocoloFacade.deleteItemProtocolo((Int64)dvRow.Cells[0].Value, ApplicationConstants.EXAME_EXTRA);
                                break;

                            case "AS":
                                protocoloFacade.deleteItemProtocolo((Int64)dvRow.Cells[0].Value, ApplicationConstants.ASO);
                                break;

                            case "P":
                                protocoloFacade.deleteItemProtocolo((Int64)dvRow.Cells[0].Value, ApplicationConstants.PRODUTO);
                                break;

                            default:
                                protocoloFacade.deleteDocumentoProtocolo(new DocumentoProtocolo((Int64)dvRow.Cells[0].Value, null, null, null, DateTime.Now));
                                break;
                        }
                    }
                }
                if (!exclusao)
                    throw new Exception("Você deve marcar pelo menos um item para excluir.");
                
                montaDataGrid();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        public void montaDataGrid()
        {
            try
            {
                ProtocoloFacade protocoloFacade = ProtocoloFacade.getInstance();

                this.dgItens.Columns.Clear();

                DataSet ds = protocoloFacade.findAllItensByProtocolo(protocolo);
                dgItens.DataSource = ds.Tables["ExamesProtocolo"].DefaultView;

                dgItens.Columns[0].HeaderText = "id";
                dgItens.Columns[0].Visible = false;

                dgItens.Columns[1].HeaderText = "Código do Item";
                dgItens.Columns[1].ReadOnly = true;

                dgItens.Columns[2].HeaderText = "Cliente";
                dgItens.Columns[2].Visible = false;

                dgItens.Columns[3].HeaderText = "Colaborador";
                dgItens.Columns[3].ReadOnly = true;

                dgItens.Columns[4].HeaderText = "RG";
                dgItens.Columns[4].ReadOnly = true;

                dgItens.Columns[5].HeaderText = "CPF";
                dgItens.Columns[5].ReadOnly = true;

                dgItens.Columns[6].HeaderText = "Periodicidade";
                dgItens.Columns[6].ReadOnly = true;

                dgItens.Columns[7].HeaderText = "Descrição";
                dgItens.Columns[7].ReadOnly = true;

                dgItens.Columns[8].HeaderText = "Data ";
                dgItens.Columns[8].ReadOnly = true;

                dgItens.Columns[9].HeaderText = "Laudo";
                dgItens.Columns[9].ReadOnly = true;

                dgItens.Columns[10].HeaderText = "Tipo";
                dgItens.Columns[10].Visible = false;

                dgItens.Columns[11].HeaderText = "Selec";
                dgItens.Columns[11].DisplayIndex = 0;

                montaTotal(ds);

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        public void montaTotal(DataSet ds)
        {
            try
            {
                /* preenchendo totais em tela. */
                textTotalExame.Text = Convert.ToString(Convert.ToInt32(ds.Tables["Total"].Rows[0]["TotalExamePcmso"]) + Convert.ToInt32(ds.Tables["Total"].Rows[0]["TotalExameExtra"]));
                textTotalPcmso.Text = ds.Tables["Total"].Rows[0]["TotalExamePcmso"].ToString();
                textTotalExtra.Text = ds.Tables["Total"].Rows[0]["TotalExameExtra"].ToString();
                textExameLaudado.Text = ds.Tables["Total"].Rows[0]["ExameLaudado"].ToString();
                textQuantidadeAso.Text = ds.Tables["Total"].Rows[0]["QuantidadeAso"].ToString();
                textEstudo.Text = ds.Tables["Total"].Rows[0]["QuantidadeEstudo"].ToString();
                textQuantidadeProduto.Text = ds.Tables["Total"].Rows[0]["QuantidadeProduto"].ToString();
                textTotalDocumento.Text = ds.Tables["Total"].Rows[0]["QuantidadeDocumentoProtocolo"].ToString();

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        protected void btnFinalizar_Click(object sender, EventArgs e)
        {
            try
            {
                if (protocolo == null)
                    throw new Exception("Você deve gerar primeiro um protocolo para depois finalizá-lo");

                if (MessageBox.Show("Deseja validar o protocolo. Ele não poderá ser mais alterado", "Confirmação", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                {
                    if (dgItens.Rows.Count == 0)
                        throw new Exception("O protocolo deve possuir pelo menos 1 item.");

                    this.Cursor = Cursors.WaitCursor;

                    ProtocoloFacade protocoloFacade = ProtocoloFacade.getInstance();
                                        
                    frmProtocoloFinalizar finalizarProtocolo = new frmProtocoloFinalizar(protocolo);
                    finalizarProtocolo.ShowDialog();

                    if (finalizarProtocolo.Finalizado){
                        protocolo = protocoloFacade.findById((Int64)protocolo.Id);
                        this.Close();
                    }

                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }

        protected void btn_novo_Click(object sender, EventArgs e)
        {
            try
            {
                if (cliente == null)
                    throw new Exception("Você deve selecionar primeiro o cliente.");

                ProtocoloFacade protocoloFacade = ProtocoloFacade.getInstance();
                protocolo = protocoloFacade.insertProtocolo(new Protocolo(null, string.Empty, string.Empty, ApplicationConstants.CONSTRUCAO, DateTime.Now, PermissionamentoFacade.usuarioAutenticado, null, null, string.Empty, null, null, string.Empty, string.Empty, cliente));
                textCodigo.Text = protocolo.Numero;
                textDataGravacao.Text = Convert.ToDateTime(protocolo.DataGravacao).ToShortDateString();
                btnCliente.Enabled = false;
                btnExcluirCliente.Enabled = false;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
    }
}
