﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using SWS.Entidade;
using SWS.View.ViewHelper;
using SWS.Facade;
using SWS.View.Resources;
using SWS.Excecao;
using SWS.ViewHelper;



namespace SWS.View
{
    public partial class frm_PpraEpiIncluir : BaseFormConsulta
    {
        private Epi epi = null;

        public static String Msg01 = " Atenção ";
        public static String Msg02 = " Selecione o EPI";

        private GheFonteAgente gheFonteAgente;

        public frm_PpraEpiIncluir(GheFonteAgente gheFonteAgente)
        {
            InitializeComponent();
            this.gheFonteAgente = gheFonteAgente;
        }

        private void btn_fechar_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void bt_buscar_Click(object sender, EventArgs e)
        {
            frm_PpraEpiBusca formPpraEpiBusca = new frm_PpraEpiBusca(gheFonteAgente);
            formPpraEpiBusca.ShowDialog();

            if (formPpraEpiBusca.getEpi() != null)
            {
                epi = formPpraEpiBusca.getEpi();
                text_descricao.Text = epi.Descricao;
                text_finalidade.Text = epi.Finalidade;
                iniciaComboTipoEpi();
            }

        }

        public void iniciaComboTipoEpi()
        {
            ComboHelper.CarregaEPi(cb_tipoEpi);
        }

        private void btn_ok_Click(object sender, EventArgs e)
        {

            try
            {
                PpraFacade ppraFacade = PpraFacade.getInstance();

                if (epi == null)
                {
                    throw new Exception(Msg02);
                }

                /* método que ira buscar se existe um objeto com as seguintes caracteristicas:
                *  mesmo, epi, mesmo ghe_fonte_agente e mesma classificação. */

                if (ppraFacade.findGheFonteAgenteEpi(new GheFonteAgenteEpi(null, gheFonteAgente, epi, rb_medidaControle.Checked? String.Empty : ((SelectItem)cb_tipoEpi.SelectedItem).Valor, rb_medidaControle.Checked ? ApplicationConstants.MEDIDA_CONTROLE : rb_epc.Checked ? ApplicationConstants.EPC : ApplicationConstants.EPI)))
                {
                    throw new GheFonteAgenteEpiException(GheFonteAgenteEpiException.msg);
                }

                ppraFacade.incluirGheFonteAgenteEpi(new GheFonteAgenteEpi(null, gheFonteAgente, epi, rb_medidaControle.Checked ? String.Empty : ((SelectItem)cb_tipoEpi.SelectedItem).Valor, rb_medidaControle.Checked ? ApplicationConstants.MEDIDA_CONTROLE : rb_epc.Checked ? ApplicationConstants.EPC : ApplicationConstants.EPI));
                this.Close();
                
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }

        }

        private void rb_medidaControle_CheckedChanged(object sender, EventArgs e)
        {
            grp_tipoEpi.Enabled = rb_medidaControle.Checked ? false : true;
            
        }

    }
}
