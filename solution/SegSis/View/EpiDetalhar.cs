﻿using SWS.Entidade;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace SWS.View
{
    public partial class frmEpiDetalhar : frmEpiIncluir
    {
        public frmEpiDetalhar(Epi epi)
        {
            InitializeComponent();
            text_descricao.Text = epi.Descricao;
            text_ca.Text = epi.Ca;
            text_finalidade.Text = epi.Finalidade;
            btn_gravar.Enabled = false;
            lbl_limpar.Enabled = false;
        }
    }
}
