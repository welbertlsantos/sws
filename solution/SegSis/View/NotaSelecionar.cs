﻿using SWS.Entidade;
using SWS.Facade;
using SWS.View.Resources;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SWS.View
{
    public partial class frmNotaSelecionar : frmTemplateConsulta
    {

        private Ghe ghe;
        private List<GheNota> notasGhe;

        public List<GheNota> NotaGhe
        {
            get { return notasGhe; }
            set { notasGhe = value; }
        }

        public frmNotaSelecionar(Ghe ghe)
        {
            InitializeComponent();
            this.ghe = ghe;
            montaGrid();
        }

        private void btnGravarNota_Click(object sender, EventArgs e)
        {
            bool flagSelecao = false;
            List<GheNota> notasErroGhe = new List<GheNota>();
            try
            {
                EstudoFacade pcmsoFacade = EstudoFacade.getInstance();
                List<Nota> notasInGhe = pcmsoFacade.findAllNotaByGhe(ghe).ToList();

                foreach (DataGridViewRow row in dgvNota.Rows)
                {
                    if ((bool)row.Cells[3].Value)
                    {
                        flagSelecao = true;
                     
                        Nota nota = new Nota();
                        nota.Id = (long)row.Cells[0].Value;
                        nota.Descricao = row.Cells[1].Value.ToString();
                        nota.Conteudo = row.Cells[2].Value.ToString();
                        nota.Situacao = ApplicationConstants.ATIVO;

                        /* verificando se a nota já foi inserida no Ghe */
                        GheNota gheNota = new GheNota();
                        gheNota.Nota = nota;
                        gheNota.Ghe = ghe;

                        if (!notasInGhe.Contains(nota))
                            pcmsoFacade.insertGheNota(gheNota);
                        else
                            notasErroGhe.Add(gheNota);
                    }
                }

                if (!flagSelecao)
                    throw new Exception("Você deve selecionar uma nota para inclusão.");

                if (notasErroGhe.Count > 0)
                {
                    StringBuilder notas = new StringBuilder();
                    notas.Append("A(s) nota(s) abaixo já está(ão) presente(s) no GHE:" + System.Environment.NewLine);
                    int i = 1;
                    foreach (GheNota gheNota in notasErroGhe)
                    {
                        notas.Append(gheNota.Nota.Descricao);
                        if (i < notasErroGhe.Count)
                            notas.Append(", ");
                        else
                            notas.Append(".");
                        i++;
                    }

                    MessageBox.Show(notas.ToString(), "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
                this.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }

        private void btnNotaIncluir_Click(object sender, EventArgs e)
        {
            frmNotaIncluir notaIncluir = new frmNotaIncluir();
            notaIncluir.ShowDialog();

            if (notaIncluir.Nota != null)
                montaGrid();
            
        }

        private void montaGrid()
        {
            try
            {
                dgvNota.Columns.Clear();

                EstudoFacade pcmsoFacade = EstudoFacade.getInstance();
                Nota nota = new Nota();
                nota.Situacao = ApplicationConstants.ATIVO;
                DataSet ds = pcmsoFacade.findNotaByFilter(nota);

                dgvNota.ColumnCount = 3;

                dgvNota.Columns[0].HeaderText = "id";
                dgvNota.Columns[0].Visible = false;
                dgvNota.Columns[1].HeaderText = "Titulo";
                dgvNota.Columns[2].HeaderText = "Conteúdo";

                DataGridViewCheckBoxColumn check = new DataGridViewCheckBoxColumn();
                check.Name = "Selecionar";
                dgvNota.Columns.Add(check);
                dgvNota.Columns[3].DisplayIndex = 0;
                dgvNota.Columns[3].Width = 30;



                foreach (DataRow dvRow in ds.Tables[0].Rows)
                    dgvNota.Rows.Add(
                        dvRow["ID_NORMA"],
                        dvRow["DESCRICAO"].ToString(),
                        dvRow["CONTEUDO"].ToString(),
                        false
                        );

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }

        private void btnFechar_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
