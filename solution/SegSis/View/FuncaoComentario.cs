﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using SWS.Entidade;
using SWS.Facade;
using SWS.View.Resources;

namespace SWS.View
{
    public partial class frmFuncaoComentario : SWS.View.frmTemplateConsulta
    {
        private static String msg1 = "Selecione uma linha.";

        private Funcao funcao;

        private ComentarioFuncao comentarioFuncao;

        public ComentarioFuncao ComentarioFuncao
        {
            get { return comentarioFuncao; }
            set { comentarioFuncao = value; }
        }

        public frmFuncaoComentario(Funcao funcao)
        {
            InitializeComponent();
            this.funcao = funcao;
            montaDataGrid();
        }

        private void btnGravar_Click(object sender, EventArgs e)
        {
            try
            {
                if (dgAtividade.CurrentRow == null)
                    throw new Exception(msg1);

                comentarioFuncao = new ComentarioFuncao((Int64)dgAtividade.CurrentRow.Cells[0].Value, funcao, (String)dgAtividade.CurrentRow.Cells[2].Value, (string)dgAtividade.CurrentRow.Cells[3].Value);

                this.Close();

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
            
        }

        private void btnNovo_Click(object sender, EventArgs e)
        {
            try
            {
                EstudoFacade pcmsoFacade = EstudoFacade.getInstance();

                frmFuncaoComentarioIncluir incluirComentario = new frmFuncaoComentarioIncluir();
                incluirComentario.ShowDialog();

                if (incluirComentario.ComentarioFuncao != null)
                {
                    comentarioFuncao = pcmsoFacade.insertComentarioFuncao(new ComentarioFuncao(null, funcao, incluirComentario.ComentarioFuncao.Atividade, ApplicationConstants.ATIVO));
                    montaDataGrid();
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
        }

        private void btnFechar_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        public void montaDataGrid()
        {
            try
            {
                this.dgAtividade.Columns.Clear();

                this.dgAtividade.ColumnCount = 4;

                EstudoFacade pcmsoFacade = EstudoFacade.getInstance();

                dgAtividade.Columns[0].HeaderText = "Id";
                dgAtividade.Columns[0].Visible = false;

                dgAtividade.Columns[1].HeaderText = "IdFuncao";
                dgAtividade.Columns[1].Visible = false;

                dgAtividade.Columns[2].HeaderText = "Atividade";

                dgAtividade.Columns[3].HeaderText = "Situação";
                dgAtividade.Columns[3].Visible = false;

                pcmsoFacade.findAllComentarioFuncaoAtivoByFuncao(funcao).ForEach(delegate(ComentarioFuncao comentarioFuncao)
                {
                    dgAtividade.Rows.Add(comentarioFuncao.Id, comentarioFuncao.Funcao.Id, comentarioFuncao.Atividade, comentarioFuncao.Situacao);
                });

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void dgAtividade_DataBindingComplete(object sender, DataGridViewBindingCompleteEventArgs e)
        {
            DataGridView dgv = sender as DataGridView;
            dgv.ClearSelection();
        }
    }
}
