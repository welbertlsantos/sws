﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using SegSis.Facade;
using SegSis.Entidade;
using SegSis.View.Resources;

namespace SegSis.View
{
    public partial class frm_PcmsoPeriodicidadeSelecionar : BaseFormConsulta
    {
        frm_PcmsoAvulso formPcmsoAvulso = null;

        public frm_PcmsoPeriodicidadeSelecionar(frm_PcmsoAvulso formPcmsoAvulso)
        {
            InitializeComponent();
            this.formPcmsoAvulso = formPcmsoAvulso;
        }

        

        public void montaDataGrid()
        {
            PcmsoFacade pcmsoFacade = PcmsoFacade.getInstance();

            Periodicidade periodicidade = new Periodicidade();
            periodicidade.setDescricao(text_descricao.Text);
            
            DataSet ds = pcmsoFacade.findAllPeriodicidadesNotInGheFonteAgenteExame(periodicidade, formPcmsoAvulso.gheFonteAgenteExame);

            grd_periodicidade.DataSource = ds.Tables["Periodicidades"].DefaultView;
            grd_periodicidade.AutoResizeColumns();
            
            grd_periodicidade.Columns[0].HeaderText = "id_periodicidade";
            grd_periodicidade.Columns[1].HeaderText = "Descrição";

            grd_periodicidade.Columns[0].Visible = false;
        }

        private void btn_buscar_Click(object sender, EventArgs e)
        {
            try
            {
                this.grd_periodicidade.Columns.Clear();
                montaDataGrid();
                text_descricao.Focus();
            }
            catch (Exception Ex)
            {
                MessageBox.Show(Ex.Message);
            }
        }

        private void btn_ok_Click(object sender, EventArgs e)
        {
            PcmsoFacade pcmsoFacade = PcmsoFacade.getInstance();
            Periodicidade periodicidade = null;

            try
            {
                foreach (DataGridViewRow dvRow in grd_periodicidade.SelectedRows)
                {
                    Int32 cellValue = dvRow.Index; // selecao da grid
                    Int64 id = (Int64)grd_periodicidade.Rows[cellValue].Cells[0].Value; // id do item selecionado.

                    periodicidade = new Periodicidade(id);

                    //Salvar GheFonteAgenteExamePeriodicidade
                    pcmsoFacade.incluirGheFonteAgenteExamePeriodicidade(new GheFonteAgenteExamePeriodicidade(periodicidade, formPcmsoAvulso.gheFonteAgenteExame, ApplicationConstants.ATIVO));
                }

                this.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }

        }

        private void btn_fechar_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
