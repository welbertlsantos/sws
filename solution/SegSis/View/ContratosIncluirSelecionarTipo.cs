﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using SWS.View.Resources;
using SWS.Entidade;

namespace SWS.View
{
    public partial class frmContratoIncluirSelecionarTipo : Form
    {
        private Contrato contrato;

        public Contrato Contrato
        {
            get { return contrato; }
            set { contrato = value; }
        }
        
        public frmContratoIncluirSelecionarTipo()
        {
            InitializeComponent();
        }

        private void btnIncluirContrato_Click(object sender, EventArgs e)
        {
            frmContratosIncluir formIncluirContrato = new frmContratosIncluir();
            formIncluirContrato.ShowDialog();

            if (formIncluirContrato.Contrato != null)
            {
                contrato = formIncluirContrato.Contrato;
                this.Close();
            }
        }

        private void btnIncluirProposta_Click(object sender, EventArgs e)
        {
            frmContratosPropostaIncluir formContratoProposta = new frmContratosPropostaIncluir();
            formContratoProposta.ShowDialog();
            
            if (formContratoProposta.Proposta != null)
            {
                contrato = formContratoProposta.Proposta;
                this.Close();
            }
        }

        private void btnIncluirAditivo_Click(object sender, EventArgs e)
        {
            try
            {
                /* será incluído um aditivo, porém é necessário um contrato do sistema fechado para se 
                 * incluir um aditivo.*/

                frmContratosAditivos formAditivo = new frmContratosAditivos();
                formAditivo.ShowDialog();

                if (formAditivo.Aditivo != null)
                {
                    contrato = formAditivo.Aditivo;
                    this.Close();
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
    }
}
