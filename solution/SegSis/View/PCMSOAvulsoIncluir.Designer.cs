﻿namespace SWS.View
{
    partial class frmPcmsoAvulsoIncluir
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle7 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmPcmsoAvulsoIncluir));
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            this.flpAcao = new System.Windows.Forms.FlowLayoutPanel();
            this.btIcluir = new System.Windows.Forms.Button();
            this.btn_pcmso_limpar = new System.Windows.Forms.Button();
            this.btn_fechar = new System.Windows.Forms.Button();
            this.btCoordenador = new System.Windows.Forms.Button();
            this.textCoordenador = new System.Windows.Forms.TextBox();
            this.dataCriacao = new System.Windows.Forms.DateTimePicker();
            this.lblDataValidade = new System.Windows.Forms.TextBox();
            this.lblMedicoCoordenador = new System.Windows.Forms.TextBox();
            this.cbGrauRisco = new SWS.ComboBoxWithBorder();
            this.lblCliente = new System.Windows.Forms.TextBox();
            this.lblRisco = new System.Windows.Forms.TextBox();
            this.textContratada = new System.Windows.Forms.TextBox();
            this.lblCodigoPCMSO = new System.Windows.Forms.TextBox();
            this.lblContratada = new System.Windows.Forms.TextBox();
            this.textCliente = new System.Windows.Forms.TextBox();
            this.btContratada = new System.Windows.Forms.Button();
            this.textCodigo = new System.Windows.Forms.MaskedTextBox();
            this.btCliente = new System.Windows.Forms.Button();
            this.btExcluirEstimativa = new System.Windows.Forms.Button();
            this.btIncluirEstimativa = new System.Windows.Forms.Button();
            this.grbEstimativa = new System.Windows.Forms.GroupBox();
            this.dgvEstimativa = new System.Windows.Forms.DataGridView();
            this.btGheExcluir = new System.Windows.Forms.Button();
            this.btGheIncluir = new System.Windows.Forms.Button();
            this.grbGhe = new System.Windows.Forms.GroupBox();
            this.dgvGhe = new System.Windows.Forms.DataGridView();
            this.btSetorExcluir = new System.Windows.Forms.Button();
            this.btSetorIncluir = new System.Windows.Forms.Button();
            this.grbSetor = new System.Windows.Forms.GroupBox();
            this.dgvSetor = new System.Windows.Forms.DataGridView();
            this.btFuncaoExcluir = new System.Windows.Forms.Button();
            this.btFuncaoIncluir = new System.Windows.Forms.Button();
            this.grbFuncao = new System.Windows.Forms.GroupBox();
            this.dgvFuncao = new System.Windows.Forms.DataGridView();
            this.btAgenteIncluir = new System.Windows.Forms.Button();
            this.btAgenteExcluir = new System.Windows.Forms.Button();
            this.grbAgente = new System.Windows.Forms.GroupBox();
            this.dgvAgente = new System.Windows.Forms.DataGridView();
            this.btExameIncluir = new System.Windows.Forms.Button();
            this.btExameExcluir = new System.Windows.Forms.Button();
            this.btExameAlterar = new System.Windows.Forms.Button();
            this.grbExame = new System.Windows.Forms.GroupBox();
            this.dgvExame = new System.Windows.Forms.DataGridView();
            this.btPeriodicidadeIncluir = new System.Windows.Forms.Button();
            this.grbPeriodicidade = new System.Windows.Forms.GroupBox();
            this.dgvPeriodicidade = new System.Windows.Forms.DataGridView();
            this.btPeriodicidadeExcluir = new System.Windows.Forms.Button();
            this.flpPeriodicidade = new System.Windows.Forms.FlowLayoutPanel();
            this.flpExame = new System.Windows.Forms.FlowLayoutPanel();
            this.flpAgente = new System.Windows.Forms.FlowLayoutPanel();
            this.flpFuncao = new System.Windows.Forms.FlowLayoutPanel();
            this.flpSetor = new System.Windows.Forms.FlowLayoutPanel();
            this.flpGhe = new System.Windows.Forms.FlowLayoutPanel();
            this.btnAlterarGhe = new System.Windows.Forms.Button();
            this.btnDuplicar = new System.Windows.Forms.Button();
            this.flpEstimativa = new System.Windows.Forms.FlowLayoutPanel();
            this.lblNumeroContrato = new System.Windows.Forms.TextBox();
            this.blDataInicioContrato = new System.Windows.Forms.TextBox();
            this.lblTerminoContrato = new System.Windows.Forms.TextBox();
            this.textNumeroContrato = new System.Windows.Forms.TextBox();
            this.textInicioContrato = new System.Windows.Forms.TextBox();
            this.textFimContrato = new System.Windows.Forms.TextBox();
            this.dataValidade = new System.Windows.Forms.DateTimePicker();
            this.lblObra = new System.Windows.Forms.TextBox();
            this.textObra = new System.Windows.Forms.TextBox();
            ((System.ComponentModel.ISupportInitialize)(this.spForm)).BeginInit();
            this.spForm.Panel1.SuspendLayout();
            this.spForm.Panel2.SuspendLayout();
            this.spForm.SuspendLayout();
            this.pnlForm.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.banner)).BeginInit();
            this.flpAcao.SuspendLayout();
            this.grbEstimativa.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvEstimativa)).BeginInit();
            this.grbGhe.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvGhe)).BeginInit();
            this.grbSetor.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvSetor)).BeginInit();
            this.grbFuncao.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvFuncao)).BeginInit();
            this.grbAgente.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvAgente)).BeginInit();
            this.grbExame.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvExame)).BeginInit();
            this.grbPeriodicidade.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvPeriodicidade)).BeginInit();
            this.flpPeriodicidade.SuspendLayout();
            this.flpExame.SuspendLayout();
            this.flpAgente.SuspendLayout();
            this.flpFuncao.SuspendLayout();
            this.flpSetor.SuspendLayout();
            this.flpGhe.SuspendLayout();
            this.flpEstimativa.SuspendLayout();
            this.SuspendLayout();
            // 
            // spForm
            // 
            this.spForm.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            // 
            // spForm.Panel1
            // 
            this.spForm.Panel1.Controls.Add(this.flpAcao);
            // 
            // pnlForm
            // 
            this.pnlForm.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.pnlForm.Controls.Add(this.textObra);
            this.pnlForm.Controls.Add(this.flpExame);
            this.pnlForm.Controls.Add(this.lblObra);
            this.pnlForm.Controls.Add(this.textFimContrato);
            this.pnlForm.Controls.Add(this.textInicioContrato);
            this.pnlForm.Controls.Add(this.textNumeroContrato);
            this.pnlForm.Controls.Add(this.lblTerminoContrato);
            this.pnlForm.Controls.Add(this.blDataInicioContrato);
            this.pnlForm.Controls.Add(this.lblNumeroContrato);
            this.pnlForm.Controls.Add(this.flpEstimativa);
            this.pnlForm.Controls.Add(this.flpGhe);
            this.pnlForm.Controls.Add(this.flpSetor);
            this.pnlForm.Controls.Add(this.flpFuncao);
            this.pnlForm.Controls.Add(this.flpAgente);
            this.pnlForm.Controls.Add(this.flpPeriodicidade);
            this.pnlForm.Controls.Add(this.lblDataValidade);
            this.pnlForm.Controls.Add(this.grbPeriodicidade);
            this.pnlForm.Controls.Add(this.grbExame);
            this.pnlForm.Controls.Add(this.grbAgente);
            this.pnlForm.Controls.Add(this.grbFuncao);
            this.pnlForm.Controls.Add(this.grbSetor);
            this.pnlForm.Controls.Add(this.grbGhe);
            this.pnlForm.Controls.Add(this.grbEstimativa);
            this.pnlForm.Controls.Add(this.btCoordenador);
            this.pnlForm.Controls.Add(this.textCoordenador);
            this.pnlForm.Controls.Add(this.dataCriacao);
            this.pnlForm.Controls.Add(this.lblMedicoCoordenador);
            this.pnlForm.Controls.Add(this.cbGrauRisco);
            this.pnlForm.Controls.Add(this.lblCliente);
            this.pnlForm.Controls.Add(this.lblRisco);
            this.pnlForm.Controls.Add(this.textContratada);
            this.pnlForm.Controls.Add(this.lblCodigoPCMSO);
            this.pnlForm.Controls.Add(this.lblContratada);
            this.pnlForm.Controls.Add(this.textCliente);
            this.pnlForm.Controls.Add(this.btContratada);
            this.pnlForm.Controls.Add(this.textCodigo);
            this.pnlForm.Controls.Add(this.btCliente);
            this.pnlForm.Controls.Add(this.dataValidade);
            this.pnlForm.Size = new System.Drawing.Size(764, 1970);
            // 
            // banner
            // 
            this.banner.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            // 
            // flpAcao
            // 
            this.flpAcao.Controls.Add(this.btIcluir);
            this.flpAcao.Controls.Add(this.btn_pcmso_limpar);
            this.flpAcao.Controls.Add(this.btn_fechar);
            this.flpAcao.Dock = System.Windows.Forms.DockStyle.Fill;
            this.flpAcao.Location = new System.Drawing.Point(0, 0);
            this.flpAcao.Name = "flpAcao";
            this.flpAcao.Size = new System.Drawing.Size(784, 35);
            this.flpAcao.TabIndex = 0;
            // 
            // btIcluir
            // 
            this.btIcluir.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btIcluir.Image = global::SWS.Properties.Resources.Gravar;
            this.btIcluir.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btIcluir.Location = new System.Drawing.Point(3, 3);
            this.btIcluir.Name = "btIcluir";
            this.btIcluir.Size = new System.Drawing.Size(75, 23);
            this.btIcluir.TabIndex = 10;
            this.btIcluir.TabStop = false;
            this.btIcluir.Text = "&Iniciar";
            this.btIcluir.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btIcluir.UseVisualStyleBackColor = true;
            this.btIcluir.Click += new System.EventHandler(this.btn_pcmso_incluir_Click);
            // 
            // btn_pcmso_limpar
            // 
            this.btn_pcmso_limpar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_pcmso_limpar.Image = global::SWS.Properties.Resources.vassoura;
            this.btn_pcmso_limpar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btn_pcmso_limpar.Location = new System.Drawing.Point(84, 3);
            this.btn_pcmso_limpar.Name = "btn_pcmso_limpar";
            this.btn_pcmso_limpar.Size = new System.Drawing.Size(75, 23);
            this.btn_pcmso_limpar.TabIndex = 9;
            this.btn_pcmso_limpar.TabStop = false;
            this.btn_pcmso_limpar.Text = "&Limpar";
            this.btn_pcmso_limpar.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btn_pcmso_limpar.UseVisualStyleBackColor = true;
            this.btn_pcmso_limpar.Click += new System.EventHandler(this.btn_pcmso_limpar_Click);
            // 
            // btn_fechar
            // 
            this.btn_fechar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_fechar.Image = global::SWS.Properties.Resources.close;
            this.btn_fechar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btn_fechar.Location = new System.Drawing.Point(165, 3);
            this.btn_fechar.Name = "btn_fechar";
            this.btn_fechar.Size = new System.Drawing.Size(75, 23);
            this.btn_fechar.TabIndex = 8;
            this.btn_fechar.TabStop = false;
            this.btn_fechar.Text = "&Fechar";
            this.btn_fechar.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btn_fechar.UseVisualStyleBackColor = true;
            this.btn_fechar.Click += new System.EventHandler(this.btn_fechar_Click);
            // 
            // btCoordenador
            // 
            this.btCoordenador.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btCoordenador.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btCoordenador.Image = global::SWS.Properties.Resources.busca;
            this.btCoordenador.Location = new System.Drawing.Point(723, 93);
            this.btCoordenador.Name = "btCoordenador";
            this.btCoordenador.Size = new System.Drawing.Size(26, 21);
            this.btCoordenador.TabIndex = 4;
            this.btCoordenador.UseVisualStyleBackColor = true;
            this.btCoordenador.Click += new System.EventHandler(this.btCoordenador_Click);
            // 
            // textCoordenador
            // 
            this.textCoordenador.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.textCoordenador.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.textCoordenador.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textCoordenador.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textCoordenador.Location = new System.Drawing.Point(171, 93);
            this.textCoordenador.MaxLength = 100;
            this.textCoordenador.Name = "textCoordenador";
            this.textCoordenador.ReadOnly = true;
            this.textCoordenador.Size = new System.Drawing.Size(553, 21);
            this.textCoordenador.TabIndex = 41;
            this.textCoordenador.TabStop = false;
            // 
            // dataCriacao
            // 
            this.dataCriacao.Checked = false;
            this.dataCriacao.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dataCriacao.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dataCriacao.Location = new System.Drawing.Point(171, 113);
            this.dataCriacao.Name = "dataCriacao";
            this.dataCriacao.ShowCheckBox = true;
            this.dataCriacao.Size = new System.Drawing.Size(120, 21);
            this.dataCriacao.TabIndex = 5;
            // 
            // lblDataValidade
            // 
            this.lblDataValidade.BackColor = System.Drawing.Color.LightGray;
            this.lblDataValidade.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblDataValidade.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDataValidade.Location = new System.Drawing.Point(10, 113);
            this.lblDataValidade.Name = "lblDataValidade";
            this.lblDataValidade.ReadOnly = true;
            this.lblDataValidade.Size = new System.Drawing.Size(162, 21);
            this.lblDataValidade.TabIndex = 42;
            this.lblDataValidade.TabStop = false;
            this.lblDataValidade.Text = "Vigência";
            // 
            // lblMedicoCoordenador
            // 
            this.lblMedicoCoordenador.BackColor = System.Drawing.Color.LightGray;
            this.lblMedicoCoordenador.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblMedicoCoordenador.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblMedicoCoordenador.Location = new System.Drawing.Point(10, 93);
            this.lblMedicoCoordenador.Name = "lblMedicoCoordenador";
            this.lblMedicoCoordenador.ReadOnly = true;
            this.lblMedicoCoordenador.Size = new System.Drawing.Size(162, 21);
            this.lblMedicoCoordenador.TabIndex = 40;
            this.lblMedicoCoordenador.TabStop = false;
            this.lblMedicoCoordenador.Text = "Médido Coordenador";
            // 
            // cbGrauRisco
            // 
            this.cbGrauRisco.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.cbGrauRisco.BackColor = System.Drawing.Color.White;
            this.cbGrauRisco.BorderColor = System.Drawing.Color.DimGray;
            this.cbGrauRisco.BorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid;
            this.cbGrauRisco.FormattingEnabled = true;
            this.cbGrauRisco.Location = new System.Drawing.Point(171, 73);
            this.cbGrauRisco.Name = "cbGrauRisco";
            this.cbGrauRisco.Size = new System.Drawing.Size(578, 21);
            this.cbGrauRisco.TabIndex = 3;
            // 
            // lblCliente
            // 
            this.lblCliente.BackColor = System.Drawing.Color.LightGray;
            this.lblCliente.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblCliente.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCliente.Location = new System.Drawing.Point(10, 13);
            this.lblCliente.Name = "lblCliente";
            this.lblCliente.ReadOnly = true;
            this.lblCliente.Size = new System.Drawing.Size(162, 21);
            this.lblCliente.TabIndex = 36;
            this.lblCliente.TabStop = false;
            this.lblCliente.Text = "Cliente";
            // 
            // lblRisco
            // 
            this.lblRisco.BackColor = System.Drawing.Color.LightGray;
            this.lblRisco.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblRisco.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblRisco.Location = new System.Drawing.Point(10, 73);
            this.lblRisco.Name = "lblRisco";
            this.lblRisco.ReadOnly = true;
            this.lblRisco.Size = new System.Drawing.Size(162, 21);
            this.lblRisco.TabIndex = 39;
            this.lblRisco.TabStop = false;
            this.lblRisco.Text = "Risco do PCMSO";
            // 
            // textContratada
            // 
            this.textContratada.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.textContratada.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.textContratada.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textContratada.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textContratada.Location = new System.Drawing.Point(171, 33);
            this.textContratada.MaxLength = 100;
            this.textContratada.Name = "textContratada";
            this.textContratada.ReadOnly = true;
            this.textContratada.Size = new System.Drawing.Size(553, 21);
            this.textContratada.TabIndex = 29;
            this.textContratada.TabStop = false;
            // 
            // lblCodigoPCMSO
            // 
            this.lblCodigoPCMSO.BackColor = System.Drawing.Color.LightGray;
            this.lblCodigoPCMSO.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblCodigoPCMSO.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCodigoPCMSO.Location = new System.Drawing.Point(10, 53);
            this.lblCodigoPCMSO.Name = "lblCodigoPCMSO";
            this.lblCodigoPCMSO.ReadOnly = true;
            this.lblCodigoPCMSO.Size = new System.Drawing.Size(162, 21);
            this.lblCodigoPCMSO.TabIndex = 38;
            this.lblCodigoPCMSO.TabStop = false;
            this.lblCodigoPCMSO.Text = "Código do PCMSO";
            // 
            // lblContratada
            // 
            this.lblContratada.BackColor = System.Drawing.Color.LightGray;
            this.lblContratada.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblContratada.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblContratada.Location = new System.Drawing.Point(10, 33);
            this.lblContratada.Name = "lblContratada";
            this.lblContratada.ReadOnly = true;
            this.lblContratada.Size = new System.Drawing.Size(162, 21);
            this.lblContratada.TabIndex = 37;
            this.lblContratada.TabStop = false;
            this.lblContratada.Text = "Cliente Contratante";
            // 
            // textCliente
            // 
            this.textCliente.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.textCliente.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.textCliente.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textCliente.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textCliente.Location = new System.Drawing.Point(171, 13);
            this.textCliente.MaxLength = 100;
            this.textCliente.Name = "textCliente";
            this.textCliente.ReadOnly = true;
            this.textCliente.Size = new System.Drawing.Size(553, 21);
            this.textCliente.TabIndex = 31;
            this.textCliente.TabStop = false;
            // 
            // btContratada
            // 
            this.btContratada.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btContratada.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btContratada.Image = global::SWS.Properties.Resources.busca;
            this.btContratada.Location = new System.Drawing.Point(723, 33);
            this.btContratada.Name = "btContratada";
            this.btContratada.Size = new System.Drawing.Size(26, 21);
            this.btContratada.TabIndex = 2;
            this.btContratada.UseVisualStyleBackColor = true;
            this.btContratada.Click += new System.EventHandler(this.btn_contratada_Click);
            // 
            // textCodigo
            // 
            this.textCodigo.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.textCodigo.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.textCodigo.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textCodigo.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textCodigo.ForeColor = System.Drawing.Color.Black;
            this.textCodigo.Location = new System.Drawing.Point(171, 53);
            this.textCodigo.Mask = "0000,00,000000/00";
            this.textCodigo.Name = "textCodigo";
            this.textCodigo.PromptChar = ' ';
            this.textCodigo.ReadOnly = true;
            this.textCodigo.Size = new System.Drawing.Size(578, 22);
            this.textCodigo.TabIndex = 30;
            this.textCodigo.TabStop = false;
            // 
            // btCliente
            // 
            this.btCliente.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btCliente.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btCliente.Image = global::SWS.Properties.Resources.busca;
            this.btCliente.Location = new System.Drawing.Point(723, 13);
            this.btCliente.Name = "btCliente";
            this.btCliente.Size = new System.Drawing.Size(26, 21);
            this.btCliente.TabIndex = 1;
            this.btCliente.UseVisualStyleBackColor = true;
            this.btCliente.Click += new System.EventHandler(this.btn_cliente_Click);
            // 
            // btExcluirEstimativa
            // 
            this.btExcluirEstimativa.Enabled = false;
            this.btExcluirEstimativa.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btExcluirEstimativa.Image = ((System.Drawing.Image)(resources.GetObject("btExcluirEstimativa.Image")));
            this.btExcluirEstimativa.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btExcluirEstimativa.Location = new System.Drawing.Point(84, 3);
            this.btExcluirEstimativa.Name = "btExcluirEstimativa";
            this.btExcluirEstimativa.Size = new System.Drawing.Size(75, 23);
            this.btExcluirEstimativa.TabIndex = 46;
            this.btExcluirEstimativa.TabStop = false;
            this.btExcluirEstimativa.Text = "&Excluir";
            this.btExcluirEstimativa.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btExcluirEstimativa.UseVisualStyleBackColor = true;
            this.btExcluirEstimativa.Click += new System.EventHandler(this.btExcluirEstimativa_Click);
            // 
            // btIncluirEstimativa
            // 
            this.btIncluirEstimativa.Enabled = false;
            this.btIncluirEstimativa.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btIncluirEstimativa.Image = ((System.Drawing.Image)(resources.GetObject("btIncluirEstimativa.Image")));
            this.btIncluirEstimativa.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btIncluirEstimativa.Location = new System.Drawing.Point(3, 3);
            this.btIncluirEstimativa.Name = "btIncluirEstimativa";
            this.btIncluirEstimativa.Size = new System.Drawing.Size(75, 23);
            this.btIncluirEstimativa.TabIndex = 6;
            this.btIncluirEstimativa.Text = "&Incluir";
            this.btIncluirEstimativa.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btIncluirEstimativa.UseVisualStyleBackColor = true;
            this.btIncluirEstimativa.Click += new System.EventHandler(this.btIncluirEstimativa_Click);
            // 
            // grbEstimativa
            // 
            this.grbEstimativa.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.grbEstimativa.Controls.Add(this.dgvEstimativa);
            this.grbEstimativa.Enabled = false;
            this.grbEstimativa.Location = new System.Drawing.Point(10, 224);
            this.grbEstimativa.Name = "grbEstimativa";
            this.grbEstimativa.Size = new System.Drawing.Size(739, 200);
            this.grbEstimativa.TabIndex = 44;
            this.grbEstimativa.TabStop = false;
            this.grbEstimativa.Text = "Estimativa";
            // 
            // dgvEstimativa
            // 
            this.dgvEstimativa.AllowUserToAddRows = false;
            this.dgvEstimativa.AllowUserToDeleteRows = false;
            this.dgvEstimativa.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this.dgvEstimativa.BackgroundColor = System.Drawing.Color.White;
            this.dgvEstimativa.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dataGridViewCellStyle7.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle7.BackColor = System.Drawing.Color.LightGray;
            dataGridViewCellStyle7.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle7.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle7.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle7.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle7.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvEstimativa.DefaultCellStyle = dataGridViewCellStyle7;
            this.dgvEstimativa.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvEstimativa.Location = new System.Drawing.Point(3, 16);
            this.dgvEstimativa.MultiSelect = false;
            this.dgvEstimativa.Name = "dgvEstimativa";
            this.dgvEstimativa.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvEstimativa.Size = new System.Drawing.Size(733, 181);
            this.dgvEstimativa.TabIndex = 1;
            this.dgvEstimativa.TabStop = false;
            this.dgvEstimativa.CellEndEdit += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvEstimativa_CellEndEdit);
            this.dgvEstimativa.DataError += new System.Windows.Forms.DataGridViewDataErrorEventHandler(this.dgvEstimativa_DataError);
            this.dgvEstimativa.EditingControlShowing += new System.Windows.Forms.DataGridViewEditingControlShowingEventHandler(this.dgvEstimativa_EditingControlShowing);
            this.dgvEstimativa.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.dgvEstimativa_KeyPress);
            // 
            // btGheExcluir
            // 
            this.btGheExcluir.Enabled = false;
            this.btGheExcluir.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btGheExcluir.Image = ((System.Drawing.Image)(resources.GetObject("btGheExcluir.Image")));
            this.btGheExcluir.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btGheExcluir.Location = new System.Drawing.Point(165, 3);
            this.btGheExcluir.Name = "btGheExcluir";
            this.btGheExcluir.Size = new System.Drawing.Size(75, 23);
            this.btGheExcluir.TabIndex = 49;
            this.btGheExcluir.TabStop = false;
            this.btGheExcluir.Text = "&Excluir";
            this.btGheExcluir.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btGheExcluir.UseVisualStyleBackColor = true;
            this.btGheExcluir.Click += new System.EventHandler(this.btn_ghe_excluir_Click);
            // 
            // btGheIncluir
            // 
            this.btGheIncluir.Enabled = false;
            this.btGheIncluir.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btGheIncluir.Image = ((System.Drawing.Image)(resources.GetObject("btGheIncluir.Image")));
            this.btGheIncluir.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btGheIncluir.Location = new System.Drawing.Point(3, 3);
            this.btGheIncluir.Name = "btGheIncluir";
            this.btGheIncluir.Size = new System.Drawing.Size(75, 23);
            this.btGheIncluir.TabIndex = 8;
            this.btGheIncluir.Text = "&Incluir";
            this.btGheIncluir.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btGheIncluir.UseVisualStyleBackColor = true;
            this.btGheIncluir.Click += new System.EventHandler(this.btn_ghe_incluir_Click);
            // 
            // grbGhe
            // 
            this.grbGhe.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.grbGhe.Controls.Add(this.dgvGhe);
            this.grbGhe.Enabled = false;
            this.grbGhe.Location = new System.Drawing.Point(10, 465);
            this.grbGhe.Name = "grbGhe";
            this.grbGhe.Size = new System.Drawing.Size(739, 200);
            this.grbGhe.TabIndex = 47;
            this.grbGhe.TabStop = false;
            this.grbGhe.Text = "Ghe";
            // 
            // dgvGhe
            // 
            this.dgvGhe.AllowUserToAddRows = false;
            this.dgvGhe.AllowUserToDeleteRows = false;
            this.dgvGhe.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this.dgvGhe.BackgroundColor = System.Drawing.Color.White;
            this.dgvGhe.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle6.BackColor = System.Drawing.Color.LightGray;
            dataGridViewCellStyle6.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle6.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle6.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle6.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle6.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvGhe.DefaultCellStyle = dataGridViewCellStyle6;
            this.dgvGhe.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvGhe.Location = new System.Drawing.Point(3, 16);
            this.dgvGhe.MultiSelect = false;
            this.dgvGhe.Name = "dgvGhe";
            this.dgvGhe.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvGhe.Size = new System.Drawing.Size(733, 181);
            this.dgvGhe.TabIndex = 7;
            this.dgvGhe.TabStop = false;
            this.dgvGhe.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.grd_ghe_CellClick);
            this.dgvGhe.CellEndEdit += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvGhe_CellEndEdit);
            this.dgvGhe.DataBindingComplete += new System.Windows.Forms.DataGridViewBindingCompleteEventHandler(this.dgvGhe_DataBindingComplete);
            this.dgvGhe.DataError += new System.Windows.Forms.DataGridViewDataErrorEventHandler(this.dgvGhe_DataError);
            this.dgvGhe.EditingControlShowing += new System.Windows.Forms.DataGridViewEditingControlShowingEventHandler(this.dgvGhe_EditingControlShowing);
            this.dgvGhe.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.dgvGhe_KeyPress);
            // 
            // btSetorExcluir
            // 
            this.btSetorExcluir.Enabled = false;
            this.btSetorExcluir.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btSetorExcluir.Image = ((System.Drawing.Image)(resources.GetObject("btSetorExcluir.Image")));
            this.btSetorExcluir.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btSetorExcluir.Location = new System.Drawing.Point(84, 3);
            this.btSetorExcluir.Name = "btSetorExcluir";
            this.btSetorExcluir.Size = new System.Drawing.Size(75, 23);
            this.btSetorExcluir.TabIndex = 52;
            this.btSetorExcluir.TabStop = false;
            this.btSetorExcluir.Text = "&Excluir";
            this.btSetorExcluir.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btSetorExcluir.UseVisualStyleBackColor = true;
            this.btSetorExcluir.Click += new System.EventHandler(this.btn_setor_excluir_Click);
            // 
            // btSetorIncluir
            // 
            this.btSetorIncluir.Enabled = false;
            this.btSetorIncluir.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btSetorIncluir.Image = ((System.Drawing.Image)(resources.GetObject("btSetorIncluir.Image")));
            this.btSetorIncluir.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btSetorIncluir.Location = new System.Drawing.Point(3, 3);
            this.btSetorIncluir.Name = "btSetorIncluir";
            this.btSetorIncluir.Size = new System.Drawing.Size(75, 23);
            this.btSetorIncluir.TabIndex = 10;
            this.btSetorIncluir.Text = "&Incluir";
            this.btSetorIncluir.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btSetorIncluir.UseVisualStyleBackColor = true;
            this.btSetorIncluir.Click += new System.EventHandler(this.btn_setor_incluir_Click);
            // 
            // grbSetor
            // 
            this.grbSetor.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.grbSetor.Controls.Add(this.dgvSetor);
            this.grbSetor.Enabled = false;
            this.grbSetor.Location = new System.Drawing.Point(10, 700);
            this.grbSetor.Name = "grbSetor";
            this.grbSetor.Size = new System.Drawing.Size(739, 214);
            this.grbSetor.TabIndex = 50;
            this.grbSetor.TabStop = false;
            this.grbSetor.Text = "Setores";
            // 
            // dgvSetor
            // 
            this.dgvSetor.AllowUserToAddRows = false;
            this.dgvSetor.AllowUserToDeleteRows = false;
            this.dgvSetor.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this.dgvSetor.BackgroundColor = System.Drawing.Color.White;
            this.dgvSetor.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle5.BackColor = System.Drawing.Color.LightGray;
            dataGridViewCellStyle5.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle5.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle5.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle5.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle5.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvSetor.DefaultCellStyle = dataGridViewCellStyle5;
            this.dgvSetor.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvSetor.Location = new System.Drawing.Point(3, 16);
            this.dgvSetor.MultiSelect = false;
            this.dgvSetor.Name = "dgvSetor";
            this.dgvSetor.ReadOnly = true;
            this.dgvSetor.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvSetor.Size = new System.Drawing.Size(733, 195);
            this.dgvSetor.TabIndex = 9;
            this.dgvSetor.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.grd_setor_CellClick);
            this.dgvSetor.DataBindingComplete += new System.Windows.Forms.DataGridViewBindingCompleteEventHandler(this.dgvSetor_DataBindingComplete);
            // 
            // btFuncaoExcluir
            // 
            this.btFuncaoExcluir.Enabled = false;
            this.btFuncaoExcluir.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btFuncaoExcluir.Image = ((System.Drawing.Image)(resources.GetObject("btFuncaoExcluir.Image")));
            this.btFuncaoExcluir.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btFuncaoExcluir.Location = new System.Drawing.Point(84, 3);
            this.btFuncaoExcluir.Name = "btFuncaoExcluir";
            this.btFuncaoExcluir.Size = new System.Drawing.Size(75, 23);
            this.btFuncaoExcluir.TabIndex = 55;
            this.btFuncaoExcluir.TabStop = false;
            this.btFuncaoExcluir.Text = "&Excluir";
            this.btFuncaoExcluir.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btFuncaoExcluir.UseVisualStyleBackColor = true;
            this.btFuncaoExcluir.Click += new System.EventHandler(this.btn_funcao_excluir_Click);
            // 
            // btFuncaoIncluir
            // 
            this.btFuncaoIncluir.Enabled = false;
            this.btFuncaoIncluir.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btFuncaoIncluir.Image = ((System.Drawing.Image)(resources.GetObject("btFuncaoIncluir.Image")));
            this.btFuncaoIncluir.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btFuncaoIncluir.Location = new System.Drawing.Point(3, 3);
            this.btFuncaoIncluir.Name = "btFuncaoIncluir";
            this.btFuncaoIncluir.Size = new System.Drawing.Size(75, 23);
            this.btFuncaoIncluir.TabIndex = 12;
            this.btFuncaoIncluir.Text = "&Incluir";
            this.btFuncaoIncluir.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btFuncaoIncluir.UseVisualStyleBackColor = true;
            this.btFuncaoIncluir.Click += new System.EventHandler(this.btn_funcao_incluir_Click);
            // 
            // grbFuncao
            // 
            this.grbFuncao.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.grbFuncao.Controls.Add(this.dgvFuncao);
            this.grbFuncao.Enabled = false;
            this.grbFuncao.Location = new System.Drawing.Point(10, 949);
            this.grbFuncao.Name = "grbFuncao";
            this.grbFuncao.Size = new System.Drawing.Size(739, 200);
            this.grbFuncao.TabIndex = 54;
            this.grbFuncao.TabStop = false;
            this.grbFuncao.Text = "Funções";
            // 
            // dgvFuncao
            // 
            this.dgvFuncao.AllowUserToAddRows = false;
            this.dgvFuncao.AllowUserToDeleteRows = false;
            this.dgvFuncao.BackgroundColor = System.Drawing.Color.White;
            this.dgvFuncao.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle4.BackColor = System.Drawing.Color.LightGray;
            dataGridViewCellStyle4.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle4.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle4.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle4.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle4.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvFuncao.DefaultCellStyle = dataGridViewCellStyle4;
            this.dgvFuncao.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvFuncao.Location = new System.Drawing.Point(3, 16);
            this.dgvFuncao.MultiSelect = false;
            this.dgvFuncao.Name = "dgvFuncao";
            this.dgvFuncao.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvFuncao.Size = new System.Drawing.Size(733, 181);
            this.dgvFuncao.TabIndex = 11;
            this.dgvFuncao.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvFuncao_CellContentClick);
            this.dgvFuncao.DataBindingComplete += new System.Windows.Forms.DataGridViewBindingCompleteEventHandler(this.dgvFuncao_DataBindingComplete);
            // 
            // btAgenteIncluir
            // 
            this.btAgenteIncluir.Enabled = false;
            this.btAgenteIncluir.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btAgenteIncluir.Image = ((System.Drawing.Image)(resources.GetObject("btAgenteIncluir.Image")));
            this.btAgenteIncluir.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btAgenteIncluir.Location = new System.Drawing.Point(3, 3);
            this.btAgenteIncluir.Name = "btAgenteIncluir";
            this.btAgenteIncluir.Size = new System.Drawing.Size(75, 23);
            this.btAgenteIncluir.TabIndex = 14;
            this.btAgenteIncluir.Text = "&Incluir";
            this.btAgenteIncluir.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btAgenteIncluir.UseVisualStyleBackColor = true;
            this.btAgenteIncluir.Click += new System.EventHandler(this.btn_agente_incluir_Click);
            // 
            // btAgenteExcluir
            // 
            this.btAgenteExcluir.Enabled = false;
            this.btAgenteExcluir.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btAgenteExcluir.Image = ((System.Drawing.Image)(resources.GetObject("btAgenteExcluir.Image")));
            this.btAgenteExcluir.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btAgenteExcluir.Location = new System.Drawing.Point(84, 3);
            this.btAgenteExcluir.Name = "btAgenteExcluir";
            this.btAgenteExcluir.Size = new System.Drawing.Size(75, 23);
            this.btAgenteExcluir.TabIndex = 15;
            this.btAgenteExcluir.Text = "&Excluir";
            this.btAgenteExcluir.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btAgenteExcluir.UseVisualStyleBackColor = true;
            this.btAgenteExcluir.Click += new System.EventHandler(this.btn_agente_excluir_Click);
            // 
            // grbAgente
            // 
            this.grbAgente.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.grbAgente.Controls.Add(this.dgvAgente);
            this.grbAgente.Enabled = false;
            this.grbAgente.Location = new System.Drawing.Point(10, 1184);
            this.grbAgente.Name = "grbAgente";
            this.grbAgente.Size = new System.Drawing.Size(739, 199);
            this.grbAgente.TabIndex = 56;
            this.grbAgente.TabStop = false;
            this.grbAgente.Text = "Agentes";
            // 
            // dgvAgente
            // 
            this.dgvAgente.AllowUserToAddRows = false;
            this.dgvAgente.AllowUserToDeleteRows = false;
            this.dgvAgente.AllowUserToOrderColumns = true;
            this.dgvAgente.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this.dgvAgente.BackgroundColor = System.Drawing.Color.White;
            this.dgvAgente.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle3.BackColor = System.Drawing.Color.LightGray;
            dataGridViewCellStyle3.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvAgente.DefaultCellStyle = dataGridViewCellStyle3;
            this.dgvAgente.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvAgente.Location = new System.Drawing.Point(3, 16);
            this.dgvAgente.MultiSelect = false;
            this.dgvAgente.Name = "dgvAgente";
            this.dgvAgente.ReadOnly = true;
            this.dgvAgente.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvAgente.Size = new System.Drawing.Size(733, 180);
            this.dgvAgente.TabIndex = 13;
            this.dgvAgente.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.grd_agente_CellClick);
            this.dgvAgente.DataBindingComplete += new System.Windows.Forms.DataGridViewBindingCompleteEventHandler(this.dgvAgente_DataBindingComplete);
            // 
            // btExameIncluir
            // 
            this.btExameIncluir.Enabled = false;
            this.btExameIncluir.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btExameIncluir.Image = ((System.Drawing.Image)(resources.GetObject("btExameIncluir.Image")));
            this.btExameIncluir.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btExameIncluir.Location = new System.Drawing.Point(3, 3);
            this.btExameIncluir.Name = "btExameIncluir";
            this.btExameIncluir.Size = new System.Drawing.Size(75, 23);
            this.btExameIncluir.TabIndex = 17;
            this.btExameIncluir.Text = "&Incluir";
            this.btExameIncluir.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btExameIncluir.UseVisualStyleBackColor = true;
            this.btExameIncluir.Click += new System.EventHandler(this.btn_exame_incluir_Click);
            // 
            // btExameExcluir
            // 
            this.btExameExcluir.Enabled = false;
            this.btExameExcluir.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btExameExcluir.Image = ((System.Drawing.Image)(resources.GetObject("btExameExcluir.Image")));
            this.btExameExcluir.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btExameExcluir.Location = new System.Drawing.Point(165, 3);
            this.btExameExcluir.Name = "btExameExcluir";
            this.btExameExcluir.Size = new System.Drawing.Size(75, 23);
            this.btExameExcluir.TabIndex = 18;
            this.btExameExcluir.TabStop = false;
            this.btExameExcluir.Text = "&Excluir";
            this.btExameExcluir.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btExameExcluir.UseVisualStyleBackColor = true;
            this.btExameExcluir.Click += new System.EventHandler(this.btn_exame_excluir_Click);
            // 
            // btExameAlterar
            // 
            this.btExameAlterar.Enabled = false;
            this.btExameAlterar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btExameAlterar.Image = global::SWS.Properties.Resources.Alterar;
            this.btExameAlterar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btExameAlterar.Location = new System.Drawing.Point(84, 3);
            this.btExameAlterar.Name = "btExameAlterar";
            this.btExameAlterar.Size = new System.Drawing.Size(75, 23);
            this.btExameAlterar.TabIndex = 62;
            this.btExameAlterar.TabStop = false;
            this.btExameAlterar.Text = "&Alterar";
            this.btExameAlterar.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btExameAlterar.UseVisualStyleBackColor = true;
            this.btExameAlterar.Click += new System.EventHandler(this.btn_exame_alterar_Click);
            // 
            // grbExame
            // 
            this.grbExame.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.grbExame.Controls.Add(this.dgvExame);
            this.grbExame.Enabled = false;
            this.grbExame.Location = new System.Drawing.Point(10, 1418);
            this.grbExame.Name = "grbExame";
            this.grbExame.Size = new System.Drawing.Size(739, 200);
            this.grbExame.TabIndex = 60;
            this.grbExame.TabStop = false;
            this.grbExame.Text = "Exames";
            // 
            // dgvExame
            // 
            this.dgvExame.AllowUserToAddRows = false;
            this.dgvExame.AllowUserToDeleteRows = false;
            this.dgvExame.BackgroundColor = System.Drawing.Color.White;
            this.dgvExame.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.Color.Silver;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvExame.DefaultCellStyle = dataGridViewCellStyle2;
            this.dgvExame.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvExame.Location = new System.Drawing.Point(3, 16);
            this.dgvExame.MultiSelect = false;
            this.dgvExame.Name = "dgvExame";
            this.dgvExame.ReadOnly = true;
            this.dgvExame.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvExame.Size = new System.Drawing.Size(733, 181);
            this.dgvExame.TabIndex = 16;
            this.dgvExame.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.grd_exame_CellClick);
            this.dgvExame.DataBindingComplete += new System.Windows.Forms.DataGridViewBindingCompleteEventHandler(this.dgvExame_DataBindingComplete);
            // 
            // btPeriodicidadeIncluir
            // 
            this.btPeriodicidadeIncluir.Enabled = false;
            this.btPeriodicidadeIncluir.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btPeriodicidadeIncluir.Image = ((System.Drawing.Image)(resources.GetObject("btPeriodicidadeIncluir.Image")));
            this.btPeriodicidadeIncluir.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btPeriodicidadeIncluir.Location = new System.Drawing.Point(3, 3);
            this.btPeriodicidadeIncluir.Name = "btPeriodicidadeIncluir";
            this.btPeriodicidadeIncluir.Size = new System.Drawing.Size(75, 23);
            this.btPeriodicidadeIncluir.TabIndex = 20;
            this.btPeriodicidadeIncluir.Text = "&Incluir";
            this.btPeriodicidadeIncluir.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btPeriodicidadeIncluir.UseVisualStyleBackColor = true;
            this.btPeriodicidadeIncluir.Click += new System.EventHandler(this.btn_periodicidade_incluir_Click);
            // 
            // grbPeriodicidade
            // 
            this.grbPeriodicidade.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.grbPeriodicidade.Controls.Add(this.dgvPeriodicidade);
            this.grbPeriodicidade.Enabled = false;
            this.grbPeriodicidade.Location = new System.Drawing.Point(10, 1661);
            this.grbPeriodicidade.Name = "grbPeriodicidade";
            this.grbPeriodicidade.Size = new System.Drawing.Size(739, 205);
            this.grbPeriodicidade.TabIndex = 65;
            this.grbPeriodicidade.TabStop = false;
            this.grbPeriodicidade.Text = "Periodicidades";
            // 
            // dgvPeriodicidade
            // 
            this.dgvPeriodicidade.AllowUserToAddRows = false;
            this.dgvPeriodicidade.AllowUserToDeleteRows = false;
            this.dgvPeriodicidade.AllowUserToOrderColumns = true;
            this.dgvPeriodicidade.BackgroundColor = System.Drawing.Color.White;
            this.dgvPeriodicidade.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.Silver;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvPeriodicidade.DefaultCellStyle = dataGridViewCellStyle1;
            this.dgvPeriodicidade.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvPeriodicidade.Location = new System.Drawing.Point(3, 16);
            this.dgvPeriodicidade.MultiSelect = false;
            this.dgvPeriodicidade.Name = "dgvPeriodicidade";
            this.dgvPeriodicidade.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvPeriodicidade.Size = new System.Drawing.Size(733, 186);
            this.dgvPeriodicidade.TabIndex = 19;
            this.dgvPeriodicidade.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.grd_periodicidade_CellClick);
            this.dgvPeriodicidade.CellEndEdit += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvPeriodicidade_CellEndEdit);
            this.dgvPeriodicidade.DataBindingComplete += new System.Windows.Forms.DataGridViewBindingCompleteEventHandler(this.dgvPeriodicidade_DataBindingComplete);
            // 
            // btPeriodicidadeExcluir
            // 
            this.btPeriodicidadeExcluir.Enabled = false;
            this.btPeriodicidadeExcluir.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btPeriodicidadeExcluir.Image = ((System.Drawing.Image)(resources.GetObject("btPeriodicidadeExcluir.Image")));
            this.btPeriodicidadeExcluir.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btPeriodicidadeExcluir.Location = new System.Drawing.Point(84, 3);
            this.btPeriodicidadeExcluir.Name = "btPeriodicidadeExcluir";
            this.btPeriodicidadeExcluir.Size = new System.Drawing.Size(75, 23);
            this.btPeriodicidadeExcluir.TabIndex = 64;
            this.btPeriodicidadeExcluir.TabStop = false;
            this.btPeriodicidadeExcluir.Text = "&Excluir";
            this.btPeriodicidadeExcluir.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btPeriodicidadeExcluir.UseVisualStyleBackColor = true;
            this.btPeriodicidadeExcluir.Click += new System.EventHandler(this.btn_periodicidade_excluir_Click);
            // 
            // flpPeriodicidade
            // 
            this.flpPeriodicidade.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.flpPeriodicidade.Controls.Add(this.btPeriodicidadeIncluir);
            this.flpPeriodicidade.Controls.Add(this.btPeriodicidadeExcluir);
            this.flpPeriodicidade.Enabled = false;
            this.flpPeriodicidade.Location = new System.Drawing.Point(10, 1869);
            this.flpPeriodicidade.Name = "flpPeriodicidade";
            this.flpPeriodicidade.Size = new System.Drawing.Size(739, 30);
            this.flpPeriodicidade.TabIndex = 66;
            // 
            // flpExame
            // 
            this.flpExame.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.flpExame.Controls.Add(this.btExameIncluir);
            this.flpExame.Controls.Add(this.btExameAlterar);
            this.flpExame.Controls.Add(this.btExameExcluir);
            this.flpExame.Enabled = false;
            this.flpExame.Location = new System.Drawing.Point(10, 1624);
            this.flpExame.Name = "flpExame";
            this.flpExame.Size = new System.Drawing.Size(739, 31);
            this.flpExame.TabIndex = 67;
            // 
            // flpAgente
            // 
            this.flpAgente.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.flpAgente.Controls.Add(this.btAgenteIncluir);
            this.flpAgente.Controls.Add(this.btAgenteExcluir);
            this.flpAgente.Enabled = false;
            this.flpAgente.Location = new System.Drawing.Point(10, 1386);
            this.flpAgente.Name = "flpAgente";
            this.flpAgente.Size = new System.Drawing.Size(739, 30);
            this.flpAgente.TabIndex = 68;
            // 
            // flpFuncao
            // 
            this.flpFuncao.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.flpFuncao.Controls.Add(this.btFuncaoIncluir);
            this.flpFuncao.Controls.Add(this.btFuncaoExcluir);
            this.flpFuncao.Enabled = false;
            this.flpFuncao.Location = new System.Drawing.Point(10, 1152);
            this.flpFuncao.Name = "flpFuncao";
            this.flpFuncao.Size = new System.Drawing.Size(739, 30);
            this.flpFuncao.TabIndex = 69;
            // 
            // flpSetor
            // 
            this.flpSetor.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.flpSetor.Controls.Add(this.btSetorIncluir);
            this.flpSetor.Controls.Add(this.btSetorExcluir);
            this.flpSetor.Enabled = false;
            this.flpSetor.Location = new System.Drawing.Point(10, 917);
            this.flpSetor.Name = "flpSetor";
            this.flpSetor.Size = new System.Drawing.Size(739, 32);
            this.flpSetor.TabIndex = 70;
            // 
            // flpGhe
            // 
            this.flpGhe.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.flpGhe.Controls.Add(this.btGheIncluir);
            this.flpGhe.Controls.Add(this.btnAlterarGhe);
            this.flpGhe.Controls.Add(this.btGheExcluir);
            this.flpGhe.Controls.Add(this.btnDuplicar);
            this.flpGhe.Enabled = false;
            this.flpGhe.Location = new System.Drawing.Point(10, 668);
            this.flpGhe.Name = "flpGhe";
            this.flpGhe.Size = new System.Drawing.Size(739, 32);
            this.flpGhe.TabIndex = 71;
            // 
            // btnAlterarGhe
            // 
            this.btnAlterarGhe.Enabled = false;
            this.btnAlterarGhe.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnAlterarGhe.Image = global::SWS.Properties.Resources.Alterar;
            this.btnAlterarGhe.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnAlterarGhe.Location = new System.Drawing.Point(84, 3);
            this.btnAlterarGhe.Name = "btnAlterarGhe";
            this.btnAlterarGhe.Size = new System.Drawing.Size(75, 23);
            this.btnAlterarGhe.TabIndex = 8;
            this.btnAlterarGhe.TabStop = false;
            this.btnAlterarGhe.Text = "&Alterar";
            this.btnAlterarGhe.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnAlterarGhe.UseVisualStyleBackColor = true;
            this.btnAlterarGhe.Click += new System.EventHandler(this.btnAlterarGhe_Click);
            // 
            // btnDuplicar
            // 
            this.btnDuplicar.Enabled = false;
            this.btnDuplicar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnDuplicar.Image = global::SWS.Properties.Resources.copiar;
            this.btnDuplicar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnDuplicar.Location = new System.Drawing.Point(246, 3);
            this.btnDuplicar.Name = "btnDuplicar";
            this.btnDuplicar.Size = new System.Drawing.Size(75, 23);
            this.btnDuplicar.TabIndex = 50;
            this.btnDuplicar.TabStop = false;
            this.btnDuplicar.Text = "&Duplicar";
            this.btnDuplicar.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnDuplicar.UseVisualStyleBackColor = true;
            this.btnDuplicar.Click += new System.EventHandler(this.btnDuplicar_Click);
            // 
            // flpEstimativa
            // 
            this.flpEstimativa.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.flpEstimativa.Controls.Add(this.btIncluirEstimativa);
            this.flpEstimativa.Controls.Add(this.btExcluirEstimativa);
            this.flpEstimativa.Enabled = false;
            this.flpEstimativa.Location = new System.Drawing.Point(10, 427);
            this.flpEstimativa.Name = "flpEstimativa";
            this.flpEstimativa.Size = new System.Drawing.Size(739, 32);
            this.flpEstimativa.TabIndex = 72;
            // 
            // lblNumeroContrato
            // 
            this.lblNumeroContrato.BackColor = System.Drawing.Color.LightGray;
            this.lblNumeroContrato.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblNumeroContrato.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNumeroContrato.Location = new System.Drawing.Point(10, 133);
            this.lblNumeroContrato.Name = "lblNumeroContrato";
            this.lblNumeroContrato.ReadOnly = true;
            this.lblNumeroContrato.Size = new System.Drawing.Size(162, 21);
            this.lblNumeroContrato.TabIndex = 73;
            this.lblNumeroContrato.TabStop = false;
            this.lblNumeroContrato.Text = "Número do contrato";
            // 
            // blDataInicioContrato
            // 
            this.blDataInicioContrato.BackColor = System.Drawing.Color.LightGray;
            this.blDataInicioContrato.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.blDataInicioContrato.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.blDataInicioContrato.Location = new System.Drawing.Point(10, 153);
            this.blDataInicioContrato.Name = "blDataInicioContrato";
            this.blDataInicioContrato.ReadOnly = true;
            this.blDataInicioContrato.Size = new System.Drawing.Size(162, 21);
            this.blDataInicioContrato.TabIndex = 74;
            this.blDataInicioContrato.TabStop = false;
            this.blDataInicioContrato.Text = "Início do Contrato";
            // 
            // lblTerminoContrato
            // 
            this.lblTerminoContrato.BackColor = System.Drawing.Color.LightGray;
            this.lblTerminoContrato.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblTerminoContrato.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTerminoContrato.Location = new System.Drawing.Point(10, 173);
            this.lblTerminoContrato.Name = "lblTerminoContrato";
            this.lblTerminoContrato.ReadOnly = true;
            this.lblTerminoContrato.Size = new System.Drawing.Size(162, 21);
            this.lblTerminoContrato.TabIndex = 75;
            this.lblTerminoContrato.TabStop = false;
            this.lblTerminoContrato.Text = "Término do Contrato";
            // 
            // textNumeroContrato
            // 
            this.textNumeroContrato.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.textNumeroContrato.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textNumeroContrato.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.textNumeroContrato.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textNumeroContrato.Location = new System.Drawing.Point(171, 133);
            this.textNumeroContrato.MaxLength = 15;
            this.textNumeroContrato.Name = "textNumeroContrato";
            this.textNumeroContrato.Size = new System.Drawing.Size(578, 21);
            this.textNumeroContrato.TabIndex = 6;
            // 
            // textInicioContrato
            // 
            this.textInicioContrato.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.textInicioContrato.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textInicioContrato.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.textInicioContrato.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textInicioContrato.Location = new System.Drawing.Point(171, 153);
            this.textInicioContrato.MaxLength = 15;
            this.textInicioContrato.Name = "textInicioContrato";
            this.textInicioContrato.Size = new System.Drawing.Size(578, 21);
            this.textInicioContrato.TabIndex = 7;
            // 
            // textFimContrato
            // 
            this.textFimContrato.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.textFimContrato.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textFimContrato.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.textFimContrato.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textFimContrato.Location = new System.Drawing.Point(171, 173);
            this.textFimContrato.MaxLength = 15;
            this.textFimContrato.Name = "textFimContrato";
            this.textFimContrato.Size = new System.Drawing.Size(578, 21);
            this.textFimContrato.TabIndex = 8;
            // 
            // dataValidade
            // 
            this.dataValidade.Checked = false;
            this.dataValidade.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dataValidade.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dataValidade.Location = new System.Drawing.Point(292, 113);
            this.dataValidade.Name = "dataValidade";
            this.dataValidade.ShowCheckBox = true;
            this.dataValidade.Size = new System.Drawing.Size(120, 21);
            this.dataValidade.TabIndex = 78;
            // 
            // lblObra
            // 
            this.lblObra.BackColor = System.Drawing.Color.LightGray;
            this.lblObra.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblObra.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblObra.Location = new System.Drawing.Point(10, 193);
            this.lblObra.Name = "lblObra";
            this.lblObra.ReadOnly = true;
            this.lblObra.Size = new System.Drawing.Size(162, 21);
            this.lblObra.TabIndex = 79;
            this.lblObra.TabStop = false;
            this.lblObra.Text = "Nome da Obra";
            // 
            // textObra
            // 
            this.textObra.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.textObra.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textObra.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.textObra.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textObra.Location = new System.Drawing.Point(171, 193);
            this.textObra.MaxLength = 255;
            this.textObra.Name = "textObra";
            this.textObra.Size = new System.Drawing.Size(578, 21);
            this.textObra.TabIndex = 9;
            // 
            // frmPcmsoAvulsoIncluir
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.ClientSize = new System.Drawing.Size(784, 503);
            this.Name = "frmPcmsoAvulsoIncluir";
            this.Text = "INCLUIR PCMSO AVULSO";
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.frmPcmsoAvulsoIncluir_KeyDown);
            this.spForm.Panel1.ResumeLayout(false);
            this.spForm.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.spForm)).EndInit();
            this.spForm.ResumeLayout(false);
            this.pnlForm.ResumeLayout(false);
            this.pnlForm.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.banner)).EndInit();
            this.flpAcao.ResumeLayout(false);
            this.grbEstimativa.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvEstimativa)).EndInit();
            this.grbGhe.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvGhe)).EndInit();
            this.grbSetor.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvSetor)).EndInit();
            this.grbFuncao.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvFuncao)).EndInit();
            this.grbAgente.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvAgente)).EndInit();
            this.grbExame.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvExame)).EndInit();
            this.grbPeriodicidade.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvPeriodicidade)).EndInit();
            this.flpPeriodicidade.ResumeLayout(false);
            this.flpExame.ResumeLayout(false);
            this.flpAgente.ResumeLayout(false);
            this.flpFuncao.ResumeLayout(false);
            this.flpSetor.ResumeLayout(false);
            this.flpGhe.ResumeLayout(false);
            this.flpEstimativa.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        protected System.Windows.Forms.FlowLayoutPanel flpAcao;
        protected System.Windows.Forms.Button btIcluir;
        protected System.Windows.Forms.Button btn_fechar;
        protected System.Windows.Forms.Button btn_pcmso_limpar;
        protected System.Windows.Forms.Button btCoordenador;
        protected System.Windows.Forms.TextBox textCoordenador;
        protected System.Windows.Forms.DateTimePicker dataCriacao;
        protected System.Windows.Forms.TextBox lblDataValidade;
        protected System.Windows.Forms.TextBox lblMedicoCoordenador;
        protected ComboBoxWithBorder cbGrauRisco;
        protected System.Windows.Forms.TextBox lblCliente;
        protected System.Windows.Forms.TextBox lblRisco;
        protected System.Windows.Forms.TextBox textContratada;
        protected System.Windows.Forms.TextBox lblCodigoPCMSO;
        protected System.Windows.Forms.TextBox lblContratada;
        protected System.Windows.Forms.TextBox textCliente;
        protected System.Windows.Forms.Button btContratada;
        protected System.Windows.Forms.MaskedTextBox textCodigo;
        protected System.Windows.Forms.Button btCliente;
        protected System.Windows.Forms.Button btExcluirEstimativa;
        protected System.Windows.Forms.Button btIncluirEstimativa;
        protected System.Windows.Forms.GroupBox grbEstimativa;
        protected System.Windows.Forms.DataGridView dgvEstimativa;
        protected System.Windows.Forms.Button btGheExcluir;
        protected System.Windows.Forms.Button btGheIncluir;
        protected System.Windows.Forms.GroupBox grbGhe;
        protected System.Windows.Forms.DataGridView dgvGhe;
        protected System.Windows.Forms.Button btSetorExcluir;
        protected System.Windows.Forms.Button btSetorIncluir;
        protected System.Windows.Forms.GroupBox grbSetor;
        protected System.Windows.Forms.DataGridView dgvSetor;
        protected System.Windows.Forms.Button btFuncaoExcluir;
        protected System.Windows.Forms.Button btFuncaoIncluir;
        protected System.Windows.Forms.GroupBox grbFuncao;
        protected System.Windows.Forms.DataGridView dgvFuncao;
        protected System.Windows.Forms.Button btAgenteIncluir;
        protected System.Windows.Forms.Button btAgenteExcluir;
        protected System.Windows.Forms.GroupBox grbAgente;
        protected System.Windows.Forms.DataGridView dgvAgente;
        protected System.Windows.Forms.Button btExameIncluir;
        protected System.Windows.Forms.Button btExameExcluir;
        protected System.Windows.Forms.Button btExameAlterar;
        protected System.Windows.Forms.GroupBox grbExame;
        protected System.Windows.Forms.DataGridView dgvExame;
        protected System.Windows.Forms.Button btPeriodicidadeIncluir;
        protected System.Windows.Forms.GroupBox grbPeriodicidade;
        protected System.Windows.Forms.DataGridView dgvPeriodicidade;
        protected System.Windows.Forms.Button btPeriodicidadeExcluir;
        protected System.Windows.Forms.TextBox lblNumeroContrato;
        protected System.Windows.Forms.FlowLayoutPanel flpSetor;
        protected System.Windows.Forms.FlowLayoutPanel flpFuncao;
        protected System.Windows.Forms.FlowLayoutPanel flpAgente;
        protected System.Windows.Forms.FlowLayoutPanel flpExame;
        protected System.Windows.Forms.FlowLayoutPanel flpPeriodicidade;
        protected System.Windows.Forms.TextBox textFimContrato;
        protected System.Windows.Forms.TextBox textInicioContrato;
        protected System.Windows.Forms.TextBox textNumeroContrato;
        protected System.Windows.Forms.TextBox lblTerminoContrato;
        protected System.Windows.Forms.TextBox blDataInicioContrato;
        protected System.Windows.Forms.FlowLayoutPanel flpEstimativa;
        protected System.Windows.Forms.FlowLayoutPanel flpGhe;
        protected System.Windows.Forms.Button btnAlterarGhe;
        protected System.Windows.Forms.Button btnDuplicar;
        protected System.Windows.Forms.DateTimePicker dataValidade;
        protected System.Windows.Forms.TextBox textObra;
        protected System.Windows.Forms.TextBox lblObra;
    }
}
