﻿namespace SWS.View
{
    partial class frmCobrancaDesdobrar
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.flpAcao = new System.Windows.Forms.FlowLayoutPanel();
            this.btnGravar = new System.Windows.Forms.Button();
            this.btnFechar = new System.Windows.Forms.Button();
            this.textDataVencimento = new System.Windows.Forms.TextBox();
            this.lblDataVencimento = new System.Windows.Forms.TextBox();
            this.textDataEmissao = new System.Windows.Forms.TextBox();
            this.lblDataEmissao = new System.Windows.Forms.TextBox();
            this.textNumeroNota = new System.Windows.Forms.TextBox();
            this.textCliente = new System.Windows.Forms.TextBox();
            this.textParcela = new System.Windows.Forms.TextBox();
            this.textDocumento = new System.Windows.Forms.TextBox();
            this.lblCliente = new System.Windows.Forms.TextBox();
            this.lblDocumento = new System.Windows.Forms.TextBox();
            this.lblParcela = new System.Windows.Forms.TextBox();
            this.lblNumeroNota = new System.Windows.Forms.TextBox();
            ((System.ComponentModel.ISupportInitialize)(this.scForm)).BeginInit();
            this.scForm.Panel1.SuspendLayout();
            this.scForm.Panel2.SuspendLayout();
            this.scForm.SuspendLayout();
            this.pnlForm.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.banner)).BeginInit();
            this.flpAcao.SuspendLayout();
            this.SuspendLayout();
            // 
            // scForm
            // 
            // 
            // scForm.Panel1
            // 
            this.scForm.Panel1.Controls.Add(this.flpAcao);
            // 
            // pnlForm
            // 
            this.pnlForm.Controls.Add(this.textDataVencimento);
            this.pnlForm.Controls.Add(this.lblDataVencimento);
            this.pnlForm.Controls.Add(this.textDataEmissao);
            this.pnlForm.Controls.Add(this.lblDataEmissao);
            this.pnlForm.Controls.Add(this.textNumeroNota);
            this.pnlForm.Controls.Add(this.textCliente);
            this.pnlForm.Controls.Add(this.textParcela);
            this.pnlForm.Controls.Add(this.textDocumento);
            this.pnlForm.Controls.Add(this.lblCliente);
            this.pnlForm.Controls.Add(this.lblDocumento);
            this.pnlForm.Controls.Add(this.lblParcela);
            this.pnlForm.Controls.Add(this.lblNumeroNota);
            // 
            // flpAcao
            // 
            this.flpAcao.Controls.Add(this.btnGravar);
            this.flpAcao.Controls.Add(this.btnFechar);
            this.flpAcao.Dock = System.Windows.Forms.DockStyle.Fill;
            this.flpAcao.Location = new System.Drawing.Point(0, 0);
            this.flpAcao.Name = "flpAcao";
            this.flpAcao.Size = new System.Drawing.Size(534, 30);
            this.flpAcao.TabIndex = 0;
            // 
            // btnGravar
            // 
            this.btnGravar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnGravar.Image = global::SWS.Properties.Resources.Gravar;
            this.btnGravar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnGravar.Location = new System.Drawing.Point(3, 3);
            this.btnGravar.Name = "btnGravar";
            this.btnGravar.Size = new System.Drawing.Size(71, 23);
            this.btnGravar.TabIndex = 6;
            this.btnGravar.Text = "&Gravar";
            this.btnGravar.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnGravar.UseVisualStyleBackColor = true;
            // 
            // btnFechar
            // 
            this.btnFechar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnFechar.Image = global::SWS.Properties.Resources.close;
            this.btnFechar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnFechar.Location = new System.Drawing.Point(80, 3);
            this.btnFechar.Name = "btnFechar";
            this.btnFechar.Size = new System.Drawing.Size(71, 23);
            this.btnFechar.TabIndex = 7;
            this.btnFechar.Text = "&Fechar";
            this.btnFechar.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnFechar.UseVisualStyleBackColor = true;
            // 
            // textDataVencimento
            // 
            this.textDataVencimento.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.textDataVencimento.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textDataVencimento.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textDataVencimento.Location = new System.Drawing.Point(143, 107);
            this.textDataVencimento.Name = "textDataVencimento";
            this.textDataVencimento.ReadOnly = true;
            this.textDataVencimento.Size = new System.Drawing.Size(364, 21);
            this.textDataVencimento.TabIndex = 61;
            this.textDataVencimento.TabStop = false;
            // 
            // lblDataVencimento
            // 
            this.lblDataVencimento.BackColor = System.Drawing.Color.LightGray;
            this.lblDataVencimento.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblDataVencimento.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDataVencimento.Location = new System.Drawing.Point(8, 107);
            this.lblDataVencimento.Name = "lblDataVencimento";
            this.lblDataVencimento.ReadOnly = true;
            this.lblDataVencimento.Size = new System.Drawing.Size(136, 21);
            this.lblDataVencimento.TabIndex = 60;
            this.lblDataVencimento.TabStop = false;
            this.lblDataVencimento.Text = "Data de Vencimento";
            // 
            // textDataEmissao
            // 
            this.textDataEmissao.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.textDataEmissao.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textDataEmissao.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textDataEmissao.Location = new System.Drawing.Point(143, 87);
            this.textDataEmissao.Name = "textDataEmissao";
            this.textDataEmissao.ReadOnly = true;
            this.textDataEmissao.Size = new System.Drawing.Size(364, 21);
            this.textDataEmissao.TabIndex = 59;
            this.textDataEmissao.TabStop = false;
            // 
            // lblDataEmissao
            // 
            this.lblDataEmissao.BackColor = System.Drawing.Color.LightGray;
            this.lblDataEmissao.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblDataEmissao.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDataEmissao.Location = new System.Drawing.Point(8, 87);
            this.lblDataEmissao.Name = "lblDataEmissao";
            this.lblDataEmissao.ReadOnly = true;
            this.lblDataEmissao.Size = new System.Drawing.Size(136, 21);
            this.lblDataEmissao.TabIndex = 58;
            this.lblDataEmissao.TabStop = false;
            this.lblDataEmissao.Text = "Data de Emissão";
            // 
            // textNumeroNota
            // 
            this.textNumeroNota.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.textNumeroNota.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textNumeroNota.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textNumeroNota.Location = new System.Drawing.Point(143, 7);
            this.textNumeroNota.Name = "textNumeroNota";
            this.textNumeroNota.ReadOnly = true;
            this.textNumeroNota.Size = new System.Drawing.Size(364, 21);
            this.textNumeroNota.TabIndex = 57;
            this.textNumeroNota.TabStop = false;
            // 
            // textCliente
            // 
            this.textCliente.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.textCliente.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textCliente.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textCliente.Location = new System.Drawing.Point(143, 67);
            this.textCliente.Name = "textCliente";
            this.textCliente.ReadOnly = true;
            this.textCliente.Size = new System.Drawing.Size(364, 21);
            this.textCliente.TabIndex = 56;
            this.textCliente.TabStop = false;
            // 
            // textParcela
            // 
            this.textParcela.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.textParcela.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textParcela.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textParcela.Location = new System.Drawing.Point(143, 47);
            this.textParcela.Name = "textParcela";
            this.textParcela.ReadOnly = true;
            this.textParcela.Size = new System.Drawing.Size(364, 21);
            this.textParcela.TabIndex = 54;
            this.textParcela.TabStop = false;
            // 
            // textDocumento
            // 
            this.textDocumento.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.textDocumento.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textDocumento.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textDocumento.Location = new System.Drawing.Point(143, 27);
            this.textDocumento.Name = "textDocumento";
            this.textDocumento.ReadOnly = true;
            this.textDocumento.Size = new System.Drawing.Size(364, 21);
            this.textDocumento.TabIndex = 53;
            this.textDocumento.TabStop = false;
            // 
            // lblCliente
            // 
            this.lblCliente.BackColor = System.Drawing.Color.LightGray;
            this.lblCliente.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblCliente.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCliente.Location = new System.Drawing.Point(8, 67);
            this.lblCliente.Name = "lblCliente";
            this.lblCliente.ReadOnly = true;
            this.lblCliente.Size = new System.Drawing.Size(136, 21);
            this.lblCliente.TabIndex = 52;
            this.lblCliente.TabStop = false;
            this.lblCliente.Text = "Cliente";
            // 
            // lblDocumento
            // 
            this.lblDocumento.BackColor = System.Drawing.Color.LightGray;
            this.lblDocumento.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblDocumento.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDocumento.Location = new System.Drawing.Point(8, 27);
            this.lblDocumento.Name = "lblDocumento";
            this.lblDocumento.ReadOnly = true;
            this.lblDocumento.Size = new System.Drawing.Size(136, 21);
            this.lblDocumento.TabIndex = 50;
            this.lblDocumento.TabStop = false;
            this.lblDocumento.Text = "Documento";
            // 
            // lblParcela
            // 
            this.lblParcela.BackColor = System.Drawing.Color.LightGray;
            this.lblParcela.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblParcela.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblParcela.Location = new System.Drawing.Point(8, 47);
            this.lblParcela.Name = "lblParcela";
            this.lblParcela.ReadOnly = true;
            this.lblParcela.Size = new System.Drawing.Size(136, 21);
            this.lblParcela.TabIndex = 49;
            this.lblParcela.TabStop = false;
            this.lblParcela.Text = "Parcela";
            // 
            // lblNumeroNota
            // 
            this.lblNumeroNota.BackColor = System.Drawing.Color.LightGray;
            this.lblNumeroNota.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblNumeroNota.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNumeroNota.Location = new System.Drawing.Point(8, 7);
            this.lblNumeroNota.Name = "lblNumeroNota";
            this.lblNumeroNota.ReadOnly = true;
            this.lblNumeroNota.Size = new System.Drawing.Size(136, 21);
            this.lblNumeroNota.TabIndex = 48;
            this.lblNumeroNota.TabStop = false;
            this.lblNumeroNota.Text = "Número da NF";
            // 
            // frmCobrancaDesdobrar
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(534, 361);
            this.Name = "frmCobrancaDesdobrar";
            this.Text = "DESDOBRAR COBRANÇA";
            this.scForm.Panel1.ResumeLayout(false);
            this.scForm.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.scForm)).EndInit();
            this.scForm.ResumeLayout(false);
            this.pnlForm.ResumeLayout(false);
            this.pnlForm.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.banner)).EndInit();
            this.flpAcao.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.FlowLayoutPanel flpAcao;
        private System.Windows.Forms.Button btnGravar;
        private System.Windows.Forms.Button btnFechar;
        private System.Windows.Forms.TextBox textDataVencimento;
        private System.Windows.Forms.TextBox lblDataVencimento;
        private System.Windows.Forms.TextBox textDataEmissao;
        private System.Windows.Forms.TextBox lblDataEmissao;
        private System.Windows.Forms.TextBox textNumeroNota;
        private System.Windows.Forms.TextBox textCliente;
        private System.Windows.Forms.TextBox textParcela;
        private System.Windows.Forms.TextBox textDocumento;
        private System.Windows.Forms.TextBox lblCliente;
        private System.Windows.Forms.TextBox lblDocumento;
        private System.Windows.Forms.TextBox lblParcela;
        private System.Windows.Forms.TextBox lblNumeroNota;
    }
}