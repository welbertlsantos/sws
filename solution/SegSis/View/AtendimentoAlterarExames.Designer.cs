﻿namespace SWS.View
{
    partial class frmAtendimentoAlterarExames
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            this.flpAcao = new System.Windows.Forms.FlowLayoutPanel();
            this.btnConfirmar = new System.Windows.Forms.Button();
            this.btnCancelar = new System.Windows.Forms.Button();
            this.grbExamesCancelados = new System.Windows.Forms.GroupBox();
            this.dgvExamesCancelados = new System.Windows.Forms.DataGridView();
            this.grbExamesPcmso = new System.Windows.Forms.GroupBox();
            this.dgvExamesPcmso = new System.Windows.Forms.DataGridView();
            ((System.ComponentModel.ISupportInitialize)(this.spForm)).BeginInit();
            this.spForm.Panel1.SuspendLayout();
            this.spForm.Panel2.SuspendLayout();
            this.spForm.SuspendLayout();
            this.pnlForm.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.banner)).BeginInit();
            this.flpAcao.SuspendLayout();
            this.grbExamesCancelados.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvExamesCancelados)).BeginInit();
            this.grbExamesPcmso.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvExamesPcmso)).BeginInit();
            this.SuspendLayout();
            // 
            // spForm
            // 
            // 
            // spForm.Panel1
            // 
            this.spForm.Panel1.Controls.Add(this.flpAcao);
            // 
            // pnlForm
            // 
            this.pnlForm.Controls.Add(this.grbExamesPcmso);
            this.pnlForm.Controls.Add(this.grbExamesCancelados);
            this.pnlForm.Size = new System.Drawing.Size(764, 495);
            // 
            // flpAcao
            // 
            this.flpAcao.Controls.Add(this.btnConfirmar);
            this.flpAcao.Controls.Add(this.btnCancelar);
            this.flpAcao.Dock = System.Windows.Forms.DockStyle.Fill;
            this.flpAcao.Location = new System.Drawing.Point(0, 0);
            this.flpAcao.Name = "flpAcao";
            this.flpAcao.Size = new System.Drawing.Size(784, 35);
            this.flpAcao.TabIndex = 0;
            // 
            // btnConfirmar
            // 
            this.btnConfirmar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnConfirmar.Image = global::SWS.Properties.Resources.fechar_ico;
            this.btnConfirmar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnConfirmar.Location = new System.Drawing.Point(3, 3);
            this.btnConfirmar.Name = "btnConfirmar";
            this.btnConfirmar.Size = new System.Drawing.Size(75, 23);
            this.btnConfirmar.TabIndex = 0;
            this.btnConfirmar.Text = "Confirmar";
            this.btnConfirmar.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnConfirmar.UseVisualStyleBackColor = true;
            this.btnConfirmar.Click += new System.EventHandler(this.btnConfirmar_Click);
            // 
            // btnCancelar
            // 
            this.btnCancelar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnCancelar.Image = global::SWS.Properties.Resources.close;
            this.btnCancelar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnCancelar.Location = new System.Drawing.Point(84, 3);
            this.btnCancelar.Name = "btnCancelar";
            this.btnCancelar.Size = new System.Drawing.Size(75, 23);
            this.btnCancelar.TabIndex = 1;
            this.btnCancelar.Text = "Cancelar";
            this.btnCancelar.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnCancelar.UseVisualStyleBackColor = true;
            this.btnCancelar.Click += new System.EventHandler(this.btnCancelar_Click);
            // 
            // grbExamesCancelados
            // 
            this.grbExamesCancelados.Controls.Add(this.dgvExamesCancelados);
            this.grbExamesCancelados.Location = new System.Drawing.Point(9, 8);
            this.grbExamesCancelados.Name = "grbExamesCancelados";
            this.grbExamesCancelados.Size = new System.Drawing.Size(749, 220);
            this.grbExamesCancelados.TabIndex = 0;
            this.grbExamesCancelados.TabStop = false;
            this.grbExamesCancelados.Text = "Exames que serão cancelados";
            // 
            // dgvExamesCancelados
            // 
            this.dgvExamesCancelados.AllowUserToAddRows = false;
            this.dgvExamesCancelados.AllowUserToDeleteRows = false;
            this.dgvExamesCancelados.AllowUserToOrderColumns = true;
            this.dgvExamesCancelados.AllowUserToResizeRows = false;
            dataGridViewCellStyle3.BackColor = System.Drawing.Color.Gainsboro;
            this.dgvExamesCancelados.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle3;
            this.dgvExamesCancelados.BackgroundColor = System.Drawing.Color.White;
            this.dgvExamesCancelados.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle4.BackColor = System.Drawing.Color.Silver;
            dataGridViewCellStyle4.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle4.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle4.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle4.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle4.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvExamesCancelados.DefaultCellStyle = dataGridViewCellStyle4;
            this.dgvExamesCancelados.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvExamesCancelados.Location = new System.Drawing.Point(3, 16);
            this.dgvExamesCancelados.Name = "dgvExamesCancelados";
            this.dgvExamesCancelados.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvExamesCancelados.Size = new System.Drawing.Size(743, 201);
            this.dgvExamesCancelados.TabIndex = 1;
            this.dgvExamesCancelados.TabStop = false;
            // 
            // grbExamesPcmso
            // 
            this.grbExamesPcmso.Controls.Add(this.dgvExamesPcmso);
            this.grbExamesPcmso.Location = new System.Drawing.Point(9, 234);
            this.grbExamesPcmso.Name = "grbExamesPcmso";
            this.grbExamesPcmso.Size = new System.Drawing.Size(749, 218);
            this.grbExamesPcmso.TabIndex = 1;
            this.grbExamesPcmso.TabStop = false;
            this.grbExamesPcmso.Text = "Exames que serão incluídos";
            // 
            // dgvExamesPcmso
            // 
            this.dgvExamesPcmso.AllowUserToAddRows = false;
            this.dgvExamesPcmso.AllowUserToDeleteRows = false;
            this.dgvExamesPcmso.AllowUserToOrderColumns = true;
            this.dgvExamesPcmso.AllowUserToResizeRows = false;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.Gainsboro;
            this.dgvExamesPcmso.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle1;
            this.dgvExamesPcmso.BackgroundColor = System.Drawing.Color.White;
            this.dgvExamesPcmso.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.Color.Silver;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvExamesPcmso.DefaultCellStyle = dataGridViewCellStyle2;
            this.dgvExamesPcmso.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvExamesPcmso.Location = new System.Drawing.Point(3, 16);
            this.dgvExamesPcmso.Name = "dgvExamesPcmso";
            this.dgvExamesPcmso.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvExamesPcmso.Size = new System.Drawing.Size(743, 199);
            this.dgvExamesPcmso.TabIndex = 0;
            this.dgvExamesPcmso.TabStop = false;
            // 
            // frmAtendimentoAlterarExames
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(784, 561);
            this.Name = "frmAtendimentoAlterarExames";
            this.Text = "ALTERAÇÃO NOS EXAMES DO ATENDIMENTO";
            this.spForm.Panel1.ResumeLayout(false);
            this.spForm.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.spForm)).EndInit();
            this.spForm.ResumeLayout(false);
            this.pnlForm.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.banner)).EndInit();
            this.flpAcao.ResumeLayout(false);
            this.grbExamesCancelados.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvExamesCancelados)).EndInit();
            this.grbExamesPcmso.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvExamesPcmso)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.FlowLayoutPanel flpAcao;
        private System.Windows.Forms.Button btnConfirmar;
        private System.Windows.Forms.Button btnCancelar;
        private System.Windows.Forms.GroupBox grbExamesPcmso;
        private System.Windows.Forms.GroupBox grbExamesCancelados;
        private System.Windows.Forms.DataGridView dgvExamesPcmso;
        private System.Windows.Forms.DataGridView dgvExamesCancelados;
    }
}