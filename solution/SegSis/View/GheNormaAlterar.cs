﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using SWS.Facade;
using SWS.Entidade;
using SWS.View.Resources;

namespace SWS.View
{
    public partial class frm_GheNormaAlterar : BaseFormConsulta
    {
        HashSet<Nota> normaSelecionada = new HashSet<Nota>();

        public HashSet<Nota> NormaSelecionada
        {
            get { return normaSelecionada; }
            set { normaSelecionada = value; }
        }

        private Ghe ghe;

        public frm_GheNormaAlterar(Ghe ghe)
        {
            InitializeComponent();
            this.ghe = ghe;
            validaPermissoes();
        }

        private void validaPermissoes()
        {
            PermissionamentoFacade permissionamentoFacade = PermissionamentoFacade.getInstance();
            btn_novo.Enabled = permissionamentoFacade.hasPermission("NORMA", "INCLUIR");
        }

        private void btn_limpar_Click(object sender, EventArgs e)
        {
            text_descricao.Text = String.Empty;
            grd_norma.Columns.Clear();
        }

        private void btn_buscar_Click(object sender, EventArgs e)
        {
            try
            {
                montaDataGrid();
                text_descricao.Focus();

            }
            catch (Exception Ex)
            {
                MessageBox.Show(Ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void montaDataGrid()
        {

            EstudoFacade pcmsoFacade = EstudoFacade.getInstance();

            DataSet ds = pcmsoFacade.findNotaGheByFilter(new Nota(null, text_descricao.Text.ToUpper(), ApplicationConstants.ATIVO, String.Empty), ghe);

            grd_norma.DataSource = ds.Tables["Normas"].DefaultView;

            grd_norma.Columns[0].HeaderText = "ID";
            grd_norma.Columns[0].Visible = false;

            grd_norma.Columns[1].HeaderText = "Título";
            grd_norma.Columns[1].ReadOnly = true;

            grd_norma.Columns[2].HeaderText = "Situação";
            grd_norma.Columns[2].Visible = false;

            grd_norma.Columns[3].HeaderText = "Conteúdo";
            grd_norma.Columns[3].ReadOnly = true;

            grd_norma.Columns[4].HeaderText = "Selec";
            grd_norma.Columns[4].DisplayIndex = 0;

        }

        private void btn_novo_Click(object sender, EventArgs e)
        {
            frmNotaIncluir formNotaIncluir = new frmNotaIncluir();
            formNotaIncluir.ShowDialog();

            if (formNotaIncluir.Nota != null)
            {
                text_descricao.Text = formNotaIncluir.Nota.Descricao;
                btn_buscar.PerformClick();
            }

        }

        private void btn_fechar_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btn_OK_Click(object sender, EventArgs e)
        {
            try
            {
                foreach (DataGridViewRow dvRow in grd_norma.Rows)
                {

                    if ((Boolean)dvRow.Cells[4].Value == true)
                        normaSelecionada.Add(new Nota((Int64)dvRow.Cells[0].Value, (String)dvRow.Cells[1].Value, (String)dvRow.Cells[2].Value, (String)dvRow.Cells[3].Value));
                }

                this.Close();
            
            }
            catch (Exception Ex)
            {
                MessageBox.Show(Ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
    }
}
