﻿namespace SWS.View
{
    partial class frmPcmsosAvulso
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            this.flpAcao = new System.Windows.Forms.FlowLayoutPanel();
            this.btn_incluir = new System.Windows.Forms.Button();
            this.btn_alterar = new System.Windows.Forms.Button();
            this.btnReplica = new System.Windows.Forms.Button();
            this.btn_pesquisar = new System.Windows.Forms.Button();
            this.btn_limpar = new System.Windows.Forms.Button();
            this.btn_fechar = new System.Windows.Forms.Button();
            this.textCodigo = new System.Windows.Forms.MaskedTextBox();
            this.lblCodigo = new System.Windows.Forms.TextBox();
            this.textCliente = new System.Windows.Forms.TextBox();
            this.btCliente = new System.Windows.Forms.Button();
            this.lblCliente = new System.Windows.Forms.TextBox();
            this.llbContratante = new System.Windows.Forms.TextBox();
            this.textContratante = new System.Windows.Forms.TextBox();
            this.btContrante = new System.Windows.Forms.Button();
            this.grb_pcmso = new System.Windows.Forms.GroupBox();
            this.dgvPcmso = new System.Windows.Forms.DataGridView();
            this.tpPcmsoAvulso = new System.Windows.Forms.ToolTip(this.components);
            this.btnExcluir = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.spForm)).BeginInit();
            this.spForm.Panel1.SuspendLayout();
            this.spForm.Panel2.SuspendLayout();
            this.spForm.SuspendLayout();
            this.pnlForm.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.banner)).BeginInit();
            this.flpAcao.SuspendLayout();
            this.grb_pcmso.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvPcmso)).BeginInit();
            this.SuspendLayout();
            // 
            // spForm
            // 
            // 
            // spForm.Panel1
            // 
            this.spForm.Panel1.Controls.Add(this.flpAcao);
            // 
            // pnlForm
            // 
            this.pnlForm.Controls.Add(this.grb_pcmso);
            this.pnlForm.Controls.Add(this.textContratante);
            this.pnlForm.Controls.Add(this.btContrante);
            this.pnlForm.Controls.Add(this.llbContratante);
            this.pnlForm.Controls.Add(this.lblCliente);
            this.pnlForm.Controls.Add(this.textCliente);
            this.pnlForm.Controls.Add(this.btCliente);
            this.pnlForm.Controls.Add(this.lblCodigo);
            this.pnlForm.Controls.Add(this.textCodigo);
            // 
            // flpAcao
            // 
            this.flpAcao.Controls.Add(this.btn_incluir);
            this.flpAcao.Controls.Add(this.btn_alterar);
            this.flpAcao.Controls.Add(this.btnReplica);
            this.flpAcao.Controls.Add(this.btn_pesquisar);
            this.flpAcao.Controls.Add(this.btn_limpar);
            this.flpAcao.Controls.Add(this.btnExcluir);
            this.flpAcao.Controls.Add(this.btn_fechar);
            this.flpAcao.Dock = System.Windows.Forms.DockStyle.Fill;
            this.flpAcao.Location = new System.Drawing.Point(0, 0);
            this.flpAcao.Name = "flpAcao";
            this.flpAcao.Size = new System.Drawing.Size(784, 35);
            this.flpAcao.TabIndex = 0;
            // 
            // btn_incluir
            // 
            this.btn_incluir.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_incluir.Image = global::SWS.Properties.Resources.icone_mais;
            this.btn_incluir.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btn_incluir.Location = new System.Drawing.Point(3, 3);
            this.btn_incluir.Name = "btn_incluir";
            this.btn_incluir.Size = new System.Drawing.Size(75, 23);
            this.btn_incluir.TabIndex = 25;
            this.btn_incluir.Text = "&Incluir";
            this.btn_incluir.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.tpPcmsoAvulso.SetToolTip(this.btn_incluir, "Incluir um novo PCMSO avulso para o cliente.");
            this.btn_incluir.UseVisualStyleBackColor = true;
            this.btn_incluir.Click += new System.EventHandler(this.btn_incluir_Click);
            // 
            // btn_alterar
            // 
            this.btn_alterar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_alterar.Image = global::SWS.Properties.Resources.Alterar;
            this.btn_alterar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btn_alterar.Location = new System.Drawing.Point(84, 3);
            this.btn_alterar.Name = "btn_alterar";
            this.btn_alterar.Size = new System.Drawing.Size(75, 23);
            this.btn_alterar.TabIndex = 26;
            this.btn_alterar.Text = "&Alterar";
            this.btn_alterar.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.tpPcmsoAvulso.SetToolTip(this.btn_alterar, "Altera um PCMSO já existente.");
            this.btn_alterar.UseVisualStyleBackColor = true;
            this.btn_alterar.Click += new System.EventHandler(this.btn_alterar_Click);
            // 
            // btnReplica
            // 
            this.btnReplica.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnReplica.Image = global::SWS.Properties.Resources.copiar;
            this.btnReplica.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnReplica.Location = new System.Drawing.Point(165, 3);
            this.btnReplica.Name = "btnReplica";
            this.btnReplica.Size = new System.Drawing.Size(75, 23);
            this.btnReplica.TabIndex = 27;
            this.btnReplica.Text = "&Replicar";
            this.btnReplica.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.tpPcmsoAvulso.SetToolTip(this.btnReplica, "Replica o estudo antigo criando um novo PCMSO para o cliente selecionado.");
            this.btnReplica.UseVisualStyleBackColor = true;
            this.btnReplica.Click += new System.EventHandler(this.btnReplica_Click);
            // 
            // btn_pesquisar
            // 
            this.btn_pesquisar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_pesquisar.Image = global::SWS.Properties.Resources.lupa;
            this.btn_pesquisar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btn_pesquisar.Location = new System.Drawing.Point(246, 3);
            this.btn_pesquisar.Name = "btn_pesquisar";
            this.btn_pesquisar.Size = new System.Drawing.Size(64, 23);
            this.btn_pesquisar.TabIndex = 28;
            this.btn_pesquisar.Text = "&Pesquisar";
            this.btn_pesquisar.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.tpPcmsoAvulso.SetToolTip(this.btn_pesquisar, "Pesquisa o PCMSO de acordo com os filtros selecionados em tela.");
            this.btn_pesquisar.UseVisualStyleBackColor = true;
            this.btn_pesquisar.Click += new System.EventHandler(this.btn_pesquisar_Click);
            // 
            // btn_limpar
            // 
            this.btn_limpar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_limpar.Image = global::SWS.Properties.Resources.vassoura;
            this.btn_limpar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btn_limpar.Location = new System.Drawing.Point(316, 3);
            this.btn_limpar.Name = "btn_limpar";
            this.btn_limpar.Size = new System.Drawing.Size(64, 23);
            this.btn_limpar.TabIndex = 29;
            this.btn_limpar.Text = "&Limpar";
            this.btn_limpar.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.tpPcmsoAvulso.SetToolTip(this.btn_limpar, "Limpa as informações preenchidas na tela.");
            this.btn_limpar.UseVisualStyleBackColor = true;
            this.btn_limpar.Click += new System.EventHandler(this.btn_limpar_Click);
            // 
            // btn_fechar
            // 
            this.btn_fechar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_fechar.Image = global::SWS.Properties.Resources.close;
            this.btn_fechar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btn_fechar.Location = new System.Drawing.Point(467, 3);
            this.btn_fechar.Name = "btn_fechar";
            this.btn_fechar.Size = new System.Drawing.Size(75, 23);
            this.btn_fechar.TabIndex = 27;
            this.btn_fechar.TabStop = false;
            this.btn_fechar.Text = "Fec&har";
            this.btn_fechar.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btn_fechar.UseVisualStyleBackColor = true;
            this.btn_fechar.Click += new System.EventHandler(this.btn_fechar_Click);
            // 
            // textCodigo
            // 
            this.textCodigo.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textCodigo.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textCodigo.ForeColor = System.Drawing.Color.Blue;
            this.textCodigo.Location = new System.Drawing.Point(158, 13);
            this.textCodigo.Mask = "0000,00,000000/00";
            this.textCodigo.Name = "textCodigo";
            this.textCodigo.PromptChar = ' ';
            this.textCodigo.Size = new System.Drawing.Size(581, 21);
            this.textCodigo.TabIndex = 3;
            // 
            // lblCodigo
            // 
            this.lblCodigo.BackColor = System.Drawing.Color.Silver;
            this.lblCodigo.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblCodigo.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCodigo.Location = new System.Drawing.Point(3, 13);
            this.lblCodigo.Name = "lblCodigo";
            this.lblCodigo.ReadOnly = true;
            this.lblCodigo.Size = new System.Drawing.Size(156, 21);
            this.lblCodigo.TabIndex = 4;
            this.lblCodigo.TabStop = false;
            this.lblCodigo.Text = "Código PCMSO";
            // 
            // textCliente
            // 
            this.textCliente.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.textCliente.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textCliente.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textCliente.Location = new System.Drawing.Point(158, 33);
            this.textCliente.MaxLength = 100;
            this.textCliente.Name = "textCliente";
            this.textCliente.ReadOnly = true;
            this.textCliente.Size = new System.Drawing.Size(549, 21);
            this.textCliente.TabIndex = 6;
            this.textCliente.TabStop = false;
            // 
            // btCliente
            // 
            this.btCliente.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btCliente.Image = global::SWS.Properties.Resources.busca;
            this.btCliente.Location = new System.Drawing.Point(704, 33);
            this.btCliente.Name = "btCliente";
            this.btCliente.Size = new System.Drawing.Size(35, 21);
            this.btCliente.TabIndex = 5;
            this.tpPcmsoAvulso.SetToolTip(this.btCliente, "Seleciono o cliente para pesquisar O PCMSO.");
            this.btCliente.UseVisualStyleBackColor = true;
            this.btCliente.Click += new System.EventHandler(this.btCliente_Click);
            // 
            // lblCliente
            // 
            this.lblCliente.BackColor = System.Drawing.Color.Silver;
            this.lblCliente.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblCliente.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCliente.Location = new System.Drawing.Point(3, 33);
            this.lblCliente.Name = "lblCliente";
            this.lblCliente.ReadOnly = true;
            this.lblCliente.Size = new System.Drawing.Size(156, 21);
            this.lblCliente.TabIndex = 7;
            this.lblCliente.TabStop = false;
            this.lblCliente.Text = "Cliente";
            // 
            // llbContratante
            // 
            this.llbContratante.BackColor = System.Drawing.Color.Silver;
            this.llbContratante.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.llbContratante.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.llbContratante.Location = new System.Drawing.Point(3, 53);
            this.llbContratante.Name = "llbContratante";
            this.llbContratante.ReadOnly = true;
            this.llbContratante.Size = new System.Drawing.Size(156, 21);
            this.llbContratante.TabIndex = 8;
            this.llbContratante.TabStop = false;
            this.llbContratante.Text = "Contratante";
            // 
            // textContratante
            // 
            this.textContratante.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.textContratante.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textContratante.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textContratante.Location = new System.Drawing.Point(158, 53);
            this.textContratante.MaxLength = 100;
            this.textContratante.Name = "textContratante";
            this.textContratante.ReadOnly = true;
            this.textContratante.Size = new System.Drawing.Size(549, 21);
            this.textContratante.TabIndex = 10;
            this.textContratante.TabStop = false;
            // 
            // btContrante
            // 
            this.btContrante.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btContrante.Image = global::SWS.Properties.Resources.busca;
            this.btContrante.Location = new System.Drawing.Point(704, 53);
            this.btContrante.Name = "btContrante";
            this.btContrante.Size = new System.Drawing.Size(35, 21);
            this.btContrante.TabIndex = 9;
            this.tpPcmsoAvulso.SetToolTip(this.btContrante, "Selecione o cliente contratante ou a unidade para pesquisar os PCMSOs.");
            this.btContrante.UseVisualStyleBackColor = true;
            this.btContrante.Click += new System.EventHandler(this.btContrante_Click);
            // 
            // grb_pcmso
            // 
            this.grb_pcmso.Controls.Add(this.dgvPcmso);
            this.grb_pcmso.Location = new System.Drawing.Point(3, 80);
            this.grb_pcmso.Name = "grb_pcmso";
            this.grb_pcmso.Size = new System.Drawing.Size(738, 372);
            this.grb_pcmso.TabIndex = 17;
            this.grb_pcmso.TabStop = false;
            this.grb_pcmso.Text = "PCMSO";
            // 
            // dgvPcmso
            // 
            this.dgvPcmso.AllowUserToAddRows = false;
            this.dgvPcmso.AllowUserToDeleteRows = false;
            this.dgvPcmso.AllowUserToOrderColumns = true;
            this.dgvPcmso.AllowUserToResizeRows = false;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.Gainsboro;
            this.dgvPcmso.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle1;
            this.dgvPcmso.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this.dgvPcmso.BackgroundColor = System.Drawing.Color.White;
            this.dgvPcmso.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.Color.LightGray;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvPcmso.DefaultCellStyle = dataGridViewCellStyle2;
            this.dgvPcmso.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvPcmso.Location = new System.Drawing.Point(3, 16);
            this.dgvPcmso.MultiSelect = false;
            this.dgvPcmso.Name = "dgvPcmso";
            this.dgvPcmso.ReadOnly = true;
            this.dgvPcmso.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvPcmso.Size = new System.Drawing.Size(732, 353);
            this.dgvPcmso.TabIndex = 14;
            this.dgvPcmso.CellMouseDoubleClick += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.dgvPcmso_CellMouseDoubleClick);
            // 
            // btnExcluir
            // 
            this.btnExcluir.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnExcluir.Image = global::SWS.Properties.Resources.lixeira;
            this.btnExcluir.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnExcluir.Location = new System.Drawing.Point(386, 3);
            this.btnExcluir.Name = "btnExcluir";
            this.btnExcluir.Size = new System.Drawing.Size(75, 23);
            this.btnExcluir.TabIndex = 30;
            this.btnExcluir.Text = "&Excluir";
            this.btnExcluir.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnExcluir.UseVisualStyleBackColor = true;
            this.btnExcluir.Click += new System.EventHandler(this.btnExcluir_Click);
            // 
            // frmPcmsosAvulso
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.ClientSize = new System.Drawing.Size(784, 561);
            this.Name = "frmPcmsosAvulso";
            this.Text = "PESQUISAR PCMSO AVULSO";
            this.spForm.Panel1.ResumeLayout(false);
            this.spForm.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.spForm)).EndInit();
            this.spForm.ResumeLayout(false);
            this.pnlForm.ResumeLayout(false);
            this.pnlForm.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.banner)).EndInit();
            this.flpAcao.ResumeLayout(false);
            this.grb_pcmso.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvPcmso)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.FlowLayoutPanel flpAcao;
        private System.Windows.Forms.Button btn_fechar;
        private System.Windows.Forms.Button btn_pesquisar;
        private System.Windows.Forms.Button btn_alterar;
        private System.Windows.Forms.Button btn_limpar;
        private System.Windows.Forms.Button btn_incluir;
        private System.Windows.Forms.TextBox lblCodigo;
        private System.Windows.Forms.MaskedTextBox textCodigo;
        private System.Windows.Forms.TextBox llbContratante;
        private System.Windows.Forms.TextBox lblCliente;
        public System.Windows.Forms.TextBox textCliente;
        private System.Windows.Forms.Button btCliente;
        public System.Windows.Forms.TextBox textContratante;
        private System.Windows.Forms.Button btContrante;
        private System.Windows.Forms.GroupBox grb_pcmso;
        private System.Windows.Forms.DataGridView dgvPcmso;
        private System.Windows.Forms.ToolTip tpPcmsoAvulso;
        private System.Windows.Forms.Button btnReplica;
        private System.Windows.Forms.Button btnExcluir;
    }
}
