﻿namespace SWS.View
{
    partial class frmPcmsoAvulsoGheIncluir
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.flpAcao = new System.Windows.Forms.FlowLayoutPanel();
            this.btn_salvar = new System.Windows.Forms.Button();
            this.btn_fechar = new System.Windows.Forms.Button();
            this.lblExposto = new System.Windows.Forms.TextBox();
            this.textExposto = new System.Windows.Forms.TextBox();
            this.lblDescricao = new System.Windows.Forms.TextBox();
            this.textDescricao = new System.Windows.Forms.TextBox();
            this.IncluirGheErrorProvider = new System.Windows.Forms.ErrorProvider(this.components);
            this.lblNumeroPo = new System.Windows.Forms.TextBox();
            this.textNumeroPo = new System.Windows.Forms.TextBox();
            ((System.ComponentModel.ISupportInitialize)(this.scForm)).BeginInit();
            this.scForm.Panel1.SuspendLayout();
            this.scForm.Panel2.SuspendLayout();
            this.scForm.SuspendLayout();
            this.pnlForm.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.banner)).BeginInit();
            this.flpAcao.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.IncluirGheErrorProvider)).BeginInit();
            this.SuspendLayout();
            // 
            // scForm
            // 
            // 
            // scForm.Panel1
            // 
            this.scForm.Panel1.Controls.Add(this.flpAcao);
            // 
            // pnlForm
            // 
            this.pnlForm.Controls.Add(this.lblNumeroPo);
            this.pnlForm.Controls.Add(this.textNumeroPo);
            this.pnlForm.Controls.Add(this.lblExposto);
            this.pnlForm.Controls.Add(this.textExposto);
            this.pnlForm.Controls.Add(this.lblDescricao);
            this.pnlForm.Controls.Add(this.textDescricao);
            // 
            // flpAcao
            // 
            this.flpAcao.Controls.Add(this.btn_salvar);
            this.flpAcao.Controls.Add(this.btn_fechar);
            this.flpAcao.Dock = System.Windows.Forms.DockStyle.Fill;
            this.flpAcao.Location = new System.Drawing.Point(0, 0);
            this.flpAcao.Name = "flpAcao";
            this.flpAcao.Size = new System.Drawing.Size(534, 30);
            this.flpAcao.TabIndex = 0;
            // 
            // btn_salvar
            // 
            this.btn_salvar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_salvar.Image = global::SWS.Properties.Resources.Gravar;
            this.btn_salvar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btn_salvar.Location = new System.Drawing.Point(3, 3);
            this.btn_salvar.Name = "btn_salvar";
            this.btn_salvar.Size = new System.Drawing.Size(75, 23);
            this.btn_salvar.TabIndex = 5;
            this.btn_salvar.Text = "&Salvar";
            this.btn_salvar.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btn_salvar.UseVisualStyleBackColor = true;
            this.btn_salvar.Click += new System.EventHandler(this.btn_salvar_Click);
            // 
            // btn_fechar
            // 
            this.btn_fechar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_fechar.Image = global::SWS.Properties.Resources.close;
            this.btn_fechar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btn_fechar.Location = new System.Drawing.Point(84, 3);
            this.btn_fechar.Name = "btn_fechar";
            this.btn_fechar.Size = new System.Drawing.Size(75, 23);
            this.btn_fechar.TabIndex = 6;
            this.btn_fechar.Text = "&Fechar";
            this.btn_fechar.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btn_fechar.UseVisualStyleBackColor = true;
            this.btn_fechar.Click += new System.EventHandler(this.btn_fechar_Click);
            // 
            // lblExposto
            // 
            this.lblExposto.BackColor = System.Drawing.Color.LightGray;
            this.lblExposto.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblExposto.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblExposto.Location = new System.Drawing.Point(3, 27);
            this.lblExposto.Name = "lblExposto";
            this.lblExposto.ReadOnly = true;
            this.lblExposto.Size = new System.Drawing.Size(127, 21);
            this.lblExposto.TabIndex = 11;
            this.lblExposto.TabStop = false;
            this.lblExposto.Text = "Número de Expostos";
            // 
            // textExposto
            // 
            this.textExposto.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textExposto.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.textExposto.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textExposto.Location = new System.Drawing.Point(129, 27);
            this.textExposto.MaxLength = 8;
            this.textExposto.Name = "textExposto";
            this.textExposto.Size = new System.Drawing.Size(371, 21);
            this.textExposto.TabIndex = 2;
            this.textExposto.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.textExposto_KeyPress);
            // 
            // lblDescricao
            // 
            this.lblDescricao.BackColor = System.Drawing.Color.LightGray;
            this.lblDescricao.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblDescricao.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDescricao.Location = new System.Drawing.Point(3, 7);
            this.lblDescricao.Name = "lblDescricao";
            this.lblDescricao.ReadOnly = true;
            this.lblDescricao.Size = new System.Drawing.Size(127, 21);
            this.lblDescricao.TabIndex = 10;
            this.lblDescricao.TabStop = false;
            this.lblDescricao.Text = "Descrição";
            // 
            // textDescricao
            // 
            this.textDescricao.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textDescricao.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.textDescricao.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textDescricao.Location = new System.Drawing.Point(129, 7);
            this.textDescricao.MaxLength = 100;
            this.textDescricao.Name = "textDescricao";
            this.textDescricao.Size = new System.Drawing.Size(371, 21);
            this.textDescricao.TabIndex = 1;
            // 
            // IncluirGheErrorProvider
            // 
            this.IncluirGheErrorProvider.ContainerControl = this;
            // 
            // lblNumeroPo
            // 
            this.lblNumeroPo.BackColor = System.Drawing.Color.LightGray;
            this.lblNumeroPo.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblNumeroPo.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNumeroPo.Location = new System.Drawing.Point(3, 47);
            this.lblNumeroPo.Name = "lblNumeroPo";
            this.lblNumeroPo.ReadOnly = true;
            this.lblNumeroPo.Size = new System.Drawing.Size(127, 21);
            this.lblNumeroPo.TabIndex = 13;
            this.lblNumeroPo.TabStop = false;
            this.lblNumeroPo.Text = "Número Po";
            // 
            // textNumeroPo
            // 
            this.textNumeroPo.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textNumeroPo.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.textNumeroPo.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textNumeroPo.Location = new System.Drawing.Point(129, 47);
            this.textNumeroPo.MaxLength = 15;
            this.textNumeroPo.Name = "textNumeroPo";
            this.textNumeroPo.Size = new System.Drawing.Size(371, 21);
            this.textNumeroPo.TabIndex = 3;
            // 
            // frmPcmsoGhe
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.ClientSize = new System.Drawing.Size(534, 361);
            this.Name = "frmPcmsoGhe";
            this.Text = "INCLUIR GHE";
            this.scForm.Panel1.ResumeLayout(false);
            this.scForm.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.scForm)).EndInit();
            this.scForm.ResumeLayout(false);
            this.pnlForm.ResumeLayout(false);
            this.pnlForm.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.banner)).EndInit();
            this.flpAcao.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.IncluirGheErrorProvider)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        protected System.Windows.Forms.FlowLayoutPanel flpAcao;
        protected System.Windows.Forms.Button btn_fechar;
        protected System.Windows.Forms.Button btn_salvar;
        protected System.Windows.Forms.TextBox lblExposto;
        protected System.Windows.Forms.TextBox textExposto;
        protected System.Windows.Forms.TextBox lblDescricao;
        protected System.Windows.Forms.TextBox textDescricao;
        protected System.Windows.Forms.ErrorProvider IncluirGheErrorProvider;
        protected System.Windows.Forms.TextBox lblNumeroPo;
        protected System.Windows.Forms.TextBox textNumeroPo;
    }
}
