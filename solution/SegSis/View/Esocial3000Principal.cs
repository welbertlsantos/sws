﻿using SWS.Entidade;
using SWS.Facade;
using SWS.Resources;
using SWS.View.Resources;
using SWS.View.ViewHelper;
using SWS.ViewHelper;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml;
using System.Xml.Serialization;

namespace SWS.View
{
    public partial class frmEsocial3000Principal : frmTemplate
    {

        private string diretorioTemporario;
        private string diretorioExportacao;
        private string[] arquivos;
        private Cliente cliente;
        private List<LoteEsocialAso> lista2220Cancelados = new List<LoteEsocialAso>();
        private List<LoteEsocialMonitoramento> lista2240Cancelados = new List<LoteEsocialMonitoramento>();

        public frmEsocial3000Principal()
        {
            InitializeComponent();
            ComboHelper.tipoEvento(this.cbTipoEvento);
            validaPermissoes();
        }

        private void btnFechar_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btPesquisar_Click(object sender, EventArgs e)
        {
            try
            {
                this.Cursor = Cursors.WaitCursor;
                if (validaCampoTela())
                {
                    EsocialFacade esocialFacade = EsocialFacade.getInstance();
                    if (string.Equals(((SelectItem)cbTipoEvento.SelectedItem).Valor, ApplicationConstants.TIPO_2220))
                    {

                        lista2220Cancelados = esocialFacade.findAllCancelados2220ByFilter(this.cliente, Convert.ToDateTime(periodoInicial.Text + " 00:00:00"), Convert.ToDateTime(periodoFinal.Text + " 23:59:59"));
                        lista2240Cancelados.Clear();
                        montaDataGrid2220();


                    }
                    else
                    {
                        lista2240Cancelados = esocialFacade.findAllCancelados2240ByFilter(this.cliente, Convert.ToDateTime(periodoInicial.Text + " 00:00:00"), Convert.ToDateTime(periodoFinal.Text + " 23:59:59"));
                        lista2220Cancelados.Clear();
                        montaDataGrid2240();
                    }

                }

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }

        }

        private void montaDataGrid2240()
        {
            try
            {
                this.Cursor = Cursors.WaitCursor;
                this.dgvLoteCancelado.Columns.Clear();
                this.dgvLoteCancelado.ColumnCount = 12;
                this.dgvLoteCancelado.Columns[0].HeaderText = "IdEsocialLoteMonitoramento";
                this.dgvLoteCancelado.Columns[0].Visible = false;
                this.dgvLoteCancelado.Columns[1].HeaderText = "IdEsocialLote";
                this.dgvLoteCancelado.Columns[1].Visible = false;
                this.dgvLoteCancelado.Columns[2].HeaderText = "idMonitoramento";
                this.dgvLoteCancelado.Columns[2].Visible = false;
                this.dgvLoteCancelado.Columns[3].HeaderText = "Código Lote";
                this.dgvLoteCancelado.Columns[4].HeaderText = "Número Recibo";
                this.dgvLoteCancelado.Columns[5].HeaderText = "Data Cancelamento";
                this.dgvLoteCancelado.Columns[6].HeaderText = "Usuário Cancelou";
                this.dgvLoteCancelado.Columns[7].HeaderText = "Período Monitoramento";
                this.dgvLoteCancelado.Columns[8].HeaderText = "Funcionário";
                this.dgvLoteCancelado.Columns[9].HeaderText = "CPF";
                this.dgvLoteCancelado.Columns[10].HeaderText = "Cliente";
                this.dgvLoteCancelado.Columns[11].HeaderText = "CNPJ";

                this.lista2240Cancelados.ForEach(delegate(LoteEsocialMonitoramento lote)
                {
                    this.dgvLoteCancelado.Rows.Add(
                        lote.Id,
                        lote.LoteEsocial.Id,
                        lote.Monitoramento.Id,
                        lote.LoteEsocial.codigo,
                        lote.NRecibo,
                        String.Format("{0:dd/MM/yyyy}", lote.DataCancelamento),
                        lote.UsuarioCancelou,
                        String.Format("{0:MM/yyyyy}", lote.LoteEsocial.periodo),
                        lote.Monitoramento.ClienteFuncaoFuncionario.Funcionario.Nome,
                        ValidaCampoHelper.FormataCpf(lote.Monitoramento.ClienteFuncaoFuncionario.Funcionario.Cpf),
                        lote.Monitoramento.Cliente.RazaoSocial,
                        ValidaCampoHelper.FormataCnpj(lote.Monitoramento.Cliente.Cnpj)
                        );
                });
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }

        private void montaDataGrid2220()
        {
            try
            {
                this.Cursor = Cursors.WaitCursor;
                dgvLoteCancelado.Columns.Clear();
                dgvLoteCancelado.ColumnCount = 13;

                dgvLoteCancelado.Columns[0].HeaderText = "idLoteASo";
                dgvLoteCancelado.Columns[0].Visible = false;
                dgvLoteCancelado.Columns[1].HeaderText = "idAso";
                dgvLoteCancelado.Columns[1].Visible = false;
                dgvLoteCancelado.Columns[2].HeaderText = "idLote";
                dgvLoteCancelado.Columns[2].Visible = false;
                dgvLoteCancelado.Columns[3].HeaderText = "CodigoLote";
                dgvLoteCancelado.Columns[4].HeaderText = "Numero Recibo";
                dgvLoteCancelado.Columns[5].HeaderText = "Data de Cancelamento";
                dgvLoteCancelado.Columns[6].HeaderText = "Usuario cancelou";
                dgvLoteCancelado.Columns[7].HeaderText = "Codigo Atendimento";
                dgvLoteCancelado.Columns[8].HeaderText = "Funcionário";
                dgvLoteCancelado.Columns[9].HeaderText = "CPF";
                dgvLoteCancelado.Columns[10].HeaderText = "Tipo Atendimento";
                dgvLoteCancelado.Columns[11].HeaderText = "Cliente";
                dgvLoteCancelado.Columns[12].HeaderText = "CNPJ";

                this.lista2220Cancelados.ForEach(delegate(LoteEsocialAso loteAso)
                {
                    this.dgvLoteCancelado.Rows.Add(
                        loteAso.Id,
                        loteAso.idAso,
                        loteAso.idLoteEsocialAso,
                        loteAso.loteEsocial.codigo,
                        loteAso.nrebido,
                        String.Format("{0:dd/MM/yyyy}", loteAso.dataCancelamento),
                        loteAso.usuarioCancelou,
                        loteAso.aso.Codigo,
                        loteAso.aso.Funcionario.Nome,
                        ValidaCampoHelper.FormataCpf(loteAso.aso.Funcionario.Cpf),
                        loteAso.aso.Periodicidade.Descricao,
                        loteAso.aso.Cliente.RazaoSocial,
                        ValidaCampoHelper.FormataCnpj(loteAso.aso.Cliente.Cnpj));
                });
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }

        private void btnCliente_Click(object sender, EventArgs e)
        {
            try
            {
                frmClienteSelecionar selecionarCliente = new frmClienteSelecionar(null, null, false, null, null, null, null, null, true);
                selecionarCliente.ShowDialog();

                if (selecionarCliente.Cliente != null)
                {
                    this.cliente = selecionarCliente.Cliente;
                    textCliente.Text = this.cliente.RazaoSocial + " - " + (this.cliente.tipoCliente() == 1 ? ValidaCampoHelper.FormataCnpj(this.cliente.Cnpj) : ValidaCampoHelper.FormataCpf(this.cliente.Cnpj));
                       
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private bool validaCampoTela()
        {
            bool retorno = true;
            try
            {
                if (periodoInicial.Checked && !periodoFinal.Checked)
                    throw new Exception("Você deve selecionar o período final da pesquisa.");

                if (!periodoInicial.Checked & periodoFinal.Checked)
                    throw new Exception("Você deve selecionar o período inicial da pesquisa.");

                if (!periodoInicial.Checked && !periodoFinal.Checked)
                    throw new Exception("Você deve selecionar algum período para pesquisa.");

                if (Convert.ToDateTime(periodoInicial.Text) > Convert.ToDateTime(periodoFinal.Text))
                    throw new Exception("O período inicial não pode ser maior que o período final");

                if (cliente == null)
                    throw new Exception("Você deve selecionar um cliente para a pesquisa");

                    
            }
            catch (Exception ex)
            {
                retorno = false;
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

            return retorno;

        }

        private void btnExcluirCliente_Click(object sender, EventArgs e)
        {
            try
            {
                if (this.cliente == null)
                    throw new Exception("Você deve selecionar primeiro o cliente");

                this.cliente = null;
                this.textCliente.Text = string.Empty;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }

        private void btIncluir_Click(object sender, EventArgs e)
        {
            frmEsocial3000 incluirEvento3000 = new frmEsocial3000();
            incluirEvento3000.ShowDialog();

            /* preenchendo informações do que foi selecionado na inclusão */

            if (incluirEvento3000.Gerado == true)
            {
                this.cliente = incluirEvento3000.Cliente;
                textCliente.Text = cliente.RazaoSocial + " - " + ValidaCampoHelper.FormataCpf(cliente.Cnpj);
                cbTipoEvento.SelectedValue = incluirEvento3000.Tipo;
                periodoInicial.Checked = true;
                periodoFinal.Checked = true;
                int ano = DateTime.Now.Year;
                int mes = DateTime.Now.Month;
                int dia = DateTime.Now.Day;
                periodoInicial.Value = Convert.ToDateTime(new DateTime(ano, mes, dia));
                periodoFinal.Value = Convert.ToDateTime(new DateTime(ano, mes, dia).AddHours(23).AddMinutes(59).AddSeconds(59));
                btPesquisar.PerformClick();
            }
            
        }

        public string GetTemporaryDirectory()
        {
            string tempFolder = Path.GetTempFileName();
            File.Delete(tempFolder);
            Directory.CreateDirectory(tempFolder);

            return tempFolder;
        }

        private void btExportar_Click(object sender, EventArgs e)
        {
            try
            {
                this.Cursor = Cursors.WaitCursor;

                if (this.dgvLoteCancelado.CurrentRow == null)
                    throw new Exception("Selecione um registro para exportar ");

                if (string.Equals(((SelectItem)cbTipoEvento.SelectedItem).Nome, ApplicationConstants.TIPO_2220))
                {
                    gerar2220();

                }
                else
                {
                    gerar2240();
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }

        }

        private T Deserialize<T>(string input) where T : class
        {
            XmlSerializer ser = new XmlSerializer(typeof(T));

            XmlWriterSettings settings = new XmlWriterSettings()
            {
                Encoding = new UnicodeEncoding(false, false),
                Indent = false,
                OmitXmlDeclaration = false
            };

            using (StringReader sr = new StringReader(input))
            {
                return (T)ser.Deserialize(sr);
            }
        }

        private string Serialize<T>(T ObjectToSerialize)
        {
            XmlSerializer xmlSerializer = new XmlSerializer(ObjectToSerialize.GetType());
            var xmlnsEmpty = new XmlSerializerNamespaces();
            xmlnsEmpty.Add("", "http://www.esocial.gov.br/schema/evt/evtExclusao/v_S_01_02_00");

            using (Utf8StringWriter textWriter = new Utf8StringWriter())
            {
                xmlSerializer.Serialize(textWriter, ObjectToSerialize, xmlnsEmpty);
                return textWriter.ToString().Replace("utf-8", "UTF-8");
            }
        }
        
        public sealed class Utf8StringWriter : StringWriter
        {
            public override Encoding Encoding
            {
                get
                {
                    return Encoding.UTF8;
                }
            }
        }

        public void gerar2220()
        {
            try
            {
                this.Cursor = Cursors.WaitCursor;

                if (this.dgvLoteCancelado.CurrentRow == null)
                    throw new Exception("Selecione um registro para exportar. ");

                SWS.Modelo.modelo3000.ESocial esocial = new Modelo.modelo3000.ESocial();
                SWS.Modelo.modelo3000.EvtExclusao evtExclusao = new Modelo.modelo3000.EvtExclusao();
                SWS.Modelo.modelo3000.IdeEvento ideEvento = new Modelo.modelo3000.IdeEvento();
                SWS.Modelo.modelo3000.IdeEmpregador ideEmpregador = new Modelo.modelo3000.IdeEmpregador();
                SWS.Modelo.modelo3000.InfoExclusao infoExclusao = new Modelo.modelo3000.InfoExclusao();
                SWS.Modelo.modelo3000.IdeTrabalhador ideTrabalhador = new Modelo.modelo3000.IdeTrabalhador();

                diretorioTemporario = GetTemporaryDirectory();

                EsocialFacade esocialFacade = EsocialFacade.getInstance();
                PermissionamentoFacade permissionamentoFacade = PermissionamentoFacade.getInstance();

                DateTime sequenciaData = DateTime.Now;

                LoteEsocialAso loteEsocialAso = loteEsocialAso = esocialFacade.findLoteEsocialAsoById((long)dgvLoteCancelado.CurrentRow.Cells[0].Value);
                string evento = "S-2220";
                string nomeArquivo = nomeArquivo = loteEsocialAso.aso.Funcionario.Nome.Replace(" ", "_") + "_" + loteEsocialAso.loteEsocial.codigo + ".xml";

                try
                {

                    using (StreamWriter writer = new StreamWriter(diretorioTemporario + "//" + nomeArquivo, false, Encoding.UTF8))
                    {
                        /* ideEvento */
                        ideEvento.tpAmb = "1";
                        ideEvento.procEmi = "1";

                        Dictionary<string, string> configuracao = new Dictionary<string, string>();
                        string versao;
                        configuracao = permissionamentoFacade.findAllConfiguracao();
                        configuracao.TryGetValue(ConfigurationConstans.VERSAO_SISTEMA, out versao);

                        ideEvento.verProc = versao;

                        /* ideEmpregador */
                        ideEmpregador.TpInsc = loteEsocialAso.aso.Cliente.tipoCliente().ToString();
                        ideEmpregador.NrInsc = loteEsocialAso.aso.Cliente.Cnpj.Length == 14
                                        ? loteEsocialAso.aso.Cliente.Cnpj.Substring(0, 8)
                                        : loteEsocialAso.aso.Cliente.Cnpj;

                        /* ideTrabalhador */
                        ideTrabalhador.cpfTrab = loteEsocialAso.aso.Funcionario.Cpf;

                        /* infoExclusao */
                        infoExclusao.tpEvento = evento;
                        infoExclusao.nrRecEvt = loteEsocialAso.nrebido;
                        infoExclusao.ideTrabalhador = ideTrabalhador;

                        /* evtExclusao */
                        evtExclusao.ideEmpregador = ideEmpregador;
                        evtExclusao.ideEvento = ideEvento;
                        evtExclusao.infoExclusao = infoExclusao;

                        StringBuilder stringId = new StringBuilder();
                        stringId.Append("ID");
                        stringId.Append(loteEsocialAso.aso.Cliente.tipoCliente().ToString());

                        if (loteEsocialAso.aso.Cliente.Cnpj.Length == 14)
                        {
                            stringId.Append(loteEsocialAso.aso.Cliente.Cnpj.Substring(0, 8).PadRight(14, '0'));
                        }
                        else
                        {
                            stringId.Append(loteEsocialAso.aso.Cliente.Cnpj.PadRight(14, '0'));
                        }


                        stringId.Append(String.Format("{0:yyyyMMddHHmmss}", sequenciaData));
                        stringId.Append("1".PadLeft(5, '0'));

                        evtExclusao.Id = stringId.ToString();

                        /* esocial */
                        esocial.evtExclusao = evtExclusao;
                        String xml = Serialize(esocial);

                        writer.Write(xml);
                        writer.Flush();
                    }

                    /* gerar arquivo de saída do zip */
                    FolderBrowserDialog fbd1 = new FolderBrowserDialog();
                    fbd1.Description = "Selecione uma pasta onde o arquivo de exportação do E-Social evento 3300 será criado.";
                    fbd1.RootFolder = Environment.SpecialFolder.MyComputer;
                    fbd1.ShowNewFolderButton = true;

                    DialogResult dr = fbd1.ShowDialog();
                    //Exibe a caixa de diálogo
                    if (dr == System.Windows.Forms.DialogResult.OK)
                    {
                        //Exibe a pasta selecionada
                        diretorioExportacao = fbd1.SelectedPath;
                    }

                    /* lista todos os arquivos do diretorio temporário */
                    arquivos = Directory.GetFiles(diretorioTemporario);

                    /* setando o local de destino dos arquivos */
                    string localDestino = diretorioExportacao + "\\" + loteEsocialAso.aso.Funcionario.Nome.Replace(" ", "_") + "_" + loteEsocialAso.loteEsocial.codigo + ".zip";

                    /* criando arquivo zip */
                    SWS.Helper.FileHelper.criarArquivoZip(arquivos, localDestino);
                    MessageBox.Show("Arquivo gerado com sucesso", "Confirmação", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                catch (Exception ex)
                {
                    MessageBox.Show("Não foi possível salvar o seu arquivo. Informe o erro: " + ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }

        public void gerar2240()
        {
            try
            {
                this.Cursor = Cursors.WaitCursor;

                if (this.dgvLoteCancelado.CurrentRow == null)
                    throw new Exception("Selecione um registro para exportar. ");

                SWS.Modelo.modelo3000.ESocial esocial = new Modelo.modelo3000.ESocial();
                SWS.Modelo.modelo3000.EvtExclusao evtExclusao = new Modelo.modelo3000.EvtExclusao();
                SWS.Modelo.modelo3000.IdeEvento ideEvento = new Modelo.modelo3000.IdeEvento();
                SWS.Modelo.modelo3000.IdeEmpregador ideEmpregador = new Modelo.modelo3000.IdeEmpregador();
                SWS.Modelo.modelo3000.InfoExclusao infoExclusao = new Modelo.modelo3000.InfoExclusao();
                SWS.Modelo.modelo3000.IdeTrabalhador ideTrabalhador = new Modelo.modelo3000.IdeTrabalhador();

                diretorioTemporario = GetTemporaryDirectory();

                EsocialFacade esocialFacade = EsocialFacade.getInstance();
                PermissionamentoFacade permissionamentoFacade = PermissionamentoFacade.getInstance();

                DateTime sequenciaData = DateTime.Now;

                LoteEsocialMonitoramento loteEsocialMonitoramento = esocialFacade.findLoteEsocialMonitoramentoById((long)dgvLoteCancelado.CurrentRow.Cells[0].Value);
                string evento = "S-2240";
                string nomeArquivo = nomeArquivo = loteEsocialMonitoramento.Monitoramento.ClienteFuncaoFuncionario.Funcionario.Nome.Replace(" ", "_") + "_" + loteEsocialMonitoramento.LoteEsocial.codigo + ".xml";

                try
                {

                    using (StreamWriter writer = new StreamWriter(diretorioTemporario + "//" + nomeArquivo, false, Encoding.UTF8))
                    {
                        /* ideEvento */
                        ideEvento.tpAmb = "1";
                        ideEvento.procEmi = "1";

                        Dictionary<string, string> configuracao = new Dictionary<string, string>();
                        string versao;
                        configuracao = permissionamentoFacade.findAllConfiguracao();
                        configuracao.TryGetValue(ConfigurationConstans.VERSAO_SISTEMA, out versao);

                        ideEvento.verProc = versao;

                        /* ideEmpregador */
                        ideEmpregador.TpInsc = loteEsocialMonitoramento.Monitoramento.Cliente.tipoCliente().ToString();
                        ideEmpregador.NrInsc = loteEsocialMonitoramento.Monitoramento.Cliente.Cnpj.Length == 14
                                        ? loteEsocialMonitoramento.Monitoramento.Cliente.Cnpj.Substring(0, 8)
                                        : loteEsocialMonitoramento.Monitoramento.Cliente.Cnpj;

                        /* ideTrabalhador */
                        ideTrabalhador.cpfTrab = loteEsocialMonitoramento.Monitoramento.ClienteFuncaoFuncionario.Funcionario.Cpf;

                        /* infoExclusao */
                        infoExclusao.tpEvento = evento;
                        infoExclusao.nrRecEvt = loteEsocialMonitoramento.NRecibo;
                        infoExclusao.ideTrabalhador = ideTrabalhador;

                        /* evtExclusao */
                        evtExclusao.ideEmpregador = ideEmpregador;
                        evtExclusao.ideEvento = ideEvento;
                        evtExclusao.infoExclusao = infoExclusao;

                        StringBuilder stringId = new StringBuilder();
                        stringId.Append("ID");
                        stringId.Append(loteEsocialMonitoramento.Monitoramento.Cliente.tipoCliente().ToString());

                        if (loteEsocialMonitoramento.Monitoramento.Cliente.Cnpj.Length == 14)
                        {
                            stringId.Append(loteEsocialMonitoramento.Monitoramento.Cliente.Cnpj.Substring(0, 8).PadRight(14, '0'));
                        }
                        else
                        {
                            stringId.Append(loteEsocialMonitoramento.Monitoramento.Cliente.Cnpj.PadRight(14, '0'));
                        }


                        stringId.Append(String.Format("{0:yyyyMMddHHmmss}", sequenciaData));
                        stringId.Append("1".PadLeft(5, '0'));

                        evtExclusao.Id = stringId.ToString();

                        /* esocial */
                        esocial.evtExclusao = evtExclusao;
                        String xml = Serialize(esocial);

                        writer.Write(xml);
                        writer.Flush();
                    }

                    /* gerar arquivo de saída do zip */
                    FolderBrowserDialog fbd1 = new FolderBrowserDialog();
                    fbd1.Description = "Selecione uma pasta onde o arquivo de exportação do E-Social evento 3300 será criado.";
                    fbd1.RootFolder = Environment.SpecialFolder.MyComputer;
                    fbd1.ShowNewFolderButton = true;

                    DialogResult dr = fbd1.ShowDialog();
                    //Exibe a caixa de diálogo
                    if (dr == System.Windows.Forms.DialogResult.OK)
                    {
                        //Exibe a pasta selecionada
                        diretorioExportacao = fbd1.SelectedPath;
                    }

                    /* lista todos os arquivos do diretorio temporário */
                    arquivos = Directory.GetFiles(diretorioTemporario);

                    /* setando o local de destino dos arquivos */
                    string localDestino = diretorioExportacao + "\\" + loteEsocialMonitoramento.Monitoramento.ClienteFuncaoFuncionario.Funcionario.Nome.Replace(" ", "_") + "_" + loteEsocialMonitoramento.LoteEsocial.codigo + ".zip";

                    /* criando arquivo zip */
                    SWS.Helper.FileHelper.criarArquivoZip(arquivos, localDestino);
                    MessageBox.Show("Arquivo gerado com sucesso", "Confirmação", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                catch (Exception ex)
                {
                    MessageBox.Show("Não foi possível salvar o seu arquivo. Informe o erro: " + ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }

        private void frmEsocial3000Principal_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter || e.KeyCode == Keys.Return)
                SendKeys.Send("{TAB}");
        }

        private void validaPermissoes()
        {
            PermissionamentoFacade permissionamentoFacade = PermissionamentoFacade.getInstance();

            btIncluir.Enabled = permissionamentoFacade.hasPermission("ESOCIAL-3000", "INCLUIR");
            btExportar.Enabled = permissionamentoFacade.hasPermission("ESOCIAL-3000", "EXPORTAR");
        }
    }
}
