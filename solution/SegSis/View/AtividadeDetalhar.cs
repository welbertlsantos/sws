﻿using SWS.Entidade;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace SWS.View
{
    public partial class frmAtividadeDetalhar : frmAtividadeIncluir
    {
        public frmAtividadeDetalhar(Atividade atividade) :base()
        {
            InitializeComponent();
            textAtividade.Text = atividade.Descricao;
            btnGravar.Enabled = false;

        }
    }
}
