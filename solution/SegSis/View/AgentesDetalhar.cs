﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using SWS.Entidade;

namespace SWS.View
{
    public partial class frmAgentesDetalhar : frmAgentesIncluir
    {
        public frmAgentesDetalhar(Agente agente) :base()
        {
            InitializeComponent();

            this.agente = agente;

            /* montando informação da tela */
            textDescricao.Text = agente.Descricao;
            textDescricao.ReadOnly = true;

            textTrajetoria.Text = agente.Trajetoria;
            textTrajetoria.ReadOnly = true;

            textDano.Text = agente.Dano;
            textDano.ReadOnly = true;
            
            textLimite.Text = agente.Limite;
            textLimite.ReadOnly = true;
            
            cbRisco.SelectedValue = agente.Risco.Id;
            cbRisco.Text = agente.Risco.Descricao;
            cbRisco.Enabled = false;

            btGravar.Enabled = false;
            btLimpar.Enabled = false;

            textIntensidade.Text = agente.Intensidade;
            textIntensidade.ReadOnly = true;
            textTecnica.Text = agente.Tecnica;
            textTecnica.ReadOnly = true;

            textCodigoEsocial.Text = agente.CodigoEsocial;
            textCodigoEsocial.ReadOnly = true;
        }
    }
}
