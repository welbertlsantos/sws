﻿namespace SWS.View
{
    partial class frmClienteFuncaoFuncionarioAlterar
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.flpAcao = new System.Windows.Forms.FlowLayoutPanel();
            this.btConfirmar = new System.Windows.Forms.Button();
            this.btFechar = new System.Windows.Forms.Button();
            this.cbEstagiario = new SWS.ComboBoxWithBorder();
            this.cbVinculo = new SWS.ComboBoxWithBorder();
            this.textMatricula = new System.Windows.Forms.TextBox();
            this.lblEstagiario = new System.Windows.Forms.TextBox();
            this.lblVinculo = new System.Windows.Forms.TextBox();
            this.lblMatricula = new System.Windows.Forms.TextBox();
            this.cbBrPdh = new SWS.ComboBoxWithBorder();
            this.lblBrPdh = new System.Windows.Forms.TextBox();
            this.textRegime = new System.Windows.Forms.TextBox();
            this.lblRegime = new System.Windows.Forms.TextBox();
            this.dataAdmissao = new System.Windows.Forms.DateTimePicker();
            this.lblFuncionario = new System.Windows.Forms.Label();
            this.lblDataAdmissao = new System.Windows.Forms.TextBox();
            this.textFantasia = new System.Windows.Forms.TextBox();
            this.lblCnpj = new System.Windows.Forms.TextBox();
            this.textCnpj = new System.Windows.Forms.MaskedTextBox();
            this.lblFantasia = new System.Windows.Forms.TextBox();
            this.lblCliente = new System.Windows.Forms.Label();
            this.textRazaoSocial = new System.Windows.Forms.TextBox();
            this.lblRazaoSocial = new System.Windows.Forms.TextBox();
            this.lblFuncao = new System.Windows.Forms.TextBox();
            this.textFuncao = new System.Windows.Forms.TextBox();
            this.btnFuncao = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.scForm)).BeginInit();
            this.scForm.Panel1.SuspendLayout();
            this.scForm.Panel2.SuspendLayout();
            this.scForm.SuspendLayout();
            this.pnlForm.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.banner)).BeginInit();
            this.flpAcao.SuspendLayout();
            this.SuspendLayout();
            // 
            // scForm
            // 
            // 
            // scForm.Panel1
            // 
            this.scForm.Panel1.Controls.Add(this.flpAcao);
            // 
            // pnlForm
            // 
            this.pnlForm.Controls.Add(this.btnFuncao);
            this.pnlForm.Controls.Add(this.textFuncao);
            this.pnlForm.Controls.Add(this.lblFuncao);
            this.pnlForm.Controls.Add(this.cbEstagiario);
            this.pnlForm.Controls.Add(this.cbVinculo);
            this.pnlForm.Controls.Add(this.textMatricula);
            this.pnlForm.Controls.Add(this.lblEstagiario);
            this.pnlForm.Controls.Add(this.lblVinculo);
            this.pnlForm.Controls.Add(this.lblMatricula);
            this.pnlForm.Controls.Add(this.cbBrPdh);
            this.pnlForm.Controls.Add(this.lblBrPdh);
            this.pnlForm.Controls.Add(this.textRegime);
            this.pnlForm.Controls.Add(this.lblRegime);
            this.pnlForm.Controls.Add(this.dataAdmissao);
            this.pnlForm.Controls.Add(this.lblFuncionario);
            this.pnlForm.Controls.Add(this.lblDataAdmissao);
            this.pnlForm.Controls.Add(this.textFantasia);
            this.pnlForm.Controls.Add(this.lblCnpj);
            this.pnlForm.Controls.Add(this.textCnpj);
            this.pnlForm.Controls.Add(this.lblFantasia);
            this.pnlForm.Controls.Add(this.lblCliente);
            this.pnlForm.Controls.Add(this.textRazaoSocial);
            this.pnlForm.Controls.Add(this.lblRazaoSocial);
            // 
            // flpAcao
            // 
            this.flpAcao.Controls.Add(this.btConfirmar);
            this.flpAcao.Controls.Add(this.btFechar);
            this.flpAcao.Dock = System.Windows.Forms.DockStyle.Fill;
            this.flpAcao.Location = new System.Drawing.Point(0, 0);
            this.flpAcao.Name = "flpAcao";
            this.flpAcao.Size = new System.Drawing.Size(534, 30);
            this.flpAcao.TabIndex = 1;
            // 
            // btConfirmar
            // 
            this.btConfirmar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btConfirmar.Image = global::SWS.Properties.Resources.Gravar;
            this.btConfirmar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btConfirmar.Location = new System.Drawing.Point(3, 3);
            this.btConfirmar.Name = "btConfirmar";
            this.btConfirmar.Size = new System.Drawing.Size(75, 23);
            this.btConfirmar.TabIndex = 6;
            this.btConfirmar.Text = "Gravar";
            this.btConfirmar.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btConfirmar.UseVisualStyleBackColor = true;
            this.btConfirmar.Click += new System.EventHandler(this.btConfirmar_Click);
            // 
            // btFechar
            // 
            this.btFechar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btFechar.Image = global::SWS.Properties.Resources.close;
            this.btFechar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btFechar.Location = new System.Drawing.Point(84, 3);
            this.btFechar.Name = "btFechar";
            this.btFechar.Size = new System.Drawing.Size(75, 23);
            this.btFechar.TabIndex = 7;
            this.btFechar.Text = "Fechar";
            this.btFechar.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btFechar.UseVisualStyleBackColor = true;
            this.btFechar.Click += new System.EventHandler(this.btFechar_Click);
            // 
            // cbEstagiario
            // 
            this.cbEstagiario.BackColor = System.Drawing.Color.LightGray;
            this.cbEstagiario.BorderColor = System.Drawing.Color.Gray;
            this.cbEstagiario.BorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid;
            this.cbEstagiario.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cbEstagiario.FormattingEnabled = true;
            this.cbEstagiario.Location = new System.Drawing.Point(214, 236);
            this.cbEstagiario.Name = "cbEstagiario";
            this.cbEstagiario.Size = new System.Drawing.Size(288, 21);
            this.cbEstagiario.TabIndex = 66;
            // 
            // cbVinculo
            // 
            this.cbVinculo.BackColor = System.Drawing.Color.LightGray;
            this.cbVinculo.BorderColor = System.Drawing.Color.Gray;
            this.cbVinculo.BorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid;
            this.cbVinculo.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cbVinculo.FormattingEnabled = true;
            this.cbVinculo.Location = new System.Drawing.Point(214, 216);
            this.cbVinculo.Name = "cbVinculo";
            this.cbVinculo.Size = new System.Drawing.Size(288, 21);
            this.cbVinculo.TabIndex = 65;
            // 
            // textMatricula
            // 
            this.textMatricula.BackColor = System.Drawing.Color.White;
            this.textMatricula.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textMatricula.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.textMatricula.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textMatricula.Location = new System.Drawing.Point(214, 196);
            this.textMatricula.MaxLength = 30;
            this.textMatricula.Name = "textMatricula";
            this.textMatricula.Size = new System.Drawing.Size(288, 21);
            this.textMatricula.TabIndex = 64;
            // 
            // lblEstagiario
            // 
            this.lblEstagiario.BackColor = System.Drawing.Color.LightGray;
            this.lblEstagiario.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblEstagiario.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblEstagiario.Location = new System.Drawing.Point(15, 236);
            this.lblEstagiario.Name = "lblEstagiario";
            this.lblEstagiario.ReadOnly = true;
            this.lblEstagiario.Size = new System.Drawing.Size(200, 21);
            this.lblEstagiario.TabIndex = 80;
            this.lblEstagiario.TabStop = false;
            this.lblEstagiario.Text = "É estagiário?";
            // 
            // lblVinculo
            // 
            this.lblVinculo.BackColor = System.Drawing.Color.LightGray;
            this.lblVinculo.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblVinculo.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblVinculo.Location = new System.Drawing.Point(15, 216);
            this.lblVinculo.Name = "lblVinculo";
            this.lblVinculo.ReadOnly = true;
            this.lblVinculo.Size = new System.Drawing.Size(200, 21);
            this.lblVinculo.TabIndex = 79;
            this.lblVinculo.TabStop = false;
            this.lblVinculo.Text = "Possui vínculo?";
            // 
            // lblMatricula
            // 
            this.lblMatricula.BackColor = System.Drawing.Color.LightGray;
            this.lblMatricula.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblMatricula.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblMatricula.Location = new System.Drawing.Point(15, 196);
            this.lblMatricula.Name = "lblMatricula";
            this.lblMatricula.ReadOnly = true;
            this.lblMatricula.Size = new System.Drawing.Size(200, 21);
            this.lblMatricula.TabIndex = 78;
            this.lblMatricula.TabStop = false;
            this.lblMatricula.Text = "Matrícula";
            // 
            // cbBrPdh
            // 
            this.cbBrPdh.BackColor = System.Drawing.Color.LightGray;
            this.cbBrPdh.BorderColor = System.Drawing.Color.Gray;
            this.cbBrPdh.BorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid;
            this.cbBrPdh.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cbBrPdh.FormattingEnabled = true;
            this.cbBrPdh.Location = new System.Drawing.Point(214, 176);
            this.cbBrPdh.Name = "cbBrPdh";
            this.cbBrPdh.Size = new System.Drawing.Size(288, 21);
            this.cbBrPdh.TabIndex = 63;
            // 
            // lblBrPdh
            // 
            this.lblBrPdh.BackColor = System.Drawing.Color.LightGray;
            this.lblBrPdh.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblBrPdh.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblBrPdh.Location = new System.Drawing.Point(15, 176);
            this.lblBrPdh.Name = "lblBrPdh";
            this.lblBrPdh.ReadOnly = true;
            this.lblBrPdh.Size = new System.Drawing.Size(200, 21);
            this.lblBrPdh.TabIndex = 77;
            this.lblBrPdh.TabStop = false;
            this.lblBrPdh.Text = "Informação extra PPP";
            // 
            // textRegime
            // 
            this.textRegime.BackColor = System.Drawing.Color.White;
            this.textRegime.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textRegime.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.textRegime.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textRegime.Location = new System.Drawing.Point(214, 156);
            this.textRegime.MaxLength = 15;
            this.textRegime.Name = "textRegime";
            this.textRegime.Size = new System.Drawing.Size(288, 21);
            this.textRegime.TabIndex = 62;
            // 
            // lblRegime
            // 
            this.lblRegime.BackColor = System.Drawing.Color.LightGray;
            this.lblRegime.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblRegime.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblRegime.Location = new System.Drawing.Point(15, 156);
            this.lblRegime.Name = "lblRegime";
            this.lblRegime.ReadOnly = true;
            this.lblRegime.Size = new System.Drawing.Size(200, 21);
            this.lblRegime.TabIndex = 76;
            this.lblRegime.TabStop = false;
            this.lblRegime.Text = "Regime de revezamento";
            // 
            // dataAdmissao
            // 
            this.dataAdmissao.CalendarFont = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dataAdmissao.Checked = false;
            this.dataAdmissao.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dataAdmissao.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dataAdmissao.Location = new System.Drawing.Point(214, 136);
            this.dataAdmissao.Name = "dataAdmissao";
            this.dataAdmissao.ShowCheckBox = true;
            this.dataAdmissao.Size = new System.Drawing.Size(288, 21);
            this.dataAdmissao.TabIndex = 61;
            // 
            // lblFuncionario
            // 
            this.lblFuncionario.AutoSize = true;
            this.lblFuncionario.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblFuncionario.ForeColor = System.Drawing.Color.Red;
            this.lblFuncionario.Location = new System.Drawing.Point(12, 118);
            this.lblFuncionario.Name = "lblFuncionario";
            this.lblFuncionario.Size = new System.Drawing.Size(72, 15);
            this.lblFuncionario.TabIndex = 75;
            this.lblFuncionario.Text = "Funcionário";
            // 
            // lblDataAdmissao
            // 
            this.lblDataAdmissao.BackColor = System.Drawing.Color.LightGray;
            this.lblDataAdmissao.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblDataAdmissao.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDataAdmissao.Location = new System.Drawing.Point(15, 136);
            this.lblDataAdmissao.Name = "lblDataAdmissao";
            this.lblDataAdmissao.ReadOnly = true;
            this.lblDataAdmissao.Size = new System.Drawing.Size(200, 21);
            this.lblDataAdmissao.TabIndex = 74;
            this.lblDataAdmissao.TabStop = false;
            this.lblDataAdmissao.Text = "Data de Admissão";
            // 
            // textFantasia
            // 
            this.textFantasia.BackColor = System.Drawing.Color.White;
            this.textFantasia.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textFantasia.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.textFantasia.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textFantasia.ForeColor = System.Drawing.Color.Blue;
            this.textFantasia.Location = new System.Drawing.Point(214, 46);
            this.textFantasia.Name = "textFantasia";
            this.textFantasia.ReadOnly = true;
            this.textFantasia.Size = new System.Drawing.Size(288, 21);
            this.textFantasia.TabIndex = 70;
            this.textFantasia.TabStop = false;
            // 
            // lblCnpj
            // 
            this.lblCnpj.BackColor = System.Drawing.Color.LightGray;
            this.lblCnpj.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblCnpj.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCnpj.Location = new System.Drawing.Point(15, 66);
            this.lblCnpj.Name = "lblCnpj";
            this.lblCnpj.ReadOnly = true;
            this.lblCnpj.Size = new System.Drawing.Size(200, 21);
            this.lblCnpj.TabIndex = 72;
            this.lblCnpj.TabStop = false;
            this.lblCnpj.Text = "CNPJ";
            // 
            // textCnpj
            // 
            this.textCnpj.BackColor = System.Drawing.Color.White;
            this.textCnpj.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textCnpj.ForeColor = System.Drawing.Color.Blue;
            this.textCnpj.Location = new System.Drawing.Point(214, 66);
            this.textCnpj.Mask = "99,999,999/9999-99";
            this.textCnpj.Name = "textCnpj";
            this.textCnpj.PromptChar = ' ';
            this.textCnpj.ReadOnly = true;
            this.textCnpj.Size = new System.Drawing.Size(288, 21);
            this.textCnpj.TabIndex = 73;
            this.textCnpj.TabStop = false;
            // 
            // lblFantasia
            // 
            this.lblFantasia.BackColor = System.Drawing.Color.LightGray;
            this.lblFantasia.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblFantasia.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblFantasia.Location = new System.Drawing.Point(15, 46);
            this.lblFantasia.Name = "lblFantasia";
            this.lblFantasia.ReadOnly = true;
            this.lblFantasia.Size = new System.Drawing.Size(200, 21);
            this.lblFantasia.TabIndex = 71;
            this.lblFantasia.TabStop = false;
            this.lblFantasia.Text = "Nome de Fantasia";
            // 
            // lblCliente
            // 
            this.lblCliente.AutoSize = true;
            this.lblCliente.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCliente.ForeColor = System.Drawing.Color.Red;
            this.lblCliente.Location = new System.Drawing.Point(12, 8);
            this.lblCliente.Name = "lblCliente";
            this.lblCliente.Size = new System.Drawing.Size(45, 15);
            this.lblCliente.TabIndex = 69;
            this.lblCliente.Text = "Cliente";
            // 
            // textRazaoSocial
            // 
            this.textRazaoSocial.BackColor = System.Drawing.Color.White;
            this.textRazaoSocial.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textRazaoSocial.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.textRazaoSocial.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textRazaoSocial.ForeColor = System.Drawing.Color.Blue;
            this.textRazaoSocial.Location = new System.Drawing.Point(214, 26);
            this.textRazaoSocial.Name = "textRazaoSocial";
            this.textRazaoSocial.ReadOnly = true;
            this.textRazaoSocial.Size = new System.Drawing.Size(288, 21);
            this.textRazaoSocial.TabIndex = 67;
            this.textRazaoSocial.TabStop = false;
            // 
            // lblRazaoSocial
            // 
            this.lblRazaoSocial.BackColor = System.Drawing.Color.LightGray;
            this.lblRazaoSocial.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblRazaoSocial.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblRazaoSocial.Location = new System.Drawing.Point(15, 26);
            this.lblRazaoSocial.Name = "lblRazaoSocial";
            this.lblRazaoSocial.ReadOnly = true;
            this.lblRazaoSocial.Size = new System.Drawing.Size(200, 21);
            this.lblRazaoSocial.TabIndex = 68;
            this.lblRazaoSocial.TabStop = false;
            this.lblRazaoSocial.Text = "Razão Social";
            // 
            // lblFuncao
            // 
            this.lblFuncao.BackColor = System.Drawing.Color.LightGray;
            this.lblFuncao.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblFuncao.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblFuncao.Location = new System.Drawing.Point(15, 86);
            this.lblFuncao.Name = "lblFuncao";
            this.lblFuncao.ReadOnly = true;
            this.lblFuncao.Size = new System.Drawing.Size(200, 21);
            this.lblFuncao.TabIndex = 81;
            this.lblFuncao.TabStop = false;
            this.lblFuncao.Text = "Função";
            // 
            // textFuncao
            // 
            this.textFuncao.BackColor = System.Drawing.Color.LightGray;
            this.textFuncao.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textFuncao.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.textFuncao.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textFuncao.ForeColor = System.Drawing.Color.Blue;
            this.textFuncao.Location = new System.Drawing.Point(214, 86);
            this.textFuncao.MaxLength = 100;
            this.textFuncao.Name = "textFuncao";
            this.textFuncao.ReadOnly = true;
            this.textFuncao.Size = new System.Drawing.Size(257, 21);
            this.textFuncao.TabIndex = 82;
            this.textFuncao.TabStop = false;
            // 
            // btnFuncao
            // 
            this.btnFuncao.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnFuncao.Image = global::SWS.Properties.Resources.busca;
            this.btnFuncao.Location = new System.Drawing.Point(468, 86);
            this.btnFuncao.Name = "btnFuncao";
            this.btnFuncao.Size = new System.Drawing.Size(34, 21);
            this.btnFuncao.TabIndex = 83;
            this.btnFuncao.UseVisualStyleBackColor = true;
            this.btnFuncao.Click += new System.EventHandler(this.btnFuncao_Click);
            // 
            // frmClienteFuncionarioAlterar
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(534, 361);
            this.Name = "frmClienteFuncionarioAlterar";
            this.Text = "ALTERAR FUNCIONÁRIO";
            this.scForm.Panel1.ResumeLayout(false);
            this.scForm.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.scForm)).EndInit();
            this.scForm.ResumeLayout(false);
            this.pnlForm.ResumeLayout(false);
            this.pnlForm.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.banner)).EndInit();
            this.flpAcao.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        protected System.Windows.Forms.FlowLayoutPanel flpAcao;
        protected System.Windows.Forms.Button btConfirmar;
        protected System.Windows.Forms.Button btFechar;
        protected ComboBoxWithBorder cbEstagiario;
        protected ComboBoxWithBorder cbVinculo;
        protected System.Windows.Forms.TextBox textMatricula;
        protected System.Windows.Forms.TextBox lblEstagiario;
        protected System.Windows.Forms.TextBox lblVinculo;
        protected System.Windows.Forms.TextBox lblMatricula;
        protected ComboBoxWithBorder cbBrPdh;
        protected System.Windows.Forms.TextBox lblBrPdh;
        protected System.Windows.Forms.TextBox textRegime;
        protected System.Windows.Forms.TextBox lblRegime;
        protected System.Windows.Forms.DateTimePicker dataAdmissao;
        protected System.Windows.Forms.Label lblFuncionario;
        protected System.Windows.Forms.TextBox lblDataAdmissao;
        protected System.Windows.Forms.TextBox textFantasia;
        protected System.Windows.Forms.TextBox lblCnpj;
        protected System.Windows.Forms.MaskedTextBox textCnpj;
        protected System.Windows.Forms.TextBox lblFantasia;
        protected System.Windows.Forms.Label lblCliente;
        protected System.Windows.Forms.TextBox textRazaoSocial;
        protected System.Windows.Forms.TextBox lblRazaoSocial;
        private System.Windows.Forms.Button btnFuncao;
        protected System.Windows.Forms.TextBox textFuncao;
        protected System.Windows.Forms.TextBox lblFuncao;
    }
}