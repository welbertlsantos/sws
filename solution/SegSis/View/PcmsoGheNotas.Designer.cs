﻿namespace SWS.View
{
    partial class frm_PcmsoGheNotas
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frm_PcmsoGheNotas));
            this.grbNota = new System.Windows.Forms.GroupBox();
            this.btn_excluir = new System.Windows.Forms.Button();
            this.dgNota = new System.Windows.Forms.DataGridView();
            this.grb_paginacao = new System.Windows.Forms.GroupBox();
            this.btn_fechar = new System.Windows.Forms.Button();
            this.btn_incluir = new System.Windows.Forms.Button();
            this.grbNota.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgNota)).BeginInit();
            this.grb_paginacao.SuspendLayout();
            this.SuspendLayout();
            // 
            // grbNota
            // 
            this.grbNota.Controls.Add(this.dgNota);
            this.grbNota.Location = new System.Drawing.Point(9, 12);
            this.grbNota.Name = "grbNota";
            this.grbNota.Size = new System.Drawing.Size(579, 283);
            this.grbNota.TabIndex = 1;
            this.grbNota.TabStop = false;
            this.grbNota.Text = "Notas";
            // 
            // btn_excluir
            // 
            this.btn_excluir.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_excluir.Image = global::SWS.Properties.Resources.lixeira;
            this.btn_excluir.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btn_excluir.Location = new System.Drawing.Point(93, 301);
            this.btn_excluir.Name = "btn_excluir";
            this.btn_excluir.Size = new System.Drawing.Size(75, 23);
            this.btn_excluir.TabIndex = 4;
            this.btn_excluir.Text = "&Excluir";
            this.btn_excluir.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btn_excluir.UseVisualStyleBackColor = true;
            this.btn_excluir.Click += new System.EventHandler(this.btn_excluir_Click);
            // 
            // dgNota
            // 
            this.dgNota.AllowUserToAddRows = false;
            this.dgNota.AllowUserToDeleteRows = false;
            this.dgNota.AllowUserToOrderColumns = true;
            this.dgNota.AllowUserToResizeRows = false;
            this.dgNota.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this.dgNota.BackgroundColor = System.Drawing.Color.White;
            this.dgNota.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgNota.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgNota.Location = new System.Drawing.Point(3, 16);
            this.dgNota.Name = "dgNota";
            this.dgNota.ReadOnly = true;
            this.dgNota.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgNota.Size = new System.Drawing.Size(573, 264);
            this.dgNota.TabIndex = 3;
            // 
            // grb_paginacao
            // 
            this.grb_paginacao.Controls.Add(this.btn_fechar);
            this.grb_paginacao.Location = new System.Drawing.Point(12, 341);
            this.grb_paginacao.Name = "grb_paginacao";
            this.grb_paginacao.Size = new System.Drawing.Size(576, 47);
            this.grb_paginacao.TabIndex = 2;
            this.grb_paginacao.TabStop = false;
            // 
            // btn_fechar
            // 
            this.btn_fechar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_fechar.Image = global::SWS.Properties.Resources.close;
            this.btn_fechar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btn_fechar.Location = new System.Drawing.Point(6, 18);
            this.btn_fechar.Name = "btn_fechar";
            this.btn_fechar.Size = new System.Drawing.Size(75, 23);
            this.btn_fechar.TabIndex = 7;
            this.btn_fechar.Text = "&Fechar";
            this.btn_fechar.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btn_fechar.UseVisualStyleBackColor = true;
            this.btn_fechar.Click += new System.EventHandler(this.btn_fechar_Click);
            // 
            // btn_incluir
            // 
            this.btn_incluir.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_incluir.Image = global::SWS.Properties.Resources.fechar_ico;
            this.btn_incluir.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btn_incluir.Location = new System.Drawing.Point(12, 301);
            this.btn_incluir.Name = "btn_incluir";
            this.btn_incluir.Size = new System.Drawing.Size(75, 23);
            this.btn_incluir.TabIndex = 6;
            this.btn_incluir.Text = "&Incluir";
            this.btn_incluir.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btn_incluir.UseVisualStyleBackColor = true;
            this.btn_incluir.Click += new System.EventHandler(this.btn_incluir_Click);
            // 
            // frm_PcmsoGheNotas
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(600, 400);
            this.ControlBox = false;
            this.Controls.Add(this.btn_excluir);
            this.Controls.Add(this.btn_incluir);
            this.Controls.Add(this.grb_paginacao);
            this.Controls.Add(this.grbNota);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "frm_PcmsoGheNotas";
            this.Text = "NOTAS NO GHE";
            this.grbNota.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgNota)).EndInit();
            this.grb_paginacao.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox grbNota;
        private System.Windows.Forms.DataGridView dgNota;
        private System.Windows.Forms.GroupBox grb_paginacao;
        private System.Windows.Forms.Button btn_fechar;
        private System.Windows.Forms.Button btn_incluir;
        private System.Windows.Forms.Button btn_excluir;
    }
}