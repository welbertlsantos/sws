﻿using SWS.Entidade;
using SWS.Facade;
using SWS.View.ViewHelper;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace SWS.View
{
    public partial class frmContratosConsulta : frmContratosIncluir
    {
        public frmContratosConsulta(Contrato contrato, Empresa empresa)
        {
            InitializeComponent();
            this.contrato = contrato;

            /* preenchendo informações de tela */

            textCodigo.Text = contrato.CodigoContrato;
            dataElaboracao.Text = contrato.DataElaboracao.ToString();
            dataElaboracao.Enabled = false;
            dataInicio.Text = contrato.DataInicio != null ? contrato.DataInicio.ToString() : string.Empty;
            dataInicio.Enabled = false;
            dataTermino.Text = contrato.DataFim != null ? contrato.DataFim.ToString() : string.Empty;
            dataTermino.Enabled = false;

            this.cliente = contrato.Cliente;
            textCliente.Text = contrato.Cliente.RazaoSocial;
            btnCliente.Enabled = false;
            this.prestador = contrato.Prestador;
            textPrestador.Text = contrato.Prestador != null ? prestador.RazaoSocial : string.Empty;
            btnPrestador.Enabled = false;

            MontaDataGridExame();
            MontaDataGridProduto();

            /* montando condições do contrato */

            cbBloqueio.SelectedValue = contrato.BloqueiaInadimplencia == true ? "true" : "false";
            cbBloqueio.Enabled = false;
            textDiasBloqueio.Text = contrato.DiasInadimplenciaBloqueio == null ? string.Empty : contrato.DiasInadimplenciaBloqueio.ToString();
            textDiasBloqueio.ReadOnly = true;
            
            cbValorMensal.SelectedValue = contrato.GeraMensalidade == true ? "true" : "false";
            cbValorMensal.Enabled = false;
            textValorMensalidade.Text = contrato.ValorMensalidade.ToString();
            textValorMensalidade.ReadOnly = true;

            cbNumeroVidas.SelectedValue = contrato.GeraNumeroVidas == true ? "true" : "false";
            cbNumeroVidas.Enabled = false;
            textValorNumeroVidas.Text = contrato.ValorNumeroVidas.ToString();
            textValorNumeroVidas.ReadOnly = true;

            cbCobrancaAutomatica.SelectedValue = contrato.CobrancaAutomatica == true ? "true" : "false";
            cbCobrancaAutomatica.Enabled = false;
            textDiaCobranca.Text = contrato.DataCobranca.ToString();
            textDiaCobranca.ReadOnly = true;

            textComentario.Text = contrato.Observacao;
            textComentario.ReadOnly = true;

            btnGravar.Enabled = false;
            btnIncluirExame.Enabled = false;
            btnExcluirExame.Enabled = false;
            btnIncluirProduto.Enabled = false;
            btnExcluirProduto.Enabled = false;

            textEmpresa.Text = empresa.RazaoSocial;
            textNomeFantasia.Text = empresa.Fantasia;
            textCNPJ.Text = ValidaCampoHelper.FormataCnpj(empresa.Cnpj);

        }

        protected override void MontaDataGridExame()
        {
            try
            {
                if (contrato != null)
                {
                    dgvExame.Columns.Clear();
                    dgvExame.ReadOnly = true;

                    FinanceiroFacade financeiroFacade = FinanceiroFacade.getInstance();

                    DataSet ds = financeiroFacade.findAllContratoExameByContrato(contrato);

                    dgvExame.ColumnCount = 15;

                    dgvExame.Columns[0].HeaderText = "Id_contrato_exame";
                    dgvExame.Columns[0].Visible = false;

                    dgvExame.Columns[1].HeaderText = "Código";

                    dgvExame.Columns[2].HeaderText = "Exame";

                    dgvExame.Columns[3].HeaderText = "Preço no Contrato R$";
                    dgvExame.Columns[3].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
                    dgvExame.Columns[3].HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleRight;
                    dgvExame.Columns[3].DefaultCellStyle.BackColor = Color.LemonChiffon;

                    dgvExame.Columns[4].HeaderText = "Custo no Contrato R$";
                    dgvExame.Columns[4].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
                    dgvExame.Columns[4].HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleRight;
                    dgvExame.Columns[4].DefaultCellStyle.BackColor = Color.LemonChiffon;

                    dgvExame.Columns[5].HeaderText = "Data Inclusão";
                    dgvExame.Columns[5].Visible = false;

                    dgvExame.Columns[6].HeaderText = "Situacao";
                    dgvExame.Columns[6].Visible = false;

                    dgvExame.Columns[7].HeaderText = "flagAltera";
                    dgvExame.Columns[7].Visible = false;

                    dgvExame.Columns[8].HeaderText = "Situação Exame";
                    dgvExame.Columns[8].Visible = false;

                    dgvExame.Columns[9].HeaderText = "Laboratório";
                    dgvExame.Columns[9].Visible = false;

                    dgvExame.Columns[10].HeaderText = "Prioridade";
                    dgvExame.Columns[10].Visible = false;

                    dgvExame.Columns[11].HeaderText = "Externo";
                    dgvExame.Columns[11].Visible = false;

                    dgvExame.Columns[12].HeaderText = "Libera Documento";
                    dgvExame.Columns[12].Visible = false;

                    dgvExame.Columns[13].HeaderText = "Lucro Bruto %";
                    dgvExame.Columns[13].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
                    dgvExame.Columns[13].HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleRight;

                    dgvExame.Columns[14].HeaderText = "Padrão Contrato";
                    dgvExame.Columns[14].Visible = false;


                    foreach (DataRow dataRow in ds.Tables[0].Rows)
                        dgvExame.Rows.Add(dataRow["id_contrato_exame"], dataRow["id_exame"], dataRow["descricao"], dataRow["preco_contrato"], dataRow["custo_contrato"], dataRow["data_inclusao"], dataRow["situacao"], dataRow["altera"], dataRow["situacao_exame"], dataRow["laboratorio"], dataRow["prioridade"], dataRow["externo"], dataRow["libera_documento"], dataRow["lucro"], dataRow["padrao_contrato"]);
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        protected override void MontaDataGridProduto()
        {
            try
            {
                if (contrato != null)
                {

                    dgvProduto.Columns.Clear();
                    dgvProduto.ReadOnly = true;

                    FinanceiroFacade financeiroFacade = FinanceiroFacade.getInstance();

                    DataSet ds = financeiroFacade.findAllContratoProdutoByContrato(contrato);

                    dgvProduto.ColumnCount = 13;

                    dgvProduto.Columns[0].HeaderText = "Id_Produto_contrato";
                    dgvProduto.Columns[0].Visible = false;

                    dgvProduto.Columns[1].HeaderText = "Código";

                    dgvProduto.Columns[2].HeaderText = "Produto";

                    dgvProduto.Columns[3].HeaderText = "Preço no Contrato R$";
                    dgvProduto.Columns[3].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
                    dgvProduto.Columns[3].HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleRight;
                    dgvProduto.Columns[3].DefaultCellStyle.BackColor = Color.LemonChiffon;

                    dgvProduto.Columns[4].HeaderText = "Custo no Contrato R$";
                    dgvProduto.Columns[4].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
                    dgvProduto.Columns[4].HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleRight;
                    dgvProduto.Columns[4].DefaultCellStyle.BackColor = Color.LemonChiffon;

                    dgvProduto.Columns[5].HeaderText = "Data Inclusão";
                    dgvProduto.Columns[5].Visible = false;

                    dgvProduto.Columns[6].HeaderText = "Situacao";
                    dgvProduto.Columns[6].Visible = false;

                    dgvProduto.Columns[7].HeaderText = "FlagAltera";
                    dgvProduto.Columns[7].Visible = false;

                    dgvProduto.Columns[8].HeaderText = "Situação Produto";
                    dgvProduto.Columns[8].Visible = false;

                    dgvProduto.Columns[9].HeaderText = "Tipo";
                    dgvProduto.Columns[9].Visible = false;

                    dgvProduto.Columns[10].HeaderText = "Tipo Estudo";
                    dgvProduto.Columns[10].Visible = false;

                    dgvProduto.Columns[11].HeaderText = "Lucro Bruto %";
                    dgvProduto.Columns[11].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
                    dgvProduto.Columns[11].HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleRight;
                    
                    dgvProduto.Columns[12].HeaderText = "Padrão Contrato";
                    dgvProduto.Columns[12].Visible = false;


                    foreach (DataRow dataRow in ds.Tables[0].Rows)
                        dgvProduto.Rows.Add(dataRow["id_produto_contrato"], dataRow["id_produto"], dataRow["nome"], dataRow["preco_contratado"], dataRow["custo_contrato"], dataRow["data_inclusao"], dataRow["situacao"], dataRow["altera"], dataRow["situacao_produto"], dataRow["tipo"], dataRow["tipo_estudo"], dataRow["lucro"], dataRow["padrao_contrato"]);
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
    }
}
