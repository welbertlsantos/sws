﻿namespace SWS.View
{
    partial class frmVendedorIncluir
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.flpAcao = new System.Windows.Forms.FlowLayoutPanel();
            this.btnGravar = new System.Windows.Forms.Button();
            this.btnLimpar = new System.Windows.Forms.Button();
            this.btnFechar = new System.Windows.Forms.Button();
            this.textNome = new System.Windows.Forms.TextBox();
            this.lblNome = new System.Windows.Forms.TextBox();
            this.lblCpf = new System.Windows.Forms.TextBox();
            this.lblRg = new System.Windows.Forms.TextBox();
            this.lblEndereco = new System.Windows.Forms.TextBox();
            this.lblNumero = new System.Windows.Forms.TextBox();
            this.lblComplemento = new System.Windows.Forms.TextBox();
            this.lblBairro = new System.Windows.Forms.TextBox();
            this.lblUf = new System.Windows.Forms.TextBox();
            this.lblCidade = new System.Windows.Forms.TextBox();
            this.lblCep = new System.Windows.Forms.TextBox();
            this.lblEmail = new System.Windows.Forms.TextBox();
            this.lblTelefone = new System.Windows.Forms.TextBox();
            this.lblCelular = new System.Windows.Forms.TextBox();
            this.lblComissao = new System.Windows.Forms.TextBox();
            this.textCpf = new System.Windows.Forms.MaskedTextBox();
            this.textRG = new System.Windows.Forms.TextBox();
            this.textEndereco = new System.Windows.Forms.TextBox();
            this.textNumero = new System.Windows.Forms.TextBox();
            this.textComplemento = new System.Windows.Forms.TextBox();
            this.textBairro = new System.Windows.Forms.TextBox();
            this.cbUf = new SWS.ComboBoxWithBorder();
            this.textCep = new System.Windows.Forms.MaskedTextBox();
            this.textEmail = new System.Windows.Forms.TextBox();
            this.textTelefone = new System.Windows.Forms.TextBox();
            this.textCelular = new System.Windows.Forms.TextBox();
            this.textComissao = new System.Windows.Forms.TextBox();
            this.errorIncluirVendedor = new System.Windows.Forms.ErrorProvider(this.components);
            this.btnCidade = new System.Windows.Forms.Button();
            this.textCidade = new System.Windows.Forms.TextBox();
            ((System.ComponentModel.ISupportInitialize)(this.spForm)).BeginInit();
            this.spForm.Panel1.SuspendLayout();
            this.spForm.Panel2.SuspendLayout();
            this.spForm.SuspendLayout();
            this.pnlForm.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.banner)).BeginInit();
            this.flpAcao.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errorIncluirVendedor)).BeginInit();
            this.SuspendLayout();
            // 
            // spForm
            // 
            // 
            // spForm.Panel1
            // 
            this.spForm.Panel1.Controls.Add(this.flpAcao);
            // 
            // pnlForm
            // 
            this.pnlForm.Controls.Add(this.btnCidade);
            this.pnlForm.Controls.Add(this.textCidade);
            this.pnlForm.Controls.Add(this.textComissao);
            this.pnlForm.Controls.Add(this.textCelular);
            this.pnlForm.Controls.Add(this.textTelefone);
            this.pnlForm.Controls.Add(this.textEmail);
            this.pnlForm.Controls.Add(this.textCep);
            this.pnlForm.Controls.Add(this.cbUf);
            this.pnlForm.Controls.Add(this.textBairro);
            this.pnlForm.Controls.Add(this.textComplemento);
            this.pnlForm.Controls.Add(this.textNumero);
            this.pnlForm.Controls.Add(this.textEndereco);
            this.pnlForm.Controls.Add(this.textRG);
            this.pnlForm.Controls.Add(this.textCpf);
            this.pnlForm.Controls.Add(this.lblComissao);
            this.pnlForm.Controls.Add(this.lblCelular);
            this.pnlForm.Controls.Add(this.lblTelefone);
            this.pnlForm.Controls.Add(this.lblEmail);
            this.pnlForm.Controls.Add(this.lblCep);
            this.pnlForm.Controls.Add(this.lblCidade);
            this.pnlForm.Controls.Add(this.lblUf);
            this.pnlForm.Controls.Add(this.lblBairro);
            this.pnlForm.Controls.Add(this.lblComplemento);
            this.pnlForm.Controls.Add(this.lblNumero);
            this.pnlForm.Controls.Add(this.lblEndereco);
            this.pnlForm.Controls.Add(this.lblRg);
            this.pnlForm.Controls.Add(this.lblCpf);
            this.pnlForm.Controls.Add(this.textNome);
            this.pnlForm.Controls.Add(this.lblNome);
            // 
            // flpAcao
            // 
            this.flpAcao.Controls.Add(this.btnGravar);
            this.flpAcao.Controls.Add(this.btnLimpar);
            this.flpAcao.Controls.Add(this.btnFechar);
            this.flpAcao.Dock = System.Windows.Forms.DockStyle.Fill;
            this.flpAcao.Location = new System.Drawing.Point(0, 0);
            this.flpAcao.Name = "flpAcao";
            this.flpAcao.Size = new System.Drawing.Size(784, 35);
            this.flpAcao.TabIndex = 18;
            // 
            // btnGravar
            // 
            this.btnGravar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnGravar.Image = global::SWS.Properties.Resources.Gravar;
            this.btnGravar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnGravar.Location = new System.Drawing.Point(3, 3);
            this.btnGravar.Name = "btnGravar";
            this.btnGravar.Size = new System.Drawing.Size(77, 23);
            this.btnGravar.TabIndex = 18;
            this.btnGravar.Text = "&Gravar";
            this.btnGravar.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnGravar.UseVisualStyleBackColor = true;
            this.btnGravar.Click += new System.EventHandler(this.btnGravar_Click);
            // 
            // btnLimpar
            // 
            this.btnLimpar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnLimpar.Image = global::SWS.Properties.Resources.vassoura;
            this.btnLimpar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnLimpar.Location = new System.Drawing.Point(86, 3);
            this.btnLimpar.Name = "btnLimpar";
            this.btnLimpar.Size = new System.Drawing.Size(77, 23);
            this.btnLimpar.TabIndex = 19;
            this.btnLimpar.TabStop = false;
            this.btnLimpar.Text = "Limpar";
            this.btnLimpar.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnLimpar.UseVisualStyleBackColor = true;
            this.btnLimpar.Click += new System.EventHandler(this.btnLimpar_Click);
            // 
            // btnFechar
            // 
            this.btnFechar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnFechar.Image = global::SWS.Properties.Resources.close;
            this.btnFechar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnFechar.Location = new System.Drawing.Point(169, 3);
            this.btnFechar.Name = "btnFechar";
            this.btnFechar.Size = new System.Drawing.Size(77, 23);
            this.btnFechar.TabIndex = 20;
            this.btnFechar.Text = "&Fechar";
            this.btnFechar.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnFechar.UseVisualStyleBackColor = true;
            this.btnFechar.Click += new System.EventHandler(this.btnFechar_Click);
            // 
            // textNome
            // 
            this.textNome.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textNome.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.textNome.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textNome.Location = new System.Drawing.Point(149, 3);
            this.textNome.MaxLength = 100;
            this.textNome.Name = "textNome";
            this.textNome.Size = new System.Drawing.Size(590, 21);
            this.textNome.TabIndex = 1;
            // 
            // lblNome
            // 
            this.lblNome.BackColor = System.Drawing.Color.Silver;
            this.lblNome.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblNome.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNome.Location = new System.Drawing.Point(3, 3);
            this.lblNome.MaxLength = 100;
            this.lblNome.Name = "lblNome";
            this.lblNome.ReadOnly = true;
            this.lblNome.Size = new System.Drawing.Size(147, 21);
            this.lblNome.TabIndex = 27;
            this.lblNome.TabStop = false;
            this.lblNome.Text = "Nome";
            // 
            // lblCpf
            // 
            this.lblCpf.BackColor = System.Drawing.Color.Silver;
            this.lblCpf.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblCpf.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCpf.Location = new System.Drawing.Point(3, 23);
            this.lblCpf.MaxLength = 100;
            this.lblCpf.Name = "lblCpf";
            this.lblCpf.ReadOnly = true;
            this.lblCpf.Size = new System.Drawing.Size(147, 21);
            this.lblCpf.TabIndex = 28;
            this.lblCpf.TabStop = false;
            this.lblCpf.Text = "CPF";
            // 
            // lblRg
            // 
            this.lblRg.BackColor = System.Drawing.Color.Silver;
            this.lblRg.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblRg.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblRg.Location = new System.Drawing.Point(3, 43);
            this.lblRg.MaxLength = 100;
            this.lblRg.Name = "lblRg";
            this.lblRg.ReadOnly = true;
            this.lblRg.Size = new System.Drawing.Size(147, 21);
            this.lblRg.TabIndex = 29;
            this.lblRg.TabStop = false;
            this.lblRg.Text = "RG";
            // 
            // lblEndereco
            // 
            this.lblEndereco.BackColor = System.Drawing.Color.Silver;
            this.lblEndereco.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblEndereco.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblEndereco.Location = new System.Drawing.Point(3, 63);
            this.lblEndereco.MaxLength = 100;
            this.lblEndereco.Name = "lblEndereco";
            this.lblEndereco.ReadOnly = true;
            this.lblEndereco.Size = new System.Drawing.Size(147, 21);
            this.lblEndereco.TabIndex = 30;
            this.lblEndereco.TabStop = false;
            this.lblEndereco.Text = "Endereço";
            // 
            // lblNumero
            // 
            this.lblNumero.BackColor = System.Drawing.Color.Silver;
            this.lblNumero.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblNumero.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNumero.Location = new System.Drawing.Point(3, 83);
            this.lblNumero.MaxLength = 100;
            this.lblNumero.Name = "lblNumero";
            this.lblNumero.ReadOnly = true;
            this.lblNumero.Size = new System.Drawing.Size(147, 21);
            this.lblNumero.TabIndex = 31;
            this.lblNumero.TabStop = false;
            this.lblNumero.Text = "Número";
            // 
            // lblComplemento
            // 
            this.lblComplemento.BackColor = System.Drawing.Color.Silver;
            this.lblComplemento.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblComplemento.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblComplemento.Location = new System.Drawing.Point(3, 103);
            this.lblComplemento.MaxLength = 100;
            this.lblComplemento.Name = "lblComplemento";
            this.lblComplemento.ReadOnly = true;
            this.lblComplemento.Size = new System.Drawing.Size(147, 21);
            this.lblComplemento.TabIndex = 32;
            this.lblComplemento.TabStop = false;
            this.lblComplemento.Text = "Complemento";
            // 
            // lblBairro
            // 
            this.lblBairro.BackColor = System.Drawing.Color.Silver;
            this.lblBairro.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblBairro.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblBairro.Location = new System.Drawing.Point(3, 123);
            this.lblBairro.MaxLength = 100;
            this.lblBairro.Name = "lblBairro";
            this.lblBairro.ReadOnly = true;
            this.lblBairro.Size = new System.Drawing.Size(147, 21);
            this.lblBairro.TabIndex = 33;
            this.lblBairro.TabStop = false;
            this.lblBairro.Text = "Bairro";
            // 
            // lblUf
            // 
            this.lblUf.BackColor = System.Drawing.Color.Silver;
            this.lblUf.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblUf.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblUf.Location = new System.Drawing.Point(3, 143);
            this.lblUf.MaxLength = 100;
            this.lblUf.Name = "lblUf";
            this.lblUf.ReadOnly = true;
            this.lblUf.Size = new System.Drawing.Size(147, 21);
            this.lblUf.TabIndex = 34;
            this.lblUf.TabStop = false;
            this.lblUf.Text = "UF";
            // 
            // lblCidade
            // 
            this.lblCidade.BackColor = System.Drawing.Color.Silver;
            this.lblCidade.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblCidade.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCidade.Location = new System.Drawing.Point(3, 163);
            this.lblCidade.MaxLength = 100;
            this.lblCidade.Name = "lblCidade";
            this.lblCidade.ReadOnly = true;
            this.lblCidade.Size = new System.Drawing.Size(147, 21);
            this.lblCidade.TabIndex = 35;
            this.lblCidade.TabStop = false;
            this.lblCidade.Text = "Cidade";
            // 
            // lblCep
            // 
            this.lblCep.BackColor = System.Drawing.Color.Silver;
            this.lblCep.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblCep.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCep.Location = new System.Drawing.Point(3, 183);
            this.lblCep.MaxLength = 100;
            this.lblCep.Name = "lblCep";
            this.lblCep.ReadOnly = true;
            this.lblCep.Size = new System.Drawing.Size(147, 21);
            this.lblCep.TabIndex = 36;
            this.lblCep.TabStop = false;
            this.lblCep.Text = "CEP";
            // 
            // lblEmail
            // 
            this.lblEmail.BackColor = System.Drawing.Color.Silver;
            this.lblEmail.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblEmail.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblEmail.Location = new System.Drawing.Point(3, 203);
            this.lblEmail.MaxLength = 100;
            this.lblEmail.Name = "lblEmail";
            this.lblEmail.ReadOnly = true;
            this.lblEmail.Size = new System.Drawing.Size(147, 21);
            this.lblEmail.TabIndex = 37;
            this.lblEmail.TabStop = false;
            this.lblEmail.Text = "E-mail";
            // 
            // lblTelefone
            // 
            this.lblTelefone.BackColor = System.Drawing.Color.Silver;
            this.lblTelefone.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblTelefone.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTelefone.Location = new System.Drawing.Point(3, 223);
            this.lblTelefone.MaxLength = 100;
            this.lblTelefone.Name = "lblTelefone";
            this.lblTelefone.ReadOnly = true;
            this.lblTelefone.Size = new System.Drawing.Size(147, 21);
            this.lblTelefone.TabIndex = 38;
            this.lblTelefone.TabStop = false;
            this.lblTelefone.Text = "Telefone";
            // 
            // lblCelular
            // 
            this.lblCelular.BackColor = System.Drawing.Color.Silver;
            this.lblCelular.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblCelular.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCelular.Location = new System.Drawing.Point(3, 243);
            this.lblCelular.MaxLength = 100;
            this.lblCelular.Name = "lblCelular";
            this.lblCelular.ReadOnly = true;
            this.lblCelular.Size = new System.Drawing.Size(147, 21);
            this.lblCelular.TabIndex = 39;
            this.lblCelular.TabStop = false;
            this.lblCelular.Text = "Celular";
            // 
            // lblComissao
            // 
            this.lblComissao.BackColor = System.Drawing.Color.Silver;
            this.lblComissao.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblComissao.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblComissao.Location = new System.Drawing.Point(3, 263);
            this.lblComissao.MaxLength = 100;
            this.lblComissao.Name = "lblComissao";
            this.lblComissao.ReadOnly = true;
            this.lblComissao.Size = new System.Drawing.Size(147, 21);
            this.lblComissao.TabIndex = 40;
            this.lblComissao.TabStop = false;
            this.lblComissao.Text = "Comissão";
            // 
            // textCpf
            // 
            this.textCpf.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textCpf.Location = new System.Drawing.Point(149, 23);
            this.textCpf.Mask = "999,999,999-99";
            this.textCpf.Name = "textCpf";
            this.textCpf.PromptChar = ' ';
            this.textCpf.Size = new System.Drawing.Size(590, 21);
            this.textCpf.TabIndex = 2;
            // 
            // textRG
            // 
            this.textRG.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textRG.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.textRG.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textRG.Location = new System.Drawing.Point(149, 43);
            this.textRG.MaxLength = 15;
            this.textRG.Name = "textRG";
            this.textRG.Size = new System.Drawing.Size(590, 21);
            this.textRG.TabIndex = 3;
            // 
            // textEndereco
            // 
            this.textEndereco.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textEndereco.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.textEndereco.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textEndereco.Location = new System.Drawing.Point(149, 63);
            this.textEndereco.MaxLength = 100;
            this.textEndereco.Name = "textEndereco";
            this.textEndereco.Size = new System.Drawing.Size(590, 21);
            this.textEndereco.TabIndex = 4;
            // 
            // textNumero
            // 
            this.textNumero.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textNumero.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.textNumero.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textNumero.Location = new System.Drawing.Point(149, 83);
            this.textNumero.MaxLength = 10;
            this.textNumero.Name = "textNumero";
            this.textNumero.Size = new System.Drawing.Size(590, 21);
            this.textNumero.TabIndex = 5;
            // 
            // textComplemento
            // 
            this.textComplemento.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textComplemento.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.textComplemento.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textComplemento.Location = new System.Drawing.Point(149, 103);
            this.textComplemento.MaxLength = 100;
            this.textComplemento.Name = "textComplemento";
            this.textComplemento.Size = new System.Drawing.Size(590, 21);
            this.textComplemento.TabIndex = 6;
            // 
            // textBairro
            // 
            this.textBairro.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBairro.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.textBairro.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBairro.Location = new System.Drawing.Point(149, 123);
            this.textBairro.MaxLength = 50;
            this.textBairro.Name = "textBairro";
            this.textBairro.Size = new System.Drawing.Size(590, 21);
            this.textBairro.TabIndex = 7;
            // 
            // cbUf
            // 
            this.cbUf.BackColor = System.Drawing.Color.LightGray;
            this.cbUf.BorderColor = System.Drawing.Color.DimGray;
            this.cbUf.BorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid;
            this.cbUf.FormattingEnabled = true;
            this.cbUf.Location = new System.Drawing.Point(149, 143);
            this.cbUf.Name = "cbUf";
            this.cbUf.Size = new System.Drawing.Size(590, 21);
            this.cbUf.TabIndex = 8;
            // 
            // textCep
            // 
            this.textCep.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textCep.Location = new System.Drawing.Point(149, 183);
            this.textCep.Mask = "99,999-999";
            this.textCep.Name = "textCep";
            this.textCep.PromptChar = ' ';
            this.textCep.Size = new System.Drawing.Size(590, 21);
            this.textCep.TabIndex = 10;
            // 
            // textEmail
            // 
            this.textEmail.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textEmail.CharacterCasing = System.Windows.Forms.CharacterCasing.Lower;
            this.textEmail.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textEmail.Location = new System.Drawing.Point(149, 203);
            this.textEmail.MaxLength = 50;
            this.textEmail.Name = "textEmail";
            this.textEmail.Size = new System.Drawing.Size(590, 21);
            this.textEmail.TabIndex = 11;
            // 
            // textTelefone
            // 
            this.textTelefone.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textTelefone.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.textTelefone.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textTelefone.Location = new System.Drawing.Point(149, 223);
            this.textTelefone.MaxLength = 15;
            this.textTelefone.Name = "textTelefone";
            this.textTelefone.Size = new System.Drawing.Size(590, 21);
            this.textTelefone.TabIndex = 12;
            // 
            // textCelular
            // 
            this.textCelular.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textCelular.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.textCelular.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textCelular.Location = new System.Drawing.Point(149, 243);
            this.textCelular.MaxLength = 15;
            this.textCelular.Name = "textCelular";
            this.textCelular.Size = new System.Drawing.Size(590, 21);
            this.textCelular.TabIndex = 13;
            // 
            // textComissao
            // 
            this.textComissao.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textComissao.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.textComissao.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textComissao.Location = new System.Drawing.Point(149, 263);
            this.textComissao.MaxLength = 15;
            this.textComissao.Name = "textComissao";
            this.textComissao.Size = new System.Drawing.Size(590, 21);
            this.textComissao.TabIndex = 14;
            this.textComissao.Text = "0,00";
            this.textComissao.Enter += new System.EventHandler(this.textComissao_Enter);
            this.textComissao.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.textComissao_KeyPress);
            this.textComissao.Leave += new System.EventHandler(this.textComissao_Leave);
            // 
            // errorIncluirVendedor
            // 
            this.errorIncluirVendedor.ContainerControl = this;
            // 
            // btnCidade
            // 
            this.btnCidade.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnCidade.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnCidade.Image = global::SWS.Properties.Resources.busca;
            this.btnCidade.Location = new System.Drawing.Point(705, 163);
            this.btnCidade.Name = "btnCidade";
            this.btnCidade.Size = new System.Drawing.Size(34, 21);
            this.btnCidade.TabIndex = 9;
            this.btnCidade.UseVisualStyleBackColor = true;
            this.btnCidade.Click += new System.EventHandler(this.btCidade_Click);
            // 
            // textCidade
            // 
            this.textCidade.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.textCidade.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.textCidade.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textCidade.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textCidade.Location = new System.Drawing.Point(149, 163);
            this.textCidade.Name = "textCidade";
            this.textCidade.ReadOnly = true;
            this.textCidade.Size = new System.Drawing.Size(557, 21);
            this.textCidade.TabIndex = 42;
            this.textCidade.TabStop = false;
            // 
            // frmVendedorIncluir
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(784, 561);
            this.Name = "frmVendedorIncluir";
            this.Text = "INCLUIR VENDEDOR";
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.frmVendedorIncluir_KeyDown);
            this.spForm.Panel1.ResumeLayout(false);
            this.spForm.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.spForm)).EndInit();
            this.spForm.ResumeLayout(false);
            this.pnlForm.ResumeLayout(false);
            this.pnlForm.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.banner)).EndInit();
            this.flpAcao.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.errorIncluirVendedor)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.FlowLayoutPanel flpAcao;
        protected System.Windows.Forms.Button btnGravar;
        protected System.Windows.Forms.Button btnLimpar;
        protected System.Windows.Forms.Button btnFechar;
        protected System.Windows.Forms.TextBox textNome;
        protected System.Windows.Forms.TextBox lblNome;
        protected System.Windows.Forms.TextBox lblCpf;
        protected System.Windows.Forms.TextBox lblRg;
        protected System.Windows.Forms.TextBox lblEndereco;
        protected System.Windows.Forms.TextBox lblNumero;
        protected System.Windows.Forms.TextBox lblComplemento;
        protected System.Windows.Forms.TextBox lblBairro;
        protected System.Windows.Forms.TextBox lblUf;
        protected System.Windows.Forms.TextBox lblCidade;
        protected System.Windows.Forms.TextBox lblCep;
        protected System.Windows.Forms.TextBox lblEmail;
        protected System.Windows.Forms.TextBox lblCelular;
        protected System.Windows.Forms.TextBox lblTelefone;
        protected System.Windows.Forms.TextBox textBairro;
        protected System.Windows.Forms.TextBox textComplemento;
        protected System.Windows.Forms.TextBox textNumero;
        protected System.Windows.Forms.TextBox textEndereco;
        protected System.Windows.Forms.TextBox textRG;
        protected System.Windows.Forms.TextBox lblComissao;
        protected System.Windows.Forms.MaskedTextBox textCpf;
        protected System.Windows.Forms.TextBox textComissao;
        protected System.Windows.Forms.TextBox textCelular;
        protected System.Windows.Forms.TextBox textTelefone;
        protected System.Windows.Forms.TextBox textEmail;
        protected System.Windows.Forms.MaskedTextBox textCep;
        protected ComboBoxWithBorder cbUf;
        protected System.Windows.Forms.Button btnCidade;
        protected System.Windows.Forms.TextBox textCidade;
        protected System.Windows.Forms.ErrorProvider errorIncluirVendedor;
    }
}