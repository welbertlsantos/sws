﻿using SWS.Entidade;
using SWS.Helper;
using SWS.View.ViewHelper;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace SWS.View
{
    public partial class frmEmpresaDetalhar : frmEmpresaIncluir
    {
        public frmEmpresaDetalhar(Empresa empresa)
        {
            InitializeComponent();

            text_razaoSocial.Text = empresa.RazaoSocial;
            text_razaoSocial.ReadOnly = true;
            
            text_fantasia.Text = empresa.Fantasia;
            text_fantasia.ReadOnly = true;

            text_cnpj.Text = empresa.Cnpj;
            text_cnpj.ReadOnly = true;

            text_inscricao.Text = empresa.Inscricao;
            text_inscricao.ReadOnly = true;

            text_endereco.Text = empresa.Endereco;
            text_endereco.ReadOnly = true;
            
            text_numero.Text = empresa.Numero;
            text_numero.ReadOnly = true;
            
            text_complemento.Text = empresa.Complemento;
            text_complemento.ReadOnly = true;
            
            text_bairro.Text = empresa.Bairro;
            text_bairro.ReadOnly = true;

            text_cep.Text = empresa.Cep;
            text_cep.ReadOnly = true;

            cbUf.SelectedValue = empresa.Uf;
            cbUf.Enabled = false;

            if (!String.IsNullOrEmpty(empresa.Uf) && empresa.Cidade != null)
                cbCidade.SelectedValue = empresa.Cidade.Id.ToString();

            cbCidade.Enabled = false;

            text_telefone1.Text = empresa.Telefone1;
            text_telefone1.ReadOnly = true;

            text_telefone2.Text = empresa.Telefone2;
            text_telefone2.ReadOnly = true;

            text_email.Text = empresa.Email;
            text_email.ReadOnly = true;

            text_site.Text = empresa.Site;
            text_site.ReadOnly = true;

            cbSimples.SelectedValue = empresa.SimplesNacional == true ? "true" : "false";
            cbSimples.Enabled = false;

            if (empresa.Logo != null)
                pic_box_logo.Image = FileHelper.byteArrayToImage(empresa.Logo);

            pic_box_logo.Enabled = false;

            bt_abrir_file_dialog.Enabled = false;
            btn_gravar.Enabled = false;
            lbl_limpar.Enabled = false;

            textMatriz.Text = empresa.Matriz != null ? empresa.Matriz.RazaoSocial : string.Empty;
            btnMatriz.Enabled = false;

            textCodigoCnes.Text = empresa.CodigoCnes;
            textCodigoCnes.Enabled = false;

            textValorMinimoImpostoFederal.Text = empresa.ValorMinimoImpostoFederal.ToString();
            textValorMinimoImpostoFederal.Enabled = false;

            textValorMinimoIR.Text = empresa.ValorMinimoIR.ToString();
            textValorMinimoIR.Enabled = false;

            textAliquotaPIS.Text = empresa.AliquotaPIS.ToString();
            textAliquotaPIS.Enabled = false;

            textAliquotaCOFINS.Text = empresa.AliquotaCOFINS.ToString();
            textAliquotaCOFINS.Enabled = false;

            textAliquotaIR.Text = empresa.AliquotaIR.ToString();
            textAliquotaIR.Enabled = false;
            
            textAliquotaCSLL.Text = empresa.AliquotaCSLL.ToString();
            textAliquotaCSLL.Enabled = false;

            textAliquotaISS.Text = empresa.AliquotaISS.ToString();
            textAliquotaISS.Enabled = false;


        }
    }
}
