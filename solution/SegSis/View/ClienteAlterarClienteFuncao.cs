﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using SegSis.Facade;
using SegSis.Entidade;
using SegSis.View.Resources;
using SegSis.View.ViewHelper;

namespace SegSis.View
{
    public partial class frm_ClienteAlterarFuncaoBuscar : BaseFormConsulta
    {
        Cliente cliente = null;
        Funcao funcao = null;

        private List<ClienteFuncao> clienteFuncoes = new List<ClienteFuncao>();
        public List<ClienteFuncao> getClienteFuncoes()
        {
            return this.clienteFuncoes;
        }

        private ClienteFuncao clienteFuncao = null;
        public ClienteFuncao getClienteFuncao()
        {
            return this.clienteFuncao;
        }

        public frm_ClienteAlterarFuncaoBuscar(Cliente cliente)
        {
            InitializeComponent();
            this.cliente = cliente;
            validaPermissao();
        }

        private void btn_fechar_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void validaPermissao()
        {
            PermissionamentoFacade permissionamentoFacade = PermissionamentoFacade.getInstance();

            btn_novo.Enabled = permissionamentoFacade.hasPermission("FUNCAO", "INCLUIR");
        }

        private void btn_novo_Click(object sender, EventArgs e)
        {
            frm_funcao_incluir formFuncaoIncluir = new frm_funcao_incluir();
            formFuncaoIncluir.ShowDialog();

            if (formFuncaoIncluir.getFuncao() != null)
            {
                funcao = formFuncaoIncluir.getFuncao();
                
                clienteFuncao = new ClienteFuncao(null, cliente, funcao,
                    ApplicationConstants.ATIVO, DateTime.Now, null);

                
                PpraFacade ppraFacade = PpraFacade.getInstance();
                ClienteFacade clienteFacede = ClienteFacade.getInstance();
                
                clienteFuncao = ppraFacade.incluirClienteFuncao(clienteFuncao);
                clienteFuncoes = clienteFacede.findAllFuncaoByCliente(cliente);
            }
            
            this.Close();
        }

        private void btn_buscar_Click(object sender, EventArgs e)
        {
            try
            {
                this.Cursor = Cursors.WaitCursor;
                montaDataGrid();
                text_descricao.Focus();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }

        public void montaDataGrid()
        {

            PpraFacade ppraFacade = PpraFacade.getInstance();

            grd_funcao.Columns.Clear();

            DataSet ds = ppraFacade.findAllFuncaoAtivaNotInClient(new Funcao(null,text_descricao.Text.ToUpper(), 
               text_cbo.Text,ApplicationConstants.ATIVO,String.Empty), cliente);

            grd_funcao.DataSource = ds.Tables["Funcoes"].DefaultView;

            grd_funcao.Columns[0].HeaderText = "id_funcao";
            grd_funcao.Columns[0].Visible = false;
            grd_funcao.Columns[0].DisplayIndex = 1;
            grd_funcao.Columns[0].ReadOnly = true;

            grd_funcao.Columns[1].HeaderText = "Descrição";
            grd_funcao.Columns[1].DisplayIndex = 2;
            grd_funcao.Columns[1].ReadOnly = true;

            grd_funcao.Columns[2].HeaderText = "CBO";
            grd_funcao.Columns[2].DisplayIndex = 3;
            grd_funcao.Columns[2].ReadOnly = true;

            grd_funcao.Columns[3].HeaderText = "Situação";
            grd_funcao.Columns[3].Visible = false;
            grd_funcao.Columns[3].DisplayIndex = 4;
            grd_funcao.Columns[3].ReadOnly = true;

            grd_funcao.Columns[4].HeaderText = "Comentário";
            grd_funcao.Columns[4].DisplayIndex = 5;
            grd_funcao.Columns[4].ReadOnly = true;
            grd_funcao.Columns[4].Visible = false;

            grd_funcao.Columns[5].HeaderText = "Selec";
            grd_funcao.Columns[5].DisplayIndex = 0;

        }

        public void marcaTodos(Boolean estado)
        {
            foreach (DataGridViewRow dvRow in grd_funcao.Rows)
            {
                dvRow.Cells[5].Value = estado;
            }
        }

        private void chk_marcaDesmarca_CheckedChanged(object sender, EventArgs e)
        {
            if (chk_marcaDesmarca.Checked)
            {
                marcaTodos(true);
            }
            else
            {
                marcaTodos(false);
            }
        }

        private void text_cbo_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = ValidaCampoHelper.FormataCampoNumerico(sender, e);
        }

        private void btn_incluir_Click(object sender, EventArgs e)
        {
            try
            {
                PpraFacade ppraFacade = PpraFacade.getInstance();
                ClienteFacade clienteFacade = ClienteFacade.getInstance();
                
                foreach (DataGridViewRow dvRom in grd_funcao.Rows)
                {
                    if ((Boolean)dvRom.Cells[5].Value == true)
                    {
                        Funcao funcao = new Funcao((Int64)dvRom.Cells[0].Value, (String)dvRom.Cells[1].Value,
                            (String)dvRom.Cells[2].Value, (String)dvRom.Cells[3].Value, (String)dvRom.Cells[4].Value);

                        ClienteFuncao clienteFuncao = new ClienteFuncao(null, cliente, funcao);

                        clienteFuncao = ppraFacade.incluirClienteFuncao(clienteFuncao);

                        clienteFuncoes = clienteFacade.findAllFuncaoByCliente(cliente);

                    }
                }
                
                this.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
    }
}
