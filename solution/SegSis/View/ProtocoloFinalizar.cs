﻿using SWS.Entidade;
using SWS.Facade;
using SWS.View.Resources;
using SWS.View.ViewHelper;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace SWS.View
{
    public partial class frmProtocoloFinalizar : frmTemplate
    {
        private Protocolo protocolo;
        private bool finalizado;

        public bool Finalizado
        {
            get { return finalizado; }
            set { finalizado = value; }
        }

        public Protocolo Protocolo
        {
            get { return protocolo; }
            set { protocolo = value; }
        }

        public frmProtocoloFinalizar(Protocolo protocolo)
        {
            InitializeComponent();
            this.Protocolo = protocolo;
            textCodigo.Text = protocolo.Numero.ToString();
            textDataGravacao.Text = String.Format("{0:dd/MM/yyyy}", protocolo.DataGravacao.ToString());
            ActiveControl = textEnviado;
            this.montaDataGrid();
        }

        private void montaDataGrid() 
        {
            try
            {
                this.Cursor = Cursors.WaitCursor;
                ProtocoloFacade protocoloFacade = ProtocoloFacade.getInstance();
                this.dgvItens.Columns.Clear();

                DataSet ds = protocoloFacade.findAllItensByProtocolo(protocolo);
                dgvItens.DataSource = ds.Tables["ExamesProtocolo"].DefaultView;

                dgvItens.Columns[0].HeaderText = "id";
                dgvItens.Columns[0].Visible = false;

                dgvItens.Columns[1].HeaderText = "Código do Item";
                dgvItens.Columns[1].ReadOnly = true;

                dgvItens.Columns[2].HeaderText = "Cliente";
                dgvItens.Columns[2].ReadOnly = true;

                dgvItens.Columns[3].HeaderText = "Colaborador";
                dgvItens.Columns[3].ReadOnly = true;

                dgvItens.Columns[4].HeaderText = "RG";
                dgvItens.Columns[4].ReadOnly = true;

                dgvItens.Columns[5].HeaderText = "CPF";
                dgvItens.Columns[5].ReadOnly = true;

                dgvItens.Columns[6].HeaderText = "Periodicidade";
                dgvItens.Columns[6].ReadOnly = true;

                dgvItens.Columns[7].HeaderText = "Descrição";
                dgvItens.Columns[7].ReadOnly = true;

                dgvItens.Columns[8].HeaderText = "Data";
                dgvItens.Columns[8].ReadOnly = true;

                dgvItens.Columns[9].HeaderText = "Laudo";
                dgvItens.Columns[9].ReadOnly = true;

                dgvItens.Columns[10].HeaderText = "Tipo";
                dgvItens.Columns[10].Visible = false;

                dgvItens.Columns[11].HeaderText = "Selec";
                dgvItens.Columns[11].Visible = false;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }

        private void dgvItens_RowPrePaint(object sender, DataGridViewRowPrePaintEventArgs e)
        {
            DataGridView dgv = sender as DataGridView;

            if (String.Equals(ApplicationConstants.ASO, dgv.Rows[e.RowIndex].Cells[10].Value))
            {
                dgv.Rows[e.RowIndex].DefaultCellStyle.ForeColor = Color.Red;
            }
        }

        private bool validaDadosTela()
        {
            bool retorno = false;
            try
            {
                if (String.IsNullOrEmpty(textEnviado.Text))
                    throw new Exception("Você deve informar como esse protocolo foi enviado.");

                if (String.IsNullOrEmpty(textObservacao.Text))
                    throw new Exception("O campo observação é obrigatório");
            }
            catch (Exception ex)
            {
                retorno = true;
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            return retorno;
        }

        private void btnConfirmar_Click(object sender, EventArgs e)
        {
            try
            {
                if (!validaDadosTela())
                {
                    protocolo.Enviado = textEnviado.Text.Trim().ToUpper();
                    protocolo.DocumentoResponsavel = this.textDocumentoEnvio.Text.Trim().ToUpper();
                    protocolo.Observacao = textObservacao.Text.Trim().ToUpper();
                    protocolo.DataFinalizacao = DateTime.Now;
                    protocolo.UsuarioFinalizou = PermissionamentoFacade.usuarioAutenticado;

                    ProtocoloFacade protocoloFacade = ProtocoloFacade.getInstance();

                    protocoloFacade.finalizaProtocolo(protocolo);
                    MessageBox.Show("Protocolo finalizado com sucesso. Anota o número:" + ValidaCampoHelper.RetornaCodigoAtendimentoFormatado(protocolo.Numero));
                    Finalizado = true;
                    this.Close();

                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btnFechar_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
