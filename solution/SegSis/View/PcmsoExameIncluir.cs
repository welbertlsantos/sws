﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using SWS.Entidade;
using SWS.Facade;
using SWS.View.ViewHelper;
using SWS.View.Resources;

namespace SWS.View
{
    public partial class frm_PcmsoExameIncluir : BaseFormConsulta
    {
        private static String msg01 = "Selecione um Exame!";
        private static String msg02 = "Não é possível gravar um Exame no PCMSO sem uma Periodicidade.";
        private static String msg03 = "Selecione uma Função!";
        private static String msg04 = "Obrigatório pelo menos um tipo de situação para o exame. " + System.Environment.NewLine + 
            "Reveja o grupo tipo de exame e marque uma ou mais opções ";

        private static String msg05 = "O campo idade não pode ser vazio. Para não limitar a idade para o exame use 0.";

        private frm_PcmsoExames formPcmsoExame;
        private frm_PcmsoSemPpraIncluir formPcmsoSemPpraIncluir;
        public Int64 idGheFonteAgente;
        private Cliente cliente;
        private Ghe ghe;
        public Exame exame;
        private frmPcmsoAvulsoIncluir frmPcmsoAvulsoIncluir;
        public GheFonteAgenteExame gheFonteAgenteExameAlterado;

        private Boolean altera = false;

        public Dictionary<Int64, Dictionary<Funcao, Int32>> excecaoIdadeFuncao = new Dictionary<Int64, Dictionary<Funcao, Int32>>();
        
        public frm_PcmsoExameIncluir(frm_PcmsoExames formPcmsoExame, Cliente cliente, Ghe ghe, GheFonteAgenteExame gheFonteAgenteExameAlterado)
        {
            InitializeComponent();

            try
            {
                PcmsoFacade pcmsoFacade = PcmsoFacade.getInstance();

                this.formPcmsoExame = formPcmsoExame;
                this.idGheFonteAgente = formPcmsoExame.idGheFonteAgente;
                this.cliente = cliente;
                this.ghe = ghe;
                this.gheFonteAgenteExameAlterado = gheFonteAgenteExameAlterado;

                inicializa();

                if (gheFonteAgenteExameAlterado != null)
                {
                    grb_periodicidade.Visible = false;

                    altera = true;

                    //Buscando o exame
                    exame = pcmsoFacade.findExameById((Int64)gheFonteAgenteExameAlterado.Exame.Id);
                    btn_exame.Enabled = false;
                    text_descricao.Text = exame.Descricao;

                    //Preenchendo lista de exceções
                    excecaoIdadeFuncao = pcmsoFacade.findGheSetorClienteFuncaoExameByGheAndExame(ghe, exame);
                    montaGridExcecaoExamesPorFuncao();

                    //Preenchendo idade
                    text_idadeExame.Text = gheFonteAgenteExameAlterado.IdadeExame.ToString();
                    text_idadeExame.Enabled = true;

                    chkTrabalhoAltura.Checked = gheFonteAgenteExameAlterado.Altura;
                    chkEspacoConfinado.Checked = gheFonteAgenteExameAlterado.Confinado;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        public frm_PcmsoExameIncluir(frmPcmsoAvulsoIncluir frmPcmsoAvulsoIncluir, Cliente cliente, Ghe ghe, GheFonteAgenteExame gheFonteAgenteExameAlterado)
        {
            InitializeComponent();
            
            try
            {
                PcmsoFacade pcmsoFacade = PcmsoFacade.getInstance();

                this.frmPcmsoAvulsoIncluir = frmPcmsoAvulsoIncluir;
                //this.idGheFonteAgente = (Int64)frm_PcmsoAvulso.gheFonteAgente.getId();
                this.cliente = cliente;
                this.ghe = ghe;
                this.gheFonteAgenteExameAlterado = gheFonteAgenteExameAlterado;

                inicializa();

                if (gheFonteAgenteExameAlterado != null)
                {
                    grb_periodicidade.Visible = false;

                    altera = true;

                    //Buscando o exame
                    exame = pcmsoFacade.findExameById((Int64)gheFonteAgenteExameAlterado.Exame.Id);
                    btn_exame.Enabled = false;
                    text_descricao.Text = exame.Descricao;

                    //Preenchendo lista de exceções
                    excecaoIdadeFuncao = pcmsoFacade.findGheSetorClienteFuncaoExameByGheAndExame(ghe, exame);
                    montaGridExcecaoExamesPorFuncao();

                    //Preenchendo idade
                    text_idadeExame.Text = gheFonteAgenteExameAlterado.IdadeExame.ToString();
                    text_idadeExame.Enabled = true;

                    chkTrabalhoAltura.Checked = gheFonteAgenteExameAlterado.Altura;
                    chkEspacoConfinado.Checked = gheFonteAgenteExameAlterado.Confinado;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        public frm_PcmsoExameIncluir(frm_PcmsoSemPpraIncluir formPcmsoSemPpraIncluir, Cliente cliente, Ghe ghe, GheFonteAgenteExame gheFonteAgenteExameAlterado)
        {
            InitializeComponent();

            try
            {
                PcmsoFacade pcmsoFacade = PcmsoFacade.getInstance();

                this.formPcmsoSemPpraIncluir = formPcmsoSemPpraIncluir;
                this.idGheFonteAgente = formPcmsoSemPpraIncluir.idGheFonteAgente;
                this.cliente = cliente;
                this.ghe = ghe;
                this.gheFonteAgenteExameAlterado = gheFonteAgenteExameAlterado;

                inicializa();

                if (gheFonteAgenteExameAlterado != null)
                {
                    grb_periodicidade.Visible = false;

                    altera = true;

                    //Buscando o exame
                    exame = pcmsoFacade.findExameById((Int64)gheFonteAgenteExameAlterado.Exame.Id);
                    btn_exame.Enabled = false;
                    text_descricao.Text = exame.Descricao;

                    //Preenchendo lista de exceções
                    excecaoIdadeFuncao = pcmsoFacade.findGheSetorClienteFuncaoExameByGheAndExame(ghe, exame);
                    montaGridExcecaoExamesPorFuncao();

                    //Preenchendo idade
                    text_idadeExame.Text = gheFonteAgenteExameAlterado.IdadeExame.ToString();
                    text_idadeExame.Enabled = true;

                    chkTrabalhoAltura.Checked = gheFonteAgenteExameAlterado.Altura;
                    chkEspacoConfinado.Checked = gheFonteAgenteExameAlterado.Confinado;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void btn_fechar_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btn_exame_Click(object sender, EventArgs e)
        {
            frm_PcmsoExamesBuscar formPcmsoExamesBuscar = new frm_PcmsoExamesBuscar(this);
            formPcmsoExamesBuscar.ShowDialog();
        }

        private void inicializa()
        {
            try
            {
                PpraFacade ppraFacade = PpraFacade.getInstance();

                if (ghe != null)
                {
                    ghe = ppraFacade.findGheById(ghe.Id);
                }

                this.text_cliente.Text = cliente.RazaoSocial;
                this.text_ghe.Text = ghe.Descricao;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void montaGridExcecaoExamesPorFuncao()
        {
            List<Int64> list = new List<Int64>(excecaoIdadeFuncao.Keys);

            KeyValuePair<Funcao, Int32> funcaoExcecao;
            
            grd_excecao_exames.Columns.Clear();

            grd_excecao_exames.ColumnCount = 3;

            grd_excecao_exames.Columns[0].Name = "ID";
            grd_excecao_exames.Columns[1].Name = "Função";
            grd_excecao_exames.Columns[2].Name = "Idade";

            grd_excecao_exames.Columns[0].Visible = false;

            Int32 rowindex = 0;

            Funcao func = null;

            foreach (KeyValuePair<Int64, Dictionary<Funcao, Int32>> dict in excecaoIdadeFuncao)
            {
                funcaoExcecao = dict.Value.First();

                func = funcaoExcecao.Key;

                this.grd_excecao_exames.Rows.Insert(rowindex, func.Id, func.Descricao, funcaoExcecao.Value);

                rowindex++;
            }
        }

        private void btn_incluir_Click(object sender, EventArgs e)
        {
            try
            {
                PcmsoFacade pcmsoFacade = PcmsoFacade.getInstance();

                Boolean verifica = false;

                //Significa uma nova inclusao
                if (gheFonteAgenteExameAlterado == null)
                {
                    //incluindo primeiro gheFonteAgenteExameDao
                    if (exame != null)
                    {
                        foreach (DataGridViewRow dvRow in grd_periodicidade.Rows)
                        {
                            if ((Boolean)dvRow.Cells[2].Value == true)
                            {
                                verifica = true;
                            }
                        }

                        if (verifica == true)
                        {
                            if (!String.IsNullOrEmpty(text_idadeExame.Text))
                            {

                                GheFonteAgente gheFonteAgente = new GheFonteAgente(idGheFonteAgente, null, null, null, null, null, string.Empty, string.Empty);
                                GheFonteAgenteExame gheFonteAgenteExame = new GheFonteAgenteExame(null, exame, gheFonteAgente, Convert.ToInt32(text_idadeExame.Text),
                                    ApplicationConstants.ATIVO, chkEspacoConfinado.Checked ? true : false, chkTrabalhoAltura.Checked ? true : false, chkEletricidade.Checked ? true : false);

                                gheFonteAgenteExame = pcmsoFacade.incluirGheFonteAgenteExame(gheFonteAgenteExame);

                                // incluindo as periodicidades dos exames
                                GheFonteAgenteExamePeriodicidade gheFonteAgenteExamePeriodicidade = new GheFonteAgenteExamePeriodicidade();

                                foreach (DataGridViewRow dvRow in grd_periodicidade.Rows)
                                {
                                    if ((Boolean)dvRow.Cells[2].Value == true)
                                    {
                                        gheFonteAgenteExamePeriodicidade.GheFonteAgenteExame = gheFonteAgenteExame;
                                        gheFonteAgenteExamePeriodicidade.Periodicidade = new Periodicidade((Int64)dvRow.Cells[0].Value, (String)dvRow.Cells[1].Value, null);
                                        gheFonteAgenteExamePeriodicidade.Situacao = ApplicationConstants.ATIVO;
                                        pcmsoFacade.incluirGheFonteAgenteExamePeriodicidade(gheFonteAgenteExamePeriodicidade);
                                    }
                                }

                                Funcao funcao = null;

                                //Incluindo exceções de idade para o exame por função
                                foreach (DataGridViewRow dvRow in grd_excecao_exames.Rows)
                                {
                                    funcao = new Funcao();
                                    funcao.Id = (Int64)dvRow.Cells[0].Value;
                                    funcao.Descricao = (String)dvRow.Cells[1].Value;

                                    GheSetorClienteFuncaoExame gheSetorClienteFuncaoExame = null;

                                    //Buscando gheSetorClienteFuncao aplicados a função independente do setor
                                    foreach (GheSetorClienteFuncao gheSetorClienteFuncao in pcmsoFacade.findGheSetorClienteFuncaoByGheAndFuncao(ghe, funcao))
                                    {
                                        gheSetorClienteFuncaoExame = new GheSetorClienteFuncaoExame(gheSetorClienteFuncao, exame, (Int32)dvRow.Cells[2].Value, gheFonteAgenteExame);

                                        pcmsoFacade.incluiGheSetorClienteFuncaoExame(gheSetorClienteFuncaoExame);
                                    }
                                }

                                if (formPcmsoExame != null)
                                {
                                    formPcmsoExame.montaDataGridExame();
                                }

                                if (formPcmsoSemPpraIncluir != null)
                                {
                                    formPcmsoSemPpraIncluir.montaDataGridExame();
                                }

                                this.Close();
                            }
                            else
                            {
                                throw new Exception(msg05);
                            }
                        }
                        else
                        {
                            MessageBox.Show(msg02);
                        }
                    }
                    else
                    {
                        MessageBox.Show(msg01);
                    }
                }
                //Significa uma alteração
                else
                {
                    //Alterar a idade geral do exame no ghe
                    gheFonteAgenteExameAlterado.IdadeExame = Convert.ToInt32(text_idadeExame.Text);
                    pcmsoFacade.updateGheFonteAgenteExame(gheFonteAgenteExameAlterado);

                    //Limpar a listagem de exceções de idade por função e criar com a seleção.
                    pcmsoFacade.deleteGheSetorClienteFuncaoExame(ghe, exame);

                    //Gravando a nova lista de exceções
                    Funcao funcao = null;
                    //Incluindo exceções de idade para o exame por função
                    foreach (DataGridViewRow dvRow in grd_excecao_exames.Rows)
                    {
                        funcao = new Funcao();
                        funcao.Id = (Int64)dvRow.Cells[0].Value;
                        funcao.Descricao = (String)dvRow.Cells[1].Value;

                        GheSetorClienteFuncaoExame gheSetorClienteFuncaoExame = null;

                        //Buscando gheSetorClienteFuncao aplicados a função independente do setor
                        foreach (GheSetorClienteFuncao gheSetorClienteFuncao in pcmsoFacade.findGheSetorClienteFuncaoByGheAndFuncao(ghe, funcao))
                        {
                            gheSetorClienteFuncaoExame = new GheSetorClienteFuncaoExame(gheSetorClienteFuncao, exame, (Int32)dvRow.Cells[2].Value, gheFonteAgenteExameAlterado);

                            pcmsoFacade.incluiGheSetorClienteFuncaoExame(gheSetorClienteFuncaoExame);
                        }
                    }

                    this.Close();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        public void montaGridPeriodicidade()
        {
            try
            {
                grd_periodicidade.Columns.Clear();

                PcmsoFacade pcmsoFacade = PcmsoFacade.getInstance();
                DataSet ds = pcmsoFacade.findAllPeriodicidade();

                grd_periodicidade.DataSource = ds.Tables["Periodicidades"].DefaultView;
                
                grd_periodicidade.Columns[0].HeaderText = "Id";
                grd_periodicidade.Columns[1].HeaderText = "Descrição";       
                grd_periodicidade.Columns[0].Visible = false;
                
                //criando componete check na gride
                DataGridViewCheckBoxColumn check = new DataGridViewCheckBoxColumn();

                grd_periodicidade.Columns.Add(check);
                check.HeaderText = "Incluir ";
                check.Name = "checkSelecao";
                
                grd_periodicidade.Columns[1].ReadOnly = true;

                // reordenando coluna de index

                grd_periodicidade.Columns[0].DisplayIndex = 1;
                grd_periodicidade.Columns[1].DisplayIndex = 2;
                
                grd_periodicidade.Columns[2].DisplayIndex = 0;

                foreach (DataGridViewRow dvRow in grd_periodicidade.Rows)
                {
                    dvRow.Cells[2].Value = false;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void text_idadeExame_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = ValidaCampoHelper.FormataCampoNumerico(sender, e);
        }

        private void btn_incluirExcecao_Click(object sender, EventArgs e)
        {
            if (exame != null && exame.Id != null)
            {
                frm_PcmsoFuncaoGheSelecionar formPcmsoFuncaoGheSelecionar = new frm_PcmsoFuncaoGheSelecionar(this, cliente, ghe, exame);
                formPcmsoFuncaoGheSelecionar.ShowDialog();

                montaGridExcecaoExamesPorFuncao();
            }
            else
            {
                MessageBox.Show(msg01);
            }
        }

        private void btn_excluirExcecao_Click(object sender, EventArgs e)
        {
            try
            {
                PcmsoFacade pcmsoFacade = PcmsoFacade.getInstance();

                if (this.grd_excecao_exames.CurrentRow != null)
                {
                    Int64 id = (Int64)grd_excecao_exames.CurrentRow.Cells[0].Value;

                    excecaoIdadeFuncao.Remove(id);

                    Funcao funcaoRemove = new Funcao();
                    funcaoRemove.Id =  id;

                    pcmsoFacade.deleteGheSetorClienteFuncaoExame(ghe, exame, funcaoRemove);

                    montaGridExcecaoExamesPorFuncao();
                }
                else
                {
                    MessageBox.Show(msg03);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void chkTrabalhoAltura_CheckedChanged(object sender, EventArgs e)
        {
            try
            {
                if (altera)
                {

                    PcmsoFacade pcmsoFacade = PcmsoFacade.getInstance();

                    pcmsoFacade.updateGheFonteAgenteExame(new GheFonteAgenteExame(
                        gheFonteAgenteExameAlterado.Id,
                        null,
                        null,
                        0,
                        String.Empty,
                       chkEspacoConfinado.Checked ? true : false,
                       chkTrabalhoAltura.Checked ? true : false,
                       chkEletricidade.Checked ? true : false));
                }
                               
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
        }

        private void chkEspacoConfinado_CheckedChanged(object sender, EventArgs e)
        {
            chkTrabalhoAltura_CheckedChanged(sender, e);           
        }

        

    }
}
