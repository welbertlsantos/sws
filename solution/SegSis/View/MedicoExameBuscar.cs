﻿using SWS.Entidade;
using SWS.Facade;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace SWS.View
{
    public partial class frmMedicoExameBuscar : frmTemplateConsulta
    {

        private List<MedicoExame> medicos = new List<MedicoExame>();
        private Medico medico;

        public Medico Medico
        {
            get { return medico; }
            set { medico = value; }
        }
        

        public frmMedicoExameBuscar(Exame exame)
        {
            InitializeComponent();
            EstudoFacade pcmsoFacade = EstudoFacade.getInstance();
            medicos = pcmsoFacade.findAllMedicoExameByExame(exame);
            montaGrid();
        }

        private void montaGrid()
        {
            dgvMedico.Columns.Clear();
            dgvMedico.ColumnCount = 3;

            dgvMedico.Columns[0].HeaderText = "idMedico";
            dgvMedico.Columns[0].Visible = false;
            dgvMedico.Columns[1].HeaderText = "Médico";
            dgvMedico.Columns[2].HeaderText = "CRM";

            medicos.ForEach(delegate(MedicoExame medicoExame)
            {
                dgvMedico.Rows.Add(medicoExame.Medico.Id, medicoExame.Medico.Nome, medicoExame.Medico.Crm);
            });

        }

        private void btnFechar_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnIncluir_Click(object sender, EventArgs e)
        {
            try
            {
                if (dgvMedico.CurrentRow == null)
                    throw new Exception("Selecione uma linha.");

                Medico = new Medico((long?)dgvMedico.CurrentRow.Cells[0].Value, dgvMedico.CurrentRow.Cells[1].Value.ToString(), dgvMedico.CurrentRow.Cells[2].Value.ToString(), string.Empty, string.Empty, string.Empty, string.Empty, string.Empty, string.Empty, string.Empty, string.Empty, string.Empty, string.Empty, null, string.Empty, string.Empty, string.Empty, null, string.Empty, string.Empty, string.Empty);
                this.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void dgvMedico_CellMouseDoubleClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            try
            {
                btnIncluir.PerformClick();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }
    }
}
