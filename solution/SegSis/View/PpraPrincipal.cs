﻿using SWS.Entidade;
using SWS.Facade;
using SWS.View.Resources;
using SWS.View.ViewHelper;
using SWS.ViewHelper;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace SWS.View
{
    public partial class frmPpraPrincipal : frmTemplate
    {
        private Estudo ppra;

        public Estudo Ppra
        {
            get { return ppra; }
            set { ppra = value; }
        }

        private Cliente cliente;

        public Cliente Cliente
        {
            get { return cliente; }
            set { cliente = value; }
        }

        private Usuario tecnico;

        public Usuario Tecnico
        {
            get { return tecnico; }
            set { tecnico = value; }
        }

        private Usuario responsavel;

        public Usuario Responsavel
        {
            get { return responsavel; }
            set { responsavel = value; }
        }


        public frmPpraPrincipal()
        {
            InitializeComponent();
            validaPermissoes();
            ComboHelper.situacaoEstudo(cbSituacao);
        }

        private void validaPermissoes()
        {
            PermissionamentoFacade permissionamentoFacade = PermissionamentoFacade.getInstance();

            btnAlterar.Enabled = permissionamentoFacade.hasPermission("PPRA", "ALTERAR");
            btnIncluir.Enabled = permissionamentoFacade.hasPermission("PPRA", "INCLUIR");
            btnExcluir.Enabled = permissionamentoFacade.hasPermission("PPRA", "EXCLUIR");
            btnImprimir.Enabled = permissionamentoFacade.hasPermission("PPRA", "IMPRIMIR");
            btnAnalisar.Enabled = permissionamentoFacade.hasPermission("PPRA", "ENVIAR");
            btnFinalizar.Enabled = permissionamentoFacade.hasPermission("PPRA", "FINALIZAR");
            btnRevisar.Enabled = permissionamentoFacade.hasPermission("PPRA", "REVISAR");
            btnCorrigir.Enabled = permissionamentoFacade.hasPermission("PPRA", "CORRIGIR");

        }

        private void btCliente_Click(object sender, EventArgs e)
        {
            try
            {
                frmClienteSelecionar selecionarCliente = new frmClienteSelecionar(null, null, false, null, null, false, null, false, true);
                selecionarCliente.ShowDialog();

                if (selecionarCliente.Cliente != null)
                {
                    cliente = selecionarCliente.Cliente;
                    textCliente.Text = cliente.RazaoSocial;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }

        private void btnExcluirCliente_Click(object sender, EventArgs e)
        {
            try
            {
                if (cliente == null)
                    throw new Exception("Você deve primeiro selecionar o cliente.");

                cliente = null;
                textCliente.Text = string.Empty;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }

        private void btnElaborador_Click(object sender, EventArgs e)
        {
            try
            {
                frmUsuarioTecnicoSelecionar selecionarTecnico = new frmUsuarioTecnicoSelecionar();
                selecionarTecnico.Text = "RESPONSÁVEL PELA ELABORAÇÃO DO ESTUDO";
                selecionarTecnico.ShowDialog();

                if (selecionarTecnico.Usuario != null)
                {
                    this.Tecnico = selecionarTecnico.Usuario;
                    textUsuarioElaborador.Text = Tecnico.Nome;

                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }

        private void btnExcluirElaborador_Click(object sender, EventArgs e)
        {
            try
            {
                if (Tecnico == null)
                    throw new Exception("Você deve selecionar o usuário responsável pela elaboração do estudo");

                Tecnico = null;
                textUsuarioElaborador.Text = string.Empty;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }

        private void btnResponsavel_Click(object sender, EventArgs e)
        {
            try
            {
                frmUsuarioFuncaoSelecionar selecionarUsuarioResponsavel = new frmUsuarioFuncaoSelecionar();
                selecionarUsuarioResponsavel.ShowDialog();

                if (selecionarUsuarioResponsavel.UsuarioTecnico != null)
                {
                    Responsavel = selecionarUsuarioResponsavel.UsuarioTecnico;
                    textResponsavel.Text = Responsavel.Nome;
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }

        private void btnExcluirResponsavel_Click(object sender, EventArgs e)
        {
            try
            {
                if (Responsavel == null)
                    throw new Exception("Você deve primeiro selecionar o Responsável");

                Responsavel = null;
                textResponsavel.Text = string.Empty;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }

        private void btnIncluir_Click(object sender, EventArgs e)
        {
            try
            {
                frmPpraIncluir incluirPpra = new frmPpraIncluir();
                incluirPpra.ShowDialog();

                if (incluirPpra.Ppra != null)
                {
                    MessageBox.Show("Estudo incluído com sucesso.", "Confirmação", MessageBoxButtons.OK, MessageBoxIcon.Information);
                 
                }


            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }

        private void btnAlterar_Click(object sender, EventArgs e)
        {
            try
            {
                if (dgvEstudo.CurrentRow == null)
                    throw new Exception("Você deve selecionar o estudo para alterá-lo");

                PpraFacade ppraFacade = PpraFacade.getInstance();
                Ppra = ppraFacade.findPpraById((long)dgvEstudo.CurrentRow.Cells[0].Value);

                if (!string.Equals(ppra.Situacao, ApplicationConstants.CONSTRUCAO))
                    throw new Exception("Somente estudos em construção podem ser alterados");

                /* TODO - Resolver com nova tela de incluir o ppra */
                frm_ppraAlterar alterarPpra = new frm_ppraAlterar(ppra, null);
                alterarPpra.ShowDialog();

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }

        private void bt_pesquisar_Click(object sender, EventArgs e)
        {
            try
            {
                if (!validaDadosTela())
                {
                    montaDataGrid();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }

        private bool validaDadosTela()
        {
            bool result = false;
            try
            {
                if (
                    string.IsNullOrEmpty(textCodigo.Text.Replace(".", "").Replace("/", "").Trim())
                    && !dataInicial.Checked && !dataFinal.Checked
                    && cbSituacao.SelectedIndex == 0
                    && cliente == null
                    && Tecnico == null
                    && Responsavel == null)
                    throw new Exception("Você deve selecionar algum filtro para iniciar a pesquisa");


                if (dataInicial.Checked && !dataFinal.Checked)
                    throw new Exception("Você deve selecionar a data final para realizar a pesquisa.");

                if (dataFinal.Checked && !dataInicial.Checked)
                    throw new Exception("Voce deve selecionar a data inicial para realizar a pesquisa");

                if (dataFinal.Value < dataInicial.Value)
                    throw new Exception("A data final deverá ser maior que a data Final");

                if (!dataInicial.Checked && !dataFinal.Checked)
                    throw new Exception("Você deve selecionar um período para iniciar a pesquisa.");

            }
            catch (Exception ex)
            {
                result = true;
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            return result;

        }

        private void montaDataGrid()
        {
            try
            {
                PpraFacade ppraFacade = PpraFacade.getInstance();

                this.Cursor = Cursors.WaitCursor;

                ppra = new Estudo();
                ppra.CodigoEstudo = textCodigo.Text;
                ppra.VendedorCliente = cliente != null ? new VendedorCliente(null, cliente, null, String.Empty) : null;
                ppra.Tecno = Tecnico;
                ppra.Engenheiro = Responsavel;
                ppra.Situacao = ((SelectItem)cbSituacao.SelectedItem).Valor;
                ppra.DataCriacao = dataInicial.Value;

                DataSet ds = ppraFacade.findPpraByFilter(ppra, dataFinal.Value);

                dgvEstudo.DataSource = ds.Tables["Estudos"].DefaultView;

                dgvEstudo.Columns[0].HeaderText = "ID";
                dgvEstudo.Columns[0].Visible = false;
                dgvEstudo.Columns[1].HeaderText = "Código PPRA";
                dgvEstudo.Columns[2].HeaderText = "Data da Criação";
                dgvEstudo.Columns[3].HeaderText = "Data de Vencimento";
                dgvEstudo.Columns[4].HeaderText = "Data de Fechamento";
                dgvEstudo.Columns[5].HeaderText = "Situação";
                dgvEstudo.Columns[6].HeaderText = "Cliente";
                dgvEstudo.Columns[6].DisplayIndex = 0;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }

        private void btnFechar_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnImprimir_Click(object sender, EventArgs e)
        {
            try
            {
                if (dgvEstudo.CurrentRow == null)
                    throw new Exception("Você deve selecionar uma estudo para imprimir.");

                PpraFacade ppraFacade = PpraFacade.getInstance();
                RelatorioFacade relatorioFacade = RelatorioFacade.getInstance();

                this.Cursor = Cursors.WaitCursor;

                Estudo estudo = ppraFacade.findPpraById((long)dgvEstudo.CurrentRow.Cells[0].Value);

                ppraFacade.montaQuadroVersao((Int64)dgvEstudo.CurrentRow.Cells[0].Value);
                ppraFacade.geraBaseFuncaoEpi((Int64)dgvEstudo.CurrentRow.Cells[0].Value);

                String nomeRelatorio = relatorioFacade.findNomeRelatorioById((Int64?)estudo.IdRelatorio);

                var process = new Process();


                process.StartInfo.FileName = "Relatorio\\jre7\\bin\\java";
                process.StartInfo.Arguments = " -jar Relatorio\\MiddlewareJasper.jar " + "Relatorio\\Report\\" + nomeRelatorio + " ID_PPRA:" + (Int64)dgvEstudo.CurrentRow.Cells[0].Value + ":INTEGER";
                process.StartInfo.UseShellExecute = false;
                process.StartInfo.RedirectStandardError = true;
                process.StartInfo.RedirectStandardInput = true;
                process.StartInfo.RedirectStandardOutput = true;
                process.StartInfo.CreateNoWindow = true;
                process.Start();
                process.StandardOutput.ReadToEnd();
                process.WaitForExit();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }

        private void btnRevisar_Click(object sender, EventArgs e)
        {
            try
            {
                if (dgvEstudo.CurrentRow == null)
                    throw new Exception("Você deve selecionar um estudo para iniciar a revisão.");

                this.Cursor = Cursors.WaitCursor;

                PpraFacade ppraFacade = PpraFacade.getInstance();

                long id = (long)dgvEstudo.CurrentRow.Cells[0].Value;

                if (!ppraFacade.verificaPodeReplicarByRevisao(id))
                    throw new Exception("Este PPRA não é a última versão. Só é possível criar revisão da última versão!");


                if (!ppraFacade.verificaPodeReplicarByPpra(id))
                    throw new Exception("Somente é possível criar revisão de um PPRA finalizado!");

                frmEstudoJustificativa incluirJustificativa = new frmEstudoJustificativa();
                incluirJustificativa.ShowDialog();

                if (!string.IsNullOrEmpty(incluirJustificativa.Justificativa))
                {
                    ppraFacade.replicarPpra(id, incluirJustificativa.Justificativa, false);
                    MessageBox.Show("Revisão gravada com suceeso.", "Confirmação", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    montaDataGrid();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }

        private void btnCorrigir_Click(object sender, EventArgs e)
        {
            try
            {
                if (dgvEstudo.CurrentRow == null)
                    throw new Exception("Selecione um estudo para continuar.");

                this.Cursor = Cursors.WaitCursor;

                PpraFacade ppraFacade = PpraFacade.getInstance();
                LogFacade logFacade = LogFacade.getInstance();

                ppra = ppraFacade.findPpraById((long)dgvEstudo.CurrentRow.Cells[0].Value);

                if (!String.Equals(ppra.Situacao, ApplicationConstants.FECHADO))
                    throw new Exception("Somente estudos finalizados podem ser corrigidos.");

                if (MessageBox.Show("Deseja iniciar uma correção para o estudo selecionado?", "Atençao", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                {

                    frmEstudoJustificativa incluirJustificativa = new frmEstudoJustificativa();
                    incluirJustificativa.ShowDialog();

                    if (!string.IsNullOrEmpty(incluirJustificativa.Justificativa))
                    {
                        /* incluindo a correcao do estudo */
                        CorrecaoEstudo correcaoEstudo = new CorrecaoEstudo(null, PermissionamentoFacade.usuarioAutenticado, ppra, incluirJustificativa.Justificativa, DateTime.Now);
                        correcaoEstudo = logFacade.insertCorrecaoEstudo(correcaoEstudo);
                        MessageBox.Show("Histórico da correção gravado com sucesso", "Confirmação", MessageBoxButtons.OK, MessageBoxIcon.Information);

                        /* alterando o ppra */
                        frm_ppraAlterar altera = new frm_ppraAlterar(ppra, null);
                        altera.ShowDialog();
                        montaDataGrid();
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }


        }

        private void btnFinalizar_Click(object sender, EventArgs e)
        {
            try
            {
                if (dgvEstudo.CurrentRow == null)
                    throw new Exception("Selecione um estudo para finalizar.");

                PpraFacade ppraFacade = PpraFacade.getInstance();

                this.Cursor = Cursors.WaitCursor;

                if (MessageBox.Show("Deseja finalizar esse PPRA?", "Atenção", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                {
                    ppra = ppraFacade.findPpraById((long)dgvEstudo.CurrentRow.Cells[0].Value);

                    if (ppra.Situacao != ApplicationConstants.ATIVO)
                        throw new Exception("Não é possível finalizar um PPRA que não esteja em análise.");

                    frmEstudoTipoProduto pesquisarTipoProduto = new frmEstudoTipoProduto(ApplicationConstants.PPRA);
                    pesquisarTipoProduto.ShowDialog();

                    if (pesquisarTipoProduto.Produto != null)
                    {
                        ppraFacade.mudaEstadoPpra(ppra, ApplicationConstants.FECHADO, pesquisarTipoProduto.Produto);
                        MessageBox.Show("PPRA finalizado com sucesso.", "Confirmação", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        montaDataGrid();
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btnAnalisar_Click(object sender, EventArgs e)
        {
            try
            {
                if (dgvEstudo.CurrentRow == null)
                    throw new Exception("Selecione um estudo para enviar para análise.");

                this.Cursor = Cursors.WaitCursor;

                PpraFacade ppraFacade = PpraFacade.getInstance();

                ppra = ppraFacade.findPpraById((long)dgvEstudo.CurrentRow.Cells[0].Value);

                if (ppra.Situacao.Equals(ApplicationConstants.ATIVO))
                {
                    if (MessageBox.Show("Estudo já está em análise, deseja enviá-lo para construção? ", "Atenção", MessageBoxButtons.YesNo) == DialogResult.Yes)
                    {
                        ppraFacade.mudaEstadoPpra(ppra, ApplicationConstants.CONSTRUCAO, null);
                        MessageBox.Show("PPRA enviado novamente para construção com sucesso.", "Confirmação", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        montaDataGrid();

                    }

                }
                else if (ppra.Situacao.Equals(ApplicationConstants.CONSTRUCAO))
                {
                    if (MessageBox.Show("Deseja enviar esse estudo para análise?", "Atenção", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                    {

                        frmUsuarioFuncaoSelecionar selecionarResponsavel = new frmUsuarioFuncaoSelecionar();
                        selecionarResponsavel.ShowDialog();

                        if (selecionarResponsavel.UsuarioTecnico != null)
                        {
                            ppra.Engenheiro = selecionarResponsavel.UsuarioTecnico;
                            ppraFacade.mudaEstadoPpra(ppra, ApplicationConstants.ATIVO, null);
                            MessageBox.Show("PPRA enviado para analise com sucesso.", "Confirmação", MessageBoxButtons.OK, MessageBoxIcon.Information);
                            montaDataGrid();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }

        }

        private void btnExcluir_Click(object sender, EventArgs e)
        {
            try
            {
                if (dgvEstudo.CurrentRow == null)
                    throw new Exception("Selecione um estudo para excluir.");

                PpraFacade ppraFacade = PpraFacade.getInstance();

                this.Cursor = Cursors.WaitCursor;

                ppra = ppraFacade.findPpraById((long)dgvEstudo.CurrentRow.Cells[0].Value);

                if (string.Equals(ppra.Situacao, ApplicationConstants.FECHADO))
                    throw new Exception("Não é possível excluir um estudo que já está fechado.");


                ppraFacade.excluiPpra((long)dgvEstudo.CurrentRow.Cells[0].Value);
                MessageBox.Show("Estudo excluído com sucesso.", "Confirmação", MessageBoxButtons.OK, MessageBoxIcon.Information);
                montaDataGrid();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }


        }

        private void dgvEstudo_RowPrePaint(object sender, DataGridViewRowPrePaintEventArgs e)
        {
            DataGridView dvg = sender as DataGridView;
            String valor = dvg.Rows[e.RowIndex].Cells[5].Value.ToString();

            if (String.Equals(valor, ApplicationConstants.FECHADO))
                dvg.Rows[e.RowIndex].DefaultCellStyle.ForeColor = Color.Green;
            else if (String.Equals(valor, ApplicationConstants.ATIVO))
                dvg.Rows[e.RowIndex].DefaultCellStyle.ForeColor = Color.Blue;
        }

        private void dgvEstudo_CellContentDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                this.Cursor = Cursors.WaitCursor;

                if (dgvEstudo.CurrentRow == null)
                    throw new Exception("Selecione uma estudo.");

                PpraFacade ppraFacade = PpraFacade.getInstance();

                ppra = ppraFacade.findPpraById((Int64)dgvEstudo.CurrentRow.Cells[0].Value);

                /* verificando se o estudo pode ter sido corrigido */
                if (!String.Equals(ApplicationConstants.FECHADO, ppra.Situacao))
                    throw new Exception("Somente estudos finalizados podem ter sido corrigidos.");

                frmEstudoCorrecao ConsultaCorrecao = new frmEstudoCorrecao(ppra);
                ConsultaCorrecao.ShowDialog();

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }
    }
}
