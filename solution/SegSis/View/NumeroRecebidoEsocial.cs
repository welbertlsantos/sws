﻿using SWS.View.ViewHelper;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SWS.View
{
    public partial class frmNumeroRecebidoEsocial : frmTemplateConsulta
    {
        string numeroRecibo;

        public string NumeroRecibo
        {
            get { return numeroRecibo; }
            set { numeroRecibo = value; }
        }

        
        public frmNumeroRecebidoEsocial()
        {
            InitializeComponent();
        }

        private void textRecibo_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = ValidaCampoHelper.formataNumerosEPonto(sender, e);
        }

        private void btnFechar_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btIGravar_Click(object sender, EventArgs e)
        {
            try
            {
                if (string.IsNullOrEmpty(textRecibo.Text.Trim()))
                    throw new Exception("O número de recibo deve ser uma string com 23 posições e só aceita números e o caracter '.' ");

                if (textRecibo.Text.Length != 23)
                    throw new Exception("O número de recibo deve ter 23 caracteres. ");

                this.numeroRecibo = textRecibo.Text.Trim();
                this.Close();

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
    }
}
