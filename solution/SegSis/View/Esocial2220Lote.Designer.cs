﻿namespace SWS.View
{
    partial class frmEsocial2220Lote
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            this.flpAcao = new System.Windows.Forms.FlowLayoutPanel();
            this.btnIncluir = new System.Windows.Forms.Button();
            this.btnPesquisa = new System.Windows.Forms.Button();
            this.btFechar = new System.Windows.Forms.Button();
            this.lblDataEmissao = new System.Windows.Forms.TextBox();
            this.btnCliente = new System.Windows.Forms.Button();
            this.clienteText = new System.Windows.Forms.TextBox();
            this.periodoCriacaoInicial = new System.Windows.Forms.DateTimePicker();
            this.periodoCriacaoFinal = new System.Windows.Forms.DateTimePicker();
            this.lblCliente = new System.Windows.Forms.TextBox();
            this.btnExcluirCliente = new System.Windows.Forms.Button();
            this.chk_marcarTodos = new System.Windows.Forms.CheckBox();
            this.grbAtendimento = new System.Windows.Forms.GroupBox();
            this.dgvAtendimento = new System.Windows.Forms.DataGridView();
            this.grbTotal = new System.Windows.Forms.GroupBox();
            this.lblTotalTitulo = new System.Windows.Forms.Label();
            this.textTotal = new System.Windows.Forms.TextBox();
            ((System.ComponentModel.ISupportInitialize)(this.spForm)).BeginInit();
            this.spForm.Panel1.SuspendLayout();
            this.spForm.Panel2.SuspendLayout();
            this.spForm.SuspendLayout();
            this.pnlForm.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.banner)).BeginInit();
            this.flpAcao.SuspendLayout();
            this.grbAtendimento.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvAtendimento)).BeginInit();
            this.grbTotal.SuspendLayout();
            this.SuspendLayout();
            // 
            // spForm
            // 
            // 
            // spForm.Panel1
            // 
            this.spForm.Panel1.Controls.Add(this.flpAcao);
            // 
            // pnlForm
            // 
            this.pnlForm.Controls.Add(this.grbTotal);
            this.pnlForm.Controls.Add(this.grbAtendimento);
            this.pnlForm.Controls.Add(this.chk_marcarTodos);
            this.pnlForm.Controls.Add(this.btnExcluirCliente);
            this.pnlForm.Controls.Add(this.lblDataEmissao);
            this.pnlForm.Controls.Add(this.btnCliente);
            this.pnlForm.Controls.Add(this.lblCliente);
            this.pnlForm.Controls.Add(this.clienteText);
            this.pnlForm.Controls.Add(this.periodoCriacaoFinal);
            this.pnlForm.Controls.Add(this.periodoCriacaoInicial);
            // 
            // flpAcao
            // 
            this.flpAcao.Controls.Add(this.btnIncluir);
            this.flpAcao.Controls.Add(this.btnPesquisa);
            this.flpAcao.Controls.Add(this.btFechar);
            this.flpAcao.Dock = System.Windows.Forms.DockStyle.Fill;
            this.flpAcao.Location = new System.Drawing.Point(0, 0);
            this.flpAcao.Name = "flpAcao";
            this.flpAcao.Size = new System.Drawing.Size(784, 35);
            this.flpAcao.TabIndex = 0;
            // 
            // btnIncluir
            // 
            this.btnIncluir.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnIncluir.Image = global::SWS.Properties.Resources.icone_mais;
            this.btnIncluir.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnIncluir.Location = new System.Drawing.Point(3, 3);
            this.btnIncluir.Name = "btnIncluir";
            this.btnIncluir.Size = new System.Drawing.Size(75, 23);
            this.btnIncluir.TabIndex = 83;
            this.btnIncluir.TabStop = false;
            this.btnIncluir.Text = "&Incluir";
            this.btnIncluir.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnIncluir.UseVisualStyleBackColor = true;
            this.btnIncluir.Click += new System.EventHandler(this.btnIncluir_Click);
            // 
            // btnPesquisa
            // 
            this.btnPesquisa.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnPesquisa.Image = global::SWS.Properties.Resources.lupa;
            this.btnPesquisa.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnPesquisa.Location = new System.Drawing.Point(84, 3);
            this.btnPesquisa.Name = "btnPesquisa";
            this.btnPesquisa.Size = new System.Drawing.Size(75, 23);
            this.btnPesquisa.TabIndex = 85;
            this.btnPesquisa.TabStop = false;
            this.btnPesquisa.Text = "&Pesquisar";
            this.btnPesquisa.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnPesquisa.UseVisualStyleBackColor = true;
            this.btnPesquisa.Click += new System.EventHandler(this.btnPesquisa_Click);
            // 
            // btFechar
            // 
            this.btFechar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btFechar.Image = global::SWS.Properties.Resources.close;
            this.btFechar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btFechar.Location = new System.Drawing.Point(165, 3);
            this.btFechar.Name = "btFechar";
            this.btFechar.Size = new System.Drawing.Size(75, 23);
            this.btFechar.TabIndex = 84;
            this.btFechar.TabStop = false;
            this.btFechar.Text = "&Fechar";
            this.btFechar.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btFechar.UseVisualStyleBackColor = true;
            this.btFechar.Click += new System.EventHandler(this.btFechar_Click);
            // 
            // lblDataEmissao
            // 
            this.lblDataEmissao.BackColor = System.Drawing.Color.LightGray;
            this.lblDataEmissao.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblDataEmissao.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDataEmissao.Location = new System.Drawing.Point(3, 3);
            this.lblDataEmissao.Name = "lblDataEmissao";
            this.lblDataEmissao.ReadOnly = true;
            this.lblDataEmissao.Size = new System.Drawing.Size(138, 21);
            this.lblDataEmissao.TabIndex = 49;
            this.lblDataEmissao.TabStop = false;
            this.lblDataEmissao.Text = "Período de atendimento";
            // 
            // btnCliente
            // 
            this.btnCliente.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnCliente.Image = global::SWS.Properties.Resources.busca;
            this.btnCliente.Location = new System.Drawing.Point(700, 23);
            this.btnCliente.Name = "btnCliente";
            this.btnCliente.Size = new System.Drawing.Size(30, 21);
            this.btnCliente.TabIndex = 53;
            this.btnCliente.UseVisualStyleBackColor = true;
            this.btnCliente.Click += new System.EventHandler(this.btnCliente_Click);
            // 
            // clienteText
            // 
            this.clienteText.BackColor = System.Drawing.SystemColors.Control;
            this.clienteText.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.clienteText.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.clienteText.Location = new System.Drawing.Point(140, 23);
            this.clienteText.MaxLength = 100;
            this.clienteText.Name = "clienteText";
            this.clienteText.ReadOnly = true;
            this.clienteText.Size = new System.Drawing.Size(564, 21);
            this.clienteText.TabIndex = 54;
            this.clienteText.TabStop = false;
            // 
            // periodoCriacaoInicial
            // 
            this.periodoCriacaoInicial.Checked = false;
            this.periodoCriacaoInicial.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.periodoCriacaoInicial.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.periodoCriacaoInicial.Location = new System.Drawing.Point(140, 3);
            this.periodoCriacaoInicial.Name = "periodoCriacaoInicial";
            this.periodoCriacaoInicial.ShowCheckBox = true;
            this.periodoCriacaoInicial.Size = new System.Drawing.Size(107, 21);
            this.periodoCriacaoInicial.TabIndex = 50;
            // 
            // periodoCriacaoFinal
            // 
            this.periodoCriacaoFinal.Checked = false;
            this.periodoCriacaoFinal.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.periodoCriacaoFinal.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.periodoCriacaoFinal.Location = new System.Drawing.Point(246, 3);
            this.periodoCriacaoFinal.Name = "periodoCriacaoFinal";
            this.periodoCriacaoFinal.ShowCheckBox = true;
            this.periodoCriacaoFinal.Size = new System.Drawing.Size(107, 21);
            this.periodoCriacaoFinal.TabIndex = 52;
            // 
            // lblCliente
            // 
            this.lblCliente.BackColor = System.Drawing.Color.LightGray;
            this.lblCliente.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblCliente.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCliente.Location = new System.Drawing.Point(3, 23);
            this.lblCliente.Name = "lblCliente";
            this.lblCliente.ReadOnly = true;
            this.lblCliente.Size = new System.Drawing.Size(138, 21);
            this.lblCliente.TabIndex = 51;
            this.lblCliente.TabStop = false;
            this.lblCliente.Text = "Cliente";
            // 
            // btnExcluirCliente
            // 
            this.btnExcluirCliente.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnExcluirCliente.Image = global::SWS.Properties.Resources.close;
            this.btnExcluirCliente.Location = new System.Drawing.Point(729, 23);
            this.btnExcluirCliente.Name = "btnExcluirCliente";
            this.btnExcluirCliente.Size = new System.Drawing.Size(30, 21);
            this.btnExcluirCliente.TabIndex = 55;
            this.btnExcluirCliente.UseVisualStyleBackColor = true;
            this.btnExcluirCliente.Click += new System.EventHandler(this.btnExcluirCliente_Click);
            // 
            // chk_marcarTodos
            // 
            this.chk_marcarTodos.AutoSize = true;
            this.chk_marcarTodos.Checked = true;
            this.chk_marcarTodos.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chk_marcarTodos.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.chk_marcarTodos.Location = new System.Drawing.Point(3, 50);
            this.chk_marcarTodos.Name = "chk_marcarTodos";
            this.chk_marcarTodos.Size = new System.Drawing.Size(145, 17);
            this.chk_marcarTodos.TabIndex = 56;
            this.chk_marcarTodos.Text = "Marcar / desmarcar todos";
            this.chk_marcarTodos.UseVisualStyleBackColor = true;
            this.chk_marcarTodos.CheckedChanged += new System.EventHandler(this.chk_marcarTodos_CheckedChanged);
            // 
            // grbAtendimento
            // 
            this.grbAtendimento.Controls.Add(this.dgvAtendimento);
            this.grbAtendimento.Location = new System.Drawing.Point(3, 73);
            this.grbAtendimento.Name = "grbAtendimento";
            this.grbAtendimento.Size = new System.Drawing.Size(756, 343);
            this.grbAtendimento.TabIndex = 57;
            this.grbAtendimento.TabStop = false;
            this.grbAtendimento.Text = "Atendimentos";
            // 
            // dgvAtendimento
            // 
            this.dgvAtendimento.AllowUserToAddRows = false;
            this.dgvAtendimento.AllowUserToDeleteRows = false;
            this.dgvAtendimento.AllowUserToOrderColumns = true;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.Gainsboro;
            this.dgvAtendimento.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle1;
            this.dgvAtendimento.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this.dgvAtendimento.BackgroundColor = System.Drawing.Color.White;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Arial", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvAtendimento.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle2;
            this.dgvAtendimento.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle3.BackColor = System.Drawing.Color.LightGray;
            dataGridViewCellStyle3.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvAtendimento.DefaultCellStyle = dataGridViewCellStyle3;
            this.dgvAtendimento.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvAtendimento.Location = new System.Drawing.Point(3, 16);
            this.dgvAtendimento.Name = "dgvAtendimento";
            this.dgvAtendimento.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.CellSelect;
            this.dgvAtendimento.Size = new System.Drawing.Size(750, 324);
            this.dgvAtendimento.TabIndex = 14;
            // 
            // grbTotal
            // 
            this.grbTotal.Controls.Add(this.lblTotalTitulo);
            this.grbTotal.Controls.Add(this.textTotal);
            this.grbTotal.Location = new System.Drawing.Point(6, 417);
            this.grbTotal.Name = "grbTotal";
            this.grbTotal.Size = new System.Drawing.Size(311, 42);
            this.grbTotal.TabIndex = 58;
            this.grbTotal.TabStop = false;
            this.grbTotal.Text = "Totais";
            // 
            // lblTotalTitulo
            // 
            this.lblTotalTitulo.AutoSize = true;
            this.lblTotalTitulo.Location = new System.Drawing.Point(40, 17);
            this.lblTotalTitulo.Name = "lblTotalTitulo";
            this.lblTotalTitulo.Size = new System.Drawing.Size(188, 13);
            this.lblTotalTitulo.TabIndex = 29;
            this.lblTotalTitulo.Text = "Quantidade de  atendimentos filtrados:";
            // 
            // textTotal
            // 
            this.textTotal.BackColor = System.Drawing.Color.LightBlue;
            this.textTotal.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textTotal.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textTotal.ForeColor = System.Drawing.Color.Black;
            this.textTotal.Location = new System.Drawing.Point(234, 13);
            this.textTotal.MaxLength = 10;
            this.textTotal.Name = "textTotal";
            this.textTotal.ReadOnly = true;
            this.textTotal.Size = new System.Drawing.Size(71, 23);
            this.textTotal.TabIndex = 30;
            this.textTotal.TabStop = false;
            this.textTotal.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // frmEsocial2220Lote
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(784, 561);
            this.Name = "frmEsocial2220Lote";
            this.Text = "E-SOCIAL EVENTO 2220 - LOTE";
            this.spForm.Panel1.ResumeLayout(false);
            this.spForm.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.spForm)).EndInit();
            this.spForm.ResumeLayout(false);
            this.pnlForm.ResumeLayout(false);
            this.pnlForm.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.banner)).EndInit();
            this.flpAcao.ResumeLayout(false);
            this.grbAtendimento.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvAtendimento)).EndInit();
            this.grbTotal.ResumeLayout(false);
            this.grbTotal.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.FlowLayoutPanel flpAcao;
        private System.Windows.Forms.Button btnIncluir;
        private System.Windows.Forms.Button btnPesquisa;
        private System.Windows.Forms.Button btFechar;
        private System.Windows.Forms.TextBox lblDataEmissao;
        private System.Windows.Forms.Button btnCliente;
        private System.Windows.Forms.TextBox lblCliente;
        public System.Windows.Forms.TextBox clienteText;
        private System.Windows.Forms.DateTimePicker periodoCriacaoFinal;
        private System.Windows.Forms.DateTimePicker periodoCriacaoInicial;
        private System.Windows.Forms.Button btnExcluirCliente;
        private System.Windows.Forms.CheckBox chk_marcarTodos;
        private System.Windows.Forms.GroupBox grbAtendimento;
        public System.Windows.Forms.DataGridView dgvAtendimento;
        private System.Windows.Forms.GroupBox grbTotal;
        private System.Windows.Forms.Label lblTotalTitulo;
        private System.Windows.Forms.TextBox textTotal;
    }
}