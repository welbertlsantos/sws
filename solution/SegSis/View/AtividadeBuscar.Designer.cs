﻿namespace SWS.View
{
    partial class frmAtividadeBuscar
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            this.flpAcao = new System.Windows.Forms.FlowLayoutPanel();
            this.btnConfirmar = new System.Windows.Forms.Button();
            this.btnFechar = new System.Windows.Forms.Button();
            this.btnAtividadeBuscar = new System.Windows.Forms.Button();
            this.textAtividade = new System.Windows.Forms.TextBox();
            this.lblAtividade = new System.Windows.Forms.TextBox();
            this.grbAtividade = new System.Windows.Forms.GroupBox();
            this.dgvAtividade = new System.Windows.Forms.DataGridView();
            this.btnNovo = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.scForm)).BeginInit();
            this.scForm.Panel1.SuspendLayout();
            this.scForm.Panel2.SuspendLayout();
            this.scForm.SuspendLayout();
            this.pnlForm.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.banner)).BeginInit();
            this.flpAcao.SuspendLayout();
            this.grbAtividade.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvAtividade)).BeginInit();
            this.SuspendLayout();
            // 
            // scForm
            // 
            // 
            // scForm.Panel1
            // 
            this.scForm.Panel1.Controls.Add(this.flpAcao);
            // 
            // pnlForm
            // 
            this.pnlForm.Controls.Add(this.grbAtividade);
            this.pnlForm.Controls.Add(this.btnAtividadeBuscar);
            this.pnlForm.Controls.Add(this.textAtividade);
            this.pnlForm.Controls.Add(this.lblAtividade);
            // 
            // flpAcao
            // 
            this.flpAcao.Controls.Add(this.btnConfirmar);
            this.flpAcao.Controls.Add(this.btnNovo);
            this.flpAcao.Controls.Add(this.btnFechar);
            this.flpAcao.Dock = System.Windows.Forms.DockStyle.Fill;
            this.flpAcao.Location = new System.Drawing.Point(0, 0);
            this.flpAcao.Name = "flpAcao";
            this.flpAcao.Size = new System.Drawing.Size(534, 30);
            this.flpAcao.TabIndex = 0;
            // 
            // btnConfirmar
            // 
            this.btnConfirmar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnConfirmar.Image = global::SWS.Properties.Resources.fechar_ico;
            this.btnConfirmar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnConfirmar.Location = new System.Drawing.Point(3, 3);
            this.btnConfirmar.Name = "btnConfirmar";
            this.btnConfirmar.Size = new System.Drawing.Size(75, 23);
            this.btnConfirmar.TabIndex = 12;
            this.btnConfirmar.TabStop = false;
            this.btnConfirmar.Text = "&Confirmar";
            this.btnConfirmar.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnConfirmar.UseVisualStyleBackColor = true;
            this.btnConfirmar.Click += new System.EventHandler(this.btnConfirmar_Click);
            // 
            // btnFechar
            // 
            this.btnFechar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnFechar.Image = global::SWS.Properties.Resources.close;
            this.btnFechar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnFechar.Location = new System.Drawing.Point(167, 3);
            this.btnFechar.Name = "btnFechar";
            this.btnFechar.Size = new System.Drawing.Size(75, 23);
            this.btnFechar.TabIndex = 11;
            this.btnFechar.TabStop = false;
            this.btnFechar.Text = "&Fechar";
            this.btnFechar.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnFechar.UseVisualStyleBackColor = true;
            this.btnFechar.Click += new System.EventHandler(this.btnFechar_Click);
            // 
            // btnAtividadeBuscar
            // 
            this.btnAtividadeBuscar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnAtividadeBuscar.Image = global::SWS.Properties.Resources.busca;
            this.btnAtividadeBuscar.Location = new System.Drawing.Point(477, 3);
            this.btnAtividadeBuscar.Name = "btnAtividadeBuscar";
            this.btnAtividadeBuscar.Size = new System.Drawing.Size(34, 21);
            this.btnAtividadeBuscar.TabIndex = 9;
            this.btnAtividadeBuscar.TabStop = false;
            this.btnAtividadeBuscar.UseVisualStyleBackColor = true;
            this.btnAtividadeBuscar.Click += new System.EventHandler(this.btnAtividadeBuscar_Click);
            // 
            // textAtividade
            // 
            this.textAtividade.BackColor = System.Drawing.Color.White;
            this.textAtividade.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textAtividade.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.textAtividade.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textAtividade.Location = new System.Drawing.Point(139, 3);
            this.textAtividade.MaxLength = 100;
            this.textAtividade.Name = "textAtividade";
            this.textAtividade.Size = new System.Drawing.Size(339, 21);
            this.textAtividade.TabIndex = 10;
            this.textAtividade.TabStop = false;
            // 
            // lblAtividade
            // 
            this.lblAtividade.BackColor = System.Drawing.Color.Silver;
            this.lblAtividade.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblAtividade.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblAtividade.Location = new System.Drawing.Point(3, 3);
            this.lblAtividade.MaxLength = 100;
            this.lblAtividade.Name = "lblAtividade";
            this.lblAtividade.ReadOnly = true;
            this.lblAtividade.Size = new System.Drawing.Size(137, 21);
            this.lblAtividade.TabIndex = 11;
            this.lblAtividade.Text = "Atividade";
            // 
            // grbAtividade
            // 
            this.grbAtividade.Controls.Add(this.dgvAtividade);
            this.grbAtividade.Location = new System.Drawing.Point(3, 30);
            this.grbAtividade.Name = "grbAtividade";
            this.grbAtividade.Size = new System.Drawing.Size(508, 256);
            this.grbAtividade.TabIndex = 12;
            this.grbAtividade.TabStop = false;
            this.grbAtividade.Text = "Atividades";
            // 
            // dgvAtividade
            // 
            this.dgvAtividade.AllowUserToAddRows = false;
            this.dgvAtividade.AllowUserToDeleteRows = false;
            this.dgvAtividade.AllowUserToOrderColumns = true;
            this.dgvAtividade.AllowUserToResizeRows = false;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.Gainsboro;
            this.dgvAtividade.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle1;
            this.dgvAtividade.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this.dgvAtividade.BackgroundColor = System.Drawing.Color.White;
            this.dgvAtividade.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.Color.LightGray;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvAtividade.DefaultCellStyle = dataGridViewCellStyle2;
            this.dgvAtividade.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvAtividade.Location = new System.Drawing.Point(3, 16);
            this.dgvAtividade.MultiSelect = false;
            this.dgvAtividade.Name = "dgvAtividade";
            this.dgvAtividade.ReadOnly = true;
            this.dgvAtividade.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvAtividade.Size = new System.Drawing.Size(502, 237);
            this.dgvAtividade.TabIndex = 0;
            // 
            // btnNovo
            // 
            this.btnNovo.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnNovo.Image = global::SWS.Properties.Resources.icone_mais;
            this.btnNovo.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnNovo.Location = new System.Drawing.Point(84, 3);
            this.btnNovo.Name = "btnNovo";
            this.btnNovo.Size = new System.Drawing.Size(77, 23);
            this.btnNovo.TabIndex = 16;
            this.btnNovo.TabStop = false;
            this.btnNovo.Text = "&Nova";
            this.btnNovo.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnNovo.UseVisualStyleBackColor = true;
            this.btnNovo.Click += new System.EventHandler(this.btnNovo_Click);
            // 
            // frmAtividadeBuscar
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(534, 361);
            this.Name = "frmAtividadeBuscar";
            this.Text = "LOCALIZAR ATIVIDADE";
            this.scForm.Panel1.ResumeLayout(false);
            this.scForm.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.scForm)).EndInit();
            this.scForm.ResumeLayout(false);
            this.pnlForm.ResumeLayout(false);
            this.pnlForm.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.banner)).EndInit();
            this.flpAcao.ResumeLayout(false);
            this.grbAtividade.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvAtividade)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.FlowLayoutPanel flpAcao;
        protected System.Windows.Forms.Button btnConfirmar;
        protected System.Windows.Forms.Button btnFechar;
        private System.Windows.Forms.GroupBox grbAtividade;
        protected System.Windows.Forms.DataGridView dgvAtividade;
        protected System.Windows.Forms.Button btnAtividadeBuscar;
        protected System.Windows.Forms.TextBox textAtividade;
        protected System.Windows.Forms.TextBox lblAtividade;
        protected System.Windows.Forms.Button btnNovo;
    }
}