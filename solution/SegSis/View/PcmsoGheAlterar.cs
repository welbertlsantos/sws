﻿using SWS.Entidade;
using SWS.Facade;
using SWS.View.Resources;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SWS.View
{
    public partial class frmPcmsoGheAlterar : frmPcmsoGheIncluir
    {
        public frmPcmsoGheAlterar(Ghe ghe, Estudo pcmso) : base()
        {
            InitializeComponent();
            this.Ghe = ghe;
            this.pcmso = pcmso;
            this.textDescricao.Text = ghe.Descricao;
            this.textExposto.Text = ghe.Nexp.ToString();
            this.textNumeroPo.Text = ghe.NumeroPo;
            this.ghe.Estudo = pcmso;
            this.grbNota.Enabled = true;
            this.flpNota.Enabled = true;
            this.lblNotaGhe.Text = string.Empty;
            montaGrid();
        }

        protected override void btnSalvar_Click(object sender, EventArgs e)
        {
            try
            {
                if (validaCampos())
                {
                    EstudoFacade pcmsoFacade = EstudoFacade.getInstance();
                    ghe.Estudo = pcmso;
                    ghe.Descricao = textDescricao.Text.Trim();
                    ghe.Nexp = Convert.ToInt32(textExposto.Text);
                    ghe.Situacao = ApplicationConstants.ATIVO;
                    ghe.NumeroPo = textNumeroPo.Text.Trim();

                    ghe = pcmsoFacade.updateGhe(ghe);
                    MessageBox.Show("GHE alterado com sucesso.", "Confirmação", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    this.Close();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }
    }
}
