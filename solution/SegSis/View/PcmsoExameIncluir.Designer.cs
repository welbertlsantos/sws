﻿namespace SWS.View
{
    partial class frm_PcmsoExameIncluir
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frm_PcmsoExameIncluir));
            this.grb_paginacao = new System.Windows.Forms.GroupBox();
            this.btn_fechar = new System.Windows.Forms.Button();
            this.btn_incluir = new System.Windows.Forms.Button();
            this.grb_periodicidade = new System.Windows.Forms.GroupBox();
            this.grd_periodicidade = new System.Windows.Forms.DataGridView();
            this.grb_excecao_exames = new System.Windows.Forms.GroupBox();
            this.grd_excecao_exames = new System.Windows.Forms.DataGridView();
            this.btn_excluirExcecao = new System.Windows.Forms.Button();
            this.btn_incluirExcecao = new System.Windows.Forms.Button();
            this.grb_exame = new System.Windows.Forms.GroupBox();
            this.text_ghe = new System.Windows.Forms.TextBox();
            this.lbl_ghe = new System.Windows.Forms.Label();
            this.lbl_cliente = new System.Windows.Forms.Label();
            this.text_cliente = new System.Windows.Forms.TextBox();
            this.text_idadeExame = new System.Windows.Forms.TextBox();
            this.lbl_idadeExame = new System.Windows.Forms.Label();
            this.btn_exame = new System.Windows.Forms.Button();
            this.text_descricao = new System.Windows.Forms.TextBox();
            this.lbl_descricao = new System.Windows.Forms.Label();
            this.grbSituacaoExame = new System.Windows.Forms.GroupBox();
            this.chkEspacoConfinado = new System.Windows.Forms.CheckBox();
            this.chkTrabalhoAltura = new System.Windows.Forms.CheckBox();
            this.tpExamePcmoIncluir = new System.Windows.Forms.ToolTip(this.components);
            this.chkEletricidade = new System.Windows.Forms.CheckBox();
            this.grb_paginacao.SuspendLayout();
            this.grb_periodicidade.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grd_periodicidade)).BeginInit();
            this.grb_excecao_exames.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grd_excecao_exames)).BeginInit();
            this.grb_exame.SuspendLayout();
            this.grbSituacaoExame.SuspendLayout();
            this.SuspendLayout();
            // 
            // grb_paginacao
            // 
            this.grb_paginacao.Controls.Add(this.btn_fechar);
            this.grb_paginacao.Controls.Add(this.btn_incluir);
            this.grb_paginacao.Location = new System.Drawing.Point(16, 620);
            this.grb_paginacao.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.grb_paginacao.Name = "grb_paginacao";
            this.grb_paginacao.Padding = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.grb_paginacao.Size = new System.Drawing.Size(927, 64);
            this.grb_paginacao.TabIndex = 2;
            this.grb_paginacao.TabStop = false;
            // 
            // btn_fechar
            // 
            this.btn_fechar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_fechar.Image = global::SWS.Properties.Resources.close;
            this.btn_fechar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btn_fechar.Location = new System.Drawing.Point(119, 23);
            this.btn_fechar.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.btn_fechar.Name = "btn_fechar";
            this.btn_fechar.Size = new System.Drawing.Size(100, 28);
            this.btn_fechar.TabIndex = 5;
            this.btn_fechar.Text = "&Fechar";
            this.btn_fechar.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btn_fechar.UseVisualStyleBackColor = true;
            this.btn_fechar.Click += new System.EventHandler(this.btn_fechar_Click);
            // 
            // btn_incluir
            // 
            this.btn_incluir.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_incluir.Image = global::SWS.Properties.Resources.Gravar;
            this.btn_incluir.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btn_incluir.Location = new System.Drawing.Point(11, 23);
            this.btn_incluir.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.btn_incluir.Name = "btn_incluir";
            this.btn_incluir.Size = new System.Drawing.Size(100, 28);
            this.btn_incluir.TabIndex = 4;
            this.btn_incluir.Text = "&Gravar";
            this.btn_incluir.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btn_incluir.UseVisualStyleBackColor = true;
            this.btn_incluir.Click += new System.EventHandler(this.btn_incluir_Click);
            // 
            // grb_periodicidade
            // 
            this.grb_periodicidade.Controls.Add(this.grd_periodicidade);
            this.grb_periodicidade.Location = new System.Drawing.Point(16, 391);
            this.grb_periodicidade.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.grb_periodicidade.Name = "grb_periodicidade";
            this.grb_periodicidade.Padding = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.grb_periodicidade.Size = new System.Drawing.Size(927, 230);
            this.grb_periodicidade.TabIndex = 1;
            this.grb_periodicidade.TabStop = false;
            this.grb_periodicidade.Text = "Periodicidade do Exame";
            // 
            // grd_periodicidade
            // 
            this.grd_periodicidade.AllowUserToAddRows = false;
            this.grd_periodicidade.AllowUserToDeleteRows = false;
            this.grd_periodicidade.AllowUserToOrderColumns = true;
            this.grd_periodicidade.AllowUserToResizeRows = false;
            this.grd_periodicidade.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this.grd_periodicidade.BackgroundColor = System.Drawing.Color.White;
            this.grd_periodicidade.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.grd_periodicidade.Dock = System.Windows.Forms.DockStyle.Fill;
            this.grd_periodicidade.Location = new System.Drawing.Point(4, 19);
            this.grd_periodicidade.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.grd_periodicidade.Name = "grd_periodicidade";
            this.grd_periodicidade.RowHeadersVisible = false;
            this.grd_periodicidade.Size = new System.Drawing.Size(919, 207);
            this.grd_periodicidade.TabIndex = 3;
            // 
            // grb_excecao_exames
            // 
            this.grb_excecao_exames.Controls.Add(this.grd_excecao_exames);
            this.grb_excecao_exames.Location = new System.Drawing.Point(276, 180);
            this.grb_excecao_exames.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.grb_excecao_exames.Name = "grb_excecao_exames";
            this.grb_excecao_exames.Padding = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.grb_excecao_exames.Size = new System.Drawing.Size(667, 169);
            this.grb_excecao_exames.TabIndex = 3;
            this.grb_excecao_exames.TabStop = false;
            this.grb_excecao_exames.Text = "Idade mínima para realização do exame por Função no GHE";
            // 
            // grd_excecao_exames
            // 
            this.grd_excecao_exames.AllowUserToAddRows = false;
            this.grd_excecao_exames.AllowUserToDeleteRows = false;
            this.grd_excecao_exames.AllowUserToOrderColumns = true;
            this.grd_excecao_exames.AllowUserToResizeRows = false;
            this.grd_excecao_exames.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this.grd_excecao_exames.BackgroundColor = System.Drawing.Color.White;
            this.grd_excecao_exames.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.grd_excecao_exames.Dock = System.Windows.Forms.DockStyle.Fill;
            this.grd_excecao_exames.Location = new System.Drawing.Point(4, 19);
            this.grd_excecao_exames.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.grd_excecao_exames.Name = "grd_excecao_exames";
            this.grd_excecao_exames.ReadOnly = true;
            this.grd_excecao_exames.Size = new System.Drawing.Size(659, 146);
            this.grd_excecao_exames.TabIndex = 3;
            // 
            // btn_excluirExcecao
            // 
            this.btn_excluirExcecao.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_excluirExcecao.Image = global::SWS.Properties.Resources.lixeira;
            this.btn_excluirExcecao.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btn_excluirExcecao.Location = new System.Drawing.Point(384, 356);
            this.btn_excluirExcecao.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.btn_excluirExcecao.Name = "btn_excluirExcecao";
            this.btn_excluirExcecao.Size = new System.Drawing.Size(100, 28);
            this.btn_excluirExcecao.TabIndex = 6;
            this.btn_excluirExcecao.Text = "&Excluir";
            this.btn_excluirExcecao.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btn_excluirExcecao.UseVisualStyleBackColor = true;
            this.btn_excluirExcecao.Click += new System.EventHandler(this.btn_excluirExcecao_Click);
            // 
            // btn_incluirExcecao
            // 
            this.btn_incluirExcecao.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_incluirExcecao.Image = global::SWS.Properties.Resources.fechar_ico;
            this.btn_incluirExcecao.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btn_incluirExcecao.Location = new System.Drawing.Point(276, 356);
            this.btn_incluirExcecao.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.btn_incluirExcecao.Name = "btn_incluirExcecao";
            this.btn_incluirExcecao.Size = new System.Drawing.Size(100, 28);
            this.btn_incluirExcecao.TabIndex = 5;
            this.btn_incluirExcecao.Text = "&Incluir";
            this.btn_incluirExcecao.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btn_incluirExcecao.UseVisualStyleBackColor = true;
            this.btn_incluirExcecao.Click += new System.EventHandler(this.btn_incluirExcecao_Click);
            // 
            // grb_exame
            // 
            this.grb_exame.Controls.Add(this.text_ghe);
            this.grb_exame.Controls.Add(this.lbl_ghe);
            this.grb_exame.Controls.Add(this.lbl_cliente);
            this.grb_exame.Controls.Add(this.text_cliente);
            this.grb_exame.Controls.Add(this.text_idadeExame);
            this.grb_exame.Controls.Add(this.lbl_idadeExame);
            this.grb_exame.Controls.Add(this.btn_exame);
            this.grb_exame.Controls.Add(this.text_descricao);
            this.grb_exame.Controls.Add(this.lbl_descricao);
            this.grb_exame.Location = new System.Drawing.Point(16, 2);
            this.grb_exame.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.grb_exame.Name = "grb_exame";
            this.grb_exame.Padding = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.grb_exame.Size = new System.Drawing.Size(927, 170);
            this.grb_exame.TabIndex = 0;
            this.grb_exame.TabStop = false;
            this.grb_exame.Text = "Exame";
            // 
            // text_ghe
            // 
            this.text_ghe.BackColor = System.Drawing.Color.White;
            this.text_ghe.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.text_ghe.Enabled = false;
            this.text_ghe.ForeColor = System.Drawing.Color.Black;
            this.text_ghe.Location = new System.Drawing.Point(99, 54);
            this.text_ghe.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.text_ghe.MaxLength = 100;
            this.text_ghe.Name = "text_ghe";
            this.text_ghe.ReadOnly = true;
            this.text_ghe.Size = new System.Drawing.Size(707, 22);
            this.text_ghe.TabIndex = 13;
            // 
            // lbl_ghe
            // 
            this.lbl_ghe.AutoSize = true;
            this.lbl_ghe.Location = new System.Drawing.Point(8, 58);
            this.lbl_ghe.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lbl_ghe.Name = "lbl_ghe";
            this.lbl_ghe.Size = new System.Drawing.Size(38, 17);
            this.lbl_ghe.TabIndex = 12;
            this.lbl_ghe.Text = "GHE";
            // 
            // lbl_cliente
            // 
            this.lbl_cliente.AutoSize = true;
            this.lbl_cliente.Location = new System.Drawing.Point(8, 20);
            this.lbl_cliente.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lbl_cliente.Name = "lbl_cliente";
            this.lbl_cliente.Size = new System.Drawing.Size(51, 17);
            this.lbl_cliente.TabIndex = 11;
            this.lbl_cliente.Text = "Cliente";
            // 
            // text_cliente
            // 
            this.text_cliente.BackColor = System.Drawing.Color.White;
            this.text_cliente.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.text_cliente.Enabled = false;
            this.text_cliente.ForeColor = System.Drawing.Color.Black;
            this.text_cliente.Location = new System.Drawing.Point(99, 17);
            this.text_cliente.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.text_cliente.MaxLength = 100;
            this.text_cliente.Name = "text_cliente";
            this.text_cliente.ReadOnly = true;
            this.text_cliente.Size = new System.Drawing.Size(707, 22);
            this.text_cliente.TabIndex = 10;
            // 
            // text_idadeExame
            // 
            this.text_idadeExame.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.text_idadeExame.Enabled = false;
            this.text_idadeExame.ForeColor = System.Drawing.Color.Blue;
            this.text_idadeExame.Location = new System.Drawing.Point(299, 130);
            this.text_idadeExame.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.text_idadeExame.MaxLength = 3;
            this.text_idadeExame.Name = "text_idadeExame";
            this.text_idadeExame.Size = new System.Drawing.Size(49, 22);
            this.text_idadeExame.TabIndex = 2;
            this.text_idadeExame.Text = "0";
            this.text_idadeExame.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.text_idadeExame.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.text_idadeExame_KeyPress);
            // 
            // lbl_idadeExame
            // 
            this.lbl_idadeExame.AutoSize = true;
            this.lbl_idadeExame.Location = new System.Drawing.Point(8, 134);
            this.lbl_idadeExame.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lbl_idadeExame.Name = "lbl_idadeExame";
            this.lbl_idadeExame.Size = new System.Drawing.Size(286, 17);
            this.lbl_idadeExame.TabIndex = 2;
            this.lbl_idadeExame.Text = "Idade mínima para realizar o exame no GHE";
            // 
            // btn_exame
            // 
            this.btn_exame.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_exame.Image = global::SWS.Properties.Resources.lupa;
            this.btn_exame.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btn_exame.Location = new System.Drawing.Point(815, 92);
            this.btn_exame.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.btn_exame.Name = "btn_exame";
            this.btn_exame.Size = new System.Drawing.Size(100, 28);
            this.btn_exame.TabIndex = 1;
            this.btn_exame.Text = "&Exame";
            this.btn_exame.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btn_exame.UseVisualStyleBackColor = true;
            this.btn_exame.Click += new System.EventHandler(this.btn_exame_Click);
            // 
            // text_descricao
            // 
            this.text_descricao.BackColor = System.Drawing.Color.White;
            this.text_descricao.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.text_descricao.ForeColor = System.Drawing.SystemColors.MenuHighlight;
            this.text_descricao.Location = new System.Drawing.Point(99, 94);
            this.text_descricao.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.text_descricao.MaxLength = 100;
            this.text_descricao.Name = "text_descricao";
            this.text_descricao.ReadOnly = true;
            this.text_descricao.Size = new System.Drawing.Size(707, 22);
            this.text_descricao.TabIndex = 1;
            this.text_descricao.TabStop = false;
            // 
            // lbl_descricao
            // 
            this.lbl_descricao.AutoSize = true;
            this.lbl_descricao.Location = new System.Drawing.Point(8, 97);
            this.lbl_descricao.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lbl_descricao.Name = "lbl_descricao";
            this.lbl_descricao.Size = new System.Drawing.Size(71, 17);
            this.lbl_descricao.TabIndex = 0;
            this.lbl_descricao.Text = "Descrição";
            // 
            // grbSituacaoExame
            // 
            this.grbSituacaoExame.Controls.Add(this.chkEletricidade);
            this.grbSituacaoExame.Controls.Add(this.chkEspacoConfinado);
            this.grbSituacaoExame.Controls.Add(this.chkTrabalhoAltura);
            this.grbSituacaoExame.Location = new System.Drawing.Point(16, 181);
            this.grbSituacaoExame.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.grbSituacaoExame.Name = "grbSituacaoExame";
            this.grbSituacaoExame.Padding = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.grbSituacaoExame.Size = new System.Drawing.Size(252, 117);
            this.grbSituacaoExame.TabIndex = 7;
            this.grbSituacaoExame.TabStop = false;
            this.grbSituacaoExame.Text = "Tipo de Exame";
            // 
            // chkEspacoConfinado
            // 
            this.chkEspacoConfinado.AutoSize = true;
            this.chkEspacoConfinado.Location = new System.Drawing.Point(12, 53);
            this.chkEspacoConfinado.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.chkEspacoConfinado.Name = "chkEspacoConfinado";
            this.chkEspacoConfinado.Size = new System.Drawing.Size(226, 21);
            this.chkEspacoConfinado.TabIndex = 2;
            this.chkEspacoConfinado.Text = "Trabalho em espaço confinado";
            this.tpExamePcmoIncluir.SetToolTip(this.chkEspacoConfinado, "Marque essa opção somente no caso do exame for caracterizado\r\npara trabalho em es" +
        "paço confinado.");
            this.chkEspacoConfinado.UseVisualStyleBackColor = true;
            this.chkEspacoConfinado.CheckedChanged += new System.EventHandler(this.chkEspacoConfinado_CheckedChanged);
            // 
            // chkTrabalhoAltura
            // 
            this.chkTrabalhoAltura.AutoSize = true;
            this.chkTrabalhoAltura.Location = new System.Drawing.Point(12, 23);
            this.chkTrabalhoAltura.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.chkTrabalhoAltura.Name = "chkTrabalhoAltura";
            this.chkTrabalhoAltura.Size = new System.Drawing.Size(151, 21);
            this.chkTrabalhoAltura.TabIndex = 1;
            this.chkTrabalhoAltura.Text = "Trabalho em Altura";
            this.tpExamePcmoIncluir.SetToolTip(this.chkTrabalhoAltura, "Marque essa opçao somente no caso do exame for caracterizado\r\npara trabalho em al" +
        "tura. ");
            this.chkTrabalhoAltura.UseVisualStyleBackColor = true;
            this.chkTrabalhoAltura.CheckedChanged += new System.EventHandler(this.chkTrabalhoAltura_CheckedChanged);
            // 
            // tpExamePcmoIncluir
            // 
            this.tpExamePcmoIncluir.AutoPopDelay = 6000;
            this.tpExamePcmoIncluir.InitialDelay = 500;
            this.tpExamePcmoIncluir.ReshowDelay = 100;
            // 
            // chkEletricidade
            // 
            this.chkEletricidade.AutoSize = true;
            this.chkEletricidade.Location = new System.Drawing.Point(11, 82);
            this.chkEletricidade.Margin = new System.Windows.Forms.Padding(4);
            this.chkEletricidade.Name = "chkEletricidade";
            this.chkEletricidade.Size = new System.Drawing.Size(194, 21);
            this.chkEletricidade.TabIndex = 3;
            this.chkEletricidade.Text = "Trabalho com eletricidade";
            this.tpExamePcmoIncluir.SetToolTip(this.chkEletricidade, "Marque essa opção somente no caso do exame for caracterizado\r\npara trabalho em tr" +
        "abalho com eletricidade");
            this.chkEletricidade.UseVisualStyleBackColor = true;
            // 
            // frm_PcmsoExameIncluir
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(957, 690);
            this.ControlBox = false;
            this.Controls.Add(this.grbSituacaoExame);
            this.Controls.Add(this.btn_excluirExcecao);
            this.Controls.Add(this.grb_excecao_exames);
            this.Controls.Add(this.btn_incluirExcecao);
            this.Controls.Add(this.grb_paginacao);
            this.Controls.Add(this.grb_periodicidade);
            this.Controls.Add(this.grb_exame);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(5, 5, 5, 5);
            this.Name = "frm_PcmsoExameIncluir";
            this.Text = "INCLUIR EXAMES NO PCMSO";
            this.grb_paginacao.ResumeLayout(false);
            this.grb_periodicidade.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.grd_periodicidade)).EndInit();
            this.grb_excecao_exames.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.grd_excecao_exames)).EndInit();
            this.grb_exame.ResumeLayout(false);
            this.grb_exame.PerformLayout();
            this.grbSituacaoExame.ResumeLayout(false);
            this.grbSituacaoExame.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox grb_exame;
        private System.Windows.Forms.Button btn_exame;
        private System.Windows.Forms.Label lbl_descricao;
        private System.Windows.Forms.GroupBox grb_periodicidade;
        private System.Windows.Forms.GroupBox grb_paginacao;
        private System.Windows.Forms.Button btn_fechar;
        private System.Windows.Forms.Button btn_incluir;
        public System.Windows.Forms.TextBox text_descricao;
        private System.Windows.Forms.DataGridView grd_periodicidade;
        private System.Windows.Forms.Label lbl_idadeExame;
        public System.Windows.Forms.TextBox text_idadeExame;
        private System.Windows.Forms.GroupBox grb_excecao_exames;
        private System.Windows.Forms.DataGridView grd_excecao_exames;
        private System.Windows.Forms.Button btn_excluirExcecao;
        private System.Windows.Forms.Button btn_incluirExcecao;
        private System.Windows.Forms.TextBox text_ghe;
        private System.Windows.Forms.Label lbl_ghe;
        private System.Windows.Forms.Label lbl_cliente;
        private System.Windows.Forms.TextBox text_cliente;
        private System.Windows.Forms.GroupBox grbSituacaoExame;
        private System.Windows.Forms.CheckBox chkEspacoConfinado;
        private System.Windows.Forms.ToolTip tpExamePcmoIncluir;
        private System.Windows.Forms.CheckBox chkTrabalhoAltura;
        private System.Windows.Forms.CheckBox chkEletricidade;
    }
}