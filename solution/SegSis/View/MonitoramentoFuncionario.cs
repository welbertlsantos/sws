﻿using SWS.Entidade;
using SWS.Facade;
using SWS.View.ViewHelper;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace SWS.View
{
    public partial class frmMonitoramentoFuncionario : frmTemplate
    {
        protected Monitoramento monitoramento;
        protected DateTime periodo;

        public Monitoramento Monitoramento
        {
            get { return monitoramento; }
            set { monitoramento = value; }
        }
        protected bool validaDados = true;

        public bool ValidaDados
        {
            get { return validaDados; }
            set { validaDados = value; }
        }
        protected Estudo pcmso = null;
        protected List<ClienteFuncaoFuncionario> funcoesFuncionarioInCliente = new List<ClienteFuncaoFuncionario>();
        protected ClienteFuncaoFuncionario clienteFuncaoFuncionario = null;
        protected List<Ghe> ghesInPcmso = new List<Ghe>();
        protected List<GheSetor> ghesSetores = new List<GheSetor>();
        protected List<GheSetorClienteFuncao> gheSetorClienteFuncaoList = new List<GheSetorClienteFuncao>();
        protected List<GheFonte> gheFontes = new List<GheFonte>();
        protected List<GheFonteAgente> gheFonteAgentes = new List<GheFonteAgente>();
        protected List<MonitoramentoGheFonteAgente> monitoramentoGheFonteAgentes = new List<MonitoramentoGheFonteAgente>();
        protected List<EpiMonitoramento> monitoramentoEpis = new List<EpiMonitoramento>();
        
        protected Usuario responsavel = null;

        public frmMonitoramentoFuncionario() { InitializeComponent(); }

        public frmMonitoramentoFuncionario(ClienteFuncaoFuncionario clienteFuncaoFuncionario, DateTime periodo)
        {
            InitializeComponent();
            this.ControlBox = false;
            this.clienteFuncaoFuncionario = clienteFuncaoFuncionario;
            montaDadosTela();
            this.periodo = periodo;
            
            if (!validaDados) btnFechar.PerformClick();

            /* desabilitando os controles */

            grbMonitoramentoAgente.Enabled = false;
            flpMonitoramentoAgente.Enabled = false;
        }

        protected virtual void montaDadosTela()
        {
            try
            {
                textNome.Text = clienteFuncaoFuncionario.Funcionario.Nome;
                textCpf.Text = clienteFuncaoFuncionario.Funcionario.Cpf;
                textCliente.Text = clienteFuncaoFuncionario.ClienteFuncao.Cliente.RazaoSocial;
                /* cliente pessoa fisica = 2, pessoa juridica = 1 */
                if ((int)clienteFuncaoFuncionario.ClienteFuncao.Cliente.tipoCliente() == 1)
                {
                    textCnpj.Text = clienteFuncaoFuncionario.ClienteFuncao.Cliente.Cnpj;
                }
                else
                {
                    textCnpj.Mask = "999,999,999-99";
                    textCnpj.Text = clienteFuncaoFuncionario.ClienteFuncao.Cliente.Cnpj;
                }
                

                /* recuperando a função do funcionário */
                FuncionarioFacade funcionarioFacade = FuncionarioFacade.getInstance();

                funcoesFuncionarioInCliente = funcionarioFacade.findClienteFuncaoFuncionarioByFuncionarioAndSituacao(clienteFuncaoFuncionario.Funcionario, true);
                if (funcoesFuncionarioInCliente.Count == 0)
                    throw new Exception("O funcionário não está ativo em nenhuma função. O processo não pode continuar");

                clienteFuncaoFuncionario = funcoesFuncionarioInCliente.Find(x => x.ClienteFuncao.Cliente.Id == clienteFuncaoFuncionario.ClienteFuncao.Cliente.Id && x.DataDesligamento == null);
                if (clienteFuncaoFuncionario == null) throw new Exception("O funcionário não foi encontrando em nenhuma função ativada no cliente.");
                    
                textFuncao.Text = clienteFuncaoFuncionario.ClienteFuncao.Funcao.Descricao;

                /* encontrando o pcmso do cliente para continuar */

                EstudoFacade pcmsoFacade = EstudoFacade.getInstance();

                List<Estudo> pcmsos = pcmsoFacade.findLastPcmsoByCliente(clienteFuncaoFuncionario.ClienteFuncao.Cliente).ToList();

                if (pcmsos.Count == 0) throw new Exception("Não foi encontrando nenhum pcmso ativo para o cliente. O processo não pode continuar.");
                if (pcmsos.Count > 1)
                {
                    if (MessageBox.Show("Foi encontrado mais de um pcmso ativo para o cliente. Selecione o pcmso na próxima tela.", "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Question) == DialogResult.OK)
                    {
                        frmAtendimentoPcmso selecionarPcmso = new frmAtendimentoPcmso(clienteFuncaoFuncionario);
                        selecionarPcmso.ShowDialog();

                        if (selecionarPcmso.Pcmso != null)
                        {
                            pcmso = selecionarPcmso.Pcmso;
                            textPcmso.Text = pcmso.CodigoEstudo;
                        }
                        else throw new Exception("Você deve selecionar o pcmso para continuar o processo.");
                    }
                }
                else
                {
                    pcmso = pcmsos[0];
                    textPcmso.Text = pcmso.CodigoEstudo;
                }

                /* verificando agora o ghe que o funcionário se encontra */
                /* removendo itens duplicados */

                ghesInPcmso = pcmsoFacade.findAllGheInPcmsoByClienteFuncao(clienteFuncaoFuncionario.ClienteFuncao, pcmso).Distinct().ToList();



                if (ghesInPcmso.Count == 0) throw new Exception("A função " + 
                    clienteFuncaoFuncionario.ClienteFuncao.Funcao.Descricao + 
                    "[" + clienteFuncaoFuncionario.ClienteFuncao.Funcao.Id + "]" + " não foi encontrada em nenhum GHE. O processo não poderá continuar.");
                
                /* encontrando o ghe e montando a grid com informações */
                findGheSetor();

                /* encontrar o(s) gheFonte(s) que o colaborador está exposto */
                findGheFonte();

                /* encontrando os agentes / Riscos de acordo com a lista de GhesFontes encontrados */
                findGheFonteAgente();

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
                validaDados = false;
            }
            
        }

        protected virtual void btnGravar_Click(object sender, EventArgs e)
        {
            try
            {
                if (!validacoesMonitoramento())
                {
                    EsocialFacade esocialFacade = EsocialFacade.getInstance();
                    Monitoramento newMonitoramento = new Monitoramento(null, null, this.clienteFuncaoFuncionario, this.responsavel, Convert.ToDateTime(dataInicioMonitoramento.Text), textObservacao.Text, clienteFuncaoFuncionario.ClienteFuncao.Cliente, periodo, pcmso);
                    if (dataFimMonitormento.Checked)
                        newMonitoramento.DataFimCondicao = Convert.ToDateTime(dataFimMonitormento.Text);
                    
                    this.Monitoramento = esocialFacade.insertMonitoramento(newMonitoramento);

                    MessageBox.Show("Monitoramento do funcionário gravado com sucesso.", "Confirmação", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    grbMonitoramentoAgente.Enabled = true;
                    flpMonitoramentoAgente.Enabled = true;
                    btnGravar.Enabled = false;
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }

        protected virtual void btnFechar_Click(object sender, EventArgs e)
        {
            try
            {
                /* verificar se já foi feito algum monitoramento para o agente */

                if (this.monitoramentoGheFonteAgentes.Count == 0)
                    throw new Exception("Deve existir pelo menos um agente que tenha sido monitorado.");

                this.Close();
                
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        protected virtual void btnIncluirResponsavel_Click(object sender, EventArgs e)
        {
            try
            {
                frmUsuarioTecnicoSelecionar selecionarResponsavel = new frmUsuarioTecnicoSelecionar();
                selecionarResponsavel.ShowDialog();

                if (selecionarResponsavel.Usuario != null)
                {
                    this.responsavel = selecionarResponsavel.Usuario;
                    textResponsavel.Text = responsavel.Nome;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }


        }

        protected virtual void btnExcluirResponsavel_Click(object sender, EventArgs e)
        {
            try
            {
                if (responsavel == null) throw new Exception("Você deve primeiro selecionar o responsável para depois excluí-lo");
                responsavel = null;
                textResponsavel.Text = string.Empty;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        protected void btnIncluirMonitoramentoAgente_Click(object sender, EventArgs e)
        {
            try
            {
                /* inclusão do monitoramento */
                GheFonteAgente gheFonteAgenteSelecionado = gheFonteAgentes.Find(x => x.Id == (long)dgvRisco.CurrentRow.Cells[0].Value);
                if (string.IsNullOrEmpty(gheFonteAgenteSelecionado.Agente.CodigoEsocial))
                    throw new Exception("Você deve informar no cadastro do agente o código do E-Social correspondente");

                /* verificando se ja existe um monitoramento para esse agente */

                EsocialFacade esocialFacade = EsocialFacade.getInstance();
                if (esocialFacade.findMonitoramentoGheFonteAgenteByGheFonteAgente(gheFonteAgenteSelecionado, this.monitoramento) != null)
                    throw new Exception("Já existe um monitoramento para esse agente ");

                /* somente deverá ser incluído o monitoramento do agente caso o agente seja diferente de 09.01.001 */

                if (string.Equals(gheFonteAgenteSelecionado.Agente.CodigoEsocial, "09.01.001"))
                {
                    MessageBox.Show("Esse agente não é necessário realizar monitoramento. Risco não identificado de acordo com a tabela 24 do E-Social. Será incluído apenas um monitoramento vazio.", "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    this.monitoramentoGheFonteAgentes.Add(esocialFacade.insertMonitoramentoGheFonteAgente(new MonitoramentoGheFonteAgente(null, gheFonteAgenteSelecionado, monitoramento, string.Empty, null, null, string.Empty, string.Empty, string.Empty, string.Empty, string.Empty, string.Empty, string.Empty, string.Empty, string.Empty, string.Empty, string.Empty, string.Empty, string.Empty)));
                    MessageBox.Show("Monitoramento agente incluído com sucesso.", "Confirmação", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    this.montaGridMonitoramentoAgente();
                    
                }
                else 
                {
                    frmMonitoramentoAgente incluirMonitoramentoAgente = new frmMonitoramentoAgente(gheFonteAgenteSelecionado, this.Monitoramento);
                    incluirMonitoramentoAgente.ShowDialog();

                    if (incluirMonitoramentoAgente.MonitoramentoGheFonteAgente != null)
                    {
                        monitoramentoGheFonteAgentes.Add(incluirMonitoramentoAgente.MonitoramentoGheFonteAgente);
                        MessageBox.Show("Monitoramento agente incluído com sucesso.", "Confirmação", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        montaGridMonitoramentoAgente();
                    }
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }

        protected virtual void btnAlterarMonitoramentoAgente_Click(object sender, EventArgs e)
        {
            try
            {
                if (dgvMonitoramentoAgente.CurrentRow == null)
                    throw new Exception("Selecione uma monitoramento do agente para alterar");

                /* recuperando o monitoramento de acordo com o index da grid */

                MonitoramentoGheFonteAgente monitoramentoAgenteAlterar = monitoramentoGheFonteAgentes.Find(x => x.Id == (long)dgvMonitoramentoAgente.CurrentRow.Cells[0].Value);

                /* se o agente selecionado for um não identificado, não é necessario ter monitoramento */

                if (monitoramentoAgenteAlterar != null && string.Equals(monitoramentoAgenteAlterar.GheFonteAgente.Agente.CodigoEsocial, "09.01.001"))
                    throw new Exception("Não é possível alterar o monitoramento para risco não identificado de acordo com a tabela do E-Social.");

                frmMonitoramentoAgenteAlterar alterarMonitoramento = new frmMonitoramentoAgenteAlterar(monitoramentoAgenteAlterar);
                alterarMonitoramento.ShowDialog();
                /* removendo a lista e incluindo novamente */
                monitoramentoGheFonteAgentes.Remove(monitoramentoAgenteAlterar);
                /* incluindo o novo elemento */
                monitoramentoGheFonteAgentes.Add(alterarMonitoramento.MonitoramentoGheFonteAgente);
                montaGridMonitoramentoAgente();

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        protected void btnExcluirMonitoramentoAgente_Click(object sender, EventArgs e)
        {
            try
            {
                if (this.dgvMonitoramentoAgente.CurrentRow == null)
                    throw new Exception("Selecione um monitoramento para excluír");

                if (MessageBox.Show("Deseja excluir o Monitoramento Agente Selecionado", "Atenção", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                {
                    /* recuperar da lista o elemento selecionado */

                    MonitoramentoGheFonteAgente monitoramentoAgenteExcluir = monitoramentoGheFonteAgentes.Find(x => x.Id == (long)dgvMonitoramentoAgente.CurrentRow.Cells[0].Value);

                    if (monitoramentoAgenteExcluir == null)
                        throw new Exception("Monitoramento não encontrado para exclusão");

                    EsocialFacade esocialFacade = EsocialFacade.getInstance();
                    esocialFacade.deleteMonitoramentoGheFonteAgente(monitoramentoAgenteExcluir);
                    monitoramentoGheFonteAgentes.Remove(monitoramentoAgenteExcluir);
                    montaGridMonitoramentoAgente();
                    MessageBox.Show("Monitoramento Agente excluído com sucesso", "Confirmação", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }

        protected void findGheSetor()
        {
            try
            {
                EstudoFacade pcmsoFacade = EstudoFacade.getInstance();
                
                if (ghesInPcmso.Count > 1)
                {
                    MessageBox.Show("O função que o funcionário está lotado se encontra em mais de um GHE. Você deve selecionar o(s) GHE(s).", "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Information);

                    /* iniciando a busca por setores do GHE */
                    ghesSetores.Clear();
                    List<GheSetor> ghesInTemp = new List<GheSetor>();

                    foreach (Ghe ghe in ghesInPcmso)
                    {
                        ghesInTemp = pcmsoFacade.findAllGheSetor(ghe).ToList();

                        foreach (GheSetor gheSetor in ghesInTemp)
                        {
                            ghesSetores.Add(gheSetor);
                        }
                    }

                    /* excluindo os setores que não tem a função presente */

                    List<GheSetor> gheSetorTemp = new List<GheSetor>();

                    foreach (GheSetor gs in ghesSetores)
                    {
                        GheSetorClienteFuncao gheSetorClienteFuncaoAuxiliar = null;
                        List<GheSetorClienteFuncao> gheSetorClienteFuncaoTemp = pcmsoFacade.findAllGheSetorClienteFuncao(gs).ToList();

                        gheSetorClienteFuncaoAuxiliar = gheSetorClienteFuncaoTemp.Find(x => x.ClienteFuncao.Id == clienteFuncaoFuncionario.ClienteFuncao.Id);

                        if (gheSetorClienteFuncaoAuxiliar != null)
                        {
                            gheSetorTemp.Add(gheSetorClienteFuncaoAuxiliar.GheSetor);
                        }
                    }
                    
                    /* igualando as listas */
                    ghesSetores = gheSetorTemp;
                    montaDataGridGheSetor(false);

                }
                else
                {
                    /* existe apenas um ghe para a função */

                    ghesSetores = pcmsoFacade.findAllGheSetor(ghesInPcmso[0]).ToList();
                    /* montando a grid com opção para seleção */

                    /* excluindo os setores que não tem a função presente */

                    List<GheSetor> gheSetorTemp = new List<GheSetor>();

                    foreach (GheSetor gs in ghesSetores)
                    {
                        GheSetorClienteFuncao gheSetorClienteFuncaoAuxiliar = null;
                        List<GheSetorClienteFuncao> gheSetorClienteFuncaoTemp = pcmsoFacade.findAllGheSetorClienteFuncao(gs).ToList();

                        gheSetorClienteFuncaoAuxiliar = gheSetorClienteFuncaoTemp.Find(x => x.ClienteFuncao.Id == clienteFuncaoFuncionario.ClienteFuncao.Id);

                        if (gheSetorClienteFuncaoAuxiliar != null)
                        {
                            gheSetorTemp.Add(gheSetorClienteFuncaoAuxiliar.GheSetor);
                        }
                    }

                    /* igualando as listas */
                    ghesSetores = gheSetorTemp;
                    montaDataGridGheSetor(true);
                    
                    
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }

        protected void montaDataGridGheSetor(bool marcar)
        {
            try
            {
                /* caso a função tenha mais de um gheSetor então será necessário selecionar os setores
                 * que o funcionário está lotado */

                if (ghesSetores.Count > 1)
                {
                    MessageBox.Show("A função foi encontrada em mais de um setor dentro do mesmo ghe. Você deve selecionar em quais setores a função se encontra. Lembrando que caso ela esteja em mais de um setor será realizado a soma dos riscos.", "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Information);

                    dgvGhe.Columns.Clear();
                    dgvGhe.ColumnCount = 5;

                    dgvGhe.Columns[0].HeaderText = "idGheSetor";
                    dgvGhe.Columns[0].Visible = false;

                    dgvGhe.Columns[1].HeaderText = "idGhe";
                    dgvGhe.Columns[1].Visible = false;

                    dgvGhe.Columns[2].HeaderText = "Ghe";
                    dgvGhe.Columns[2].ReadOnly = true;

                    dgvGhe.Columns[3].HeaderText = "idSetor";
                    dgvGhe.Columns[3].Visible = false;

                    dgvGhe.Columns[4].HeaderText = "Setor";
                    dgvGhe.Columns[4].ReadOnly = true;

                    DataGridViewCheckBoxColumn checkBox = new DataGridViewCheckBoxColumn();
                    checkBox.Name = "Seleconar";
                    dgvGhe.Columns.Add(checkBox);
                    dgvGhe.Columns[5].DisplayIndex = 0;

                    foreach (GheSetor gs in ghesSetores)
                        dgvGhe.Rows.Add(
                            gs.Id,
                            gs.Ghe.Id,
                            gs.Ghe.Descricao,
                            gs.Setor.Id,
                            gs.Setor.Descricao,
                            false);
                }
                else
                {

                    dgvGhe.Columns.Clear();
                    dgvGhe.ColumnCount = 5;

                    dgvGhe.Columns[0].HeaderText = "idGheSetor";
                    dgvGhe.Columns[0].Visible = false;

                    dgvGhe.Columns[1].HeaderText = "idGhe";
                    dgvGhe.Columns[1].Visible = false;

                    dgvGhe.Columns[2].HeaderText = "Ghe";
                    dgvGhe.Columns[2].ReadOnly = true;

                    dgvGhe.Columns[3].HeaderText = "idSetor";
                    dgvGhe.Columns[3].Visible = false;

                    dgvGhe.Columns[4].HeaderText = "Setor";
                    dgvGhe.Columns[4].ReadOnly = true;

                    DataGridViewCheckBoxColumn checkBox = new DataGridViewCheckBoxColumn();
                    checkBox.Name = "Seleconar";
                    dgvGhe.Columns.Add(checkBox);
                    dgvGhe.Columns[5].DisplayIndex = 0;

                    foreach (GheSetor gs in ghesSetores)
                        dgvGhe.Rows.Add(
                            gs.Id,
                            gs.Ghe.Id,
                            gs.Ghe.Descricao,
                            gs.Setor.Id,
                            gs.Setor.Descricao,
                            marcar);

                    dgvGhe.ReadOnly = marcar;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }

        protected void findGheFonte()
        {
            try
            {
                EstudoFacade pcmsoFacade = EstudoFacade.getInstance();
                List<Ghe> ghesSelecionados = new List<Ghe>();
                gheFontes.Clear();

                foreach (DataGridViewRow dvRow in dgvGhe.Rows)
                {
                    if ((bool)dvRow.Cells[5].EditedFormattedValue == true)
                    {
                        ghesSelecionados.Add(new Ghe((long)dvRow.Cells[1].Value, pcmso, dvRow.Cells[2].Value.ToString(), 0, "A", string.Empty));
                    }
                }

                List<GheFonte> ghesFontesTemp = new List<GheFonte>();

                foreach (Ghe ghe in ghesSelecionados)
                {
                    ghesFontesTemp = pcmsoFacade.findAllGheFonteByGhe(ghe).ToList();

                    foreach (GheFonte gheFonte in ghesFontesTemp)
                    {
                        gheFontes.Add(gheFonte);
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        protected void findGheFonteAgente()
        {
            try
            {
                EstudoFacade pcmsoFacade = EstudoFacade.getInstance();
                List<GheFonteAgente> gheFonteAgenteTemp = new List<GheFonteAgente>();
                gheFonteAgentes.Clear();

                foreach (GheFonte gheFonte in gheFontes)
                {
                    gheFonteAgenteTemp = pcmsoFacade.findAllGheFonteAgenteByGheFonte(gheFonte).ToList();

                    foreach (GheFonteAgente gheFonteAgente in gheFonteAgenteTemp)
                        gheFonteAgentes.Add(gheFonteAgente);
                }

                /* agrupando por agente */
                gheFonteAgentes.OrderBy(x => x.Agente.Descricao).ThenBy(x => x.Agente.Risco.Descricao);

                montaDataGridAgente();

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        protected void montaDataGridAgente()
        {
            try
            {
                dgvRisco.Columns.Clear();

                dgvRisco.ColumnCount = 3;

                dgvRisco.Columns[0].HeaderText = "idGheFonteAgente";
                dgvRisco.Columns[0].Visible = false;

                dgvRisco.Columns[1].HeaderText = "Agente Nocivo";
                dgvRisco.Columns[2].HeaderText = "Risco";

                dgvRisco.ReadOnly = true;

                foreach (GheFonteAgente gheFonteAgente in gheFonteAgentes)
                    dgvRisco.Rows.Add(
                        gheFonteAgente.Id,
                        gheFonteAgente.Agente.Descricao,
                        gheFonteAgente.Agente.Risco.Descricao);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        protected void montaGridMonitoramentoAgente()
        {
            try
            {
                dgvMonitoramentoAgente.Columns.Clear();
                dgvMonitoramentoAgente.ColumnCount = 6;

                dgvMonitoramentoAgente.Columns[0].HeaderText = "idMonitoramentoGheFonteAgente";
                dgvMonitoramentoAgente.Columns[0].Visible = false;

                dgvMonitoramentoAgente.Columns[1].HeaderText = "Agente";
                
                dgvMonitoramentoAgente.Columns[2].HeaderText = "Tipo Avaliação";

                dgvMonitoramentoAgente.Columns[3].HeaderText = "Itensidade";
                dgvMonitoramentoAgente.Columns[4].HeaderText = "Limite/Tolerência";

                dgvMonitoramentoAgente.Columns[5].HeaderText = "Técnica de Medição";
                

                foreach (MonitoramentoGheFonteAgente monitAgente in monitoramentoGheFonteAgentes)
                {
                    dgvMonitoramentoAgente.Rows.Add(
                        monitAgente.Id,
                        monitAgente.GheFonteAgente.Agente.Descricao,
                        string.Equals(monitAgente.TipoAvaliacao, "1") ? "QUANTITATIVA" : string.Equals(monitAgente.TipoAvaliacao, "2") ? "QUALITATIVA" : string.Empty,
                        monitAgente.IntensidadeConcentracao,
                        monitAgente.LimiteTolerancia,
                        monitAgente.TecnicaMedicao);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }

        protected bool validacoesMonitoramento()
        {

            bool retorno = false;
            try
            {
                if (!dataInicioMonitoramento.Checked)
                    throw new Exception("É necessário informar a data de início da condição.");

                if (responsavel == null)
                    throw new Exception("É necessário informar o responsável pelo monitoramento.");

                if (string.IsNullOrEmpty(textObservacao.Text))
                    throw new Exception("O monitoramento deve conter alguma observação.");

                /* verificando se existe algum agente que não tenha o código do e-social */

                List<GheFonteAgente> agentesSemCodigoEsocial = gheFonteAgentes.FindAll(x => string.IsNullOrEmpty(x.Agente.CodigoEsocial));
                if (agentesSemCodigoEsocial.Count > 0)
                {
                    StringBuilder query = new StringBuilder();
                    foreach (GheFonteAgente gheFonteAgente in agentesSemCodigoEsocial)
                    {
                        query.Append("Agente : " + gheFonteAgente.Agente.Descricao + " [ " + gheFonteAgente.Agente.Id + " ] \r\n ");
                    }

                    throw new Exception("Não será permitido gravar o monitoramento pois existem agentes sem o código do E-Social " + query.ToString());
                }

                //if (Convert.ToDateTime(dataInicioMonitoramento.Text) > Convert.ToDateTime("2023/01/16"))
                //    if (!dataFimMonitormento.Checked)
                //        throw new Exception("A data final do monitoramento é obrigatória para monitoramentos realizados a partir de 16/01/2023");

                //if (Convert.ToDateTime(dataInicioMonitoramento.Text) > Convert.ToDateTime(dataFimMonitormento.Text))
                //    throw new Exception("A data fim do monitoramento não pode ser menor que a data inicio");

            }
            catch (Exception ex)
            {
                retorno = true;
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            return retorno;
        }

        protected virtual void dgvRisco_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void dgvGhe_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                if (e.ColumnIndex == 5)
                {
                    findGheFonte();
                    findGheFonteAgente();
                }
                
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }
        
    }
}
