﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using SWS.Facade;
using SWS.Entidade;
using SWS.Excecao;
using SWS.View.ViewHelper;
using System.Text.RegularExpressions;
using SWS.View.Resources;
using System.Security;
using System.IO;
using SWS.Helper;
using SWS.ViewHelper;
using SWS.Resources;
using System.Web;

namespace SWS.View
{
    public partial class frmClientesIncluir : SWS.View.frmTemplate
    {
        protected Cliente cliente;

        public Cliente Cliente
        {
            get { return cliente; }
            set { cliente = value; }
        }

        protected Cliente credenciada;

        public Cliente Credenciada
        {
            get { return credenciada; }
            set { credenciada = value; }
        }

        protected Cliente matriz;

        public Cliente Matriz
        {
            get { return matriz; }
            set { matriz = value; }
        }

        protected Ramo ramo;

        public Ramo Ramo
        {
            get { return ramo; }
            set { ramo = value; }
        }

        protected CidadeIbge cidadeComercial;

        public CidadeIbge CidadeComercial
        {
            get { return cidadeComercial; }
            set { cidadeComercial = value; }
        }

        protected CidadeIbge cidadeCobranca;

        public CidadeIbge CidadeCobranca
        {
            get { return cidadeCobranca; }
            set { cidadeCobranca = value; }
        }

        protected List<ClienteCnae> cnaes = new List<ClienteCnae>();

        public List<ClienteCnae> Cnaes
        {
            get { return cnaes; }
            set { cnaes = value; }
        }

        protected Arquivo arquivo;

        public Arquivo Arquivo
        {
            get { return arquivo; }
            set { arquivo = value; }
        }

        protected ClienteArquivo clienteArquivo;

        public ClienteArquivo ClienteArquivo
        {
            get { return clienteArquivo; }
            set { clienteArquivo = value; }
        }

        protected List<VendedorCliente> vendedores = new List<VendedorCliente>();

        public List<VendedorCliente> Vendedores
        {
            get { return vendedores; }
            set { vendedores = value; }
        }

        protected List<ClienteFuncao> funcoes = new List<ClienteFuncao>();

        public List<ClienteFuncao> Funcoes
        {
            get { return funcoes; }
            set { funcoes = value; }
        }

        protected List<Contato> contatos = new List<Contato>();

        public List<Contato> Contatos
        {
            get { return contatos; }
            set { contatos = value; }
        }

        protected List<CentroCusto> centroCustoCliente = new List<CentroCusto>();

        public List<CentroCusto> CentroCustoCliente
        {
            get { return centroCustoCliente; }
            set { centroCustoCliente = value; }
        }

        protected string aliquotaIss = String.Empty;

        protected TabPage[] tpEnderecoCopy;

        public frmClientesIncluir()
        {
            InitializeComponent();
            ComboHelper.comboTipoCliente(cbTipoCliente);
            ComboHelper.Boolean(cbClienteVIP, false);
            ComboHelper.Boolean(cbContrato, false);
            ComboHelper.Boolean(cbParticular, false);
            ComboHelper.Boolean(cbPrestador, false);
            ComboHelper.Boolean(cbValorLiquido, false);
            ComboHelper.unidadeFederativa(cbUFComercial);
            ComboHelper.unidadeFederativa(cbUFCobranca);
            ComboHelper.Boolean(cbCentroCusto, false);
            ComboHelper.Boolean(cbBloqueado, false);
            ComboHelper.Boolean(cbCredenciadora, false);
            ComboHelper.Boolean(cbUsaPo, false);
            ComboHelper.Boolean(cbSimplesNacional, false);

            /* salvando tabpage de endereco de cobranca */
            tpEnderecoCopy = new TabPage[] { tpComercial, tpCobranca };

            ActiveControl = textRazaoSocial;
            this.MaximizeBox = true;
            this.WindowState = FormWindowState.Maximized;
        }

        protected virtual void btVendedor_Click(object sender, EventArgs e)
        {
            try
            {
                frmVendedorBuscar vendedorSelecionar = new frmVendedorBuscar();
                vendedorSelecionar.ShowDialog();

                if (vendedorSelecionar.Vendedor != null)
                {
                    vendedores.Add(new VendedorCliente(null, cliente, vendedorSelecionar.Vendedor, ApplicationConstants.ATIVO));
                    textVendedor.Text = vendedorSelecionar.Vendedor.Nome;

                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }

        protected void btFechar_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        protected void btLimpar_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Deseja excluir todos os dados da tela e voltar as informações default?", "Atenção", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
            {
                ComboHelper.comboTipoCliente(cbTipoCliente);
                vendedores.Clear();
                textVendedor.Text = String.Empty;
                Matriz = null;
                textMatriz.Text = String.Empty;
                credenciada = null;
                textCredenciada.Text = String.Empty;
                textRazaoSocial.Text = String.Empty;
                textCNPJ.Text = String.Empty;
                textFantasia.Text = String.Empty;
                textInscricao.Text = String.Empty;
                textEmail.Text = String.Empty;
                textSite.Text = String.Empty;
                ramo = null;
                textRamo.Text = String.Empty;
                textJornada.Text = String.Empty;
                textEscopo.Text = String.Empty;
                textLogradouroComercial.Text = String.Empty;
                textNumeroComercial.Text = String.Empty;
                textComplementoComercial.Text = String.Empty;
                textBairroComercial.Text = String.Empty;
                textCEPComercial.Text = String.Empty;
                ComboHelper.unidadeFederativa(cbUFComercial);
                CidadeComercial = null;
                textCidadeComercial.Text = String.Empty;
                textTelefoneComercial.Text = String.Empty;
                textFaxComercial.Text = String.Empty;
                textLogradouroCobranca.Text = String.Empty;
                textNumeroCobranca.Text = String.Empty;
                textComplementoCobranca.Text = String.Empty;
                textBairroCobranca.Text = String.Empty;
                textCEPCobranca.Text = String.Empty;
                ComboHelper.unidadeFederativa(cbUFCobranca);
                CidadeCobranca = null;
                textCidadeCobranca.Text = String.Empty;
                textTelefoneCobranca.Text = String.Empty;
                textFaxCobranca.Text = String.Empty;
                cnaes.Clear();
                dgvCnae.Columns.Clear();
                Arquivo = null;
                pbLogo.Image = null;
                pbLogo.Refresh();
                funcoes.Clear();
                dgvFuncao.Columns.Clear();
                contatos.Clear();
                dgvContatos.Columns.Clear();

                ComboHelper.Boolean(cbClienteVIP, false);
                ComboHelper.Boolean(cbContrato, false);
                ComboHelper.Boolean(cbParticular, false);
                ComboHelper.Boolean(cbPrestador, false);
                ComboHelper.Boolean(cbValorLiquido, false);
                ComboHelper.Boolean(cbCentroCusto, false);
                ComboHelper.Boolean(cbBloqueado, false);
                ComboHelper.Boolean(cbUsaPo, false);
                ComboHelper.Boolean(cbSimplesNacional, false);

                textRazaoSocial.Focus();
                textCodigoCnes.Text = string.Empty;
            }
        }

        protected virtual void btGravar_Click(object sender, EventArgs e)
        {
            try
            {

                if (validaCampos())
                {
                    ClienteFacade clienteFacade = ClienteFacade.getInstance();

                    cliente = new Cliente(null, textRazaoSocial.Text, textFantasia.Text, string.Equals(((SelectItem)cbTipoCliente.SelectedItem).Valor, ApplicationConstants.UNIDADE) ? matriz.Cnpj : textCNPJ.Text, textInscricao.Text, textLogradouroComercial.Text, textNumeroComercial.Text, textComplementoComercial.Text, textBairroComercial.Text, textCEPComercial.Text, ((SelectItem)cbUFComercial.SelectedItem).Valor.ToString(), textTelefoneComercial.Text, textFaxComercial.Text, textEmail.Text, textSite.Text, null, String.Empty, String.Empty, String.Empty, textEscopo.Text, textJornada.Text, ApplicationConstants.ATIVO, 0, 0, textLogradouroCobranca.Text, textNumeroCobranca.Text, textComplementoCobranca.Text, textBairroCobranca.Text, textCEPCobranca.Text, ((SelectItem)cbUFCobranca.SelectedItem).Valor.ToString(), textTelefoneCobranca.Text, null, textFaxCobranca.Text, Convert.ToBoolean(((SelectItem)cbClienteVIP.SelectedItem).Valor), String.Empty, Convert.ToBoolean(((SelectItem)cbContrato.SelectedItem).Valor), matriz, credenciada, Convert.ToBoolean(((SelectItem)this.cbParticular.SelectedItem).Valor), matriz != null ? true : false, credenciada != null ? true : false, Convert.ToBoolean(((SelectItem)this.cbPrestador.SelectedItem).Valor), cidadeComercial, cidadeCobranca, true, Convert.ToBoolean(((SelectItem)this.cbValorLiquido.SelectedItem).Valor), 0 , textCodigoCnes.Text, Convert.ToBoolean(((SelectItem)cbCentroCusto.SelectedItem).Valor), Convert.ToBoolean(((SelectItem)cbBloqueado.SelectedItem).Valor), Convert.ToBoolean(((SelectItem)cbCredenciadora.SelectedItem).Valor), string.Equals(((SelectItem)cbTipoCliente.SelectedItem).Valor, ApplicationConstants.FISICA) ? true : false, Convert.ToBoolean(((SelectItem)cbUsaPo.SelectedItem).Valor), Convert.ToBoolean(((SelectItem)cbSimplesNacional.SelectedItem).Valor)); 

                    /* alimentando o cadastro do cliente com informações do vendedor */
                    cliente.VendedorCliente = vendedores;

                    /* lista de cnaes selecionados */
                    cliente.ClienteCnae = cnaes;

                    /* incluindo clienteFuncao no cliente */
                    cliente.Funcoes = funcoes;

                    /* incluindo contatos do cliente */
                    cliente.Contatos = contatos;

                    /* incluindo o ramo de atividade do cliente */
                    cliente.Ramo = ramo;

                    /* incluindo os  centro de custos no cliente */
                    cliente.CentroCusto = centroCustoCliente;

                    cliente = clienteFacade.insertCliente(cliente);

                    /* incluindo a Logo */

                    if (arquivo != null)
                    {
                        arquivo = clienteFacade.incluirArquivo(arquivo);
                        clienteArquivo = new ClienteArquivo(null, arquivo, cliente, null, null, ApplicationConstants.ATIVO);
                        clienteFacade.incluirClienteArquivo(clienteArquivo);

                    }

                    MessageBox.Show("Cliente incluído com sucesso.", "Confirmaçao", MessageBoxButtons.OK, MessageBoxIcon.Information);

                    this.Close();
                }

            }
            catch (Exception Ex)
            {
                MessageBox.Show(Ex.Message, "Atençao", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }

        protected Boolean validaCampos()
        {
            Boolean retorno = true;

            try
            {
                if (cbTipoCliente.SelectedIndex == -1)
                    throw new Exception("Selecione um tipo de cliente.");

                /* verificando a validaçao necessaria por tipo de cliente */

                if (string.Equals(((SelectItem)cbTipoCliente.SelectedItem).Valor, ApplicationConstants.CREDENCIADA))
                {
                    /* cliente credenciada */

                    if (credenciada == null)
                        throw new Exception("Obrigatorio informar a empresa credenciada. ");

                    if (funcoes.Count == 0)
                        throw new Exception("Obrigatorio ter pelo menos uma funçao registrada.");

                    if (cnaes.Count == 0)
                        throw new Exception("E necessario ter pelo menos um cnae registrado.");

                    if (!cnaes.Any(x => x.Flag_pr == true))
                        throw new Exception("E necessario informar o cnae principal do cliente.");

                    if (vendedores.Count == 0)
                        throw new Exception("E necessario infomar quem é o vendedor do cliente. ");

                    if (String.IsNullOrEmpty(textLogradouroComercial.Text) ||
                        String.IsNullOrEmpty(textNumeroComercial.Text) ||
                        String.IsNullOrEmpty(textBairroComercial.Text) ||
                        String.IsNullOrEmpty(textCEPComercial.Text) ||
                        cidadeComercial == null ||
                        cbUFComercial.SelectedIndex == -1)
                        throw new Exception("Rever o endereco comercial digitado.");


                    if (String.IsNullOrEmpty(textLogradouroCobranca.Text) ||
                        String.IsNullOrEmpty(textNumeroCobranca.Text) ||
                        String.IsNullOrEmpty(textBairroCobranca.Text) ||
                        String.IsNullOrEmpty(textCEPCobranca.Text) ||
                        cidadeCobranca == null ||
                        cbUFCobranca.SelectedIndex == -1)
                        throw new Exception("Rever o endereco de cobrança digitado.");

                    if (cbClienteVIP.SelectedIndex == -1 ||
                        cbContrato.SelectedIndex == -1 ||
                        cbParticular.SelectedIndex == -1 ||
                        cbPrestador.SelectedIndex == -1 ||
                        cbValorLiquido.SelectedIndex == -1)
                        throw new Exception("Rever aba de condições.");

                    if (Convert.ToBoolean(((SelectItem)cbContrato.SelectedItem).Valor) == true)
                        throw new Exception("A empresa credenciada nao pode usar contrato.");

                    if (Convert.ToBoolean(((SelectItem)cbCentroCusto.SelectedItem).Valor) == true) 
                        throw new Exception("Cliente credenciado não poderá usar centro de custo.");


                }
                else if (string.Equals(((SelectItem)cbTipoCliente.SelectedItem).Valor, ApplicationConstants.UNIDADE))
                {
                    /* cliente unidade */

                    if (matriz == null)
                        throw new Exception("É necessario informar a matriz.");

                    if (String.IsNullOrEmpty(textLogradouroComercial.Text) ||
                        String.IsNullOrEmpty(textNumeroComercial.Text) ||
                        String.IsNullOrEmpty(textBairroComercial.Text) ||
                        String.IsNullOrEmpty(textCEPComercial.Text) ||
                        cidadeComercial == null ||
                        cbUFComercial.SelectedIndex == -1)
                        throw new Exception("Rever o endereco comercial digitado.");

                    if (Convert.ToBoolean(((SelectItem)cbCentroCusto.SelectedItem).Valor) == true)
                        throw new Exception("Cliente unidade não poderá usar centro de custo.");

                }
                else if (string.Equals(((SelectItem)cbTipoCliente.SelectedItem).Valor, ApplicationConstants.PADRAO))
                {
                    /* cliente padrao */

                    if (matriz != null || credenciada != null)
                        throw new Exception("O cliente e uma empresa padrao então não pode ser referenciado uma matriz ou uma credenciada.");

                    if (funcoes.Count == 0)
                        throw new Exception("Obrigatorio ter pelo menos uma funçao registrada.");

                    if (cnaes.Count == 0)
                        throw new Exception("É necessario ter pelo menos um cnae registrado.");

                    if (!cnaes.Any(x => x.Flag_pr == true))
                        throw new Exception("É necessario informar o cnae principal do cliente");

                    if (vendedores.Count == 0)
                        throw new Exception("É necessario infomar quem é o vendedor do cliente.");

                    if (String.IsNullOrEmpty(textLogradouroComercial.Text) ||
                        String.IsNullOrEmpty(textNumeroComercial.Text) ||
                        String.IsNullOrEmpty(textBairroComercial.Text) ||
                        String.IsNullOrEmpty(textCEPComercial.Text) ||
                        cidadeComercial == null ||
                        cbUFComercial.SelectedIndex == -1)
                        throw new Exception("Rever o endereco comercial digitado.");


                    if (String.IsNullOrEmpty(textLogradouroCobranca.Text) ||
                        String.IsNullOrEmpty(textNumeroCobranca.Text) ||
                        String.IsNullOrEmpty(textBairroCobranca.Text) ||
                        String.IsNullOrEmpty(textCEPCobranca.Text) ||
                        cidadeCobranca == null ||
                        cbUFCobranca.SelectedIndex == -1)
                        throw new Exception("Rever o endereco de cobrança digitado.");

                    if (cbClienteVIP.SelectedIndex == -1 ||
                        cbContrato.SelectedIndex == -1 ||
                        cbParticular.SelectedIndex == -1 ||
                        cbPrestador.SelectedIndex == -1 ||
                        cbValorLiquido.SelectedIndex == -1)
                        throw new Exception("Rever aba de condições.");

                    if (Convert.ToBoolean(((SelectItem)cbPrestador.SelectedItem).Valor) && string.IsNullOrEmpty(textCodigoCnes.Text))
                        throw new Exception("Cliente é um prestador de serviços. Obrigatório informar o código cnes do estabelecimento.");

                    if (Convert.ToBoolean(((SelectItem)cbCentroCusto.SelectedItem).Valor) == true && centroCustoCliente.Count == 0)
                        throw new Exception("Cliente marcado para usar centro de custo. Obrigatório pelo menos um centro de custo cadastrado.");
                }
                else
                {
                    /* Cliente é uma pessoa física. */

                    if (vendedores.Count == 0)
                        throw new Exception("É necessario infomar quem é o vendedor do cliente.");

                    if (matriz != null || credenciada != null)
                        throw new Exception("O cliente é uma pessoa física então não pode ser referenciado uma matriz ou uma credenciada.");


                    if (string.IsNullOrEmpty(textRazaoSocial.Text))
                    {
                        textRazaoSocial.Focus();
                        throw new Exception("O nome é obrigatório.");
                    }

                    //if (string.IsNullOrEmpty(textInscricao.Text))
                    //{
                    //    textInscricao.Focus();
                    //    throw new Exception("O RG é obrigatório.");
                    //}


                    if (String.IsNullOrEmpty(textLogradouroComercial.Text) ||
                        String.IsNullOrEmpty(textNumeroComercial.Text) ||
                        String.IsNullOrEmpty(textBairroComercial.Text) ||
                        String.IsNullOrEmpty(textCEPComercial.Text) ||
                        cidadeComercial == null ||
                        cbUFComercial.SelectedIndex == -1)
                        throw new Exception("Rever o endereco digitado.");

                    if (funcoes.Count == 0)
                        throw new Exception("Obrigatorio ter pelo menos uma funçao registrada.");

                    if (cnaes.Count == 0)
                        throw new Exception("Obrigatório ter pelo menos um cnae cadastrado");

                }
            }
            catch (Exception ex)
            {
                retorno = false;
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

            return retorno;
        }

        protected virtual void btIncluiCnae_Click(object sender, EventArgs e)
        {
            try
            {
                ClienteFacade clienteFacade = ClienteFacade.getInstance();

                frmCnaeSelecionar formClienteCnaeSelecionar = new frmCnaeSelecionar();
                formClienteCnaeSelecionar.ShowDialog();                

                if (formClienteCnaeSelecionar.Cnae != null)
                {
                    /* verificar se o cnae já está presente na coleção */
                    
                    if (cnaes.Exists(x => x.Cnae.Id == formClienteCnaeSelecionar.Cnae.Id ))
                        throw new Exception("O CNAE selecionado já está incluído. Por favor selecione outro.");

                    cnaes.Add(new ClienteCnae(cliente, formClienteCnaeSelecionar.Cnae, false));
                
                    montaGridCnae();

                }

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        protected virtual void btExcluiCnae_Click(object sender, EventArgs e)
        {
            try
            {
                if (dgvCnae.CurrentRow == null)
                    throw new Exception("Selecione uma linha.");

                cnaes.Clear();

                foreach (DataGridViewRow dvRom in dgvCnae.SelectedRows)
                    dgvCnae.Rows.RemoveAt(dvRom.Index);

                foreach (DataGridViewRow dvRows in dgvCnae.Rows)
                    cnaes.Add(new ClienteCnae(cliente, new Cnae(Convert.ToInt64(dvRows.Cells[0].Value), dvRows.Cells[1].Value.ToString(), dvRows.Cells[2].Value.ToString(), String.Empty, String.Empty, String.Empty), Convert.ToBoolean(dvRows.Cells[3].Value)));

                montaGridCnae();

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atençao", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }

        protected void btCopiarComercial_Click(object sender, EventArgs e)
        {
            try
            {

                textLogradouroCobranca.Text = textLogradouroComercial.Text;
                textNumeroCobranca.Text = textNumeroComercial.Text;
                textComplementoCobranca.Text = textComplementoComercial.Text;
                textBairroCobranca.Text = textBairroComercial.Text;
                textCEPCobranca.Text = textCEPComercial.Text;
                if (cbUFComercial.SelectedIndex != -1)
                    cbUFCobranca.SelectedValue = ((SelectItem)cbUFComercial.SelectedItem).Valor;

                if (cidadeComercial != null)
                {
                    cidadeCobranca = cidadeComercial;
                    textCidadeCobranca.Text = textCidadeComercial.Text;
                }

                textTelefoneCobranca.Text = textTelefoneComercial.Text;
                textFaxCobranca.Text = textFaxComercial.Text;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        protected void frmClienteIncluir_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter || e.KeyCode == Keys.Return)
                SendKeys.Send("{TAB}");
        }

        protected virtual void btIncluirLogo_Click(object sender, EventArgs e)
        {
            OpenFileDialog clienteLogoFileDialog = new OpenFileDialog();

            clienteLogoFileDialog.Multiselect = false;
            clienteLogoFileDialog.Title = "Selecionar Logo";
            clienteLogoFileDialog.InitialDirectory = @"C:\";
            //filtra para exibir somente arquivos de imagens
            clienteLogoFileDialog.Filter = "Images(*.JPG) |*.JPG";
            clienteLogoFileDialog.CheckFileExists = true;
            clienteLogoFileDialog.CheckPathExists = true;
            clienteLogoFileDialog.FilterIndex = 2;
            clienteLogoFileDialog.RestoreDirectory = true;
            clienteLogoFileDialog.ReadOnlyChecked = true;
            clienteLogoFileDialog.ShowReadOnly = true;

            DialogResult dr = clienteLogoFileDialog.ShowDialog();

            if (dr == System.Windows.Forms.DialogResult.OK)
            {
                String nomeArquivo = clienteLogoFileDialog.FileName;
                String nomeArquivoFinal = nomeArquivo.Substring(nomeArquivo.LastIndexOf('\\') + 1);

                // cria um PictureBox
                try
                {
                    Image Imagem = Image.FromFile(nomeArquivo);
                    pbLogo.SizeMode = PictureBoxSizeMode.StretchImage;
                    if (Imagem.Width > 500 && Imagem.Height > 500)
                        throw new LogoException("A imagem deve ter um tamanho máximo de 500 X 500 pixels.");

                    pbLogo.Image = Imagem;

                    byte[] conteudo = FileHelper.CarregarArquivoImagem(nomeArquivo, 5242880);

                    Arquivo = new Arquivo(null, nomeArquivoFinal, "image/jpeg", conteudo.Length, conteudo);
                }
                catch (SecurityException ex)
                {
                    // O usuário  não possui permissão para ler arquivos
                    MessageBox.Show("Erro de segurança Contate o administrador de segurança da rede.\n\n" +
                                                "Mensagem : " + ex.Message + "\n\n" +
                                                "Detalhes (enviar ao suporte):\n\n" + ex.StackTrace);
                }
                catch (LogoException ex)
                {
                    MessageBox.Show(ex.Message);
                }
                catch (Exception ex)
                {
                    // Não pode carregar a imagem (problemas de permissão)
                    MessageBox.Show("Não é possível exibir a imagem : " + nomeArquivoFinal
                                                + ". Você pode não ter permissão para ler o arquivo , ou " +
                                                " ele pode estar corrompido.\n\nErro reportado : " + ex.Message);
                }
            }
        }

        protected virtual void btIncluirFuncao_Click(object sender, EventArgs e)
        {
            try
            {
                frmFuncaoPesquisa pesquisarFuncao = new frmFuncaoPesquisa();
                pesquisarFuncao.ShowDialog();

                if (pesquisarFuncao.Funcoes.Count > 0)
                {
                    /* verificando se alguma função selecioanada já está presente no cliente */
                    foreach (DataGridViewRow dvRow in dgvFuncao.Rows)
                        if (pesquisarFuncao.Funcoes.Exists(x => x.Id == (long)dvRow.Cells[0].Value))
                            throw new Exception("A função : " + dvRow.Cells[1].Value + " já foi incluída nesse cliente. Refaça sua pesquisa.");

                    pesquisarFuncao.Funcoes.ForEach(delegate(Funcao funcao)
                    {
                        funcoes.Add(new ClienteFuncao(null, cliente, funcao, ApplicationConstants.ATIVO, DateTime.Now, null));
                    });

                    montaGridFuncoes();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atençao", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }

        protected virtual void btExcluirFuncao_Click(object sender, EventArgs e)
        {
            try
            {
                if (dgvFuncao.CurrentRow == null)
                    throw new Exception("Selecione uma linha.");

                funcoes.RemoveAll(x => x.Funcao.Id == (long)dgvFuncao.CurrentRow.Cells[0].Value);
                montaGridFuncoes();

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        protected void montaGridFuncoes()
        {
            try
            {
                dgvFuncao.Columns.Clear();

                dgvFuncao.ColumnCount = 5;

                dgvFuncao.Columns[0].HeaderText = "Código Função";
                dgvFuncao.Columns[0].ReadOnly = true;
                dgvFuncao.Columns[0].Width = 50;

                dgvFuncao.Columns[1].HeaderText = "Descrição";
                dgvFuncao.Columns[1].Width = 350;

                dgvFuncao.Columns[2].HeaderText = "Cod_CBO";
                
                dgvFuncao.Columns[3].HeaderText = "Situação";
                dgvFuncao.Columns[3].Visible = false;
                
                dgvFuncao.Columns[4].HeaderText = "Comentário";
                dgvFuncao.Columns[4].Visible = false;

                foreach (ClienteFuncao clienteFuncao in Funcoes)
                {
                    dgvFuncao.Rows.Add(clienteFuncao.Funcao.Id,
                          clienteFuncao.Funcao.Descricao,
                          clienteFuncao.Funcao.CodCbo,
                          clienteFuncao.Funcao.Situacao,
                          clienteFuncao.Funcao.Comentario);
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        protected void montaGridCnae()
        {
            try
            {
                dgvCnae.Columns.Clear();

                dgvCnae.ColumnCount = 4;

                dgvCnae.Columns[0].HeaderText = "id Cnae";
                dgvCnae.Columns[0].Visible = false;
                dgvCnae.Columns[1].HeaderText = "Codigo Cnae";
                dgvCnae.Columns[2].HeaderText = "Atividade";
                dgvCnae.Columns[3].HeaderText = "FlagPrincipal";
                dgvCnae.Columns[3].Visible = false;

                foreach (ClienteCnae clienteCnae in cnaes)
                {
                    this.dgvCnae.Rows.Add(clienteCnae.Cnae.Id, clienteCnae.Cnae.CodCnae, clienteCnae.Cnae.Atividade, clienteCnae.Flag_pr);
                }

                desabilitaSort(dgvCnae);

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        protected virtual void btMatriz_Click(object sender, EventArgs e)
        {
            frmClienteSelecionar formClienteTipo = new frmClienteSelecionar(null, null, false, null, null, null, null, false, true);
            formClienteTipo.ShowDialog();

            if (formClienteTipo.Cliente != null)
            {
                Matriz = formClienteTipo.Cliente;
                this.textMatriz.Text = Matriz.RazaoSocial;
                this.textCNPJ.Text = Matriz.Cnpj;
            }
        }

        protected virtual void btCredenciada_Click(object sender, EventArgs e)
        {
            frmClienteSelecionar formClienteTipo = new frmClienteSelecionar(null, false, false, null, null, null, true, false, true);
            formClienteTipo.ShowDialog();

            if (formClienteTipo.Cliente != null)
            {
                credenciada = formClienteTipo.Cliente;
                this.textCredenciada.Text = credenciada.RazaoSocial;
                this.textCNPJ.Text = String.Empty;
            }
        }

        protected virtual void btRamo_Click(object sender, EventArgs e)
        {
            frmRamoSelecionar selecionarRamo = new frmRamoSelecionar();
            selecionarRamo.ShowDialog();

            if (selecionarRamo.Ramo != null)
            {
                ramo = selecionarRamo.Ramo;
                textRamo.Text = ramo.Descricao;
            }
        }

        protected virtual void btExcluirLogo_Click(object sender, EventArgs e)
        {
            try
            {
                arquivo = null;
                pbLogo.Image = null;

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }

        protected virtual void btIncluirContato_Click(object sender, EventArgs e)
        {
            try
            {
                frmContatoIncluir contatoIncluir = new frmContatoIncluir();
                contatoIncluir.ShowDialog();

                if (contatoIncluir.Contato != null)
                {
                    /* verificando se na lista já tem algum contato marcado como responsável. Só poderá haver
                     * um responsável pelo contrato  */

                    if (contatos.Exists(x => x.ResponsavelContrato == true))
                        throw new Exception("Já existe um contato cadastrado como responsável.");

                    if (contatos.Exists(x => x.ResponsavelEstudo == true))
                        throw new Exception("Já existe um contato cadastrado como responsável pelo estudo.");

                    contatos.Add(contatoIncluir.Contato);
                }

                montaGridContato();

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        protected void montaGridContato()
        {
            try
            {
                this.dgvContatos.Columns.Clear();

                this.Cursor = Cursors.WaitCursor;

                this.dgvContatos.ColumnCount = 17;

                this.dgvContatos.Columns[0].HeaderText = "Id";
                this.dgvContatos.Columns[0].Visible = false;

                this.dgvContatos.Columns[1].HeaderText = "Tipo de Contato";
                this.dgvContatos.Columns[2].HeaderText = "Nome";
                this.dgvContatos.Columns[3].HeaderText = "CPF";
                this.dgvContatos.Columns[4].HeaderText = "RG";
                this.dgvContatos.Columns[5].HeaderText = "Data de Nascimento";
                this.dgvContatos.Columns[6].HeaderText = "Endereço";
                this.dgvContatos.Columns[7].HeaderText = "Numero";
                this.dgvContatos.Columns[8].HeaderText = "Complemento";
                this.dgvContatos.Columns[9].HeaderText = "Bairro";
                this.dgvContatos.Columns[10].HeaderText = "Cidade";
                this.dgvContatos.Columns[11].HeaderText = "UF";
                this.dgvContatos.Columns[12].HeaderText = "CEP";
                this.dgvContatos.Columns[13].HeaderText = "Telefone";
                this.dgvContatos.Columns[14].HeaderText = "Celular";
                this.dgvContatos.Columns[15].HeaderText = "Email";
                this.dgvContatos.Columns[16].HeaderText = "PIS/PASEP";

                foreach (Contato contato in contatos)
                {
                    this.dgvContatos.Rows.Add(contato.Id == null ? null : (Int64?)Convert.ToInt64(contato.Id),
                        contato.TipoContato.Descricao,
                        contato.Nome,
                        String.IsNullOrEmpty(contato.Cpf) ? String.Empty : ValidaCampoHelper.FormataCpf(contato.Cpf),
                        contato.Rg,
                        contato.DataNascimento == null ? null : String.Format("{0:dd/MM/yyyy}", contato.DataNascimento),
                        contato.Endereco,
                        contato.Numero,
                        contato.Complemento,
                        contato.Bairro,
                        contato.Cidade == null ? String.Empty : contato.Cidade.Nome,
                        contato.Uf,
                        String.IsNullOrEmpty(contato.Cep) ? String.Empty : ValidaCampoHelper.FormataCEP(contato.Cep),
                        contato.Telefone,
                        contato.Celular,
                        contato.Email,
                        contato.PisPasep
                        );
                }

                desabilitaSort(dgvContatos);


            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }

        }

        protected virtual void btExcluirContato_Click(object sender, EventArgs e)
        {
            try
            {
                if (dgvContatos.CurrentRow == null)
                    throw new Exception("Selecione uma linha.");

                int index = dgvContatos.CurrentRow.Index;

                dgvContatos.Rows.RemoveAt(index);
                contatos.RemoveAt(index);
                montaGridContato();

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atençao", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        protected void btImprimir_Click(object sender, EventArgs e)
        {
            try
            {
                throw new NotImplementedException("Metodo nao implementado");
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atençao", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        protected void btCidadeComercial_Click(object sender, EventArgs e)
        {
            try
            {
                if (cbUFComercial.SelectedIndex == 0)
                    throw new Exception("Você deve selecionar primeiro a unidade federativa.");

                frmCidadePesquisa cidadeSeleciona = new frmCidadePesquisa(((SelectItem)cbUFComercial.SelectedItem).Valor.ToString());
                cidadeSeleciona.ShowDialog();

                if (cidadeSeleciona.Cidade != null)
                {
                    cidadeComercial = cidadeSeleciona.Cidade;
                    textCidadeComercial.Text = cidadeComercial.Nome;
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        protected void btCidadeCobranca_Click(object sender, EventArgs e)
        {
            try
            {
                if (cbUFCobranca.SelectedIndex == 0)
                    throw new Exception("Você deve selecionar primeiro a unidade federativa.");

                frmCidadePesquisa cidadeSeleciona = new frmCidadePesquisa(((SelectItem)cbUFCobranca.SelectedItem).Valor.ToString());
                cidadeSeleciona.ShowDialog();

                if (cidadeSeleciona.Cidade != null)
                {
                    cidadeCobranca = cidadeSeleciona.Cidade;
                    textCidadeCobranca.Text = cidadeCobranca.Nome;
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        protected void chanceControleConfiguracao(Boolean value)
        {
            this.cbClienteVIP.Enabled = value;
            this.cbContrato.Enabled = value;
            this.cbParticular.Enabled = value;
            this.cbPrestador.Enabled = value;
            this.cbValorLiquido.Enabled = value;
            
        }

        protected void changeControlesTela(Boolean value)
        {
            this.grbCnae.Enabled = value;
            this.btIncluirCnae.Enabled = value;
            this.btExcluirCnae.Enabled = value;
            this.grbFuncao.Enabled = value;
            this.btIncluirFuncao.Enabled = value;
            this.btExcluirFuncao.Enabled = value;
            this.grbContato.Enabled = value;
            this.btIncluirContato.Enabled = value;
            this.btExcluirContato.Enabled = value;
            this.grbLogo.Enabled = value;
            this.btIncluirLogo.Enabled = value;
            this.btExcluirLogo.Enabled = value;
            
        }

        protected void textCNPJ_Leave(object sender, EventArgs e)
        {
            try
            {
                if (string.Equals(((SelectItem)cbTipoCliente.SelectedItem).Valor, ApplicationConstants.FISICA))
                {
                    /* validando o CPF digitado */
                    if (!ValidaCampoHelper.ValidaCPF(textCNPJ.Text))
                        throw new Exception("CPF inválido.");
                }
                else if (textCNPJ.Text.Replace(".", "").Replace("-", "").Replace("/", "").Trim().Length == 11)
                {
                    if (!ValidaCampoHelper.ValidaCPF(textCNPJ.Text))
                        throw new Exception("CPF inválido.");
                    textCNPJ.Mask = "999,999,999-99";
                }
                else if (!String.IsNullOrEmpty(textCNPJ.Text.Replace(".", "").Replace("-", "").Replace("/", "").Trim()))
                {
                    if (!ValidaCampoHelper.ValidaCNPJ(textCNPJ.Text))
                        throw new Exception("CNPJ inválido.");
                    textCNPJ.Mask = "99,999,999/9999-99";
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atençao", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                textCNPJ.Focus();
                textCNPJ.Select(0, 0);
            }
        }

        protected void textCEPComercial_Leave(object sender, EventArgs e)
        {
            try
            {
                if (!ValidaCampoHelper.ValidaCepDigitado(textCEPComercial.Text))
                {
                    textCEPComercial.Focus();
                    throw new FuncionarioException(FuncionarioException.msg6);
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atençao", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
        }

        protected void desabilitaSort(DataGridView dataGridView)
        {
            foreach (DataGridViewColumn column in dataGridView.Columns)
                column.SortMode = DataGridViewColumnSortMode.NotSortable;
        }

        protected void dgvCnae_RowPrePaint(object sender, DataGridViewRowPrePaintEventArgs e)
        {
            DataGridView dgv = sender as DataGridView;

            if (Convert.ToBoolean(dgv.Rows[e.RowIndex].Cells[3].Value) == true)
                dgv.Rows[e.RowIndex].DefaultCellStyle.ForeColor = Color.Green;

        }

        protected virtual void dgvCnae_DoubleClick(object sender, EventArgs e)
        {
            try
            {
                if (dgvCnae.Rows.Count == 0)
                    throw new Exception("Selecione uma linha.");

                /* zerando o cnae principal da lista */
                foreach (ClienteCnae cnae in cnaes)
                    cnae.Flag_pr = false;

                /* selecionado o cnae para o principal */
                ClienteCnae cnaePrincipal = cnaes.ElementAt(dgvCnae.CurrentRow.Index);
                cnaePrincipal.Flag_pr = true;

                montaGridCnae();


            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atençao", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        protected virtual void dgvContatos_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                if (dgvContatos.Rows.Count > 0)
                {
                    Contato contatoAlterar = contatos.ElementAt(e.RowIndex);
                    frmContatoAlterar formAlterarContato = new frmContatoAlterar(contatoAlterar);
                    formAlterarContato.ShowDialog();
                    /* removendo o contato anterior da lista */
                    contatos.Remove(contatoAlterar);
                    /* adicionado o novo contato alterado na lista */
                    contatos.Add(formAlterarContato.Contato);
                    montaGridContato();
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atençao", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        protected void textCodigoCnes_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = ValidaCampoHelper.FormataDigitacaoNumero(sender, e, textCodigoCnes.Text, 2);
        }

        protected void frmClientesIncluir_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter || e.KeyCode == Keys.Return)
                SendKeys.Send("{TAB}");
        }

        protected virtual void cbCentroCusto_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (string.Equals(((SelectItem)cbCentroCusto.SelectedItem).Valor, "true"))
            {
                grbCentroCusto.Enabled = true;
                flpAcaoCentroCusto.Enabled = true;
            }
            else
            {
                centroCustoCliente.Clear();
                montaGridCentroCusto();
                grbCentroCusto.Enabled = false;
                flpAcaoCentroCusto.Enabled = false;
            }
        }

        protected virtual void btnIncluirCentroCusto_Click(object sender, EventArgs e)
        {
            try
            {
                frmClienteCentroCustoIncluir incluirCentroCusto = new frmClienteCentroCustoIncluir();
                incluirCentroCusto.ShowDialog();

                if (incluirCentroCusto.CentroCusto != null)
                {
                    /* verificando se já existe um centro de custo cadastrado com esse nome na lista */
                    CentroCusto centroCustoProcura = centroCustoCliente.Find(x => string.Equals(x.Descricao, incluirCentroCusto.CentroCusto.Descricao));
                    if (centroCustoProcura != null)
                        throw new Exception("Já existe um centro de custo cadastrado com esse nome.");
                    
                    centroCustoCliente.Add(incluirCentroCusto.CentroCusto);
                    montaGridCentroCusto();
                }
                

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atençao", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }

        protected virtual void btnExcluirCentroCusto_Click(object sender, EventArgs e)
        {
            try
            {
                if (dgvCentroCusto.CurrentRow == null)
                    throw new Exception("Selecione uma linha.");

                int index = centroCustoCliente.FindIndex(x => string.Equals(x.Descricao, dgvCentroCusto.CurrentRow.Cells[1].Value));
                centroCustoCliente.RemoveAt(index);
                dgvCentroCusto.Rows.RemoveAt(index);

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atençao", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        protected void montaGridCentroCusto()
        {
            try
            {
                dgvCentroCusto.Columns.Clear();
                dgvCentroCusto.ColumnCount = 2;
                
                dgvCentroCusto.Columns[0].HeaderText = "id";
                dgvCentroCusto.Columns[0].Visible = false;

                dgvCentroCusto.Columns[1].HeaderText = "Centro de Custo";

                centroCustoCliente.ForEach(delegate(CentroCusto centroCusto)
                {
                    dgvCentroCusto.Rows.Add(centroCusto.Id, centroCusto.Descricao);
                });

                if (centroCustoCliente.Count == 0)
                    dgvCentroCusto.Columns.Clear();

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atençao", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        protected virtual void dgvCentroCusto_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                frmClienteCentroCustoAlterar alteraCentroCusto = new frmClienteCentroCustoAlterar(centroCustoCliente.Find(x => string.Equals(x.Descricao, dgvCentroCusto.CurrentRow.Cells[1].Value)));
                alteraCentroCusto.ShowDialog();
                montaGridCentroCusto();

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atençao", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        protected virtual void cbTipoCliente_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                if (string.Equals(((SelectItem)cbTipoCliente.SelectedItem).Valor, ApplicationConstants.PADRAO))
                {
                    /* cliente padrão */
                    credenciada = null;
                    btCredenciada.Enabled = false;
                    textCredenciada.Text = String.Empty;

                    matriz = null;
                    btMatriz.Enabled = false;
                    textMatriz.Text = String.Empty;

                    lblRazaoSocial.Text = "Razão Social";
                    lblCNPJ.Text = "CNPJ";
                    lblInscricao.Text = "Inscrição Estadual / Municipal";
                    textJornada.ReadOnly = false;
                    textEscopo.ReadOnly = false;
                    textCNPJ.Mask = "99,999,999/9999-99";

                    btVendedor.Enabled = true;
                    btRamo.Enabled = true;

                    /* habilitando abas de endereco */
                    tpEndereco.Enabled = true;
                    tpEndereco.TabPages.Clear();
                    tpComercial.Text = "Comercial";

                    foreach (TabPage tp in tpEnderecoCopy)
                        tpEndereco.TabPages.Add(tp);

                    /* habilitando controles de configurações do cliente */
                    chanceControleConfiguracao(true);

                    /* habilitando demais situações */
                    changeControlesTela(true);
                }
                else if (string.Equals(((SelectItem)cbTipoCliente.SelectedItem).Valor, ApplicationConstants.UNIDADE))
                {
                    /* cliente unidade */
                    btVendedor.Enabled = false;
                    this.vendedores.Clear();
                    textVendedor.Text = String.Empty;
                    btCredenciada.Enabled = false;
                    credenciada = null;
                    textCredenciada.Text = String.Empty;
                    textCNPJ.Text = String.Empty;
                    btRamo.Enabled = false;
                    btMatriz.Enabled = true;

                    lblRazaoSocial.Text = "Nome da Unidade";
                    lblCNPJ.Text = "CNPJ";
                    lblNomeFantasia.Text = "Nome de Fantasia";
                    textJornada.ReadOnly = true;
                    textEscopo.ReadOnly = true;
                    lblInscricao.Text = "Inscrição Estadual / Municipal ";
                    textCNPJ.Mask = "99,999,999/9999-99";

                    /* habilitando controles de configurações do cliente */
                    chanceControleConfiguracao(false);

                    /* habilitando demais situações */
                    changeControlesTela(false);

                    /* habilitando abas de endereco */
                    this.tpEndereco.Enabled = true;
                    tpEndereco.TabPages.Clear();
                    tpEndereco.TabPages.Add(tpEnderecoCopy[0]);
                    tpComercial.Text = "Comercial";

                    grbCentroCusto.Enabled = false;
                    cbCentroCusto.Enabled = false;
                    cbBloqueado.Enabled = true;

                }
                else if (string.Equals(((SelectItem)cbTipoCliente.SelectedItem).Valor, ApplicationConstants.CREDENCIADA))
                {
                    /* cliente credenciada */
                    btVendedor.Enabled = true;
                    this.matriz = null;
                    this.textMatriz.Text = String.Empty;
                    this.btMatriz.Enabled = false;
                    this.btCredenciada.Enabled = true;
                    this.textCNPJ.ReadOnly = false;
                    this.btRamo.Enabled = true;

                    lblRazaoSocial.Text = "Razão Social/Nome";
                    lblCNPJ.Text = "CNPJ/CPF";
                    lblNomeFantasia.Text = "Nome de Fantasia/Apelido";
                    lblInscricao.Text = "Inscrição Estadual / Municipal/ RG ";
                    textJornada.ReadOnly = false;
                    textEscopo.ReadOnly = false;

                    textCNPJ.Text = string.Empty;
                    textCNPJ.Mask = string.Empty;
                    //textCNPJ.Mask = "99,999,999/9999-99";

                    this.tpEndereco.Enabled = true;
                    /* habilitando controles de configurações do cliente */
                    chanceControleConfiguracao(true);

                    /* habilitando abas de endereco */

                    tpEndereco.TabPages.Clear();
                    tpComercial.Text = "Comercial";
                    foreach (TabPage tp in tpEnderecoCopy)
                        tpEndereco.TabPages.Add(tp);

                    /* habilitando demais situações */
                    changeControlesTela(true);

                    grbCentroCusto.Enabled = false;
                    cbCentroCusto.Enabled = false;
                    cbBloqueado.Enabled = true;
                }
                else
                {
                    /* cliente pessoa física */
                    btVendedor.Enabled = true;
                    btRamo.Enabled = true;

                    matriz = null;
                    btMatriz.Enabled = false;
                    textMatriz.Text = String.Empty;

                    credenciada = null;
                    btCredenciada.Enabled = false;
                    textCredenciada.Text = String.Empty;

                    lblRazaoSocial.Text = "Nome/Razao Social";
                    lblCNPJ.Text = "CPF";
                    lblNomeFantasia.Text = "Apelido/NomeFantasia";
                    lblInscricao.Text = "RG/Inscrição Municipal";
                    textJornada.ReadOnly = true;
                    textEscopo.ReadOnly = true;
                    textCNPJ.Mask = "999,999,999-99";

                    /* habilitando abas de endereco */
                    this.tpEndereco.Enabled = true;
                    tpEndereco.TabPages.Clear();
                    tpComercial.Text = "Comercial";
                    
                    foreach (TabPage tp in tpEnderecoCopy)
                        tpEndereco.TabPages.Add(tp);

                    grbCnae.Enabled = true;
                    btIncluirCnae.Enabled = true;
                    btExcluirCnae.Enabled = true;

                    cbPrestador.Enabled = false;
                    textCodigoCnes.ReadOnly = true;

                    grbCentroCusto.Enabled = false;
                    btnIncluirCentroCusto.Enabled = false;
                    btnExcluirCentroCusto.Enabled = false;
                    cbCentroCusto.Enabled = false;

                    cbCredenciadora.Enabled = false;

                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }

        private void textCNPJ_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = ValidaCampoHelper.FormataCampoNumerico(sender, e);
        }

    }
}
