﻿namespace SWS.View
{
    partial class frmMedicoIncluir
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.flpAcao = new System.Windows.Forms.FlowLayoutPanel();
            this.bt_gravar = new System.Windows.Forms.Button();
            this.bt_limpar = new System.Windows.Forms.Button();
            this.bt_fechar = new System.Windows.Forms.Button();
            this.lblNome = new System.Windows.Forms.TextBox();
            this.lblCrm = new System.Windows.Forms.TextBox();
            this.lblEndereco = new System.Windows.Forms.TextBox();
            this.lblNumero = new System.Windows.Forms.TextBox();
            this.lblComplemento = new System.Windows.Forms.TextBox();
            this.lblBairro = new System.Windows.Forms.TextBox();
            this.lblCep = new System.Windows.Forms.TextBox();
            this.lblUF = new System.Windows.Forms.TextBox();
            this.lblCidade = new System.Windows.Forms.TextBox();
            this.lblEmail = new System.Windows.Forms.TextBox();
            this.lblCelular = new System.Windows.Forms.TextBox();
            this.lblTelefone = new System.Windows.Forms.TextBox();
            this.text_nome = new System.Windows.Forms.TextBox();
            this.text_crm = new System.Windows.Forms.TextBox();
            this.text_endereco = new System.Windows.Forms.TextBox();
            this.text_numero = new System.Windows.Forms.TextBox();
            this.text_complemento = new System.Windows.Forms.TextBox();
            this.text_bairro = new System.Windows.Forms.TextBox();
            this.text_cep = new System.Windows.Forms.MaskedTextBox();
            this.cb_uf = new SWS.ComboBoxWithBorder();
            this.cbCidade = new SWS.ComboBoxWithBorder();
            this.text_telefone2 = new System.Windows.Forms.TextBox();
            this.text_email = new System.Windows.Forms.TextBox();
            this.text_telefone1 = new System.Windows.Forms.TextBox();
            this.IncluirMedicoErrorProvider = new System.Windows.Forms.ErrorProvider(this.components);
            this.lblPisPasep = new System.Windows.Forms.TextBox();
            this.textPisPasep = new System.Windows.Forms.MaskedTextBox();
            this.lblUfCrm = new System.Windows.Forms.TextBox();
            this.cbUfCrm = new SWS.ComboBoxWithBorder();
            this.textDocumentoExtra = new System.Windows.Forms.TextBox();
            this.lblDocumentoExtra = new System.Windows.Forms.TextBox();
            this.btnAssinatura = new System.Windows.Forms.Button();
            this.lblAssinatura = new System.Windows.Forms.Label();
            this.pbAssinatura = new System.Windows.Forms.PictureBox();
            this.lblCpf = new System.Windows.Forms.TextBox();
            this.textCpf = new System.Windows.Forms.MaskedTextBox();
            this.lblRqe = new System.Windows.Forms.TextBox();
            this.textRqe = new System.Windows.Forms.TextBox();
            ((System.ComponentModel.ISupportInitialize)(this.spForm)).BeginInit();
            this.spForm.Panel1.SuspendLayout();
            this.spForm.Panel2.SuspendLayout();
            this.spForm.SuspendLayout();
            this.pnlForm.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.banner)).BeginInit();
            this.flpAcao.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.IncluirMedicoErrorProvider)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbAssinatura)).BeginInit();
            this.SuspendLayout();
            // 
            // spForm
            // 
            // 
            // spForm.Panel1
            // 
            this.spForm.Panel1.Controls.Add(this.flpAcao);
            // 
            // pnlForm
            // 
            this.pnlForm.Controls.Add(this.textRqe);
            this.pnlForm.Controls.Add(this.lblRqe);
            this.pnlForm.Controls.Add(this.textCpf);
            this.pnlForm.Controls.Add(this.lblCpf);
            this.pnlForm.Controls.Add(this.btnAssinatura);
            this.pnlForm.Controls.Add(this.lblAssinatura);
            this.pnlForm.Controls.Add(this.pbAssinatura);
            this.pnlForm.Controls.Add(this.textDocumentoExtra);
            this.pnlForm.Controls.Add(this.lblDocumentoExtra);
            this.pnlForm.Controls.Add(this.cbUfCrm);
            this.pnlForm.Controls.Add(this.lblUfCrm);
            this.pnlForm.Controls.Add(this.textPisPasep);
            this.pnlForm.Controls.Add(this.lblPisPasep);
            this.pnlForm.Controls.Add(this.text_telefone2);
            this.pnlForm.Controls.Add(this.text_email);
            this.pnlForm.Controls.Add(this.text_telefone1);
            this.pnlForm.Controls.Add(this.cbCidade);
            this.pnlForm.Controls.Add(this.cb_uf);
            this.pnlForm.Controls.Add(this.text_cep);
            this.pnlForm.Controls.Add(this.text_bairro);
            this.pnlForm.Controls.Add(this.text_complemento);
            this.pnlForm.Controls.Add(this.text_numero);
            this.pnlForm.Controls.Add(this.text_endereco);
            this.pnlForm.Controls.Add(this.text_crm);
            this.pnlForm.Controls.Add(this.text_nome);
            this.pnlForm.Controls.Add(this.lblTelefone);
            this.pnlForm.Controls.Add(this.lblCelular);
            this.pnlForm.Controls.Add(this.lblEmail);
            this.pnlForm.Controls.Add(this.lblCidade);
            this.pnlForm.Controls.Add(this.lblUF);
            this.pnlForm.Controls.Add(this.lblCep);
            this.pnlForm.Controls.Add(this.lblBairro);
            this.pnlForm.Controls.Add(this.lblComplemento);
            this.pnlForm.Controls.Add(this.lblNumero);
            this.pnlForm.Controls.Add(this.lblEndereco);
            this.pnlForm.Controls.Add(this.lblCrm);
            this.pnlForm.Controls.Add(this.lblNome);
            this.pnlForm.Size = new System.Drawing.Size(764, 600);
            // 
            // flpAcao
            // 
            this.flpAcao.Controls.Add(this.bt_gravar);
            this.flpAcao.Controls.Add(this.bt_limpar);
            this.flpAcao.Controls.Add(this.bt_fechar);
            this.flpAcao.Dock = System.Windows.Forms.DockStyle.Fill;
            this.flpAcao.Location = new System.Drawing.Point(0, 0);
            this.flpAcao.Name = "flpAcao";
            this.flpAcao.Size = new System.Drawing.Size(784, 35);
            this.flpAcao.TabIndex = 0;
            // 
            // bt_gravar
            // 
            this.bt_gravar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.bt_gravar.Image = global::SWS.Properties.Resources.Gravar;
            this.bt_gravar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.bt_gravar.Location = new System.Drawing.Point(3, 3);
            this.bt_gravar.Name = "bt_gravar";
            this.bt_gravar.Size = new System.Drawing.Size(75, 23);
            this.bt_gravar.TabIndex = 17;
            this.bt_gravar.TabStop = false;
            this.bt_gravar.Text = "&Gravar";
            this.bt_gravar.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.bt_gravar.UseVisualStyleBackColor = true;
            this.bt_gravar.Click += new System.EventHandler(this.bt_gravar_Click);
            // 
            // bt_limpar
            // 
            this.bt_limpar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.bt_limpar.Image = global::SWS.Properties.Resources.vassoura;
            this.bt_limpar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.bt_limpar.Location = new System.Drawing.Point(84, 3);
            this.bt_limpar.Name = "bt_limpar";
            this.bt_limpar.Size = new System.Drawing.Size(75, 23);
            this.bt_limpar.TabIndex = 16;
            this.bt_limpar.TabStop = false;
            this.bt_limpar.Text = "&Limpar";
            this.bt_limpar.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.bt_limpar.UseVisualStyleBackColor = true;
            this.bt_limpar.Click += new System.EventHandler(this.bt_limpar_Click);
            // 
            // bt_fechar
            // 
            this.bt_fechar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.bt_fechar.Image = global::SWS.Properties.Resources.close;
            this.bt_fechar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.bt_fechar.Location = new System.Drawing.Point(165, 3);
            this.bt_fechar.Name = "bt_fechar";
            this.bt_fechar.Size = new System.Drawing.Size(75, 23);
            this.bt_fechar.TabIndex = 18;
            this.bt_fechar.TabStop = false;
            this.bt_fechar.Text = "&Fechar";
            this.bt_fechar.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.bt_fechar.UseVisualStyleBackColor = true;
            this.bt_fechar.Click += new System.EventHandler(this.bt_fechar_Click);
            // 
            // lblNome
            // 
            this.lblNome.BackColor = System.Drawing.Color.LightGray;
            this.lblNome.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblNome.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNome.Location = new System.Drawing.Point(12, 15);
            this.lblNome.Name = "lblNome";
            this.lblNome.ReadOnly = true;
            this.lblNome.Size = new System.Drawing.Size(137, 21);
            this.lblNome.TabIndex = 0;
            this.lblNome.TabStop = false;
            this.lblNome.Text = "Nome";
            // 
            // lblCrm
            // 
            this.lblCrm.BackColor = System.Drawing.Color.LightGray;
            this.lblCrm.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblCrm.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCrm.Location = new System.Drawing.Point(12, 35);
            this.lblCrm.Name = "lblCrm";
            this.lblCrm.ReadOnly = true;
            this.lblCrm.Size = new System.Drawing.Size(137, 21);
            this.lblCrm.TabIndex = 1;
            this.lblCrm.TabStop = false;
            this.lblCrm.Text = "CRM";
            // 
            // lblEndereco
            // 
            this.lblEndereco.BackColor = System.Drawing.Color.LightGray;
            this.lblEndereco.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblEndereco.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblEndereco.Location = new System.Drawing.Point(12, 95);
            this.lblEndereco.Name = "lblEndereco";
            this.lblEndereco.ReadOnly = true;
            this.lblEndereco.Size = new System.Drawing.Size(137, 21);
            this.lblEndereco.TabIndex = 2;
            this.lblEndereco.TabStop = false;
            this.lblEndereco.Text = "Endereço";
            // 
            // lblNumero
            // 
            this.lblNumero.BackColor = System.Drawing.Color.LightGray;
            this.lblNumero.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblNumero.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNumero.Location = new System.Drawing.Point(12, 115);
            this.lblNumero.Name = "lblNumero";
            this.lblNumero.ReadOnly = true;
            this.lblNumero.Size = new System.Drawing.Size(137, 21);
            this.lblNumero.TabIndex = 3;
            this.lblNumero.TabStop = false;
            this.lblNumero.Text = "Número";
            // 
            // lblComplemento
            // 
            this.lblComplemento.BackColor = System.Drawing.Color.LightGray;
            this.lblComplemento.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblComplemento.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblComplemento.Location = new System.Drawing.Point(12, 135);
            this.lblComplemento.Name = "lblComplemento";
            this.lblComplemento.ReadOnly = true;
            this.lblComplemento.Size = new System.Drawing.Size(137, 21);
            this.lblComplemento.TabIndex = 4;
            this.lblComplemento.TabStop = false;
            this.lblComplemento.Text = "Complemento";
            // 
            // lblBairro
            // 
            this.lblBairro.BackColor = System.Drawing.Color.LightGray;
            this.lblBairro.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblBairro.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblBairro.Location = new System.Drawing.Point(12, 155);
            this.lblBairro.Name = "lblBairro";
            this.lblBairro.ReadOnly = true;
            this.lblBairro.Size = new System.Drawing.Size(137, 21);
            this.lblBairro.TabIndex = 5;
            this.lblBairro.TabStop = false;
            this.lblBairro.Text = "Bairro";
            // 
            // lblCep
            // 
            this.lblCep.BackColor = System.Drawing.Color.LightGray;
            this.lblCep.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblCep.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCep.Location = new System.Drawing.Point(12, 175);
            this.lblCep.Name = "lblCep";
            this.lblCep.ReadOnly = true;
            this.lblCep.Size = new System.Drawing.Size(137, 21);
            this.lblCep.TabIndex = 6;
            this.lblCep.TabStop = false;
            this.lblCep.Text = "CEP";
            // 
            // lblUF
            // 
            this.lblUF.BackColor = System.Drawing.Color.LightGray;
            this.lblUF.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblUF.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblUF.Location = new System.Drawing.Point(12, 195);
            this.lblUF.Name = "lblUF";
            this.lblUF.ReadOnly = true;
            this.lblUF.Size = new System.Drawing.Size(137, 21);
            this.lblUF.TabIndex = 7;
            this.lblUF.TabStop = false;
            this.lblUF.Text = "UF";
            // 
            // lblCidade
            // 
            this.lblCidade.BackColor = System.Drawing.Color.LightGray;
            this.lblCidade.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblCidade.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCidade.Location = new System.Drawing.Point(12, 215);
            this.lblCidade.Name = "lblCidade";
            this.lblCidade.ReadOnly = true;
            this.lblCidade.Size = new System.Drawing.Size(137, 21);
            this.lblCidade.TabIndex = 8;
            this.lblCidade.TabStop = false;
            this.lblCidade.Text = "Cidade";
            // 
            // lblEmail
            // 
            this.lblEmail.BackColor = System.Drawing.Color.LightGray;
            this.lblEmail.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblEmail.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblEmail.Location = new System.Drawing.Point(12, 235);
            this.lblEmail.Name = "lblEmail";
            this.lblEmail.ReadOnly = true;
            this.lblEmail.Size = new System.Drawing.Size(137, 21);
            this.lblEmail.TabIndex = 9;
            this.lblEmail.TabStop = false;
            this.lblEmail.Text = "E-mail";
            // 
            // lblCelular
            // 
            this.lblCelular.BackColor = System.Drawing.Color.LightGray;
            this.lblCelular.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblCelular.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCelular.Location = new System.Drawing.Point(12, 275);
            this.lblCelular.Name = "lblCelular";
            this.lblCelular.ReadOnly = true;
            this.lblCelular.Size = new System.Drawing.Size(137, 21);
            this.lblCelular.TabIndex = 10;
            this.lblCelular.TabStop = false;
            this.lblCelular.Text = "Celular";
            // 
            // lblTelefone
            // 
            this.lblTelefone.BackColor = System.Drawing.Color.LightGray;
            this.lblTelefone.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblTelefone.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTelefone.Location = new System.Drawing.Point(12, 255);
            this.lblTelefone.Name = "lblTelefone";
            this.lblTelefone.ReadOnly = true;
            this.lblTelefone.Size = new System.Drawing.Size(137, 21);
            this.lblTelefone.TabIndex = 11;
            this.lblTelefone.TabStop = false;
            this.lblTelefone.Text = "Telefone";
            // 
            // text_nome
            // 
            this.text_nome.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.text_nome.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.text_nome.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.text_nome.Location = new System.Drawing.Point(148, 15);
            this.text_nome.MaxLength = 100;
            this.text_nome.Name = "text_nome";
            this.text_nome.Size = new System.Drawing.Size(578, 21);
            this.text_nome.TabIndex = 1;
            // 
            // text_crm
            // 
            this.text_crm.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.text_crm.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.text_crm.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.text_crm.Location = new System.Drawing.Point(148, 35);
            this.text_crm.MaxLength = 12;
            this.text_crm.Name = "text_crm";
            this.text_crm.Size = new System.Drawing.Size(578, 21);
            this.text_crm.TabIndex = 2;
            // 
            // text_endereco
            // 
            this.text_endereco.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.text_endereco.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.text_endereco.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.text_endereco.Location = new System.Drawing.Point(148, 95);
            this.text_endereco.MaxLength = 100;
            this.text_endereco.Name = "text_endereco";
            this.text_endereco.Size = new System.Drawing.Size(578, 21);
            this.text_endereco.TabIndex = 5;
            // 
            // text_numero
            // 
            this.text_numero.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.text_numero.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.text_numero.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.text_numero.Location = new System.Drawing.Point(148, 115);
            this.text_numero.MaxLength = 10;
            this.text_numero.Name = "text_numero";
            this.text_numero.Size = new System.Drawing.Size(578, 21);
            this.text_numero.TabIndex = 6;
            // 
            // text_complemento
            // 
            this.text_complemento.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.text_complemento.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.text_complemento.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.text_complemento.Location = new System.Drawing.Point(148, 135);
            this.text_complemento.MaxLength = 100;
            this.text_complemento.Name = "text_complemento";
            this.text_complemento.Size = new System.Drawing.Size(578, 21);
            this.text_complemento.TabIndex = 7;
            // 
            // text_bairro
            // 
            this.text_bairro.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.text_bairro.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.text_bairro.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.text_bairro.Location = new System.Drawing.Point(148, 155);
            this.text_bairro.MaxLength = 50;
            this.text_bairro.Name = "text_bairro";
            this.text_bairro.Size = new System.Drawing.Size(578, 21);
            this.text_bairro.TabIndex = 8;
            // 
            // text_cep
            // 
            this.text_cep.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.text_cep.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.text_cep.Location = new System.Drawing.Point(148, 175);
            this.text_cep.Mask = "99,999-999";
            this.text_cep.Name = "text_cep";
            this.text_cep.PromptChar = ' ';
            this.text_cep.Size = new System.Drawing.Size(578, 21);
            this.text_cep.TabIndex = 9;
            this.text_cep.Leave += new System.EventHandler(this.text_cep_Leave);
            // 
            // cb_uf
            // 
            this.cb_uf.BackColor = System.Drawing.Color.LightGray;
            this.cb_uf.BorderColor = System.Drawing.Color.DimGray;
            this.cb_uf.BorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid;
            this.cb_uf.FormattingEnabled = true;
            this.cb_uf.Location = new System.Drawing.Point(148, 195);
            this.cb_uf.Name = "cb_uf";
            this.cb_uf.Size = new System.Drawing.Size(578, 21);
            this.cb_uf.TabIndex = 10;
            this.cb_uf.SelectedIndexChanged += new System.EventHandler(this.cb_uf_SelectedIndexChanged);
            // 
            // cbCidade
            // 
            this.cbCidade.BackColor = System.Drawing.Color.LightGray;
            this.cbCidade.BorderColor = System.Drawing.Color.DimGray;
            this.cbCidade.BorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid;
            this.cbCidade.FormattingEnabled = true;
            this.cbCidade.Location = new System.Drawing.Point(148, 215);
            this.cbCidade.Name = "cbCidade";
            this.cbCidade.Size = new System.Drawing.Size(578, 21);
            this.cbCidade.TabIndex = 11;
            this.cbCidade.Click += new System.EventHandler(this.cbCidade_Click);
            // 
            // text_telefone2
            // 
            this.text_telefone2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.text_telefone2.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.text_telefone2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.text_telefone2.Location = new System.Drawing.Point(148, 275);
            this.text_telefone2.MaxLength = 15;
            this.text_telefone2.Name = "text_telefone2";
            this.text_telefone2.Size = new System.Drawing.Size(578, 21);
            this.text_telefone2.TabIndex = 14;
            // 
            // text_email
            // 
            this.text_email.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.text_email.CharacterCasing = System.Windows.Forms.CharacterCasing.Lower;
            this.text_email.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.text_email.Location = new System.Drawing.Point(148, 235);
            this.text_email.MaxLength = 50;
            this.text_email.Name = "text_email";
            this.text_email.Size = new System.Drawing.Size(578, 21);
            this.text_email.TabIndex = 12;
            // 
            // text_telefone1
            // 
            this.text_telefone1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.text_telefone1.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.text_telefone1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.text_telefone1.Location = new System.Drawing.Point(148, 255);
            this.text_telefone1.MaxLength = 15;
            this.text_telefone1.Name = "text_telefone1";
            this.text_telefone1.Size = new System.Drawing.Size(578, 21);
            this.text_telefone1.TabIndex = 13;
            // 
            // IncluirMedicoErrorProvider
            // 
            this.IncluirMedicoErrorProvider.ContainerControl = this;
            // 
            // lblPisPasep
            // 
            this.lblPisPasep.BackColor = System.Drawing.Color.LightGray;
            this.lblPisPasep.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblPisPasep.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPisPasep.Location = new System.Drawing.Point(12, 295);
            this.lblPisPasep.Name = "lblPisPasep";
            this.lblPisPasep.ReadOnly = true;
            this.lblPisPasep.Size = new System.Drawing.Size(137, 21);
            this.lblPisPasep.TabIndex = 13;
            this.lblPisPasep.TabStop = false;
            this.lblPisPasep.Text = "PIS-PASEP";
            // 
            // textPisPasep
            // 
            this.textPisPasep.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textPisPasep.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textPisPasep.Location = new System.Drawing.Point(148, 295);
            this.textPisPasep.Mask = "999,99999,99-9";
            this.textPisPasep.Name = "textPisPasep";
            this.textPisPasep.PromptChar = ' ';
            this.textPisPasep.Size = new System.Drawing.Size(578, 21);
            this.textPisPasep.TabIndex = 15;
            // 
            // lblUfCrm
            // 
            this.lblUfCrm.BackColor = System.Drawing.Color.LightGray;
            this.lblUfCrm.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblUfCrm.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblUfCrm.Location = new System.Drawing.Point(12, 55);
            this.lblUfCrm.Name = "lblUfCrm";
            this.lblUfCrm.ReadOnly = true;
            this.lblUfCrm.Size = new System.Drawing.Size(137, 21);
            this.lblUfCrm.TabIndex = 17;
            this.lblUfCrm.TabStop = false;
            this.lblUfCrm.Text = "UF-CRM";
            // 
            // cbUfCrm
            // 
            this.cbUfCrm.BackColor = System.Drawing.Color.LightGray;
            this.cbUfCrm.BorderColor = System.Drawing.Color.DimGray;
            this.cbUfCrm.BorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid;
            this.cbUfCrm.FormattingEnabled = true;
            this.cbUfCrm.Location = new System.Drawing.Point(148, 55);
            this.cbUfCrm.Name = "cbUfCrm";
            this.cbUfCrm.Size = new System.Drawing.Size(578, 21);
            this.cbUfCrm.TabIndex = 3;
            // 
            // textDocumentoExtra
            // 
            this.textDocumentoExtra.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textDocumentoExtra.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.textDocumentoExtra.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textDocumentoExtra.Location = new System.Drawing.Point(148, 315);
            this.textDocumentoExtra.MaxLength = 15;
            this.textDocumentoExtra.Name = "textDocumentoExtra";
            this.textDocumentoExtra.Size = new System.Drawing.Size(578, 21);
            this.textDocumentoExtra.TabIndex = 16;
            // 
            // lblDocumentoExtra
            // 
            this.lblDocumentoExtra.BackColor = System.Drawing.Color.LightGray;
            this.lblDocumentoExtra.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblDocumentoExtra.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDocumentoExtra.Location = new System.Drawing.Point(12, 315);
            this.lblDocumentoExtra.Name = "lblDocumentoExtra";
            this.lblDocumentoExtra.ReadOnly = true;
            this.lblDocumentoExtra.Size = new System.Drawing.Size(137, 21);
            this.lblDocumentoExtra.TabIndex = 18;
            this.lblDocumentoExtra.TabStop = false;
            this.lblDocumentoExtra.Text = "Documento Extra";
            // 
            // btnAssinatura
            // 
            this.btnAssinatura.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnAssinatura.Image = global::SWS.Properties.Resources.Gravar;
            this.btnAssinatura.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnAssinatura.Location = new System.Drawing.Point(12, 523);
            this.btnAssinatura.Name = "btnAssinatura";
            this.btnAssinatura.Size = new System.Drawing.Size(75, 23);
            this.btnAssinatura.TabIndex = 18;
            this.btnAssinatura.Text = "&Abrir";
            this.btnAssinatura.UseVisualStyleBackColor = true;
            this.btnAssinatura.Click += new System.EventHandler(this.btnAssinatura_Click);
            // 
            // lblAssinatura
            // 
            this.lblAssinatura.AutoSize = true;
            this.lblAssinatura.Location = new System.Drawing.Point(9, 359);
            this.lblAssinatura.Name = "lblAssinatura";
            this.lblAssinatura.Size = new System.Drawing.Size(134, 13);
            this.lblAssinatura.TabIndex = 62;
            this.lblAssinatura.Text = "Assinatura de Documentos";
            // 
            // pbAssinatura
            // 
            this.pbAssinatura.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pbAssinatura.Location = new System.Drawing.Point(12, 375);
            this.pbAssinatura.Name = "pbAssinatura";
            this.pbAssinatura.Size = new System.Drawing.Size(333, 142);
            this.pbAssinatura.TabIndex = 61;
            this.pbAssinatura.TabStop = false;
            // 
            // lblCpf
            // 
            this.lblCpf.BackColor = System.Drawing.Color.LightGray;
            this.lblCpf.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblCpf.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCpf.Location = new System.Drawing.Point(12, 75);
            this.lblCpf.Name = "lblCpf";
            this.lblCpf.ReadOnly = true;
            this.lblCpf.Size = new System.Drawing.Size(137, 21);
            this.lblCpf.TabIndex = 63;
            this.lblCpf.TabStop = false;
            this.lblCpf.Text = "CPF";
            // 
            // textCpf
            // 
            this.textCpf.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textCpf.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textCpf.Location = new System.Drawing.Point(148, 75);
            this.textCpf.Mask = "999,999,999-99";
            this.textCpf.Name = "textCpf";
            this.textCpf.PromptChar = ' ';
            this.textCpf.Size = new System.Drawing.Size(578, 21);
            this.textCpf.TabIndex = 4;
            this.textCpf.Leave += new System.EventHandler(this.textCpf_Leave);
            // 
            // lblRqe
            // 
            this.lblRqe.BackColor = System.Drawing.Color.LightGray;
            this.lblRqe.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblRqe.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblRqe.Location = new System.Drawing.Point(12, 335);
            this.lblRqe.Name = "lblRqe";
            this.lblRqe.ReadOnly = true;
            this.lblRqe.Size = new System.Drawing.Size(137, 21);
            this.lblRqe.TabIndex = 64;
            this.lblRqe.TabStop = false;
            this.lblRqe.Text = "RQE";
            // 
            // textRqe
            // 
            this.textRqe.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textRqe.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.textRqe.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textRqe.Location = new System.Drawing.Point(148, 335);
            this.textRqe.MaxLength = 15;
            this.textRqe.Name = "textRqe";
            this.textRqe.Size = new System.Drawing.Size(578, 21);
            this.textRqe.TabIndex = 17;
            // 
            // frmMedicoIncluir
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(784, 561);
            this.Name = "frmMedicoIncluir";
            this.Text = "INCLUIR MÉDICO";
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.frmMedicoIncluir_KeyDown);
            this.spForm.Panel1.ResumeLayout(false);
            this.spForm.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.spForm)).EndInit();
            this.spForm.ResumeLayout(false);
            this.pnlForm.ResumeLayout(false);
            this.pnlForm.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.banner)).EndInit();
            this.flpAcao.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.IncluirMedicoErrorProvider)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbAssinatura)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.FlowLayoutPanel flpAcao;
        protected System.Windows.Forms.TextBox lblCrm;
        protected System.Windows.Forms.TextBox lblNome;
        protected System.Windows.Forms.TextBox lblEndereco;
        protected System.Windows.Forms.TextBox lblComplemento;
        protected System.Windows.Forms.TextBox lblNumero;
        protected System.Windows.Forms.TextBox lblBairro;
        protected System.Windows.Forms.TextBox lblCep;
        protected System.Windows.Forms.TextBox lblCidade;
        protected System.Windows.Forms.TextBox lblUF;
        protected System.Windows.Forms.TextBox lblTelefone;
        protected System.Windows.Forms.TextBox lblCelular;
        protected System.Windows.Forms.TextBox lblEmail;
        protected System.Windows.Forms.TextBox text_nome;
        protected System.Windows.Forms.TextBox text_crm;
        protected System.Windows.Forms.TextBox text_endereco;
        protected System.Windows.Forms.TextBox text_numero;
        protected System.Windows.Forms.TextBox text_complemento;
        protected System.Windows.Forms.TextBox text_bairro;
        protected System.Windows.Forms.MaskedTextBox text_cep;
        protected ComboBoxWithBorder cb_uf;
        protected ComboBoxWithBorder cbCidade;
        protected System.Windows.Forms.TextBox text_telefone2;
        protected System.Windows.Forms.TextBox text_email;
        protected System.Windows.Forms.TextBox text_telefone1;
        protected System.Windows.Forms.MaskedTextBox textPisPasep;
        protected System.Windows.Forms.TextBox lblPisPasep;
        protected System.Windows.Forms.ErrorProvider IncluirMedicoErrorProvider;
        protected System.Windows.Forms.Button bt_fechar;
        protected System.Windows.Forms.Button bt_gravar;
        protected System.Windows.Forms.Button bt_limpar;
        protected ComboBoxWithBorder cbUfCrm;
        protected System.Windows.Forms.TextBox lblUfCrm;
        protected System.Windows.Forms.TextBox textDocumentoExtra;
        protected System.Windows.Forms.TextBox lblDocumentoExtra;
        protected System.Windows.Forms.Button btnAssinatura;
        protected System.Windows.Forms.Label lblAssinatura;
        protected System.Windows.Forms.PictureBox pbAssinatura;
        protected System.Windows.Forms.MaskedTextBox textCpf;
        protected System.Windows.Forms.TextBox lblCpf;
        protected System.Windows.Forms.TextBox textRqe;
        protected System.Windows.Forms.TextBox lblRqe;
    }
}