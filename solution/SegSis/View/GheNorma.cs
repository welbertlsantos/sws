﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using SWS.Facade;
using SWS.Entidade;
using SWS.View.Resources;

namespace SWS.View
{
    public partial class frm_GheNorma : BaseFormConsulta
    {

        private HashSet<Nota> normaSelecionada = new HashSet<Nota>();

        public HashSet<Nota> NormaSelecionada
        {
            get { return normaSelecionada; }
            set { normaSelecionada = value; }
        }
        
        public frm_GheNorma()
        {
            InitializeComponent();
            validaPermissoes();
        }

        private void validaPermissoes()
        {
            PermissionamentoFacade permissionamentoFacade = PermissionamentoFacade.getInstance();
            btn_novo.Enabled = permissionamentoFacade.hasPermission("NORMA", "INCLUIR");
        }

        private void btn_fechar_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btn_buscar_Click(object sender, EventArgs e)
        {
            try
            {
                montaDataGrid();
                text_descricao.Focus();

            }
            catch (Exception Ex)
            {
                MessageBox.Show(Ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void montaDataGrid()
        {

            EstudoFacade pcmsoFacade = EstudoFacade.getInstance();

            DataSet ds = pcmsoFacade.findNotaByFilter(new Nota(null, text_descricao.Text, ApplicationConstants.ATIVO, string.Empty));

            // Montando a gride de normas.

            grd_norma.DataSource = ds.Tables["Normas"].DefaultView;

            grd_norma.Columns[0].HeaderText = "ID";
            grd_norma.Columns[0].Visible = false;

            grd_norma.Columns[1].HeaderText = "Título";
            grd_norma.Columns[1].ReadOnly = true;
            
            grd_norma.Columns[2].HeaderText = "Situação";
            grd_norma.Columns[2].Visible = false;

            grd_norma.Columns[3].HeaderText = "Conteúdo";
            grd_norma.Columns[3].ReadOnly = true;
            
            grd_norma.Columns[4].HeaderText = "Selec";
            grd_norma.Columns[4].DisplayIndex = 0;

        }

        private void btn_novo_Click(object sender, EventArgs e)
        {
            frmNotaIncluir formNormaIncluir = new frmNotaIncluir();
            formNormaIncluir.ShowDialog();

            if (formNormaIncluir.Nota != null)
            {
                text_descricao.Text = formNormaIncluir.Nota.Descricao.ToUpper();
                btn_buscar.PerformClick();
            }

        }

        private void btn_OK_Click(object sender, EventArgs e)
        {
            try
            {
                foreach (DataGridViewRow dvRow in grd_norma.Rows)
                {
                    if ((Boolean)dvRow.Cells[4].Value == true)
                        normaSelecionada.Add(new Nota((Int64)dvRow.Cells[0].Value, (String)dvRow.Cells[1].Value, (String)dvRow.Cells[2].Value, (String)dvRow.Cells[3].Value));
                }

                this.Close();
            }
            catch (Exception Ex)
            {
                MessageBox.Show(Ex.Message,"Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        
    }
}
