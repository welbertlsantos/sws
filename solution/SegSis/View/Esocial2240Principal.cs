﻿using SWS.Entidade;
using SWS.Facade;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using SWS.Esocial;
using System.IO;
using SWS.Resources;
using System.Globalization;
using System.Xml.Serialization;
using System.Xml;
using SWS.View.ViewHelper;
using SWS.ViewHelper;

namespace SWS.View
{
    public partial class frmEsocial2240Principal : frmTemplate
    {
        private Cliente cliente;
        private string diretorioTemporario;
        private string diretorioExportacao;
        private string[] arquivos;

        
        public frmEsocial2240Principal()
        {
            InitializeComponent();

            ComboHelper.comboMes(cbMesMonitoramento);
            ComboHelper.comboAno(cbAnoMonitoramento);

            /* setando valores default para os combos */

            cbMesMonitoramento.SelectedValue = DateTime.Now.Month.ToString();
            cbAnoMonitoramento.SelectedValue = DateTime.Now.Year.ToString();

            validaPermissoes();
        }
        private void validaPermissoes()
        {
            PermissionamentoFacade permissionamentoFacade = PermissionamentoFacade.getInstance();

            btnIncluir.Enabled = permissionamentoFacade.hasPermission("ESOCIAL-2240", "INCLUIR");
            btnPesquisa.Enabled = permissionamentoFacade.hasPermission("ESOCIAL-2240", "PESQUISAR");
            btnExcluir.Enabled = permissionamentoFacade.hasPermission("ESOCIAL-2240", "EXCLUIR");
            btnExportar.Enabled = permissionamentoFacade.hasPermission("ESOCIAL-2240", "EXPORTAR");
        }


        private void btnIncluir_Click(object sender, EventArgs e)
        {
            frmEsocial2240Lote gerarLote = new frmEsocial2240Lote();
            gerarLote.ShowDialog();
        }

        private void btnFechar_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnCliente_Click(object sender, EventArgs e)
        {
            try
            {
                frmClienteSelecionar selecionarCliente = new frmClienteSelecionar(null, null, false, null, null, null, null, null, null);
                selecionarCliente.ShowDialog();

                if (selecionarCliente.Cliente != null)
                {
                    cliente = selecionarCliente.Cliente;
                    this.textCliente.Text = this.cliente.RazaoSocial + " - " + ((int)this.cliente.tipoCliente() == 1 ? ValidaCampoHelper.FormataCnpj(this.cliente.Cnpj) : ValidaCampoHelper.FormataCpf(this.cliente.Cnpj));
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btnExcluirCliente_Click(object sender, EventArgs e)
        {
            try
            {
                if (cliente == null)
                    throw new Exception("Você deve selecionar primeiro o cliente.");

                cliente = null;
                textCliente.Text = string.Empty;

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        public void montaDataGrid()
        {
            try
            {
                this.Cursor = Cursors.WaitCursor;

                this.dgvLote.Columns.Clear();
                textTotal.Text = String.Empty;

                EsocialFacade esocialFacade = EsocialFacade.getInstance();

                LoteEsocial loteEsocial = new LoteEsocial();
                loteEsocial.cliente = cliente;
                loteEsocial.Tipo = "2240";

                DataSet ds = esocialFacade.findEsocialByFilter(loteEsocial, new DateTime(Convert.ToInt32(((SelectItem)cbAnoMonitoramento.SelectedItem).Valor), Convert.ToInt32(((SelectItem)cbMesMonitoramento.SelectedItem).Valor), 1), new DateTime(Convert.ToInt32(((SelectItem)cbAnoMonitoramento.SelectedItem).Valor), Convert.ToInt32(((SelectItem)cbMesMonitoramento.SelectedItem).Valor), DateTime.DaysInMonth(Convert.ToInt32(((SelectItem)cbAnoMonitoramento.SelectedItem).Valor), Convert.ToInt32(((SelectItem)cbMesMonitoramento.SelectedItem).Valor))));

                dgvLote.DataSource = ds.Tables["Lotes"].DefaultView;

                // montando ids das colunas

                dgvLote.Columns[0].HeaderText = "id_esocial_lote";
                dgvLote.Columns[0].Visible = false;

                dgvLote.Columns[1].HeaderText = "Código";
                dgvLote.Columns[2].HeaderText = "Razão Social";
                dgvLote.Columns[3].HeaderText = "CNPJ";
                dgvLote.Columns[4].HeaderText = "Tipo";
                dgvLote.Columns[5].HeaderText = "Data Criação";
                dgvLote.Columns[6].HeaderText = "Data de Movimento";

                textTotal.Text = dgvLote.RowCount.ToString();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }

        private void btnPesquisa_Click(object sender, EventArgs e)
        {
            try
            {

                montaDataGrid();

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btnExcluir_Click(object sender, EventArgs e)
        {
            try
            {
                if (dgvLote.CurrentRow == null)
                    throw new Exception("Selecione um lote para excluir.");

                EsocialFacade esocialFacade = EsocialFacade.getInstance();

                if (MessageBox.Show("Deseja excluir o lote E-Social gerado?", "Confirmação", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                {
                    LoteEsocial esocialLoteExcluir = new LoteEsocial();
                    esocialLoteExcluir.Id = (long)dgvLote.CurrentRow.Cells[0].Value;
                    esocialFacade.deleteEsocialLote(esocialLoteExcluir);
                    MessageBox.Show("Lote Esocial excluído com sucesso.", "Confirmação", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    montaDataGrid();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btnExportar_Click(object sender, EventArgs e)
        {
            this.Cursor = Cursors.WaitCursor;
            CultureInfo myCult = new CultureInfo("pt-Br");
            myCult.NumberFormat.NumberDecimalDigits = 4;
            myCult.NumberFormat.NumberDecimalSeparator = ".";

            /* carregando classes do modelo xml */


            EsocialFacade esocialFacade = EsocialFacade.getInstance();
            PermissionamentoFacade permissionamentoFacade = PermissionamentoFacade.getInstance();

            List<string> orgaosEpeciais = new List<string> { "CRM", "CREA" };


            if (dgvLote.CurrentRow == null)
                throw new Exception("Selecione uma linha para exportação");

            diretorioTemporario = GetTemporaryDirectory();

            LoteEsocial loteEsocial = esocialFacade.findLoteEsocialById((long)dgvLote.CurrentRow.Cells[0].Value);

            DateTime sequenciaData = DateTime.Now;
            List<Monitoramento> monitoramentos = esocialFacade.findAllByLote(loteEsocial);
            Monitoramento monitoramentoCopy = new Monitoramento();
                
            try
            {

                foreach (Monitoramento monitoramento in monitoramentos)
                {
                    monitoramentoCopy = monitoramento;
                    string nomeFuncionario = monitoramento.ClienteFuncaoFuncionario != null ? monitoramento.ClienteFuncaoFuncionario.Funcionario.Nome : monitoramento.ClienteFuncionario.Funcionario.Nome;
                    
                    modelo2240.IdeEmpregador ideEmpregador = new Esocial.modelo2240.IdeEmpregador();
                    modelo2240.IdeVinculo ideVinculo = new Esocial.modelo2240.IdeVinculo();
                    modelo2240.RespReg respReg = new modelo2240.RespReg();
                    modelo2240.IdeEvento ideEvento = new modelo2240.IdeEvento();
                    modelo2240.InfoAmb infoAmb = new modelo2240.InfoAmb();
                    modelo2240.InfoAtiv infoAtiv = new modelo2240.InfoAtiv();
                    modelo2240.EpiCompl epiCompl = new modelo2240.EpiCompl();
                    modelo2240.Obs obs = new modelo2240.Obs();
                    modelo2240.Epi epi = new modelo2240.Epi();
                    modelo2240.EpcEpi epcEpi = new modelo2240.EpcEpi();
                    modelo2240.AgNoc agNoc = new modelo2240.AgNoc();
                    modelo2240.InfoExpRisco infoExpRisco = new modelo2240.InfoExpRisco();
                    modelo2240.EvtExpRisco evtExpRisco = new modelo2240.EvtExpRisco();
                    modelo2240.ESocial eSocial = new modelo2240.ESocial();

                    using (StreamWriter writer = new StreamWriter(diretorioTemporario + "//" + nomeFuncionario.Replace(" ", "_") + "_" + String.Format("{0:ddMMyyyy}", monitoramento.Periodo) + ".xml", false, Encoding.UTF8))
                    {
                        sequenciaData = sequenciaData.AddSeconds(1);

                        /* ideEvento */
                        ideEvento.indRetif = "1";
                        ideEvento.tpAmb = "1";
                        ideEvento.procEmi = "1";
                        Dictionary<string, string> configuracao = new Dictionary<string, string>();
                        string versao;
                        configuracao = permissionamentoFacade.findAllConfiguracao();
                        configuracao.TryGetValue(ConfigurationConstans.VERSAO_SISTEMA, out versao);
                        ideEvento.verProc = versao;



                        /* ideEmpregador */

                        ideEmpregador.tpInsc = monitoramento.Cliente.tipoCliente().ToString();
                        
                        ideEmpregador.nrInsc = monitoramento.Cliente.Cnpj.Length == 14
                            ? monitoramento.Cliente.Cnpj.Substring(0,8)
                            : monitoramento.Cliente.Cnpj;

                        /* ideVinculo */
                        ideVinculo.cpfTrab = monitoramento.ClienteFuncaoFuncionario != null ? monitoramento.ClienteFuncaoFuncionario.Funcionario.Cpf : monitoramento.ClienteFuncionario.Funcionario.Cpf;
                        ideVinculo.matricula = monitoramento.ClienteFuncaoFuncionario != null ? monitoramento.ClienteFuncaoFuncionario.Matricula : monitoramento.ClienteFuncionario.Matricula;

                        /* infoAmb */

                        if (monitoramento.Estudo.ClienteContratante == null)
                        {
                            infoAmb.localAmb = "1";
                            infoAmb.dscSetor = esocialFacade.findSetorInLotacaoFuncionarioByMonitoramento(monitoramento);
                            infoAmb.tpInsc = monitoramento.Cliente.tipoCliente().ToString();
                            infoAmb.nrInsc = monitoramento.Cliente.Cnpj;
                        }
                        else
                        {
                            infoAmb.localAmb = "2";
                            infoAmb.dscSetor = esocialFacade.findSetorInLotacaoFuncionarioByMonitoramento(monitoramento);
                            infoAmb.tpInsc = monitoramento.Estudo.ClienteContratante.tipoCliente().ToString();
                            infoAmb.nrInsc = monitoramento.Estudo.ClienteContratante.Cnpj;
                        }


                        /* infoAtiv */
                        string atividade = esocialFacade.findAtividadeDoFuncionarioByMonitoramento(monitoramento);
                        infoAtiv.dscAtivDes = string.IsNullOrEmpty(atividade) ? esocialFacade.findFuncaoLotacaoFuncionarioByMonitoramento(monitoramento) : atividade;


                        /* agNoc */
                        List<string> codigosEspeciais = new List<string> { "01.01.001", "01.02.001", "01.03.001", "01.04.001", "01.05.001", "01.06.001", "01.07.001", "01.08.001", "01.09.001", "01.10.001", "01.12.001", "01.13.001", "01.14.001", "01.15.001", "01.16.001", "01.17.001", "01.18.001", "05.01.001" };
                        List<string> codigosEspeciaisConcentracao = new List<string> { "01.18.001", "02.01.014" };
                        List<modelo2240.AgNoc> agentesIsMonitoramento = new List<modelo2240.AgNoc>();
                        modelo2240.AgNoc agNocTemp;
                        List<MonitoramentoGheFonteAgente> monitoramentoAgentes = esocialFacade.findAllMonitoramentoAgenteByMonitoramento(monitoramento);
                        List<MonitoramentoGheFonteAgente> somenteAgentesNaoIdentificados = new List<MonitoramentoGheFonteAgente>();

                        /* excluindo os agentes não identificados caso exista outros agentes. Caso tenha somente o não identificado ele será inserido. */

                        List<MonitoramentoGheFonteAgente> monitoramentoAgentesExcluindoNaoIdentificado = new List<MonitoramentoGheFonteAgente>();
                        if (monitoramentoAgentes.Count != 1)
                        {
                            foreach (MonitoramentoGheFonteAgente monitoramentoAgente in monitoramentoAgentes)
                            {
                                if (!string.Equals(monitoramentoAgente.GheFonteAgente.Agente.CodigoEsocial, "09.01.001"))
                                {
                                    monitoramentoAgentesExcluindoNaoIdentificado.Add(monitoramentoAgente);
                                }
                                else
                                {
                                    somenteAgentesNaoIdentificados.Add(monitoramentoAgente);
                                }
                            }
                        }
                        else
                        {
                            monitoramentoAgentesExcluindoNaoIdentificado = monitoramentoAgentes;
                        }

                        /* caso tenha somente agentes não identificados, somente um será incluído */
                        if (somenteAgentesNaoIdentificados.Count == monitoramentoAgentes.Count)
                        {
                            /* excluindo os elementos da lista e deixando apenas 1 */
                            MonitoramentoGheFonteAgente monitoramentoCopia = monitoramentoAgentes.First();
                            monitoramentoAgentesExcluindoNaoIdentificado.Clear();
                            monitoramentoAgentesExcluindoNaoIdentificado.Add(monitoramentoCopia);

                        }


                        foreach (MonitoramentoGheFonteAgente monitoramentoAgente in monitoramentoAgentesExcluindoNaoIdentificado)
                        {
                            agNocTemp = new modelo2240.AgNoc();
                            
                            /* caso o agente seja não identificado então a informação referente ao grupo epcEpi não deve ser informado no arquivo */

                            epcEpi = new modelo2240.EpcEpi();
                            epiCompl = new modelo2240.EpiCompl();
                            
                            agNocTemp.codAgNoc = monitoramentoAgente.GheFonteAgente.Agente.CodigoEsocial;
                            if (codigosEspeciais.Contains(monitoramentoAgente.GheFonteAgente.Agente.CodigoEsocial))
                            {
                                agNocTemp.dscAgNoc = monitoramentoAgente.GheFonteAgente.Agente.Descricao;
                            }

                            if (!string.IsNullOrEmpty(monitoramentoAgente.TipoAvaliacao))
                                agNocTemp.tpAval = monitoramentoAgente.TipoAvaliacao;
                            
                            if (string.Equals(monitoramentoAgente.TipoAvaliacao, "1"))
                            {
                                agNocTemp.intConc = string.Format(myCult, "{0:N}", monitoramentoAgente.IntensidadeConcentracao);

                                if (codigosEspeciaisConcentracao.Contains(monitoramentoAgente.GheFonteAgente.Agente.CodigoEsocial))
                                {
                                    agNocTemp.limTol = string.Format(myCult, "{0:N}", monitoramentoAgente.LimiteTolerancia);
                                }
                                agNocTemp.unMed = monitoramentoAgente.UnidadeMedicao;
                                agNocTemp.tecMedicao = monitoramentoAgente.TecnicaMedicao;

                                /* incluindo a informação do número do processo judicial para agentes que são 05.01.001 */

                                if (!String.IsNullOrEmpty(monitoramentoAgente.NumeroProcessoJudicial))
                                {
                                    agNocTemp.nrProcJud = monitoramentoAgente.NumeroProcessoJudicial;
                                }
                            }

                            /* incluindo a informação do número do processo judicial para agentes que são 05.01.001 */

                            if (monitoramentoAgente.GheFonteAgente.Agente.CodigoEsocial == "05.01.001"
                                && !String.IsNullOrEmpty(monitoramentoAgente.NumeroProcessoJudicial))
                            {
                                agNocTemp.nrProcJud = monitoramentoAgente.NumeroProcessoJudicial;
                            }


                            /* epcEpi */

                            if (!string.Equals(monitoramentoAgente.GheFonteAgente.Agente.CodigoEsocial, "09.01.001"))
                            {

                                epcEpi.utilizEPC = monitoramentoAgente.UtilizaEpc;

                                if (!string.IsNullOrEmpty(monitoramentoAgente.EficaciaEpc))
                                    epcEpi.eficEpc = monitoramentoAgente.EficaciaEpc;

                                epcEpi.utilizEPI = monitoramentoAgente.UtilizaEpi;

                                if (!string.IsNullOrEmpty(monitoramentoAgente.EficaciaEpi))
                                    epcEpi.eficEpi = monitoramentoAgente.EficaciaEpi;

                                /* somente pode existir epi e epiCompl  quando utilizaEpi for igual a 2 */

                                if (string.Equals(epcEpi.utilizEPI, "2"))
                                {
                                    List<modelo2240.Epi> epis = new List<modelo2240.Epi>();
                                    modelo2240.Epi epiTemp;
                                    List<EpiMonitoramento> epiMonitoramentos = esocialFacade.findAllEpiMonitoramentoByMonitoramentoGheFonteAgente(monitoramentoAgente);

                                    foreach (EpiMonitoramento epiMonitoramento in epiMonitoramentos)
                                    {
                                        epiTemp = new modelo2240.Epi();
                                        if (!string.IsNullOrEmpty(epiMonitoramento.Epi.Ca))
                                        {
                                            epiTemp.docAval = epiMonitoramento.Epi.Ca;
                                        }
                                        epis.Add(epiTemp);
                                    }

                                    if (epis.Count > 0)
                                    {
                                        epiCompl.medProtecao = monitoramentoAgente.MedidaProtecao;
                                        epiCompl.condFuncto = monitoramentoAgente.CondicaoFuncionamento;
                                        epiCompl.usoInint = monitoramentoAgente.UsoIniterrupto;
                                        epiCompl.przValid = monitoramentoAgente.PrazoValidade;
                                        epiCompl.periodicTroca = monitoramentoAgente.PeriodicidadeTroca;
                                        epiCompl.higienizacao = monitoramentoAgente.Higienizacao;

                                        epcEpi.epiCompl = epiCompl;
                                        epcEpi.epi = epis;
                                    }
                                }
                                
                                agNocTemp.epcEpi = epcEpi;
                            }
                            
                            agentesIsMonitoramento.Add(agNocTemp);
                        }

                        /* responsável pela monitoração */
                        respReg.cpfResp = monitoramento.UsuarioResponsavel.Cpf;
                        respReg.ideOC = string.Equals(monitoramento.UsuarioResponsavel.OrgaoEmissor, "CRM") ? "1" : string.Equals(monitoramento.UsuarioResponsavel.OrgaoEmissor, "CREA") ? "4" : "9";

                        if (!orgaosEpeciais.Contains(monitoramento.UsuarioResponsavel.OrgaoEmissor))
                            respReg.dscOC = monitoramento.UsuarioResponsavel.OrgaoEmissor;

                        respReg.nrOC = monitoramento.UsuarioResponsavel.DocumentoExtra;
                        respReg.ufOC = monitoramento.UsuarioResponsavel.UfOrgaoEmissor;

                        /* Observação */
                        obs.obsCompl = monitoramento.Observacao;

                        /* infoExpRisco */
                        infoExpRisco.dtIniCondicao = string.Format("{0:yyyy-MM-dd}", monitoramento.DataInicioCondicao);
                        if (monitoramento.DataFimCondicao != null && (Convert.ToDateTime(monitoramento.DataFimCondicao) != Convert.ToDateTime("01/01/0001 00:00:00")))
                            infoExpRisco.dtFimCondicao = string.Format("{0:yyyy-MM-dd}", monitoramento.DataFimCondicao);

                        infoExpRisco.infoAmb = infoAmb;
                        infoExpRisco.infoAtiv = infoAtiv;
                        infoExpRisco.agNoc = agentesIsMonitoramento;
                        infoExpRisco.respReg = respReg;
                        infoExpRisco.obs = obs;

                        StringBuilder stringId = new StringBuilder();
                        stringId.Append("ID");
                        stringId.Append(monitoramento.Cliente.tipoCliente().ToString());

                        if (monitoramento.Cliente.Cnpj.Length == 14)
                        {
                            stringId.Append(monitoramento.Cliente.Cnpj.Substring(0, 8).PadRight(14, '0'));
                        }
                        else
                        {
                            stringId.Append(monitoramento.Cliente.Cnpj.PadRight(14, '0'));
                        }

                        stringId.Append(String.Format("{0:yyyyMMddHHmmss}", sequenciaData));
                        stringId.Append("1".PadLeft(5, '0'));

                        evtExpRisco.Id = stringId.ToString();
                        evtExpRisco.ideEvento = ideEvento;
                        evtExpRisco.ideEmpregador = ideEmpregador;
                        evtExpRisco.ideVinculo = ideVinculo;
                        evtExpRisco.infoExpRisco = infoExpRisco;

                        eSocial.evtExpRisco = evtExpRisco;

                        String xml = Serialize(eSocial);
                        writer.Write(xml);
                        writer.Flush();

                    }
                }

                /* gerar arquivo de saída do zip */
                FolderBrowserDialog fbd1 = new FolderBrowserDialog();
                fbd1.Description = "Selecione uma pasta onde o arquivo de exportação do E-Social evento 2240 será criado.";
                fbd1.RootFolder = Environment.SpecialFolder.MyComputer;
                fbd1.ShowNewFolderButton = true;

                DialogResult dr = fbd1.ShowDialog();
                //Exibe a caixa de diálogo
                if (dr == System.Windows.Forms.DialogResult.OK)
                {
                    //Exibe a pasta selecionada
                    diretorioExportacao = fbd1.SelectedPath;
                }

                /* lista todos os arquivos do diretorio temporário */
                arquivos = Directory.GetFiles(diretorioTemporario);

                /* setando o local de destino dos arquivos */
                string localDestino = diretorioExportacao + "\\" + dgvLote.CurrentRow.Cells[1].Value.ToString() + ".zip";

                /* criando arquivo zip */
                SWS.Helper.FileHelper.criarArquivoZip(arquivos, localDestino);

                MessageBox.Show("Arquivo gerado com sucesso", "Confirmação", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            catch (Exception ex)
            {
                StringBuilder query = new StringBuilder();
                query.Append("Problemas para gerar o monitoramento:" + System.Environment.NewLine);
                query.Append(monitoramentoCopy.ClienteFuncaoFuncionario.Funcionario.Nome + System.Environment.NewLine);
                query.Append(ex.Message);
                MessageBox.Show(query.ToString(), "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);

            }
            finally
            {
                this.Cursor = Cursors.Default;
            }

        }

        public string GetTemporaryDirectory()
        {
            string tempFolder = Path.GetTempFileName();
            File.Delete(tempFolder);
            Directory.CreateDirectory(tempFolder);

            return tempFolder;
        }

        private T Deserialize<T>(string input) where T : class
        {
            XmlSerializer ser = new XmlSerializer(typeof(T));

            XmlWriterSettings settings = new XmlWriterSettings()
            {
                Encoding = new UnicodeEncoding(false, false),
                Indent = false,
                OmitXmlDeclaration = false
            };

            using (StringReader sr = new StringReader(input))
            {
                return (T)ser.Deserialize(sr);
            }
        }

        private string Serialize<T>(T ObjectToSerialize)
        {
            XmlSerializer xmlSerializer = new XmlSerializer(ObjectToSerialize.GetType());
            var xmlnsEmpty = new XmlSerializerNamespaces();
            xmlnsEmpty.Add("", "http://www.esocial.gov.br/schema/evt/evtExpRisco/v_S_01_02_00");

            using (Utf8StringWriter textWriter = new Utf8StringWriter())
            {
                xmlSerializer.Serialize(textWriter, ObjectToSerialize, xmlnsEmpty);
                return textWriter.ToString().Replace("utf-8", "UTF-8");
            }
        }

        public sealed class Utf8StringWriter : StringWriter
        {
            public override Encoding Encoding
            {
                get
                {
                    return Encoding.UTF8;
                }
            }
        }
    }
}
