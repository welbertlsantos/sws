﻿namespace SWS.View
{
    partial class frmMonitoramentoFuncionario
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        protected void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle7 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle8 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle9 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle10 = new System.Windows.Forms.DataGridViewCellStyle();
            this.flowLayoutPanel1 = new System.Windows.Forms.FlowLayoutPanel();
            this.btnGravar = new System.Windows.Forms.Button();
            this.btnFechar = new System.Windows.Forms.Button();
            this.lblPcmso = new System.Windows.Forms.TextBox();
            this.lblFuncao = new System.Windows.Forms.TextBox();
            this.lblCnpj = new System.Windows.Forms.TextBox();
            this.lblCliente = new System.Windows.Forms.TextBox();
            this.lblNome = new System.Windows.Forms.TextBox();
            this.lblCpf = new System.Windows.Forms.TextBox();
            this.textPcmso = new System.Windows.Forms.MaskedTextBox();
            this.textFuncao = new System.Windows.Forms.TextBox();
            this.textCliente = new System.Windows.Forms.TextBox();
            this.textCnpj = new System.Windows.Forms.MaskedTextBox();
            this.textNome = new System.Windows.Forms.TextBox();
            this.textCpf = new System.Windows.Forms.MaskedTextBox();
            this.grb_ghe = new System.Windows.Forms.GroupBox();
            this.dgvGhe = new System.Windows.Forms.DataGridView();
            this.grbRisco = new System.Windows.Forms.GroupBox();
            this.dgvRisco = new System.Windows.Forms.DataGridView();
            this.grbMonitoramentoAgente = new System.Windows.Forms.GroupBox();
            this.dgvMonitoramentoAgente = new System.Windows.Forms.DataGridView();
            this.flpMonitoramentoAgente = new System.Windows.Forms.FlowLayoutPanel();
            this.btnIncluirMonitoramentoAgente = new System.Windows.Forms.Button();
            this.btnAlterarMonitoramentoAgente = new System.Windows.Forms.Button();
            this.btnExcluirMonitoramentoAgente = new System.Windows.Forms.Button();
            this.lblResponsavel = new System.Windows.Forms.TextBox();
            this.textResponsavel = new System.Windows.Forms.TextBox();
            this.btnExcluirResponsavel = new System.Windows.Forms.Button();
            this.btnIncluirResponsavel = new System.Windows.Forms.Button();
            this.lblDataInicio = new System.Windows.Forms.TextBox();
            this.dataInicioMonitoramento = new System.Windows.Forms.DateTimePicker();
            this.gbrObservacao = new System.Windows.Forms.GroupBox();
            this.textObservacao = new System.Windows.Forms.TextBox();
            this.dataFimMonitormento = new System.Windows.Forms.DateTimePicker();
            this.lblDataFimCondicao = new System.Windows.Forms.TextBox();
            this.tpMonitoramento = new System.Windows.Forms.ToolTip(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.spForm)).BeginInit();
            this.spForm.Panel1.SuspendLayout();
            this.spForm.Panel2.SuspendLayout();
            this.spForm.SuspendLayout();
            this.pnlForm.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.banner)).BeginInit();
            this.flowLayoutPanel1.SuspendLayout();
            this.grb_ghe.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvGhe)).BeginInit();
            this.grbRisco.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvRisco)).BeginInit();
            this.grbMonitoramentoAgente.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvMonitoramentoAgente)).BeginInit();
            this.flpMonitoramentoAgente.SuspendLayout();
            this.gbrObservacao.SuspendLayout();
            this.SuspendLayout();
            // 
            // spForm
            // 
            // 
            // spForm.Panel1
            // 
            this.spForm.Panel1.Controls.Add(this.flowLayoutPanel1);
            // 
            // pnlForm
            // 
            this.pnlForm.Controls.Add(this.dataFimMonitormento);
            this.pnlForm.Controls.Add(this.lblDataFimCondicao);
            this.pnlForm.Controls.Add(this.gbrObservacao);
            this.pnlForm.Controls.Add(this.grbMonitoramentoAgente);
            this.pnlForm.Controls.Add(this.dataInicioMonitoramento);
            this.pnlForm.Controls.Add(this.lblDataInicio);
            this.pnlForm.Controls.Add(this.btnExcluirResponsavel);
            this.pnlForm.Controls.Add(this.btnIncluirResponsavel);
            this.pnlForm.Controls.Add(this.textResponsavel);
            this.pnlForm.Controls.Add(this.lblResponsavel);
            this.pnlForm.Controls.Add(this.flpMonitoramentoAgente);
            this.pnlForm.Controls.Add(this.grbRisco);
            this.pnlForm.Controls.Add(this.grb_ghe);
            this.pnlForm.Controls.Add(this.textPcmso);
            this.pnlForm.Controls.Add(this.textFuncao);
            this.pnlForm.Controls.Add(this.textCliente);
            this.pnlForm.Controls.Add(this.textCnpj);
            this.pnlForm.Controls.Add(this.textNome);
            this.pnlForm.Controls.Add(this.textCpf);
            this.pnlForm.Controls.Add(this.lblPcmso);
            this.pnlForm.Controls.Add(this.lblFuncao);
            this.pnlForm.Controls.Add(this.lblCnpj);
            this.pnlForm.Controls.Add(this.lblCliente);
            this.pnlForm.Controls.Add(this.lblNome);
            this.pnlForm.Controls.Add(this.lblCpf);
            this.pnlForm.Size = new System.Drawing.Size(764, 856);
            // 
            // flowLayoutPanel1
            // 
            this.flowLayoutPanel1.Controls.Add(this.btnGravar);
            this.flowLayoutPanel1.Controls.Add(this.btnFechar);
            this.flowLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.flowLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.flowLayoutPanel1.Name = "flowLayoutPanel1";
            this.flowLayoutPanel1.Size = new System.Drawing.Size(784, 35);
            this.flowLayoutPanel1.TabIndex = 0;
            // 
            // btnGravar
            // 
            this.btnGravar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnGravar.Image = global::SWS.Properties.Resources.Gravar;
            this.btnGravar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnGravar.Location = new System.Drawing.Point(3, 3);
            this.btnGravar.Name = "btnGravar";
            this.btnGravar.Size = new System.Drawing.Size(75, 23);
            this.btnGravar.TabIndex = 0;
            this.btnGravar.Text = "Gravar";
            this.btnGravar.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnGravar.UseVisualStyleBackColor = true;
            this.btnGravar.Click += new System.EventHandler(this.btnGravar_Click);
            // 
            // btnFechar
            // 
            this.btnFechar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnFechar.Image = global::SWS.Properties.Resources.close;
            this.btnFechar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnFechar.Location = new System.Drawing.Point(84, 3);
            this.btnFechar.Name = "btnFechar";
            this.btnFechar.Size = new System.Drawing.Size(75, 23);
            this.btnFechar.TabIndex = 2;
            this.btnFechar.Text = "Fechar";
            this.btnFechar.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnFechar.UseVisualStyleBackColor = true;
            this.btnFechar.Click += new System.EventHandler(this.btnFechar_Click);
            // 
            // lblPcmso
            // 
            this.lblPcmso.BackColor = System.Drawing.Color.LightGray;
            this.lblPcmso.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblPcmso.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPcmso.Location = new System.Drawing.Point(6, 103);
            this.lblPcmso.MaxLength = 15;
            this.lblPcmso.Name = "lblPcmso";
            this.lblPcmso.ReadOnly = true;
            this.lblPcmso.Size = new System.Drawing.Size(147, 21);
            this.lblPcmso.TabIndex = 34;
            this.lblPcmso.TabStop = false;
            this.lblPcmso.Text = "PCMSO";
            // 
            // lblFuncao
            // 
            this.lblFuncao.BackColor = System.Drawing.Color.LightGray;
            this.lblFuncao.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblFuncao.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblFuncao.Location = new System.Drawing.Point(6, 83);
            this.lblFuncao.MaxLength = 15;
            this.lblFuncao.Name = "lblFuncao";
            this.lblFuncao.ReadOnly = true;
            this.lblFuncao.Size = new System.Drawing.Size(147, 21);
            this.lblFuncao.TabIndex = 29;
            this.lblFuncao.TabStop = false;
            this.lblFuncao.Text = "Função";
            // 
            // lblCnpj
            // 
            this.lblCnpj.BackColor = System.Drawing.Color.LightGray;
            this.lblCnpj.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblCnpj.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCnpj.Location = new System.Drawing.Point(6, 63);
            this.lblCnpj.MaxLength = 15;
            this.lblCnpj.Name = "lblCnpj";
            this.lblCnpj.ReadOnly = true;
            this.lblCnpj.Size = new System.Drawing.Size(147, 21);
            this.lblCnpj.TabIndex = 30;
            this.lblCnpj.TabStop = false;
            this.lblCnpj.Text = "CNPJ";
            // 
            // lblCliente
            // 
            this.lblCliente.BackColor = System.Drawing.Color.LightGray;
            this.lblCliente.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblCliente.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCliente.Location = new System.Drawing.Point(6, 43);
            this.lblCliente.MaxLength = 15;
            this.lblCliente.Name = "lblCliente";
            this.lblCliente.ReadOnly = true;
            this.lblCliente.Size = new System.Drawing.Size(147, 21);
            this.lblCliente.TabIndex = 31;
            this.lblCliente.TabStop = false;
            this.lblCliente.Text = "Cliente";
            // 
            // lblNome
            // 
            this.lblNome.BackColor = System.Drawing.Color.LightGray;
            this.lblNome.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblNome.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNome.Location = new System.Drawing.Point(6, 3);
            this.lblNome.MaxLength = 15;
            this.lblNome.Name = "lblNome";
            this.lblNome.ReadOnly = true;
            this.lblNome.Size = new System.Drawing.Size(147, 21);
            this.lblNome.TabIndex = 32;
            this.lblNome.TabStop = false;
            this.lblNome.Text = "Nome do Funcionário";
            // 
            // lblCpf
            // 
            this.lblCpf.BackColor = System.Drawing.Color.LightGray;
            this.lblCpf.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblCpf.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCpf.Location = new System.Drawing.Point(6, 23);
            this.lblCpf.MaxLength = 15;
            this.lblCpf.Name = "lblCpf";
            this.lblCpf.ReadOnly = true;
            this.lblCpf.Size = new System.Drawing.Size(147, 21);
            this.lblCpf.TabIndex = 33;
            this.lblCpf.TabStop = false;
            this.lblCpf.Text = "CPF";
            // 
            // textPcmso
            // 
            this.textPcmso.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.textPcmso.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.textPcmso.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textPcmso.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textPcmso.ForeColor = System.Drawing.Color.Blue;
            this.textPcmso.Location = new System.Drawing.Point(152, 103);
            this.textPcmso.Mask = "0000,00,000000/00";
            this.textPcmso.Name = "textPcmso";
            this.textPcmso.PromptChar = ' ';
            this.textPcmso.ReadOnly = true;
            this.textPcmso.Size = new System.Drawing.Size(585, 21);
            this.textPcmso.TabIndex = 40;
            this.textPcmso.TabStop = false;
            // 
            // textFuncao
            // 
            this.textFuncao.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.textFuncao.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.textFuncao.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textFuncao.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.textFuncao.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textFuncao.ForeColor = System.Drawing.Color.Blue;
            this.textFuncao.Location = new System.Drawing.Point(152, 83);
            this.textFuncao.MaxLength = 100;
            this.textFuncao.Name = "textFuncao";
            this.textFuncao.ReadOnly = true;
            this.textFuncao.Size = new System.Drawing.Size(585, 21);
            this.textFuncao.TabIndex = 35;
            this.textFuncao.TabStop = false;
            // 
            // textCliente
            // 
            this.textCliente.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.textCliente.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.textCliente.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textCliente.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.textCliente.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textCliente.ForeColor = System.Drawing.Color.Blue;
            this.textCliente.Location = new System.Drawing.Point(152, 43);
            this.textCliente.MaxLength = 100;
            this.textCliente.Name = "textCliente";
            this.textCliente.ReadOnly = true;
            this.textCliente.Size = new System.Drawing.Size(585, 21);
            this.textCliente.TabIndex = 36;
            this.textCliente.TabStop = false;
            // 
            // textCnpj
            // 
            this.textCnpj.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.textCnpj.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.textCnpj.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textCnpj.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textCnpj.ForeColor = System.Drawing.Color.Blue;
            this.textCnpj.Location = new System.Drawing.Point(152, 63);
            this.textCnpj.Mask = "00,000,000/0000-00";
            this.textCnpj.Name = "textCnpj";
            this.textCnpj.PromptChar = ' ';
            this.textCnpj.ReadOnly = true;
            this.textCnpj.Size = new System.Drawing.Size(585, 21);
            this.textCnpj.TabIndex = 37;
            this.textCnpj.TabStop = false;
            // 
            // textNome
            // 
            this.textNome.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.textNome.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.textNome.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textNome.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.textNome.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textNome.ForeColor = System.Drawing.Color.Blue;
            this.textNome.Location = new System.Drawing.Point(152, 3);
            this.textNome.MaxLength = 100;
            this.textNome.Name = "textNome";
            this.textNome.ReadOnly = true;
            this.textNome.Size = new System.Drawing.Size(585, 21);
            this.textNome.TabIndex = 38;
            this.textNome.TabStop = false;
            // 
            // textCpf
            // 
            this.textCpf.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.textCpf.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.textCpf.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textCpf.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textCpf.ForeColor = System.Drawing.Color.Blue;
            this.textCpf.Location = new System.Drawing.Point(152, 23);
            this.textCpf.Mask = "999,999,999-99";
            this.textCpf.Name = "textCpf";
            this.textCpf.PromptChar = ' ';
            this.textCpf.ReadOnly = true;
            this.textCpf.Size = new System.Drawing.Size(585, 21);
            this.textCpf.TabIndex = 39;
            this.textCpf.TabStop = false;
            // 
            // grb_ghe
            // 
            this.grb_ghe.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.grb_ghe.Controls.Add(this.dgvGhe);
            this.grb_ghe.Location = new System.Drawing.Point(6, 318);
            this.grb_ghe.Name = "grb_ghe";
            this.grb_ghe.Size = new System.Drawing.Size(734, 136);
            this.grb_ghe.TabIndex = 70;
            this.grb_ghe.TabStop = false;
            this.grb_ghe.Text = "Ghe / Setor";
            // 
            // dgvGhe
            // 
            this.dgvGhe.AllowUserToAddRows = false;
            this.dgvGhe.AllowUserToDeleteRows = false;
            this.dgvGhe.AllowUserToOrderColumns = true;
            this.dgvGhe.AllowUserToResizeRows = false;
            dataGridViewCellStyle6.BackColor = System.Drawing.Color.Gainsboro;
            this.dgvGhe.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle6;
            this.dgvGhe.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this.dgvGhe.BackgroundColor = System.Drawing.Color.White;
            this.dgvGhe.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dataGridViewCellStyle7.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle7.BackColor = System.Drawing.Color.Silver;
            dataGridViewCellStyle7.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle7.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle7.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle7.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle7.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvGhe.DefaultCellStyle = dataGridViewCellStyle7;
            this.dgvGhe.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvGhe.Location = new System.Drawing.Point(3, 16);
            this.dgvGhe.MultiSelect = false;
            this.dgvGhe.Name = "dgvGhe";
            this.dgvGhe.RowHeadersVisible = false;
            this.dgvGhe.Size = new System.Drawing.Size(728, 117);
            this.dgvGhe.TabIndex = 1;
            this.dgvGhe.TabStop = false;
            this.dgvGhe.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvGhe_CellContentClick);
            // 
            // grbRisco
            // 
            this.grbRisco.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.grbRisco.Controls.Add(this.dgvRisco);
            this.grbRisco.Location = new System.Drawing.Point(6, 460);
            this.grbRisco.Name = "grbRisco";
            this.grbRisco.Size = new System.Drawing.Size(734, 136);
            this.grbRisco.TabIndex = 71;
            this.grbRisco.TabStop = false;
            this.grbRisco.Text = "Agentes / Riscos";
            // 
            // dgvRisco
            // 
            this.dgvRisco.AllowUserToAddRows = false;
            this.dgvRisco.AllowUserToDeleteRows = false;
            this.dgvRisco.AllowUserToResizeRows = false;
            dataGridViewCellStyle8.BackColor = System.Drawing.Color.Gainsboro;
            this.dgvRisco.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle8;
            this.dgvRisco.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this.dgvRisco.BackgroundColor = System.Drawing.Color.White;
            this.dgvRisco.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dataGridViewCellStyle9.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle9.BackColor = System.Drawing.Color.Silver;
            dataGridViewCellStyle9.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle9.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle9.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle9.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle9.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvRisco.DefaultCellStyle = dataGridViewCellStyle9;
            this.dgvRisco.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvRisco.Location = new System.Drawing.Point(3, 16);
            this.dgvRisco.MultiSelect = false;
            this.dgvRisco.Name = "dgvRisco";
            this.dgvRisco.RowHeadersWidth = 35;
            this.dgvRisco.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvRisco.Size = new System.Drawing.Size(728, 117);
            this.dgvRisco.TabIndex = 0;
            this.dgvRisco.TabStop = false;
            this.dgvRisco.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvRisco_CellContentClick);
            // 
            // grbMonitoramentoAgente
            // 
            this.grbMonitoramentoAgente.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.grbMonitoramentoAgente.Controls.Add(this.dgvMonitoramentoAgente);
            this.grbMonitoramentoAgente.Location = new System.Drawing.Point(6, 602);
            this.grbMonitoramentoAgente.Name = "grbMonitoramentoAgente";
            this.grbMonitoramentoAgente.Size = new System.Drawing.Size(734, 175);
            this.grbMonitoramentoAgente.TabIndex = 73;
            this.grbMonitoramentoAgente.TabStop = false;
            this.grbMonitoramentoAgente.Text = "Monitoramento dos Agentes";
            // 
            // dgvMonitoramentoAgente
            // 
            this.dgvMonitoramentoAgente.AllowUserToAddRows = false;
            this.dgvMonitoramentoAgente.AllowUserToDeleteRows = false;
            this.dgvMonitoramentoAgente.AllowUserToOrderColumns = true;
            this.dgvMonitoramentoAgente.AllowUserToResizeColumns = false;
            this.dgvMonitoramentoAgente.AllowUserToResizeRows = false;
            this.dgvMonitoramentoAgente.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this.dgvMonitoramentoAgente.BackgroundColor = System.Drawing.Color.White;
            this.dgvMonitoramentoAgente.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dataGridViewCellStyle10.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle10.BackColor = System.Drawing.Color.Silver;
            dataGridViewCellStyle10.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle10.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle10.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle10.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle10.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvMonitoramentoAgente.DefaultCellStyle = dataGridViewCellStyle10;
            this.dgvMonitoramentoAgente.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvMonitoramentoAgente.Location = new System.Drawing.Point(3, 16);
            this.dgvMonitoramentoAgente.MultiSelect = false;
            this.dgvMonitoramentoAgente.Name = "dgvMonitoramentoAgente";
            this.dgvMonitoramentoAgente.ReadOnly = true;
            this.dgvMonitoramentoAgente.RowHeadersWidth = 35;
            this.dgvMonitoramentoAgente.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvMonitoramentoAgente.Size = new System.Drawing.Size(728, 156);
            this.dgvMonitoramentoAgente.TabIndex = 3;
            this.dgvMonitoramentoAgente.TabStop = false;
            // 
            // flpMonitoramentoAgente
            // 
            this.flpMonitoramentoAgente.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.flpMonitoramentoAgente.Controls.Add(this.btnIncluirMonitoramentoAgente);
            this.flpMonitoramentoAgente.Controls.Add(this.btnAlterarMonitoramentoAgente);
            this.flpMonitoramentoAgente.Controls.Add(this.btnExcluirMonitoramentoAgente);
            this.flpMonitoramentoAgente.Location = new System.Drawing.Point(6, 780);
            this.flpMonitoramentoAgente.Name = "flpMonitoramentoAgente";
            this.flpMonitoramentoAgente.Size = new System.Drawing.Size(734, 32);
            this.flpMonitoramentoAgente.TabIndex = 74;
            // 
            // btnIncluirMonitoramentoAgente
            // 
            this.btnIncluirMonitoramentoAgente.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnIncluirMonitoramentoAgente.Image = global::SWS.Properties.Resources.icone_mais;
            this.btnIncluirMonitoramentoAgente.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnIncluirMonitoramentoAgente.Location = new System.Drawing.Point(3, 3);
            this.btnIncluirMonitoramentoAgente.Name = "btnIncluirMonitoramentoAgente";
            this.btnIncluirMonitoramentoAgente.Size = new System.Drawing.Size(75, 23);
            this.btnIncluirMonitoramentoAgente.TabIndex = 5;
            this.btnIncluirMonitoramentoAgente.TabStop = false;
            this.btnIncluirMonitoramentoAgente.Text = "&Incluir";
            this.btnIncluirMonitoramentoAgente.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnIncluirMonitoramentoAgente.UseVisualStyleBackColor = true;
            this.btnIncluirMonitoramentoAgente.Click += new System.EventHandler(this.btnIncluirMonitoramentoAgente_Click);
            // 
            // btnAlterarMonitoramentoAgente
            // 
            this.btnAlterarMonitoramentoAgente.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnAlterarMonitoramentoAgente.Image = global::SWS.Properties.Resources.Alterar;
            this.btnAlterarMonitoramentoAgente.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnAlterarMonitoramentoAgente.Location = new System.Drawing.Point(84, 3);
            this.btnAlterarMonitoramentoAgente.Name = "btnAlterarMonitoramentoAgente";
            this.btnAlterarMonitoramentoAgente.Size = new System.Drawing.Size(75, 23);
            this.btnAlterarMonitoramentoAgente.TabIndex = 67;
            this.btnAlterarMonitoramentoAgente.Text = "&Alterar";
            this.btnAlterarMonitoramentoAgente.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnAlterarMonitoramentoAgente.UseVisualStyleBackColor = true;
            this.btnAlterarMonitoramentoAgente.Click += new System.EventHandler(this.btnAlterarMonitoramentoAgente_Click);
            // 
            // btnExcluirMonitoramentoAgente
            // 
            this.btnExcluirMonitoramentoAgente.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnExcluirMonitoramentoAgente.Image = global::SWS.Properties.Resources.lixeira;
            this.btnExcluirMonitoramentoAgente.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnExcluirMonitoramentoAgente.Location = new System.Drawing.Point(165, 3);
            this.btnExcluirMonitoramentoAgente.Name = "btnExcluirMonitoramentoAgente";
            this.btnExcluirMonitoramentoAgente.Size = new System.Drawing.Size(75, 23);
            this.btnExcluirMonitoramentoAgente.TabIndex = 68;
            this.btnExcluirMonitoramentoAgente.Text = "&Excluir";
            this.btnExcluirMonitoramentoAgente.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnExcluirMonitoramentoAgente.UseVisualStyleBackColor = true;
            this.btnExcluirMonitoramentoAgente.Click += new System.EventHandler(this.btnExcluirMonitoramentoAgente_Click);
            // 
            // lblResponsavel
            // 
            this.lblResponsavel.BackColor = System.Drawing.Color.LightGray;
            this.lblResponsavel.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblResponsavel.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblResponsavel.Location = new System.Drawing.Point(6, 123);
            this.lblResponsavel.MaxLength = 15;
            this.lblResponsavel.Name = "lblResponsavel";
            this.lblResponsavel.ReadOnly = true;
            this.lblResponsavel.Size = new System.Drawing.Size(147, 21);
            this.lblResponsavel.TabIndex = 76;
            this.lblResponsavel.TabStop = false;
            this.lblResponsavel.Text = "Responsável Monit.";
            // 
            // textResponsavel
            // 
            this.textResponsavel.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.textResponsavel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.textResponsavel.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textResponsavel.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.textResponsavel.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textResponsavel.ForeColor = System.Drawing.Color.Blue;
            this.textResponsavel.Location = new System.Drawing.Point(152, 123);
            this.textResponsavel.MaxLength = 100;
            this.textResponsavel.Name = "textResponsavel";
            this.textResponsavel.ReadOnly = true;
            this.textResponsavel.Size = new System.Drawing.Size(526, 21);
            this.textResponsavel.TabIndex = 77;
            this.textResponsavel.TabStop = false;
            // 
            // btnExcluirResponsavel
            // 
            this.btnExcluirResponsavel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnExcluirResponsavel.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnExcluirResponsavel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnExcluirResponsavel.Image = global::SWS.Properties.Resources.close;
            this.btnExcluirResponsavel.Location = new System.Drawing.Point(706, 123);
            this.btnExcluirResponsavel.Name = "btnExcluirResponsavel";
            this.btnExcluirResponsavel.Size = new System.Drawing.Size(31, 21);
            this.btnExcluirResponsavel.TabIndex = 79;
            this.btnExcluirResponsavel.UseVisualStyleBackColor = true;
            this.btnExcluirResponsavel.Click += new System.EventHandler(this.btnExcluirResponsavel_Click);
            // 
            // btnIncluirResponsavel
            // 
            this.btnIncluirResponsavel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnIncluirResponsavel.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnIncluirResponsavel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnIncluirResponsavel.Image = global::SWS.Properties.Resources.busca;
            this.btnIncluirResponsavel.Location = new System.Drawing.Point(676, 123);
            this.btnIncluirResponsavel.Name = "btnIncluirResponsavel";
            this.btnIncluirResponsavel.Size = new System.Drawing.Size(31, 21);
            this.btnIncluirResponsavel.TabIndex = 78;
            this.btnIncluirResponsavel.UseVisualStyleBackColor = true;
            this.btnIncluirResponsavel.Click += new System.EventHandler(this.btnIncluirResponsavel_Click);
            // 
            // lblDataInicio
            // 
            this.lblDataInicio.BackColor = System.Drawing.Color.LightGray;
            this.lblDataInicio.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblDataInicio.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDataInicio.Location = new System.Drawing.Point(6, 143);
            this.lblDataInicio.MaxLength = 15;
            this.lblDataInicio.Name = "lblDataInicio";
            this.lblDataInicio.ReadOnly = true;
            this.lblDataInicio.Size = new System.Drawing.Size(147, 21);
            this.lblDataInicio.TabIndex = 80;
            this.lblDataInicio.TabStop = false;
            this.lblDataInicio.Text = "Dt Ini. Monit.";
            // 
            // dataInicioMonitoramento
            // 
            this.dataInicioMonitoramento.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dataInicioMonitoramento.Checked = false;
            this.dataInicioMonitoramento.CustomFormat = "dd/MM/yyyy";
            this.dataInicioMonitoramento.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dataInicioMonitoramento.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dataInicioMonitoramento.Location = new System.Drawing.Point(152, 143);
            this.dataInicioMonitoramento.Name = "dataInicioMonitoramento";
            this.dataInicioMonitoramento.ShowCheckBox = true;
            this.dataInicioMonitoramento.Size = new System.Drawing.Size(128, 21);
            this.dataInicioMonitoramento.TabIndex = 81;
            // 
            // gbrObservacao
            // 
            this.gbrObservacao.Controls.Add(this.textObservacao);
            this.gbrObservacao.Location = new System.Drawing.Point(6, 194);
            this.gbrObservacao.Name = "gbrObservacao";
            this.gbrObservacao.Size = new System.Drawing.Size(734, 118);
            this.gbrObservacao.TabIndex = 82;
            this.gbrObservacao.TabStop = false;
            this.gbrObservacao.Text = "Observação";
            // 
            // textObservacao
            // 
            this.textObservacao.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.textObservacao.Dock = System.Windows.Forms.DockStyle.Fill;
            this.textObservacao.Location = new System.Drawing.Point(3, 16);
            this.textObservacao.MaxLength = 999;
            this.textObservacao.Multiline = true;
            this.textObservacao.Name = "textObservacao";
            this.textObservacao.Size = new System.Drawing.Size(728, 99);
            this.textObservacao.TabIndex = 0;
            // 
            // dataFimMonitormento
            // 
            this.dataFimMonitormento.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dataFimMonitormento.Checked = false;
            this.dataFimMonitormento.CustomFormat = "dd/MM/yyyy";
            this.dataFimMonitormento.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dataFimMonitormento.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dataFimMonitormento.Location = new System.Drawing.Point(152, 163);
            this.dataFimMonitormento.Name = "dataFimMonitormento";
            this.dataFimMonitormento.ShowCheckBox = true;
            this.dataFimMonitormento.Size = new System.Drawing.Size(128, 21);
            this.dataFimMonitormento.TabIndex = 84;
            this.tpMonitoramento.SetToolTip(this.dataFimMonitormento, "Data de preenchimento obrigatório se a data inicio\r\ndo monitoramento for igual ou" +
        " maior do que \r\n16/01/2023");
            // 
            // lblDataFimCondicao
            // 
            this.lblDataFimCondicao.BackColor = System.Drawing.Color.LightGray;
            this.lblDataFimCondicao.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblDataFimCondicao.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDataFimCondicao.Location = new System.Drawing.Point(6, 163);
            this.lblDataFimCondicao.MaxLength = 15;
            this.lblDataFimCondicao.Name = "lblDataFimCondicao";
            this.lblDataFimCondicao.ReadOnly = true;
            this.lblDataFimCondicao.Size = new System.Drawing.Size(147, 21);
            this.lblDataFimCondicao.TabIndex = 83;
            this.lblDataFimCondicao.TabStop = false;
            this.lblDataFimCondicao.Text = "Dt Fim Monit";
            // 
            // tpMonitoramento
            // 
            this.tpMonitoramento.BackColor = System.Drawing.Color.White;
            this.tpMonitoramento.ForeColor = System.Drawing.Color.Blue;
            this.tpMonitoramento.ToolTipIcon = System.Windows.Forms.ToolTipIcon.Info;
            // 
            // frmMonitoramentoFuncionario
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(784, 561);
            this.Name = "frmMonitoramentoFuncionario";
            this.Text = "INCLUSÃO DO MONITORAMENTO DO FUNCIONÁRIO";
            this.spForm.Panel1.ResumeLayout(false);
            this.spForm.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.spForm)).EndInit();
            this.spForm.ResumeLayout(false);
            this.pnlForm.ResumeLayout(false);
            this.pnlForm.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.banner)).EndInit();
            this.flowLayoutPanel1.ResumeLayout(false);
            this.grb_ghe.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvGhe)).EndInit();
            this.grbRisco.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvRisco)).EndInit();
            this.grbMonitoramentoAgente.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvMonitoramentoAgente)).EndInit();
            this.flpMonitoramentoAgente.ResumeLayout(false);
            this.gbrObservacao.ResumeLayout(false);
            this.gbrObservacao.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        protected System.Windows.Forms.FlowLayoutPanel flowLayoutPanel1;
        protected System.Windows.Forms.Button btnGravar;
        protected System.Windows.Forms.Button btnFechar;
        protected System.Windows.Forms.TextBox lblPcmso;
        protected System.Windows.Forms.TextBox lblFuncao;
        protected System.Windows.Forms.TextBox lblCnpj;
        protected System.Windows.Forms.TextBox lblCliente;
        protected System.Windows.Forms.TextBox lblNome;
        protected System.Windows.Forms.TextBox lblCpf;
        protected System.Windows.Forms.MaskedTextBox textPcmso;
        protected System.Windows.Forms.TextBox textFuncao;
        protected System.Windows.Forms.TextBox textCliente;
        protected System.Windows.Forms.MaskedTextBox textCnpj;
        protected System.Windows.Forms.TextBox textNome;
        protected System.Windows.Forms.MaskedTextBox textCpf;
        protected System.Windows.Forms.GroupBox grb_ghe;
        protected System.Windows.Forms.DataGridView dgvGhe;
        protected System.Windows.Forms.GroupBox grbRisco;
        protected System.Windows.Forms.DataGridView dgvRisco;
        protected System.Windows.Forms.GroupBox grbMonitoramentoAgente;
        protected System.Windows.Forms.DataGridView dgvMonitoramentoAgente;
        protected System.Windows.Forms.TextBox textResponsavel;
        protected System.Windows.Forms.TextBox lblResponsavel;
        protected System.Windows.Forms.FlowLayoutPanel flpMonitoramentoAgente;
        protected System.Windows.Forms.Button btnIncluirMonitoramentoAgente;
        protected System.Windows.Forms.Button btnAlterarMonitoramentoAgente;
        protected System.Windows.Forms.Button btnExcluirMonitoramentoAgente;
        protected System.Windows.Forms.TextBox lblDataInicio;
        protected System.Windows.Forms.Button btnExcluirResponsavel;
        protected System.Windows.Forms.Button btnIncluirResponsavel;
        protected System.Windows.Forms.GroupBox gbrObservacao;
        protected System.Windows.Forms.TextBox textObservacao;
        protected System.Windows.Forms.DateTimePicker dataInicioMonitoramento;
        protected System.Windows.Forms.DateTimePicker dataFimMonitormento;
        private System.Windows.Forms.ToolTip tpMonitoramento;
        protected System.Windows.Forms.TextBox lblDataFimCondicao;
    }
}