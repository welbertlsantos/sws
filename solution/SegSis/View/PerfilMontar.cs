﻿using SWS.Facade;
using SWS.View.Resources;
using SWS.ViewHelper;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace SWS.View
{
    public partial class frmPerfilMontar : frmTemplate
    {
        private bool objetoEmCriacao = true;
        
        public frmPerfilMontar()
        {
            InitializeComponent();
            iniciaComboPerfil();
        }

        private void iniciaComboPerfil()
        {
            PermissionamentoFacade permissionamentoFacade = PermissionamentoFacade.getInstance();

            foreach (DataRow rows in permissionamentoFacade.listaTodosPerfis().Tables[0].Rows)
            {
                if (string.Equals(rows["situacao"].ToString(), ApplicationConstants.ATIVO))
                {
                    cbPerfil.Items.Add(new SelectItem(rows["id_perfil"].ToString(), rows["descricao"].ToString())); 
                }
            }

            cbPerfil.ValueMember = "nome";
            cbPerfil.DropDownStyle = ComboBoxStyle.DropDownList;

            
        }

        private void btn_fechar_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void cbPerfil_SelectedIndexChanged(object sender, EventArgs e)
        {
            limpaListaDominio();
            limpaListaAcao();

            if (!((SelectItem)cbPerfil.SelectedItem).Valor.Equals("-1"))
                iniciaListaDominio();
        }

        private void limpaListaAcao()
        {
            clb_acao.Items.Clear();
        }

        private void limpaListaDominio()
        {
            lb_dominio.Items.Clear();
        }

        private void iniciaListaDominio()
        {
            UsuarioFacade usuarioFacade = UsuarioFacade.getInstance();

            foreach (DataRow rows in usuarioFacade.listaTodosDominios().Tables[0].Rows)
                lb_dominio.Items.Add(new SelectItem(rows["id_dominio"].ToString(), rows["descricao"].ToString()));

            lb_dominio.ValueMember = "nome";
        }

        private void iniciaListaAcao()
        {
            PermissionamentoFacade permissionamentoFacade = PermissionamentoFacade.getInstance();
            
            objetoEmCriacao = true;

            foreach (DataRow rows in permissionamentoFacade.listaTodasAcoesByDominio(Convert.ToInt64(((SelectItem)lb_dominio.SelectedItem).Valor)).Tables[0].Rows)
                clb_acao.Items.Add(new SelectItem(rows["id_acao"].ToString(), rows["descricao"].ToString()), verificaItemSelecionado(Convert.ToInt64(((SelectItem)cbPerfil.SelectedItem).Valor), Convert.ToInt64(((SelectItem)lb_dominio.SelectedItem).Valor), Convert.ToInt64(rows["id_acao"].ToString())));

            objetoEmCriacao = false;

            clb_acao.ValueMember = "nome";
        }

        private CheckState verificaItemSelecionado(long idPerfil, long idDominio, long idAcao)
        {
            CheckState selecionado = CheckState.Unchecked;

            PermissionamentoFacade permissionamentoFacade = PermissionamentoFacade.getInstance();

            if (permissionamentoFacade.verificaAcaoSelecionadaParaPerfilDominio(idPerfil, idDominio, idAcao))
                selecionado = CheckState.Checked;

            return selecionado;
        }

        private void lb_dominio_SelectedIndexChanged(object sender, EventArgs e)
        {
            limpaListaAcao();
            iniciaListaAcao();
        }

        private void clb_acao_ItemCheck(object sender, ItemCheckEventArgs e)
        {
            PermissionamentoFacade permissionamentoFacade = PermissionamentoFacade.getInstance();
            
            SelectItem itemSelecionado = clb_acao.Items[e.Index] as SelectItem;

            if (!objetoEmCriacao)
            {
                if (e.CurrentValue.Equals(CheckState.Checked))
                    permissionamentoFacade.removeAcaoDominioFromPerfil(Convert.ToInt64(((SelectItem)cbPerfil.SelectedItem).Valor), Convert.ToInt64(((SelectItem)lb_dominio.SelectedItem).Valor), Convert.ToInt64(itemSelecionado.Valor));
                else
                    permissionamentoFacade.criaAcaoDominioFromPerfil(Convert.ToInt64(((SelectItem)cbPerfil.SelectedItem).Valor), Convert.ToInt64(((SelectItem)lb_dominio.SelectedItem).Valor), Convert.ToInt64(itemSelecionado.Valor));
            }
        }
    }
}
