﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace SWS.View
{
    public partial class frmEstudoJustificativa : frmTemplateConsulta
    {
        private string justificativa;

        public string Justificativa
        {
            get { return justificativa; }
            set { justificativa = value; }
        }
        
        public frmEstudoJustificativa()
        {
            InitializeComponent();
            ActiveControl = textJustificativa;
        }

        private void btnFechar_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnConfirmar_Click(object sender, EventArgs e)
        {
            try
            {
                if (string.IsNullOrEmpty(textJustificativa.Text.Trim()))
                    throw new Exception("Você deve inserir uma justificativa");

                if (textJustificativa.Text.Length < 15)
                    throw new Exception("A justifificativa deverá ter mais de 30 caracteres.");

                Justificativa = textJustificativa.Text.Trim();
                this.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }



        


    }
}
