﻿using SWS.Entidade;
using SWS.Facade;
using SWS.View.Resources;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace SWS.View
{
    public partial class frmNotaAlterar : frmNotaIncluir
    {
        public frmNotaAlterar(Nota nota) :base()
        {
            InitializeComponent();
            this.Nota = nota;
            textTitulo.Text = Nota.Descricao;
            textConteudo.Text = Nota.Conteudo;
        }

        protected override void btnGravar_Click(object sender, EventArgs e)
        {
            try
            {
                if (validaCamposObrigatorio())
                {
                    EstudoFacade pcmsoFacade = EstudoFacade.getInstance();
                    Nota = pcmsoFacade.updateNota(new Nota(Nota.Id, textTitulo.Text, ApplicationConstants.ATIVO, textConteudo.Text));
                    MessageBox.Show("Nota alterada com sucesso.", "Confirmação", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    this.Close();

                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }
    }
}
