﻿using SWS.Entidade;
using SWS.Facade;
using SWS.View.Resources;
using SWS.View.ViewHelper;
using SWS.ViewHelper;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace SWS.View
{
    public partial class frmCnaeAlterar : frmCnaeIncluir
    {
        
        public frmCnaeAlterar(Cnae cnae) :base()
        {
            InitializeComponent();
            this.Cnae = cnae;

            textCodigo.Text = cnae.CodCnae;
            textAtividade.Text = cnae.Atividade;
            cbGrauRisco.SelectedValue = cnae.GrauRisco;
            textGrupo.Text = cnae.Grupo;
            bt_incluir.Text = "Alterar";
        }

        protected override void bt_incluir_Click(object sender, EventArgs e)
        {
            this.IncluirCnaeErrorProvider.Clear();

            try
            {
                if (validaCamposObrigatorio())
                {

                    ClienteFacade clienteFacade = ClienteFacade.getInstance();

                    if (!validadorCnae(textCodigo.Text))
                        throw new Exception("CNAE inválido. Padrão aceito é 99.99-9/99.");

                    Cnae = clienteFacade.alteraCnae(new Cnae(Cnae.Id, ValidaCampoHelper.RetornaCnaSemFormato(textCodigo.Text), textAtividade.Text, (((SelectItem)cbGrauRisco.SelectedItem).Valor), textGrupo.Text, ApplicationConstants.ATIVO));

                    MessageBox.Show("Cnae alterado com sucesso.", "Confirmação", MessageBoxButtons.OK, MessageBoxIcon.Information);

                    this.Close();
                }
            }
            catch (Exception Ex)
            {
                MessageBox.Show(Ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
    }
}
