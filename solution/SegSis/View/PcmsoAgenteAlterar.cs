﻿using SWS.Entidade;
using SWS.Facade;
using SWS.View.Resources;
using SWS.View.ViewHelper;
using SWS.ViewHelper;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SWS.View
{
    public partial class frmPcmsoAgenteAlterar : frmPcmsoAgenteIncluir
    {
        public frmPcmsoAgenteAlterar(GheFonteAgente gheFonteAgente ) :base()
        {
            InitializeComponent();
            this.gheFonteAgente = gheFonteAgente;
            this.gradExposicao = this.gheFonteAgente.GradExposicao;
            this.gradEfeito = this.gheFonteAgente.GradEfeito;
            this.gradSoma = this.gheFonteAgente.GradSoma;
            this.agente = this.gheFonteAgente.Agente;
            btnAgenteIncluir.Enabled = false;
            btnAgenteExcluir.Enabled = false;
            textAgente.Text = gheFonteAgente.Agente.Descricao;
            montaComboExposicao(cbExposicao);
            montaComboEfeito(cbEfeito);
            ComboHelper.tempoExposicao(cbTempoExposicao);

            cbEfeito.SelectedValue = gheFonteAgente.GradEfeito.Id.ToString() ;
            cbExposicao.SelectedValue = gheFonteAgente.GradExposicao.Id.ToString();
            cbTempoExposicao.SelectedValue = gheFonteAgente.TempoExposicao;
            montaQuadroExplicativo();
        }

        protected override void btnGravar_Click(object sender, EventArgs e)
        {
            try
            {
                if (validaCamposTela())
                {
                    EstudoFacade pcmsoFacade = EstudoFacade.getInstance();

                    GheFonteAgente.GradEfeito = gradEfeito;
                    GheFonteAgente.GradExposicao = gradExposicao;
                    GheFonteAgente.GradSoma = gradSoma;
                    GheFonteAgente.Situacao = ApplicationConstants.ATIVO;
                    GheFonteAgente.TempoExposicao = ((SelectItem)cbTempoExposicao.SelectedItem).Valor;

                    GheFonteAgente = pcmsoFacade.updateGheFonteAgente(gheFonteAgente);
                    MessageBox.Show("Agente alterado com sucesso", "Confirmação", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    this.Close();
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }
    }
}
