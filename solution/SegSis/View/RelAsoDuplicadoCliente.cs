﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using SWS.Facade;
using SWS.Entidade;
using SWS.View.Resources;

namespace SWS.View
{
    public partial class frm_RelAsoDuplicadoCliente : BaseFormConsulta
    {

        frm_RelAsoDuplicado formRelatorioAsoDuplicado;
        private static String msg1 = "Selecione uma linha";


        public frm_RelAsoDuplicadoCliente(frm_RelAsoDuplicado formRelatorioAsoDuplicado)
        {
            InitializeComponent();
            this.formRelatorioAsoDuplicado = formRelatorioAsoDuplicado;
        }

        private void btn_buscar_Click(object sender, EventArgs e)
        {
            try
            {
                this.grd_cliente.Columns.Clear();
                montaDataGrid();
                text_razaoSocial.Focus();
            }
            catch (Exception Ex)
            {
                MessageBox.Show(Ex.Message);
            }
        }

        public void montaDataGrid()
        {
            ClienteFacade clienteFacade = ClienteFacade.getInstance();

            Cliente cliente = new Cliente();

            cliente.RazaoSocial = text_razaoSocial.Text;
            cliente.Fantasia = text_fantasia.Text;
            cliente.Situacao = ApplicationConstants.ATIVO;

            DataSet ds = clienteFacade.findClienteAtivoByFilter(cliente);

            grd_cliente.DataSource = ds.Tables["Clientes"].DefaultView;

            grd_cliente.Columns[0].HeaderText = "ID";
            grd_cliente.Columns[1].HeaderText = "Razão Social";

            grd_cliente.Columns[0].Visible = false;

        }

        private void btn_ok_Click(object sender, EventArgs e)
        {
            try
            {
                this.Cursor = Cursors.WaitCursor;

                if (grd_cliente.CurrentRow != null)
                {

                    ClienteFacade clienteFacade = ClienteFacade.getInstance();

                    Int64 id = (Int64)grd_cliente.CurrentRow.Cells[0].Value;

                    Cliente cliente = clienteFacade.findClienteById(id);

                    formRelatorioAsoDuplicado.setCliente(cliente);
                    formRelatorioAsoDuplicado.setTextoCliente(cliente.RazaoSocial);
                    this.Close();
                    
                }
                else
                {
                    throw new Exception(msg1);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }

        private void frm_RelAsoDuplicadoCliente_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                this.SelectNextControl(this.ActiveControl, !e.Shift, true, true, true);
            }
        }

        private void btn_fechar_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
