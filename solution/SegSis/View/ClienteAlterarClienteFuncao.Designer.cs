﻿namespace SegSis.View
{
    partial class frm_ClienteAlterarFuncaoBuscar
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frm_ClienteAlterarFuncaoBuscar));
            this.grb_paginacao = new System.Windows.Forms.GroupBox();
            this.btn_incluir = new System.Windows.Forms.Button();
            this.btn_fechar = new System.Windows.Forms.Button();
            this.btn_novo = new System.Windows.Forms.Button();
            this.grb_grid = new System.Windows.Forms.GroupBox();
            this.chk_marcaDesmarca = new System.Windows.Forms.CheckBox();
            this.grd_funcao = new System.Windows.Forms.DataGridView();
            this.grb_dados = new System.Windows.Forms.GroupBox();
            this.lbl_cbo = new System.Windows.Forms.Label();
            this.text_cbo = new System.Windows.Forms.TextBox();
            this.lbl_descricao = new System.Windows.Forms.Label();
            this.btn_buscar = new System.Windows.Forms.Button();
            this.text_descricao = new System.Windows.Forms.TextBox();
            this.grb_paginacao.SuspendLayout();
            this.grb_grid.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grd_funcao)).BeginInit();
            this.grb_dados.SuspendLayout();
            this.SuspendLayout();
            // 
            // grb_paginacao
            // 
            this.grb_paginacao.Controls.Add(this.btn_incluir);
            this.grb_paginacao.Controls.Add(this.btn_fechar);
            this.grb_paginacao.Controls.Add(this.btn_novo);
            this.grb_paginacao.Location = new System.Drawing.Point(4, 345);
            this.grb_paginacao.Name = "grb_paginacao";
            this.grb_paginacao.Size = new System.Drawing.Size(593, 51);
            this.grb_paginacao.TabIndex = 11;
            this.grb_paginacao.TabStop = false;
            // 
            // btn_incluir
            // 
            this.btn_incluir.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_incluir.Image = global::SegSis.Properties.Resources.fechar_ico;
            this.btn_incluir.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btn_incluir.Location = new System.Drawing.Point(9, 19);
            this.btn_incluir.Name = "btn_incluir";
            this.btn_incluir.Size = new System.Drawing.Size(75, 23);
            this.btn_incluir.TabIndex = 5;
            this.btn_incluir.Text = "&Confirmar";
            this.btn_incluir.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btn_incluir.UseVisualStyleBackColor = true;
            this.btn_incluir.Click += new System.EventHandler(this.btn_incluir_Click);
            // 
            // btn_fechar
            // 
            this.btn_fechar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_fechar.Image = global::SegSis.Properties.Resources.close;
            this.btn_fechar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btn_fechar.Location = new System.Drawing.Point(171, 19);
            this.btn_fechar.Name = "btn_fechar";
            this.btn_fechar.Size = new System.Drawing.Size(75, 23);
            this.btn_fechar.TabIndex = 7;
            this.btn_fechar.Text = "&Fechar";
            this.btn_fechar.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btn_fechar.UseVisualStyleBackColor = true;
            this.btn_fechar.Click += new System.EventHandler(this.btn_fechar_Click);
            // 
            // btn_novo
            // 
            this.btn_novo.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_novo.Image = global::SegSis.Properties.Resources.icone_mais1;
            this.btn_novo.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btn_novo.Location = new System.Drawing.Point(90, 19);
            this.btn_novo.Name = "btn_novo";
            this.btn_novo.Size = new System.Drawing.Size(75, 23);
            this.btn_novo.TabIndex = 6;
            this.btn_novo.Text = "&Novo";
            this.btn_novo.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btn_novo.UseVisualStyleBackColor = true;
            this.btn_novo.Click += new System.EventHandler(this.btn_novo_Click);
            // 
            // grb_grid
            // 
            this.grb_grid.Controls.Add(this.chk_marcaDesmarca);
            this.grb_grid.Controls.Add(this.grd_funcao);
            this.grb_grid.Location = new System.Drawing.Point(4, 100);
            this.grb_grid.Name = "grb_grid";
            this.grb_grid.Size = new System.Drawing.Size(593, 239);
            this.grb_grid.TabIndex = 10;
            this.grb_grid.TabStop = false;
            // 
            // chk_marcaDesmarca
            // 
            this.chk_marcaDesmarca.AutoSize = true;
            this.chk_marcaDesmarca.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.chk_marcaDesmarca.Location = new System.Drawing.Point(9, 12);
            this.chk_marcaDesmarca.Name = "chk_marcaDesmarca";
            this.chk_marcaDesmarca.Size = new System.Drawing.Size(145, 17);
            this.chk_marcaDesmarca.TabIndex = 5;
            this.chk_marcaDesmarca.Text = "Marcar/Desmarcar Todos";
            this.chk_marcaDesmarca.UseVisualStyleBackColor = true;
            this.chk_marcaDesmarca.CheckedChanged += new System.EventHandler(this.chk_marcaDesmarca_CheckedChanged);
            // 
            // grd_funcao
            // 
            this.grd_funcao.AllowUserToAddRows = false;
            this.grd_funcao.AllowUserToDeleteRows = false;
            this.grd_funcao.AllowUserToOrderColumns = true;
            this.grd_funcao.AllowUserToResizeRows = false;
            this.grd_funcao.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this.grd_funcao.BackgroundColor = System.Drawing.Color.White;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.grd_funcao.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.grd_funcao.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.grd_funcao.Location = new System.Drawing.Point(6, 35);
            this.grd_funcao.Name = "grd_funcao";
            this.grd_funcao.RowHeadersVisible = false;
            this.grd_funcao.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.grd_funcao.Size = new System.Drawing.Size(578, 198);
            this.grd_funcao.TabIndex = 4;
            // 
            // grb_dados
            // 
            this.grb_dados.Controls.Add(this.lbl_cbo);
            this.grb_dados.Controls.Add(this.text_cbo);
            this.grb_dados.Controls.Add(this.lbl_descricao);
            this.grb_dados.Controls.Add(this.btn_buscar);
            this.grb_dados.Controls.Add(this.text_descricao);
            this.grb_dados.Location = new System.Drawing.Point(4, 5);
            this.grb_dados.Name = "grb_dados";
            this.grb_dados.Size = new System.Drawing.Size(593, 89);
            this.grb_dados.TabIndex = 9;
            this.grb_dados.TabStop = false;
            this.grb_dados.Text = "Filtro";
            // 
            // lbl_cbo
            // 
            this.lbl_cbo.AutoSize = true;
            this.lbl_cbo.Location = new System.Drawing.Point(420, 16);
            this.lbl_cbo.Name = "lbl_cbo";
            this.lbl_cbo.Size = new System.Drawing.Size(29, 13);
            this.lbl_cbo.TabIndex = 5;
            this.lbl_cbo.Text = "CBO";
            // 
            // text_cbo
            // 
            this.text_cbo.BackColor = System.Drawing.Color.White;
            this.text_cbo.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.text_cbo.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.text_cbo.ForeColor = System.Drawing.Color.Black;
            this.text_cbo.Location = new System.Drawing.Point(423, 32);
            this.text_cbo.MaxLength = 6;
            this.text_cbo.Name = "text_cbo";
            this.text_cbo.Size = new System.Drawing.Size(161, 20);
            this.text_cbo.TabIndex = 2;
            this.text_cbo.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.text_cbo_KeyPress);
            // 
            // lbl_descricao
            // 
            this.lbl_descricao.AutoSize = true;
            this.lbl_descricao.Location = new System.Drawing.Point(3, 16);
            this.lbl_descricao.Name = "lbl_descricao";
            this.lbl_descricao.Size = new System.Drawing.Size(55, 13);
            this.lbl_descricao.TabIndex = 3;
            this.lbl_descricao.Text = "Descricao";
            // 
            // btn_buscar
            // 
            this.btn_buscar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_buscar.Image = global::SegSis.Properties.Resources.lupa;
            this.btn_buscar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btn_buscar.Location = new System.Drawing.Point(6, 58);
            this.btn_buscar.Name = "btn_buscar";
            this.btn_buscar.Size = new System.Drawing.Size(78, 23);
            this.btn_buscar.TabIndex = 3;
            this.btn_buscar.Text = "&Buscar";
            this.btn_buscar.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btn_buscar.UseVisualStyleBackColor = true;
            this.btn_buscar.Click += new System.EventHandler(this.btn_buscar_Click);
            // 
            // text_descricao
            // 
            this.text_descricao.BackColor = System.Drawing.Color.White;
            this.text_descricao.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.text_descricao.ForeColor = System.Drawing.Color.Black;
            this.text_descricao.Location = new System.Drawing.Point(6, 32);
            this.text_descricao.MaxLength = 100;
            this.text_descricao.Name = "text_descricao";
            this.text_descricao.Size = new System.Drawing.Size(399, 20);
            this.text_descricao.TabIndex = 1;
            // 
            // frm_ClienteAlterarFuncaoBuscar
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(600, 400);
            this.ControlBox = false;
            this.Controls.Add(this.grb_paginacao);
            this.Controls.Add(this.grb_grid);
            this.Controls.Add(this.grb_dados);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "frm_ClienteAlterarFuncaoBuscar";
            this.Text = "INCLUIR FUNÇÃO";
            this.grb_paginacao.ResumeLayout(false);
            this.grb_grid.ResumeLayout(false);
            this.grb_grid.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grd_funcao)).EndInit();
            this.grb_dados.ResumeLayout(false);
            this.grb_dados.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox grb_paginacao;
        private System.Windows.Forms.Button btn_incluir;
        private System.Windows.Forms.Button btn_fechar;
        private System.Windows.Forms.Button btn_novo;
        private System.Windows.Forms.GroupBox grb_grid;
        private System.Windows.Forms.CheckBox chk_marcaDesmarca;
        private System.Windows.Forms.DataGridView grd_funcao;
        private System.Windows.Forms.GroupBox grb_dados;
        private System.Windows.Forms.Label lbl_cbo;
        private System.Windows.Forms.TextBox text_cbo;
        private System.Windows.Forms.Label lbl_descricao;
        private System.Windows.Forms.Button btn_buscar;
        private System.Windows.Forms.TextBox text_descricao;
    }
}