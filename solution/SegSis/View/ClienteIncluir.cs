﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using SegSis.Facade;
using SegSis.Entidade;
using SegSis.Excecao;
using SegSis.View.ViewHelper;
using System.Text.RegularExpressions;
using SegSis.View.Resources;
using System.Security;
using System.IO;
using SegSis.Helper;
using SegSis.ViewHelper;
using SegSis.Resources;


namespace SegSis.View
{
    public partial class frm_cliente_inclui : BaseForm
    {

        public Cliente getCliente()
        {
            return this.cliente;
        }

        private Arquivo arquivo;
        private ClienteArquivo clienteArquivo;
        private Cliente cliente = null;

        private Vendedor vendedor = null;

        public HashSet<Cnae> cnaeSelecionados = null;
        public Cnae cnaePrincipal = null; 

        public static String Msg01 = "Cliente incluído com sucesso.";
        public static String Msg02 = "CNPJ inválido.";
        public static String Msg03 = "Não e possível incluir o Cliente. CNPJ já cadastrado.";
        public static String Msg04 = "O cliente deve ter pelo menos um CNAE vinculado";
        public static String Msg05 = "Atenção";
        public static String Msg06 = "Confirmação";
        public static String Msg07 = "Informar o CNAE principal";
        public static String Msg08 = "Obrigatório selecionar a matriz.";
        public static String Msg09 = "Obrigatório selecionar a credenciada.";
        public static String Msg10 = "O cliente não pode ser unidade dele mesmo.";
        public static String Msg11 = "O cliente não pode ser credenciável dele mesmo.";
        public static String msg12 = "Para copiar os dados é necessário pelo menos selecionar a UF e Cidade na aba comercial. ";
        public static String msg13 = "Verificar o CEP digitado.";
        public static String msg14 = "A Razão Social do cliente é obrigatória.";
        public static String msg15 = "O cliente deve ter um vendedor cadastrado.";
        public static String msg16 = "O CNPJ do cliente é obrigatório.";
        public static String msg17 = "A inscrição não pode ser nula. Caso não tenha informação coloque ISENTO.";
        public static String msg18 = "Rever aba de endereço de cobrança.";
        public static String msg19 = "a Cidade é obrigatória.";
        public static String msg20 = "Unidade Federativa do cliente é obrigatória.";
        public static String msg21 = "O cliente deve possuir pelo menos um função cadastrada.";
        public static String msg22 = "Cliente reativado com sucesso.";


        private Medico coordenador = null;
        public Medico getCoordernador()
        {
            return this.coordenador;
        }
        
        private List<ClienteFuncao> funcoes = new List<ClienteFuncao>();

        List<VendedorCliente> vendedores = new List<VendedorCliente>();

        private Cliente credenciada = null;
        private Cliente matriz = null;
        private String valorIssChecked = String.Empty;

        private TabPage[] tabTelas;
        private TabPage[] tabTelasUnidade;

        Boolean vip = false;
        Boolean usaContrato = false;
        Boolean particular = false;
        Boolean empresaCredenciada = false;
        Boolean unidade = false;
        Boolean destacaIss = false;
        Boolean geraCobrancaValorLiquido = false;

        CidadeIbge cidadeIbge = null;
        CidadeIbge cidadeIbgeCobranca = null;

        Boolean clienteRecuperado = false;

        Boolean? checkClienteRetorno = null;

        public frm_cliente_inclui()
        {
            InitializeComponent();
            this.cnaeSelecionados = new HashSet<Cnae>();
            cb_uf.DataSource = null;
            cb_ufCob.DataSource = null;

            /* salvando as tabpages */

            tabTelas = new TabPage[] { tb_endereco, tb_enderecoCob, tb_cnae, tb_medico, tab_logo, tb_funcoes, tb_estudo, tp_configuracao };
            tabTelasUnidade = new TabPage[] {tb_endereco, tab_logo};
        }

        private void btn_fechar_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void lbl_limpar_Click(object sender, EventArgs e)
        {
            text_razaoSocial.Text = String.Empty;
            text_cnpj.Text = String.Empty;
            text_fantasia.Text = String.Empty;
            text_inscricao.Text = String.Empty;
            text_estMasc.Text = String.Empty;
            text_estFem.Text = String.Empty;
            text_endereco.Text = String.Empty;
            text_numero.Text = String.Empty;
            text_complemento.Text = String.Empty;
            text_bairro.Text = String.Empty;
            text_cep.Text = String.Empty;

            cb_uf.DataSource = null;
            cb_cidade.DataSource = null;
            text_telefone1.Text = String.Empty;
            text_telefone2.Text = String.Empty;
            text_email.Text = String.Empty;
            text_site.Text = String.Empty;
            text_nome.Text = String.Empty;
            text_cargo.Text = String.Empty;
            text_documento.Text = String.Empty;
            text_telefoneContato.Text = String.Empty;
            text_jornada.Text = String.Empty;
            text_escopo.Text = String.Empty;
            text_razaoSocial.Focus();
            chk_vip.Checked = false;
            chk_usaContrato.Checked = false;

            // ENDERECO DE COBRANCA

            text_enderecoCob.Text = string.Empty;
            text_numeroCob.Text = string.Empty;
            text_complementoCob.Text = string.Empty;
            text_bairroCob.Text = string.Empty;
            text_cepCob.Text = string.Empty;
            text_telefoneCob.Text = String.Empty;
            cb_ufCob.DataSource = null;
            cb_cidadeCobranca.DataSource = null;

            // preenchendo campo estimativa com valor default

            text_estMasc.Text = Convert.ToString(0);
            text_estFem.Text = Convert.ToString(0);
            
        }

        private void frm_cliente_inclui_Load(object sender, EventArgs e)
        {
            text_vendedor.Enabled = false;
            ComboHelper.iniciaComboTipoCliente(cb_tipoCliente);

            String aliquotaIss = String.Empty;
            PermissionamentoFacade.mapaConfiguracoes.TryGetValue(ConfigurationConstans.ISS_PADRAO, out aliquotaIss);
            text_aliquotaIss.Text = ValidaCampoHelper.FormataValorMonetario(aliquotaIss);
        }

        private void btn_gravar_Click(object sender, EventArgs e)
        {
            try
            {

                /* validando o cnpj do cliente para ver se está no padrão */
                if (!validaCnpj())
                {
                    throw new Exception(Msg02);
                }

                /* validando o cep para ver se está no padrão */
                if (!ValidaCampoHelper.ValidaCepDigitado(text_cep.Text))
                {
                    throw new Exception(msg13);
                }
                 
                if (validaCampos())
                {

                    ClienteFacade clienteFacade = ClienteFacade.getInstance();
                    
                    cidadeIbge = clienteFacade.findCidadeIbgeById(Convert.ToInt64(((SelectItem)cb_cidade.SelectedItem).Valor));

                    /* caso o cliente for unidade então não será necessário ter dados de cobrança */
                    if (!String.Equals(((SelectItem)cb_tipoCliente.SelectedItem).Valor, ApplicationConstants.UNIDADE))
                    {
                    
                        /* se o cliente for credenciável é obrigatório uma credenciada */
                        if (String.Equals(((SelectItem)cb_tipoCliente.SelectedItem).Valor, ApplicationConstants.CREDENCIADA))
                        {
                            if (credenciada == null)
                            {
                                throw new Exception(Msg09);
                            }
                        }

                        cidadeIbgeCobranca = clienteFacade.findCidadeIbgeById(Convert.ToInt64(((SelectItem)cb_cidadeCobranca.SelectedItem).Valor));

                        vip = chk_vip.Checked ? true : false;
                        usaContrato = chk_usaContrato.Checked ? true : false;
                        particular = chk_particular.Checked ? true : false;
                        empresaCredenciada = String.Equals(((SelectItem)cb_tipoCliente.SelectedItem).Valor, ApplicationConstants.CREDENCIADA) ? true : false;
                        destacaIss = chk_destacaIss.Checked ? true : false;
                        geraCobrancaValorLiquido = chk_geraCobrancaValorLiquido.Checked ? true : false;
                    }
                    else
                    {
                        unidade = true;
                        if (matriz == null)
                        {
                            throw new Exception(Msg08);
                        }
                    }
                    
                    cliente = new Cliente(clienteRecuperado ? cliente.getId() : null, text_razaoSocial.Text, text_fantasia.Text,
                                    text_cnpj.Text, text_inscricao.Text.ToUpper(), text_endereco.Text,
                                    text_numero.Text, text_complemento.Text, text_bairro.Text,
                                    text_cep.Text, Convert.ToString(cb_uf.SelectedValue),
                                    text_telefone1.Text, text_telefone2.Text, text_email.Text,
                                    text_site.Text, clienteRecuperado? cliente.getDataCadastro() : null, text_nome.Text, text_cargo.Text,
                                    text_documento.Text, text_escopo.Text,
                                    text_jornada.Text, ApplicationConstants.ATIVO, Convert.ToInt32(text_estMasc.Text), Convert.ToInt32(text_estFem.Text),
                                    text_enderecoCob.Text, text_numeroCob.Text, text_complementoCob.Text,
                                    text_bairroCob.Text, text_cepCob.Text,
                                    Convert.ToString(cb_ufCob.SelectedValue), text_telefoneCob.Text, coordenador, text_telefoneCob.Text, vip,
                                    text_telefoneContato.Text, usaContrato, matriz,  credenciada, particular, unidade, empresaCredenciada, 
                                chk_prestador.Checked ? true : false, cidadeIbge, cidadeIbgeCobranca, destacaIss, geraCobrancaValorLiquido, Convert.ToDecimal(text_aliquotaIss.Text));

                        
                    
                    /* alimentando o cadastro do cliente com informações do vendedor */

                    vendedores.Add(new VendedorCliente(null, cliente, vendedor, ApplicationConstants.ATIVO));
                    cliente.setVendedorCliente(vendedores);
                    
                    /* caso a empresa seja unidade então não será necessário ter cnae e outros atributos */
                    
                    if (!String.Equals(((SelectItem)cb_tipoCliente.SelectedItem).Valor, ApplicationConstants.UNIDADE))
                    {
                        /* lista de cnaes selecionados */
                        cliente.setCnae(cnaeSelecionados);
                        
                        /* incluindo clienteFuncao no cliente */
                        cliente.setFuncoes(funcoes);
                    }
                    
                    /* verificando se o cadastro foi reaproveitado */

                    if (clienteRecuperado)
                    {
                        cliente.setVendedorCliente(vendedores);
                        cliente = clienteFacade.clienteAlterar(cliente);
                        MessageBox.Show(msg22, "Confirmação", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        
                    }
                    else
                    {
                        cliente = clienteFacade.incluirCliente(cliente, cnaePrincipal);


                        /* incluindo a Logo */

                        if (arquivo != null)
                        {
                            arquivo = clienteFacade.incluirArquivo(arquivo);
                            clienteArquivo = new ClienteArquivo(null, arquivo, cliente, null, null, ApplicationConstants.ATIVO);
                            clienteFacade.incluirClienteArquivo(clienteArquivo);
                        }


                        MessageBox.Show(Msg01, Msg06, MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }
                    
                    this.Close();
                }
            }
            catch (Exception Ex)
            {
                MessageBox.Show(Ex.Message);
            }
        }
        
        private Boolean validaCampos()
        {
            Boolean retorno = Convert.ToBoolean(true);
            
            try
            {
                /* dados gerais */

                if (string.IsNullOrEmpty(text_razaoSocial.Text))
                {
                    throw new Exception(msg14);
                }
                
                if (String.IsNullOrEmpty(text_vendedor.Text))
                {
                    throw new Exception(msg15);
                }

                if (cb_cidade.SelectedIndex == -1)
                {
                    throw new Exception(msg19);
                }

                /* validando informaçoes de cliente  */
                if (!String.Equals(((SelectItem)(cb_tipoCliente.SelectedItem)).Valor, ApplicationConstants.UNIDADE))
                {
                    
                    /* verificando se já existe um CNPJ cadastrado no sistema. Caos seja cliente
                     * recuperado não há a necessidade de checar o cnpj */

                    if (!clienteRecuperado)
                    {
                        string cnpj = Convert.ToString(text_cnpj.Text.Replace(".", "").Replace(",", "").Replace("-", "").Replace("/", ""));

                        ClienteFacade clienteFacade = ClienteFacade.getInstance();

                        Cliente cliente = clienteFacade.findClienteByCnpj(cnpj);

                        if (cliente != null)
                        {
                            throw new Exception(Msg03);
                        }
                    }

                    if (string.IsNullOrEmpty(text_cnpj.Text.Replace(".", "").Replace(",", "").Replace("-", "").Replace("/", "")))
                    {
                        throw new Exception(msg16);
                    }
                    
                    if (string.IsNullOrEmpty(text_inscricao.Text))
                    {
                        throw new Exception(msg17);
                    }

                    if (grd_cnae.Rows.Count.Equals(0))
                    {
                        throw new Exception(Msg04);
                    }

                    if (String.IsNullOrEmpty(text_cnaePrincipal.Text.Replace(".", "").Replace(",", "").Replace("-", "").Replace("/", "").Trim()))
                    {
                        throw new Exception(Msg07);
                    }

                    if (String.IsNullOrEmpty(text_enderecoCob.Text))
                    {
                        throw new Exception(msg18);
                    }

                    if (String.IsNullOrEmpty(this.text_numeroCob.Text))
                    {
                        throw new Exception(msg18);
                    }

                    if (String.IsNullOrEmpty(this.text_bairroCob.Text))
                    {
                        throw new Exception(msg18);
                    }

                    if (String.IsNullOrEmpty(this.text_cepCob.Text))
                    {
                        throw new Exception(msg18);
                    }

                    if (cb_cidadeCobranca.SelectedIndex == -1)
                    {
                        throw new Exception(msg18);
                    }

                    if (dg_funcao.RowCount == 0)
                    {
                        throw new Exception(msg21);
                    }
                }
            }
            catch (Exception ex)
            {
                retorno = false;
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }

            return retorno;
        }

        private Boolean validaCnpj()
        {
            Boolean retorno = Convert.ToBoolean(true);

            try
            {
                if (Regex.IsMatch(Convert.ToString(this.text_cnpj), @"\d"))
                {
                    if (!ValidaCampoHelper.ValidaCNPJ(this.text_cnpj.Text))
                    {
                        throw new Exception(Msg02);
                    }
                }
            }
            catch (Exception ex)
            {
                retorno = false;
                MessageBox.Show(ex.Message);
            }
         
            return retorno;
        }

        private void btn_vendedor_Click(object sender, EventArgs e)
        {
            try
            {
                frm_clienteVendedorSelecionar_Incluir formClienteVendedorSelecionar = new frm_clienteVendedorSelecionar_Incluir(this);
                formClienteVendedorSelecionar.ShowDialog();

                String mensagem1 = "Vendedor Alterado com sucesso.";

                if (formClienteVendedorSelecionar.getVendedor() != null)
                {
                    if (clienteRecuperado)
                    {
                        /* gravando novo vendedor no cliente */

                        VendedorFacade vendedorFacade = VendedorFacade.getInstance();

                        vendedor = formClienteVendedorSelecionar.getVendedor();
                        text_vendedor.Text = vendedor.getNome();

                        vendedorFacade.updateVendedorCliente(new VendedorCliente(null, cliente, vendedor, ApplicationConstants.ATIVO));

                        MessageBox.Show(mensagem1, "Confirmação", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        
                    }
                    else
                    {
                        vendedor = formClienteVendedorSelecionar.getVendedor();
                        text_vendedor.Text = vendedor.getNome().ToUpper();
                    }
                    
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }

        }

        private void btn_incluiCnae_Click(object sender, EventArgs e)
        {
            try
            {
                if (!clienteRecuperado)
                {
                    
                    ClienteFacade clienteFacade = ClienteFacade.getInstance();

                    frm_cliente_cnae_selecionar formClienteCnaeSelecionar = new frm_cliente_cnae_selecionar(null);
                    formClienteCnaeSelecionar.ShowDialog();

                    if (((cnaeSelecionados.Count) + (formClienteCnaeSelecionar.getCnaeSelecionados().Count) > 5))
                    {

                        throw new CnaeInvalidoException(CnaeInvalidoException.msg1);
                    }
                    else if (formClienteCnaeSelecionar.getCnae() != null)
                    {
                        cnaeSelecionados.UnionWith(formClienteCnaeSelecionar.getCnaeSelecionados());

                        montaGridCnae();
                    }
                    
                }
                else
                {
                    /* incluir cnae no cliente recuperado */

                    ClienteFacade clienteFacade = ClienteFacade.getInstance();
                    
                    frm_cliente_cnae_selecionar formCnae = new frm_cliente_cnae_selecionar(cliente);
                    formCnae.ShowDialog();

                    cnaeSelecionados = clienteFacade.findClienteCnaeByCliente(cliente);
                    montaGridCnae();

                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void btn_excluiCnae_Click(object sender, EventArgs e)
        {
            Cnae cnae = null;

            if (!clienteRecuperado)
            {
                foreach (DataGridViewRow dvRow in grd_cnae.SelectedRows)
                {

                    Int32 cellValue = dvRow.Index;

                    Int64 id = (Int64)grd_cnae.Rows[cellValue].Cells[0].Value; // id do cnae
                    String codCnae = (String)grd_cnae.Rows[cellValue].Cells[1].Value; // codigo do cnae
                    String atividade = (String)grd_cnae.Rows[cellValue].Cells[2].Value; // Atividade

                    cnae = new Cnae(id, codCnae, atividade);

                    if (!(cnaePrincipal == null) && cnae.getId() == cnaePrincipal.getId())
                    {
                        cnaePrincipal = null;
                        text_cnaePrincipal.Text = String.Empty;
                    }
                    cnaeSelecionados.Remove(cnae);
                    grd_cnae.Rows.Remove(dvRow);

                }
            }
            else
            {
                /* excluindo o cnae do cliente */
                ClienteFacade clienteFacade = ClienteFacade.getInstance();

                clienteFacade.deleteClienteCnaeInCliente(new ClienteCnae(cliente, new Cnae((Int64)grd_cnae.CurrentRow.Cells[0].Value,
                    (String)grd_cnae.CurrentRow.Cells[1].Value, (String)grd_cnae.CurrentRow.Cells[2].Value)));

                cnaeSelecionados = clienteFacade.findClienteCnaeByCliente(cliente);
                montaGridCnae();
                
            }
        }

        private void btn_cnaePrincipal_Click(object sender, EventArgs e)
        {

            if (cnaeSelecionados != null)
            {
                frm_clienteCnaePrincipalAlterar formClienteCnaePrincipal = new frm_clienteCnaePrincipalAlterar(cnaeSelecionados);
                formClienteCnaePrincipal.ShowDialog();

                if (formClienteCnaePrincipal.getCnae() != null)
                {
                    cnaePrincipal = formClienteCnaePrincipal.getCnae();
                    text_cnaePrincipal.Text = cnaePrincipal.getCodCnae();

                }
            }
        }
        
        private void text_estMasc_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!Char.IsDigit(e.KeyChar) && e.KeyChar != (char)8)
            {
                e.Handled = true;

            }
        }

        private void text_estFem_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!Char.IsDigit(e.KeyChar) && e.KeyChar != (char)8)
            {
                e.Handled = true;

            }
        }

        private void btn_copiar_Click(object sender, EventArgs e)
        {
            try
            {
                if (cb_uf.Items.Count > 0 && cb_cidade.Items.Count > 0)
                {
                    text_enderecoCob.Text = text_endereco.Text;
                    text_numeroCob.Text = text_numero.Text;
                    text_complementoCob.Text = text_complemento.Text;
                    text_bairroCob.Text = text_bairro.Text;
                    text_cepCob.Text = text_cep.Text;

                    text_telefoneCob.Text = text_telefone1.Text;
                    text_faxCobranca.Text = text_telefone2.Text;

                    // copiando dados do combobox


                    ComboHelper.startComboUfSave(cb_ufCob);
                    cb_ufCob.SelectedValue = cb_uf.SelectedValue;
                    cb_ufCob.SelectedItem = cb_uf.SelectedItem;

                    ComboHelper.iniciaComboCidade(cb_cidadeCobranca, Convert.ToString(cb_ufCob.SelectedValue));
                    cb_cidadeCobranca.SelectedValue = cb_cidade.SelectedValue;
                    cb_cidadeCobranca.SelectedItem = cb_cidade.SelectedItem;
                }
                else
                {
                    throw new Exception(msg12);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void btn_coordenador_Click(object sender, EventArgs e)
        {
            try
            {
                this.Cursor = Cursors.WaitCursor;
                
                frm_ClienteIncluirMedico formClienteIncluirMedico = new frm_ClienteIncluirMedico(this);
                formClienteIncluirMedico.ShowDialog();

                if (formClienteIncluirMedico.getMedico() != null)
                {
                    coordenador = formClienteIncluirMedico.getMedico();
                    text_nomeCoordenador.Text = coordenador.getNome();
                }


            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }

        private void btn_excluirCoordenador_Click(object sender, EventArgs e)
        {
            text_nomeCoordenador.Text = String.Empty;
            coordenador = null;
        }

        private void frm_cliente_inclui_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                this.SelectNextControl(this.ActiveControl, !e.Shift, true, true, true);
            }
        }

        private void bt_abrir_file_dialog_Click(object sender, EventArgs e)
        {
            OpenFileDialog clienteLogoFileDialog = new OpenFileDialog();

            clienteLogoFileDialog.Multiselect = false;
            clienteLogoFileDialog.Title = "Selecionar Logo";
            clienteLogoFileDialog.InitialDirectory = @"C:\";
            //filtra para exibir somente arquivos de imagens
            clienteLogoFileDialog.Filter = "Images(*.JPG) |*.JPG";
            clienteLogoFileDialog.CheckFileExists = true;
            clienteLogoFileDialog.CheckPathExists = true;
            clienteLogoFileDialog.FilterIndex = 2;
            clienteLogoFileDialog.RestoreDirectory = true;
            clienteLogoFileDialog.ReadOnlyChecked = true;
            clienteLogoFileDialog.ShowReadOnly = true;

            DialogResult dr = clienteLogoFileDialog.ShowDialog();

            if (dr == System.Windows.Forms.DialogResult.OK)
            {
                String nomeArquivo = clienteLogoFileDialog.FileName;
                String nomeArquivoFinal = nomeArquivo.Substring(nomeArquivo.LastIndexOf('\\') + 1);

                // cria um PictureBox
                try
                {
                    Image Imagem = Image.FromFile(nomeArquivo);
                    pic_box_logo.SizeMode = PictureBoxSizeMode.StretchImage;
                    if (Imagem.Width > 120 && Imagem.Height > 100)
                    {
                        throw new LogoException("A imagem deve ter um tamanho máximo de 120 X 100 pixels.");
                    }
                    
                    pic_box_logo.Image = Imagem;

                    byte[] conteudo = FileHelper.CarregarArquivoImagem(nomeArquivo, 5242880);
                    

                    arquivo = new Arquivo(null, nomeArquivoFinal, "image/jpeg", conteudo.Length, conteudo);
                }
                catch (SecurityException ex)
                {
                    // O usuário  não possui permissão para ler arquivos
                    MessageBox.Show("Erro de segurança Contate o administrador de segurança da rede.\n\n" +
                                                "Mensagem : " + ex.Message + "\n\n" +
                                                "Detalhes (enviar ao suporte):\n\n" + ex.StackTrace);
                }
                catch (LogoException ex)
                {
                    MessageBox.Show(ex.Message);
                }
                catch (Exception ex)
                {
                    // Não pode carregar a imagem (problemas de permissão)
                    MessageBox.Show("Não é possível exibir a imagem : " + nomeArquivoFinal
                                                + ". Você pode não ter permissão para ler o arquivo , ou " +
                                                " ele pode estar corrompido.\n\nErro reportado : " + ex.Message);
                }
            }
        }

        private void btn_incluirFuncao_Click(object sender, EventArgs e)
        {
            ClienteFacade clienteFacade = ClienteFacade.getInstance();
            
            if (clienteRecuperado)
            {
                frm_ClienteIncluirFuncaoBuscar formClienteIncluirFuncaoBuscar = new frm_ClienteIncluirFuncaoBuscar(cliente, cliente.getFuncoes(), clienteRecuperado);
                formClienteIncluirFuncaoBuscar.ShowDialog();
                funcoes = formClienteIncluirFuncaoBuscar.getFuncoes();
                cliente.setFuncoes(formClienteIncluirFuncaoBuscar.getFuncoes());
                montaGridFuncoes();

            }
            else
            {
                frm_ClienteIncluirFuncaoBuscar formClienteIncluirFuncaoBuscar = new frm_ClienteIncluirFuncaoBuscar(null, funcoes, null);
                formClienteIncluirFuncaoBuscar.ShowDialog();
                funcoes = formClienteIncluirFuncaoBuscar.getFuncoes();
                montaGridFuncoes();
            }
            
        }

        private void btn_excluir_Click(object sender, EventArgs e)
        {
            try
            {
                if (dg_funcao.CurrentRow != null)
                {
                    if (MessageBox.Show("Deseja excluir a função?", "Atenção", MessageBoxButtons.YesNo) == DialogResult.Yes)
                    {
                        Funcao funcao = new Funcao((Int64)dg_funcao.CurrentRow.Cells[0].Value,
                            (String)dg_funcao.CurrentRow.Cells[1].Value,
                            (String)dg_funcao.CurrentRow.Cells[2].Value,
                            (String)dg_funcao.CurrentRow.Cells[3].Value,
                            (String)dg_funcao.CurrentRow.Cells[4].Value);

                        ClienteFuncao clienteFuncao = new ClienteFuncao(null, cliente, funcao);

                        if (clienteRecuperado)
                        {
                            PpraFacade ppraFacade = PpraFacade.getInstance();
                            ClienteFacade clienteFacade = ClienteFacade.getInstance();

                            ClienteFuncao clienteFuncaoFind = funcoes.Find(delegate(ClienteFuncao cf)
                            {
                                return cf.getFuncao().getIdFuncao() == funcao.getIdFuncao();
                            }
                            );

                            ppraFacade.deleteClienteFuncao(clienteFuncaoFind);
                            
                            funcoes = clienteFacade.findAllFuncaoByCliente(cliente);
                            cliente.setFuncoes(funcoes);
                            montaGridFuncoes();

                        }
                        else
                        {
                            funcoes.Remove(clienteFuncao);
                            montaGridFuncoes();
                        }


                        

                    }
                }
                else
                {
                    throw new Exception("Selecione uma linha");
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        public void montaGridFuncoes()
        {
            try
            {
                dg_funcao.Columns.Clear();

                if (funcoes.Count > 0)
                {
                    dg_funcao.ColumnCount = 5;

                    dg_funcao.Columns[0].HeaderText = "Id_Funcao";
                    dg_funcao.Columns[0].Visible = false;

                    dg_funcao.Columns[1].HeaderText = "Descrição";
                    dg_funcao.Columns[2].HeaderText = "Cod_CBO";
                    dg_funcao.Columns[3].HeaderText = "Situação";
                    dg_funcao.Columns[3].Visible = false;
                    dg_funcao.Columns[4].HeaderText = "Comentário";
                    dg_funcao.Columns[4].Visible = false;
                    
                    foreach (ClienteFuncao clienteFuncao in funcoes)
                    {
                        dg_funcao.Rows.Add(clienteFuncao.getFuncao().getIdFuncao(),
                            clienteFuncao.getFuncao().getDescricao(),
                            clienteFuncao.getFuncao().getCodCbo(),
                            clienteFuncao.getFuncao().getSituacao(),
                            clienteFuncao.getFuncao().getComentario());
                    }
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        public Boolean validaDadosDigitados()
        {

            Boolean resultado = true;

            try
            {
                // verificando o cep digitado

                if (!ValidaCampoHelper.ValidaCepDigitado(text_cep.Text))
                {
                    resultado = false;
                    throw new FuncionarioException(FuncionarioException.msg6);
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }

            return resultado;

        }

        private void cb_tipoCliente_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                // se o cliente for uma credenciada então não será permitido a inclusão de uma unidade.
                if (String.Equals(((SelectItem)(cb_tipoCliente).SelectedItem).Valor, ApplicationConstants.CREDENCIADA))
                {
                    this.gb_Matriz.Enabled = false;
                    this.gb_credenciada.Enabled = true;
                    this.text_cnpj.ReadOnly = false;
                    this.matriz = null;
                    this.text_matriz.Text = String.Empty;

                    chk_vip.Enabled = false;
                    chk_vip.Checked = false;

                    chk_usaContrato.Enabled = false;
                    chk_usaContrato.Checked = false;

                    chk_particular.Enabled = false;
                    chk_particular.Checked = false;
                    
                    chk_destacaIss.Enabled = false;
                    chk_destacaIss.Checked = false;

                    chk_geraCobrancaValorLiquido.Enabled = false;
                    chk_geraCobrancaValorLiquido.Checked = false;
                    
                    this.text_aliquotaIss.Enabled = false;
                    this.text_aliquotaIss.Text = "0";

                    chk_prestador.Checked = false;
                    chk_prestador.Enabled = false;

                    /* excluindo as tabelas da tela */
                    tb_paginacao.TabPages.Clear();

                    /* incluindo as tabelas devidas */

                    foreach (TabPage tp in tabTelas)
                    {
                        tb_paginacao.TabPages.Add(tp);
                    }

                }

                // se o cliente for uma unidade então não será permitido a inclusão de uma matriz.
                if (String.Equals(((SelectItem)(cb_tipoCliente).SelectedItem).Valor, ApplicationConstants.UNIDADE))
                {
                    
                    this.gb_credenciada.Enabled = false;
                    this.gb_Matriz.Enabled = true;
                    this.text_cnpj.ReadOnly = true;
                    this.credenciada = null;
                    this.text_credenciada.Text = String.Empty;

                    chk_vip.Enabled = false;
                    chk_vip.Checked = false;

                    chk_usaContrato.Enabled = false;
                    chk_usaContrato.Checked = false;

                    chk_particular.Enabled = false;
                    chk_particular.Checked = false;

                    chk_destacaIss.Enabled = false;
                    chk_destacaIss.Checked = false;

                    chk_geraCobrancaValorLiquido.Enabled = false;
                    chk_geraCobrancaValorLiquido.Checked = false;

                    this.text_aliquotaIss.Enabled = false;
                    this.text_aliquotaIss.Text = "0";

                    chk_prestador.Checked = false;
                    chk_prestador.Enabled = false;

                    /* novos bloqueios para unidade
                     * removendo as tabelas que não são utilizadas pela unidade */

                    tb_paginacao.TabPages.Clear();

                    foreach (TabPage tp in tabTelasUnidade)
                    {
                        tb_paginacao.TabPages.Add(tp);
                    }

                }

                // se o cliente for uma empresa normal então não será permitido a inclusão de uma unidade e credenciada.
                if (String.Equals(((SelectItem)(cb_tipoCliente).SelectedItem).Valor, ApplicationConstants.NORMAL))
                {
                    this.gb_credenciada.Enabled = false;
                    this.gb_Matriz.Enabled = false;
                    this.text_cnpj.ReadOnly = false;
                    this.credenciada = null;
                    this.matriz = null;
                    this.text_credenciada.Text = String.Empty;
                    this.text_matriz.Text = String.Empty;

                    chk_vip.Enabled = true;

                    chk_usaContrato.Enabled = true;

                    chk_particular.Enabled = true;

                    chk_destacaIss.Enabled = true;

                    chk_geraCobrancaValorLiquido.Enabled = true;

                    this.text_aliquotaIss.Enabled = true;

                    chk_prestador.Enabled = true;

                    /* excluindo as tabelas da tela */
                    tb_paginacao.TabPages.Clear();

                    /* incluindo as tabelas devidas */

                    foreach (TabPage tp in tabTelas)
                    {
                        tb_paginacao.TabPages.Add(tp);
                    }
                }
                
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }

        }

        private void btn_matiz_Click(object sender, EventArgs e)
        {
            frm_ClienteTipoSelecionar formClienteTipo = new frm_ClienteTipoSelecionar(null,null, false,null, null);
            formClienteTipo.ShowDialog();

            if (formClienteTipo.getCliente() != null)
            {
                matriz = formClienteTipo.getCliente();
                this.text_matriz.Text = matriz.getRazaoSocial();
                this.text_cnpj.Text = matriz.getCnpj();
            }
        }

        private void btn_credenciada_Click(object sender, EventArgs e)
        {
            frm_ClienteTipoSelecionar formClienteTipo = new frm_ClienteTipoSelecionar(null, false, false, null, null);
            formClienteTipo.ShowDialog();

            if (formClienteTipo.getCliente() != null)
            {
                credenciada = formClienteTipo.getCliente();
                this.text_credenciada.Text = credenciada.getRazaoSocial();
                this.text_cnpj.Text = String.Empty;
            }
        }

        private void cb_uf_Click(object sender, EventArgs e)
        {
            try
            {
                ComboHelper.startComboUfSave(cb_uf);
                cb_cidade.DataSource = null;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void cb_ufCob_Click(object sender, EventArgs e)
        {
            try
            {
                ComboHelper.startComboUfSave(cb_ufCob);
                cb_cidadeCobranca.DataSource = null;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void cb_cidade_Click(object sender, EventArgs e)
        {
            try
            {
                if (cb_uf.SelectedIndex != -1)
                {
                    ComboHelper.iniciaComboCidade(cb_cidade, Convert.ToString(cb_uf.SelectedValue));
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void cb_cidadeCobranca_Click(object sender, EventArgs e)
        {
            try
            {
                if (cb_ufCob.SelectedIndex != -1)
                {
                    ComboHelper.iniciaComboCidade(cb_cidadeCobranca, Convert.ToString(cb_ufCob.SelectedValue));
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void text_aliquotaIss_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = ValidaCampoHelper.FormataDigitacaoNumero(sender, e, text_aliquotaIss.Text);
        }

        private void text_aliquotaIss_Leave(object sender, EventArgs e)
        {
            
            if (String.IsNullOrEmpty(text_aliquotaIss.Text))
            {
                text_aliquotaIss.Text = valorIssChecked;
            }
            
            text_aliquotaIss.Text = ValidaCampoHelper.FormataValorMonetario(text_aliquotaIss.Text);
        }

        private void text_aliquotaIss_Enter(object sender, EventArgs e)
        {
            valorIssChecked = text_aliquotaIss.Text;
            text_aliquotaIss.Text = String.Empty;
        }

        private void chk_destacaIss_CheckStateChanged(object sender, EventArgs e)
        {
            try
            {
                if (!chk_destacaIss.Checked)
                {
                    text_aliquotaIss.Text = "0,00";
                    text_aliquotaIss.Enabled = false;
                }
                else
                {
                    text_aliquotaIss.Enabled = true;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void text_cnpj_Leave(object sender, EventArgs e)
        {
            try
            {
                ClienteFacade clienteFacade = ClienteFacade.getInstance();

                String mensagem1 = "Já existe um cliente cadastrado com esse cnpj. Deseja reativar o cadastro?";

                if (validaCnpj())
                {
                    /* verificando se existe cliente com o mesmo cnpj cadastrado no sistema.*/
                    Cliente clienteProcura = clienteFacade.findClienteByCnpj(text_cnpj.Text.Replace(".", "").Replace("/", "").Replace("-", ""));

                    if (clienteProcura != null)
                    {
                        /* verificando se existe já existe um cliente cadastrado com esse cnpj e 
                         * se esse cliente encontra-se inativo. */
                        if (checkClienteRetorno == null || checkClienteRetorno == false)
                        {
                            checkClienteRetorno = true;
                            cliente = clienteProcura;

                            if (clienteProcura != null && String.Equals(clienteProcura.getSituacao(), ApplicationConstants.DESATIVADO))
                            {
                                if (MessageBox.Show(mensagem1, "Atenção", MessageBoxButtons.YesNo, MessageBoxIcon.Information) == DialogResult.Yes)
                                {
                                    validaEventoLeave(clienteProcura);
                                }
                            }

                        }
                        else
                        {
                            validaEventoLeave(clienteProcura);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }

        public void validaEventoLeave(Cliente clienteProcura)
        {
            try
            {
                this.Cursor = Cursors.WaitCursor;

                ClienteFacade clienteFacade = ClienteFacade.getInstance();

                clienteRecuperado = true;
                
                #region central

                ComboHelper.iniciaComboTipoCliente(cb_tipoCliente);

                /* verificando o tipo de cliente */

                if (clienteProcura.getUnidade())
                {
                    cb_tipoCliente.SelectedValue = ApplicationConstants.UNIDADE;
                }
                else if (clienteProcura.getCredenciada() != null)
                {
                    cb_tipoCliente.SelectedValue = ApplicationConstants.CREDENCIADA;
                }
                else
                {
                    cb_tipoCliente.SelectedValue = ApplicationConstants.NORMAL;
                }

                /* preenchendo vendedor cliente */

                vendedores = clienteProcura.getVendedorCliente();
                VendedorCliente vendedorNoCliente = vendedores.ElementAt(0);

                vendedor = vendedorNoCliente.getVendedor();
                text_vendedor.Text = vendedorNoCliente.getVendedor().getNome();

                text_razaoSocial.Text = clienteProcura.getRazaoSocial();
                text_fantasia.Text = clienteProcura.getFantasia();
                text_inscricao.Text = clienteProcura.getInscricao();

                #endregion

                /* verificando o tipo de cliente */

                tabTelas = new TabPage[] { tb_endereco, tb_enderecoCob, tb_cnae, tb_medico, tab_logo, tb_funcoes, tb_estudo, tp_configuracao };
                tabTelasUnidade = new TabPage[] { tb_endereco, tab_logo };
                tb_paginacao.TabPages.Clear();

                if (!String.Equals(((SelectItem)cb_tipoCliente.SelectedItem).Valor, ApplicationConstants.UNIDADE))
                {
                    /* preenchendo as tabelas da pagina */

                    foreach (TabPage tp in tabTelas)
                    {
                        tb_paginacao.TabPages.Add(tp);
                    }

                    #region endereco

                    text_endereco.Text = clienteProcura.getEndereco();
                    text_numero.Text = clienteProcura.getNumero();
                    text_complemento.Text = clienteProcura.getComplemento();
                    text_bairro.Text = clienteProcura.getBairro();
                    text_cep.Text = clienteProcura.getCep();

                    ComboHelper.startComboUfSave(cb_uf);
                    cb_uf.SelectedValue = clienteProcura.getUf();

                    ComboHelper.iniciaComboCidade(cb_cidade, clienteProcura.getUf());
                    cb_cidade.SelectedIndex = cb_cidade.FindStringExact(clienteProcura.getCidadeIbge().getNome());

                    text_telefone1.Text = clienteProcura.getTelefone1();
                    text_telefone2.Text = clienteProcura.getTelefone2();

                    text_email.Text = clienteProcura.getEmail();
                    text_site.Text = clienteProcura.getSite();

                    #endregion

                    #region enderecoCobranca

                    text_enderecoCob.Text = clienteProcura.getEnderecoCob();
                    text_numeroCob.Text = clienteProcura.getNumeroCob();
                    text_complementoCob.Text = clienteProcura.getComplementoCob();
                    text_bairroCob.Text = clienteProcura.getBairroCob();
                    text_cepCob.Text = clienteProcura.getCepCob();

                    ComboHelper.startComboUfSave(cb_ufCob);
                    cb_ufCob.SelectedValue = clienteProcura.getUfCob();

                    ComboHelper.iniciaComboCidade(cb_cidadeCobranca, clienteProcura.getUf());
                    cb_cidadeCobranca.SelectedIndex = cb_cidade.FindStringExact(clienteProcura.getCidadeIbge().getNome());

                    text_telefoneCob.Text = clienteProcura.getTelefoneCob();
                    text_faxCobranca.Text = clienteProcura.getTelefone2Cob();

                    #endregion

                    #region cnae

                    cnaeSelecionados = clienteFacade.findClienteCnaeByCliente(clienteProcura);

                    montaGridCnae();
                    
                    cnaePrincipal = clienteFacade.findCnaePrincipalCliente((Int64)clienteProcura.getId());
                    text_cnaePrincipal.Text = cnaePrincipal.getCodCnae();

                    #endregion

                    #region medico

                    coordenador = clienteProcura.getCoordenador();

                    if (coordenador != null)
                    {
                        text_nomeCoordenador.Text = clienteProcura.getCoordenador().getNome();
                    }


                    #endregion

                    #region logo

                    clienteArquivo = clienteFacade.findClienteArquivoByCliente(clienteProcura);

                    if (clienteArquivo != null && clienteArquivo.getArquivo() != null)
                    {
                        arquivo = clienteFacade.findArquivoById(clienteArquivo.getArquivo().getId());

                        if (arquivo != null)
                        {
                            pic_box_logo.Image = FileHelper.byteArrayToImage(arquivo.getConteudo());
                        }
                    }


                    #endregion

                    #region funcoes

                    funcoes = clienteFacade.findAllFuncaoByCliente(clienteProcura);
                    cliente.setFuncoes(funcoes);
                    montaGridFuncoes();

                    #endregion

                    #region ppra e pcmso

                    text_estFem.Text = clienteProcura.getEstFem().ToString();
                    text_estMasc.Text = clienteProcura.getEstMasc().ToString();

                    text_nome.Text = clienteProcura.getResponsavel();
                    text_telefoneContato.Text = clienteProcura.getTelefoneContato();
                    text_escopo.Text = clienteProcura.getEscopo();
                    text_cargo.Text = clienteProcura.getCargo();
                    text_documento.Text = clienteProcura.getDocumento();
                    text_jornada.Text = clienteProcura.getJornada();

                    #endregion

                    #region configuracao

                    chk_vip.Checked = clienteProcura.getVip();
                    chk_usaContrato.Checked = clienteProcura.getUsaContrato();
                    chk_particular.Checked = clienteProcura.getParticular();
                    chk_prestador.Checked = clienteProcura.getPrestador();
                    chk_destacaIss.Checked = clienteProcura.getDestacaIss();
                    chk_geraCobrancaValorLiquido.Checked = clienteProcura.getGeraCobrancaValorLiquido();
                    text_aliquotaIss.Text = clienteProcura.getAliquotaIss().ToString();

                    #endregion

                }
                else
                {
                    /* preenchendo as tabelas da pagina */
                    foreach (TabPage tp in tabTelasUnidade)
                    {
                        tb_paginacao.TabPages.Add(tp);
                    }

                    #region endereco

                    text_endereco.Text = clienteProcura.getEndereco();
                    text_numero.Text = clienteProcura.getNumero();
                    text_complemento.Text = clienteProcura.getComplemento();
                    text_bairro.Text = clienteProcura.getBairro();
                    text_cep.Text = clienteProcura.getCep();

                    ComboHelper.startComboUfSave(cb_uf);
                    cb_uf.SelectedValue = clienteProcura.getUf();

                    ComboHelper.iniciaComboCidade(cb_cidade, clienteProcura.getUf());
                    cb_cidade.SelectedIndex = cb_cidade.FindStringExact(clienteProcura.getCidadeIbge().getNome());

                    text_telefone1.Text = clienteProcura.getTelefone1();
                    text_telefone2.Text = clienteProcura.getTelefone2();

                    text_email.Text = clienteProcura.getEmail();
                    text_site.Text = clienteProcura.getSite();

                    #endregion

                    #region logo

                    clienteArquivo = clienteFacade.findClienteArquivoByCliente(clienteProcura);

                    if (clienteArquivo != null && clienteArquivo.getArquivo() != null)
                    {
                        arquivo = clienteFacade.findArquivoById(clienteArquivo.getArquivo().getId());

                        if (arquivo != null)
                        {
                            pic_box_logo.Image = FileHelper.byteArrayToImage(arquivo.getConteudo());
                        }
                    }


                    #endregion
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }

        public void montaGridCnae()
        {
            try
            {
                grd_cnae.Columns.Clear();

                grd_cnae.ColumnCount = 3;

                grd_cnae.Columns[0].Name = "ID";
                grd_cnae.Columns[0].Visible = false;

                grd_cnae.Columns[1].Name = "Código Cnae";
                
                grd_cnae.Columns[2].Name = "Atividade";

                foreach (Cnae cnae in this.cnaeSelecionados)
                {
                    this.grd_cnae.Rows.Add(cnae.getId(), cnae.getCodCnae(), cnae.getAtividade());
                }
                
                // classificando a coluna da grid para exibir os cnaes em order ascentente.
                this.grd_cnae.Sort(this.grd_cnae.Columns[1], ListSortDirection.Ascending);
                
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }

        }
            
    }
}
