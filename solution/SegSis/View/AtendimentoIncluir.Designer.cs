﻿namespace SWS.View
{
    partial class frmAtendimentoIncluir
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle7 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle8 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            this.flpAcao = new System.Windows.Forms.FlowLayoutPanel();
            this.btn_grava = new System.Windows.Forms.Button();
            this.btFechar = new System.Windows.Forms.Button();
            this.textNome = new System.Windows.Forms.TextBox();
            this.textRg = new System.Windows.Forms.TextBox();
            this.textCpf = new System.Windows.Forms.MaskedTextBox();
            this.lblCpf = new System.Windows.Forms.TextBox();
            this.lblRg = new System.Windows.Forms.TextBox();
            this.lblNome = new System.Windows.Forms.TextBox();
            this.grb_ghe = new System.Windows.Forms.GroupBox();
            this.dgvGhe = new System.Windows.Forms.DataGridView();
            this.grb_risco = new System.Windows.Forms.GroupBox();
            this.dgvRisco = new System.Windows.Forms.DataGridView();
            this.grbExamePcmso = new System.Windows.Forms.GroupBox();
            this.chk_examesPCMSO = new System.Windows.Forms.CheckBox();
            this.dgvExamePcmso = new System.Windows.Forms.DataGridView();
            this.grbExameAvulso = new System.Windows.Forms.GroupBox();
            this.chkExamesAvulso = new System.Windows.Forms.CheckBox();
            this.dgvExameAvulso = new System.Windows.Forms.DataGridView();
            this.btn_incluirExames = new System.Windows.Forms.Button();
            this.btFuncionario = new System.Windows.Forms.Button();
            this.lblIdade = new System.Windows.Forms.TextBox();
            this.textIdade = new System.Windows.Forms.TextBox();
            this.textPrestador = new System.Windows.Forms.TextBox();
            this.lblPrestador = new System.Windows.Forms.TextBox();
            this.btPrestador = new System.Windows.Forms.Button();
            this.textCnpjPrestador = new System.Windows.Forms.MaskedTextBox();
            this.lblCnpjPrestador = new System.Windows.Forms.TextBox();
            this.IncluirAtendimentoError = new System.Windows.Forms.ErrorProvider(this.components);
            this.grbObservação = new System.Windows.Forms.GroupBox();
            this.textObservacao = new System.Windows.Forms.TextBox();
            this.gbbAtendimentos = new System.Windows.Forms.GroupBox();
            this.dgvAtendimentos = new System.Windows.Forms.DataGridView();
            this.tpAtendimento = new System.Windows.Forms.ToolTip(this.components);
            this.lblCentroCusto = new System.Windows.Forms.TextBox();
            this.btnCentroCusto = new System.Windows.Forms.Button();
            this.textCentroCusto = new System.Windows.Forms.TextBox();
            this.btnMedicoExaminador = new System.Windows.Forms.Button();
            this.textMedicoExaminador = new System.Windows.Forms.TextBox();
            this.lblMedicoExaminador = new System.Windows.Forms.TextBox();
            this.flpExameAvulso = new System.Windows.Forms.FlowLayoutPanel();
            this.btnModificarDataTranscritoExtra = new System.Windows.Forms.Button();
            this.btnAplicarDataTranscritoPcmso = new System.Windows.Forms.Button();
            this.flpGheSetor = new System.Windows.Forms.FlowLayoutPanel();
            this.btnIncluirGhe = new System.Windows.Forms.Button();
            this.btnExcluirGhe = new System.Windows.Forms.Button();
            this.flpRisco = new System.Windows.Forms.FlowLayoutPanel();
            this.btnIncluirRisco = new System.Windows.Forms.Button();
            this.btnExcluirRisco = new System.Windows.Forms.Button();
            this.flpExamePcmso = new System.Windows.Forms.FlowLayoutPanel();
            this.textCodigoPO = new System.Windows.Forms.TextBox();
            this.lblDataVencimento = new System.Windows.Forms.TextBox();
            this.lblSenhaAtendimento = new System.Windows.Forms.TextBox();
            this.cbConfinado = new SWS.ComboBoxWithBorder();
            this.lblPrioridade = new System.Windows.Forms.TextBox();
            this.lblEspacoConfinado = new System.Windows.Forms.TextBox();
            this.textSenha = new System.Windows.Forms.TextBox();
            this.cbAltura = new SWS.ComboBoxWithBorder();
            this.lblCodigoPO = new System.Windows.Forms.TextBox();
            this.lblTrabalhoAltura = new System.Windows.Forms.TextBox();
            this.lblDataAtendimento = new System.Windows.Forms.TextBox();
            this.cbPrioridade = new SWS.ComboBoxWithBorder();
            this.dataVencimento = new System.Windows.Forms.DateTimePicker();
            this.dataAso = new System.Windows.Forms.DateTimePicker();
            this.btnExcluirCoordenador = new System.Windows.Forms.Button();
            this.btCliente = new System.Windows.Forms.Button();
            this.btPcmso = new System.Windows.Forms.Button();
            this.btnIncluirMedicoCoordenador = new System.Windows.Forms.Button();
            this.cbTipoAtendimento = new SWS.ComboBoxWithBorder();
            this.lblTipoAtendimento = new System.Windows.Forms.TextBox();
            this.textCliente = new System.Windows.Forms.TextBox();
            this.lblCliente = new System.Windows.Forms.TextBox();
            this.textCnpj = new System.Windows.Forms.MaskedTextBox();
            this.lblCnpj = new System.Windows.Forms.TextBox();
            this.textFuncao = new System.Windows.Forms.TextBox();
            this.lblFuncao = new System.Windows.Forms.TextBox();
            this.textPcmso = new System.Windows.Forms.MaskedTextBox();
            this.lblPcmso = new System.Windows.Forms.TextBox();
            this.textdataValidadePcmso = new System.Windows.Forms.TextBox();
            this.lblDataValidade = new System.Windows.Forms.TextBox();
            this.textCoordenador = new System.Windows.Forms.TextBox();
            this.lblCoordenador = new System.Windows.Forms.TextBox();
            this.textBloqueio = new System.Windows.Forms.TextBox();
            this.cbBloqueio = new SWS.ComboBoxWithBorder();
            this.lblEletricidade = new System.Windows.Forms.TextBox();
            this.cbEletricidade = new SWS.ComboBoxWithBorder();
            this.grbAtendimento = new System.Windows.Forms.GroupBox();
            this.grbColaborador = new System.Windows.Forms.GroupBox();
            ((System.ComponentModel.ISupportInitialize)(this.spForm)).BeginInit();
            this.spForm.Panel1.SuspendLayout();
            this.spForm.Panel2.SuspendLayout();
            this.spForm.SuspendLayout();
            this.pnlForm.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.banner)).BeginInit();
            this.flpAcao.SuspendLayout();
            this.grb_ghe.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvGhe)).BeginInit();
            this.grb_risco.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvRisco)).BeginInit();
            this.grbExamePcmso.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvExamePcmso)).BeginInit();
            this.grbExameAvulso.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvExameAvulso)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.IncluirAtendimentoError)).BeginInit();
            this.grbObservação.SuspendLayout();
            this.gbbAtendimentos.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvAtendimentos)).BeginInit();
            this.flpExameAvulso.SuspendLayout();
            this.flpGheSetor.SuspendLayout();
            this.flpRisco.SuspendLayout();
            this.flpExamePcmso.SuspendLayout();
            this.grbAtendimento.SuspendLayout();
            this.grbColaborador.SuspendLayout();
            this.SuspendLayout();
            // 
            // spForm
            // 
            this.spForm.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.spForm.Location = new System.Drawing.Point(0, 48);
            this.spForm.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            // 
            // spForm.Panel1
            // 
            this.spForm.Panel1.Controls.Add(this.flpAcao);
            this.spForm.Size = new System.Drawing.Size(784, 509);
            this.spForm.SplitterWidth = 5;
            // 
            // pnlForm
            // 
            this.pnlForm.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.pnlForm.AutoScroll = true;
            this.pnlForm.Controls.Add(this.grbColaborador);
            this.pnlForm.Controls.Add(this.grbAtendimento);
            this.pnlForm.Controls.Add(this.flpExamePcmso);
            this.pnlForm.Controls.Add(this.flpGheSetor);
            this.pnlForm.Controls.Add(this.flpRisco);
            this.pnlForm.Controls.Add(this.flpExameAvulso);
            this.pnlForm.Controls.Add(this.gbbAtendimentos);
            this.pnlForm.Controls.Add(this.grbObservação);
            this.pnlForm.Controls.Add(this.grbExameAvulso);
            this.pnlForm.Controls.Add(this.grbExamePcmso);
            this.pnlForm.Controls.Add(this.grb_risco);
            this.pnlForm.Controls.Add(this.grb_ghe);
            this.pnlForm.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.pnlForm.Size = new System.Drawing.Size(781, 469);
            // 
            // banner
            // 
            this.banner.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.banner.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.banner.Size = new System.Drawing.Size(784, 40);
            // 
            // flpAcao
            // 
            this.flpAcao.Controls.Add(this.btn_grava);
            this.flpAcao.Controls.Add(this.btFechar);
            this.flpAcao.Dock = System.Windows.Forms.DockStyle.Fill;
            this.flpAcao.Location = new System.Drawing.Point(0, 0);
            this.flpAcao.Name = "flpAcao";
            this.flpAcao.Size = new System.Drawing.Size(784, 35);
            this.flpAcao.TabIndex = 0;
            // 
            // btn_grava
            // 
            this.btn_grava.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_grava.Image = global::SWS.Properties.Resources.Gravar;
            this.btn_grava.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btn_grava.Location = new System.Drawing.Point(3, 3);
            this.btn_grava.Name = "btn_grava";
            this.btn_grava.Size = new System.Drawing.Size(75, 23);
            this.btn_grava.TabIndex = 1;
            this.btn_grava.TabStop = false;
            this.btn_grava.Text = "&Gravar";
            this.btn_grava.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btn_grava.UseVisualStyleBackColor = true;
            this.btn_grava.Click += new System.EventHandler(this.btn_grava_Click);
            // 
            // btFechar
            // 
            this.btFechar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btFechar.Image = global::SWS.Properties.Resources.close;
            this.btFechar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btFechar.Location = new System.Drawing.Point(84, 3);
            this.btFechar.Name = "btFechar";
            this.btFechar.Size = new System.Drawing.Size(75, 23);
            this.btFechar.TabIndex = 3;
            this.btFechar.TabStop = false;
            this.btFechar.Text = "&Fechar";
            this.btFechar.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btFechar.UseVisualStyleBackColor = true;
            this.btFechar.Click += new System.EventHandler(this.btFechar_Click);
            // 
            // textNome
            // 
            this.textNome.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.textNome.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.textNome.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textNome.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.textNome.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textNome.ForeColor = System.Drawing.Color.Blue;
            this.textNome.Location = new System.Drawing.Point(152, 15);
            this.textNome.MaxLength = 100;
            this.textNome.Name = "textNome";
            this.textNome.ReadOnly = true;
            this.textNome.Size = new System.Drawing.Size(518, 21);
            this.textNome.TabIndex = 0;
            this.textNome.TabStop = false;
            // 
            // textRg
            // 
            this.textRg.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.textRg.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.textRg.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textRg.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textRg.Location = new System.Drawing.Point(152, 53);
            this.textRg.MaxLength = 15;
            this.textRg.Name = "textRg";
            this.textRg.ReadOnly = true;
            this.textRg.Size = new System.Drawing.Size(550, 21);
            this.textRg.TabIndex = 0;
            this.textRg.TabStop = false;
            // 
            // textCpf
            // 
            this.textCpf.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.textCpf.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.textCpf.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textCpf.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textCpf.ForeColor = System.Drawing.Color.Blue;
            this.textCpf.Location = new System.Drawing.Point(152, 34);
            this.textCpf.Mask = "999,999,999-99";
            this.textCpf.Name = "textCpf";
            this.textCpf.PromptChar = ' ';
            this.textCpf.ReadOnly = true;
            this.textCpf.Size = new System.Drawing.Size(550, 21);
            this.textCpf.TabIndex = 0;
            this.textCpf.TabStop = false;
            // 
            // lblCpf
            // 
            this.lblCpf.BackColor = System.Drawing.Color.LightGray;
            this.lblCpf.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblCpf.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCpf.Location = new System.Drawing.Point(6, 34);
            this.lblCpf.MaxLength = 15;
            this.lblCpf.Name = "lblCpf";
            this.lblCpf.ReadOnly = true;
            this.lblCpf.Size = new System.Drawing.Size(147, 21);
            this.lblCpf.TabIndex = 0;
            this.lblCpf.TabStop = false;
            this.lblCpf.Text = "CPF";
            // 
            // lblRg
            // 
            this.lblRg.BackColor = System.Drawing.Color.LightGray;
            this.lblRg.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblRg.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblRg.Location = new System.Drawing.Point(6, 53);
            this.lblRg.MaxLength = 15;
            this.lblRg.Name = "lblRg";
            this.lblRg.ReadOnly = true;
            this.lblRg.Size = new System.Drawing.Size(147, 21);
            this.lblRg.TabIndex = 0;
            this.lblRg.TabStop = false;
            this.lblRg.Text = "RG";
            // 
            // lblNome
            // 
            this.lblNome.BackColor = System.Drawing.Color.LightGray;
            this.lblNome.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblNome.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNome.Location = new System.Drawing.Point(6, 15);
            this.lblNome.MaxLength = 15;
            this.lblNome.Name = "lblNome";
            this.lblNome.ReadOnly = true;
            this.lblNome.Size = new System.Drawing.Size(147, 21);
            this.lblNome.TabIndex = 0;
            this.lblNome.TabStop = false;
            this.lblNome.Text = "Nome do colaborador";
            // 
            // grb_ghe
            // 
            this.grb_ghe.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.grb_ghe.Controls.Add(this.dgvGhe);
            this.grb_ghe.Location = new System.Drawing.Point(4, 705);
            this.grb_ghe.Name = "grb_ghe";
            this.grb_ghe.Size = new System.Drawing.Size(706, 152);
            this.grb_ghe.TabIndex = 36;
            this.grb_ghe.TabStop = false;
            this.grb_ghe.Text = "Ghe / Setor";
            // 
            // dgvGhe
            // 
            this.dgvGhe.AllowUserToAddRows = false;
            this.dgvGhe.AllowUserToDeleteRows = false;
            this.dgvGhe.AllowUserToOrderColumns = true;
            this.dgvGhe.AllowUserToResizeRows = false;
            dataGridViewCellStyle7.BackColor = System.Drawing.Color.Gainsboro;
            this.dgvGhe.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle7;
            this.dgvGhe.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this.dgvGhe.BackgroundColor = System.Drawing.Color.White;
            this.dgvGhe.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dataGridViewCellStyle8.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle8.BackColor = System.Drawing.Color.Silver;
            dataGridViewCellStyle8.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle8.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle8.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle8.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle8.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvGhe.DefaultCellStyle = dataGridViewCellStyle8;
            this.dgvGhe.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvGhe.Location = new System.Drawing.Point(3, 16);
            this.dgvGhe.MultiSelect = false;
            this.dgvGhe.Name = "dgvGhe";
            this.dgvGhe.RowHeadersVisible = false;
            this.dgvGhe.Size = new System.Drawing.Size(700, 133);
            this.dgvGhe.TabIndex = 1;
            this.dgvGhe.TabStop = false;
            this.dgvGhe.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvGhe_CellContentClick);
            // 
            // grb_risco
            // 
            this.grb_risco.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.grb_risco.Controls.Add(this.dgvRisco);
            this.grb_risco.Location = new System.Drawing.Point(6, 898);
            this.grb_risco.Name = "grb_risco";
            this.grb_risco.Size = new System.Drawing.Size(704, 157);
            this.grb_risco.TabIndex = 37;
            this.grb_risco.TabStop = false;
            this.grb_risco.Text = "Riscos";
            // 
            // dgvRisco
            // 
            this.dgvRisco.AllowUserToAddRows = false;
            this.dgvRisco.AllowUserToDeleteRows = false;
            this.dgvRisco.AllowUserToOrderColumns = true;
            this.dgvRisco.AllowUserToResizeRows = false;
            dataGridViewCellStyle5.BackColor = System.Drawing.Color.Gainsboro;
            this.dgvRisco.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle5;
            this.dgvRisco.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this.dgvRisco.BackgroundColor = System.Drawing.Color.White;
            this.dgvRisco.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle6.BackColor = System.Drawing.Color.Silver;
            dataGridViewCellStyle6.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle6.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle6.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle6.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle6.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvRisco.DefaultCellStyle = dataGridViewCellStyle6;
            this.dgvRisco.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvRisco.Location = new System.Drawing.Point(3, 16);
            this.dgvRisco.Name = "dgvRisco";
            this.dgvRisco.RowHeadersVisible = false;
            this.dgvRisco.Size = new System.Drawing.Size(698, 138);
            this.dgvRisco.TabIndex = 0;
            this.dgvRisco.TabStop = false;
            // 
            // grbExamePcmso
            // 
            this.grbExamePcmso.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.grbExamePcmso.Controls.Add(this.chk_examesPCMSO);
            this.grbExamePcmso.Controls.Add(this.dgvExamePcmso);
            this.grbExamePcmso.Location = new System.Drawing.Point(6, 1096);
            this.grbExamePcmso.Name = "grbExamePcmso";
            this.grbExamePcmso.Size = new System.Drawing.Size(704, 280);
            this.grbExamePcmso.TabIndex = 38;
            this.grbExamePcmso.TabStop = false;
            this.grbExamePcmso.Text = "Sugestão de exames baseado no PCMSO";
            // 
            // chk_examesPCMSO
            // 
            this.chk_examesPCMSO.AutoSize = true;
            this.chk_examesPCMSO.CheckAlign = System.Drawing.ContentAlignment.BottomLeft;
            this.chk_examesPCMSO.Checked = true;
            this.chk_examesPCMSO.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chk_examesPCMSO.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.chk_examesPCMSO.Location = new System.Drawing.Point(6, 14);
            this.chk_examesPCMSO.Name = "chk_examesPCMSO";
            this.chk_examesPCMSO.Size = new System.Drawing.Size(160, 18);
            this.chk_examesPCMSO.TabIndex = 39;
            this.chk_examesPCMSO.TabStop = false;
            this.chk_examesPCMSO.Text = "Marcar / Desmarcar Todos";
            this.chk_examesPCMSO.UseVisualStyleBackColor = true;
            this.chk_examesPCMSO.CheckedChanged += new System.EventHandler(this.chk_examesPCMSO_CheckedChanged);
            // 
            // dgvExamePcmso
            // 
            this.dgvExamePcmso.AllowUserToAddRows = false;
            this.dgvExamePcmso.AllowUserToDeleteRows = false;
            this.dgvExamePcmso.AllowUserToOrderColumns = true;
            this.dgvExamePcmso.AllowUserToResizeRows = false;
            this.dgvExamePcmso.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dgvExamePcmso.BackgroundColor = System.Drawing.Color.White;
            this.dgvExamePcmso.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle4.BackColor = System.Drawing.Color.Silver;
            dataGridViewCellStyle4.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle4.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle4.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle4.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle4.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvExamePcmso.DefaultCellStyle = dataGridViewCellStyle4;
            this.dgvExamePcmso.Location = new System.Drawing.Point(3, 38);
            this.dgvExamePcmso.MultiSelect = false;
            this.dgvExamePcmso.Name = "dgvExamePcmso";
            this.dgvExamePcmso.RowHeadersVisible = false;
            this.dgvExamePcmso.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.CellSelect;
            this.dgvExamePcmso.Size = new System.Drawing.Size(698, 239);
            this.dgvExamePcmso.TabIndex = 3;
            this.dgvExamePcmso.TabStop = false;
            this.dgvExamePcmso.CellValueChanged += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvExamePcmso_CellValueChanged);
            this.dgvExamePcmso.CurrentCellDirtyStateChanged += new System.EventHandler(this.dgvExamePcmso_CurrentCellDirtyStateChanged);
            // 
            // grbExameAvulso
            // 
            this.grbExameAvulso.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.grbExameAvulso.Controls.Add(this.chkExamesAvulso);
            this.grbExameAvulso.Controls.Add(this.dgvExameAvulso);
            this.grbExameAvulso.Location = new System.Drawing.Point(9, 1420);
            this.grbExameAvulso.Name = "grbExameAvulso";
            this.grbExameAvulso.Size = new System.Drawing.Size(701, 255);
            this.grbExameAvulso.TabIndex = 40;
            this.grbExameAvulso.TabStop = false;
            this.grbExameAvulso.Text = "Exames avulso";
            // 
            // chkExamesAvulso
            // 
            this.chkExamesAvulso.AutoSize = true;
            this.chkExamesAvulso.CheckAlign = System.Drawing.ContentAlignment.BottomLeft;
            this.chkExamesAvulso.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.chkExamesAvulso.Location = new System.Drawing.Point(3, 16);
            this.chkExamesAvulso.Name = "chkExamesAvulso";
            this.chkExamesAvulso.Size = new System.Drawing.Size(160, 18);
            this.chkExamesAvulso.TabIndex = 3;
            this.chkExamesAvulso.TabStop = false;
            this.chkExamesAvulso.Text = "Marcar / Desmarcar Todos";
            this.chkExamesAvulso.UseVisualStyleBackColor = true;
            this.chkExamesAvulso.CheckedChanged += new System.EventHandler(this.chk_examesExtras_CheckedChanged);
            // 
            // dgvExameAvulso
            // 
            this.dgvExameAvulso.AllowUserToAddRows = false;
            this.dgvExameAvulso.AllowUserToDeleteRows = false;
            this.dgvExameAvulso.AllowUserToOrderColumns = true;
            this.dgvExameAvulso.AllowUserToResizeRows = false;
            this.dgvExameAvulso.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dgvExameAvulso.BackgroundColor = System.Drawing.Color.White;
            this.dgvExameAvulso.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle3.BackColor = System.Drawing.Color.Silver;
            dataGridViewCellStyle3.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvExameAvulso.DefaultCellStyle = dataGridViewCellStyle3;
            this.dgvExameAvulso.Location = new System.Drawing.Point(3, 40);
            this.dgvExameAvulso.MultiSelect = false;
            this.dgvExameAvulso.Name = "dgvExameAvulso";
            this.dgvExameAvulso.RowHeadersVisible = false;
            this.dgvExameAvulso.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.CellSelect;
            this.dgvExameAvulso.Size = new System.Drawing.Size(695, 212);
            this.dgvExameAvulso.TabIndex = 4;
            this.dgvExameAvulso.TabStop = false;
            this.dgvExameAvulso.CellValueChanged += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvExameAvulso_CellValueChanged);
            this.dgvExameAvulso.CurrentCellDirtyStateChanged += new System.EventHandler(this.dgvExameAvulso_CurrentCellDirtyStateChanged);
            // 
            // btn_incluirExames
            // 
            this.btn_incluirExames.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_incluirExames.Image = global::SWS.Properties.Resources.icone_mais;
            this.btn_incluirExames.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btn_incluirExames.Location = new System.Drawing.Point(3, 3);
            this.btn_incluirExames.Name = "btn_incluirExames";
            this.btn_incluirExames.Size = new System.Drawing.Size(75, 23);
            this.btn_incluirExames.TabIndex = 5;
            this.btn_incluirExames.TabStop = false;
            this.btn_incluirExames.Text = "&Incluir";
            this.btn_incluirExames.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btn_incluirExames.UseVisualStyleBackColor = true;
            this.btn_incluirExames.Click += new System.EventHandler(this.btn_incluirExames_Click);
            // 
            // btFuncionario
            // 
            this.btFuncionario.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btFuncionario.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btFuncionario.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btFuncionario.Image = global::SWS.Properties.Resources.busca;
            this.btFuncionario.Location = new System.Drawing.Point(668, 15);
            this.btFuncionario.Name = "btFuncionario";
            this.btFuncionario.Size = new System.Drawing.Size(34, 21);
            this.btFuncionario.TabIndex = 1;
            this.btFuncionario.Text = " ";
            this.btFuncionario.UseVisualStyleBackColor = true;
            this.btFuncionario.Click += new System.EventHandler(this.btFuncionario_Click);
            // 
            // lblIdade
            // 
            this.lblIdade.BackColor = System.Drawing.Color.LightGray;
            this.lblIdade.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblIdade.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblIdade.Location = new System.Drawing.Point(6, 71);
            this.lblIdade.MaxLength = 15;
            this.lblIdade.Name = "lblIdade";
            this.lblIdade.ReadOnly = true;
            this.lblIdade.Size = new System.Drawing.Size(147, 21);
            this.lblIdade.TabIndex = 0;
            this.lblIdade.TabStop = false;
            this.lblIdade.Text = "Idade";
            // 
            // textIdade
            // 
            this.textIdade.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.textIdade.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.textIdade.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textIdade.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textIdade.ForeColor = System.Drawing.Color.Blue;
            this.textIdade.Location = new System.Drawing.Point(152, 71);
            this.textIdade.MaxLength = 15;
            this.textIdade.Name = "textIdade";
            this.textIdade.ReadOnly = true;
            this.textIdade.Size = new System.Drawing.Size(550, 21);
            this.textIdade.TabIndex = 0;
            this.textIdade.TabStop = false;
            // 
            // textPrestador
            // 
            this.textPrestador.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.textPrestador.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.textPrestador.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textPrestador.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.textPrestador.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textPrestador.ForeColor = System.Drawing.Color.Blue;
            this.textPrestador.Location = new System.Drawing.Point(149, 379);
            this.textPrestador.MaxLength = 100;
            this.textPrestador.Name = "textPrestador";
            this.textPrestador.ReadOnly = true;
            this.textPrestador.Size = new System.Drawing.Size(505, 21);
            this.textPrestador.TabIndex = 45;
            this.textPrestador.TabStop = false;
            // 
            // lblPrestador
            // 
            this.lblPrestador.BackColor = System.Drawing.Color.LightGray;
            this.lblPrestador.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblPrestador.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPrestador.Location = new System.Drawing.Point(3, 379);
            this.lblPrestador.MaxLength = 15;
            this.lblPrestador.Name = "lblPrestador";
            this.lblPrestador.ReadOnly = true;
            this.lblPrestador.Size = new System.Drawing.Size(147, 21);
            this.lblPrestador.TabIndex = 44;
            this.lblPrestador.TabStop = false;
            this.lblPrestador.Text = "Prestador de Serviços";
            // 
            // btPrestador
            // 
            this.btPrestador.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btPrestador.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btPrestador.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btPrestador.Image = global::SWS.Properties.Resources.busca;
            this.btPrestador.Location = new System.Drawing.Point(651, 379);
            this.btPrestador.Name = "btPrestador";
            this.btPrestador.Size = new System.Drawing.Size(34, 21);
            this.btPrestador.TabIndex = 17;
            this.btPrestador.UseVisualStyleBackColor = true;
            this.btPrestador.Click += new System.EventHandler(this.btPrestador_Click);
            // 
            // textCnpjPrestador
            // 
            this.textCnpjPrestador.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.textCnpjPrestador.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.textCnpjPrestador.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textCnpjPrestador.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textCnpjPrestador.ForeColor = System.Drawing.Color.Blue;
            this.textCnpjPrestador.Location = new System.Drawing.Point(149, 399);
            this.textCnpjPrestador.Mask = "00,000,000/0000-00";
            this.textCnpjPrestador.Name = "textCnpjPrestador";
            this.textCnpjPrestador.PromptChar = ' ';
            this.textCnpjPrestador.ReadOnly = true;
            this.textCnpjPrestador.Size = new System.Drawing.Size(536, 21);
            this.textCnpjPrestador.TabIndex = 50;
            this.textCnpjPrestador.TabStop = false;
            // 
            // lblCnpjPrestador
            // 
            this.lblCnpjPrestador.BackColor = System.Drawing.Color.LightGray;
            this.lblCnpjPrestador.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblCnpjPrestador.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCnpjPrestador.Location = new System.Drawing.Point(3, 399);
            this.lblCnpjPrestador.MaxLength = 15;
            this.lblCnpjPrestador.Name = "lblCnpjPrestador";
            this.lblCnpjPrestador.ReadOnly = true;
            this.lblCnpjPrestador.Size = new System.Drawing.Size(147, 21);
            this.lblCnpjPrestador.TabIndex = 49;
            this.lblCnpjPrestador.TabStop = false;
            this.lblCnpjPrestador.Text = "CNPJ";
            // 
            // IncluirAtendimentoError
            // 
            this.IncluirAtendimentoError.ContainerControl = this;
            // 
            // grbObservação
            // 
            this.grbObservação.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.grbObservação.Controls.Add(this.textObservacao);
            this.grbObservação.Location = new System.Drawing.Point(12, 1716);
            this.grbObservação.Name = "grbObservação";
            this.grbObservação.Size = new System.Drawing.Size(698, 127);
            this.grbObservação.TabIndex = 52;
            this.grbObservação.TabStop = false;
            this.grbObservação.Text = "Observação";
            // 
            // textObservacao
            // 
            this.textObservacao.BackColor = System.Drawing.Color.White;
            this.textObservacao.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textObservacao.Dock = System.Windows.Forms.DockStyle.Fill;
            this.textObservacao.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textObservacao.Location = new System.Drawing.Point(3, 16);
            this.textObservacao.Multiline = true;
            this.textObservacao.Name = "textObservacao";
            this.textObservacao.Size = new System.Drawing.Size(692, 108);
            this.textObservacao.TabIndex = 18;
            this.textObservacao.TabStop = false;
            // 
            // gbbAtendimentos
            // 
            this.gbbAtendimentos.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gbbAtendimentos.BackColor = System.Drawing.Color.LightYellow;
            this.gbbAtendimentos.Controls.Add(this.dgvAtendimentos);
            this.gbbAtendimentos.Location = new System.Drawing.Point(3, 112);
            this.gbbAtendimentos.Name = "gbbAtendimentos";
            this.gbbAtendimentos.Size = new System.Drawing.Size(707, 145);
            this.gbbAtendimentos.TabIndex = 57;
            this.gbbAtendimentos.TabStop = false;
            this.gbbAtendimentos.Text = "Atendimentos anteriores";
            // 
            // dgvAtendimentos
            // 
            this.dgvAtendimentos.AllowUserToAddRows = false;
            this.dgvAtendimentos.AllowUserToDeleteRows = false;
            this.dgvAtendimentos.AllowUserToOrderColumns = true;
            this.dgvAtendimentos.AllowUserToResizeRows = false;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.LightGray;
            this.dgvAtendimentos.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle1;
            this.dgvAtendimentos.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this.dgvAtendimentos.BackgroundColor = System.Drawing.Color.White;
            this.dgvAtendimentos.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.Color.Silver;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvAtendimentos.DefaultCellStyle = dataGridViewCellStyle2;
            this.dgvAtendimentos.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvAtendimentos.Location = new System.Drawing.Point(3, 16);
            this.dgvAtendimentos.MultiSelect = false;
            this.dgvAtendimentos.Name = "dgvAtendimentos";
            this.dgvAtendimentos.ReadOnly = true;
            this.dgvAtendimentos.RowHeadersVisible = false;
            this.dgvAtendimentos.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvAtendimentos.Size = new System.Drawing.Size(701, 126);
            this.dgvAtendimentos.TabIndex = 2;
            this.dgvAtendimentos.TabStop = false;
            this.dgvAtendimentos.CellContentDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvAtendimentos_CellContentDoubleClick);
            this.dgvAtendimentos.CellFormatting += new System.Windows.Forms.DataGridViewCellFormattingEventHandler(this.dgvAtendimentos_CellFormatting);
            // 
            // lblCentroCusto
            // 
            this.lblCentroCusto.BackColor = System.Drawing.Color.LightGray;
            this.lblCentroCusto.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblCentroCusto.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCentroCusto.Location = new System.Drawing.Point(3, 359);
            this.lblCentroCusto.MaxLength = 15;
            this.lblCentroCusto.Name = "lblCentroCusto";
            this.lblCentroCusto.ReadOnly = true;
            this.lblCentroCusto.Size = new System.Drawing.Size(147, 21);
            this.lblCentroCusto.TabIndex = 58;
            this.lblCentroCusto.TabStop = false;
            this.lblCentroCusto.Text = "Centro de Custo";
            // 
            // btnCentroCusto
            // 
            this.btnCentroCusto.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnCentroCusto.Enabled = false;
            this.btnCentroCusto.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnCentroCusto.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCentroCusto.Image = global::SWS.Properties.Resources.busca;
            this.btnCentroCusto.Location = new System.Drawing.Point(651, 359);
            this.btnCentroCusto.Name = "btnCentroCusto";
            this.btnCentroCusto.Size = new System.Drawing.Size(34, 21);
            this.btnCentroCusto.TabIndex = 16;
            this.btnCentroCusto.UseVisualStyleBackColor = true;
            this.btnCentroCusto.Click += new System.EventHandler(this.btnCentroCusto_Click);
            // 
            // textCentroCusto
            // 
            this.textCentroCusto.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.textCentroCusto.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.textCentroCusto.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textCentroCusto.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.textCentroCusto.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textCentroCusto.ForeColor = System.Drawing.Color.Blue;
            this.textCentroCusto.Location = new System.Drawing.Point(149, 359);
            this.textCentroCusto.MaxLength = 50;
            this.textCentroCusto.Name = "textCentroCusto";
            this.textCentroCusto.ReadOnly = true;
            this.textCentroCusto.Size = new System.Drawing.Size(505, 21);
            this.textCentroCusto.TabIndex = 59;
            this.textCentroCusto.TabStop = false;
            // 
            // btnMedicoExaminador
            // 
            this.btnMedicoExaminador.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnMedicoExaminador.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnMedicoExaminador.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnMedicoExaminador.Image = global::SWS.Properties.Resources.busca;
            this.btnMedicoExaminador.Location = new System.Drawing.Point(651, 339);
            this.btnMedicoExaminador.Name = "btnMedicoExaminador";
            this.btnMedicoExaminador.Size = new System.Drawing.Size(34, 21);
            this.btnMedicoExaminador.TabIndex = 15;
            this.btnMedicoExaminador.UseVisualStyleBackColor = true;
            this.btnMedicoExaminador.Click += new System.EventHandler(this.btnMedicoExaminador_Click);
            // 
            // textMedicoExaminador
            // 
            this.textMedicoExaminador.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.textMedicoExaminador.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.textMedicoExaminador.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textMedicoExaminador.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textMedicoExaminador.Location = new System.Drawing.Point(149, 339);
            this.textMedicoExaminador.MaxLength = 100;
            this.textMedicoExaminador.Name = "textMedicoExaminador";
            this.textMedicoExaminador.ReadOnly = true;
            this.textMedicoExaminador.Size = new System.Drawing.Size(505, 21);
            this.textMedicoExaminador.TabIndex = 0;
            this.textMedicoExaminador.TabStop = false;
            // 
            // lblMedicoExaminador
            // 
            this.lblMedicoExaminador.BackColor = System.Drawing.Color.LightGray;
            this.lblMedicoExaminador.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblMedicoExaminador.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblMedicoExaminador.Location = new System.Drawing.Point(3, 339);
            this.lblMedicoExaminador.MaxLength = 15;
            this.lblMedicoExaminador.Name = "lblMedicoExaminador";
            this.lblMedicoExaminador.ReadOnly = true;
            this.lblMedicoExaminador.Size = new System.Drawing.Size(147, 21);
            this.lblMedicoExaminador.TabIndex = 0;
            this.lblMedicoExaminador.TabStop = false;
            this.lblMedicoExaminador.Text = "Médico Examinador";
            // 
            // flpExameAvulso
            // 
            this.flpExameAvulso.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.flpExameAvulso.Controls.Add(this.btn_incluirExames);
            this.flpExameAvulso.Controls.Add(this.btnModificarDataTranscritoExtra);
            this.flpExameAvulso.Location = new System.Drawing.Point(12, 1678);
            this.flpExameAvulso.Name = "flpExameAvulso";
            this.flpExameAvulso.Size = new System.Drawing.Size(698, 32);
            this.flpExameAvulso.TabIndex = 61;
            // 
            // btnModificarDataTranscritoExtra
            // 
            this.btnModificarDataTranscritoExtra.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnModificarDataTranscritoExtra.Image = global::SWS.Properties.Resources.copiar;
            this.btnModificarDataTranscritoExtra.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnModificarDataTranscritoExtra.Location = new System.Drawing.Point(84, 3);
            this.btnModificarDataTranscritoExtra.Name = "btnModificarDataTranscritoExtra";
            this.btnModificarDataTranscritoExtra.Size = new System.Drawing.Size(75, 23);
            this.btnModificarDataTranscritoExtra.TabIndex = 67;
            this.btnModificarDataTranscritoExtra.Text = "&Modificar Data Transcrito";
            this.btnModificarDataTranscritoExtra.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnModificarDataTranscritoExtra.UseVisualStyleBackColor = true;
            this.btnModificarDataTranscritoExtra.Click += new System.EventHandler(this.btnModificarDataTranscritoExtra_Click);
            // 
            // btnAplicarDataTranscritoPcmso
            // 
            this.btnAplicarDataTranscritoPcmso.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnAplicarDataTranscritoPcmso.Image = global::SWS.Properties.Resources.copiar;
            this.btnAplicarDataTranscritoPcmso.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnAplicarDataTranscritoPcmso.Location = new System.Drawing.Point(3, 3);
            this.btnAplicarDataTranscritoPcmso.Name = "btnAplicarDataTranscritoPcmso";
            this.btnAplicarDataTranscritoPcmso.Size = new System.Drawing.Size(75, 23);
            this.btnAplicarDataTranscritoPcmso.TabIndex = 66;
            this.btnAplicarDataTranscritoPcmso.TabStop = false;
            this.btnAplicarDataTranscritoPcmso.Text = "&Modificar Data Transcrito";
            this.btnAplicarDataTranscritoPcmso.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnAplicarDataTranscritoPcmso.UseVisualStyleBackColor = true;
            this.btnAplicarDataTranscritoPcmso.Click += new System.EventHandler(this.btnAplicarDataTranscrito_Click);
            // 
            // flpGheSetor
            // 
            this.flpGheSetor.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.flpGheSetor.Controls.Add(this.btnIncluirGhe);
            this.flpGheSetor.Controls.Add(this.btnExcluirGhe);
            this.flpGheSetor.Location = new System.Drawing.Point(7, 860);
            this.flpGheSetor.Name = "flpGheSetor";
            this.flpGheSetor.Size = new System.Drawing.Size(703, 32);
            this.flpGheSetor.TabIndex = 69;
            // 
            // btnIncluirGhe
            // 
            this.btnIncluirGhe.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnIncluirGhe.Image = global::SWS.Properties.Resources.icone_mais;
            this.btnIncluirGhe.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnIncluirGhe.Location = new System.Drawing.Point(3, 3);
            this.btnIncluirGhe.Name = "btnIncluirGhe";
            this.btnIncluirGhe.Size = new System.Drawing.Size(75, 23);
            this.btnIncluirGhe.TabIndex = 5;
            this.btnIncluirGhe.TabStop = false;
            this.btnIncluirGhe.Text = "&Incluir";
            this.btnIncluirGhe.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnIncluirGhe.UseVisualStyleBackColor = true;
            this.btnIncluirGhe.Click += new System.EventHandler(this.btnIncluirGhe_Click);
            // 
            // btnExcluirGhe
            // 
            this.btnExcluirGhe.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnExcluirGhe.Image = global::SWS.Properties.Resources.lixeira;
            this.btnExcluirGhe.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnExcluirGhe.Location = new System.Drawing.Point(84, 3);
            this.btnExcluirGhe.Name = "btnExcluirGhe";
            this.btnExcluirGhe.Size = new System.Drawing.Size(75, 23);
            this.btnExcluirGhe.TabIndex = 67;
            this.btnExcluirGhe.Text = "&Excluir";
            this.btnExcluirGhe.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnExcluirGhe.UseVisualStyleBackColor = true;
            this.btnExcluirGhe.Click += new System.EventHandler(this.btnExcluirGhe_Click);
            // 
            // flpRisco
            // 
            this.flpRisco.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.flpRisco.Controls.Add(this.btnIncluirRisco);
            this.flpRisco.Controls.Add(this.btnExcluirRisco);
            this.flpRisco.Location = new System.Drawing.Point(9, 1058);
            this.flpRisco.Name = "flpRisco";
            this.flpRisco.Size = new System.Drawing.Size(701, 32);
            this.flpRisco.TabIndex = 70;
            // 
            // btnIncluirRisco
            // 
            this.btnIncluirRisco.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnIncluirRisco.Image = global::SWS.Properties.Resources.icone_mais;
            this.btnIncluirRisco.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnIncluirRisco.Location = new System.Drawing.Point(3, 3);
            this.btnIncluirRisco.Name = "btnIncluirRisco";
            this.btnIncluirRisco.Size = new System.Drawing.Size(75, 23);
            this.btnIncluirRisco.TabIndex = 5;
            this.btnIncluirRisco.TabStop = false;
            this.btnIncluirRisco.Text = "&Incluir";
            this.btnIncluirRisco.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnIncluirRisco.UseVisualStyleBackColor = true;
            this.btnIncluirRisco.Click += new System.EventHandler(this.btnIncluirRisco_Click);
            // 
            // btnExcluirRisco
            // 
            this.btnExcluirRisco.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnExcluirRisco.Image = global::SWS.Properties.Resources.lixeira;
            this.btnExcluirRisco.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnExcluirRisco.Location = new System.Drawing.Point(84, 3);
            this.btnExcluirRisco.Name = "btnExcluirRisco";
            this.btnExcluirRisco.Size = new System.Drawing.Size(75, 23);
            this.btnExcluirRisco.TabIndex = 67;
            this.btnExcluirRisco.Text = "&Excluir";
            this.btnExcluirRisco.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnExcluirRisco.UseVisualStyleBackColor = true;
            this.btnExcluirRisco.Click += new System.EventHandler(this.btnExcluirRisco_Click);
            // 
            // flpExamePcmso
            // 
            this.flpExamePcmso.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.flpExamePcmso.Controls.Add(this.btnAplicarDataTranscritoPcmso);
            this.flpExamePcmso.Location = new System.Drawing.Point(9, 1382);
            this.flpExamePcmso.Name = "flpExamePcmso";
            this.flpExamePcmso.Size = new System.Drawing.Size(701, 32);
            this.flpExamePcmso.TabIndex = 72;
            // 
            // textCodigoPO
            // 
            this.textCodigoPO.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.textCodigoPO.BackColor = System.Drawing.Color.White;
            this.textCodigoPO.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textCodigoPO.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.textCodigoPO.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textCodigoPO.ForeColor = System.Drawing.Color.Black;
            this.textCodigoPO.Location = new System.Drawing.Point(149, 259);
            this.textCodigoPO.MaxLength = 15;
            this.textCodigoPO.Name = "textCodigoPO";
            this.textCodigoPO.Size = new System.Drawing.Size(536, 21);
            this.textCodigoPO.TabIndex = 11;
            // 
            // lblDataVencimento
            // 
            this.lblDataVencimento.BackColor = System.Drawing.Color.LightGray;
            this.lblDataVencimento.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblDataVencimento.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDataVencimento.Location = new System.Drawing.Point(3, 219);
            this.lblDataVencimento.MaxLength = 15;
            this.lblDataVencimento.Name = "lblDataVencimento";
            this.lblDataVencimento.ReadOnly = true;
            this.lblDataVencimento.Size = new System.Drawing.Size(147, 21);
            this.lblDataVencimento.TabIndex = 62;
            this.lblDataVencimento.TabStop = false;
            this.lblDataVencimento.Text = "Data de Vencimento";
            // 
            // lblSenhaAtendimento
            // 
            this.lblSenhaAtendimento.BackColor = System.Drawing.Color.LightGray;
            this.lblSenhaAtendimento.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblSenhaAtendimento.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSenhaAtendimento.Location = new System.Drawing.Point(3, 239);
            this.lblSenhaAtendimento.MaxLength = 15;
            this.lblSenhaAtendimento.Name = "lblSenhaAtendimento";
            this.lblSenhaAtendimento.ReadOnly = true;
            this.lblSenhaAtendimento.Size = new System.Drawing.Size(147, 21);
            this.lblSenhaAtendimento.TabIndex = 0;
            this.lblSenhaAtendimento.TabStop = false;
            this.lblSenhaAtendimento.Text = "Senha do Atendimento";
            // 
            // cbConfinado
            // 
            this.cbConfinado.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.cbConfinado.BackColor = System.Drawing.Color.LightGray;
            this.cbConfinado.BorderColor = System.Drawing.Color.DimGray;
            this.cbConfinado.BorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid;
            this.cbConfinado.Enabled = false;
            this.cbConfinado.FormattingEnabled = true;
            this.cbConfinado.Location = new System.Drawing.Point(149, 279);
            this.cbConfinado.Name = "cbConfinado";
            this.cbConfinado.Size = new System.Drawing.Size(536, 21);
            this.cbConfinado.TabIndex = 12;
            this.cbConfinado.SelectedIndexChanged += new System.EventHandler(this.cbConfinado_SelectedIndexChanged);
            // 
            // lblPrioridade
            // 
            this.lblPrioridade.BackColor = System.Drawing.Color.LightGray;
            this.lblPrioridade.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblPrioridade.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPrioridade.Location = new System.Drawing.Point(3, 179);
            this.lblPrioridade.MaxLength = 15;
            this.lblPrioridade.Name = "lblPrioridade";
            this.lblPrioridade.ReadOnly = true;
            this.lblPrioridade.Size = new System.Drawing.Size(147, 21);
            this.lblPrioridade.TabIndex = 64;
            this.lblPrioridade.TabStop = false;
            this.lblPrioridade.Text = "Prioridade";
            // 
            // lblEspacoConfinado
            // 
            this.lblEspacoConfinado.BackColor = System.Drawing.Color.LightGray;
            this.lblEspacoConfinado.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblEspacoConfinado.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblEspacoConfinado.Location = new System.Drawing.Point(3, 279);
            this.lblEspacoConfinado.MaxLength = 15;
            this.lblEspacoConfinado.Name = "lblEspacoConfinado";
            this.lblEspacoConfinado.ReadOnly = true;
            this.lblEspacoConfinado.Size = new System.Drawing.Size(147, 21);
            this.lblEspacoConfinado.TabIndex = 53;
            this.lblEspacoConfinado.TabStop = false;
            this.lblEspacoConfinado.Text = "Espaço confinado";
            // 
            // textSenha
            // 
            this.textSenha.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.textSenha.BackColor = System.Drawing.SystemColors.Window;
            this.textSenha.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textSenha.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.textSenha.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textSenha.Location = new System.Drawing.Point(149, 239);
            this.textSenha.MaxLength = 6;
            this.textSenha.Name = "textSenha";
            this.textSenha.Size = new System.Drawing.Size(536, 21);
            this.textSenha.TabIndex = 10;
            this.textSenha.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.textSenha_KeyPress);
            // 
            // cbAltura
            // 
            this.cbAltura.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.cbAltura.BackColor = System.Drawing.Color.LightGray;
            this.cbAltura.BorderColor = System.Drawing.Color.DimGray;
            this.cbAltura.BorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid;
            this.cbAltura.Enabled = false;
            this.cbAltura.FormattingEnabled = true;
            this.cbAltura.Location = new System.Drawing.Point(149, 299);
            this.cbAltura.Name = "cbAltura";
            this.cbAltura.Size = new System.Drawing.Size(536, 21);
            this.cbAltura.TabIndex = 13;
            this.cbAltura.SelectedIndexChanged += new System.EventHandler(this.cbAltura_SelectedIndexChanged);
            // 
            // lblCodigoPO
            // 
            this.lblCodigoPO.BackColor = System.Drawing.Color.LightGray;
            this.lblCodigoPO.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblCodigoPO.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCodigoPO.Location = new System.Drawing.Point(3, 259);
            this.lblCodigoPO.MaxLength = 15;
            this.lblCodigoPO.Name = "lblCodigoPO";
            this.lblCodigoPO.ReadOnly = true;
            this.lblCodigoPO.Size = new System.Drawing.Size(147, 21);
            this.lblCodigoPO.TabIndex = 63;
            this.lblCodigoPO.TabStop = false;
            this.lblCodigoPO.Text = "Código PO";
            // 
            // lblTrabalhoAltura
            // 
            this.lblTrabalhoAltura.BackColor = System.Drawing.Color.LightGray;
            this.lblTrabalhoAltura.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblTrabalhoAltura.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTrabalhoAltura.Location = new System.Drawing.Point(3, 299);
            this.lblTrabalhoAltura.MaxLength = 15;
            this.lblTrabalhoAltura.Name = "lblTrabalhoAltura";
            this.lblTrabalhoAltura.ReadOnly = true;
            this.lblTrabalhoAltura.Size = new System.Drawing.Size(147, 21);
            this.lblTrabalhoAltura.TabIndex = 54;
            this.lblTrabalhoAltura.TabStop = false;
            this.lblTrabalhoAltura.Text = "Trabalho em altura";
            // 
            // lblDataAtendimento
            // 
            this.lblDataAtendimento.BackColor = System.Drawing.Color.LightGray;
            this.lblDataAtendimento.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblDataAtendimento.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDataAtendimento.Location = new System.Drawing.Point(3, 199);
            this.lblDataAtendimento.MaxLength = 15;
            this.lblDataAtendimento.Name = "lblDataAtendimento";
            this.lblDataAtendimento.ReadOnly = true;
            this.lblDataAtendimento.Size = new System.Drawing.Size(147, 21);
            this.lblDataAtendimento.TabIndex = 0;
            this.lblDataAtendimento.TabStop = false;
            this.lblDataAtendimento.Text = "Data do Atendimento";
            // 
            // cbPrioridade
            // 
            this.cbPrioridade.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.cbPrioridade.BackColor = System.Drawing.Color.LightGray;
            this.cbPrioridade.BorderColor = System.Drawing.Color.DimGray;
            this.cbPrioridade.BorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid;
            this.cbPrioridade.FormattingEnabled = true;
            this.cbPrioridade.Location = new System.Drawing.Point(149, 179);
            this.cbPrioridade.Name = "cbPrioridade";
            this.cbPrioridade.Size = new System.Drawing.Size(536, 21);
            this.cbPrioridade.TabIndex = 7;
            // 
            // dataVencimento
            // 
            this.dataVencimento.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dataVencimento.Checked = false;
            this.dataVencimento.CustomFormat = "dd/MM/yyyy";
            this.dataVencimento.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dataVencimento.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dataVencimento.Location = new System.Drawing.Point(149, 219);
            this.dataVencimento.Name = "dataVencimento";
            this.dataVencimento.ShowCheckBox = true;
            this.dataVencimento.Size = new System.Drawing.Size(536, 21);
            this.dataVencimento.TabIndex = 9;
            // 
            // dataAso
            // 
            this.dataAso.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dataAso.Checked = false;
            this.dataAso.CustomFormat = "dd/MM/yyyy HH:mm:ss";
            this.dataAso.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dataAso.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dataAso.Location = new System.Drawing.Point(149, 199);
            this.dataAso.Name = "dataAso";
            this.dataAso.ShowCheckBox = true;
            this.dataAso.Size = new System.Drawing.Size(536, 21);
            this.dataAso.TabIndex = 8;
            this.dataAso.ValueChanged += new System.EventHandler(this.dataAso_ValueChanged);
            // 
            // btnExcluirCoordenador
            // 
            this.btnExcluirCoordenador.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnExcluirCoordenador.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnExcluirCoordenador.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnExcluirCoordenador.Image = global::SWS.Properties.Resources.close;
            this.btnExcluirCoordenador.Location = new System.Drawing.Point(651, 159);
            this.btnExcluirCoordenador.Name = "btnExcluirCoordenador";
            this.btnExcluirCoordenador.Size = new System.Drawing.Size(34, 21);
            this.btnExcluirCoordenador.TabIndex = 68;
            this.btnExcluirCoordenador.TabStop = false;
            this.btnExcluirCoordenador.UseVisualStyleBackColor = true;
            this.btnExcluirCoordenador.Click += new System.EventHandler(this.btnExcluirCoordenador_Click);
            // 
            // btCliente
            // 
            this.btCliente.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btCliente.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btCliente.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btCliente.Image = global::SWS.Properties.Resources.busca;
            this.btCliente.Location = new System.Drawing.Point(651, 59);
            this.btCliente.Name = "btCliente";
            this.btCliente.Size = new System.Drawing.Size(34, 21);
            this.btCliente.TabIndex = 4;
            this.btCliente.UseVisualStyleBackColor = true;
            this.btCliente.Click += new System.EventHandler(this.btCliente_Click);
            // 
            // btPcmso
            // 
            this.btPcmso.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btPcmso.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btPcmso.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btPcmso.Image = global::SWS.Properties.Resources.busca;
            this.btPcmso.Location = new System.Drawing.Point(651, 119);
            this.btPcmso.Name = "btPcmso";
            this.btPcmso.Size = new System.Drawing.Size(34, 21);
            this.btPcmso.TabIndex = 5;
            this.btPcmso.UseVisualStyleBackColor = true;
            this.btPcmso.Click += new System.EventHandler(this.btPcmso_Click);
            // 
            // btnIncluirMedicoCoordenador
            // 
            this.btnIncluirMedicoCoordenador.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnIncluirMedicoCoordenador.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnIncluirMedicoCoordenador.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnIncluirMedicoCoordenador.Image = global::SWS.Properties.Resources.busca;
            this.btnIncluirMedicoCoordenador.Location = new System.Drawing.Point(620, 159);
            this.btnIncluirMedicoCoordenador.Name = "btnIncluirMedicoCoordenador";
            this.btnIncluirMedicoCoordenador.Size = new System.Drawing.Size(34, 21);
            this.btnIncluirMedicoCoordenador.TabIndex = 6;
            this.btnIncluirMedicoCoordenador.UseVisualStyleBackColor = true;
            this.btnIncluirMedicoCoordenador.Click += new System.EventHandler(this.btnIncluirMedicoCoordenador_Click);
            // 
            // cbTipoAtendimento
            // 
            this.cbTipoAtendimento.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.cbTipoAtendimento.BackColor = System.Drawing.Color.LightGray;
            this.cbTipoAtendimento.BorderColor = System.Drawing.Color.DimGray;
            this.cbTipoAtendimento.BorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid;
            this.cbTipoAtendimento.FormattingEnabled = true;
            this.cbTipoAtendimento.Location = new System.Drawing.Point(149, 39);
            this.cbTipoAtendimento.Name = "cbTipoAtendimento";
            this.cbTipoAtendimento.Size = new System.Drawing.Size(536, 21);
            this.cbTipoAtendimento.TabIndex = 3;
            this.cbTipoAtendimento.SelectedIndexChanged += new System.EventHandler(this.cbTipoAtendimento_SelectedIndexChanged);
            // 
            // lblTipoAtendimento
            // 
            this.lblTipoAtendimento.BackColor = System.Drawing.Color.LightGray;
            this.lblTipoAtendimento.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblTipoAtendimento.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTipoAtendimento.Location = new System.Drawing.Point(3, 39);
            this.lblTipoAtendimento.MaxLength = 15;
            this.lblTipoAtendimento.Name = "lblTipoAtendimento";
            this.lblTipoAtendimento.ReadOnly = true;
            this.lblTipoAtendimento.Size = new System.Drawing.Size(147, 21);
            this.lblTipoAtendimento.TabIndex = 0;
            this.lblTipoAtendimento.TabStop = false;
            this.lblTipoAtendimento.Text = "Tipo de Atendimento";
            // 
            // textCliente
            // 
            this.textCliente.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.textCliente.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.textCliente.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textCliente.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.textCliente.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textCliente.ForeColor = System.Drawing.Color.Blue;
            this.textCliente.Location = new System.Drawing.Point(149, 59);
            this.textCliente.MaxLength = 100;
            this.textCliente.Name = "textCliente";
            this.textCliente.ReadOnly = true;
            this.textCliente.Size = new System.Drawing.Size(505, 21);
            this.textCliente.TabIndex = 0;
            this.textCliente.TabStop = false;
            // 
            // lblCliente
            // 
            this.lblCliente.BackColor = System.Drawing.Color.LightGray;
            this.lblCliente.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblCliente.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCliente.Location = new System.Drawing.Point(3, 59);
            this.lblCliente.MaxLength = 15;
            this.lblCliente.Name = "lblCliente";
            this.lblCliente.ReadOnly = true;
            this.lblCliente.Size = new System.Drawing.Size(147, 21);
            this.lblCliente.TabIndex = 0;
            this.lblCliente.TabStop = false;
            this.lblCliente.Text = "Cliente";
            // 
            // textCnpj
            // 
            this.textCnpj.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.textCnpj.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.textCnpj.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textCnpj.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textCnpj.ForeColor = System.Drawing.Color.Blue;
            this.textCnpj.Location = new System.Drawing.Point(149, 79);
            this.textCnpj.Mask = "00,000,000/0000-00";
            this.textCnpj.Name = "textCnpj";
            this.textCnpj.PromptChar = ' ';
            this.textCnpj.ReadOnly = true;
            this.textCnpj.Size = new System.Drawing.Size(536, 21);
            this.textCnpj.TabIndex = 0;
            this.textCnpj.TabStop = false;
            // 
            // lblCnpj
            // 
            this.lblCnpj.BackColor = System.Drawing.Color.LightGray;
            this.lblCnpj.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblCnpj.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCnpj.Location = new System.Drawing.Point(3, 79);
            this.lblCnpj.MaxLength = 15;
            this.lblCnpj.Name = "lblCnpj";
            this.lblCnpj.ReadOnly = true;
            this.lblCnpj.Size = new System.Drawing.Size(147, 21);
            this.lblCnpj.TabIndex = 0;
            this.lblCnpj.TabStop = false;
            this.lblCnpj.Text = "CNPJ";
            // 
            // textFuncao
            // 
            this.textFuncao.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.textFuncao.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.textFuncao.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textFuncao.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.textFuncao.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textFuncao.ForeColor = System.Drawing.Color.Blue;
            this.textFuncao.Location = new System.Drawing.Point(149, 99);
            this.textFuncao.MaxLength = 100;
            this.textFuncao.Name = "textFuncao";
            this.textFuncao.ReadOnly = true;
            this.textFuncao.Size = new System.Drawing.Size(536, 21);
            this.textFuncao.TabIndex = 0;
            this.textFuncao.TabStop = false;
            // 
            // lblFuncao
            // 
            this.lblFuncao.BackColor = System.Drawing.Color.LightGray;
            this.lblFuncao.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblFuncao.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblFuncao.Location = new System.Drawing.Point(3, 99);
            this.lblFuncao.MaxLength = 15;
            this.lblFuncao.Name = "lblFuncao";
            this.lblFuncao.ReadOnly = true;
            this.lblFuncao.Size = new System.Drawing.Size(147, 21);
            this.lblFuncao.TabIndex = 0;
            this.lblFuncao.TabStop = false;
            this.lblFuncao.Text = "Função";
            // 
            // textPcmso
            // 
            this.textPcmso.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.textPcmso.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.textPcmso.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textPcmso.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textPcmso.ForeColor = System.Drawing.Color.Blue;
            this.textPcmso.Location = new System.Drawing.Point(149, 119);
            this.textPcmso.Mask = "0000,00,000000/00";
            this.textPcmso.Name = "textPcmso";
            this.textPcmso.PromptChar = ' ';
            this.textPcmso.ReadOnly = true;
            this.textPcmso.Size = new System.Drawing.Size(505, 21);
            this.textPcmso.TabIndex = 31;
            this.textPcmso.TabStop = false;
            // 
            // lblPcmso
            // 
            this.lblPcmso.BackColor = System.Drawing.Color.LightGray;
            this.lblPcmso.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblPcmso.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPcmso.Location = new System.Drawing.Point(3, 119);
            this.lblPcmso.MaxLength = 15;
            this.lblPcmso.Name = "lblPcmso";
            this.lblPcmso.ReadOnly = true;
            this.lblPcmso.Size = new System.Drawing.Size(147, 21);
            this.lblPcmso.TabIndex = 28;
            this.lblPcmso.TabStop = false;
            this.lblPcmso.Text = "PCMSO";
            // 
            // textdataValidadePcmso
            // 
            this.textdataValidadePcmso.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.textdataValidadePcmso.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.textdataValidadePcmso.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textdataValidadePcmso.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.textdataValidadePcmso.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textdataValidadePcmso.ForeColor = System.Drawing.Color.Blue;
            this.textdataValidadePcmso.Location = new System.Drawing.Point(149, 139);
            this.textdataValidadePcmso.MaxLength = 100;
            this.textdataValidadePcmso.Name = "textdataValidadePcmso";
            this.textdataValidadePcmso.ReadOnly = true;
            this.textdataValidadePcmso.Size = new System.Drawing.Size(536, 21);
            this.textdataValidadePcmso.TabIndex = 51;
            this.textdataValidadePcmso.TabStop = false;
            // 
            // lblDataValidade
            // 
            this.lblDataValidade.BackColor = System.Drawing.Color.LightGray;
            this.lblDataValidade.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblDataValidade.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDataValidade.Location = new System.Drawing.Point(3, 139);
            this.lblDataValidade.MaxLength = 15;
            this.lblDataValidade.Name = "lblDataValidade";
            this.lblDataValidade.ReadOnly = true;
            this.lblDataValidade.Size = new System.Drawing.Size(147, 21);
            this.lblDataValidade.TabIndex = 32;
            this.lblDataValidade.TabStop = false;
            this.lblDataValidade.Text = "Data de Validade";
            // 
            // textCoordenador
            // 
            this.textCoordenador.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.textCoordenador.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.textCoordenador.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textCoordenador.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.textCoordenador.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textCoordenador.ForeColor = System.Drawing.Color.Blue;
            this.textCoordenador.Location = new System.Drawing.Point(149, 159);
            this.textCoordenador.MaxLength = 100;
            this.textCoordenador.Name = "textCoordenador";
            this.textCoordenador.ReadOnly = true;
            this.textCoordenador.Size = new System.Drawing.Size(476, 21);
            this.textCoordenador.TabIndex = 35;
            this.textCoordenador.TabStop = false;
            // 
            // lblCoordenador
            // 
            this.lblCoordenador.BackColor = System.Drawing.Color.LightGray;
            this.lblCoordenador.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblCoordenador.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCoordenador.Location = new System.Drawing.Point(3, 159);
            this.lblCoordenador.MaxLength = 15;
            this.lblCoordenador.Name = "lblCoordenador";
            this.lblCoordenador.ReadOnly = true;
            this.lblCoordenador.Size = new System.Drawing.Size(147, 21);
            this.lblCoordenador.TabIndex = 34;
            this.lblCoordenador.TabStop = false;
            this.lblCoordenador.Text = "Coordenador";
            // 
            // textBloqueio
            // 
            this.textBloqueio.BackColor = System.Drawing.Color.LightGray;
            this.textBloqueio.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBloqueio.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBloqueio.Location = new System.Drawing.Point(3, 19);
            this.textBloqueio.MaxLength = 15;
            this.textBloqueio.Name = "textBloqueio";
            this.textBloqueio.ReadOnly = true;
            this.textBloqueio.Size = new System.Drawing.Size(147, 21);
            this.textBloqueio.TabIndex = 73;
            this.textBloqueio.TabStop = false;
            this.textBloqueio.Text = "Gravar Bloqueado?";
            // 
            // cbBloqueio
            // 
            this.cbBloqueio.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.cbBloqueio.BackColor = System.Drawing.Color.LightGray;
            this.cbBloqueio.BorderColor = System.Drawing.Color.DimGray;
            this.cbBloqueio.BorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid;
            this.cbBloqueio.FormattingEnabled = true;
            this.cbBloqueio.Location = new System.Drawing.Point(149, 19);
            this.cbBloqueio.Name = "cbBloqueio";
            this.cbBloqueio.Size = new System.Drawing.Size(536, 21);
            this.cbBloqueio.TabIndex = 2;
            // 
            // lblEletricidade
            // 
            this.lblEletricidade.BackColor = System.Drawing.Color.LightGray;
            this.lblEletricidade.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblEletricidade.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblEletricidade.Location = new System.Drawing.Point(3, 319);
            this.lblEletricidade.MaxLength = 15;
            this.lblEletricidade.Name = "lblEletricidade";
            this.lblEletricidade.ReadOnly = true;
            this.lblEletricidade.Size = new System.Drawing.Size(147, 21);
            this.lblEletricidade.TabIndex = 75;
            this.lblEletricidade.TabStop = false;
            this.lblEletricidade.Text = "Serviços em Eletricidade";
            // 
            // cbEletricidade
            // 
            this.cbEletricidade.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.cbEletricidade.BackColor = System.Drawing.Color.LightGray;
            this.cbEletricidade.BorderColor = System.Drawing.Color.DimGray;
            this.cbEletricidade.BorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid;
            this.cbEletricidade.Enabled = false;
            this.cbEletricidade.FormattingEnabled = true;
            this.cbEletricidade.Location = new System.Drawing.Point(149, 319);
            this.cbEletricidade.Name = "cbEletricidade";
            this.cbEletricidade.Size = new System.Drawing.Size(536, 21);
            this.cbEletricidade.TabIndex = 14;
            this.cbEletricidade.SelectedIndexChanged += new System.EventHandler(this.cbEletricidade_SelectedIndexChanged);
            // 
            // grbAtendimento
            // 
            this.grbAtendimento.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.grbAtendimento.Controls.Add(this.btnCentroCusto);
            this.grbAtendimento.Controls.Add(this.btnMedicoExaminador);
            this.grbAtendimento.Controls.Add(this.btnExcluirCoordenador);
            this.grbAtendimento.Controls.Add(this.cbEletricidade);
            this.grbAtendimento.Controls.Add(this.btPcmso);
            this.grbAtendimento.Controls.Add(this.lblEletricidade);
            this.grbAtendimento.Controls.Add(this.btCliente);
            this.grbAtendimento.Controls.Add(this.textBloqueio);
            this.grbAtendimento.Controls.Add(this.textCnpjPrestador);
            this.grbAtendimento.Controls.Add(this.textCentroCusto);
            this.grbAtendimento.Controls.Add(this.lblCnpjPrestador);
            this.grbAtendimento.Controls.Add(this.cbBloqueio);
            this.grbAtendimento.Controls.Add(this.btPrestador);
            this.grbAtendimento.Controls.Add(this.lblCentroCusto);
            this.grbAtendimento.Controls.Add(this.textPrestador);
            this.grbAtendimento.Controls.Add(this.lblTipoAtendimento);
            this.grbAtendimento.Controls.Add(this.cbTipoAtendimento);
            this.grbAtendimento.Controls.Add(this.textCodigoPO);
            this.grbAtendimento.Controls.Add(this.lblPrestador);
            this.grbAtendimento.Controls.Add(this.cbPrioridade);
            this.grbAtendimento.Controls.Add(this.lblCodigoPO);
            this.grbAtendimento.Controls.Add(this.btnIncluirMedicoCoordenador);
            this.grbAtendimento.Controls.Add(this.lblDataVencimento);
            this.grbAtendimento.Controls.Add(this.cbAltura);
            this.grbAtendimento.Controls.Add(this.lblPrioridade);
            this.grbAtendimento.Controls.Add(this.lblTrabalhoAltura);
            this.grbAtendimento.Controls.Add(this.cbConfinado);
            this.grbAtendimento.Controls.Add(this.lblCliente);
            this.grbAtendimento.Controls.Add(this.textCliente);
            this.grbAtendimento.Controls.Add(this.lblEspacoConfinado);
            this.grbAtendimento.Controls.Add(this.lblCnpj);
            this.grbAtendimento.Controls.Add(this.textCnpj);
            this.grbAtendimento.Controls.Add(this.lblFuncao);
            this.grbAtendimento.Controls.Add(this.textMedicoExaminador);
            this.grbAtendimento.Controls.Add(this.textFuncao);
            this.grbAtendimento.Controls.Add(this.lblMedicoExaminador);
            this.grbAtendimento.Controls.Add(this.lblPcmso);
            this.grbAtendimento.Controls.Add(this.textPcmso);
            this.grbAtendimento.Controls.Add(this.lblDataValidade);
            this.grbAtendimento.Controls.Add(this.textdataValidadePcmso);
            this.grbAtendimento.Controls.Add(this.textSenha);
            this.grbAtendimento.Controls.Add(this.lblCoordenador);
            this.grbAtendimento.Controls.Add(this.lblSenhaAtendimento);
            this.grbAtendimento.Controls.Add(this.textCoordenador);
            this.grbAtendimento.Controls.Add(this.lblDataAtendimento);
            this.grbAtendimento.Controls.Add(this.dataAso);
            this.grbAtendimento.Controls.Add(this.dataVencimento);
            this.grbAtendimento.Location = new System.Drawing.Point(4, 263);
            this.grbAtendimento.Name = "grbAtendimento";
            this.grbAtendimento.Size = new System.Drawing.Size(706, 436);
            this.grbAtendimento.TabIndex = 77;
            this.grbAtendimento.TabStop = false;
            this.grbAtendimento.Text = "Atendimento";
            // 
            // grbColaborador
            // 
            this.grbColaborador.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.grbColaborador.Controls.Add(this.btFuncionario);
            this.grbColaborador.Controls.Add(this.lblNome);
            this.grbColaborador.Controls.Add(this.textRg);
            this.grbColaborador.Controls.Add(this.textNome);
            this.grbColaborador.Controls.Add(this.lblCpf);
            this.grbColaborador.Controls.Add(this.lblRg);
            this.grbColaborador.Controls.Add(this.lblIdade);
            this.grbColaborador.Controls.Add(this.textIdade);
            this.grbColaborador.Controls.Add(this.textCpf);
            this.grbColaborador.Location = new System.Drawing.Point(3, 3);
            this.grbColaborador.Name = "grbColaborador";
            this.grbColaborador.Size = new System.Drawing.Size(707, 103);
            this.grbColaborador.TabIndex = 78;
            this.grbColaborador.TabStop = false;
            this.grbColaborador.Text = "Dados do Colaborador";
            // 
            // frmAtendimentoIncluir
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.ClientSize = new System.Drawing.Size(784, 561);
            this.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.Name = "frmAtendimentoIncluir";
            this.Text = "INCLUIR ATENDIMENTO";
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.frmAtendimentoIncluir_KeyDown);
            this.spForm.Panel1.ResumeLayout(false);
            this.spForm.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.spForm)).EndInit();
            this.spForm.ResumeLayout(false);
            this.pnlForm.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.banner)).EndInit();
            this.flpAcao.ResumeLayout(false);
            this.grb_ghe.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvGhe)).EndInit();
            this.grb_risco.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvRisco)).EndInit();
            this.grbExamePcmso.ResumeLayout(false);
            this.grbExamePcmso.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvExamePcmso)).EndInit();
            this.grbExameAvulso.ResumeLayout(false);
            this.grbExameAvulso.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvExameAvulso)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.IncluirAtendimentoError)).EndInit();
            this.grbObservação.ResumeLayout(false);
            this.grbObservação.PerformLayout();
            this.gbbAtendimentos.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvAtendimentos)).EndInit();
            this.flpExameAvulso.ResumeLayout(false);
            this.flpGheSetor.ResumeLayout(false);
            this.flpRisco.ResumeLayout(false);
            this.flpExamePcmso.ResumeLayout(false);
            this.grbAtendimento.ResumeLayout(false);
            this.grbAtendimento.PerformLayout();
            this.grbColaborador.ResumeLayout(false);
            this.grbColaborador.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.FlowLayoutPanel flpAcao;
        private System.Windows.Forms.Button btFechar;
        private System.Windows.Forms.TextBox lblRg;
        private System.Windows.Forms.TextBox lblCpf;
        private System.Windows.Forms.TextBox textNome;
        private System.Windows.Forms.TextBox textRg;
        private System.Windows.Forms.MaskedTextBox textCpf;
        private System.Windows.Forms.TextBox lblNome;
        private System.Windows.Forms.GroupBox grb_ghe;
        private System.Windows.Forms.GroupBox grb_risco;
        private System.Windows.Forms.DataGridView dgvRisco;
        private System.Windows.Forms.GroupBox grbExamePcmso;
        private System.Windows.Forms.DataGridView dgvExamePcmso;
        private System.Windows.Forms.CheckBox chk_examesPCMSO;
        private System.Windows.Forms.CheckBox chkExamesAvulso;
        private System.Windows.Forms.Button btn_incluirExames;
        private System.Windows.Forms.GroupBox grbExameAvulso;
        private System.Windows.Forms.DataGridView dgvExameAvulso;
        private System.Windows.Forms.Button btn_grava;
        private System.Windows.Forms.Button btFuncionario;
        private System.Windows.Forms.Button btPrestador;
        private System.Windows.Forms.TextBox textPrestador;
        private System.Windows.Forms.TextBox textIdade;
        private System.Windows.Forms.TextBox lblIdade;
        private System.Windows.Forms.TextBox lblPrestador;
        private System.Windows.Forms.MaskedTextBox textCnpjPrestador;
        private System.Windows.Forms.TextBox lblCnpjPrestador;
        private System.Windows.Forms.ErrorProvider IncluirAtendimentoError;
        private System.Windows.Forms.GroupBox grbObservação;
        private System.Windows.Forms.TextBox textObservacao;
        private System.Windows.Forms.GroupBox gbbAtendimentos;
        private System.Windows.Forms.DataGridView dgvAtendimentos;
        private System.Windows.Forms.DataGridView dgvGhe;
        private System.Windows.Forms.ToolTip tpAtendimento;
        private System.Windows.Forms.Button btnCentroCusto;
        private System.Windows.Forms.TextBox textCentroCusto;
        private System.Windows.Forms.TextBox lblCentroCusto;
        private System.Windows.Forms.Button btnMedicoExaminador;
        public System.Windows.Forms.TextBox textMedicoExaminador;
        private System.Windows.Forms.TextBox lblMedicoExaminador;
        private System.Windows.Forms.FlowLayoutPanel flpExameAvulso;
        private System.Windows.Forms.Button btnAplicarDataTranscritoPcmso;
        private System.Windows.Forms.Button btnModificarDataTranscritoExtra;
        private System.Windows.Forms.FlowLayoutPanel flpExamePcmso;
        private System.Windows.Forms.FlowLayoutPanel flpGheSetor;
        private System.Windows.Forms.Button btnIncluirGhe;
        private System.Windows.Forms.Button btnExcluirGhe;
        private System.Windows.Forms.FlowLayoutPanel flpRisco;
        private System.Windows.Forms.Button btnIncluirRisco;
        private System.Windows.Forms.Button btnExcluirRisco;
        private ComboBoxWithBorder cbBloqueio;
        private System.Windows.Forms.TextBox textBloqueio;
        private System.Windows.Forms.Button btnExcluirCoordenador;
        private System.Windows.Forms.Button btnIncluirMedicoCoordenador;
        private ComboBoxWithBorder cbPrioridade;
        private System.Windows.Forms.TextBox lblPrioridade;
        private System.Windows.Forms.TextBox textCodigoPO;
        private System.Windows.Forms.TextBox lblCodigoPO;
        private System.Windows.Forms.TextBox lblDataVencimento;
        private ComboBoxWithBorder cbAltura;
        private ComboBoxWithBorder cbConfinado;
        private ComboBoxWithBorder cbTipoAtendimento;
        private System.Windows.Forms.TextBox lblTrabalhoAltura;
        private System.Windows.Forms.TextBox lblEspacoConfinado;
        private System.Windows.Forms.TextBox textdataValidadePcmso;
        public System.Windows.Forms.TextBox textSenha;
        private System.Windows.Forms.TextBox lblSenhaAtendimento;
        private System.Windows.Forms.TextBox textCoordenador;
        private System.Windows.Forms.TextBox lblCoordenador;
        private System.Windows.Forms.TextBox lblDataValidade;
        private System.Windows.Forms.MaskedTextBox textPcmso;
        private System.Windows.Forms.Button btPcmso;
        private System.Windows.Forms.TextBox lblPcmso;
        private System.Windows.Forms.TextBox textFuncao;
        private System.Windows.Forms.TextBox lblFuncao;
        private System.Windows.Forms.TextBox textCliente;
        private System.Windows.Forms.MaskedTextBox textCnpj;
        private System.Windows.Forms.TextBox lblCnpj;
        private System.Windows.Forms.Button btCliente;
        private System.Windows.Forms.TextBox lblDataAtendimento;
        private System.Windows.Forms.TextBox lblCliente;
        private System.Windows.Forms.TextBox lblTipoAtendimento;
        private System.Windows.Forms.DateTimePicker dataVencimento;
        private System.Windows.Forms.DateTimePicker dataAso;
        private ComboBoxWithBorder cbEletricidade;
        private System.Windows.Forms.TextBox lblEletricidade;
        private System.Windows.Forms.GroupBox grbAtendimento;
        private System.Windows.Forms.GroupBox grbColaborador;
    }
}
