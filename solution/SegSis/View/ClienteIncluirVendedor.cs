﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using SegSis.Facade;
using SegSis.Entidade;
using SegSis.Excecao;
using SegSis.View.ViewHelper;
using System.Text.RegularExpressions;
using SegSis.View.Resources;

namespace SegSis.View
{
    public partial class frm_clienteVendedorSelecionar_Incluir : BaseFormConsulta
    {
        frm_cliente_inclui formClienteIncluir;

        private Vendedor vendedor = null;

        public Vendedor getVendedor()
        {
            return this.vendedor;
        }
        
        private static String Msg01 = "Selecione uma linha";
        private static String msg02 = "Você não selecionou o vendedor";
        
        public frm_clienteVendedorSelecionar_Incluir(frm_cliente_inclui formClienteIncluir)
        {
            InitializeComponent();
            this.formClienteIncluir = formClienteIncluir;
            validaPermissoes();
        }

        public void setVendedor(Vendedor vendedor)
        {
            this.vendedor = vendedor;
        }

        private void validaPermissoes()
        {
            PermissionamentoFacade permissionamentoFacade = PermissionamentoFacade.getInstance();

            btn_incluir.Enabled = permissionamentoFacade.hasPermission("VENDEDOR", "INCLUIR");
        }

        private void btn_buscar_Click(object sender, EventArgs e)
        {
            try
            {
                this.grd_vendedor.Columns.Clear();
                montaDataGrid();
                text_nome.Focus();
            }
            catch (Exception Ex)
            {
                MessageBox.Show(Ex.Message);
            }
        }

        public void montaDataGrid()
        {
            VendedorFacade vendedorFacade = VendedorFacade.getInstance();

            Vendedor vendedor = new Vendedor();
            DataSet ds = null;

            vendedor.setNome(text_nome.Text);

            if (formClienteIncluir.getCliente() != null)
            {
                ds = vendedorFacade.findVendedorNotInCliente(vendedor, formClienteIncluir.getCliente());
            }
            else
            {
                ds = vendedorFacade.findVendedorByName(vendedor);
            }

            grd_vendedor.DataSource = ds.Tables["Vendedores"].DefaultView;
            
            grd_vendedor.Columns[0].HeaderText = "ID";
            grd_vendedor.Columns[1].HeaderText = "Nome";

            grd_vendedor.Columns[0].Visible = false;
            
        }

        private void btn_fechar_Click(object sender, EventArgs e)
        {
            this.Close();

        }

        private void btn_ok_Click(object sender, EventArgs e)
        {
            try
            {
                this.Cursor = Cursors.WaitCursor;

                VendedorFacade vendedorFacade = VendedorFacade.getInstance();
               

                if (grd_vendedor.CurrentRow != null)
                {

                    Int64 id = (Int64)grd_vendedor.CurrentRow.Cells[0].Value;

                    vendedor = vendedorFacade.findVendedorById(id);
                    this.Close();

                }
                else
                {
                    MessageBox.Show(Msg01);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }

        private void btn_incluir_Click(object sender, EventArgs e)
        {
            try
            {
                this.Cursor = Cursors.WaitCursor;

                frm_vendedor_incluir formVendedorIncluir = new frm_vendedor_incluir();
                formVendedorIncluir.ShowDialog();

                if (formVendedorIncluir.getVendedor() != null)
                {
                    vendedor = formVendedorIncluir.getVendedor();
                    text_nome.Text = vendedor.getNome();
                    btn_buscar.PerformClick();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }

        }

    }
}
