﻿namespace SWS.View
{
    partial class frm_ppraReplicarOld
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frm_ppraReplicarOld));
            this.pnl_revisao = new System.Windows.Forms.Panel();
            this.grb_dados = new System.Windows.Forms.GroupBox();
            this.text_comentario = new System.Windows.Forms.TextBox();
            this.lbl_comentario = new System.Windows.Forms.Label();
            this.grb_paginacao = new System.Windows.Forms.GroupBox();
            this.bt_ok = new System.Windows.Forms.Button();
            this.pnl_revisao.SuspendLayout();
            this.grb_dados.SuspendLayout();
            this.grb_paginacao.SuspendLayout();
            this.SuspendLayout();
            // 
            // pnl_revisao
            // 
            this.pnl_revisao.BackColor = System.Drawing.Color.White;
            this.pnl_revisao.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnl_revisao.Controls.Add(this.grb_dados);
            this.pnl_revisao.Controls.Add(this.grb_paginacao);
            this.pnl_revisao.Location = new System.Drawing.Point(4, 5);
            this.pnl_revisao.Name = "pnl_revisao";
            this.pnl_revisao.Size = new System.Drawing.Size(577, 152);
            this.pnl_revisao.TabIndex = 0;
            // 
            // grb_dados
            // 
            this.grb_dados.Controls.Add(this.text_comentario);
            this.grb_dados.Controls.Add(this.lbl_comentario);
            this.grb_dados.Location = new System.Drawing.Point(8, 7);
            this.grb_dados.Name = "grb_dados";
            this.grb_dados.Size = new System.Drawing.Size(559, 80);
            this.grb_dados.TabIndex = 0;
            this.grb_dados.TabStop = false;
            // 
            // text_comentario
            // 
            this.text_comentario.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.text_comentario.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.text_comentario.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.text_comentario.ForeColor = System.Drawing.Color.Black;
            this.text_comentario.Location = new System.Drawing.Point(10, 27);
            this.text_comentario.MaxLength = 255;
            this.text_comentario.Multiline = true;
            this.text_comentario.Name = "text_comentario";
            this.text_comentario.Size = new System.Drawing.Size(543, 36);
            this.text_comentario.TabIndex = 1;
            // 
            // lbl_comentario
            // 
            this.lbl_comentario.AutoSize = true;
            this.lbl_comentario.Location = new System.Drawing.Point(7, 10);
            this.lbl_comentario.Name = "lbl_comentario";
            this.lbl_comentario.Size = new System.Drawing.Size(60, 13);
            this.lbl_comentario.TabIndex = 0;
            this.lbl_comentario.Text = "Comentário";
            // 
            // grb_paginacao
            // 
            this.grb_paginacao.BackColor = System.Drawing.Color.White;
            this.grb_paginacao.Controls.Add(this.bt_ok);
            this.grb_paginacao.Location = new System.Drawing.Point(7, 93);
            this.grb_paginacao.Name = "grb_paginacao";
            this.grb_paginacao.Size = new System.Drawing.Size(560, 51);
            this.grb_paginacao.TabIndex = 0;
            this.grb_paginacao.TabStop = false;
            // 
            // bt_ok
            // 
            this.bt_ok.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.bt_ok.Location = new System.Drawing.Point(11, 19);
            this.bt_ok.Name = "bt_ok";
            this.bt_ok.Size = new System.Drawing.Size(71, 22);
            this.bt_ok.TabIndex = 2;
            this.bt_ok.Text = "&OK";
            this.bt_ok.UseVisualStyleBackColor = true;
            this.bt_ok.Click += new System.EventHandler(this.bt_ok_Click);
            // 
            // frm_ppraReplicar
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(584, 162);
            this.ControlBox = false;
            this.Controls.Add(this.pnl_revisao);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "frm_ppraReplicar";
            this.Text = "REVISAR PPRA";
            this.Load += new System.EventHandler(this.frm_ppraReplicar_Load);
            this.pnl_revisao.ResumeLayout(false);
            this.grb_dados.ResumeLayout(false);
            this.grb_dados.PerformLayout();
            this.grb_paginacao.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel pnl_revisao;
        private System.Windows.Forms.GroupBox grb_dados;
        private System.Windows.Forms.TextBox text_comentario;
        private System.Windows.Forms.Label lbl_comentario;
        private System.Windows.Forms.GroupBox grb_paginacao;
        private System.Windows.Forms.Button bt_ok;
    }
}