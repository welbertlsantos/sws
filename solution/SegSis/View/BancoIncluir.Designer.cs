﻿namespace SWS.View
{
    partial class frmBancoIncluir
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.flpAcao = new System.Windows.Forms.FlowLayoutPanel();
            this.textCodigo = new System.Windows.Forms.TextBox();
            this.textNome = new System.Windows.Forms.TextBox();
            this.lblBanco = new System.Windows.Forms.TextBox();
            this.lblCodigo = new System.Windows.Forms.TextBox();
            this.lblCaixa = new System.Windows.Forms.TextBox();
            this.lblBoleto = new System.Windows.Forms.TextBox();
            this.cbCaixa = new SWS.ComboBoxWithBorder();
            this.cbBoleto = new SWS.ComboBoxWithBorder();
            this.btnFechar = new System.Windows.Forms.Button();
            this.btnGravar = new System.Windows.Forms.Button();
            this.btnLimpar = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.spForm)).BeginInit();
            this.spForm.Panel1.SuspendLayout();
            this.spForm.Panel2.SuspendLayout();
            this.spForm.SuspendLayout();
            this.pnlForm.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.banner)).BeginInit();
            this.flpAcao.SuspendLayout();
            this.SuspendLayout();
            // 
            // spForm
            // 
            // 
            // spForm.Panel1
            // 
            this.spForm.Panel1.Controls.Add(this.flpAcao);
            // 
            // pnlForm
            // 
            this.pnlForm.Controls.Add(this.cbBoleto);
            this.pnlForm.Controls.Add(this.cbCaixa);
            this.pnlForm.Controls.Add(this.lblBoleto);
            this.pnlForm.Controls.Add(this.lblCaixa);
            this.pnlForm.Controls.Add(this.lblCodigo);
            this.pnlForm.Controls.Add(this.lblBanco);
            this.pnlForm.Controls.Add(this.textCodigo);
            this.pnlForm.Controls.Add(this.textNome);
            // 
            // flpAcao
            // 
            this.flpAcao.Controls.Add(this.btnGravar);
            this.flpAcao.Controls.Add(this.btnLimpar);
            this.flpAcao.Controls.Add(this.btnFechar);
            this.flpAcao.Dock = System.Windows.Forms.DockStyle.Fill;
            this.flpAcao.Location = new System.Drawing.Point(0, 0);
            this.flpAcao.Name = "flpAcao";
            this.flpAcao.Size = new System.Drawing.Size(784, 35);
            this.flpAcao.TabIndex = 0;
            // 
            // textCodigo
            // 
            this.textCodigo.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textCodigo.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.textCodigo.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textCodigo.Location = new System.Drawing.Point(156, 38);
            this.textCodigo.MaxLength = 3;
            this.textCodigo.Name = "textCodigo";
            this.textCodigo.Size = new System.Drawing.Size(583, 21);
            this.textCodigo.TabIndex = 2;
            this.textCodigo.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.textCodigo_KeyPress);
            // 
            // textNome
            // 
            this.textNome.AcceptsReturn = true;
            this.textNome.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textNome.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.textNome.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textNome.Location = new System.Drawing.Point(156, 18);
            this.textNome.MaxLength = 100;
            this.textNome.Name = "textNome";
            this.textNome.Size = new System.Drawing.Size(583, 21);
            this.textNome.TabIndex = 1;
            // 
            // lblBanco
            // 
            this.lblBanco.AcceptsReturn = true;
            this.lblBanco.BackColor = System.Drawing.Color.LightGray;
            this.lblBanco.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblBanco.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblBanco.Location = new System.Drawing.Point(12, 18);
            this.lblBanco.MaxLength = 100;
            this.lblBanco.Name = "lblBanco";
            this.lblBanco.ReadOnly = true;
            this.lblBanco.Size = new System.Drawing.Size(145, 21);
            this.lblBanco.TabIndex = 5;
            this.lblBanco.TabStop = false;
            this.lblBanco.Text = "Nome do Banco";
            // 
            // lblCodigo
            // 
            this.lblCodigo.AcceptsReturn = true;
            this.lblCodigo.BackColor = System.Drawing.Color.LightGray;
            this.lblCodigo.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblCodigo.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCodigo.Location = new System.Drawing.Point(12, 38);
            this.lblCodigo.MaxLength = 100;
            this.lblCodigo.Name = "lblCodigo";
            this.lblCodigo.ReadOnly = true;
            this.lblCodigo.Size = new System.Drawing.Size(145, 21);
            this.lblCodigo.TabIndex = 6;
            this.lblCodigo.TabStop = false;
            this.lblCodigo.Text = "Código bancário";
            // 
            // lblCaixa
            // 
            this.lblCaixa.AcceptsReturn = true;
            this.lblCaixa.BackColor = System.Drawing.Color.LightGray;
            this.lblCaixa.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblCaixa.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCaixa.Location = new System.Drawing.Point(12, 58);
            this.lblCaixa.MaxLength = 100;
            this.lblCaixa.Name = "lblCaixa";
            this.lblCaixa.ReadOnly = true;
            this.lblCaixa.Size = new System.Drawing.Size(145, 21);
            this.lblCaixa.TabIndex = 7;
            this.lblCaixa.TabStop = false;
            this.lblCaixa.Text = "É caixa?";
            // 
            // lblBoleto
            // 
            this.lblBoleto.AcceptsReturn = true;
            this.lblBoleto.BackColor = System.Drawing.Color.LightGray;
            this.lblBoleto.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblBoleto.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblBoleto.Location = new System.Drawing.Point(12, 78);
            this.lblBoleto.MaxLength = 100;
            this.lblBoleto.Name = "lblBoleto";
            this.lblBoleto.ReadOnly = true;
            this.lblBoleto.Size = new System.Drawing.Size(145, 21);
            this.lblBoleto.TabIndex = 8;
            this.lblBoleto.TabStop = false;
            this.lblBoleto.Tag = "";
            this.lblBoleto.Text = "Emite boleto?";
            // 
            // cbCaixa
            // 
            this.cbCaixa.BackColor = System.Drawing.Color.LightGray;
            this.cbCaixa.BorderColor = System.Drawing.Color.DimGray;
            this.cbCaixa.BorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid;
            this.cbCaixa.FormattingEnabled = true;
            this.cbCaixa.Location = new System.Drawing.Point(156, 58);
            this.cbCaixa.Name = "cbCaixa";
            this.cbCaixa.Size = new System.Drawing.Size(583, 21);
            this.cbCaixa.TabIndex = 3;
            this.cbCaixa.SelectedValueChanged += new System.EventHandler(this.cbCaixa_SelectedValueChanged);
            // 
            // cbBoleto
            // 
            this.cbBoleto.BackColor = System.Drawing.Color.LightGray;
            this.cbBoleto.BorderColor = System.Drawing.Color.DimGray;
            this.cbBoleto.BorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid;
            this.cbBoleto.FormattingEnabled = true;
            this.cbBoleto.Location = new System.Drawing.Point(156, 78);
            this.cbBoleto.Name = "cbBoleto";
            this.cbBoleto.Size = new System.Drawing.Size(583, 21);
            this.cbBoleto.TabIndex = 4;
            this.cbBoleto.SelectedValueChanged += new System.EventHandler(this.cbBoleto_SelectedValueChanged);
            // 
            // btnFechar
            // 
            this.btnFechar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnFechar.Image = global::SWS.Properties.Resources.close;
            this.btnFechar.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnFechar.Location = new System.Drawing.Point(165, 3);
            this.btnFechar.Name = "btnFechar";
            this.btnFechar.Size = new System.Drawing.Size(75, 23);
            this.btnFechar.TabIndex = 6;
            this.btnFechar.Text = "&Fechar";
            this.btnFechar.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnFechar.UseVisualStyleBackColor = true;
            this.btnFechar.Click += new System.EventHandler(this.btnFechar_Click);
            // 
            // btnGravar
            // 
            this.btnGravar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnGravar.Image = global::SWS.Properties.Resources.fechar_ico;
            this.btnGravar.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnGravar.Location = new System.Drawing.Point(3, 3);
            this.btnGravar.Name = "btnGravar";
            this.btnGravar.Size = new System.Drawing.Size(75, 23);
            this.btnGravar.TabIndex = 5;
            this.btnGravar.Text = "&Gravar";
            this.btnGravar.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnGravar.UseVisualStyleBackColor = true;
            this.btnGravar.Click += new System.EventHandler(this.btnGravar_Click);
            // 
            // btnLimpar
            // 
            this.btnLimpar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnLimpar.Image = global::SWS.Properties.Resources.vassoura;
            this.btnLimpar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnLimpar.Location = new System.Drawing.Point(84, 3);
            this.btnLimpar.Name = "btnLimpar";
            this.btnLimpar.Size = new System.Drawing.Size(75, 23);
            this.btnLimpar.TabIndex = 18;
            this.btnLimpar.TabStop = false;
            this.btnLimpar.Text = "&Limpar";
            this.btnLimpar.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnLimpar.UseVisualStyleBackColor = true;
            this.btnLimpar.Click += new System.EventHandler(this.btnLimpar_Click);
            // 
            // frmBancoIncluir
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(784, 561);
            this.Name = "frmBancoIncluir";
            this.Text = "INCLUIR BANCO";
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.frmBancoIncluir_KeyDown);
            this.spForm.Panel1.ResumeLayout(false);
            this.spForm.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.spForm)).EndInit();
            this.spForm.ResumeLayout(false);
            this.pnlForm.ResumeLayout(false);
            this.pnlForm.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.banner)).EndInit();
            this.flpAcao.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        protected System.Windows.Forms.TextBox lblBoleto;
        protected System.Windows.Forms.TextBox lblCaixa;
        protected System.Windows.Forms.TextBox lblCodigo;
        protected System.Windows.Forms.TextBox lblBanco;
        protected System.Windows.Forms.TextBox textCodigo;
        protected System.Windows.Forms.TextBox textNome;
        protected System.Windows.Forms.FlowLayoutPanel flpAcao;
        protected ComboBoxWithBorder cbBoleto;
        protected ComboBoxWithBorder cbCaixa;
        protected System.Windows.Forms.Button btnGravar;
        protected System.Windows.Forms.Button btnLimpar;
        protected System.Windows.Forms.Button btnFechar;

    }
}