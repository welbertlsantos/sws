﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using SWS.Facade;
using SWS.Entidade;
using SWS.Excecao;
using SWS.View.ViewHelper;
using System.Text.RegularExpressions;
using SWS.View.Resources;
using System.Security;
using System.IO;
using SWS.Helper;
using SWS.ViewHelper;
using SWS.Resources;

namespace SWS.View
{
    public partial class frmClienteAlterar : frmClientesIncluir
    {
        
        protected VendedorCliente vendedorAtivo = null;

        public frmClienteAlterar(Cliente cliente) :base()
        {
            InitializeComponent();
            this.cliente = cliente;

            /* montando informações na tela */

            if (cliente.Unidade)
                cbTipoCliente.Text = "Unidade";
            else if (cliente.Credenciada)
                cbTipoCliente.Text = "Credenciada";
            else if (!cliente.Unidade && !cliente.Credenciada && !cliente.Fisica)
                cbTipoCliente.Text = "Pessoa Jurídica/MEI";
            else
                cbTipoCliente.Text = "Pessoa Física/CEI";

            vendedores = cliente.VendedorCliente;
            vendedorAtivo = vendedores.Find(x => String.Equals(x.Situacao, ApplicationConstants.ATIVO));
            if (vendedorAtivo != null)
                textVendedor.Text = vendedorAtivo.Vendedor.Nome;

            if (cliente.Matriz != null)
            {
                matriz = cliente.Matriz;
                textMatriz.Text = matriz.RazaoSocial;
                btCredenciada.Enabled = false;
            }

            if (cliente.CredenciadaCliente != null)
            {
                credenciada = cliente.CredenciadaCliente;
                textCredenciada.Text = credenciada.RazaoSocial;
                btMatriz.Enabled = false;
            }

            textRazaoSocial.Text = cliente.RazaoSocial;
            textCNPJ.Text = cliente.Cnpj;
            textFantasia.Text = cliente.Fantasia;
            textInscricao.Text = cliente.Inscricao;
            textEmail.Text = cliente.Email;
            textSite.Text = cliente.Site;
            textCodigoCnes.Text = cliente.CodigoCnes;

            ramo = cliente.Ramo;
            textRamo.Text = cliente.Ramo != null ? cliente.Ramo.Descricao : String.Empty;
            textJornada.Text = cliente.Jornada;
            textEscopo.Text = cliente.Escopo;

            /* salvando páginas de endereco */

            tpEnderecoCopy = new TabPage[] { tpComercial, tpCobranca };
            /* populando informação de endereco. Padrão para todos os clientes */

            /* verificando o tipo de cliente e o tipo de informação exibida em tela */

            if (cliente.Unidade)
            {
                /* cliente é unidade */
                lblRazaoSocial.Text = "Razão Social";
                lblCNPJ.Text = "CNPJ";
                lblInscricao.Text = "Inscrição Estadual / Municipal";
                textCNPJ.Mask = "99,999,999/9999-99";


                chanceControleConfiguracao(false);
                changeControlesTela(false);
                tpEndereco.TabPages.Clear();
                tpEndereco.TabPages.Add(tpEnderecoCopy[0]);

                textLogradouroComercial.Text = cliente.Endereco;
                textNumeroComercial.Text = cliente.Numero;
                textComplementoComercial.Text = cliente.Complemento;
                textBairroComercial.Text = cliente.Bairro;
                textCEPComercial.Text = cliente.Cep;
                ComboHelper.startComboUfSave(cbUFComercial);
                cbUFComercial.SelectedValue = cliente.Uf;
                cidadeComercial = cliente.CidadeIbge;
                textCidadeComercial.Text = cliente.CidadeIbge.Nome;
                textTelefoneComercial.Text = cliente.Telefone1;
                textFaxComercial.Text = cliente.Telefone2;

            }
            else if (cliente.Cnpj.Length != 11)
            {
                /* cliente credenciada ou padrão. Seguem a mesma lógica de tela. */
                lblRazaoSocial.Text = "Razão Social";
                lblCNPJ.Text = "CNPJ";
                lblInscricao.Text = "Inscrição Estadual / Municipal";
                textCNPJ.Mask = "99,999,999/9999-99";

                textLogradouroComercial.Text = cliente.Endereco;
                textNumeroComercial.Text = cliente.Numero;
                textComplementoComercial.Text = cliente.Complemento;
                textBairroComercial.Text = cliente.Bairro;
                textCEPComercial.Text = cliente.Cep;
                ComboHelper.startComboUfSave(cbUFComercial);
                cbUFComercial.SelectedValue = cliente.Uf;
                cidadeComercial = cliente.CidadeIbge;
                textCidadeComercial.Text = cliente.CidadeIbge.Nome;
                textTelefoneComercial.Text = cliente.Telefone1;
                textFaxComercial.Text = cliente.Telefone2;

                textLogradouroCobranca.Text = cliente.EnderecoCob;
                textNumeroCobranca.Text = cliente.NumeroCob;
                textComplementoCobranca.Text = cliente.ComplementoCob;
                textBairroCobranca.Text = cliente.BairroCob;
                textCEPCobranca.Text = cliente.CepCob;
                ComboHelper.startComboUfSave(cbUFCobranca);
                cbUFCobranca.SelectedValue = cliente.UfCob;
                cidadeCobranca = cliente.CidadeIbgeCobranca;
                textCidadeCobranca.Text = cliente.CidadeIbgeCobranca.Nome;
                textTelefoneCobranca.Text = cliente.TelefoneCob;
                textFaxCobranca.Text = cliente.Telefone2Cob;

                /* se o cliente é uma empresa padrão então ele não poder ter matriz e nem credenciada */

                if (string.Equals(((SelectItem)cbTipoCliente.SelectedItem).Valor, ApplicationConstants.PADRAO))
                {
                    btCredenciada.Enabled = false;
                    btMatriz.Enabled = false;
                }

                /* montando dados do cnae */
                cnaes = cliente.ClienteCnae;
                montaGridCnae();

                /* montando dados da logo do cliente */

                ClienteFacade clienteFacade = ClienteFacade.getInstance();
                clienteArquivo = clienteFacade.findClienteArquivoByCliente(cliente);
                if (clienteArquivo != null)
                    pbLogo.Image = FileHelper.byteArrayToImage(clienteArquivo.Arquivo.Conteudo);

                /* montando funcoes do cliente */

                funcoes = cliente.Funcoes;
                montaGridFuncoes();

                /* montando informações sobre o contato */

                contatos = clienteFacade.findByCliente(cliente);
                montaGridContato();

                /* montando bloco de condições */

                ComboHelper.Boolean(cbClienteVIP, true);
                ComboHelper.Boolean(cbContrato, true);
                ComboHelper.Boolean(cbParticular, true);
                ComboHelper.Boolean(cbPrestador, true);
                ComboHelper.Boolean(cbValorLiquido, true);
                ComboHelper.Boolean(cbBloqueado, false);
                ComboHelper.Boolean(cbCentroCusto, false);
                ComboHelper.Boolean(cbCredenciadora, false);
                ComboHelper.Boolean(cbUsaPo, false);
                ComboHelper.Boolean(cbSimplesNacional, false);

                cbClienteVIP.SelectedValue = cliente.Vip == true ? "true" : "false";
                cbContrato.SelectedValue = cliente.UsaContrato == true ? "true" : "false";
                cbParticular.SelectedValue = cliente.Particular == true ? "true" : "false";
                cbPrestador.SelectedValue = cliente.Prestador == true ? "true" : "false";
                cbValorLiquido.SelectedValue = cliente.GeraCobrancaValorLiquido == true ? "true" : "false";
                cbBloqueado.SelectedValue = cliente.Bloqueado == true ? "true" : "false";
                cbCentroCusto.SelectedValue = cliente.UsaCentroCusto == true ? "true" : "false";
                cbCredenciadora.SelectedValue = cliente.Credenciadora == true ? "true" : "false";
                cbUsaPo.SelectedValue = cliente.UsaPo ? "true" : "false";
                cbSimplesNacional.SelectedValue = cliente.SimplesNacional ? "true" : "false";

                if (cliente.UsaCentroCusto)
                {
                    CentroCustoCliente = cliente.CentroCusto;
                    montaGridCentroCusto();
                }

            }
            else
            {
                /* cliente pessoa física */
                /* montando dados da tela para cliente pessoa física */

                chanceControleConfiguracao(true);
                changeControlesTela(true);
                
                tpEndereco.TabPages.Clear();
                tpEndereco.TabPages.Add(tpEnderecoCopy[0]);
                tpEndereco.TabPages.Add(tpEnderecoCopy[1]);

                textLogradouroComercial.Text = cliente.Endereco;
                textNumeroComercial.Text = cliente.Numero;
                textComplementoComercial.Text = cliente.Complemento;
                textBairroComercial.Text = cliente.Bairro;
                textCEPComercial.Text = cliente.Cep;
                ComboHelper.startComboUfSave(cbUFComercial);
                cbUFComercial.SelectedValue = cliente.Uf;
                cidadeComercial = cliente.CidadeIbge;
                textCidadeComercial.Text = cliente.CidadeIbge.Nome;
                textTelefoneComercial.Text = cliente.Telefone1;
                textFaxComercial.Text = cliente.Telefone2;

                textLogradouroCobranca.Text = cliente.EnderecoCob;
                textNumeroCobranca.Text = cliente.NumeroCob;
                textComplementoCobranca.Text = cliente.ComplementoCob;
                textBairroCobranca.Text = cliente.BairroCob;
                textCEPCobranca.Text = cliente.CepCob;
                ComboHelper.startComboUfSave(cbUFCobranca);
                cbUFCobranca.SelectedValue = cliente.UfCob;
                cidadeCobranca = cliente.CidadeIbgeCobranca;
                textCidadeCobranca.Text = cliente.CidadeIbgeCobranca != null ? cliente.CidadeIbgeCobranca.Nome : string.Empty ;
                textTelefoneCobranca.Text = cliente.TelefoneCob;
                textFaxCobranca.Text = cliente.Telefone2Cob;

                lblRazaoSocial.Text = "Nome";
                lblCNPJ.Text = "CPF";
                lblInscricao.Text = "RG";
                textCNPJ.Mask = "999,999,999-99";

                grbCnae.Enabled = true;
                btIncluirCnae.Enabled = true;
                btExcluirCnae.Enabled = true;

                /* montando dados da logo do cliente */

                ClienteFacade clienteFacade = ClienteFacade.getInstance();
                clienteArquivo = clienteFacade.findClienteArquivoByCliente(cliente);
                if (clienteArquivo != null)
                    pbLogo.Image = FileHelper.byteArrayToImage(clienteArquivo.Arquivo.Conteudo);

                /* montando gride de cnae */
                cnaes = cliente.ClienteCnae;
                montaGridCnae();

                /* montando funcoes do cliente */

                funcoes = cliente.Funcoes;
                montaGridFuncoes();

                /* montando informações sobre o contato */

                contatos = clienteFacade.findByCliente(cliente);
                montaGridContato();

                /* montando bloco de condições */

                ComboHelper.Boolean(cbClienteVIP, true);
                ComboHelper.Boolean(cbContrato, true);
                ComboHelper.Boolean(cbParticular, true);
                cbPrestador.Enabled = false;
                textCodigoCnes.Enabled = false;
                ComboHelper.Boolean(cbValorLiquido, true);
                ComboHelper.Boolean(cbBloqueado, false);
                cbCentroCusto.Enabled = false;
                cbCredenciadora.Enabled = false;

                cbClienteVIP.SelectedValue = cliente.Vip == true ? "true" : "false";
                cbContrato.SelectedValue = cliente.UsaContrato == true ? "true" : "false";
                cbParticular.SelectedValue = cliente.Particular == true ? "true" : "false";
                cbValorLiquido.SelectedValue = cliente.GeraCobrancaValorLiquido == true ? "true" : "false";
                cbBloqueado.SelectedValue = cliente.Bloqueado == true ? "true" : "false";
            }
        }

        protected override void btGravar_Click(object sender, EventArgs e)
        {
            try
            {
                if (validaCampos())
                {
                    ClienteFacade clienteFacade = ClienteFacade.getInstance();

                    cliente = new Cliente(cliente.Id, textRazaoSocial.Text, textFantasia.Text, string.Equals(((SelectItem)cbTipoCliente.SelectedItem).Valor, ApplicationConstants.UNIDADE) ? matriz.Cnpj : textCNPJ.Text, textInscricao.Text, textLogradouroComercial.Text, textNumeroComercial.Text, textComplementoComercial.Text, textBairroComercial.Text, textCEPComercial.Text, ((SelectItem)cbUFComercial.SelectedItem).Valor.ToString(), textTelefoneComercial.Text, textFaxComercial.Text, textEmail.Text, textSite.Text, cliente.DataCadastro, String.Empty, String.Empty, String.Empty, textEscopo.Text, textJornada.Text, ApplicationConstants.ATIVO, 0, 0, textLogradouroCobranca.Text, textNumeroCobranca.Text, textComplementoCobranca.Text, textBairroCobranca.Text, textCEPCobranca.Text, ((SelectItem)cbUFCobranca.SelectedItem) == null ? string.Empty : ((SelectItem)cbUFCobranca.SelectedItem).Valor.ToString(), textTelefoneCobranca.Text, null, textFaxCobranca.Text, Convert.ToBoolean(((SelectItem)cbClienteVIP.SelectedItem).Valor), String.Empty, Convert.ToBoolean(((SelectItem)cbContrato.SelectedItem).Valor), matriz, credenciada, Convert.ToBoolean(((SelectItem)this.cbParticular.SelectedItem).Valor), matriz != null ? true : false, credenciada != null ? true : false, Convert.ToBoolean(((SelectItem)this.cbPrestador.SelectedItem).Valor), cidadeComercial, cidadeCobranca, true, Convert.ToBoolean(((SelectItem)this.cbValorLiquido.SelectedItem).Valor), 0, textCodigoCnes.Text, Convert.ToBoolean(((SelectItem)cbCentroCusto.SelectedItem).Valor), Convert.ToBoolean(((SelectItem)cbBloqueado.SelectedItem).Valor), Convert.ToBoolean(((SelectItem)cbCredenciadora.SelectedItem).Valor), string.Equals(((SelectItem)cbTipoCliente.SelectedItem).Valor, ApplicationConstants.FISICA) ? true : false, Convert.ToBoolean(((SelectItem)cbUsaPo.SelectedItem).Valor), Convert.ToBoolean(((SelectItem)cbSimplesNacional.SelectedItem).Valor));
                    
                    /* incluindo o ramo de atividade do cliente */
                    cliente.Ramo = ramo;

                    cliente = clienteFacade.clienteAlterar(cliente);

                    MessageBox.Show("Cliente alterado com sucesso.", "Confirmaçao", MessageBoxButtons.OK, MessageBoxIcon.Information);

                    this.Close();
                }


            }
            catch (Exception Ex)
            {
                MessageBox.Show(Ex.Message, "Atençao", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }

        }

        protected override void btVendedor_Click(object sender, EventArgs e)
        {
            try
            {
                if (MessageBox.Show("Deseja alterar o vendedor do cliente?", "Confirmação", MessageBoxButtons.YesNo) == DialogResult.Yes)
                /* alterando o vendedor do cliente para o novo vendedor */
                {
                    frmVendedorBuscar selecionarVendedor = new frmVendedorBuscar();
                    selecionarVendedor.ShowDialog();

                    if (selecionarVendedor.Vendedor != null)
                    {
                        VendedorFacade vendedorFacede = VendedorFacade.getInstance();
                        ClienteFacade clienteFacade = ClienteFacade.getInstance();

                        /* verificando se o vendedor selecionado não é o mesmo vendedor gravado */

                        VendedorCliente vendedorAtivo = vendedores.Find(x => String.Equals(x.Situacao, ApplicationConstants.ATIVO));

                        if (vendedorAtivo != null)
                        {
                            if (vendedorAtivo.Vendedor.Id != selecionarVendedor.Vendedor.Id)
                            {
                                vendedorFacede.updateVendedorCliente(new VendedorCliente(null, cliente, selecionarVendedor.Vendedor, ApplicationConstants.ATIVO));
                                textVendedor.Text = selecionarVendedor.Vendedor.Nome;
                                vendedores = clienteFacade.findClienteVendedorByCliente(cliente);
                                MessageBox.Show("Vendedor alterado com sucesso.", "Confirmação", MessageBoxButtons.OK, MessageBoxIcon.Information);
                            }
                        }
                        else
                        {
                            /* cliente não tem vendedor ativo porque provavelmente era uma unidade */

                            vendedores.Add(vendedorFacede.insertVendedorCliente(new VendedorCliente(null, cliente, selecionarVendedor.Vendedor, ApplicationConstants.ATIVO)));
                            textVendedor.Text = selecionarVendedor.Vendedor.Nome;

                        }
                    }
                }


            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }

        }

        protected override void btIncluiCnae_Click(object sender, EventArgs e)
        {
            try
            {
                if (MessageBox.Show("Deseja incluir mais um CNAE para o cliente?", "Atenção", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                {
                    frmCnaeSelecionar selecionarCnae = new frmCnaeSelecionar();
                    selecionarCnae.ShowDialog();

                    if (selecionarCnae.Cnae != null)
                    {

                        /* vericando se o cnae selecionado não está incluído na grid */

                        if (cnaes.Exists(x => x.Cnae.Id == selecionarCnae.Cnae.Id))
                            throw new Exception("CNAE já incluido no cliente. Por favor selecione outro.");

                        ClienteFacade clienteFacade = ClienteFacade.getInstance();
                        ClienteCnae clienteCnaeIncluir = clienteFacade.incluirCnaeCliente(new ClienteCnae(cliente, selecionarCnae.Cnae, false));
                        cnaes.Add(clienteCnaeIncluir);

                        MessageBox.Show("CNAE incluído com sucesso.", "Confirmação", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        montaGridCnae();

                    }
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        protected override void btExcluiCnae_Click(object sender, EventArgs e)
        {
            try
            {
                if (dgvCnae.CurrentRow == null)
                    throw new Exception("Selecione uma linha.");

                if (MessageBox.Show("Deseja excluir o cnae selecionado?", "Atenção", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                {
                    ClienteFacade clienteFacede = ClienteFacade.getInstance();

                    clienteFacede.deleteClienteCnaeInCliente(cnaes.ElementAt(dgvCnae.CurrentRow.Index));
                    cnaes.RemoveAt(dgvCnae.CurrentRow.Index);
                    MessageBox.Show("CNAE excluído com sucesso.", "Confirmação", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    montaGridCnae();

                }

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atençao", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }

        }

        protected override void btIncluirLogo_Click(object sender, EventArgs e)
        {
            Image image = null;
            String nomeArquivo = String.Empty;
            String nomeArquivoFinal = String.Empty;

            try
            {

                if (MessageBox.Show("Deseja incluir uma novo logo para o cliente? A logo antiga será excluída.", "Atenção", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                {

                    ClienteFacade clienteFacade = ClienteFacade.getInstance();

                    /* incluindo uma nova logomarca para o cliente */
                    OpenFileDialog clienteLogoFileDialog = new OpenFileDialog();

                    clienteLogoFileDialog.Multiselect = false;
                    clienteLogoFileDialog.Title = "Selecionar Logo";
                    clienteLogoFileDialog.InitialDirectory = @"C:\";
                    /* filtra para exibir somente arquivos de imagens */
                    clienteLogoFileDialog.Filter = "Images(*.JPG) |*.JPG";
                    clienteLogoFileDialog.CheckFileExists = true;
                    clienteLogoFileDialog.CheckPathExists = true;
                    clienteLogoFileDialog.FilterIndex = 2;
                    clienteLogoFileDialog.RestoreDirectory = true;
                    clienteLogoFileDialog.ReadOnlyChecked = true;
                    clienteLogoFileDialog.ShowReadOnly = true;

                    DialogResult dr = clienteLogoFileDialog.ShowDialog();

                    if (dr == System.Windows.Forms.DialogResult.OK)
                    {

                        nomeArquivo = clienteLogoFileDialog.FileName;
                        nomeArquivoFinal = nomeArquivo.Substring(nomeArquivo.LastIndexOf('\\') + 1);

                        /* cria um PictureBox */
                        image = Image.FromFile(nomeArquivo);
                        pbLogo.SizeMode = PictureBoxSizeMode.StretchImage;

                        if (image.Width > 500 && image.Height > 500)
                            throw new LogoException("A imagem deve ter um tamanho máximo de 500 X 500 pixels.");

                        /* excluindo a logo anterior do cliente*/
                        if (clienteArquivo != null)
                            clienteFacade.finalizaClienteArquivo(clienteArquivo);

                        pbLogo.Image = image;

                        byte[] conteudo = FileHelper.CarregarArquivoImagem(nomeArquivo, 5242880);

                        Arquivo novaLogo = new Arquivo(null, nomeArquivoFinal, "image/jpeg", conteudo.Length, conteudo);

                        clienteFacade.incluirArquivo(novaLogo);
                        clienteFacade.incluirClienteArquivo(new ClienteArquivo(null, novaLogo, cliente, null, null, ApplicationConstants.ATIVO));

                        MessageBox.Show("Logo incluída com sucesso.", "Confirmação", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }

                }
            }
            catch (SecurityException ex)
            {
                // O usuário  não possui permissão para ler arquivos
                MessageBox.Show("Erro de segurança Contate o administrador de segurança da rede.\n\n" +
                                            "Mensagem : " + ex.Message + "\n\n" +
                                            "Detalhes (enviar ao suporte):\n\n" + ex.StackTrace);
            }
            catch (LogoException ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
            catch (Exception ex)
            {
                // Não pode carregar a imagem (problemas de permissão)
                MessageBox.Show("Não é possível exibir a imagem : " + nomeArquivoFinal
                                            + ". Você pode não ter permissão para ler o arquivo , ou " +
                                            " ele pode estar corrompido.\n\nErro reportado : " + ex.Message);
            }
        }

        protected override void btIncluirFuncao_Click(object sender, EventArgs e)
        {
            try
            {
                if (MessageBox.Show("Deseja incluir uma nova função para o cliente?", "Atenção", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                {

                    frmFuncaoPesquisa pesquisarFuncao = new frmFuncaoPesquisa();
                    pesquisarFuncao.ShowDialog();

                    if (pesquisarFuncao.Funcoes.Count > 0)
                    {
                        /* verificando se já não tem alguma função selecionada que já esteja incluída no cliente */

                        foreach (DataGridViewRow dvRow in dgvFuncao.Rows)
                            if (pesquisarFuncao.Funcoes.Exists(x => x.Id == (long)dvRow.Cells[0].Value))
                                throw new Exception("A função : " + dvRow.Cells[1].Value + " já está incluída no cliente. Refaça sua pesquisa.");

                        EstudoFacade pcmsoFacade = EstudoFacade.getInstance();

                        pesquisarFuncao.Funcoes.ForEach(delegate(Funcao funcao)
                        {
                            funcoes.Add(pcmsoFacade.insertClienteFuncao(new ClienteFuncao(null, cliente, funcao, ApplicationConstants.ATIVO, DateTime.Now, null)));
                        });

                        MessageBox.Show("Função(ões) incluída(s) com sucesso.", "Confirmação", MessageBoxButtons.OK, MessageBoxIcon.Information);

                        montaGridFuncoes();
                    }
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atençao", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }

        }

        protected override void btExcluirFuncao_Click(object sender, EventArgs e)
        {
            try
            {
                if (dgvFuncao.CurrentRow == null)
                    throw new Exception("Selecione uma linha.");

                if (MessageBox.Show("Deseja excluir a função selecionada?", "Atenção", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                {
                    EstudoFacade pcmsoFacade = EstudoFacade.getInstance();
                    FuncionarioFacade funcionarioFacade = FuncionarioFacade.getInstance();

                    ClienteFuncao clienteFuncaoExcluir = funcoes.Find(x => x.Funcao.Id == (long)dgvFuncao.CurrentRow.Cells[0].Value);

                    /* verificar se o função está sendo usada por algum funcionário e informar 
                     * os funcionários que estão ligados a essa função */

                    List<ClienteFuncaoFuncionario> listClienteFuncaoFuncionario = funcionarioFacade.findAllByClienteFuncao(clienteFuncaoExcluir);

                    if (listClienteFuncaoFuncionario.Count > 0)
                    {
                        StringBuilder funcionarios = new StringBuilder();

                        foreach (ClienteFuncaoFuncionario clienteFuncaoFuncionario in listClienteFuncaoFuncionario)
                        {
                            funcionarios.Append(clienteFuncaoFuncionario.Funcionario.Nome +  " [ " + ValidaCampoHelper.FormataCpf(clienteFuncaoFuncionario.Funcionario.Cpf) + " ]" + System.Environment.NewLine);
                        }

                        throw new Exception("Não é permitido excluir a função com funcionario ativo na função. Resolva esse problema primeiro. " + System.Environment.NewLine + System.Environment.NewLine + funcionarios.ToString());
                       
                    }

                    funcoes.Remove(clienteFuncaoExcluir);
                    clienteFuncaoExcluir.Situacao = ApplicationConstants.DESATIVADO;
                    clienteFuncaoExcluir.DataExclusao = DateTime.Now;
                    pcmsoFacade.updateClienteFuncao(clienteFuncaoExcluir);
                    MessageBox.Show("Função excluída do cliente com sucesso.", "Confirmação", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    montaGridFuncoes();
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atençao", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
        }

        protected override void btMatriz_Click(object sender, EventArgs e)
        {
            try
            {
                if (MessageBox.Show("Deseja alterar a matriz desse cliente?", "Atenção", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                {
                    frmClienteSelecionar selecionarMatriz = new frmClienteSelecionar(null, null, false, null, null, null, null, false, true);
                    selecionarMatriz.ShowDialog();

                    if (selecionarMatriz.Cliente != null)
                    {
                        matriz = selecionarMatriz.Cliente;
                        textMatriz.Text = matriz.RazaoSocial;
                        MessageBox.Show("Matriz alterada com sucesso.", "Confirmação", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atençao", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
        }

        protected override void btCredenciada_Click(object sender, EventArgs e)
        {
            try
            {
                if (MessageBox.Show("Deseja alterar a credenciada nesse cliente?", "Atenção", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                {
                    frmClienteSelecionar selecionarCredenciada = new frmClienteSelecionar(null, false, false, null, true, null, true, false, true);
                    selecionarCredenciada.ShowDialog();

                    if (selecionarCredenciada.Cliente != null)
                    {
                        credenciada = selecionarCredenciada.Cliente;
                        textCredenciada.Text = credenciada.RazaoSocial;
                        MessageBox.Show("Credenciada alterada com sucesso.", "Confirmação", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atençao", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
        }

        protected override void btRamo_Click(object sender, EventArgs e)
        {
            try
            {
                if (MessageBox.Show("Deseja alterar ou incluir o ramo de atividade do cliente?", "Atenção", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                {
                    frmRamoSelecionar selecionarRamo = new frmRamoSelecionar();
                    selecionarRamo.ShowDialog();

                    if (selecionarRamo.Ramo != null)
                    {
                        ramo = selecionarRamo.Ramo;
                        textRamo.Text = ramo.Descricao;
                        MessageBox.Show("Ramo de atividade alterado com sucesso.", "Confirmação", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }

                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atençao", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
        }

        protected override void btExcluirLogo_Click(object sender, EventArgs e)
        {
            try
            {
                if (MessageBox.Show("Deseja excluir a logo marca do cliente?", "Atenção", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                {
                    ClienteFacade clienteFacade = ClienteFacade.getInstance();
                    clienteFacade.finalizaClienteArquivo(clienteArquivo);
                    pbLogo.Image = null;
                    MessageBox.Show("Logo marca excluída com sucesso.", "Confirmação", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atençao", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }

        }

        protected override void btIncluirContato_Click(object sender, EventArgs e)
        {
            try
            {
                if (MessageBox.Show("Deseja incluir mais um contato para o cliente?", "Atenção", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                {
                    frmContatoIncluir incluirContato = new frmContatoIncluir();
                    incluirContato.ShowDialog();

                    if (incluirContato.Contato != null)
                    {
                        if (incluirContato.Contato.ResponsavelContrato == true && contatos.Exists(x => x.ResponsavelContrato == true))
                            throw new Exception("Já existe um contato responsável pelo contrato da empresa.");

                        if (incluirContato.Contato.ResponsavelEstudo == true && contatos.Exists(x => x.ResponsavelEstudo == true))
                            throw new Exception("Já existe um contato responsável pelo PCMSO da empresa.");

                        ClienteFacade clienteFacade = ClienteFacade.getInstance();
                        Contato newContato = incluirContato.Contato;
                        newContato.Cliente = cliente;
                        contatos.Add(clienteFacade.insertContato(newContato));
                        MessageBox.Show("Contato incluído com sucesso.", "Confirmação", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        montaGridContato();
                    }
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atençao", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
        }

        protected override void btExcluirContato_Click(object sender, EventArgs e)
        {
            try
            {
                if (dgvContatos.CurrentRow == null)
                    throw new Exception("Selecione uma linha.");

                if (MessageBox.Show("Deseja excluir o contato selecionado?", "Atenção", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                {
                    ClienteFacade clienteFacade = ClienteFacade.getInstance();

                    Contato contatoExcluir = null;

                    contatoExcluir = contatos.Find(x => x.Id == Convert.ToInt64(dgvContatos.CurrentRow.Cells[0].Value));

                    clienteFacade.deleteContato(contatoExcluir);

                    contatos.Remove(contatoExcluir);
                    MessageBox.Show("Contato excluído com sucesso.", "Confirmação", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    montaGridContato();
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atençao", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
        }

        protected override void dgvCnae_DoubleClick(object sender, EventArgs e)
        {
            try
            {
                ClienteCnae cnaePrincipal = null;
                ClienteFacade clienteFacede = ClienteFacade.getInstance();

                cnaePrincipal = cnaes.Find(x => x.Flag_pr == true);

                if (cnaePrincipal != null)
                {
                    if (cnaePrincipal.Cnae.Id != (Int64)dgvCnae.CurrentRow.Cells[0].Value)
                    {

                        if (MessageBox.Show("Deseja alterar o CNAE principal do cliente", "Atenção", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                        {
                            /* atualizar o cnae principal do cliente */

                            cnaePrincipal.Flag_pr = false;
                            clienteFacede.updateClienteCnae(cnaePrincipal, String.Empty);

                            /* gravando o novo cnae principal do cliente */
                            ClienteCnae novoCnaePrincipal = cnaes.Find(x => x.Cnae.Id == (Int64)dgvCnae.CurrentRow.Cells[0].Value);
                            novoCnaePrincipal.Flag_pr = true;
                            clienteFacede.updateClienteCnae(novoCnaePrincipal, String.Empty);
                            MessageBox.Show("CNAE principal alterado com sucesso.", "Confirmação", MessageBoxButtons.OK, MessageBoxIcon.Information);
                            montaGridCnae();
                        }
                    }
                }
                else
                {
                    /* cliente sem cnae principal */
                    /* gravando o novo cnae principal do cliente */
                    if (cnaes.Count > 0)
                    {
                        ClienteCnae novoCnaePrincipal = cnaes.Find(x => x.Cnae.Id == (Int64)dgvCnae.CurrentRow.Cells[0].Value);
                        novoCnaePrincipal.Flag_pr = true;
                        clienteFacede.updateClienteCnae(novoCnaePrincipal, String.Empty);
                        MessageBox.Show("CNAE principal alterado com sucesso.", "Confirmação", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        montaGridCnae();
                    }

                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atençao", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
        }

        protected override void dgvContatos_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                if (dgvContatos.Rows.Count > 0)
                {
                    Contato contatoAlterar = contatos.Find(x => x.Id == (Int64)dgvContatos.Rows[e.RowIndex].Cells[0].Value);
                    frmContatoAlterar formAlterarContato = new frmContatoAlterar(contatoAlterar);
                    formAlterarContato.ShowDialog();

                    /* alterando o contato do cliente no banco */
                    ClienteFacade clienteFacade = ClienteFacade.getInstance();

                    /* removendo o contato anterior da lista */
                    contatos.Remove(contatoAlterar);

                    contatoAlterar = formAlterarContato.Contato;
                    contatoAlterar.Cliente = cliente;
                    clienteFacade.updateContato(contatoAlterar);

                    /* adicionado o novo contato alterado na lista */
                    contatos.Add(contatoAlterar);
                    montaGridContato();
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atençao", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
        }

        protected override void btnIncluirCentroCusto_Click(object sender, EventArgs e)
        {
            try
            {
                frmClienteCentroCustoIncluir incluirCentroCusto = new frmClienteCentroCustoIncluir();
                incluirCentroCusto.ShowDialog();

                if (incluirCentroCusto.CentroCusto != null)
                {
                    ClienteFacade clienteFacade = ClienteFacade.getInstance();

                    /* verificando se já existe um centro de custo cadastrado com esse nome */

                    CentroCusto centroCustoProcura = CentroCustoCliente.Find(x => string.Equals(x.Descricao, incluirCentroCusto.CentroCusto.Descricao));

                    if (centroCustoProcura != null)
                        throw new Exception("Já existe um centro de custo cadastrado com esse nome.");

                    CentroCusto centroCustoIncluir = clienteFacade.insertCentroCusto(new CentroCusto(null, cliente, incluirCentroCusto.CentroCusto.Descricao, string.Empty));
                    CentroCustoCliente.Add(centroCustoIncluir);
                    montaGridCentroCusto();
                }


            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atençao", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }

        protected override void btnExcluirCentroCusto_Click(object sender, EventArgs e)
        {
            try
            {
                if (dgvCentroCusto.CurrentRow == null)
                    throw new Exception("Selecione uma linha.");

                CentroCusto centroCustoExcluir = centroCustoCliente.Find(x => x.Id == (long)dgvCentroCusto.CurrentRow.Cells[0].Value);
                ClienteFacade clienteFacade = ClienteFacade.getInstance();
                clienteFacade.deleteCentroCusto(centroCustoExcluir);
                centroCustoCliente.Remove(centroCustoExcluir);
                montaGridCentroCusto();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atençao", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        protected override void dgvCentroCusto_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                frmClienteCentroCustoAlterar alteraCentroCusto = new frmClienteCentroCustoAlterar(centroCustoCliente.Find(x => x.Id == (long)dgvCentroCusto.CurrentRow.Cells[0].Value));
                alteraCentroCusto.ShowDialog();

                ClienteFacade clienteFacade = ClienteFacade.getInstance();

                /* verificando se já existe um centro de custo cadastrado com esse nome */

                CentroCusto centroCustoProcura = CentroCustoCliente.Find(x => string.Equals(x.Descricao, alteraCentroCusto.CentroCusto.Descricao));

                if (centroCustoProcura != null)
                    throw new Exception("Já existe um centro de custo cadastrado com esse nome.");

                CentroCusto centroCustoAltera = centroCustoCliente.Find(x => x.Id == (long)dgvCentroCusto.CurrentRow.Cells[0].Value);

                clienteFacade.updateCentroCusto(centroCustoAltera);
                montaGridCentroCusto();

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atençao", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        protected void cbCentroCusto_SelectedValueChanged(object sender, EventArgs e)
        {
            if (string.Equals(((SelectItem)cbCentroCusto.SelectedItem).Valor, "true"))
            {
                grbCentroCusto.Enabled = true;
                flpAcaoCentroCusto.Enabled = true;
            }
            else if (string.Equals(((SelectItem)cbCentroCusto.SelectedItem).Valor, "false") && centroCustoCliente.Count > 0)
            {
                if (MessageBox.Show("Você deseja desabilitar o recurso de centro de custo para o cliente selecionado? Todos os centros de custos serão excluídos.", "Confirmação", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                {
                    ClienteFacade clienteFacade = ClienteFacade.getInstance();
                    centroCustoCliente.ForEach(delegate(CentroCusto centroCusto)
                    {
                        clienteFacade.deleteCentroCusto(centroCusto);
                    });

                    centroCustoCliente.Clear();
                    montaGridCentroCusto();
                    grbCentroCusto.Enabled = false;
                    flpAcaoCentroCusto.Enabled = false;
                }
            }
            else
            {
                centroCustoCliente.Clear();
                montaGridCentroCusto();
                grbCentroCusto.Enabled = false;
                flpAcaoCentroCusto.Enabled = false;
            }
        }

        protected override void cbTipoCliente_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                if (string.Equals(((SelectItem)cbTipoCliente.SelectedItem).Valor, ApplicationConstants.PADRAO))
                {


                    /* cliente padrão */
                    btVendedor.Enabled = true;

                    matriz = null;
                    textMatriz.Text = String.Empty;
                    btMatriz.Enabled = false;

                    credenciada = null;
                    textCredenciada.Text = String.Empty;
                    btCredenciada.Enabled = false;

                    /* habilitando CNPJ do cliente */
                    textCNPJ.ReadOnly = false;

                    /* habilitando ramo de atividade */
                    btRamo.Enabled = true;

                    /* verificando se a empresa é uma unidade. Caso seja então será 
                     * incluído nos controles a opção de endereco de cobrança. */

                    if (cliente.Unidade && tpEndereco.TabCount == 1)
                        tpEndereco.TabPages.Add(tpEnderecoCopy[1]);

                    /* habilitando demais controles da tela */
                    changeControlesTela(true);
                    chanceControleConfiguracao(true);
                }
                else if (string.Equals(((SelectItem)cbTipoCliente.SelectedItem).Valor, ApplicationConstants.UNIDADE))
                {
                    /* cliente unidade */
                    /* empresa pode ser padrão ou credencida ou pessoa física */



                    /* excluindo vendedor do cliente */
                    textVendedor.Text = String.Empty;
                    VendedorCliente vendedorAtivo = vendedores.Find(x => String.Equals(x.Situacao, ApplicationConstants.ATIVO));
                    if (vendedorAtivo != null)
                    {
                        vendedorAtivo.Situacao = ApplicationConstants.DESATIVADO;
                        VendedorFacade vendedorFacade = VendedorFacade.getInstance();
                        vendedorFacade.updateVendedorCliente(vendedorAtivo);
                    }

                    btVendedor.Enabled = false;

                    /* habiliando a matriz */
                    btMatriz.Enabled = true;

                    /* excluindo a informação da credenciada */
                    textCredenciada.Text = String.Empty;
                    credenciada = null;
                    btCredenciada.Enabled = false;

                    /* alterando o comportamento do cnpj. Cliente unidade utiliza o cnpj da matrix */
                    textCNPJ.Text = String.Empty;
                    textCNPJ.ReadOnly = true;

                    /* excluindo o ramo de atividade */
                    ramo = null;
                    textRamo.Text = String.Empty;
                    btRamo.Enabled = false;

                    /* excluindo informação do endereco de cobranca do cliente */
                    textLogradouroCobranca.Text = String.Empty;
                    textNumeroCobranca.Text = String.Empty;
                    textComplementoCobranca.Text = String.Empty;
                    textBairroCobranca.Text = String.Empty;
                    textCEPCobranca.Text = String.Empty;
                    textCidadeCobranca.Text = String.Empty;
                    cidadeCobranca = null;
                    cbUFCobranca.DataSource = null;
                    cbUFCobranca.Items.Clear();
                    textTelefoneCobranca.Text = String.Empty;
                    textFaxCobranca.Text = String.Empty;

                    if (tpEndereco.TabCount == 2)
                    {
                        tpEndereco.TabPages.Clear();
                        tpEndereco.TabPages.Add(tpEnderecoCopy[0]);
                    }

                    /* excluindo todos os cnaes do cliente */
                    ClienteFacade clienteFacade = ClienteFacade.getInstance();
                    cnaes.ForEach(delegate(ClienteCnae clienteCnae)
                    {
                        clienteFacade.deleteClienteCnaeInCliente(clienteCnae);
                    });

                    cnaes.Clear();
                    montaGridCnae();

                    /* excluindo informação de logo do cliente */

                    if (clienteArquivo != null)
                    {
                        clienteFacade.finalizaClienteArquivo(clienteArquivo);
                        pbLogo.Image = null;
                    }

                    /* excluindo informação de funções do cliente */
                    EstudoFacade pcmsoFacade = EstudoFacade.getInstance();
                    foreach (ClienteFuncao clienteFuncao in funcoes)
                    {
                        clienteFuncao.Situacao = ApplicationConstants.DESATIVADO;
                        clienteFuncao.DataExclusao = DateTime.Now;
                        pcmsoFacade.updateClienteFuncao(clienteFuncao);
                    }
                    funcoes.Clear();
                    montaGridFuncoes();

                    /* excluindo todos os contatos do cliente */
                    contatos.ForEach(delegate(Contato contato)
                    {
                        clienteFacade.deleteContato(contato);

                    });

                    contatos.Clear();
                    montaGridContato();

                    /* desabilitando controles de tela */
                    chanceControleConfiguracao(false);
                    changeControlesTela(false);
                }
                else if (string.Equals(((SelectItem)cbTipoCliente.SelectedItem).Valor, ApplicationConstants.CREDENCIADA))
                {
                    /* cliente credenciada */
                    /* ou o cliente pode ser uma empresa padrão ou unidade*/

                    btVendedor.Enabled = true;

                    /* alterando o comportamento da matriz */
                    matriz = null;
                    textMatriz.Text = String.Empty;
                    btMatriz.Enabled = false;

                    /* habilitando comportamento de credenciada */
                    btCredenciada.Enabled = true;

                    /* habilitando CNPJ do cliente */
                    textCNPJ.ReadOnly = false;

                    /* habilitando ramo de atividade */
                    btRamo.Enabled = true;

                    /* verificando se a empresa é uma unidade ou física. Caso seja então será 
                     * incluído nos controles a opção de endereco de cobrança. */

                    if (cliente.Unidade || cliente.Fisica && tpEndereco.TabCount == 1)
                        tpEndereco.TabPages.Add(tpEnderecoCopy[1]);

                    /* habilitando demais controles da tela */
                    changeControlesTela(true);
                    chanceControleConfiguracao(true);
                }
                else
                {
                    /* cliente sendo mudado para pessoa física. Nesse caso ele era um cliente padrão, unidade ou credenciada. */
                    btVendedor.Enabled = true;
                    btRamo.Enabled = true;

                    matriz = null;
                    btMatriz.Enabled = false;
                    textMatriz.Text = String.Empty;

                    credenciada = null;
                    btCredenciada.Enabled = false;
                    textCredenciada.Text = String.Empty;

                    lblRazaoSocial.Text = "Nome";
                    lblCNPJ.Text = "CPF";
                    lblNomeFantasia.Text = "Apelido";
                    textJornada.ReadOnly = true;
                    textEscopo.ReadOnly = true;

                    /* habilitando abas de endereco */
                    this.tpEndereco.Enabled = true;
                    tpEndereco.TabPages.Clear();
                    tpEndereco.TabPages.Add(tpEnderecoCopy[0]);
                    tpComercial.Text = "Endereço";

                    grbCnae.Enabled = false;
                    btIncluirCnae.Enabled = false;
                    btExcluirCnae.Enabled = false;

                    cbPrestador.Enabled = false;
                    textCodigoCnes.ReadOnly = true;

                    grbCentroCusto.Enabled = false;
                    btnIncluirCentroCusto.Enabled = false;
                    btnExcluirCentroCusto.Enabled = false;
                    cbCentroCusto.Enabled = false;
                    cbCredenciadora.Enabled = false;


                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }
    }
}
