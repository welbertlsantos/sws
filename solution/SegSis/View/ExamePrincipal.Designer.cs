﻿namespace SWS.View
{
    partial class frmExamePrincipal
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            this.flpAcao = new System.Windows.Forms.FlowLayoutPanel();
            this.bt_incluir = new System.Windows.Forms.Button();
            this.bt_alterar = new System.Windows.Forms.Button();
            this.bt_excluir = new System.Windows.Forms.Button();
            this.btn_reativar = new System.Windows.Forms.Button();
            this.bt_detalhar = new System.Windows.Forms.Button();
            this.bt_pesquisar = new System.Windows.Forms.Button();
            this.bt_limpar = new System.Windows.Forms.Button();
            this.bt_fechar = new System.Windows.Forms.Button();
            this.lblExame = new System.Windows.Forms.TextBox();
            this.lblSituacao = new System.Windows.Forms.TextBox();
            this.cbSituacao = new SWS.ComboBoxWithBorder();
            this.text_descricao = new System.Windows.Forms.TextBox();
            this.grb_exames = new System.Windows.Forms.GroupBox();
            this.grd_exame = new System.Windows.Forms.DataGridView();
            this.lblCorDesativado = new System.Windows.Forms.Label();
            this.lblDesativado = new System.Windows.Forms.Label();
            this.lblClassificacao = new System.Windows.Forms.TextBox();
            this.cbClassificacaoExame = new SWS.ComboBoxWithBorder();
            ((System.ComponentModel.ISupportInitialize)(this.spForm)).BeginInit();
            this.spForm.Panel1.SuspendLayout();
            this.spForm.Panel2.SuspendLayout();
            this.spForm.SuspendLayout();
            this.pnlForm.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.banner)).BeginInit();
            this.flpAcao.SuspendLayout();
            this.grb_exames.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grd_exame)).BeginInit();
            this.SuspendLayout();
            // 
            // spForm
            // 
            // 
            // spForm.Panel1
            // 
            this.spForm.Panel1.Controls.Add(this.flpAcao);
            // 
            // pnlForm
            // 
            this.pnlForm.Controls.Add(this.cbClassificacaoExame);
            this.pnlForm.Controls.Add(this.lblClassificacao);
            this.pnlForm.Controls.Add(this.lblDesativado);
            this.pnlForm.Controls.Add(this.lblCorDesativado);
            this.pnlForm.Controls.Add(this.grb_exames);
            this.pnlForm.Controls.Add(this.text_descricao);
            this.pnlForm.Controls.Add(this.cbSituacao);
            this.pnlForm.Controls.Add(this.lblSituacao);
            this.pnlForm.Controls.Add(this.lblExame);
            // 
            // flpAcao
            // 
            this.flpAcao.Controls.Add(this.bt_incluir);
            this.flpAcao.Controls.Add(this.bt_alterar);
            this.flpAcao.Controls.Add(this.bt_excluir);
            this.flpAcao.Controls.Add(this.btn_reativar);
            this.flpAcao.Controls.Add(this.bt_detalhar);
            this.flpAcao.Controls.Add(this.bt_pesquisar);
            this.flpAcao.Controls.Add(this.bt_limpar);
            this.flpAcao.Controls.Add(this.bt_fechar);
            this.flpAcao.Dock = System.Windows.Forms.DockStyle.Fill;
            this.flpAcao.Location = new System.Drawing.Point(0, 0);
            this.flpAcao.Name = "flpAcao";
            this.flpAcao.Size = new System.Drawing.Size(784, 35);
            this.flpAcao.TabIndex = 0;
            // 
            // bt_incluir
            // 
            this.bt_incluir.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.bt_incluir.Image = global::SWS.Properties.Resources.icone_mais;
            this.bt_incluir.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.bt_incluir.Location = new System.Drawing.Point(3, 3);
            this.bt_incluir.Name = "bt_incluir";
            this.bt_incluir.Size = new System.Drawing.Size(75, 23);
            this.bt_incluir.TabIndex = 15;
            this.bt_incluir.TabStop = false;
            this.bt_incluir.Text = "&Incluir";
            this.bt_incluir.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.bt_incluir.UseVisualStyleBackColor = true;
            this.bt_incluir.Click += new System.EventHandler(this.bt_incluir_Click);
            // 
            // bt_alterar
            // 
            this.bt_alterar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.bt_alterar.Image = global::SWS.Properties.Resources.Alterar;
            this.bt_alterar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.bt_alterar.Location = new System.Drawing.Point(84, 3);
            this.bt_alterar.Name = "bt_alterar";
            this.bt_alterar.Size = new System.Drawing.Size(75, 23);
            this.bt_alterar.TabIndex = 16;
            this.bt_alterar.TabStop = false;
            this.bt_alterar.Text = "&Alterar";
            this.bt_alterar.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.bt_alterar.UseVisualStyleBackColor = true;
            this.bt_alterar.Click += new System.EventHandler(this.bt_alterar_Click);
            // 
            // bt_excluir
            // 
            this.bt_excluir.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.bt_excluir.Image = global::SWS.Properties.Resources.lixeira;
            this.bt_excluir.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.bt_excluir.Location = new System.Drawing.Point(165, 3);
            this.bt_excluir.Name = "bt_excluir";
            this.bt_excluir.Size = new System.Drawing.Size(75, 23);
            this.bt_excluir.TabIndex = 17;
            this.bt_excluir.TabStop = false;
            this.bt_excluir.Text = "&Excluir";
            this.bt_excluir.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.bt_excluir.UseVisualStyleBackColor = true;
            this.bt_excluir.Click += new System.EventHandler(this.bt_excluir_Click);
            // 
            // btn_reativar
            // 
            this.btn_reativar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_reativar.Image = global::SWS.Properties.Resources.devolucao;
            this.btn_reativar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btn_reativar.Location = new System.Drawing.Point(246, 3);
            this.btn_reativar.Name = "btn_reativar";
            this.btn_reativar.Size = new System.Drawing.Size(75, 23);
            this.btn_reativar.TabIndex = 20;
            this.btn_reativar.TabStop = false;
            this.btn_reativar.Text = "&Reativar";
            this.btn_reativar.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btn_reativar.UseVisualStyleBackColor = true;
            this.btn_reativar.Click += new System.EventHandler(this.btn_reativar_Click);
            // 
            // bt_detalhar
            // 
            this.bt_detalhar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.bt_detalhar.Image = global::SWS.Properties.Resources.lupa;
            this.bt_detalhar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.bt_detalhar.Location = new System.Drawing.Point(327, 3);
            this.bt_detalhar.Name = "bt_detalhar";
            this.bt_detalhar.Size = new System.Drawing.Size(75, 23);
            this.bt_detalhar.TabIndex = 18;
            this.bt_detalhar.TabStop = false;
            this.bt_detalhar.Text = "&Detalhar";
            this.bt_detalhar.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.bt_detalhar.UseVisualStyleBackColor = true;
            this.bt_detalhar.Click += new System.EventHandler(this.bt_detalhar_Click);
            // 
            // bt_pesquisar
            // 
            this.bt_pesquisar.AccessibleName = "";
            this.bt_pesquisar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.bt_pesquisar.Image = global::SWS.Properties.Resources.busca;
            this.bt_pesquisar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.bt_pesquisar.Location = new System.Drawing.Point(408, 3);
            this.bt_pesquisar.Name = "bt_pesquisar";
            this.bt_pesquisar.Size = new System.Drawing.Size(75, 23);
            this.bt_pesquisar.TabIndex = 14;
            this.bt_pesquisar.TabStop = false;
            this.bt_pesquisar.Text = "&Pesquisar";
            this.bt_pesquisar.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.bt_pesquisar.UseVisualStyleBackColor = true;
            this.bt_pesquisar.Click += new System.EventHandler(this.bt_pesquisar_Click);
            // 
            // bt_limpar
            // 
            this.bt_limpar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.bt_limpar.Image = global::SWS.Properties.Resources.vassoura;
            this.bt_limpar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.bt_limpar.Location = new System.Drawing.Point(489, 3);
            this.bt_limpar.Name = "bt_limpar";
            this.bt_limpar.Size = new System.Drawing.Size(75, 23);
            this.bt_limpar.TabIndex = 21;
            this.bt_limpar.TabStop = false;
            this.bt_limpar.Text = "&Limpar";
            this.bt_limpar.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.bt_limpar.UseVisualStyleBackColor = true;
            this.bt_limpar.Click += new System.EventHandler(this.bt_limpar_Click);
            // 
            // bt_fechar
            // 
            this.bt_fechar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.bt_fechar.Image = global::SWS.Properties.Resources.close;
            this.bt_fechar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.bt_fechar.Location = new System.Drawing.Point(570, 3);
            this.bt_fechar.Name = "bt_fechar";
            this.bt_fechar.Size = new System.Drawing.Size(75, 23);
            this.bt_fechar.TabIndex = 19;
            this.bt_fechar.TabStop = false;
            this.bt_fechar.Text = "&Fechar";
            this.bt_fechar.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.bt_fechar.UseVisualStyleBackColor = true;
            this.bt_fechar.Click += new System.EventHandler(this.bt_fechar_Click);
            // 
            // lblExame
            // 
            this.lblExame.BackColor = System.Drawing.Color.LightGray;
            this.lblExame.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblExame.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblExame.Location = new System.Drawing.Point(7, 9);
            this.lblExame.Name = "lblExame";
            this.lblExame.ReadOnly = true;
            this.lblExame.Size = new System.Drawing.Size(127, 21);
            this.lblExame.TabIndex = 0;
            this.lblExame.TabStop = false;
            this.lblExame.Text = "Exame";
            // 
            // lblSituacao
            // 
            this.lblSituacao.BackColor = System.Drawing.Color.LightGray;
            this.lblSituacao.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblSituacao.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSituacao.Location = new System.Drawing.Point(7, 29);
            this.lblSituacao.Name = "lblSituacao";
            this.lblSituacao.ReadOnly = true;
            this.lblSituacao.Size = new System.Drawing.Size(127, 21);
            this.lblSituacao.TabIndex = 1;
            this.lblSituacao.TabStop = false;
            this.lblSituacao.Text = "Situação";
            // 
            // cbSituacao
            // 
            this.cbSituacao.BackColor = System.Drawing.Color.LightGray;
            this.cbSituacao.BorderColor = System.Drawing.Color.DimGray;
            this.cbSituacao.BorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid;
            this.cbSituacao.FormattingEnabled = true;
            this.cbSituacao.Location = new System.Drawing.Point(133, 29);
            this.cbSituacao.Name = "cbSituacao";
            this.cbSituacao.Size = new System.Drawing.Size(608, 21);
            this.cbSituacao.TabIndex = 2;
            // 
            // text_descricao
            // 
            this.text_descricao.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.text_descricao.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.text_descricao.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.text_descricao.Location = new System.Drawing.Point(133, 9);
            this.text_descricao.MaxLength = 100;
            this.text_descricao.Name = "text_descricao";
            this.text_descricao.Size = new System.Drawing.Size(608, 21);
            this.text_descricao.TabIndex = 3;
            // 
            // grb_exames
            // 
            this.grb_exames.Controls.Add(this.grd_exame);
            this.grb_exames.Location = new System.Drawing.Point(7, 76);
            this.grb_exames.Name = "grb_exames";
            this.grb_exames.Size = new System.Drawing.Size(737, 358);
            this.grb_exames.TabIndex = 4;
            this.grb_exames.TabStop = false;
            this.grb_exames.Text = "Exames";
            // 
            // grd_exame
            // 
            this.grd_exame.AllowUserToAddRows = false;
            this.grd_exame.AllowUserToDeleteRows = false;
            this.grd_exame.AllowUserToOrderColumns = true;
            this.grd_exame.AllowUserToResizeColumns = false;
            this.grd_exame.AllowUserToResizeRows = false;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.Gainsboro;
            this.grd_exame.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle1;
            this.grd_exame.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this.grd_exame.BackgroundColor = System.Drawing.Color.White;
            this.grd_exame.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.Color.LightGray;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.grd_exame.DefaultCellStyle = dataGridViewCellStyle2;
            this.grd_exame.Dock = System.Windows.Forms.DockStyle.Fill;
            this.grd_exame.Location = new System.Drawing.Point(3, 16);
            this.grd_exame.Name = "grd_exame";
            this.grd_exame.ReadOnly = true;
            this.grd_exame.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.grd_exame.Size = new System.Drawing.Size(731, 339);
            this.grd_exame.TabIndex = 7;
            this.grd_exame.CellMouseDoubleClick += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.grd_exame_CellMouseDoubleClick);
            this.grd_exame.RowPrePaint += new System.Windows.Forms.DataGridViewRowPrePaintEventHandler(this.grd_exame_RowPrePaint);
            // 
            // lblCorDesativado
            // 
            this.lblCorDesativado.AutoSize = true;
            this.lblCorDesativado.BackColor = System.Drawing.Color.Red;
            this.lblCorDesativado.Location = new System.Drawing.Point(10, 438);
            this.lblCorDesativado.Name = "lblCorDesativado";
            this.lblCorDesativado.Size = new System.Drawing.Size(13, 13);
            this.lblCorDesativado.TabIndex = 5;
            this.lblCorDesativado.Text = "  ";
            // 
            // lblDesativado
            // 
            this.lblDesativado.AutoSize = true;
            this.lblDesativado.Location = new System.Drawing.Point(29, 438);
            this.lblDesativado.Name = "lblDesativado";
            this.lblDesativado.Size = new System.Drawing.Size(104, 13);
            this.lblDesativado.TabIndex = 6;
            this.lblDesativado.Text = "Exames desativados";
            // 
            // lblClassificacao
            // 
            this.lblClassificacao.BackColor = System.Drawing.Color.LightGray;
            this.lblClassificacao.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblClassificacao.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblClassificacao.Location = new System.Drawing.Point(7, 49);
            this.lblClassificacao.Name = "lblClassificacao";
            this.lblClassificacao.ReadOnly = true;
            this.lblClassificacao.Size = new System.Drawing.Size(127, 21);
            this.lblClassificacao.TabIndex = 7;
            this.lblClassificacao.TabStop = false;
            this.lblClassificacao.Text = "Classificacao";
            // 
            // cbClassificacaoExame
            // 
            this.cbClassificacaoExame.BackColor = System.Drawing.Color.LightGray;
            this.cbClassificacaoExame.BorderColor = System.Drawing.Color.DimGray;
            this.cbClassificacaoExame.BorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid;
            this.cbClassificacaoExame.FormattingEnabled = true;
            this.cbClassificacaoExame.Location = new System.Drawing.Point(133, 49);
            this.cbClassificacaoExame.Name = "cbClassificacaoExame";
            this.cbClassificacaoExame.Size = new System.Drawing.Size(608, 21);
            this.cbClassificacaoExame.TabIndex = 8;
            // 
            // frmExamePrincipal
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(784, 561);
            this.Name = "frmExamePrincipal";
            this.Text = "GERENCIAR EXAMES";
            this.spForm.Panel1.ResumeLayout(false);
            this.spForm.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.spForm)).EndInit();
            this.spForm.ResumeLayout(false);
            this.pnlForm.ResumeLayout(false);
            this.pnlForm.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.banner)).EndInit();
            this.flpAcao.ResumeLayout(false);
            this.grb_exames.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.grd_exame)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.FlowLayoutPanel flpAcao;
        private System.Windows.Forms.Button btn_reativar;
        private System.Windows.Forms.Button bt_fechar;
        private System.Windows.Forms.Button bt_detalhar;
        private System.Windows.Forms.Button bt_excluir;
        private System.Windows.Forms.Button bt_alterar;
        private System.Windows.Forms.Button bt_incluir;
        private System.Windows.Forms.Button bt_pesquisar;
        private ComboBoxWithBorder cbSituacao;
        private System.Windows.Forms.TextBox lblSituacao;
        private System.Windows.Forms.TextBox lblExame;
        private System.Windows.Forms.TextBox text_descricao;
        private System.Windows.Forms.Label lblDesativado;
        private System.Windows.Forms.Label lblCorDesativado;
        private System.Windows.Forms.GroupBox grb_exames;
        private System.Windows.Forms.DataGridView grd_exame;
        private System.Windows.Forms.Button bt_limpar;
        private ComboBoxWithBorder cbClassificacaoExame;
        private System.Windows.Forms.TextBox lblClassificacao;
    }
}