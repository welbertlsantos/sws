﻿namespace SWS.View
{
    partial class frmExamesExtrasClienteFuncao
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            this.flpAcao = new System.Windows.Forms.FlowLayoutPanel();
            this.btConfirmar = new System.Windows.Forms.Button();
            this.btIncluirFuncao = new System.Windows.Forms.Button();
            this.btFechar = new System.Windows.Forms.Button();
            this.grbFuncao = new System.Windows.Forms.GroupBox();
            this.dgvFuncao = new System.Windows.Forms.DataGridView();
            ((System.ComponentModel.ISupportInitialize)(this.scForm)).BeginInit();
            this.scForm.Panel1.SuspendLayout();
            this.scForm.Panel2.SuspendLayout();
            this.scForm.SuspendLayout();
            this.pnlForm.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.banner)).BeginInit();
            this.flpAcao.SuspendLayout();
            this.grbFuncao.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvFuncao)).BeginInit();
            this.SuspendLayout();
            // 
            // scForm
            // 
            // 
            // scForm.Panel1
            // 
            this.scForm.Panel1.Controls.Add(this.flpAcao);
            // 
            // pnlForm
            // 
            this.pnlForm.Controls.Add(this.grbFuncao);
            // 
            // flpAcao
            // 
            this.flpAcao.Controls.Add(this.btConfirmar);
            this.flpAcao.Controls.Add(this.btIncluirFuncao);
            this.flpAcao.Controls.Add(this.btFechar);
            this.flpAcao.Dock = System.Windows.Forms.DockStyle.Fill;
            this.flpAcao.Location = new System.Drawing.Point(0, 0);
            this.flpAcao.Name = "flpAcao";
            this.flpAcao.Size = new System.Drawing.Size(534, 30);
            this.flpAcao.TabIndex = 0;
            // 
            // btConfirmar
            // 
            this.btConfirmar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btConfirmar.Image = global::SWS.Properties.Resources.Gravar;
            this.btConfirmar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btConfirmar.Location = new System.Drawing.Point(3, 3);
            this.btConfirmar.Name = "btConfirmar";
            this.btConfirmar.Size = new System.Drawing.Size(75, 23);
            this.btConfirmar.TabIndex = 6;
            this.btConfirmar.TabStop = false;
            this.btConfirmar.Text = "&Confirmar";
            this.btConfirmar.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btConfirmar.UseVisualStyleBackColor = true;
            this.btConfirmar.Click += new System.EventHandler(this.btConfirmar_Click);
            // 
            // btIncluirFuncao
            // 
            this.btIncluirFuncao.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btIncluirFuncao.Image = global::SWS.Properties.Resources.incluir;
            this.btIncluirFuncao.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btIncluirFuncao.Location = new System.Drawing.Point(84, 3);
            this.btIncluirFuncao.Name = "btIncluirFuncao";
            this.btIncluirFuncao.Size = new System.Drawing.Size(75, 23);
            this.btIncluirFuncao.TabIndex = 7;
            this.btIncluirFuncao.TabStop = false;
            this.btIncluirFuncao.Text = "&Nova";
            this.btIncluirFuncao.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btIncluirFuncao.UseVisualStyleBackColor = true;
            this.btIncluirFuncao.Click += new System.EventHandler(this.btIncluirFuncao_Click);
            // 
            // btFechar
            // 
            this.btFechar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btFechar.Image = global::SWS.Properties.Resources.close;
            this.btFechar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btFechar.Location = new System.Drawing.Point(165, 3);
            this.btFechar.Name = "btFechar";
            this.btFechar.Size = new System.Drawing.Size(75, 23);
            this.btFechar.TabIndex = 8;
            this.btFechar.TabStop = false;
            this.btFechar.Text = "&Sair";
            this.btFechar.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btFechar.UseVisualStyleBackColor = true;
            this.btFechar.Click += new System.EventHandler(this.btFechar_Click);
            // 
            // grbFuncao
            // 
            this.grbFuncao.Controls.Add(this.dgvFuncao);
            this.grbFuncao.Location = new System.Drawing.Point(12, 14);
            this.grbFuncao.Name = "grbFuncao";
            this.grbFuncao.Size = new System.Drawing.Size(490, 260);
            this.grbFuncao.TabIndex = 55;
            this.grbFuncao.TabStop = false;
            this.grbFuncao.Text = "Funções";
            // 
            // dgvFuncao
            // 
            this.dgvFuncao.AllowUserToAddRows = false;
            this.dgvFuncao.AllowUserToDeleteRows = false;
            this.dgvFuncao.AllowUserToOrderColumns = true;
            this.dgvFuncao.AllowUserToResizeRows = false;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.Gainsboro;
            this.dgvFuncao.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle1;
            this.dgvFuncao.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.DisplayedCells;
            this.dgvFuncao.BackgroundColor = System.Drawing.Color.White;
            this.dgvFuncao.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.Color.LightGray;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvFuncao.DefaultCellStyle = dataGridViewCellStyle2;
            this.dgvFuncao.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvFuncao.Location = new System.Drawing.Point(3, 16);
            this.dgvFuncao.MultiSelect = false;
            this.dgvFuncao.Name = "dgvFuncao";
            this.dgvFuncao.ReadOnly = true;
            this.dgvFuncao.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvFuncao.Size = new System.Drawing.Size(484, 241);
            this.dgvFuncao.TabIndex = 0;
            this.dgvFuncao.CellMouseDoubleClick += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.dgvFuncao_CellMouseDoubleClick);
            // 
            // frmExamesExtrasClienteFuncao
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(534, 361);
            this.Name = "frmExamesExtrasClienteFuncao";
            this.Text = "INCLUIR FUNÇÃO";
            this.scForm.Panel1.ResumeLayout(false);
            this.scForm.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.scForm)).EndInit();
            this.scForm.ResumeLayout(false);
            this.pnlForm.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.banner)).EndInit();
            this.flpAcao.ResumeLayout(false);
            this.grbFuncao.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvFuncao)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.FlowLayoutPanel flpAcao;
        private System.Windows.Forms.Button btConfirmar;
        private System.Windows.Forms.Button btIncluirFuncao;
        private System.Windows.Forms.Button btFechar;
        private System.Windows.Forms.GroupBox grbFuncao;
        private System.Windows.Forms.DataGridView dgvFuncao;
    }
}