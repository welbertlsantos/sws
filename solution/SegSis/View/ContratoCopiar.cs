﻿using SWS.Entidade;
using SWS.Facade;
using SWS.View.Resources;
using SWS.View.ViewHelper;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SWS.View
{
    public partial class frmContratoCopiar : frmTemplateConsulta
    {
        public frmContratoCopiar()
        {
            InitializeComponent();
            ActiveControl = textCodigo;
        }

        private Cliente clienteOrigem;

        public Cliente ClienteOrigem
        {
            get { return clienteOrigem; }
            set { clienteOrigem = value; }
        }

        private Cliente clienteDestino;

        public Cliente ClienteDestino
        {
            get { return clienteDestino; }
            set { clienteDestino = value; }
        }

        private void btnFechar_Click(object sender, EventArgs e)
        {
            this.Close();

        }

        private void btnCliente_Click(object sender, EventArgs e)
        {
            try
            {
                frmClienteSelecionar selecionarCliente = new frmClienteSelecionar(null, null, null, null, true, null, null, null, true);
                selecionarCliente.ShowDialog();

                if (selecionarCliente.Cliente != null)
                {
                    this.ClienteOrigem = selecionarCliente.Cliente;
                    textCliente.Text = this.ClienteOrigem.RazaoSocial + "[ " + (this.ClienteOrigem.Cnpj.Length == 14 ? ValidaCampoHelper.FormataCnpj(this.ClienteOrigem.Cnpj) : ValidaCampoHelper.FormataCpf(this.ClienteOrigem.Cnpj)) + " ]";

                }

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btnExcluirCliente_Click(object sender, EventArgs e)
        {
            try
            {
                if (this.clienteOrigem == null)
                    throw new Exception("Você deve selecionar primeiro o cliente.");

                this.clienteOrigem = null;
                textCliente.Text = string.Empty;
                ActiveControl = btnCliente;
                    
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
            }

        }

        private void btnBuscar_Click(object sender, EventArgs e)
        {
            try
            {
                if (this.clienteOrigem == null && string.IsNullOrEmpty(textCodigo.Text.Replace(".","").Replace("/","")))
                    throw new Exception("Você deve selecionar o cliente");
                
                montaGridContrato();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
            }
        }

        private void montaGridContrato()
        {
            try
            {
                this.Cursor = Cursors.WaitCursor;
                dgvContrato.Columns.Clear();
                dgvContrato.ColumnCount = 7;

                FinanceiroFacade financeiroFacade = FinanceiroFacade.getInstance();


                dgvContrato.Columns[0].HeaderText = "ID_CONTRATO";
                dgvContrato.Columns[0].Visible = false;

                dgvContrato.Columns[1].HeaderText = "CÓDIGO";

                dgvContrato.Columns[2].HeaderText = "ID_CLIENTE";
                dgvContrato.Columns[2].Visible = false;

                dgvContrato.Columns[3].HeaderText = "CLIENTE";

                dgvContrato.Columns[4].HeaderText = "CNPJ";

                dgvContrato.Columns[5].HeaderText = "DT INÍCIO VIGÊNCIA";

                dgvContrato.Columns[6].HeaderText = "DT FIM VIGÊNCIA";


                Contrato contrato = new Contrato();
                contrato.CodigoContrato = textCodigo.Text;
                contrato.Cliente = this.clienteOrigem;
                
                DataSet ds = financeiroFacade.findContratoByFilter(contrato, null, null, null, null);

                foreach (DataRow row in ds.Tables[0].Rows)
                {
                    if (string.Equals(row["SITUACAO"], ApplicationConstants.CONTRATO_FECHADO))
                    {
                        dgvContrato.Rows.Add(row["ID_CONTRATO"],
                            row["CODIGO_CONTRATO"],
                            row["ID_CLIENTE"],
                            row["RAZAO_SOCIAL"],
                            ValidaCampoHelper.FormataCnpj(row["CNPJ"].ToString()),
                            string.Format("{0:dd/MM/yyyy HH:mm:ss}", row["DATA_INICIO"].ToString()),
                            string.Format("{0:dd/MM/yyyy HH:mm:ss}", row["DATA_FIM"].ToString())
                        );
                    }
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }

        }

        private void btnClienteDuplicado_Click(object sender, EventArgs e)
        {
            try
            {
                if (dgvContrato.Rows.Count == 0)
                    throw new Exception("Você deve buscar o contrato para ser duplicado.");

                frmClienteSelecionar selecionarClienteDestino = new frmClienteSelecionar(null, null, null, null, true, null, null, null, true);
                selecionarClienteDestino.ShowDialog();

                if (selecionarClienteDestino != null)
                {
                    this.clienteDestino = selecionarClienteDestino.Cliente;
                    this.textClienteDuplicado.Text = this.clienteDestino.RazaoSocial + "[ " + (this.clienteDestino.Cnpj.Length == 14 ? ValidaCampoHelper.FormataCnpj(this.clienteDestino.Cnpj) : ValidaCampoHelper.FormataCpf(this.clienteDestino.Cnpj)) + " ]";
                }
                ActiveControl = btnCopiar;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btnExcluirClienteDuplicado_Click(object sender, EventArgs e)
        {
            try
            {
                if (this.clienteDestino == null)
                    throw new Exception("Cliente destino não selecionado");

                this.clienteDestino = null;
                textClienteDuplicado.Text = string.Empty;
                ActiveControl = btnClienteDuplicado;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
            }

        }

        private void btnCopiar_Click(object sender, EventArgs e)
        {
            try
            {
                /* gerando a duplicação do contrato e solicitando as datas para validação */
                this.Cursor = Cursors.WaitCursor;

                if (dgvContrato.CurrentRow == null)
                    throw new Exception("Você deve selecinar um contrato na tabela.");

                FinanceiroFacade financeiroFacade = FinanceiroFacade.getInstance();

                Contrato contratoOrigem = new Contrato();
                contratoOrigem.Id = Convert.ToInt64(dgvContrato.CurrentRow.Cells[0].Value);
                contratoOrigem = financeiroFacade.findContratoById((long)contratoOrigem.Id);


                Contrato contratoNovo = new Contrato();
                contratoNovo.Cliente = this.clienteDestino;
                contratoNovo.Situacao = ApplicationConstants.CONTRATO_GRAVADO;

                frmContratoDuplicarData selecionarDatas = new frmContratoDuplicarData();
                selecionarDatas.ShowDialog();

                if (selecionarDatas.DataInicioVigencia != null)
                {
                    contratoNovo.DataElaboracao = DateTime.Now;
                    contratoNovo.DataInicio = selecionarDatas.DataInicioVigencia;
                    contratoNovo.DataFim = selecionarDatas.DataFimVigencia;

                    /* iniciando a duplicação do contrato */

                    contratoNovo = financeiroFacade.copiarContrato(contratoNovo, contratoOrigem);
                    MessageBox.Show("Contrato copiado com sucesso. Anote o número do novo contrato" + System.Environment.NewLine + "Codigo: " + contratoNovo.CodigoContrato, "Confirmação", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    this.Close();

                }
                else throw new Exception("Você não selecionou a data de Inicio e/ou Data de Fim da vigência do novo contrato.");
                
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
           


        }

    }
}
