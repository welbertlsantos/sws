﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using SegSis.Entidade;
using SegSis.Facade;
using SegSis.View.Resources;

namespace SegSis.View
{
    public partial class frm_FuncaoComentarioBuscar : BaseFormConsulta
    {
        private static String msg1 = "Selecione uma linha.";
        
        private Funcao funcao;

        private ComentarioFuncao comentarioFuncao = null;

        public ComentarioFuncao getComentarioFuncao()
        {
            return this.comentarioFuncao;
        }
        
        public frm_FuncaoComentarioBuscar(Funcao funcao)
        {
            InitializeComponent();
            this.funcao = funcao;
            montaDataGrid();
        }

        private void btnFechar_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        public void montaDataGrid()
        {
            try
            {
                this.dgAtividade.Columns.Clear();

                PpraFacade ppraFacade = PpraFacade.getInstance();

                DataSet ds = ppraFacade.findAllComentarioFuncaoAtivoByFuncao(funcao);

                dgAtividade.DataSource = ds.Tables[0].DefaultView;

                dgAtividade.Columns[0].HeaderText = "Id";
                dgAtividade.Columns[0].Visible = false;

                dgAtividade.Columns[1].HeaderText = "IdFuncao";
                dgAtividade.Columns[1].Visible = false;

                dgAtividade.Columns[2].HeaderText = "Atividade";

                dgAtividade.Columns[3].HeaderText = "Situação";
                dgAtividade.Columns[3].Visible = false;

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
        }

        private void btnGravar_Click(object sender, EventArgs e)
        {
            try
            {
                if (dgAtividade.CurrentRow == null)
                {
                    throw new Exception(msg1);
                }

                comentarioFuncao = new ComentarioFuncao((Int64)dgAtividade.CurrentRow.Cells[0].Value,
                    funcao, (String)dgAtividade.CurrentRow.Cells[2].Value,
                    (String)dgAtividade.CurrentRow.Cells[3].Value);

                this.Close();

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
            
        }

        private void btnNovo_Click(object sender, EventArgs e)
        {
            try
            {
                PpraFacade ppraFacade = PpraFacade.getInstance();
                
                frm_ComentarioFuncaoIncluir incluirComentario = new frm_ComentarioFuncaoIncluir();
                incluirComentario.ShowDialog();

                if (!String.IsNullOrEmpty(incluirComentario.getAtividade().Trim()))
                {
                    comentarioFuncao = ppraFacade.insertComentarioFuncao(new ComentarioFuncao(null, funcao, incluirComentario.getAtividade().ToUpper(),
                        ApplicationConstants.ATIVO));
                    
                    montaDataGrid();
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
        }
    }
}
