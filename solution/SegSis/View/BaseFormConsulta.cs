﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace SWS.View
{
    public partial class BaseFormConsulta : Form
    {
        public BaseFormConsulta()
        {
            
            this.StartPosition = FormStartPosition.CenterScreen;
            this.AutoSize = false;
            this.MaximizeBox = false;
            this.MinimizeBox = true;
            this.FormBorderStyle = FormBorderStyle.FixedSingle;
            this.BackColor = System.Drawing.Color.White;
            this.Icon = ((System.Drawing.Icon)(SWS.Properties.Resources.icone));
            InitializeComponent();

        }
    }
}
