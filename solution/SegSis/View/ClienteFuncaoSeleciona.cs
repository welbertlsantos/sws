﻿using SWS.Entidade;
using SWS.Facade;
using SWS.View.Resources;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace SWS.View
{
    public partial class frmClienteFuncaoSeleciona : frmTemplateConsulta
    {
        private Cliente cliente;
        private ClienteFuncao clienteFuncao;

        public ClienteFuncao ClienteFuncao
        {
            get { return clienteFuncao; }
            set { clienteFuncao = value; }
        }
        
        public frmClienteFuncaoSeleciona(Cliente cliente)
        {
            InitializeComponent();
            validaPermissao();
            this.cliente = cliente;
            if (cliente != null)
            {
                textRazaoSocial.Text = cliente.RazaoSocial;
                textFantasia.Text = cliente.Fantasia;
                textCnpj.Text = cliente.Cnpj;
            }

            ActiveControl = textFuncao;

        }

        private void btFechar_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnIncluirFuncao_Click(object sender, EventArgs e)
        {
            try
            {
                frmFuncaoPesquisa pesquisarFuncao = new frmFuncaoPesquisa();
                pesquisarFuncao.ShowDialog();

                if (pesquisarFuncao.Funcoes.Count > 0)
                {
                    /* verificando se não existe alguma função selecionada que já tenha sido incluída antes */

                    foreach (DataGridViewRow dvRow in dgvFuncao.Rows)
                        if (pesquisarFuncao.Funcoes.Exists(x => x.Id == (long)dvRow.Cells[1].Value))
                            throw new Exception(" Já existe a função: " + dvRow.Cells[2].Value + " cadastrada para o cliente. Refaça sua seleção.");
                    
                    EstudoFacade pcmsoFacade = EstudoFacade.getInstance();

                    pesquisarFuncao.Funcoes.ForEach(delegate(Funcao funcao)
                    {
                        pcmsoFacade.insertClienteFuncao(new ClienteFuncao(null, cliente, funcao, ApplicationConstants.ATIVO, DateTime.Now, null));
                    });

                    montaGrid();
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btConfirmar_Click(object sender, EventArgs e)
        {
            try
            {
                if (dgvFuncao.CurrentRow == null)
                    throw new Exception("Selecione uma função");

                EstudoFacade pcmsoFacade = EstudoFacade.getInstance();

                clienteFuncao = pcmsoFacade.findClienteFuncaoById((Int64)dgvFuncao.CurrentRow.Cells[0].Value);
                this.Close();

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btBuscar_Click(object sender, EventArgs e)
        {
            montaGrid();
        }

        private void validaPermissao()
        {
            /* verificando permissões de botões */
            PermissionamentoFacade permissionamentoFacade = PermissionamentoFacade.getInstance();
            btnIncluirFuncao.Enabled = permissionamentoFacade.hasPermission("FUNCAOCLIENTE", "INCLUIR");
        }

        protected void montaGrid()
        {
            try
            {
                this.Cursor = Cursors.WaitCursor;
                dgvFuncao.Columns.Clear();

                dgvFuncao.ColumnCount = 4;

                ClienteFacade clienteFacade = ClienteFacade.getInstance();
                DataSet ds = clienteFacade.findFuncaoByClienteFuncao(new ClienteFuncao(null, cliente, new Funcao(null, textFuncao.Text, String.Empty, ApplicationConstants.ATIVO, string.Empty), String.Empty, null, null));

                dgvFuncao.Columns[0].HeaderText = "id_seg_cliente_funcao";
                dgvFuncao.Columns[0].Visible = false;

                dgvFuncao.Columns[1].HeaderText = "Código Função";
                dgvFuncao.Columns[1].ReadOnly = true;
                dgvFuncao.Columns[1].Width = 50;

                dgvFuncao.Columns[2].HeaderText = "Nome / Cargo";
                dgvFuncao.Columns[2].Width = 350;

                dgvFuncao.Columns[3].HeaderText = "Código CBO";

                foreach (DataRow row in ds.Tables[0].Rows)
                {
                    dgvFuncao.Rows.Add(Convert.ToInt64(row["id_seg_cliente_funcao"]), Convert.ToInt64(row["id_funcao"]),
                        row["descricao"].ToString(), row["cod_cbo"].ToString());
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }

        private void dgvFuncao_CellMouseDoubleClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            try
            {
                btConfirmar.PerformClick();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }
    }
}
