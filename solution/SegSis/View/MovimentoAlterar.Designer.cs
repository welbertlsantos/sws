﻿namespace SWS.View
{
    partial class frmMovimentoAlterar
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.flpAcao = new System.Windows.Forms.FlowLayoutPanel();
            this.btn_confirma = new System.Windows.Forms.Button();
            this.btn_fechar = new System.Windows.Forms.Button();
            this.lblItem = new System.Windows.Forms.TextBox();
            this.textItem = new System.Windows.Forms.TextBox();
            this.lblPrecoAtual = new System.Windows.Forms.TextBox();
            this.textPrecoAtual = new System.Windows.Forms.TextBox();
            this.textPrecoNovo = new System.Windows.Forms.TextBox();
            this.lblPrecoNovo = new System.Windows.Forms.TextBox();
            this.grb_opcoes = new System.Windows.Forms.GroupBox();
            this.rb_todosOsItensFiltrados = new System.Windows.Forms.RadioButton();
            this.rb_somenteSelecionado = new System.Windows.Forms.RadioButton();
            ((System.ComponentModel.ISupportInitialize)(this.scForm)).BeginInit();
            this.scForm.Panel1.SuspendLayout();
            this.scForm.Panel2.SuspendLayout();
            this.scForm.SuspendLayout();
            this.pnlForm.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.banner)).BeginInit();
            this.flpAcao.SuspendLayout();
            this.grb_opcoes.SuspendLayout();
            this.SuspendLayout();
            // 
            // scForm
            // 
            // 
            // scForm.Panel1
            // 
            this.scForm.Panel1.Controls.Add(this.flpAcao);
            // 
            // pnlForm
            // 
            this.pnlForm.Controls.Add(this.grb_opcoes);
            this.pnlForm.Controls.Add(this.textPrecoNovo);
            this.pnlForm.Controls.Add(this.lblPrecoNovo);
            this.pnlForm.Controls.Add(this.textPrecoAtual);
            this.pnlForm.Controls.Add(this.lblPrecoAtual);
            this.pnlForm.Controls.Add(this.textItem);
            this.pnlForm.Controls.Add(this.lblItem);
            // 
            // flpAcao
            // 
            this.flpAcao.Controls.Add(this.btn_confirma);
            this.flpAcao.Controls.Add(this.btn_fechar);
            this.flpAcao.Dock = System.Windows.Forms.DockStyle.Fill;
            this.flpAcao.Location = new System.Drawing.Point(0, 0);
            this.flpAcao.Name = "flpAcao";
            this.flpAcao.Size = new System.Drawing.Size(534, 30);
            this.flpAcao.TabIndex = 0;
            // 
            // btn_confirma
            // 
            this.btn_confirma.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_confirma.Image = global::SWS.Properties.Resources.fechar_ico;
            this.btn_confirma.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btn_confirma.Location = new System.Drawing.Point(3, 3);
            this.btn_confirma.Name = "btn_confirma";
            this.btn_confirma.Size = new System.Drawing.Size(75, 23);
            this.btn_confirma.TabIndex = 6;
            this.btn_confirma.TabStop = false;
            this.btn_confirma.Text = "&Confirma";
            this.btn_confirma.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btn_confirma.UseVisualStyleBackColor = true;
            this.btn_confirma.Click += new System.EventHandler(this.btn_confirma_Click);
            // 
            // btn_fechar
            // 
            this.btn_fechar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_fechar.Image = global::SWS.Properties.Resources.close;
            this.btn_fechar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btn_fechar.Location = new System.Drawing.Point(84, 3);
            this.btn_fechar.Name = "btn_fechar";
            this.btn_fechar.Size = new System.Drawing.Size(75, 23);
            this.btn_fechar.TabIndex = 7;
            this.btn_fechar.TabStop = false;
            this.btn_fechar.Text = "&Fechar";
            this.btn_fechar.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btn_fechar.UseVisualStyleBackColor = true;
            this.btn_fechar.Click += new System.EventHandler(this.btn_fechar_Click);
            // 
            // lblItem
            // 
            this.lblItem.BackColor = System.Drawing.Color.LightGray;
            this.lblItem.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblItem.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblItem.Location = new System.Drawing.Point(8, 8);
            this.lblItem.Name = "lblItem";
            this.lblItem.ReadOnly = true;
            this.lblItem.Size = new System.Drawing.Size(70, 21);
            this.lblItem.TabIndex = 0;
            this.lblItem.TabStop = false;
            this.lblItem.Text = "Item";
            // 
            // textItem
            // 
            this.textItem.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.textItem.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textItem.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textItem.Location = new System.Drawing.Point(77, 8);
            this.textItem.Name = "textItem";
            this.textItem.ReadOnly = true;
            this.textItem.Size = new System.Drawing.Size(416, 21);
            this.textItem.TabIndex = 2;
            this.textItem.TabStop = false;
            // 
            // lblPrecoAtual
            // 
            this.lblPrecoAtual.BackColor = System.Drawing.Color.LightGray;
            this.lblPrecoAtual.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblPrecoAtual.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPrecoAtual.Location = new System.Drawing.Point(8, 28);
            this.lblPrecoAtual.Name = "lblPrecoAtual";
            this.lblPrecoAtual.ReadOnly = true;
            this.lblPrecoAtual.Size = new System.Drawing.Size(70, 21);
            this.lblPrecoAtual.TabIndex = 3;
            this.lblPrecoAtual.TabStop = false;
            this.lblPrecoAtual.Text = "Preço Atual";
            // 
            // textPrecoAtual
            // 
            this.textPrecoAtual.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.textPrecoAtual.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textPrecoAtual.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textPrecoAtual.ForeColor = System.Drawing.Color.Red;
            this.textPrecoAtual.Location = new System.Drawing.Point(77, 28);
            this.textPrecoAtual.Name = "textPrecoAtual";
            this.textPrecoAtual.ReadOnly = true;
            this.textPrecoAtual.Size = new System.Drawing.Size(416, 21);
            this.textPrecoAtual.TabIndex = 4;
            // 
            // textPrecoNovo
            // 
            this.textPrecoNovo.BackColor = System.Drawing.Color.White;
            this.textPrecoNovo.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textPrecoNovo.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textPrecoNovo.Location = new System.Drawing.Point(77, 66);
            this.textPrecoNovo.Name = "textPrecoNovo";
            this.textPrecoNovo.Size = new System.Drawing.Size(416, 21);
            this.textPrecoNovo.TabIndex = 1;
            this.textPrecoNovo.Enter += new System.EventHandler(this.textPrecoNovo_Enter);
            this.textPrecoNovo.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.textPrecoNovo_KeyPress);
            this.textPrecoNovo.Leave += new System.EventHandler(this.textPrecoNovo_Leave);
            // 
            // lblPrecoNovo
            // 
            this.lblPrecoNovo.BackColor = System.Drawing.Color.LightGray;
            this.lblPrecoNovo.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblPrecoNovo.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPrecoNovo.Location = new System.Drawing.Point(8, 66);
            this.lblPrecoNovo.Name = "lblPrecoNovo";
            this.lblPrecoNovo.ReadOnly = true;
            this.lblPrecoNovo.Size = new System.Drawing.Size(70, 21);
            this.lblPrecoNovo.TabIndex = 5;
            this.lblPrecoNovo.TabStop = false;
            this.lblPrecoNovo.Text = "Novo Preço";
            // 
            // grb_opcoes
            // 
            this.grb_opcoes.Controls.Add(this.rb_todosOsItensFiltrados);
            this.grb_opcoes.Controls.Add(this.rb_somenteSelecionado);
            this.grb_opcoes.Location = new System.Drawing.Point(8, 93);
            this.grb_opcoes.Name = "grb_opcoes";
            this.grb_opcoes.Size = new System.Drawing.Size(487, 105);
            this.grb_opcoes.TabIndex = 6;
            this.grb_opcoes.TabStop = false;
            this.grb_opcoes.Text = "Opções";
            // 
            // rb_todosOsItensFiltrados
            // 
            this.rb_todosOsItensFiltrados.AutoSize = true;
            this.rb_todosOsItensFiltrados.Location = new System.Drawing.Point(8, 63);
            this.rb_todosOsItensFiltrados.Name = "rb_todosOsItensFiltrados";
            this.rb_todosOsItensFiltrados.Size = new System.Drawing.Size(234, 30);
            this.rb_todosOsItensFiltrados.TabIndex = 3;
            this.rb_todosOsItensFiltrados.TabStop = true;
            this.rb_todosOsItensFiltrados.Text = "Aplicar alteração de preço em todos os itens\r\nFiltrados?";
            this.rb_todosOsItensFiltrados.UseVisualStyleBackColor = true;
            // 
            // rb_somenteSelecionado
            // 
            this.rb_somenteSelecionado.AutoSize = true;
            this.rb_somenteSelecionado.Checked = true;
            this.rb_somenteSelecionado.Location = new System.Drawing.Point(7, 20);
            this.rb_somenteSelecionado.Name = "rb_somenteSelecionado";
            this.rb_somenteSelecionado.Size = new System.Drawing.Size(192, 30);
            this.rb_somenteSelecionado.TabIndex = 2;
            this.rb_somenteSelecionado.TabStop = true;
            this.rb_somenteSelecionado.Text = "Aplicar alteração de preço somente\r\nno item Selecionado?";
            this.rb_somenteSelecionado.UseVisualStyleBackColor = true;
            // 
            // frmMovimentoAlterar
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(534, 361);
            this.Name = "frmMovimentoAlterar";
            this.Text = "ALTERAR MOVIMENTO";
            this.scForm.Panel1.ResumeLayout(false);
            this.scForm.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.scForm)).EndInit();
            this.scForm.ResumeLayout(false);
            this.pnlForm.ResumeLayout(false);
            this.pnlForm.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.banner)).EndInit();
            this.flpAcao.ResumeLayout(false);
            this.grb_opcoes.ResumeLayout(false);
            this.grb_opcoes.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.FlowLayoutPanel flpAcao;
        private System.Windows.Forms.Button btn_fechar;
        private System.Windows.Forms.Button btn_confirma;
        private System.Windows.Forms.TextBox lblItem;
        private System.Windows.Forms.TextBox lblPrecoAtual;
        private System.Windows.Forms.TextBox textItem;
        private System.Windows.Forms.TextBox textPrecoAtual;
        private System.Windows.Forms.TextBox textPrecoNovo;
        private System.Windows.Forms.TextBox lblPrecoNovo;
        private System.Windows.Forms.GroupBox grb_opcoes;
        private System.Windows.Forms.RadioButton rb_todosOsItensFiltrados;
        private System.Windows.Forms.RadioButton rb_somenteSelecionado;
    }
}