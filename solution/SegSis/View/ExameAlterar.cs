﻿using SWS.Entidade;
using SWS.Facade;
using SWS.View.Resources;
using SWS.View.ViewHelper;
using SWS.ViewHelper;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace SWS.View
{
    public partial class frmExameAlterar : frmExameIncluir
    {
        public frmExameAlterar(Exame exame) :base()
        {
            InitializeComponent();

            this.exame = exame;

            text_descricao.Text = exame.Descricao;

            text_preco.Text = ValidaCampoHelper.FormataValorMonetario(Convert.ToString(exame.Preco));
            text_precoCusto.Text = ValidaCampoHelper.FormataValorMonetario(Convert.ToString(exame.Custo));

            text_prioridade.Text = Convert.ToString(exame.Prioridade);

            cbExterno.SelectedValue = exame.Externo == true ? "true" : "false";
            cbLaboratorio.SelectedValue = exame.Laboratorio == true ? "true" : "false";
            cbDocumento.SelectedValue = exame.LiberaDocumento == true ? "true" : "false";
            cbPadraoContrato.SelectedValue = exame.PadraoContrato == true ? "true" : "false";

            EstudoFacade pcmsoFacade = EstudoFacade.getInstance();

            MedicoExame = pcmsoFacade.findAllMedicoExameByExame(exame);
            MontaGridMedicoExame();

            textCodigoTuss.Text = exame.CodigoTuss;
            cbExameComplementar.SelectedValue = exame.ExameComplementar == true ? "true" : "false";

            if (exame.RelatorioExame != null)
            {
                RelatorioExameSelecionado = exame.RelatorioExame;
                textRelatorio.Text = RelatorioExameSelecionado.Relatorio.NmRelatorio;
            }

            cbPeriodoVencimento.SelectedValue = exame.PeriodoVencimento == null ? "6" : Convert.ToString(exame.PeriodoVencimento);
            
        }

        protected override void btn_gravar_Click(object sender, EventArgs e)
        {
            try
            {
                if (validaCamposObrigatorio())
                {
                    EstudoFacade pcmsoFacade = EstudoFacade.getInstance();

                    if (!pcmsoFacade.findExameInUsed(exame) || !pcmsoFacade.findExameInExameAso(exame))
                        exame = pcmsoFacade.updateExame(new Exame(exame.Id, text_descricao.Text.Trim(), exame.Situacao, Convert.ToBoolean(((SelectItem)cbLaboratorio.SelectedItem).Valor), Convert.ToDecimal(text_preco.Text), Convert.ToInt32(text_prioridade.Text), Convert.ToDecimal(text_precoCusto.Text), Convert.ToBoolean(((SelectItem)cbExterno.SelectedItem).Valor), Convert.ToBoolean(((SelectItem)cbDocumento.SelectedItem).Valor), textCodigoTuss.Text, Convert.ToBoolean(((SelectItem)cbExameComplementar.SelectedItem).Valor), Convert.ToInt32(((SelectItem)cbPeriodoVencimento.SelectedItem).Valor), Convert.ToBoolean(((SelectItem)cbPadraoContrato.SelectedItem).Valor)));
                    else
                    {
                        if (!String.Equals(exame.Descricao, text_descricao.Text.Trim()))
                        {
                            if (MessageBox.Show("O exame já foi utilizado em um estudo ou em um atendimento. O campo descrição não pode ser alterado. Deseja continuar com a alteração dos outros campos?", "Atençao", MessageBoxButtons.YesNo, MessageBoxIcon.Exclamation) == DialogResult.Yes)
                                exame = pcmsoFacade.updateExame(new Exame(exame.Id, exame.Descricao, exame.Situacao, (bool?)Convert.ToBoolean(((SelectItem)cbLaboratorio.SelectedItem).Valor), Convert.ToDecimal(text_preco.Text), Convert.ToInt32(text_prioridade.Text), Convert.ToDecimal(text_precoCusto.Text), Convert.ToBoolean(((SelectItem)cbExterno.SelectedItem).Valor), Convert.ToBoolean(((SelectItem)cbDocumento.SelectedItem).Valor), textCodigoTuss.Text, Convert.ToBoolean(((SelectItem)cbExameComplementar.SelectedItem).Valor), Convert.ToInt32(((SelectItem)cbPeriodoVencimento.SelectedItem).Valor), Convert.ToBoolean(((SelectItem)cbPadraoContrato.SelectedItem).Valor)));
                        }
                        else
                            exame = pcmsoFacade.updateExame(new Exame(exame.Id, text_descricao.Text.Trim(), exame.Situacao, (bool?)Convert.ToBoolean(((SelectItem)cbLaboratorio.SelectedItem).Valor), Convert.ToDecimal(text_preco.Text), Convert.ToInt32(text_prioridade.Text), Convert.ToDecimal(text_precoCusto.Text), Convert.ToBoolean(((SelectItem)cbExterno.SelectedItem).Valor), Convert.ToBoolean(((SelectItem)cbDocumento.SelectedItem).Valor), textCodigoTuss.Text, Convert.ToBoolean(((SelectItem)cbExameComplementar.SelectedItem).Valor), Convert.ToInt32(((SelectItem)cbPeriodoVencimento.SelectedItem).Valor), Convert.ToBoolean(((SelectItem)cbPadraoContrato.SelectedItem).Valor)));

                    }

                    MessageBox.Show("Exame alterado com sucesso.", "Confirmação",  MessageBoxButtons.OK, MessageBoxIcon.Information);

                    this.Close();
                }
            }
            catch (Exception Ex)
            {
                MessageBox.Show(Ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }

        protected override void btIncluirMedico_Click(object sender, EventArgs e)
        {
            try
            {
                frmMedicoBuscar medicoBuscar = new frmMedicoBuscar();
                medicoBuscar.ShowDialog();

                if (medicoBuscar.Medico != null)
                {
                    /* verificando se o médico já foi inserido na coleção */
                    MedicoExame medicoExameBusca = MedicoExame.Find(x => x.Medico.Id == medicoBuscar.Medico.Id);

                    if (medicoExameBusca != null)
                        throw new Exception("Médico já inserido na coleção.");

                    EstudoFacade pcmsoFacade = EstudoFacade.getInstance();

                    MedicoExame.Add(pcmsoFacade.insertMedicoExame(new MedicoExame(null, exame, medicoBuscar.Medico, null)));
                    MontaGridMedicoExame();
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }

        protected override void btExcluirMedico_Click(object sender, EventArgs e)
        {
            try
            {
                MedicoExame medicoExameExcluir = MedicoExame.Find(x => x.Medico.Id == (long)dgvMedico.CurrentRow.Cells[1].Value);
                
                EstudoFacade pcmsoFacade = EstudoFacade.getInstance();
                pcmsoFacade.deleteMedicoExame(medicoExameExcluir);
                
                MedicoExame.Remove(medicoExameExcluir);
                dgvMedico.Rows.RemoveAt((int)dgvMedico.CurrentRow.Index);

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }

        protected override void btnRelatorio_Click(object sender, EventArgs e)
        {
            try
            {
                frmExameRelatorio selecionarRelatorio = new frmExameRelatorio();
                selecionarRelatorio.ShowDialog();

                if (selecionarRelatorio.RelatorioSelecionado != null)
                {
                    RelatorioFacade relatorioFacade = RelatorioFacade.getInstance();

                    /* verificando se o relatório selecionado é diferente do que consta já cadastrado */
                    if (RelatorioExameSelecionado != null && (RelatorioExameSelecionado.Relatorio.Id != selecionarRelatorio.RelatorioSelecionado.Id))
                    {
                        /* o relatório atual será desativado */
                        relatorioFacade.updateRelatorioExame(new RelatorioExame(RelatorioExameSelecionado.IdRelatorioExame, RelatorioExameSelecionado.Exame, RelatorioExameSelecionado.Relatorio, ApplicationConstants.DESATIVADO, DateTime.Now));

                        /* inclusão de um novo relatorio exame */
                        RelatorioExameSelecionado = relatorioFacade.insertRelatorioExame(new RelatorioExame(null, exame, selecionarRelatorio.RelatorioSelecionado, ApplicationConstants.ATIVO, DateTime.Now));
                        textRelatorio.Text = RelatorioExameSelecionado.Relatorio.NmRelatorio;

                    }
                    else
                    {
                        /* inclusão de um novo relatorio exame */
                        RelatorioExameSelecionado = relatorioFacade.insertRelatorioExame(new RelatorioExame(null, exame, selecionarRelatorio.RelatorioSelecionado, ApplicationConstants.ATIVO, DateTime.Now));
                        textRelatorio.Text = RelatorioExameSelecionado.Relatorio.NmRelatorio;

                    }

                }

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }


        }
    }
}
