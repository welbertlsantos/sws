﻿using SWS.Entidade;
using SWS.Excecao;
using SWS.Facade;
using SWS.Helper;
using SWS.View.Resources;
using SWS.View.ViewHelper;
using SWS.ViewHelper;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace SWS.View
{
    public partial class frmMedicoAlterar : frmMedicoIncluir
    {
        public frmMedicoAlterar(Medico medico):base()
        {
            InitializeComponent();
            
            this.medico = medico;

            ComboHelper.unidadeFederativa(cb_uf);
            this.cb_uf.SelectedValue = medico.Uf;
            
            if (medico.Cidade != null)
            {
                cbCidade.SelectedValue = medico.Cidade.Id.ToString();
            }

            /* preenchendo informações do arquivo */

            if (medico.Assinatura != null)
            {
                assinatura = new Arquivo(null, String.Empty, medico.Mimetype, 0, medico.Assinatura);
                pbAssinatura.Image = FileHelper.byteArrayToImage(medico.Assinatura);
            }

            this.text_nome.Text = medico.Nome;
            this.text_crm.Text = medico.Crm;
            this.text_telefone1.Text = medico.Telefone1;
            this.text_telefone2.Text = medico.Telefone2;
            this.text_email.Text = medico.Email;
            this.text_endereco.Text = medico.Endereco;
            this.text_numero.Text = medico.Numero;
            this.text_complemento.Text = medico.Complemento;
            this.text_bairro.Text = medico.Bairro;
            this.text_cep.Text = medico.Cep;
            this.textPisPasep.Text = medico.Pispasep;
            this.textCpf.Text = medico.Cpf;


            ComboHelper.unidadeFederativa(cbUfCrm);
            cbUfCrm.SelectedValue = medico.UfCrm;

            textDocumentoExtra.Text = medico.DocumentoExtra;

        }

        protected override void bt_gravar_Click(object sender, EventArgs e)
        {
            this.IncluirMedicoErrorProvider.Clear();

            try
            {
                if (validaCamposObrigatorios())
                {
                    EstudoFacade pcmsoFacade = EstudoFacade.getInstance();
                    ClienteFacade clienteFacade = ClienteFacade.getInstance();

                    if (pcmsoFacade.findMedicoEstudoByMedico(medico))
                        throw new MedicoException(MedicoException.msg2);

                    medico = pcmsoFacade.updateMedico(new Medico(medico.Id, text_nome.Text.Trim(), text_crm.Text, ApplicationConstants.ATIVO, text_telefone1.Text, text_telefone2.Text, text_email.Text, text_endereco.Text, text_numero.Text, text_complemento.Text, text_bairro.Text, text_cep.Text, ((SelectItem)cb_uf.SelectedItem).Valor.ToString(), cbCidade.SelectedIndex == -1 ? null : clienteFacade.findCidadeIbgeById(Convert.ToInt64(((SelectItem)cbCidade.SelectedItem).Valor)), textPisPasep.Text, ((SelectItem)cbUfCrm.SelectedItem).Valor, textDocumentoExtra.Text, assinatura == null ? null : assinatura.Conteudo, assinatura == null ? string.Empty : assinatura.Mimetype, textCpf.Text, textRqe.Text));

                    MessageBox.Show("Médico alterado com sucesso.", "Confirmação", MessageBoxButtons.OK, MessageBoxIcon.Information);
                            
                    this.Close();
                }
            }

            catch (Exception Ex)
            {
                MessageBox.Show(Ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
            finally
            {
                this.Cursor = Cursors.Default;

            }
        }
    }
}
