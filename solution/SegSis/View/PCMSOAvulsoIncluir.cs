﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using SWS.Entidade;
using SWS.View.ViewHelper;
using SWS.Facade;
using SWS.View.Resources;

namespace SWS.View
{
    public partial class frmPcmsoAvulsoIncluir : frmTemplate
    {

        protected Cliente cliente;

        public Cliente Cliente
        {
            get { return cliente; }
            set { cliente = value; }
        }

        protected Cliente clienteContratada;

        public Cliente ClienteContratada
        {
            get { return clienteContratada; }
            set { clienteContratada = value; }
        }

        protected Estudo estudo;

        public Estudo Estudo
        {
            get { return estudo; }
            set { estudo = value; }
        }

        protected Ghe ghe;
        protected GheSetor gheSetor;
        protected GheSetorClienteFuncao gheSetorClienteFuncao;

        protected GheFonte gheFonte;

        protected GheFonteAgente gheFonteAgente;

        protected GheFonteAgenteExame gheFonteAgenteExame;

        protected GheFonteAgenteExamePeriodicidade gheFonteAgenteExamePeriodicidade;

        protected MedicoEstudo coordenador;

        public MedicoEstudo Coordenador
        {
            get { return coordenador; }
            set { coordenador = value; }
        }
        
        public frmPcmsoAvulsoIncluir()
        {
            InitializeComponent();
            ComboHelper.grauRisco(cbGrauRisco);
            ActiveControl = btCliente;
            this.MaximizeBox = true;
            this.WindowState = FormWindowState.Maximized;
        }

        protected void btn_cliente_Click(object sender, EventArgs e)
        {
            try
            {
                frmClienteSelecionar formCliente = new frmClienteSelecionar(null, null, false, null, null, null, null, null, true);
                formCliente.ShowDialog();

                if (formCliente.Cliente != null)
                {
                    cliente = formCliente.Cliente;
                    textCliente.Text = cliente.RazaoSocial;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        protected void btn_fechar_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        protected void btn_contratada_Click(object sender, EventArgs e)
        {
            try
            {
                frmClienteSelecionar formCliente = new frmClienteSelecionar(null, null, null, null, null, null, null, null, true);
                formCliente.ShowDialog();

                if (formCliente.Cliente != null)
                {
                    /* verificando se o cliente contratante não é cliente principal */

                    if (formCliente.Cliente.Id == cliente.Id)
                        throw new Exception("O contrantante não pode ser o cliente principal do PCMSO.");

                    clienteContratada = formCliente.Cliente;
                    textContratada.Text = clienteContratada.RazaoSocial;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        protected void btn_ghe_incluir_Click(object sender, EventArgs e)
        {
            try
            {
                frmPcmsoAvulsoGheIncluir formPcmsoAvulsoGheIncluir = new frmPcmsoAvulsoGheIncluir(estudo);
                formPcmsoAvulsoGheIncluir.ShowDialog();

                if (formPcmsoAvulsoGheIncluir.Ghe != null)
                    montaGridGhe();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        protected void btn_ghe_excluir_Click(object sender, EventArgs e)
        {
            try
            {
                if (dgvGhe.CurrentRow == null)
                    throw new Exception("Selecione um GHE.");

                if (MessageBox.Show("Gostaria realmente de excluir o Ghe " + ghe.Descricao, "Confirmação", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                {
                    this.Cursor = Cursors.WaitCursor;

                    EstudoFacade pcmsoFacade = EstudoFacade.getInstance();

                    pcmsoFacade.deleteGhe(new Ghe((Int64)dgvGhe.CurrentRow.Cells[0].Value, estudo, dgvGhe.CurrentRow.Cells[2].Value.ToString(), Convert.ToInt32(dgvGhe.CurrentRow.Cells[3].Value), ApplicationConstants.DESATIVADO, dgvGhe.CurrentRow.Cells[4].Value.ToString()));

                    ghe = null;
                    gheSetor = null;
                    gheSetorClienteFuncao = null;
                    gheFonte = null;
                    gheFonteAgente = null;
                    gheFonteAgenteExame = null;
                    gheFonteAgenteExamePeriodicidade = null;
                    dgvSetor.Columns.Clear();
                    dgvFuncao.Columns.Clear();
                    dgvAgente.Columns.Clear();
                    dgvExame.Columns.Clear();
                    dgvPeriodicidade.Columns.Clear();
                    montaGridGhe();
                }

            }
            catch (Exception Ex)
            {
                MessageBox.Show(Ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }

        protected void montaGridGhe()
        {
            try
            {
                EstudoFacade pcmsoFacade = EstudoFacade.getInstance();

                DataSet ds = pcmsoFacade.findGheByPcmso(estudo);

                dgvGhe.Columns.Clear();

                dgvGhe.DataSource = ds.Tables[0].DefaultView;

                dgvGhe.Columns[0].HeaderText = "id_ghe";
                dgvGhe.Columns[0].Visible = false;

                dgvGhe.Columns[1].HeaderText = "id_estudo";
                dgvGhe.Columns[1].Visible = false;

                dgvGhe.Columns[2].HeaderText = "Descrição";
                dgvGhe.Columns[2].Visible = true;
                dgvGhe.Columns[2].ReadOnly = true;

                dgvGhe.Columns[3].HeaderText = "Nexp";
                dgvGhe.Columns[3].Visible = true;
                dgvGhe.Columns[3].DefaultCellStyle.BackColor = Color.White;

                dgvGhe.Columns[4].HeaderText = "Número PO";
                dgvGhe.Columns[4].Visible = true;
                dgvGhe.Columns[4].DefaultCellStyle.BackColor = Color.White;

                dgvSetor.Columns.Clear();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }

        protected void btn_setor_incluir_Click(object sender, EventArgs e)
        {
            try
            {
                if (dgvGhe.CurrentRow == null || ghe == null)
                    throw new Exception("Selecione um GHE.");

                frmPcmsoSetor formPcmsoAvulsoSetorSelecionar = new frmPcmsoSetor(ghe);
                formPcmsoAvulsoSetorSelecionar.ShowDialog();

                if (formPcmsoAvulsoSetorSelecionar.GheSetor.Count > 0)
                    montaGridGheSetor();

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        protected virtual void btn_setor_excluir_Click(object sender, EventArgs e)
        {
            try
            {
                if (dgvSetor.CurrentRow == null)
                    throw new Exception("Selecione um Setor.");

                if (MessageBox.Show(" Gostaria realmente de excluir o Setor " + gheSetor.Setor.Descricao, "Atenção", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                {
                    this.Cursor = Cursors.WaitCursor;
                    EstudoFacade pcmsoFacade = EstudoFacade.getInstance();

                    pcmsoFacade.deleteGheSetor(new GheSetor((Int64)dgvSetor.CurrentRow.Cells[0].Value, new Setor((long)dgvSetor.CurrentRow.Cells[1].Value, dgvSetor.CurrentRow.Cells[2].Value.ToString(), ApplicationConstants.ATIVO), new Ghe((Int64)dgvGhe.CurrentRow.Cells[0].Value, estudo, dgvGhe.CurrentRow.Cells[2].Value.ToString(), Convert.ToInt32(dgvGhe.CurrentRow.Cells[3].Value), ApplicationConstants.DESATIVADO, dgvGhe.CurrentRow.Cells[4].Value.ToString()), ApplicationConstants.DESATIVADO));

                    gheSetor = null;
                    gheSetorClienteFuncao = null;
                    dgvFuncao.Columns.Clear();
                    montaGridGheSetor();

                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }

        protected void montaGridGheSetor()
        {
            try
            {
                if (dgvGhe.CurrentRow == null)
                    throw new Exception("Selecione um GHE.");

                EstudoFacade pcmsoFacade = EstudoFacade.getInstance();

                dgvSetor.Columns.Clear();

                dgvSetor.DataSource = pcmsoFacade.findAllSetorByGhe(ghe).Tables[0].DefaultView;

                dgvSetor.Columns[0].HeaderText = "id_ghe_Setor";
                dgvSetor.Columns[0].Visible = false;

                dgvSetor.Columns[1].HeaderText = "id_setor";
                dgvSetor.Columns[1].Visible = false;

                dgvSetor.Columns[2].HeaderText = "Setor";

                dgvSetor.Columns[3].HeaderText = "Id_ghe";
                dgvSetor.Columns[3].Visible = false;

                dgvSetor.Columns[4].HeaderText = "Ghe";
                dgvSetor.Columns[4].Visible = false;

                dgvFuncao.Columns.Clear();


            }
            catch (Exception Ex)
            {
                MessageBox.Show(Ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        protected void btn_funcao_incluir_Click(object sender, EventArgs e)
        {
            try
            {
                if (dgvSetor.CurrentRow == null || gheSetor == null)
                    throw new Exception("Selecione um Setor.");

                frmPcmsoFuncao formPcmsoAvulsoFuncaoSelecionar = new frmPcmsoFuncao(cliente, gheSetor);
                formPcmsoAvulsoFuncaoSelecionar.ShowDialog();

                if (formPcmsoAvulsoFuncaoSelecionar.GheSetorClienteFuncoes.Count > 0)
                    montaGridFuncao();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        protected virtual void btn_funcao_excluir_Click(object sender, EventArgs e)
        {
            try
            {
                if (dgvFuncao.CurrentRow == null)
                    throw new Exception("Selecione uma Função.");

                if (gheSetorClienteFuncao == null)
                    throw new Exception("Selecione a função para excluir.");

                if (MessageBox.Show("Gostaria realmente de excluir a Função " + gheSetorClienteFuncao.ClienteFuncao.Funcao.Descricao + "do Setor selecionado?", "Atenção", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                {
                    this.Cursor = Cursors.WaitCursor;
                    EstudoFacade pcmsoFacade = EstudoFacade.getInstance();
                    pcmsoFacade.deleteGheSetorClienteFuncao(gheSetorClienteFuncao);
                    gheSetorClienteFuncao = null;
                    montaGridFuncao();

                }

            }
            catch (Exception Ex)
            {
                MessageBox.Show(Ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }

        protected void montaGridFuncao()
        {

            try
            {
                if (dgvSetor.CurrentRow == null)
                    throw new Exception("Selecione um Setor.");

                EstudoFacade pcmsoFacade = EstudoFacade.getInstance();

                dgvFuncao.Columns.Clear();

                dgvFuncao.DataSource = pcmsoFacade.findAllGheSetorClienteFuncaoByGheSetor(gheSetor).Tables[0].DefaultView;

                dgvFuncao.Columns[0].HeaderText = "id_ghe_setor_cliente_funcao";
                dgvFuncao.Columns[0].Visible = false;

                dgvFuncao.Columns[1].HeaderText = "id_ghe_setor";
                dgvFuncao.Columns[1].Visible = false;

                dgvFuncao.Columns[2].HeaderText = "Código Função";
                dgvFuncao.Columns[2].ReadOnly = true;
                dgvFuncao.Columns[2].Width = 50;

                dgvFuncao.Columns[3].HeaderText = "Descrição";
                dgvFuncao.Columns[3].ReadOnly = true;
                dgvFuncao.Columns[3].Width = 350;

                dgvFuncao.Columns[4].HeaderText = "Atividade";
                dgvFuncao.Columns[4].Visible = true;
                dgvFuncao.Columns[4].ReadOnly = true;

                dgvFuncao.Columns[5].HeaderText = "Espaço confinado";
                dgvFuncao.Columns[5].Visible = true;
                dgvFuncao.Columns[5].DefaultCellStyle.BackColor = Color.White;

                dgvFuncao.Columns[6].HeaderText = "Trabalho em altura";
                dgvFuncao.Columns[6].Visible = true;
                dgvFuncao.Columns[6].DefaultCellStyle.BackColor = Color.White;

                dgvFuncao.Columns[7].HeaderText = "id_seg_cliente_funcao";
                dgvFuncao.Columns[7].Visible = false;

                dgvFuncao.Columns[8].HeaderText = "id_comentario_funcao";
                dgvFuncao.Columns[8].Visible = false;

                dgvFuncao.Columns[9].HeaderText = "Serv. Eletricidade";
                dgvFuncao.Columns[9].Visible = true;
                dgvFuncao.Columns[9].DefaultCellStyle.BackColor = Color.White;

                DataGridViewButtonColumn btn = new DataGridViewButtonColumn();
                dgvFuncao.Columns.Add(btn);

                btn.HeaderText = "...";
                btn.Text = "...";
                btn.Name = "btAtividade";
                btn.UseColumnTextForButtonValue = true;
                dgvFuncao.Columns[10].DisplayIndex = 4;
                btn.FlatStyle = FlatStyle.Flat;
                
            }
            catch (Exception Ex)
            {
                MessageBox.Show(Ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        protected void btn_agente_incluir_Click(object sender, EventArgs e)
        {
            try
            {
                if (dgvGhe.CurrentRow == null || ghe == null)
                    throw new Exception("Selecione um GHE.");

                frmPcmsoAvulsoAgente incluirAgente = new frmPcmsoAvulsoAgente(ghe);
                incluirAgente.ShowDialog();

                montaGridAgente();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }

        protected virtual void btn_agente_excluir_Click(object sender, EventArgs e)
        {
            try
            {

                if (dgvAgente.CurrentRow == null)
                    throw new Exception("Selecione um Agente.");

                if (MessageBox.Show("Gostaria realmente de excluir o Agente: " + gheFonteAgente.Agente.Descricao + "?", "Atenção", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.Yes)
                {

                    this.Cursor = Cursors.WaitCursor;
                    EstudoFacade pcmsoFacade = EstudoFacade.getInstance();

                    pcmsoFacade.deleteGheFonteAgente(gheFonteAgente);
                    gheFonteAgente = null;
                    gheFonteAgenteExame = null;
                    gheFonteAgenteExamePeriodicidade = null;
                    montaGridAgente();
                    dgvExame.Columns.Clear();
                    dgvPeriodicidade.Columns.Clear();

                }
            }
            catch (Exception Ex)
            {
                MessageBox.Show(Ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }

        protected void montaGridAgente()
        {
            try
            {
                EstudoFacade pcmsoFacade = EstudoFacade.getInstance();

                dgvAgente.Columns.Clear();

                HashSet<GheFonte> gheFontes = pcmsoFacade.findAllGheFonteByGhe(ghe);
                List<GheFonte> gheFonteList = new List<GheFonte>();

                foreach (GheFonte gf in gheFontes)
                {
                    gheFonteList.Add(gf);
                }
                
                this.gheFonte = gheFonteList.Find(x => x.Fonte.Id == 1);

                if (gheFonte != null)
                {
                    DataSet ds = pcmsoFacade.findAllAgenteByGheFonte(gheFonte);

                    dgvAgente.DataSource = ds.Tables[0].DefaultView;

                    dgvAgente.Columns[0].HeaderText = "Id_ghe_fonte_agente";
                    dgvAgente.Columns[0].Visible = false;

                    dgvAgente.Columns[1].HeaderText = "Id_agente";
                    dgvAgente.Columns[1].Visible = false;

                    dgvAgente.Columns[2].HeaderText = "Agente";

                    dgvAgente.Columns[3].HeaderText = "id_ghe_fonte";
                    dgvAgente.Columns[3].Visible = false;

                    dgvAgente.Columns[4].HeaderText = "Risco";
                }

            }
            catch (Exception Ex)
            {
                MessageBox.Show(Ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        protected void btn_exame_incluir_Click(object sender, EventArgs e)
        {
            try
            {
                if (dgvAgente.CurrentRow == null || gheFonteAgente == null)
                    throw new Exception("Selecione um Agente.");

                frmPcmsoExamesIncluir inclurExames = new frmPcmsoExamesIncluir(gheFonteAgente);
                inclurExames.ShowDialog();

                montaGridExames();
                dgvPeriodicidade.Columns.Clear();

            }
            catch (Exception Ex)
            {
                MessageBox.Show(Ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        protected virtual void btn_exame_excluir_Click(object sender, EventArgs e)
        {
            try
            {

                if (dgvExame.CurrentRow == null || gheFonteAgenteExame == null)
                    throw new Exception("Selecione um Exame.");

                if (MessageBox.Show("Gostaria realmente de excluir o Exame: " + gheFonteAgenteExame.Exame.Descricao + "?", "Atenção", MessageBoxButtons.YesNo) == DialogResult.Yes)
                {
                    this.Cursor = Cursors.WaitCursor;
                    EstudoFacade pcmsoFacade = EstudoFacade.getInstance();
                    pcmsoFacade.deleteGheFonteAgenteExame(gheFonteAgenteExame);

                    gheFonteAgenteExame = null;
                    montaGridExames();
                    dgvPeriodicidade.Columns.Clear();

                }

            }
            catch (Exception Ex)
            {
                MessageBox.Show(Ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
             
        }

        protected void btn_exame_alterar_Click(object sender, EventArgs e)
        {
            try
            {

                EstudoFacade pcmsoFacade = EstudoFacade.getInstance();

                if (dgvExame.CurrentRow == null || gheFonteAgenteExame == null)
                    throw new Exception("Selecione um Exame.");

                frmPcmsoExamesAlterar examesAltera = new frmPcmsoExamesAlterar(gheFonteAgenteExame);
                examesAltera.ShowDialog();
                
                montaGridExames();

            }
            catch (Exception Ex)
            {
                MessageBox.Show(Ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
               
        }

        protected void montaGridExames()
        {
            try
            {
                dgvExame.Columns.Clear();

                EstudoFacade pcmsoFacade = EstudoFacade.getInstance();

                DataSet ds = pcmsoFacade.findAllExamesByGheFonteAgente(gheFonteAgente);

                dgvExame.DataSource = ds.Tables["Exames"].DefaultView;

                dgvExame.Columns[0].HeaderText = "idGheFonteAgenteExame";
                dgvExame.Columns[0].Visible = false;

                dgvExame.Columns[1].HeaderText = "Código Exame";
                dgvExame.Columns[1].ReadOnly = true;
                dgvExame.Columns[1].DisplayIndex = 0;
                dgvExame.Columns[1].Width = 50;

                dgvExame.Columns[2].HeaderText = "IdGheFonteAgente";
                dgvExame.Columns[2].Visible = false;

                dgvExame.Columns[3].HeaderText = "Idade Exame";
                dgvExame.Columns[3].ValueType = typeof(String);

                dgvExame.Columns[4].HeaderText = "Exame";
                dgvExame.Columns[4].ReadOnly = true;
                dgvExame.Columns[4].DisplayIndex = 1;
                dgvExame.Columns[4].Width = 350;

                dgvExame.Columns[5].HeaderText = "Situação";
                dgvExame.Columns[5].Visible = false;

                dgvExame.Columns[6].HeaderText = "Laboratório";
                dgvExame.Columns[6].ReadOnly = true;
                dgvExame.Columns[6].DisplayIndex = 2;

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        protected void btn_periodicidade_incluir_Click(object sender, EventArgs e)
        {
            try
            {
                if (dgvExame.CurrentRow == null || gheFonteAgenteExame == null)
                    throw new Exception("Selecione um Exame.");

                frmPcmsoPeriodicidade periodicidade = new frmPcmsoPeriodicidade(gheFonteAgenteExame);
                periodicidade.ShowDialog();
                MontaGridPeriodicidade();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        protected virtual void btn_periodicidade_excluir_Click(object sender, EventArgs e)
        {
            try
            {

                if (dgvPeriodicidade.CurrentRow == null || gheFonteAgenteExamePeriodicidade == null)
                    throw new Exception("Selecione uma Periodicidade.");


                if (MessageBox.Show("Gostaria realmente de excluir a Periodicidade: " + gheFonteAgenteExamePeriodicidade.Periodicidade.Descricao + "?", "Confirmação", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                {
                    this.Cursor = Cursors.WaitCursor;
                    EstudoFacade pcmsoFacade = EstudoFacade.getInstance();
                    pcmsoFacade.deleteGheFonteAgenteExamePeriodicidade(gheFonteAgenteExamePeriodicidade);
                    gheFonteAgenteExamePeriodicidade = null;
                    MontaGridPeriodicidade();

                }

            }
            catch (Exception Ex)
            {
                MessageBox.Show(Ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }

        protected void MontaGridPeriodicidade()
        {
            try
            {
                dgvPeriodicidade.Columns.Clear();

                EstudoFacade pcmsoFacade = EstudoFacade.getInstance();
                
                DataSet ds = pcmsoFacade.findAllPeriodicidadeByGheFonteAgenteExame((Int64)gheFonteAgenteExame.Id);

                //dgvPeriodicidade.DataSource = ds.Tables["Periodicidades"].DefaultView;

                dgvPeriodicidade.ColumnCount = 2;

                dgvPeriodicidade.Columns[0].HeaderText = "idPeriodicidade";
                dgvPeriodicidade.Columns[0].Visible = false;

                dgvPeriodicidade.Columns[1].HeaderText = "Periodicidade";
                dgvPeriodicidade.Columns[1].Width = 200;
                dgvPeriodicidade.Columns[1].ReadOnly = true;

                DataGridViewComboBoxColumn periodoVencimento = new DataGridViewComboBoxColumn();
                dgvPeriodicidade.Columns.Add(periodoVencimento);
                ComboHelper.periodoVencimentoCombo(periodoVencimento);
                dgvPeriodicidade.Columns[2].HeaderText = "Vencimento";
                dgvPeriodicidade.Columns[2].DefaultCellStyle.BackColor = Color.White;
                dgvPeriodicidade.Columns[2].Width = 150;

                foreach (DataRow dvRow in ds.Tables[0].Rows)
                    dgvPeriodicidade.Rows.Add(Convert.ToInt64(dvRow["id_periodicidade"]), dvRow["descricao"].ToString(), dvRow["periodo_vencimento"] == null ? "" : dvRow["periodo_vencimento"].ToString());

                dgvPeriodicidade.Columns[1].DisplayIndex = 1;
                dgvPeriodicidade.Columns[2].DisplayIndex = 2;

            }

            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
             
        }

        protected virtual void btCoordenador_Click(object sender, EventArgs e)
        {
            try
            {
                frmPcmsoMedico selecionarMedico = new frmPcmsoMedico();
                selecionarMedico.ShowDialog();

                if (selecionarMedico.Medico != null)
                {
                    coordenador = new MedicoEstudo(estudo, selecionarMedico.Medico, true, ApplicationConstants.ATIVO);
                    textCoordenador.Text = coordenador.Medico.Nome + "  CRM: " + coordenador.Medico.Crm;
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        protected void btIncluirEstimativa_Click(object sender, EventArgs e)
        {
            try
            {
                frmPcmsoEstimativa formEstimativa = new frmPcmsoEstimativa(estudo);
                formEstimativa.ShowDialog();
                montaGridEstimativa();

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
        }

        protected void btExcluirEstimativa_Click(object sender, EventArgs e)
        {
            try
            {
                if (dgvEstimativa.CurrentRow == null)
                    throw new Exception("Selecione uma estimativa para excluir.");

                EstudoFacade pcmsoFacade = EstudoFacade.getInstance();

                pcmsoFacade.deleteEstimativaEstudo(new EstimativaEstudo(Convert.ToInt64(dgvEstimativa.CurrentRow.Cells[0].Value),
                                estudo, new Estimativa(Convert.ToInt64(dgvEstimativa.CurrentRow.Cells[1].Value), (String)dgvEstimativa.CurrentRow.Cells[3].Value, String.Empty),
                                Convert.ToInt32(dgvEstimativa.CurrentRow.Cells[4].EditedFormattedValue)));

                montaGridEstimativa();


            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
        }

        protected void montaGridEstimativa()
        {
            this.dgvEstimativa.Columns.Clear();

            EstudoFacade pcmsoFacade = EstudoFacade.getInstance();

            DataSet ds = pcmsoFacade.findAllEstimativaInEstudo(estudo);

            dgvEstimativa.DataSource = ds.Tables[0].DefaultView;

            dgvEstimativa.Columns[0].HeaderText = "Id_Estimativa_estudo";
            dgvEstimativa.Columns[0].Visible = false;

            dgvEstimativa.Columns[1].HeaderText = "Id_Estimativa";
            dgvEstimativa.Columns[1].Visible = false;

            dgvEstimativa.Columns[2].HeaderText = "Id Estudo";
            dgvEstimativa.Columns[2].Visible = false;

            dgvEstimativa.Columns[3].HeaderText = "Estimativa";
            dgvEstimativa.Columns[3].ReadOnly = true;

            dgvEstimativa.Columns[4].HeaderText = "Qtde";
            dgvEstimativa.Columns[4].DefaultCellStyle.BackColor = Color.White;

        }

        protected virtual void btn_pcmso_incluir_Click(object sender, EventArgs e)
        {
            try
            {
                ClienteFacade clienteFacade = ClienteFacade.getInstance();
                
                EstudoFacade pcmsoFacade = EstudoFacade.getInstance();

                if (cliente == null)
                    throw new Exception("Selecione um Cliente.");

                /* regra de manter um pcmso avulso para o cliente não existe mais porque não temos
                 * mais o módulo pcmso. - 18/05/2019 - sprint 1 */

                if (pcmsoFacade.verificaExistePcmsoAvusoCadastrado(cliente, clienteContratada))
                {
                    if (MessageBox.Show("Já existe um Pcmso Avulso para esse cliente, deseja continuar", "Atenção", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                    {
                        estudo = new Estudo();
                        estudo.ClienteContratante = clienteContratada;
                        estudo.TipoEstudo = true;
                        estudo.Avulso = true;
                        estudo.DataCriacao = dataCriacao.Checked ? Convert.ToDateTime(dataCriacao.Text) : (DateTime?)null;
                        estudo.DataVencimento = dataValidade.Checked ? Convert.ToDateTime(dataValidade.Text) : (DateTime?)null;
                        estudo.Situacao = ApplicationConstants.FECHADO;
                        estudo.GrauRisco = Convert.ToString(cbGrauRisco.SelectedValue);
                        estudo.VendedorCliente = clienteFacade.findClienteVendedorByCliente(cliente).Find(x => String.Equals(x.Situacao, ApplicationConstants.ATIVO));
                        estudo.NumContrato = textNumeroContrato.Text;
                        estudo.DataInicioContrato = textInicioContrato.Text;
                        estudo.DataFimContrato = textFimContrato.Text;
                        estudo.Obra = textObra.Text.Trim();
                        this.Cursor = Cursors.WaitCursor;

                        if (estudo.ClienteContratante != null)
                            if (string.IsNullOrEmpty(textNumeroContrato.Text))
                                throw new Exception("Existe um contratante. O número do contrato é obrigatório.");

                        estudo = pcmsoFacade.insertPcmso(estudo, null);
                        /* gravando coordenador do estudo */
                        if (coordenador != null)
                        {
                            coordenador.Pcmso = estudo;
                            pcmsoFacade.insertMedicoEstudo(coordenador);
                        }

                        this.textCodigo.Text = estudo.CodigoEstudo;

                        habilitaControles();

                        /* desativando os controles */

                        btCliente.Enabled = false;
                        btContratada.Enabled = false;
                        btIcluir.Enabled = false;
                        btn_pcmso_limpar.Enabled = false;

                    }
                }
                else
                {
                    /* pcmso novo */
                    estudo = new Estudo();
                    estudo.ClienteContratante = clienteContratada;
                    estudo.TipoEstudo = true;
                    estudo.Avulso = true;
                    estudo.DataCriacao = dataCriacao.Checked ? Convert.ToDateTime(dataCriacao.Text) : (DateTime?)null;
                    estudo.DataVencimento = dataValidade.Checked ? Convert.ToDateTime(dataValidade.Text) : (DateTime?)null;
                    estudo.Situacao = ApplicationConstants.FECHADO;
                    estudo.GrauRisco = Convert.ToString(cbGrauRisco.SelectedValue);
                    estudo.VendedorCliente = clienteFacade.findClienteVendedorByCliente(cliente).Find(x => String.Equals(x.Situacao, ApplicationConstants.ATIVO));
                    estudo.NumContrato = textNumeroContrato.Text;
                    estudo.DataInicioContrato = textInicioContrato.Text;
                    estudo.DataFimContrato = textFimContrato.Text;
                    estudo.Obra = textObra.Text.Trim();
                    this.Cursor = Cursors.WaitCursor;

                    if (estudo.ClienteContratante != null)
                        if (string.IsNullOrEmpty(textNumeroContrato.Text))
                            throw new Exception("Existe um contratante. O número do contrato é obrigatório.");

                    estudo = pcmsoFacade.insertPcmso(estudo, null);
                    /* gravando coordenador do estudo */
                    if (coordenador != null)
                    {
                        coordenador.Pcmso = estudo;
                        pcmsoFacade.insertMedicoEstudo(coordenador);
                    }

                    this.textCodigo.Text = estudo.CodigoEstudo;

                    habilitaControles();

                    /* desativando os controles */

                    btCliente.Enabled = false;
                    btContratada.Enabled = false;
                    btIcluir.Enabled = false;
                    btn_pcmso_limpar.Enabled = false;

                }

            }
            catch (Exception Ex)
            {
                MessageBox.Show(Ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }

        protected void btn_pcmso_limpar_Click(object sender, EventArgs e)
        {
            this.cliente = null;
            this.clienteContratada = null;
            this.textCliente.Text = String.Empty;
            this.textContratada.Text = String.Empty;
        }

        protected void grd_ghe_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            ghe = new Ghe((Int64)dgvGhe.CurrentRow.Cells[0].Value, estudo, dgvGhe.CurrentRow.Cells[2].Value.ToString(), Convert.ToInt32(dgvGhe.CurrentRow.Cells[3].Value), ApplicationConstants.ATIVO, dgvGhe.CurrentRow.Cells[4].Value.ToString());
            montaGridGheSetor();
            montaGridAgente();
            gheFonteAgenteExame = null;
            dgvExame.Columns.Clear();
            gheFonteAgenteExamePeriodicidade = null;
            dgvPeriodicidade.Columns.Clear();
        }

        protected void grd_setor_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            gheSetor = new GheSetor((Int64)dgvSetor.CurrentRow.Cells[0].Value, new Setor((long)dgvSetor.CurrentRow.Cells[1].Value, dgvSetor.CurrentRow.Cells[2].Value.ToString(), ApplicationConstants.ATIVO), ghe, ApplicationConstants.ATIVO);
            this.montaGridFuncao();
        }

        protected void grd_agente_CellClick(object sender, DataGridViewCellEventArgs e)
        {

            EstudoFacade pcmsoFacade = EstudoFacade.getInstance();
            gheFonteAgente = pcmsoFacade.findGheFonteAgenteById((Int64)dgvAgente.CurrentRow.Cells[0].Value);
            montaGridExames();
            dgvPeriodicidade.Columns.Clear();

        }

        protected void grd_exame_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            EstudoFacade pcmsoFacade = EstudoFacade.getInstance();
            gheFonteAgenteExame = pcmsoFacade.findGheFonteAgenteExameById((Int64)dgvExame.CurrentRow.Cells[0].Value);
            MontaGridPeriodicidade();

        }

        protected void dgvEstimativa_CellEndEdit(object sender, DataGridViewCellEventArgs e)
        {
            try
            {

                /* validando o valor digitado caso o usuário não tenha digitado nenhuma informação o campo
                 * obrigatoriamente ficará com zero */

                if (String.IsNullOrEmpty(dgvEstimativa.Rows[e.RowIndex].Cells[e.ColumnIndex].EditedFormattedValue.ToString().Trim()))
                {
                    this.dgvEstimativa.Rows[e.RowIndex].Cells[e.ColumnIndex].Value = 0;
                }

                /* gravando a informação no banco de dados */

                EstudoFacade pcmsoFacade = EstudoFacade.getInstance();


                pcmsoFacade.updateEstimativaEstudo(new EstimativaEstudo(Convert.ToInt64(dgvEstimativa.Rows[e.RowIndex].Cells[0].Value),
                                estudo, new Estimativa(Convert.ToInt64(dgvEstimativa.Rows[e.RowIndex].Cells[1].Value), (String)dgvEstimativa.Rows[e.RowIndex].Cells[3].Value, String.Empty),
                                Convert.ToInt32(dgvEstimativa.Rows[e.RowIndex].Cells[4].EditedFormattedValue)));


            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
        }

        protected void dgvEstimativa_DataError(object sender, DataGridViewDataErrorEventArgs e)
        {
            try
            {
                /*validando valor digitado para não dar estouro de inteiro32 */

                int number = Int32.Parse((String)dgvEstimativa.Rows[e.RowIndex].Cells[e.ColumnIndex].EditedFormattedValue);
            }

            catch (OverflowException)
            {
                MessageBox.Show("Número maior do que o permitido para o campo. ", "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
        }

        protected void dgvEstimativa_EditingControlShowing(object sender, DataGridViewEditingControlShowingEventArgs e)
        {
            if (e.Control is DataGridViewTextBoxEditingControl)
            {
                e.Control.KeyPress += new KeyPressEventHandler(dgvEstimativa_KeyPress);

            }
        }

        protected void dgvEstimativa_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!Char.IsDigit(e.KeyChar) && e.KeyChar != (char)8)
            {
                e.Handled = true;
            }
        }

        protected void dgvGhe_DataBindingComplete(object sender, DataGridViewBindingCompleteEventArgs e)
        {
            DataGridView gridView = sender as DataGridView;
            gridView.ClearSelection();
        }

        protected void dgvSetor_DataBindingComplete(object sender, DataGridViewBindingCompleteEventArgs e)
        {
            DataGridView dgv = sender as DataGridView;
            dgv.ClearSelection();
        }

        protected void dgvFuncao_DataBindingComplete(object sender, DataGridViewBindingCompleteEventArgs e)
        {
            DataGridView dgv = sender as DataGridView;
            dgv.ClearSelection();
        }

        protected void grd_periodicidade_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            EstudoFacade pcmsoFacade = EstudoFacade.getInstance();
            gheFonteAgenteExamePeriodicidade = new GheFonteAgenteExamePeriodicidade(new Periodicidade((Int64)dgvPeriodicidade.CurrentRow.Cells[0].Value, dgvPeriodicidade.CurrentRow.Cells[1].Value.ToString(), null), gheFonteAgenteExame, ApplicationConstants.ATIVO, dgvPeriodicidade.CurrentRow.Cells[2].Value == string.Empty ? (int?)null : Convert.ToInt32(dgvPeriodicidade.CurrentRow.Cells[2].Value));
        }

        protected void dgvAgente_DataBindingComplete(object sender, DataGridViewBindingCompleteEventArgs e)
        {
            DataGridView dgv = sender as DataGridView;
            dgv.ClearSelection();
        }

        protected void dgvExame_DataBindingComplete(object sender, DataGridViewBindingCompleteEventArgs e)
        {
            DataGridView dgv = sender as DataGridView;
            dgv.ClearSelection();
        }

        protected void dgvPeriodicidade_DataBindingComplete(object sender, DataGridViewBindingCompleteEventArgs e)
        {
            DataGridView dgv = sender as DataGridView;
            dgv.ClearSelection();
        }

        protected void dgvGhe_CellEndEdit(object sender, DataGridViewCellEventArgs e)
        {
            try
            {

                /* validando o valor digitado caso o usuário não tenha digitado nenhuma informação o campo
                 * obrigatoriamente ficará com zero */

                if (e.ColumnIndex == 3) {
                if (String.IsNullOrEmpty(dgvGhe.Rows[e.RowIndex].Cells[e.ColumnIndex].EditedFormattedValue.ToString().Trim()))
                    this.dgvGhe.Rows[e.RowIndex].Cells[e.ColumnIndex].Value = 0;
                }

                /* gravando a informação no banco de dados */

                EstudoFacade pcmsoFacade = EstudoFacade.getInstance();

                pcmsoFacade.updateGhe(new Ghe(Convert.ToInt64(dgvGhe.Rows[e.RowIndex].Cells[0].Value), estudo,
                                 dgvGhe.Rows[e.RowIndex].Cells[2].Value.ToString(),
                                 Convert.ToInt32(dgvGhe.Rows[e.RowIndex].Cells[3].EditedFormattedValue), ApplicationConstants.ATIVO, dgvGhe.Rows[e.RowIndex].Cells[4].Value.ToString()));


            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }

        }

        protected void dgvGhe_DataError(object sender, DataGridViewDataErrorEventArgs e)
        {
            try
            {
                /*validando valor digitado para não dar estouro de inteiro32 */

                int number = Int32.Parse((String)dgvGhe.Rows[e.RowIndex].Cells[3].EditedFormattedValue);
            }

            catch (OverflowException)
            {
                MessageBox.Show("Número maior do que o permitido para o campo.", "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }   

        }

        protected void dgvGhe_EditingControlShowing(object sender, DataGridViewEditingControlShowingEventArgs e)
        {
            if (e.Control is DataGridViewTextBoxEditingControl)
                e.Control.KeyPress += new KeyPressEventHandler(dgvGhe_KeyPress);
        }

        protected void dgvGhe_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = ValidaCampoHelper.FormataCampoNumerico(sender, e);
        }

        protected void habilitaControles()
        {
            grbEstimativa.Enabled = true;
            flpEstimativa.Enabled = true;
            btIncluirEstimativa.Enabled = true;
            btExcluirEstimativa.Enabled = true;

            grbGhe.Enabled = true;
            flpGhe.Enabled = true;
            btGheIncluir.Enabled = true;
            btGheExcluir.Enabled = true;
            btnAlterarGhe.Enabled = true;
            btnDuplicar.Enabled = true;

            grbSetor.Enabled = true;
            flpSetor.Enabled = true;
            btSetorIncluir.Enabled = true;
            btSetorExcluir.Enabled = true;

            grbFuncao.Enabled = true;
            flpFuncao.Enabled = true;
            btFuncaoIncluir.Enabled = true;
            btFuncaoExcluir.Enabled = true;

            grbAgente.Enabled = true;
            flpAgente.Enabled = true;
            btAgenteIncluir.Enabled = true;
            btAgenteExcluir.Enabled = true;

            grbExame.Enabled = true;
            flpExame.Enabled = true;
            btExameIncluir.Enabled = true;
            btExameExcluir.Enabled = true;
            btExameAlterar.Enabled = true;

            grbPeriodicidade.Enabled = true;
            flpPeriodicidade.Enabled = true;
            btPeriodicidadeIncluir.Enabled = true;
            btPeriodicidadeExcluir.Enabled = true;
        }

        private void frmPcmsoAvulsoIncluir_KeyDown(object sender, KeyEventArgs e)
        {
            if ((e.KeyCode == Keys.Enter) || (e.KeyCode == Keys.Return))
            {
                SendKeys.Send("{TAB}");
            }
        }

        private void desabilitaSort(DataGridView dataGridView)
        {
            foreach (DataGridViewColumn column in dataGridView.Columns)
                column.SortMode = DataGridViewColumnSortMode.NotSortable;
        }

        private void dgvFuncao_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                if (e.RowIndex != -1)
                {
                    gheSetorClienteFuncao = new GheSetorClienteFuncao((Int64)dgvFuncao.Rows[e.RowIndex].Cells[0].Value, new ClienteFuncao((Int64)dgvFuncao.Rows[e.RowIndex].Cells[7].Value, cliente, new Funcao((Int64)dgvFuncao.Rows[e.RowIndex].Cells[2].Value, dgvFuncao.Rows[e.RowIndex].Cells[3].Value.ToString(), String.Empty, ApplicationConstants.ATIVO, dgvFuncao.Rows[e.RowIndex].Cells[4].Value.ToString()), ApplicationConstants.ATIVO, null, null), gheSetor, (Boolean)dgvFuncao.Rows[e.RowIndex].Cells[5].Value, (Boolean)dgvFuncao.Rows[e.RowIndex].Cells[6].Value, ApplicationConstants.ATIVO, dgvFuncao.Rows[e.RowIndex].Cells[8].Value == DBNull.Value ? null : new ComentarioFuncao((long)dgvFuncao.Rows[e.RowIndex].Cells[8].Value, null, dgvFuncao.Rows[e.RowIndex].Cells[4].ToString(), string.Empty), (bool)dgvFuncao.Rows[e.RowIndex].Cells[9].Value);

                    EstudoFacade pcmsoFacade = EstudoFacade.getInstance();

                    // dispara evento para clique do botão na gride de funções
                    if (e.ColumnIndex == 10)
                    {
                        frmFuncaoComentario comentario = new frmFuncaoComentario(new Funcao((Int64)dgvFuncao.Rows[e.RowIndex].Cells[2].Value, dgvFuncao.Rows[e.RowIndex].Cells[3].Value.ToString(), String.Empty, ApplicationConstants.ATIVO, dgvFuncao.Rows[e.RowIndex].Cells[4].Value.ToString()));
                        comentario.ShowDialog();

                        if (comentario.ComentarioFuncao != null)
                        {
                            gheSetorClienteFuncao.ComentarioFuncao = comentario.ComentarioFuncao;
                            pcmsoFacade.updateGheSetorClienteFuncao(gheSetorClienteFuncao);
                            montaGridFuncao();
                        }

                    }

                    if (e.ColumnIndex == 5 || e.ColumnIndex == 6 || e.ColumnIndex == 9)
                    {
                        /* atualizar status do trabalho em altura ou espaço confinado e eletricidade */

                        dgvFuncao.EndEdit();

                        gheSetorClienteFuncao.Altura = Convert.ToBoolean(dgvFuncao.Rows[e.RowIndex].Cells[6].EditedFormattedValue);
                        gheSetorClienteFuncao.Confinado = Convert.ToBoolean(dgvFuncao.Rows[e.RowIndex].Cells[5].EditedFormattedValue);
                        gheSetorClienteFuncao.Eletricidade = Convert.ToBoolean(dgvFuncao.Rows[e.RowIndex].Cells[9].EditedFormattedValue);

                        pcmsoFacade.updateGheSetorClienteFuncao(gheSetorClienteFuncao);

                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        protected void btnAlterarGhe_Click(object sender, EventArgs e)
        {
            try
            {
                if (dgvGhe.CurrentRow == null)
                    throw new Exception("Selecione um GHE para alterar.");

                if (ghe == null)
                    throw new Exception("Selecione primeiro o GHE.");

                frmPcmsoAvulsoGheAlterar alterarGhe = new frmPcmsoAvulsoGheAlterar(ghe);
                alterarGhe.ShowDialog();
                
                montaGridGhe();

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }

        private void dgvPeriodicidade_CellEndEdit(object sender, DataGridViewCellEventArgs e)
        {
            if (e.ColumnIndex == 2)
            {
                EstudoFacade pcmsoFacade = EstudoFacade.getInstance();
                pcmsoFacade.updateGheFonteAgenteExamePeriodicidade(new GheFonteAgenteExamePeriodicidade(new Periodicidade((long)dgvPeriodicidade.Rows[e.RowIndex].Cells[0].Value, dgvPeriodicidade.Rows[e.RowIndex].Cells[1].Value.ToString(), null), gheFonteAgenteExame, ApplicationConstants.ATIVO, dgvPeriodicidade.Rows[e.RowIndex].Cells[2].Value == string.Empty ? (int?)null : Convert.ToInt32(dgvPeriodicidade.Rows[e.RowIndex].Cells[2].Value)));
            }
        }

        private void btnDuplicar_Click(object sender, EventArgs e)
        {
            try
            {
                if (ghe == null)
                    throw new Exception("Selecione o ghe que deseja replicar.");

                frmPcmsoAvulsoGheIncluir incluirGhe = new frmPcmsoAvulsoGheIncluir(estudo);
                incluirGhe.ShowDialog();

                if (incluirGhe.Ghe != null && incluirGhe.Ghe.Id != null)
                {
                    /* replicar o Ghe */
                    this.Cursor = Cursors.WaitCursor;
                    EstudoFacade pcmsoFacade = EstudoFacade.getInstance();

                    pcmsoFacade.replicarGhe(incluirGhe.Ghe, ghe);

                    MessageBox.Show("Ghe replicado com sucesso.", "Confirmação", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    montaGridGhe();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }

        }

    }
}
