﻿using SWS.Entidade;
using SWS.Facade;
using SWS.View.Resources;
using SWS.ViewHelper;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace SWS.View
{
    public partial class frmAgentesAlterar : frmAgentesIncluir
    {
        public frmAgentesAlterar(Agente agente) :base()
        {
            InitializeComponent();
            this.agente = agente;

            textDescricao.Text = agente.Descricao;
            textTrajetoria.Text = agente.Trajetoria;
            textDano.Text = agente.Dano;
            textLimite.Text = agente.Limite;
            cbRisco.SelectedValue = agente.Risco.Id;
            cbRisco.Text = agente.Risco.Descricao;
            textIntensidade.Text = agente.Intensidade;
            textTecnica.Text = agente.Tecnica;
            textCodigoEsocial.Text = agente.CodigoEsocial;
        }

        protected override void btGravar_Click(object sender, EventArgs e)
        {
            try
            {
                this.errorAgenteIncluir.Clear();
                if (validaCamposObrigatorio())
                {

                    EstudoFacade PcmsoFacade = EstudoFacade.getInstance();

                    agente = PcmsoFacade.updateAgente(new Agente((Int64?)agente.Id, new Risco(Convert.ToInt64(((SelectItem)cbRisco.SelectedItem).Valor), ((SelectItem)cbRisco.SelectedItem).Nome), textDescricao.Text, textTrajetoria.Text, textDano.Text, textLimite.Text, ApplicationConstants.ATIVO, false, textIntensidade.Text, textTecnica.Text, textCodigoEsocial.Text));

                    MessageBox.Show("Agente alterado com sucesso.", "Confirmação", MessageBoxButtons.OK, MessageBoxIcon.Information);

                    this.Close();

                }

            }
            catch (Exception ex) 
            { 
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
    }
}
