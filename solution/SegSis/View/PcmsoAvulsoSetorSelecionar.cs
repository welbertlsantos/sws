﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using SegSis.Entidade;
using SegSis.Facade;
using SegSis.View.Resources;

namespace SegSis.View
{
    public partial class frm_PcmsoAvulsoSetorSelecionar : BaseFormConsulta
    {
        frm_PcmsoAvulso formPcmsoAvulso;
        
        Setor setor = new Setor();
        
        public static String Msg1 = "Selecione pelo menos uma fonte";
        public static String Msg2 = "Atenção";

        public void setSetor(Setor setor)
        {
            this.setor = setor;
        }

        public frm_PcmsoAvulsoSetorSelecionar(frm_PcmsoAvulso formPcmsoAvulso)
        {
            InitializeComponent();
            this.formPcmsoAvulso = formPcmsoAvulso;
            validaPermissoes();
        }

        private void validaPermissoes()
        {
            PermissionamentoFacade permissionamentoFacade = PermissionamentoFacade.getInstance();
            this.btn_novo.Enabled = permissionamentoFacade.hasPermission("SETOR", "INCLUIR");
        }

        

        public void montaDataGrid()
        {
            try
            {
                PpraFacade ppraFacade = PpraFacade.getInstance();
                
                setor.setDescricao(Convert.ToString(text_descricao.Text));

                DataSet ds = ppraFacade.findAllSetorAtivo(setor, formPcmsoAvulso.ghe, null);
                
                grd_setor.DataSource = ds.Tables["Setores"].DefaultView;

                grd_setor.Columns[0].HeaderText = "id_setor";
                grd_setor.Columns[0].Visible = false;

                grd_setor.Columns[1].HeaderText = "Descrição";
                grd_setor.Columns[1].ReadOnly = true;
                
                grd_setor.Columns[2].HeaderText = "Situação";
                grd_setor.Columns[2].Visible = false;

                grd_setor.Columns[3].HeaderText = "Selec";
                grd_setor.Columns[3].DisplayIndex = 0;

            }
            catch (Exception Ex)
            {
                MessageBox.Show(Ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
        }

        private void btn_buscar_Click(object sender, EventArgs e)
        {
            try
            {
                this.grd_setor.Columns.Clear();
                montaDataGrid();
                text_descricao.Focus();

            }
            catch (Exception Ex)
            {
                MessageBox.Show(Ex.Message);
            }
        }

        private void btn_incluir_Click(object sender, EventArgs e)
        {
            try
            {
                PcmsoFacade pcmsoFacade = PcmsoFacade.getInstance();

                Setor setorSelecionado = new Setor();

                if (grd_setor.CurrentRow != null)
                {
                    foreach (DataGridViewRow dvRow in grd_setor.Rows)
                    {
                        if ((Boolean)dvRow.Cells[3].Value)
                        {
                            Int64 id = (Int64)dvRow.Cells[0].Value;
                            String descricao = (String)dvRow.Cells[1].Value;

                            setorSelecionado.setId(id);
                            setorSelecionado.setDescricao(descricao);

                            GheSetor gheSetor = new GheSetor(null, setorSelecionado, formPcmsoAvulso.ghe, ApplicationConstants.ATIVO);

                            pcmsoFacade.incluirGheSetor(gheSetor);
                        }
                    }

                    formPcmsoAvulso.montaGridGheSetor();

                    this.Close();
                }
                else
                {
                    MessageBox.Show(Msg1, Msg2);
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void btn_novo_Click(object sender, EventArgs e)
        {
            frm_setor_incluir formSetorIncluir = new frm_setor_incluir();
            formSetorIncluir.ShowDialog();

            if (formSetorIncluir.getSetor() != null)
            {
                text_descricao.Text = formSetorIncluir.getSetor().getDescricao().ToUpper();
                btn_buscar.PerformClick();
            }
            

            try
            {
                PcmsoFacade pcmsoFacade = PcmsoFacade.getInstance();
                
                if (setor.getId() != null)
                {
                    GheSetor gheSetor = new GheSetor(null, setor, formPcmsoAvulso.ghe, ApplicationConstants.ATIVO);

                    pcmsoFacade.incluirGheSetor(gheSetor);

                    formPcmsoAvulso.montaGridGheSetor();
                    
                    this.Close();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void btn_fechar_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
