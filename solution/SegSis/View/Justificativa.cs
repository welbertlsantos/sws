﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace SWS.View
{
    public partial class frmJustificativa : frmTemplateConsulta
    {

        private string justificativa;

        public string Justificativa
        {
            get { return justificativa; }
            set { justificativa = value; }
        }
        
        
        public frmJustificativa(string text)
        {
            InitializeComponent();
            ActiveControl = text_justificativa;
            this.Text = text;
        }

        private void btn_fechar_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btn_gravar_Click(object sender, EventArgs e)
        {
            try
            {
                if (text_justificativa.TextLength < 10)
                    throw new Exception("A justificativa deve ter mais de 10 posições.");
                
                this.Justificativa = text_justificativa.Text.Trim();
                this.Close();

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
    }
}
