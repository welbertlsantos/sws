﻿using SWS.Entidade;
using SWS.Facade;
using SWS.View.Resources;
using SWS.View.ViewHelper;
using SWS.ViewHelper;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace SWS.View
{
    public partial class frmEpiPrincipal : frmTemplate
    {
        Epi epi;
        
        public frmEpiPrincipal()
        {
            InitializeComponent();
            ComboHelper.comboSituacao(cbSituacao);
            validaPermissoes();
        }

        private void validaPermissoes()
        {
            PermissionamentoFacade permissionamentoFacade = PermissionamentoFacade.getInstance();

            bt_alterar.Enabled = permissionamentoFacade.hasPermission("EPI", "ALTERAR");
            bt_incluir.Enabled = permissionamentoFacade.hasPermission("EPI", "INCLUIR");
            bt_excluir.Enabled = permissionamentoFacade.hasPermission("EPI", "EXCLUIR");
            bt_detalhar.Enabled = permissionamentoFacade.hasPermission("EPI", "DETALHAR");
            btn_reativar.Enabled = permissionamentoFacade.hasPermission("EPI", "REATIVAR");
        }

        private void bt_incluir_Click(object sender, EventArgs e)
        {
            frmEpiIncluir ManterEpi = new frmEpiIncluir();
            ManterEpi.ShowDialog();

            if (ManterEpi.Epi != null)
            {
                epi = ManterEpi.Epi;
                text_descricao.Text = epi.Descricao;
                montaDataGrid();
            }
        }

        private void bt_alterar_Click(object sender, EventArgs e)
        {
            try
            {
                if (grd_epi.CurrentRow == null)
                    throw new Exception ("Selecione um epi.");
                
                EstudoFacade pcmsoFacade = EstudoFacade.getInstance();

                Epi epi = pcmsoFacade.findEpiById((Int64)grd_epi.CurrentRow.Cells[0].Value);

                if (!String.Equals(epi.Situacao, ApplicationConstants.ATIVO))
                    throw new Exception ("Somente EPIs ativos podem ser alterados.");
                    
                frmEpiAlterar alterarEpi = new frmEpiAlterar(epi);
                alterarEpi.ShowDialog();

                epi = alterarEpi.Epi;
                text_descricao.Text = epi.Descricao;
                montaDataGrid();

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void bt_detalhar_Click(object sender, EventArgs e)
        {
            try
            {
                if (grd_epi.CurrentRow == null)
                    throw new Exception ("Selecione um EPI.");

                EstudoFacade pcmsoFacade = EstudoFacade.getInstance();

                frmEpiDetalhar detalharEpi = new frmEpiDetalhar(pcmsoFacade.findEpiById((Int64)grd_epi.CurrentRow.Cells[0].Value));
                detalharEpi.ShowDialog();

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }

        private void bt_excluir_Click(object sender, EventArgs e)
        {
            try
            {
                if (grd_epi.CurrentRow == null)
                    throw new Exception ("Selecione um EPI.");
             
                EstudoFacade pcmsoFacade = EstudoFacade.getInstance();
                
                Epi epiExcluir = pcmsoFacade.findEpiById((Int64)grd_epi.CurrentRow.Cells[0].Value);

                if (!String.Equals(epiExcluir.Situacao, ApplicationConstants.ATIVO))
                    throw new Exception ("Somente EPIs ativos podem ser excluídos.");

                if (MessageBox.Show("Gostaria realmente de excluir o EPI?", "Confirmação", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                {
                    epiExcluir.Situacao = ApplicationConstants.DESATIVADO;
                    pcmsoFacade.updateEpi(epiExcluir);
                    MessageBox.Show("EPI excluído com sucesso.", "Confirmação", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    montaDataGrid();
                
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btn_reativar_Click(object sender, EventArgs e)
        {
            try
            {
                if (grd_epi.CurrentRow == null)
                    throw new Exception("Selecione um EPI.");
                
                EstudoFacade pcmsoFacade = EstudoFacade.getInstance();

                Epi epiReativar = pcmsoFacade.findEpiById((Int64)grd_epi.CurrentRow.Cells[0].Value);

                if (!String.Equals(epiReativar.Situacao, ApplicationConstants.DESATIVADO))
                    throw new Exception ("Somente EPIs desativados podem ser ativados.");

                if (MessageBox.Show("Deseja reativar o EPI selecionado?", "Confirmação", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                {
                    epiReativar.Situacao = ApplicationConstants.ATIVO;
                    pcmsoFacade.updateEpi(epiReativar);
                    MessageBox.Show("EPI ativado com sucesso.", "Confirmação", MessageBoxButtons.OK, MessageBoxIcon.Information);

                    montaDataGrid();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }

        private void bt_limpar_Click(object sender, EventArgs e)
        {
            text_descricao.Text = String.Empty;
            text_ca.Text = String.Empty;
            ComboHelper.comboSituacao(cbSituacao);
            grd_epi.Columns.Clear();
        }

        private void bt_pesquisar_Click(object sender, EventArgs e)
        {
            try
            {
                montaDataGrid();
                text_descricao.Focus();
            }
            catch (Exception Ex)
            {
                MessageBox.Show(Ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void bt_fechar_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        public void montaDataGrid()
        {
            EstudoFacade pcmsoFacade = EstudoFacade.getInstance();

            DataSet ds = pcmsoFacade.findEpiByFilter(new Epi(null, text_descricao.Text.Trim(), string.Empty, text_ca.Text.Trim(), cbSituacao.SelectedIndex == -1 ? string.Empty : (((SelectItem)cbSituacao.SelectedItem).Valor)));

            grd_epi.DataSource = ds.Tables["Epis"].DefaultView;

            grd_epi.Columns[0].HeaderText = "ID";
            grd_epi.Columns[0].Visible = false;

            grd_epi.Columns[1].HeaderText = "Descrição";
            grd_epi.Columns[2].HeaderText = "Finalidade";
            grd_epi.Columns[3].HeaderText = "CA";
            grd_epi.Columns[4].HeaderText = "Situação";
            grd_epi.Columns[4].Visible = false;
        }

        private void grd_epi_RowPrePaint(object sender, DataGridViewRowPrePaintEventArgs e)
        {
            DataGridView dgv = sender as DataGridView;

            string situacao = dgv.Rows[e.RowIndex].Cells[4].Value.ToString();

            if (String.Equals(situacao, ApplicationConstants.DESATIVADO))
                dgv.Rows[e.RowIndex].DefaultCellStyle.ForeColor = Color.Red;

        }

        private void grd_epi_CellMouseDoubleClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            try
            {
                bt_alterar.PerformClick();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }

    }
}
