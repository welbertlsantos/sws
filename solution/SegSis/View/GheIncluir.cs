﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using SWS.Entidade;
using SWS.Facade;
using SWS.Excecao;

namespace SWS.View
{
    public partial class frm_GheIncluir : BaseFormConsulta
    {
        private Ghe ghe;

        public Ghe Ghe
        {
            get { return ghe; }
            set { ghe = value; }
        }

        private Estudo estudo;

        public HashSet<Nota> normasIncluidas = new HashSet<Nota>();
        
        public frm_GheIncluir(Estudo estudo)
        {
            InitializeComponent();
            this.estudo = estudo;
        }

        private void btn_fechar_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void text_nexp_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = ViewHelper.ValidaCampoHelper.FormataDigitacaoNumero(sender, e, text_nexp.Text, 2);
        }

        private void btn_salvar_Click(object sender, EventArgs e)
        {
            try
            {
                EstudoFacade pcmsoFacade = EstudoFacade.getInstance();
                
                if (String.IsNullOrEmpty(text_descricaoGhe.Text))
                    throw new Exception("A descrição e obrigatória.");

                if (string.IsNullOrEmpty(text_nexp.Text.Trim()))
                    throw new Exception("Campo Número de exposto não pode ser nulo.");

                ghe = new Ghe(null, estudo, text_descricaoGhe.Text, Convert.ToInt32(text_nexp.Text), string.Empty, textNumeroPO.Text);
                ghe.Norma = normasIncluidas;

                ghe = pcmsoFacade.insertGhe(ghe);

                this.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }

        private void btn_inserir_Click(object sender, EventArgs e)
        {
            frm_GheNorma formGheNorma = new frm_GheNorma();
            formGheNorma.ShowDialog();

            if (formGheNorma.NormaSelecionada.Count > 0)
            {
                normasIncluidas.UnionWith(formGheNorma.NormaSelecionada);
                montaGride();
            }

        }

        public void montaGride()
        {
            grd_norma.Columns.Clear();

            foreach (Nota norma in normasIncluidas)
            {

                grd_norma.ColumnCount = 4;

                grd_norma.Columns[0].Name = "ID";
                grd_norma.Columns[0].Visible = false;

                grd_norma.Columns[1].Name = "Título";
                grd_norma.Columns[1].ReadOnly = true;
                
                grd_norma.Columns[2].Name = "Situação";
                grd_norma.Columns[2].Visible = false;

                grd_norma.Columns[3].Name = "Conteúdo";
                grd_norma.Columns[3].ReadOnly = true;

                this.grd_norma.Rows.Add(norma.Id, norma.Descricao, norma.Situacao, norma.Conteudo);

            }
        }

        private void btn_excluir_Click(object sender, EventArgs e)
        {
            try
            {
                if (grd_norma.CurrentRow == null)
                    throw new Exception("Selecione uma nota para excluir.");

                Nota normaExcluir = normasIncluidas.First(x => x.Id == (Int64)grd_norma.CurrentRow.Cells[0].Value);

                normasIncluidas.Remove(normaExcluir);
                montaGride();

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }

        }

    }
}
