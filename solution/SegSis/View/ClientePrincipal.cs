﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using SWS.Facade;
using SWS.View.ViewHelper;
using SWS.Entidade;
using SWS.ViewHelper;
using SWS.View.Resources;
using System.Text.RegularExpressions;

namespace SWS.View
{
    public partial class frmClientePrincipal : SWS.View.frmTemplate
    {
        
        private Cliente cliente;

        public Cliente Cliente
        {
            get { return cliente; }
            set { cliente = value; }
        }
        
        public frmClientePrincipal()
        {
            InitializeComponent();
            validaPermissoes();
            /* montando informaçoes dos Combos */
            ComboHelper.unidadeFederativa(cbUf);
            ComboHelper.comboTipoClienteFilter(cbTipoCliente);
            ComboHelper.comboSituacao(cbSituacao);
            ActiveControl = textCliente;
        }

        private void bt_incluir_Click(object sender, EventArgs e)
        {
            try
            {
                frmClientesIncluir FormIncluir = new frmClientesIncluir();
                FormIncluir.ShowDialog();

                if (FormIncluir.Cliente != null)
                {
                    cliente = FormIncluir.Cliente;
                    montaDataGrid();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }

        private void bt_alterar_Click(object sender, EventArgs e)
        {
            try
            {
                if (dgvCliente.CurrentRow == null)
                    throw new Exception("Selecione uma linha.");

                ClienteFacade clienteFacade = ClienteFacade.getInstance();

                cliente = clienteFacade.findClienteById((Int64)dgvCliente.CurrentRow.Cells[0].Value);

                if (!String.Equals(dgvCliente.CurrentRow.Cells[22].Value.ToString(), ApplicationConstants.ATIVO))
                    throw new Exception("Somente clientes ativos podem ser alterados.");

                frmClienteAlterar formClienteAlterar = new frmClienteAlterar(cliente);
                formClienteAlterar.ShowDialog();

                cliente = formClienteAlterar.Cliente;

                montaDataGrid();

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            
        }

        private void bt_excluir_Click(object sender, EventArgs e)
        {
            try
            {
                if (dgvCliente.CurrentRow == null)
                    throw new Exception("Selecione uma linha.");

                    ClienteFacade clienteFacade = ClienteFacade.getInstance();

                    if (MessageBox.Show("Gostaria realmente de excluir o Cliente", "Confirmação", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                    {
                        if (!String.Equals(dgvCliente.CurrentRow.Cells[22].Value.ToString(), ApplicationConstants.ATIVO))
                            throw new Exception("Somente cliente ativo pode ser excluído.");


                        clienteFacade.excluiCliente((Int64)dgvCliente.CurrentRow.Cells[0].Value, ApplicationConstants.DESATIVADO);

                        MessageBox.Show("Cliente excluído com sucesso!", "Confirmação", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        montaDataGrid();
                    }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btn_reativar_Click(object sender, EventArgs e)
        {
            try
            {
                if (dgvCliente.CurrentRow == null)
                    throw new Exception("Selecione uma linha.");
                    
                ClienteFacade clienteFacade = ClienteFacade.getInstance();

                if (MessageBox.Show("Deseja reativar o cliente selecionado?", "Confirmação", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                {
                
                    if (!String.Equals(dgvCliente.CurrentRow.Cells[22].Value.ToString(), ApplicationConstants.DESATIVADO))
                        throw new Exception("Somente cliente inativo pode ser reativado. ");
                        
                    clienteFacade.excluiCliente((Int64)dgvCliente.CurrentRow.Cells[0].Value, ApplicationConstants.ATIVO);

                    MessageBox.Show("Cliente reativado com sucesso. ", "Confirmação", MessageBoxButtons.OK, MessageBoxIcon.Information);

                    montaDataGrid();
                        
                }
                
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }

        private void bt_pesquisar_Click(object sender, EventArgs e)
        {
            try
            {
                if (validaCampos())
                {

                    Int32 nfiltro = 0;

                    // validando campos de pesquisa.

                    if (!String.IsNullOrEmpty(textCliente.Text))
                        nfiltro++;

                    if (!String.IsNullOrEmpty(textFantasia.Text))
                        nfiltro++;

                    if (!String.IsNullOrEmpty(textCnpj.Text.Replace("-", "").Replace("/", "").Replace(".", "").Trim()))
                        nfiltro++;

                    if (cbCidade.SelectedIndex != -1)
                        nfiltro++;

                    if (cbSituacao.SelectedIndex != 0)
                        nfiltro++;

                    if (dataInicial.Checked && dataFinal.Checked)
                        nfiltro++;

                    if (cbUf.SelectedIndex != 0)
                        nfiltro++;

                    if (nfiltro == 0)
                    {
                        if (MessageBox.Show("Você não selecionou nenhum fitro. Sua pesquisa poderá demorar um pouco. Deseja continuar mesmo assim?",
                            "Confirmação", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                        {
                            cliente = new Cliente();
                            cliente.RazaoSocial = this.textCliente.Text;
                            cliente.Fantasia = this.textFantasia.Text;
                            cliente.Cnpj = this.textCnpj.Text;
                            cliente.Situacao = ((SelectItem)cbSituacao.SelectedItem).Valor;
                            cliente.Uf = ((SelectItem)cbUf.SelectedItem).Valor;
                            cliente.DataCadastro = dataInicial.Checked ? Convert.ToDateTime(dataInicial.Text) : (DateTime?)null;
                            montaDataGrid();
                        }
                    }
                    else
                    {
                        cliente = new Cliente();
                        cliente.RazaoSocial = this.textCliente.Text;
                        cliente.Fantasia = this.textFantasia.Text;
                        cliente.Cnpj = this.textCnpj.Text;
                        cliente.Situacao = ((SelectItem)cbSituacao.SelectedItem).Valor;
                        cliente.Uf = ((SelectItem)cbUf.SelectedItem).Valor;
                        cliente.DataCadastro = dataInicial.Checked ? Convert.ToDateTime(dataInicial.Text) : (DateTime?)null;
                        montaDataGrid();
                    }

                }
            }
            catch (Exception Ex)
            {
                MessageBox.Show(Ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            
        }

        private void bt_detalhar_Click(object sender, EventArgs e)
        {
            try
            {
                if (dgvCliente.CurrentRow == null)
                    throw new Exception("Selecione uma linha.");

                ClienteFacade clienteFacade = ClienteFacade.getInstance();

                cliente = clienteFacade.findClienteById((Int64)dgvCliente.CurrentRow.Cells[0].Value);

                frmClientesDetalhar formClienteDetalhar = new frmClientesDetalhar(cliente);
                formClienteDetalhar.ShowDialog();

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void bt_limpar_Click(object sender, EventArgs e)
        {
            this.textCliente.Text = String.Empty;
            this.textFantasia.Text = String.Empty;
            this.textCnpj.Text = String.Empty;
            ComboHelper.unidadeFederativa(cbUf);
            cbCidade.DataSource = null;
            cbCidade.Items.Clear();
            ComboHelper.comboTipoClienteFilter(cbTipoCliente);
            ComboHelper.comboSituacao(cbSituacao);
            dataInicial.Checked = false;
            dataFinal.Checked = false;
            dgvCliente.Columns.Clear();
        }

        private void bt_fechar_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void validaPermissoes()
        {
            PermissionamentoFacade permissionamentoFacade = PermissionamentoFacade.getInstance();

            bt_alterar.Enabled = permissionamentoFacade.hasPermission("CLIENTE", "ALTERAR");
            bt_incluir.Enabled = permissionamentoFacade.hasPermission("CLIENTE", "INCLUIR");
            bt_excluir.Enabled = permissionamentoFacade.hasPermission("CLIENTE", "EXCLUIR");
            bt_detalhar.Enabled = permissionamentoFacade.hasPermission("CLIENTE", "DETALHAR");
            btn_reativar.Enabled = permissionamentoFacade.hasPermission("CLIENTE", "REATIVAR");
        }

        public void montaDataGrid()
        {
            ClienteFacade clienteFacade = ClienteFacade.getInstance();

            Boolean? credenciada = null;
            Boolean? unidade = null;
            bool? fisica = null;
           

            if (cbCidade.SelectedIndex != -1)
            {
                cliente.CidadeIbge = clienteFacade.findCidadeIbgeById((Convert.ToInt64(((SelectItem)cbCidade.SelectedItem).Valor)));
            }

            if (cbTipoCliente.SelectedIndex != -1)
            {

                if (String.Equals(((SelectItem)cbTipoCliente.SelectedItem).Valor, ApplicationConstants.UNIDADE))
                    unidade = true;
                else if (String.Equals(((SelectItem)cbTipoCliente.SelectedItem).Valor, ApplicationConstants.CREDENCIADA))
                    credenciada = true;
                else if (String.Equals(((SelectItem)cbTipoCliente.SelectedItem).Valor, ApplicationConstants.PADRAO))
                {
                    unidade = false;
                    credenciada = false;
                    fisica = false;
                }
                else if (string.Equals(((SelectItem)cbTipoCliente.SelectedItem).Valor, ApplicationConstants.FISICA))
                {
                    fisica = true;
                }
                    
                
            }

            DataSet ds = clienteFacade.findClienteByFilter(cliente, null, credenciada, unidade, dataFinal.Checked ? Convert.ToDateTime(dataFinal.Text) : (DateTime?)null, null, null, null, fisica);

            dgvCliente.DataSource = ds.Tables["Clientes"].DefaultView;

            dgvCliente.Columns[0].HeaderText = "ID";
            dgvCliente.Columns[0].Visible = false;

            dgvCliente.Columns[1].HeaderText = "Razão Social";

            dgvCliente.Columns[2].HeaderText = "Nome Fantasia";
            dgvCliente.Columns[3].HeaderText = "CNPJ";
            dgvCliente.Columns[4].HeaderText = "Inscrição Estadual";
            dgvCliente.Columns[5].HeaderText = "Endereço";
            dgvCliente.Columns[6].HeaderText = "Número";
            dgvCliente.Columns[7].HeaderText = "Complemento";
            dgvCliente.Columns[8].HeaderText = "Bairro";
            dgvCliente.Columns[9].HeaderText = "Cidade";
            dgvCliente.Columns[10].HeaderText = "CEP";
            dgvCliente.Columns[11].HeaderText = "UF";
            dgvCliente.Columns[12].HeaderText = "Telefone";
            dgvCliente.Columns[13].HeaderText = "FAX";
            dgvCliente.Columns[14].HeaderText = "Email";

            dgvCliente.Columns[15].HeaderText = "Site";
            dgvCliente.Columns[15].Visible = false;

            dgvCliente.Columns[16].HeaderText = "Data Cadastro";

            dgvCliente.Columns[17].HeaderText = "Responsável";
            dgvCliente.Columns[17].Visible = false;

            dgvCliente.Columns[18].HeaderText = "Cargo";
            dgvCliente.Columns[18].Visible = false;

            dgvCliente.Columns[19].HeaderText = "Documento";
            dgvCliente.Columns[19].Visible = false;

            dgvCliente.Columns[20].HeaderText = "Escopo";
            dgvCliente.Columns[20].Visible = false;

            dgvCliente.Columns[21].HeaderText = "Jornada";
            dgvCliente.Columns[21].Visible = false;

            dgvCliente.Columns[22].HeaderText = "Situação";

            dgvCliente.Columns[23].HeaderText = "Estimativa Masc";
            dgvCliente.Columns[23].Visible = false;

            dgvCliente.Columns[24].HeaderText = "Estimativa Fem";
            dgvCliente.Columns[24].Visible = false;

            dgvCliente.Columns[25].HeaderText = "Endereço Cobrança";
            dgvCliente.Columns[25].Visible = false;

            dgvCliente.Columns[26].HeaderText = "Numero Cobrança";
            dgvCliente.Columns[26].Visible = false;

            dgvCliente.Columns[27].HeaderText = "Complemento Cobrança";
            dgvCliente.Columns[27].Visible = false;

            dgvCliente.Columns[28].HeaderText = "Bairro Cobrança";
            dgvCliente.Columns[28].Visible = false;

            dgvCliente.Columns[29].HeaderText = "Cep Cobrança";
            dgvCliente.Columns[29].Visible = false;

            dgvCliente.Columns[30].HeaderText = "Cidade Cobrança";
            dgvCliente.Columns[30].Visible = false;

            dgvCliente.Columns[31].HeaderText = "UF Cobrança";
            dgvCliente.Columns[31].Visible = false;

            dgvCliente.Columns[32].HeaderText = "Telefone Cobrança";
            dgvCliente.Columns[32].Visible = false;

            dgvCliente.Columns[33].HeaderText = "Fax Cobrança";
            dgvCliente.Columns[33].Visible = false;

            dgvCliente.Columns[34].HeaderText = "Coordenador";
            dgvCliente.Columns[34].Visible = false;

            dgvCliente.Columns[35].HeaderText = "VIP";

            dgvCliente.Columns[36].HeaderText = "Telefone do Responsável";
            dgvCliente.Columns[36].Visible = false;

            dgvCliente.Columns[37].HeaderText = "Usa Contrato";

            dgvCliente.Columns[38].HeaderText = "Particular";

            dgvCliente.Columns[39].HeaderText = "Unidade";

            dgvCliente.Columns[40].HeaderText = "Pode ser Credenciada";

            dgvCliente.Columns[41].HeaderText = "Pode ser Prestador";

        }

        private Boolean validaCampos()
        {
            Boolean retorno = true;

            try
            {
                if (dataInicial.Checked)
                {
                    if (!dataFinal.Checked)
                        throw new Exception("Você precisa marcar a data final para pesquisa.");
                    
                    if (Convert.ToDateTime(this.dataInicial.Text) > Convert.ToDateTime(this.dataFinal.Text))
                        throw new Exception("A data final deve ser maior que a inicial!");
                }

                if (dataFinal.Checked)
                {
                    if (!dataInicial.Checked)
                        throw new Exception("Você precisa marcar a data inicial para pesquisa.");

                    if (Convert.ToDateTime(this.dataInicial.Text) > Convert.ToDateTime(this.dataFinal.Text))
                        throw new Exception("A data final deve ser maior que a inicial!");

                }

                if (!string.Equals(textCnpj.Text, string.Empty) && textCnpj.Text.Replace(",","").Replace(".","").Replace("-","").Replace("/","").Trim().Length != 11) 
                {

                    if (Regex.IsMatch(Convert.ToString(this.textCnpj), @"\d"))

                        if (!ValidaCampoHelper.ValidaCNPJ(this.textCnpj.Text))
                            throw new Exception("CNPJ digitado é inválido");
                }
                else
                {
                    if (Regex.IsMatch(Convert.ToString(this.textCnpj), @"\d"))

                        if (!ValidaCampoHelper.ValidaCPF(this.textCnpj.Text))
                            throw new Exception("CPF digitado é inválido");
                }
            }
            catch (Exception ex)
            {
                retorno = false;
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

            return retorno;

        }

        private void dgvCliente_RowPrePaint(object sender, DataGridViewRowPrePaintEventArgs e)
        {
            DataGridView dgv = sender as DataGridView;

            if (String.Equals(dgv.Rows[e.RowIndex].Cells[22].Value, ApplicationConstants.DESATIVADO))
            {
                dgv.Rows[e.RowIndex].DefaultCellStyle.ForeColor = Color.Red;
                dgv.Rows[e.RowIndex].DefaultCellStyle.Font = new System.Drawing.Font(this.Font, FontStyle.Strikeout);
            }

        }

        private void cbUf_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                if (this.cbUf.SelectedIndex == -1)
                    throw new Exception("Selecione uma Unidade Federativa primeiro.");

                ComboHelper.iniciaComboCidade(cbCidade, ((SelectItem)cbUf.SelectedItem).Valor.ToString());

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void cbCidade_Click(object sender, EventArgs e)
        {
            try
            {
                if (cbUf.SelectedIndex == 0)
                    throw new Exception("Selecione uma Unidade Federativa primeiro.");

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void dgvCliente_CellMouseDoubleClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            try
            {
                bt_alterar.PerformClick();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }

        private void cbTipoCliente_SelectedIndexChanged(object sender, EventArgs e)
        {
            lblCliente.Text = "Nome";
            lblFantasia.Text = "Apelido/Nome Fantasia";
            lblCnpj.Text = "CPF/CNPJ";
        }

        private void textCnpj_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = ValidaCampoHelper.FormataCampoNumerico(sender, e);
        }

        private void textCnpj_Leave(object sender, EventArgs e)
        {
            if (!string.Equals(textCnpj.Text, string.Empty) && textCnpj.Text.Replace(".", "").Replace(",", "").Replace("/", "").Replace("-", "").Trim().Length != 11)
            {
                textCnpj.Text = ValidaCampoHelper.FormataCnpj(textCnpj.Text);
                textCnpj.Mask = "999,999,999/9999-99";
            }
            else
            {
                textCnpj.Text = ValidaCampoHelper.FormataCpf(textCnpj.Text);
                textCnpj.Mask = "999,999,999-99";
            }
        }

        private void textCnpj_Enter(object sender, EventArgs e)
        {
            textCnpj.Mask = string.Empty;
        }
    }
}
