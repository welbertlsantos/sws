﻿namespace SegSis.View
{
    partial class frm_cliente_detalhar
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frm_cliente_detalhar));
            this.grb_situacao = new System.Windows.Forms.GroupBox();
            this.rb_desativado = new System.Windows.Forms.RadioButton();
            this.rb_ativo = new System.Windows.Forms.RadioButton();
            this.lbl_telefoneContato = new System.Windows.Forms.Label();
            this.text_telefoneContato = new System.Windows.Forms.TextBox();
            this.text_documento = new System.Windows.Forms.TextBox();
            this.lbl_documento = new System.Windows.Forms.Label();
            this.text_cargo = new System.Windows.Forms.TextBox();
            this.lbl_cargo = new System.Windows.Forms.Label();
            this.text_nome = new System.Windows.Forms.TextBox();
            this.lbl_responsavel = new System.Windows.Forms.Label();
            this.grb_paginacao = new System.Windows.Forms.GroupBox();
            this.btn_fechar = new System.Windows.Forms.Button();
            this.tb_paginacao = new System.Windows.Forms.TabControl();
            this.tbEndereco = new System.Windows.Forms.TabPage();
            this.text_cidade = new System.Windows.Forms.TextBox();
            this.text_uf = new System.Windows.Forms.TextBox();
            this.text_site = new System.Windows.Forms.TextBox();
            this.lbl_site = new System.Windows.Forms.Label();
            this.text_email = new System.Windows.Forms.TextBox();
            this.lbl_email = new System.Windows.Forms.Label();
            this.grb_telefone = new System.Windows.Forms.GroupBox();
            this.text_telefone2 = new System.Windows.Forms.TextBox();
            this.text_telefone1 = new System.Windows.Forms.TextBox();
            this.lbl_telefone = new System.Windows.Forms.Label();
            this.lbl_fax = new System.Windows.Forms.Label();
            this.lbl_uf = new System.Windows.Forms.Label();
            this.text_cep = new System.Windows.Forms.MaskedTextBox();
            this.lbl_cep = new System.Windows.Forms.Label();
            this.lbl_cidade = new System.Windows.Forms.Label();
            this.text_bairro = new System.Windows.Forms.TextBox();
            this.lbl_bairro = new System.Windows.Forms.Label();
            this.text_complemento = new System.Windows.Forms.TextBox();
            this.lbl_complemento = new System.Windows.Forms.Label();
            this.text_numero = new System.Windows.Forms.TextBox();
            this.lbl_numero = new System.Windows.Forms.Label();
            this.text_endereco = new System.Windows.Forms.TextBox();
            this.lbl_endereco = new System.Windows.Forms.Label();
            this.tbEnderecoCob = new System.Windows.Forms.TabPage();
            this.text_cidadeCobranca = new System.Windows.Forms.TextBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.text_telefone2Cob = new System.Windows.Forms.TextBox();
            this.text_telefone1Cob = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.text_ufCob = new System.Windows.Forms.TextBox();
            this.lbl_ufCob = new System.Windows.Forms.Label();
            this.text_cepCob = new System.Windows.Forms.MaskedTextBox();
            this.lbl_cepCob = new System.Windows.Forms.Label();
            this.lbl_cidadeCob = new System.Windows.Forms.Label();
            this.text_bairroCob = new System.Windows.Forms.TextBox();
            this.lbl_bairroCob = new System.Windows.Forms.Label();
            this.text_complementoCob = new System.Windows.Forms.TextBox();
            this.lbl_complementoCob = new System.Windows.Forms.Label();
            this.text_numeroCob = new System.Windows.Forms.TextBox();
            this.lbl_numeroCob = new System.Windows.Forms.Label();
            this.text_enderecoCob = new System.Windows.Forms.TextBox();
            this.lbl_enderecoCob = new System.Windows.Forms.Label();
            this.tbCnae = new System.Windows.Forms.TabPage();
            this.grb_cnaePrincipal = new System.Windows.Forms.GroupBox();
            this.text_cnaePrincipal = new System.Windows.Forms.MaskedTextBox();
            this.grd_cnae = new System.Windows.Forms.DataGridView();
            this.tbAdicionais = new System.Windows.Forms.TabPage();
            this.tbMedicoCoordenador = new System.Windows.Forms.TabPage();
            this.grb_coordenador = new System.Windows.Forms.GroupBox();
            this.text_nomeCoordenador = new System.Windows.Forms.TextBox();
            this.tbLogo = new System.Windows.Forms.TabPage();
            this.pic_box_logo = new System.Windows.Forms.PictureBox();
            this.tbFuncao = new System.Windows.Forms.TabPage();
            this.dg_funcao = new System.Windows.Forms.DataGridView();
            this.tbEstudo = new System.Windows.Forms.TabPage();
            this.text_escopo = new System.Windows.Forms.TextBox();
            this.lbl_escopo = new System.Windows.Forms.Label();
            this.text_jornada = new System.Windows.Forms.TextBox();
            this.lbl_jornada = new System.Windows.Forms.Label();
            this.grb_estimativa = new System.Windows.Forms.GroupBox();
            this.text_estFem = new System.Windows.Forms.TextBox();
            this.text_estMasc = new System.Windows.Forms.TextBox();
            this.lbl_estFem = new System.Windows.Forms.Label();
            this.lbl_estMasc = new System.Windows.Forms.Label();
            this.tbConfiguracao = new System.Windows.Forms.TabPage();
            this.chk_prestador = new System.Windows.Forms.CheckBox();
            this.text_aliquotaIss = new System.Windows.Forms.TextBox();
            this.lbl_aliquotaIss = new System.Windows.Forms.Label();
            this.chk_geraCobrancaValorLiquido = new System.Windows.Forms.CheckBox();
            this.chk_destacaIss = new System.Windows.Forms.CheckBox();
            this.chk_particular = new System.Windows.Forms.CheckBox();
            this.chk_usaContrato = new System.Windows.Forms.CheckBox();
            this.chk_vip = new System.Windows.Forms.CheckBox();
            this.tbCredenciada = new System.Windows.Forms.TabPage();
            this.gbrCredenciada = new System.Windows.Forms.GroupBox();
            this.dgCredenciada = new System.Windows.Forms.DataGridView();
            this.grb_dataCadastro = new System.Windows.Forms.GroupBox();
            this.dt_cadastro = new System.Windows.Forms.DateTimePicker();
            this.grb_vendedor = new System.Windows.Forms.GroupBox();
            this.text_vendedor = new System.Windows.Forms.TextBox();
            this.grb_dados = new System.Windows.Forms.GroupBox();
            this.text_razaoSocial = new System.Windows.Forms.TextBox();
            this.text_inscricao = new System.Windows.Forms.TextBox();
            this.text_fantasia = new System.Windows.Forms.TextBox();
            this.text_cnpj = new System.Windows.Forms.MaskedTextBox();
            this.lbl_razaoSocial = new System.Windows.Forms.Label();
            this.lbl_inscricao = new System.Windows.Forms.Label();
            this.lbl_fantasia = new System.Windows.Forms.Label();
            this.lbl_cnpj = new System.Windows.Forms.Label();
            this.cb_tipoCliente = new System.Windows.Forms.ComboBox();
            this.gb_credenciada = new System.Windows.Forms.GroupBox();
            this.text_credenciada = new System.Windows.Forms.TextBox();
            this.text_matriz = new System.Windows.Forms.TextBox();
            this.gb_Matriz = new System.Windows.Forms.GroupBox();
            this.grb_tipoCliente = new System.Windows.Forms.GroupBox();
            this.panel_banner.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox_banner)).BeginInit();
            this.panel.SuspendLayout();
            this.grb_situacao.SuspendLayout();
            this.grb_paginacao.SuspendLayout();
            this.tb_paginacao.SuspendLayout();
            this.tbEndereco.SuspendLayout();
            this.grb_telefone.SuspendLayout();
            this.tbEnderecoCob.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.tbCnae.SuspendLayout();
            this.grb_cnaePrincipal.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grd_cnae)).BeginInit();
            this.tbMedicoCoordenador.SuspendLayout();
            this.grb_coordenador.SuspendLayout();
            this.tbLogo.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pic_box_logo)).BeginInit();
            this.tbFuncao.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dg_funcao)).BeginInit();
            this.tbEstudo.SuspendLayout();
            this.grb_estimativa.SuspendLayout();
            this.tbConfiguracao.SuspendLayout();
            this.tbCredenciada.SuspendLayout();
            this.gbrCredenciada.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgCredenciada)).BeginInit();
            this.grb_dataCadastro.SuspendLayout();
            this.grb_vendedor.SuspendLayout();
            this.grb_dados.SuspendLayout();
            this.gb_credenciada.SuspendLayout();
            this.gb_Matriz.SuspendLayout();
            this.grb_tipoCliente.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel
            // 
            this.panel.Controls.Add(this.gb_credenciada);
            this.panel.Controls.Add(this.gb_Matriz);
            this.panel.Controls.Add(this.grb_tipoCliente);
            this.panel.Controls.Add(this.grb_situacao);
            this.panel.Controls.Add(this.grb_dados);
            this.panel.Controls.Add(this.grb_paginacao);
            this.panel.Controls.Add(this.grb_dataCadastro);
            this.panel.Controls.Add(this.grb_vendedor);
            // 
            // grb_situacao
            // 
            this.grb_situacao.BackColor = System.Drawing.Color.White;
            this.grb_situacao.Controls.Add(this.rb_desativado);
            this.grb_situacao.Controls.Add(this.rb_ativo);
            this.grb_situacao.Enabled = false;
            this.grb_situacao.Location = new System.Drawing.Point(643, -1);
            this.grb_situacao.Name = "grb_situacao";
            this.grb_situacao.Size = new System.Drawing.Size(140, 40);
            this.grb_situacao.TabIndex = 38;
            this.grb_situacao.TabStop = false;
            this.grb_situacao.Text = "Situação";
            // 
            // rb_desativado
            // 
            this.rb_desativado.AutoSize = true;
            this.rb_desativado.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.rb_desativado.Location = new System.Drawing.Point(55, 14);
            this.rb_desativado.Name = "rb_desativado";
            this.rb_desativado.Size = new System.Drawing.Size(78, 17);
            this.rb_desativado.TabIndex = 1;
            this.rb_desativado.Text = "Desativado";
            this.rb_desativado.UseVisualStyleBackColor = true;
            // 
            // rb_ativo
            // 
            this.rb_ativo.AutoSize = true;
            this.rb_ativo.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.rb_ativo.Location = new System.Drawing.Point(7, 14);
            this.rb_ativo.Name = "rb_ativo";
            this.rb_ativo.Size = new System.Drawing.Size(48, 17);
            this.rb_ativo.TabIndex = 0;
            this.rb_ativo.Text = "Ativo";
            this.rb_ativo.UseVisualStyleBackColor = true;
            // 
            // lbl_telefoneContato
            // 
            this.lbl_telefoneContato.AutoSize = true;
            this.lbl_telefoneContato.Location = new System.Drawing.Point(321, 94);
            this.lbl_telefoneContato.Name = "lbl_telefoneContato";
            this.lbl_telefoneContato.Size = new System.Drawing.Size(49, 13);
            this.lbl_telefoneContato.TabIndex = 35;
            this.lbl_telefoneContato.Text = "Telefone";
            // 
            // text_telefoneContato
            // 
            this.text_telefoneContato.BackColor = System.Drawing.SystemColors.Control;
            this.text_telefoneContato.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.text_telefoneContato.ForeColor = System.Drawing.Color.Blue;
            this.text_telefoneContato.Location = new System.Drawing.Point(324, 107);
            this.text_telefoneContato.MaxLength = 15;
            this.text_telefoneContato.Name = "text_telefoneContato";
            this.text_telefoneContato.ReadOnly = true;
            this.text_telefoneContato.Size = new System.Drawing.Size(162, 20);
            this.text_telefoneContato.TabIndex = 34;
            this.text_telefoneContato.TabStop = false;
            // 
            // text_documento
            // 
            this.text_documento.BackColor = System.Drawing.SystemColors.Control;
            this.text_documento.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.text_documento.ForeColor = System.Drawing.Color.Blue;
            this.text_documento.Location = new System.Drawing.Point(6, 107);
            this.text_documento.MaxLength = 50;
            this.text_documento.Name = "text_documento";
            this.text_documento.ReadOnly = true;
            this.text_documento.Size = new System.Drawing.Size(289, 20);
            this.text_documento.TabIndex = 20;
            this.text_documento.TabStop = false;
            // 
            // lbl_documento
            // 
            this.lbl_documento.AutoSize = true;
            this.lbl_documento.Location = new System.Drawing.Point(3, 94);
            this.lbl_documento.Name = "lbl_documento";
            this.lbl_documento.Size = new System.Drawing.Size(62, 13);
            this.lbl_documento.TabIndex = 18;
            this.lbl_documento.Text = "Documento";
            // 
            // text_cargo
            // 
            this.text_cargo.BackColor = System.Drawing.SystemColors.Control;
            this.text_cargo.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.text_cargo.ForeColor = System.Drawing.Color.Blue;
            this.text_cargo.Location = new System.Drawing.Point(492, 72);
            this.text_cargo.MaxLength = 50;
            this.text_cargo.Name = "text_cargo";
            this.text_cargo.ReadOnly = true;
            this.text_cargo.Size = new System.Drawing.Size(256, 20);
            this.text_cargo.TabIndex = 19;
            this.text_cargo.TabStop = false;
            // 
            // lbl_cargo
            // 
            this.lbl_cargo.AutoSize = true;
            this.lbl_cargo.Location = new System.Drawing.Point(489, 59);
            this.lbl_cargo.Name = "lbl_cargo";
            this.lbl_cargo.Size = new System.Drawing.Size(35, 13);
            this.lbl_cargo.TabIndex = 16;
            this.lbl_cargo.Text = "Cargo";
            // 
            // text_nome
            // 
            this.text_nome.BackColor = System.Drawing.SystemColors.Control;
            this.text_nome.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.text_nome.ForeColor = System.Drawing.Color.Blue;
            this.text_nome.Location = new System.Drawing.Point(6, 72);
            this.text_nome.MaxLength = 100;
            this.text_nome.Name = "text_nome";
            this.text_nome.ReadOnly = true;
            this.text_nome.Size = new System.Drawing.Size(480, 20);
            this.text_nome.TabIndex = 18;
            this.text_nome.TabStop = false;
            // 
            // lbl_responsavel
            // 
            this.lbl_responsavel.AutoSize = true;
            this.lbl_responsavel.Location = new System.Drawing.Point(3, 59);
            this.lbl_responsavel.Name = "lbl_responsavel";
            this.lbl_responsavel.Size = new System.Drawing.Size(115, 13);
            this.lbl_responsavel.TabIndex = 14;
            this.lbl_responsavel.Text = "Nome do Responsável";
            // 
            // grb_paginacao
            // 
            this.grb_paginacao.BackColor = System.Drawing.Color.White;
            this.grb_paginacao.Controls.Add(this.btn_fechar);
            this.grb_paginacao.Location = new System.Drawing.Point(3, 464);
            this.grb_paginacao.Name = "grb_paginacao";
            this.grb_paginacao.Size = new System.Drawing.Size(781, 51);
            this.grb_paginacao.TabIndex = 36;
            this.grb_paginacao.TabStop = false;
            // 
            // btn_fechar
            // 
            this.btn_fechar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_fechar.Image = global::SegSis.Properties.Resources.close;
            this.btn_fechar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btn_fechar.Location = new System.Drawing.Point(6, 19);
            this.btn_fechar.Name = "btn_fechar";
            this.btn_fechar.Size = new System.Drawing.Size(77, 23);
            this.btn_fechar.TabIndex = 1;
            this.btn_fechar.Text = "&Fechar";
            this.btn_fechar.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btn_fechar.UseVisualStyleBackColor = true;
            this.btn_fechar.Click += new System.EventHandler(this.btn_fechar_Click);
            // 
            // tb_paginacao
            // 
            this.tb_paginacao.Controls.Add(this.tbEndereco);
            this.tb_paginacao.Controls.Add(this.tbEnderecoCob);
            this.tb_paginacao.Controls.Add(this.tbCnae);
            this.tb_paginacao.Controls.Add(this.tbAdicionais);
            this.tb_paginacao.Controls.Add(this.tbMedicoCoordenador);
            this.tb_paginacao.Controls.Add(this.tbLogo);
            this.tb_paginacao.Controls.Add(this.tbFuncao);
            this.tb_paginacao.Controls.Add(this.tbEstudo);
            this.tb_paginacao.Controls.Add(this.tbConfiguracao);
            this.tb_paginacao.Controls.Add(this.tbCredenciada);
            this.tb_paginacao.Location = new System.Drawing.Point(12, 251);
            this.tb_paginacao.Name = "tb_paginacao";
            this.tb_paginacao.SelectedIndex = 0;
            this.tb_paginacao.Size = new System.Drawing.Size(775, 264);
            this.tb_paginacao.TabIndex = 35;
            // 
            // tbEndereco
            // 
            this.tbEndereco.BackColor = System.Drawing.Color.White;
            this.tbEndereco.Controls.Add(this.text_cidade);
            this.tbEndereco.Controls.Add(this.text_uf);
            this.tbEndereco.Controls.Add(this.text_site);
            this.tbEndereco.Controls.Add(this.lbl_site);
            this.tbEndereco.Controls.Add(this.text_email);
            this.tbEndereco.Controls.Add(this.lbl_email);
            this.tbEndereco.Controls.Add(this.grb_telefone);
            this.tbEndereco.Controls.Add(this.lbl_uf);
            this.tbEndereco.Controls.Add(this.text_cep);
            this.tbEndereco.Controls.Add(this.lbl_cep);
            this.tbEndereco.Controls.Add(this.lbl_cidade);
            this.tbEndereco.Controls.Add(this.text_bairro);
            this.tbEndereco.Controls.Add(this.lbl_bairro);
            this.tbEndereco.Controls.Add(this.text_complemento);
            this.tbEndereco.Controls.Add(this.lbl_complemento);
            this.tbEndereco.Controls.Add(this.text_numero);
            this.tbEndereco.Controls.Add(this.lbl_numero);
            this.tbEndereco.Controls.Add(this.text_endereco);
            this.tbEndereco.Controls.Add(this.lbl_endereco);
            this.tbEndereco.Location = new System.Drawing.Point(4, 22);
            this.tbEndereco.Name = "tbEndereco";
            this.tbEndereco.Padding = new System.Windows.Forms.Padding(3);
            this.tbEndereco.Size = new System.Drawing.Size(767, 238);
            this.tbEndereco.TabIndex = 0;
            this.tbEndereco.Text = "Endereço";
            // 
            // text_cidade
            // 
            this.text_cidade.BackColor = System.Drawing.SystemColors.Control;
            this.text_cidade.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.text_cidade.ForeColor = System.Drawing.Color.Blue;
            this.text_cidade.Location = new System.Drawing.Point(9, 99);
            this.text_cidade.MaxLength = 2;
            this.text_cidade.Name = "text_cidade";
            this.text_cidade.ReadOnly = true;
            this.text_cidade.Size = new System.Drawing.Size(283, 20);
            this.text_cidade.TabIndex = 35;
            this.text_cidade.TabStop = false;
            // 
            // text_uf
            // 
            this.text_uf.BackColor = System.Drawing.SystemColors.Control;
            this.text_uf.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.text_uf.ForeColor = System.Drawing.Color.Blue;
            this.text_uf.Location = new System.Drawing.Point(310, 99);
            this.text_uf.MaxLength = 2;
            this.text_uf.Name = "text_uf";
            this.text_uf.ReadOnly = true;
            this.text_uf.Size = new System.Drawing.Size(43, 20);
            this.text_uf.TabIndex = 34;
            this.text_uf.TabStop = false;
            // 
            // text_site
            // 
            this.text_site.BackColor = System.Drawing.SystemColors.Control;
            this.text_site.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.text_site.ForeColor = System.Drawing.Color.Blue;
            this.text_site.Location = new System.Drawing.Point(310, 143);
            this.text_site.MaxLength = 50;
            this.text_site.Name = "text_site";
            this.text_site.ReadOnly = true;
            this.text_site.Size = new System.Drawing.Size(451, 20);
            this.text_site.TabIndex = 17;
            this.text_site.TabStop = false;
            // 
            // lbl_site
            // 
            this.lbl_site.AutoSize = true;
            this.lbl_site.Location = new System.Drawing.Point(307, 127);
            this.lbl_site.Name = "lbl_site";
            this.lbl_site.Size = new System.Drawing.Size(25, 13);
            this.lbl_site.TabIndex = 30;
            this.lbl_site.Text = "Site";
            // 
            // text_email
            // 
            this.text_email.BackColor = System.Drawing.SystemColors.Control;
            this.text_email.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.text_email.ForeColor = System.Drawing.Color.Blue;
            this.text_email.Location = new System.Drawing.Point(9, 142);
            this.text_email.MaxLength = 50;
            this.text_email.Name = "text_email";
            this.text_email.ReadOnly = true;
            this.text_email.Size = new System.Drawing.Size(283, 20);
            this.text_email.TabIndex = 16;
            this.text_email.TabStop = false;
            // 
            // lbl_email
            // 
            this.lbl_email.AutoSize = true;
            this.lbl_email.Location = new System.Drawing.Point(6, 126);
            this.lbl_email.Name = "lbl_email";
            this.lbl_email.Size = new System.Drawing.Size(32, 13);
            this.lbl_email.TabIndex = 28;
            this.lbl_email.Text = "Email";
            // 
            // grb_telefone
            // 
            this.grb_telefone.Controls.Add(this.text_telefone2);
            this.grb_telefone.Controls.Add(this.text_telefone1);
            this.grb_telefone.Controls.Add(this.lbl_telefone);
            this.grb_telefone.Controls.Add(this.lbl_fax);
            this.grb_telefone.Location = new System.Drawing.Point(433, 75);
            this.grb_telefone.Name = "grb_telefone";
            this.grb_telefone.Size = new System.Drawing.Size(328, 50);
            this.grb_telefone.TabIndex = 27;
            this.grb_telefone.TabStop = false;
            // 
            // text_telefone2
            // 
            this.text_telefone2.BackColor = System.Drawing.SystemColors.Control;
            this.text_telefone2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.text_telefone2.ForeColor = System.Drawing.Color.Blue;
            this.text_telefone2.Location = new System.Drawing.Point(167, 24);
            this.text_telefone2.MaxLength = 15;
            this.text_telefone2.Name = "text_telefone2";
            this.text_telefone2.ReadOnly = true;
            this.text_telefone2.Size = new System.Drawing.Size(149, 20);
            this.text_telefone2.TabIndex = 33;
            this.text_telefone2.TabStop = false;
            // 
            // text_telefone1
            // 
            this.text_telefone1.BackColor = System.Drawing.SystemColors.Control;
            this.text_telefone1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.text_telefone1.ForeColor = System.Drawing.Color.Blue;
            this.text_telefone1.Location = new System.Drawing.Point(9, 24);
            this.text_telefone1.MaxLength = 15;
            this.text_telefone1.Name = "text_telefone1";
            this.text_telefone1.ReadOnly = true;
            this.text_telefone1.Size = new System.Drawing.Size(149, 20);
            this.text_telefone1.TabIndex = 14;
            this.text_telefone1.TabStop = false;
            // 
            // lbl_telefone
            // 
            this.lbl_telefone.AutoSize = true;
            this.lbl_telefone.Location = new System.Drawing.Point(6, 8);
            this.lbl_telefone.Name = "lbl_telefone";
            this.lbl_telefone.Size = new System.Drawing.Size(49, 13);
            this.lbl_telefone.TabIndex = 11;
            this.lbl_telefone.Text = "Telefone";
            // 
            // lbl_fax
            // 
            this.lbl_fax.AutoSize = true;
            this.lbl_fax.Location = new System.Drawing.Point(164, 8);
            this.lbl_fax.Name = "lbl_fax";
            this.lbl_fax.Size = new System.Drawing.Size(24, 13);
            this.lbl_fax.TabIndex = 26;
            this.lbl_fax.Text = "Fax";
            // 
            // lbl_uf
            // 
            this.lbl_uf.AutoSize = true;
            this.lbl_uf.Location = new System.Drawing.Point(307, 83);
            this.lbl_uf.Name = "lbl_uf";
            this.lbl_uf.Size = new System.Drawing.Size(21, 13);
            this.lbl_uf.TabIndex = 24;
            this.lbl_uf.Text = "UF";
            // 
            // text_cep
            // 
            this.text_cep.BackColor = System.Drawing.SystemColors.Control;
            this.text_cep.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.text_cep.ForeColor = System.Drawing.Color.Blue;
            this.text_cep.Location = new System.Drawing.Point(589, 55);
            this.text_cep.Mask = "99,999-999";
            this.text_cep.Name = "text_cep";
            this.text_cep.PromptChar = ' ';
            this.text_cep.ReadOnly = true;
            this.text_cep.Size = new System.Drawing.Size(82, 20);
            this.text_cep.TabIndex = 11;
            this.text_cep.TabStop = false;
            // 
            // lbl_cep
            // 
            this.lbl_cep.AutoSize = true;
            this.lbl_cep.Location = new System.Drawing.Point(586, 39);
            this.lbl_cep.Name = "lbl_cep";
            this.lbl_cep.Size = new System.Drawing.Size(28, 13);
            this.lbl_cep.TabIndex = 22;
            this.lbl_cep.Text = "CEP";
            // 
            // lbl_cidade
            // 
            this.lbl_cidade.AutoSize = true;
            this.lbl_cidade.Location = new System.Drawing.Point(6, 83);
            this.lbl_cidade.Name = "lbl_cidade";
            this.lbl_cidade.Size = new System.Drawing.Size(40, 13);
            this.lbl_cidade.TabIndex = 20;
            this.lbl_cidade.Text = "Cidade";
            // 
            // text_bairro
            // 
            this.text_bairro.BackColor = System.Drawing.SystemColors.Control;
            this.text_bairro.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.text_bairro.ForeColor = System.Drawing.Color.Blue;
            this.text_bairro.Location = new System.Drawing.Point(310, 55);
            this.text_bairro.MaxLength = 50;
            this.text_bairro.Name = "text_bairro";
            this.text_bairro.ReadOnly = true;
            this.text_bairro.Size = new System.Drawing.Size(253, 20);
            this.text_bairro.TabIndex = 10;
            this.text_bairro.TabStop = false;
            // 
            // lbl_bairro
            // 
            this.lbl_bairro.AutoSize = true;
            this.lbl_bairro.Location = new System.Drawing.Point(307, 39);
            this.lbl_bairro.Name = "lbl_bairro";
            this.lbl_bairro.Size = new System.Drawing.Size(34, 13);
            this.lbl_bairro.TabIndex = 18;
            this.lbl_bairro.Text = "Bairro";
            // 
            // text_complemento
            // 
            this.text_complemento.BackColor = System.Drawing.SystemColors.Control;
            this.text_complemento.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.text_complemento.ForeColor = System.Drawing.Color.Blue;
            this.text_complemento.Location = new System.Drawing.Point(9, 55);
            this.text_complemento.MaxLength = 100;
            this.text_complemento.Name = "text_complemento";
            this.text_complemento.ReadOnly = true;
            this.text_complemento.Size = new System.Drawing.Size(283, 20);
            this.text_complemento.TabIndex = 9;
            this.text_complemento.TabStop = false;
            // 
            // lbl_complemento
            // 
            this.lbl_complemento.AutoSize = true;
            this.lbl_complemento.Location = new System.Drawing.Point(6, 39);
            this.lbl_complemento.Name = "lbl_complemento";
            this.lbl_complemento.Size = new System.Drawing.Size(71, 13);
            this.lbl_complemento.TabIndex = 16;
            this.lbl_complemento.Text = "Complemento";
            // 
            // text_numero
            // 
            this.text_numero.BackColor = System.Drawing.SystemColors.Control;
            this.text_numero.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.text_numero.ForeColor = System.Drawing.Color.Blue;
            this.text_numero.Location = new System.Drawing.Point(589, 16);
            this.text_numero.MaxLength = 10;
            this.text_numero.Name = "text_numero";
            this.text_numero.ReadOnly = true;
            this.text_numero.Size = new System.Drawing.Size(172, 20);
            this.text_numero.TabIndex = 8;
            this.text_numero.TabStop = false;
            // 
            // lbl_numero
            // 
            this.lbl_numero.AutoSize = true;
            this.lbl_numero.Location = new System.Drawing.Point(586, 3);
            this.lbl_numero.Name = "lbl_numero";
            this.lbl_numero.Size = new System.Drawing.Size(44, 13);
            this.lbl_numero.TabIndex = 14;
            this.lbl_numero.Text = "Número";
            // 
            // text_endereco
            // 
            this.text_endereco.BackColor = System.Drawing.SystemColors.Control;
            this.text_endereco.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.text_endereco.ForeColor = System.Drawing.Color.Blue;
            this.text_endereco.Location = new System.Drawing.Point(9, 16);
            this.text_endereco.MaxLength = 100;
            this.text_endereco.Name = "text_endereco";
            this.text_endereco.ReadOnly = true;
            this.text_endereco.Size = new System.Drawing.Size(554, 20);
            this.text_endereco.TabIndex = 7;
            this.text_endereco.TabStop = false;
            // 
            // lbl_endereco
            // 
            this.lbl_endereco.AutoSize = true;
            this.lbl_endereco.Location = new System.Drawing.Point(6, 3);
            this.lbl_endereco.Name = "lbl_endereco";
            this.lbl_endereco.Size = new System.Drawing.Size(53, 13);
            this.lbl_endereco.TabIndex = 12;
            this.lbl_endereco.Text = "Endereço";
            // 
            // tbEnderecoCob
            // 
            this.tbEnderecoCob.BackColor = System.Drawing.Color.White;
            this.tbEnderecoCob.Controls.Add(this.text_cidadeCobranca);
            this.tbEnderecoCob.Controls.Add(this.groupBox1);
            this.tbEnderecoCob.Controls.Add(this.text_ufCob);
            this.tbEnderecoCob.Controls.Add(this.lbl_ufCob);
            this.tbEnderecoCob.Controls.Add(this.text_cepCob);
            this.tbEnderecoCob.Controls.Add(this.lbl_cepCob);
            this.tbEnderecoCob.Controls.Add(this.lbl_cidadeCob);
            this.tbEnderecoCob.Controls.Add(this.text_bairroCob);
            this.tbEnderecoCob.Controls.Add(this.lbl_bairroCob);
            this.tbEnderecoCob.Controls.Add(this.text_complementoCob);
            this.tbEnderecoCob.Controls.Add(this.lbl_complementoCob);
            this.tbEnderecoCob.Controls.Add(this.text_numeroCob);
            this.tbEnderecoCob.Controls.Add(this.lbl_numeroCob);
            this.tbEnderecoCob.Controls.Add(this.text_enderecoCob);
            this.tbEnderecoCob.Controls.Add(this.lbl_enderecoCob);
            this.tbEnderecoCob.Location = new System.Drawing.Point(4, 22);
            this.tbEnderecoCob.Name = "tbEnderecoCob";
            this.tbEnderecoCob.Padding = new System.Windows.Forms.Padding(3);
            this.tbEnderecoCob.Size = new System.Drawing.Size(767, 238);
            this.tbEnderecoCob.TabIndex = 3;
            this.tbEnderecoCob.Text = "Endereco de Cobrança";
            // 
            // text_cidadeCobranca
            // 
            this.text_cidadeCobranca.BackColor = System.Drawing.SystemColors.Control;
            this.text_cidadeCobranca.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.text_cidadeCobranca.ForeColor = System.Drawing.Color.Black;
            this.text_cidadeCobranca.Location = new System.Drawing.Point(9, 97);
            this.text_cidadeCobranca.MaxLength = 100;
            this.text_cidadeCobranca.Name = "text_cidadeCobranca";
            this.text_cidadeCobranca.ReadOnly = true;
            this.text_cidadeCobranca.Size = new System.Drawing.Size(283, 20);
            this.text_cidadeCobranca.TabIndex = 84;
            this.text_cidadeCobranca.TabStop = false;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.text_telefone2Cob);
            this.groupBox1.Controls.Add(this.text_telefone1Cob);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Location = new System.Drawing.Point(433, 75);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(328, 50);
            this.groupBox1.TabIndex = 83;
            this.groupBox1.TabStop = false;
            // 
            // text_telefone2Cob
            // 
            this.text_telefone2Cob.BackColor = System.Drawing.SystemColors.Control;
            this.text_telefone2Cob.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.text_telefone2Cob.ForeColor = System.Drawing.Color.Blue;
            this.text_telefone2Cob.Location = new System.Drawing.Point(167, 24);
            this.text_telefone2Cob.MaxLength = 15;
            this.text_telefone2Cob.Name = "text_telefone2Cob";
            this.text_telefone2Cob.ReadOnly = true;
            this.text_telefone2Cob.Size = new System.Drawing.Size(149, 20);
            this.text_telefone2Cob.TabIndex = 33;
            this.text_telefone2Cob.TabStop = false;
            // 
            // text_telefone1Cob
            // 
            this.text_telefone1Cob.BackColor = System.Drawing.SystemColors.Control;
            this.text_telefone1Cob.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.text_telefone1Cob.ForeColor = System.Drawing.Color.Blue;
            this.text_telefone1Cob.Location = new System.Drawing.Point(9, 24);
            this.text_telefone1Cob.MaxLength = 15;
            this.text_telefone1Cob.Name = "text_telefone1Cob";
            this.text_telefone1Cob.ReadOnly = true;
            this.text_telefone1Cob.Size = new System.Drawing.Size(149, 20);
            this.text_telefone1Cob.TabIndex = 14;
            this.text_telefone1Cob.TabStop = false;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(6, 8);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(49, 13);
            this.label1.TabIndex = 11;
            this.label1.Text = "Telefone";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(164, 8);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(24, 13);
            this.label2.TabIndex = 26;
            this.label2.Text = "Fax";
            // 
            // text_ufCob
            // 
            this.text_ufCob.BackColor = System.Drawing.SystemColors.Control;
            this.text_ufCob.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.text_ufCob.ForeColor = System.Drawing.Color.Black;
            this.text_ufCob.Location = new System.Drawing.Point(310, 99);
            this.text_ufCob.MaxLength = 100;
            this.text_ufCob.Name = "text_ufCob";
            this.text_ufCob.ReadOnly = true;
            this.text_ufCob.Size = new System.Drawing.Size(43, 20);
            this.text_ufCob.TabIndex = 82;
            this.text_ufCob.TabStop = false;
            // 
            // lbl_ufCob
            // 
            this.lbl_ufCob.AutoSize = true;
            this.lbl_ufCob.Location = new System.Drawing.Point(307, 83);
            this.lbl_ufCob.Name = "lbl_ufCob";
            this.lbl_ufCob.Size = new System.Drawing.Size(21, 13);
            this.lbl_ufCob.TabIndex = 81;
            this.lbl_ufCob.Text = "UF";
            // 
            // text_cepCob
            // 
            this.text_cepCob.BackColor = System.Drawing.SystemColors.Control;
            this.text_cepCob.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.text_cepCob.ForeColor = System.Drawing.Color.Black;
            this.text_cepCob.Location = new System.Drawing.Point(589, 55);
            this.text_cepCob.Mask = "99,999-999";
            this.text_cepCob.Name = "text_cepCob";
            this.text_cepCob.PromptChar = ' ';
            this.text_cepCob.ReadOnly = true;
            this.text_cepCob.Size = new System.Drawing.Size(82, 20);
            this.text_cepCob.TabIndex = 72;
            this.text_cepCob.TabStop = false;
            // 
            // lbl_cepCob
            // 
            this.lbl_cepCob.AutoSize = true;
            this.lbl_cepCob.Location = new System.Drawing.Point(586, 39);
            this.lbl_cepCob.Name = "lbl_cepCob";
            this.lbl_cepCob.Size = new System.Drawing.Size(28, 13);
            this.lbl_cepCob.TabIndex = 80;
            this.lbl_cepCob.Text = "CEP";
            // 
            // lbl_cidadeCob
            // 
            this.lbl_cidadeCob.AutoSize = true;
            this.lbl_cidadeCob.Location = new System.Drawing.Point(6, 81);
            this.lbl_cidadeCob.Name = "lbl_cidadeCob";
            this.lbl_cidadeCob.Size = new System.Drawing.Size(40, 13);
            this.lbl_cidadeCob.TabIndex = 79;
            this.lbl_cidadeCob.Text = "Cidade";
            // 
            // text_bairroCob
            // 
            this.text_bairroCob.BackColor = System.Drawing.SystemColors.Control;
            this.text_bairroCob.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.text_bairroCob.ForeColor = System.Drawing.Color.Black;
            this.text_bairroCob.Location = new System.Drawing.Point(310, 55);
            this.text_bairroCob.MaxLength = 100;
            this.text_bairroCob.Name = "text_bairroCob";
            this.text_bairroCob.ReadOnly = true;
            this.text_bairroCob.Size = new System.Drawing.Size(253, 20);
            this.text_bairroCob.TabIndex = 71;
            this.text_bairroCob.TabStop = false;
            // 
            // lbl_bairroCob
            // 
            this.lbl_bairroCob.AutoSize = true;
            this.lbl_bairroCob.Location = new System.Drawing.Point(307, 39);
            this.lbl_bairroCob.Name = "lbl_bairroCob";
            this.lbl_bairroCob.Size = new System.Drawing.Size(34, 13);
            this.lbl_bairroCob.TabIndex = 78;
            this.lbl_bairroCob.Text = "Bairro";
            // 
            // text_complementoCob
            // 
            this.text_complementoCob.BackColor = System.Drawing.SystemColors.Control;
            this.text_complementoCob.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.text_complementoCob.ForeColor = System.Drawing.Color.Black;
            this.text_complementoCob.Location = new System.Drawing.Point(9, 55);
            this.text_complementoCob.MaxLength = 100;
            this.text_complementoCob.Name = "text_complementoCob";
            this.text_complementoCob.ReadOnly = true;
            this.text_complementoCob.Size = new System.Drawing.Size(283, 20);
            this.text_complementoCob.TabIndex = 70;
            this.text_complementoCob.TabStop = false;
            // 
            // lbl_complementoCob
            // 
            this.lbl_complementoCob.AutoSize = true;
            this.lbl_complementoCob.Location = new System.Drawing.Point(6, 39);
            this.lbl_complementoCob.Name = "lbl_complementoCob";
            this.lbl_complementoCob.Size = new System.Drawing.Size(71, 13);
            this.lbl_complementoCob.TabIndex = 77;
            this.lbl_complementoCob.Text = "Complemento";
            // 
            // text_numeroCob
            // 
            this.text_numeroCob.BackColor = System.Drawing.SystemColors.Control;
            this.text_numeroCob.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.text_numeroCob.ForeColor = System.Drawing.Color.Black;
            this.text_numeroCob.Location = new System.Drawing.Point(589, 16);
            this.text_numeroCob.MaxLength = 30;
            this.text_numeroCob.Name = "text_numeroCob";
            this.text_numeroCob.ReadOnly = true;
            this.text_numeroCob.Size = new System.Drawing.Size(172, 20);
            this.text_numeroCob.TabIndex = 69;
            this.text_numeroCob.TabStop = false;
            // 
            // lbl_numeroCob
            // 
            this.lbl_numeroCob.AutoSize = true;
            this.lbl_numeroCob.Location = new System.Drawing.Point(586, 3);
            this.lbl_numeroCob.Name = "lbl_numeroCob";
            this.lbl_numeroCob.Size = new System.Drawing.Size(44, 13);
            this.lbl_numeroCob.TabIndex = 76;
            this.lbl_numeroCob.Text = "Número";
            // 
            // text_enderecoCob
            // 
            this.text_enderecoCob.BackColor = System.Drawing.SystemColors.Control;
            this.text_enderecoCob.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.text_enderecoCob.ForeColor = System.Drawing.Color.Black;
            this.text_enderecoCob.Location = new System.Drawing.Point(9, 16);
            this.text_enderecoCob.MaxLength = 200;
            this.text_enderecoCob.Name = "text_enderecoCob";
            this.text_enderecoCob.ReadOnly = true;
            this.text_enderecoCob.Size = new System.Drawing.Size(554, 20);
            this.text_enderecoCob.TabIndex = 68;
            this.text_enderecoCob.TabStop = false;
            // 
            // lbl_enderecoCob
            // 
            this.lbl_enderecoCob.AutoSize = true;
            this.lbl_enderecoCob.Location = new System.Drawing.Point(6, 3);
            this.lbl_enderecoCob.Name = "lbl_enderecoCob";
            this.lbl_enderecoCob.Size = new System.Drawing.Size(53, 13);
            this.lbl_enderecoCob.TabIndex = 73;
            this.lbl_enderecoCob.Text = "Endereço";
            // 
            // tbCnae
            // 
            this.tbCnae.BackColor = System.Drawing.Color.White;
            this.tbCnae.Controls.Add(this.grb_cnaePrincipal);
            this.tbCnae.Controls.Add(this.grd_cnae);
            this.tbCnae.Location = new System.Drawing.Point(4, 22);
            this.tbCnae.Name = "tbCnae";
            this.tbCnae.Padding = new System.Windows.Forms.Padding(3);
            this.tbCnae.Size = new System.Drawing.Size(767, 238);
            this.tbCnae.TabIndex = 1;
            this.tbCnae.Text = "CNAE";
            // 
            // grb_cnaePrincipal
            // 
            this.grb_cnaePrincipal.Controls.Add(this.text_cnaePrincipal);
            this.grb_cnaePrincipal.Location = new System.Drawing.Point(588, 6);
            this.grb_cnaePrincipal.Name = "grb_cnaePrincipal";
            this.grb_cnaePrincipal.Size = new System.Drawing.Size(169, 56);
            this.grb_cnaePrincipal.TabIndex = 5;
            this.grb_cnaePrincipal.TabStop = false;
            this.grb_cnaePrincipal.Text = "Cnae Principal";
            // 
            // text_cnaePrincipal
            // 
            this.text_cnaePrincipal.BackColor = System.Drawing.SystemColors.Control;
            this.text_cnaePrincipal.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.text_cnaePrincipal.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.text_cnaePrincipal.Location = new System.Drawing.Point(15, 19);
            this.text_cnaePrincipal.Mask = "00,00-0/00";
            this.text_cnaePrincipal.Name = "text_cnaePrincipal";
            this.text_cnaePrincipal.ReadOnly = true;
            this.text_cnaePrincipal.Size = new System.Drawing.Size(138, 20);
            this.text_cnaePrincipal.TabIndex = 6;
            this.text_cnaePrincipal.TabStop = false;
            this.text_cnaePrincipal.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // grd_cnae
            // 
            this.grd_cnae.AllowUserToAddRows = false;
            this.grd_cnae.AllowUserToDeleteRows = false;
            this.grd_cnae.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.DisplayedCells;
            this.grd_cnae.BackgroundColor = System.Drawing.Color.White;
            this.grd_cnae.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.grd_cnae.Location = new System.Drawing.Point(6, 6);
            this.grd_cnae.Name = "grd_cnae";
            this.grd_cnae.ReadOnly = true;
            this.grd_cnae.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.grd_cnae.Size = new System.Drawing.Size(563, 173);
            this.grd_cnae.TabIndex = 0;
            this.grd_cnae.TabStop = false;
            // 
            // tbAdicionais
            // 
            this.tbAdicionais.BackColor = System.Drawing.Color.White;
            this.tbAdicionais.Location = new System.Drawing.Point(4, 22);
            this.tbAdicionais.Name = "tbAdicionais";
            this.tbAdicionais.Size = new System.Drawing.Size(767, 238);
            this.tbAdicionais.TabIndex = 2;
            this.tbAdicionais.Text = "Adicionais";
            // 
            // tbMedicoCoordenador
            // 
            this.tbMedicoCoordenador.Controls.Add(this.grb_coordenador);
            this.tbMedicoCoordenador.Location = new System.Drawing.Point(4, 22);
            this.tbMedicoCoordenador.Name = "tbMedicoCoordenador";
            this.tbMedicoCoordenador.Padding = new System.Windows.Forms.Padding(3);
            this.tbMedicoCoordenador.Size = new System.Drawing.Size(767, 238);
            this.tbMedicoCoordenador.TabIndex = 4;
            this.tbMedicoCoordenador.Text = "Médico Coordenador";
            this.tbMedicoCoordenador.UseVisualStyleBackColor = true;
            // 
            // grb_coordenador
            // 
            this.grb_coordenador.Controls.Add(this.text_nomeCoordenador);
            this.grb_coordenador.Location = new System.Drawing.Point(3, 6);
            this.grb_coordenador.Name = "grb_coordenador";
            this.grb_coordenador.Size = new System.Drawing.Size(761, 54);
            this.grb_coordenador.TabIndex = 2;
            this.grb_coordenador.TabStop = false;
            // 
            // text_nomeCoordenador
            // 
            this.text_nomeCoordenador.BackColor = System.Drawing.SystemColors.Control;
            this.text_nomeCoordenador.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.text_nomeCoordenador.Location = new System.Drawing.Point(6, 19);
            this.text_nomeCoordenador.MaxLength = 200;
            this.text_nomeCoordenador.Name = "text_nomeCoordenador";
            this.text_nomeCoordenador.ReadOnly = true;
            this.text_nomeCoordenador.Size = new System.Drawing.Size(729, 20);
            this.text_nomeCoordenador.TabIndex = 1;
            this.text_nomeCoordenador.TabStop = false;
            // 
            // tbLogo
            // 
            this.tbLogo.Controls.Add(this.pic_box_logo);
            this.tbLogo.Location = new System.Drawing.Point(4, 22);
            this.tbLogo.Name = "tbLogo";
            this.tbLogo.Padding = new System.Windows.Forms.Padding(3);
            this.tbLogo.Size = new System.Drawing.Size(767, 238);
            this.tbLogo.TabIndex = 5;
            this.tbLogo.Text = "Logo";
            this.tbLogo.UseVisualStyleBackColor = true;
            // 
            // pic_box_logo
            // 
            this.pic_box_logo.BackColor = System.Drawing.Color.White;
            this.pic_box_logo.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pic_box_logo.Location = new System.Drawing.Point(36, 26);
            this.pic_box_logo.Name = "pic_box_logo";
            this.pic_box_logo.Size = new System.Drawing.Size(120, 100);
            this.pic_box_logo.TabIndex = 25;
            this.pic_box_logo.TabStop = false;
            // 
            // tbFuncao
            // 
            this.tbFuncao.Controls.Add(this.dg_funcao);
            this.tbFuncao.Location = new System.Drawing.Point(4, 22);
            this.tbFuncao.Name = "tbFuncao";
            this.tbFuncao.Padding = new System.Windows.Forms.Padding(3);
            this.tbFuncao.Size = new System.Drawing.Size(767, 238);
            this.tbFuncao.TabIndex = 6;
            this.tbFuncao.Text = "Funções";
            this.tbFuncao.UseVisualStyleBackColor = true;
            // 
            // dg_funcao
            // 
            this.dg_funcao.AllowUserToAddRows = false;
            this.dg_funcao.AllowUserToDeleteRows = false;
            this.dg_funcao.AllowUserToResizeRows = false;
            this.dg_funcao.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this.dg_funcao.BackgroundColor = System.Drawing.Color.White;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dg_funcao.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.dg_funcao.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dg_funcao.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dg_funcao.Location = new System.Drawing.Point(3, 3);
            this.dg_funcao.Name = "dg_funcao";
            this.dg_funcao.ReadOnly = true;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dg_funcao.RowHeadersDefaultCellStyle = dataGridViewCellStyle2;
            this.dg_funcao.Size = new System.Drawing.Size(761, 232);
            this.dg_funcao.TabIndex = 4;
            // 
            // tbEstudo
            // 
            this.tbEstudo.Controls.Add(this.lbl_telefoneContato);
            this.tbEstudo.Controls.Add(this.text_escopo);
            this.tbEstudo.Controls.Add(this.text_telefoneContato);
            this.tbEstudo.Controls.Add(this.lbl_escopo);
            this.tbEstudo.Controls.Add(this.text_documento);
            this.tbEstudo.Controls.Add(this.lbl_documento);
            this.tbEstudo.Controls.Add(this.text_jornada);
            this.tbEstudo.Controls.Add(this.lbl_jornada);
            this.tbEstudo.Controls.Add(this.text_cargo);
            this.tbEstudo.Controls.Add(this.grb_estimativa);
            this.tbEstudo.Controls.Add(this.lbl_cargo);
            this.tbEstudo.Controls.Add(this.text_nome);
            this.tbEstudo.Controls.Add(this.lbl_responsavel);
            this.tbEstudo.Location = new System.Drawing.Point(4, 22);
            this.tbEstudo.Name = "tbEstudo";
            this.tbEstudo.Padding = new System.Windows.Forms.Padding(3);
            this.tbEstudo.Size = new System.Drawing.Size(767, 238);
            this.tbEstudo.TabIndex = 7;
            this.tbEstudo.Text = "PPRA / PCMSO";
            this.tbEstudo.UseVisualStyleBackColor = true;
            // 
            // text_escopo
            // 
            this.text_escopo.BackColor = System.Drawing.SystemColors.Control;
            this.text_escopo.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.text_escopo.ForeColor = System.Drawing.Color.Black;
            this.text_escopo.Location = new System.Drawing.Point(6, 147);
            this.text_escopo.Multiline = true;
            this.text_escopo.Name = "text_escopo";
            this.text_escopo.ReadOnly = true;
            this.text_escopo.Size = new System.Drawing.Size(755, 36);
            this.text_escopo.TabIndex = 40;
            this.text_escopo.TabStop = false;
            // 
            // lbl_escopo
            // 
            this.lbl_escopo.AutoSize = true;
            this.lbl_escopo.Location = new System.Drawing.Point(6, 131);
            this.lbl_escopo.Name = "lbl_escopo";
            this.lbl_escopo.Size = new System.Drawing.Size(105, 13);
            this.lbl_escopo.TabIndex = 39;
            this.lbl_escopo.Text = "Escopo da Atividade";
            // 
            // text_jornada
            // 
            this.text_jornada.BackColor = System.Drawing.SystemColors.Control;
            this.text_jornada.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.text_jornada.ForeColor = System.Drawing.Color.Black;
            this.text_jornada.Location = new System.Drawing.Point(6, 204);
            this.text_jornada.Multiline = true;
            this.text_jornada.Name = "text_jornada";
            this.text_jornada.ReadOnly = true;
            this.text_jornada.Size = new System.Drawing.Size(755, 30);
            this.text_jornada.TabIndex = 38;
            this.text_jornada.TabStop = false;
            // 
            // lbl_jornada
            // 
            this.lbl_jornada.AutoSize = true;
            this.lbl_jornada.Location = new System.Drawing.Point(6, 188);
            this.lbl_jornada.Name = "lbl_jornada";
            this.lbl_jornada.Size = new System.Drawing.Size(105, 13);
            this.lbl_jornada.TabIndex = 37;
            this.lbl_jornada.Text = "Jornada de Trabalho";
            // 
            // grb_estimativa
            // 
            this.grb_estimativa.Controls.Add(this.text_estFem);
            this.grb_estimativa.Controls.Add(this.text_estMasc);
            this.grb_estimativa.Controls.Add(this.lbl_estFem);
            this.grb_estimativa.Controls.Add(this.lbl_estMasc);
            this.grb_estimativa.Location = new System.Drawing.Point(3, 6);
            this.grb_estimativa.Name = "grb_estimativa";
            this.grb_estimativa.Size = new System.Drawing.Size(211, 50);
            this.grb_estimativa.TabIndex = 36;
            this.grb_estimativa.TabStop = false;
            this.grb_estimativa.Text = "Estimativa Funcionários";
            // 
            // text_estFem
            // 
            this.text_estFem.BackColor = System.Drawing.SystemColors.Control;
            this.text_estFem.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.text_estFem.ForeColor = System.Drawing.Color.Blue;
            this.text_estFem.Location = new System.Drawing.Point(166, 19);
            this.text_estFem.Name = "text_estFem";
            this.text_estFem.ReadOnly = true;
            this.text_estFem.Size = new System.Drawing.Size(36, 20);
            this.text_estFem.TabIndex = 4;
            this.text_estFem.TabStop = false;
            // 
            // text_estMasc
            // 
            this.text_estMasc.BackColor = System.Drawing.SystemColors.Control;
            this.text_estMasc.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.text_estMasc.ForeColor = System.Drawing.Color.Blue;
            this.text_estMasc.Location = new System.Drawing.Point(66, 18);
            this.text_estMasc.Name = "text_estMasc";
            this.text_estMasc.ReadOnly = true;
            this.text_estMasc.Size = new System.Drawing.Size(36, 20);
            this.text_estMasc.TabIndex = 3;
            this.text_estMasc.TabStop = false;
            // 
            // lbl_estFem
            // 
            this.lbl_estFem.AutoSize = true;
            this.lbl_estFem.Location = new System.Drawing.Point(108, 22);
            this.lbl_estFem.Name = "lbl_estFem";
            this.lbl_estFem.Size = new System.Drawing.Size(52, 13);
            this.lbl_estFem.TabIndex = 2;
            this.lbl_estFem.Text = "Feminino:";
            // 
            // lbl_estMasc
            // 
            this.lbl_estMasc.AutoSize = true;
            this.lbl_estMasc.Location = new System.Drawing.Point(6, 22);
            this.lbl_estMasc.Name = "lbl_estMasc";
            this.lbl_estMasc.Size = new System.Drawing.Size(58, 13);
            this.lbl_estMasc.TabIndex = 0;
            this.lbl_estMasc.Text = "Masculino:";
            // 
            // tbConfiguracao
            // 
            this.tbConfiguracao.Controls.Add(this.chk_prestador);
            this.tbConfiguracao.Controls.Add(this.text_aliquotaIss);
            this.tbConfiguracao.Controls.Add(this.lbl_aliquotaIss);
            this.tbConfiguracao.Controls.Add(this.chk_geraCobrancaValorLiquido);
            this.tbConfiguracao.Controls.Add(this.chk_destacaIss);
            this.tbConfiguracao.Controls.Add(this.chk_particular);
            this.tbConfiguracao.Controls.Add(this.chk_usaContrato);
            this.tbConfiguracao.Controls.Add(this.chk_vip);
            this.tbConfiguracao.Location = new System.Drawing.Point(4, 22);
            this.tbConfiguracao.Name = "tbConfiguracao";
            this.tbConfiguracao.Padding = new System.Windows.Forms.Padding(3);
            this.tbConfiguracao.Size = new System.Drawing.Size(767, 238);
            this.tbConfiguracao.TabIndex = 8;
            this.tbConfiguracao.Text = "Configuração";
            this.tbConfiguracao.UseVisualStyleBackColor = true;
            // 
            // chk_prestador
            // 
            this.chk_prestador.AutoSize = true;
            this.chk_prestador.Enabled = false;
            this.chk_prestador.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.chk_prestador.Location = new System.Drawing.Point(9, 77);
            this.chk_prestador.Name = "chk_prestador";
            this.chk_prestador.Size = new System.Drawing.Size(118, 17);
            this.chk_prestador.TabIndex = 46;
            this.chk_prestador.Text = "Pode ser prestador?";
            this.chk_prestador.UseVisualStyleBackColor = true;
            // 
            // text_aliquotaIss
            // 
            this.text_aliquotaIss.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.text_aliquotaIss.Location = new System.Drawing.Point(9, 160);
            this.text_aliquotaIss.MaxLength = 11;
            this.text_aliquotaIss.Name = "text_aliquotaIss";
            this.text_aliquotaIss.ReadOnly = true;
            this.text_aliquotaIss.Size = new System.Drawing.Size(74, 20);
            this.text_aliquotaIss.TabIndex = 45;
            this.text_aliquotaIss.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // lbl_aliquotaIss
            // 
            this.lbl_aliquotaIss.AutoSize = true;
            this.lbl_aliquotaIss.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.lbl_aliquotaIss.Location = new System.Drawing.Point(6, 144);
            this.lbl_aliquotaIss.Name = "lbl_aliquotaIss";
            this.lbl_aliquotaIss.Size = new System.Drawing.Size(82, 13);
            this.lbl_aliquotaIss.TabIndex = 44;
            this.lbl_aliquotaIss.Text = "Alíquota de ISS";
            // 
            // chk_geraCobrancaValorLiquido
            // 
            this.chk_geraCobrancaValorLiquido.AutoSize = true;
            this.chk_geraCobrancaValorLiquido.Enabled = false;
            this.chk_geraCobrancaValorLiquido.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.chk_geraCobrancaValorLiquido.Location = new System.Drawing.Point(9, 124);
            this.chk_geraCobrancaValorLiquido.Name = "chk_geraCobrancaValorLiquido";
            this.chk_geraCobrancaValorLiquido.Size = new System.Drawing.Size(264, 17);
            this.chk_geraCobrancaValorLiquido.TabIndex = 43;
            this.chk_geraCobrancaValorLiquido.Text = "Gerar Cobrança com Valor Líquido de Nota Fiscal?";
            this.chk_geraCobrancaValorLiquido.UseVisualStyleBackColor = true;
            // 
            // chk_destacaIss
            // 
            this.chk_destacaIss.AutoSize = true;
            this.chk_destacaIss.Enabled = false;
            this.chk_destacaIss.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.chk_destacaIss.Location = new System.Drawing.Point(9, 101);
            this.chk_destacaIss.Name = "chk_destacaIss";
            this.chk_destacaIss.Size = new System.Drawing.Size(155, 17);
            this.chk_destacaIss.TabIndex = 42;
            this.chk_destacaIss.Text = "Destaca ISS na nota fiscal?";
            this.chk_destacaIss.UseVisualStyleBackColor = true;
            // 
            // chk_particular
            // 
            this.chk_particular.AutoSize = true;
            this.chk_particular.Enabled = false;
            this.chk_particular.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.chk_particular.Location = new System.Drawing.Point(9, 52);
            this.chk_particular.Name = "chk_particular";
            this.chk_particular.Size = new System.Drawing.Size(108, 17);
            this.chk_particular.TabIndex = 40;
            this.chk_particular.Text = "Cliente Particular?";
            this.chk_particular.UseVisualStyleBackColor = true;
            // 
            // chk_usaContrato
            // 
            this.chk_usaContrato.AutoSize = true;
            this.chk_usaContrato.Enabled = false;
            this.chk_usaContrato.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.chk_usaContrato.Location = new System.Drawing.Point(9, 28);
            this.chk_usaContrato.Name = "chk_usaContrato";
            this.chk_usaContrato.Size = new System.Drawing.Size(124, 17);
            this.chk_usaContrato.TabIndex = 26;
            this.chk_usaContrato.Text = "Cliente usa Contrato?";
            this.chk_usaContrato.UseVisualStyleBackColor = true;
            // 
            // chk_vip
            // 
            this.chk_vip.AutoSize = true;
            this.chk_vip.Enabled = false;
            this.chk_vip.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.chk_vip.Location = new System.Drawing.Point(9, 6);
            this.chk_vip.Name = "chk_vip";
            this.chk_vip.Size = new System.Drawing.Size(81, 17);
            this.chk_vip.TabIndex = 39;
            this.chk_vip.Text = "Cliente VIP?";
            this.chk_vip.UseVisualStyleBackColor = true;
            // 
            // tbCredenciada
            // 
            this.tbCredenciada.Controls.Add(this.gbrCredenciada);
            this.tbCredenciada.Location = new System.Drawing.Point(4, 22);
            this.tbCredenciada.Name = "tbCredenciada";
            this.tbCredenciada.Size = new System.Drawing.Size(767, 238);
            this.tbCredenciada.TabIndex = 9;
            this.tbCredenciada.Text = "Credenciadas";
            this.tbCredenciada.UseVisualStyleBackColor = true;
            // 
            // gbrCredenciada
            // 
            this.gbrCredenciada.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gbrCredenciada.Controls.Add(this.dgCredenciada);
            this.gbrCredenciada.Location = new System.Drawing.Point(6, 3);
            this.gbrCredenciada.Name = "gbrCredenciada";
            this.gbrCredenciada.Size = new System.Drawing.Size(758, 229);
            this.gbrCredenciada.TabIndex = 1;
            this.gbrCredenciada.TabStop = false;
            this.gbrCredenciada.Text = "Empresas Credenciadas";
            // 
            // dgCredenciada
            // 
            this.dgCredenciada.AllowUserToAddRows = false;
            this.dgCredenciada.AllowUserToDeleteRows = false;
            this.dgCredenciada.AllowUserToOrderColumns = true;
            this.dgCredenciada.AllowUserToResizeRows = false;
            this.dgCredenciada.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.DisplayedCells;
            this.dgCredenciada.BackgroundColor = System.Drawing.Color.White;
            this.dgCredenciada.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle3.BackColor = System.Drawing.Color.LightGray;
            dataGridViewCellStyle3.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgCredenciada.DefaultCellStyle = dataGridViewCellStyle3;
            this.dgCredenciada.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgCredenciada.Location = new System.Drawing.Point(3, 16);
            this.dgCredenciada.MultiSelect = false;
            this.dgCredenciada.Name = "dgCredenciada";
            this.dgCredenciada.ReadOnly = true;
            this.dgCredenciada.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgCredenciada.Size = new System.Drawing.Size(752, 210);
            this.dgCredenciada.TabIndex = 0;
            // 
            // grb_dataCadastro
            // 
            this.grb_dataCadastro.BackColor = System.Drawing.Color.White;
            this.grb_dataCadastro.Controls.Add(this.dt_cadastro);
            this.grb_dataCadastro.Enabled = false;
            this.grb_dataCadastro.Location = new System.Drawing.Point(541, -1);
            this.grb_dataCadastro.Name = "grb_dataCadastro";
            this.grb_dataCadastro.Size = new System.Drawing.Size(98, 40);
            this.grb_dataCadastro.TabIndex = 34;
            this.grb_dataCadastro.TabStop = false;
            this.grb_dataCadastro.Text = "Data Cadastro";
            // 
            // dt_cadastro
            // 
            this.dt_cadastro.Enabled = false;
            this.dt_cadastro.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dt_cadastro.Location = new System.Drawing.Point(6, 14);
            this.dt_cadastro.Name = "dt_cadastro";
            this.dt_cadastro.Size = new System.Drawing.Size(86, 20);
            this.dt_cadastro.TabIndex = 0;
            this.dt_cadastro.TabStop = false;
            // 
            // grb_vendedor
            // 
            this.grb_vendedor.BackColor = System.Drawing.Color.White;
            this.grb_vendedor.Controls.Add(this.text_vendedor);
            this.grb_vendedor.Location = new System.Drawing.Point(7, 0);
            this.grb_vendedor.Name = "grb_vendedor";
            this.grb_vendedor.Size = new System.Drawing.Size(529, 39);
            this.grb_vendedor.TabIndex = 32;
            this.grb_vendedor.TabStop = false;
            this.grb_vendedor.Text = "Vendedor";
            // 
            // text_vendedor
            // 
            this.text_vendedor.BackColor = System.Drawing.SystemColors.Control;
            this.text_vendedor.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.text_vendedor.ForeColor = System.Drawing.Color.Blue;
            this.text_vendedor.Location = new System.Drawing.Point(6, 14);
            this.text_vendedor.Name = "text_vendedor";
            this.text_vendedor.ReadOnly = true;
            this.text_vendedor.Size = new System.Drawing.Size(512, 20);
            this.text_vendedor.TabIndex = 5;
            this.text_vendedor.TabStop = false;
            // 
            // grb_dados
            // 
            this.grb_dados.BackColor = System.Drawing.Color.White;
            this.grb_dados.Controls.Add(this.text_razaoSocial);
            this.grb_dados.Controls.Add(this.text_inscricao);
            this.grb_dados.Controls.Add(this.text_fantasia);
            this.grb_dados.Controls.Add(this.text_cnpj);
            this.grb_dados.Controls.Add(this.lbl_razaoSocial);
            this.grb_dados.Controls.Add(this.lbl_inscricao);
            this.grb_dados.Controls.Add(this.lbl_fantasia);
            this.grb_dados.Controls.Add(this.lbl_cnpj);
            this.grb_dados.Location = new System.Drawing.Point(7, 118);
            this.grb_dados.Name = "grb_dados";
            this.grb_dados.Size = new System.Drawing.Size(776, 82);
            this.grb_dados.TabIndex = 31;
            this.grb_dados.TabStop = false;
            this.grb_dados.Text = "Dados";
            // 
            // text_razaoSocial
            // 
            this.text_razaoSocial.BackColor = System.Drawing.SystemColors.Control;
            this.text_razaoSocial.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.text_razaoSocial.ForeColor = System.Drawing.Color.Blue;
            this.text_razaoSocial.Location = new System.Drawing.Point(6, 26);
            this.text_razaoSocial.MaxLength = 150;
            this.text_razaoSocial.Name = "text_razaoSocial";
            this.text_razaoSocial.ReadOnly = true;
            this.text_razaoSocial.Size = new System.Drawing.Size(533, 20);
            this.text_razaoSocial.TabIndex = 1;
            this.text_razaoSocial.TabStop = false;
            // 
            // text_inscricao
            // 
            this.text_inscricao.BackColor = System.Drawing.SystemColors.Control;
            this.text_inscricao.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.text_inscricao.Location = new System.Drawing.Point(561, 59);
            this.text_inscricao.MaxLength = 20;
            this.text_inscricao.Name = "text_inscricao";
            this.text_inscricao.ReadOnly = true;
            this.text_inscricao.Size = new System.Drawing.Size(184, 20);
            this.text_inscricao.TabIndex = 4;
            this.text_inscricao.TabStop = false;
            // 
            // text_fantasia
            // 
            this.text_fantasia.BackColor = System.Drawing.SystemColors.Control;
            this.text_fantasia.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.text_fantasia.Location = new System.Drawing.Point(6, 59);
            this.text_fantasia.MaxLength = 100;
            this.text_fantasia.Name = "text_fantasia";
            this.text_fantasia.ReadOnly = true;
            this.text_fantasia.Size = new System.Drawing.Size(533, 20);
            this.text_fantasia.TabIndex = 3;
            this.text_fantasia.TabStop = false;
            // 
            // text_cnpj
            // 
            this.text_cnpj.BackColor = System.Drawing.SystemColors.Control;
            this.text_cnpj.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.text_cnpj.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.text_cnpj.ForeColor = System.Drawing.Color.Black;
            this.text_cnpj.Location = new System.Drawing.Point(561, 26);
            this.text_cnpj.Mask = "99,999,999/9999-99";
            this.text_cnpj.Name = "text_cnpj";
            this.text_cnpj.PromptChar = ' ';
            this.text_cnpj.ReadOnly = true;
            this.text_cnpj.Size = new System.Drawing.Size(146, 20);
            this.text_cnpj.TabIndex = 2;
            this.text_cnpj.TabStop = false;
            this.text_cnpj.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // lbl_razaoSocial
            // 
            this.lbl_razaoSocial.AutoSize = true;
            this.lbl_razaoSocial.Location = new System.Drawing.Point(3, 13);
            this.lbl_razaoSocial.Name = "lbl_razaoSocial";
            this.lbl_razaoSocial.Size = new System.Drawing.Size(70, 13);
            this.lbl_razaoSocial.TabIndex = 7;
            this.lbl_razaoSocial.Text = "Razão Social";
            // 
            // lbl_inscricao
            // 
            this.lbl_inscricao.AutoSize = true;
            this.lbl_inscricao.Location = new System.Drawing.Point(558, 46);
            this.lbl_inscricao.Name = "lbl_inscricao";
            this.lbl_inscricao.Size = new System.Drawing.Size(94, 13);
            this.lbl_inscricao.TabIndex = 8;
            this.lbl_inscricao.Text = "Inscrição Estadual";
            // 
            // lbl_fantasia
            // 
            this.lbl_fantasia.AutoSize = true;
            this.lbl_fantasia.Location = new System.Drawing.Point(3, 46);
            this.lbl_fantasia.Name = "lbl_fantasia";
            this.lbl_fantasia.Size = new System.Drawing.Size(78, 13);
            this.lbl_fantasia.TabIndex = 10;
            this.lbl_fantasia.Text = "Nome Fantasia";
            // 
            // lbl_cnpj
            // 
            this.lbl_cnpj.AutoSize = true;
            this.lbl_cnpj.Location = new System.Drawing.Point(558, 13);
            this.lbl_cnpj.Name = "lbl_cnpj";
            this.lbl_cnpj.Size = new System.Drawing.Size(34, 13);
            this.lbl_cnpj.TabIndex = 9;
            this.lbl_cnpj.Text = "CNPJ";
            // 
            // cb_tipoCliente
            // 
            this.cb_tipoCliente.BackColor = System.Drawing.Color.LightGray;
            this.cb_tipoCliente.Enabled = false;
            this.cb_tipoCliente.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cb_tipoCliente.FormattingEnabled = true;
            this.cb_tipoCliente.Location = new System.Drawing.Point(8, 13);
            this.cb_tipoCliente.Name = "cb_tipoCliente";
            this.cb_tipoCliente.Size = new System.Drawing.Size(115, 21);
            this.cb_tipoCliente.TabIndex = 0;
            // 
            // gb_credenciada
            // 
            this.gb_credenciada.Controls.Add(this.text_credenciada);
            this.gb_credenciada.Enabled = false;
            this.gb_credenciada.Location = new System.Drawing.Point(7, 80);
            this.gb_credenciada.Name = "gb_credenciada";
            this.gb_credenciada.Size = new System.Drawing.Size(776, 36);
            this.gb_credenciada.TabIndex = 42;
            this.gb_credenciada.TabStop = false;
            this.gb_credenciada.Text = "Credenciada";
            // 
            // text_credenciada
            // 
            this.text_credenciada.BackColor = System.Drawing.Color.LightGray;
            this.text_credenciada.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.text_credenciada.Enabled = false;
            this.text_credenciada.Location = new System.Drawing.Point(6, 12);
            this.text_credenciada.Name = "text_credenciada";
            this.text_credenciada.Size = new System.Drawing.Size(763, 20);
            this.text_credenciada.TabIndex = 1;
            // 
            // text_matriz
            // 
            this.text_matriz.BackColor = System.Drawing.Color.LightGray;
            this.text_matriz.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.text_matriz.Enabled = false;
            this.text_matriz.Location = new System.Drawing.Point(8, 16);
            this.text_matriz.Name = "text_matriz";
            this.text_matriz.Size = new System.Drawing.Size(616, 20);
            this.text_matriz.TabIndex = 1;
            // 
            // gb_Matriz
            // 
            this.gb_Matriz.Controls.Add(this.text_matriz);
            this.gb_Matriz.Enabled = false;
            this.gb_Matriz.Location = new System.Drawing.Point(7, 39);
            this.gb_Matriz.Name = "gb_Matriz";
            this.gb_Matriz.Size = new System.Drawing.Size(630, 40);
            this.gb_Matriz.TabIndex = 41;
            this.gb_Matriz.TabStop = false;
            this.gb_Matriz.Text = "Matriz";
            // 
            // grb_tipoCliente
            // 
            this.grb_tipoCliente.Controls.Add(this.cb_tipoCliente);
            this.grb_tipoCliente.Enabled = false;
            this.grb_tipoCliente.Location = new System.Drawing.Point(643, 40);
            this.grb_tipoCliente.Name = "grb_tipoCliente";
            this.grb_tipoCliente.Size = new System.Drawing.Size(140, 39);
            this.grb_tipoCliente.TabIndex = 40;
            this.grb_tipoCliente.TabStop = false;
            this.grb_tipoCliente.Text = "Tipo Cliente";
            // 
            // frm_cliente_detalhar
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(794, 568);
            this.Controls.Add(this.tb_paginacao);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "frm_cliente_detalhar";
            this.Text = "DETALHAR CLIENTE";
            this.Controls.SetChildIndex(this.panel_banner, 0);
            this.Controls.SetChildIndex(this.panel, 0);
            this.Controls.SetChildIndex(this.tb_paginacao, 0);
            this.panel_banner.ResumeLayout(false);
            this.panel_banner.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox_banner)).EndInit();
            this.panel.ResumeLayout(false);
            this.grb_situacao.ResumeLayout(false);
            this.grb_situacao.PerformLayout();
            this.grb_paginacao.ResumeLayout(false);
            this.tb_paginacao.ResumeLayout(false);
            this.tbEndereco.ResumeLayout(false);
            this.tbEndereco.PerformLayout();
            this.grb_telefone.ResumeLayout(false);
            this.grb_telefone.PerformLayout();
            this.tbEnderecoCob.ResumeLayout(false);
            this.tbEnderecoCob.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.tbCnae.ResumeLayout(false);
            this.grb_cnaePrincipal.ResumeLayout(false);
            this.grb_cnaePrincipal.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grd_cnae)).EndInit();
            this.tbMedicoCoordenador.ResumeLayout(false);
            this.grb_coordenador.ResumeLayout(false);
            this.grb_coordenador.PerformLayout();
            this.tbLogo.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pic_box_logo)).EndInit();
            this.tbFuncao.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dg_funcao)).EndInit();
            this.tbEstudo.ResumeLayout(false);
            this.tbEstudo.PerformLayout();
            this.grb_estimativa.ResumeLayout(false);
            this.grb_estimativa.PerformLayout();
            this.tbConfiguracao.ResumeLayout(false);
            this.tbConfiguracao.PerformLayout();
            this.tbCredenciada.ResumeLayout(false);
            this.gbrCredenciada.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgCredenciada)).EndInit();
            this.grb_dataCadastro.ResumeLayout(false);
            this.grb_vendedor.ResumeLayout(false);
            this.grb_vendedor.PerformLayout();
            this.grb_dados.ResumeLayout(false);
            this.grb_dados.PerformLayout();
            this.gb_credenciada.ResumeLayout(false);
            this.gb_credenciada.PerformLayout();
            this.gb_Matriz.ResumeLayout(false);
            this.gb_Matriz.PerformLayout();
            this.grb_tipoCliente.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox grb_situacao;
        private System.Windows.Forms.RadioButton rb_desativado;
        private System.Windows.Forms.RadioButton rb_ativo;
        private System.Windows.Forms.TextBox text_documento;
        private System.Windows.Forms.Label lbl_documento;
        private System.Windows.Forms.TextBox text_cargo;
        private System.Windows.Forms.Label lbl_cargo;
        private System.Windows.Forms.TextBox text_nome;
        private System.Windows.Forms.Label lbl_responsavel;
        private System.Windows.Forms.GroupBox grb_paginacao;
        private System.Windows.Forms.Button btn_fechar;
        private System.Windows.Forms.TabControl tb_paginacao;
        private System.Windows.Forms.TabPage tbEndereco;
        private System.Windows.Forms.TextBox text_uf;
        private System.Windows.Forms.TextBox text_site;
        private System.Windows.Forms.Label lbl_site;
        private System.Windows.Forms.TextBox text_email;
        private System.Windows.Forms.Label lbl_email;
        private System.Windows.Forms.GroupBox grb_telefone;
        private System.Windows.Forms.TextBox text_telefone2;
        private System.Windows.Forms.TextBox text_telefone1;
        private System.Windows.Forms.Label lbl_telefone;
        private System.Windows.Forms.Label lbl_fax;
        private System.Windows.Forms.Label lbl_uf;
        private System.Windows.Forms.MaskedTextBox text_cep;
        private System.Windows.Forms.Label lbl_cep;
        private System.Windows.Forms.Label lbl_cidade;
        private System.Windows.Forms.TextBox text_bairro;
        private System.Windows.Forms.Label lbl_bairro;
        private System.Windows.Forms.TextBox text_complemento;
        private System.Windows.Forms.Label lbl_complemento;
        private System.Windows.Forms.TextBox text_numero;
        private System.Windows.Forms.Label lbl_numero;
        private System.Windows.Forms.TextBox text_endereco;
        private System.Windows.Forms.Label lbl_endereco;
        private System.Windows.Forms.TabPage tbCnae;
        private System.Windows.Forms.DataGridView grd_cnae;
        private System.Windows.Forms.TabPage tbAdicionais;
        private System.Windows.Forms.GroupBox grb_dataCadastro;
        private System.Windows.Forms.DateTimePicker dt_cadastro;
        private System.Windows.Forms.GroupBox grb_vendedor;
        public System.Windows.Forms.TextBox text_vendedor;
        private System.Windows.Forms.GroupBox grb_dados;
        private System.Windows.Forms.TextBox text_razaoSocial;
        private System.Windows.Forms.TextBox text_inscricao;
        private System.Windows.Forms.TextBox text_fantasia;
        private System.Windows.Forms.MaskedTextBox text_cnpj;
        private System.Windows.Forms.Label lbl_razaoSocial;
        private System.Windows.Forms.Label lbl_inscricao;
        private System.Windows.Forms.Label lbl_fantasia;
        private System.Windows.Forms.Label lbl_cnpj;
        private System.Windows.Forms.GroupBox grb_estimativa;
        private System.Windows.Forms.TextBox text_estFem;
        private System.Windows.Forms.TextBox text_estMasc;
        private System.Windows.Forms.Label lbl_estFem;
        private System.Windows.Forms.Label lbl_estMasc;
        private System.Windows.Forms.GroupBox grb_cnaePrincipal;
        public System.Windows.Forms.MaskedTextBox text_cnaePrincipal;
        private System.Windows.Forms.TabPage tbEnderecoCob;
        private System.Windows.Forms.TextBox text_ufCob;
        private System.Windows.Forms.Label lbl_ufCob;
        private System.Windows.Forms.MaskedTextBox text_cepCob;
        private System.Windows.Forms.Label lbl_cepCob;
        private System.Windows.Forms.Label lbl_cidadeCob;
        private System.Windows.Forms.TextBox text_bairroCob;
        private System.Windows.Forms.Label lbl_bairroCob;
        private System.Windows.Forms.TextBox text_complementoCob;
        private System.Windows.Forms.Label lbl_complementoCob;
        private System.Windows.Forms.TextBox text_numeroCob;
        private System.Windows.Forms.Label lbl_numeroCob;
        private System.Windows.Forms.TextBox text_enderecoCob;
        private System.Windows.Forms.Label lbl_enderecoCob;
        private System.Windows.Forms.TabPage tbMedicoCoordenador;
        private System.Windows.Forms.GroupBox grb_coordenador;
        public System.Windows.Forms.TextBox text_nomeCoordenador;
        private System.Windows.Forms.TabPage tbLogo;
        private System.Windows.Forms.PictureBox pic_box_logo;
        private System.Windows.Forms.CheckBox chk_vip;
        private System.Windows.Forms.TabPage tbFuncao;
        private System.Windows.Forms.DataGridView dg_funcao;
        private System.Windows.Forms.Label lbl_telefoneContato;
        private System.Windows.Forms.TextBox text_telefoneContato;
        private System.Windows.Forms.CheckBox chk_usaContrato;
        private System.Windows.Forms.TabPage tbEstudo;
        private System.Windows.Forms.TabPage tbConfiguracao;
        private System.Windows.Forms.TextBox text_escopo;
        private System.Windows.Forms.Label lbl_escopo;
        private System.Windows.Forms.TextBox text_jornada;
        private System.Windows.Forms.Label lbl_jornada;
        private System.Windows.Forms.GroupBox gb_credenciada;
        private System.Windows.Forms.TextBox text_credenciada;
        private System.Windows.Forms.GroupBox gb_Matriz;
        private System.Windows.Forms.TextBox text_matriz;
        private System.Windows.Forms.GroupBox grb_tipoCliente;
        private System.Windows.Forms.ComboBox cb_tipoCliente;
        private System.Windows.Forms.CheckBox chk_particular;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.TextBox text_telefone2Cob;
        private System.Windows.Forms.TextBox text_telefone1Cob;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox text_cidade;
        private System.Windows.Forms.TextBox text_cidadeCobranca;
        private System.Windows.Forms.CheckBox chk_geraCobrancaValorLiquido;
        private System.Windows.Forms.CheckBox chk_destacaIss;
        private System.Windows.Forms.TextBox text_aliquotaIss;
        private System.Windows.Forms.Label lbl_aliquotaIss;
        private System.Windows.Forms.CheckBox chk_prestador;
        private System.Windows.Forms.TabPage tbCredenciada;
        private System.Windows.Forms.GroupBox gbrCredenciada;
        private System.Windows.Forms.DataGridView dgCredenciada;
    }
}