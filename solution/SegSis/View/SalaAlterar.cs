﻿using SWS.Entidade;
using SWS.Facade;
using SWS.View.Resources;
using SWS.ViewHelper;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace SWS.View
{
    public partial class frmSalaAlterar : frmSalaIncluir 
    {
        public frmSalaAlterar(Sala sala) :base()
        {
            InitializeComponent();

            /* preenchendo os dados da sala */
            this.Sala = sala;

            textNome.Text = Sala.Descricao;
            textAndar.Text = sala.Andar.ToString();
            cbVIP.SelectedValue = sala.Vip ? "true" : "false";
            ExamesInSala = sala.Exames;
            textSlug.Text = Sala.SlugSala;
            textQtdeChamada.Text = sala.QtdeChamada.ToString();
            montaGrid();
        }

        protected override void btn_incluir_Click(object sender, EventArgs e)
        {
            try
            {
                if (validaCampos())
                {
                    this.Cursor = Cursors.WaitCursor;

                    FilaFacade filaFacade = FilaFacade.getInstance();

                    sala = new Sala(sala.Id, textNome.Text, ApplicationConstants.ATIVO, Convert.ToInt32(textAndar.Text), Convert.ToBoolean(((SelectItem)cbVIP.SelectedItem).Valor), PermissionamentoFacade.usuarioAutenticado.Empresa, textSlug.Text, Convert.ToInt32(textQtdeChamada.Text));
                    sala = filaFacade.updateSala(sala);
                    MessageBox.Show("Sala alterada com sucesso!", "Confirmação", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    this.Close();
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }

        }

        protected override void btn_incluirExame_Click(object sender, EventArgs e)
        {
            try
            {
                frmExameBuscar buscarExame = new frmExameBuscar(true);
                buscarExame.ShowDialog();

                StringBuilder resultado = new StringBuilder(); 

                if (buscarExame.Exames.Count > 0 )
                {
                    FilaFacade filaFacade = FilaFacade.getInstance();

                    buscarExame.Exames.ForEach(delegate(Exame exame)
                    {
                        if (ExamesInSala.Exists(x => x.Exame.Id == exame.Id))
                            resultado.Append(exame.Descricao + System.Environment.NewLine);
                        else
                            ExamesInSala.Add(filaFacade.insertSalaExame(new SalaExame(null, exame, Sala, 0, 0, ApplicationConstants.ATIVO)));
                    });

                    if (!string.IsNullOrEmpty(resultado.ToString()))
                        MessageBox.Show("O(s) exame(s): " + resultado.ToString() + "Já estavam incluído(s) na sala.", "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Warning);

                    montaGrid();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }

        protected override void btn_excluirExame_Click(object sender, EventArgs e)
        {
            try
            {
                if (dgvExame.CurrentRow == null)
                    throw new Exception("Selecione uma linha.");

                if (MessageBox.Show("Deseja excluir o exame selecionado?", "Atenção", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                {
                    FilaFacade filaFacade = FilaFacade.getInstance();
                    SalaExame salaExameRemove = ExamesInSala.Find(x => x.Exame.Id == (long)dgvExame.CurrentRow.Cells[1].Value);
                    filaFacade.deleteSalaExame(salaExameRemove);
                    ExamesInSala.Remove(salaExameRemove);
                    dgvExame.Rows.RemoveAt(dgvExame.CurrentRow.Index);
                    MessageBox.Show("Exame removido com sucesso.", "Confirmação", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }

        protected override void dgvExame_CellLeave(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                if (e.ColumnIndex == 4)
                {
                    if (string.IsNullOrEmpty(dgvExame.Rows[e.RowIndex].Cells[4].EditedFormattedValue.ToString()))
                        dgvExame.Rows[e.RowIndex].Cells[4].Value = 0;

                    FilaFacade filaFacade = FilaFacade.getInstance();
                    SalaExame salaExameAlterar = ExamesInSala.Find(x => x.Id == (long)dgvExame.Rows[e.RowIndex].Cells[0].Value);
                    salaExameAlterar.TempoMedioAtendimento = Convert.ToInt32(dgvExame.Rows[e.RowIndex].Cells[4].Value);
                    filaFacade.updateSalaExame(salaExameAlterar);
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            
            
        }
    }
}
