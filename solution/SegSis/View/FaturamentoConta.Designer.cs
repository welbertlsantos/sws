﻿namespace SWS.View
{
    partial class frmFaturamentoConta
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            this.flpAcao = new System.Windows.Forms.FlowLayoutPanel();
            this.btn_confirma = new System.Windows.Forms.Button();
            this.btn_fechar = new System.Windows.Forms.Button();
            this.lblBanco = new System.Windows.Forms.TextBox();
            this.cb_banco = new SWS.ComboBoxWithBorder();
            this.grb_conta = new System.Windows.Forms.GroupBox();
            this.dg_conta = new System.Windows.Forms.DataGridView();
            ((System.ComponentModel.ISupportInitialize)(this.scForm)).BeginInit();
            this.scForm.Panel1.SuspendLayout();
            this.scForm.Panel2.SuspendLayout();
            this.scForm.SuspendLayout();
            this.pnlForm.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.banner)).BeginInit();
            this.flpAcao.SuspendLayout();
            this.grb_conta.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dg_conta)).BeginInit();
            this.SuspendLayout();
            // 
            // scForm
            // 
            // 
            // scForm.Panel1
            // 
            this.scForm.Panel1.Controls.Add(this.flpAcao);
            // 
            // pnlForm
            // 
            this.pnlForm.Controls.Add(this.grb_conta);
            this.pnlForm.Controls.Add(this.cb_banco);
            this.pnlForm.Controls.Add(this.lblBanco);
            // 
            // flpAcao
            // 
            this.flpAcao.Controls.Add(this.btn_confirma);
            this.flpAcao.Controls.Add(this.btn_fechar);
            this.flpAcao.Dock = System.Windows.Forms.DockStyle.Fill;
            this.flpAcao.Location = new System.Drawing.Point(0, 0);
            this.flpAcao.Name = "flpAcao";
            this.flpAcao.Size = new System.Drawing.Size(534, 30);
            this.flpAcao.TabIndex = 0;
            // 
            // btn_confirma
            // 
            this.btn_confirma.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_confirma.Image = global::SWS.Properties.Resources.Gravar;
            this.btn_confirma.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btn_confirma.Location = new System.Drawing.Point(3, 3);
            this.btn_confirma.Name = "btn_confirma";
            this.btn_confirma.Size = new System.Drawing.Size(75, 23);
            this.btn_confirma.TabIndex = 2;
            this.btn_confirma.Text = "&Confirma";
            this.btn_confirma.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btn_confirma.UseVisualStyleBackColor = true;
            this.btn_confirma.Click += new System.EventHandler(this.btn_confirma_Click);
            // 
            // btn_fechar
            // 
            this.btn_fechar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_fechar.Image = global::SWS.Properties.Resources.close;
            this.btn_fechar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btn_fechar.Location = new System.Drawing.Point(84, 3);
            this.btn_fechar.Name = "btn_fechar";
            this.btn_fechar.Size = new System.Drawing.Size(75, 23);
            this.btn_fechar.TabIndex = 3;
            this.btn_fechar.Text = "&Fechar";
            this.btn_fechar.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btn_fechar.UseVisualStyleBackColor = true;
            this.btn_fechar.Click += new System.EventHandler(this.btn_fechar_Click);
            // 
            // lblBanco
            // 
            this.lblBanco.BackColor = System.Drawing.Color.LightGray;
            this.lblBanco.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblBanco.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblBanco.Location = new System.Drawing.Point(12, 13);
            this.lblBanco.Name = "lblBanco";
            this.lblBanco.ReadOnly = true;
            this.lblBanco.Size = new System.Drawing.Size(100, 21);
            this.lblBanco.TabIndex = 0;
            this.lblBanco.TabStop = false;
            this.lblBanco.Text = "Banco";
            // 
            // cb_banco
            // 
            this.cb_banco.BackColor = System.Drawing.Color.LightGray;
            this.cb_banco.BorderColor = System.Drawing.Color.DimGray;
            this.cb_banco.BorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid;
            this.cb_banco.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cb_banco.FormattingEnabled = true;
            this.cb_banco.Location = new System.Drawing.Point(111, 13);
            this.cb_banco.Name = "cb_banco";
            this.cb_banco.Size = new System.Drawing.Size(386, 21);
            this.cb_banco.TabIndex = 1;
            this.cb_banco.SelectedIndexChanged += new System.EventHandler(this.cb_banco_SelectedIndexChanged);
            // 
            // grb_conta
            // 
            this.grb_conta.Controls.Add(this.dg_conta);
            this.grb_conta.Location = new System.Drawing.Point(12, 44);
            this.grb_conta.Name = "grb_conta";
            this.grb_conta.Size = new System.Drawing.Size(488, 230);
            this.grb_conta.TabIndex = 2;
            this.grb_conta.TabStop = false;
            this.grb_conta.Text = "Conta";
            // 
            // dg_conta
            // 
            this.dg_conta.AllowUserToAddRows = false;
            this.dg_conta.AllowUserToDeleteRows = false;
            this.dg_conta.AllowUserToOrderColumns = true;
            this.dg_conta.AllowUserToResizeRows = false;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.Gainsboro;
            this.dg_conta.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle1;
            this.dg_conta.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.DisplayedCells;
            this.dg_conta.BackgroundColor = System.Drawing.Color.White;
            this.dg_conta.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.Color.LightGray;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dg_conta.DefaultCellStyle = dataGridViewCellStyle2;
            this.dg_conta.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dg_conta.Location = new System.Drawing.Point(3, 16);
            this.dg_conta.MultiSelect = false;
            this.dg_conta.Name = "dg_conta";
            this.dg_conta.ReadOnly = true;
            this.dg_conta.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dg_conta.Size = new System.Drawing.Size(482, 211);
            this.dg_conta.TabIndex = 0;
            this.dg_conta.CellMouseDoubleClick += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.dg_conta_CellMouseDoubleClick);
            // 
            // frmFaturamentoConta
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(534, 361);
            this.Name = "frmFaturamentoConta";
            this.Text = "CONTA PARA FATURAMENTO";
            this.scForm.Panel1.ResumeLayout(false);
            this.scForm.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.scForm)).EndInit();
            this.scForm.ResumeLayout(false);
            this.pnlForm.ResumeLayout(false);
            this.pnlForm.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.banner)).EndInit();
            this.flpAcao.ResumeLayout(false);
            this.grb_conta.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dg_conta)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.FlowLayoutPanel flpAcao;
        private System.Windows.Forms.Button btn_fechar;
        private System.Windows.Forms.Button btn_confirma;
        private System.Windows.Forms.TextBox lblBanco;
        private ComboBoxWithBorder cb_banco;
        private System.Windows.Forms.GroupBox grb_conta;
        private System.Windows.Forms.DataGridView dg_conta;
    }
}