﻿namespace SWS.View
{
    partial class frmPcmsoIncluirTipo
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmPcmsoIncluirTipo));
            this.btIncluirPcmso = new System.Windows.Forms.Button();
            this.btPcmsoAvulso = new System.Windows.Forms.Button();
            this.lblIncluirPcmso = new System.Windows.Forms.Label();
            this.lblIncluirPcmsoAvulso = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // btIncluirPcmso
            // 
            this.btIncluirPcmso.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btIncluirPcmso.Image = global::SWS.Properties.Resources.proposta;
            this.btIncluirPcmso.Location = new System.Drawing.Point(298, 21);
            this.btIncluirPcmso.Name = "btIncluirPcmso";
            this.btIncluirPcmso.Size = new System.Drawing.Size(123, 68);
            this.btIncluirPcmso.TabIndex = 4;
            this.btIncluirPcmso.UseVisualStyleBackColor = true;
            this.btIncluirPcmso.Click += new System.EventHandler(this.btIncluirPcmso_Click);
            // 
            // btPcmsoAvulso
            // 
            this.btPcmsoAvulso.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btPcmsoAvulso.Image = global::SWS.Properties.Resources.contrato;
            this.btPcmsoAvulso.Location = new System.Drawing.Point(133, 21);
            this.btPcmsoAvulso.Name = "btPcmsoAvulso";
            this.btPcmsoAvulso.Size = new System.Drawing.Size(123, 68);
            this.btPcmsoAvulso.TabIndex = 3;
            this.btPcmsoAvulso.UseVisualStyleBackColor = true;
            this.btPcmsoAvulso.Click += new System.EventHandler(this.btPcmsoAvulso_Click);
            // 
            // lblIncluirPcmso
            // 
            this.lblIncluirPcmso.AutoSize = true;
            this.lblIncluirPcmso.Location = new System.Drawing.Point(320, 96);
            this.lblIncluirPcmso.Name = "lblIncluirPcmso";
            this.lblIncluirPcmso.Size = new System.Drawing.Size(76, 13);
            this.lblIncluirPcmso.TabIndex = 6;
            this.lblIncluirPcmso.Text = "Incluir PCMSO";
            // 
            // lblIncluirPcmsoAvulso
            // 
            this.lblIncluirPcmsoAvulso.AutoSize = true;
            this.lblIncluirPcmsoAvulso.Location = new System.Drawing.Point(146, 96);
            this.lblIncluirPcmsoAvulso.Name = "lblIncluirPcmsoAvulso";
            this.lblIncluirPcmsoAvulso.Size = new System.Drawing.Size(110, 13);
            this.lblIncluirPcmsoAvulso.TabIndex = 5;
            this.lblIncluirPcmsoAvulso.Text = "Incluir PCMSO avulso";
            // 
            // frmPcmsoIncluirTipo
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(555, 132);
            this.Controls.Add(this.lblIncluirPcmso);
            this.Controls.Add(this.lblIncluirPcmsoAvulso);
            this.Controls.Add(this.btIncluirPcmso);
            this.Controls.Add(this.btPcmsoAvulso);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmPcmsoIncluirTipo";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "SELECIONAR TIPO DE PCMSO";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btIncluirPcmso;
        private System.Windows.Forms.Button btPcmsoAvulso;
        private System.Windows.Forms.Label lblIncluirPcmso;
        private System.Windows.Forms.Label lblIncluirPcmsoAvulso;
    }
}