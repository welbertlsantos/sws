﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using SWS.Entidade;
using SWS.View.ViewHelper;
using SWS.Facade;
using SWS.View.Resources;
using SWS.ViewHelper;

namespace SWS.View
{
    public partial class frmPcmsoExamesAlterar : frmTemplateConsulta
    {
        private GheFonteAgenteExame gheFonteAgenteExame;

        public GheFonteAgenteExame GheFonteAgenteExame
        {
            get { return gheFonteAgenteExame; }
            set { gheFonteAgenteExame = value; }
        }
        private static String msg4 = "Selecione uma linha,";
        private static String msg5 = "O campo idade não pode ser vazio. Para não limitar a idade para o exame use 0.";
        private static String msg6 = "Exames no setor alterado com sucesso.";

        private Dictionary<Int64, Dictionary<Funcao, Int32>> excecaoIdadeFuncao = new Dictionary<Int64, Dictionary<Funcao, Int32>>();


        public frmPcmsoExamesAlterar(GheFonteAgenteExame gheFonteAgenteExame)
        {
            InitializeComponent();
            this.gheFonteAgenteExame = gheFonteAgenteExame;
            EstudoFacade pcmsoFacade = EstudoFacade.getInstance();
            textGhe.Text = gheFonteAgenteExame.GheFonteAgente.GheFonte.Ghe.Descricao;
            textExame.Text = gheFonteAgenteExame.Exame.Descricao;
            textIdade.Text = gheFonteAgenteExame.IdadeExame.ToString();
            ComboHelper.Boolean(cbAltura, gheFonteAgenteExame.Altura);
            ComboHelper.Boolean(cbEspaco, gheFonteAgenteExame.Confinado);
            ComboHelper.Boolean(cbEletricidade, gheFonteAgenteExame.Eletricidade);
            
            montaGridPeriodicidade();

            excecaoIdadeFuncao = pcmsoFacade.findGheSetorClienteFuncaoExameByGheAndExame(gheFonteAgenteExame.GheFonteAgente.GheFonte.Ghe, gheFonteAgenteExame.Exame);
            montaGrirExcecao();
            ActiveControl = textIdade;

        }

        private void montaGrirExcecao()
        {
            try
            {
                List<Int64> list = new List<Int64>(excecaoIdadeFuncao.Keys);

                KeyValuePair<Funcao, Int32> funcaoExcecao;

                dgvExcecao.Columns.Clear();

                dgvExcecao.ColumnCount = 3;

                dgvExcecao.Columns[0].Name = "ID";
                dgvExcecao.Columns[0].Visible = false;

                dgvExcecao.Columns[1].Name = "Função";
                dgvExcecao.Columns[2].Name = "Idade";

                Int32 rowindex = 0;

                Funcao func = null;

                foreach (KeyValuePair<Int64, Dictionary<Funcao, Int32>> dict in excecaoIdadeFuncao)
                {
                    funcaoExcecao = dict.Value.First();

                    func = funcaoExcecao.Key;

                    this.dgvExcecao.Rows.Insert(rowindex, func.Id, func.Descricao, funcaoExcecao.Value);

                    rowindex++;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }

        private void montaGridPeriodicidade()
        {
            try
            {
                EstudoFacade pcmsoFacade = EstudoFacade.getInstance();
                DataSet ds = pcmsoFacade.findAllPeriodicidadeByGheFonteAgenteExame((Int64)gheFonteAgenteExame.Id);

                //dgvPeriodicidade.DataSource = ds.Tables[0].DefaultView;

                dgvPeriodicidade.ColumnCount = 2;

                dgvPeriodicidade.Columns[0].HeaderText = "id";
                dgvPeriodicidade.Columns[0].Visible = false;
                
                dgvPeriodicidade.Columns[1].HeaderText = "Periodicidade";
                dgvPeriodicidade.Columns[1].Width = 200;

                DataGridViewComboBoxColumn periodoVencimento = new DataGridViewComboBoxColumn();
                dgvPeriodicidade.Columns.Add(periodoVencimento);
                ComboHelper.periodoVencimentoCombo(periodoVencimento);
                dgvPeriodicidade.Columns[2].HeaderText = "Vencimento";
                dgvPeriodicidade.Columns[2].DefaultCellStyle.BackColor = Color.LightYellow;
                dgvPeriodicidade.Columns[2].Width = 150;

                foreach (DataRow dvRow in ds.Tables[0].Rows)
                    dgvPeriodicidade.Rows.Add((long)dvRow["id_periodicidade"], dvRow["descricao"].ToString(), dvRow["periodo_vencimento"] == DBNull.Value ? string.Empty : dvRow["periodo_vencimento"].ToString());

                dgvPeriodicidade.Columns[1].DisplayIndex = 1;
                dgvPeriodicidade.Columns[2].DisplayIndex = 2;


            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btFechar_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btIncluir_Click(object sender, EventArgs e)
        {
            try
            {
                /* atualizando o gheFonteAgenteExame em relação a idade geral */

                EstudoFacade pcmsoFacade = EstudoFacade.getInstance();

                this.Cursor = Cursors.WaitCursor;

                if (String.IsNullOrEmpty(textIdade.Text))
                    throw new Exception(msg5);

                gheFonteAgenteExame.IdadeExame = Convert.ToInt32(textIdade.Text);
                pcmsoFacade.updateGheFonteAgenteExame(new GheFonteAgenteExame((long)gheFonteAgenteExame.Id, gheFonteAgenteExame.Exame, gheFonteAgenteExame.GheFonteAgente, string.IsNullOrEmpty(textIdade.Text) ? 0 : Convert.ToInt32(textIdade.Text), ApplicationConstants.ATIVO, Convert.ToBoolean(((SelectItem)cbEspaco.SelectedItem).Valor), Convert.ToBoolean(((SelectItem)cbAltura.SelectedItem).Valor), Convert.ToBoolean(((SelectItem)cbEletricidade.SelectedItem).Valor)));
                MessageBox.Show(msg6, "Confirmação", MessageBoxButtons.OK, MessageBoxIcon.Information);
                this.Close();

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }

        private void dgvPeriodicidade_DataBindingComplete(object sender, DataGridViewBindingCompleteEventArgs e)
        {
            DataGridView gridView = sender as DataGridView;
            gridView.ClearSelection();
        }

        private void btIncluirExcecao_Click(object sender, EventArgs e)
        {
            try
            {
                frmPcmsoFuncoesGhe funcaoGhe = new frmPcmsoFuncoesGhe(gheFonteAgenteExame.GheFonteAgente.GheFonte.Ghe, gheFonteAgenteExame.Exame);
                funcaoGhe.ShowDialog();

                if (funcaoGhe.ExcecaoIdadeFuncao.Count > 0)
                {
                    excecaoIdadeFuncao.Union(funcaoGhe.ExcecaoIdadeFuncao);
                    montaGrirExcecao();
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void dgvExcecao_CellEndEdit(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                if (dgvExcecao.CurrentCell == null)
                    throw new Exception(msg4);

                if (String.IsNullOrEmpty(dgvExcecao.CurrentRow.Cells[2].EditedFormattedValue.ToString()))
                    throw new Exception(msg5);

                EstudoFacade pcmsoFacade = EstudoFacade.getInstance();

                List<GheSetorClienteFuncaoExame> gheSetorClienteFuncaoExameList = pcmsoFacade.findGheSetorClienteFuncaoExameByGheAndFuncao(gheFonteAgenteExame.GheFonteAgente.GheFonte.Ghe, pcmsoFacade.findFuncaoById((Int64)dgvExcecao.CurrentRow.Cells[0].Value));
                
                GheSetorClienteFuncaoExame gheSetorClienteFuncaoExame = gheSetorClienteFuncaoExameList.First(x => x.Exame.Id == gheFonteAgenteExame.Exame.Id);

                gheSetorClienteFuncaoExame.Idade = Convert.ToInt32(dgvExcecao.CurrentRow.Cells[2].EditedFormattedValue);

                pcmsoFacade.updateGheSetorClienteFuncaoExame(gheSetorClienteFuncaoExame);

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void textIdade_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = ValidaCampoHelper.FormataCampoNumerico(sender, e);
        }

        private void dgvPeriodicidade_CellEndEdit(object sender, DataGridViewCellEventArgs e)
        {
            if (e.ColumnIndex == 2)
            {
                EstudoFacade pcmsoFacade = EstudoFacade.getInstance();
                pcmsoFacade.updateGheFonteAgenteExamePeriodicidade(new GheFonteAgenteExamePeriodicidade(new Periodicidade((long)dgvPeriodicidade.Rows[e.RowIndex].Cells[0].Value, dgvPeriodicidade.Rows[e.RowIndex].Cells[1].Value.ToString(), null), gheFonteAgenteExame, ApplicationConstants.ATIVO, dgvPeriodicidade.Rows[e.RowIndex].Cells[2].Value == string.Empty ? (int?)null : Convert.ToInt32(dgvPeriodicidade.Rows[e.RowIndex].Cells[2].Value)));
                MessageBox.Show("Alteração realizada com sucesso.", "Confirmação", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        
    }
}
