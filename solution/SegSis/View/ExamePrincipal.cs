﻿using SWS.Entidade;
using SWS.Facade;
using SWS.View.Resources;
using SWS.View.ViewHelper;
using SWS.ViewHelper;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace SWS.View
{
    public partial class frmExamePrincipal : frmTemplate
    {
        private Exame exame;
        
        public frmExamePrincipal()
        {
            InitializeComponent();
            validaPermissoes();
            ComboHelper.comboSituacao(cbSituacao);
            ComboHelper.classificacaoExame(cbClassificacaoExame);
        }

        private void bt_incluir_Click(object sender, EventArgs e)
        {
            frmExameIncluir formExameIncluir = new frmExameIncluir();
            formExameIncluir.ShowDialog();

            if (formExameIncluir.Exame != null)
            {
                exame = new Exame();
                montaGridExame();
            }
        }

        private void bt_alterar_Click(object sender, EventArgs e)
        {
            try
            {
                this.Cursor = Cursors.WaitCursor;

                if (grd_exame.CurrentRow == null)
                    throw new Exception("Selecione um exame.");
                
                EstudoFacade pcmsoFacade = EstudoFacade.getInstance();
                Exame exameAltera = pcmsoFacade.findExameById((Int64)grd_exame.CurrentRow.Cells[0].Value);
                
                if (!String.Equals(exameAltera.Situacao, ApplicationConstants.ATIVO))
                    throw new Exception("Somente exames ativos podem ser alterados.");

                frmExameAlterar formExameAlterar = new frmExameAlterar(exameAltera);
                formExameAlterar.ShowDialog();

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

            finally
            {
                this.Cursor = Cursors.Default;
            }
        }

        private void bt_excluir_Click(object sender, EventArgs e)
        {
            try
            {
                this.Cursor = Cursors.WaitCursor;

                if (grd_exame.CurrentRow == null)
                    throw new Exception("Selecione um exame.");
                
                EstudoFacade pcmsoFacade = EstudoFacade.getInstance();

                Exame exameExcluir = pcmsoFacade.findExameById((Int64)grd_exame.CurrentRow.Cells[0].Value);
                    
                if (!String.Equals(exameExcluir.Situacao, ApplicationConstants.ATIVO))
                    throw new Exception("Somente exames ativos podem ser desativados.");

                if (MessageBox.Show("Deseja excluir o desativar selecionado?", "Confirmação", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                {
                    pcmsoFacade.deleteExame(exameExcluir);
                    MessageBox.Show("Exame excluído com sucesso", "Confirmação", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    exame = new Exame();
                    montaGridExame();
                    
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK , MessageBoxIcon.Error);
            }

            finally
            {
                this.Cursor = Cursors.Default;
            }
        }

        private void btn_reativar_Click(object sender, EventArgs e)
        {
            try
            {
                if (grd_exame.CurrentRow == null)
                    throw new Exception("Selecione um exame.");

                if (MessageBox.Show("Deseja reativar o exame selecionado?", "Confirmação", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                {
                    EstudoFacade pcmsoFacade = EstudoFacade.getInstance();

                    Exame exameReativar = pcmsoFacade.findExameById((Int64)grd_exame.CurrentRow.Cells[0].Value);

                    if (!String.Equals(exameReativar.Situacao, ApplicationConstants.DESATIVADO))
                        throw new Exception("Somente exames desativados podem ser reativados.");

                    exameReativar.Situacao = ApplicationConstants.ATIVO;
                    exameReativar = pcmsoFacade.updateExame(exameReativar);
                    MessageBox.Show("Exame reativado com sucesso.", "Confirmação", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    exame = new Exame();
                    montaGridExame();
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void bt_detalhar_Click(object sender, EventArgs e)
        {
            try
            {
                this.Cursor = Cursors.WaitCursor;

                if (grd_exame.CurrentRow == null)
                    throw new Exception("Selecione um exame.");
                
                EstudoFacade pcmsoFacade = EstudoFacade.getInstance();

                frmExameDetalhar formPcmsoDetalhar = new frmExameDetalhar(pcmsoFacade.findExameById((Int64)grd_exame.CurrentRow.Cells[0].Value));
                formPcmsoDetalhar.ShowDialog();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção",MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

            finally
            {
                this.Cursor = Cursors.Default;
            }
        }

        private void bt_pesquisar_Click(object sender, EventArgs e)
        {
            try
            {
                exame = new Exame(null, text_descricao.Text, ((SelectItem)cbSituacao.SelectedItem).Valor, Convert.ToBoolean(((SelectItem)cbClassificacaoExame.SelectedItem).Valor), 0, 0, 0, false, false, string.Empty, false, null, false);
                montaGridExame();
                text_descricao.Focus();
            }
            catch (Exception Ex)
            {
                MessageBox.Show(Ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void bt_fechar_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void validaPermissoes()
        {
            PermissionamentoFacade permissionamentoFacade = PermissionamentoFacade.getInstance();

            bt_alterar.Enabled = permissionamentoFacade.hasPermission("EXAME", "ALTERAR");
            bt_incluir.Enabled = permissionamentoFacade.hasPermission("EXAME", "INCLUIR");
            bt_excluir.Enabled = permissionamentoFacade.hasPermission("EXAME", "EXCLUIR");
            bt_detalhar.Enabled = permissionamentoFacade.hasPermission("EXAME", "DETALHAR");
            btn_reativar.Enabled = permissionamentoFacade.hasPermission("EXAME", "REATIVAR");
        }

        public void montaGridExame()
        {
            try
            {
                this.Cursor = Cursors.WaitCursor;
                EstudoFacade pcmsoFacade = EstudoFacade.getInstance();

                DataSet ds = pcmsoFacade.findExameByFilter(exame);

                grd_exame.DataSource = ds.Tables["Exames"].DefaultView;

                grd_exame.Columns[0].HeaderText = "Código Exame";
                grd_exame.Columns[0].Width = 20;

                grd_exame.Columns[1].HeaderText = "Descrição";

                grd_exame.Columns[2].HeaderText = "Situação";

                grd_exame.Columns[3].HeaderText = "Exame de Laboratório";

                grd_exame.Columns[4].HeaderText = "Preço Tabela";
                grd_exame.Columns[4].Visible = false;

                grd_exame.Columns[5].HeaderText = "Prioridade";
                grd_exame.Columns[5].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;

                grd_exame.Columns[6].HeaderText = "Preço de Custo";
                grd_exame.Columns[6].Visible = false;

                grd_exame.Columns[7].HeaderText = "Exame Externo";
                grd_exame.Columns[7].Visible = false;

                grd_exame.Columns[8].HeaderText = "Documentação";
                grd_exame.Columns[8].Visible = false;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }

        private void bt_limpar_Click(object sender, EventArgs e)
        {
            text_descricao.Text = String.Empty;
            ComboHelper.comboSituacao(cbSituacao);
            grd_exame.Columns.Clear();
            text_descricao.Focus();
        }

        private void grd_exame_RowPrePaint(object sender, DataGridViewRowPrePaintEventArgs e)
        {
            DataGridView dgv = sender as DataGridView;

            if (String.Equals(dgv.Rows[e.RowIndex].Cells[2].Value, ApplicationConstants.DESATIVADO))
                dgv.Rows[e.RowIndex].DefaultCellStyle.ForeColor = Color.Red;

        }

        private void grd_exame_CellMouseDoubleClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            try
            {
                bt_alterar.PerformClick();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }

    }
}
