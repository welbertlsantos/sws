﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace SWS.View
{
    public partial class frmEsocialLoteDetalhe : frmTemplate
    {
        public frmEsocialLoteDetalhe(DataSet atendimentos)
        {
            InitializeComponent();
            montaDataGrid(atendimentos);

        }

        private void btn_fechar_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        public void montaDataGrid(DataSet ds)
        {
            try
            {
                this.Cursor = Cursors.WaitCursor;

                dg_examePendente.DataSource = ds.Tables["Asos"].DefaultView;

                // montando ids das colunas
                dg_examePendente.Columns[0].HeaderText = "id_aso";
                dg_examePendente.Columns[0].Visible = false;

                dg_examePendente.Columns[1].HeaderText = "id_cliente_funcao_funcionario";
                dg_examePendente.Columns[1].Visible = false;

                dg_examePendente.Columns[2].HeaderText = "Código Atendimento";
                dg_examePendente.Columns[2].ReadOnly = true;

                dg_examePendente.Columns[3].HeaderText = "Data";
                dg_examePendente.Columns[3].ReadOnly = true;

                dg_examePendente.Columns[4].HeaderText = "Nome Funcionário";
                dg_examePendente.Columns[4].ReadOnly = true;

                dg_examePendente.Columns[5].HeaderText = "CPF";
                dg_examePendente.Columns[5].ReadOnly = true;

                dg_examePendente.Columns[6].HeaderText = "Matrícula";
                dg_examePendente.Columns[6].ReadOnly = true;

                dg_examePendente.Columns[7].HeaderText = "Razão Social";
                dg_examePendente.Columns[7].ReadOnly = true;

                dg_examePendente.Columns[8].HeaderText = "CNPJ";
                dg_examePendente.Columns[8].ReadOnly = true;

                dg_examePendente.Columns[9].HeaderText = "id_periodicidade";
                dg_examePendente.Columns[9].Visible = false;

                dg_examePendente.Columns[10].HeaderText = "Periodicidade";
                dg_examePendente.Columns[10].ReadOnly = true;

                dg_examePendente.Columns[11].HeaderText = "Situação";
                dg_examePendente.Columns[11].ReadOnly = true;

                dg_examePendente.Columns[12].HeaderText = "Data Finalização";
                dg_examePendente.Columns[12].ReadOnly = true;

                textTotal.Text = dg_examePendente.RowCount.ToString();

                this.Cursor = Cursors.Default;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }

        private void btn_fechar_Click_1(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
