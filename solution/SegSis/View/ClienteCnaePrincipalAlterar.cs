﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using SegSis.Entidade;
using SegSis.Facade;
using SegSis.View.Resources;

namespace SegSis.View
{
    public partial class frmClienteCnaePrincipalAlterar : BaseForm
    {
        Cnae cnaePrincipal = null;
        Cliente cliente = null;
        Boolean alterado = false;
        public Boolean getAlterado()
        {
            return this.alterado;
        }

        public Cnae getCnaePrincipal()
        {
            return this.cnaePrincipal;
        }

        public frmClienteCnaePrincipalAlterar(Cliente cliente)
        {
            InitializeComponent();
            this.cliente = cliente;
            montaDataGrid();
        }

        private void btn_ok_Click(object sender, EventArgs e)
        {
            ClienteFacade clienteFacade = ClienteFacade.getInstance();

            cnaePrincipal = clienteFacade.findCnaeById((Int64)grd_cnae.CurrentRow.Cells[0].Value);
            
            /* verificando se o cnaePrincipal foi alterado */

            Cnae cnaePrincipalOriginal = clienteFacade.findCnaePrincipalCliente((Int64)cliente.getId());

            if (cnaePrincipal != cnaePrincipalOriginal)
            {
                clienteFacade.updateClienteCnae(new ClienteCnae(cliente, cnaePrincipalOriginal, String.Empty), ApplicationConstants.SEGUNDARIO);
                clienteFacade.updateClienteCnae(new ClienteCnae(cliente, cnaePrincipal, String.Empty), ApplicationConstants.PRINCIPAL);
                alterado = true;
            }
            
            this.Close();
        }

        public void montaDataGrid()
        {

            foreach (Cnae cnae in cliente.getCnae())
            {

                grd_cnae.ColumnCount = 3;

                grd_cnae.Columns[0].Name = "ID";
                grd_cnae.Columns[0].Visible = false;

                grd_cnae.Columns[1].Name = "Código Cnae";
                grd_cnae.Columns[2].Name = "Atividade";

                this.grd_cnae.Rows.Add(cnae.getId(), cnae.getCodCnae(), cnae.getAtividade());
            }
        }
    }
}
