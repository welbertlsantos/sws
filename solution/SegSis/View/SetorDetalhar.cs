﻿using SWS.Entidade;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace SWS.View
{
    public partial class frmSetorDetalhar : frmSetorIncluir
    {
        public frmSetorDetalhar(Setor setor) :base()
        {
            InitializeComponent();
            this.Setor = setor;
            textDescricao.Text = Setor.Descricao;
            textDescricao.ReadOnly = true;
            btn_gravar.Visible = false;
        }
    }
}
