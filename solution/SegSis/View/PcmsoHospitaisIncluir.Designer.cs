﻿namespace SWS.View
{
    partial class frm_PcmsoHospitaisIncluir
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frm_PcmsoHospitaisIncluir));
            this.grb_Dados = new System.Windows.Forms.GroupBox();
            this.btn_buscar = new System.Windows.Forms.Button();
            this.text_descricao = new System.Windows.Forms.TextBox();
            this.grb_hospital = new System.Windows.Forms.GroupBox();
            this.grd_hospital = new System.Windows.Forms.DataGridView();
            this.grb_paginacao = new System.Windows.Forms.GroupBox();
            this.btn_novo = new System.Windows.Forms.Button();
            this.btn_fechar = new System.Windows.Forms.Button();
            this.btn_incluir = new System.Windows.Forms.Button();
            this.grb_Dados.SuspendLayout();
            this.grb_hospital.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grd_hospital)).BeginInit();
            this.grb_paginacao.SuspendLayout();
            this.SuspendLayout();
            // 
            // grb_Dados
            // 
            this.grb_Dados.Controls.Add(this.btn_buscar);
            this.grb_Dados.Controls.Add(this.text_descricao);
            this.grb_Dados.Location = new System.Drawing.Point(12, 12);
            this.grb_Dados.Name = "grb_Dados";
            this.grb_Dados.Size = new System.Drawing.Size(576, 78);
            this.grb_Dados.TabIndex = 0;
            this.grb_Dados.TabStop = false;
            this.grb_Dados.Text = "Hospital";
            // 
            // btn_buscar
            // 
            this.btn_buscar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_buscar.Image = global::SWS.Properties.Resources.lupa;
            this.btn_buscar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btn_buscar.Location = new System.Drawing.Point(6, 45);
            this.btn_buscar.Name = "btn_buscar";
            this.btn_buscar.Size = new System.Drawing.Size(75, 23);
            this.btn_buscar.TabIndex = 2;
            this.btn_buscar.Text = "&Buscar";
            this.btn_buscar.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btn_buscar.UseVisualStyleBackColor = true;
            this.btn_buscar.Click += new System.EventHandler(this.btn_buscar_Click);
            // 
            // text_descricao
            // 
            this.text_descricao.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.text_descricao.Location = new System.Drawing.Point(6, 19);
            this.text_descricao.MaxLength = 100;
            this.text_descricao.Name = "text_descricao";
            this.text_descricao.Size = new System.Drawing.Size(564, 20);
            this.text_descricao.TabIndex = 1;
            // 
            // grb_hospital
            // 
            this.grb_hospital.Controls.Add(this.grd_hospital);
            this.grb_hospital.Location = new System.Drawing.Point(12, 97);
            this.grb_hospital.Name = "grb_hospital";
            this.grb_hospital.Size = new System.Drawing.Size(576, 246);
            this.grb_hospital.TabIndex = 1;
            this.grb_hospital.TabStop = false;
            this.grb_hospital.Text = "Hospitais";
            // 
            // grd_hospital
            // 
            this.grd_hospital.AllowUserToAddRows = false;
            this.grd_hospital.AllowUserToDeleteRows = false;
            this.grd_hospital.AllowUserToOrderColumns = true;
            this.grd_hospital.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.DisplayedCells;
            this.grd_hospital.BackgroundColor = System.Drawing.Color.White;
            this.grd_hospital.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.grd_hospital.Location = new System.Drawing.Point(6, 19);
            this.grd_hospital.MultiSelect = false;
            this.grd_hospital.Name = "grd_hospital";
            this.grd_hospital.RowHeadersVisible = false;
            this.grd_hospital.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.grd_hospital.Size = new System.Drawing.Size(564, 221);
            this.grd_hospital.TabIndex = 3;
            // 
            // grb_paginacao
            // 
            this.grb_paginacao.Controls.Add(this.btn_novo);
            this.grb_paginacao.Controls.Add(this.btn_fechar);
            this.grb_paginacao.Controls.Add(this.btn_incluir);
            this.grb_paginacao.Location = new System.Drawing.Point(12, 349);
            this.grb_paginacao.Name = "grb_paginacao";
            this.grb_paginacao.Size = new System.Drawing.Size(576, 42);
            this.grb_paginacao.TabIndex = 0;
            this.grb_paginacao.TabStop = false;
            // 
            // btn_novo
            // 
            this.btn_novo.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_novo.Image = global::SWS.Properties.Resources.icone_mais;
            this.btn_novo.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btn_novo.Location = new System.Drawing.Point(87, 13);
            this.btn_novo.Name = "btn_novo";
            this.btn_novo.Size = new System.Drawing.Size(75, 23);
            this.btn_novo.TabIndex = 5;
            this.btn_novo.Text = "&Novo";
            this.btn_novo.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btn_novo.UseVisualStyleBackColor = true;
            this.btn_novo.Click += new System.EventHandler(this.btn_novo_Click);
            // 
            // btn_fechar
            // 
            this.btn_fechar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_fechar.Image = global::SWS.Properties.Resources.close;
            this.btn_fechar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btn_fechar.Location = new System.Drawing.Point(168, 13);
            this.btn_fechar.Name = "btn_fechar";
            this.btn_fechar.Size = new System.Drawing.Size(75, 23);
            this.btn_fechar.TabIndex = 6;
            this.btn_fechar.Text = "&Fechar";
            this.btn_fechar.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btn_fechar.UseVisualStyleBackColor = true;
            this.btn_fechar.Click += new System.EventHandler(this.btn_fechar_Click);
            // 
            // btn_incluir
            // 
            this.btn_incluir.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_incluir.Image = global::SWS.Properties.Resources.fechar_ico;
            this.btn_incluir.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btn_incluir.Location = new System.Drawing.Point(6, 13);
            this.btn_incluir.Name = "btn_incluir";
            this.btn_incluir.Size = new System.Drawing.Size(75, 23);
            this.btn_incluir.TabIndex = 4;
            this.btn_incluir.Text = "&Incluir";
            this.btn_incluir.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btn_incluir.UseVisualStyleBackColor = true;
            this.btn_incluir.Click += new System.EventHandler(this.btn_incluir_Click);
            // 
            // frm_PcmsoHospitaisIncluir
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(600, 400);
            this.ControlBox = false;
            this.Controls.Add(this.grb_paginacao);
            this.Controls.Add(this.grb_hospital);
            this.Controls.Add(this.grb_Dados);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "frm_PcmsoHospitaisIncluir";
            this.Text = "INCLUIR HOSPITAIS";
            this.grb_Dados.ResumeLayout(false);
            this.grb_Dados.PerformLayout();
            this.grb_hospital.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.grd_hospital)).EndInit();
            this.grb_paginacao.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox grb_Dados;
        private System.Windows.Forms.Button btn_buscar;
        private System.Windows.Forms.TextBox text_descricao;
        private System.Windows.Forms.GroupBox grb_hospital;
        private System.Windows.Forms.DataGridView grd_hospital;
        private System.Windows.Forms.GroupBox grb_paginacao;
        private System.Windows.Forms.Button btn_fechar;
        private System.Windows.Forms.Button btn_incluir;
        private System.Windows.Forms.Button btn_novo;
    }
}