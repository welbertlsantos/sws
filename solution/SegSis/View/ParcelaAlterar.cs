﻿using SWS.Entidade;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace SWS.View
{
    public partial class frmParcelaAlterar : frmParcelaIncluir
    {
        public frmParcelaAlterar(Parcela parcela)
        {
            InitializeComponent();
            Parcela = parcela;
            textDias.Text = parcela.Dias.ToString();
            ActiveControl = textDias;

        }

        protected override void btnGravar_Click(object sender, EventArgs e)
        {
            try
            {
                if (ValidaTela())
                {
                    Parcela = new Parcela(Parcela.Id, Parcela.Plano, Convert.ToInt32(textDias.Text));
                    this.Close();
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }
    }
}
