﻿namespace SWS.View
{
    partial class frmAtendimentoAlterar
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle7 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            this.flpAcao = new System.Windows.Forms.FlowLayoutPanel();
            this.btn_salvar = new System.Windows.Forms.Button();
            this.btn_fechar = new System.Windows.Forms.Button();
            this.lblCodigoAtendimento = new System.Windows.Forms.TextBox();
            this.grbAtendimento = new System.Windows.Forms.GroupBox();
            this.cbEletricidade = new SWS.ComboBoxWithBorder();
            this.lblEletricidade = new System.Windows.Forms.TextBox();
            this.cbConfinado = new SWS.ComboBoxWithBorder();
            this.cbAltura = new SWS.ComboBoxWithBorder();
            this.lblConfinado = new System.Windows.Forms.TextBox();
            this.lblAltura = new System.Windows.Forms.TextBox();
            this.textSenha = new System.Windows.Forms.TextBox();
            this.lblSenha = new System.Windows.Forms.TextBox();
            this.btnTipoAtendimento = new System.Windows.Forms.Button();
            this.cbPrioridade = new SWS.ComboBoxWithBorder();
            this.lblPrioridade = new System.Windows.Forms.TextBox();
            this.lblDataVencimento = new System.Windows.Forms.TextBox();
            this.textCodigoPo = new System.Windows.Forms.TextBox();
            this.lblCodigoPo = new System.Windows.Forms.TextBox();
            this.text_periodicidade = new System.Windows.Forms.TextBox();
            this.text_codigo = new System.Windows.Forms.TextBox();
            this.lblDataAtendimento = new System.Windows.Forms.TextBox();
            this.lblTipoAtendimento = new System.Windows.Forms.TextBox();
            this.dataVencimento = new System.Windows.Forms.DateTimePicker();
            this.dataAtendimento = new System.Windows.Forms.DateTimePicker();
            this.grbFuncionario = new System.Windows.Forms.GroupBox();
            this.cb_TipoSangue = new SWS.ComboBoxWithBorder();
            this.textFuncaoFuncionario = new System.Windows.Forms.TextBox();
            this.lblFuncao = new System.Windows.Forms.TextBox();
            this.textPis = new System.Windows.Forms.MaskedTextBox();
            this.lblPis = new System.Windows.Forms.TextBox();
            this.cb_fatorRH = new SWS.ComboBoxWithBorder();
            this.lblMatricula = new System.Windows.Forms.TextBox();
            this.lblFatorRH = new System.Windows.Forms.TextBox();
            this.lblTipoSanguineo = new System.Windows.Forms.TextBox();
            this.lblCPF = new System.Windows.Forms.TextBox();
            this.lblDataNascimento = new System.Windows.Forms.TextBox();
            this.lblRg = new System.Windows.Forms.TextBox();
            this.lblNomeFuncionario = new System.Windows.Forms.TextBox();
            this.text_cpf = new System.Windows.Forms.MaskedTextBox();
            this.dt_nascimento = new System.Windows.Forms.DateTimePicker();
            this.text_matricula = new System.Windows.Forms.TextBox();
            this.text_rg = new System.Windows.Forms.TextBox();
            this.text_nome = new System.Windows.Forms.TextBox();
            this.grbCliente = new System.Windows.Forms.GroupBox();
            this.btnCentroCusto = new System.Windows.Forms.Button();
            this.textCentroCusto = new System.Windows.Forms.TextBox();
            this.lblCentroCusto = new System.Windows.Forms.TextBox();
            this.cb_uf = new SWS.ComboBoxWithBorder();
            this.text_cep = new System.Windows.Forms.MaskedTextBox();
            this.text_cnpj = new System.Windows.Forms.MaskedTextBox();
            this.text_cidade = new System.Windows.Forms.TextBox();
            this.text_bairro = new System.Windows.Forms.TextBox();
            this.text_complemento = new System.Windows.Forms.TextBox();
            this.text_numero = new System.Windows.Forms.TextBox();
            this.text_endereco = new System.Windows.Forms.TextBox();
            this.text_razaoSocial = new System.Windows.Forms.TextBox();
            this.lblCNPJ = new System.Windows.Forms.TextBox();
            this.lblEndereco = new System.Windows.Forms.TextBox();
            this.lblNumero = new System.Windows.Forms.TextBox();
            this.lblComplemento = new System.Windows.Forms.TextBox();
            this.lblBairro = new System.Windows.Forms.TextBox();
            this.lblCEP = new System.Windows.Forms.TextBox();
            this.lblCidade = new System.Windows.Forms.TextBox();
            this.lblUF = new System.Windows.Forms.TextBox();
            this.lblRazaoSocial = new System.Windows.Forms.TextBox();
            this.grb_exame = new System.Windows.Forms.GroupBox();
            this.dg_exame = new System.Windows.Forms.DataGridView();
            this.grbMedico = new System.Windows.Forms.GroupBox();
            this.btnExcluirExaminador = new System.Windows.Forms.Button();
            this.btnExaminador = new System.Windows.Forms.Button();
            this.text_celularExaminador = new System.Windows.Forms.TextBox();
            this.text_telefoneExaminador = new System.Windows.Forms.TextBox();
            this.text_crmExaminador = new System.Windows.Forms.TextBox();
            this.text_examinador = new System.Windows.Forms.TextBox();
            this.lblCRMExaminador = new System.Windows.Forms.TextBox();
            this.lblTelefoneExaminador = new System.Windows.Forms.TextBox();
            this.lblCelularExaminador = new System.Windows.Forms.TextBox();
            this.lblExaminador = new System.Windows.Forms.TextBox();
            this.text_celularCoordenador = new System.Windows.Forms.TextBox();
            this.text_telefoneCoordenador = new System.Windows.Forms.TextBox();
            this.text_crmCoordenador = new System.Windows.Forms.TextBox();
            this.text_coordenador = new System.Windows.Forms.TextBox();
            this.lblCoordenador = new System.Windows.Forms.TextBox();
            this.lblCRMCoordenador = new System.Windows.Forms.TextBox();
            this.lblTelefoneCoordenador = new System.Windows.Forms.TextBox();
            this.lblCelularCoordenador = new System.Windows.Forms.TextBox();
            this.tp1 = new System.Windows.Forms.ToolTip(this.components);
            this.grbRisco = new System.Windows.Forms.GroupBox();
            this.dgvRisco = new System.Windows.Forms.DataGridView();
            this.flpAcaoAgente = new System.Windows.Forms.FlowLayoutPanel();
            this.btnIncluirAgente = new System.Windows.Forms.Button();
            this.btnExcluirAgente = new System.Windows.Forms.Button();
            this.GheSetor = new System.Windows.Forms.GroupBox();
            this.dgvGheSetor = new System.Windows.Forms.DataGridView();
            this.flpAcaoGheSetor = new System.Windows.Forms.FlowLayoutPanel();
            this.btnIncluirGheSetor = new System.Windows.Forms.Button();
            this.btnAlterarGheSetor = new System.Windows.Forms.Button();
            this.btnExcluirGheSetor = new System.Windows.Forms.Button();
            this.grbObservacao = new System.Windows.Forms.GroupBox();
            this.textObservacao = new System.Windows.Forms.TextBox();
            this.grbCoordenador = new System.Windows.Forms.GroupBox();
            this.btnExcluirCoordenador = new System.Windows.Forms.Button();
            this.btnCoordenador = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.spForm)).BeginInit();
            this.spForm.Panel1.SuspendLayout();
            this.spForm.Panel2.SuspendLayout();
            this.spForm.SuspendLayout();
            this.pnlForm.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.banner)).BeginInit();
            this.flpAcao.SuspendLayout();
            this.grbAtendimento.SuspendLayout();
            this.grbFuncionario.SuspendLayout();
            this.grbCliente.SuspendLayout();
            this.grb_exame.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dg_exame)).BeginInit();
            this.grbMedico.SuspendLayout();
            this.grbRisco.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvRisco)).BeginInit();
            this.flpAcaoAgente.SuspendLayout();
            this.GheSetor.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvGheSetor)).BeginInit();
            this.flpAcaoGheSetor.SuspendLayout();
            this.grbObservacao.SuspendLayout();
            this.grbCoordenador.SuspendLayout();
            this.SuspendLayout();
            // 
            // spForm
            // 
            // 
            // spForm.Panel1
            // 
            this.spForm.Panel1.Controls.Add(this.flpAcao);
            this.spForm.SplitterDistance = 43;
            // 
            // pnlForm
            // 
            this.pnlForm.AutoScroll = true;
            this.pnlForm.Controls.Add(this.grbCoordenador);
            this.pnlForm.Controls.Add(this.grbObservacao);
            this.pnlForm.Controls.Add(this.GheSetor);
            this.pnlForm.Controls.Add(this.flpAcaoAgente);
            this.pnlForm.Controls.Add(this.flpAcaoGheSetor);
            this.pnlForm.Controls.Add(this.grbRisco);
            this.pnlForm.Controls.Add(this.grbMedico);
            this.pnlForm.Controls.Add(this.grb_exame);
            this.pnlForm.Controls.Add(this.grbCliente);
            this.pnlForm.Controls.Add(this.grbFuncionario);
            this.pnlForm.Controls.Add(this.grbAtendimento);
            this.pnlForm.Margin = new System.Windows.Forms.Padding(5, 5, 5, 5);
            this.pnlForm.Size = new System.Drawing.Size(1016, 2123);
            // 
            // flpAcao
            // 
            this.flpAcao.Controls.Add(this.btn_salvar);
            this.flpAcao.Controls.Add(this.btn_fechar);
            this.flpAcao.Dock = System.Windows.Forms.DockStyle.Fill;
            this.flpAcao.Location = new System.Drawing.Point(0, 0);
            this.flpAcao.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.flpAcao.Name = "flpAcao";
            this.flpAcao.Size = new System.Drawing.Size(1045, 43);
            this.flpAcao.TabIndex = 0;
            // 
            // btn_salvar
            // 
            this.btn_salvar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_salvar.Image = global::SWS.Properties.Resources.Gravar;
            this.btn_salvar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btn_salvar.Location = new System.Drawing.Point(4, 4);
            this.btn_salvar.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.btn_salvar.Name = "btn_salvar";
            this.btn_salvar.Size = new System.Drawing.Size(100, 28);
            this.btn_salvar.TabIndex = 30;
            this.btn_salvar.TabStop = false;
            this.btn_salvar.Text = "&Salvar";
            this.btn_salvar.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btn_salvar.UseVisualStyleBackColor = true;
            this.btn_salvar.Click += new System.EventHandler(this.btn_salvar_Click);
            // 
            // btn_fechar
            // 
            this.btn_fechar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_fechar.Image = global::SWS.Properties.Resources.close;
            this.btn_fechar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btn_fechar.Location = new System.Drawing.Point(112, 4);
            this.btn_fechar.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.btn_fechar.Name = "btn_fechar";
            this.btn_fechar.Size = new System.Drawing.Size(100, 28);
            this.btn_fechar.TabIndex = 31;
            this.btn_fechar.TabStop = false;
            this.btn_fechar.Text = "&Fechar";
            this.btn_fechar.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btn_fechar.UseVisualStyleBackColor = true;
            this.btn_fechar.Click += new System.EventHandler(this.btn_fechar_Click);
            // 
            // lblCodigoAtendimento
            // 
            this.lblCodigoAtendimento.BackColor = System.Drawing.Color.LightGray;
            this.lblCodigoAtendimento.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblCodigoAtendimento.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCodigoAtendimento.Location = new System.Drawing.Point(8, 23);
            this.lblCodigoAtendimento.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.lblCodigoAtendimento.MaxLength = 15;
            this.lblCodigoAtendimento.Name = "lblCodigoAtendimento";
            this.lblCodigoAtendimento.ReadOnly = true;
            this.lblCodigoAtendimento.Size = new System.Drawing.Size(187, 24);
            this.lblCodigoAtendimento.TabIndex = 1;
            this.lblCodigoAtendimento.TabStop = false;
            this.lblCodigoAtendimento.Text = "Código do Atendimento";
            // 
            // grbAtendimento
            // 
            this.grbAtendimento.BackColor = System.Drawing.Color.White;
            this.grbAtendimento.Controls.Add(this.cbEletricidade);
            this.grbAtendimento.Controls.Add(this.lblEletricidade);
            this.grbAtendimento.Controls.Add(this.cbConfinado);
            this.grbAtendimento.Controls.Add(this.cbAltura);
            this.grbAtendimento.Controls.Add(this.lblConfinado);
            this.grbAtendimento.Controls.Add(this.lblAltura);
            this.grbAtendimento.Controls.Add(this.textSenha);
            this.grbAtendimento.Controls.Add(this.lblSenha);
            this.grbAtendimento.Controls.Add(this.btnTipoAtendimento);
            this.grbAtendimento.Controls.Add(this.cbPrioridade);
            this.grbAtendimento.Controls.Add(this.lblPrioridade);
            this.grbAtendimento.Controls.Add(this.lblDataVencimento);
            this.grbAtendimento.Controls.Add(this.textCodigoPo);
            this.grbAtendimento.Controls.Add(this.lblCodigoPo);
            this.grbAtendimento.Controls.Add(this.text_periodicidade);
            this.grbAtendimento.Controls.Add(this.text_codigo);
            this.grbAtendimento.Controls.Add(this.lblDataAtendimento);
            this.grbAtendimento.Controls.Add(this.lblTipoAtendimento);
            this.grbAtendimento.Controls.Add(this.lblCodigoAtendimento);
            this.grbAtendimento.Controls.Add(this.dataVencimento);
            this.grbAtendimento.Controls.Add(this.dataAtendimento);
            this.grbAtendimento.Location = new System.Drawing.Point(16, 21);
            this.grbAtendimento.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.grbAtendimento.Name = "grbAtendimento";
            this.grbAtendimento.Padding = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.grbAtendimento.Size = new System.Drawing.Size(984, 288);
            this.grbAtendimento.TabIndex = 2;
            this.grbAtendimento.TabStop = false;
            this.grbAtendimento.Text = "Atendimento";
            // 
            // cbEletricidade
            // 
            this.cbEletricidade.BackColor = System.Drawing.Color.LightGray;
            this.cbEletricidade.BorderColor = System.Drawing.Color.DimGray;
            this.cbEletricidade.BorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid;
            this.cbEletricidade.FormattingEnabled = true;
            this.cbEletricidade.Location = new System.Drawing.Point(195, 245);
            this.cbEletricidade.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.cbEletricidade.Name = "cbEletricidade";
            this.cbEletricidade.Size = new System.Drawing.Size(776, 24);
            this.cbEletricidade.TabIndex = 24;
            this.cbEletricidade.TabStop = false;
            // 
            // lblEletricidade
            // 
            this.lblEletricidade.BackColor = System.Drawing.Color.LightGray;
            this.lblEletricidade.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblEletricidade.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblEletricidade.Location = new System.Drawing.Point(8, 245);
            this.lblEletricidade.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.lblEletricidade.MaxLength = 15;
            this.lblEletricidade.Name = "lblEletricidade";
            this.lblEletricidade.ReadOnly = true;
            this.lblEletricidade.Size = new System.Drawing.Size(187, 24);
            this.lblEletricidade.TabIndex = 23;
            this.lblEletricidade.TabStop = false;
            this.lblEletricidade.Text = "Serv. em Eletricidade";
            // 
            // cbConfinado
            // 
            this.cbConfinado.BackColor = System.Drawing.Color.LightGray;
            this.cbConfinado.BorderColor = System.Drawing.Color.DimGray;
            this.cbConfinado.BorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid;
            this.cbConfinado.FormattingEnabled = true;
            this.cbConfinado.Location = new System.Drawing.Point(195, 220);
            this.cbConfinado.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.cbConfinado.Name = "cbConfinado";
            this.cbConfinado.Size = new System.Drawing.Size(776, 24);
            this.cbConfinado.TabIndex = 20;
            this.cbConfinado.TabStop = false;
            // 
            // cbAltura
            // 
            this.cbAltura.BackColor = System.Drawing.Color.LightGray;
            this.cbAltura.BorderColor = System.Drawing.Color.DimGray;
            this.cbAltura.BorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid;
            this.cbAltura.FormattingEnabled = true;
            this.cbAltura.Location = new System.Drawing.Point(195, 196);
            this.cbAltura.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.cbAltura.Name = "cbAltura";
            this.cbAltura.Size = new System.Drawing.Size(776, 24);
            this.cbAltura.TabIndex = 19;
            this.cbAltura.TabStop = false;
            // 
            // lblConfinado
            // 
            this.lblConfinado.BackColor = System.Drawing.Color.LightGray;
            this.lblConfinado.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblConfinado.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblConfinado.Location = new System.Drawing.Point(8, 220);
            this.lblConfinado.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.lblConfinado.MaxLength = 15;
            this.lblConfinado.Name = "lblConfinado";
            this.lblConfinado.ReadOnly = true;
            this.lblConfinado.Size = new System.Drawing.Size(187, 24);
            this.lblConfinado.TabIndex = 18;
            this.lblConfinado.TabStop = false;
            this.lblConfinado.Text = "Confinado";
            // 
            // lblAltura
            // 
            this.lblAltura.BackColor = System.Drawing.Color.LightGray;
            this.lblAltura.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblAltura.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblAltura.Location = new System.Drawing.Point(8, 196);
            this.lblAltura.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.lblAltura.MaxLength = 15;
            this.lblAltura.Name = "lblAltura";
            this.lblAltura.ReadOnly = true;
            this.lblAltura.Size = new System.Drawing.Size(187, 24);
            this.lblAltura.TabIndex = 17;
            this.lblAltura.TabStop = false;
            this.lblAltura.Text = "Altura";
            // 
            // textSenha
            // 
            this.textSenha.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.textSenha.BackColor = System.Drawing.SystemColors.Window;
            this.textSenha.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textSenha.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.textSenha.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textSenha.Location = new System.Drawing.Point(195, 146);
            this.textSenha.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.textSenha.MaxLength = 6;
            this.textSenha.Name = "textSenha";
            this.textSenha.Size = new System.Drawing.Size(777, 24);
            this.textSenha.TabIndex = 5;
            this.textSenha.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.textSenha_KeyPress);
            // 
            // lblSenha
            // 
            this.lblSenha.BackColor = System.Drawing.Color.LightGray;
            this.lblSenha.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblSenha.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSenha.Location = new System.Drawing.Point(8, 146);
            this.lblSenha.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.lblSenha.MaxLength = 15;
            this.lblSenha.Name = "lblSenha";
            this.lblSenha.ReadOnly = true;
            this.lblSenha.Size = new System.Drawing.Size(187, 24);
            this.lblSenha.TabIndex = 16;
            this.lblSenha.TabStop = false;
            this.lblSenha.Text = "Senha Atendimento";
            // 
            // btnTipoAtendimento
            // 
            this.btnTipoAtendimento.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnTipoAtendimento.Image = global::SWS.Properties.Resources.busca;
            this.btnTipoAtendimento.Location = new System.Drawing.Point(927, 97);
            this.btnTipoAtendimento.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.btnTipoAtendimento.Name = "btnTipoAtendimento";
            this.btnTipoAtendimento.Size = new System.Drawing.Size(45, 26);
            this.btnTipoAtendimento.TabIndex = 3;
            this.btnTipoAtendimento.UseVisualStyleBackColor = true;
            this.btnTipoAtendimento.Click += new System.EventHandler(this.btnTipoAtendimento_Click);
            // 
            // cbPrioridade
            // 
            this.cbPrioridade.BackColor = System.Drawing.Color.LightGray;
            this.cbPrioridade.BorderColor = System.Drawing.Color.DimGray;
            this.cbPrioridade.BorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid;
            this.cbPrioridade.FormattingEnabled = true;
            this.cbPrioridade.Location = new System.Drawing.Point(195, 171);
            this.cbPrioridade.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.cbPrioridade.Name = "cbPrioridade";
            this.cbPrioridade.Size = new System.Drawing.Size(776, 24);
            this.cbPrioridade.TabIndex = 14;
            this.cbPrioridade.TabStop = false;
            // 
            // lblPrioridade
            // 
            this.lblPrioridade.BackColor = System.Drawing.Color.LightGray;
            this.lblPrioridade.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblPrioridade.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPrioridade.Location = new System.Drawing.Point(8, 171);
            this.lblPrioridade.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.lblPrioridade.MaxLength = 15;
            this.lblPrioridade.Name = "lblPrioridade";
            this.lblPrioridade.ReadOnly = true;
            this.lblPrioridade.Size = new System.Drawing.Size(187, 24);
            this.lblPrioridade.TabIndex = 13;
            this.lblPrioridade.TabStop = false;
            this.lblPrioridade.Text = "Prioridade";
            // 
            // lblDataVencimento
            // 
            this.lblDataVencimento.BackColor = System.Drawing.Color.LightGray;
            this.lblDataVencimento.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblDataVencimento.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDataVencimento.Location = new System.Drawing.Point(8, 73);
            this.lblDataVencimento.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.lblDataVencimento.MaxLength = 15;
            this.lblDataVencimento.Name = "lblDataVencimento";
            this.lblDataVencimento.ReadOnly = true;
            this.lblDataVencimento.Size = new System.Drawing.Size(187, 24);
            this.lblDataVencimento.TabIndex = 11;
            this.lblDataVencimento.TabStop = false;
            this.lblDataVencimento.Text = "Data de Vencimento";
            // 
            // textCodigoPo
            // 
            this.textCodigoPo.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.textCodigoPo.BackColor = System.Drawing.SystemColors.Window;
            this.textCodigoPo.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textCodigoPo.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.textCodigoPo.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textCodigoPo.Location = new System.Drawing.Point(195, 122);
            this.textCodigoPo.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.textCodigoPo.MaxLength = 15;
            this.textCodigoPo.Name = "textCodigoPo";
            this.textCodigoPo.Size = new System.Drawing.Size(777, 24);
            this.textCodigoPo.TabIndex = 4;
            // 
            // lblCodigoPo
            // 
            this.lblCodigoPo.BackColor = System.Drawing.Color.LightGray;
            this.lblCodigoPo.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblCodigoPo.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCodigoPo.Location = new System.Drawing.Point(8, 122);
            this.lblCodigoPo.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.lblCodigoPo.MaxLength = 15;
            this.lblCodigoPo.Name = "lblCodigoPo";
            this.lblCodigoPo.ReadOnly = true;
            this.lblCodigoPo.Size = new System.Drawing.Size(187, 24);
            this.lblCodigoPo.TabIndex = 8;
            this.lblCodigoPo.TabStop = false;
            this.lblCodigoPo.Text = "Código PO";
            // 
            // text_periodicidade
            // 
            this.text_periodicidade.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.text_periodicidade.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.text_periodicidade.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.text_periodicidade.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.text_periodicidade.Location = new System.Drawing.Point(195, 97);
            this.text_periodicidade.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.text_periodicidade.MaxLength = 100;
            this.text_periodicidade.Name = "text_periodicidade";
            this.text_periodicidade.ReadOnly = true;
            this.text_periodicidade.Size = new System.Drawing.Size(747, 24);
            this.text_periodicidade.TabIndex = 7;
            this.text_periodicidade.TabStop = false;
            // 
            // text_codigo
            // 
            this.text_codigo.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.text_codigo.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.text_codigo.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.text_codigo.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.text_codigo.Location = new System.Drawing.Point(195, 23);
            this.text_codigo.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.text_codigo.MaxLength = 12;
            this.text_codigo.Name = "text_codigo";
            this.text_codigo.ReadOnly = true;
            this.text_codigo.Size = new System.Drawing.Size(777, 24);
            this.text_codigo.TabIndex = 6;
            this.text_codigo.TabStop = false;
            // 
            // lblDataAtendimento
            // 
            this.lblDataAtendimento.BackColor = System.Drawing.Color.LightGray;
            this.lblDataAtendimento.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblDataAtendimento.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDataAtendimento.Location = new System.Drawing.Point(8, 48);
            this.lblDataAtendimento.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.lblDataAtendimento.MaxLength = 15;
            this.lblDataAtendimento.Name = "lblDataAtendimento";
            this.lblDataAtendimento.ReadOnly = true;
            this.lblDataAtendimento.Size = new System.Drawing.Size(187, 24);
            this.lblDataAtendimento.TabIndex = 3;
            this.lblDataAtendimento.TabStop = false;
            this.lblDataAtendimento.Text = "Data do Atendimento";
            // 
            // lblTipoAtendimento
            // 
            this.lblTipoAtendimento.BackColor = System.Drawing.Color.LightGray;
            this.lblTipoAtendimento.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblTipoAtendimento.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTipoAtendimento.Location = new System.Drawing.Point(8, 97);
            this.lblTipoAtendimento.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.lblTipoAtendimento.MaxLength = 15;
            this.lblTipoAtendimento.Name = "lblTipoAtendimento";
            this.lblTipoAtendimento.ReadOnly = true;
            this.lblTipoAtendimento.Size = new System.Drawing.Size(187, 24);
            this.lblTipoAtendimento.TabIndex = 2;
            this.lblTipoAtendimento.TabStop = false;
            this.lblTipoAtendimento.Text = "Tipo de Atendimento";
            // 
            // dataVencimento
            // 
            this.dataVencimento.CustomFormat = "dd/MM/yyyy HH:mm:ss";
            this.dataVencimento.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dataVencimento.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dataVencimento.Location = new System.Drawing.Point(193, 73);
            this.dataVencimento.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.dataVencimento.Name = "dataVencimento";
            this.dataVencimento.ShowCheckBox = true;
            this.dataVencimento.Size = new System.Drawing.Size(777, 24);
            this.dataVencimento.TabIndex = 2;
            this.dataVencimento.Value = new System.DateTime(2013, 10, 21, 17, 29, 18, 0);
            // 
            // dataAtendimento
            // 
            this.dataAtendimento.CustomFormat = "dd/MM/yyyy HH:mm:ss";
            this.dataAtendimento.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dataAtendimento.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dataAtendimento.Location = new System.Drawing.Point(195, 48);
            this.dataAtendimento.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.dataAtendimento.Name = "dataAtendimento";
            this.dataAtendimento.ShowCheckBox = true;
            this.dataAtendimento.Size = new System.Drawing.Size(776, 24);
            this.dataAtendimento.TabIndex = 1;
            this.dataAtendimento.Value = new System.DateTime(2013, 10, 21, 17, 29, 18, 0);
            // 
            // grbFuncionario
            // 
            this.grbFuncionario.BackColor = System.Drawing.Color.White;
            this.grbFuncionario.Controls.Add(this.cb_TipoSangue);
            this.grbFuncionario.Controls.Add(this.textFuncaoFuncionario);
            this.grbFuncionario.Controls.Add(this.lblFuncao);
            this.grbFuncionario.Controls.Add(this.textPis);
            this.grbFuncionario.Controls.Add(this.lblPis);
            this.grbFuncionario.Controls.Add(this.cb_fatorRH);
            this.grbFuncionario.Controls.Add(this.lblMatricula);
            this.grbFuncionario.Controls.Add(this.lblFatorRH);
            this.grbFuncionario.Controls.Add(this.lblTipoSanguineo);
            this.grbFuncionario.Controls.Add(this.lblCPF);
            this.grbFuncionario.Controls.Add(this.lblDataNascimento);
            this.grbFuncionario.Controls.Add(this.lblRg);
            this.grbFuncionario.Controls.Add(this.lblNomeFuncionario);
            this.grbFuncionario.Controls.Add(this.text_cpf);
            this.grbFuncionario.Controls.Add(this.dt_nascimento);
            this.grbFuncionario.Controls.Add(this.text_matricula);
            this.grbFuncionario.Controls.Add(this.text_rg);
            this.grbFuncionario.Controls.Add(this.text_nome);
            this.grbFuncionario.Location = new System.Drawing.Point(15, 316);
            this.grbFuncionario.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.grbFuncionario.Name = "grbFuncionario";
            this.grbFuncionario.Padding = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.grbFuncionario.Size = new System.Drawing.Size(984, 263);
            this.grbFuncionario.TabIndex = 3;
            this.grbFuncionario.TabStop = false;
            this.grbFuncionario.Text = " Funcionário";
            // 
            // cb_TipoSangue
            // 
            this.cb_TipoSangue.BackColor = System.Drawing.Color.LightGray;
            this.cb_TipoSangue.BorderColor = System.Drawing.Color.DimGray;
            this.cb_TipoSangue.BorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid;
            this.cb_TipoSangue.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cb_TipoSangue.FormattingEnabled = true;
            this.cb_TipoSangue.Location = new System.Drawing.Point(195, 122);
            this.cb_TipoSangue.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.cb_TipoSangue.Name = "cb_TipoSangue";
            this.cb_TipoSangue.Size = new System.Drawing.Size(775, 25);
            this.cb_TipoSangue.TabIndex = 4;
            // 
            // textFuncaoFuncionario
            // 
            this.textFuncaoFuncionario.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.textFuncaoFuncionario.BackColor = System.Drawing.SystemColors.Window;
            this.textFuncaoFuncionario.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textFuncaoFuncionario.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.textFuncaoFuncionario.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textFuncaoFuncionario.Location = new System.Drawing.Point(195, 220);
            this.textFuncaoFuncionario.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.textFuncaoFuncionario.MaxLength = 100;
            this.textFuncaoFuncionario.Name = "textFuncaoFuncionario";
            this.textFuncaoFuncionario.Size = new System.Drawing.Size(775, 24);
            this.textFuncaoFuncionario.TabIndex = 25;
            // 
            // lblFuncao
            // 
            this.lblFuncao.BackColor = System.Drawing.Color.LightGray;
            this.lblFuncao.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblFuncao.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblFuncao.Location = new System.Drawing.Point(8, 220);
            this.lblFuncao.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.lblFuncao.MaxLength = 15;
            this.lblFuncao.Name = "lblFuncao";
            this.lblFuncao.ReadOnly = true;
            this.lblFuncao.Size = new System.Drawing.Size(187, 24);
            this.lblFuncao.TabIndex = 25;
            this.lblFuncao.TabStop = false;
            this.lblFuncao.Text = "Função";
            // 
            // textPis
            // 
            this.textPis.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.textPis.BackColor = System.Drawing.SystemColors.Window;
            this.textPis.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textPis.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textPis.Location = new System.Drawing.Point(195, 196);
            this.textPis.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.textPis.Mask = "999,99999,99-9";
            this.textPis.Name = "textPis";
            this.textPis.PromptChar = ' ';
            this.textPis.Size = new System.Drawing.Size(775, 24);
            this.textPis.TabIndex = 24;
            // 
            // lblPis
            // 
            this.lblPis.BackColor = System.Drawing.Color.LightGray;
            this.lblPis.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblPis.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPis.Location = new System.Drawing.Point(8, 196);
            this.lblPis.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.lblPis.MaxLength = 15;
            this.lblPis.Name = "lblPis";
            this.lblPis.ReadOnly = true;
            this.lblPis.Size = new System.Drawing.Size(187, 24);
            this.lblPis.TabIndex = 23;
            this.lblPis.TabStop = false;
            this.lblPis.Text = "PIS/PASEP";
            // 
            // cb_fatorRH
            // 
            this.cb_fatorRH.BackColor = System.Drawing.Color.LightGray;
            this.cb_fatorRH.BorderColor = System.Drawing.Color.DimGray;
            this.cb_fatorRH.BorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid;
            this.cb_fatorRH.FormattingEnabled = true;
            this.cb_fatorRH.Location = new System.Drawing.Point(195, 146);
            this.cb_fatorRH.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.cb_fatorRH.Name = "cb_fatorRH";
            this.cb_fatorRH.Size = new System.Drawing.Size(775, 24);
            this.cb_fatorRH.TabIndex = 4;
            // 
            // lblMatricula
            // 
            this.lblMatricula.BackColor = System.Drawing.Color.LightGray;
            this.lblMatricula.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblMatricula.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblMatricula.Location = new System.Drawing.Point(8, 171);
            this.lblMatricula.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.lblMatricula.MaxLength = 15;
            this.lblMatricula.Name = "lblMatricula";
            this.lblMatricula.ReadOnly = true;
            this.lblMatricula.Size = new System.Drawing.Size(187, 24);
            this.lblMatricula.TabIndex = 22;
            this.lblMatricula.TabStop = false;
            this.lblMatricula.Text = "Matrícula";
            // 
            // lblFatorRH
            // 
            this.lblFatorRH.BackColor = System.Drawing.Color.LightGray;
            this.lblFatorRH.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblFatorRH.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblFatorRH.Location = new System.Drawing.Point(8, 146);
            this.lblFatorRH.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.lblFatorRH.MaxLength = 15;
            this.lblFatorRH.Name = "lblFatorRH";
            this.lblFatorRH.ReadOnly = true;
            this.lblFatorRH.Size = new System.Drawing.Size(187, 24);
            this.lblFatorRH.TabIndex = 21;
            this.lblFatorRH.TabStop = false;
            this.lblFatorRH.Text = "Fator RH";
            // 
            // lblTipoSanguineo
            // 
            this.lblTipoSanguineo.BackColor = System.Drawing.Color.LightGray;
            this.lblTipoSanguineo.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblTipoSanguineo.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTipoSanguineo.Location = new System.Drawing.Point(8, 122);
            this.lblTipoSanguineo.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.lblTipoSanguineo.MaxLength = 15;
            this.lblTipoSanguineo.Name = "lblTipoSanguineo";
            this.lblTipoSanguineo.ReadOnly = true;
            this.lblTipoSanguineo.Size = new System.Drawing.Size(187, 24);
            this.lblTipoSanguineo.TabIndex = 20;
            this.lblTipoSanguineo.TabStop = false;
            this.lblTipoSanguineo.Text = "Tipo Sanguíneo";
            // 
            // lblCPF
            // 
            this.lblCPF.BackColor = System.Drawing.Color.LightGray;
            this.lblCPF.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblCPF.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCPF.Location = new System.Drawing.Point(8, 97);
            this.lblCPF.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.lblCPF.MaxLength = 15;
            this.lblCPF.Name = "lblCPF";
            this.lblCPF.ReadOnly = true;
            this.lblCPF.Size = new System.Drawing.Size(187, 24);
            this.lblCPF.TabIndex = 19;
            this.lblCPF.TabStop = false;
            this.lblCPF.Text = "CPF";
            // 
            // lblDataNascimento
            // 
            this.lblDataNascimento.BackColor = System.Drawing.Color.LightGray;
            this.lblDataNascimento.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblDataNascimento.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDataNascimento.Location = new System.Drawing.Point(8, 73);
            this.lblDataNascimento.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.lblDataNascimento.MaxLength = 15;
            this.lblDataNascimento.Name = "lblDataNascimento";
            this.lblDataNascimento.ReadOnly = true;
            this.lblDataNascimento.Size = new System.Drawing.Size(187, 24);
            this.lblDataNascimento.TabIndex = 18;
            this.lblDataNascimento.TabStop = false;
            this.lblDataNascimento.Text = "Data de Nascimento";
            // 
            // lblRg
            // 
            this.lblRg.BackColor = System.Drawing.Color.LightGray;
            this.lblRg.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblRg.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblRg.Location = new System.Drawing.Point(8, 48);
            this.lblRg.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.lblRg.MaxLength = 15;
            this.lblRg.Name = "lblRg";
            this.lblRg.ReadOnly = true;
            this.lblRg.Size = new System.Drawing.Size(187, 24);
            this.lblRg.TabIndex = 17;
            this.lblRg.TabStop = false;
            this.lblRg.Text = "RG";
            // 
            // lblNomeFuncionario
            // 
            this.lblNomeFuncionario.BackColor = System.Drawing.Color.LightGray;
            this.lblNomeFuncionario.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblNomeFuncionario.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNomeFuncionario.Location = new System.Drawing.Point(8, 23);
            this.lblNomeFuncionario.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.lblNomeFuncionario.MaxLength = 15;
            this.lblNomeFuncionario.Name = "lblNomeFuncionario";
            this.lblNomeFuncionario.ReadOnly = true;
            this.lblNomeFuncionario.Size = new System.Drawing.Size(187, 24);
            this.lblNomeFuncionario.TabIndex = 16;
            this.lblNomeFuncionario.TabStop = false;
            this.lblNomeFuncionario.Text = "Nome do Funcionário";
            // 
            // text_cpf
            // 
            this.text_cpf.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.text_cpf.BackColor = System.Drawing.SystemColors.Window;
            this.text_cpf.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.text_cpf.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.text_cpf.Location = new System.Drawing.Point(193, 97);
            this.text_cpf.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.text_cpf.Mask = "000,000,000-00";
            this.text_cpf.Name = "text_cpf";
            this.text_cpf.PromptChar = ' ';
            this.text_cpf.Size = new System.Drawing.Size(777, 24);
            this.text_cpf.TabIndex = 12;
            // 
            // dt_nascimento
            // 
            this.dt_nascimento.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dt_nascimento.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dt_nascimento.Location = new System.Drawing.Point(193, 73);
            this.dt_nascimento.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.dt_nascimento.Name = "dt_nascimento";
            this.dt_nascimento.Size = new System.Drawing.Size(776, 24);
            this.dt_nascimento.TabIndex = 11;
            // 
            // text_matricula
            // 
            this.text_matricula.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.text_matricula.BackColor = System.Drawing.SystemColors.Window;
            this.text_matricula.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.text_matricula.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.text_matricula.Location = new System.Drawing.Point(195, 171);
            this.text_matricula.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.text_matricula.MaxLength = 30;
            this.text_matricula.Name = "text_matricula";
            this.text_matricula.Size = new System.Drawing.Size(775, 24);
            this.text_matricula.TabIndex = 15;
            // 
            // text_rg
            // 
            this.text_rg.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.text_rg.BackColor = System.Drawing.SystemColors.Window;
            this.text_rg.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.text_rg.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.text_rg.Location = new System.Drawing.Point(193, 48);
            this.text_rg.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.text_rg.MaxLength = 15;
            this.text_rg.Name = "text_rg";
            this.text_rg.Size = new System.Drawing.Size(777, 24);
            this.text_rg.TabIndex = 10;
            // 
            // text_nome
            // 
            this.text_nome.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.text_nome.BackColor = System.Drawing.SystemColors.Window;
            this.text_nome.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.text_nome.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.text_nome.Location = new System.Drawing.Point(193, 23);
            this.text_nome.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.text_nome.MaxLength = 100;
            this.text_nome.Name = "text_nome";
            this.text_nome.Size = new System.Drawing.Size(777, 24);
            this.text_nome.TabIndex = 9;
            // 
            // grbCliente
            // 
            this.grbCliente.Controls.Add(this.btnCentroCusto);
            this.grbCliente.Controls.Add(this.textCentroCusto);
            this.grbCliente.Controls.Add(this.lblCentroCusto);
            this.grbCliente.Controls.Add(this.cb_uf);
            this.grbCliente.Controls.Add(this.text_cep);
            this.grbCliente.Controls.Add(this.text_cnpj);
            this.grbCliente.Controls.Add(this.text_cidade);
            this.grbCliente.Controls.Add(this.text_bairro);
            this.grbCliente.Controls.Add(this.text_complemento);
            this.grbCliente.Controls.Add(this.text_numero);
            this.grbCliente.Controls.Add(this.text_endereco);
            this.grbCliente.Controls.Add(this.text_razaoSocial);
            this.grbCliente.Controls.Add(this.lblCNPJ);
            this.grbCliente.Controls.Add(this.lblEndereco);
            this.grbCliente.Controls.Add(this.lblNumero);
            this.grbCliente.Controls.Add(this.lblComplemento);
            this.grbCliente.Controls.Add(this.lblBairro);
            this.grbCliente.Controls.Add(this.lblCEP);
            this.grbCliente.Controls.Add(this.lblCidade);
            this.grbCliente.Controls.Add(this.lblUF);
            this.grbCliente.Controls.Add(this.lblRazaoSocial);
            this.grbCliente.Location = new System.Drawing.Point(16, 587);
            this.grbCliente.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.grbCliente.Name = "grbCliente";
            this.grbCliente.Padding = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.grbCliente.Size = new System.Drawing.Size(983, 286);
            this.grbCliente.TabIndex = 4;
            this.grbCliente.TabStop = false;
            this.grbCliente.Text = "Cliente";
            // 
            // btnCentroCusto
            // 
            this.btnCentroCusto.Enabled = false;
            this.btnCentroCusto.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnCentroCusto.Image = global::SWS.Properties.Resources.busca;
            this.btnCentroCusto.Location = new System.Drawing.Point(932, 244);
            this.btnCentroCusto.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.btnCentroCusto.Name = "btnCentroCusto";
            this.btnCentroCusto.Size = new System.Drawing.Size(40, 26);
            this.btnCentroCusto.TabIndex = 44;
            this.btnCentroCusto.UseVisualStyleBackColor = true;
            this.btnCentroCusto.Click += new System.EventHandler(this.btnCentroCusto_Click);
            // 
            // textCentroCusto
            // 
            this.textCentroCusto.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.textCentroCusto.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.textCentroCusto.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textCentroCusto.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textCentroCusto.Location = new System.Drawing.Point(195, 244);
            this.textCentroCusto.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.textCentroCusto.MaxLength = 50;
            this.textCentroCusto.Name = "textCentroCusto";
            this.textCentroCusto.ReadOnly = true;
            this.textCentroCusto.Size = new System.Drawing.Size(747, 24);
            this.textCentroCusto.TabIndex = 43;
            // 
            // lblCentroCusto
            // 
            this.lblCentroCusto.BackColor = System.Drawing.Color.LightGray;
            this.lblCentroCusto.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblCentroCusto.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCentroCusto.Location = new System.Drawing.Point(8, 244);
            this.lblCentroCusto.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.lblCentroCusto.MaxLength = 15;
            this.lblCentroCusto.Name = "lblCentroCusto";
            this.lblCentroCusto.ReadOnly = true;
            this.lblCentroCusto.Size = new System.Drawing.Size(187, 24);
            this.lblCentroCusto.TabIndex = 42;
            this.lblCentroCusto.TabStop = false;
            this.lblCentroCusto.Text = "Centro de Custo";
            // 
            // cb_uf
            // 
            this.cb_uf.BackColor = System.Drawing.Color.LightGray;
            this.cb_uf.BorderColor = System.Drawing.Color.DimGray;
            this.cb_uf.BorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid;
            this.cb_uf.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cb_uf.FormattingEnabled = true;
            this.cb_uf.Location = new System.Drawing.Point(195, 220);
            this.cb_uf.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.cb_uf.Name = "cb_uf";
            this.cb_uf.Size = new System.Drawing.Size(776, 21);
            this.cb_uf.TabIndex = 41;
            // 
            // text_cep
            // 
            this.text_cep.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.text_cep.BackColor = System.Drawing.SystemColors.Window;
            this.text_cep.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.text_cep.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.text_cep.Location = new System.Drawing.Point(195, 171);
            this.text_cep.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.text_cep.Mask = "00,000-000";
            this.text_cep.Name = "text_cep";
            this.text_cep.PromptChar = ' ';
            this.text_cep.Size = new System.Drawing.Size(777, 24);
            this.text_cep.TabIndex = 38;
            // 
            // text_cnpj
            // 
            this.text_cnpj.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.text_cnpj.BackColor = System.Drawing.SystemColors.Window;
            this.text_cnpj.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.text_cnpj.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.text_cnpj.Location = new System.Drawing.Point(195, 48);
            this.text_cnpj.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.text_cnpj.Mask = "00,000,000/0000-00";
            this.text_cnpj.Name = "text_cnpj";
            this.text_cnpj.PromptChar = ' ';
            this.text_cnpj.Size = new System.Drawing.Size(777, 24);
            this.text_cnpj.TabIndex = 33;
            // 
            // text_cidade
            // 
            this.text_cidade.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.text_cidade.BackColor = System.Drawing.SystemColors.Window;
            this.text_cidade.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.text_cidade.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.text_cidade.Location = new System.Drawing.Point(195, 196);
            this.text_cidade.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.text_cidade.MaxLength = 50;
            this.text_cidade.Name = "text_cidade";
            this.text_cidade.Size = new System.Drawing.Size(777, 24);
            this.text_cidade.TabIndex = 39;
            // 
            // text_bairro
            // 
            this.text_bairro.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.text_bairro.BackColor = System.Drawing.SystemColors.Window;
            this.text_bairro.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.text_bairro.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.text_bairro.Location = new System.Drawing.Point(195, 146);
            this.text_bairro.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.text_bairro.MaxLength = 50;
            this.text_bairro.Name = "text_bairro";
            this.text_bairro.Size = new System.Drawing.Size(777, 24);
            this.text_bairro.TabIndex = 37;
            // 
            // text_complemento
            // 
            this.text_complemento.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.text_complemento.BackColor = System.Drawing.SystemColors.Window;
            this.text_complemento.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.text_complemento.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.text_complemento.Location = new System.Drawing.Point(195, 122);
            this.text_complemento.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.text_complemento.MaxLength = 100;
            this.text_complemento.Name = "text_complemento";
            this.text_complemento.Size = new System.Drawing.Size(777, 24);
            this.text_complemento.TabIndex = 36;
            // 
            // text_numero
            // 
            this.text_numero.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.text_numero.BackColor = System.Drawing.SystemColors.Window;
            this.text_numero.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.text_numero.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.text_numero.Location = new System.Drawing.Point(195, 97);
            this.text_numero.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.text_numero.MaxLength = 10;
            this.text_numero.Name = "text_numero";
            this.text_numero.Size = new System.Drawing.Size(777, 24);
            this.text_numero.TabIndex = 35;
            // 
            // text_endereco
            // 
            this.text_endereco.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.text_endereco.BackColor = System.Drawing.SystemColors.Window;
            this.text_endereco.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.text_endereco.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.text_endereco.Location = new System.Drawing.Point(195, 73);
            this.text_endereco.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.text_endereco.MaxLength = 100;
            this.text_endereco.Name = "text_endereco";
            this.text_endereco.Size = new System.Drawing.Size(777, 24);
            this.text_endereco.TabIndex = 34;
            // 
            // text_razaoSocial
            // 
            this.text_razaoSocial.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.text_razaoSocial.BackColor = System.Drawing.SystemColors.Window;
            this.text_razaoSocial.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.text_razaoSocial.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.text_razaoSocial.Location = new System.Drawing.Point(195, 23);
            this.text_razaoSocial.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.text_razaoSocial.MaxLength = 100;
            this.text_razaoSocial.Name = "text_razaoSocial";
            this.text_razaoSocial.Size = new System.Drawing.Size(777, 24);
            this.text_razaoSocial.TabIndex = 32;
            // 
            // lblCNPJ
            // 
            this.lblCNPJ.BackColor = System.Drawing.Color.LightGray;
            this.lblCNPJ.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblCNPJ.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCNPJ.Location = new System.Drawing.Point(8, 48);
            this.lblCNPJ.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.lblCNPJ.MaxLength = 15;
            this.lblCNPJ.Name = "lblCNPJ";
            this.lblCNPJ.ReadOnly = true;
            this.lblCNPJ.Size = new System.Drawing.Size(187, 24);
            this.lblCNPJ.TabIndex = 31;
            this.lblCNPJ.TabStop = false;
            this.lblCNPJ.Text = "CNPJ";
            // 
            // lblEndereco
            // 
            this.lblEndereco.BackColor = System.Drawing.Color.LightGray;
            this.lblEndereco.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblEndereco.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblEndereco.Location = new System.Drawing.Point(8, 73);
            this.lblEndereco.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.lblEndereco.MaxLength = 15;
            this.lblEndereco.Name = "lblEndereco";
            this.lblEndereco.ReadOnly = true;
            this.lblEndereco.Size = new System.Drawing.Size(187, 24);
            this.lblEndereco.TabIndex = 30;
            this.lblEndereco.TabStop = false;
            this.lblEndereco.Text = "Endereço";
            // 
            // lblNumero
            // 
            this.lblNumero.BackColor = System.Drawing.Color.LightGray;
            this.lblNumero.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblNumero.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNumero.Location = new System.Drawing.Point(8, 97);
            this.lblNumero.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.lblNumero.MaxLength = 15;
            this.lblNumero.Name = "lblNumero";
            this.lblNumero.ReadOnly = true;
            this.lblNumero.Size = new System.Drawing.Size(187, 24);
            this.lblNumero.TabIndex = 29;
            this.lblNumero.TabStop = false;
            this.lblNumero.Text = "Número";
            // 
            // lblComplemento
            // 
            this.lblComplemento.BackColor = System.Drawing.Color.LightGray;
            this.lblComplemento.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblComplemento.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblComplemento.Location = new System.Drawing.Point(8, 122);
            this.lblComplemento.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.lblComplemento.MaxLength = 15;
            this.lblComplemento.Name = "lblComplemento";
            this.lblComplemento.ReadOnly = true;
            this.lblComplemento.Size = new System.Drawing.Size(187, 24);
            this.lblComplemento.TabIndex = 28;
            this.lblComplemento.TabStop = false;
            this.lblComplemento.Text = "Complemento";
            // 
            // lblBairro
            // 
            this.lblBairro.BackColor = System.Drawing.Color.LightGray;
            this.lblBairro.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblBairro.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblBairro.Location = new System.Drawing.Point(8, 146);
            this.lblBairro.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.lblBairro.MaxLength = 15;
            this.lblBairro.Name = "lblBairro";
            this.lblBairro.ReadOnly = true;
            this.lblBairro.Size = new System.Drawing.Size(187, 24);
            this.lblBairro.TabIndex = 27;
            this.lblBairro.TabStop = false;
            this.lblBairro.Text = "Bairro";
            // 
            // lblCEP
            // 
            this.lblCEP.BackColor = System.Drawing.Color.LightGray;
            this.lblCEP.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblCEP.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCEP.Location = new System.Drawing.Point(8, 171);
            this.lblCEP.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.lblCEP.MaxLength = 15;
            this.lblCEP.Name = "lblCEP";
            this.lblCEP.ReadOnly = true;
            this.lblCEP.Size = new System.Drawing.Size(187, 24);
            this.lblCEP.TabIndex = 26;
            this.lblCEP.TabStop = false;
            this.lblCEP.Text = "CEP";
            // 
            // lblCidade
            // 
            this.lblCidade.BackColor = System.Drawing.Color.LightGray;
            this.lblCidade.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblCidade.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCidade.Location = new System.Drawing.Point(8, 196);
            this.lblCidade.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.lblCidade.MaxLength = 15;
            this.lblCidade.Name = "lblCidade";
            this.lblCidade.ReadOnly = true;
            this.lblCidade.Size = new System.Drawing.Size(187, 24);
            this.lblCidade.TabIndex = 25;
            this.lblCidade.TabStop = false;
            this.lblCidade.Text = "Cidade";
            // 
            // lblUF
            // 
            this.lblUF.BackColor = System.Drawing.Color.LightGray;
            this.lblUF.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblUF.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblUF.Location = new System.Drawing.Point(8, 219);
            this.lblUF.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.lblUF.MaxLength = 15;
            this.lblUF.Name = "lblUF";
            this.lblUF.ReadOnly = true;
            this.lblUF.Size = new System.Drawing.Size(187, 24);
            this.lblUF.TabIndex = 24;
            this.lblUF.TabStop = false;
            this.lblUF.Text = "UF";
            // 
            // lblRazaoSocial
            // 
            this.lblRazaoSocial.BackColor = System.Drawing.Color.LightGray;
            this.lblRazaoSocial.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblRazaoSocial.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblRazaoSocial.Location = new System.Drawing.Point(8, 23);
            this.lblRazaoSocial.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.lblRazaoSocial.MaxLength = 15;
            this.lblRazaoSocial.Name = "lblRazaoSocial";
            this.lblRazaoSocial.ReadOnly = true;
            this.lblRazaoSocial.Size = new System.Drawing.Size(187, 24);
            this.lblRazaoSocial.TabIndex = 23;
            this.lblRazaoSocial.TabStop = false;
            this.lblRazaoSocial.Text = "Razão Social";
            // 
            // grb_exame
            // 
            this.grb_exame.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.grb_exame.Controls.Add(this.dg_exame);
            this.grb_exame.Location = new System.Drawing.Point(16, 1349);
            this.grb_exame.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.grb_exame.Name = "grb_exame";
            this.grb_exame.Padding = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.grb_exame.Size = new System.Drawing.Size(981, 263);
            this.grb_exame.TabIndex = 19;
            this.grb_exame.TabStop = false;
            this.grb_exame.Text = "Exames";
            // 
            // dg_exame
            // 
            this.dg_exame.AllowUserToAddRows = false;
            this.dg_exame.AllowUserToDeleteRows = false;
            this.dg_exame.AllowUserToOrderColumns = true;
            this.dg_exame.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this.dg_exame.BackgroundColor = System.Drawing.Color.White;
            this.dg_exame.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle6.BackColor = System.Drawing.Color.LightGray;
            dataGridViewCellStyle6.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle6.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle6.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle6.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle6.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dg_exame.DefaultCellStyle = dataGridViewCellStyle6;
            this.dg_exame.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dg_exame.Location = new System.Drawing.Point(4, 19);
            this.dg_exame.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.dg_exame.Name = "dg_exame";
            dataGridViewCellStyle7.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dg_exame.RowsDefaultCellStyle = dataGridViewCellStyle7;
            this.dg_exame.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.CellSelect;
            this.dg_exame.Size = new System.Drawing.Size(973, 240);
            this.dg_exame.TabIndex = 18;
            this.dg_exame.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dg_exame_CellContentClick);
            // 
            // grbMedico
            // 
            this.grbMedico.Controls.Add(this.btnExcluirExaminador);
            this.grbMedico.Controls.Add(this.btnExaminador);
            this.grbMedico.Controls.Add(this.text_celularExaminador);
            this.grbMedico.Controls.Add(this.text_telefoneExaminador);
            this.grbMedico.Controls.Add(this.text_crmExaminador);
            this.grbMedico.Controls.Add(this.text_examinador);
            this.grbMedico.Controls.Add(this.lblCRMExaminador);
            this.grbMedico.Controls.Add(this.lblTelefoneExaminador);
            this.grbMedico.Controls.Add(this.lblCelularExaminador);
            this.grbMedico.Controls.Add(this.lblExaminador);
            this.grbMedico.Location = new System.Drawing.Point(15, 1627);
            this.grbMedico.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.grbMedico.Name = "grbMedico";
            this.grbMedico.Padding = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.grbMedico.Size = new System.Drawing.Size(985, 134);
            this.grbMedico.TabIndex = 20;
            this.grbMedico.TabStop = false;
            this.grbMedico.Text = "Examinador";
            // 
            // btnExcluirExaminador
            // 
            this.btnExcluirExaminador.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnExcluirExaminador.Image = global::SWS.Properties.Resources.close;
            this.btnExcluirExaminador.Location = new System.Drawing.Point(935, 21);
            this.btnExcluirExaminador.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.btnExcluirExaminador.Name = "btnExcluirExaminador";
            this.btnExcluirExaminador.Size = new System.Drawing.Size(40, 26);
            this.btnExcluirExaminador.TabIndex = 48;
            this.btnExcluirExaminador.UseVisualStyleBackColor = true;
            this.btnExcluirExaminador.Click += new System.EventHandler(this.btnExcluirExaminador_Click);
            // 
            // btnExaminador
            // 
            this.btnExaminador.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnExaminador.Image = global::SWS.Properties.Resources.busca;
            this.btnExaminador.Location = new System.Drawing.Point(896, 21);
            this.btnExaminador.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.btnExaminador.Name = "btnExaminador";
            this.btnExaminador.Size = new System.Drawing.Size(40, 26);
            this.btnExaminador.TabIndex = 47;
            this.btnExaminador.UseVisualStyleBackColor = true;
            this.btnExaminador.Click += new System.EventHandler(this.btnExaminador_Click);
            // 
            // text_celularExaminador
            // 
            this.text_celularExaminador.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.text_celularExaminador.BackColor = System.Drawing.SystemColors.Window;
            this.text_celularExaminador.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.text_celularExaminador.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.text_celularExaminador.Location = new System.Drawing.Point(196, 95);
            this.text_celularExaminador.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.text_celularExaminador.MaxLength = 15;
            this.text_celularExaminador.Name = "text_celularExaminador";
            this.text_celularExaminador.Size = new System.Drawing.Size(778, 24);
            this.text_celularExaminador.TabIndex = 36;
            // 
            // text_telefoneExaminador
            // 
            this.text_telefoneExaminador.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.text_telefoneExaminador.BackColor = System.Drawing.SystemColors.Window;
            this.text_telefoneExaminador.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.text_telefoneExaminador.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.text_telefoneExaminador.Location = new System.Drawing.Point(196, 70);
            this.text_telefoneExaminador.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.text_telefoneExaminador.MaxLength = 15;
            this.text_telefoneExaminador.Name = "text_telefoneExaminador";
            this.text_telefoneExaminador.Size = new System.Drawing.Size(778, 24);
            this.text_telefoneExaminador.TabIndex = 35;
            // 
            // text_crmExaminador
            // 
            this.text_crmExaminador.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.text_crmExaminador.BackColor = System.Drawing.SystemColors.Window;
            this.text_crmExaminador.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.text_crmExaminador.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.text_crmExaminador.Location = new System.Drawing.Point(196, 46);
            this.text_crmExaminador.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.text_crmExaminador.MaxLength = 12;
            this.text_crmExaminador.Name = "text_crmExaminador";
            this.text_crmExaminador.Size = new System.Drawing.Size(778, 24);
            this.text_crmExaminador.TabIndex = 34;
            // 
            // text_examinador
            // 
            this.text_examinador.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.text_examinador.BackColor = System.Drawing.SystemColors.Window;
            this.text_examinador.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.text_examinador.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.text_examinador.Location = new System.Drawing.Point(196, 21);
            this.text_examinador.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.text_examinador.MaxLength = 100;
            this.text_examinador.Name = "text_examinador";
            this.text_examinador.Size = new System.Drawing.Size(701, 24);
            this.text_examinador.TabIndex = 33;
            // 
            // lblCRMExaminador
            // 
            this.lblCRMExaminador.BackColor = System.Drawing.Color.LightGray;
            this.lblCRMExaminador.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblCRMExaminador.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCRMExaminador.Location = new System.Drawing.Point(9, 46);
            this.lblCRMExaminador.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.lblCRMExaminador.MaxLength = 15;
            this.lblCRMExaminador.Name = "lblCRMExaminador";
            this.lblCRMExaminador.ReadOnly = true;
            this.lblCRMExaminador.Size = new System.Drawing.Size(187, 24);
            this.lblCRMExaminador.TabIndex = 32;
            this.lblCRMExaminador.TabStop = false;
            this.lblCRMExaminador.Text = "CRM";
            // 
            // lblTelefoneExaminador
            // 
            this.lblTelefoneExaminador.BackColor = System.Drawing.Color.LightGray;
            this.lblTelefoneExaminador.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblTelefoneExaminador.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTelefoneExaminador.Location = new System.Drawing.Point(9, 70);
            this.lblTelefoneExaminador.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.lblTelefoneExaminador.MaxLength = 15;
            this.lblTelefoneExaminador.Name = "lblTelefoneExaminador";
            this.lblTelefoneExaminador.ReadOnly = true;
            this.lblTelefoneExaminador.Size = new System.Drawing.Size(187, 24);
            this.lblTelefoneExaminador.TabIndex = 31;
            this.lblTelefoneExaminador.TabStop = false;
            this.lblTelefoneExaminador.Text = "Telefone";
            // 
            // lblCelularExaminador
            // 
            this.lblCelularExaminador.BackColor = System.Drawing.Color.LightGray;
            this.lblCelularExaminador.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblCelularExaminador.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCelularExaminador.Location = new System.Drawing.Point(9, 95);
            this.lblCelularExaminador.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.lblCelularExaminador.MaxLength = 15;
            this.lblCelularExaminador.Name = "lblCelularExaminador";
            this.lblCelularExaminador.ReadOnly = true;
            this.lblCelularExaminador.Size = new System.Drawing.Size(187, 24);
            this.lblCelularExaminador.TabIndex = 30;
            this.lblCelularExaminador.TabStop = false;
            this.lblCelularExaminador.Text = "Celular";
            // 
            // lblExaminador
            // 
            this.lblExaminador.BackColor = System.Drawing.Color.LightGray;
            this.lblExaminador.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblExaminador.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblExaminador.Location = new System.Drawing.Point(9, 21);
            this.lblExaminador.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.lblExaminador.MaxLength = 15;
            this.lblExaminador.Name = "lblExaminador";
            this.lblExaminador.ReadOnly = true;
            this.lblExaminador.Size = new System.Drawing.Size(187, 24);
            this.lblExaminador.TabIndex = 25;
            this.lblExaminador.TabStop = false;
            this.lblExaminador.Text = "Nome";
            // 
            // text_celularCoordenador
            // 
            this.text_celularCoordenador.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.text_celularCoordenador.BackColor = System.Drawing.SystemColors.Window;
            this.text_celularCoordenador.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.text_celularCoordenador.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.text_celularCoordenador.Location = new System.Drawing.Point(196, 96);
            this.text_celularCoordenador.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.text_celularCoordenador.MaxLength = 15;
            this.text_celularCoordenador.Name = "text_celularCoordenador";
            this.text_celularCoordenador.Size = new System.Drawing.Size(778, 24);
            this.text_celularCoordenador.TabIndex = 40;
            // 
            // text_telefoneCoordenador
            // 
            this.text_telefoneCoordenador.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.text_telefoneCoordenador.BackColor = System.Drawing.SystemColors.Window;
            this.text_telefoneCoordenador.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.text_telefoneCoordenador.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.text_telefoneCoordenador.Location = new System.Drawing.Point(196, 71);
            this.text_telefoneCoordenador.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.text_telefoneCoordenador.MaxLength = 15;
            this.text_telefoneCoordenador.Name = "text_telefoneCoordenador";
            this.text_telefoneCoordenador.Size = new System.Drawing.Size(778, 24);
            this.text_telefoneCoordenador.TabIndex = 39;
            // 
            // text_crmCoordenador
            // 
            this.text_crmCoordenador.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.text_crmCoordenador.BackColor = System.Drawing.SystemColors.Window;
            this.text_crmCoordenador.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.text_crmCoordenador.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.text_crmCoordenador.Location = new System.Drawing.Point(196, 47);
            this.text_crmCoordenador.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.text_crmCoordenador.MaxLength = 12;
            this.text_crmCoordenador.Name = "text_crmCoordenador";
            this.text_crmCoordenador.Size = new System.Drawing.Size(778, 24);
            this.text_crmCoordenador.TabIndex = 38;
            // 
            // text_coordenador
            // 
            this.text_coordenador.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.text_coordenador.BackColor = System.Drawing.SystemColors.Window;
            this.text_coordenador.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.text_coordenador.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.text_coordenador.Location = new System.Drawing.Point(195, 22);
            this.text_coordenador.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.text_coordenador.MaxLength = 100;
            this.text_coordenador.Name = "text_coordenador";
            this.text_coordenador.Size = new System.Drawing.Size(709, 24);
            this.text_coordenador.TabIndex = 37;
            // 
            // lblCoordenador
            // 
            this.lblCoordenador.BackColor = System.Drawing.Color.LightGray;
            this.lblCoordenador.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblCoordenador.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCoordenador.Location = new System.Drawing.Point(9, 22);
            this.lblCoordenador.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.lblCoordenador.MaxLength = 15;
            this.lblCoordenador.Name = "lblCoordenador";
            this.lblCoordenador.ReadOnly = true;
            this.lblCoordenador.Size = new System.Drawing.Size(187, 24);
            this.lblCoordenador.TabIndex = 29;
            this.lblCoordenador.TabStop = false;
            this.lblCoordenador.Text = "Nome";
            // 
            // lblCRMCoordenador
            // 
            this.lblCRMCoordenador.BackColor = System.Drawing.Color.LightGray;
            this.lblCRMCoordenador.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblCRMCoordenador.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCRMCoordenador.Location = new System.Drawing.Point(9, 47);
            this.lblCRMCoordenador.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.lblCRMCoordenador.MaxLength = 15;
            this.lblCRMCoordenador.Name = "lblCRMCoordenador";
            this.lblCRMCoordenador.ReadOnly = true;
            this.lblCRMCoordenador.Size = new System.Drawing.Size(187, 24);
            this.lblCRMCoordenador.TabIndex = 28;
            this.lblCRMCoordenador.TabStop = false;
            this.lblCRMCoordenador.Text = "CRM";
            // 
            // lblTelefoneCoordenador
            // 
            this.lblTelefoneCoordenador.BackColor = System.Drawing.Color.LightGray;
            this.lblTelefoneCoordenador.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblTelefoneCoordenador.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTelefoneCoordenador.Location = new System.Drawing.Point(9, 71);
            this.lblTelefoneCoordenador.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.lblTelefoneCoordenador.MaxLength = 15;
            this.lblTelefoneCoordenador.Name = "lblTelefoneCoordenador";
            this.lblTelefoneCoordenador.ReadOnly = true;
            this.lblTelefoneCoordenador.Size = new System.Drawing.Size(187, 24);
            this.lblTelefoneCoordenador.TabIndex = 27;
            this.lblTelefoneCoordenador.TabStop = false;
            this.lblTelefoneCoordenador.Text = "Telefone";
            // 
            // lblCelularCoordenador
            // 
            this.lblCelularCoordenador.BackColor = System.Drawing.Color.LightGray;
            this.lblCelularCoordenador.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblCelularCoordenador.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCelularCoordenador.Location = new System.Drawing.Point(9, 96);
            this.lblCelularCoordenador.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.lblCelularCoordenador.MaxLength = 15;
            this.lblCelularCoordenador.Name = "lblCelularCoordenador";
            this.lblCelularCoordenador.ReadOnly = true;
            this.lblCelularCoordenador.Size = new System.Drawing.Size(187, 24);
            this.lblCelularCoordenador.TabIndex = 26;
            this.lblCelularCoordenador.TabStop = false;
            this.lblCelularCoordenador.Text = "Celular";
            // 
            // grbRisco
            // 
            this.grbRisco.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.grbRisco.Controls.Add(this.dgvRisco);
            this.grbRisco.Location = new System.Drawing.Point(16, 1098);
            this.grbRisco.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.grbRisco.Name = "grbRisco";
            this.grbRisco.Padding = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.grbRisco.Size = new System.Drawing.Size(981, 192);
            this.grbRisco.TabIndex = 20;
            this.grbRisco.TabStop = false;
            this.grbRisco.Text = "Riscos";
            // 
            // dgvRisco
            // 
            this.dgvRisco.AllowUserToAddRows = false;
            this.dgvRisco.AllowUserToDeleteRows = false;
            this.dgvRisco.AllowUserToOrderColumns = true;
            this.dgvRisco.AllowUserToResizeRows = false;
            dataGridViewCellStyle3.BackColor = System.Drawing.Color.Gainsboro;
            dataGridViewCellStyle3.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dgvRisco.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle3;
            this.dgvRisco.BackgroundColor = System.Drawing.Color.White;
            this.dgvRisco.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle4.BackColor = System.Drawing.Color.LightGray;
            dataGridViewCellStyle4.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle4.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle4.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle4.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle4.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvRisco.DefaultCellStyle = dataGridViewCellStyle4;
            this.dgvRisco.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvRisco.Location = new System.Drawing.Point(4, 19);
            this.dgvRisco.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.dgvRisco.Name = "dgvRisco";
            dataGridViewCellStyle5.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dgvRisco.RowsDefaultCellStyle = dataGridViewCellStyle5;
            this.dgvRisco.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvRisco.Size = new System.Drawing.Size(973, 169);
            this.dgvRisco.TabIndex = 18;
            // 
            // flpAcaoAgente
            // 
            this.flpAcaoAgente.Controls.Add(this.btnIncluirAgente);
            this.flpAcaoAgente.Controls.Add(this.btnExcluirAgente);
            this.flpAcaoAgente.Location = new System.Drawing.Point(16, 1294);
            this.flpAcaoAgente.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.flpAcaoAgente.Name = "flpAcaoAgente";
            this.flpAcaoAgente.Size = new System.Drawing.Size(985, 39);
            this.flpAcaoAgente.TabIndex = 21;
            // 
            // btnIncluirAgente
            // 
            this.btnIncluirAgente.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnIncluirAgente.Image = global::SWS.Properties.Resources.icone_mais;
            this.btnIncluirAgente.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnIncluirAgente.Location = new System.Drawing.Point(4, 4);
            this.btnIncluirAgente.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.btnIncluirAgente.Name = "btnIncluirAgente";
            this.btnIncluirAgente.Size = new System.Drawing.Size(100, 28);
            this.btnIncluirAgente.TabIndex = 0;
            this.btnIncluirAgente.Text = "Incluir";
            this.btnIncluirAgente.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnIncluirAgente.UseVisualStyleBackColor = true;
            this.btnIncluirAgente.Click += new System.EventHandler(this.btnIncluirAgente_Click);
            // 
            // btnExcluirAgente
            // 
            this.btnExcluirAgente.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnExcluirAgente.Image = global::SWS.Properties.Resources.lixeira;
            this.btnExcluirAgente.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnExcluirAgente.Location = new System.Drawing.Point(112, 4);
            this.btnExcluirAgente.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.btnExcluirAgente.Name = "btnExcluirAgente";
            this.btnExcluirAgente.Size = new System.Drawing.Size(100, 28);
            this.btnExcluirAgente.TabIndex = 1;
            this.btnExcluirAgente.Text = "Excluir";
            this.btnExcluirAgente.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnExcluirAgente.UseVisualStyleBackColor = true;
            this.btnExcluirAgente.Click += new System.EventHandler(this.btnExcluirAgente_Click);
            // 
            // GheSetor
            // 
            this.GheSetor.Controls.Add(this.dgvGheSetor);
            this.GheSetor.Location = new System.Drawing.Point(16, 880);
            this.GheSetor.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.GheSetor.Name = "GheSetor";
            this.GheSetor.Padding = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.GheSetor.Size = new System.Drawing.Size(984, 160);
            this.GheSetor.TabIndex = 22;
            this.GheSetor.TabStop = false;
            this.GheSetor.Text = "Ghe(s) e Setor(es)";
            // 
            // dgvGheSetor
            // 
            this.dgvGheSetor.AllowUserToAddRows = false;
            this.dgvGheSetor.AllowUserToDeleteRows = false;
            this.dgvGheSetor.AllowUserToOrderColumns = true;
            this.dgvGheSetor.AllowUserToResizeRows = false;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.Gainsboro;
            this.dgvGheSetor.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle1;
            this.dgvGheSetor.BackgroundColor = System.Drawing.Color.White;
            this.dgvGheSetor.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.Color.LightGray;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvGheSetor.DefaultCellStyle = dataGridViewCellStyle2;
            this.dgvGheSetor.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvGheSetor.Location = new System.Drawing.Point(4, 19);
            this.dgvGheSetor.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.dgvGheSetor.MultiSelect = false;
            this.dgvGheSetor.Name = "dgvGheSetor";
            this.dgvGheSetor.ReadOnly = true;
            this.dgvGheSetor.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvGheSetor.Size = new System.Drawing.Size(976, 137);
            this.dgvGheSetor.TabIndex = 0;
            // 
            // flpAcaoGheSetor
            // 
            this.flpAcaoGheSetor.Controls.Add(this.btnIncluirGheSetor);
            this.flpAcaoGheSetor.Controls.Add(this.btnAlterarGheSetor);
            this.flpAcaoGheSetor.Controls.Add(this.btnExcluirGheSetor);
            this.flpAcaoGheSetor.Location = new System.Drawing.Point(16, 1044);
            this.flpAcaoGheSetor.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.flpAcaoGheSetor.Name = "flpAcaoGheSetor";
            this.flpAcaoGheSetor.Size = new System.Drawing.Size(981, 39);
            this.flpAcaoGheSetor.TabIndex = 22;
            // 
            // btnIncluirGheSetor
            // 
            this.btnIncluirGheSetor.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnIncluirGheSetor.Image = global::SWS.Properties.Resources.icone_mais;
            this.btnIncluirGheSetor.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnIncluirGheSetor.Location = new System.Drawing.Point(4, 4);
            this.btnIncluirGheSetor.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.btnIncluirGheSetor.Name = "btnIncluirGheSetor";
            this.btnIncluirGheSetor.Size = new System.Drawing.Size(100, 28);
            this.btnIncluirGheSetor.TabIndex = 0;
            this.btnIncluirGheSetor.Text = "Incluir";
            this.btnIncluirGheSetor.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnIncluirGheSetor.UseVisualStyleBackColor = true;
            this.btnIncluirGheSetor.Click += new System.EventHandler(this.btnIncluirGheSetor_Click);
            // 
            // btnAlterarGheSetor
            // 
            this.btnAlterarGheSetor.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnAlterarGheSetor.Image = global::SWS.Properties.Resources.Alterar;
            this.btnAlterarGheSetor.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnAlterarGheSetor.Location = new System.Drawing.Point(112, 4);
            this.btnAlterarGheSetor.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.btnAlterarGheSetor.Name = "btnAlterarGheSetor";
            this.btnAlterarGheSetor.Size = new System.Drawing.Size(100, 28);
            this.btnAlterarGheSetor.TabIndex = 2;
            this.btnAlterarGheSetor.Text = "Alterar";
            this.btnAlterarGheSetor.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnAlterarGheSetor.UseVisualStyleBackColor = true;
            this.btnAlterarGheSetor.Click += new System.EventHandler(this.btnAlterarGheSetor_Click);
            // 
            // btnExcluirGheSetor
            // 
            this.btnExcluirGheSetor.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnExcluirGheSetor.Image = global::SWS.Properties.Resources.lixeira;
            this.btnExcluirGheSetor.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnExcluirGheSetor.Location = new System.Drawing.Point(220, 4);
            this.btnExcluirGheSetor.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.btnExcluirGheSetor.Name = "btnExcluirGheSetor";
            this.btnExcluirGheSetor.Size = new System.Drawing.Size(100, 28);
            this.btnExcluirGheSetor.TabIndex = 1;
            this.btnExcluirGheSetor.Text = "Excluir";
            this.btnExcluirGheSetor.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnExcluirGheSetor.UseVisualStyleBackColor = true;
            this.btnExcluirGheSetor.Click += new System.EventHandler(this.btnExcluirGheSetor_Click);
            // 
            // grbObservacao
            // 
            this.grbObservacao.Controls.Add(this.textObservacao);
            this.grbObservacao.Location = new System.Drawing.Point(16, 1913);
            this.grbObservacao.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.grbObservacao.Name = "grbObservacao";
            this.grbObservacao.Padding = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.grbObservacao.Size = new System.Drawing.Size(987, 123);
            this.grbObservacao.TabIndex = 23;
            this.grbObservacao.TabStop = false;
            this.grbObservacao.Text = "Observação no atendimento";
            // 
            // textObservacao
            // 
            this.textObservacao.BackColor = System.Drawing.SystemColors.Window;
            this.textObservacao.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textObservacao.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.textObservacao.Dock = System.Windows.Forms.DockStyle.Fill;
            this.textObservacao.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textObservacao.Location = new System.Drawing.Point(4, 19);
            this.textObservacao.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.textObservacao.Multiline = true;
            this.textObservacao.Name = "textObservacao";
            this.textObservacao.Size = new System.Drawing.Size(979, 100);
            this.textObservacao.TabIndex = 34;
            // 
            // grbCoordenador
            // 
            this.grbCoordenador.Controls.Add(this.btnExcluirCoordenador);
            this.grbCoordenador.Controls.Add(this.btnCoordenador);
            this.grbCoordenador.Controls.Add(this.text_celularCoordenador);
            this.grbCoordenador.Controls.Add(this.lblCoordenador);
            this.grbCoordenador.Controls.Add(this.text_telefoneCoordenador);
            this.grbCoordenador.Controls.Add(this.text_coordenador);
            this.grbCoordenador.Controls.Add(this.text_crmCoordenador);
            this.grbCoordenador.Controls.Add(this.lblCRMCoordenador);
            this.grbCoordenador.Controls.Add(this.lblTelefoneCoordenador);
            this.grbCoordenador.Controls.Add(this.lblCelularCoordenador);
            this.grbCoordenador.Location = new System.Drawing.Point(16, 1769);
            this.grbCoordenador.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.grbCoordenador.Name = "grbCoordenador";
            this.grbCoordenador.Padding = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.grbCoordenador.Size = new System.Drawing.Size(984, 134);
            this.grbCoordenador.TabIndex = 24;
            this.grbCoordenador.TabStop = false;
            this.grbCoordenador.Text = "Coordenador";
            // 
            // btnExcluirCoordenador
            // 
            this.btnExcluirCoordenador.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnExcluirCoordenador.Image = global::SWS.Properties.Resources.close;
            this.btnExcluirCoordenador.Location = new System.Drawing.Point(935, 22);
            this.btnExcluirCoordenador.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.btnExcluirCoordenador.Name = "btnExcluirCoordenador";
            this.btnExcluirCoordenador.Size = new System.Drawing.Size(40, 26);
            this.btnExcluirCoordenador.TabIndex = 46;
            this.btnExcluirCoordenador.UseVisualStyleBackColor = true;
            this.btnExcluirCoordenador.Click += new System.EventHandler(this.btnExcluirCoordenador_Click);
            // 
            // btnCoordenador
            // 
            this.btnCoordenador.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnCoordenador.Image = global::SWS.Properties.Resources.busca;
            this.btnCoordenador.Location = new System.Drawing.Point(896, 22);
            this.btnCoordenador.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.btnCoordenador.Name = "btnCoordenador";
            this.btnCoordenador.Size = new System.Drawing.Size(40, 26);
            this.btnCoordenador.TabIndex = 45;
            this.btnCoordenador.UseVisualStyleBackColor = true;
            this.btnCoordenador.Click += new System.EventHandler(this.btnCoordenador_Click);
            // 
            // frmAtendimentoAlterar
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1045, 690);
            this.Margin = new System.Windows.Forms.Padding(5, 5, 5, 5);
            this.Name = "frmAtendimentoAlterar";
            this.Text = "ALTERAR INFORMAÇÕES DO ATENDIMENTO";
            this.spForm.Panel1.ResumeLayout(false);
            this.spForm.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.spForm)).EndInit();
            this.spForm.ResumeLayout(false);
            this.pnlForm.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.banner)).EndInit();
            this.flpAcao.ResumeLayout(false);
            this.grbAtendimento.ResumeLayout(false);
            this.grbAtendimento.PerformLayout();
            this.grbFuncionario.ResumeLayout(false);
            this.grbFuncionario.PerformLayout();
            this.grbCliente.ResumeLayout(false);
            this.grbCliente.PerformLayout();
            this.grb_exame.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dg_exame)).EndInit();
            this.grbMedico.ResumeLayout(false);
            this.grbMedico.PerformLayout();
            this.grbRisco.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvRisco)).EndInit();
            this.flpAcaoAgente.ResumeLayout(false);
            this.GheSetor.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvGheSetor)).EndInit();
            this.flpAcaoGheSetor.ResumeLayout(false);
            this.grbObservacao.ResumeLayout(false);
            this.grbObservacao.PerformLayout();
            this.grbCoordenador.ResumeLayout(false);
            this.grbCoordenador.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.FlowLayoutPanel flpAcao;
        private System.Windows.Forms.Button btn_fechar;
        private System.Windows.Forms.Button btn_salvar;
        private System.Windows.Forms.GroupBox grbAtendimento;
        private System.Windows.Forms.TextBox lblCodigoAtendimento;
        private System.Windows.Forms.TextBox lblDataAtendimento;
        private System.Windows.Forms.TextBox lblTipoAtendimento;
        private System.Windows.Forms.DateTimePicker dataAtendimento;
        private System.Windows.Forms.TextBox text_periodicidade;
        private System.Windows.Forms.TextBox text_codigo;
        private System.Windows.Forms.GroupBox grbFuncionario;
        private System.Windows.Forms.TextBox lblNomeFuncionario;
        private System.Windows.Forms.MaskedTextBox text_cpf;
        private System.Windows.Forms.DateTimePicker dt_nascimento;
        private System.Windows.Forms.TextBox text_matricula;
        private System.Windows.Forms.TextBox text_rg;
        private System.Windows.Forms.TextBox text_nome;
        private System.Windows.Forms.TextBox lblRg;
        private System.Windows.Forms.TextBox lblDataNascimento;
        private System.Windows.Forms.TextBox lblCPF;
        private System.Windows.Forms.TextBox lblMatricula;
        private System.Windows.Forms.TextBox lblFatorRH;
        private System.Windows.Forms.TextBox lblTipoSanguineo;
        private ComboBoxWithBorder cb_TipoSangue;
        private ComboBoxWithBorder cb_fatorRH;
        private System.Windows.Forms.GroupBox grbCliente;
        private System.Windows.Forms.TextBox lblRazaoSocial;
        private System.Windows.Forms.TextBox lblCNPJ;
        private System.Windows.Forms.TextBox lblEndereco;
        private System.Windows.Forms.TextBox lblNumero;
        private System.Windows.Forms.TextBox lblComplemento;
        private System.Windows.Forms.TextBox lblBairro;
        private System.Windows.Forms.TextBox lblCEP;
        private System.Windows.Forms.TextBox lblCidade;
        private System.Windows.Forms.TextBox lblUF;
        private System.Windows.Forms.MaskedTextBox text_cep;
        private System.Windows.Forms.MaskedTextBox text_cnpj;
        private System.Windows.Forms.TextBox text_cidade;
        private System.Windows.Forms.TextBox text_bairro;
        private System.Windows.Forms.TextBox text_complemento;
        private System.Windows.Forms.TextBox text_numero;
        private System.Windows.Forms.TextBox text_endereco;
        private System.Windows.Forms.TextBox text_razaoSocial;
        private ComboBoxWithBorder cb_uf;
        private System.Windows.Forms.GroupBox grb_exame;
        private System.Windows.Forms.DataGridView dg_exame;
        private System.Windows.Forms.GroupBox grbMedico;
        private System.Windows.Forms.TextBox lblExaminador;
        private System.Windows.Forms.TextBox lblCRMExaminador;
        private System.Windows.Forms.TextBox lblTelefoneExaminador;
        private System.Windows.Forms.TextBox lblCelularExaminador;
        private System.Windows.Forms.TextBox lblCoordenador;
        private System.Windows.Forms.TextBox lblCRMCoordenador;
        private System.Windows.Forms.TextBox lblTelefoneCoordenador;
        private System.Windows.Forms.TextBox lblCelularCoordenador;
        private System.Windows.Forms.TextBox text_celularCoordenador;
        private System.Windows.Forms.TextBox text_telefoneCoordenador;
        private System.Windows.Forms.TextBox text_crmCoordenador;
        private System.Windows.Forms.TextBox text_coordenador;
        private System.Windows.Forms.TextBox text_celularExaminador;
        private System.Windows.Forms.TextBox text_telefoneExaminador;
        private System.Windows.Forms.TextBox text_crmExaminador;
        private System.Windows.Forms.TextBox text_examinador;
        private System.Windows.Forms.ToolTip tp1;
        private System.Windows.Forms.Button btnCentroCusto;
        private System.Windows.Forms.TextBox textCentroCusto;
        private System.Windows.Forms.TextBox lblCentroCusto;
        private System.Windows.Forms.TextBox lblDataVencimento;
        private System.Windows.Forms.TextBox textCodigoPo;
        private System.Windows.Forms.TextBox lblCodigoPo;
        private System.Windows.Forms.DateTimePicker dataVencimento;
        private ComboBoxWithBorder cbPrioridade;
        private System.Windows.Forms.TextBox lblPrioridade;
        private System.Windows.Forms.GroupBox grbRisco;
        private System.Windows.Forms.DataGridView dgvRisco;
        private System.Windows.Forms.Button btnTipoAtendimento;
        private System.Windows.Forms.MaskedTextBox textPis;
        private System.Windows.Forms.TextBox lblPis;
        private System.Windows.Forms.TextBox textSenha;
        private System.Windows.Forms.TextBox lblSenha;
        private System.Windows.Forms.FlowLayoutPanel flpAcaoAgente;
        private System.Windows.Forms.Button btnIncluirAgente;
        private System.Windows.Forms.Button btnExcluirAgente;
        private System.Windows.Forms.GroupBox GheSetor;
        private System.Windows.Forms.FlowLayoutPanel flpAcaoGheSetor;
        private System.Windows.Forms.Button btnIncluirGheSetor;
        private System.Windows.Forms.Button btnExcluirGheSetor;
        private System.Windows.Forms.DataGridView dgvGheSetor;
        private System.Windows.Forms.Button btnAlterarGheSetor;
        private System.Windows.Forms.TextBox textFuncaoFuncionario;
        private System.Windows.Forms.TextBox lblFuncao;
        private System.Windows.Forms.GroupBox grbObservacao;
        private System.Windows.Forms.TextBox textObservacao;
        private System.Windows.Forms.GroupBox grbCoordenador;
        private System.Windows.Forms.Button btnExcluirCoordenador;
        private System.Windows.Forms.Button btnCoordenador;
        private System.Windows.Forms.Button btnExcluirExaminador;
        private System.Windows.Forms.Button btnExaminador;
        private ComboBoxWithBorder cbConfinado;
        private ComboBoxWithBorder cbAltura;
        private System.Windows.Forms.TextBox lblConfinado;
        private System.Windows.Forms.TextBox lblAltura;
        private ComboBoxWithBorder cbEletricidade;
        private System.Windows.Forms.TextBox lblEletricidade;
    }
}