﻿namespace SWS.View
{
    partial class frmClientesIncluir
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmClientesIncluir));
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            this.flpAcao = new System.Windows.Forms.FlowLayoutPanel();
            this.btGravar = new System.Windows.Forms.Button();
            this.btImprimir = new System.Windows.Forms.Button();
            this.btLimpar = new System.Windows.Forms.Button();
            this.btFechar = new System.Windows.Forms.Button();
            this.btRamo = new System.Windows.Forms.Button();
            this.lblJornada = new System.Windows.Forms.TextBox();
            this.lblRamo = new System.Windows.Forms.TextBox();
            this.lblEscopo = new System.Windows.Forms.TextBox();
            this.textRamo = new System.Windows.Forms.TextBox();
            this.textJornada = new System.Windows.Forms.TextBox();
            this.textEscopo = new System.Windows.Forms.TextBox();
            this.textSite = new System.Windows.Forms.TextBox();
            this.lblSite = new System.Windows.Forms.TextBox();
            this.lblEmail = new System.Windows.Forms.TextBox();
            this.tpEndereco = new System.Windows.Forms.TabControl();
            this.tpComercial = new System.Windows.Forms.TabPage();
            this.textFaxComercial = new System.Windows.Forms.TextBox();
            this.lblFaxComercial = new System.Windows.Forms.TextBox();
            this.lblTelefoneComercial = new System.Windows.Forms.TextBox();
            this.textTelefoneComercial = new System.Windows.Forms.TextBox();
            this.lblCidade = new System.Windows.Forms.TextBox();
            this.btCidadeComercial = new System.Windows.Forms.Button();
            this.textCidadeComercial = new System.Windows.Forms.TextBox();
            this.textCEPComercial = new System.Windows.Forms.MaskedTextBox();
            this.lblUF = new System.Windows.Forms.TextBox();
            this.lblCEP = new System.Windows.Forms.TextBox();
            this.lblBairroComercial = new System.Windows.Forms.TextBox();
            this.lblComplementoComercial = new System.Windows.Forms.TextBox();
            this.lblNumeroComercial = new System.Windows.Forms.TextBox();
            this.lblEnderecoComercial = new System.Windows.Forms.TextBox();
            this.textLogradouroComercial = new System.Windows.Forms.TextBox();
            this.textNumeroComercial = new System.Windows.Forms.TextBox();
            this.textComplementoComercial = new System.Windows.Forms.TextBox();
            this.textBairroComercial = new System.Windows.Forms.TextBox();
            this.cbUFComercial = new SWS.ComboBoxWithBorder();
            this.tpCobranca = new System.Windows.Forms.TabPage();
            this.btCopiarComercial = new System.Windows.Forms.Button();
            this.btCidadeCobranca = new System.Windows.Forms.Button();
            this.textFaxCobranca = new System.Windows.Forms.TextBox();
            this.lblFaxCobranca = new System.Windows.Forms.TextBox();
            this.lblTelefoneCobranca = new System.Windows.Forms.TextBox();
            this.textTelefoneCobranca = new System.Windows.Forms.TextBox();
            this.lblCidadeCobranca = new System.Windows.Forms.TextBox();
            this.textCidadeCobranca = new System.Windows.Forms.TextBox();
            this.textCEPCobranca = new System.Windows.Forms.MaskedTextBox();
            this.lblUFCobranca = new System.Windows.Forms.TextBox();
            this.lblCEPCobranca = new System.Windows.Forms.TextBox();
            this.lblBairroCobranca = new System.Windows.Forms.TextBox();
            this.lblComplementoCobranca = new System.Windows.Forms.TextBox();
            this.lblNumeroCobranca = new System.Windows.Forms.TextBox();
            this.lblEnderecoCobranca = new System.Windows.Forms.TextBox();
            this.textLogradouroCobranca = new System.Windows.Forms.TextBox();
            this.textNumeroCobranca = new System.Windows.Forms.TextBox();
            this.textComplementoCobranca = new System.Windows.Forms.TextBox();
            this.textBairroCobranca = new System.Windows.Forms.TextBox();
            this.cbUFCobranca = new SWS.ComboBoxWithBorder();
            this.textEmail = new System.Windows.Forms.TextBox();
            this.textInscricao = new System.Windows.Forms.TextBox();
            this.lblInscricao = new System.Windows.Forms.TextBox();
            this.lblNomeFantasia = new System.Windows.Forms.TextBox();
            this.textFantasia = new System.Windows.Forms.TextBox();
            this.lblCNPJ = new System.Windows.Forms.TextBox();
            this.textRazaoSocial = new System.Windows.Forms.TextBox();
            this.btCredenciada = new System.Windows.Forms.Button();
            this.textCNPJ = new System.Windows.Forms.MaskedTextBox();
            this.textCredenciada = new System.Windows.Forms.TextBox();
            this.btMatriz = new System.Windows.Forms.Button();
            this.textMatriz = new System.Windows.Forms.TextBox();
            this.btVendedor = new System.Windows.Forms.Button();
            this.textVendedor = new System.Windows.Forms.TextBox();
            this.lblMatriz = new System.Windows.Forms.TextBox();
            this.lblCredenciada = new System.Windows.Forms.TextBox();
            this.lblVendedor = new System.Windows.Forms.TextBox();
            this.lblRazaoSocial = new System.Windows.Forms.TextBox();
            this.lbltipoCliente = new System.Windows.Forms.TextBox();
            this.lblCnaePrincipal = new System.Windows.Forms.Label();
            this.grbCnae = new System.Windows.Forms.GroupBox();
            this.dgvCnae = new System.Windows.Forms.DataGridView();
            this.btExcluirCnae = new System.Windows.Forms.Button();
            this.btIncluirCnae = new System.Windows.Forms.Button();
            this.btExcluirLogo = new System.Windows.Forms.Button();
            this.grbLogo = new System.Windows.Forms.GroupBox();
            this.pbLogo = new System.Windows.Forms.PictureBox();
            this.btIncluirLogo = new System.Windows.Forms.Button();
            this.lblContato = new System.Windows.Forms.Label();
            this.btExcluirContato = new System.Windows.Forms.Button();
            this.btIncluirContato = new System.Windows.Forms.Button();
            this.grbContato = new System.Windows.Forms.GroupBox();
            this.dgvContatos = new System.Windows.Forms.DataGridView();
            this.btExcluirFuncao = new System.Windows.Forms.Button();
            this.grbFuncao = new System.Windows.Forms.GroupBox();
            this.dgvFuncao = new System.Windows.Forms.DataGridView();
            this.btIncluirFuncao = new System.Windows.Forms.Button();
            this.lblSimplesNacional = new System.Windows.Forms.TextBox();
            this.lblValorLiquido = new System.Windows.Forms.TextBox();
            this.cbValorLiquido = new SWS.ComboBoxWithBorder();
            this.lblPrestador = new System.Windows.Forms.TextBox();
            this.cbPrestador = new SWS.ComboBoxWithBorder();
            this.lblClienteParticular = new System.Windows.Forms.TextBox();
            this.cbParticular = new SWS.ComboBoxWithBorder();
            this.lblClienteContrato = new System.Windows.Forms.TextBox();
            this.cbContrato = new SWS.ComboBoxWithBorder();
            this.lblClienteVIP = new System.Windows.Forms.TextBox();
            this.cbClienteVIP = new SWS.ComboBoxWithBorder();
            this.tpCliente = new System.Windows.Forms.ToolTip(this.components);
            this.cbTipoCliente = new SWS.ComboBoxWithBorder();
            this.lblCodigoCnes = new System.Windows.Forms.TextBox();
            this.textCodigoCnes = new System.Windows.Forms.TextBox();
            this.lblCentroCusto = new System.Windows.Forms.TextBox();
            this.cbCentroCusto = new SWS.ComboBoxWithBorder();
            this.grbCentroCusto = new System.Windows.Forms.GroupBox();
            this.dgvCentroCusto = new System.Windows.Forms.DataGridView();
            this.flpAcaoCentroCusto = new System.Windows.Forms.FlowLayoutPanel();
            this.btnIncluirCentroCusto = new System.Windows.Forms.Button();
            this.btnExcluirCentroCusto = new System.Windows.Forms.Button();
            this.lblSituacaoAtendimento = new System.Windows.Forms.TextBox();
            this.cbBloqueado = new SWS.ComboBoxWithBorder();
            this.lblCredenciadora = new System.Windows.Forms.TextBox();
            this.cbCredenciadora = new SWS.ComboBoxWithBorder();
            this.lblUsaPo = new System.Windows.Forms.TextBox();
            this.cbUsaPo = new SWS.ComboBoxWithBorder();
            this.cbSimplesNacional = new SWS.ComboBoxWithBorder();
            ((System.ComponentModel.ISupportInitialize)(this.spForm)).BeginInit();
            this.spForm.Panel1.SuspendLayout();
            this.spForm.Panel2.SuspendLayout();
            this.spForm.SuspendLayout();
            this.pnlForm.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.banner)).BeginInit();
            this.flpAcao.SuspendLayout();
            this.tpEndereco.SuspendLayout();
            this.tpComercial.SuspendLayout();
            this.tpCobranca.SuspendLayout();
            this.grbCnae.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvCnae)).BeginInit();
            this.grbLogo.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbLogo)).BeginInit();
            this.grbContato.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvContatos)).BeginInit();
            this.grbFuncao.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvFuncao)).BeginInit();
            this.grbCentroCusto.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvCentroCusto)).BeginInit();
            this.flpAcaoCentroCusto.SuspendLayout();
            this.SuspendLayout();
            // 
            // spForm
            // 
            this.spForm.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            // 
            // spForm.Panel1
            // 
            this.spForm.Panel1.Controls.Add(this.flpAcao);
            this.spForm.TabIndex = 600;
            // 
            // pnlForm
            // 
            this.pnlForm.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.pnlForm.AutoScroll = true;
            this.pnlForm.Controls.Add(this.cbSimplesNacional);
            this.pnlForm.Controls.Add(this.cbUsaPo);
            this.pnlForm.Controls.Add(this.lblUsaPo);
            this.pnlForm.Controls.Add(this.cbTipoCliente);
            this.pnlForm.Controls.Add(this.cbCredenciadora);
            this.pnlForm.Controls.Add(this.lblCredenciadora);
            this.pnlForm.Controls.Add(this.cbBloqueado);
            this.pnlForm.Controls.Add(this.lblSituacaoAtendimento);
            this.pnlForm.Controls.Add(this.flpAcaoCentroCusto);
            this.pnlForm.Controls.Add(this.grbCentroCusto);
            this.pnlForm.Controls.Add(this.cbCentroCusto);
            this.pnlForm.Controls.Add(this.lblCentroCusto);
            this.pnlForm.Controls.Add(this.textCodigoCnes);
            this.pnlForm.Controls.Add(this.lblCodigoCnes);
            this.pnlForm.Controls.Add(this.lblSimplesNacional);
            this.pnlForm.Controls.Add(this.lblValorLiquido);
            this.pnlForm.Controls.Add(this.cbValorLiquido);
            this.pnlForm.Controls.Add(this.lblPrestador);
            this.pnlForm.Controls.Add(this.cbPrestador);
            this.pnlForm.Controls.Add(this.lblClienteParticular);
            this.pnlForm.Controls.Add(this.cbParticular);
            this.pnlForm.Controls.Add(this.lblClienteContrato);
            this.pnlForm.Controls.Add(this.cbContrato);
            this.pnlForm.Controls.Add(this.lblClienteVIP);
            this.pnlForm.Controls.Add(this.cbClienteVIP);
            this.pnlForm.Controls.Add(this.lblContato);
            this.pnlForm.Controls.Add(this.btExcluirContato);
            this.pnlForm.Controls.Add(this.btIncluirContato);
            this.pnlForm.Controls.Add(this.grbContato);
            this.pnlForm.Controls.Add(this.btExcluirFuncao);
            this.pnlForm.Controls.Add(this.grbFuncao);
            this.pnlForm.Controls.Add(this.btIncluirFuncao);
            this.pnlForm.Controls.Add(this.btExcluirLogo);
            this.pnlForm.Controls.Add(this.grbLogo);
            this.pnlForm.Controls.Add(this.btIncluirLogo);
            this.pnlForm.Controls.Add(this.lblCnaePrincipal);
            this.pnlForm.Controls.Add(this.grbCnae);
            this.pnlForm.Controls.Add(this.btExcluirCnae);
            this.pnlForm.Controls.Add(this.btIncluirCnae);
            this.pnlForm.Controls.Add(this.btRamo);
            this.pnlForm.Controls.Add(this.lblJornada);
            this.pnlForm.Controls.Add(this.lblRamo);
            this.pnlForm.Controls.Add(this.lblEscopo);
            this.pnlForm.Controls.Add(this.textRamo);
            this.pnlForm.Controls.Add(this.textJornada);
            this.pnlForm.Controls.Add(this.textEscopo);
            this.pnlForm.Controls.Add(this.textSite);
            this.pnlForm.Controls.Add(this.lblSite);
            this.pnlForm.Controls.Add(this.lblEmail);
            this.pnlForm.Controls.Add(this.tpEndereco);
            this.pnlForm.Controls.Add(this.textEmail);
            this.pnlForm.Controls.Add(this.textInscricao);
            this.pnlForm.Controls.Add(this.lblInscricao);
            this.pnlForm.Controls.Add(this.lblNomeFantasia);
            this.pnlForm.Controls.Add(this.textFantasia);
            this.pnlForm.Controls.Add(this.lblCNPJ);
            this.pnlForm.Controls.Add(this.textRazaoSocial);
            this.pnlForm.Controls.Add(this.btCredenciada);
            this.pnlForm.Controls.Add(this.textCNPJ);
            this.pnlForm.Controls.Add(this.textCredenciada);
            this.pnlForm.Controls.Add(this.btMatriz);
            this.pnlForm.Controls.Add(this.textMatriz);
            this.pnlForm.Controls.Add(this.btVendedor);
            this.pnlForm.Controls.Add(this.textVendedor);
            this.pnlForm.Controls.Add(this.lblMatriz);
            this.pnlForm.Controls.Add(this.lblCredenciada);
            this.pnlForm.Controls.Add(this.lblVendedor);
            this.pnlForm.Controls.Add(this.lblRazaoSocial);
            this.pnlForm.Controls.Add(this.lbltipoCliente);
            this.pnlForm.Size = new System.Drawing.Size(781, 464);
            // 
            // banner
            // 
            this.banner.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.banner.Size = new System.Drawing.Size(784, 37);
            // 
            // flpAcao
            // 
            this.flpAcao.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.flpAcao.Controls.Add(this.btGravar);
            this.flpAcao.Controls.Add(this.btImprimir);
            this.flpAcao.Controls.Add(this.btLimpar);
            this.flpAcao.Controls.Add(this.btFechar);
            this.flpAcao.Location = new System.Drawing.Point(0, 0);
            this.flpAcao.Name = "flpAcao";
            this.flpAcao.Size = new System.Drawing.Size(784, 35);
            this.flpAcao.TabIndex = 0;
            // 
            // btGravar
            // 
            this.btGravar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btGravar.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btGravar.Image = global::SWS.Properties.Resources.fechar_ico;
            this.btGravar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btGravar.Location = new System.Drawing.Point(3, 3);
            this.btGravar.Name = "btGravar";
            this.btGravar.Size = new System.Drawing.Size(77, 23);
            this.btGravar.TabIndex = 37;
            this.btGravar.Text = "&Gravar";
            this.btGravar.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btGravar.UseVisualStyleBackColor = true;
            this.btGravar.Click += new System.EventHandler(this.btGravar_Click);
            // 
            // btImprimir
            // 
            this.btImprimir.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btImprimir.Image = global::SWS.Properties.Resources.impressora1;
            this.btImprimir.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btImprimir.Location = new System.Drawing.Point(86, 3);
            this.btImprimir.Name = "btImprimir";
            this.btImprimir.Size = new System.Drawing.Size(77, 23);
            this.btImprimir.TabIndex = 39;
            this.btImprimir.Text = "&Imprimir";
            this.btImprimir.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btImprimir.UseVisualStyleBackColor = true;
            this.btImprimir.Click += new System.EventHandler(this.btImprimir_Click);
            // 
            // btLimpar
            // 
            this.btLimpar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btLimpar.Image = global::SWS.Properties.Resources.vassoura;
            this.btLimpar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btLimpar.Location = new System.Drawing.Point(169, 3);
            this.btLimpar.Name = "btLimpar";
            this.btLimpar.Size = new System.Drawing.Size(77, 23);
            this.btLimpar.TabIndex = 38;
            this.btLimpar.TabStop = false;
            this.btLimpar.Text = "&Limpar";
            this.btLimpar.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btLimpar.UseVisualStyleBackColor = true;
            this.btLimpar.Click += new System.EventHandler(this.btLimpar_Click);
            // 
            // btFechar
            // 
            this.btFechar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btFechar.Image = global::SWS.Properties.Resources.close;
            this.btFechar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btFechar.Location = new System.Drawing.Point(252, 3);
            this.btFechar.Name = "btFechar";
            this.btFechar.Size = new System.Drawing.Size(77, 23);
            this.btFechar.TabIndex = 40;
            this.btFechar.Text = "&Fechar";
            this.btFechar.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btFechar.UseVisualStyleBackColor = true;
            this.btFechar.Click += new System.EventHandler(this.btFechar_Click);
            // 
            // btRamo
            // 
            this.btRamo.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btRamo.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btRamo.Image = global::SWS.Properties.Resources.busca;
            this.btRamo.Location = new System.Drawing.Point(530, 203);
            this.btRamo.Name = "btRamo";
            this.btRamo.Size = new System.Drawing.Size(34, 21);
            this.btRamo.TabIndex = 9;
            this.btRamo.TabStop = false;
            this.btRamo.UseVisualStyleBackColor = true;
            this.btRamo.Click += new System.EventHandler(this.btRamo_Click);
            // 
            // lblJornada
            // 
            this.lblJornada.BackColor = System.Drawing.Color.LightGray;
            this.lblJornada.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblJornada.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblJornada.Location = new System.Drawing.Point(3, 223);
            this.lblJornada.Name = "lblJornada";
            this.lblJornada.ReadOnly = true;
            this.lblJornada.Size = new System.Drawing.Size(200, 21);
            this.lblJornada.TabIndex = 97;
            this.lblJornada.TabStop = false;
            this.lblJornada.Text = "Jornada de Trabalho";
            // 
            // lblRamo
            // 
            this.lblRamo.BackColor = System.Drawing.Color.LightGray;
            this.lblRamo.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblRamo.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblRamo.Location = new System.Drawing.Point(3, 203);
            this.lblRamo.Name = "lblRamo";
            this.lblRamo.ReadOnly = true;
            this.lblRamo.Size = new System.Drawing.Size(200, 21);
            this.lblRamo.TabIndex = 100;
            this.lblRamo.TabStop = false;
            this.lblRamo.Text = "Ramo de Atividade";
            // 
            // lblEscopo
            // 
            this.lblEscopo.BackColor = System.Drawing.Color.LightGray;
            this.lblEscopo.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblEscopo.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblEscopo.Location = new System.Drawing.Point(3, 243);
            this.lblEscopo.Name = "lblEscopo";
            this.lblEscopo.ReadOnly = true;
            this.lblEscopo.Size = new System.Drawing.Size(200, 21);
            this.lblEscopo.TabIndex = 98;
            this.lblEscopo.TabStop = false;
            this.lblEscopo.Text = "Escopo da Atividade";
            // 
            // textRamo
            // 
            this.textRamo.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.textRamo.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.textRamo.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textRamo.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textRamo.Location = new System.Drawing.Point(202, 203);
            this.textRamo.MaxLength = 50;
            this.textRamo.Name = "textRamo";
            this.textRamo.ReadOnly = true;
            this.textRamo.Size = new System.Drawing.Size(329, 21);
            this.textRamo.TabIndex = 99;
            this.textRamo.TabStop = false;
            // 
            // textJornada
            // 
            this.textJornada.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.textJornada.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textJornada.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.textJornada.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textJornada.Location = new System.Drawing.Point(202, 223);
            this.textJornada.MaxLength = 300;
            this.textJornada.Name = "textJornada";
            this.textJornada.Size = new System.Drawing.Size(362, 21);
            this.textJornada.TabIndex = 10;
            // 
            // textEscopo
            // 
            this.textEscopo.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.textEscopo.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textEscopo.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.textEscopo.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textEscopo.Location = new System.Drawing.Point(200, 243);
            this.textEscopo.MaxLength = 600;
            this.textEscopo.Name = "textEscopo";
            this.textEscopo.Size = new System.Drawing.Size(364, 21);
            this.textEscopo.TabIndex = 11;
            // 
            // textSite
            // 
            this.textSite.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.textSite.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textSite.CharacterCasing = System.Windows.Forms.CharacterCasing.Lower;
            this.textSite.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textSite.Location = new System.Drawing.Point(202, 183);
            this.textSite.MaxLength = 50;
            this.textSite.Name = "textSite";
            this.textSite.Size = new System.Drawing.Size(362, 21);
            this.textSite.TabIndex = 8;
            // 
            // lblSite
            // 
            this.lblSite.BackColor = System.Drawing.Color.LightGray;
            this.lblSite.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblSite.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSite.Location = new System.Drawing.Point(3, 183);
            this.lblSite.Name = "lblSite";
            this.lblSite.ReadOnly = true;
            this.lblSite.Size = new System.Drawing.Size(200, 21);
            this.lblSite.TabIndex = 96;
            this.lblSite.TabStop = false;
            this.lblSite.Text = "Site";
            // 
            // lblEmail
            // 
            this.lblEmail.BackColor = System.Drawing.Color.LightGray;
            this.lblEmail.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblEmail.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblEmail.Location = new System.Drawing.Point(3, 163);
            this.lblEmail.Name = "lblEmail";
            this.lblEmail.ReadOnly = true;
            this.lblEmail.Size = new System.Drawing.Size(200, 21);
            this.lblEmail.TabIndex = 95;
            this.lblEmail.TabStop = false;
            this.lblEmail.Text = "E-mail";
            // 
            // tpEndereco
            // 
            this.tpEndereco.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tpEndereco.Controls.Add(this.tpComercial);
            this.tpEndereco.Controls.Add(this.tpCobranca);
            this.tpEndereco.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tpEndereco.Location = new System.Drawing.Point(3, 297);
            this.tpEndereco.Name = "tpEndereco";
            this.tpEndereco.SelectedIndex = 0;
            this.tpEndereco.Size = new System.Drawing.Size(561, 255);
            this.tpEndereco.TabIndex = 86;
            this.tpEndereco.TabStop = false;
            // 
            // tpComercial
            // 
            this.tpComercial.Controls.Add(this.textFaxComercial);
            this.tpComercial.Controls.Add(this.lblFaxComercial);
            this.tpComercial.Controls.Add(this.lblTelefoneComercial);
            this.tpComercial.Controls.Add(this.textTelefoneComercial);
            this.tpComercial.Controls.Add(this.lblCidade);
            this.tpComercial.Controls.Add(this.btCidadeComercial);
            this.tpComercial.Controls.Add(this.textCidadeComercial);
            this.tpComercial.Controls.Add(this.textCEPComercial);
            this.tpComercial.Controls.Add(this.lblUF);
            this.tpComercial.Controls.Add(this.lblCEP);
            this.tpComercial.Controls.Add(this.lblBairroComercial);
            this.tpComercial.Controls.Add(this.lblComplementoComercial);
            this.tpComercial.Controls.Add(this.lblNumeroComercial);
            this.tpComercial.Controls.Add(this.lblEnderecoComercial);
            this.tpComercial.Controls.Add(this.textLogradouroComercial);
            this.tpComercial.Controls.Add(this.textNumeroComercial);
            this.tpComercial.Controls.Add(this.textComplementoComercial);
            this.tpComercial.Controls.Add(this.textBairroComercial);
            this.tpComercial.Controls.Add(this.cbUFComercial);
            this.tpComercial.Location = new System.Drawing.Point(4, 24);
            this.tpComercial.Name = "tpComercial";
            this.tpComercial.Padding = new System.Windows.Forms.Padding(3);
            this.tpComercial.Size = new System.Drawing.Size(553, 227);
            this.tpComercial.TabIndex = 0;
            this.tpComercial.Text = "Comercial";
            this.tpComercial.UseVisualStyleBackColor = true;
            // 
            // textFaxComercial
            // 
            this.textFaxComercial.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.textFaxComercial.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textFaxComercial.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.textFaxComercial.Location = new System.Drawing.Point(205, 166);
            this.textFaxComercial.MaxLength = 15;
            this.textFaxComercial.Name = "textFaxComercial";
            this.textFaxComercial.Size = new System.Drawing.Size(342, 21);
            this.textFaxComercial.TabIndex = 21;
            // 
            // lblFaxComercial
            // 
            this.lblFaxComercial.BackColor = System.Drawing.Color.LightGray;
            this.lblFaxComercial.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblFaxComercial.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblFaxComercial.Location = new System.Drawing.Point(6, 166);
            this.lblFaxComercial.Name = "lblFaxComercial";
            this.lblFaxComercial.ReadOnly = true;
            this.lblFaxComercial.Size = new System.Drawing.Size(200, 21);
            this.lblFaxComercial.TabIndex = 42;
            this.lblFaxComercial.TabStop = false;
            this.lblFaxComercial.Text = "Fax / Celular";
            // 
            // lblTelefoneComercial
            // 
            this.lblTelefoneComercial.BackColor = System.Drawing.Color.LightGray;
            this.lblTelefoneComercial.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblTelefoneComercial.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTelefoneComercial.Location = new System.Drawing.Point(6, 146);
            this.lblTelefoneComercial.Name = "lblTelefoneComercial";
            this.lblTelefoneComercial.ReadOnly = true;
            this.lblTelefoneComercial.Size = new System.Drawing.Size(200, 21);
            this.lblTelefoneComercial.TabIndex = 41;
            this.lblTelefoneComercial.TabStop = false;
            this.lblTelefoneComercial.Text = "Telefone";
            // 
            // textTelefoneComercial
            // 
            this.textTelefoneComercial.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.textTelefoneComercial.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textTelefoneComercial.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.textTelefoneComercial.Location = new System.Drawing.Point(205, 146);
            this.textTelefoneComercial.MaxLength = 15;
            this.textTelefoneComercial.Name = "textTelefoneComercial";
            this.textTelefoneComercial.Size = new System.Drawing.Size(342, 21);
            this.textTelefoneComercial.TabIndex = 20;
            // 
            // lblCidade
            // 
            this.lblCidade.BackColor = System.Drawing.Color.LightGray;
            this.lblCidade.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblCidade.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCidade.Location = new System.Drawing.Point(6, 126);
            this.lblCidade.Name = "lblCidade";
            this.lblCidade.ReadOnly = true;
            this.lblCidade.Size = new System.Drawing.Size(200, 21);
            this.lblCidade.TabIndex = 40;
            this.lblCidade.TabStop = false;
            this.lblCidade.Text = "Cidade";
            // 
            // btCidadeComercial
            // 
            this.btCidadeComercial.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btCidadeComercial.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btCidadeComercial.Image = global::SWS.Properties.Resources.busca;
            this.btCidadeComercial.Location = new System.Drawing.Point(513, 126);
            this.btCidadeComercial.Name = "btCidadeComercial";
            this.btCidadeComercial.Size = new System.Drawing.Size(34, 21);
            this.btCidadeComercial.TabIndex = 19;
            this.btCidadeComercial.TabStop = false;
            this.btCidadeComercial.UseVisualStyleBackColor = true;
            this.btCidadeComercial.Click += new System.EventHandler(this.btCidadeComercial_Click);
            // 
            // textCidadeComercial
            // 
            this.textCidadeComercial.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.textCidadeComercial.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.textCidadeComercial.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textCidadeComercial.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textCidadeComercial.Location = new System.Drawing.Point(205, 126);
            this.textCidadeComercial.Name = "textCidadeComercial";
            this.textCidadeComercial.ReadOnly = true;
            this.textCidadeComercial.Size = new System.Drawing.Size(314, 21);
            this.textCidadeComercial.TabIndex = 39;
            this.textCidadeComercial.TabStop = false;
            // 
            // textCEPComercial
            // 
            this.textCEPComercial.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.textCEPComercial.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textCEPComercial.Location = new System.Drawing.Point(205, 86);
            this.textCEPComercial.Mask = "99,999-999";
            this.textCEPComercial.Name = "textCEPComercial";
            this.textCEPComercial.PromptChar = ' ';
            this.textCEPComercial.Size = new System.Drawing.Size(342, 21);
            this.textCEPComercial.TabIndex = 17;
            this.textCEPComercial.Leave += new System.EventHandler(this.textCEPComercial_Leave);
            // 
            // lblUF
            // 
            this.lblUF.BackColor = System.Drawing.Color.LightGray;
            this.lblUF.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblUF.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblUF.Location = new System.Drawing.Point(6, 106);
            this.lblUF.Name = "lblUF";
            this.lblUF.ReadOnly = true;
            this.lblUF.Size = new System.Drawing.Size(200, 21);
            this.lblUF.TabIndex = 37;
            this.lblUF.TabStop = false;
            this.lblUF.Text = "Unidade Federativa";
            // 
            // lblCEP
            // 
            this.lblCEP.BackColor = System.Drawing.Color.LightGray;
            this.lblCEP.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblCEP.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCEP.Location = new System.Drawing.Point(6, 86);
            this.lblCEP.Name = "lblCEP";
            this.lblCEP.ReadOnly = true;
            this.lblCEP.Size = new System.Drawing.Size(200, 21);
            this.lblCEP.TabIndex = 36;
            this.lblCEP.TabStop = false;
            this.lblCEP.Text = "CEP";
            // 
            // lblBairroComercial
            // 
            this.lblBairroComercial.BackColor = System.Drawing.Color.LightGray;
            this.lblBairroComercial.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblBairroComercial.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblBairroComercial.Location = new System.Drawing.Point(6, 66);
            this.lblBairroComercial.Name = "lblBairroComercial";
            this.lblBairroComercial.ReadOnly = true;
            this.lblBairroComercial.Size = new System.Drawing.Size(200, 21);
            this.lblBairroComercial.TabIndex = 35;
            this.lblBairroComercial.TabStop = false;
            this.lblBairroComercial.Text = "Bairro";
            // 
            // lblComplementoComercial
            // 
            this.lblComplementoComercial.BackColor = System.Drawing.Color.LightGray;
            this.lblComplementoComercial.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblComplementoComercial.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblComplementoComercial.Location = new System.Drawing.Point(6, 46);
            this.lblComplementoComercial.Name = "lblComplementoComercial";
            this.lblComplementoComercial.ReadOnly = true;
            this.lblComplementoComercial.Size = new System.Drawing.Size(200, 21);
            this.lblComplementoComercial.TabIndex = 34;
            this.lblComplementoComercial.TabStop = false;
            this.lblComplementoComercial.Text = "Complemento";
            // 
            // lblNumeroComercial
            // 
            this.lblNumeroComercial.BackColor = System.Drawing.Color.LightGray;
            this.lblNumeroComercial.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblNumeroComercial.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNumeroComercial.Location = new System.Drawing.Point(6, 26);
            this.lblNumeroComercial.Name = "lblNumeroComercial";
            this.lblNumeroComercial.ReadOnly = true;
            this.lblNumeroComercial.Size = new System.Drawing.Size(200, 21);
            this.lblNumeroComercial.TabIndex = 33;
            this.lblNumeroComercial.TabStop = false;
            this.lblNumeroComercial.Text = "Número";
            // 
            // lblEnderecoComercial
            // 
            this.lblEnderecoComercial.BackColor = System.Drawing.Color.LightGray;
            this.lblEnderecoComercial.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblEnderecoComercial.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblEnderecoComercial.Location = new System.Drawing.Point(6, 6);
            this.lblEnderecoComercial.Name = "lblEnderecoComercial";
            this.lblEnderecoComercial.ReadOnly = true;
            this.lblEnderecoComercial.Size = new System.Drawing.Size(200, 21);
            this.lblEnderecoComercial.TabIndex = 32;
            this.lblEnderecoComercial.TabStop = false;
            this.lblEnderecoComercial.Text = "Logradouro";
            // 
            // textLogradouroComercial
            // 
            this.textLogradouroComercial.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.textLogradouroComercial.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textLogradouroComercial.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.textLogradouroComercial.Location = new System.Drawing.Point(205, 6);
            this.textLogradouroComercial.MaxLength = 100;
            this.textLogradouroComercial.Name = "textLogradouroComercial";
            this.textLogradouroComercial.Size = new System.Drawing.Size(342, 21);
            this.textLogradouroComercial.TabIndex = 13;
            // 
            // textNumeroComercial
            // 
            this.textNumeroComercial.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.textNumeroComercial.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textNumeroComercial.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.textNumeroComercial.Location = new System.Drawing.Point(205, 26);
            this.textNumeroComercial.MaxLength = 10;
            this.textNumeroComercial.Name = "textNumeroComercial";
            this.textNumeroComercial.Size = new System.Drawing.Size(342, 21);
            this.textNumeroComercial.TabIndex = 14;
            // 
            // textComplementoComercial
            // 
            this.textComplementoComercial.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.textComplementoComercial.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textComplementoComercial.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.textComplementoComercial.Location = new System.Drawing.Point(205, 46);
            this.textComplementoComercial.MaxLength = 100;
            this.textComplementoComercial.Name = "textComplementoComercial";
            this.textComplementoComercial.Size = new System.Drawing.Size(342, 21);
            this.textComplementoComercial.TabIndex = 15;
            // 
            // textBairroComercial
            // 
            this.textBairroComercial.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.textBairroComercial.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBairroComercial.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.textBairroComercial.Location = new System.Drawing.Point(205, 66);
            this.textBairroComercial.MaxLength = 50;
            this.textBairroComercial.Name = "textBairroComercial";
            this.textBairroComercial.Size = new System.Drawing.Size(342, 21);
            this.textBairroComercial.TabIndex = 16;
            // 
            // cbUFComercial
            // 
            this.cbUFComercial.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.cbUFComercial.BackColor = System.Drawing.Color.LightGray;
            this.cbUFComercial.BorderColor = System.Drawing.Color.DimGray;
            this.cbUFComercial.BorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid;
            this.cbUFComercial.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cbUFComercial.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbUFComercial.FormattingEnabled = true;
            this.cbUFComercial.Location = new System.Drawing.Point(205, 106);
            this.cbUFComercial.Name = "cbUFComercial";
            this.cbUFComercial.Size = new System.Drawing.Size(342, 21);
            this.cbUFComercial.TabIndex = 18;
            // 
            // tpCobranca
            // 
            this.tpCobranca.Controls.Add(this.btCopiarComercial);
            this.tpCobranca.Controls.Add(this.btCidadeCobranca);
            this.tpCobranca.Controls.Add(this.textFaxCobranca);
            this.tpCobranca.Controls.Add(this.lblFaxCobranca);
            this.tpCobranca.Controls.Add(this.lblTelefoneCobranca);
            this.tpCobranca.Controls.Add(this.textTelefoneCobranca);
            this.tpCobranca.Controls.Add(this.lblCidadeCobranca);
            this.tpCobranca.Controls.Add(this.textCidadeCobranca);
            this.tpCobranca.Controls.Add(this.textCEPCobranca);
            this.tpCobranca.Controls.Add(this.lblUFCobranca);
            this.tpCobranca.Controls.Add(this.lblCEPCobranca);
            this.tpCobranca.Controls.Add(this.lblBairroCobranca);
            this.tpCobranca.Controls.Add(this.lblComplementoCobranca);
            this.tpCobranca.Controls.Add(this.lblNumeroCobranca);
            this.tpCobranca.Controls.Add(this.lblEnderecoCobranca);
            this.tpCobranca.Controls.Add(this.textLogradouroCobranca);
            this.tpCobranca.Controls.Add(this.textNumeroCobranca);
            this.tpCobranca.Controls.Add(this.textComplementoCobranca);
            this.tpCobranca.Controls.Add(this.textBairroCobranca);
            this.tpCobranca.Controls.Add(this.cbUFCobranca);
            this.tpCobranca.Location = new System.Drawing.Point(4, 24);
            this.tpCobranca.Name = "tpCobranca";
            this.tpCobranca.Padding = new System.Windows.Forms.Padding(3);
            this.tpCobranca.Size = new System.Drawing.Size(553, 227);
            this.tpCobranca.TabIndex = 1;
            this.tpCobranca.Text = "Cobrança";
            this.tpCobranca.UseVisualStyleBackColor = true;
            // 
            // btCopiarComercial
            // 
            this.btCopiarComercial.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btCopiarComercial.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btCopiarComercial.Image = ((System.Drawing.Image)(resources.GetObject("btCopiarComercial.Image")));
            this.btCopiarComercial.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btCopiarComercial.Location = new System.Drawing.Point(6, 193);
            this.btCopiarComercial.Name = "btCopiarComercial";
            this.btCopiarComercial.Size = new System.Drawing.Size(77, 23);
            this.btCopiarComercial.TabIndex = 62;
            this.btCopiarComercial.TabStop = false;
            this.btCopiarComercial.Text = "Copiar";
            this.btCopiarComercial.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btCopiarComercial.UseVisualStyleBackColor = true;
            this.btCopiarComercial.Click += new System.EventHandler(this.btCopiarComercial_Click);
            // 
            // btCidadeCobranca
            // 
            this.btCidadeCobranca.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btCidadeCobranca.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btCidadeCobranca.Image = global::SWS.Properties.Resources.busca;
            this.btCidadeCobranca.Location = new System.Drawing.Point(768, 126);
            this.btCidadeCobranca.Name = "btCidadeCobranca";
            this.btCidadeCobranca.Size = new System.Drawing.Size(34, 21);
            this.btCidadeCobranca.TabIndex = 61;
            this.btCidadeCobranca.TabStop = false;
            this.btCidadeCobranca.UseVisualStyleBackColor = true;
            this.btCidadeCobranca.Click += new System.EventHandler(this.btCidadeCobranca_Click);
            // 
            // textFaxCobranca
            // 
            this.textFaxCobranca.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.textFaxCobranca.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textFaxCobranca.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.textFaxCobranca.Location = new System.Drawing.Point(205, 166);
            this.textFaxCobranca.MaxLength = 15;
            this.textFaxCobranca.Name = "textFaxCobranca";
            this.textFaxCobranca.Size = new System.Drawing.Size(597, 21);
            this.textFaxCobranca.TabIndex = 50;
            this.textFaxCobranca.TabStop = false;
            // 
            // lblFaxCobranca
            // 
            this.lblFaxCobranca.BackColor = System.Drawing.Color.LightGray;
            this.lblFaxCobranca.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblFaxCobranca.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblFaxCobranca.Location = new System.Drawing.Point(6, 166);
            this.lblFaxCobranca.Name = "lblFaxCobranca";
            this.lblFaxCobranca.ReadOnly = true;
            this.lblFaxCobranca.Size = new System.Drawing.Size(200, 21);
            this.lblFaxCobranca.TabIndex = 60;
            this.lblFaxCobranca.TabStop = false;
            this.lblFaxCobranca.Text = "Fax / Celular";
            // 
            // lblTelefoneCobranca
            // 
            this.lblTelefoneCobranca.BackColor = System.Drawing.Color.LightGray;
            this.lblTelefoneCobranca.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblTelefoneCobranca.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTelefoneCobranca.Location = new System.Drawing.Point(6, 146);
            this.lblTelefoneCobranca.Name = "lblTelefoneCobranca";
            this.lblTelefoneCobranca.ReadOnly = true;
            this.lblTelefoneCobranca.Size = new System.Drawing.Size(200, 21);
            this.lblTelefoneCobranca.TabIndex = 59;
            this.lblTelefoneCobranca.TabStop = false;
            this.lblTelefoneCobranca.Text = "Telefone";
            // 
            // textTelefoneCobranca
            // 
            this.textTelefoneCobranca.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.textTelefoneCobranca.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textTelefoneCobranca.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.textTelefoneCobranca.Location = new System.Drawing.Point(205, 146);
            this.textTelefoneCobranca.MaxLength = 15;
            this.textTelefoneCobranca.Name = "textTelefoneCobranca";
            this.textTelefoneCobranca.Size = new System.Drawing.Size(597, 21);
            this.textTelefoneCobranca.TabIndex = 49;
            this.textTelefoneCobranca.TabStop = false;
            // 
            // lblCidadeCobranca
            // 
            this.lblCidadeCobranca.BackColor = System.Drawing.Color.LightGray;
            this.lblCidadeCobranca.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblCidadeCobranca.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCidadeCobranca.Location = new System.Drawing.Point(6, 126);
            this.lblCidadeCobranca.Name = "lblCidadeCobranca";
            this.lblCidadeCobranca.ReadOnly = true;
            this.lblCidadeCobranca.Size = new System.Drawing.Size(200, 21);
            this.lblCidadeCobranca.TabIndex = 58;
            this.lblCidadeCobranca.TabStop = false;
            this.lblCidadeCobranca.Text = "Cidade";
            // 
            // textCidadeCobranca
            // 
            this.textCidadeCobranca.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.textCidadeCobranca.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.textCidadeCobranca.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textCidadeCobranca.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textCidadeCobranca.Location = new System.Drawing.Point(205, 126);
            this.textCidadeCobranca.Name = "textCidadeCobranca";
            this.textCidadeCobranca.ReadOnly = true;
            this.textCidadeCobranca.Size = new System.Drawing.Size(569, 21);
            this.textCidadeCobranca.TabIndex = 57;
            this.textCidadeCobranca.TabStop = false;
            // 
            // textCEPCobranca
            // 
            this.textCEPCobranca.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.textCEPCobranca.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textCEPCobranca.Location = new System.Drawing.Point(205, 86);
            this.textCEPCobranca.Mask = "99,999-999";
            this.textCEPCobranca.Name = "textCEPCobranca";
            this.textCEPCobranca.PromptChar = ' ';
            this.textCEPCobranca.Size = new System.Drawing.Size(597, 21);
            this.textCEPCobranca.TabIndex = 47;
            this.textCEPCobranca.TabStop = false;
            // 
            // lblUFCobranca
            // 
            this.lblUFCobranca.BackColor = System.Drawing.Color.LightGray;
            this.lblUFCobranca.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblUFCobranca.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblUFCobranca.Location = new System.Drawing.Point(6, 106);
            this.lblUFCobranca.Name = "lblUFCobranca";
            this.lblUFCobranca.ReadOnly = true;
            this.lblUFCobranca.Size = new System.Drawing.Size(200, 21);
            this.lblUFCobranca.TabIndex = 56;
            this.lblUFCobranca.TabStop = false;
            this.lblUFCobranca.Text = "Unidade Federativa";
            // 
            // lblCEPCobranca
            // 
            this.lblCEPCobranca.BackColor = System.Drawing.Color.LightGray;
            this.lblCEPCobranca.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblCEPCobranca.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCEPCobranca.Location = new System.Drawing.Point(6, 86);
            this.lblCEPCobranca.Name = "lblCEPCobranca";
            this.lblCEPCobranca.ReadOnly = true;
            this.lblCEPCobranca.Size = new System.Drawing.Size(200, 21);
            this.lblCEPCobranca.TabIndex = 55;
            this.lblCEPCobranca.TabStop = false;
            this.lblCEPCobranca.Text = "CEP";
            // 
            // lblBairroCobranca
            // 
            this.lblBairroCobranca.BackColor = System.Drawing.Color.LightGray;
            this.lblBairroCobranca.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblBairroCobranca.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblBairroCobranca.Location = new System.Drawing.Point(6, 66);
            this.lblBairroCobranca.Name = "lblBairroCobranca";
            this.lblBairroCobranca.ReadOnly = true;
            this.lblBairroCobranca.Size = new System.Drawing.Size(200, 21);
            this.lblBairroCobranca.TabIndex = 54;
            this.lblBairroCobranca.TabStop = false;
            this.lblBairroCobranca.Text = "Bairro";
            // 
            // lblComplementoCobranca
            // 
            this.lblComplementoCobranca.BackColor = System.Drawing.Color.LightGray;
            this.lblComplementoCobranca.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblComplementoCobranca.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblComplementoCobranca.Location = new System.Drawing.Point(6, 46);
            this.lblComplementoCobranca.Name = "lblComplementoCobranca";
            this.lblComplementoCobranca.ReadOnly = true;
            this.lblComplementoCobranca.Size = new System.Drawing.Size(200, 21);
            this.lblComplementoCobranca.TabIndex = 53;
            this.lblComplementoCobranca.TabStop = false;
            this.lblComplementoCobranca.Text = "Complemento";
            // 
            // lblNumeroCobranca
            // 
            this.lblNumeroCobranca.BackColor = System.Drawing.Color.LightGray;
            this.lblNumeroCobranca.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblNumeroCobranca.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNumeroCobranca.Location = new System.Drawing.Point(6, 26);
            this.lblNumeroCobranca.Name = "lblNumeroCobranca";
            this.lblNumeroCobranca.ReadOnly = true;
            this.lblNumeroCobranca.Size = new System.Drawing.Size(200, 21);
            this.lblNumeroCobranca.TabIndex = 52;
            this.lblNumeroCobranca.TabStop = false;
            this.lblNumeroCobranca.Text = "Número";
            // 
            // lblEnderecoCobranca
            // 
            this.lblEnderecoCobranca.BackColor = System.Drawing.Color.LightGray;
            this.lblEnderecoCobranca.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblEnderecoCobranca.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblEnderecoCobranca.Location = new System.Drawing.Point(6, 6);
            this.lblEnderecoCobranca.Name = "lblEnderecoCobranca";
            this.lblEnderecoCobranca.ReadOnly = true;
            this.lblEnderecoCobranca.Size = new System.Drawing.Size(200, 21);
            this.lblEnderecoCobranca.TabIndex = 51;
            this.lblEnderecoCobranca.TabStop = false;
            this.lblEnderecoCobranca.Text = "Logradouro";
            // 
            // textLogradouroCobranca
            // 
            this.textLogradouroCobranca.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.textLogradouroCobranca.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textLogradouroCobranca.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.textLogradouroCobranca.Location = new System.Drawing.Point(205, 6);
            this.textLogradouroCobranca.MaxLength = 100;
            this.textLogradouroCobranca.Name = "textLogradouroCobranca";
            this.textLogradouroCobranca.Size = new System.Drawing.Size(597, 21);
            this.textLogradouroCobranca.TabIndex = 43;
            this.textLogradouroCobranca.TabStop = false;
            // 
            // textNumeroCobranca
            // 
            this.textNumeroCobranca.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.textNumeroCobranca.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textNumeroCobranca.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.textNumeroCobranca.Location = new System.Drawing.Point(205, 26);
            this.textNumeroCobranca.MaxLength = 10;
            this.textNumeroCobranca.Name = "textNumeroCobranca";
            this.textNumeroCobranca.Size = new System.Drawing.Size(597, 21);
            this.textNumeroCobranca.TabIndex = 44;
            this.textNumeroCobranca.TabStop = false;
            // 
            // textComplementoCobranca
            // 
            this.textComplementoCobranca.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.textComplementoCobranca.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textComplementoCobranca.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.textComplementoCobranca.Location = new System.Drawing.Point(205, 46);
            this.textComplementoCobranca.MaxLength = 100;
            this.textComplementoCobranca.Name = "textComplementoCobranca";
            this.textComplementoCobranca.Size = new System.Drawing.Size(597, 21);
            this.textComplementoCobranca.TabIndex = 45;
            this.textComplementoCobranca.TabStop = false;
            // 
            // textBairroCobranca
            // 
            this.textBairroCobranca.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.textBairroCobranca.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBairroCobranca.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.textBairroCobranca.Location = new System.Drawing.Point(205, 66);
            this.textBairroCobranca.MaxLength = 50;
            this.textBairroCobranca.Name = "textBairroCobranca";
            this.textBairroCobranca.Size = new System.Drawing.Size(597, 21);
            this.textBairroCobranca.TabIndex = 46;
            this.textBairroCobranca.TabStop = false;
            // 
            // cbUFCobranca
            // 
            this.cbUFCobranca.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.cbUFCobranca.BackColor = System.Drawing.Color.LightGray;
            this.cbUFCobranca.BorderColor = System.Drawing.Color.Black;
            this.cbUFCobranca.BorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid;
            this.cbUFCobranca.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cbUFCobranca.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbUFCobranca.FormattingEnabled = true;
            this.cbUFCobranca.Location = new System.Drawing.Point(205, 106);
            this.cbUFCobranca.Name = "cbUFCobranca";
            this.cbUFCobranca.Size = new System.Drawing.Size(597, 21);
            this.cbUFCobranca.TabIndex = 48;
            this.cbUFCobranca.TabStop = false;
            // 
            // textEmail
            // 
            this.textEmail.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.textEmail.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textEmail.CharacterCasing = System.Windows.Forms.CharacterCasing.Lower;
            this.textEmail.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textEmail.Location = new System.Drawing.Point(202, 163);
            this.textEmail.MaxLength = 50;
            this.textEmail.Name = "textEmail";
            this.textEmail.Size = new System.Drawing.Size(362, 21);
            this.textEmail.TabIndex = 7;
            // 
            // textInscricao
            // 
            this.textInscricao.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.textInscricao.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textInscricao.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.textInscricao.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textInscricao.Location = new System.Drawing.Point(202, 143);
            this.textInscricao.MaxLength = 20;
            this.textInscricao.Name = "textInscricao";
            this.textInscricao.Size = new System.Drawing.Size(362, 21);
            this.textInscricao.TabIndex = 6;
            // 
            // lblInscricao
            // 
            this.lblInscricao.BackColor = System.Drawing.Color.LightGray;
            this.lblInscricao.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblInscricao.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblInscricao.Location = new System.Drawing.Point(3, 143);
            this.lblInscricao.Name = "lblInscricao";
            this.lblInscricao.ReadOnly = true;
            this.lblInscricao.Size = new System.Drawing.Size(200, 21);
            this.lblInscricao.TabIndex = 94;
            this.lblInscricao.TabStop = false;
            this.lblInscricao.Text = "Inscrição Estadual / Municipal";
            // 
            // lblNomeFantasia
            // 
            this.lblNomeFantasia.BackColor = System.Drawing.Color.LightGray;
            this.lblNomeFantasia.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblNomeFantasia.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNomeFantasia.Location = new System.Drawing.Point(3, 123);
            this.lblNomeFantasia.Name = "lblNomeFantasia";
            this.lblNomeFantasia.ReadOnly = true;
            this.lblNomeFantasia.Size = new System.Drawing.Size(200, 21);
            this.lblNomeFantasia.TabIndex = 93;
            this.lblNomeFantasia.TabStop = false;
            this.lblNomeFantasia.Text = "Nome de Fantasia";
            // 
            // textFantasia
            // 
            this.textFantasia.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.textFantasia.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textFantasia.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.textFantasia.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textFantasia.Location = new System.Drawing.Point(202, 123);
            this.textFantasia.MaxLength = 100;
            this.textFantasia.Name = "textFantasia";
            this.textFantasia.Size = new System.Drawing.Size(362, 21);
            this.textFantasia.TabIndex = 5;
            // 
            // lblCNPJ
            // 
            this.lblCNPJ.BackColor = System.Drawing.Color.LightGray;
            this.lblCNPJ.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblCNPJ.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCNPJ.Location = new System.Drawing.Point(3, 103);
            this.lblCNPJ.Name = "lblCNPJ";
            this.lblCNPJ.ReadOnly = true;
            this.lblCNPJ.Size = new System.Drawing.Size(200, 21);
            this.lblCNPJ.TabIndex = 92;
            this.lblCNPJ.TabStop = false;
            this.lblCNPJ.Text = "CNPJ";
            // 
            // textRazaoSocial
            // 
            this.textRazaoSocial.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.textRazaoSocial.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textRazaoSocial.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.textRazaoSocial.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textRazaoSocial.Location = new System.Drawing.Point(202, 83);
            this.textRazaoSocial.MaxLength = 100;
            this.textRazaoSocial.Name = "textRazaoSocial";
            this.textRazaoSocial.Size = new System.Drawing.Size(362, 21);
            this.textRazaoSocial.TabIndex = 3;
            // 
            // btCredenciada
            // 
            this.btCredenciada.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btCredenciada.Enabled = false;
            this.btCredenciada.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btCredenciada.Image = global::SWS.Properties.Resources.busca;
            this.btCredenciada.Location = new System.Drawing.Point(530, 63);
            this.btCredenciada.Name = "btCredenciada";
            this.btCredenciada.Size = new System.Drawing.Size(34, 21);
            this.btCredenciada.TabIndex = 73;
            this.btCredenciada.TabStop = false;
            this.btCredenciada.UseVisualStyleBackColor = true;
            this.btCredenciada.Click += new System.EventHandler(this.btCredenciada_Click);
            // 
            // textCNPJ
            // 
            this.textCNPJ.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.textCNPJ.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textCNPJ.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textCNPJ.ForeColor = System.Drawing.Color.Blue;
            this.textCNPJ.Location = new System.Drawing.Point(202, 102);
            this.textCNPJ.Mask = "99,999,999/9999-99";
            this.textCNPJ.Name = "textCNPJ";
            this.textCNPJ.PromptChar = ' ';
            this.textCNPJ.Size = new System.Drawing.Size(362, 22);
            this.textCNPJ.TabIndex = 4;
            this.textCNPJ.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.textCNPJ_KeyPress);
            this.textCNPJ.Leave += new System.EventHandler(this.textCNPJ_Leave);
            // 
            // textCredenciada
            // 
            this.textCredenciada.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.textCredenciada.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.textCredenciada.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textCredenciada.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textCredenciada.Location = new System.Drawing.Point(202, 63);
            this.textCredenciada.Name = "textCredenciada";
            this.textCredenciada.ReadOnly = true;
            this.textCredenciada.Size = new System.Drawing.Size(329, 21);
            this.textCredenciada.TabIndex = 77;
            this.textCredenciada.TabStop = false;
            // 
            // btMatriz
            // 
            this.btMatriz.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btMatriz.Enabled = false;
            this.btMatriz.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btMatriz.Image = global::SWS.Properties.Resources.busca;
            this.btMatriz.Location = new System.Drawing.Point(530, 43);
            this.btMatriz.Name = "btMatriz";
            this.btMatriz.Size = new System.Drawing.Size(34, 21);
            this.btMatriz.TabIndex = 72;
            this.btMatriz.TabStop = false;
            this.btMatriz.UseVisualStyleBackColor = true;
            this.btMatriz.Click += new System.EventHandler(this.btMatriz_Click);
            // 
            // textMatriz
            // 
            this.textMatriz.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.textMatriz.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.textMatriz.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textMatriz.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textMatriz.Location = new System.Drawing.Point(202, 43);
            this.textMatriz.Name = "textMatriz";
            this.textMatriz.ReadOnly = true;
            this.textMatriz.Size = new System.Drawing.Size(329, 21);
            this.textMatriz.TabIndex = 75;
            this.textMatriz.TabStop = false;
            // 
            // btVendedor
            // 
            this.btVendedor.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btVendedor.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btVendedor.Image = global::SWS.Properties.Resources.busca;
            this.btVendedor.Location = new System.Drawing.Point(530, 23);
            this.btVendedor.Name = "btVendedor";
            this.btVendedor.Size = new System.Drawing.Size(34, 21);
            this.btVendedor.TabIndex = 2;
            this.btVendedor.UseVisualStyleBackColor = true;
            this.btVendedor.Click += new System.EventHandler(this.btVendedor_Click);
            // 
            // textVendedor
            // 
            this.textVendedor.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.textVendedor.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.textVendedor.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textVendedor.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textVendedor.Location = new System.Drawing.Point(202, 23);
            this.textVendedor.MaxLength = 100;
            this.textVendedor.Name = "textVendedor";
            this.textVendedor.ReadOnly = true;
            this.textVendedor.Size = new System.Drawing.Size(329, 21);
            this.textVendedor.TabIndex = 81;
            this.textVendedor.TabStop = false;
            // 
            // lblMatriz
            // 
            this.lblMatriz.BackColor = System.Drawing.Color.LightGray;
            this.lblMatriz.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblMatriz.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblMatriz.Location = new System.Drawing.Point(3, 43);
            this.lblMatriz.Name = "lblMatriz";
            this.lblMatriz.ReadOnly = true;
            this.lblMatriz.Size = new System.Drawing.Size(200, 21);
            this.lblMatriz.TabIndex = 89;
            this.lblMatriz.TabStop = false;
            this.lblMatriz.Text = "Matriz";
            // 
            // lblCredenciada
            // 
            this.lblCredenciada.BackColor = System.Drawing.Color.LightGray;
            this.lblCredenciada.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblCredenciada.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCredenciada.Location = new System.Drawing.Point(3, 63);
            this.lblCredenciada.Name = "lblCredenciada";
            this.lblCredenciada.ReadOnly = true;
            this.lblCredenciada.Size = new System.Drawing.Size(200, 21);
            this.lblCredenciada.TabIndex = 90;
            this.lblCredenciada.TabStop = false;
            this.lblCredenciada.Text = "Credenciada";
            // 
            // lblVendedor
            // 
            this.lblVendedor.BackColor = System.Drawing.Color.LightGray;
            this.lblVendedor.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblVendedor.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblVendedor.Location = new System.Drawing.Point(3, 23);
            this.lblVendedor.Name = "lblVendedor";
            this.lblVendedor.ReadOnly = true;
            this.lblVendedor.Size = new System.Drawing.Size(200, 21);
            this.lblVendedor.TabIndex = 88;
            this.lblVendedor.TabStop = false;
            this.lblVendedor.Text = "Vendedor";
            // 
            // lblRazaoSocial
            // 
            this.lblRazaoSocial.BackColor = System.Drawing.Color.LightGray;
            this.lblRazaoSocial.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblRazaoSocial.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblRazaoSocial.Location = new System.Drawing.Point(3, 83);
            this.lblRazaoSocial.Name = "lblRazaoSocial";
            this.lblRazaoSocial.ReadOnly = true;
            this.lblRazaoSocial.Size = new System.Drawing.Size(200, 21);
            this.lblRazaoSocial.TabIndex = 91;
            this.lblRazaoSocial.TabStop = false;
            this.lblRazaoSocial.Text = "Razão Social";
            // 
            // lbltipoCliente
            // 
            this.lbltipoCliente.BackColor = System.Drawing.Color.LightGray;
            this.lbltipoCliente.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lbltipoCliente.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbltipoCliente.Location = new System.Drawing.Point(3, 3);
            this.lbltipoCliente.Name = "lbltipoCliente";
            this.lbltipoCliente.ReadOnly = true;
            this.lbltipoCliente.Size = new System.Drawing.Size(200, 21);
            this.lbltipoCliente.TabIndex = 87;
            this.lbltipoCliente.TabStop = false;
            this.lbltipoCliente.Text = "Tipo de Cliente";
            // 
            // lblCnaePrincipal
            // 
            this.lblCnaePrincipal.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lblCnaePrincipal.AutoSize = true;
            this.lblCnaePrincipal.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCnaePrincipal.ForeColor = System.Drawing.Color.Red;
            this.lblCnaePrincipal.Location = new System.Drawing.Point(184, 681);
            this.lblCnaePrincipal.Name = "lblCnaePrincipal";
            this.lblCnaePrincipal.Size = new System.Drawing.Size(333, 15);
            this.lblCnaePrincipal.TabIndex = 106;
            this.lblCnaePrincipal.Text = "De um duplo click na linha para selecionar o cnae principal.";
            // 
            // grbCnae
            // 
            this.grbCnae.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.grbCnae.Controls.Add(this.dgvCnae);
            this.grbCnae.Location = new System.Drawing.Point(3, 555);
            this.grbCnae.Name = "grbCnae";
            this.grbCnae.Size = new System.Drawing.Size(561, 118);
            this.grbCnae.TabIndex = 105;
            this.grbCnae.TabStop = false;
            this.grbCnae.Text = "CNAE";
            // 
            // dgvCnae
            // 
            this.dgvCnae.AllowUserToAddRows = false;
            this.dgvCnae.AllowUserToDeleteRows = false;
            this.dgvCnae.AllowUserToOrderColumns = true;
            this.dgvCnae.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dgvCnae.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.DisplayedCells;
            this.dgvCnae.BackgroundColor = System.Drawing.Color.White;
            this.dgvCnae.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle5.BackColor = System.Drawing.Color.LightGray;
            dataGridViewCellStyle5.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle5.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle5.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle5.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle5.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvCnae.DefaultCellStyle = dataGridViewCellStyle5;
            this.dgvCnae.GridColor = System.Drawing.Color.Gray;
            this.dgvCnae.Location = new System.Drawing.Point(3, 16);
            this.dgvCnae.Name = "dgvCnae";
            this.dgvCnae.ReadOnly = true;
            this.dgvCnae.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvCnae.Size = new System.Drawing.Size(555, 99);
            this.dgvCnae.TabIndex = 3;
            this.dgvCnae.TabStop = false;
            this.dgvCnae.RowPrePaint += new System.Windows.Forms.DataGridViewRowPrePaintEventHandler(this.dgvCnae_RowPrePaint);
            this.dgvCnae.DoubleClick += new System.EventHandler(this.dgvCnae_DoubleClick);
            // 
            // btExcluirCnae
            // 
            this.btExcluirCnae.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btExcluirCnae.Image = global::SWS.Properties.Resources.lixeira;
            this.btExcluirCnae.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btExcluirCnae.Location = new System.Drawing.Point(88, 678);
            this.btExcluirCnae.Name = "btExcluirCnae";
            this.btExcluirCnae.Size = new System.Drawing.Size(75, 23);
            this.btExcluirCnae.TabIndex = 104;
            this.btExcluirCnae.TabStop = false;
            this.btExcluirCnae.Text = "Excluir";
            this.btExcluirCnae.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btExcluirCnae.UseVisualStyleBackColor = true;
            this.btExcluirCnae.Click += new System.EventHandler(this.btExcluiCnae_Click);
            // 
            // btIncluirCnae
            // 
            this.btIncluirCnae.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btIncluirCnae.Image = ((System.Drawing.Image)(resources.GetObject("btIncluirCnae.Image")));
            this.btIncluirCnae.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btIncluirCnae.Location = new System.Drawing.Point(6, 678);
            this.btIncluirCnae.Name = "btIncluirCnae";
            this.btIncluirCnae.Size = new System.Drawing.Size(75, 23);
            this.btIncluirCnae.TabIndex = 22;
            this.btIncluirCnae.Text = "Incluir";
            this.btIncluirCnae.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btIncluirCnae.UseVisualStyleBackColor = true;
            this.btIncluirCnae.Click += new System.EventHandler(this.btIncluiCnae_Click);
            // 
            // btExcluirLogo
            // 
            this.btExcluirLogo.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btExcluirLogo.Image = global::SWS.Properties.Resources.lixeira;
            this.btExcluirLogo.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btExcluirLogo.Location = new System.Drawing.Point(89, 828);
            this.btExcluirLogo.Name = "btExcluirLogo";
            this.btExcluirLogo.Size = new System.Drawing.Size(77, 23);
            this.btExcluirLogo.TabIndex = 108;
            this.btExcluirLogo.Text = "Excluir";
            this.btExcluirLogo.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btExcluirLogo.UseVisualStyleBackColor = true;
            this.btExcluirLogo.Click += new System.EventHandler(this.btExcluirLogo_Click);
            // 
            // grbLogo
            // 
            this.grbLogo.Controls.Add(this.pbLogo);
            this.grbLogo.Location = new System.Drawing.Point(3, 704);
            this.grbLogo.Name = "grbLogo";
            this.grbLogo.Size = new System.Drawing.Size(309, 121);
            this.grbLogo.TabIndex = 109;
            this.grbLogo.TabStop = false;
            this.grbLogo.Text = "Logomarca";
            // 
            // pbLogo
            // 
            this.pbLogo.BackColor = System.Drawing.Color.White;
            this.pbLogo.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pbLogo.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pbLogo.Location = new System.Drawing.Point(3, 16);
            this.pbLogo.Name = "pbLogo";
            this.pbLogo.Size = new System.Drawing.Size(303, 102);
            this.pbLogo.TabIndex = 0;
            this.pbLogo.TabStop = false;
            // 
            // btIncluirLogo
            // 
            this.btIncluirLogo.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btIncluirLogo.Image = ((System.Drawing.Image)(resources.GetObject("btIncluirLogo.Image")));
            this.btIncluirLogo.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btIncluirLogo.Location = new System.Drawing.Point(7, 828);
            this.btIncluirLogo.Name = "btIncluirLogo";
            this.btIncluirLogo.Size = new System.Drawing.Size(77, 23);
            this.btIncluirLogo.TabIndex = 23;
            this.btIncluirLogo.Text = "Incluir";
            this.btIncluirLogo.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btIncluirLogo.UseVisualStyleBackColor = true;
            this.btIncluirLogo.Click += new System.EventHandler(this.btIncluirLogo_Click);
            // 
            // lblContato
            // 
            this.lblContato.AutoSize = true;
            this.lblContato.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblContato.ForeColor = System.Drawing.Color.Red;
            this.lblContato.Location = new System.Drawing.Point(546, 1165);
            this.lblContato.Name = "lblContato";
            this.lblContato.Size = new System.Drawing.Size(206, 15);
            this.lblContato.TabIndex = 116;
            this.lblContato.Text = "Duplo click no contato para alterá-lo.";
            // 
            // btExcluirContato
            // 
            this.btExcluirContato.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btExcluirContato.Image = global::SWS.Properties.Resources.lixeira;
            this.btExcluirContato.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btExcluirContato.Location = new System.Drawing.Point(86, 1168);
            this.btExcluirContato.Name = "btExcluirContato";
            this.btExcluirContato.Size = new System.Drawing.Size(75, 23);
            this.btExcluirContato.TabIndex = 113;
            this.btExcluirContato.Text = "Excluir";
            this.btExcluirContato.UseVisualStyleBackColor = true;
            this.btExcluirContato.Click += new System.EventHandler(this.btExcluirContato_Click);
            // 
            // btIncluirContato
            // 
            this.btIncluirContato.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btIncluirContato.Image = ((System.Drawing.Image)(resources.GetObject("btIncluirContato.Image")));
            this.btIncluirContato.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btIncluirContato.Location = new System.Drawing.Point(5, 1168);
            this.btIncluirContato.Name = "btIncluirContato";
            this.btIncluirContato.Size = new System.Drawing.Size(75, 23);
            this.btIncluirContato.TabIndex = 25;
            this.btIncluirContato.Text = "Incluir";
            this.btIncluirContato.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btIncluirContato.UseVisualStyleBackColor = true;
            this.btIncluirContato.Click += new System.EventHandler(this.btIncluirContato_Click);
            // 
            // grbContato
            // 
            this.grbContato.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.grbContato.Controls.Add(this.dgvContatos);
            this.grbContato.Location = new System.Drawing.Point(1, 1035);
            this.grbContato.Name = "grbContato";
            this.grbContato.Size = new System.Drawing.Size(563, 130);
            this.grbContato.TabIndex = 115;
            this.grbContato.TabStop = false;
            this.grbContato.Text = "Contatos";
            // 
            // dgvContatos
            // 
            this.dgvContatos.AllowUserToAddRows = false;
            this.dgvContatos.AllowUserToDeleteRows = false;
            this.dgvContatos.AllowUserToResizeRows = false;
            this.dgvContatos.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dgvContatos.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.DisplayedCells;
            this.dgvContatos.BackgroundColor = System.Drawing.Color.White;
            this.dgvContatos.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.Color.LightGray;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvContatos.DefaultCellStyle = dataGridViewCellStyle2;
            this.dgvContatos.Location = new System.Drawing.Point(3, 16);
            this.dgvContatos.MultiSelect = false;
            this.dgvContatos.Name = "dgvContatos";
            this.dgvContatos.ReadOnly = true;
            this.dgvContatos.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvContatos.Size = new System.Drawing.Size(557, 111);
            this.dgvContatos.TabIndex = 100;
            this.dgvContatos.TabStop = false;
            this.dgvContatos.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvContatos_CellContentClick);
            // 
            // btExcluirFuncao
            // 
            this.btExcluirFuncao.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btExcluirFuncao.Image = global::SWS.Properties.Resources.lixeira;
            this.btExcluirFuncao.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btExcluirFuncao.Location = new System.Drawing.Point(86, 1009);
            this.btExcluirFuncao.Name = "btExcluirFuncao";
            this.btExcluirFuncao.Size = new System.Drawing.Size(75, 23);
            this.btExcluirFuncao.TabIndex = 111;
            this.btExcluirFuncao.TabStop = false;
            this.btExcluirFuncao.Text = "Excluir";
            this.btExcluirFuncao.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btExcluirFuncao.UseVisualStyleBackColor = true;
            this.btExcluirFuncao.Click += new System.EventHandler(this.btExcluirFuncao_Click);
            // 
            // grbFuncao
            // 
            this.grbFuncao.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.grbFuncao.Controls.Add(this.dgvFuncao);
            this.grbFuncao.Location = new System.Drawing.Point(1, 855);
            this.grbFuncao.Name = "grbFuncao";
            this.grbFuncao.Size = new System.Drawing.Size(563, 151);
            this.grbFuncao.TabIndex = 114;
            this.grbFuncao.TabStop = false;
            this.grbFuncao.Text = "Funções e atividades";
            // 
            // dgvFuncao
            // 
            this.dgvFuncao.AllowUserToAddRows = false;
            this.dgvFuncao.AllowUserToDeleteRows = false;
            this.dgvFuncao.AllowUserToResizeRows = false;
            this.dgvFuncao.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dgvFuncao.BackgroundColor = System.Drawing.Color.White;
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle3.BackColor = System.Drawing.Color.LightGray;
            dataGridViewCellStyle3.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvFuncao.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle3;
            this.dgvFuncao.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle4.BackColor = System.Drawing.Color.LightGray;
            dataGridViewCellStyle4.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle4.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle4.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle4.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle4.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvFuncao.DefaultCellStyle = dataGridViewCellStyle4;
            this.dgvFuncao.Location = new System.Drawing.Point(3, 16);
            this.dgvFuncao.MultiSelect = false;
            this.dgvFuncao.Name = "dgvFuncao";
            this.dgvFuncao.ReadOnly = true;
            this.dgvFuncao.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvFuncao.Size = new System.Drawing.Size(557, 132);
            this.dgvFuncao.TabIndex = 68;
            this.dgvFuncao.TabStop = false;
            // 
            // btIncluirFuncao
            // 
            this.btIncluirFuncao.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btIncluirFuncao.Image = ((System.Drawing.Image)(resources.GetObject("btIncluirFuncao.Image")));
            this.btIncluirFuncao.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btIncluirFuncao.Location = new System.Drawing.Point(4, 1009);
            this.btIncluirFuncao.Name = "btIncluirFuncao";
            this.btIncluirFuncao.Size = new System.Drawing.Size(75, 23);
            this.btIncluirFuncao.TabIndex = 24;
            this.btIncluirFuncao.Text = "Incluir";
            this.btIncluirFuncao.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btIncluirFuncao.UseVisualStyleBackColor = true;
            this.btIncluirFuncao.Click += new System.EventHandler(this.btIncluirFuncao_Click);
            // 
            // lblSimplesNacional
            // 
            this.lblSimplesNacional.BackColor = System.Drawing.Color.LightGray;
            this.lblSimplesNacional.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblSimplesNacional.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSimplesNacional.Location = new System.Drawing.Point(3, 1298);
            this.lblSimplesNacional.Name = "lblSimplesNacional";
            this.lblSimplesNacional.ReadOnly = true;
            this.lblSimplesNacional.Size = new System.Drawing.Size(200, 21);
            this.lblSimplesNacional.TabIndex = 130;
            this.lblSimplesNacional.TabStop = false;
            this.lblSimplesNacional.Text = "Simples Nacional";
            // 
            // lblValorLiquido
            // 
            this.lblValorLiquido.BackColor = System.Drawing.Color.LightGray;
            this.lblValorLiquido.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblValorLiquido.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblValorLiquido.Location = new System.Drawing.Point(3, 1278);
            this.lblValorLiquido.Name = "lblValorLiquido";
            this.lblValorLiquido.ReadOnly = true;
            this.lblValorLiquido.Size = new System.Drawing.Size(200, 21);
            this.lblValorLiquido.TabIndex = 129;
            this.lblValorLiquido.TabStop = false;
            this.lblValorLiquido.Text = "Usa valor líquido na cobrança?";
            // 
            // cbValorLiquido
            // 
            this.cbValorLiquido.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.cbValorLiquido.BackColor = System.Drawing.Color.LightGray;
            this.cbValorLiquido.BorderColor = System.Drawing.Color.DimGray;
            this.cbValorLiquido.BorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid;
            this.cbValorLiquido.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cbValorLiquido.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbValorLiquido.FormattingEnabled = true;
            this.cbValorLiquido.Location = new System.Drawing.Point(202, 1278);
            this.cbValorLiquido.Name = "cbValorLiquido";
            this.cbValorLiquido.Size = new System.Drawing.Size(363, 21);
            this.cbValorLiquido.TabIndex = 122;
            this.cbValorLiquido.Tag = "31";
            // 
            // lblPrestador
            // 
            this.lblPrestador.BackColor = System.Drawing.Color.LightGray;
            this.lblPrestador.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblPrestador.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPrestador.Location = new System.Drawing.Point(3, 1258);
            this.lblPrestador.Name = "lblPrestador";
            this.lblPrestador.ReadOnly = true;
            this.lblPrestador.Size = new System.Drawing.Size(200, 21);
            this.lblPrestador.TabIndex = 127;
            this.lblPrestador.TabStop = false;
            this.lblPrestador.Text = "Pode ser prestador?";
            // 
            // cbPrestador
            // 
            this.cbPrestador.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.cbPrestador.BackColor = System.Drawing.Color.LightGray;
            this.cbPrestador.BorderColor = System.Drawing.Color.DimGray;
            this.cbPrestador.BorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid;
            this.cbPrestador.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cbPrestador.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbPrestador.FormattingEnabled = true;
            this.cbPrestador.Location = new System.Drawing.Point(202, 1258);
            this.cbPrestador.Name = "cbPrestador";
            this.cbPrestador.Size = new System.Drawing.Size(363, 21);
            this.cbPrestador.TabIndex = 29;
            // 
            // lblClienteParticular
            // 
            this.lblClienteParticular.BackColor = System.Drawing.Color.LightGray;
            this.lblClienteParticular.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblClienteParticular.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblClienteParticular.Location = new System.Drawing.Point(3, 1238);
            this.lblClienteParticular.Name = "lblClienteParticular";
            this.lblClienteParticular.ReadOnly = true;
            this.lblClienteParticular.Size = new System.Drawing.Size(200, 21);
            this.lblClienteParticular.TabIndex = 126;
            this.lblClienteParticular.TabStop = false;
            this.lblClienteParticular.Text = "É cliente particular";
            // 
            // cbParticular
            // 
            this.cbParticular.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.cbParticular.BackColor = System.Drawing.Color.LightGray;
            this.cbParticular.BorderColor = System.Drawing.Color.DimGray;
            this.cbParticular.BorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid;
            this.cbParticular.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cbParticular.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbParticular.FormattingEnabled = true;
            this.cbParticular.Location = new System.Drawing.Point(202, 1238);
            this.cbParticular.Name = "cbParticular";
            this.cbParticular.Size = new System.Drawing.Size(363, 21);
            this.cbParticular.TabIndex = 28;
            // 
            // lblClienteContrato
            // 
            this.lblClienteContrato.BackColor = System.Drawing.Color.LightGray;
            this.lblClienteContrato.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblClienteContrato.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblClienteContrato.Location = new System.Drawing.Point(3, 1218);
            this.lblClienteContrato.Name = "lblClienteContrato";
            this.lblClienteContrato.ReadOnly = true;
            this.lblClienteContrato.Size = new System.Drawing.Size(200, 21);
            this.lblClienteContrato.TabIndex = 125;
            this.lblClienteContrato.TabStop = false;
            this.lblClienteContrato.Text = "Utiliza contrato?";
            // 
            // cbContrato
            // 
            this.cbContrato.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.cbContrato.BackColor = System.Drawing.Color.LightGray;
            this.cbContrato.BorderColor = System.Drawing.Color.DimGray;
            this.cbContrato.BorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid;
            this.cbContrato.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cbContrato.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbContrato.FormattingEnabled = true;
            this.cbContrato.Location = new System.Drawing.Point(202, 1218);
            this.cbContrato.Name = "cbContrato";
            this.cbContrato.Size = new System.Drawing.Size(363, 21);
            this.cbContrato.TabIndex = 27;
            // 
            // lblClienteVIP
            // 
            this.lblClienteVIP.BackColor = System.Drawing.Color.LightGray;
            this.lblClienteVIP.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblClienteVIP.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblClienteVIP.Location = new System.Drawing.Point(3, 1198);
            this.lblClienteVIP.Name = "lblClienteVIP";
            this.lblClienteVIP.ReadOnly = true;
            this.lblClienteVIP.Size = new System.Drawing.Size(200, 21);
            this.lblClienteVIP.TabIndex = 124;
            this.lblClienteVIP.TabStop = false;
            this.lblClienteVIP.Text = "É cliente VIP?";
            // 
            // cbClienteVIP
            // 
            this.cbClienteVIP.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.cbClienteVIP.BackColor = System.Drawing.Color.LightGray;
            this.cbClienteVIP.BorderColor = System.Drawing.Color.DimGray;
            this.cbClienteVIP.BorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid;
            this.cbClienteVIP.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cbClienteVIP.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbClienteVIP.FormattingEnabled = true;
            this.cbClienteVIP.Location = new System.Drawing.Point(202, 1198);
            this.cbClienteVIP.Name = "cbClienteVIP";
            this.cbClienteVIP.Size = new System.Drawing.Size(363, 21);
            this.cbClienteVIP.TabIndex = 26;
            // 
            // tpCliente
            // 
            this.tpCliente.ToolTipIcon = System.Windows.Forms.ToolTipIcon.Info;
            // 
            // cbTipoCliente
            // 
            this.cbTipoCliente.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.cbTipoCliente.BackColor = System.Drawing.Color.LightGray;
            this.cbTipoCliente.BorderColor = System.Drawing.Color.DimGray;
            this.cbTipoCliente.BorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid;
            this.cbTipoCliente.FormattingEnabled = true;
            this.cbTipoCliente.Location = new System.Drawing.Point(202, 3);
            this.cbTipoCliente.Name = "cbTipoCliente";
            this.cbTipoCliente.Size = new System.Drawing.Size(362, 21);
            this.cbTipoCliente.TabIndex = 1;
            this.tpCliente.SetToolTip(this.cbTipoCliente, "Informe o tipo de cliente que será cadastrado.");
            this.cbTipoCliente.SelectedIndexChanged += new System.EventHandler(this.cbTipoCliente_SelectedIndexChanged);
            // 
            // lblCodigoCnes
            // 
            this.lblCodigoCnes.BackColor = System.Drawing.Color.LightGray;
            this.lblCodigoCnes.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblCodigoCnes.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCodigoCnes.Location = new System.Drawing.Point(3, 1318);
            this.lblCodigoCnes.Name = "lblCodigoCnes";
            this.lblCodigoCnes.ReadOnly = true;
            this.lblCodigoCnes.Size = new System.Drawing.Size(200, 21);
            this.lblCodigoCnes.TabIndex = 131;
            this.lblCodigoCnes.TabStop = false;
            this.lblCodigoCnes.Text = "Código CNES";
            // 
            // textCodigoCnes
            // 
            this.textCodigoCnes.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.textCodigoCnes.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textCodigoCnes.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textCodigoCnes.Location = new System.Drawing.Point(202, 1318);
            this.textCodigoCnes.MaxLength = 7;
            this.textCodigoCnes.Name = "textCodigoCnes";
            this.textCodigoCnes.Size = new System.Drawing.Size(363, 21);
            this.textCodigoCnes.TabIndex = 33;
            this.textCodigoCnes.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.textCodigoCnes_KeyPress);
            // 
            // lblCentroCusto
            // 
            this.lblCentroCusto.BackColor = System.Drawing.Color.LightGray;
            this.lblCentroCusto.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblCentroCusto.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCentroCusto.Location = new System.Drawing.Point(3, 1338);
            this.lblCentroCusto.Name = "lblCentroCusto";
            this.lblCentroCusto.ReadOnly = true;
            this.lblCentroCusto.Size = new System.Drawing.Size(200, 21);
            this.lblCentroCusto.TabIndex = 132;
            this.lblCentroCusto.TabStop = false;
            this.lblCentroCusto.Text = "Usa Centro de Custo?";
            // 
            // cbCentroCusto
            // 
            this.cbCentroCusto.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.cbCentroCusto.BackColor = System.Drawing.Color.LightGray;
            this.cbCentroCusto.BorderColor = System.Drawing.Color.DimGray;
            this.cbCentroCusto.BorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid;
            this.cbCentroCusto.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cbCentroCusto.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbCentroCusto.FormattingEnabled = true;
            this.cbCentroCusto.Location = new System.Drawing.Point(202, 1338);
            this.cbCentroCusto.Name = "cbCentroCusto";
            this.cbCentroCusto.Size = new System.Drawing.Size(363, 21);
            this.cbCentroCusto.TabIndex = 34;
            this.cbCentroCusto.SelectedIndexChanged += new System.EventHandler(this.cbCentroCusto_SelectedIndexChanged);
            // 
            // grbCentroCusto
            // 
            this.grbCentroCusto.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.grbCentroCusto.Controls.Add(this.dgvCentroCusto);
            this.grbCentroCusto.Location = new System.Drawing.Point(3, 1407);
            this.grbCentroCusto.Name = "grbCentroCusto";
            this.grbCentroCusto.Size = new System.Drawing.Size(562, 163);
            this.grbCentroCusto.TabIndex = 134;
            this.grbCentroCusto.TabStop = false;
            this.grbCentroCusto.Text = "CentroCusto";
            // 
            // dgvCentroCusto
            // 
            this.dgvCentroCusto.AllowUserToAddRows = false;
            this.dgvCentroCusto.AllowUserToDeleteRows = false;
            this.dgvCentroCusto.AllowUserToOrderColumns = true;
            this.dgvCentroCusto.AllowUserToResizeRows = false;
            this.dgvCentroCusto.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.DisplayedCells;
            this.dgvCentroCusto.BackgroundColor = System.Drawing.Color.White;
            this.dgvCentroCusto.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.LightGray;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvCentroCusto.DefaultCellStyle = dataGridViewCellStyle1;
            this.dgvCentroCusto.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvCentroCusto.Location = new System.Drawing.Point(3, 16);
            this.dgvCentroCusto.Name = "dgvCentroCusto";
            this.dgvCentroCusto.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvCentroCusto.Size = new System.Drawing.Size(556, 144);
            this.dgvCentroCusto.TabIndex = 126;
            this.dgvCentroCusto.TabStop = false;
            this.dgvCentroCusto.CellDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvCentroCusto_CellDoubleClick);
            // 
            // flpAcaoCentroCusto
            // 
            this.flpAcaoCentroCusto.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.flpAcaoCentroCusto.Controls.Add(this.btnIncluirCentroCusto);
            this.flpAcaoCentroCusto.Controls.Add(this.btnExcluirCentroCusto);
            this.flpAcaoCentroCusto.Enabled = false;
            this.flpAcaoCentroCusto.Location = new System.Drawing.Point(3, 1575);
            this.flpAcaoCentroCusto.Name = "flpAcaoCentroCusto";
            this.flpAcaoCentroCusto.Size = new System.Drawing.Size(562, 35);
            this.flpAcaoCentroCusto.TabIndex = 135;
            // 
            // btnIncluirCentroCusto
            // 
            this.btnIncluirCentroCusto.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnIncluirCentroCusto.Image = global::SWS.Properties.Resources.icone_mais;
            this.btnIncluirCentroCusto.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnIncluirCentroCusto.Location = new System.Drawing.Point(3, 3);
            this.btnIncluirCentroCusto.Name = "btnIncluirCentroCusto";
            this.btnIncluirCentroCusto.Size = new System.Drawing.Size(75, 23);
            this.btnIncluirCentroCusto.TabIndex = 35;
            this.btnIncluirCentroCusto.Text = "&Incluir";
            this.btnIncluirCentroCusto.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnIncluirCentroCusto.UseVisualStyleBackColor = true;
            this.btnIncluirCentroCusto.Click += new System.EventHandler(this.btnIncluirCentroCusto_Click);
            // 
            // btnExcluirCentroCusto
            // 
            this.btnExcluirCentroCusto.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnExcluirCentroCusto.Image = global::SWS.Properties.Resources.lixeira;
            this.btnExcluirCentroCusto.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnExcluirCentroCusto.Location = new System.Drawing.Point(84, 3);
            this.btnExcluirCentroCusto.Name = "btnExcluirCentroCusto";
            this.btnExcluirCentroCusto.Size = new System.Drawing.Size(75, 23);
            this.btnExcluirCentroCusto.TabIndex = 128;
            this.btnExcluirCentroCusto.TabStop = false;
            this.btnExcluirCentroCusto.Text = "&Excluir";
            this.btnExcluirCentroCusto.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnExcluirCentroCusto.UseVisualStyleBackColor = true;
            this.btnExcluirCentroCusto.Click += new System.EventHandler(this.btnExcluirCentroCusto_Click);
            // 
            // lblSituacaoAtendimento
            // 
            this.lblSituacaoAtendimento.BackColor = System.Drawing.Color.LightGray;
            this.lblSituacaoAtendimento.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblSituacaoAtendimento.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSituacaoAtendimento.Location = new System.Drawing.Point(3, 270);
            this.lblSituacaoAtendimento.Name = "lblSituacaoAtendimento";
            this.lblSituacaoAtendimento.ReadOnly = true;
            this.lblSituacaoAtendimento.Size = new System.Drawing.Size(200, 21);
            this.lblSituacaoAtendimento.TabIndex = 136;
            this.lblSituacaoAtendimento.TabStop = false;
            this.lblSituacaoAtendimento.Text = "Bloqueia Atendimento?";
            // 
            // cbBloqueado
            // 
            this.cbBloqueado.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.cbBloqueado.BackColor = System.Drawing.Color.LemonChiffon;
            this.cbBloqueado.BorderColor = System.Drawing.Color.DimGray;
            this.cbBloqueado.BorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid;
            this.cbBloqueado.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cbBloqueado.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbBloqueado.FormattingEnabled = true;
            this.cbBloqueado.Location = new System.Drawing.Point(202, 270);
            this.cbBloqueado.Name = "cbBloqueado";
            this.cbBloqueado.Size = new System.Drawing.Size(362, 21);
            this.cbBloqueado.TabIndex = 12;
            // 
            // lblCredenciadora
            // 
            this.lblCredenciadora.BackColor = System.Drawing.Color.LightGray;
            this.lblCredenciadora.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblCredenciadora.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCredenciadora.Location = new System.Drawing.Point(3, 1358);
            this.lblCredenciadora.Name = "lblCredenciadora";
            this.lblCredenciadora.ReadOnly = true;
            this.lblCredenciadora.Size = new System.Drawing.Size(200, 21);
            this.lblCredenciadora.TabIndex = 137;
            this.lblCredenciadora.TabStop = false;
            this.lblCredenciadora.Text = "Pode ser Credenciadora?";
            // 
            // cbCredenciadora
            // 
            this.cbCredenciadora.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.cbCredenciadora.BackColor = System.Drawing.Color.LightGray;
            this.cbCredenciadora.BorderColor = System.Drawing.Color.DimGray;
            this.cbCredenciadora.BorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid;
            this.cbCredenciadora.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cbCredenciadora.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbCredenciadora.FormattingEnabled = true;
            this.cbCredenciadora.Location = new System.Drawing.Point(202, 1358);
            this.cbCredenciadora.Name = "cbCredenciadora";
            this.cbCredenciadora.Size = new System.Drawing.Size(363, 21);
            this.cbCredenciadora.TabIndex = 35;
            // 
            // lblUsaPo
            // 
            this.lblUsaPo.BackColor = System.Drawing.Color.LightGray;
            this.lblUsaPo.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblUsaPo.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblUsaPo.Location = new System.Drawing.Point(3, 1378);
            this.lblUsaPo.Name = "lblUsaPo";
            this.lblUsaPo.ReadOnly = true;
            this.lblUsaPo.Size = new System.Drawing.Size(200, 21);
            this.lblUsaPo.TabIndex = 138;
            this.lblUsaPo.TabStop = false;
            this.lblUsaPo.Text = "Cliente usa PO?";
            // 
            // cbUsaPo
            // 
            this.cbUsaPo.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.cbUsaPo.BackColor = System.Drawing.Color.LightGray;
            this.cbUsaPo.BorderColor = System.Drawing.Color.DimGray;
            this.cbUsaPo.BorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid;
            this.cbUsaPo.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cbUsaPo.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbUsaPo.FormattingEnabled = true;
            this.cbUsaPo.Location = new System.Drawing.Point(202, 1378);
            this.cbUsaPo.Name = "cbUsaPo";
            this.cbUsaPo.Size = new System.Drawing.Size(363, 21);
            this.cbUsaPo.TabIndex = 36;
            // 
            // cbSimplesNacional
            // 
            this.cbSimplesNacional.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.cbSimplesNacional.BackColor = System.Drawing.Color.LightGray;
            this.cbSimplesNacional.BorderColor = System.Drawing.Color.DimGray;
            this.cbSimplesNacional.BorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid;
            this.cbSimplesNacional.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cbSimplesNacional.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbSimplesNacional.FormattingEnabled = true;
            this.cbSimplesNacional.Location = new System.Drawing.Point(202, 1298);
            this.cbSimplesNacional.Name = "cbSimplesNacional";
            this.cbSimplesNacional.Size = new System.Drawing.Size(363, 21);
            this.cbSimplesNacional.TabIndex = 139;
            this.cbSimplesNacional.Tag = "31";
            // 
            // frmClientesIncluir
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.ClientSize = new System.Drawing.Size(784, 561);
            this.Name = "frmClientesIncluir";
            this.Text = "INCLUIR CLIENTE";
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.frmClientesIncluir_KeyDown);
            this.spForm.Panel1.ResumeLayout(false);
            this.spForm.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.spForm)).EndInit();
            this.spForm.ResumeLayout(false);
            this.pnlForm.ResumeLayout(false);
            this.pnlForm.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.banner)).EndInit();
            this.flpAcao.ResumeLayout(false);
            this.tpEndereco.ResumeLayout(false);
            this.tpComercial.ResumeLayout(false);
            this.tpComercial.PerformLayout();
            this.tpCobranca.ResumeLayout(false);
            this.tpCobranca.PerformLayout();
            this.grbCnae.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvCnae)).EndInit();
            this.grbLogo.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pbLogo)).EndInit();
            this.grbContato.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvContatos)).EndInit();
            this.grbFuncao.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvFuncao)).EndInit();
            this.grbCentroCusto.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvCentroCusto)).EndInit();
            this.flpAcaoCentroCusto.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.FlowLayoutPanel flpAcao;
        protected System.Windows.Forms.Button btGravar;
        protected System.Windows.Forms.Button btImprimir;
        protected System.Windows.Forms.Button btLimpar;
        protected System.Windows.Forms.Button btFechar;
        protected System.Windows.Forms.Button btRamo;
        protected System.Windows.Forms.TextBox lblJornada;
        protected System.Windows.Forms.TextBox lblRamo;
        protected System.Windows.Forms.TextBox lblEscopo;
        protected System.Windows.Forms.TextBox textRamo;
        protected System.Windows.Forms.TextBox textJornada;
        protected System.Windows.Forms.TextBox textEscopo;
        protected System.Windows.Forms.TextBox textSite;
        protected System.Windows.Forms.TextBox lblSite;
        protected System.Windows.Forms.TextBox lblEmail;
        protected System.Windows.Forms.TabControl tpEndereco;
        protected System.Windows.Forms.TabPage tpComercial;
        protected System.Windows.Forms.TextBox textFaxComercial;
        protected System.Windows.Forms.TextBox lblFaxComercial;
        protected System.Windows.Forms.TextBox lblTelefoneComercial;
        protected System.Windows.Forms.TextBox textTelefoneComercial;
        protected System.Windows.Forms.TextBox lblCidade;
        protected System.Windows.Forms.Button btCidadeComercial;
        protected System.Windows.Forms.TextBox textCidadeComercial;
        protected System.Windows.Forms.MaskedTextBox textCEPComercial;
        protected System.Windows.Forms.TextBox lblUF;
        protected System.Windows.Forms.TextBox lblCEP;
        protected System.Windows.Forms.TextBox lblBairroComercial;
        protected System.Windows.Forms.TextBox lblComplementoComercial;
        protected System.Windows.Forms.TextBox lblNumeroComercial;
        protected System.Windows.Forms.TextBox lblEnderecoComercial;
        protected System.Windows.Forms.TextBox textLogradouroComercial;
        protected System.Windows.Forms.TextBox textNumeroComercial;
        protected System.Windows.Forms.TextBox textComplementoComercial;
        protected System.Windows.Forms.TextBox textBairroComercial;
        protected ComboBoxWithBorder cbUFComercial;
        protected System.Windows.Forms.TabPage tpCobranca;
        protected System.Windows.Forms.Button btCopiarComercial;
        protected System.Windows.Forms.Button btCidadeCobranca;
        protected System.Windows.Forms.TextBox textFaxCobranca;
        protected System.Windows.Forms.TextBox lblFaxCobranca;
        protected System.Windows.Forms.TextBox lblTelefoneCobranca;
        protected System.Windows.Forms.TextBox textTelefoneCobranca;
        protected System.Windows.Forms.TextBox lblCidadeCobranca;
        protected System.Windows.Forms.TextBox textCidadeCobranca;
        protected System.Windows.Forms.MaskedTextBox textCEPCobranca;
        protected System.Windows.Forms.TextBox lblUFCobranca;
        protected System.Windows.Forms.TextBox lblCEPCobranca;
        protected System.Windows.Forms.TextBox lblBairroCobranca;
        protected System.Windows.Forms.TextBox lblComplementoCobranca;
        protected System.Windows.Forms.TextBox lblNumeroCobranca;
        protected System.Windows.Forms.TextBox lblEnderecoCobranca;
        protected System.Windows.Forms.TextBox textLogradouroCobranca;
        protected System.Windows.Forms.TextBox textNumeroCobranca;
        protected System.Windows.Forms.TextBox textComplementoCobranca;
        protected System.Windows.Forms.TextBox textBairroCobranca;
        protected ComboBoxWithBorder cbUFCobranca;
        protected System.Windows.Forms.TextBox textEmail;
        protected System.Windows.Forms.TextBox textInscricao;
        protected System.Windows.Forms.TextBox lblInscricao;
        protected System.Windows.Forms.TextBox lblNomeFantasia;
        protected System.Windows.Forms.TextBox textFantasia;
        protected System.Windows.Forms.TextBox lblCNPJ;
        protected System.Windows.Forms.TextBox textRazaoSocial;
        protected System.Windows.Forms.Button btCredenciada;
        protected System.Windows.Forms.MaskedTextBox textCNPJ;
        protected System.Windows.Forms.TextBox textCredenciada;
        protected System.Windows.Forms.Button btMatriz;
        protected System.Windows.Forms.TextBox textMatriz;
        protected System.Windows.Forms.Button btVendedor;
        protected System.Windows.Forms.TextBox textVendedor;
        protected System.Windows.Forms.TextBox lblMatriz;
        protected System.Windows.Forms.TextBox lblCredenciada;
        protected System.Windows.Forms.TextBox lblVendedor;
        protected System.Windows.Forms.TextBox lblRazaoSocial;
        protected System.Windows.Forms.TextBox lbltipoCliente;
        protected System.Windows.Forms.Label lblCnaePrincipal;
        protected System.Windows.Forms.GroupBox grbCnae;
        protected System.Windows.Forms.DataGridView dgvCnae;
        protected System.Windows.Forms.Button btExcluirCnae;
        protected System.Windows.Forms.Button btIncluirCnae;
        protected System.Windows.Forms.Button btExcluirLogo;
        protected System.Windows.Forms.GroupBox grbLogo;
        protected System.Windows.Forms.PictureBox pbLogo;
        protected System.Windows.Forms.Button btIncluirLogo;
        protected System.Windows.Forms.Label lblContato;
        protected System.Windows.Forms.Button btExcluirContato;
        protected System.Windows.Forms.Button btIncluirContato;
        protected System.Windows.Forms.GroupBox grbContato;
        protected System.Windows.Forms.DataGridView dgvContatos;
        protected System.Windows.Forms.Button btExcluirFuncao;
        protected System.Windows.Forms.GroupBox grbFuncao;
        protected System.Windows.Forms.DataGridView dgvFuncao;
        protected System.Windows.Forms.Button btIncluirFuncao;
        protected System.Windows.Forms.TextBox lblSimplesNacional;
        protected System.Windows.Forms.TextBox lblValorLiquido;
        protected ComboBoxWithBorder cbValorLiquido;
        protected System.Windows.Forms.TextBox lblPrestador;
        protected ComboBoxWithBorder cbPrestador;
        protected System.Windows.Forms.TextBox lblClienteParticular;
        protected ComboBoxWithBorder cbParticular;
        protected System.Windows.Forms.TextBox lblClienteContrato;
        protected ComboBoxWithBorder cbContrato;
        protected System.Windows.Forms.TextBox lblClienteVIP;
        protected ComboBoxWithBorder cbClienteVIP;
        protected System.Windows.Forms.ToolTip tpCliente;
        protected System.Windows.Forms.TextBox lblCodigoCnes;
        protected System.Windows.Forms.TextBox textCodigoCnes;
        protected ComboBoxWithBorder cbCentroCusto;
        protected System.Windows.Forms.TextBox lblCentroCusto;
        protected ComboBoxWithBorder cbBloqueado;
        protected System.Windows.Forms.TextBox lblSituacaoAtendimento;
        protected System.Windows.Forms.FlowLayoutPanel flpAcaoCentroCusto;
        protected System.Windows.Forms.Button btnIncluirCentroCusto;
        protected System.Windows.Forms.Button btnExcluirCentroCusto;
        protected System.Windows.Forms.GroupBox grbCentroCusto;
        protected System.Windows.Forms.DataGridView dgvCentroCusto;
        protected System.Windows.Forms.TextBox lblCredenciadora;
        protected ComboBoxWithBorder cbCredenciadora;
        protected ComboBoxWithBorder cbTipoCliente;
        protected ComboBoxWithBorder cbUsaPo;
        protected System.Windows.Forms.TextBox lblUsaPo;
        protected ComboBoxWithBorder cbSimplesNacional;
    }
}
