﻿using SWS.Entidade;
using SWS.Facade;
using SWS.View.Resources;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SWS.View
{
    public partial class frmFonteSelecionar : frmTemplateConsulta
    {
        Fonte fonte;

        public Fonte Fonte
        {
            get { return fonte; }
            set { fonte = value; }
        }
        
        public frmFonteSelecionar()
        {
            InitializeComponent();
            ActiveControl = textFonte;
            validaPermissao();
        }

        private void btnGravar_Click(object sender, EventArgs e)
        {
            try
            {
                if (dgvFonte.CurrentRow == null)
                    throw new Exception("Selecione uma fonte.");

                EstudoFacade pcmsoFacade = EstudoFacade.getInstance();
                Fonte = pcmsoFacade.findFonteById((long)dgvFonte.CurrentRow.Cells[0].Value);
                this.Close();

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }

        private void btnBuscar_Click(object sender, EventArgs e)
        {
            montaGrid();
        }

        private void btnNovo_Click(object sender, EventArgs e)
        {
            frmFonteIncluir incluirNovaFonte = new frmFonteIncluir();
            incluirNovaFonte.ShowDialog();

            if (incluirNovaFonte.Fonte != null)
            {
                Fonte = incluirNovaFonte.Fonte;
                this.Close();
            }
        }

        private void btnFechar_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void validaPermissao()
        {
            PermissionamentoFacade permissionamentoFacade = PermissionamentoFacade.getInstance();
            btnNovo.Enabled = permissionamentoFacade.hasPermission("FONTE", "INCLUIR");
        }

        private void montaGrid()
        {
            try
            {
                dgvFonte.ColumnCount = 2;

                EstudoFacade pcmsoFacade = EstudoFacade.getInstance();

                Fonte fonte = new Fonte();
                fonte.Descricao = textFonte.Text.Trim();
                fonte.Situacao = ApplicationConstants.ATIVO;
                fonte.Privado = false;

                this.Cursor = Cursors.WaitCursor;
                DataSet ds = pcmsoFacade.findFonteByFilter(fonte);

                dgvFonte.Columns[0].HeaderText = "idFonte";
                dgvFonte.Columns[0].Visible = false;
                dgvFonte.Columns[1].HeaderText = "Fonte";

                foreach (DataRow row in ds.Tables[0].Rows)
                    dgvFonte.Rows.Add(
                        row["ID_FONTE"],
                        row["DESCRICAO"].ToString(),
                        false);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
            
        }

    }
}
