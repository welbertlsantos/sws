﻿namespace SWS.View
{
    partial class frmEpiPrincipal
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            this.flpAcao = new System.Windows.Forms.FlowLayoutPanel();
            this.bt_incluir = new System.Windows.Forms.Button();
            this.bt_alterar = new System.Windows.Forms.Button();
            this.bt_detalhar = new System.Windows.Forms.Button();
            this.bt_excluir = new System.Windows.Forms.Button();
            this.btn_reativar = new System.Windows.Forms.Button();
            this.bt_limpar = new System.Windows.Forms.Button();
            this.bt_pesquisar = new System.Windows.Forms.Button();
            this.bt_fechar = new System.Windows.Forms.Button();
            this.lblEpi = new System.Windows.Forms.TextBox();
            this.lblCA = new System.Windows.Forms.TextBox();
            this.lblSituacao = new System.Windows.Forms.TextBox();
            this.text_ca = new System.Windows.Forms.TextBox();
            this.text_descricao = new System.Windows.Forms.TextBox();
            this.cbSituacao = new SWS.ComboBoxWithBorder();
            this.grb_gride = new System.Windows.Forms.GroupBox();
            this.grd_epi = new System.Windows.Forms.DataGridView();
            this.lblCorInativo = new System.Windows.Forms.Label();
            this.lblLegendaInativo = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.spForm)).BeginInit();
            this.spForm.Panel1.SuspendLayout();
            this.spForm.Panel2.SuspendLayout();
            this.spForm.SuspendLayout();
            this.pnlForm.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.banner)).BeginInit();
            this.flpAcao.SuspendLayout();
            this.grb_gride.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grd_epi)).BeginInit();
            this.SuspendLayout();
            // 
            // spForm
            // 
            // 
            // spForm.Panel1
            // 
            this.spForm.Panel1.Controls.Add(this.flpAcao);
            // 
            // pnlForm
            // 
            this.pnlForm.Controls.Add(this.lblLegendaInativo);
            this.pnlForm.Controls.Add(this.lblCorInativo);
            this.pnlForm.Controls.Add(this.grb_gride);
            this.pnlForm.Controls.Add(this.cbSituacao);
            this.pnlForm.Controls.Add(this.text_ca);
            this.pnlForm.Controls.Add(this.text_descricao);
            this.pnlForm.Controls.Add(this.lblSituacao);
            this.pnlForm.Controls.Add(this.lblCA);
            this.pnlForm.Controls.Add(this.lblEpi);
            // 
            // flpAcao
            // 
            this.flpAcao.Controls.Add(this.bt_incluir);
            this.flpAcao.Controls.Add(this.bt_alterar);
            this.flpAcao.Controls.Add(this.bt_detalhar);
            this.flpAcao.Controls.Add(this.bt_excluir);
            this.flpAcao.Controls.Add(this.btn_reativar);
            this.flpAcao.Controls.Add(this.bt_limpar);
            this.flpAcao.Controls.Add(this.bt_pesquisar);
            this.flpAcao.Controls.Add(this.bt_fechar);
            this.flpAcao.Dock = System.Windows.Forms.DockStyle.Fill;
            this.flpAcao.Location = new System.Drawing.Point(0, 0);
            this.flpAcao.Name = "flpAcao";
            this.flpAcao.Size = new System.Drawing.Size(784, 35);
            this.flpAcao.TabIndex = 0;
            // 
            // bt_incluir
            // 
            this.bt_incluir.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.bt_incluir.Image = global::SWS.Properties.Resources.icone_mais;
            this.bt_incluir.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.bt_incluir.Location = new System.Drawing.Point(3, 3);
            this.bt_incluir.Name = "bt_incluir";
            this.bt_incluir.Size = new System.Drawing.Size(71, 22);
            this.bt_incluir.TabIndex = 16;
            this.bt_incluir.Text = "&Incluir";
            this.bt_incluir.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.bt_incluir.UseVisualStyleBackColor = true;
            this.bt_incluir.Click += new System.EventHandler(this.bt_incluir_Click);
            // 
            // bt_alterar
            // 
            this.bt_alterar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.bt_alterar.Image = global::SWS.Properties.Resources.Alterar;
            this.bt_alterar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.bt_alterar.Location = new System.Drawing.Point(80, 3);
            this.bt_alterar.Name = "bt_alterar";
            this.bt_alterar.Size = new System.Drawing.Size(71, 22);
            this.bt_alterar.TabIndex = 17;
            this.bt_alterar.Text = "&Alterar";
            this.bt_alterar.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.bt_alterar.UseVisualStyleBackColor = true;
            this.bt_alterar.Click += new System.EventHandler(this.bt_alterar_Click);
            // 
            // bt_detalhar
            // 
            this.bt_detalhar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.bt_detalhar.Image = global::SWS.Properties.Resources.lupa;
            this.bt_detalhar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.bt_detalhar.Location = new System.Drawing.Point(157, 3);
            this.bt_detalhar.Name = "bt_detalhar";
            this.bt_detalhar.Size = new System.Drawing.Size(71, 22);
            this.bt_detalhar.TabIndex = 19;
            this.bt_detalhar.Text = "&Detalhar";
            this.bt_detalhar.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.bt_detalhar.UseVisualStyleBackColor = true;
            this.bt_detalhar.Click += new System.EventHandler(this.bt_detalhar_Click);
            // 
            // bt_excluir
            // 
            this.bt_excluir.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.bt_excluir.Image = global::SWS.Properties.Resources.lixeira;
            this.bt_excluir.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.bt_excluir.Location = new System.Drawing.Point(234, 3);
            this.bt_excluir.Name = "bt_excluir";
            this.bt_excluir.Size = new System.Drawing.Size(71, 22);
            this.bt_excluir.TabIndex = 18;
            this.bt_excluir.Text = "&Excluir";
            this.bt_excluir.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.bt_excluir.UseVisualStyleBackColor = true;
            this.bt_excluir.Click += new System.EventHandler(this.bt_excluir_Click);
            // 
            // btn_reativar
            // 
            this.btn_reativar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_reativar.Image = global::SWS.Properties.Resources.devolucao;
            this.btn_reativar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btn_reativar.Location = new System.Drawing.Point(311, 3);
            this.btn_reativar.Name = "btn_reativar";
            this.btn_reativar.Size = new System.Drawing.Size(71, 22);
            this.btn_reativar.TabIndex = 21;
            this.btn_reativar.Text = "&Reativar";
            this.btn_reativar.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btn_reativar.UseVisualStyleBackColor = true;
            this.btn_reativar.Click += new System.EventHandler(this.btn_reativar_Click);
            // 
            // bt_limpar
            // 
            this.bt_limpar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.bt_limpar.Image = global::SWS.Properties.Resources.vassoura;
            this.bt_limpar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.bt_limpar.Location = new System.Drawing.Point(388, 3);
            this.bt_limpar.Name = "bt_limpar";
            this.bt_limpar.Size = new System.Drawing.Size(71, 22);
            this.bt_limpar.TabIndex = 15;
            this.bt_limpar.Text = "Limpar";
            this.bt_limpar.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.bt_limpar.UseVisualStyleBackColor = true;
            this.bt_limpar.Click += new System.EventHandler(this.bt_limpar_Click);
            // 
            // bt_pesquisar
            // 
            this.bt_pesquisar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.bt_pesquisar.Image = global::SWS.Properties.Resources.lupa;
            this.bt_pesquisar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.bt_pesquisar.Location = new System.Drawing.Point(465, 3);
            this.bt_pesquisar.Name = "bt_pesquisar";
            this.bt_pesquisar.Size = new System.Drawing.Size(71, 22);
            this.bt_pesquisar.TabIndex = 14;
            this.bt_pesquisar.Text = "Pesquisar";
            this.bt_pesquisar.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.bt_pesquisar.UseVisualStyleBackColor = true;
            this.bt_pesquisar.Click += new System.EventHandler(this.bt_pesquisar_Click);
            // 
            // bt_fechar
            // 
            this.bt_fechar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.bt_fechar.Image = global::SWS.Properties.Resources.close;
            this.bt_fechar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.bt_fechar.Location = new System.Drawing.Point(542, 3);
            this.bt_fechar.Name = "bt_fechar";
            this.bt_fechar.Size = new System.Drawing.Size(71, 22);
            this.bt_fechar.TabIndex = 20;
            this.bt_fechar.Text = "&Fechar";
            this.bt_fechar.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.bt_fechar.UseVisualStyleBackColor = true;
            this.bt_fechar.Click += new System.EventHandler(this.bt_fechar_Click);
            // 
            // lblEpi
            // 
            this.lblEpi.BackColor = System.Drawing.Color.LightGray;
            this.lblEpi.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblEpi.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblEpi.Location = new System.Drawing.Point(11, 6);
            this.lblEpi.Name = "lblEpi";
            this.lblEpi.ReadOnly = true;
            this.lblEpi.Size = new System.Drawing.Size(100, 21);
            this.lblEpi.TabIndex = 0;
            this.lblEpi.TabStop = false;
            this.lblEpi.Text = "EPI";
            // 
            // lblCA
            // 
            this.lblCA.BackColor = System.Drawing.Color.LightGray;
            this.lblCA.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblCA.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCA.Location = new System.Drawing.Point(11, 26);
            this.lblCA.Name = "lblCA";
            this.lblCA.ReadOnly = true;
            this.lblCA.Size = new System.Drawing.Size(100, 21);
            this.lblCA.TabIndex = 1;
            this.lblCA.TabStop = false;
            this.lblCA.Text = "CA";
            // 
            // lblSituacao
            // 
            this.lblSituacao.BackColor = System.Drawing.Color.LightGray;
            this.lblSituacao.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblSituacao.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSituacao.Location = new System.Drawing.Point(11, 46);
            this.lblSituacao.Name = "lblSituacao";
            this.lblSituacao.ReadOnly = true;
            this.lblSituacao.Size = new System.Drawing.Size(100, 21);
            this.lblSituacao.TabIndex = 2;
            this.lblSituacao.TabStop = false;
            this.lblSituacao.Text = "Situação";
            // 
            // text_ca
            // 
            this.text_ca.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.text_ca.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.text_ca.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.text_ca.Location = new System.Drawing.Point(110, 26);
            this.text_ca.MaxLength = 50;
            this.text_ca.Name = "text_ca";
            this.text_ca.Size = new System.Drawing.Size(634, 21);
            this.text_ca.TabIndex = 4;
            // 
            // text_descricao
            // 
            this.text_descricao.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.text_descricao.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.text_descricao.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.text_descricao.Location = new System.Drawing.Point(110, 6);
            this.text_descricao.MaxLength = 100;
            this.text_descricao.Name = "text_descricao";
            this.text_descricao.Size = new System.Drawing.Size(634, 21);
            this.text_descricao.TabIndex = 3;
            // 
            // cbSituacao
            // 
            this.cbSituacao.BackColor = System.Drawing.Color.LightGray;
            this.cbSituacao.BorderColor = System.Drawing.Color.DimGray;
            this.cbSituacao.BorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid;
            this.cbSituacao.FormattingEnabled = true;
            this.cbSituacao.Location = new System.Drawing.Point(110, 46);
            this.cbSituacao.Name = "cbSituacao";
            this.cbSituacao.Size = new System.Drawing.Size(634, 21);
            this.cbSituacao.TabIndex = 5;
            // 
            // grb_gride
            // 
            this.grb_gride.BackColor = System.Drawing.Color.White;
            this.grb_gride.Controls.Add(this.grd_epi);
            this.grb_gride.Location = new System.Drawing.Point(12, 73);
            this.grb_gride.Name = "grb_gride";
            this.grb_gride.Size = new System.Drawing.Size(734, 361);
            this.grb_gride.TabIndex = 32;
            this.grb_gride.TabStop = false;
            this.grb_gride.Text = "EPI";
            // 
            // grd_epi
            // 
            this.grd_epi.AllowUserToAddRows = false;
            this.grd_epi.AllowUserToDeleteRows = false;
            this.grd_epi.AllowUserToOrderColumns = true;
            this.grd_epi.AllowUserToResizeRows = false;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.Gainsboro;
            this.grd_epi.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle1;
            this.grd_epi.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this.grd_epi.BackgroundColor = System.Drawing.Color.White;
            this.grd_epi.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.Color.LightGray;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.grd_epi.DefaultCellStyle = dataGridViewCellStyle2;
            this.grd_epi.Dock = System.Windows.Forms.DockStyle.Fill;
            this.grd_epi.Location = new System.Drawing.Point(3, 16);
            this.grd_epi.MultiSelect = false;
            this.grd_epi.Name = "grd_epi";
            this.grd_epi.ReadOnly = true;
            this.grd_epi.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.grd_epi.Size = new System.Drawing.Size(728, 342);
            this.grd_epi.TabIndex = 9;
            this.grd_epi.CellMouseDoubleClick += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.grd_epi_CellMouseDoubleClick);
            this.grd_epi.RowPrePaint += new System.Windows.Forms.DataGridViewRowPrePaintEventHandler(this.grd_epi_RowPrePaint);
            // 
            // lblCorInativo
            // 
            this.lblCorInativo.AutoSize = true;
            this.lblCorInativo.BackColor = System.Drawing.Color.Red;
            this.lblCorInativo.Location = new System.Drawing.Point(15, 438);
            this.lblCorInativo.Name = "lblCorInativo";
            this.lblCorInativo.Size = new System.Drawing.Size(13, 13);
            this.lblCorInativo.TabIndex = 33;
            this.lblCorInativo.Text = "  ";
            // 
            // lblLegendaInativo
            // 
            this.lblLegendaInativo.AutoSize = true;
            this.lblLegendaInativo.Location = new System.Drawing.Point(34, 438);
            this.lblLegendaInativo.Name = "lblLegendaInativo";
            this.lblLegendaInativo.Size = new System.Drawing.Size(79, 13);
            this.lblLegendaInativo.TabIndex = 34;
            this.lblLegendaInativo.Text = "EPI desativado";
            // 
            // frmEpiPrincipal
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(784, 561);
            this.Name = "frmEpiPrincipal";
            this.Text = "GERENCIAR EPI";
            this.spForm.Panel1.ResumeLayout(false);
            this.spForm.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.spForm)).EndInit();
            this.spForm.ResumeLayout(false);
            this.pnlForm.ResumeLayout(false);
            this.pnlForm.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.banner)).EndInit();
            this.flpAcao.ResumeLayout(false);
            this.grb_gride.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.grd_epi)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.FlowLayoutPanel flpAcao;
        private System.Windows.Forms.TextBox lblEpi;
        private System.Windows.Forms.TextBox lblCA;
        private System.Windows.Forms.TextBox lblSituacao;
        private System.Windows.Forms.Button bt_limpar;
        private System.Windows.Forms.Button btn_reativar;
        private System.Windows.Forms.Button bt_fechar;
        private System.Windows.Forms.Button bt_detalhar;
        private System.Windows.Forms.Button bt_excluir;
        private System.Windows.Forms.Button bt_alterar;
        private System.Windows.Forms.Button bt_pesquisar;
        private System.Windows.Forms.Button bt_incluir;
        private ComboBoxWithBorder cbSituacao;
        private System.Windows.Forms.TextBox text_ca;
        private System.Windows.Forms.TextBox text_descricao;
        private System.Windows.Forms.GroupBox grb_gride;
        private System.Windows.Forms.DataGridView grd_epi;
        private System.Windows.Forms.Label lblLegendaInativo;
        private System.Windows.Forms.Label lblCorInativo;
    }
}