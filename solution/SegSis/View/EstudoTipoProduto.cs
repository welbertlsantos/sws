﻿using SWS.Entidade;
using SWS.Facade;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace SWS.View
{
    public partial class frmEstudoTipoProduto : frmTemplateConsulta
    {
        private string tipoEstudo;
        
        private Produto produtoSelecionado;

        public Produto Produto
        {
            get { return produtoSelecionado; }
            set { produtoSelecionado = value; }
        }

        public frmEstudoTipoProduto(string tipoEstudo)
        {
            InitializeComponent();
            this.tipoEstudo = tipoEstudo;
            montaDataGrid();
        }

        public void montaDataGrid()
        {
            try
            {
                FinanceiroFacade financeiroFacade = FinanceiroFacade.getInstance();

                DataSet ds = financeiroFacade.findProdutoByTipoEstudo(tipoEstudo);

                this.dgvProduto.DataSource = ds.Tables["Produtos"].DefaultView;

                dgvProduto.Columns[0].HeaderText = "Id_produto";
                dgvProduto.Columns[0].Visible = false;
                dgvProduto.Columns[1].HeaderText = "Descrição";
                dgvProduto.Columns[2].HeaderText = "Situação";
                dgvProduto.Columns[2].Visible = false;
                dgvProduto.Columns[3].HeaderText = "Preço de Venda";
                dgvProduto.Columns[3].Visible = false;
                dgvProduto.Columns[4].HeaderText = "FlagCarga";
                dgvProduto.Columns[4].Visible = false;
                dgvProduto.Columns[5].HeaderText = "Preço de Custo";
                dgvProduto.Columns[5].Visible = false;
                dgvProduto.Columns[6].HeaderText = "Tipo de Estudo";
                dgvProduto.Columns[6].Visible = false;

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btn_fechar_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btn_confirma_Click(object sender, EventArgs e)
        {
            try
            {
                if (dgvProduto.CurrentRow == null)
                    throw new Exception("Selecione um produto.");

                this.Cursor = Cursors.WaitCursor;

                FinanceiroFacade financeiroFacede = FinanceiroFacade.getInstance();

                Produto = financeiroFacede.findProdutoById((long)dgvProduto.CurrentRow.Cells[0].Value);
                this.Close();

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }

        }
    }
}
