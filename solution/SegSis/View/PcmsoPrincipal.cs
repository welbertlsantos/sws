﻿using SWS.Entidade;
using SWS.Facade;
using SWS.View.Resources;
using SWS.View.ViewHelper;
using SWS.ViewHelper;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SWS.View
{
    public partial class frmPcmsoPrincipal : frmTemplate
    {

        Cliente cliente;

        Usuario elaborador;
        Usuario responsavel;

        public DateTime dtFinalPesquisa;

        public String comentario;

        private Estudo pcmso;

        public Estudo Pcmso
        {
            get { return pcmso; }
            set { pcmso = value; }
        }
        
        public frmPcmsoPrincipal()
        {
            InitializeComponent();
            validaPermissoes();
            ComboHelper.situacaoEstudo(cbSituacao);
        }

        private void validaPermissoes()
        {
            PermissionamentoFacade permissionamentoFacade = PermissionamentoFacade.getInstance();

            btnIncluir.Enabled = permissionamentoFacade.hasPermission("PCMSO", "INCLUIR");
            btnAlterar.Enabled = permissionamentoFacade.hasPermission("PCMSO", "ALTERAR");
            btnExcluir.Enabled = permissionamentoFacade.hasPermission("PCMSO", "EXCLUIR");
            btnImprimir.Enabled = permissionamentoFacade.hasPermission("PCMSO", "IMPRIMIR");
            btnEnviar.Enabled = permissionamentoFacade.hasPermission("PCMSO", "ENVIAR");
            btnFinalizar.Enabled = permissionamentoFacade.hasPermission("PCMSO", "FINALIZAR");
            btnRevisionar.Enabled = permissionamentoFacade.hasPermission("PCMSO", "REVISAR");
            btnCorrigir.Enabled = permissionamentoFacade.hasPermission("PCMSO", "CORRIGIR");
            btnReativar.Enabled = permissionamentoFacade.hasPermission("PCMSO", "REATIVAR");
            btnConverter.Enabled = permissionamentoFacade.hasPermission("PCMSO", "CONVERTER");
            btnReplicar.Enabled = permissionamentoFacade.hasPermission("PCMSO", "REPLICAR");

        }

        private void btnFechar_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnPesquisar_Click(object sender, EventArgs e)
        {
            try
            {
                if (!validaDadosTela())
                    montaDataGrid();

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }

        private bool validaDadosTela()
        {
            bool retorno = false;
            try
            {
                if (Convert.ToDateTime(dataInicial.Text) > Convert.ToDateTime(dataFinal.Text))
                    throw new Exception("A data inicial deve ser menor que a data final. Reveja as informações digitadas");

            }
            catch (Exception ex)
            {
                retorno = true;
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            return retorno;

        }

        private void montaDataGrid()
        {
            try
            {
                EstudoFacade pcmsoFacade = EstudoFacade.getInstance();

                this.Cursor = Cursors.WaitCursor;
                dgvPcmso.Columns.Clear();

                pcmso = new Estudo();
                pcmso.CodigoEstudo = textCodigo.Text;
                pcmso.VendedorCliente = cliente != null ? new VendedorCliente(null, cliente, null, String.Empty) : null;
                pcmso.Engenheiro = responsavel;
                pcmso.Tecno = elaborador;
                pcmso.DataCriacao = dataInicial.Checked ? Convert.ToDateTime(dataInicial.Text) : (DateTime?)null;
                pcmso.Situacao = ((SelectItem)cbSituacao.SelectedItem).Valor;
                pcmso.TipoEstudo = true;

                DataSet ds = pcmsoFacade.findByFilter(pcmso, Convert.ToDateTime(dataFinal.Text));

                dgvPcmso.ColumnCount = 12;

                dgvPcmso.Columns[0].HeaderText = "idEstudo";
                dgvPcmso.Columns[0].Visible = false;
                dgvPcmso.Columns[1].HeaderText = "Código PCMSO";
                dgvPcmso.Columns[2].HeaderText = "Data da Criação";
                dgvPcmso.Columns[3].HeaderText = "Data de Vencimento";
                dgvPcmso.Columns[4].HeaderText = "Data de Fechamento";
                dgvPcmso.Columns[5].HeaderText = "Situação";
                dgvPcmso.Columns[6].HeaderText = "Cliente";
                dgvPcmso.Columns[6].DisplayIndex = 1;
                dgvPcmso.Columns[7].HeaderText = "Contratante";
                dgvPcmso.Columns[8].HeaderText = "Obra";
                dgvPcmso.Columns[9].HeaderText = "CNPJ Cliente";
                dgvPcmso.Columns[9].DisplayIndex = 7;
                dgvPcmso.Columns[10].HeaderText = "CNPJ Contratante";
                dgvPcmso.Columns[10].DisplayIndex = 8;
                dgvPcmso.Columns[11].HeaderText = "Avulso";
                dgvPcmso.Columns[11].Visible = false;

                foreach (DataRow dvRow in ds.Tables[0].Rows)
                {
                    dgvPcmso.Rows.Add(
                        dvRow["id_estudo"],
                        dvRow["codigo_estudo"].ToString(),
                        String.Format("{0:dd/MM/yyyy}", dvRow["data_criacao"]),
                        String.Format("{0:dd/MM/yyyy}", dvRow["data_vencimento"]),
                        String.Format("{0:dd/MM/yyyy}", dvRow["data_fechamento"]),
                        dvRow["situacao"].ToString(),
                        dvRow["cliente"].ToString(),
                        dvRow["contratante"].ToString(),
                        dvRow["obra"].ToString(),
                        dvRow["cnpj_cliente"].ToString(),
                        dvRow["cnpj_contratante"].ToString(),
                        dvRow["avulso"]);
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }

        }

        private void btnIncluir_Click(object sender, EventArgs e)
        {
            frmPcmsoIncluirTipo selecionarTipoPcmso = new frmPcmsoIncluirTipo();
            selecionarTipoPcmso.ShowDialog();

            if (selecionarTipoPcmso.Pcmso != null)
            {
                textCodigo.Text = selecionarTipoPcmso.Pcmso.CodigoEstudo;
                montaDataGrid();
            }
            
        }

        private void btnAlterar_Click(object sender, EventArgs e)
        {
            try
            {
                if (dgvPcmso.CurrentRow == null)
                    throw new Exception("Selecione um PCMSO para alterar");

                EstudoFacade pcmsoFacade = EstudoFacade.getInstance();

                /* verificando o tipo de PCMSO que será alterado */

                Pcmso = pcmsoFacade.findById((long)dgvPcmso.CurrentRow.Cells[0].Value);

                if (pcmso.Avulso)
                {
                    if (!string.Equals(pcmso.Situacao, ApplicationConstants.FECHADO))
                        throw new Exception("Pcmso encontra-se INATIVO. Somente PCMSO ativo pode ser alterado. Reative o pcmso caso precise realizar uma alteração");
                    PcmsoAvulsoAlterar alterarPcmsoAvulso = new PcmsoAvulsoAlterar(pcmso);
                    alterarPcmsoAvulso.ShowDialog();
                    textCodigo.Text = pcmso.CodigoEstudo;
                    montaDataGrid();
                }
                else
                {
                    /* pcmso completo. Irá seguir com as regras do pcmso */
                    if (!string.Equals(pcmso.Situacao, ApplicationConstants.CONSTRUCAO))
                        throw new Exception("É permitido alterar somente um PCMSO que esteja em contrução");

                    frmPcmsoAlterar alterarPcmso = new frmPcmsoAlterar(Pcmso);
                    alterarPcmso.ShowDialog();

                    textCodigo.Text = Pcmso.CodigoEstudo;
                    montaDataGrid();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }

        private void btnCorrigir_Click(object sender, EventArgs e)
        {
            try
            {
                if (dgvPcmso.CurrentRow == null)
                    throw new Exception("Selecione uma linha");

                EstudoFacade pcmsoFacade = EstudoFacade.getInstance();

                this.Pcmso = pcmsoFacade.findById((long)dgvPcmso.CurrentRow.Cells[0].Value);

                if (Convert.ToBoolean(pcmso.Avulso))
                    throw new Exception("PCMSO criado como avulso não podem ser corrigido");

                if (!string.Equals(Pcmso.Situacao, ApplicationConstants.FECHADO))
                    throw new Exception("Somente estudos finalizados podem ser corrigidos");

                if (MessageBox.Show("Deseja iniciar a correção do estudo?", "Confirmaçao", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                {
                    frmPcmsoCorrecao correcao = new frmPcmsoCorrecao(pcmso);
                    correcao.ShowDialog();

                    if (!string.IsNullOrEmpty(correcao.Historico))
                    {
                        /* iniciando a correção */

                        LogFacade logFacade = LogFacade.getInstance();
                        CorrecaoEstudo correcaoEstudo = new CorrecaoEstudo(
                            null,
                            PermissionamentoFacade.usuarioAutenticado,
                            pcmso,
                            correcao.Historico,
                            DateTime.Now);

                        logFacade.insertCorrecaoEstudo(correcaoEstudo);
                        frmPcmsoAlterar alterarPcmso = new frmPcmsoAlterar(Pcmso);
                        alterarPcmso.ShowDialog();
                        textCodigo.Text = Pcmso.CodigoEstudo;
                        montaDataGrid();
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btnRevisionar_Click(object sender, EventArgs e)
        {
            try
            {
                if (dgvPcmso.CurrentRow == null)
                    throw new Exception("Selecione uma linha.");

                EstudoFacade pcmsoFacade = EstudoFacade.getInstance();
                Pcmso = pcmsoFacade.findById((long)dgvPcmso.CurrentRow.Cells[0].Value);

                if (Convert.ToBoolean(Pcmso.Avulso))
                    throw new Exception("Pcmso Avulso não é permitido iniciar uma revisão.");

                if (!string.Equals(Pcmso.Situacao, ApplicationConstants.FECHADO))
                    throw new Exception("Somente é possível criar revisão de um PCMSO finalizado!");

                if (!pcmsoFacade.verificaPodeReplicarByRevisao((long)pcmso.Id))
                    throw new Exception("Este PCMSO não é a última versão. Só é possível criar revisão da última versão!");

                frmPcmsoComentarioRevisao incluirComentarioRevisao = new frmPcmsoComentarioRevisao();
                incluirComentarioRevisao.ShowDialog();

                if (!string.IsNullOrEmpty(incluirComentarioRevisao.ComentarioReplicacao))
                {
                    Estudo estudoReplica = pcmsoFacade.replicarPcmso((long)dgvPcmso.CurrentRow.Cells[0].Value, incluirComentarioRevisao.ComentarioReplicacao, false);
                    MessageBox.Show("Revisão concluída com sucesso", "Confirmação", MessageBoxButtons.OK, MessageBoxIcon.Information);

                    textCodigo.Text = estudoReplica.CodigoEstudo;
                    montaDataGrid();

                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }

        private void btnFinalizar_Click(object sender, EventArgs e)
        {
            try
            {
                if (dgvPcmso.CurrentRow == null)
                    throw new Exception("Selecione uma linha");

                EstudoFacade pcmsoFacade = EstudoFacade.getInstance();
                FinanceiroFacade financeiroFacade = FinanceiroFacade.getInstance();

                Pcmso = pcmsoFacade.findById((long)dgvPcmso.CurrentRow.Cells[0].Value);

                if (Convert.ToBoolean(Pcmso.Avulso))
                    throw new Exception("PCMSO gravado com avulso não pode ser finalizado.");

                if (!string.Equals(Pcmso.Situacao, ApplicationConstants.ABERTA))
                    throw new Exception("Somente PCMSO em análise pode ser finalizado.");

                /* selecionando o tipo de PCMSO devido ao grau */
                Produto produtoPcmso;

                switch (pcmso.GrauRisco)
                {
                    case "2":
                        produtoPcmso = financeiroFacade.findProdutoByDescricao("PCMSO_GRAU2");
                        break;
                    case "3":
                        produtoPcmso = financeiroFacade.findProdutoByDescricao("PCMSO_GRAU3");
                        break;
                    case "4":
                        produtoPcmso = financeiroFacade.findProdutoByDescricao("PCMSO_GRAU4");
                        break;
                    default:
                        produtoPcmso = financeiroFacade.findProdutoByDescricao("PCMSO_GRAU1");
                        break;
                }

                pcmsoFacade.changeEstadoPcmso(Pcmso, ApplicationConstants.FECHADO, produtoPcmso);
                MessageBox.Show("PCMSO finalizado com sucesso.", "Confirmação", MessageBoxButtons.OK, MessageBoxIcon.Information);
                textCodigo.Text = Pcmso.CodigoEstudo;
                montaDataGrid();

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }

        private void btnEnviar_Click(object sender, EventArgs e)
        {
            try
            {
                if (dgvPcmso.CurrentRow == null)
                    throw new Exception("Selecione uma linha.");

                EstudoFacade pcmsoFacade = EstudoFacade.getInstance();

                Pcmso = pcmsoFacade.findById((long)dgvPcmso.CurrentRow.Cells[0].Value);

                if (Pcmso.Avulso)
                    throw new Exception("PCMSO gravado como avulso não é permitido essa ação.");

                if (string.Equals(Pcmso.Situacao, ApplicationConstants.CONSTRUCAO))
                {
                    if (MessageBox.Show("Deseja enviar o PCMSO selecionado para análise?", "Atenção", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                    {

                        frmSelecionarCoordenador selecionarCoordenador = new frmSelecionarCoordenador();
                        selecionarCoordenador.ShowDialog();

                        if (selecionarCoordenador.Coordenador != null)
                        {
                            pcmso.Engenheiro = selecionarCoordenador.Coordenador;
                            pcmsoFacade.changeEstadoPcmso(Pcmso, ApplicationConstants.ATIVO, null);
                            MessageBox.Show("PCMSO enviado para análise com sucesso.", "Confirmação", MessageBoxButtons.OK, MessageBoxIcon.Information);
                            textCodigo.Text = Pcmso.CodigoEstudo;
                            montaDataGrid();
                        }
                    }
                }

                if (string.Equals(Pcmso.Situacao, ApplicationConstants.ATIVO))
                {
                    if (MessageBox.Show("Deseja retornar o PCMSO selecionado para construção?", "Atenção", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                    {
                        pcmsoFacade.changeEstadoPcmso(Pcmso, ApplicationConstants.CONSTRUCAO, null);
                        MessageBox.Show("PCMSO enviado para análise com sucesso.", "Confirmação", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        textCodigo.Text = Pcmso.CodigoEstudo;
                        montaDataGrid();
                    }

                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }

        private void btnImprimir_Click(object sender, EventArgs e)
        {
            try
            {
                if (dgvPcmso.CurrentRow == null)
                    throw new Exception("Selecione uma linha.");

                this.Cursor = Cursors.WaitCursor;

                EstudoFacade pcmsoFacade = EstudoFacade.getInstance();

                Pcmso = pcmsoFacade.findById((long)dgvPcmso.CurrentRow.Cells[0].Value);

                if (Pcmso.Avulso)
                    throw new Exception("PCMSO incluído com avulso não pode ser impresso.");

                RelatorioFacade relatorioFacade = RelatorioFacade.getInstance();

                pcmsoFacade.montaQuadroVersao((long)Pcmso.Id);
                String nomeRelatorio = relatorioFacade.findNomeRelatorioById((long)Pcmso.IdRelatorio);

                var process = new Process();

                process.StartInfo.FileName = "Relatorio\\jre7\\bin\\java";
                process.StartInfo.Arguments = " -jar Relatorio\\MiddlewareJasper.jar " + "Relatorio\\Report\\" + nomeRelatorio + " ID_PCMSO:" + Pcmso.Id + ":INTEGER" + " ID_EMPRESA:" + PermissionamentoFacade.usuarioAutenticado.Empresa.Id + ":INTEGER";
                process.StartInfo.UseShellExecute = false;
                process.StartInfo.RedirectStandardError = true;
                process.StartInfo.RedirectStandardInput = true;
                process.StartInfo.RedirectStandardOutput = true;
                process.StartInfo.CreateNoWindow = true;

                process.Start();
                process.StandardOutput.ReadToEnd();
                process.WaitForExit();

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }

        }

        private void btnExcluir_Click(object sender, EventArgs e)
        {
            try
            {
                if (dgvPcmso.CurrentRow == null)
                    throw new Exception("Selecione uma linha.");

                EstudoFacade pcmsoFacade = EstudoFacade.getInstance();
                Pcmso = pcmsoFacade.findById((long)dgvPcmso.CurrentRow.Cells[0].Value);

                if (Pcmso.Avulso)
                {
                    if (MessageBox.Show("Deseja excluir o PCMSO selecionado?", "Atenção", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                    {
                        pcmsoFacade.deletePcmso(Pcmso);
                        MessageBox.Show("PCMSO excluído com sucesso", "Confirmação", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        textCodigo.Text = string.Empty;
                        montaDataGrid();
                    }
                }
                else
                {
                    if (string.Equals(Pcmso.Situacao, ApplicationConstants.FECHADO))
                        throw new Exception("Não é possível excluir um PCMSO que esteja finalzado");

                    if (MessageBox.Show("Deseja excluir o PCMSO selecionado?", "Atenção", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                    {
                        pcmsoFacade.deletePcmso(Pcmso);
                        MessageBox.Show("PCMSO excluído com sucesso", "Confirmação", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        textCodigo.Text = string.Empty;
                        montaDataGrid();
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }

        private void dgvPcmso_RowPrePaint(object sender, DataGridViewRowPrePaintEventArgs e)
        {
            DataGridView dgv = sender as DataGridView;

            if (String.Equals(dgv.Rows[e.RowIndex].Cells[5].Value, ApplicationConstants.FECHADO)
                && !Convert.ToBoolean(dgv.Rows[e.RowIndex].Cells[11].Value))
                dgv.Rows[e.RowIndex].DefaultCellStyle.ForeColor = Color.Green;

            if (String.Equals(dgv.Rows[e.RowIndex].Cells[5].Value, ApplicationConstants.ATIVO))
                dgv.Rows[e.RowIndex].DefaultCellStyle.ForeColor = Color.Blue;

            if (String.Equals(dgv.Rows[e.RowIndex].Cells[5].Value, ApplicationConstants.DESATIVADO))
                dgv.Rows[e.RowIndex].DefaultCellStyle.ForeColor = Color.Red;

        }

        private void dgvPcmso_CellContentDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                EstudoFacade pcmsoFacade = EstudoFacade.getInstance();

                Pcmso = pcmsoFacade.findById((long)dgvPcmso.CurrentRow.Cells[0].Value);

                if (!string.Equals(Pcmso.Situacao, ApplicationConstants.FECHADO))
                    throw new Exception("Somente estudos finalizados podem ter sido corrigidos.");

                frmPcmsoCorrecaoList Consulta = new frmPcmsoCorrecaoList(Pcmso);
                Consulta.ShowDialog();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }

        private void btnCliente_Click(object sender, EventArgs e)
        {
            frmClienteSelecionar formCliente = new frmClienteSelecionar(null, null, false, null, null, null, null, null, true);
            formCliente.ShowDialog();

            if (formCliente.Cliente != null)
            {
                cliente = formCliente.Cliente;
                this.textCliente.Text = cliente.RazaoSocial;
            }
        }

        private void btnExcluirCliente_Click(object sender, EventArgs e)
        {
            try
            {
                if (cliente == null)
                    throw new Exception("Selecione primeiro o cliente.");

                this.cliente = null;
                textCliente.Text = string.Empty;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }

        private void btnResponsavel_Click(object sender, EventArgs e)
        {
            frmPcmsoResponsavel selecionarResponsavel = new frmPcmsoResponsavel();
            selecionarResponsavel.ShowDialog();

            if (selecionarResponsavel.UsuarioResponsavel != null)
            {
                responsavel = selecionarResponsavel.UsuarioResponsavel;
                textResponsavel.Text = selecionarResponsavel.UsuarioResponsavel.Nome;
            }
        }

        private void btnExcluirResponsavel_Click(object sender, EventArgs e)
        {
            try
            {
                if (responsavel == null)
                    throw new Exception("Selecione primeiro o responsável pelo PCMSO");

                responsavel = null;
                textResponsavel.Text = string.Empty;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }

        private void btnElaborador_Click(object sender, EventArgs e)
        {
            frmUsuarioTecnicoSelecionar formElaborador = new frmUsuarioTecnicoSelecionar();
            formElaborador.ShowDialog();

            if (formElaborador.Usuario != null)
            {
                elaborador = formElaborador.Usuario;
                textUsuarioElaborador.Text = elaborador.Nome;
            }
        }

        private void btnExcluirElaborador_Click(object sender, EventArgs e)
        {
            try
            {
                if (elaborador == null)
                    throw new Exception("Selecione primeiro o elaborador");

                elaborador = null;
                textUsuarioElaborador.Text = string.Empty;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }

        private void btnReativar_Click(object sender, EventArgs e)
        {
            try
            {
                /* função somente disponível para pcmso avulso */
                if (dgvPcmso.CurrentRow == null)
                    throw new Exception("Selecione um pcmso para reativar.");

                if (MessageBox.Show("Deseja reativar o estudo selecionado", "Atenção", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                {
                    this.Cursor = Cursors.WaitCursor;
                    Estudo pcmsoReativar = new Estudo();
                    EstudoFacade pcmsoFacade = EstudoFacade.getInstance();
                    pcmsoReativar = pcmsoFacade.findById((long)dgvPcmso.CurrentRow.Cells[0].Value);

                    if (!pcmsoReativar.Avulso)
                        throw new Exception("Somente PCMSO avulso pode ser reativado");

                    pcmsoReativar = pcmsoFacade.reativar(pcmsoReativar);
                    MessageBox.Show("Estudo reativado com sucesso", "Confirmação", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    textCodigo.Text = pcmsoReativar.CodigoEstudo;
                    ComboHelper.situacaoEstudo(cbSituacao);
                    montaDataGrid();
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }

        }

        private void btnConverter_Click(object sender, EventArgs e)
        {
            try
            {
                if (dgvPcmso.CurrentRow == null)
                    throw new Exception("Você deve selecionar um estudo.");

                if (MessageBox.Show("Deseja converter o pcmso selecionado?", "Atenção", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                {
                    EstudoFacade pcmsoFacade = EstudoFacade.getInstance();
                    RelatorioFacade relatorioFacade = RelatorioFacade.getInstance();

                    Estudo pcmsoConverter = new Estudo();
                    pcmsoConverter = pcmsoFacade.findById((long)dgvPcmso.CurrentRow.Cells[0].Value);

                    if (!pcmsoConverter.Avulso)
                        throw new Exception("Somente PCMSO avulso pode ser convertido.");

                    pcmsoConverter.Avulso = false;
                    pcmsoConverter.Situacao = ApplicationConstants.CONSTRUCAO;

                    /* recuperando informação do relatório ativo do pcmso */
                    pcmsoConverter.IdRelatorio = relatorioFacade.findUltimaVersaoByTipo(ApplicationConstants.PCMSO);

                    pcmsoFacade.updatePcmso(pcmsoConverter);

                    MessageBox.Show("PCMSO convertido com sucesso", "Confirmação", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    textCodigo.Text = pcmsoConverter.CodigoEstudo;
                    ComboHelper.situacaoEstudo(cbSituacao);
                    montaDataGrid();

                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }

        }

        private void btnReplicar_Click(object sender, EventArgs e)
        {
            try
            {
                if (dgvPcmso.CurrentRow == null)
                    throw new Exception("Selecione um estudo para replicar.");

                if (MessageBox.Show("Deseja replicar o estudo selecionado?", "Atenção", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                {
                    EstudoFacade pcmsoFacade = EstudoFacade.getInstance();

                    Estudo pcmsoReplicar = new Estudo();
                    pcmsoReplicar = pcmsoFacade.findById((long)dgvPcmso.CurrentRow.Cells[0].Value);

                    if (!pcmsoReplicar.Avulso)
                        throw new Exception("Somente PCMSO avulso pode ser Replicado.");

                    frmPcmsoAvulsoReplicar replicaPcmso = new frmPcmsoAvulsoReplicar(pcmsoReplicar);
                    replicaPcmso.ShowDialog();

                    if (replicaPcmso.PcmsoReplicado != null)
                    {
                        textCodigo.Text = replicaPcmso.PcmsoReplicado.CodigoEstudo;
                        montaDataGrid();
                    }

                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Atenção", ex.Message, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
            }

        }

    }
}
