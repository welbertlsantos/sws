﻿namespace SWS.View
{
    partial class frmPcmsoExamesAlterar
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            this.flpAcao = new System.Windows.Forms.FlowLayoutPanel();
            this.btIncluir = new System.Windows.Forms.Button();
            this.btFechar = new System.Windows.Forms.Button();
            this.grbPeriodicidade = new System.Windows.Forms.GroupBox();
            this.dgvPeriodicidade = new System.Windows.Forms.DataGridView();
            this.btExcluirExcecao = new System.Windows.Forms.Button();
            this.grbExcecao = new System.Windows.Forms.GroupBox();
            this.dgvExcecao = new System.Windows.Forms.DataGridView();
            this.btIncluirExcecao = new System.Windows.Forms.Button();
            this.cbEspaco = new SWS.ComboBoxWithBorder();
            this.cbAltura = new SWS.ComboBoxWithBorder();
            this.lblEspaco = new System.Windows.Forms.TextBox();
            this.lblAltura = new System.Windows.Forms.TextBox();
            this.textIdade = new System.Windows.Forms.TextBox();
            this.llbIdade = new System.Windows.Forms.TextBox();
            this.lblExame = new System.Windows.Forms.TextBox();
            this.lblGhe = new System.Windows.Forms.TextBox();
            this.textGhe = new System.Windows.Forms.TextBox();
            this.textExame = new System.Windows.Forms.TextBox();
            this.cbEletricidade = new SWS.ComboBoxWithBorder();
            this.lblEletricidade = new System.Windows.Forms.TextBox();
            ((System.ComponentModel.ISupportInitialize)(this.scForm)).BeginInit();
            this.scForm.Panel1.SuspendLayout();
            this.scForm.Panel2.SuspendLayout();
            this.scForm.SuspendLayout();
            this.pnlForm.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.banner)).BeginInit();
            this.flpAcao.SuspendLayout();
            this.grbPeriodicidade.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvPeriodicidade)).BeginInit();
            this.grbExcecao.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvExcecao)).BeginInit();
            this.SuspendLayout();
            // 
            // scForm
            // 
            this.scForm.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            // 
            // scForm.Panel1
            // 
            this.scForm.Panel1.Controls.Add(this.flpAcao);
            this.scForm.Size = new System.Drawing.Size(534, 570);
            this.scForm.SplitterDistance = 32;
            this.scForm.SplitterWidth = 5;
            // 
            // pnlForm
            // 
            this.pnlForm.Controls.Add(this.cbEletricidade);
            this.pnlForm.Controls.Add(this.lblEletricidade);
            this.pnlForm.Controls.Add(this.grbPeriodicidade);
            this.pnlForm.Controls.Add(this.btExcluirExcecao);
            this.pnlForm.Controls.Add(this.grbExcecao);
            this.pnlForm.Controls.Add(this.btIncluirExcecao);
            this.pnlForm.Controls.Add(this.cbEspaco);
            this.pnlForm.Controls.Add(this.cbAltura);
            this.pnlForm.Controls.Add(this.lblEspaco);
            this.pnlForm.Controls.Add(this.lblAltura);
            this.pnlForm.Controls.Add(this.textIdade);
            this.pnlForm.Controls.Add(this.llbIdade);
            this.pnlForm.Controls.Add(this.lblExame);
            this.pnlForm.Controls.Add(this.lblGhe);
            this.pnlForm.Controls.Add(this.textGhe);
            this.pnlForm.Controls.Add(this.textExame);
            this.pnlForm.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.pnlForm.Size = new System.Drawing.Size(514, 580);
            // 
            // flpAcao
            // 
            this.flpAcao.Controls.Add(this.btIncluir);
            this.flpAcao.Controls.Add(this.btFechar);
            this.flpAcao.Dock = System.Windows.Forms.DockStyle.Fill;
            this.flpAcao.Location = new System.Drawing.Point(0, 0);
            this.flpAcao.Name = "flpAcao";
            this.flpAcao.Size = new System.Drawing.Size(534, 32);
            this.flpAcao.TabIndex = 0;
            // 
            // btIncluir
            // 
            this.btIncluir.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btIncluir.Image = global::SWS.Properties.Resources.Gravar;
            this.btIncluir.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btIncluir.Location = new System.Drawing.Point(3, 3);
            this.btIncluir.Name = "btIncluir";
            this.btIncluir.Size = new System.Drawing.Size(75, 23);
            this.btIncluir.TabIndex = 8;
            this.btIncluir.Text = "&Gravar";
            this.btIncluir.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btIncluir.UseVisualStyleBackColor = true;
            this.btIncluir.Click += new System.EventHandler(this.btIncluir_Click);
            // 
            // btFechar
            // 
            this.btFechar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btFechar.Image = global::SWS.Properties.Resources.close;
            this.btFechar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btFechar.Location = new System.Drawing.Point(84, 3);
            this.btFechar.Name = "btFechar";
            this.btFechar.Size = new System.Drawing.Size(75, 23);
            this.btFechar.TabIndex = 9;
            this.btFechar.Text = "&Fechar";
            this.btFechar.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btFechar.UseVisualStyleBackColor = true;
            this.btFechar.Click += new System.EventHandler(this.btFechar_Click);
            // 
            // grbPeriodicidade
            // 
            this.grbPeriodicidade.Controls.Add(this.dgvPeriodicidade);
            this.grbPeriodicidade.Location = new System.Drawing.Point(11, 261);
            this.grbPeriodicidade.Name = "grbPeriodicidade";
            this.grbPeriodicidade.Size = new System.Drawing.Size(489, 232);
            this.grbPeriodicidade.TabIndex = 47;
            this.grbPeriodicidade.TabStop = false;
            this.grbPeriodicidade.Text = "Periodicidade do Exame";
            // 
            // dgvPeriodicidade
            // 
            this.dgvPeriodicidade.AllowUserToAddRows = false;
            this.dgvPeriodicidade.AllowUserToDeleteRows = false;
            this.dgvPeriodicidade.AllowUserToOrderColumns = true;
            this.dgvPeriodicidade.AllowUserToResizeRows = false;
            this.dgvPeriodicidade.BackgroundColor = System.Drawing.Color.White;
            this.dgvPeriodicidade.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.LightGray;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvPeriodicidade.DefaultCellStyle = dataGridViewCellStyle1;
            this.dgvPeriodicidade.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvPeriodicidade.Location = new System.Drawing.Point(3, 16);
            this.dgvPeriodicidade.Name = "dgvPeriodicidade";
            this.dgvPeriodicidade.Size = new System.Drawing.Size(483, 213);
            this.dgvPeriodicidade.TabIndex = 3;
            this.dgvPeriodicidade.CellEndEdit += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvPeriodicidade_CellEndEdit);
            this.dgvPeriodicidade.DataBindingComplete += new System.Windows.Forms.DataGridViewBindingCompleteEventHandler(this.dgvPeriodicidade_DataBindingComplete);
            // 
            // btExcluirExcecao
            // 
            this.btExcluirExcecao.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btExcluirExcecao.Image = global::SWS.Properties.Resources.lixeira;
            this.btExcluirExcecao.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btExcluirExcecao.Location = new System.Drawing.Point(92, 234);
            this.btExcluirExcecao.Name = "btExcluirExcecao";
            this.btExcluirExcecao.Size = new System.Drawing.Size(75, 23);
            this.btExcluirExcecao.TabIndex = 46;
            this.btExcluirExcecao.Text = "&Excluir";
            this.btExcluirExcecao.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btExcluirExcecao.UseVisualStyleBackColor = true;
            // 
            // grbExcecao
            // 
            this.grbExcecao.Controls.Add(this.dgvExcecao);
            this.grbExcecao.Location = new System.Drawing.Point(11, 143);
            this.grbExcecao.Name = "grbExcecao";
            this.grbExcecao.Size = new System.Drawing.Size(492, 84);
            this.grbExcecao.TabIndex = 44;
            this.grbExcecao.TabStop = false;
            this.grbExcecao.Text = "Idade mínima para realização do exame por Função no GHE";
            // 
            // dgvExcecao
            // 
            this.dgvExcecao.AllowUserToAddRows = false;
            this.dgvExcecao.AllowUserToDeleteRows = false;
            this.dgvExcecao.AllowUserToOrderColumns = true;
            this.dgvExcecao.AllowUserToResizeRows = false;
            dataGridViewCellStyle2.BackColor = System.Drawing.Color.Gainsboro;
            this.dgvExcecao.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle2;
            this.dgvExcecao.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this.dgvExcecao.BackgroundColor = System.Drawing.Color.White;
            this.dgvExcecao.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle3.BackColor = System.Drawing.Color.LightGray;
            dataGridViewCellStyle3.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvExcecao.DefaultCellStyle = dataGridViewCellStyle3;
            this.dgvExcecao.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvExcecao.Location = new System.Drawing.Point(3, 16);
            this.dgvExcecao.MultiSelect = false;
            this.dgvExcecao.Name = "dgvExcecao";
            this.dgvExcecao.Size = new System.Drawing.Size(486, 65);
            this.dgvExcecao.TabIndex = 3;
            this.dgvExcecao.CellEndEdit += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvExcecao_CellEndEdit);
            // 
            // btIncluirExcecao
            // 
            this.btIncluirExcecao.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btIncluirExcecao.Image = global::SWS.Properties.Resources.fechar_ico;
            this.btIncluirExcecao.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btIncluirExcecao.Location = new System.Drawing.Point(11, 234);
            this.btIncluirExcecao.Name = "btIncluirExcecao";
            this.btIncluirExcecao.Size = new System.Drawing.Size(75, 23);
            this.btIncluirExcecao.TabIndex = 45;
            this.btIncluirExcecao.Text = "&Incluir";
            this.btIncluirExcecao.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btIncluirExcecao.UseVisualStyleBackColor = true;
            this.btIncluirExcecao.Click += new System.EventHandler(this.btIncluirExcecao_Click);
            // 
            // cbEspaco
            // 
            this.cbEspaco.BorderColor = System.Drawing.Color.DimGray;
            this.cbEspaco.BorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid;
            this.cbEspaco.FormattingEnabled = true;
            this.cbEspaco.Location = new System.Drawing.Point(128, 92);
            this.cbEspaco.Name = "cbEspaco";
            this.cbEspaco.Size = new System.Drawing.Size(375, 21);
            this.cbEspaco.TabIndex = 43;
            // 
            // cbAltura
            // 
            this.cbAltura.BorderColor = System.Drawing.Color.DimGray;
            this.cbAltura.BorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid;
            this.cbAltura.FormattingEnabled = true;
            this.cbAltura.Location = new System.Drawing.Point(128, 73);
            this.cbAltura.Name = "cbAltura";
            this.cbAltura.Size = new System.Drawing.Size(375, 21);
            this.cbAltura.TabIndex = 42;
            // 
            // lblEspaco
            // 
            this.lblEspaco.BackColor = System.Drawing.Color.Silver;
            this.lblEspaco.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblEspaco.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblEspaco.Location = new System.Drawing.Point(11, 92);
            this.lblEspaco.Name = "lblEspaco";
            this.lblEspaco.ReadOnly = true;
            this.lblEspaco.Size = new System.Drawing.Size(118, 21);
            this.lblEspaco.TabIndex = 41;
            this.lblEspaco.TabStop = false;
            this.lblEspaco.Text = "Espaço confinado";
            // 
            // lblAltura
            // 
            this.lblAltura.BackColor = System.Drawing.Color.Silver;
            this.lblAltura.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblAltura.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblAltura.Location = new System.Drawing.Point(11, 73);
            this.lblAltura.Name = "lblAltura";
            this.lblAltura.ReadOnly = true;
            this.lblAltura.Size = new System.Drawing.Size(118, 21);
            this.lblAltura.TabIndex = 40;
            this.lblAltura.TabStop = false;
            this.lblAltura.Text = "Trabalho em altura";
            // 
            // textIdade
            // 
            this.textIdade.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textIdade.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textIdade.ForeColor = System.Drawing.Color.Blue;
            this.textIdade.Location = new System.Drawing.Point(128, 54);
            this.textIdade.MaxLength = 3;
            this.textIdade.Name = "textIdade";
            this.textIdade.Size = new System.Drawing.Size(375, 21);
            this.textIdade.TabIndex = 39;
            this.textIdade.Text = "0";
            this.textIdade.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.textIdade_KeyPress);
            // 
            // llbIdade
            // 
            this.llbIdade.BackColor = System.Drawing.Color.Silver;
            this.llbIdade.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.llbIdade.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.llbIdade.Location = new System.Drawing.Point(11, 54);
            this.llbIdade.Name = "llbIdade";
            this.llbIdade.ReadOnly = true;
            this.llbIdade.Size = new System.Drawing.Size(118, 21);
            this.llbIdade.TabIndex = 38;
            this.llbIdade.TabStop = false;
            this.llbIdade.Text = "Idade mínima";
            // 
            // lblExame
            // 
            this.lblExame.BackColor = System.Drawing.Color.Silver;
            this.lblExame.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblExame.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblExame.Location = new System.Drawing.Point(11, 36);
            this.lblExame.Name = "lblExame";
            this.lblExame.ReadOnly = true;
            this.lblExame.Size = new System.Drawing.Size(118, 21);
            this.lblExame.TabIndex = 37;
            this.lblExame.TabStop = false;
            this.lblExame.Text = "Exame";
            // 
            // lblGhe
            // 
            this.lblGhe.BackColor = System.Drawing.Color.Silver;
            this.lblGhe.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblGhe.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblGhe.Location = new System.Drawing.Point(11, 17);
            this.lblGhe.Name = "lblGhe";
            this.lblGhe.ReadOnly = true;
            this.lblGhe.Size = new System.Drawing.Size(118, 21);
            this.lblGhe.TabIndex = 36;
            this.lblGhe.TabStop = false;
            this.lblGhe.Text = "GHE";
            // 
            // textGhe
            // 
            this.textGhe.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.textGhe.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textGhe.Enabled = false;
            this.textGhe.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textGhe.ForeColor = System.Drawing.Color.Black;
            this.textGhe.Location = new System.Drawing.Point(128, 17);
            this.textGhe.MaxLength = 100;
            this.textGhe.Name = "textGhe";
            this.textGhe.ReadOnly = true;
            this.textGhe.Size = new System.Drawing.Size(375, 21);
            this.textGhe.TabIndex = 35;
            // 
            // textExame
            // 
            this.textExame.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.textExame.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textExame.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textExame.ForeColor = System.Drawing.SystemColors.MenuHighlight;
            this.textExame.Location = new System.Drawing.Point(128, 36);
            this.textExame.MaxLength = 100;
            this.textExame.Name = "textExame";
            this.textExame.ReadOnly = true;
            this.textExame.Size = new System.Drawing.Size(375, 21);
            this.textExame.TabIndex = 34;
            this.textExame.TabStop = false;
            // 
            // cbEletricidade
            // 
            this.cbEletricidade.BorderColor = System.Drawing.Color.DimGray;
            this.cbEletricidade.BorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid;
            this.cbEletricidade.FormattingEnabled = true;
            this.cbEletricidade.Location = new System.Drawing.Point(128, 110);
            this.cbEletricidade.Name = "cbEletricidade";
            this.cbEletricidade.Size = new System.Drawing.Size(375, 21);
            this.cbEletricidade.TabIndex = 49;
            // 
            // lblEletricidade
            // 
            this.lblEletricidade.BackColor = System.Drawing.Color.Silver;
            this.lblEletricidade.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblEletricidade.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblEletricidade.Location = new System.Drawing.Point(11, 110);
            this.lblEletricidade.Name = "lblEletricidade";
            this.lblEletricidade.ReadOnly = true;
            this.lblEletricidade.Size = new System.Drawing.Size(118, 21);
            this.lblEletricidade.TabIndex = 48;
            this.lblEletricidade.TabStop = false;
            this.lblEletricidade.Text = "Serv. em Eletricidade";
            // 
            // frmPcmsoExamesAlterar
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(534, 530);
            this.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.Name = "frmPcmsoExamesAlterar";
            this.Text = "ALTERA EXAME NO PCMSO";
            this.scForm.Panel1.ResumeLayout(false);
            this.scForm.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.scForm)).EndInit();
            this.scForm.ResumeLayout(false);
            this.pnlForm.ResumeLayout(false);
            this.pnlForm.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.banner)).EndInit();
            this.flpAcao.ResumeLayout(false);
            this.grbPeriodicidade.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvPeriodicidade)).EndInit();
            this.grbExcecao.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvExcecao)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.FlowLayoutPanel flpAcao;
        protected System.Windows.Forms.Button btIncluir;
        protected System.Windows.Forms.Button btFechar;
        protected System.Windows.Forms.GroupBox grbPeriodicidade;
        protected System.Windows.Forms.DataGridView dgvPeriodicidade;
        protected System.Windows.Forms.Button btExcluirExcecao;
        protected System.Windows.Forms.GroupBox grbExcecao;
        protected System.Windows.Forms.DataGridView dgvExcecao;
        protected System.Windows.Forms.Button btIncluirExcecao;
        protected ComboBoxWithBorder cbEspaco;
        protected ComboBoxWithBorder cbAltura;
        protected System.Windows.Forms.TextBox lblEspaco;
        protected System.Windows.Forms.TextBox lblAltura;
        protected System.Windows.Forms.TextBox textIdade;
        protected System.Windows.Forms.TextBox llbIdade;
        protected System.Windows.Forms.TextBox lblExame;
        protected System.Windows.Forms.TextBox lblGhe;
        protected System.Windows.Forms.TextBox textGhe;
        protected System.Windows.Forms.TextBox textExame;
        protected ComboBoxWithBorder cbEletricidade;
        protected System.Windows.Forms.TextBox lblEletricidade;
    }
}