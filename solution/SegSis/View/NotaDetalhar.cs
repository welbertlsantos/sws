﻿using SWS.Entidade;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace SWS.View
{
    public partial class frmNotaDetalhar : frmNotaIncluir
    {
        public frmNotaDetalhar(Nota nota) :base()
        {
            InitializeComponent();
            textTitulo.Text = nota.Descricao;
            textConteudo.Text = nota.Conteudo;
            btnGravar.Enabled = false;
            ActiveControl = textTitulo;
        }
    }
}
