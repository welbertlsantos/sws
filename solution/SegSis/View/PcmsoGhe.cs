﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using SWS.Facade;
using SWS.Entidade;
using SWS.View.ViewHelper;
using SWS.View.Resources;

namespace SWS.View
{
    public partial class frmPcmsoGhe : SWS.View.frmTemplateConsulta
    {

        protected Estudo estudo;

        protected Ghe ghe;

        public Ghe Ghe
        {
            get { return ghe; }
            set { ghe = value; }
        }

        public frmPcmsoGhe(Estudo estudo)
        {
            InitializeComponent();
            this.estudo = estudo;
            ActiveControl = textDescricao;
        }

        public frmPcmsoGhe() { InitializeComponent(); }

        protected virtual void btn_salvar_Click(object sender, EventArgs e)
        {
            try
            {
                this.IncluirGheErrorProvider.Clear();
                this.Cursor = Cursors.WaitCursor;

                if (validaCampos())
                {
                    PcmsoFacade pcmsoFacade = PcmsoFacade.getInstance();
                    ghe = new Ghe((Int64?)null, estudo, textDescricao.Text, Convert.ToInt32(textExposto.Text), ApplicationConstants.ATIVO, textNumeroPo.Text);

                    ghe = pcmsoFacade.incluirGhePcmso(ghe);
                    this.Close();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }

        protected void btn_fechar_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        protected Boolean validaCampos()
        {
            Boolean retorno = true;

            if (String.IsNullOrEmpty(textDescricao.Text))
            {
                this.IncluirGheErrorProvider.SetError(this.textDescricao, "Campo obrigatório.");
                retorno = false;
            }

            if (String.IsNullOrEmpty(textExposto.Text))
            {
                this.IncluirGheErrorProvider.SetError(textExposto, "Campo Obrigatório.");
                retorno = false;
            }
            
            return retorno;
        }

        protected void textExposto_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = ValidaCampoHelper.FormataCampoNumerico(sender, e);
        }


    }
}
