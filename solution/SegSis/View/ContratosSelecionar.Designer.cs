﻿namespace SWS.View
{
    partial class frmContratosSelecionar
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            this.flpAcao = new System.Windows.Forms.FlowLayoutPanel();
            this.btn_confirma = new System.Windows.Forms.Button();
            this.btn_fechar = new System.Windows.Forms.Button();
            this.grb_contrato = new System.Windows.Forms.GroupBox();
            this.dg_contrato = new System.Windows.Forms.DataGridView();
            ((System.ComponentModel.ISupportInitialize)(this.scForm)).BeginInit();
            this.scForm.Panel1.SuspendLayout();
            this.scForm.Panel2.SuspendLayout();
            this.scForm.SuspendLayout();
            this.pnlForm.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.banner)).BeginInit();
            this.flpAcao.SuspendLayout();
            this.grb_contrato.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dg_contrato)).BeginInit();
            this.SuspendLayout();
            // 
            // scForm
            // 
            // 
            // scForm.Panel1
            // 
            this.scForm.Panel1.Controls.Add(this.flpAcao);
            // 
            // pnlForm
            // 
            this.pnlForm.Controls.Add(this.grb_contrato);
            // 
            // flpAcao
            // 
            this.flpAcao.Controls.Add(this.btn_confirma);
            this.flpAcao.Controls.Add(this.btn_fechar);
            this.flpAcao.Dock = System.Windows.Forms.DockStyle.Fill;
            this.flpAcao.Location = new System.Drawing.Point(0, 0);
            this.flpAcao.Name = "flpAcao";
            this.flpAcao.Size = new System.Drawing.Size(534, 30);
            this.flpAcao.TabIndex = 0;
            // 
            // btn_confirma
            // 
            this.btn_confirma.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_confirma.Image = global::SWS.Properties.Resources.fechar_ico;
            this.btn_confirma.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btn_confirma.Location = new System.Drawing.Point(3, 3);
            this.btn_confirma.Name = "btn_confirma";
            this.btn_confirma.Size = new System.Drawing.Size(75, 23);
            this.btn_confirma.TabIndex = 5;
            this.btn_confirma.TabStop = false;
            this.btn_confirma.Text = "&Confirmar";
            this.btn_confirma.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btn_confirma.UseVisualStyleBackColor = true;
            this.btn_confirma.Click += new System.EventHandler(this.btn_confirma_Click);
            // 
            // btn_fechar
            // 
            this.btn_fechar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_fechar.Image = global::SWS.Properties.Resources.close;
            this.btn_fechar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btn_fechar.Location = new System.Drawing.Point(84, 3);
            this.btn_fechar.Name = "btn_fechar";
            this.btn_fechar.Size = new System.Drawing.Size(75, 23);
            this.btn_fechar.TabIndex = 6;
            this.btn_fechar.TabStop = false;
            this.btn_fechar.Text = "&Fechar";
            this.btn_fechar.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btn_fechar.UseVisualStyleBackColor = true;
            this.btn_fechar.Click += new System.EventHandler(this.btn_fechar_Click);
            // 
            // grb_contrato
            // 
            this.grb_contrato.Controls.Add(this.dg_contrato);
            this.grb_contrato.Location = new System.Drawing.Point(3, 3);
            this.grb_contrato.Name = "grb_contrato";
            this.grb_contrato.Size = new System.Drawing.Size(508, 271);
            this.grb_contrato.TabIndex = 1;
            this.grb_contrato.TabStop = false;
            this.grb_contrato.Text = "Contratos";
            // 
            // dg_contrato
            // 
            this.dg_contrato.AllowUserToAddRows = false;
            this.dg_contrato.AllowUserToDeleteRows = false;
            this.dg_contrato.AllowUserToOrderColumns = true;
            this.dg_contrato.AllowUserToResizeRows = false;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.Gainsboro;
            this.dg_contrato.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle1;
            this.dg_contrato.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.DisplayedCells;
            this.dg_contrato.BackgroundColor = System.Drawing.Color.White;
            this.dg_contrato.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.Color.LightGray;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dg_contrato.DefaultCellStyle = dataGridViewCellStyle2;
            this.dg_contrato.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dg_contrato.Location = new System.Drawing.Point(3, 16);
            this.dg_contrato.MultiSelect = false;
            this.dg_contrato.Name = "dg_contrato";
            this.dg_contrato.ReadOnly = true;
            this.dg_contrato.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dg_contrato.Size = new System.Drawing.Size(502, 252);
            this.dg_contrato.TabIndex = 1;
            this.dg_contrato.CellMouseDoubleClick += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.dg_contrato_CellMouseDoubleClick);
            // 
            // frmContratosSelecionar
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(534, 361);
            this.Name = "frmContratosSelecionar";
            this.Text = "SELECIONAR CONTRATOS";
            this.scForm.Panel1.ResumeLayout(false);
            this.scForm.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.scForm)).EndInit();
            this.scForm.ResumeLayout(false);
            this.pnlForm.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.banner)).EndInit();
            this.flpAcao.ResumeLayout(false);
            this.grb_contrato.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dg_contrato)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.FlowLayoutPanel flpAcao;
        private System.Windows.Forms.Button btn_fechar;
        private System.Windows.Forms.Button btn_confirma;
        private System.Windows.Forms.GroupBox grb_contrato;
        private System.Windows.Forms.DataGridView dg_contrato;
    }
}