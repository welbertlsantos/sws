﻿using SWS.Entidade;
using SWS.Facade;
using SWS.View.Resources;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace SWS.View
{
    public partial class frmEpiIncluir : frmTemplate
    {
        protected Epi epi;

        public Epi Epi
        {
            get { return epi; }
            set { epi = value; }
        }
        
        public frmEpiIncluir()
        {
            InitializeComponent();
            ActiveControl = text_descricao;
        }

        protected void frmEpiIncluir_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter || e.KeyCode == Keys.Return)
                SendKeys.Send("{TAB}");
        }

        protected virtual void btn_gravar_Click(object sender, EventArgs e)
        {
            try
            {
                if (validaCamposObrigatorio())
                {
                    EstudoFacade pcmsoFacade = EstudoFacade.getInstance();

                    epi = pcmsoFacade.insertEpi(new Epi(null, Convert.ToString(text_descricao.Text.Trim()), Convert.ToString(text_finalidade.Text.Trim()), Convert.ToString(text_ca.Text.Trim()), ApplicationConstants.ATIVO));

                    MessageBox.Show("EPI incluído com sucesso.", "Confirmação", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    this.Close();
                }
            }
            catch (Exception Ex)
            {
                MessageBox.Show(Ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        protected void lbl_limpar_Click(object sender, EventArgs e)
        {
            text_descricao.Text = String.Empty;
            text_finalidade.Text = String.Empty;
            text_ca.Text = String.Empty;
            text_descricao.Focus();
        }

        protected void btn_fechar_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        protected bool validaCamposObrigatorio()
        {
            bool retorno = true;
            try
            {
                if (string.IsNullOrEmpty(text_descricao.Text))
                    throw new Exception ("A Descrição é obrigatória!");

            }
            catch (Exception ex)
            {
                retorno = false;
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            return retorno;
        }
    }
}
