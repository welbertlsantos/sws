﻿namespace SWS.View
{
    partial class frmRelatorioMovimentoFinanceiro
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.flpAcao = new System.Windows.Forms.FlowLayoutPanel();
            this.btnImprimir = new System.Windows.Forms.Button();
            this.btnFechar = new System.Windows.Forms.Button();
            this.lblCliente = new System.Windows.Forms.TextBox();
            this.textCliente = new System.Windows.Forms.TextBox();
            this.btnExcluirCliente = new System.Windows.Forms.Button();
            this.btnCliente = new System.Windows.Forms.Button();
            this.lblPeriodo = new System.Windows.Forms.TextBox();
            this.dtFinalMovimento = new System.Windows.Forms.DateTimePicker();
            this.dtInicialMovimento = new System.Windows.Forms.DateTimePicker();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.dtInicialAtendimento = new System.Windows.Forms.DateTimePicker();
            this.dtFinalAtendimento = new System.Windows.Forms.DateTimePicker();
            this.lblTipoRelatorio = new System.Windows.Forms.TextBox();
            this.lblSituacaoMovimento = new System.Windows.Forms.TextBox();
            this.lblTipoAnalise = new System.Windows.Forms.TextBox();
            this.lblOrdenacao = new System.Windows.Forms.TextBox();
            this.lblUnidade = new System.Windows.Forms.TextBox();
            this.cbTipoRelatorio = new SWS.ComboBoxWithBorder();
            this.cbSituacaoMovimento = new SWS.ComboBoxWithBorder();
            this.cbTipoAnalise = new SWS.ComboBoxWithBorder();
            this.cbOrdenacao = new SWS.ComboBoxWithBorder();
            this.cbUnidade = new SWS.ComboBoxWithBorder();
            this.lblCentroCusto = new System.Windows.Forms.TextBox();
            this.cbCentroCusto = new SWS.ComboBoxWithBorder();
            this.textCredenciada = new System.Windows.Forms.TextBox();
            this.excluirCredenciada = new System.Windows.Forms.Button();
            this.btnCredenciada = new System.Windows.Forms.Button();
            this.lblCredenciada = new System.Windows.Forms.TextBox();
            ((System.ComponentModel.ISupportInitialize)(this.scForm)).BeginInit();
            this.scForm.Panel1.SuspendLayout();
            this.scForm.Panel2.SuspendLayout();
            this.scForm.SuspendLayout();
            this.pnlForm.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.banner)).BeginInit();
            this.flpAcao.SuspendLayout();
            this.SuspendLayout();
            // 
            // scForm
            // 
            // 
            // scForm.Panel1
            // 
            this.scForm.Panel1.Controls.Add(this.flpAcao);
            this.scForm.SplitterDistance = 36;
            // 
            // pnlForm
            // 
            this.pnlForm.Controls.Add(this.textCredenciada);
            this.pnlForm.Controls.Add(this.excluirCredenciada);
            this.pnlForm.Controls.Add(this.btnCredenciada);
            this.pnlForm.Controls.Add(this.lblCredenciada);
            this.pnlForm.Controls.Add(this.cbCentroCusto);
            this.pnlForm.Controls.Add(this.lblCentroCusto);
            this.pnlForm.Controls.Add(this.cbUnidade);
            this.pnlForm.Controls.Add(this.cbOrdenacao);
            this.pnlForm.Controls.Add(this.cbTipoAnalise);
            this.pnlForm.Controls.Add(this.cbSituacaoMovimento);
            this.pnlForm.Controls.Add(this.cbTipoRelatorio);
            this.pnlForm.Controls.Add(this.lblUnidade);
            this.pnlForm.Controls.Add(this.lblOrdenacao);
            this.pnlForm.Controls.Add(this.lblTipoAnalise);
            this.pnlForm.Controls.Add(this.lblSituacaoMovimento);
            this.pnlForm.Controls.Add(this.lblTipoRelatorio);
            this.pnlForm.Controls.Add(this.textBox1);
            this.pnlForm.Controls.Add(this.dtFinalAtendimento);
            this.pnlForm.Controls.Add(this.dtInicialAtendimento);
            this.pnlForm.Controls.Add(this.lblPeriodo);
            this.pnlForm.Controls.Add(this.textCliente);
            this.pnlForm.Controls.Add(this.dtFinalMovimento);
            this.pnlForm.Controls.Add(this.dtInicialMovimento);
            this.pnlForm.Controls.Add(this.btnExcluirCliente);
            this.pnlForm.Controls.Add(this.btnCliente);
            this.pnlForm.Controls.Add(this.lblCliente);
            this.pnlForm.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlForm.Margin = new System.Windows.Forms.Padding(5);
            this.pnlForm.Size = new System.Drawing.Size(712, 353);
            // 
            // flpAcao
            // 
            this.flpAcao.Controls.Add(this.btnImprimir);
            this.flpAcao.Controls.Add(this.btnFechar);
            this.flpAcao.Dock = System.Windows.Forms.DockStyle.Fill;
            this.flpAcao.Location = new System.Drawing.Point(0, 0);
            this.flpAcao.Margin = new System.Windows.Forms.Padding(4);
            this.flpAcao.Name = "flpAcao";
            this.flpAcao.Size = new System.Drawing.Size(712, 36);
            this.flpAcao.TabIndex = 0;
            // 
            // btnImprimir
            // 
            this.btnImprimir.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnImprimir.Image = global::SWS.Properties.Resources.impressora1;
            this.btnImprimir.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnImprimir.Location = new System.Drawing.Point(4, 4);
            this.btnImprimir.Margin = new System.Windows.Forms.Padding(4);
            this.btnImprimir.Name = "btnImprimir";
            this.btnImprimir.Size = new System.Drawing.Size(100, 28);
            this.btnImprimir.TabIndex = 2;
            this.btnImprimir.Text = "&Imprimir";
            this.btnImprimir.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnImprimir.UseVisualStyleBackColor = true;
            this.btnImprimir.Click += new System.EventHandler(this.btn_imprimir_Click);
            // 
            // btnFechar
            // 
            this.btnFechar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnFechar.Image = global::SWS.Properties.Resources.close;
            this.btnFechar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnFechar.Location = new System.Drawing.Point(112, 4);
            this.btnFechar.Margin = new System.Windows.Forms.Padding(4);
            this.btnFechar.Name = "btnFechar";
            this.btnFechar.Size = new System.Drawing.Size(100, 28);
            this.btnFechar.TabIndex = 3;
            this.btnFechar.Text = "&Fechar";
            this.btnFechar.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnFechar.UseVisualStyleBackColor = true;
            this.btnFechar.Click += new System.EventHandler(this.button2_Click);
            // 
            // lblCliente
            // 
            this.lblCliente.BackColor = System.Drawing.Color.LightGray;
            this.lblCliente.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblCliente.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCliente.Location = new System.Drawing.Point(16, 16);
            this.lblCliente.Margin = new System.Windows.Forms.Padding(4);
            this.lblCliente.Name = "lblCliente";
            this.lblCliente.ReadOnly = true;
            this.lblCliente.Size = new System.Drawing.Size(191, 24);
            this.lblCliente.TabIndex = 0;
            this.lblCliente.TabStop = false;
            this.lblCliente.Text = "Cliente / Credenciadora";
            // 
            // textCliente
            // 
            this.textCliente.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.textCliente.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textCliente.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textCliente.Location = new System.Drawing.Point(207, 16);
            this.textCliente.Margin = new System.Windows.Forms.Padding(4);
            this.textCliente.MaxLength = 100;
            this.textCliente.Name = "textCliente";
            this.textCliente.ReadOnly = true;
            this.textCliente.Size = new System.Drawing.Size(383, 24);
            this.textCliente.TabIndex = 1;
            this.textCliente.TabStop = false;
            // 
            // btnExcluirCliente
            // 
            this.btnExcluirCliente.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnExcluirCliente.Image = global::SWS.Properties.Resources.close;
            this.btnExcluirCliente.Location = new System.Drawing.Point(633, 16);
            this.btnExcluirCliente.Margin = new System.Windows.Forms.Padding(4);
            this.btnExcluirCliente.Name = "btnExcluirCliente";
            this.btnExcluirCliente.Size = new System.Drawing.Size(45, 26);
            this.btnExcluirCliente.TabIndex = 4;
            this.btnExcluirCliente.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnExcluirCliente.UseVisualStyleBackColor = true;
            this.btnExcluirCliente.Click += new System.EventHandler(this.btn_limpar_Click);
            // 
            // btnCliente
            // 
            this.btnCliente.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnCliente.Image = global::SWS.Properties.Resources.busca;
            this.btnCliente.Location = new System.Drawing.Point(589, 16);
            this.btnCliente.Margin = new System.Windows.Forms.Padding(4);
            this.btnCliente.Name = "btnCliente";
            this.btnCliente.Size = new System.Drawing.Size(45, 26);
            this.btnCliente.TabIndex = 3;
            this.btnCliente.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnCliente.UseVisualStyleBackColor = true;
            this.btnCliente.Click += new System.EventHandler(this.btn_selecionar_Click);
            // 
            // lblPeriodo
            // 
            this.lblPeriodo.BackColor = System.Drawing.Color.LightGray;
            this.lblPeriodo.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblPeriodo.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPeriodo.Location = new System.Drawing.Point(16, 64);
            this.lblPeriodo.Margin = new System.Windows.Forms.Padding(4);
            this.lblPeriodo.Name = "lblPeriodo";
            this.lblPeriodo.ReadOnly = true;
            this.lblPeriodo.Size = new System.Drawing.Size(191, 24);
            this.lblPeriodo.TabIndex = 5;
            this.lblPeriodo.TabStop = false;
            this.lblPeriodo.Text = "Lançamento Movimento";
            // 
            // dtFinalMovimento
            // 
            this.dtFinalMovimento.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtFinalMovimento.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtFinalMovimento.Location = new System.Drawing.Point(364, 64);
            this.dtFinalMovimento.Margin = new System.Windows.Forms.Padding(4);
            this.dtFinalMovimento.Name = "dtFinalMovimento";
            this.dtFinalMovimento.ShowCheckBox = true;
            this.dtFinalMovimento.Size = new System.Drawing.Size(159, 24);
            this.dtFinalMovimento.TabIndex = 7;
            // 
            // dtInicialMovimento
            // 
            this.dtInicialMovimento.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtInicialMovimento.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtInicialMovimento.Location = new System.Drawing.Point(205, 64);
            this.dtInicialMovimento.Margin = new System.Windows.Forms.Padding(4);
            this.dtInicialMovimento.Name = "dtInicialMovimento";
            this.dtInicialMovimento.ShowCheckBox = true;
            this.dtInicialMovimento.Size = new System.Drawing.Size(159, 24);
            this.dtInicialMovimento.TabIndex = 6;
            // 
            // textBox1
            // 
            this.textBox1.BackColor = System.Drawing.Color.LightGray;
            this.textBox1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox1.Location = new System.Drawing.Point(16, 88);
            this.textBox1.Margin = new System.Windows.Forms.Padding(4);
            this.textBox1.Name = "textBox1";
            this.textBox1.ReadOnly = true;
            this.textBox1.Size = new System.Drawing.Size(191, 24);
            this.textBox1.TabIndex = 9;
            this.textBox1.TabStop = false;
            this.textBox1.Text = "Digitação Atendimento";
            // 
            // dtInicialAtendimento
            // 
            this.dtInicialAtendimento.Checked = false;
            this.dtInicialAtendimento.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtInicialAtendimento.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtInicialAtendimento.Location = new System.Drawing.Point(205, 88);
            this.dtInicialAtendimento.Margin = new System.Windows.Forms.Padding(4);
            this.dtInicialAtendimento.Name = "dtInicialAtendimento";
            this.dtInicialAtendimento.ShowCheckBox = true;
            this.dtInicialAtendimento.Size = new System.Drawing.Size(159, 24);
            this.dtInicialAtendimento.TabIndex = 10;
            // 
            // dtFinalAtendimento
            // 
            this.dtFinalAtendimento.Checked = false;
            this.dtFinalAtendimento.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtFinalAtendimento.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtFinalAtendimento.Location = new System.Drawing.Point(364, 88);
            this.dtFinalAtendimento.Margin = new System.Windows.Forms.Padding(4);
            this.dtFinalAtendimento.Name = "dtFinalAtendimento";
            this.dtFinalAtendimento.ShowCheckBox = true;
            this.dtFinalAtendimento.Size = new System.Drawing.Size(159, 24);
            this.dtFinalAtendimento.TabIndex = 11;
            // 
            // lblTipoRelatorio
            // 
            this.lblTipoRelatorio.BackColor = System.Drawing.Color.LightGray;
            this.lblTipoRelatorio.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblTipoRelatorio.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTipoRelatorio.Location = new System.Drawing.Point(16, 137);
            this.lblTipoRelatorio.Margin = new System.Windows.Forms.Padding(4);
            this.lblTipoRelatorio.Name = "lblTipoRelatorio";
            this.lblTipoRelatorio.ReadOnly = true;
            this.lblTipoRelatorio.Size = new System.Drawing.Size(191, 24);
            this.lblTipoRelatorio.TabIndex = 15;
            this.lblTipoRelatorio.TabStop = false;
            this.lblTipoRelatorio.Text = "Tipo de Relatório";
            // 
            // lblSituacaoMovimento
            // 
            this.lblSituacaoMovimento.BackColor = System.Drawing.Color.LightGray;
            this.lblSituacaoMovimento.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblSituacaoMovimento.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSituacaoMovimento.Location = new System.Drawing.Point(16, 162);
            this.lblSituacaoMovimento.Margin = new System.Windows.Forms.Padding(4);
            this.lblSituacaoMovimento.Name = "lblSituacaoMovimento";
            this.lblSituacaoMovimento.ReadOnly = true;
            this.lblSituacaoMovimento.Size = new System.Drawing.Size(191, 24);
            this.lblSituacaoMovimento.TabIndex = 16;
            this.lblSituacaoMovimento.TabStop = false;
            this.lblSituacaoMovimento.Text = "Situação do Movimento";
            // 
            // lblTipoAnalise
            // 
            this.lblTipoAnalise.BackColor = System.Drawing.Color.LightGray;
            this.lblTipoAnalise.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblTipoAnalise.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTipoAnalise.Location = new System.Drawing.Point(16, 187);
            this.lblTipoAnalise.Margin = new System.Windows.Forms.Padding(4);
            this.lblTipoAnalise.Name = "lblTipoAnalise";
            this.lblTipoAnalise.ReadOnly = true;
            this.lblTipoAnalise.Size = new System.Drawing.Size(191, 24);
            this.lblTipoAnalise.TabIndex = 17;
            this.lblTipoAnalise.TabStop = false;
            this.lblTipoAnalise.Text = "Tipo de Análise";
            // 
            // lblOrdenacao
            // 
            this.lblOrdenacao.BackColor = System.Drawing.Color.LightGray;
            this.lblOrdenacao.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblOrdenacao.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblOrdenacao.Location = new System.Drawing.Point(16, 211);
            this.lblOrdenacao.Margin = new System.Windows.Forms.Padding(4);
            this.lblOrdenacao.Name = "lblOrdenacao";
            this.lblOrdenacao.ReadOnly = true;
            this.lblOrdenacao.Size = new System.Drawing.Size(191, 24);
            this.lblOrdenacao.TabIndex = 18;
            this.lblOrdenacao.TabStop = false;
            this.lblOrdenacao.Text = "Ordenação";
            // 
            // lblUnidade
            // 
            this.lblUnidade.BackColor = System.Drawing.Color.LightGray;
            this.lblUnidade.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblUnidade.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblUnidade.Location = new System.Drawing.Point(16, 236);
            this.lblUnidade.Margin = new System.Windows.Forms.Padding(4);
            this.lblUnidade.Name = "lblUnidade";
            this.lblUnidade.ReadOnly = true;
            this.lblUnidade.Size = new System.Drawing.Size(191, 24);
            this.lblUnidade.TabIndex = 19;
            this.lblUnidade.TabStop = false;
            this.lblUnidade.Text = "Separar por Unidade?";
            // 
            // cbTipoRelatorio
            // 
            this.cbTipoRelatorio.BackColor = System.Drawing.Color.LightGray;
            this.cbTipoRelatorio.BorderColor = System.Drawing.Color.DimGray;
            this.cbTipoRelatorio.BorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid;
            this.cbTipoRelatorio.FormattingEnabled = true;
            this.cbTipoRelatorio.Location = new System.Drawing.Point(207, 137);
            this.cbTipoRelatorio.Margin = new System.Windows.Forms.Padding(4);
            this.cbTipoRelatorio.Name = "cbTipoRelatorio";
            this.cbTipoRelatorio.Size = new System.Drawing.Size(471, 24);
            this.cbTipoRelatorio.TabIndex = 20;
            this.cbTipoRelatorio.SelectedIndexChanged += new System.EventHandler(this.cbTipoRelatório_SelectedIndexChanged);
            // 
            // cbSituacaoMovimento
            // 
            this.cbSituacaoMovimento.BackColor = System.Drawing.Color.LightGray;
            this.cbSituacaoMovimento.BorderColor = System.Drawing.Color.DimGray;
            this.cbSituacaoMovimento.BorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid;
            this.cbSituacaoMovimento.FormattingEnabled = true;
            this.cbSituacaoMovimento.Location = new System.Drawing.Point(207, 162);
            this.cbSituacaoMovimento.Margin = new System.Windows.Forms.Padding(4);
            this.cbSituacaoMovimento.Name = "cbSituacaoMovimento";
            this.cbSituacaoMovimento.Size = new System.Drawing.Size(471, 24);
            this.cbSituacaoMovimento.TabIndex = 21;
            // 
            // cbTipoAnalise
            // 
            this.cbTipoAnalise.BackColor = System.Drawing.Color.LightGray;
            this.cbTipoAnalise.BorderColor = System.Drawing.Color.DimGray;
            this.cbTipoAnalise.BorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid;
            this.cbTipoAnalise.FormattingEnabled = true;
            this.cbTipoAnalise.Location = new System.Drawing.Point(207, 187);
            this.cbTipoAnalise.Margin = new System.Windows.Forms.Padding(4);
            this.cbTipoAnalise.Name = "cbTipoAnalise";
            this.cbTipoAnalise.Size = new System.Drawing.Size(471, 24);
            this.cbTipoAnalise.TabIndex = 22;
            // 
            // cbOrdenacao
            // 
            this.cbOrdenacao.BackColor = System.Drawing.Color.LightGray;
            this.cbOrdenacao.BorderColor = System.Drawing.Color.DimGray;
            this.cbOrdenacao.BorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid;
            this.cbOrdenacao.FormattingEnabled = true;
            this.cbOrdenacao.Location = new System.Drawing.Point(207, 211);
            this.cbOrdenacao.Margin = new System.Windows.Forms.Padding(4);
            this.cbOrdenacao.Name = "cbOrdenacao";
            this.cbOrdenacao.Size = new System.Drawing.Size(471, 24);
            this.cbOrdenacao.TabIndex = 23;
            // 
            // cbUnidade
            // 
            this.cbUnidade.BackColor = System.Drawing.Color.LightGray;
            this.cbUnidade.BorderColor = System.Drawing.Color.DimGray;
            this.cbUnidade.BorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid;
            this.cbUnidade.FormattingEnabled = true;
            this.cbUnidade.Location = new System.Drawing.Point(207, 236);
            this.cbUnidade.Margin = new System.Windows.Forms.Padding(4);
            this.cbUnidade.Name = "cbUnidade";
            this.cbUnidade.Size = new System.Drawing.Size(471, 24);
            this.cbUnidade.TabIndex = 24;
            // 
            // lblCentroCusto
            // 
            this.lblCentroCusto.BackColor = System.Drawing.Color.LightGray;
            this.lblCentroCusto.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblCentroCusto.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCentroCusto.Location = new System.Drawing.Point(16, 113);
            this.lblCentroCusto.Margin = new System.Windows.Forms.Padding(4);
            this.lblCentroCusto.Name = "lblCentroCusto";
            this.lblCentroCusto.ReadOnly = true;
            this.lblCentroCusto.Size = new System.Drawing.Size(191, 24);
            this.lblCentroCusto.TabIndex = 25;
            this.lblCentroCusto.TabStop = false;
            this.lblCentroCusto.Text = "Centro de Custo";
            // 
            // cbCentroCusto
            // 
            this.cbCentroCusto.BackColor = System.Drawing.Color.LightGray;
            this.cbCentroCusto.BorderColor = System.Drawing.Color.DimGray;
            this.cbCentroCusto.BorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid;
            this.cbCentroCusto.Enabled = false;
            this.cbCentroCusto.FormattingEnabled = true;
            this.cbCentroCusto.Location = new System.Drawing.Point(207, 113);
            this.cbCentroCusto.Margin = new System.Windows.Forms.Padding(4);
            this.cbCentroCusto.Name = "cbCentroCusto";
            this.cbCentroCusto.Size = new System.Drawing.Size(471, 24);
            this.cbCentroCusto.TabIndex = 26;
            // 
            // textCredenciada
            // 
            this.textCredenciada.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.textCredenciada.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textCredenciada.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textCredenciada.Location = new System.Drawing.Point(207, 40);
            this.textCredenciada.Margin = new System.Windows.Forms.Padding(4);
            this.textCredenciada.MaxLength = 100;
            this.textCredenciada.Name = "textCredenciada";
            this.textCredenciada.ReadOnly = true;
            this.textCredenciada.Size = new System.Drawing.Size(383, 24);
            this.textCredenciada.TabIndex = 28;
            this.textCredenciada.TabStop = false;
            // 
            // excluirCredenciada
            // 
            this.excluirCredenciada.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.excluirCredenciada.Image = global::SWS.Properties.Resources.close;
            this.excluirCredenciada.Location = new System.Drawing.Point(633, 40);
            this.excluirCredenciada.Margin = new System.Windows.Forms.Padding(4);
            this.excluirCredenciada.Name = "excluirCredenciada";
            this.excluirCredenciada.Size = new System.Drawing.Size(45, 26);
            this.excluirCredenciada.TabIndex = 30;
            this.excluirCredenciada.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.excluirCredenciada.UseVisualStyleBackColor = true;
            this.excluirCredenciada.Click += new System.EventHandler(this.excluirCredenciada_Click);
            // 
            // btnCredenciada
            // 
            this.btnCredenciada.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnCredenciada.Image = global::SWS.Properties.Resources.busca;
            this.btnCredenciada.Location = new System.Drawing.Point(589, 40);
            this.btnCredenciada.Margin = new System.Windows.Forms.Padding(4);
            this.btnCredenciada.Name = "btnCredenciada";
            this.btnCredenciada.Size = new System.Drawing.Size(45, 26);
            this.btnCredenciada.TabIndex = 29;
            this.btnCredenciada.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnCredenciada.UseVisualStyleBackColor = true;
            this.btnCredenciada.Click += new System.EventHandler(this.btnCredenciada_Click);
            // 
            // lblCredenciada
            // 
            this.lblCredenciada.BackColor = System.Drawing.Color.LightGray;
            this.lblCredenciada.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblCredenciada.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCredenciada.Location = new System.Drawing.Point(16, 40);
            this.lblCredenciada.Margin = new System.Windows.Forms.Padding(4);
            this.lblCredenciada.Name = "lblCredenciada";
            this.lblCredenciada.ReadOnly = true;
            this.lblCredenciada.Size = new System.Drawing.Size(191, 24);
            this.lblCredenciada.TabIndex = 27;
            this.lblCredenciada.TabStop = false;
            this.lblCredenciada.Text = "Credenciada";
            // 
            // frmRelatorioMovimentoFinanceiro
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(712, 444);
            this.Margin = new System.Windows.Forms.Padding(5);
            this.Name = "frmRelatorioMovimentoFinanceiro";
            this.Text = "RELATÓRIO DE MOVIMENTO";
            this.scForm.Panel1.ResumeLayout(false);
            this.scForm.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.scForm)).EndInit();
            this.scForm.ResumeLayout(false);
            this.pnlForm.ResumeLayout(false);
            this.pnlForm.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.banner)).EndInit();
            this.flpAcao.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.FlowLayoutPanel flpAcao;
        private System.Windows.Forms.Button btnFechar;
        private System.Windows.Forms.Button btnImprimir;
        private System.Windows.Forms.TextBox lblCliente;
        private System.Windows.Forms.TextBox textCliente;
        private System.Windows.Forms.Button btnExcluirCliente;
        private System.Windows.Forms.Button btnCliente;
        private System.Windows.Forms.TextBox lblPeriodo;
        private System.Windows.Forms.DateTimePicker dtFinalMovimento;
        private System.Windows.Forms.DateTimePicker dtInicialMovimento;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.DateTimePicker dtFinalAtendimento;
        private System.Windows.Forms.DateTimePicker dtInicialAtendimento;
        private ComboBoxWithBorder cbOrdenacao;
        private ComboBoxWithBorder cbTipoAnalise;
        private ComboBoxWithBorder cbSituacaoMovimento;
        private ComboBoxWithBorder cbTipoRelatorio;
        private System.Windows.Forms.TextBox lblUnidade;
        private System.Windows.Forms.TextBox lblOrdenacao;
        private System.Windows.Forms.TextBox lblTipoAnalise;
        private System.Windows.Forms.TextBox lblSituacaoMovimento;
        private System.Windows.Forms.TextBox lblTipoRelatorio;
        private ComboBoxWithBorder cbUnidade;
        private ComboBoxWithBorder cbCentroCusto;
        private System.Windows.Forms.TextBox lblCentroCusto;
        private System.Windows.Forms.TextBox textCredenciada;
        private System.Windows.Forms.Button excluirCredenciada;
        private System.Windows.Forms.Button btnCredenciada;
        private System.Windows.Forms.TextBox lblCredenciada;
    }
}