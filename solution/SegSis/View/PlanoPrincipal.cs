﻿using SWS.Entidade;
using SWS.Facade;
using SWS.View.Resources;
using SWS.View.ViewHelper;
using SWS.ViewHelper;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace SWS.View
{
    public partial class frmPlanoPrincipal : frmTemplate
    {
        private Plano plano;
        
        public frmPlanoPrincipal()
        {
            InitializeComponent();
            ComboHelper.comboSituacao(cbSituacao);
            ActiveControl = textPlano;
        }

        private void btnIncluir_Click(object sender, EventArgs e)
        {
            try
            {
                frmPlanoIncluir incluirPlano = new frmPlanoIncluir();
                incluirPlano.ShowDialog();

                if (incluirPlano.Plano != null)
                {
                    plano = new Plano();
                    montaDataGrid();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }

        private void btnAlterar_Click(object sender, EventArgs e)
        {
            try
            {
                if (dgvPlano.CurrentRow == null)
                    throw new Exception("Selecione uma linha.");

                FinanceiroFacade financeiroFacade = FinanceiroFacade.getInstance();

                Plano planoAlterar = financeiroFacade.findPlanoById((long)dgvPlano.CurrentRow.Cells[0].Value);

                if (planoAlterar.Carga)
                    throw new Exception("Plano exclusivo do sistema. Não pode ser alterado.");


                frmPlanoAlterar alterarPlano = new frmPlanoAlterar(planoAlterar);
                alterarPlano.ShowDialog();

                plano = new Plano();
                montaDataGrid();
                

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }

        private void btnPesquisar_Click(object sender, EventArgs e)
        {
            try
            {
                plano = new Plano(null, textPlano.Text, ((SelectItem)cbSituacao.SelectedItem).Valor, false);
                montaDataGrid();
                textPlano.Focus();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }

        private void btnReativar_Click(object sender, EventArgs e)
        {
            try
            {
                if (dgvPlano.CurrentRow == null)
                    throw new Exception("Selecione uma linha.");

                FinanceiroFacade financeiroFacade = FinanceiroFacade.getInstance();

                Plano planoReativar = financeiroFacade.findPlanoById((long)dgvPlano.CurrentRow.Cells[0].Value);

                if (!string.Equals(planoReativar.Situacao, ApplicationConstants.DESATIVADO))
                    throw new Exception("Somente planos inativos podem ser reativados.");

                if (planoReativar.Carga)
                    throw new Exception("Plano exclusivo do sistema. Não pode ser alterado.");


                if (MessageBox.Show("Deseja reativar o plano selecionado?", "Atenção", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                {
                    planoReativar.Situacao = ApplicationConstants.ATIVO;
                    financeiroFacade.updatePlano(planoReativar);
                    MessageBox.Show("Plano reativado com sucesso.", "Confirmação", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    plano = new Plano();
                    montaDataGrid();
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }

        private void btnDetalhar_Click(object sender, EventArgs e)
        {
            try
            {
                if (dgvPlano.CurrentRow == null)
                    throw new Exception("Selecione uma linha.");

                FinanceiroFacade financeiroFacade = FinanceiroFacade.getInstance();

                frmPlanoDetalhar detalharPlano = new frmPlanoDetalhar(financeiroFacade.findPlanoById((long)dgvPlano.CurrentRow.Cells[0].Value));
                detalharPlano.ShowDialog();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }

        private void btnExcluir_Click(object sender, EventArgs e)
        {
            try
            {
                if (dgvPlano.CurrentRow == null)
                    throw new Exception("Selecione uma linha.");

                FinanceiroFacade financeiroFacade = FinanceiroFacade.getInstance();

                Plano planoExcluir = financeiroFacade.findPlanoById((long)dgvPlano.CurrentRow.Cells[0].Value);


                if (!string.Equals(planoExcluir.Situacao, ApplicationConstants.ATIVO))
                    throw new Exception("Somente planos ativos podem ser excluídos.");

                if (planoExcluir.Carga)
                    throw new Exception("Plano exclusivo do sistema. Não pode ser alterado.");

                if (MessageBox.Show("Deseja excluir o plano selecionado?", "Atenção", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                {
                    planoExcluir.Situacao = ApplicationConstants.DESATIVADO;
                    financeiroFacade.updatePlano(planoExcluir);
                    MessageBox.Show("Plano excluído com sucesso.", "Confirmação", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    plano = new Plano();
                    montaDataGrid();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }

        private void btnFechar_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        public void montaDataGrid()
        {
            try
            {
                this.Cursor = Cursors.WaitCursor;
                dgvPlano.Columns.Clear();

                FinanceiroFacade financeiroFacade = FinanceiroFacade.getInstance();

                DataSet ds = financeiroFacade.findPlanoByFilter(plano);

                dgvPlano.DataSource = ds.Tables[0].DefaultView;

                dgvPlano.Columns[0].HeaderText = "Id";
                dgvPlano.Columns[0].Visible = false;

                dgvPlano.Columns[1].HeaderText = "Descrição";

                dgvPlano.Columns[2].HeaderText = "Situação";
                dgvPlano.Columns[2].Visible = false;

                dgvPlano.Columns[3].HeaderText = "Carga";
                dgvPlano.Columns[3].Visible = false;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }

        private void dgvPlano_RowPrePaint(object sender, DataGridViewRowPrePaintEventArgs e)
        {
            DataGridView dvg = sender as DataGridView;
            
            if (String.Equals(dvg.Rows[e.RowIndex].Cells[2].Value.ToString(), ApplicationConstants.DESATIVADO))
                dvg.Rows[e.RowIndex].DefaultCellStyle.ForeColor = Color.Red;
        }

        private void dgvPlano_CellMouseDoubleClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            try
            {
                btnAlterar.PerformClick();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }
    }
}
