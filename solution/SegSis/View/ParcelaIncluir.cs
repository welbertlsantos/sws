﻿using SWS.Entidade;
using SWS.View.ViewHelper;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace SWS.View
{
    public partial class frmParcelaIncluir : frmTemplateConsulta
    {
        private Parcela parcela;

        public Parcela Parcela
        {
            get { return parcela; }
            set { parcela = value; }
        }
        
        public frmParcelaIncluir()
        {
            InitializeComponent();
            ActiveControl = textDias;
        }

        protected virtual void btnGravar_Click(object sender, EventArgs e)
        {
            try
            {
                if (ValidaTela())
                {
                    parcela = new Parcela(null, null, Convert.ToInt32(textDias.Text));
                    this.Close();
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }

        protected void btnFechar_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        protected void textDias_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = ValidaCampoHelper.FormataCampoNumerico(sender, e);
        }

        protected bool ValidaTela()
        {
            bool result = true;

            try
            {
                if (string.IsNullOrEmpty(textDias.Text.Trim()))
                    throw new Exception("O número de dias não pode ser nulo. Informe 0 para venda a vista.");
            }
            catch (Exception ex)
            {
                result = false;
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            return result;
        }

        private void frmParcelaIncluir_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter || e.KeyCode == Keys.Return)
                SendKeys.Send("{TAB}");
        }
    }
}
