﻿using SWS.Entidade;
using SWS.View.ViewHelper;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using SWS.Facade;

namespace SWS.View
{
    public partial class frmPcmsoAlterar : frmPcmsoIncluir
    {
        public frmPcmsoAlterar(Estudo pcmso) : base()
        {
            InitializeComponent();
            this.Pcmso = pcmso;

            /* preenchendo informações de tela */

            textCodigo.Text = pcmso.CodigoEstudo;
            ClienteFacade clienteFacade = ClienteFacade.getInstance();
            this.cliente = clienteFacade.findClienteById((long)pcmso.VendedorCliente.Cliente.Id);
            textCliente.Text = this.cliente.RazaoSocial + 
                "[ " + ValidaCampoHelper.FormataCnpj(this.cliente.Cnpj) + " ]";

            btnClienteIncluir.Enabled = false;
            btnClienteExcluir.Enabled = false;

            this.elaborador = pcmso.Tecno;
            textElaborador.Text = this.elaborador != null ? this.elaborador.Nome : string.Empty;

            dataCriacao.Text = pcmso.DataCriacao.ToString();
            dataCriacao.Checked = true;
            dataValidade.Text = pcmso.DataVencimento.ToString();
            dataValidade.Checked = true;

            ComboHelper.grauRisco(cbGrauRisco);
            cbGrauRisco.SelectedValue = pcmso.GrauRisco;

            this.contratante = pcmso.ClienteContratante;
            if (this.contratante != null)
            {
                textContratante.Text = this.contratante.RazaoSocial +
                    "[ " + ValidaCampoHelper.FormataCnpj(this.contratante.Cnpj) + " ]";
            }

            textIncioContrato.Text = pcmso.DataInicioContrato;
            textFimContrato.Text = pcmso.DataFimContrato;
            textNumeroContrato.Text = pcmso.NumContrato;

            /* dados da obra */
            textLocalEstudo.Text = pcmso.Obra;
            btnLocalEstudoIncluir.Enabled = true;
            btnLocalEstudoExcluir.Enabled = true;

            /* monta grides */
            montaGridEstimativa();
            montaGridGhe();
            montaGridHospital();
            montaGridMedico();
            montaGridAtividade();
            montaGridMateriaisEstudo();
            montaGridDocumento();

            /* alterado informações do controles */
            btnGravarNovo.Text = "Gravar";
            this.updateControl(true);

            ActiveControl = textCliente;

            /* pegando informações do cnae */
            EstudoFacade pcmsoFacade = EstudoFacade.getInstance();

            List<CnaeEstudo> cnaes = pcmsoFacade.findAllCnaEstudoByEstudo(pcmso);

            cnaeCliente = cnaes.Find(x => x.FlagCliente == true).Cnae;
            if (cnaeCliente != null)
            {
                textCnaeCliente.Text = ValidaCampoHelper.RetornaCnaeFormatado(cnaeCliente.CodCnae) +
                " - " + cnaeCliente.Atividade;
            }

            if (pcmso.ClienteContratante != null)
            {
                cnaeContratante = cnaes.Find(x => x.FlagCliente == false).Cnae;
                {
                    textCnaeContratante.Text = ValidaCampoHelper.RetornaCnaeFormatado(cnaeContratante.CodCnae) +
                    " - " + cnaeContratante.Atividade;
                }
            }

        }

        protected override void btnLocalEstudoIncluir_Click(object sender, EventArgs e)
        {
            EstudoFacade pcmsoFacede = EstudoFacade.getInstance();
            if (string.IsNullOrWhiteSpace(pcmso.Obra))
            {
                frmPcmsoObraIncluir selecionarObra = new frmPcmsoObraIncluir();
                selecionarObra.ShowDialog();

                if (selecionarObra.Obra != null)
                {
                    Obra obraNew = selecionarObra.Obra;
                    pcmso.Obra = obraNew.LocalObra;
                    pcmso.Endereco = obraNew.EnderecoObra;
                    pcmso.Numero = obraNew.NumeroObra;
                    pcmso.Complemento = obraNew.ComplementoObra;
                    pcmso.Bairro = obraNew.BairroObra;
                    pcmso.Cidade = obraNew.CidadeObra.Nome;
                    pcmso.Cep = obraNew.CepObra;
                    pcmso.Uf = obraNew.UnidadeFederativaObra;
                    pcmso.LocalObra = obraNew.DescritivoObra;

                    textLocalEstudo.Text = obraNew.LocalObra;
            
                    pcmsoFacede.updatePcmso(Pcmso);
                    MessageBox.Show("Dados do local do estudo incluído com sucesso.", "Confirmação", MessageBoxButtons.OK, MessageBoxIcon.Information);

                }
            }
            else
            {
                Obra obra = new Obra();
                obra.LocalObra = pcmso.Obra;
                obra.EnderecoObra = pcmso.Endereco;
                obra.NumeroObra = pcmso.Numero;
                obra.ComplementoObra = pcmso.Complemento;
                obra.BairroObra = pcmso.Bairro;
                obra.CepObra = pcmso.Cep;
                obra.CidadeObra = findCidadeIbgeByCidadeAndUf(pcmso.Cidade, pcmso.Uf);
                obra.UnidadeFederativaObra = pcmso.Uf;
                obra.DescritivoObra = pcmso.LocalObra;

                frmPcmsoObraAlterar alterarObra = new frmPcmsoObraAlterar(obra);
                alterarObra.ShowDialog();

                textLocalEstudo.Text = alterarObra.Obra.LocalObra;

                pcmso.Obra = alterarObra.Obra.LocalObra;
                pcmso.Endereco = alterarObra.Obra.EnderecoObra;
                pcmso.Numero = alterarObra.Obra.NumeroObra;
                pcmso.Complemento = alterarObra.Obra.ComplementoObra;
                pcmso.Bairro = alterarObra.Obra.BairroObra;
                pcmso.Cidade = alterarObra.Obra.CidadeObra.Nome;
                pcmso.Cep = alterarObra.Obra.CepObra;
                pcmso.Uf = alterarObra.Obra.UnidadeFederativaObra;
                pcmso.LocalObra = alterarObra.Obra.DescritivoObra;

                pcmsoFacede.updatePcmso(Pcmso);
                MessageBox.Show("Dados do local do estudo alterado com sucesso.", "Confirmação", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }

        }

        protected override void btnLocalEstudoExcluir_Click(object sender, EventArgs e)
        {
            try
            {
                if (string.IsNullOrWhiteSpace(pcmso.Obra))
                    throw new Exception("Você deve informar primeiro os dados da obra.");

                if (MessageBox.Show("Você deseja excluir os dados da obra?", "Atenção", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                {
                    pcmso.Obra = string.Empty;
                    pcmso.Endereco = string.Empty;
                    pcmso.Numero = string.Empty;
                    pcmso.Complemento = string.Empty;
                    pcmso.Bairro = string.Empty;
                    pcmso.Cidade = string.Empty;
                    pcmso.Cep = string.Empty;
                    pcmso.Uf = string.Empty;
                    pcmso.LocalObra = string.Empty;

                    textLocalEstudo.Text = string.Empty;

                    EstudoFacade pcmsoFacade = EstudoFacade.getInstance();
                    pcmsoFacade.updatePcmso(Pcmso);
                    MessageBox.Show("Dados do local do estudo excluído com sucesso.", "Confirmação", MessageBoxButtons.OK, MessageBoxIcon.Information);

                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }

        protected override void btObservacao_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrWhiteSpace(pcmso.Comentario))
            {
                frmPcmsoObservacao incluirObservacao = new frmPcmsoObservacao();
                incluirObservacao.ShowDialog();

                if (!string.IsNullOrWhiteSpace(incluirObservacao.Observacao))
                    pcmso.Comentario = incluirObservacao.Observacao;
            }
            else
            {
                frmPcmsoObservacao alterarObservacao = new frmPcmsoObservacao(pcmso.Comentario);
                alterarObservacao.ShowDialog();
                if (!string.IsNullOrWhiteSpace(alterarObservacao.Observacao))
                    pcmso.Comentario = alterarObservacao.Observacao;
            }

            EstudoFacade pcmsoFacade = EstudoFacade.getInstance();
            pcmsoFacade.updatePcmso(pcmso);
            MessageBox.Show("Observação incluída/alterada com sucesso.", "Confirmação", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }
    }
}
