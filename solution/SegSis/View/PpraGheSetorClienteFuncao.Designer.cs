﻿namespace SWS.View
{
    partial class frm_ppraGheSetorClienteFuncao
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frm_ppraGheSetorClienteFuncao));
            this.grb_funcao = new System.Windows.Forms.GroupBox();
            this.grd_funcao = new System.Windows.Forms.DataGridView();
            this.grb_paginacao = new System.Windows.Forms.GroupBox();
            this.btn_fechar = new System.Windows.Forms.Button();
            this.btn_confirmar = new System.Windows.Forms.Button();
            this.panel_banner.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox_banner)).BeginInit();
            this.panel.SuspendLayout();
            this.grb_funcao.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grd_funcao)).BeginInit();
            this.grb_paginacao.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel
            // 
            this.panel.Controls.Add(this.grb_paginacao);
            this.panel.Controls.Add(this.grb_funcao);
            this.panel.Location = new System.Drawing.Point(2, 38);
            this.panel.Size = new System.Drawing.Size(391, 330);
            // 
            // grb_funcao
            // 
            this.grb_funcao.Controls.Add(this.grd_funcao);
            this.grb_funcao.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.grb_funcao.Location = new System.Drawing.Point(3, 6);
            this.grb_funcao.Name = "grb_funcao";
            this.grb_funcao.Size = new System.Drawing.Size(383, 245);
            this.grb_funcao.TabIndex = 0;
            this.grb_funcao.TabStop = false;
            this.grb_funcao.Text = "Funções";
            // 
            // grd_funcao
            // 
            this.grd_funcao.AllowUserToAddRows = false;
            this.grd_funcao.AllowUserToDeleteRows = false;
            this.grd_funcao.AllowUserToOrderColumns = true;
            this.grd_funcao.AllowUserToResizeRows = false;
            this.grd_funcao.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this.grd_funcao.BackgroundColor = System.Drawing.Color.White;
            this.grd_funcao.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.grd_funcao.Location = new System.Drawing.Point(6, 19);
            this.grd_funcao.Name = "grd_funcao";
            this.grd_funcao.RowHeadersVisible = false;
            this.grd_funcao.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.grd_funcao.Size = new System.Drawing.Size(370, 223);
            this.grd_funcao.TabIndex = 0;
            // 
            // grb_paginacao
            // 
            this.grb_paginacao.Controls.Add(this.btn_fechar);
            this.grb_paginacao.Controls.Add(this.btn_confirmar);
            this.grb_paginacao.Location = new System.Drawing.Point(3, 257);
            this.grb_paginacao.Name = "grb_paginacao";
            this.grb_paginacao.Size = new System.Drawing.Size(383, 68);
            this.grb_paginacao.TabIndex = 0;
            this.grb_paginacao.TabStop = false;
            // 
            // btn_fechar
            // 
            this.btn_fechar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_fechar.Image = global::SWS.Properties.Resources.close;
            this.btn_fechar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btn_fechar.Location = new System.Drawing.Point(87, 19);
            this.btn_fechar.Name = "btn_fechar";
            this.btn_fechar.Size = new System.Drawing.Size(75, 23);
            this.btn_fechar.TabIndex = 1;
            this.btn_fechar.Text = "&Fechar";
            this.btn_fechar.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btn_fechar.UseVisualStyleBackColor = true;
            this.btn_fechar.Click += new System.EventHandler(this.btn_fechar_Click);
            // 
            // btn_confirmar
            // 
            this.btn_confirmar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_confirmar.Image = global::SWS.Properties.Resources.Gravar;
            this.btn_confirmar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btn_confirmar.Location = new System.Drawing.Point(6, 19);
            this.btn_confirmar.Name = "btn_confirmar";
            this.btn_confirmar.Size = new System.Drawing.Size(75, 23);
            this.btn_confirmar.TabIndex = 6;
            this.btn_confirmar.Text = "&Gravar";
            this.btn_confirmar.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btn_confirmar.UseVisualStyleBackColor = true;
            this.btn_confirmar.Click += new System.EventHandler(this.btn_confirmar_Click);
            // 
            // frm_ppraGheSetorClienteFuncao
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(394, 368);
            this.ControlBox = false;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "frm_ppraGheSetorClienteFuncao";
            this.Text = "INCLUIR FUNÇÃO";
            this.panel_banner.ResumeLayout(false);
            this.panel_banner.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox_banner)).EndInit();
            this.panel.ResumeLayout(false);
            this.grb_funcao.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.grd_funcao)).EndInit();
            this.grb_paginacao.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox grb_funcao;
        private System.Windows.Forms.DataGridView grd_funcao;
        private System.Windows.Forms.GroupBox grb_paginacao;
        private System.Windows.Forms.Button btn_confirmar;
        private System.Windows.Forms.Button btn_fechar;
    }
}