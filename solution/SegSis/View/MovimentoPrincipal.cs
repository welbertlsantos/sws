﻿using SWS.Entidade;
using SWS.Excecao;
using SWS.Facade;
using SWS.View.Resources;
using SWS.View.ViewHelper;
using SWS.ViewHelper;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace SWS.View
{
    public partial class frmMovimentoPrincipal : frmTemplate
    {
        private List<Movimento> colecaoMovimento = new List<Movimento>();

        public List<Movimento> ColecaoMovimento
        {
            get { return colecaoMovimento; }
            set { colecaoMovimento = value; }
        }
        private Cliente cliente;
        private Cliente credenciada;
        private Produto produto;
        private Exame exame;
        private Movimento movimento;
        private CentroCusto centroCusto;

        //variáveis auxiliares        
        Decimal valorTotal = 0;
        Int32 quantidadeItens = 0;

        public frmMovimentoPrincipal()
        {
            InitializeComponent();
            validaPermissoes();
            ComboHelper.situacaoMovimento(cb_situacao);
            ComboHelper.Boolean(cbClienteParticular, false);
        }

        private void text_nfe_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = ValidaCampoHelper.FormataCampoNumerico(sender, e);
        }

        private void btn_faturamento_Click(object sender, EventArgs e)
        {
            try
            {
                this.Cursor = Cursors.WaitCursor;
                montaLista();

                if (validaDadosFaturamento())
                {
                    frmFaturamento formFaturamento = new frmFaturamento(cliente, colecaoMovimento,valorTotal);
                    formFaturamento.ShowDialog();

                    if (formFaturamento.Nfe != null)
                        btn_limpar.PerformClick();
                        
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }

        private void btn_incluir_Click(object sender, EventArgs e)
        {
            try
            {
                if (MessageBox.Show("Deseja incluir um movimento avulso?", "Confirmação", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                {
                    frmMovimentoIncluir incluirAvulso = new frmMovimentoIncluir();
                    incluirAvulso.ShowDialog();

                    if (incluirAvulso.Inclusao)
                    {
                        cliente = incluirAvulso.Cliente;
                        text_cliente.Text = cliente.RazaoSocial;
                        dt_inicialLancamento.Text = incluirAvulso.DataInclusaoMovimento.ToString();
                        dt_inicialLancamento.Checked = true;
                        dt_finalLancamento.Text = incluirAvulso.DataInclusaoMovimento.ToString();
                        dt_finalLancamento.Checked = true;
                        btn_pesquisar.PerformClick();
                    }

                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btn_alterar_Click(object sender, EventArgs e)
        {
            try
            {

                /* validando sem foi selecionado algum produto ou exame */
                if (produto == null && exame == null)
                    throw new Exception("Para alterar um valor primeiro selecione o um produto ou um exame.");

                /* verificando a situação do movimento. somente movimentosLancados pendentes podem ser alterados */

                FinanceiroFacade financeiroFacade = FinanceiroFacade.getInstance();

                if (dg_movimento.CurrentRow == null)
                    throw new Exception("Selecione um movimento para alterar.");
                
                Movimento movimento = financeiroFacade.findMovimentoById((Int64)dg_movimento.CurrentRow.Cells[0].Value);

                if (!String.Equals(movimento.Situacao, ApplicationConstants.PENDENTE))
                    throw new Exception("Somente movimentosLancados pendentes podem ser alterados. ");
                
                montaLista();

                /* verificando se existe algum movimento filtrado que não seja pendente */
                if (colecaoMovimento.Exists(x => !String.Equals(x.Situacao, ApplicationConstants.PENDENTE)))
                    throw new Exception("Existe movimento que não é pendente. Refaça sua pesquisa informando apenas movimentosLancados pendentes para poder alterá-los.");

                frmMovimentoAlterar formMovimentoAlterar = new frmMovimentoAlterar(colecaoMovimento, dg_movimento.CurrentRow.Index);
                formMovimentoAlterar.ShowDialog();

                if (formMovimentoAlterar.Alteracao)
                    btn_pesquisar.PerformClick();

                
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btn_cancelar_Click(object sender, EventArgs e)
        {
            try
            {
                GheFonteAgenteExameAso gfaea = null;
                ClienteFuncaoExameASo cfea = null;
                ContratoProduto cp = null;
                Estudo pcmso = null;
                Aso aso = null;

                if (dg_movimento.CurrentRow == null)
                    throw new Exception("Selecione uma linha.");
                
                if (MessageBox.Show("Deseja cancelar o movimento selecionado? Será lançado um movimento para estornar o movimento selecionado." + System.Environment.NewLine + "Deseja continuar? ", "Confirmação", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                {
                    FinanceiroFacade financeiroFacade = FinanceiroFacade.getInstance();

                    bool situacao = false;

                    foreach (DataGridViewRow dvrow in dg_movimento.SelectedRows)
                    {
                        /* Verificando se o movimento pode ser cancelado. Somente movimentosLancados pendentes podem ser cancelados */

                        if (!String.Equals(ApplicationConstants.PENDENTE, (String)dvrow.Cells[3].Value))
                            throw new Exception("Somente movimentosLancados pendentes podem ser cancelados.");

                        /* montando itens de movimento GheFonteAgenteExameAso */

                        if (!String.IsNullOrEmpty(Convert.ToString(dvrow.Cells[12].Value)))
                            gfaea = new GheFonteAgenteExameAso((Int64)dvrow.Cells[12].Value, null, null, new GheFonteAgenteExame(null, new Exame((Int64)dvrow.Cells[14].Value, (String)dvrow.Cells[15].Value), null, 0, null, false, false, false), null, false, false, null, false, null, null, null, null, false, null, null, null, false, false, null, null, false, false, null, null, false, true);

                        /* ClienteFuncaoExameAso */
                        if (!String.IsNullOrEmpty(Convert.ToString(dvrow.Cells[13].Value)))
                            cfea = new ClienteFuncaoExameASo((Int64)dvrow.Cells[13].Value, null, null, new ClienteFuncaoExame(null, null, new Exame((Int64)dvrow.Cells[14].Value, (String)dvrow.Cells[15].Value), null, 0), null, false, false, null, false, null, null, null, null, false, null, null, null, false, null, null, null, null, true);

                        /* ContratoProduto */
                        if (!String.IsNullOrEmpty(Convert.ToString(dvrow.Cells[16].Value)))
                            cp = new ContratoProduto((Int64)dvrow.Cells[16].Value, new Produto((Int64)dvrow.Cells[17].Value, (String)dvrow.Cells[18].Value, string.Empty, 0, string.Empty, 0, false, string.Empty), null, null, null, null, null, String.Empty, true);

                        /* estudo */
                        if (!String.IsNullOrEmpty((Convert.ToString(dvrow.Cells[21].Value))))
                            pcmso = new Estudo((Int64)dvrow.Cells[21].Value);

                        /* Aso */
                        if (!String.IsNullOrEmpty((Convert.ToString(dvrow.Cells[19].Value))))
                            aso = new Aso((Int64)dvrow.Cells[19].Value, new ClienteFuncaoFuncionario((Int64)dvrow.Cells[26].Value, null, new Funcionario((Int64)dvrow.Cells[24].Value, (String)dvrow.Cells[25].Value, String.Empty, String.Empty, String.Empty, String.Empty, String.Empty, String.Empty, null, String.Empty, String.Empty, String.Empty, String.Empty, String.Empty, String.Empty, DateTime.Now, null, null, String.Empty, String.Empty, String.Empty, String.Empty, String.Empty, false, string.Empty, string.Empty, string.Empty, string.Empty), null, null, null, String.Empty, false, string.Empty, string.Empty, false, true), null);

                        Movimento movimentoCancelar = new Movimento((Int64)dvrow.Cells[0].Value, gfaea, cfea, new Cliente((Int64)dvrow.Cells[5].Value), cp, null, (DateTime)dvrow.Cells[1].Value, null, (String)dvrow.Cells[3].Value, (Decimal)dvrow.Cells[8].Value, null, pcmso, aso, (Int32)dvrow.Cells[9].Value, PermissionamentoFacade.usuarioAutenticado, null, (Decimal?)dvrow.Cells[30].Value, String.IsNullOrEmpty(dvrow.Cells[32].Value.ToString()) ? null : new Cliente((Int64)dvrow.Cells[32].Value), null, DateTime.Now, aso != null ? aso.CentroCusto : null, dvrow.Cells[37].Value == null ? null : new ContratoExame((long)dvrow.Cells[37].Value), null);

                        financeiroFacade.cancelaMovimento(movimentoCancelar);

                        situacao = true;
                    }

                    if (situacao)
                    {
                        MessageBox.Show("Movimento cancelado com sucesso!", "Confirmação", MessageBoxButtons.OK, MessageBoxIcon.Information);

                        /* rodando novamente a pesquisa com os itens que estão selecionados */

                        btn_pesquisar.PerformClick();
                    }
                }
                
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error );
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }

        private void btn_fechar_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btn_limpar_Click(object sender, EventArgs e)
        {
            cliente = null;
            centroCusto = null;
            text_cliente.Text = String.Empty;
            textCentroCusto.Text = string.Empty;
            ComboHelper.situacaoMovimento(cb_situacao);
            ComboHelper.Boolean(cbClienteParticular, false);
            dt_inicialFaturamento.Checked = false;
            dt_finalFaturamento.Checked = false;
            dt_inicialLancamento.Checked = false;
            dt_finalLancamento.Checked = false;
            this.text_nfe.Text = String.Empty;
            this.dg_movimento.Columns.Clear();
            this.produto = null;
            text_produto.Text = String.Empty;
            this.exame = null;
            text_exame.Text = String.Empty;
            btn_exame.Enabled = true;
            btn_produto.Enabled = true;
            /* zerando a soma de variáveis */
            quantidadeItens = 0;
            valorTotal = 0;
            //atualizaSoma();
            zeraTotal();
            this.credenciada = null;
            textCredenciada.Text = string.Empty;
        }

        private void btn_pesquisar_Click(object sender, EventArgs e)
        {
            try
            {
                this.dg_movimento.Columns.Clear();
                zeraTotal();

                if (validaPesquisa())
                {
                    /* montando movimento para pesquisa. */
                    movimento = new Movimento(null, null, null, cliente, null, !String.IsNullOrEmpty(text_nfe.Text) ? new Nfe(null, null, null, Convert.ToInt64(text_nfe.Text.Trim()), 0, DateTime.Now, null, null, 0, 0, null, null, null, null, DateTime.Now, null, string.Empty, null, string.Empty, null, null, null) : null, dt_inicialLancamento.Checked ? Convert.ToDateTime(dt_inicialLancamento.Text + " 00:00:00") : (DateTime?)null, dt_inicialFaturamento.Checked ? Convert.ToDateTime(dt_inicialFaturamento.Text + " 00:00:00") : (DateTime?)null, ((SelectItem)cb_situacao.SelectedItem).Valor, null, null, null, null, null, null, PermissionamentoFacade.usuarioAutenticado.Empresa, null, null, null, DateTime.Now, centroCusto, null, this.credenciada);

                    /* validando a pesquisa em relação a performance do sistema. */

                    if (cliente == null && String.IsNullOrEmpty(text_nfe.Text) && !dt_inicialLancamento.Checked && !dt_finalLancamento.Checked && !dt_inicialFaturamento.Checked && !dt_finalFaturamento.Checked && String.IsNullOrEmpty(((SelectItem)cb_situacao.SelectedItem).Valor))
                    {
                        if (MessageBox.Show("Sua pesquisa resultará em um grande número de informações. " + System.Environment.NewLine + "Isso poderá causar lentidão no sistema. Deseja continuar?", "Atenção", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                            montaGrid(true);
                    }
                    else
                        montaGrid(true);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message,"Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btn_cliente_Click(object sender, EventArgs e)
        {
            frmClienteSelecionar movimentoCliente = new frmClienteSelecionar(null, null, false, null, null, Convert.ToBoolean(((SelectItem)cbClienteParticular.SelectedItem).Valor), null, null, null);
            movimentoCliente.ShowDialog();

            if (movimentoCliente.Cliente != null)
            {
                cliente = movimentoCliente.Cliente;
                text_cliente.Text = cliente.RazaoSocial;
                dg_movimento.Columns.Clear();
            }
        }

        private void btn_produto_Click(object sender, EventArgs e)
        {
            try
            {
                frmProdutoBusca formMovimentoProduto = new frmProdutoBusca();
                formMovimentoProduto.ShowDialog();

                if (formMovimentoProduto.Produto != null)
                {
                    exame = null;
                    text_exame.Text = String.Empty;
                    btn_exame.Enabled = false;
                    dg_movimento.Columns.Clear();
                    colecaoMovimento.Clear();

                    produto = formMovimentoProduto.Produto;
                    text_produto.Text = produto.Nome;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void btn_exame_Click(object sender, EventArgs e)
        {
            try
            {
                frmExameBuscar buscarExame = new frmExameBuscar();
                buscarExame.ShowDialog();

                if (buscarExame.Exame != null)
                {
                    produto = null;
                    text_produto.Text = String.Empty;
                    btn_produto.Enabled = false;
                    dg_movimento.Columns.Clear();
                    colecaoMovimento.Clear();

                    exame = buscarExame.Exame;
                    text_exame.Text = exame.Descricao;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void chk_marcarTodos_CheckedChanged(object sender, EventArgs e)
        {
            try
            {

                this.Cursor = Cursors.WaitCursor;

                if (chk_marcarTodos.Checked == true)
                    montaGrid(true);
                else
                {
                    montaGrid(false);
                    
                    // zerando os totalizadores
                    valorTotal = 0;
                    quantidadeItens = 0;
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }

        private void dg_movimento_ColumnAdded(object sender, DataGridViewColumnEventArgs e)
        {
            e.Column.SortMode = DataGridViewColumnSortMode.NotSortable;
        }

        private void dg_movimento_RowPrePaint(object sender, DataGridViewRowPrePaintEventArgs e)
        {
            DataGridView dgv = sender as DataGridView;

            if (String.Equals((String)dgv.Rows[e.RowIndex].Cells[3].Value, "C"))
                dgv.Rows[e.RowIndex].DefaultCellStyle.ForeColor = Color.Red;

            if (String.Equals((String)dgv.Rows[e.RowIndex].Cells[3].Value, "F"))
                dgv.Rows[e.RowIndex].DefaultCellStyle.ForeColor = Color.Green;

        }

        private void validaPermissoes()
        {
            PermissionamentoFacade permissionamentoFacade = PermissionamentoFacade.getInstance();

            btn_faturamento.Enabled = permissionamentoFacade.hasPermission("MOVIMENTO", "FATURAR");
            btn_alterar.Enabled = permissionamentoFacade.hasPermission("MOVIMENTO", "ALTERAR");

        }

        public void montaGrid(bool check)
        {
            try
            {
                FinanceiroFacade financeiroFacade = FinanceiroFacade.getInstance();

                this.Cursor = Cursors.WaitCursor;

                DataSet ds = financeiroFacade.findMovimentoByFilter(movimento, dt_finalLancamento.Checked? Convert.ToDateTime(dt_finalLancamento.Text + " 23:59:59") : (DateTime?)null, dt_finalFaturamento.Checked? Convert.ToDateTime(dt_finalFaturamento.Text + " 23:59:59") : (DateTime?)null, check, Convert.ToBoolean(((SelectItem)cbClienteParticular.SelectedItem).Valor), produto, exame, dataInicialAtendimento.Checked ? Convert.ToDateTime(dataInicialAtendimento.Text + " 00:00:00") : (DateTime?)null, dataFinalAtendimento.Checked ? Convert.ToDateTime(dataFinalAtendimento.Text + " 23:59:59" ) : (DateTime?)null );

                this.dg_movimento.DataSource = ds.Tables["Movimentos"].DefaultView;

                this.dg_movimento.Columns[0].HeaderText = "ID Movimento";
                this.dg_movimento.Columns[0].Visible = false;
                this.dg_movimento.Columns[0].ReadOnly = true;

                this.dg_movimento.Columns[1].HeaderText = "Data Inclusão";
                this.dg_movimento.Columns[1].ReadOnly = true;

                this.dg_movimento.Columns[2].HeaderText = "Data do Faturamento";
                this.dg_movimento.Columns[2].ReadOnly = true;

                this.dg_movimento.Columns[3].HeaderText = "Situação";
                this.dg_movimento.Columns[3].ReadOnly = true;

                this.dg_movimento.Columns[4].HeaderText = "Nota Fiscal";
                this.dg_movimento.Columns[4].ReadOnly = true;
                
                this.dg_movimento.Columns[5].HeaderText = "ID Cliente";
                this.dg_movimento.Columns[5].ReadOnly = true;
                this.dg_movimento.Columns[5].Visible = false;

                this.dg_movimento.Columns[6].HeaderText = "CNPJ";
                this.dg_movimento.Columns[6].ReadOnly = true;
                
                this.dg_movimento.Columns[7].HeaderText = "Cliente";
                this.dg_movimento.Columns[7].ReadOnly = true;
                
                this.dg_movimento.Columns[8].HeaderText = "Preço Unitário";
                this.dg_movimento.Columns[8].ReadOnly = true;
                this.dg_movimento.Columns[8].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
                this.dg_movimento.Columns[8].HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleRight;
                this.dg_movimento.Columns[8].DefaultCellStyle.BackColor = Color.White;
                
                this.dg_movimento.Columns[9].HeaderText = "Quantidade";
                this.dg_movimento.Columns[9].ReadOnly = true;
                this.dg_movimento.Columns[9].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
                this.dg_movimento.Columns[9].HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleRight;

                this.dg_movimento.Columns[10].HeaderText = "Valor Comissão";
                this.dg_movimento.Columns[10].Visible = false;
                this.dg_movimento.Columns[10].ReadOnly = true;
                this.dg_movimento.Columns[10].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
                this.dg_movimento.Columns[10].HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleRight;

                this.dg_movimento.Columns[11].HeaderText = "Usuário";
                this.dg_movimento.Columns[11].ReadOnly = true;

                this.dg_movimento.Columns[12].HeaderText = "ID_ghe_fonte_agente_exame_aso";
                this.dg_movimento.Columns[12].Visible = false;
                this.dg_movimento.Columns[12].ReadOnly = true;

                this.dg_movimento.Columns[13].HeaderText = "ID_cliente_função_exame_aso";
                this.dg_movimento.Columns[13].Visible = false;
                this.dg_movimento.Columns[13].ReadOnly = true;

                this.dg_movimento.Columns[14].HeaderText = "ID_exame";
                this.dg_movimento.Columns[14].Visible = false;
                this.dg_movimento.Columns[14].ReadOnly = true;

                this.dg_movimento.Columns[15].HeaderText = "Nome do Exame";
                this.dg_movimento.Columns[15].ReadOnly = true;

                this.dg_movimento.Columns[16].HeaderText = "ID_produto_contrato";
                this.dg_movimento.Columns[16].Visible = false;
                this.dg_movimento.Columns[16].ReadOnly = true;

                this.dg_movimento.Columns[17].HeaderText = "ID_produto";
                this.dg_movimento.Columns[17].Visible = false;

                this.dg_movimento.Columns[18].HeaderText = "Produto";
                this.dg_movimento.Columns[18].ReadOnly = true;

                this.dg_movimento.Columns[19].HeaderText = "ID_ASO";
                this.dg_movimento.Columns[19].Visible = false;
                this.dg_movimento.Columns[19].ReadOnly = true;

                this.dg_movimento.Columns[20].HeaderText = "Código do ASO";
                this.dg_movimento.Columns[20].ReadOnly = true;
                
                this.dg_movimento.Columns[21].HeaderText = "id Estudo";
                this.dg_movimento.Columns[21].Visible = false;
                this.dg_movimento.Columns[21].ReadOnly = true;

                this.dg_movimento.Columns[22].HeaderText = "Código Estudo";
                this.dg_movimento.Columns[22].ReadOnly = true;
                
                this.dg_movimento.Columns[23].HeaderText = "Id_usuario";
                this.dg_movimento.Columns[23].Visible = false;
                this.dg_movimento.Columns[23].ReadOnly = true;

                this.dg_movimento.Columns[24].HeaderText = "Id_funcionario";
                this.dg_movimento.Columns[24].Visible = false;
                this.dg_movimento.Columns[24].ReadOnly = true;

                this.dg_movimento.Columns[25].HeaderText = "Nome do Funcionário";
                this.dg_movimento.Columns[25].ReadOnly = true;
                
                this.dg_movimento.Columns[26].HeaderText = "Id_Cliente_Funcao_Funcionario";
                this.dg_movimento.Columns[26].Visible = false;
                this.dg_movimento.Columns[26].ReadOnly = true;

                this.dg_movimento.Columns[27].HeaderText = "Selec";
                this.dg_movimento.Columns[27].Width = 30;

                this.dg_movimento.Columns[28].HeaderText = "Id_Empresa";
                this.dg_movimento.Columns[28].Visible = false;
                this.dg_movimento.Columns[28].ReadOnly = true;

                this.dg_movimento.Columns[29].HeaderText = "Empresa";
                this.dg_movimento.Columns[29].ReadOnly = true;
                
                this.dg_movimento.Columns[30].HeaderText = "Custo Unitário";
                this.dg_movimento.Columns[30].ReadOnly = true;
                this.dg_movimento.Columns[30].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
                this.dg_movimento.Columns[30].HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleRight;
                this.dg_movimento.Columns[30].DefaultCellStyle.BackColor = Color.White;
                
                this.dg_movimento.Columns[31].HeaderText = "Prestador";
                this.dg_movimento.Columns[31].Visible = true;
                this.dg_movimento.Columns[31].ReadOnly = true;

                this.dg_movimento.Columns[32].HeaderText = "id_unidade";
                this.dg_movimento.Columns[32].Visible = false;
                this.dg_movimento.Columns[32].ReadOnly = true;

                this.dg_movimento.Columns[33].HeaderText = "Unidade";
                this.dg_movimento.Columns[33].Visible = true;
                this.dg_movimento.Columns[33].ReadOnly = true;
                
                this.dg_movimento.Columns[34].HeaderText = "Data de Gravação";
                this.dg_movimento.Columns[34].Visible = false;
                this.dg_movimento.Columns[34].ReadOnly = true;

                this.dg_movimento.Columns[35].HeaderText = "idCentroCusto";
                this.dg_movimento.Columns[35].Visible = false;

                this.dg_movimento.Columns[36].HeaderText = "Centro de Custo";
                this.dg_movimento.Columns[36].ReadOnly = true;

                this.dg_movimento.Columns[37].HeaderText = "idContratoExame";
                this.dg_movimento.Columns[37].Visible = false;

                /* ordenaçao da grid */
                this.dg_movimento.Columns[27].DisplayIndex = 0;
                this.dg_movimento.Columns[6].DisplayIndex = 1;
                this.dg_movimento.Columns[7].DisplayIndex = 2;
                this.dg_movimento.Columns[25].DisplayIndex = 3;
                this.dg_movimento.Columns[1].DisplayIndex = 4;
                this.dg_movimento.Columns[15].DisplayIndex = 5;
                this.dg_movimento.Columns[18].DisplayIndex = 6;
                this.dg_movimento.Columns[8].DisplayIndex = 7;
                this.dg_movimento.Columns[30].DisplayIndex = 8;
                this.dg_movimento.Columns[9].DisplayIndex = 9;
                this.dg_movimento.Columns[2].DisplayIndex = 10;
                this.dg_movimento.Columns[4].DisplayIndex = 11;
                this.dg_movimento.Columns[3].DisplayIndex = 12;
                this.dg_movimento.Columns[20].DisplayIndex = 13;
                this.dg_movimento.Columns[22].DisplayIndex = 14;
                this.dg_movimento.Columns[29].DisplayIndex = 15;
                this.dg_movimento.Columns[11].DisplayIndex = 16;
                this.dg_movimento.Columns[31].DisplayIndex = 17;
                this.dg_movimento.Columns[31].DisplayIndex = 18;
                

                atualizaSoma();

                chk_marcarTodos.Enabled = true;

                /* preenchendo totais */

                textQuantidade.Text = ds.Tables["Total"].Rows[0]["quantidade"].ToString();
                textValor.Text = ValidaCampoHelper.FormataValorMonetario(ds.Tables["Total"].Rows[0]["valor"].ToString());
                textQuantidadeProduto.Text = ds.Tables["Total"].Rows[0]["quantidadeProduto"].ToString();
                textQuantidadeExame.Text = ds.Tables["Total"].Rows[0]["quantidadeExame"].ToString();

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }

        public bool validaPesquisa()
        {
            bool retorno = true;
            try
            {
                /* data lançamento */
                if (dt_finalLancamento.Checked && !dt_inicialLancamento.Checked)
                    throw new Exception("Você deve marcar a data incial de lançamento.");

                if (dt_inicialLancamento.Checked && !dt_finalLancamento.Checked)
                    throw new Exception("Você deve marcar a data final de lançamento.");
            
                if (Convert.ToDateTime(dt_inicialLancamento.Text) > Convert.ToDateTime(dt_finalLancamento.Text))
                    throw new Exception("Lançamento: A data final não pode ser menor que a data inicial.");
     
                /* data faturamento */
                if (dt_inicialFaturamento.Checked && !dt_finalFaturamento.Checked)
                    throw new Exception("Você deve marcar a data final de faturamento");

                if (dt_finalFaturamento.Checked && !dt_inicialFaturamento.Checked)
                    throw new Exception("Você deve marcar a data inicial de faturamento");

                if (Convert.ToDateTime(dt_inicialFaturamento.Text) > Convert.ToDateTime(dt_finalFaturamento.Text))
                    throw new Exception("Faturamento: A data final não pode ser menor que a data inicial.");

                /* data atendimento */
                if (dataInicialAtendimento.Checked && !dataFinalAtendimento.Checked)
                    throw new Exception("Você deve selecionar a data final do atendimento.");

                if (dataFinalAtendimento.Checked && !dataInicialAtendimento.Checked)
                    throw new Exception("Você deve selecionar a data inicial do atendimento.");

                if (Convert.ToDateTime(dataInicialAtendimento.Value) > Convert.ToDateTime(dataFinalAtendimento.Value))
                    throw new Exception("A data inicial do atendimento não pode se maior que a data Final.");
            }
            catch (Exception ex)
            {
                retorno = false;
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            
            return retorno;
        }

        public void montaLista()
        {
            colecaoMovimento.Clear();

            /* zerando valor total */
            valorTotal = 0;

            foreach (DataGridViewRow dvRom in dg_movimento.Rows)
            {
                if (Convert.ToBoolean(dvRom.Cells[27].Value) == true)
                {
                    Movimento movimento = new Movimento();
                    movimento.Id = (long)dvRom.Cells[0].Value;

                    if (dvRom.Cells[12].Value != DBNull.Value)
                    {
                        GheFonteAgenteExameAso gheFonteAgenteExameAso = new GheFonteAgenteExameAso();
                        GheFonteAgenteExame gheFonteAgenteExame = new GheFonteAgenteExame();
                        Exame exame = new Exame();
                        exame.Id = (long)dvRom.Cells[14].Value;
                        exame.Descricao = dvRom.Cells[15].Value.ToString();
                        gheFonteAgenteExame.Exame = exame;
                        gheFonteAgenteExameAso.GheFonteAgenteExame = gheFonteAgenteExame;
                        movimento.GheFonteAgenteExameAso = gheFonteAgenteExameAso;
                    }
                    if (dvRom.Cells[13].Value != DBNull.Value)
                    {
                        ClienteFuncaoExameASo clienteFuncaoExameAso = new ClienteFuncaoExameASo();
                        ClienteFuncaoExame clienteFuncaoExame = new ClienteFuncaoExame();
                        Exame exame = new Exame();
                        exame.Id = (long)dvRom.Cells[14].Value;
                        exame.Descricao = dvRom.Cells[15].Value.ToString();
                        clienteFuncaoExame.Exame = exame;
                        clienteFuncaoExameAso.ClienteFuncaoExame = clienteFuncaoExame;
                        movimento.ClienteFuncaoExameAso = clienteFuncaoExameAso;
                    }

                    Cliente cliente = new Cliente();
                    cliente.Id = (long)dvRom.Cells[5].Value;
                    cliente.Cnpj = dvRom.Cells[6].Value.ToString();
                    cliente.RazaoSocial = dvRom.Cells[7].Value.ToString();
                    movimento.Cliente = cliente;

                    if (dvRom.Cells[16].Value != DBNull.Value)
                    {
                        ContratoProduto contratoProduto = new ContratoProduto();
                        Produto produto = new Produto();
                        produto.Id = (long)dvRom.Cells[17].Value;
                        produto.Descricao = dvRom.Cells[18].Value.ToString();
                        contratoProduto.Id = (long)dvRom.Cells[16].Value;
                        contratoProduto.Produto = produto;
                        movimento.ContratoProduto = contratoProduto;
                    }

                    movimento.DataInclusao = Convert.ToDateTime(dvRom.Cells[1].Value);
                    movimento.DataGravacao = Convert.ToDateTime(dvRom.Cells[34].Value);
                    movimento.DataFaturamento = dvRom.Cells[2].Value != DBNull.Value ? Convert.ToDateTime(dvRom.Cells[2].Value) : (DateTime?)null;
                    movimento.Situacao = dvRom.Cells[3].Value.ToString();
                    movimento.PrecoUnit = Convert.ToDecimal(dvRom.Cells[8].Value);
                    movimento.Quantidade = Convert.ToInt32(dvRom.Cells[9].Value);
                    movimento.ValorComissao = dvRom.Cells[10].Value != DBNull.Value ? Convert.ToDecimal(dvRom.Cells[10].Value) : 0;
                    movimento.CustoUnitario = Convert.ToDecimal(dvRom.Cells[30].Value);


                    if (dvRom.Cells[4].Value != DBNull.Value)
                    {
                        Nfe notaFiscal = new Nfe();
                        notaFiscal.Id = (long)dvRom.Cells[4].Value;
                        movimento.Nfe = notaFiscal;
                    }

                    Usuario usuario = new Usuario();
                    usuario.Id = (long)dvRom.Cells[23].Value;
                    usuario.Nome = dvRom.Cells[11].Value.ToString();

                    movimento.Usuario = usuario;

                    if (dvRom.Cells[19].Value != DBNull.Value)
                    {
                        Aso atendimento = new Aso();
                        atendimento.Id = (long)dvRom.Cells[19].Value;
                        atendimento.Codigo = dvRom.Cells[20].Value.ToString();

                        if (dvRom.Cells[26].Value != null)
                        {
                            ClienteFuncaoFuncionario clienteFuncaoFuncionario = new ClienteFuncaoFuncionario();
                            clienteFuncaoFuncionario.Id = (long)dvRom.Cells[26].Value;
                            Funcionario funcionario = new Funcionario();
                            funcionario.Id = (long)dvRom.Cells[24].Value;
                            funcionario.Nome = dvRom.Cells[25].Value.ToString();
                            clienteFuncaoFuncionario.Funcionario = funcionario;
                            atendimento.ClienteFuncaoFuncionario = clienteFuncaoFuncionario;
                        }

                        movimento.Aso = atendimento;
                    }

                    if (dvRom.Cells[21].Value != DBNull.Value)
                    {
                        Estudo pcmso = new Estudo();
                        pcmso.Id = (long)dvRom.Cells[21].Value;
                        pcmso.CodigoEstudo = dvRom.Cells[22].Value.ToString();
                        movimento.Estudo = pcmso;
                    }

                    Empresa empresa = new Empresa();
                    empresa.Id = (long)dvRom.Cells[28].Value;
                    empresa.RazaoSocial = dvRom.Cells[29].Value.ToString();
                    movimento.Empresa = empresa;

                    if (dvRom.Cells[32].Value != DBNull.Value)
                    {
                        Cliente unidade = new Cliente();
                        unidade.Id = (long)dvRom.Cells[32].Value;
                        unidade.RazaoSocial = dvRom.Cells[33].Value.ToString();
                        movimento.Unidade = unidade;
                    }

                    if (dvRom.Cells[35].Value != DBNull.Value)
                    {
                        CentroCusto centroCusto = new CentroCusto();
                        centroCusto.Id = (long)dvRom.Cells[35].Value;
                        centroCusto.Descricao = dvRom.Cells[36].Value.ToString();
                    }

                    if (dvRom.Cells[37].Value != DBNull.Value)
                    {
                        movimento.ContratoExame = new ContratoExame((long)dvRom.Cells[37].Value);
                    }

                    colecaoMovimento.Add(movimento);
                    //colecaoMovimento.Add(new Movimento((Int64)dvRom.Cells[0].Value, !String.IsNullOrEmpty(dvRom.Cells[12].Value.ToString()) ? new GheFonteAgenteExameAso((Int64)dvRom.Cells[12].Value, null, null, new GheFonteAgenteExame(null, new Exame((Int64)dvRom.Cells[14].Value, (String)dvRom.Cells[15].Value), null, 0, String.Empty, false, false, false), null, false, false, String.Empty, false, null, null, null, null, false, null, String.Empty, String.Empty, false, false, null, null, false, false, null, null, false) : null, !String.IsNullOrEmpty(dvRom.Cells[13].Value.ToString()) ? new ClienteFuncaoExameASo((Int64)dvRom.Cells[13].Value, null, null, new ClienteFuncaoExame(null, null, new Exame((Int64)dvRom.Cells[14].Value, (String)dvRom.Cells[15].Value), string.Empty, 0), null, false, false, string.Empty, false, null, null, null, null, false, null, String.Empty, String.Empty, false, null, null, null, null) : null, new Cliente((Int64)dvRom.Cells[5].Value), !String.IsNullOrEmpty(dvRom.Cells[16].Value.ToString()) ? new ContratoProduto((Int64)dvRom.Cells[16].Value, new Produto((Int64)dvRom.Cells[17].Value, (String)dvRom.Cells[18].Value, string.Empty, 0, string.Empty, 0, false, string.Empty), null, null, null, null, null, String.Empty, true) : null, !String.IsNullOrEmpty(dvRom.Cells[4].Value.ToString()) ? new Nfe((Int64)dvRom.Cells[4].Value, null, null, null, 0, DateTime.Now, null, null, 0, 0, null, null, null, null, DateTime.Now, null, String.Empty, null, String.Empty, null, null, null) : null, Convert.ToDateTime(dvRom.Cells[1].Value), !String.IsNullOrEmpty(dvRom.Cells[2].Value.ToString()) ? ((DateTime?)dvRom.Cells[2].Value) : null, (String)dvRom.Cells[3].Value,(Decimal)dvRom.Cells[8].Value, !String.IsNullOrEmpty(dvRom.Cells[10].Value.ToString()) ? (Decimal?)dvRom.Cells[10].Value : null, !String.IsNullOrEmpty(dvRom.Cells[21].Value.ToString()) ? new Estudo((Int64)dvRom.Cells[21].Value) : null, !String.IsNullOrEmpty(dvRom.Cells[19].Value.ToString()) ? new Aso((Int64)dvRom.Cells[19].Value, null, !String.IsNullOrEmpty(dvRom.Cells[26].Value.ToString()) ? new ClienteFuncaoFuncionario((Int64)dvRom.Cells[26].Value, null, new Funcionario((Int64)dvRom.Cells[24].Value, (String)dvRom.Cells[25].Value, String.Empty, String.Empty, String.Empty, String.Empty, String.Empty, String.Empty, null, String.Empty, String.Empty, String.Empty, String.Empty, String.Empty, String.Empty, DateTime.Now, null, null, String.Empty, String.Empty, String.Empty, String.Empty, String.Empty, false, string.Empty, string.Empty, string.Empty, string.Empty), null, null, null, string.Empty, false, string.Empty, string.Empty, false, true) : null, dvRom.Cells[20].Value.ToString(), DateTime.Now, null, null, null, String.Empty, null, null, String.Empty, null, null, null, null, null, null, String.Empty, String.Empty, String.Empty, null, null, string.Empty, null, null, null, string.Empty, false, string.Empty, string.Empty, string.Empty) : null, (Int32)dvRom.Cells[9].Value, new Usuario((Int64)dvRom.Cells[23].Value, (String)dvRom.Cells[11].Value, String.Empty, String.Empty, String.Empty, null, String.Empty, String.Empty, String.Empty, String.Empty, null, null, String.Empty, false, null, String.Empty, false, false, string.Empty, string.Empty), null, (Decimal?)dvRom.Cells[30].Value, String.IsNullOrEmpty(dvRom.Cells[32].Value.ToString()) ? null : new Cliente((Int64)dvRom.Cells[32].Value), null, DateTime.Now, dvRom.Cells[35].Value == DBNull.Value ? null : new CentroCusto((long)dvRom.Cells[35].Value, null, dvRom.Cells[36].Value.ToString(), string.Empty)));

                    valorTotal += ((Decimal)dvRom.Cells[8].Value * (Int32)dvRom.Cells[9].Value);
                }
            }
            
            quantidadeItens = colecaoMovimento.Count;

        }

        private bool validaDadosFaturamento()
        {
            Boolean retorno = true;
            try
            {
                if (!String.Equals(((SelectItem)cb_situacao.SelectedItem).Valor, "P"))
                {
                    this.dg_movimento.Columns.Clear(); 
                    throw new Exception("Obrigatório selecionar a situação pendente para iniciar o faturamento" + System.Environment.NewLine + "Refaça sua pesquisa.");
                }

                if (dg_movimento.RowCount == 0)
                    throw new Exception("Sem movimento para faturar. Refaça sua pesquisa.");

                if (cliente == null)
                {
                    this.dg_movimento.Columns.Clear();
                    throw new Exception("Obrigatório selecionar o cliente para faturamento." + System.Environment.NewLine + "Refaça sua pesquisa.");
                }

                if (colecaoMovimento.Count == 0)
                {
                    throw new Exception("Nenhum movimento foi selecionado para faturamento.");
                }

            }
            catch (Exception ex)
            {
                retorno = false;
                zeraTotal();
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);

            }
            return retorno;
        }

        public void zeraTotal()
        {
            this.textQuantidade.Text = String.Empty;
            this.textQuantidadeExame.Text = String.Empty;
            this.textValor.Text = String.Empty;
            this.textQuantidadeProduto.Text = String.Empty;
        }

        public void atualizaSoma()
        {
            try
            {
                FinanceiroFacade financeiroFacade = FinanceiroFacade.getInstance();

                // preenchendo campos do totalizador.
                quantidadeItens = dg_movimento.Rows.Count;
                valorTotal = financeiroFacade.findSomaMovimentoByFilter (movimento, dt_finalLancamento.Checked? Convert.ToDateTime(dt_finalLancamento.Text + " 23:59:59") : (DateTime?)null, dt_finalFaturamento.Checked? Convert.ToDateTime(dt_finalFaturamento.Text + " 23:59:59") : (DateTime?)null);

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }

        private void frmMovimentoPrincipal_KeyDown(object sender, KeyEventArgs e)
        {

        }

        private void btnCentroCusto_Click(object sender, EventArgs e)
        {
            try
            {
                if (cliente == null)
                    throw new Exception("Selecione um cliente.");

                if (!cliente.UsaCentroCusto)
                    throw new Exception("Cliente selecionado não trabalha com centro de custo.");

                frmClienteCentroCustoSelecionar selecionarCentroCusto = new frmClienteCentroCustoSelecionar(cliente);
                selecionarCentroCusto.ShowDialog();

                if (selecionarCentroCusto.CentroCustoSelecionado != null)
                {
                    centroCusto = selecionarCentroCusto.CentroCustoSelecionado;
                    textCentroCusto.Text = centroCusto.Descricao;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btnExcluirCliente_Click(object sender, EventArgs e)
        {
            try
            {
                if (cliente == null)
                    throw new Exception("Você deve selecionar primeiro o cliente.");

                cliente = null;
                text_cliente.Text = string.Empty;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btnExcluirCentroCusto_Click(object sender, EventArgs e)
        {
            try
            {
                if (centroCusto == null)
                    throw new Exception("Você deve selecionar primeiro o centro de custo.");

                centroCusto = null;
                textCentroCusto.Text = string.Empty;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btnExcluirProduto_Click(object sender, EventArgs e)
        {
            try
            {
                if (produto == null)
                    throw new Exception("Você deve selecionar primeiro o produto.");

                produto = null;
                text_produto.Text = string.Empty;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btnExcluirExame_Click(object sender, EventArgs e)
        {
            try
            {
                if (exame == null)
                    throw new Exception("Você deve selecionar primeiro o exame.");

                exame = null;
                text_exame.Text = string.Empty;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btnCredenciada_Click(object sender, EventArgs e)
        {
            try
            {
                if (this.cliente == null)
                    throw new Exception("É necessário selecionar primeiro o cliente e esse deve ser uma credenciadora.");

                if (this.cliente.Credenciadora == false)
                    throw new Exception("O cliente selecionado não é uma empresa credenciadora.");

                frmClienteCrendiadaSelecionar selecionarCredenciada = new frmClienteCrendiadaSelecionar(this.cliente);
                selecionarCredenciada.ShowDialog();

                if (selecionarCredenciada.Credenciada != null)
                {
                    this.credenciada = selecionarCredenciada.Credenciada;
                    textCredenciada.Text = this.credenciada.RazaoSocial;
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btnExcluirCredenciada_Click(object sender, EventArgs e)
        {
            try
            {
                if (this.credenciada == null)
                    throw new Exception("Deve selecionar primeiro uma credenciada antes de excluí-la.");

                this.credenciada = null;
                textCredenciada.Text = string.Empty;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }

        
    }
}
