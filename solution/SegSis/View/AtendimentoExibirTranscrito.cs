﻿using SWS.Entidade;
using SWS.Facade;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace SWS.View
{
    public partial class frmAtendimentoExibirTranscrito : frmTemplateConsulta
    {
        Aso atendimento;
        LinkedList<ItemFilaAtendimento> exameCancelado = new LinkedList<ItemFilaAtendimento>();
        
        public frmAtendimentoExibirTranscrito(Aso atendimento)
        {
            InitializeComponent();
            this.atendimento = atendimento;
            montaDataGrid();
        }

        private void btn_fechar_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void montaDataGrid()
        {
            try
            {
                this.Cursor = Cursors.WaitCursor;

                this.dgv_exame.Columns.Clear();

                FilaFacade filaFacede = FilaFacade.getInstance();

                exameCancelado = filaFacede.buscaExamesInSituacaoByAso(atendimento, null, null, null, null, true);

                dgv_exame.ColumnCount = 7;

                dgv_exame.Columns[0].HeaderText = "Código Exame";

                dgv_exame.Columns[1].HeaderText = "Exame";
                
                dgv_exame.Columns[2].HeaderText = "Exame de Laboratório?";
                dgv_exame.Columns[2].HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleCenter;
                dgv_exame.Columns[2].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;

                dgv_exame.Columns[3].HeaderText = "Exame Externo?";
                dgv_exame.Columns[3].HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleCenter;
                dgv_exame.Columns[3].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;

                dgv_exame.Columns[4].HeaderText = "Sala";
                
                dgv_exame.Columns[5].HeaderText = "Andar";
                
                dgv_exame.Columns[6].HeaderText = "IdItem";
                dgv_exame.Columns[6].Visible = false;
                

                foreach (ItemFilaAtendimento itemFila in exameCancelado)
                    dgv_exame.Rows.Add(itemFila.Exame.Id, itemFila.Exame.Descricao,
                        (bool)itemFila.Exame.Laboratorio ? "Lab" : null, (Boolean)itemFila.Exame.Externo ? "Ext" : null, itemFila.NomeSala, itemFila.Andar, itemFila.IdItem, itemFila.TipoExame);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }
    }
}
