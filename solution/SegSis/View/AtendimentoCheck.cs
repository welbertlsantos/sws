﻿using SWS.Entidade;
using SWS.Excecao;
using SWS.Facade;
using SWS.Resources;
using SWS.View.Resources;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace SWS.View
{
    public partial class frmAtendimentoCheck : frmTemplate
    {
        private Aso atendimento;

        private LinkedList<ItemFilaAtendimento> examePendente = new LinkedList<ItemFilaAtendimento>();
        private LinkedList<ItemFilaAtendimento> exameRealizado = new LinkedList<ItemFilaAtendimento>();
        private LinkedList<ItemFilaAtendimento> exameEmAtendimento = new LinkedList<ItemFilaAtendimento>();

        private Cliente prestador = null;
        
        public frmAtendimentoCheck(Aso atendimento)
        {
            InitializeComponent();
            validaPermissoes();
            this.atendimento = atendimento;
            montaGridExamePendente();
            montaGridExameRealizado();
            montaGridExameEmAtendimento();
        }

        private void btn_exibir_transcritos_Click(object sender, EventArgs e)
        {
            frmAtendimentoExibirTranscrito formAsoExibirTrascritos = new frmAtendimentoExibirTranscrito(atendimento);
            formAsoExibirTrascritos.ShowDialog();
        }

        private void btn_cancelado_Click(object sender, EventArgs e)
        {
            frmAtendimentoExibirCancelado formAsoExibirCancelado = new frmAtendimentoExibirCancelado(atendimento);
            formAsoExibirCancelado.ShowDialog();
        }

        private void btn_fechar_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btn_finalizar_Click(object sender, EventArgs e)
        {
            String geraMovimento = String.Empty;
            ItemFilaAtendimento itemFila = null;
            SalaExame salaAtendimento = null;

            try
            {
                
                this.Cursor = Cursors.WaitCursor;

                FilaFacade filaFacade = FilaFacade.getInstance();
                FinanceiroFacade financeiroFacade = FinanceiroFacade.getInstance();
                EstudoFacade pcmsoFacade = EstudoFacade.getInstance();
                ClienteFacade clienteFacade = ClienteFacade.getInstance();
                AsoFacade asoFacade = AsoFacade.getInstance();

                ClienteFuncaoExameASo clienteFuncaoExameAso = null;
                GheFonteAgenteExameAso gheFonteAgenteExameAso = null;
                Cliente prestadorItem = null;

                /* verificando se foi selecionado algum exame */

                bool flagCheck = false;
                foreach (DataGridViewRow dvRow in dg_examePendente.Rows)
                {
                    if ((bool)dvRow.Cells[8].Value == true)
                    {
                        flagCheck = true;
                        break;
                    }
                }

                if (!flagCheck)
                    throw new Exception("Você deve selecionar pelo menos um item na gride.");

                if (MessageBox.Show("Deseja finalizar o atendimento do(s) exame(s) selecionado(s)?", "Confirmação", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                {

                /* montando item da fila. 
                 * verificando se já existe uma sala para o exame. Caso exista, então a sala será mantida
                 * para garantir a coesão dos dados */

                    bool flagAtualizado = false;
                    foreach (DataGridViewRow dvRow in dg_examePendente.Rows)
                    {
                        if ((bool)dvRow.Cells[8].Value == true)
                        {
                            /* verificando o exame que atende a sala. Caso o exame seja atendimento por mais de uma sala então será necessário o usuário selecionar
                             * a sala que atendeu o exame */

                            List<SalaExame> salaExameAtendido = filaFacade.findAllSalaExameAtivoByExame(new Exame((long)dvRow.Cells[0].Value, (string)dvRow.Cells[1].Value));

                            if (salaExameAtendido.Count > 1)
                            {
                                /* mais de uma sala que atende o exame. O usuário deverá selecionar a sala que foi atendido para continuar */
                                frmAtendimentoCheckSalaAtendeExame selecionarSala = new frmAtendimentoCheckSalaAtendeExame(salaExameAtendido);
                                selecionarSala.ShowDialog();

                                if (selecionarSala.Sala != null)
                                {
                                    salaAtendimento = selecionarSala.Sala;
                                }

                            }
                            else
                            {
                                if (salaExameAtendido.Count == 0)
                                {
                                    MessageBox.Show("O Exame: " + dvRow.Cells[0].Value.ToString() + " - " + dvRow.Cells[1].Value + " não está cadastrado em nenhuma sala. O Atendimento será finalizado sem sala e não será possível laudar pela fila de atendimento.", "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                                    salaAtendimento = null;
                                }
                                else
                                {
                                    salaAtendimento = salaExameAtendido[0];
                                }
                            }
                            
                            itemFila = new ItemFilaAtendimento((Int64)dvRow.Cells[4].Value, null, null, null, null, null, null, null, (String)dvRow.Cells[5].Value, null, false, DateTime.Now, null, salaAtendimento == null ? null : salaAtendimento.Sala.Descricao, salaAtendimento == null ? null : (Int32?)salaAtendimento.Sala.Andar, null, null, null, null, null, null, String.Empty, false, string.Empty);
                            filaFacade.finalizaAtendimento(itemFila, atendimento, salaAtendimento);
                            flagAtualizado = true;
                            /* verificando se o cliente gera movimento na finalização do exame.
                            * caso o retorno seja true, então será gerado uma inclusão no movimento financeiro. */

                            PermissionamentoFacade.mapaConfiguracoes.TryGetValue(ConfigurationConstans.GERA_MOVIMENTO_FINALIZACAO_EXAME, out geraMovimento);

                            if (!String.IsNullOrEmpty(dvRow.Cells[6].Value.ToString()))
                                prestadorItem = clienteFacade.findClienteByRazaoSocical(dvRow.Cells[6].Value.ToString());

                            if (Convert.ToBoolean(geraMovimento))
                            {
                                /* verificando qual objeto será usado. ClienteFuncaoExameAso ou GheFonteAgenteExameAso */
                                if (String.Equals(ApplicationConstants.EXAME_PCMSO, dvRow.Cells[5].Value.ToString()))
                                    /* preenchendo objeto GheFonteAgenteExameAso*/
                                    gheFonteAgenteExameAso = new GheFonteAgenteExameAso((Int64)dvRow.Cells[4].Value, null, atendimento, new GheFonteAgenteExame(null, pcmsoFacade.findExameById((Int64)dvRow.Cells[0].Value), null, 0, String.Empty, false, false, false), null, false, false, String.Empty, false, null, null, null, null, false, null, String.Empty, String.Empty, false, false, prestadorItem, null, false, false, null, null, false, true);
                                else
                                    /*preenchendo objeto ClienteFuncaoExameAso */
                                    clienteFuncaoExameAso = new ClienteFuncaoExameASo((Int64)dvRow.Cells[4].Value, null, atendimento, new ClienteFuncaoExame(null, null, pcmsoFacade.findExameById((Int64)dvRow.Cells[0].Value), String.Empty, 0), null, false, false, String.Empty, false, null, null, null, null, false, null, String.Empty, String.Empty, false, prestadorItem, null, null, null, true);
                                
                                financeiroFacade.incluirMovimentoPorItem(gheFonteAgenteExameAso, clienteFuncaoExameAso);

                            }

                        }
                    }

                    if (flagAtualizado)
                    {
                        MessageBox.Show("Exame(s) finalizado(s) com sucesso", "Confirmação", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        montaGridExamePendente();
                        montaGridExameRealizado();
                        montaGridExameEmAtendimento();
                        verificaFinalizacao();
                    }
                    
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);

                /* verificando a configuracao do cliente. Finaliza por item ou
                 * se finaliza no atendimento. Caso seja por item então o item
                 * finalizado será estornado e voltará para pendente */

                if (!String.IsNullOrEmpty(geraMovimento) && Convert.ToBoolean(geraMovimento))
                {
                    FilaFacade filaFacade = FilaFacade.getInstance();
                    filaFacade.estornaFinalizacaoAtendimento(itemFila, atendimento, salaAtendimento);
                }

                montaGridExamePendente();
                montaGridExameRealizado();
                montaGridExameEmAtendimento();
                verificaFinalizacao();
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }

        private void btn_cancelar_Click(object sender, EventArgs e)
        {
            try
            {
                this.Cursor = Cursors.WaitCursor;

                FilaFacade filaFacade = FilaFacade.getInstance();

                /* verificando se foi selecioanado algum exame */
                bool flagSelecionado = false;
                foreach (DataGridViewRow dvRow in dg_examePendente.Rows)
                {
                    if ((bool)dvRow.Cells[8].Value == true)
                    {
                        flagSelecionado = true;
                        break;
                    }

                }
                if (!flagSelecionado)
                    throw new Exception("Selecione pelo menos um exame para cancelar.");

                if (MessageBox.Show("Deseja cancelar os atendimentos selecionados?", "Confirmação", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                {
                    bool flagCancelado = false;
                    foreach (DataGridViewRow dvRow in dg_examePendente.Rows)
                    {
                        if ((bool)dvRow.Cells[8].Value == true)
                        {
                            filaFacade.cancelaAtendimento(new ItemFilaAtendimento((Int64)dvRow.Cells[4].Value, null, null, null, null, null, null, null, (String)dvRow.Cells[5].Value, null, false, DateTime.Now, null, null, null, null, null, null, null, null, null, String.Empty, false, string.Empty), atendimento, false, null, null);
                            flagCancelado = true;
                        }
                    }

                    if (flagCancelado)
                    {
                        MessageBox.Show("Atendimento cancelado com sucesso.", "Confirmação", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        montaGridExamePendente();
                        montaGridExameRealizado();
                        montaGridExameEmAtendimento();
                        verificaFinalizacao();
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
                montaGridExamePendente();
                montaGridExameRealizado();
                montaGridExameEmAtendimento();
                verificaFinalizacao();

            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }

        private void btn_incluir_Click(object sender, EventArgs e)
        {
            try
            {
                /* caso o aso já tenha sido finalizado não será possível incluir nenhum exame */
                if (String.Equals(atendimento.Situacao, ApplicationConstants.FECHADO))
                    throw new AsoException(AsoException.msg13);

                frmAtendimentoIncluirExamesBuscar incluirExames = new frmAtendimentoIncluirExamesBuscar(atendimento);
                incluirExames.ShowDialog();

                if (incluirExames.ExameInAso.Count > 0)
                {
                    montaGridExamePendente();
                    montaGridExameRealizado();
                    montaGridExameEmAtendimento();
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btn_cancelarEncaminhamento_Click(object sender, EventArgs e)
        {
            try
            {
                if (MessageBox.Show("Deseja excluir o encaminhamento?", "Confirmação", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                {
                    this.Cursor = Cursors.WaitCursor;
                    EstudoFacade pcmsoFacade = EstudoFacade.getInstance();

                    /* testatando para saber se exame é tipo pcmso */
                    if (String.Equals(ApplicationConstants.EXAME_PCMSO, dg_examePendente.CurrentRow.Cells[5].Value.ToString()))
                        pcmsoFacade.deletePrestadorGheFonteAgenteExameAso(new GheFonteAgenteExameAso((Int64)dg_examePendente.CurrentRow.Cells[4].Value, null, null, null, null, false, false, String.Empty, false, null, null, null, null, false, null, String.Empty, String.Empty, false, false, null,null, false, false, null, null, false, true));
                    else
                        pcmsoFacade.deletePrestadorClienteFuncaoExameAso(new ClienteFuncaoExameASo((Int64)dg_examePendente.CurrentRow.Cells[4].Value, null, null, null, null, false, false, String.Empty, false, null, null, null, null, false, null, String.Empty,String.Empty, false, null, null, null, null, true));

                    MessageBox.Show("Encaminhamento excluído com sucesso.", "Confirmação", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    montaGridExamePendente();
                    verificaFinalizacao();
                }
                
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }

        private void btn_excluirRealizado_Click(object sender, EventArgs e)
        {
            try
            {
                this.Cursor = Cursors.WaitCursor;
                Cliente prestadorItem = null;
                String geraMovimento = String.Empty;
                GheFonteAgenteExameAso gheFonteAgenteExameAso = null;
                ClienteFuncaoExameASo clienteFuncaoExameAso = null;

                FilaFacade filaFacade = FilaFacade.getInstance();
                ClienteFacade clienteFacade = ClienteFacade.getInstance();
                EstudoFacade pcmsoFacade = EstudoFacade.getInstance();
                
                /* verificando os exames que foram selecionados */
                bool flagSelecionado = false;
                foreach (DataGridViewRow dvRow in dgvExameRealizado.Rows)
                {
                    if ((bool)dvRow.Cells[9].Value == true)
                    {
                        flagSelecionado = true;
                        break;
                    }
                }

                if (!flagSelecionado)
                    throw new Exception("Selecione um exame para finalizar.");

                if (MessageBox.Show("Deseja cancelar o(s) atendimento(s) selecionado(s)?", "Confirmação", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                {
                    bool flagCancelado = false;
                    foreach (DataGridViewRow dvRow in dgvExameRealizado.Rows)
                    {
                        
                        if ((bool)dvRow.Cells[9].Value == true)
                        {
                            /* montando item da fila */
                            if (!String.IsNullOrEmpty(dvRow.Cells[8].Value.ToString()))
                                prestadorItem = clienteFacade.findClienteByRazaoSocical(dgvExameRealizado.CurrentRow.Cells[8].Value.ToString());

                            PermissionamentoFacade.mapaConfiguracoes.TryGetValue(ConfigurationConstans.GERA_MOVIMENTO_FINALIZACAO_EXAME, out geraMovimento);
                            
                            if (Convert.ToBoolean(geraMovimento))
                            {
                                /* verificando qual objeto será usado. ClienteFuncaoExameAso ou GheFonteAgenteExameAso */
                                if (String.Equals(ApplicationConstants.EXAME_PCMSO, dvRow.Cells[7].Value.ToString()))
                                    /* preenchendo objeto GheFonteAgenteExameAso*/
                                    gheFonteAgenteExameAso = new GheFonteAgenteExameAso((Int64)dvRow.Cells[6].Value, null, atendimento, new GheFonteAgenteExame(null, pcmsoFacade.findExameById((Int64)dvRow.Cells[0].Value), null, 0, String.Empty, false, false, false), null, false, false, String.Empty, false, null, null, null, null, false, null, String.Empty, String.Empty, false, false, prestadorItem, null, false, false, null, null, false, true);
                                else
                                    /*preenchendo objeto ClienteFuncaoExameAso */
                                    clienteFuncaoExameAso = new ClienteFuncaoExameASo((Int64)dvRow.Cells[6].Value, null, atendimento, new ClienteFuncaoExame(null, null, pcmsoFacade.findExameById((Int64)dvRow.Cells[0].Value), String.Empty, 0), null, false, false, String.Empty, false, null, null, null, null, false, null, String.Empty, String.Empty, false, prestadorItem, null, null, null, true);
                            }

                            filaFacade.cancelaAtendimento(new ItemFilaAtendimento((Int64)dvRow.Cells[6].Value, null, null, null, null, null, null, null, (String)dvRow.Cells[7].Value, null, false, DateTime.Now, null, null, null, null, null, null, null, null, null, String.Empty, false, string.Empty), atendimento, true, gheFonteAgenteExameAso, clienteFuncaoExameAso);
                            flagCancelado = true;
                        }
                    }

                    if (flagCancelado)
                    {
                        MessageBox.Show("Atendimento(s) cancelado(s) com sucesso.", "Confirmação", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        montaGridExamePendente();
                        montaGridExameRealizado();
                        montaGridExameEmAtendimento();
                        verificaFinalizacao();
                    }
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
                montaGridExamePendente();
                montaGridExameRealizado();
                montaGridExameEmAtendimento();
                verificaFinalizacao();
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }

        private void validaPermissoes()
        {
            PermissionamentoFacade permissionamentoFacade = PermissionamentoFacade.getInstance();

            this.btn_finalizar.Enabled = permissionamentoFacade.hasPermission("DETALHES_ATENDIMENTO", "FINALIZAR");
            this.btn_cancelar.Enabled = permissionamentoFacade.hasPermission("DETALHES_ATENDIMENTO", "CANCELAR");
            this.btn_incluir.Enabled = permissionamentoFacade.hasPermission("DETALHES_ATENDIMENTO", "INCLUIR");
            this.btn_excluirRealizado.Enabled = permissionamentoFacade.hasPermission("DETALHES_ATENDIMENTO", "CANCELAR");
            this.btn_cancelado.Enabled = permissionamentoFacade.hasPermission("DETALHES_ATENDIMENTO", "EXIBIR");
            this.btn_cancelarEncaminhamento.Enabled = permissionamentoFacade.hasPermission("DETALHES_ATENDIMENTO", "CANCELAR_ENCAMINHAMENTO");
        }

        public void montaGridExamePendente()
        {
            try
            {
                this.Cursor = Cursors.WaitCursor;

                dg_examePendente.Columns.Clear();

                FilaFacade filaFacede = FilaFacade.getInstance();

                examePendente = filaFacede.buscaExamesInSituacaoByAso(atendimento, false, false, false, null, false);

                dg_examePendente.ColumnCount = 7;

                dg_examePendente.Columns[0].HeaderText = "Código Exame";
                dg_examePendente.Columns[0].ReadOnly = true;

                dg_examePendente.Columns[1].HeaderText = "Nome do Exame";
                dg_examePendente.Columns[1].ReadOnly = true;

                dg_examePendente.Columns[2].HeaderText = "Exame de Laboratório?";
                dg_examePendente.Columns[2].HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleCenter;
                dg_examePendente.Columns[2].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
                dg_examePendente.Columns[2].ReadOnly = true;

                dg_examePendente.Columns[3].HeaderText = "Exame Externo?";
                dg_examePendente.Columns[3].HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleCenter;
                dg_examePendente.Columns[3].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
                dg_examePendente.Columns[3].ReadOnly = true;

                dg_examePendente.Columns[4].HeaderText = "IdItem";
                dg_examePendente.Columns[4].Visible = false;

                dg_examePendente.Columns[5].HeaderText = "TipoExame";
                dg_examePendente.Columns[5].Visible = false;

                dg_examePendente.Columns[6].HeaderText = "Prestador";
                dg_examePendente.Columns[6].ReadOnly = true;

                /* incluindo o botão da grid */

                DataGridViewButtonColumn btn = new DataGridViewButtonColumn();
                dg_examePendente.Columns.Add(btn);

                btn.HeaderText = "...";
                btn.Text = "...";
                btn.Name = "btn_prestador";
                btn.UseColumnTextForButtonValue = true;
                dg_examePendente.Columns[7].DisplayIndex = 5;
                btn.FlatStyle = FlatStyle.Flat;

                /* adicionando o check na grid */
                DataGridViewCheckBoxColumn check = new DataGridViewCheckBoxColumn();
                check.Name = "selecionar";
                dg_examePendente.Columns.Add(check);
                dg_examePendente.Columns[8].DisplayIndex = 0;
                dg_examePendente.Columns[8].ReadOnly = false;

                foreach (ItemFilaAtendimento itemFila in examePendente)
                    dg_examePendente.Rows.Add(itemFila.Exame.Id, itemFila.Exame.Descricao,
                        (Boolean)itemFila.Exame.Laboratorio ? "Lab" : null, (Boolean)itemFila.Exame.Externo ? "Ext" : null, itemFila.IdItem, itemFila.TipoExame, itemFila.Prestador, null, false);

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }

        public void montaGridExameRealizado()
        {
            try
            {
                this.Cursor = Cursors.WaitCursor;

                this.dgvExameRealizado.Columns.Clear();

                FilaFacade filaFacede = FilaFacade.getInstance();

                exameRealizado = filaFacede.buscaExamesInSituacaoByAso(atendimento, true, true, false, false, false);

                dgvExameRealizado.ColumnCount = 9;

                dgvExameRealizado.Columns[0].HeaderText = "Código Exame";
                dgvExameRealizado.Columns[0].ReadOnly = true;

                dgvExameRealizado.Columns[1].HeaderText = "Nome do Exame";
                dgvExameRealizado.Columns[1].ReadOnly = true;

                dgvExameRealizado.Columns[2].HeaderText = "Exame de Laboratório?";
                dgvExameRealizado.Columns[2].HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleCenter;
                dgvExameRealizado.Columns[2].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
                dgvExameRealizado.Columns[2].ReadOnly = true;

                dgvExameRealizado.Columns[3].HeaderText = "Exame Externo?";
                dgvExameRealizado.Columns[3].HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleCenter;
                dgvExameRealizado.Columns[3].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
                dgvExameRealizado.Columns[3].ReadOnly = true;

                dgvExameRealizado.Columns[4].HeaderText = "Sala";
                dgvExameRealizado.Columns[4].ReadOnly = true;

                dgvExameRealizado.Columns[5].HeaderText = "Andar";
                dgvExameRealizado.Columns[5].ReadOnly = true;

                dgvExameRealizado.Columns[6].HeaderText = "idItem";
                dgvExameRealizado.Columns[6].Visible = false;

                dgvExameRealizado.Columns[7].HeaderText = "TipoExame";
                dgvExameRealizado.Columns[7].Visible = false;

                dgvExameRealizado.Columns[8].HeaderText = "Prestador";
                dgvExameRealizado.Columns[8].ReadOnly = true;

                DataGridViewCheckBoxColumn check = new DataGridViewCheckBoxColumn();
                check.Name = "Selecionar";
                dgvExameRealizado.Columns.Add(check);

                dgvExameRealizado.Columns[9].ReadOnly = false;
                dgvExameRealizado.Columns[9].DisplayIndex = 0;

                foreach (ItemFilaAtendimento itemFila in exameRealizado)
                    dgvExameRealizado.Rows.Add(itemFila.Exame.Id, itemFila.Exame.Descricao,
                        (Boolean)itemFila.Exame.Laboratorio ? "Lab" : null, (Boolean)itemFila.Exame.Externo ? "Ext" : null, itemFila.NomeSala, itemFila.Andar, itemFila.IdItem, itemFila.TipoExame,
                        itemFila.Prestador, false);

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }

        public void verificaFinalizacao()
        {
            /* verificando se não existe mais exames pendentes e perguntando ao usuário se ele deseja que o atendimento seja encerrado. */
            try
            {
                if (dg_examePendente.Rows.Count == 0)
                {
                    if (MessageBox.Show("Não existe mais exames pendentes. Gostaria de finalizar o atendimento?", "Confirmação", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                    {
                        AsoFacade asoFacade = AsoFacade.getInstance();
                        atendimento = asoFacade.findAtendimentoById((long)atendimento.Id);
                        atendimento.Finalizador = PermissionamentoFacade.usuarioAutenticado;
                        asoFacade.finalizaAso(atendimento);
                        MessageBox.Show("Atendimento finalizado com sucesso.", "Confirmação", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        this.Close();
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void dg_examePendente_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                EstudoFacade pcmsoFacade = EstudoFacade.getInstance();
                
                /* o clique da grid foi disparado pelo botão presente na coluna de index 7 */
                
                if (e.ColumnIndex == 7)
                {
                    /* caso tenha alguma informação de prestador, solicitar para saber se o usuario deseja alterar o prestador */
                    if (String.Equals(dg_examePendente.CurrentRow.Cells[6].Value.ToString(), String.Empty))
                    {
                        frmClienteSelecionar formPrestador = new frmClienteSelecionar(true, null, false, null, null, null, null, false, true);
                        formPrestador.ShowDialog();

                        if (formPrestador.Cliente != null)
                        {
                            prestador = formPrestador.Cliente;

                            if (String.Equals(ApplicationConstants.EXAME_PCMSO, dg_examePendente.CurrentRow.Cells[5].Value.ToString()))
                                pcmsoFacade.insertPrestadorGheFonteAgenteExameAso(new GheFonteAgenteExameAso((Int64)dg_examePendente.CurrentRow.Cells[4].Value, null, null, null, null, false, false, String.Empty, false, null, null, null, null, false, null, String.Empty, String.Empty, false, false, prestador, null, false, false, null, null, false, true));
                            else
                                pcmsoFacade.insertPrestadorClienteFuncaoExameAso(new ClienteFuncaoExameASo((Int64)dg_examePendente.CurrentRow.Cells[4].Value, null, null, null, null, false, false, String.Empty, false, null, null, null, null, false, null, String.Empty, String.Empty, false, prestador, null, null, null, true));
 
                            MessageBox.Show("Encaminhamento incluído com sucesso.", "Confirmação", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        }

                    }
                    else
                    {
                        if (MessageBox.Show("Deseja alterar o prestador do exame?", "Confirmação", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                        {
                            frmClienteSelecionar formPrestador = new frmClienteSelecionar(true, null, false, null, null, null, null, false, true);
                            formPrestador.ShowDialog();

                            if (formPrestador.Cliente != null)
                            {
                                prestador = formPrestador.Cliente;

                                if (String.Equals(ApplicationConstants.EXAME_PCMSO, dg_examePendente.CurrentRow.Cells[5].Value.ToString()))
                                    pcmsoFacade.insertPrestadorGheFonteAgenteExameAso(new GheFonteAgenteExameAso((Int64)dg_examePendente.CurrentRow.Cells[4].Value, null, null, null, null, false, false, String.Empty, false, null, null, null, null, false, null, String.Empty, String.Empty, false, false, prestador, null, false, false, null, null, false, true));
                                else
                                    pcmsoFacade.insertPrestadorClienteFuncaoExameAso(new ClienteFuncaoExameASo((Int64)dg_examePendente.CurrentRow.Cells[4].Value, null, null, null, null, false, false, String.Empty, false, null, null, null, null, false, null, String.Empty, String.Empty, false, prestador, null, null, null, true));

                                MessageBox.Show("Prestador alterado com sucesso.", "Confirmação", MessageBoxButtons.OK, MessageBoxIcon.Information );

                            }
                        }
                    }

                    montaGridExamePendente();
                    verificaFinalizacao();
                   
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }

        public void montaGridExameEmAtendimento()
        {
            try
            {
                this.Cursor = Cursors.WaitCursor;

                this.dgvExameAtendimento.Columns.Clear();

                FilaFacade filaFacede = FilaFacade.getInstance();

                exameEmAtendimento = filaFacede.buscaExamesInSituacaoByAso(atendimento, true, false, false, null, false);

                dgvExameAtendimento.ColumnCount = 9;

                dgvExameAtendimento.Columns[0].HeaderText = "Código Exame";

                dgvExameAtendimento.Columns[1].HeaderText = "Nome do Exame";

                dgvExameAtendimento.Columns[2].HeaderText = "Exame de Laboratório?";
                dgvExameAtendimento.Columns[2].HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleCenter;
                dgvExameAtendimento.Columns[2].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;

                dgvExameAtendimento.Columns[3].HeaderText = "Exame Externo?";
                dgvExameAtendimento.Columns[3].HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleCenter;
                dgvExameAtendimento.Columns[3].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;

                dgvExameAtendimento.Columns[4].HeaderText = "Sala";

                dgvExameAtendimento.Columns[5].HeaderText = "Andar";

                dgvExameAtendimento.Columns[6].HeaderText = "idItem";
                dgvExameAtendimento.Columns[6].Visible = false;

                dgvExameAtendimento.Columns[7].HeaderText = "TipoExame";
                dgvExameAtendimento.Columns[7].Visible = false;

                dgvExameAtendimento.Columns[8].HeaderText = "Prestador";

                foreach (ItemFilaAtendimento itemFila in exameEmAtendimento)
                    dgvExameAtendimento.Rows.Add(itemFila.Exame.Id, itemFila.Exame.Descricao,
                        (Boolean)itemFila.Exame.Laboratorio ? "Lab" : null, (Boolean)itemFila.Exame.Externo ? "Ext" : null, itemFila.NomeSala, itemFila.Andar, itemFila.IdItem, itemFila.TipoExame,
                        itemFila.Prestador);

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }

        private void chkMarcarTodosPendentes_CheckedChanged(object sender, EventArgs e)
        {
            try
            {
                if (chkMarcarTodosPendentes.Checked)
                {
                    foreach (DataGridViewRow dvRom in dg_examePendente.Rows)
                        dvRom.Cells[8].Value = true;
                }
                else
                {
                    foreach (DataGridViewRow dvRom in dg_examePendente.Rows)
                        dvRom.Cells[8].Value = false;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void chkmarcarTodosRealizados_CheckedChanged(object sender, EventArgs e)
        {
            try
            {
                if (chkmarcarTodosRealizados.Checked)
                {
                    foreach (DataGridViewRow dvRow in dgvExameRealizado.Rows)
                        dvRow.Cells[9].Value = true;
                }
                else
                {
                    foreach (DataGridViewRow dvRow in dgvExameRealizado.Rows)
                        dvRow.Cells[9].Value = false;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }
    }
}
