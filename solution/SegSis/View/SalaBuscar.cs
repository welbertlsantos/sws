﻿using SWS.Entidade;
using SWS.Facade;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace SWS.View
{
    public partial class frmSalaBuscar : frmTemplateConsulta
    {
        private Empresa empresa;

        public Empresa Empresa
        {
            get { return empresa; }
            set { empresa = value; }
        }

        private Sala sala;

        public Sala Sala
        {
            get { return sala; }
            set { sala = value; }
        }
        
        public frmSalaBuscar(Empresa empresa)
        {
            InitializeComponent();
            this.Empresa = empresa;
            montaGrid();
        }

        private void montaGrid()
        {
            try
            {
                dgvSala.Columns.Clear();
                dgvSala.ColumnCount = 3;
                dgvSala.Columns[0].HeaderText = "idSala";
                dgvSala.Columns[0].Visible = false;
                dgvSala.Columns[1].HeaderText = "Sala";
                dgvSala.Columns[1].Width = 300;
                dgvSala.Columns[2].HeaderText = "Slug";
                dgvSala.Columns[2].Width = 200;

                FilaFacade filaFacade = FilaFacade.getInstance();
                foreach (Sala sala in filaFacade.findAllSalaByEmpresa(PermissionamentoFacade.usuarioAutenticado.Empresa))
                    dgvSala.Rows.Add(sala.Id, sala.Descricao, sala.SlugSala);

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void dgvSala_CellMouseDoubleClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            btnConfirmar.PerformClick();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnConfirmar_Click(object sender, EventArgs e)
        {
            try
            {
                if (dgvSala.CurrentRow == null)
                    throw new Exception("Selecione uma sala.");

                FilaFacade filaFacade = FilaFacade.getInstance();
                sala = filaFacade.findSalaById((long)dgvSala.CurrentRow.Cells[0].Value);
                this.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
    }
}
