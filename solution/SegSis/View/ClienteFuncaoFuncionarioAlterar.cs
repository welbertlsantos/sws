﻿using SWS.Entidade;
using SWS.Facade;
using SWS.View.ViewHelper;
using SWS.ViewHelper;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace SWS.View
{
    public partial class frmClienteFuncaoFuncionarioAlterar : frmTemplateConsulta
    {
        private ClienteFuncaoFuncionario clienteFuncaoFuncionario;

        public ClienteFuncaoFuncionario ClienteFuncaoFuncionario
        {
            get { return clienteFuncaoFuncionario; }
            set { clienteFuncaoFuncionario = value; }
        }

        private bool flagAlteracao;

        public bool FlagAlteracao
        {
            get { return flagAlteracao; }
            set { flagAlteracao = value; }
        }

        public frmClienteFuncaoFuncionarioAlterar(ClienteFuncaoFuncionario clienteFuncaoFuncionario)
        {
            InitializeComponent();
            /* preenchendo informações de tela */
            this.ClienteFuncaoFuncionario = clienteFuncaoFuncionario;

            if (!clienteFuncaoFuncionario.ClienteFuncao.Cliente.Fisica)
            {
                lblRazaoSocial.Text = "Razão Social";
                lblFantasia.Text = "Nome de Fantasia";
                lblCnpj.Text = "CNPJ";
                textCnpj.Mask = "99,999,999/9999-99";
                textRazaoSocial.Text = clienteFuncaoFuncionario.ClienteFuncao.Cliente.RazaoSocial;
                textFantasia.Text = clienteFuncaoFuncionario.ClienteFuncao.Cliente.Fantasia;
                textCnpj.Text = clienteFuncaoFuncionario.ClienteFuncao.Cliente.Cnpj;
            }
            else
            {
                lblRazaoSocial.Text = "Nome";
                lblFantasia.Text = "Apelido";
                lblCnpj.Text = "CPF";
                textCnpj.Mask = "999,999,999-99";
                textRazaoSocial.Text = clienteFuncaoFuncionario.ClienteFuncao.Cliente.RazaoSocial;
                textFantasia.Text = clienteFuncaoFuncionario.ClienteFuncao.Cliente.Fantasia;
                textCnpj.Text = clienteFuncaoFuncionario.ClienteFuncao.Cliente.Cnpj;
            }
            
            dataAdmissao.Checked = true;
            dataAdmissao.Text = clienteFuncaoFuncionario.DataCadastro.ToString();
            textRegime.Text = clienteFuncaoFuncionario.RegimeRevezamento;
            ComboHelper.brPdh(cbBrPdh);
            cbBrPdh.SelectedValue = clienteFuncaoFuncionario.BrPdh;
            textMatricula.Text = clienteFuncaoFuncionario.Matricula;
            ComboHelper.Boolean(cbVinculo, true);
            cbVinculo.SelectedValue = clienteFuncaoFuncionario.Vinculo == true ? "true" : "false";
            ComboHelper.Boolean(cbEstagiario, false);
            cbEstagiario.SelectedValue = clienteFuncaoFuncionario.Estagiario == true ? "true" : "false";

            textFuncao.Text = this.ClienteFuncaoFuncionario.ClienteFuncao.Funcao.Descricao;

            /* caso o cliente já esteja desativado, então só será permitido alterar a matrícula do funcionário */

            if (clienteFuncaoFuncionario.DataDesligamento != null)
            {
                btnFuncao.Enabled = false;
                dataAdmissao.Enabled = false;
                cbVinculo.Enabled = false;
                cbEstagiario.Enabled = false;
                cbBrPdh.Enabled = false;
            }
        }

        private void btConfirmar_Click(object sender, EventArgs e)
        {
            try
            {
                if (!validaTela())
                    throw new Exception("Veja os dados digitados");

                /* alterando informação do clienteFuncionario passado como parâmetro */

                clienteFuncaoFuncionario.DataCadastro = Convert.ToDateTime(dataAdmissao.Text);
                clienteFuncaoFuncionario.RegimeRevezamento = textRegime.Text;
                clienteFuncaoFuncionario.BrPdh = cbBrPdh.SelectedIndex == -1 ? string.Empty : ((SelectItem)cbBrPdh.SelectedItem).Valor;
                clienteFuncaoFuncionario.Matricula = textMatricula.Text;
                clienteFuncaoFuncionario.Vinculo = Convert.ToBoolean(((SelectItem)cbVinculo.SelectedItem).Valor);
                clienteFuncaoFuncionario.Estagiario = Convert.ToBoolean(((SelectItem)cbEstagiario.SelectedItem).Valor);
                flagAlteracao = true;
                this.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btFechar_Click(object sender, EventArgs e)
        {
            flagAlteracao = false;
            this.Close();
        }

        protected bool validaTela()
        {
            bool result = true;
            try
            {
                if (!dataAdmissao.Checked)
                    throw new Exception("A data de admissão é obrigatória");

                /* validando para saber se o campo matrícula é obrigatório */
                if (Convert.ToBoolean(((SelectItem)cbVinculo.SelectedItem).Valor) == true && string.IsNullOrEmpty(textMatricula.Text))
                    throw new Exception("Funcionário marcado como vínculo de trabalho com a empresa. Nesse caso a matrícula é obrigatória e deverá ser informada a mesma matrícula que ele foi registrado.");

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
                result = false;
            }

            return result;
        }

        protected void btnFuncao_Click(object sender, EventArgs e)
        {
            try
            {
                /* alterando as funcao e mostrando somente a funcao cadastrada no cliente */
                frmClienteFuncaoSeleciona selecionarFuncaoCliente = new frmClienteFuncaoSeleciona(this.clienteFuncaoFuncionario.ClienteFuncao.Cliente);
                selecionarFuncaoCliente.ShowDialog();

                if (selecionarFuncaoCliente.ClienteFuncao != null)
                {
                    if (clienteFuncaoFuncionario.Id != null)
                    {
                        /* alterar o clienteFuncaoFuncionario para a nova Funcao Selecionada */
                        clienteFuncaoFuncionario.ClienteFuncao = selecionarFuncaoCliente.ClienteFuncao;
                        FuncionarioFacade funcionarioFacade = FuncionarioFacade.getInstance();
                        funcionarioFacade.updateClienteFuncaoFuncionario(clienteFuncaoFuncionario);
                        textFuncao.Text = clienteFuncaoFuncionario.ClienteFuncao.Funcao.Descricao;
                        MessageBox.Show("Função alterada com sucesso.", "Confirmação", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }
                    else
                    {
                        /* apenas altera a função do objeto porque se trata de uma nova inclusão de funcionários */
                        clienteFuncaoFuncionario.ClienteFuncao = selecionarFuncaoCliente.ClienteFuncao;
                        textFuncao.Text = clienteFuncaoFuncionario.ClienteFuncao.Funcao.Descricao;
                        MessageBox.Show("Função alterada com sucesso.", "Confirmação", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }
    }
}
