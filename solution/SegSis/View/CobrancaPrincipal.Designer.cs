﻿namespace SWS.View
{
    partial class frmCobrancaPrincipal
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            this.flpAcao = new System.Windows.Forms.FlowLayoutPanel();
            this.btnPesquisar = new System.Windows.Forms.Button();
            this.btnLimpar = new System.Windows.Forms.Button();
            this.btnProrrogar = new System.Windows.Forms.Button();
            this.btnAlterarFormaPgto = new System.Windows.Forms.Button();
            this.btnReativar = new System.Windows.Forms.Button();
            this.btnCancelar = new System.Windows.Forms.Button();
            this.btnEstornar = new System.Windows.Forms.Button();
            this.btnBaixar = new System.Windows.Forms.Button();
            this.btnFechar = new System.Windows.Forms.Button();
            this.lblNumeroNota = new System.Windows.Forms.TextBox();
            this.lblSituacao = new System.Windows.Forms.TextBox();
            this.lblCliente = new System.Windows.Forms.TextBox();
            this.lblEmissao = new System.Windows.Forms.TextBox();
            this.lblVencimento = new System.Windows.Forms.TextBox();
            this.lblBaixa = new System.Windows.Forms.TextBox();
            this.lblCancelamento = new System.Windows.Forms.TextBox();
            this.dataEmissaoFinal = new System.Windows.Forms.DateTimePicker();
            this.dataEmissaoInicial = new System.Windows.Forms.DateTimePicker();
            this.dataVencimentoFinal = new System.Windows.Forms.DateTimePicker();
            this.dataVencimentoInicial = new System.Windows.Forms.DateTimePicker();
            this.dataBaixaFinal = new System.Windows.Forms.DateTimePicker();
            this.dataBaixaInicial = new System.Windows.Forms.DateTimePicker();
            this.dataCancelamentoFinal = new System.Windows.Forms.DateTimePicker();
            this.dataCancelamentoInicial = new System.Windows.Forms.DateTimePicker();
            this.textNumeroNota = new System.Windows.Forms.TextBox();
            this.cbSituacao = new SWS.ComboBoxWithBorder();
            this.textCliente = new System.Windows.Forms.TextBox();
            this.btnExcluirCliente = new System.Windows.Forms.Button();
            this.btnCliente = new System.Windows.Forms.Button();
            this.grb_cobranca = new System.Windows.Forms.GroupBox();
            this.dgvCobranca = new System.Windows.Forms.DataGridView();
            this.pnlTotal = new System.Windows.Forms.Panel();
            this.textValorTotal = new System.Windows.Forms.TextBox();
            this.lblValorTotalCobranca = new System.Windows.Forms.Label();
            this.textQuantidadeCobranca = new System.Windows.Forms.TextBox();
            this.lblQuantidadeCobranca = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.spForm)).BeginInit();
            this.spForm.Panel1.SuspendLayout();
            this.spForm.Panel2.SuspendLayout();
            this.spForm.SuspendLayout();
            this.pnlForm.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.banner)).BeginInit();
            this.flpAcao.SuspendLayout();
            this.grb_cobranca.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvCobranca)).BeginInit();
            this.pnlTotal.SuspendLayout();
            this.SuspendLayout();
            // 
            // spForm
            // 
            // 
            // spForm.Panel1
            // 
            this.spForm.Panel1.Controls.Add(this.flpAcao);
            // 
            // pnlForm
            // 
            this.pnlForm.Controls.Add(this.pnlTotal);
            this.pnlForm.Controls.Add(this.grb_cobranca);
            this.pnlForm.Controls.Add(this.btnExcluirCliente);
            this.pnlForm.Controls.Add(this.btnCliente);
            this.pnlForm.Controls.Add(this.textCliente);
            this.pnlForm.Controls.Add(this.cbSituacao);
            this.pnlForm.Controls.Add(this.textNumeroNota);
            this.pnlForm.Controls.Add(this.lblCancelamento);
            this.pnlForm.Controls.Add(this.lblBaixa);
            this.pnlForm.Controls.Add(this.lblVencimento);
            this.pnlForm.Controls.Add(this.dataCancelamentoFinal);
            this.pnlForm.Controls.Add(this.dataCancelamentoInicial);
            this.pnlForm.Controls.Add(this.dataBaixaFinal);
            this.pnlForm.Controls.Add(this.dataBaixaInicial);
            this.pnlForm.Controls.Add(this.dataVencimentoFinal);
            this.pnlForm.Controls.Add(this.dataVencimentoInicial);
            this.pnlForm.Controls.Add(this.lblEmissao);
            this.pnlForm.Controls.Add(this.dataEmissaoFinal);
            this.pnlForm.Controls.Add(this.dataEmissaoInicial);
            this.pnlForm.Controls.Add(this.lblCliente);
            this.pnlForm.Controls.Add(this.lblSituacao);
            this.pnlForm.Controls.Add(this.lblNumeroNota);
            this.pnlForm.Location = new System.Drawing.Point(0, 3);
            this.pnlForm.Size = new System.Drawing.Size(764, 477);
            // 
            // flpAcao
            // 
            this.flpAcao.Controls.Add(this.btnPesquisar);
            this.flpAcao.Controls.Add(this.btnLimpar);
            this.flpAcao.Controls.Add(this.btnProrrogar);
            this.flpAcao.Controls.Add(this.btnAlterarFormaPgto);
            this.flpAcao.Controls.Add(this.btnReativar);
            this.flpAcao.Controls.Add(this.btnCancelar);
            this.flpAcao.Controls.Add(this.btnEstornar);
            this.flpAcao.Controls.Add(this.btnBaixar);
            this.flpAcao.Controls.Add(this.btnFechar);
            this.flpAcao.Location = new System.Drawing.Point(0, 0);
            this.flpAcao.Name = "flpAcao";
            this.flpAcao.Size = new System.Drawing.Size(784, 33);
            this.flpAcao.TabIndex = 2;
            // 
            // btnPesquisar
            // 
            this.btnPesquisar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnPesquisar.Image = global::SWS.Properties.Resources.lupa;
            this.btnPesquisar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnPesquisar.Location = new System.Drawing.Point(3, 3);
            this.btnPesquisar.Name = "btnPesquisar";
            this.btnPesquisar.Size = new System.Drawing.Size(75, 23);
            this.btnPesquisar.TabIndex = 30;
            this.btnPesquisar.Text = "&Pesquisar";
            this.btnPesquisar.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnPesquisar.UseVisualStyleBackColor = true;
            this.btnPesquisar.Click += new System.EventHandler(this.btnPesquisar_Click);
            // 
            // btnLimpar
            // 
            this.btnLimpar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnLimpar.Image = global::SWS.Properties.Resources.vassoura;
            this.btnLimpar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnLimpar.Location = new System.Drawing.Point(84, 3);
            this.btnLimpar.Name = "btnLimpar";
            this.btnLimpar.Size = new System.Drawing.Size(75, 23);
            this.btnLimpar.TabIndex = 31;
            this.btnLimpar.Text = "&Limpar";
            this.btnLimpar.UseVisualStyleBackColor = true;
            this.btnLimpar.Click += new System.EventHandler(this.btnLimpar_Click);
            // 
            // btnProrrogar
            // 
            this.btnProrrogar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnProrrogar.Image = global::SWS.Properties.Resources.prorrogar;
            this.btnProrrogar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnProrrogar.Location = new System.Drawing.Point(165, 3);
            this.btnProrrogar.Name = "btnProrrogar";
            this.btnProrrogar.Size = new System.Drawing.Size(75, 23);
            this.btnProrrogar.TabIndex = 38;
            this.btnProrrogar.Text = "&Prorrogar";
            this.btnProrrogar.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnProrrogar.UseVisualStyleBackColor = true;
            this.btnProrrogar.Click += new System.EventHandler(this.btnProrrogar_Click);
            // 
            // btnAlterarFormaPgto
            // 
            this.btnAlterarFormaPgto.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnAlterarFormaPgto.Image = global::SWS.Properties.Resources.Alterar;
            this.btnAlterarFormaPgto.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnAlterarFormaPgto.Location = new System.Drawing.Point(246, 3);
            this.btnAlterarFormaPgto.Name = "btnAlterarFormaPgto";
            this.btnAlterarFormaPgto.Size = new System.Drawing.Size(75, 23);
            this.btnAlterarFormaPgto.TabIndex = 37;
            this.btnAlterarFormaPgto.Text = "&Alterar Pgto";
            this.btnAlterarFormaPgto.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnAlterarFormaPgto.UseVisualStyleBackColor = true;
            this.btnAlterarFormaPgto.Click += new System.EventHandler(this.btnAlterarFormaPgto_Click);
            // 
            // btnReativar
            // 
            this.btnReativar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnReativar.Image = global::SWS.Properties.Resources.devolucao;
            this.btnReativar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnReativar.Location = new System.Drawing.Point(327, 3);
            this.btnReativar.Name = "btnReativar";
            this.btnReativar.Size = new System.Drawing.Size(75, 23);
            this.btnReativar.TabIndex = 36;
            this.btnReativar.Text = "&Reativar";
            this.btnReativar.UseVisualStyleBackColor = true;
            this.btnReativar.Click += new System.EventHandler(this.btnReativar_Click);
            // 
            // btnCancelar
            // 
            this.btnCancelar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnCancelar.Image = global::SWS.Properties.Resources.Icone_cancelar;
            this.btnCancelar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnCancelar.Location = new System.Drawing.Point(408, 3);
            this.btnCancelar.Name = "btnCancelar";
            this.btnCancelar.Size = new System.Drawing.Size(75, 23);
            this.btnCancelar.TabIndex = 35;
            this.btnCancelar.Text = "&Cancelar";
            this.btnCancelar.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnCancelar.UseVisualStyleBackColor = true;
            this.btnCancelar.Click += new System.EventHandler(this.btnCancelar_Click);
            // 
            // btnEstornar
            // 
            this.btnEstornar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnEstornar.Image = global::SWS.Properties.Resources.go_back_98505;
            this.btnEstornar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnEstornar.Location = new System.Drawing.Point(489, 3);
            this.btnEstornar.Name = "btnEstornar";
            this.btnEstornar.Size = new System.Drawing.Size(75, 23);
            this.btnEstornar.TabIndex = 34;
            this.btnEstornar.Text = "&Estornar Baixar";
            this.btnEstornar.UseVisualStyleBackColor = true;
            this.btnEstornar.Click += new System.EventHandler(this.btnEstornar_Click);
            // 
            // btnBaixar
            // 
            this.btnBaixar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnBaixar.Image = global::SWS.Properties.Resources.fechar_ico;
            this.btnBaixar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnBaixar.Location = new System.Drawing.Point(570, 3);
            this.btnBaixar.Name = "btnBaixar";
            this.btnBaixar.Size = new System.Drawing.Size(75, 23);
            this.btnBaixar.TabIndex = 33;
            this.btnBaixar.Text = "&Baixar";
            this.btnBaixar.UseVisualStyleBackColor = true;
            this.btnBaixar.Click += new System.EventHandler(this.btnBaixar_Click);
            // 
            // btnFechar
            // 
            this.btnFechar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnFechar.Image = global::SWS.Properties.Resources.close;
            this.btnFechar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnFechar.Location = new System.Drawing.Point(651, 3);
            this.btnFechar.Name = "btnFechar";
            this.btnFechar.Size = new System.Drawing.Size(75, 23);
            this.btnFechar.TabIndex = 32;
            this.btnFechar.Text = "&Fechar";
            this.btnFechar.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnFechar.UseVisualStyleBackColor = true;
            this.btnFechar.Click += new System.EventHandler(this.btnFechar_Click);
            // 
            // lblNumeroNota
            // 
            this.lblNumeroNota.BackColor = System.Drawing.Color.LightGray;
            this.lblNumeroNota.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblNumeroNota.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNumeroNota.Location = new System.Drawing.Point(6, 8);
            this.lblNumeroNota.Name = "lblNumeroNota";
            this.lblNumeroNota.ReadOnly = true;
            this.lblNumeroNota.Size = new System.Drawing.Size(153, 21);
            this.lblNumeroNota.TabIndex = 0;
            this.lblNumeroNota.TabStop = false;
            this.lblNumeroNota.Text = "Número NF";
            // 
            // lblSituacao
            // 
            this.lblSituacao.BackColor = System.Drawing.Color.LightGray;
            this.lblSituacao.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblSituacao.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSituacao.Location = new System.Drawing.Point(6, 28);
            this.lblSituacao.Name = "lblSituacao";
            this.lblSituacao.ReadOnly = true;
            this.lblSituacao.Size = new System.Drawing.Size(153, 21);
            this.lblSituacao.TabIndex = 1;
            this.lblSituacao.TabStop = false;
            this.lblSituacao.Text = "Situação";
            // 
            // lblCliente
            // 
            this.lblCliente.BackColor = System.Drawing.Color.LightGray;
            this.lblCliente.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblCliente.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCliente.Location = new System.Drawing.Point(6, 48);
            this.lblCliente.Name = "lblCliente";
            this.lblCliente.ReadOnly = true;
            this.lblCliente.Size = new System.Drawing.Size(153, 21);
            this.lblCliente.TabIndex = 2;
            this.lblCliente.TabStop = false;
            this.lblCliente.Text = "Cliente";
            // 
            // lblEmissao
            // 
            this.lblEmissao.BackColor = System.Drawing.Color.LightGray;
            this.lblEmissao.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblEmissao.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblEmissao.Location = new System.Drawing.Point(421, 8);
            this.lblEmissao.Name = "lblEmissao";
            this.lblEmissao.ReadOnly = true;
            this.lblEmissao.Size = new System.Drawing.Size(104, 21);
            this.lblEmissao.TabIndex = 3;
            this.lblEmissao.TabStop = false;
            this.lblEmissao.Text = "Dt Emissão";
            // 
            // lblVencimento
            // 
            this.lblVencimento.BackColor = System.Drawing.Color.LightGray;
            this.lblVencimento.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblVencimento.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblVencimento.Location = new System.Drawing.Point(421, 28);
            this.lblVencimento.Name = "lblVencimento";
            this.lblVencimento.ReadOnly = true;
            this.lblVencimento.Size = new System.Drawing.Size(104, 21);
            this.lblVencimento.TabIndex = 4;
            this.lblVencimento.TabStop = false;
            this.lblVencimento.Text = "Dt Vencimento";
            // 
            // lblBaixa
            // 
            this.lblBaixa.BackColor = System.Drawing.Color.LightGray;
            this.lblBaixa.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblBaixa.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblBaixa.Location = new System.Drawing.Point(421, 48);
            this.lblBaixa.Name = "lblBaixa";
            this.lblBaixa.ReadOnly = true;
            this.lblBaixa.Size = new System.Drawing.Size(104, 21);
            this.lblBaixa.TabIndex = 5;
            this.lblBaixa.TabStop = false;
            this.lblBaixa.Text = "Dt \r\nBaixa\r\n";
            // 
            // lblCancelamento
            // 
            this.lblCancelamento.BackColor = System.Drawing.Color.LightGray;
            this.lblCancelamento.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblCancelamento.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCancelamento.Location = new System.Drawing.Point(421, 68);
            this.lblCancelamento.Name = "lblCancelamento";
            this.lblCancelamento.ReadOnly = true;
            this.lblCancelamento.Size = new System.Drawing.Size(104, 21);
            this.lblCancelamento.TabIndex = 6;
            this.lblCancelamento.TabStop = false;
            this.lblCancelamento.Text = "Dt Cancelamento";
            // 
            // dataEmissaoFinal
            // 
            this.dataEmissaoFinal.Checked = false;
            this.dataEmissaoFinal.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dataEmissaoFinal.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dataEmissaoFinal.Location = new System.Drawing.Point(640, 8);
            this.dataEmissaoFinal.Name = "dataEmissaoFinal";
            this.dataEmissaoFinal.ShowCheckBox = true;
            this.dataEmissaoFinal.Size = new System.Drawing.Size(117, 21);
            this.dataEmissaoFinal.TabIndex = 8;
            // 
            // dataEmissaoInicial
            // 
            this.dataEmissaoInicial.Checked = false;
            this.dataEmissaoInicial.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dataEmissaoInicial.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dataEmissaoInicial.Location = new System.Drawing.Point(524, 8);
            this.dataEmissaoInicial.Name = "dataEmissaoInicial";
            this.dataEmissaoInicial.ShowCheckBox = true;
            this.dataEmissaoInicial.Size = new System.Drawing.Size(117, 21);
            this.dataEmissaoInicial.TabIndex = 7;
            // 
            // dataVencimentoFinal
            // 
            this.dataVencimentoFinal.Checked = false;
            this.dataVencimentoFinal.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dataVencimentoFinal.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dataVencimentoFinal.Location = new System.Drawing.Point(640, 28);
            this.dataVencimentoFinal.Name = "dataVencimentoFinal";
            this.dataVencimentoFinal.ShowCheckBox = true;
            this.dataVencimentoFinal.Size = new System.Drawing.Size(117, 21);
            this.dataVencimentoFinal.TabIndex = 10;
            // 
            // dataVencimentoInicial
            // 
            this.dataVencimentoInicial.Checked = false;
            this.dataVencimentoInicial.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dataVencimentoInicial.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dataVencimentoInicial.Location = new System.Drawing.Point(524, 28);
            this.dataVencimentoInicial.Name = "dataVencimentoInicial";
            this.dataVencimentoInicial.ShowCheckBox = true;
            this.dataVencimentoInicial.Size = new System.Drawing.Size(117, 21);
            this.dataVencimentoInicial.TabIndex = 9;
            // 
            // dataBaixaFinal
            // 
            this.dataBaixaFinal.Checked = false;
            this.dataBaixaFinal.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dataBaixaFinal.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dataBaixaFinal.Location = new System.Drawing.Point(640, 48);
            this.dataBaixaFinal.Name = "dataBaixaFinal";
            this.dataBaixaFinal.ShowCheckBox = true;
            this.dataBaixaFinal.Size = new System.Drawing.Size(117, 21);
            this.dataBaixaFinal.TabIndex = 12;
            // 
            // dataBaixaInicial
            // 
            this.dataBaixaInicial.Checked = false;
            this.dataBaixaInicial.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dataBaixaInicial.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dataBaixaInicial.Location = new System.Drawing.Point(524, 48);
            this.dataBaixaInicial.Name = "dataBaixaInicial";
            this.dataBaixaInicial.ShowCheckBox = true;
            this.dataBaixaInicial.Size = new System.Drawing.Size(117, 21);
            this.dataBaixaInicial.TabIndex = 11;
            // 
            // dataCancelamentoFinal
            // 
            this.dataCancelamentoFinal.Checked = false;
            this.dataCancelamentoFinal.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dataCancelamentoFinal.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dataCancelamentoFinal.Location = new System.Drawing.Point(640, 68);
            this.dataCancelamentoFinal.Name = "dataCancelamentoFinal";
            this.dataCancelamentoFinal.ShowCheckBox = true;
            this.dataCancelamentoFinal.Size = new System.Drawing.Size(117, 21);
            this.dataCancelamentoFinal.TabIndex = 14;
            // 
            // dataCancelamentoInicial
            // 
            this.dataCancelamentoInicial.Checked = false;
            this.dataCancelamentoInicial.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dataCancelamentoInicial.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dataCancelamentoInicial.Location = new System.Drawing.Point(524, 68);
            this.dataCancelamentoInicial.Name = "dataCancelamentoInicial";
            this.dataCancelamentoInicial.ShowCheckBox = true;
            this.dataCancelamentoInicial.Size = new System.Drawing.Size(117, 21);
            this.dataCancelamentoInicial.TabIndex = 13;
            // 
            // textNumeroNota
            // 
            this.textNumeroNota.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textNumeroNota.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textNumeroNota.ForeColor = System.Drawing.Color.Black;
            this.textNumeroNota.Location = new System.Drawing.Point(158, 8);
            this.textNumeroNota.MaxLength = 20;
            this.textNumeroNota.Name = "textNumeroNota";
            this.textNumeroNota.Size = new System.Drawing.Size(244, 21);
            this.textNumeroNota.TabIndex = 15;
            // 
            // cbSituacao
            // 
            this.cbSituacao.BackColor = System.Drawing.Color.LightGray;
            this.cbSituacao.BorderColor = System.Drawing.Color.DimGray;
            this.cbSituacao.BorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid;
            this.cbSituacao.FormattingEnabled = true;
            this.cbSituacao.Location = new System.Drawing.Point(158, 28);
            this.cbSituacao.Name = "cbSituacao";
            this.cbSituacao.Size = new System.Drawing.Size(244, 21);
            this.cbSituacao.TabIndex = 16;
            // 
            // textCliente
            // 
            this.textCliente.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.textCliente.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textCliente.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textCliente.ForeColor = System.Drawing.Color.Black;
            this.textCliente.Location = new System.Drawing.Point(158, 48);
            this.textCliente.MaxLength = 20;
            this.textCliente.Name = "textCliente";
            this.textCliente.Size = new System.Drawing.Size(178, 21);
            this.textCliente.TabIndex = 17;
            // 
            // btnExcluirCliente
            // 
            this.btnExcluirCliente.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnExcluirCliente.Image = global::SWS.Properties.Resources.close;
            this.btnExcluirCliente.Location = new System.Drawing.Point(368, 48);
            this.btnExcluirCliente.Name = "btnExcluirCliente";
            this.btnExcluirCliente.Size = new System.Drawing.Size(34, 21);
            this.btnExcluirCliente.TabIndex = 19;
            this.btnExcluirCliente.UseVisualStyleBackColor = true;
            this.btnExcluirCliente.Click += new System.EventHandler(this.btnExcluirCliente_Click);
            // 
            // btnCliente
            // 
            this.btnCliente.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnCliente.Image = global::SWS.Properties.Resources.busca;
            this.btnCliente.Location = new System.Drawing.Point(335, 48);
            this.btnCliente.Name = "btnCliente";
            this.btnCliente.Size = new System.Drawing.Size(34, 21);
            this.btnCliente.TabIndex = 18;
            this.btnCliente.UseVisualStyleBackColor = true;
            this.btnCliente.Click += new System.EventHandler(this.btnCliente_Click);
            // 
            // grb_cobranca
            // 
            this.grb_cobranca.Controls.Add(this.dgvCobranca);
            this.grb_cobranca.Location = new System.Drawing.Point(6, 95);
            this.grb_cobranca.Name = "grb_cobranca";
            this.grb_cobranca.Size = new System.Drawing.Size(751, 293);
            this.grb_cobranca.TabIndex = 20;
            this.grb_cobranca.TabStop = false;
            this.grb_cobranca.Text = "Cobranças";
            // 
            // dgvCobranca
            // 
            this.dgvCobranca.AllowUserToAddRows = false;
            this.dgvCobranca.AllowUserToDeleteRows = false;
            this.dgvCobranca.AllowUserToOrderColumns = true;
            this.dgvCobranca.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this.dgvCobranca.BackgroundColor = System.Drawing.Color.White;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Arial", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvCobranca.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.dgvCobranca.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.Color.LightGray;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvCobranca.DefaultCellStyle = dataGridViewCellStyle2;
            this.dgvCobranca.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvCobranca.Location = new System.Drawing.Point(3, 16);
            this.dgvCobranca.MultiSelect = false;
            this.dgvCobranca.Name = "dgvCobranca";
            this.dgvCobranca.ReadOnly = true;
            this.dgvCobranca.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvCobranca.Size = new System.Drawing.Size(745, 274);
            this.dgvCobranca.TabIndex = 14;
            this.dgvCobranca.RowPrePaint += new System.Windows.Forms.DataGridViewRowPrePaintEventHandler(this.dgvCobranca_RowPrePaint);
            // 
            // pnlTotal
            // 
            this.pnlTotal.BackColor = System.Drawing.Color.LightGray;
            this.pnlTotal.Controls.Add(this.textValorTotal);
            this.pnlTotal.Controls.Add(this.lblValorTotalCobranca);
            this.pnlTotal.Controls.Add(this.textQuantidadeCobranca);
            this.pnlTotal.Controls.Add(this.lblQuantidadeCobranca);
            this.pnlTotal.Location = new System.Drawing.Point(9, 392);
            this.pnlTotal.Name = "pnlTotal";
            this.pnlTotal.Size = new System.Drawing.Size(748, 29);
            this.pnlTotal.TabIndex = 21;
            // 
            // textValorTotal
            // 
            this.textValorTotal.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.textValorTotal.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textValorTotal.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textValorTotal.Location = new System.Drawing.Point(339, 4);
            this.textValorTotal.Name = "textValorTotal";
            this.textValorTotal.ReadOnly = true;
            this.textValorTotal.Size = new System.Drawing.Size(100, 21);
            this.textValorTotal.TabIndex = 3;
            this.textValorTotal.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // lblValorTotalCobranca
            // 
            this.lblValorTotalCobranca.AutoSize = true;
            this.lblValorTotalCobranca.Location = new System.Drawing.Point(258, 8);
            this.lblValorTotalCobranca.Name = "lblValorTotalCobranca";
            this.lblValorTotalCobranca.Size = new System.Drawing.Size(75, 13);
            this.lblValorTotalCobranca.TabIndex = 2;
            this.lblValorTotalCobranca.Text = "Valor Total R$";
            // 
            // textQuantidadeCobranca
            // 
            this.textQuantidadeCobranca.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.textQuantidadeCobranca.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textQuantidadeCobranca.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textQuantidadeCobranca.Location = new System.Drawing.Point(146, 4);
            this.textQuantidadeCobranca.Name = "textQuantidadeCobranca";
            this.textQuantidadeCobranca.ReadOnly = true;
            this.textQuantidadeCobranca.Size = new System.Drawing.Size(100, 21);
            this.textQuantidadeCobranca.TabIndex = 1;
            this.textQuantidadeCobranca.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // lblQuantidadeCobranca
            // 
            this.lblQuantidadeCobranca.AutoSize = true;
            this.lblQuantidadeCobranca.Location = new System.Drawing.Point(14, 8);
            this.lblQuantidadeCobranca.Name = "lblQuantidadeCobranca";
            this.lblQuantidadeCobranca.Size = new System.Drawing.Size(126, 13);
            this.lblQuantidadeCobranca.TabIndex = 0;
            this.lblQuantidadeCobranca.Text = "Quantidade de Cobrança";
            // 
            // frmCobrancaPrincipal
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(784, 561);
            this.Name = "frmCobrancaPrincipal";
            this.Text = "GERENCIAR COBRANÇAS";
            this.spForm.Panel1.ResumeLayout(false);
            this.spForm.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.spForm)).EndInit();
            this.spForm.ResumeLayout(false);
            this.pnlForm.ResumeLayout(false);
            this.pnlForm.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.banner)).EndInit();
            this.flpAcao.ResumeLayout(false);
            this.grb_cobranca.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvCobranca)).EndInit();
            this.pnlTotal.ResumeLayout(false);
            this.pnlTotal.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.FlowLayoutPanel flpAcao;
        private System.Windows.Forms.Button btnLimpar;
        private System.Windows.Forms.Button btnPesquisar;
        private System.Windows.Forms.Button btnProrrogar;
        private System.Windows.Forms.Button btnAlterarFormaPgto;
        private System.Windows.Forms.Button btnReativar;
        private System.Windows.Forms.Button btnCancelar;
        private System.Windows.Forms.Button btnEstornar;
        private System.Windows.Forms.Button btnBaixar;
        private System.Windows.Forms.Button btnFechar;
        private System.Windows.Forms.TextBox lblNumeroNota;
        private System.Windows.Forms.TextBox lblSituacao;
        private System.Windows.Forms.TextBox lblCliente;
        private System.Windows.Forms.TextBox lblEmissao;
        private System.Windows.Forms.TextBox lblVencimento;
        private System.Windows.Forms.TextBox lblBaixa;
        private System.Windows.Forms.TextBox lblCancelamento;
        private System.Windows.Forms.DateTimePicker dataCancelamentoFinal;
        private System.Windows.Forms.DateTimePicker dataCancelamentoInicial;
        private System.Windows.Forms.DateTimePicker dataBaixaFinal;
        private System.Windows.Forms.DateTimePicker dataBaixaInicial;
        private System.Windows.Forms.DateTimePicker dataVencimentoFinal;
        private System.Windows.Forms.DateTimePicker dataVencimentoInicial;
        private System.Windows.Forms.DateTimePicker dataEmissaoFinal;
        private System.Windows.Forms.DateTimePicker dataEmissaoInicial;
        private System.Windows.Forms.TextBox textNumeroNota;
        private System.Windows.Forms.TextBox textCliente;
        private ComboBoxWithBorder cbSituacao;
        private System.Windows.Forms.Button btnExcluirCliente;
        private System.Windows.Forms.Button btnCliente;
        private System.Windows.Forms.GroupBox grb_cobranca;
        public System.Windows.Forms.DataGridView dgvCobranca;
        private System.Windows.Forms.Panel pnlTotal;
        private System.Windows.Forms.TextBox textQuantidadeCobranca;
        private System.Windows.Forms.Label lblQuantidadeCobranca;
        private System.Windows.Forms.TextBox textValorTotal;
        private System.Windows.Forms.Label lblValorTotalCobranca;
    }
}