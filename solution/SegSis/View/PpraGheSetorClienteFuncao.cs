﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using SWS.Facade;
using SWS.Entidade;
using SWS.View.Resources;

namespace SWS.View
{
    public partial class frm_ppraGheSetorClienteFuncao : BaseForm
    {

        GheSetor gheSetor;
        Cliente cliente;
                
        public frm_ppraGheSetorClienteFuncao(GheSetor gheSetor, Cliente cliente)
        {
            InitializeComponent();
            this.gheSetor = gheSetor;
            this.cliente = cliente;
            montaDataGrid();
        }

        public void montaDataGrid()
        {
            try
            {
                this.Cursor = Cursors.WaitCursor;

                PpraFacade ppraFacade = PpraFacade.getInstance();

                DataSet ds = ppraFacade.findAllFuncaoAtivaByGheSetorClienteFuncao(new GheSetorClienteFuncao(null, null, gheSetor, false, false, ApplicationConstants.ATIVO, null, false), cliente);

                this.Cursor = Cursors.Default;

                grd_funcao.DataSource = ds.Tables[0].DefaultView;

                grd_funcao.Columns[0].HeaderText = "id_seg_cliente_funcao";
                grd_funcao.Columns[0].Visible = false;

                grd_funcao.Columns[1].HeaderText = "id_funcao";
                grd_funcao.Columns[1].Visible = false;

                grd_funcao.Columns[2].HeaderText = "Descrição";
                
                grd_funcao.Columns[3].HeaderText = "Selec";
                grd_funcao.Columns[3].DisplayIndex = 0;
                
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
        
        }

        private void btn_fechar_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btn_confirmar_Click(object sender, EventArgs e)
        {
            try
            {
                PpraFacade ppraFacade = PpraFacade.getInstance();

                foreach (DataGridViewRow dvRow in grd_funcao.Rows)
                {

                    if ((Boolean)dvRow.Cells[3].Value)
                    {
                        ppraFacade.inserirGheSetorClienteFuncao(new GheSetorClienteFuncao(null,  new ClienteFuncao((Int64)dvRow.Cells[0].Value, cliente, new Funcao((Int64)dvRow.Cells[1].Value, (String)dvRow.Cells[2].Value, String.Empty, String.Empty, string.Empty), string.Empty, null, null), gheSetor, false, false, ApplicationConstants.ATIVO, null, false));
                    }

                }
                
                this.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }

        }
    }
}
