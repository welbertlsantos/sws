﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using SWS.Entidade;
using SWS.Facade;
using SWS.View.ViewHelper;
using SWS.ViewHelper;
using SWS.View.Resources;
namespace SWS.View
{
    public partial class frmFuncionarioPesquisa : SWS.View.frmTemplateConsulta
    {

        Funcionario funcionario;

        public Funcionario Funcionario
        {
            get { return funcionario; }
            set { funcionario = value; }
        }

        public frmFuncionarioPesquisa()
        {
            InitializeComponent();
            ComboHelper.pesquisaFuncionario(cbChavePesquisa);
            permissionamento();
            ActiveControl = textChave;
        }

        private void btnSelecionar_Click(object sender, EventArgs e)
        {
            try
            {
                if (dgvFuncionario.CurrentRow == null)
                    throw new Exception("Selecione um funcionário.");

                FuncionarioFacade funcionarioFacade = FuncionarioFacade.getInstance();
                funcionario = funcionarioFacade.findFuncionarioById((Int64)dgvFuncionario.CurrentRow.Cells[0].Value);
                this.Close();

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
        }

        private void btnFechar_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnNovo_Click(object sender, EventArgs e)
        {
            /* adicionando um novo funcionário */

            frmFuncionarioIncluir incluirFuncionario = new frmFuncionarioIncluir();
            incluirFuncionario.ShowDialog();

            if (incluirFuncionario.Funcionario != null)
            {
                /* funcionário já incluido */
                funcionario = incluirFuncionario.Funcionario;
                this.Close();
            }
        }

        private void btnBuscar_Click(object sender, EventArgs e)
        {
            try
            {
                if (String.IsNullOrEmpty(textChave.Text))
                {
                    if (MessageBox.Show("Você não digitou nenhuma informação para pesquisa. Isso pode demorar um pouco. Deseja continuar?", "Atenção", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                    {
                        montaDataGrid();
                        textChave.Focus();
                    }
                    else
                        textChave.Focus();
                }
                else
                {
                    montaDataGrid();
                    textChave.Focus();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
        }

        public void permissionamento()
        {
            PermissionamentoFacade permissionamento = PermissionamentoFacade.getInstance();
            this.btnNovo.Enabled = permissionamento.hasPermission("FUNCIONARIO", "INCLUIR");
        }

        public void montaDataGrid()
        {
            try
            {
                this.Cursor = Cursors.WaitCursor;

                Funcionario funcionarioPesquisa = new Funcionario(null, String.Equals(((SelectItem)cbChavePesquisa.SelectedItem).Valor, ApplicationConstants.FUNCIONARIO_NOME) ? textChave.Text.Trim().ToUpper() : String.Empty, String.Equals(((SelectItem)cbChavePesquisa.SelectedItem).Valor, ApplicationConstants.FUNCIONARIO_CPF) ? textChave.Text : String.Empty, String.Equals(((SelectItem)cbChavePesquisa.SelectedItem).Valor, ApplicationConstants.FUNCIONARIO_RG) ? textChave.Text : String.Empty,String.Empty, String.Empty, String.Empty, String.Empty, null, String.Empty, String.Empty, String.Empty, String.Empty, String.Empty, String.Empty, DateTime.Now, null, null, ApplicationConstants.ATIVO, String.Empty, String.Empty, String.Empty, String.Empty, false, string.Empty, string.Empty, string.Empty, string.Empty);

                FuncionarioFacade funcionarioFacade = FuncionarioFacade.getInstance();
                DataSet ds = funcionarioFacade.findFuncionarioByFilterByFuncionario(funcionarioPesquisa);

                dgvFuncionario.Columns.Clear();

                dgvFuncionario.ColumnCount = 5;

                dgvFuncionario.Columns[0].HeaderText = "Id Funcionario";
                dgvFuncionario.Columns[0].Visible = false;
                dgvFuncionario.Columns[1].HeaderText = "Nome";
                dgvFuncionario.Columns[2].HeaderText = "CPF";
                dgvFuncionario.Columns[3].HeaderText = "RG";
                dgvFuncionario.Columns[4].HeaderText = "Data de Nascimento";
                dgvFuncionario.Columns[4].HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleCenter;
                dgvFuncionario.Columns[4].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;


                /* adicionando a gride pelas linhas do dataset */

                foreach (DataRow row in ds.Tables[0].Rows)
                {
                    dgvFuncionario.Rows.Add(
                        Convert.ToInt64(row["id_funcionario"]),
                        row["nome"].ToString(),
                        row["formata_cpf"].ToString(),
                        row["rg"].ToString(),
                        Convert.ToDateTime(row["data_nascimento"]).ToShortDateString());
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }


        }

        private void cbChavePesquisa_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                if (String.Equals(((SelectItem)cbChavePesquisa.SelectedItem).Valor, ApplicationConstants.FUNCIONARIO_NOME))
                {
                    
                    textChave.Mask = String.Empty ;
                    textChave.PromptChar = ' ';
                    textChave.Text = String.Empty;
                    textChave.Focus();
                }
                else if (String.Equals(((SelectItem)cbChavePesquisa.SelectedItem).Valor, ApplicationConstants.FUNCIONARIO_RG))
                {
                    textChave.Mask = String.Empty;
                    textChave.PromptChar = ' ';
                    textChave.Focus();
                    textChave.Text = String.Empty;
                }
                else
                {
                    textChave.Text = String.Empty;
                    textChave.Mask = "999,999,999-99";
                    textChave.PromptChar = ' ';
                    textChave.Focus();
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
        }

        private void textChave_KeyPress(object sender, KeyPressEventArgs e)
        {
            
            /* verificando o comportamento de acordo com o tipo de seleção */

            if (!String.Equals(((SelectItem)cbChavePesquisa.SelectedItem).Valor, ApplicationConstants.FUNCIONARIO_NOME))
            {
                if (!String.Equals(((SelectItem)cbChavePesquisa.SelectedItem).Valor, ApplicationConstants.FUNCIONARIO_RG))
                {
                    e.Handled = ValidaCampoHelper.FormataCampoNumerico(sender, e);

                }
            }

        }

        private void frmFuncionarioPesquisa_Load(object sender, EventArgs e)
        {
            this.textChave.Focus();
        }

        private void dgvFuncionario_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                if (dgvFuncionario.CurrentRow == null)
                    throw new Exception("Selecione um funcionário.");

                FuncionarioFacade funcionarioFacade = FuncionarioFacade.getInstance();

                frmFuncionarioAlterar alteraFuncionario = new frmFuncionarioAlterar(funcionarioFacade.findFuncionarioById((long)dgvFuncionario.Rows[e.RowIndex].Cells[0].Value));
                alteraFuncionario.ShowDialog();

                funcionario = alteraFuncionario.Funcionario;
                this.Close();

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void textChave_Leave(object sender, EventArgs e)
        {
            /* verificar o tipo de campo e converter o valor digitado */

            if (String.Equals(((SelectItem)cbChavePesquisa.SelectedItem).Valor, ApplicationConstants.FUNCIONARIO_NOME)
                || String.Equals(((SelectItem)cbChavePesquisa.SelectedItem).Valor, ApplicationConstants.FUNCIONARIO_RG))
            {
                this.textChave.Text = this.textChave.Text.ToUpper();
            } 
            else 
            {
                textChave.Mask = "999,999,999-99";
            }
        }

        private void textChave_TextChanged(object sender, EventArgs e)
        {
            int cursorPosition = textChave.SelectionStart;

            textChave.Text = textChave.Text.ToUpper();

            textChave.SelectionStart = cursorPosition;
        }

       

       
    }
}
