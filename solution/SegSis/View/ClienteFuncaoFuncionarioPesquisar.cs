﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using SWS.Entidade;
using SWS.Facade;
using SWS.View.ViewHelper;

namespace SWS.View
{
    public partial class frmClienteFuncaoFuncionarioPesquisar : BaseFormConsulta
    {
        private Cliente cliente;

        public Cliente Cliente
        {
            get { return cliente; }
            set { cliente = value; }
        }

        private List<ClienteFuncaoFuncionario> clientes = new List<ClienteFuncaoFuncionario>();

        private Funcionario funcionario;

        private static String msg1 = "Selecione um cliente na gride.";

        public frmClienteFuncaoFuncionarioPesquisar(Funcionario funcionario)
        {
            InitializeComponent();
            this.funcionario = funcionario;
            montaDataGride();
        }

        private void btnSelecionar_Click(object sender, EventArgs e)
        {
            try
            {
                if (dgvCliente.CurrentRow == null)
                    throw new Exception(msg1);

                ClienteFacade clienteFacade = ClienteFacade.getInstance();
                cliente = clienteFacade.findClienteById((Int64)dgvCliente.CurrentRow.Cells[0].Value);
                this.Close();

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
        }

        public void montaDataGride()
        {
            try
            {
                FuncionarioFacade funcionarioFacade = FuncionarioFacade.getInstance();

                clientes = funcionarioFacade.findClienteFuncaoFuncionarioByFuncionarioAndSituacao(funcionario, true);

                /* montando informações da gride */

                dgvCliente.Columns.Clear();
                dgvCliente.ColumnCount = 4;

                dgvCliente.Columns[0].HeaderText = "Id Cliente";
                dgvCliente.Columns[0].Visible = false;

                dgvCliente.Columns[1].HeaderText = "Razão Social";

                dgvCliente.Columns[2].HeaderText = "CNPJ";

                dgvCliente.Columns[3].HeaderText = "Razão Social";

                foreach (ClienteFuncaoFuncionario clienteFuncaoFuncionario in clientes)
                {
                    dgvCliente.Rows.Add(
                        clienteFuncaoFuncionario.ClienteFuncao.Cliente.Id,
                        clienteFuncaoFuncionario.ClienteFuncao.Cliente.RazaoSocial,
                        clienteFuncaoFuncionario.ClienteFuncao.Cliente.Cnpj.Length == 14 ? ValidaCampoHelper.FormataCnpj(clienteFuncaoFuncionario.ClienteFuncao.Cliente.Cnpj) : ValidaCampoHelper.FormataCpf(clienteFuncaoFuncionario.ClienteFuncao.Cliente.Cnpj),
                        clienteFuncaoFuncionario.ClienteFuncao.Cliente.Fantasia);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }

        }

        private void btnFechar_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
