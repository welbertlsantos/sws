﻿using System;
using System.Windows.Forms;
using SWS.Facade;
using SWS.Excecao;
using SWS.Entidade;
using System.IO;
using System.Drawing;
using System.Collections.Generic;
using SWS.Resources;
using SWS.ViewHelper;
using System.Net;
using System.Xml.Linq;

namespace SWS.View
{
    public partial class Frm_Login : Form
    {
        private Usuario usuarioAutenticado;
        private string versaoSistema;

        public string VersaoSistema
        {
            get { return versaoSistema; }
            set { versaoSistema = value; }
        }

        public Usuario UsuarioAutenticado
        {
            get { return usuarioAutenticado; }
            set { usuarioAutenticado = value; }
        }

        private Dictionary<string, string> configuracao = new Dictionary<string, string>();
        private string versao;

        private List<Filial> listaFilial = new List<Filial>();

        public Frm_Login()
        {
            InitializeComponent();
            this.StartPosition = FormStartPosition.CenterScreen;
            this.AutoSize = false;
            this.MaximizeBox = false;
            this.MinimizeBox = true;
            this.FormBorderStyle = FormBorderStyle.FixedSingle;
            this.versao = "13.0";
            this.stversao.Text = stversao.Text + versao;
            this.text_login.Focus();

            /* carregando informações de conexão do arquivo */
            this.carregaInformacaoConexao();
       }

        private void bt_ok_Click(object sender, EventArgs e)
        {
            try
            {
                this.Cursor = Cursors.WaitCursor;

                if (validaCamposObrigatorios())
                {
                    PermissionamentoFacade permissionamentoFacade = PermissionamentoFacade.getInstance();
                    EmpresaFacade empresaFacade = EmpresaFacade.getInstance();
                    LoginAuditoriaFacade loginAuditoriaFacade = LoginAuditoriaFacade.getInstance();

                    /* verificando a filial que foi selecionada */

                    Filial filialSelecionada = listaFilial.Find(x => x.Id == Convert.ToInt32(((SelectItem)cbFilial.SelectedItem).Valor));
                    usuarioAutenticado = permissionamentoFacade.autenticaUsuario(new Usuario(text_login.Text, Util.getMD5Hash(text_senha.Text)), filialSelecionada);

                    loginAuditoriaFacade.desativarLoginAuditoria(text_login.Text, Dns.GetHostName());
                    loginAuditoriaFacade.registrarLoginAuditoria(text_login.Text, Dns.GetHostName());

                    /* verificando se a empresa está marcada para utilização em filiais */

                    string versaoSistema;
                    
                    configuracao = permissionamentoFacade.findAllConfiguracao();
                    configuracao.TryGetValue(ConfigurationConstans.VERSAO_SISTEMA, out versaoSistema);
                    this.VersaoSistema = versaoSistema;

                    if (!string.Equals(versaoSistema, versao)) throw new Exception("Sua versão está desatualizada. Atualize o sistema SWS para a versão " + versaoSistema + ".");
                    
                    /* criando arquivo de configuração para impressão */

                    this.geraArquivoConfiguracaoEmpresa();
                    
                    frmMenu principal = new frmMenu(this);
                    this.Hide();
                    principal.Show();
                }
                
            }
            catch (UsuarioNaoAutenticadoException ex)
            {
                dlg_login.Text = ex.Message;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }

        private void bt_limpar_Click(object sender, EventArgs e)
        {
            text_login.Text = String.Empty;
            text_senha.Text = String.Empty;
            dlg_login.Text = String.Empty;
            text_login.Focus();
        }

        private Boolean validaCamposObrigatorios(){

            Boolean retorno = true;
            try
            {
                if (String.IsNullOrEmpty(text_login.Text.Trim()))
                {
                    retorno = false;
                    text_login.Focus();
                    throw new Exception("Obrigatório preencher o login");

                }
                else if (String.IsNullOrEmpty(text_senha.Text.Trim()))
                {
                    retorno = false;
                    text_senha.Focus();
                    throw new Exception("Obrigatório preencher a senha de acesso");
                }

                if (cbFilial.SelectedIndex == -1)
                {
                    retorno = false;
                    ActiveControl = cbFilial;
                    throw new Exception("Obrigatório selecionar a empresa");
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            return retorno;
            
        }

        private void Frm_Login_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
                this.SelectNextControl(this.ActiveControl, !e.Shift, true, true, true);
        }

        private void lblEsqueciSenha_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            frmEsqueciSenha esqueciSenha = new frmEsqueciSenha();
            esqueciSenha.ShowDialog();

            if (esqueciSenha.Usuario != null)
            {
                text_login.Text = esqueciSenha.Usuario.Login;
                text_senha.Text = "padrao";
                bt_ok.PerformClick();
            }

        }

        private void btnCancelar_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void montaComboFilial()
        {
            try
            {
                cbFilial.Items.Clear();

                this.listaFilial.ForEach(delegate(Filial filial)
                {
                    cbFilial.Items.Add(new SelectItem(filial.Id.ToString(), filial.Name));
                });

                cbFilial.DisplayMember = "Nome";
                cbFilial.ValueMember = "Valor";
                cbFilial.DropDownStyle = ComboBoxStyle.DropDownList;

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
                
        }

        private void carregaInformacaoConexao()
        {
            try
            {
                string currentDiretory = Directory.GetCurrentDirectory();

                string filePath = Path.Combine(currentDiretory, "Config", "system_connection.xml");


                XElement xml = XElement.Load(filePath);

                foreach (XElement elemento in xml.Elements())
                {
                    Filial filial = new Filial();
                    filial.Id = Convert.ToInt32(elemento.Attribute("id").Value);
                    filial.Name = Convert.ToString(elemento.Attribute("name").Value);
                    filial.Host = Convert.ToString(elemento.Attribute("server").Value);
                    filial.Port = Convert.ToString(elemento.Attribute("port").Value);
                    filial.User = Convert.ToString(elemento.Attribute("user").Value);
                    filial.Password = Convert.ToString(elemento.Attribute("password").Value);
                    filial.Database = Convert.ToString(elemento.Attribute("database").Value);
                    filial.Schema = Convert.ToString(elemento.Attribute("schema").Value);
                    listaFilial.Add(filial);
                }

                /* carregando combo de filiais */
                this.montaComboFilial();

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void geraArquivoConfiguracaoEmpresa()
        {
            try
            {
                string filePath = Path.Combine("C:\\SWS\\", "Relatorio", "Connection", "ConexaoRelatorio.properties");

                string host = PermissionamentoFacade.filialAutenticada.Host;
                string port = PermissionamentoFacade.filialAutenticada.Port;
                string database = PermissionamentoFacade.filialAutenticada.Database;
                string user = PermissionamentoFacade.filialAutenticada.User;
                string password = PermissionamentoFacade.filialAutenticada.Password;

                using (StreamWriter writer = new StreamWriter(filePath, false))
                {
                    writer.WriteLine("driver=org.postgresql.Driver");
                    writer.WriteLine("url=jdbc:postgresql://" + host + ":" + port + "/" + database);
                    writer.WriteLine("user=" + user);
                    writer.WriteLine("password=" + password);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
    }
}
