﻿using System;
using System.Data;
using System.Windows.Forms;
using SWS.Entidade;
using SWS.Facade;
using SWS.ViewHelper;
using SWS.View.ViewHelper;

namespace SWS.View
{
    public partial class frmAgentesIncluir : SWS.View.frmTemplate
    {
        protected Agente agente;

        public Agente Agente
        {
            get { return agente; }
            set { agente = value; }
        }

        public frmAgentesIncluir()
        {
            InitializeComponent();
            ComboHelper.carregaComboRisco(cbRisco);
            ActiveControl = textDescricao;
        }

        protected virtual void btGravar_Click(object sender, EventArgs e)
        {
            try
            {
                this.errorAgenteIncluir.Clear();
                if (validaCamposObrigatorio())
                {

                    EstudoFacade PcmsoFacade = EstudoFacade.getInstance();

                    agente = PcmsoFacade.insertAgente(new Agente(null, new Risco(Convert.ToInt64(((SelectItem)cbRisco.SelectedItem).Valor), ((SelectItem)cbRisco.SelectedItem).Nome), textDescricao.Text.Trim(),
                                textTrajetoria.Text.Trim(), textDano.Text.Trim(),
                                textLimite.Text.Trim(), string.Empty, false, textIntensidade.Text.Trim(), textTecnica.Text.Trim(), textCodigoEsocial.Text));

                    MessageBox.Show("Agente incluído com sucesso.", "Confirmação", MessageBoxButtons.OK, MessageBoxIcon.Information);

                    this.Close();

                }

            }
            catch (Exception ex) { MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error); }
        }

        protected void btLimpar_Click(object sender, EventArgs e)
        {
            try
            {
                this.textDescricao.Text = String.Empty;
                this.textDano.Text = String.Empty;
                this.textLimite.Text = String.Empty;
                this.textTrajetoria.Text = String.Empty;
                ComboHelper.carregaComboRisco(cbRisco);
                this.errorAgenteIncluir.Clear();
                textIntensidade.Text = string.Empty;
                textTecnica.Text = string.Empty;

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        protected void btFechar_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        protected Boolean validaCamposObrigatorio()
        {
            Boolean retorno = true;

            if (string.IsNullOrEmpty(textDescricao.Text.Trim()))
            {
                this.errorAgenteIncluir.SetError(this.textDescricao, "Campo obrigatório");
                retorno = false;
            }

            if (cbRisco.SelectedIndex == 0)
            {
                this.errorAgenteIncluir.SetError(cbRisco, "campo obrigatório");
                retorno = false;
            }

            if (string.IsNullOrEmpty(textCodigoEsocial.Text))
            {
                this.errorAgenteIncluir.SetError(btnCodigoEsocial, "Codigo Esocial Obrigatório");
                retorno = false;
            }

            return retorno;
        }

        protected void frmAgentesIncluir_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter || e.KeyCode == Keys.Return)
                SendKeys.Send("{TAB}");
        }

        protected void btnCodigoEsocial_Click(object sender, EventArgs e)
        {
            try
            {
                frmComplementoAgenteEsocial buscarCodigoEsocial = new frmComplementoAgenteEsocial();
                buscarCodigoEsocial.ShowDialog();

                if (buscarCodigoEsocial.ComplementoAgenteEsocial != null)
                {
                    textCodigoEsocial.Text = buscarCodigoEsocial.ComplementoAgenteEsocial.Codigo;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            
            
            
            
            

        }
    }
}
