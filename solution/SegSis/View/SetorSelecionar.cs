﻿using SWS.Entidade;
using SWS.Facade;
using SWS.View.Resources;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SWS.View
{
    public partial class frmSetorSelecionar : frmTemplateConsulta
    {
        private Setor setor;

        public Setor Setor
        {
            get { return setor; }
            set { setor = value; }
        }
        
        
        public frmSetorSelecionar()
        {
            InitializeComponent();
            validaPermissoes();
            ActiveControl = textSetor;
        }

        private void btnFechar_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnBuscar_Click(object sender, EventArgs e)
        {
            montaGridSetor();
        }

        private void montaGridSetor()
        {
            try
            {
                dgvSetor.Columns.Clear();
                dgvSetor.ColumnCount = 2;

                dgvSetor.Columns[0].HeaderText = "idSetor";
                dgvSetor.Columns[0].Visible = false;
                dgvSetor.Columns[1].HeaderText = "Setor";

                EstudoFacade pcmsoFacade = EstudoFacade.getInstance();

                Setor setor = new Setor();
                setor.Situacao = ApplicationConstants.ATIVO;
                setor.Descricao = textSetor.Text.Trim();

                DataSet ds = pcmsoFacade.findSetorByFilter(setor);

                foreach (DataRow row in ds.Tables[0].Rows)
                    dgvSetor.Rows.Add(
                        row["ID_SETOR"],
                        row["DESCRICAO"].ToString());
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }

        }

        private void validaPermissoes()
        {
            PermissionamentoFacade permissionamentoFacade = PermissionamentoFacade.getInstance();
            this.btnNovo.Enabled = permissionamentoFacade.hasPermission("SETOR", "INCLUIR");
        }

        private void btnGravar_Click(object sender, EventArgs e)
        {
            try
            {
                if (dgvSetor.CurrentRow == null)
                    throw new Exception("Selecione um setor.");

                EstudoFacade pcmsoFacade = EstudoFacade.getInstance();

                Setor = pcmsoFacade.findSetorById((long)dgvSetor.CurrentRow.Cells[0].Value);
                this.Close();
                
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }

        private void btnNovo_Click(object sender, EventArgs e)
        {
            frmSetorIncluir setorIncluir = new frmSetorIncluir();
            setorIncluir.ShowDialog();

            if (setorIncluir.Setor != null)
            {
                Setor = setorIncluir.Setor;
                this.Close();
            }
            
        }
    }
}
