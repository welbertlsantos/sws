﻿using SWS.Entidade;
using SWS.View.Resources;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace SWS.View
{
    public partial class frmProtoloObservacao : frmTemplateConsulta
    {
        Protocolo protocolo;

        public Protocolo Protocolo
        {
            get { return protocolo; }
            set { protocolo = value; }
        }
        string acao;
        private const Int32 tamanhoMinimo = 10;

        public frmProtoloObservacao(Protocolo protocolo, String acao)
        {
            InitializeComponent();
            this.protocolo = protocolo;
            this.acao = acao;

            if (String.Equals(acao, ApplicationConstants.DELETEPROTOCOLO))
            {
                /* mudanças na tela */
                this.Text = "INCLUIR MOTIVO DE CANCELAMENTO";
            }
            else if (String.Equals(acao, ApplicationConstants.FINALIZAPROTOCOLO))
            {
                this.Text = "OBSERVAÇAO NO PROTOCOLO";
                this.btnConfirmar.Visible = false;
                this.textObservacao.Enabled = false;
                this.textObservacao.ReadOnly = true;
                this.textObservacao.Text = protocolo.Observacao;
            }
        }

        private void btnCancelar_Click(object sender, EventArgs e)
        {
            try
            {
                if (!validaTela())
                {
                    protocolo.MotivoCancelamento = textObservacao.Text.ToUpper().Trim();
                    this.Close();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btnFechar_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private bool validaTela()
        {
            bool retorno = false;
            try
            {
                if (string.IsNullOrEmpty(textObservacao.Text))
                    throw new Exception("Campo obrigatório.");

                if (textObservacao.Text.Length < tamanhoMinimo)
                    throw new Exception("Tamanho mínimo para o campo é de " + tamanhoMinimo + " caracteres.");
                
            }
            catch (Exception ex)
            {
                retorno = true;
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

            return retorno;
        }
    }
}
