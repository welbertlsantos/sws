﻿using SWS.Entidade;
using SWS.Excecao;
using SWS.Facade;
using SWS.Helper;
using SWS.View.ViewHelper;
using SWS.ViewHelper;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Security;
using System.Text;
using System.Windows.Forms;

namespace SWS.View
{
    public partial class frmMedicoIncluir : frmTemplate
    {
        protected Medico medico;

        public Medico Medico
        {
            get { return medico; }
            set { medico = value; }
        }

        protected Arquivo assinatura;

        public Arquivo Assinatura
        {
            get { return assinatura; }
            set { assinatura = value; }
        }
        
        public frmMedicoIncluir()
        {
            InitializeComponent();
            ComboHelper.unidadeFederativa(cb_uf);
            ComboHelper.unidadeFederativa(cbUfCrm);
            ActiveControl = text_nome;
        }

        protected virtual void bt_gravar_Click(object sender, EventArgs e)
        {
            try
            {
                this.IncluirMedicoErrorProvider.Clear();

                if (validaCamposObrigatorios())
                {
                    EstudoFacade pcmsoFacade = EstudoFacade.getInstance();
                    ClienteFacade clienteFacade = ClienteFacade.getInstance();

                    medico = pcmsoFacade.insertMedico(new Medico(null, text_nome.Text.Trim(), text_crm.Text, string.Empty, text_telefone1.Text, text_telefone2.Text, text_email.Text, text_endereco.Text, text_numero.Text, text_complemento.Text, text_bairro.Text, text_cep.Text, ((SelectItem)cb_uf.SelectedItem).Valor.ToString(), cbCidade.SelectedIndex == -1 ? null : clienteFacade.findCidadeIbgeById(Convert.ToInt64(((SelectItem)cbCidade.SelectedItem).Valor)), textPisPasep.Text, ((SelectItem)cbUfCrm.SelectedItem).Valor, textDocumentoExtra.Text, assinatura == null ? null : assinatura.Conteudo, assinatura == null ? string.Empty : assinatura.Mimetype, textCpf.Text, textRqe.Text)); 
                    
                    MessageBox.Show("Médico incluído com sucesso.", "Confirmação", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    this.Close();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        protected void bt_limpar_Click(object sender, EventArgs e)
        {
            this.IncluirMedicoErrorProvider.Clear();
            text_nome.Text = String.Empty;
            text_crm.Text = String.Empty;
            text_nome.Focus();
            text_telefone1.Text = String.Empty;
            text_telefone2.Text = String.Empty;
            text_email.Text = String.Empty;
            text_endereco.Text = String.Empty;
            text_numero.Text = String.Empty;
            text_complemento.Text = String.Empty;
            text_bairro.Text = String.Empty;
            text_cep.Text = String.Empty;
            ComboHelper.unidadeFederativa(cb_uf);
            cbCidade.DataSource = null;
            textPisPasep.Text = string.Empty;
            ComboHelper.unidadeFederativa(cbUfCrm);
            textDocumentoExtra.Text = string.Empty;
            textCpf.Text = string.Empty;

        }

        protected void bt_fechar_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        protected Boolean validaCamposObrigatorios()
        {
            Boolean retorno = true;

            if (string.IsNullOrEmpty(text_nome.Text))
            {
                this.IncluirMedicoErrorProvider.SetError(this.text_nome, "O Nome é obrigatório!");
                retorno = false;
            }

            if (string.IsNullOrEmpty(text_crm.Text))
            {
                this.IncluirMedicoErrorProvider.SetError(this.text_crm, "O CRM é obrigatório!");
                retorno = false;
            }

            if (string.IsNullOrEmpty(text_telefone1.Text))
            {
                this.IncluirMedicoErrorProvider.SetError(this.text_telefone1, "O telefone é obrigatório.");
                retorno = false;
            }

            if (cbUfCrm.SelectedIndex == 0)
            {
                this.IncluirMedicoErrorProvider.SetError(this.cbUfCrm, "O campo UF emissor do CRM é obrigatório");
                retorno = false;
            }

            return retorno;
        }

        protected void text_cep_Leave(object sender, EventArgs e)
        {
            try
            {
                if (!ValidaCampoHelper.ValidaCepDigitado(text_cep.Text))
                {
                    throw new Exception("CEP inválido.");
                    text_cep.Focus();
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        protected void frmMedicoIncluir_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter || e.KeyCode == Keys.Return)
                SendKeys.Send("{TAB}");
        }

        protected void cb_uf_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                if (cb_uf.SelectedIndex == -1)
                    throw new Exception("Selecione uma Unidade Federativa primeiro.");

                ComboHelper.iniciaComboCidade(cbCidade, ((SelectItem)cb_uf.SelectedItem).Valor.ToString());

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        protected void cbCidade_Click(object sender, EventArgs e)
        {
            try
            {
                if (cb_uf.SelectedIndex == 0)
                    throw new Exception("Selecione uma Unidade Federativa primeiro.");

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        protected void btnAssinatura_Click(object sender, EventArgs e)
        {
            OpenFileDialog assinaturaDialog = new OpenFileDialog();

            assinaturaDialog.Multiselect = false;
            assinaturaDialog.Title = "Selecionar a imagem da assinatura";
            assinaturaDialog.InitialDirectory = @"C:\";

            //filtra para exibir somente arquivos de imagens
            assinaturaDialog.Filter = "Imagens(*.jpg;*.bmp;*.png)|*.jpg;*.bmp;*.png";
            assinaturaDialog.CheckFileExists = true;
            assinaturaDialog.CheckPathExists = true;
            assinaturaDialog.FilterIndex = 2;
            assinaturaDialog.RestoreDirectory = true;
            assinaturaDialog.ReadOnlyChecked = true;
            assinaturaDialog.ShowReadOnly = true;

            DialogResult dr = assinaturaDialog.ShowDialog();

            if (dr == System.Windows.Forms.DialogResult.OK)
            {
                String nomeArquivo = assinaturaDialog.FileName;
                String nomeArquivoFinal = nomeArquivo.Substring(nomeArquivo.LastIndexOf('\\') + 1);
                String extensaoArquivo = nomeArquivo.Substring(nomeArquivo.LastIndexOf('.') + 1);

                try
                {
                    Image Imagem = Image.FromFile(nomeArquivo);
                    pbAssinatura.SizeMode = PictureBoxSizeMode.StretchImage;
                    
                    if (Imagem.Width > 500 && Imagem.Height > 500)
                        throw new LogoException("A imagem deve ter um tamanho máximo de 500 X 500 pixels.");

                    pbAssinatura.Image = Imagem;

                    byte[] conteudo = FileHelper.CarregarArquivoImagem(nomeArquivo, 5242880);

                    assinatura = new Arquivo(null, nomeArquivoFinal, "image/" + extensaoArquivo.ToUpper(), conteudo.Length, conteudo);
                }
                catch (SecurityException ex)
                {
                    // O usuário  não possui permissão para ler arquivos
                    MessageBox.Show("Erro de segurança Contate o administrador de segurança da rede.\n\n" + "Mensagem : " + ex.Message + "\n\n" + "Detalhes (enviar ao suporte):\n\n" + ex.StackTrace, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
                catch (LogoException ex)
                {
                    MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
                catch (Exception ex)
                {
                    // Não pode carregar a imagem (problemas de permissão)
                    MessageBox.Show("Não é possível exibir a imagem : " + nomeArquivoFinal + ". Você pode não ter permissão para ler o arquivo , ou " + " ele pode estar corrompido.\n\nErro reportado : " + ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }

        }

        private void textCpf_Leave(object sender, EventArgs e)
        {
            try
            {
                try
                {
                    if (!String.IsNullOrEmpty(textCpf.Text.Replace(".", "").Replace("-", "").Trim()))
                    {
                        if (!ValidaCampoHelper.ValidaCPF(textCpf.Text))
                            throw new Exception("CPF digitado é inválido.");
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    textCpf.Focus();
                    textCpf.Select(0, 0);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }

        
    }
}
