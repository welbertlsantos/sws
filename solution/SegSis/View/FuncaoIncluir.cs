﻿using SWS.Entidade;
using SWS.Facade;
using SWS.View.Resources;
using SWS.View.ViewHelper;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace SWS.View
{
    public partial class frmFuncaoIncluir : frmTemplate
    {
        private Funcao funcao;

        public Funcao Funcao
        {
            get { return funcao; }
            set { funcao = value; }
        }

        private List<ComentarioFuncao> comentarios = new List<ComentarioFuncao>();

        public List<ComentarioFuncao> Comentarios
        {
            get { return comentarios; }
            set { comentarios = value; }
        }
        
        public frmFuncaoIncluir()
        {
            InitializeComponent();
            ActiveControl = textDescricao;
        }

        protected void textCodioCbo_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = ValidaCampoHelper.FormataCampoNumerico(sender, e);
        }

        protected virtual void btGravar_Click(object sender, EventArgs e)
        {
            try
            {
                if (validaCamposObrigatorio())
                {
                    EstudoFacade PcmsoFacade = EstudoFacade.getInstance();

                    funcao = new Funcao(null, textDescricao.Text, textCodioCbo.Text, ApplicationConstants.ATIVO, string.Empty);
                    funcao.Atividades = comentarios;

                    funcao = PcmsoFacade.insertFuncao(funcao);

                    MessageBox.Show("Função incluída com sucesso.", "Confirmação", MessageBoxButtons.OK, MessageBoxIcon.Information);

                    this.Close();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }

        protected void btFechar_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        protected virtual void btIncluirComentario_Click(object sender, EventArgs e)
        {
            try
            {
                frmFuncaoComentarioIncluir formComentarioIncluir = new frmFuncaoComentarioIncluir();
                formComentarioIncluir.ShowDialog();

                if (formComentarioIncluir.ComentarioFuncao != null)
                {
                    if (comentarios.Exists(x => string.Equals(x.Atividade, formComentarioIncluir.ComentarioFuncao)))
                        throw new Exception("Já existe essa atividade cadastrada para essa função.");
                    
                    comentarios.Add(formComentarioIncluir.ComentarioFuncao);
                    montaDataGrid();
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }

        protected virtual void btExcluirAtividade_Click(object sender, EventArgs e)
        {
            try
            {
                if (dgvComentario.CurrentRow == null)
                    throw new Exception("Selecione uma linha");

                ComentarioFuncao comentarioFuncaoExcluir = comentarios.Find(x => string.Equals(x.Atividade, (string)dgvComentario.CurrentRow.Cells[1].Value));

                if (MessageBox.Show("Deseja excluir a atividade selecionada?", "Atenção", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                {
                    comentarios.Remove(comentarioFuncaoExcluir);
                    montaDataGrid();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }

        protected void frmFuncaoIncluir_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter || e.KeyCode == Keys.Return)
                SendKeys.Send("{TAB}");
        }

        protected bool validaCamposObrigatorio()
        {
            bool retorno = true;

            try
            {
                if (string.IsNullOrEmpty(textDescricao.Text.Trim()))
                    throw new Exception("Campo nome da função é obrigatório.");

                if (dgvComentario.Rows.Count == 0)
                    throw new Exception("A função deve ter pelo menos uma atividade cadastrada.");
            }
            catch (Exception ex)
            {
                retorno = false;
                MessageBox.Show(ex.Message, "Atençao", MessageBoxButtons.OK, MessageBoxIcon.Error);
                
            }

            return retorno;
        }

        protected void montaDataGrid()
        {
            try
            {
                this.dgvComentario.Columns.Clear();
                this.dgvComentario.ColumnCount = 2;

                this.dgvComentario.Columns[0].HeaderText = "id";
                this.dgvComentario.Columns[0].Visible = false;

                this.dgvComentario.Columns[1].HeaderText = "Atividade";

                comentarios.ForEach(delegate(ComentarioFuncao comentario)
                {
                    this.dgvComentario.Rows.Add(comentario.Id, comentario.Atividade);
                });
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }

        protected virtual void dgvComentario_CellContentDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                if (dgvComentario.CurrentRow == null)
                    throw new Exception("Selecione uma linha.");

                frmFuncaoComentarioAlterar alterarComentario = new frmFuncaoComentarioAlterar(comentarios.Find(x => string.Equals(x.Atividade, (string)dgvComentario.CurrentRow.Cells[1].Value)));
                alterarComentario.ShowDialog();
                montaDataGrid();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }
    }
}
