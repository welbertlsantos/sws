﻿using SWS.Entidade;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace SWS.View
{
    public partial class frmPerfilDetalhar : frmPerfilIncluir
    {
        public frmPerfilDetalhar(Perfil perfil) :base()
        {
            InitializeComponent();
            textPerfil.Text = perfil.Descricao;
            textPerfil.Enabled = false;
            btnGravar.Enabled = false;
        }
    }
}
