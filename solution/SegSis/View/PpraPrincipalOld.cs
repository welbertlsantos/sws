﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using SWS.Facade;
using SWS.Entidade;
using SWS.Excecao;
using SWS.View.ViewHelper;
using System.Text.RegularExpressions;
using SWS.View.Resources;
using System.IO;
using System.Diagnostics;

namespace SWS.View
{
    public partial class frm_ppraOld : BaseForm
    {
       
        Cliente cliente = null; 
        
        Usuario engenheiro; 
        Usuario tecnico; 

        public static String Msg02 = " Selecione uma linha!";
        public static String Msg03 = " Deseja realmente excluir esse PPRA?";
        public static String Msg04 = " Atenção ";
        public static String Msg05 = " Confirmação ";
        public static String Msg06 = " PPRA excluído com sucesso!";
        public static String Msg07 = " Revisão concluída com sucesso !";
        public static String Msg08 = " Deseja finalizar esse PPRA?";
        public static String Msg09 = " PPRA finalizado com sucesso.";
        public static String Msg10 = " Deseja enviar esse estudo para análise?";
        public static String Msg11 = " PPRA em análise, deseja enviá-lo para Construção? ";
        public static String Msg12 = " PPRA enviado para analise com sucesso. ";
        public static String Msg13 = " PPRA enviado novamente para construção com sucesso. ";

        public static String msg14 = "Deseja iniciar uma correção para o estudo selecionado?";
        public static String msg15 = "Somente estudos finalizados podem ser corrigidos.";
        public static String msg16 = "Somente estudos finalizados podem ter sido corrigidos.";

        Estudo ppra = null;

        public String comentario = String.Empty;

        public frm_ppraOld()
        {
            InitializeComponent();
            rb_todos.Checked = true;
            ck_dataCriacao.Checked = false;
            validaPermissoes();
        }

        public void setPPra(Estudo ppra)
        {
            this.ppra = ppra;
        }

        private void validaPermissoes()
        {
            PermissionamentoFacade permissionamentoFacade = PermissionamentoFacade.getInstance();

            bt_alterar.Enabled = permissionamentoFacade.hasPermission("PPRA", "ALTERAR");
            bt_incluir.Enabled = permissionamentoFacade.hasPermission("PPRA", "INCLUIR");
            bt_excluir.Enabled = permissionamentoFacade.hasPermission("PPRA", "EXCLUIR");
            bt_imprimir.Enabled = permissionamentoFacade.hasPermission("PPRA", "IMPRIMIR");
            btn_enviarAnalise.Enabled = permissionamentoFacade.hasPermission("PPRA", "ENVIAR");
            btn_finalizar.Enabled = permissionamentoFacade.hasPermission("PPRA", "FINALIZAR");
            btn_revisao.Enabled = permissionamentoFacade.hasPermission("PPRA", "REVISAR");
            btn_corrigir.Enabled = permissionamentoFacade.hasPermission("PPRA", "CORRIGIR");

        }

        private void bt_fechar_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void ck_dataCadastro_CheckedChanged(object sender, EventArgs e)
        {
            if (ck_dataCriacao.Checked)
            {
                dtCriacao_inicial.Enabled = true;
                dtCriacao_final.Enabled = true;
            }
            else
            {
                dtCriacao_inicial.Enabled = false;
                dtCriacao_final.Enabled = false;
            }
        }

        private void bt_limpar_Click(object sender, EventArgs e)
        {
            text_codPpra.Text = String.Empty;
            rb_todos.Checked = true;
            
            cliente = null;
            text_cliente.Text = String.Empty;

            engenheiro = null;
            text_engenheiro.Text = String.Empty;

            tecnico = null;
            text_tecno.Text = String.Empty;
            
            ck_dataCriacao.Checked = false;
            grd_ppra.Columns.Clear();
        }

        private void bt_pesquisar_Click(object sender, EventArgs e)
        {
            try
            {
                this.PpraErrorProvider.Clear();

                this.Cursor = Cursors.WaitCursor;

                if (validaCampos())
                {
                    montaDataGrid();
                    text_codPpra.Focus();
                    remontarDataGrid = true;
                }
            }
            catch (Exception Ex)
            {
                MessageBox.Show(Ex.Message);
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }

        private Boolean validaCampos()
        {
            Boolean retorno = Convert.ToBoolean(true);

            if (Convert.ToDateTime(this.dtCriacao_final.Text) < Convert.ToDateTime(this.dtCriacao_inicial.Text))
            {
                this.PpraErrorProvider.SetError(this.dtCriacao_final, "A data final deve ser maior que a inicial!");
                retorno = Convert.ToBoolean(false);
            }

            return retorno;

        }

        public void montaDataGrid()
        {
            try
            {
                PpraFacade ppraFacade = PpraFacade.getInstance();

                this.Cursor = Cursors.WaitCursor;

                ppra = new Estudo();
                ppra.CodigoEstudo = text_codPpra.Text;
                ppra.VendedorCliente = cliente != null ? new VendedorCliente(null, cliente, null, String.Empty) : null;
                ppra.Tecno = tecnico;
                ppra.Engenheiro = engenheiro;
                ppra.Situacao = rb_aguardando.Checked ? ApplicationConstants.ATIVO : rb_construcao.Checked ? ApplicationConstants.CONSTRUCAO : rb_fechado.Checked ? ApplicationConstants.FECHADO : string.Empty;
                ppra.DataCriacao = ck_dataCriacao.Checked ? Convert.ToDateTime(dtCriacao_inicial.Text) : (DateTime?)null;

                DataSet ds = ppraFacade.findPpraByFilter(ppra, ck_dataCriacao.Checked ? Convert.ToDateTime(dtCriacao_final.Text) : (DateTime?)null);

                grd_ppra.DataSource = ds.Tables["Estudos"].DefaultView;

                grd_ppra.Columns[0].HeaderText = "ID";
                grd_ppra.Columns[0].Visible = false;
                grd_ppra.Columns[1].HeaderText = "Código PPRA";
                grd_ppra.Columns[2].HeaderText = "Data da Criação";
                grd_ppra.Columns[3].HeaderText = "Data de Vencimento";
                grd_ppra.Columns[4].HeaderText = "Data de Fechamento";
                grd_ppra.Columns[5].HeaderText = "Situação";
                grd_ppra.Columns[6].HeaderText = "Cliente";
                grd_ppra.Columns[6].DisplayIndex = 0;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
            
        }

        private void btn_clienteLocalizar_Click(object sender, EventArgs e)
        {

            frmClienteSelecionar formCliente = new frmClienteSelecionar(null, null, false, null, null, null, null, false, true);
            formCliente.ShowDialog();

            if (formCliente.Cliente != null)
            {
                cliente = formCliente.Cliente;
                text_cliente.Text = cliente.RazaoSocial;
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            frm_ppraUsuarioTecnicoSelecionar formUsuarioFuncaoInternaSelecionar = new frm_ppraUsuarioTecnicoSelecionar();
            formUsuarioFuncaoInternaSelecionar.ShowDialog();

            if (formUsuarioFuncaoInternaSelecionar.getTecnico() != null)
            {
                tecnico = formUsuarioFuncaoInternaSelecionar.getTecnico();
                text_tecno.Text = tecnico.Nome;
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            frm_ppraUsuarioEngenheiroSelecionar formUsuarioEngenheiroSelecionar = new frm_ppraUsuarioEngenheiroSelecionar();
            formUsuarioEngenheiroSelecionar.ShowDialog();

            if (formUsuarioEngenheiroSelecionar.getEngenheiro() != null)
            {
                engenheiro = formUsuarioEngenheiroSelecionar.getEngenheiro();
                text_engenheiro.Text = engenheiro.Nome;
            }
        }

        private void bt_incluir_Click(object sender, EventArgs e)
        {
            frm_ppraIncluirOld formPPRAIncluir = new frm_ppraIncluirOld();
            formPPRAIncluir.ShowDialog();
        }

        private void bt_alterar_Click(object sender, EventArgs e)
        {
            try
            {
                this.Cursor = Cursors.WaitCursor;

                if (grd_ppra.CurrentRow == null)
                    throw new Exception (Msg02);
                
                PpraFacade ppraFacade = PpraFacade.getInstance();
                ppra = ppraFacade.findPpraById((Int64)grd_ppra.CurrentRow.Cells[0].Value);

                
                // somente será possível alterar um estudo em contrucao.
                
                if (ppra.Situacao != ApplicationConstants.CONSTRUCAO)
                    throw new PpraException(PpraException.msg1);

                frm_ppraAlterar formPpraAlterar = new frm_ppraAlterar(ppra, this);
                formPpraAlterar.ShowDialog();    
               
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }

        }

        private void bt_imprimir_Click(object sender, EventArgs e)
        {
            

            try
            {
                if (grd_ppra.CurrentRow == null)
                    throw new Exception (Msg02);

                PpraFacade ppraFacade = PpraFacade.getInstance();
                RelatorioFacade relatorioFacade = RelatorioFacade.getInstance();
                    
                this.Cursor = Cursors.WaitCursor;

                Estudo estudo = ppraFacade.findPpraById((Int64)grd_ppra.CurrentRow.Cells[0].Value);

                ppraFacade.montaQuadroVersao((Int64)grd_ppra.CurrentRow.Cells[0].Value);
                ppraFacade.geraBaseFuncaoEpi((Int64)grd_ppra.CurrentRow.Cells[0].Value);

                String nomeRelatorio = relatorioFacade.findNomeRelatorioById((Int64?)estudo.IdRelatorio);

                var process = new Process();

                
                process.StartInfo.FileName = "Relatorio\\jre7\\bin\\java";
                process.StartInfo.Arguments = " -jar Relatorio\\MiddlewareJasper.jar " + "Relatorio\\Report\\" + nomeRelatorio + " ID_PPRA:" + (Int64)grd_ppra.CurrentRow.Cells[0].Value + ":INTEGER";
                process.StartInfo.UseShellExecute = false;
                process.StartInfo.RedirectStandardError = true;
                process.StartInfo.RedirectStandardInput = true;
                process.StartInfo.RedirectStandardOutput = true;
                process.StartInfo.CreateNoWindow = true;
                process.Start();
                process.StandardOutput.ReadToEnd();
                process.WaitForExit();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }

        }

        private void btn_revisao_Click(object sender, EventArgs e)
        {
            PpraFacade ppraFacade = PpraFacade.getInstance();

            try
            {
                if (grd_ppra.CurrentRow != null)
                {
                    this.Cursor = Cursors.WaitCursor;

                    Int32 cellValue = grd_ppra.CurrentRow.Index;

                    Int64 id = (Int64)grd_ppra.Rows[cellValue].Cells[0].Value;

                    if (!ppraFacade.verificaPodeReplicarByRevisao(id))
                    {
                        throw new PpraException(PpraException.msg3);
                    }

                    if (!ppraFacade.verificaPodeReplicarByPpra(id))
                    {
                        throw new PpraException(PpraException.msg2);
                    }

                    // iniciando tela de comentario para a revisão

                    frm_ppraReplicarOld formPpraReplicar = new frm_ppraReplicarOld(this);
                    formPpraReplicar.ShowDialog();

                    ppraFacade.replicarPpra(id, comentario, false);

                    MessageBox.Show(Msg07, Msg05);
                    montaDataGrid();
                }
                else
                {
                    MessageBox.Show(Msg02);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }

        private void bt_excluir_Click(object sender, EventArgs e)
        {
            try
            {
                if (grd_ppra.CurrentRow == null)
                    throw new Exception (Msg02);
                
                PpraFacade ppraFacade = PpraFacade.getInstance();

                this.Cursor = Cursors.WaitCursor;
                
                if (MessageBox.Show(Msg03, Msg05, MessageBoxButtons.YesNo) == DialogResult.Yes)
                {
                
                    Int64 id = (Int64)grd_ppra.CurrentRow.Cells[0].Value;
                    String situacao = (string)grd_ppra.CurrentRow.Cells[5].Value;

                    
                    if (situacao == ApplicationConstants.FECHADO)
                        throw new PpraException(PpraException.msg4);
                        
                    else
                        {
                            ppraFacade.excluiPpra(id);
                            MessageBox.Show(Msg06, Msg05);
                            montaDataGrid();
                        }
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }

        private void btn_finalizar_Click(object sender, EventArgs e)
        {
            try
            {
                if (grd_ppra.CurrentRow == null)
                    throw new Exception (Msg02);
                
                PpraFacade ppraFacade = PpraFacade.getInstance();
                
                this.Cursor = Cursors.WaitCursor;
                 
                if (MessageBox.Show(Msg08, Msg04, MessageBoxButtons.YesNo) == DialogResult.Yes)
                {

                    ppra = ppraFacade.findPpraById((Int64)grd_ppra.CurrentRow.Cells[0].Value);

                    
                    if (ppra.Situacao != ApplicationConstants.ATIVO)
                            throw new PpraException(PpraException.msg5);
                    
                    else
                    {

                        frm_ProdutoTipoEstudoOld formProdutoTipoEstudo = new frm_ProdutoTipoEstudoOld(ApplicationConstants.PPRA);
                        formProdutoTipoEstudo.ShowDialog();

                        Produto produto = formProdutoTipoEstudo.getProduto();

                        if (produto != null)
                        {
                                ppraFacade.mudaEstadoPpra(ppra, ApplicationConstants.FECHADO, produto);
                                MessageBox.Show(Msg09, Msg05);
                                montaDataGrid();
                            
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);

            }
            finally
            {
                this.Cursor = Cursors.Default;
            }

        }

        private void btn_enviarAnalise_Click(object sender, EventArgs e)
        {
            try
            {

                if (grd_ppra.CurrentRow == null)
                    throw new Exception(Msg02);

                this.Cursor = Cursors.WaitCursor;

                PpraFacade ppraFacade = PpraFacade.getInstance();

                ppra = ppraFacade.findPpraById((Int64)grd_ppra.CurrentRow.Cells[0].Value);

                if (ppra.Situacao.Equals(ApplicationConstants.ATIVO))
                {
                    if (MessageBox.Show(Msg11, Msg04, MessageBoxButtons.YesNo) == DialogResult.Yes)
                    {
                        ppra = ppraFacade.findPpraById((Int64)ppra.Id);
                        ppraFacade.mudaEstadoPpra(ppra, ApplicationConstants.CONSTRUCAO, null);
                        MessageBox.Show(Msg13, Msg05);
                        montaDataGrid();

                    }

                }
                else if (ppra.Situacao.Equals(ApplicationConstants.CONSTRUCAO))
                {


                    if (MessageBox.Show(Msg10, Msg04, MessageBoxButtons.YesNo) == DialogResult.Yes)
                    {

                        frm_PpraElaborador formPpraElaborador = new frm_PpraElaborador();
                        formPpraElaborador.ShowDialog();

                        if (formPpraElaborador.getElaborador() != null)
                        {
                            engenheiro = formPpraElaborador.getElaborador();
                            ppra.Engenheiro = engenheiro;
                            ppraFacade.mudaEstadoPpra(ppra, ApplicationConstants.ATIVO, null);
                            MessageBox.Show(Msg12, Msg05);
                            montaDataGrid();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }

        private void grd_ppra_RowPrePaint(object sender, DataGridViewRowPrePaintEventArgs e)
        {
            DataGridView dvg = sender as DataGridView;
            String valor = dvg.Rows[e.RowIndex].Cells[5].Value.ToString();

            if (String.Equals(valor, ApplicationConstants.FECHADO))
            {
                dvg.Rows[e.RowIndex].DefaultCellStyle.ForeColor = Color.Green;
            }
            if (String.Equals(valor, ApplicationConstants.ATIVO))
            {
                dvg.Rows[e.RowIndex].DefaultCellStyle.ForeColor = Color.Blue;
            }


        }

        private void btn_corrigir_Click(object sender, EventArgs e)
        {
            PpraFacade ppraFacade = PpraFacade.getInstance();

            try
            {
                if (grd_ppra.CurrentRow == null)
                    throw new Exception (Msg02);
                
                 
                this.Cursor = Cursors.WaitCursor;

                ppra = ppraFacade.findPpraById((Int64)grd_ppra.CurrentRow.Cells[0].Value);

                if (!String.Equals(ppra.Situacao, ApplicationConstants.FECHADO))
                    throw new Exception(msg15);

                if (MessageBox.Show(msg14, Msg04, MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                {
                    frm_CorrecaoEstudo correcao = new frm_CorrecaoEstudo(ppra);
                    correcao.ShowDialog();

                    if (correcao.getCorrecaoEstudo() != null)
                    {
                        frm_ppraAlterar altera = new frm_ppraAlterar(ppra, this);
                        altera.ShowDialog();
                        montaDataGrid();
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, Msg04, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }

        private void grd_ppra_CellContentDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                this.Cursor = Cursors.WaitCursor;

                if (grd_ppra.CurrentRow == null)
                    throw new Exception(Msg02);

                Int64 id = (Int64)grd_ppra.CurrentRow.Cells[0].Value;

                PpraFacade ppraFacade = PpraFacade.getInstance();

                ppra = ppraFacade.findPpraById(id);

                /* verificando se o estudo pode ter sido corrigido */
                if (!String.Equals(ApplicationConstants.FECHADO, ppra.Situacao))
                    throw new Exception(msg16);

                frm_CorrecaoEstudoConsultaOld Consulta = new frm_CorrecaoEstudoConsultaOld(ppra);
                Consulta.ShowDialog();

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, Msg04, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }

    }
}