﻿using System;
using System.Windows.Forms;
using SWS.ViewHelper;
using SWS.Facade;
using SWS.Entidade;
using SWS.View.ViewHelper;
using SWS.View.Resources;
using System.Drawing;
using System.Collections.Generic;
using SWS.View;


namespace SWS.View
{
    public partial class frm_usuarioSenhaAlterar : BaseFormConsulta
    {
        String login;
        Usuario usuario = null;

        public static String Msg01 = " As senhas digitadas não são iguais ";
        public static String Msg02 = " Atenção ";
        public static String Msg03 = " A senha atual digitada está inválida ";
        public static String Msg04 = " Senha alterada com sucesso";
        
        public frm_usuarioSenhaAlterar(String login)
        {
            InitializeComponent();
            this.login = login;
            
        }

        private void btn_salvar_Click(object sender, EventArgs e)
        {
            this.UsuarioSenhaAlterarErrorProvider.Clear();
            
            try
            {
                UsuarioFacade usuarioFacade = UsuarioFacade.getInstance();

                usuario = usuarioFacade.findUsuarioByLogin(login);
                
                if (isNull())
                {

                    if (validaSenha())
                    {
                        usuario.Senha = Util.getMD5Hash(text_confirmacao.Text);
                        usuarioFacade.alterarSenhaUsuario(usuario);
                        MessageBox.Show(Msg04);
                        this.Close();
                    }

                }
            }
            catch (Exception Ex)
            {
                MessageBox.Show(Ex.Message);
            }

        }

        private void btn_cancelar_Click(object sender, EventArgs e)
        {
            this.Close();
        }


        private Boolean isNull()
        {
            
            Boolean retorno = Convert.ToBoolean(true);


            if (string.IsNullOrEmpty(text_senhaAtual.Text))
            {
                this.UsuarioSenhaAlterarErrorProvider.SetError(this.text_senhaAtual, "O campo não pode ser vazio!");
                retorno = Convert.ToBoolean(false);
            }

            if (string.IsNullOrEmpty(text_novaSenha.Text))
            {
                this.UsuarioSenhaAlterarErrorProvider.SetError(this.text_novaSenha, "O campo não pode ser vazio!");
                retorno = Convert.ToBoolean(false);
            }

            if (string.IsNullOrEmpty(text_confirmacao.Text))
            {
                this.UsuarioSenhaAlterarErrorProvider.SetError(this.text_confirmacao, "O campo não pode ser vazio!");
                retorno = Convert.ToBoolean(false);
            }

            return retorno;

        
        }

        private Boolean validaSenha()
        {
            Boolean retorno = Convert.ToBoolean(true);

            String usuarioSenhaDigitado = Util.getMD5Hash(text_senhaAtual.Text);
            String usuarioSenhaRecuperado = usuario.Senha;

            if (text_novaSenha.Text != text_confirmacao.Text )
            {
                retorno = Convert.ToBoolean(false);
                MessageBox.Show(Msg01, Msg02);
            }

            if (!String.Equals(usuarioSenhaDigitado, usuarioSenhaRecuperado))
            {
                retorno = Convert.ToBoolean(false);
                MessageBox.Show(Msg03, Msg02);
            }

            return retorno;
        }

        
    }
}
