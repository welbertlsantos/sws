﻿using SWS.Entidade;
using SWS.Facade;
using SWS.View.Resources;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace SWS.View
{
    public partial class frmPerfilAlterar : frmPerfilIncluir
    {
        public frmPerfilAlterar(Perfil perfil) :base()
        {
            InitializeComponent();
            this.Perfil = perfil;
            textPerfil.Text = Perfil.Descricao;
        }

        protected override void btnGravar_Click(object sender, EventArgs e)
        {
            try
            {
                if (ValidaCampos())
                {
                    UsuarioFacade usuarioFacade = UsuarioFacade.getInstance();
                    Perfil = usuarioFacade.updatePerfil(new Perfil(Perfil.Id, textPerfil.Text, ApplicationConstants.ATIVO));
                    MessageBox.Show("Perfil alterado com sucesso.", "Confirmação", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    this.Close();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

    }
}
