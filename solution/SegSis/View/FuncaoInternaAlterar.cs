﻿using SWS.Entidade;
using SWS.Facade;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace SWS.View
{
    public partial class frmFuncaoInternaAlterar : frmFuncaoInternaIncluir
    {
        public frmFuncaoInternaAlterar(FuncaoInterna funcaoInterna)
        {
            InitializeComponent();
            this.funcaoInterna = funcaoInterna;

            text_descricao.Text = funcaoInterna.Descricao;
        }

        protected override void btn_gravar_Click(object sender, EventArgs e)
        {
            try
            {
                if (String.IsNullOrEmpty(text_descricao.Text.Trim()))
                    throw new Exception("O campo nome é obrigatório.");
                
                UsuarioFacade UsuarioFacade = UsuarioFacade.getInstance();

                funcaoInterna = UsuarioFacade.alteraFuncaoInterna(new FuncaoInterna(funcaoInterna.Id, text_descricao.Text, funcaoInterna.Situacao, funcaoInterna.Privado));

                MessageBox.Show("Função interna alterada com sucesso.", "Confirmação", MessageBoxButtons.OK, MessageBoxIcon.Information);

                this.Close();
            }
            catch (Exception Ex)
            {
                MessageBox.Show(Ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
    }
}
