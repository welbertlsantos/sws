﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SWS.View
{
    public partial class frmContratoDuplicarData : frmTemplateConsulta
    {
        public frmContratoDuplicarData()
        {
            InitializeComponent();
        }

        DateTime dataInicioVigencia;

        public DateTime DataInicioVigencia
        {
            get { return dataInicioVigencia; }
            set { dataInicioVigencia = value; }
        }

        DateTime? dataFimVigencia;

        public DateTime? DataFimVigencia
        {
            get { return dataFimVigencia; }
            set { dataFimVigencia = value; }
        }

        private void btnConfirmar_Click(object sender, EventArgs e)
        {
            try
            {
                if (dataInicio.Checked == false)
                    throw new Exception("Você deve selecionar o período de início da vigência do contrato.");

                this.DataInicioVigencia = Convert.ToDateTime(dataInicio.Text);

                this.DataFimVigencia = dataTermino.Checked ? Convert.ToDateTime(dataTermino.Text) : (DateTime?)null;
                this.Close();

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btnFechar_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
