﻿namespace SWS.View
{
    partial class frmClienteCrendiadaSelecionar
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            this.flpAcao = new System.Windows.Forms.FlowLayoutPanel();
            this.btnConfirmar = new System.Windows.Forms.Button();
            this.bt_fechar = new System.Windows.Forms.Button();
            this.dgvCredenciadas = new System.Windows.Forms.DataGridView();
            ((System.ComponentModel.ISupportInitialize)(this.scForm)).BeginInit();
            this.scForm.Panel1.SuspendLayout();
            this.scForm.Panel2.SuspendLayout();
            this.scForm.SuspendLayout();
            this.pnlForm.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.banner)).BeginInit();
            this.flpAcao.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvCredenciadas)).BeginInit();
            this.SuspendLayout();
            // 
            // scForm
            // 
            // 
            // scForm.Panel1
            // 
            this.scForm.Panel1.Controls.Add(this.flpAcao);
            this.scForm.SplitterDistance = 36;
            // 
            // pnlForm
            // 
            this.pnlForm.Controls.Add(this.dgvCredenciadas);
            this.pnlForm.Size = new System.Drawing.Size(685, 350);
            // 
            // flpAcao
            // 
            this.flpAcao.Controls.Add(this.btnConfirmar);
            this.flpAcao.Controls.Add(this.bt_fechar);
            this.flpAcao.Dock = System.Windows.Forms.DockStyle.Fill;
            this.flpAcao.Location = new System.Drawing.Point(0, 0);
            this.flpAcao.Name = "flpAcao";
            this.flpAcao.Size = new System.Drawing.Size(712, 36);
            this.flpAcao.TabIndex = 0;
            // 
            // btnConfirmar
            // 
            this.btnConfirmar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnConfirmar.Image = global::SWS.Properties.Resources.fechar_ico;
            this.btnConfirmar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnConfirmar.Location = new System.Drawing.Point(4, 4);
            this.btnConfirmar.Margin = new System.Windows.Forms.Padding(4);
            this.btnConfirmar.Name = "btnConfirmar";
            this.btnConfirmar.Size = new System.Drawing.Size(95, 27);
            this.btnConfirmar.TabIndex = 22;
            this.btnConfirmar.Text = "&Confirmar";
            this.btnConfirmar.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnConfirmar.UseVisualStyleBackColor = true;
            this.btnConfirmar.Click += new System.EventHandler(this.btnConfirmar_Click);
            // 
            // bt_fechar
            // 
            this.bt_fechar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.bt_fechar.Image = global::SWS.Properties.Resources.close;
            this.bt_fechar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.bt_fechar.Location = new System.Drawing.Point(107, 4);
            this.bt_fechar.Margin = new System.Windows.Forms.Padding(4);
            this.bt_fechar.Name = "bt_fechar";
            this.bt_fechar.Size = new System.Drawing.Size(95, 27);
            this.bt_fechar.TabIndex = 23;
            this.bt_fechar.Text = "&Fechar";
            this.bt_fechar.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.bt_fechar.UseVisualStyleBackColor = true;
            this.bt_fechar.Click += new System.EventHandler(this.bt_fechar_Click);
            // 
            // dgvCredenciadas
            // 
            this.dgvCredenciadas.AllowUserToAddRows = false;
            this.dgvCredenciadas.AllowUserToDeleteRows = false;
            this.dgvCredenciadas.AllowUserToOrderColumns = true;
            this.dgvCredenciadas.AllowUserToResizeRows = false;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.LightGray;
            this.dgvCredenciadas.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle1;
            this.dgvCredenciadas.BackgroundColor = System.Drawing.Color.White;
            this.dgvCredenciadas.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvCredenciadas.DefaultCellStyle = dataGridViewCellStyle2;
            this.dgvCredenciadas.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvCredenciadas.Location = new System.Drawing.Point(0, 0);
            this.dgvCredenciadas.MultiSelect = false;
            this.dgvCredenciadas.Name = "dgvCredenciadas";
            this.dgvCredenciadas.ReadOnly = true;
            this.dgvCredenciadas.RowHeadersVisible = false;
            this.dgvCredenciadas.RowTemplate.Height = 24;
            this.dgvCredenciadas.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvCredenciadas.Size = new System.Drawing.Size(685, 350);
            this.dgvCredenciadas.TabIndex = 0;
            this.dgvCredenciadas.DoubleClick += new System.EventHandler(this.dgvCredenciadas_DoubleClick);
            // 
            // frmClienteCrendiadaSelecionar
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(712, 444);
            this.Name = "frmClienteCrendiadaSelecionar";
            this.Text = "SELECIONAR A CREDENCIADA";
            this.scForm.Panel1.ResumeLayout(false);
            this.scForm.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.scForm)).EndInit();
            this.scForm.ResumeLayout(false);
            this.pnlForm.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.banner)).EndInit();
            this.flpAcao.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvCredenciadas)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.FlowLayoutPanel flpAcao;
        private System.Windows.Forms.Button btnConfirmar;
        private System.Windows.Forms.Button bt_fechar;
        private System.Windows.Forms.DataGridView dgvCredenciadas;
    }
}