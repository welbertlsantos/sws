﻿namespace SWS.View
{
    partial class frmCobrancaBaixar
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            this.flpAcao = new System.Windows.Forms.FlowLayoutPanel();
            this.btnConfirmar = new System.Windows.Forms.Button();
            this.btnCancelar = new System.Windows.Forms.Button();
            this.textNumeroNota = new System.Windows.Forms.TextBox();
            this.textCliente = new System.Windows.Forms.TextBox();
            this.textFormaPagamento = new System.Windows.Forms.TextBox();
            this.textParcela = new System.Windows.Forms.TextBox();
            this.textDocumento = new System.Windows.Forms.TextBox();
            this.lblCliente = new System.Windows.Forms.TextBox();
            this.lblFormaPgto = new System.Windows.Forms.TextBox();
            this.lblDocumento = new System.Windows.Forms.TextBox();
            this.lblParcela = new System.Windows.Forms.TextBox();
            this.lblNumeroNota = new System.Windows.Forms.TextBox();
            this.textDataEmissao = new System.Windows.Forms.TextBox();
            this.lblDataEmissao = new System.Windows.Forms.TextBox();
            this.textDataVencimento = new System.Windows.Forms.TextBox();
            this.lblDataVencimento = new System.Windows.Forms.TextBox();
            this.textValorCobranca = new System.Windows.Forms.TextBox();
            this.lblValorCobranca = new System.Windows.Forms.TextBox();
            this.lblDataPagamento = new System.Windows.Forms.TextBox();
            this.dataPagamento = new System.Windows.Forms.DateTimePicker();
            this.lblValorPagamento = new System.Windows.Forms.TextBox();
            this.textValorPago = new System.Windows.Forms.TextBox();
            this.lblBanco = new System.Windows.Forms.TextBox();
            this.textBanco = new System.Windows.Forms.TextBox();
            this.btnBanco = new System.Windows.Forms.Button();
            this.grb_conta = new System.Windows.Forms.GroupBox();
            this.dgvConta = new System.Windows.Forms.DataGridView();
            ((System.ComponentModel.ISupportInitialize)(this.scForm)).BeginInit();
            this.scForm.Panel1.SuspendLayout();
            this.scForm.Panel2.SuspendLayout();
            this.scForm.SuspendLayout();
            this.pnlForm.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.banner)).BeginInit();
            this.flpAcao.SuspendLayout();
            this.grb_conta.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvConta)).BeginInit();
            this.SuspendLayout();
            // 
            // scForm
            // 
            // 
            // scForm.Panel1
            // 
            this.scForm.Panel1.Controls.Add(this.flpAcao);
            // 
            // pnlForm
            // 
            this.pnlForm.Controls.Add(this.grb_conta);
            this.pnlForm.Controls.Add(this.btnBanco);
            this.pnlForm.Controls.Add(this.textBanco);
            this.pnlForm.Controls.Add(this.lblBanco);
            this.pnlForm.Controls.Add(this.textValorPago);
            this.pnlForm.Controls.Add(this.lblValorPagamento);
            this.pnlForm.Controls.Add(this.lblDataPagamento);
            this.pnlForm.Controls.Add(this.dataPagamento);
            this.pnlForm.Controls.Add(this.textValorCobranca);
            this.pnlForm.Controls.Add(this.lblValorCobranca);
            this.pnlForm.Controls.Add(this.textDataVencimento);
            this.pnlForm.Controls.Add(this.lblDataVencimento);
            this.pnlForm.Controls.Add(this.textDataEmissao);
            this.pnlForm.Controls.Add(this.lblDataEmissao);
            this.pnlForm.Controls.Add(this.textNumeroNota);
            this.pnlForm.Controls.Add(this.textCliente);
            this.pnlForm.Controls.Add(this.textFormaPagamento);
            this.pnlForm.Controls.Add(this.textParcela);
            this.pnlForm.Controls.Add(this.textDocumento);
            this.pnlForm.Controls.Add(this.lblCliente);
            this.pnlForm.Controls.Add(this.lblFormaPgto);
            this.pnlForm.Controls.Add(this.lblDocumento);
            this.pnlForm.Controls.Add(this.lblParcela);
            this.pnlForm.Controls.Add(this.lblNumeroNota);
            this.pnlForm.Size = new System.Drawing.Size(514, 418);
            // 
            // flpAcao
            // 
            this.flpAcao.Controls.Add(this.btnConfirmar);
            this.flpAcao.Controls.Add(this.btnCancelar);
            this.flpAcao.Dock = System.Windows.Forms.DockStyle.Fill;
            this.flpAcao.Location = new System.Drawing.Point(0, 0);
            this.flpAcao.Name = "flpAcao";
            this.flpAcao.Size = new System.Drawing.Size(534, 30);
            this.flpAcao.TabIndex = 0;
            // 
            // btnConfirmar
            // 
            this.btnConfirmar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnConfirmar.Image = global::SWS.Properties.Resources.fechar_ico;
            this.btnConfirmar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnConfirmar.Location = new System.Drawing.Point(3, 3);
            this.btnConfirmar.Name = "btnConfirmar";
            this.btnConfirmar.Size = new System.Drawing.Size(75, 23);
            this.btnConfirmar.TabIndex = 5;
            this.btnConfirmar.Text = "&Confirmar";
            this.btnConfirmar.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnConfirmar.UseVisualStyleBackColor = true;
            this.btnConfirmar.Click += new System.EventHandler(this.btnConfirmar_Click);
            // 
            // btnCancelar
            // 
            this.btnCancelar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnCancelar.Image = global::SWS.Properties.Resources.close;
            this.btnCancelar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnCancelar.Location = new System.Drawing.Point(84, 3);
            this.btnCancelar.Name = "btnCancelar";
            this.btnCancelar.Size = new System.Drawing.Size(75, 23);
            this.btnCancelar.TabIndex = 6;
            this.btnCancelar.Text = "C&ancelar";
            this.btnCancelar.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnCancelar.UseVisualStyleBackColor = true;
            this.btnCancelar.Click += new System.EventHandler(this.btnCancelar_Click);
            // 
            // textNumeroNota
            // 
            this.textNumeroNota.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.textNumeroNota.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textNumeroNota.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textNumeroNota.Location = new System.Drawing.Point(143, 8);
            this.textNumeroNota.Name = "textNumeroNota";
            this.textNumeroNota.ReadOnly = true;
            this.textNumeroNota.Size = new System.Drawing.Size(364, 21);
            this.textNumeroNota.TabIndex = 41;
            this.textNumeroNota.TabStop = false;
            // 
            // textCliente
            // 
            this.textCliente.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.textCliente.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textCliente.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textCliente.Location = new System.Drawing.Point(143, 88);
            this.textCliente.Name = "textCliente";
            this.textCliente.ReadOnly = true;
            this.textCliente.Size = new System.Drawing.Size(364, 21);
            this.textCliente.TabIndex = 40;
            this.textCliente.TabStop = false;
            // 
            // textFormaPagamento
            // 
            this.textFormaPagamento.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.textFormaPagamento.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textFormaPagamento.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textFormaPagamento.Location = new System.Drawing.Point(143, 68);
            this.textFormaPagamento.Name = "textFormaPagamento";
            this.textFormaPagamento.ReadOnly = true;
            this.textFormaPagamento.Size = new System.Drawing.Size(364, 21);
            this.textFormaPagamento.TabIndex = 39;
            this.textFormaPagamento.TabStop = false;
            // 
            // textParcela
            // 
            this.textParcela.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.textParcela.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textParcela.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textParcela.Location = new System.Drawing.Point(143, 48);
            this.textParcela.Name = "textParcela";
            this.textParcela.ReadOnly = true;
            this.textParcela.Size = new System.Drawing.Size(364, 21);
            this.textParcela.TabIndex = 38;
            this.textParcela.TabStop = false;
            // 
            // textDocumento
            // 
            this.textDocumento.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.textDocumento.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textDocumento.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textDocumento.Location = new System.Drawing.Point(143, 28);
            this.textDocumento.Name = "textDocumento";
            this.textDocumento.ReadOnly = true;
            this.textDocumento.Size = new System.Drawing.Size(364, 21);
            this.textDocumento.TabIndex = 37;
            this.textDocumento.TabStop = false;
            // 
            // lblCliente
            // 
            this.lblCliente.BackColor = System.Drawing.Color.LightGray;
            this.lblCliente.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblCliente.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCliente.Location = new System.Drawing.Point(8, 88);
            this.lblCliente.Name = "lblCliente";
            this.lblCliente.ReadOnly = true;
            this.lblCliente.Size = new System.Drawing.Size(136, 21);
            this.lblCliente.TabIndex = 36;
            this.lblCliente.TabStop = false;
            this.lblCliente.Text = "Cliente";
            // 
            // lblFormaPgto
            // 
            this.lblFormaPgto.BackColor = System.Drawing.Color.LightGray;
            this.lblFormaPgto.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblFormaPgto.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblFormaPgto.Location = new System.Drawing.Point(8, 68);
            this.lblFormaPgto.Name = "lblFormaPgto";
            this.lblFormaPgto.ReadOnly = true;
            this.lblFormaPgto.Size = new System.Drawing.Size(136, 21);
            this.lblFormaPgto.TabIndex = 35;
            this.lblFormaPgto.TabStop = false;
            this.lblFormaPgto.Text = "Forma Pagamento";
            // 
            // lblDocumento
            // 
            this.lblDocumento.BackColor = System.Drawing.Color.LightGray;
            this.lblDocumento.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblDocumento.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDocumento.Location = new System.Drawing.Point(8, 28);
            this.lblDocumento.Name = "lblDocumento";
            this.lblDocumento.ReadOnly = true;
            this.lblDocumento.Size = new System.Drawing.Size(136, 21);
            this.lblDocumento.TabIndex = 34;
            this.lblDocumento.TabStop = false;
            this.lblDocumento.Text = "Documento";
            // 
            // lblParcela
            // 
            this.lblParcela.BackColor = System.Drawing.Color.LightGray;
            this.lblParcela.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblParcela.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblParcela.Location = new System.Drawing.Point(8, 48);
            this.lblParcela.Name = "lblParcela";
            this.lblParcela.ReadOnly = true;
            this.lblParcela.Size = new System.Drawing.Size(136, 21);
            this.lblParcela.TabIndex = 33;
            this.lblParcela.TabStop = false;
            this.lblParcela.Text = "Parcela";
            // 
            // lblNumeroNota
            // 
            this.lblNumeroNota.BackColor = System.Drawing.Color.LightGray;
            this.lblNumeroNota.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblNumeroNota.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNumeroNota.Location = new System.Drawing.Point(8, 8);
            this.lblNumeroNota.Name = "lblNumeroNota";
            this.lblNumeroNota.ReadOnly = true;
            this.lblNumeroNota.Size = new System.Drawing.Size(136, 21);
            this.lblNumeroNota.TabIndex = 32;
            this.lblNumeroNota.TabStop = false;
            this.lblNumeroNota.Text = "Número da NF";
            // 
            // textDataEmissao
            // 
            this.textDataEmissao.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.textDataEmissao.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textDataEmissao.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textDataEmissao.Location = new System.Drawing.Point(143, 108);
            this.textDataEmissao.Name = "textDataEmissao";
            this.textDataEmissao.ReadOnly = true;
            this.textDataEmissao.Size = new System.Drawing.Size(364, 21);
            this.textDataEmissao.TabIndex = 43;
            this.textDataEmissao.TabStop = false;
            // 
            // lblDataEmissao
            // 
            this.lblDataEmissao.BackColor = System.Drawing.Color.LightGray;
            this.lblDataEmissao.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblDataEmissao.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDataEmissao.Location = new System.Drawing.Point(8, 108);
            this.lblDataEmissao.Name = "lblDataEmissao";
            this.lblDataEmissao.ReadOnly = true;
            this.lblDataEmissao.Size = new System.Drawing.Size(136, 21);
            this.lblDataEmissao.TabIndex = 42;
            this.lblDataEmissao.TabStop = false;
            this.lblDataEmissao.Text = "Data de Emissão";
            // 
            // textDataVencimento
            // 
            this.textDataVencimento.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.textDataVencimento.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textDataVencimento.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textDataVencimento.Location = new System.Drawing.Point(143, 128);
            this.textDataVencimento.Name = "textDataVencimento";
            this.textDataVencimento.ReadOnly = true;
            this.textDataVencimento.Size = new System.Drawing.Size(364, 21);
            this.textDataVencimento.TabIndex = 45;
            this.textDataVencimento.TabStop = false;
            // 
            // lblDataVencimento
            // 
            this.lblDataVencimento.BackColor = System.Drawing.Color.LightGray;
            this.lblDataVencimento.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblDataVencimento.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDataVencimento.Location = new System.Drawing.Point(8, 128);
            this.lblDataVencimento.Name = "lblDataVencimento";
            this.lblDataVencimento.ReadOnly = true;
            this.lblDataVencimento.Size = new System.Drawing.Size(136, 21);
            this.lblDataVencimento.TabIndex = 44;
            this.lblDataVencimento.TabStop = false;
            this.lblDataVencimento.Text = "Data de Vencimento";
            // 
            // textValorCobranca
            // 
            this.textValorCobranca.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.textValorCobranca.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textValorCobranca.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textValorCobranca.Location = new System.Drawing.Point(143, 148);
            this.textValorCobranca.Name = "textValorCobranca";
            this.textValorCobranca.ReadOnly = true;
            this.textValorCobranca.Size = new System.Drawing.Size(364, 21);
            this.textValorCobranca.TabIndex = 47;
            this.textValorCobranca.TabStop = false;
            // 
            // lblValorCobranca
            // 
            this.lblValorCobranca.BackColor = System.Drawing.Color.LightGray;
            this.lblValorCobranca.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblValorCobranca.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblValorCobranca.Location = new System.Drawing.Point(8, 148);
            this.lblValorCobranca.Name = "lblValorCobranca";
            this.lblValorCobranca.ReadOnly = true;
            this.lblValorCobranca.Size = new System.Drawing.Size(136, 21);
            this.lblValorCobranca.TabIndex = 46;
            this.lblValorCobranca.TabStop = false;
            this.lblValorCobranca.Text = "Valor R$";
            // 
            // lblDataPagamento
            // 
            this.lblDataPagamento.BackColor = System.Drawing.Color.LightGray;
            this.lblDataPagamento.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblDataPagamento.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDataPagamento.Location = new System.Drawing.Point(8, 175);
            this.lblDataPagamento.Name = "lblDataPagamento";
            this.lblDataPagamento.ReadOnly = true;
            this.lblDataPagamento.Size = new System.Drawing.Size(136, 21);
            this.lblDataPagamento.TabIndex = 48;
            this.lblDataPagamento.TabStop = false;
            this.lblDataPagamento.Text = "Data de Pagamento";
            // 
            // dataPagamento
            // 
            this.dataPagamento.CustomFormat = "";
            this.dataPagamento.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dataPagamento.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dataPagamento.Location = new System.Drawing.Point(143, 175);
            this.dataPagamento.Name = "dataPagamento";
            this.dataPagamento.ShowCheckBox = true;
            this.dataPagamento.Size = new System.Drawing.Size(364, 21);
            this.dataPagamento.TabIndex = 2;
            // 
            // lblValorPagamento
            // 
            this.lblValorPagamento.BackColor = System.Drawing.Color.LightGray;
            this.lblValorPagamento.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblValorPagamento.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblValorPagamento.Location = new System.Drawing.Point(8, 195);
            this.lblValorPagamento.Name = "lblValorPagamento";
            this.lblValorPagamento.ReadOnly = true;
            this.lblValorPagamento.Size = new System.Drawing.Size(136, 21);
            this.lblValorPagamento.TabIndex = 50;
            this.lblValorPagamento.TabStop = false;
            this.lblValorPagamento.Text = "Valor Pago R$";
            // 
            // textValorPago
            // 
            this.textValorPago.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textValorPago.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textValorPago.Location = new System.Drawing.Point(143, 195);
            this.textValorPago.Name = "textValorPago";
            this.textValorPago.Size = new System.Drawing.Size(364, 21);
            this.textValorPago.TabIndex = 1;
            this.textValorPago.Enter += new System.EventHandler(this.textValorPago_Enter);
            this.textValorPago.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.textValorPago_KeyPress);
            this.textValorPago.Leave += new System.EventHandler(this.textValorPago_Leave);
            // 
            // lblBanco
            // 
            this.lblBanco.BackColor = System.Drawing.Color.LightGray;
            this.lblBanco.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblBanco.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblBanco.Location = new System.Drawing.Point(8, 215);
            this.lblBanco.Name = "lblBanco";
            this.lblBanco.ReadOnly = true;
            this.lblBanco.Size = new System.Drawing.Size(136, 21);
            this.lblBanco.TabIndex = 52;
            this.lblBanco.TabStop = false;
            this.lblBanco.Text = "Banco";
            // 
            // textBanco
            // 
            this.textBanco.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.textBanco.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBanco.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBanco.Location = new System.Drawing.Point(143, 215);
            this.textBanco.Name = "textBanco";
            this.textBanco.ReadOnly = true;
            this.textBanco.Size = new System.Drawing.Size(331, 21);
            this.textBanco.TabIndex = 53;
            this.textBanco.TabStop = false;
            // 
            // btnBanco
            // 
            this.btnBanco.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnBanco.Image = global::SWS.Properties.Resources.busca;
            this.btnBanco.Location = new System.Drawing.Point(473, 215);
            this.btnBanco.Name = "btnBanco";
            this.btnBanco.Size = new System.Drawing.Size(34, 21);
            this.btnBanco.TabIndex = 3;
            this.btnBanco.UseVisualStyleBackColor = true;
            this.btnBanco.Click += new System.EventHandler(this.btnBanco_Click);
            // 
            // grb_conta
            // 
            this.grb_conta.Controls.Add(this.dgvConta);
            this.grb_conta.Location = new System.Drawing.Point(8, 244);
            this.grb_conta.Name = "grb_conta";
            this.grb_conta.Size = new System.Drawing.Size(499, 129);
            this.grb_conta.TabIndex = 55;
            this.grb_conta.TabStop = false;
            this.grb_conta.Text = "Conta";
            // 
            // dgvConta
            // 
            this.dgvConta.AllowUserToAddRows = false;
            this.dgvConta.AllowUserToDeleteRows = false;
            this.dgvConta.AllowUserToOrderColumns = true;
            this.dgvConta.AllowUserToResizeRows = false;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.Gainsboro;
            this.dgvConta.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle1;
            this.dgvConta.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.DisplayedCells;
            this.dgvConta.BackgroundColor = System.Drawing.Color.White;
            this.dgvConta.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.Color.LightGray;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvConta.DefaultCellStyle = dataGridViewCellStyle2;
            this.dgvConta.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvConta.Location = new System.Drawing.Point(3, 16);
            this.dgvConta.MultiSelect = false;
            this.dgvConta.Name = "dgvConta";
            this.dgvConta.ReadOnly = true;
            this.dgvConta.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvConta.Size = new System.Drawing.Size(493, 110);
            this.dgvConta.TabIndex = 4;
            // 
            // frmCobrancaBaixar
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(534, 361);
            this.Name = "frmCobrancaBaixar";
            this.Text = "BAIXAR COBRANÇA";
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.frmCobrancaBaixar_KeyDown);
            this.scForm.Panel1.ResumeLayout(false);
            this.scForm.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.scForm)).EndInit();
            this.scForm.ResumeLayout(false);
            this.pnlForm.ResumeLayout(false);
            this.pnlForm.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.banner)).EndInit();
            this.flpAcao.ResumeLayout(false);
            this.grb_conta.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvConta)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.FlowLayoutPanel flpAcao;
        private System.Windows.Forms.Button btnConfirmar;
        private System.Windows.Forms.Button btnCancelar;
        private System.Windows.Forms.TextBox textValorPago;
        private System.Windows.Forms.TextBox lblValorPagamento;
        private System.Windows.Forms.TextBox lblDataPagamento;
        private System.Windows.Forms.DateTimePicker dataPagamento;
        private System.Windows.Forms.TextBox textValorCobranca;
        private System.Windows.Forms.TextBox lblValorCobranca;
        private System.Windows.Forms.TextBox textDataVencimento;
        private System.Windows.Forms.TextBox lblDataVencimento;
        private System.Windows.Forms.TextBox textDataEmissao;
        private System.Windows.Forms.TextBox lblDataEmissao;
        private System.Windows.Forms.TextBox textNumeroNota;
        private System.Windows.Forms.TextBox textCliente;
        private System.Windows.Forms.TextBox textFormaPagamento;
        private System.Windows.Forms.TextBox textParcela;
        private System.Windows.Forms.TextBox textDocumento;
        private System.Windows.Forms.TextBox lblCliente;
        private System.Windows.Forms.TextBox lblFormaPgto;
        private System.Windows.Forms.TextBox lblDocumento;
        private System.Windows.Forms.TextBox lblParcela;
        private System.Windows.Forms.TextBox lblNumeroNota;
        private System.Windows.Forms.Button btnBanco;
        private System.Windows.Forms.TextBox textBanco;
        private System.Windows.Forms.TextBox lblBanco;
        private System.Windows.Forms.GroupBox grb_conta;
        private System.Windows.Forms.DataGridView dgvConta;
    }
}