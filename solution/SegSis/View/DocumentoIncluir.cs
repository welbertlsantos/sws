﻿using SWS.Entidade;
using SWS.Facade;
using SWS.View.Resources;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace SWS.View
{
    public partial class frmDocumentoIncluir : frmTemplate
    {
        private Documento documento;

        public Documento Documento
        {
            get { return documento; }
            set { documento = value; }
        }
        
        public frmDocumentoIncluir()
        {
            InitializeComponent();
            ActiveControl = textDescricao;
        }

        protected virtual void btnGravar_Click(object sender, EventArgs e)
        {
            try
            {
                if (ValidaCamposTela())
                {
                    ProtocoloFacade protocoloFacade = ProtocoloFacade.getInstance();

                    documento = protocoloFacade.InsertDocumento(new Documento(null, textDescricao.Text, ApplicationConstants.ATIVO));
                    MessageBox.Show("Protocolo incluído com suceso.", "Confirmação", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    this.Close();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        protected void btnSair_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        protected bool ValidaCamposTela()
        {
            bool result = true;
            try
            {
                if (string.IsNullOrEmpty(textDescricao.Text.Trim()))
                    throw new Exception("Campo descrição é obrigatório.");
            }
            catch (Exception ex)
            {
                result = false;
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            return result;
        }

        protected void frmDocumentoIncluir_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter || e.KeyCode == Keys.Return)
                SendKeys.Send("{TAB}");
        }
    }
}
