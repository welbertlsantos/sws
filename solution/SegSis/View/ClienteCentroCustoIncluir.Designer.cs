﻿namespace SWS.View
{
    partial class frmClienteCentroCustoIncluir
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.flpAcao = new System.Windows.Forms.FlowLayoutPanel();
            this.btnGravar = new System.Windows.Forms.Button();
            this.btnFechar = new System.Windows.Forms.Button();
            this.lblCentroCusto = new System.Windows.Forms.TextBox();
            this.textCentroCusto = new System.Windows.Forms.TextBox();
            ((System.ComponentModel.ISupportInitialize)(this.scForm)).BeginInit();
            this.scForm.Panel1.SuspendLayout();
            this.scForm.Panel2.SuspendLayout();
            this.scForm.SuspendLayout();
            this.pnlForm.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.banner)).BeginInit();
            this.flpAcao.SuspendLayout();
            this.SuspendLayout();
            // 
            // scForm
            // 
            // 
            // scForm.Panel1
            // 
            this.scForm.Panel1.Controls.Add(this.flpAcao);
            this.scForm.TabIndex = 51;
            // 
            // pnlForm
            // 
            this.pnlForm.Controls.Add(this.textCentroCusto);
            this.pnlForm.Controls.Add(this.lblCentroCusto);
            this.pnlForm.TabIndex = 30;
            // 
            // flpAcao
            // 
            this.flpAcao.Controls.Add(this.btnGravar);
            this.flpAcao.Controls.Add(this.btnFechar);
            this.flpAcao.Dock = System.Windows.Forms.DockStyle.Fill;
            this.flpAcao.Location = new System.Drawing.Point(0, 0);
            this.flpAcao.Name = "flpAcao";
            this.flpAcao.Size = new System.Drawing.Size(534, 30);
            this.flpAcao.TabIndex = 20;
            // 
            // btnGravar
            // 
            this.btnGravar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnGravar.Image = global::SWS.Properties.Resources.Gravar;
            this.btnGravar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnGravar.Location = new System.Drawing.Point(3, 3);
            this.btnGravar.Name = "btnGravar";
            this.btnGravar.Size = new System.Drawing.Size(75, 23);
            this.btnGravar.TabIndex = 5;
            this.btnGravar.Text = "&Gravar";
            this.btnGravar.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnGravar.UseVisualStyleBackColor = true;
            this.btnGravar.Click += new System.EventHandler(this.btnGravar_Click);
            // 
            // btnFechar
            // 
            this.btnFechar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnFechar.Image = global::SWS.Properties.Resources.close;
            this.btnFechar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnFechar.Location = new System.Drawing.Point(84, 3);
            this.btnFechar.Name = "btnFechar";
            this.btnFechar.Size = new System.Drawing.Size(75, 23);
            this.btnFechar.TabIndex = 6;
            this.btnFechar.Text = "&Fechar";
            this.btnFechar.UseVisualStyleBackColor = true;
            this.btnFechar.Click += new System.EventHandler(this.btnFechar_Click);
            // 
            // lblCentroCusto
            // 
            this.lblCentroCusto.BackColor = System.Drawing.Color.LightGray;
            this.lblCentroCusto.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblCentroCusto.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCentroCusto.Location = new System.Drawing.Point(12, 17);
            this.lblCentroCusto.Name = "lblCentroCusto";
            this.lblCentroCusto.ReadOnly = true;
            this.lblCentroCusto.Size = new System.Drawing.Size(133, 21);
            this.lblCentroCusto.TabIndex = 88;
            this.lblCentroCusto.TabStop = false;
            this.lblCentroCusto.Text = "Centro de Custo";
            // 
            // textCentroCusto
            // 
            this.textCentroCusto.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textCentroCusto.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.textCentroCusto.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textCentroCusto.Location = new System.Drawing.Point(144, 17);
            this.textCentroCusto.MaxLength = 50;
            this.textCentroCusto.Name = "textCentroCusto";
            this.textCentroCusto.Size = new System.Drawing.Size(360, 21);
            this.textCentroCusto.TabIndex = 1;
            // 
            // frmClienteCentroCustoIncluir
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(534, 361);
            this.Name = "frmClienteCentroCustoIncluir";
            this.Text = "INCLUIR CENTRO DE CUSTO";
            this.Load += new System.EventHandler(this.frmClienteCentroCustoIncluir_Load);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.frmClienteCentroCustoIncluir_KeyDown);
            this.scForm.Panel1.ResumeLayout(false);
            this.scForm.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.scForm)).EndInit();
            this.scForm.ResumeLayout(false);
            this.pnlForm.ResumeLayout(false);
            this.pnlForm.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.banner)).EndInit();
            this.flpAcao.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        protected System.Windows.Forms.FlowLayoutPanel flpAcao;
        protected System.Windows.Forms.Button btnGravar;
        protected System.Windows.Forms.Button btnFechar;
        protected System.Windows.Forms.TextBox lblCentroCusto;
        protected System.Windows.Forms.TextBox textCentroCusto;
    }
}