﻿namespace SegSis.View
{
    partial class frm_clienteCnaePrincipalAlterar
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frm_clienteCnaePrincipalAlterar));
            this.grb_cnae = new System.Windows.Forms.GroupBox();
            this.grd_cnae = new System.Windows.Forms.DataGridView();
            this.grb_paginacao = new System.Windows.Forms.GroupBox();
            this.btn_ok = new System.Windows.Forms.Button();
            this.panel_banner.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox_banner)).BeginInit();
            this.panel.SuspendLayout();
            this.grb_cnae.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grd_cnae)).BeginInit();
            this.grb_paginacao.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel
            // 
            this.panel.Controls.Add(this.grb_paginacao);
            this.panel.Controls.Add(this.grb_cnae);
            this.panel.Location = new System.Drawing.Point(4, 45);
            this.panel.Size = new System.Drawing.Size(385, 315);
            // 
            // grb_cnae
            // 
            this.grb_cnae.Controls.Add(this.grd_cnae);
            this.grb_cnae.Location = new System.Drawing.Point(4, 4);
            this.grb_cnae.Name = "grb_cnae";
            this.grb_cnae.Size = new System.Drawing.Size(376, 256);
            this.grb_cnae.TabIndex = 0;
            this.grb_cnae.TabStop = false;
            this.grb_cnae.Text = "Cnae";
            // 
            // grd_cnae
            // 
            this.grd_cnae.AllowUserToAddRows = false;
            this.grd_cnae.AllowUserToDeleteRows = false;
            this.grd_cnae.AllowUserToOrderColumns = true;
            this.grd_cnae.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this.grd_cnae.BackgroundColor = System.Drawing.Color.White;
            this.grd_cnae.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.grd_cnae.Location = new System.Drawing.Point(3, 19);
            this.grd_cnae.MultiSelect = false;
            this.grd_cnae.Name = "grd_cnae";
            this.grd_cnae.ReadOnly = true;
            this.grd_cnae.RowHeadersWidth = 30;
            this.grd_cnae.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.grd_cnae.Size = new System.Drawing.Size(370, 231);
            this.grd_cnae.TabIndex = 1;
            // 
            // grb_paginacao
            // 
            this.grb_paginacao.Controls.Add(this.btn_ok);
            this.grb_paginacao.Location = new System.Drawing.Point(4, 266);
            this.grb_paginacao.Name = "grb_paginacao";
            this.grb_paginacao.Size = new System.Drawing.Size(376, 44);
            this.grb_paginacao.TabIndex = 1;
            this.grb_paginacao.TabStop = false;
            // 
            // btn_ok
            // 
            this.btn_ok.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_ok.Image = global::SegSis.Properties.Resources.fechar_ico;
            this.btn_ok.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btn_ok.Location = new System.Drawing.Point(6, 15);
            this.btn_ok.Name = "btn_ok";
            this.btn_ok.Size = new System.Drawing.Size(75, 23);
            this.btn_ok.TabIndex = 2;
            this.btn_ok.Text = "&Confirmar";
            this.btn_ok.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btn_ok.UseVisualStyleBackColor = true;
            this.btn_ok.Click += new System.EventHandler(this.btn_ok_Click);
            // 
            // frm_clienteCnaePrincipalAlterar
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(394, 368);
            this.ControlBox = false;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "frm_clienteCnaePrincipalAlterar";
            this.Text = "CNAE PRINCIPAL";
            this.panel_banner.ResumeLayout(false);
            this.panel_banner.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox_banner)).EndInit();
            this.panel.ResumeLayout(false);
            this.grb_cnae.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.grd_cnae)).EndInit();
            this.grb_paginacao.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox grb_paginacao;
        private System.Windows.Forms.GroupBox grb_cnae;
        private System.Windows.Forms.DataGridView grd_cnae;
        private System.Windows.Forms.Button btn_ok;
    }
}