﻿using SWS.Entidade;
using SWS.Facade;
using SWS.View.ViewHelper;
using SWS.ViewHelper;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace SWS.View
{
    public partial class frmMonitoramento : frmTemplate
    {
        private Cliente cliente;

        public Cliente Cliente
        {
            get { return cliente; }
            set { cliente = value; }
        }

        private void validaPermissoes()
        {
            PermissionamentoFacade permissionamentoFacade = PermissionamentoFacade.getInstance();

            btnIncluir.Enabled = permissionamentoFacade.hasPermission("ESOCIAL-2240", "INCLUIR");
            btnPesquisar.Enabled = permissionamentoFacade.hasPermission("ESOCIAL-2240", "PESQUISAR");
        }

        private List<ClienteFuncaoFuncionario> funcionarios = new List<ClienteFuncaoFuncionario>();
        private List<ClienteFuncaoFuncionario> funcionariosRestantes = new List<ClienteFuncaoFuncionario>();

        public frmMonitoramento()
        {
            InitializeComponent();

            ComboHelper.comboMes(cbMesMonitoramento);
            ComboHelper.comboAno(cbAnoMonitoramento);

            /* setando valores default para os combos */

            cbMesMonitoramento.SelectedValue = DateTime.Now.Month.ToString();
            cbAnoMonitoramento.SelectedValue = DateTime.Now.Year.ToString();

            validaPermissoes();
        }

        private void btnFechar_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnIncluir_Click(object sender, EventArgs e)
        {
            try
            {
                if (dgvFuncionario.CurrentRow == null)
                    throw new Exception("Selecione primeiro um funcionário.");
                
                FuncionarioFacade funcionarioFacade = FuncionarioFacade.getInstance();
                AsoFacade asoFacade = AsoFacade.getInstance();

                int ano = Convert.ToInt32(((SelectItem)cbAnoMonitoramento.SelectedItem).Valor);
                int mes = Convert.ToInt32(((SelectItem)cbMesMonitoramento.SelectedItem).Valor);
                int dia = DateTime.DaysInMonth(ano, mes);

                frmMonitoramentoFuncionario incluirMonitoramento = new frmMonitoramentoFuncionario(asoFacade.findClienteFuncaoFuncionarioById((long)dgvFuncionario.CurrentRow.Cells[0].Value), new DateTime(ano, mes , dia ));
                if (incluirMonitoramento.ValidaDados == true) incluirMonitoramento.ShowDialog();

                if (incluirMonitoramento.Monitoramento != null)
                {
                    funcionarios.Remove(funcionarios.Find(x => x.Id == (long)dgvFuncionario.CurrentRow.Cells[0].Value));
                    montaDataGrid(funcionarios);
                }
                    

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }

        private void btnPesquisar_Click(object sender, EventArgs e)
        {
            try
            {
                if (!validaDadosTela())
                {
                    /* iniciando processo de busca dos colaboradores */
                    this.Cursor = Cursors.WaitCursor;

                    FuncionarioFacade funcionarioFacade = FuncionarioFacade.getInstance();
                    AsoFacade asoFacade = AsoFacade.getInstance();
                    EsocialFacade esocialFacade = EsocialFacade.getInstance();

                    int ano = Convert.ToInt32(((SelectItem)cbAnoMonitoramento.SelectedItem).Valor);
                    int mes = Convert.ToInt32(((SelectItem)cbMesMonitoramento.SelectedItem).Valor);
                    int dia = DateTime.DaysInMonth(ano, mes);

                    DateTime periodo = new DateTime(ano, mes , dia );
                    DateTime periodoPassado = apuraUltimoPeriodo(ano, mes);


                    /* verificando se o cliente selecionado já teve movimento realizado no período passado, nesse caso
                     * ele deverá mostrar apenas os atendimentos dos funcionários que realizaram exames
                     * admissionais, periódicos e mudança de função */

                    if (MessageBox.Show("Deseja gerar uma carga inicial de dados?", "Atenção", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) == DialogResult.No)
                    {
                        /* zerando informações de pesquias */

                        funcionarios.Clear();
                        
                        bool monitoramentosRealizados = esocialFacade.existeMonitoramentoPassadoByClienteAndPeriodo(cliente, periodoPassado);

                        if (monitoramentosRealizados == true)
                        {
                            funcionarios = funcionarioFacade.findAllFuncionariosAtivosByClienteForMonitoramentoPeriodico(Cliente, periodo, true);

                            /* verificando os funcionários que já tiveram movimento do mês retrasado */

                            bool flag = false;

                            if (funcionarios.Count == 0)
                                throw new Exception("Não existe monitoramento para ser realizado desse cliente");

                            if (MessageBox.Show("Deseja recuperar os ultimos monitoramentos? Esse procedimento poderá demorar um pouco.", "Atenção", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                            {
                                /* comparando os funcionários para saber se já tem monitoramento anterior */

                                funcionarios.ForEach(delegate(ClienteFuncaoFuncionario clienteFuncionarioComMonitoramento)
                                {
                                    Monitoramento monitoramentoAnterior = esocialFacade.findLastMonitoramentoByClienteFuncionario(clienteFuncionarioComMonitoramento, periodoPassado);

                                    if (monitoramentoAnterior != null)
                                    {
                                        /* encontrando o monitoramento desse funcionário. 
                                            * Será incluido o  monitoramento e ele não será incluído na 
                                            * lista para novos monitoramento */

                                        esocialFacade.replicarMonitoramento(monitoramentoAnterior, periodo);
                                        flag = true;
                                    }
                                    else
                                    {
                                        funcionariosRestantes.Add(clienteFuncionarioComMonitoramento);
                                    }
                                });

                            }

                            if (!flag) MessageBox.Show("Não foi encontrado nenhum movimento pendente para replicação ou restauração ja foi realizada.", "Confirmação", MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
                            else MessageBox.Show("Monitoramento(s) foram replicados com sucesso");

                            montaDataGrid(monitoramentosRealizados == true && funcionariosRestantes.Count > 0 ? funcionariosRestantes : funcionarios);
                        }
                        else
                        {
                            /* nesse caso, seria uma carga inicial de dados. Deverá ser levantado todos os funcionários ativos do cliente */
                            funcionarios.Clear();
                            funcionarios = funcionarioFacade.findAllFuncionariosAtivosByClienteForMonitoramentoPeriodico(cliente, periodo, false);
                            montaDataGrid(funcionarios);
                        }
                    }
                    else
                    {
                        funcionarios.Clear();
                        funcionarios = funcionarioFacade.findAllFuncionariosAtivosByClienteForMonitoramentoPeriodico(cliente, periodo, false);
                        montaDataGrid(funcionarios);
                    }

                }

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }

        }

        private void btnCliente_Click(object sender, EventArgs e)
        {
            try
            {
                frmClienteSelecionar selecionarCliente = new frmClienteSelecionar(null, null, false, null, null, false, null, null, true);
                selecionarCliente.ShowDialog();

                if (selecionarCliente.Cliente != null)
                {
                    this.Cliente = selecionarCliente.Cliente;
                    this.textCliente.Text = this.Cliente.RazaoSocial + " - " + ((int)this.Cliente.tipoCliente() == 1 ? ValidaCampoHelper.FormataCnpj(this.Cliente.Cnpj) : ValidaCampoHelper.FormataCpf(this.cliente.Cnpj));
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }

        private void btnExcluirCliente_Click(object sender, EventArgs e)
        {
            try
            {
                if (this.Cliente == null) throw new Exception("Selecione primeiro o cliente.");
                this.Cliente = null;
                textCliente.Text = string.Empty;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }

        private bool validaDadosTela()
        {
            bool validaDados = false;
            try
            {
                /* verificando se o cliente foi selecionado */
                if (this.Cliente == null)
                    throw new Exception("É necessário selecionar o cliente");

            }
            catch (Exception ex)
            {
                validaDados = true;
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            return validaDados;

        }

        private void montaDataGrid(List<ClienteFuncaoFuncionario> funcionarios)
        {
            try
            {
                this.dgvFuncionario.Columns.Clear();
                this.textTotal.Text = string.Empty;
                this.Cursor = Cursors.WaitCursor;

                this.dgvFuncionario.ColumnCount = 9;

                this.dgvFuncionario.Columns[0].HeaderText = "idClienteFuncionario";
                this.dgvFuncionario.Columns[0].Visible = false;

                this.dgvFuncionario.Columns[1].HeaderText = "idFuncionario";
                this.dgvFuncionario.Columns[1].Visible = false;

                this.dgvFuncionario.Columns[2].HeaderText = "Nome do Funcionário";
                this.dgvFuncionario.Columns[3].HeaderText = "CPF";
                this.dgvFuncionario.Columns[4].HeaderText = "RG";
                this.dgvFuncionario.Columns[5].HeaderText = "PIS/PASEP";
                this.dgvFuncionario.Columns[6].HeaderText = "Data de Nascimento";
                this.dgvFuncionario.Columns[7].HeaderText = "Data de Admissão";
                this.dgvFuncionario.Columns[8].HeaderText = "Matrícula";

                /* montando a informação da grid através da lista de clienteFuncionario */

                foreach (ClienteFuncaoFuncionario clienteFuncionario in funcionarios)
                {
                    this.dgvFuncionario.Rows.Add(
                        clienteFuncionario.Id,
                        clienteFuncionario.Funcionario.Id,
                        clienteFuncionario.Funcionario.Nome,
                        ValidaCampoHelper.FormataCpf(clienteFuncionario.Funcionario.Cpf),
                        clienteFuncionario.Funcionario.Rg,
                        clienteFuncionario.Funcionario.PisPasep,
                        String.Format("{0:dd/MM/yyyy}", clienteFuncionario.Funcionario.DataNascimento),
                        String.Format("{0:dd/MM/yyyy}", clienteFuncionario.DataCadastro),
                        clienteFuncionario.Matricula
                    );
                }

                this.textTotal.Text = funcionarios.Count.ToString();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }


        }

        private DateTime apuraUltimoPeriodo(int ano, int mes)
        {
            DateTime periodoAnterior = new DateTime();

            int mesAnterior = mes -1;
            int anoAnterior = ano - 1;

            if (mes - 1 == 0)
            {
                mesAnterior = 12;
                periodoAnterior = new DateTime(anoAnterior, mesAnterior, DateTime.DaysInMonth(anoAnterior, mesAnterior));
            }
            else
            {
                periodoAnterior = new DateTime(ano, mesAnterior, DateTime.DaysInMonth(ano, mesAnterior));
            }

            return periodoAnterior;
        }

        private void frmMonitoramento_Load(object sender, EventArgs e)
        {

        }
    }
}
