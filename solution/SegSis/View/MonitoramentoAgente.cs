﻿using SWS.Entidade;
using SWS.Facade;
using SWS.View.ViewHelper;
using SWS.ViewHelper;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace SWS.View
{
    public partial class frmMonitoramentoAgente : frmTemplateConsulta
    {
        protected GheFonteAgente gheFonteAgente;
        protected List<string> codigosParaLimiteTolerancia = new List<string> { "01.18.001", "02.01.014" };
        protected MonitoramentoGheFonteAgente monitoramentoGheFonteAgente;
        protected Monitoramento monitoramento;
        protected List<EpiMonitoramento> epis = new List<EpiMonitoramento>();
        

        public MonitoramentoGheFonteAgente MonitoramentoGheFonteAgente
        {
            get { return monitoramentoGheFonteAgente; }
            set { monitoramentoGheFonteAgente = value; }
        }

        public frmMonitoramentoAgente()
        {
            InitializeComponent();
        }
        
        public frmMonitoramentoAgente(GheFonteAgente gheFonteAgente, Monitoramento monitoramento)
        {
            InitializeComponent();
            this.gheFonteAgente = gheFonteAgente;
            this.monitoramento = monitoramento;
            textAgente.Text = this.gheFonteAgente.Agente.Descricao;
            TextRisco.Text = this.gheFonteAgente.Agente.Risco.Descricao;

            ComboHelper.tipoAvaliacao(cbTipoAvaliacao);
            ComboHelper.unidadeMedicaoAgente(cbUnidadeMedida);
                      
            ComboHelper.utilizaEPC(cbUtilizaEPC);
            ComboHelper.utilizaEPI(cbUtilizaEPI);
            ComboHelper.booleandoEsocial(cbEficaciaEpc);
            ComboHelper.booleandoEsocial(cbEficaciaEPI);

            ComboHelper.booleanoEsocialSemDefault(cbMedidaProtecao);
            ComboHelper.booleanoEsocialSemDefault(cbCondicaoFuncionamento);
            ComboHelper.booleanoEsocialSemDefault(cbUsoIniterrupto);
            ComboHelper.booleanoEsocialSemDefault(cbPrazoValidade);
            ComboHelper.booleanoEsocialSemDefault(cbPeriodicidadeTroca);
            ComboHelper.booleanoEsocialSemDefault(cbHigienizacao);

            /* liberando a parte de epi somente quando for marcado a utilizacao do epi */
            flpAcaoEpi.Enabled = false;
            gbrPerguntasGrupo2.Enabled = false;


        }

        protected virtual void btnIncluir_Click(object sender, EventArgs e)
        {
            try
            {
                /* validando campos obrigatórios */
                if (!validaCamposTela())
                {
                    EsocialFacade esocialFacade = EsocialFacade.getInstance();

                    MonitoramentoGheFonteAgente monitoramentoAgenteNovo = new MonitoramentoGheFonteAgente( null,
                        gheFonteAgente,
                        monitoramento,
                        cbTipoAvaliacao.SelectedIndex == -1 ? string.Empty : ((SelectItem)cbTipoAvaliacao.SelectedItem).Valor,
                        string.IsNullOrEmpty(textIntensidade.Text.Trim()) ? Convert.ToDecimal(0) : Convert.ToDecimal(textIntensidade.Text),
                        string.IsNullOrEmpty(textLimiteTolerancia.Text.Trim()) ? Convert.ToDecimal(0) : Convert.ToDecimal(textLimiteTolerancia.Text),
                        cbUnidadeMedida.SelectedIndex == -1 ? string.Empty : ((SelectItem)cbUnidadeMedida.SelectedItem).Valor,
                        textTecnica.Text.Trim(),
                        ((SelectItem)cbUtilizaEPC.SelectedItem).Valor,
                        string.Equals(((SelectItem)cbUtilizaEPC.SelectedItem).Valor, "2") ? ((SelectItem)cbEficaciaEpc.SelectedItem).Valor : string.Empty,
                        ((SelectItem)cbUtilizaEPI.SelectedItem).Valor,
                        string.Equals(((SelectItem)cbUtilizaEPI.SelectedItem).Valor, "2") ? ((SelectItem)cbEficaciaEPI.SelectedItem).Valor : string.Empty,
                        ((SelectItem)cbMedidaProtecao.SelectedItem).Valor, 
                        ((SelectItem)cbCondicaoFuncionamento.SelectedItem).Valor,
                        ((SelectItem)cbUsoIniterrupto.SelectedItem).Valor,
                        ((SelectItem)cbPrazoValidade.SelectedItem).Valor,
                        ((SelectItem)cbPeriodicidadeTroca.SelectedItem).Valor,
                        ((SelectItem)cbHigienizacao.SelectedItem).Valor,
                        string.Empty);
                    
                    monitoramentoAgenteNovo.MonitoramentoEpis = epis;
                    monitoramentoAgenteNovo.NumeroProcessoJudicial = textNumeroProcesso.Text;

                    MonitoramentoGheFonteAgente = esocialFacade.insertMonitoramentoGheFonteAgente(monitoramentoAgenteNovo);

                    MessageBox.Show("Monitoramento do agente incluído com sucesso.", "Confirmação", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    this.Close();
                    
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }

        protected void lblFechar_Click(object sender, EventArgs e)
        {
            if (!validaCamposTela())
            {
                this.Close();
            }
            else
            {
                MessageBox.Show("Você ainda tem informações pendente para resolver antes de finalizar essa tela.", "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
            
        }

        protected void cbTipoAvaliacao_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (((SelectItem)cbTipoAvaliacao.SelectedItem).Valor == "1")
            {
                textIntensidade.Enabled = true;
                textIntensidade.Text = string.Empty;
                cbUnidadeMedida.Enabled = true;
                textTecnica.Enabled = true;
                textTecnica.Text = string.Empty;
                textLimiteTolerancia.Text = string.Empty;
                textLimiteTolerancia.Enabled = codigosParaLimiteTolerancia.Any(x => this.monitoramentoGheFonteAgente != null ? x == MonitoramentoGheFonteAgente.GheFonteAgente.Agente.CodigoEsocial : x == gheFonteAgente.Agente.CodigoEsocial) ? true : false;
                
            }
            else
            {
                textIntensidade.Enabled = false;
                cbUnidadeMedida.Enabled = false;
                textTecnica.Enabled = false;
                textLimiteTolerancia.Enabled = false;

            }

        }

        protected void cbUtilizaEPC_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (((SelectItem)cbUtilizaEPC.SelectedItem).Valor == "2")
            {
                cbEficaciaEpc.Enabled = true;
            }
            else
            {
                cbEficaciaEpc.Enabled = false;
            }
        }

        protected virtual void cbUtilizaEPI_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (((SelectItem)cbUtilizaEPI.SelectedItem).Valor == "2")
            {
                cbEficaciaEPI.Enabled = true;
                /* habilitando demais controles e carregando informação necessária */
                flpAcaoEpi.Enabled = true;
                gbrPerguntasGrupo2.Enabled = true;

            }
            else
            {
                cbEficaciaEPI.Enabled = false;
                flpAcaoEpi.Enabled = false;
                gbrPerguntasGrupo2.Enabled = false;
            }
        }

        protected void textIntensidade_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = ValidaCampoHelper.FormataDigitacaoNumero(sender, e, textIntensidade.Text, 4);
        }

        protected void textLimiteTolerancia_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = ValidaCampoHelper.FormataDigitacaoNumero(sender, e, textLimiteTolerancia.Text, 4);
        }

        protected void textIntensidade_Leave(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(textIntensidade.Text))
                textIntensidade.Text = textIntensidade.Tag.ToString();

            textIntensidade.Text = ValidaCampoHelper.formataNumeroDecimal(textIntensidade.Text, 4);
        }

        protected void textLimiteTolerancia_Leave(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(textLimiteTolerancia.Text))
                textLimiteTolerancia.Text = textLimiteTolerancia.Tag.ToString();

            textLimiteTolerancia.Text = ValidaCampoHelper.formataNumeroDecimal(textLimiteTolerancia.Text, 4);

        }

        protected void textIntensidade_Enter(object sender, EventArgs e)
        {
            textIntensidade.Tag = textIntensidade.Text;
            textIntensidade.Text = string.Empty;
        }

        protected void textLimiteTolerancia_Enter(object sender, EventArgs e)
        {
            textLimiteTolerancia.Tag = textLimiteTolerancia.Text;
            textLimiteTolerancia.Text = string.Empty;
        }

        protected bool validaCamposTela()
        {
            bool retorno = false;
            try
            {
                /* validando informação em relação a análise quantitativa */

                if (cbTipoAvaliacao.SelectedIndex != -1 && ((SelectItem)cbTipoAvaliacao.SelectedItem).Valor == "1")
                {
                    if (string.IsNullOrEmpty(textIntensidade.Text.Trim()))
                        throw new Exception("Campo intensidade obrigatório para tipo de analise quantitativa.");

                    if (cbUnidadeMedida.SelectedIndex == 0)
                        throw new Exception("Obrigatório escolher a unidade de medida para análise quantitativa.");

                    if (string.IsNullOrEmpty(textTecnica.Text.Trim()))
                        throw new Exception("Obrigatório informar o tipo de técnica para análise quantitativa.");
                }

                /* validando informação em relação ao EP */

                if (((SelectItem)cbUtilizaEPC.SelectedItem).Valor == "2")
                {
                    if (cbEficaciaEpc.SelectedIndex == -1)
                        throw new Exception("Obrigatório informar a eficácia do EPC para casos em que o uso do EPC é implementado.");
                }

                if (((SelectItem)cbUtilizaEPI.SelectedItem).Valor == "2")
                {
                    if (cbEficaciaEPI.SelectedIndex == -1 )
                        throw new Exception("Obrigatório informar a eficácia do EPI para casos em que o usuo do EPI é utilizado.");

                    if (epis.Count == 0)
                        throw new Exception("Você indicou que foi utilizado EPI porém não informou nenhum EPI.");

                    if (cbMedidaProtecao.SelectedIndex == 0)
                        throw new Exception("Você deve informar Sim ou Não para pergunta 1 do grupo 2.");

                    if (cbCondicaoFuncionamento.SelectedIndex == 0)
                        throw new Exception("Você deve informar Sim ou Não para pergunta 2 do grupo 2.");

                    if (cbUsoIniterrupto.SelectedIndex == 0)
                        throw new Exception("Você deve informar Sim ou Não para pergunta 3 do grupo 2.");

                    if (cbPrazoValidade.SelectedIndex == 0)
                        throw new Exception("Você deve informar Sim ou Não para pergunta 4 do grupo 2.");

                    if (cbPeriodicidadeTroca.SelectedIndex == 0)
                        throw new Exception("Você deve informar Sim ou Não para pergunta 5 do grupo 2.");

                    if (cbHigienizacao.SelectedIndex == 0)
                        throw new Exception("Você deve informar Sim ou Não para pergunta 6 do grupo 2.");
                }

                if (this.gheFonteAgente.Agente.CodigoEsocial == "05.01.001" && string.IsNullOrEmpty(textNumeroProcesso.Text))
                {
                    ActiveControl = textNumeroProcesso;
                    throw new Exception("Agente selecionado é do tipo 05.01.001, sendo obrigatório a partir da versão 1.2 que seja informado o número do processo judicial.");
                }
            }
            catch (Exception ex)
            {
                retorno = true;
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            return retorno;
        }

        protected virtual void btnIncluirEpi_Click(object sender, EventArgs e)
        {
            try
            {
                frmEpiBuscar buscarEpi = new frmEpiBuscar();
                buscarEpi.ShowDialog();

                if (buscarEpi.Epi != null)
                {
                    /* verificando se o epi selecionado está na lista */
                    if (epis.Exists(x => x.Epi.Id == buscarEpi.Epi.Id))
                        throw new Exception("Epi já incluido.");

                    /* incluir o epi no agente monitorado */
                    epis.Add(new EpiMonitoramento(null, buscarEpi.Epi, null));
                    montaGridEpis();
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }

        protected virtual void btnExcluirEpi_Click(object sender, EventArgs e)
        {
            try
            {
                if (dgvEpis.CurrentRow == null)
                    throw new Exception("Selecione um EPI para excluir");

                if (MessageBox.Show("Deseja excluir o EPI selecionado?", "Atenção", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                {
                    EpiMonitoramento epiMonitoramentoExcluir = epis.Find(x => x.Epi.Id == (long)dgvEpis.CurrentRow.Cells[1].Value);
                    epis.RemoveAt(dgvEpis.CurrentRow.Index);
                    MessageBox.Show("Epi excluído com sucesso", "Confirmação", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    montaGridEpis();
                }
                
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }


        }

        protected void montaGridEpis()
        {
            try
            {
                dgvEpis.Columns.Clear();
                dgvEpis.ColumnCount = 3;

                dgvEpis.Columns[0].HeaderText = "idEpiMonitoramento";
                dgvEpis.Columns[0].Visible = false;

                dgvEpis.Columns[1].HeaderText = "idEpi";
                dgvEpis.Columns[1].Visible = false;

                dgvEpis.Columns[2].HeaderText = "EPI";

                foreach (EpiMonitoramento epi in epis)
                {
                    dgvEpis.Rows.Add(epi.Id, epi.Epi.Id, epi.Epi.Descricao);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }
       
    }
}
