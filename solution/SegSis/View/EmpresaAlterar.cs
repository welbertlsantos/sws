﻿using SWS.Entidade;
using SWS.Facade;
using SWS.Helper;
using SWS.View.Resources;
using SWS.View.ViewHelper;
using SWS.ViewHelper;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace SWS.View
{
    public partial class frmEmpresaAlterar : frmEmpresaIncluir
    {
        public frmEmpresaAlterar(Empresa empresa) :base()
        {
            InitializeComponent();

            this.empresa = empresa;

            text_razaoSocial.Text = empresa.RazaoSocial;
            text_fantasia.Text = empresa.Fantasia;
            text_cnpj.Text = empresa.Cnpj;
            text_inscricao.Text = empresa.Inscricao;
            text_endereco.Text = empresa.Endereco;
            text_numero.Text = empresa.Numero;
            text_complemento.Text = empresa.Complemento;
            text_bairro.Text = empresa.Bairro;
            text_cep.Text = empresa.Cep;
            cbUf.SelectedValue = empresa.Uf;
            
            if (!String.IsNullOrEmpty(empresa.Uf) && empresa.Cidade != null)
                cbCidade.SelectedValue = empresa.Cidade.Id.ToString();
            
            text_telefone1.Text = empresa.Telefone1;
            text_telefone2.Text = empresa.Telefone2;
            text_email.Text = empresa.Email;
            text_site.Text = empresa.Site;
            cbSimples.SelectedValue = empresa.SimplesNacional == true ? "true" : "false";

            if (empresa.Logo != null)
                pic_box_logo.Image = FileHelper.byteArrayToImage(empresa.Logo);

            arquivo = new Arquivo(null, String.Empty, empresa.Mimetype, 0, empresa.Logo);

            this.matriz = empresa.Matriz;
            if (matriz != null)
                textMatriz.Text = matriz.RazaoSocial;
            textCodigoCnes.Text = empresa.CodigoCnes;

            textValorMinimoImpostoFederal.Text = empresa.ValorMinimoImpostoFederal.ToString();
            textValorMinimoIR.Text = empresa.ValorMinimoIR.ToString();
            textAliquotaPIS.Text = empresa.AliquotaPIS.ToString();
            textAliquotaCOFINS.Text = empresa.AliquotaCOFINS.ToString();
            textAliquotaIR.Text = empresa.AliquotaIR.ToString();
            textAliquotaCSLL.Text = empresa.AliquotaCSLL.ToString();
            textAliquotaISS.Text = empresa.AliquotaISS.ToString();
        }

        protected override void btn_gravar_Click(object sender, EventArgs e)
        {
            try
            {
                this.empresaErrorProvider.Clear();
            
                if (validaCamposObrigatorio())
                {

                    EmpresaFacade empresaFacade = EmpresaFacade.getInstance();
                    ClienteFacade clienteFacade = ClienteFacade.getInstance();

                    empresa = empresaFacade.updateEmpresa(new Empresa(empresa.Id, text_razaoSocial.Text, text_fantasia.Text, text_cnpj.Text, text_inscricao.Text, text_endereco.Text, text_numero.Text, text_complemento.Text, text_bairro.Text, text_cep.Text, ((SelectItem)cbUf.SelectedItem).Valor, text_telefone1.Text, text_telefone2.Text, text_email.Text, text_site.Text, empresa.DataCadastro, ApplicationConstants.ATIVO, arquivo.Mimetype, arquivo.Conteudo, clienteFacade.findCidadeIbgeById(Convert.ToInt64(((SelectItem)cbCidade.SelectedItem).Valor)), Convert.ToBoolean(((SelectItem)cbSimples.SelectedItem).Valor), matriz, textCodigoCnes.Text, Convert.ToDecimal(textValorMinimoImpostoFederal.Text), Convert.ToDecimal(textValorMinimoIR.Text), Convert.ToDecimal(textAliquotaPIS.Text), Convert.ToDecimal(textAliquotaCOFINS.Text), Convert.ToDecimal(textAliquotaIR.Text), Convert.ToDecimal(textAliquotaCSLL.Text), Convert.ToDecimal(textAliquotaISS.Text)));

                    MessageBox.Show("Empresa alterada com sucesso.", "Confirmação", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        
                    this.Close();
                }
            }
            catch (Exception Ex)
            {
                MessageBox.Show(Ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        protected override void btnMatriz_Click(object sender, EventArgs e)
        {
            try
            {

                /* buscando empresa matriz */

                frmEmpresaBuscar buscarEmpresa = new frmEmpresaBuscar();
                buscarEmpresa.ShowDialog();

                if (buscarEmpresa.Empresa != null)
                {
                    if (buscarEmpresa.Empresa.Id == empresa.Id)
                        throw new Exception("A empresa não pode ser filial dela mesma.");

                    matriz = buscarEmpresa.Empresa;
                    textMatriz.Text = matriz.RazaoSocial;
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

    
    }
            
}
