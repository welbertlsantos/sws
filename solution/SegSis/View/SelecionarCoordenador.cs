﻿using SWS.Entidade;
using SWS.Facade;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SWS.View
{
    public partial class frmSelecionarCoordenador : frmTemplateConsulta
    {
        private Usuario coordenador;

        public Usuario Coordenador
        {
            get { return coordenador; }
            set { coordenador = value; }
        }
        
        public frmSelecionarCoordenador()
        {
            InitializeComponent();
            this.MontaDataGrid();
        }

        private void btnSelecionar_Click(object sender, EventArgs e)
        {
            try
            {
                if (dgvCoordenador.CurrentRow == null)
                    throw new Exception("Selecione o coordenador");
                
                UsuarioFacade usuarioFacade = UsuarioFacade.getInstance();
                this.Coordenador = usuarioFacade.findUsuarioById((long)this.dgvCoordenador.CurrentRow.Cells[0].Value);
                this.Close();

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void btnFechar_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        public void MontaDataGrid()
        {
            try
            {
                UsuarioFacade usuarioFacade = UsuarioFacade.getInstance();

                this.dgvCoordenador.Columns.Clear();

                this.dgvCoordenador.ColumnCount = 9;

                this.dgvCoordenador.Columns[0].HeaderText = "idUsuario";
                this.dgvCoordenador.Columns[0].Visible = false;

                this.dgvCoordenador.Columns[1].HeaderText = "Nome";
                this.dgvCoordenador.Columns[2].HeaderText = "CPF";
                this.dgvCoordenador.Columns[2].Visible = false;

                this.dgvCoordenador.Columns[3].HeaderText = "RG";
                this.dgvCoordenador.Columns[3].Visible = false;

                this.dgvCoordenador.Columns[4].HeaderText = "Documento Extra";
                this.dgvCoordenador.Columns[4].Visible = false;

                this.dgvCoordenador.Columns[5].HeaderText = "Data_de_admissão";
                this.dgvCoordenador.Columns[5].Visible = false;

                this.dgvCoordenador.Columns[6].HeaderText = "Situação";
                this.dgvCoordenador.Columns[6].Visible = false;

                this.dgvCoordenador.Columns[7].HeaderText = "Login";
                this.dgvCoordenador.Columns[7].Visible = false;

                this.dgvCoordenador.Columns[8].HeaderText = "Id_médico";
                this.dgvCoordenador.Columns[8].Visible = false;

                foreach (Usuario usuario in usuarioFacade.findUsuarioByFuncaoInterna(4, null))
                {
                    if (usuario.Medico != null)
                    {
                        this.dgvCoordenador.Rows.Add(usuario.Id, usuario.Nome, usuario.Cpf,
                            usuario.Rg, usuario.DocumentoExtra, usuario.DataAdmissao,
                            usuario.Situacao, usuario.Login, usuario.Medico.Id);
                    }
                    else
                    {
                        this.dgvCoordenador.Rows.Add(usuario.Id, usuario.Nome, usuario.Cpf,
                            usuario.Rg, usuario.DocumentoExtra, usuario.DataAdmissao,
                            usuario.Situacao, usuario.Login, null);
                    }

                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
        }


    }
}
