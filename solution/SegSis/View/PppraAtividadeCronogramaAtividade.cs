﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using SWS.Facade;
using SWS.Entidade;
using SWS.Excecao;
using SWS.View.ViewHelper;
using System.Text.RegularExpressions;
using SWS.View.Resources;



namespace SWS.View
{
    public partial class frm_ppraAtvidadeCronogramaAtividade : BaseFormConsulta
    {
        private static String Msg01 = " Selecione um linha";
        private static string Msg02 = " Atenção";

        Atividade atividade = null;

        public Atividade getAtividade()
        {
            return this.atividade;
        }
       
        public frm_ppraAtvidadeCronogramaAtividade()
        {
            InitializeComponent();
            validaPermissoes();
        }

        private void validaPermissoes()
        {
            PermissionamentoFacade permissionamentoFacade = PermissionamentoFacade.getInstance();

            this.btn_novo.Enabled = permissionamentoFacade.hasPermission("ATIVIDADE", "INCLUIR");
        }

        private void btn_fechar_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btn_novo_Click(object sender, EventArgs e)
        {
            frmAtividadeIncluir formAtividadeIncluir = new frmAtividadeIncluir();
            formAtividadeIncluir.ShowDialog();

            if (formAtividadeIncluir.Atividade != null)
            {
                atividade = formAtividadeIncluir.Atividade;

                text_descricao.Text = atividade.Descricao;

                montaDataGrid();
                btn_ok.PerformClick();
            }

        }

        private void btn_buscar_Click(object sender, EventArgs e)
        {
            try
            {
                this.grd_atividade.Columns.Clear();
                montaDataGrid();
                text_descricao.Focus();
                
            }
            catch (Exception Ex)
            {
                MessageBox.Show(Ex.Message);
            }
        }

        private void btn_ok_Click(object sender, EventArgs e)
        {
            PpraFacade ppraFacade = PpraFacade.getInstance();

            try
            {
                if (grd_atividade.CurrentRow != null)
                {
                    atividade = ppraFacade.findAtividadeById((Int64)grd_atividade.CurrentRow.Cells[0].Value);
                    
                    this.Close();

                }
                else
                {
                    MessageBox.Show(Msg01, Msg02);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        public void montaDataGrid()
        {
            PpraFacade ppraFacade = PpraFacade.getInstance();

            atividade = new Atividade(null, text_descricao.Text.ToUpper().Trim(), ApplicationConstants.ATIVO);

            DataSet ds = ppraFacade.findAtividadeAtivosByDescricao(atividade);

            grd_atividade.DataSource = ds.Tables["Atividades"].DefaultView;
            
            grd_atividade.Columns[0].HeaderText = "ID_Atividade";
            grd_atividade.Columns[0].Visible = false;

            grd_atividade.Columns[1].HeaderText = "Descrição";
        }
    }
}
