﻿using SWS.Entidade;
using SWS.Facade;
using SWS.View.ViewHelper;
using SWS.ViewHelper;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace SWS.View
{
    public partial class frmContatoIncluir : frmTemplateConsulta
    {
        protected Contato contato;

        public Contato Contato
        {
            get { return contato; }
            set { contato = value; }
        }

        protected CidadeIbge cidade;

        public frmContatoIncluir()
        {
            InitializeComponent();
            ComboHelper.findTipoContato(cbTipoContato);
            ComboHelper.unidadeFederativa(cbUF);
            ComboHelper.Boolean(cbResponsavel, false);
            ComboHelper.Boolean(cbResponsavelEstudo, false);
            ActiveControl = textNome;
        }

        protected virtual void btGravar_Click(object sender, EventArgs e)
        {
            try
            {
                if (validaCampos())
                {
                    contato = new Contato(null, cidade, new TipoContato(Convert.ToInt64(((SelectItem)cbTipoContato.SelectedItem).Valor), ((SelectItem)cbTipoContato.SelectedItem).Nome), null, textNome.Text.Trim(), textCPF.Text, textRG.Text.Trim(), dataNascimento.Checked ? (DateTime?)Convert.ToDateTime(dataNascimento.Text) : null, textEndereco.Text.Trim(), textNumero.Text.Trim(), textComplemento.Text.Trim(), textBairro.Text.Trim(), ((SelectItem)cbUF.SelectedItem).Valor, textCEP.Text, textTelefone.Text.Trim(), textCelular.Text.Trim(), textPis.Text.Trim(), textEmail.Text.Trim(), Convert.ToBoolean(((SelectItem)cbResponsavel.SelectedItem).Valor), Convert.ToBoolean(((SelectItem)cbResponsavelEstudo.SelectedItem).Valor)); 
                    this.Close();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atençao", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }

        }

        protected void btLimpar_Click(object sender, EventArgs e)
        {
            ComboHelper.findTipoContato(cbTipoContato);
            textNome.Text = String.Empty;
            textNome.Focus();
            textCPF.Text = String.Empty;
            textRG.Text = String.Empty;
            dataNascimento.Checked = false;
            textEndereco.Text = String.Empty;
            textNumero.Text = String.Empty;
            textComplemento.Text = String.Empty;
            this.cidade = null;
            textBairro.Text = String.Empty;
            ComboHelper.startComboUfSave(cbUF);
            ComboHelper.Boolean(cbResponsavel, false);
            ComboHelper.Boolean(cbResponsavelEstudo, false);
            textCidade.Text = String.Empty;
            textCEP.Text = String.Empty;
            textTelefone.Text = String.Empty;
            textCelular.Text = String.Empty;
            textPis.Text = String.Empty;
            textEmail.Text = String.Empty;
        }

        protected void btFechar_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        protected void frmContatoIncluir_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter || e.KeyCode == Keys.Return)
                SendKeys.Send("{TAB}");
        }

        protected Boolean validaCampos()
        {
            Boolean retorno = true;
            try
            {
                if (cbTipoContato.SelectedIndex == 0)
                    throw new Exception("Voce deve selecionar um tipo de contato.");

                if (String.IsNullOrEmpty(textNome.Text.Trim()))
                    throw new Exception("O nome e obrigatorio.");

            }
            catch (Exception ex)
            {
                retorno = false;
                MessageBox.Show(ex.Message, "Atençao", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }

            return retorno;
        }

        protected virtual void btCidade_Click(object sender, EventArgs e)
        {
            try
            {
                if (cbUF.SelectedIndex == 0)
                    throw new Exception("Voce deve selecionar primeiro o estado.");

                frmCidadePesquisa cidadePesquisa = new frmCidadePesquisa(((SelectItem)cbUF.SelectedItem).Valor.ToString());
                cidadePesquisa.ShowDialog();

                if (cidadePesquisa.Cidade != null)
                {
                    cidade = cidadePesquisa.Cidade;
                    textCidade.Text = cidade.Nome;
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atençao", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
        }
    }
}
