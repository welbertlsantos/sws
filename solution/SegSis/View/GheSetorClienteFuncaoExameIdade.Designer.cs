﻿namespace SWS.View
{
    partial class frm_GheSetorClienteFuncaoExameIdade
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.grb_informativo = new System.Windows.Forms.GroupBox();
            this.lbl_exame = new System.Windows.Forms.Label();
            this.text_exame = new System.Windows.Forms.TextBox();
            this.text_ghe = new System.Windows.Forms.TextBox();
            this.lbl_ghe = new System.Windows.Forms.Label();
            this.lbl_cliente = new System.Windows.Forms.Label();
            this.text_cliente = new System.Windows.Forms.TextBox();
            this.lbl_descricao = new System.Windows.Forms.Label();
            this.text_funcao = new System.Windows.Forms.TextBox();
            this.lbl_idade_minima = new System.Windows.Forms.Label();
            this.text_idade_minima = new System.Windows.Forms.TextBox();
            this.grb_dados = new System.Windows.Forms.GroupBox();
            this.btn_fechar = new System.Windows.Forms.Button();
            this.btn_ok = new System.Windows.Forms.Button();
            this.grb_acao = new System.Windows.Forms.GroupBox();
            this.grb_informativo.SuspendLayout();
            this.grb_acao.SuspendLayout();
            this.SuspendLayout();
            // 
            // grb_informativo
            // 
            this.grb_informativo.Controls.Add(this.lbl_exame);
            this.grb_informativo.Controls.Add(this.text_exame);
            this.grb_informativo.Controls.Add(this.text_ghe);
            this.grb_informativo.Controls.Add(this.lbl_ghe);
            this.grb_informativo.Controls.Add(this.lbl_cliente);
            this.grb_informativo.Controls.Add(this.text_cliente);
            this.grb_informativo.Controls.Add(this.lbl_descricao);
            this.grb_informativo.Controls.Add(this.text_funcao);
            this.grb_informativo.Location = new System.Drawing.Point(8, 1);
            this.grb_informativo.Name = "grb_informativo";
            this.grb_informativo.Size = new System.Drawing.Size(564, 125);
            this.grb_informativo.TabIndex = 10;
            this.grb_informativo.TabStop = false;
            this.grb_informativo.Text = "Informações";
            // 
            // lbl_exame
            // 
            this.lbl_exame.AutoSize = true;
            this.lbl_exame.Location = new System.Drawing.Point(3, 95);
            this.lbl_exame.Name = "lbl_exame";
            this.lbl_exame.Size = new System.Drawing.Size(39, 13);
            this.lbl_exame.TabIndex = 11;
            this.lbl_exame.Text = "Exame";
            // 
            // text_exame
            // 
            this.text_exame.BackColor = System.Drawing.Color.White;
            this.text_exame.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.text_exame.Enabled = false;
            this.text_exame.ForeColor = System.Drawing.Color.Black;
            this.text_exame.Location = new System.Drawing.Point(85, 93);
            this.text_exame.MaxLength = 100;
            this.text_exame.Name = "text_exame";
            this.text_exame.Size = new System.Drawing.Size(457, 20);
            this.text_exame.TabIndex = 10;
            // 
            // text_ghe
            // 
            this.text_ghe.BackColor = System.Drawing.Color.White;
            this.text_ghe.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.text_ghe.Enabled = false;
            this.text_ghe.ForeColor = System.Drawing.Color.Black;
            this.text_ghe.Location = new System.Drawing.Point(85, 46);
            this.text_ghe.MaxLength = 100;
            this.text_ghe.Name = "text_ghe";
            this.text_ghe.ReadOnly = true;
            this.text_ghe.Size = new System.Drawing.Size(457, 20);
            this.text_ghe.TabIndex = 9;
            // 
            // lbl_ghe
            // 
            this.lbl_ghe.AutoSize = true;
            this.lbl_ghe.Location = new System.Drawing.Point(3, 48);
            this.lbl_ghe.Name = "lbl_ghe";
            this.lbl_ghe.Size = new System.Drawing.Size(30, 13);
            this.lbl_ghe.TabIndex = 8;
            this.lbl_ghe.Text = "GHE";
            // 
            // lbl_cliente
            // 
            this.lbl_cliente.AutoSize = true;
            this.lbl_cliente.Location = new System.Drawing.Point(3, 25);
            this.lbl_cliente.Name = "lbl_cliente";
            this.lbl_cliente.Size = new System.Drawing.Size(39, 13);
            this.lbl_cliente.TabIndex = 7;
            this.lbl_cliente.Text = "Cliente";
            // 
            // text_cliente
            // 
            this.text_cliente.BackColor = System.Drawing.Color.White;
            this.text_cliente.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.text_cliente.Enabled = false;
            this.text_cliente.ForeColor = System.Drawing.Color.Black;
            this.text_cliente.Location = new System.Drawing.Point(85, 23);
            this.text_cliente.MaxLength = 100;
            this.text_cliente.Name = "text_cliente";
            this.text_cliente.ReadOnly = true;
            this.text_cliente.Size = new System.Drawing.Size(457, 20);
            this.text_cliente.TabIndex = 6;
            // 
            // lbl_descricao
            // 
            this.lbl_descricao.AutoSize = true;
            this.lbl_descricao.Location = new System.Drawing.Point(3, 71);
            this.lbl_descricao.Name = "lbl_descricao";
            this.lbl_descricao.Size = new System.Drawing.Size(43, 13);
            this.lbl_descricao.TabIndex = 3;
            this.lbl_descricao.Text = "Função";
            // 
            // text_funcao
            // 
            this.text_funcao.BackColor = System.Drawing.Color.White;
            this.text_funcao.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.text_funcao.Enabled = false;
            this.text_funcao.ForeColor = System.Drawing.Color.Black;
            this.text_funcao.Location = new System.Drawing.Point(85, 69);
            this.text_funcao.MaxLength = 100;
            this.text_funcao.Name = "text_funcao";
            this.text_funcao.Size = new System.Drawing.Size(457, 20);
            this.text_funcao.TabIndex = 1;
            // 
            // lbl_idade_minima
            // 
            this.lbl_idade_minima.AutoSize = true;
            this.lbl_idade_minima.Location = new System.Drawing.Point(12, 154);
            this.lbl_idade_minima.Name = "lbl_idade_minima";
            this.lbl_idade_minima.Size = new System.Drawing.Size(72, 13);
            this.lbl_idade_minima.TabIndex = 12;
            this.lbl_idade_minima.Text = "Idade Mínima";
            // 
            // text_idade_minima
            // 
            this.text_idade_minima.BackColor = System.Drawing.Color.White;
            this.text_idade_minima.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.text_idade_minima.ForeColor = System.Drawing.Color.Black;
            this.text_idade_minima.Location = new System.Drawing.Point(93, 152);
            this.text_idade_minima.MaxLength = 2;
            this.text_idade_minima.Name = "text_idade_minima";
            this.text_idade_minima.Size = new System.Drawing.Size(55, 20);
            this.text_idade_minima.TabIndex = 13;
            this.text_idade_minima.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.text_idade_minima_KeyPress);
            // 
            // grb_dados
            // 
            this.grb_dados.Location = new System.Drawing.Point(8, 134);
            this.grb_dados.Name = "grb_dados";
            this.grb_dados.Size = new System.Drawing.Size(564, 49);
            this.grb_dados.TabIndex = 16;
            this.grb_dados.TabStop = false;
            this.grb_dados.Text = "Dados";
            // 
            // btn_fechar
            // 
            this.btn_fechar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_fechar.Image = global::SWS.Properties.Resources.close;
            this.btn_fechar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btn_fechar.Location = new System.Drawing.Point(67, 16);
            this.btn_fechar.Name = "btn_fechar";
            this.btn_fechar.Size = new System.Drawing.Size(51, 23);
            this.btn_fechar.TabIndex = 15;
            this.btn_fechar.Text = "&Fechar";
            this.btn_fechar.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btn_fechar.UseVisualStyleBackColor = true;
            this.btn_fechar.Click += new System.EventHandler(this.btn_fechar_Click);
            // 
            // btn_ok
            // 
            this.btn_ok.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_ok.Image = global::SWS.Properties.Resources.fechar_ico;
            this.btn_ok.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btn_ok.Location = new System.Drawing.Point(6, 16);
            this.btn_ok.Name = "btn_ok";
            this.btn_ok.Size = new System.Drawing.Size(55, 23);
            this.btn_ok.TabIndex = 14;
            this.btn_ok.Text = "&Ok";
            this.btn_ok.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btn_ok.UseVisualStyleBackColor = true;
            this.btn_ok.Click += new System.EventHandler(this.btn_ok_Click);
            // 
            // grb_acao
            // 
            this.grb_acao.Controls.Add(this.btn_fechar);
            this.grb_acao.Controls.Add(this.btn_ok);
            this.grb_acao.Location = new System.Drawing.Point(8, 190);
            this.grb_acao.Name = "grb_acao";
            this.grb_acao.Size = new System.Drawing.Size(564, 45);
            this.grb_acao.TabIndex = 17;
            this.grb_acao.TabStop = false;
            this.grb_acao.Text = "Ações";
            // 
            // frm_GheSetorClienteFuncaoExameIdade
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(584, 236);
            this.ControlBox = false;
            this.Controls.Add(this.grb_acao);
            this.Controls.Add(this.text_idade_minima);
            this.Controls.Add(this.lbl_idade_minima);
            this.Controls.Add(this.grb_informativo);
            this.Controls.Add(this.grb_dados);
            this.Name = "frm_GheSetorClienteFuncaoExameIdade";
            this.Text = "IDADE MÍNIMA EXAME POR FUNÇÃO";
            this.grb_informativo.ResumeLayout(false);
            this.grb_informativo.PerformLayout();
            this.grb_acao.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.GroupBox grb_informativo;
        private System.Windows.Forms.Label lbl_exame;
        private System.Windows.Forms.TextBox text_exame;
        private System.Windows.Forms.TextBox text_ghe;
        private System.Windows.Forms.Label lbl_ghe;
        private System.Windows.Forms.Label lbl_cliente;
        private System.Windows.Forms.TextBox text_cliente;
        private System.Windows.Forms.Label lbl_descricao;
        private System.Windows.Forms.TextBox text_funcao;
        private System.Windows.Forms.Label lbl_idade_minima;
        private System.Windows.Forms.TextBox text_idade_minima;
        private System.Windows.Forms.GroupBox grb_dados;
        private System.Windows.Forms.Button btn_fechar;
        private System.Windows.Forms.Button btn_ok;
        private System.Windows.Forms.GroupBox grb_acao;
    }
}