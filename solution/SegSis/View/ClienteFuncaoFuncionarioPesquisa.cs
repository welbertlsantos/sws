﻿using SWS.Entidade;
using SWS.Facade;
using SWS.View.ViewHelper;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace SWS.View
{
    public partial class frmClienteFuncaoFuncionarioPesquisa : frmTemplateConsulta
    {
        private ClienteFuncaoFuncionario clienteFuncaoFuncionario;

        public ClienteFuncaoFuncionario ClienteFuncaoFuncionario
        {
            get { return clienteFuncaoFuncionario; }
            set { clienteFuncaoFuncionario = value; }
        }

        private Cliente cliente;
        
        public frmClienteFuncaoFuncionarioPesquisa(Cliente cliente)
        {
            InitializeComponent();
            this.cliente = cliente;
        }

        private void btnFechar_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnConfirmar_Click(object sender, EventArgs e)
        {
            try
            {
                if (dgvFuncionario.CurrentRow == null)
                    throw new Exception("Selecione uma linha");

                AsoFacade asoFacade = AsoFacade.getInstance();

                this.ClienteFuncaoFuncionario = asoFacade.findClienteFuncaoFuncionarioById((long)dgvFuncionario.CurrentRow.Cells[0].Value);
                this.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }

        private void btnPesquisar_Click(object sender, EventArgs e)
        {
            try
            {
                montaGrid();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }
        
        private void montaGrid() 
        {
            try
            {
                this.Cursor = Cursors.WaitCursor;
                this.dgvFuncionario.Columns.Clear();

                this.dgvFuncionario.ColumnCount = 7;

                this.dgvFuncionario.Columns[0].HeaderText = "idClienteFuncaofuncionario";
                this.dgvFuncionario.Columns[0].Visible = false;

                this.dgvFuncionario.Columns[1].HeaderText = "idFuncionario";
                this.dgvFuncionario.Columns[1].Visible = false;

                this.dgvFuncionario.Columns[2].HeaderText = "Funcionario";
                this.dgvFuncionario.Columns[3].HeaderText = "CPF";
                this.dgvFuncionario.Columns[4].HeaderText = "Data Nascimento";
                this.dgvFuncionario.Columns[5].HeaderText = "Função";
                this.dgvFuncionario.Columns[6].HeaderText = "Matricula";

                FuncionarioFacade funcionarioFacade = FuncionarioFacade.getInstance();
                ClienteFuncaoFuncionario clienteFuncaoFuncionarioPesquisar = new ClienteFuncaoFuncionario();
                clienteFuncaoFuncionarioPesquisar.ClienteFuncao = new ClienteFuncao(null, cliente, null, "A", null, null);
                Funcionario funcionario = new Funcionario();
                funcionario.Nome = textNome.Text.Trim();
                clienteFuncaoFuncionarioPesquisar.Funcionario = funcionario;

                foreach (ClienteFuncaoFuncionario cf in funcionarioFacade.findAllFuncionarioByClienteFuncaoFuncionario(clienteFuncaoFuncionarioPesquisar))
                    dgvFuncionario.Rows.Add(
                        cf.Id,
                        cf.Funcionario.Id,
                        cf.Funcionario.Nome,
                        ValidaCampoHelper.FormataCpf(cf.Funcionario.Cpf),
                        string.Format("{0:dd/MM/yyyy}", cf.Funcionario.DataNascimento),
                        cf.ClienteFuncao.Funcao.Descricao,
                        cf.Matricula);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }

        }

        private void dgvFuncionario_CellContentDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            btnConfirmar.PerformClick();
        }
    }
}
