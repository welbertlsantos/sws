﻿namespace SWS.View
{
    partial class frmPlanoIncluir
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmPlanoIncluir));
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            this.flpAcao = new System.Windows.Forms.FlowLayoutPanel();
            this.btnGravar = new System.Windows.Forms.Button();
            this.btnLimpar = new System.Windows.Forms.Button();
            this.btnFechar = new System.Windows.Forms.Button();
            this.lblPlano = new System.Windows.Forms.TextBox();
            this.textDescricao = new System.Windows.Forms.TextBox();
            this.grbParcela = new System.Windows.Forms.GroupBox();
            this.dgvParcela = new System.Windows.Forms.DataGridView();
            this.btnExcluirParcela = new System.Windows.Forms.Button();
            this.btnIncluirParcela = new System.Windows.Forms.Button();
            this.flpParcela = new System.Windows.Forms.FlowLayoutPanel();
            this.grb_forma = new System.Windows.Forms.GroupBox();
            this.dgvForma = new System.Windows.Forms.DataGridView();
            this.btn_excluir = new System.Windows.Forms.Button();
            this.btn_incluirForma = new System.Windows.Forms.Button();
            this.flpForma = new System.Windows.Forms.FlowLayoutPanel();
            this.btnAlterar = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.spForm)).BeginInit();
            this.spForm.Panel1.SuspendLayout();
            this.spForm.Panel2.SuspendLayout();
            this.spForm.SuspendLayout();
            this.pnlForm.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.banner)).BeginInit();
            this.flpAcao.SuspendLayout();
            this.grbParcela.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvParcela)).BeginInit();
            this.flpParcela.SuspendLayout();
            this.grb_forma.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvForma)).BeginInit();
            this.flpForma.SuspendLayout();
            this.SuspendLayout();
            // 
            // spForm
            // 
            // 
            // spForm.Panel1
            // 
            this.spForm.Panel1.Controls.Add(this.flpAcao);
            // 
            // pnlForm
            // 
            this.pnlForm.Controls.Add(this.flpForma);
            this.pnlForm.Controls.Add(this.grb_forma);
            this.pnlForm.Controls.Add(this.flpParcela);
            this.pnlForm.Controls.Add(this.grbParcela);
            this.pnlForm.Controls.Add(this.textDescricao);
            this.pnlForm.Controls.Add(this.lblPlano);
            // 
            // flpAcao
            // 
            this.flpAcao.Controls.Add(this.btnGravar);
            this.flpAcao.Controls.Add(this.btnLimpar);
            this.flpAcao.Controls.Add(this.btnFechar);
            this.flpAcao.Dock = System.Windows.Forms.DockStyle.Fill;
            this.flpAcao.Location = new System.Drawing.Point(0, 0);
            this.flpAcao.Name = "flpAcao";
            this.flpAcao.Size = new System.Drawing.Size(784, 35);
            this.flpAcao.TabIndex = 0;
            // 
            // btnGravar
            // 
            this.btnGravar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnGravar.Image = ((System.Drawing.Image)(resources.GetObject("btnGravar.Image")));
            this.btnGravar.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnGravar.Location = new System.Drawing.Point(3, 3);
            this.btnGravar.Name = "btnGravar";
            this.btnGravar.Size = new System.Drawing.Size(75, 23);
            this.btnGravar.TabIndex = 16;
            this.btnGravar.Text = "&Gravar";
            this.btnGravar.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnGravar.UseVisualStyleBackColor = true;
            this.btnGravar.Click += new System.EventHandler(this.btnGravar_Click);
            // 
            // btnLimpar
            // 
            this.btnLimpar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnLimpar.Image = ((System.Drawing.Image)(resources.GetObject("btnLimpar.Image")));
            this.btnLimpar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnLimpar.Location = new System.Drawing.Point(84, 3);
            this.btnLimpar.Name = "btnLimpar";
            this.btnLimpar.Size = new System.Drawing.Size(75, 23);
            this.btnLimpar.TabIndex = 18;
            this.btnLimpar.TabStop = false;
            this.btnLimpar.Text = "&Limpar";
            this.btnLimpar.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnLimpar.UseVisualStyleBackColor = true;
            this.btnLimpar.Click += new System.EventHandler(this.btnLimpar_Click);
            // 
            // btnFechar
            // 
            this.btnFechar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnFechar.Image = ((System.Drawing.Image)(resources.GetObject("btnFechar.Image")));
            this.btnFechar.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnFechar.Location = new System.Drawing.Point(165, 3);
            this.btnFechar.Name = "btnFechar";
            this.btnFechar.Size = new System.Drawing.Size(75, 23);
            this.btnFechar.TabIndex = 17;
            this.btnFechar.Text = "&Fechar";
            this.btnFechar.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnFechar.UseVisualStyleBackColor = true;
            this.btnFechar.Click += new System.EventHandler(this.btnFechar_Click);
            // 
            // lblPlano
            // 
            this.lblPlano.BackColor = System.Drawing.Color.LightGray;
            this.lblPlano.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblPlano.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPlano.Location = new System.Drawing.Point(12, 14);
            this.lblPlano.Name = "lblPlano";
            this.lblPlano.ReadOnly = true;
            this.lblPlano.Size = new System.Drawing.Size(147, 21);
            this.lblPlano.TabIndex = 0;
            this.lblPlano.TabStop = false;
            this.lblPlano.Text = "Descrição";
            // 
            // textDescricao
            // 
            this.textDescricao.AcceptsReturn = true;
            this.textDescricao.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textDescricao.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.textDescricao.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textDescricao.Location = new System.Drawing.Point(158, 14);
            this.textDescricao.MaxLength = 100;
            this.textDescricao.Name = "textDescricao";
            this.textDescricao.Size = new System.Drawing.Size(587, 21);
            this.textDescricao.TabIndex = 1;
            // 
            // grbParcela
            // 
            this.grbParcela.BackColor = System.Drawing.Color.White;
            this.grbParcela.Controls.Add(this.dgvParcela);
            this.grbParcela.Location = new System.Drawing.Point(12, 41);
            this.grbParcela.Name = "grbParcela";
            this.grbParcela.Size = new System.Drawing.Size(228, 159);
            this.grbParcela.TabIndex = 18;
            this.grbParcela.TabStop = false;
            this.grbParcela.Text = "Parcelas";
            // 
            // dgvParcela
            // 
            this.dgvParcela.AllowUserToAddRows = false;
            this.dgvParcela.AllowUserToDeleteRows = false;
            this.dgvParcela.AllowUserToResizeColumns = false;
            this.dgvParcela.AllowUserToResizeRows = false;
            this.dgvParcela.BackgroundColor = System.Drawing.Color.White;
            this.dgvParcela.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle3.BackColor = System.Drawing.Color.LightGray;
            dataGridViewCellStyle3.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvParcela.DefaultCellStyle = dataGridViewCellStyle3;
            this.dgvParcela.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvParcela.Location = new System.Drawing.Point(3, 16);
            this.dgvParcela.Name = "dgvParcela";
            this.dgvParcela.ReadOnly = true;
            this.dgvParcela.RowHeadersVisible = false;
            this.dgvParcela.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.CellSelect;
            this.dgvParcela.Size = new System.Drawing.Size(222, 140);
            this.dgvParcela.TabIndex = 2;
            this.dgvParcela.ColumnAdded += new System.Windows.Forms.DataGridViewColumnEventHandler(this.dgvParcela_ColumnAdded);
            // 
            // btnExcluirParcela
            // 
            this.btnExcluirParcela.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnExcluirParcela.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnExcluirParcela.Image = ((System.Drawing.Image)(resources.GetObject("btnExcluirParcela.Image")));
            this.btnExcluirParcela.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnExcluirParcela.Location = new System.Drawing.Point(145, 3);
            this.btnExcluirParcela.Name = "btnExcluirParcela";
            this.btnExcluirParcela.Size = new System.Drawing.Size(65, 21);
            this.btnExcluirParcela.TabIndex = 4;
            this.btnExcluirParcela.Text = "E&xcluir";
            this.btnExcluirParcela.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnExcluirParcela.UseVisualStyleBackColor = true;
            this.btnExcluirParcela.Click += new System.EventHandler(this.btn_excluirParcela_Click);
            // 
            // btnIncluirParcela
            // 
            this.btnIncluirParcela.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnIncluirParcela.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnIncluirParcela.Image = ((System.Drawing.Image)(resources.GetObject("btnIncluirParcela.Image")));
            this.btnIncluirParcela.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnIncluirParcela.Location = new System.Drawing.Point(3, 3);
            this.btnIncluirParcela.Name = "btnIncluirParcela";
            this.btnIncluirParcela.Size = new System.Drawing.Size(65, 21);
            this.btnIncluirParcela.TabIndex = 3;
            this.btnIncluirParcela.Text = "I&ncluir";
            this.btnIncluirParcela.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnIncluirParcela.UseVisualStyleBackColor = true;
            this.btnIncluirParcela.Click += new System.EventHandler(this.btn_incluirParcela_Click);
            // 
            // flpParcela
            // 
            this.flpParcela.Controls.Add(this.btnIncluirParcela);
            this.flpParcela.Controls.Add(this.btnAlterar);
            this.flpParcela.Controls.Add(this.btnExcluirParcela);
            this.flpParcela.Location = new System.Drawing.Point(12, 204);
            this.flpParcela.Name = "flpParcela";
            this.flpParcela.Size = new System.Drawing.Size(228, 28);
            this.flpParcela.TabIndex = 19;
            // 
            // grb_forma
            // 
            this.grb_forma.Controls.Add(this.dgvForma);
            this.grb_forma.Location = new System.Drawing.Point(246, 41);
            this.grb_forma.Name = "grb_forma";
            this.grb_forma.Size = new System.Drawing.Size(499, 159);
            this.grb_forma.TabIndex = 20;
            this.grb_forma.TabStop = false;
            this.grb_forma.Text = "Forma de Pagamento";
            // 
            // dgvForma
            // 
            this.dgvForma.AllowUserToAddRows = false;
            this.dgvForma.AllowUserToDeleteRows = false;
            this.dgvForma.AllowUserToOrderColumns = true;
            this.dgvForma.AllowUserToResizeRows = false;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.Gainsboro;
            this.dgvForma.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle1;
            this.dgvForma.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this.dgvForma.BackgroundColor = System.Drawing.Color.White;
            this.dgvForma.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.Color.LightGray;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvForma.DefaultCellStyle = dataGridViewCellStyle2;
            this.dgvForma.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvForma.Location = new System.Drawing.Point(3, 16);
            this.dgvForma.MultiSelect = false;
            this.dgvForma.Name = "dgvForma";
            this.dgvForma.ReadOnly = true;
            this.dgvForma.Size = new System.Drawing.Size(493, 140);
            this.dgvForma.TabIndex = 5;
            this.dgvForma.ColumnAdded += new System.Windows.Forms.DataGridViewColumnEventHandler(this.dgvForma_ColumnAdded);
            // 
            // btn_excluir
            // 
            this.btn_excluir.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_excluir.Image = ((System.Drawing.Image)(resources.GetObject("btn_excluir.Image")));
            this.btn_excluir.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btn_excluir.Location = new System.Drawing.Point(84, 3);
            this.btn_excluir.Name = "btn_excluir";
            this.btn_excluir.Size = new System.Drawing.Size(75, 23);
            this.btn_excluir.TabIndex = 7;
            this.btn_excluir.Text = "&Excluir";
            this.btn_excluir.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btn_excluir.UseVisualStyleBackColor = true;
            this.btn_excluir.Click += new System.EventHandler(this.btn_excluir_Click);
            // 
            // btn_incluirForma
            // 
            this.btn_incluirForma.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_incluirForma.Image = ((System.Drawing.Image)(resources.GetObject("btn_incluirForma.Image")));
            this.btn_incluirForma.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btn_incluirForma.Location = new System.Drawing.Point(3, 3);
            this.btn_incluirForma.Name = "btn_incluirForma";
            this.btn_incluirForma.Size = new System.Drawing.Size(75, 23);
            this.btn_incluirForma.TabIndex = 6;
            this.btn_incluirForma.Text = "&Incluir";
            this.btn_incluirForma.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btn_incluirForma.UseVisualStyleBackColor = true;
            this.btn_incluirForma.Click += new System.EventHandler(this.btn_incluirForma_Click);
            // 
            // flpForma
            // 
            this.flpForma.Controls.Add(this.btn_incluirForma);
            this.flpForma.Controls.Add(this.btn_excluir);
            this.flpForma.Location = new System.Drawing.Point(246, 202);
            this.flpForma.Name = "flpForma";
            this.flpForma.Size = new System.Drawing.Size(499, 30);
            this.flpForma.TabIndex = 21;
            // 
            // btnAlterar
            // 
            this.btnAlterar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnAlterar.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAlterar.Image = global::SWS.Properties.Resources.Alterar;
            this.btnAlterar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnAlterar.Location = new System.Drawing.Point(74, 3);
            this.btnAlterar.Name = "btnAlterar";
            this.btnAlterar.Size = new System.Drawing.Size(65, 21);
            this.btnAlterar.TabIndex = 5;
            this.btnAlterar.Text = "&Alterar";
            this.btnAlterar.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnAlterar.UseVisualStyleBackColor = true;
            this.btnAlterar.Click += new System.EventHandler(this.btnAlterar_Click);
            // 
            // frmPlanoIncluir
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(784, 561);
            this.Name = "frmPlanoIncluir";
            this.Text = "INCLUIR PLANO DE PAGAMENTO";
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.frmPlanoIncluir_KeyDown);
            this.spForm.Panel1.ResumeLayout(false);
            this.spForm.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.spForm)).EndInit();
            this.spForm.ResumeLayout(false);
            this.pnlForm.ResumeLayout(false);
            this.pnlForm.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.banner)).EndInit();
            this.flpAcao.ResumeLayout(false);
            this.grbParcela.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvParcela)).EndInit();
            this.flpParcela.ResumeLayout(false);
            this.grb_forma.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvForma)).EndInit();
            this.flpForma.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        protected System.Windows.Forms.FlowLayoutPanel flpAcao;
        protected System.Windows.Forms.TextBox lblPlano;
        protected System.Windows.Forms.Button btnFechar;
        protected System.Windows.Forms.Button btnGravar;
        protected System.Windows.Forms.Button btnLimpar;
        protected System.Windows.Forms.TextBox textDescricao;
        protected System.Windows.Forms.FlowLayoutPanel flpParcela;
        protected System.Windows.Forms.Button btnIncluirParcela;
        protected System.Windows.Forms.Button btnExcluirParcela;
        protected System.Windows.Forms.GroupBox grbParcela;
        protected System.Windows.Forms.DataGridView dgvParcela;
        protected System.Windows.Forms.GroupBox grb_forma;
        protected System.Windows.Forms.DataGridView dgvForma;
        protected System.Windows.Forms.Button btn_excluir;
        protected System.Windows.Forms.Button btn_incluirForma;
        protected System.Windows.Forms.FlowLayoutPanel flpForma;
        protected System.Windows.Forms.Button btnAlterar;

    }
}