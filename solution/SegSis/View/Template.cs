﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace SWS.View
{
    public partial class frmTemplate : Form
    {
        public System.Windows.Forms.SplitContainer spForm;
        public System.Windows.Forms.Panel pnlForm;
        public System.Windows.Forms.PictureBox banner;
        
        public frmTemplate()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmTemplate));
            this.spForm = new System.Windows.Forms.SplitContainer();
            this.banner = new System.Windows.Forms.PictureBox();
            this.pnlForm = new System.Windows.Forms.Panel();
            ((System.ComponentModel.ISupportInitialize)(this.spForm)).BeginInit();
            this.spForm.Panel2.SuspendLayout();
            this.spForm.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.banner)).BeginInit();
            this.SuspendLayout();
            // 
            // spForm
            // 
            //this.spForm.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
            //            | System.Windows.Forms.AnchorStyles.Left)
            //            | System.Windows.Forms.AnchorStyles.Right)));
            this.spForm.Location = new System.Drawing.Point(0, 58);
            this.spForm.Name = "spForm";
            this.spForm.Orientation = System.Windows.Forms.Orientation.Horizontal;
            this.spForm.TabStop = false;
            this.spForm.IsSplitterFixed = true;
            // 
            // spForm.Panel2
            // 
            this.spForm.Panel2.AutoScroll = true;
            this.spForm.Panel2.Controls.Add(this.pnlForm);
            this.spForm.Size = new System.Drawing.Size(784, 503);
            this.spForm.SplitterDistance = 35;
            this.spForm.TabIndex = 0;
            // 
            // banner
            // 
            //this.banner.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
            //            | System.Windows.Forms.AnchorStyles.Right)));
            this.banner.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.banner.Image = global::SWS.Properties.Resources.banner1;
            this.banner.Location = new System.Drawing.Point(0, 0);
            this.banner.Name = "banner";
            this.banner.Size = new System.Drawing.Size(784, 50);
            this.banner.TabIndex = 1;
            this.banner.TabStop = false;
            // 
            // pnlForm
            // 
            //this.pnlForm.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
            //            | System.Windows.Forms.AnchorStyles.Left)
            //            | System.Windows.Forms.AnchorStyles.Right)));
            this.pnlForm.Location = new System.Drawing.Point(0, 0);
            this.pnlForm.Name = "pnlForm";
            this.pnlForm.Size = new System.Drawing.Size(764, 480);
            this.pnlForm.TabIndex = 0;
            // 
            // frmTemplate
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.Control;
            this.ClientSize = new System.Drawing.Size(784, 561);
            this.MaximizeBox = false;
            this.Controls.Add(this.banner);
            this.Controls.Add(this.spForm);
            this.KeyPreview = true;
            this.Name = "frmTemplate";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Template";
            this.spForm.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.spForm)).EndInit();
            this.spForm.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.banner)).EndInit();
            this.ResumeLayout(false);
            this.Icon = ((System.Drawing.Icon)(SWS.Properties.Resources.icone));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
        }

        private void InitializeComponent()
        {
            this.SuspendLayout();
            // 
            // frmTemplate
            // 
            this.ClientSize = new System.Drawing.Size(284, 261);
            this.Name = "frmTemplate";
            this.ResumeLayout(false);

        }
    }
}
