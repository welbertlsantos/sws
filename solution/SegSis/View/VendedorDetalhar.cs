﻿using SWS.Entidade;
using SWS.View.ViewHelper;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace SWS.View
{
    public partial class frmVendedorDetalhar : frmVendedorIncluir
    {
        public frmVendedorDetalhar(Vendedor vendedor) :base()
        {
            InitializeComponent();
            btnGravar.Visible = false;
            btnLimpar.Visible = false;

            this.Vendedor = vendedor;

            ComboHelper.unidadeFederativa(cbUf);

            textNome.Text = vendedor.Nome;
            textNome.ReadOnly = true;

            textCpf.Text = vendedor.Cpf;
            textCpf.ReadOnly = true;

            textRG.Text = vendedor.Rg;
            textRG.ReadOnly = true;
            
            textEndereco.Text = vendedor.Endereco;
            textEndereco.ReadOnly = true;

            textNumero.Text = vendedor.Numero;
            textNumero.ReadOnly = true;

            textComplemento.Text = vendedor.Complemento;
            textComplemento.ReadOnly = true;
            
            textBairro.Text = vendedor.Bairro;
            textBairro.ReadOnly = true;
            
            cbUf.SelectedValue = vendedor.Uf;
            cbUf.Enabled = false;

            this.cidade = vendedor.CidadeIbge;
            btnCidade.Enabled = false;

            textCidade.Text = cidade != null ? cidade.Nome : string.Empty;
            textCidade.ReadOnly = true;

            textCep.Text = vendedor.Cep;
            textCep.ReadOnly = true;
            
            textEmail.Text = vendedor.Email;
            textEmail.ReadOnly = true;
            
            textTelefone.Text = vendedor.Telefone1;
            textTelefone.ReadOnly = true;
            
            textCelular.Text = vendedor.Telefone2;
            textCelular.ReadOnly = true;
            
            textComissao.Text = Convert.ToString(vendedor.Comissao);
            textComissao.ReadOnly = true;
        }

        protected override void textComissao_Enter(object sender, EventArgs e) {  } 
    }
}
