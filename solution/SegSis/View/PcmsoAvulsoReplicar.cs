﻿using SWS.Entidade;
using SWS.Facade;
using SWS.View.Resources;
using SWS.View.ViewHelper;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace SWS.View
{
    public partial class frmPcmsoAvulsoReplicar : frmTemplateConsulta
    {
        private Estudo pcmsoReplicado;

        public Estudo PcmsoReplicado
        {
            get { return pcmsoReplicado; }
            set { pcmsoReplicado = value; }
        }
        
        
        private Estudo pcmso;

        private Cliente cliente;
        private Cliente contratante;
        private VendedorCliente vendedorCliente;
        
        public frmPcmsoAvulsoReplicar(Estudo pcmso)
        {
            InitializeComponent();
            this.pcmso = pcmso;
        }

        private void btnContratante_Click(object sender, EventArgs e)
        {
            try
            {
                frmClienteSelecionar selecionarContratante = new frmClienteSelecionar(null, null, null, null, null, false, null, false, true);
                selecionarContratante.ShowDialog();

                if (selecionarContratante.Cliente != null)
                {
                    if (selecionarContratante.Cliente == cliente)
                        throw new Exception("O cliente não pode ser o contratante. Selecione outro cliente.");

                    contratante = selecionarContratante.Cliente;
                    textContratante.Text = contratante.RazaoSocial + " / " + contratante.Fantasia;
                   
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }

        private void btnReplicar_Click(object sender, EventArgs e)
        {
            try
            {
                /* retirada a obrigação de somente existir um pcmso avulso por cliente. 
                 * sprint 01 - 19/05/2019 */
                
                this.Cursor = Cursors.WaitCursor;

                EstudoFacade pcmsoFacade = EstudoFacade.getInstance();

                /* iniciando replicação do estudo */
                pcmso.VendedorCliente = vendedorCliente;
                pcmso.ClienteContratante = contratante;
                pcmso.DataCriacao = DateTime.Now;
                pcmsoReplicado = pcmsoFacade.replicarPcmsoAvulso(pcmso);
                MessageBox.Show("PCMSO replicado com sucesso.", "Confirmação", MessageBoxButtons.OK, MessageBoxIcon.Information);
                this.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }

        private bool verificaPodeReplicarEstudo()
        {
            bool result = true;

            try
            {
                EstudoFacade pcmsoFacade = EstudoFacade.getInstance();
                pcmso.VendedorCliente = vendedorCliente;
                pcmso.ClienteContratante = contratante;
                pcmso.DataCriacao = null;
                DataSet ds = pcmsoFacade.findByFilter(pcmso, null);

                if (ds.Tables[0].Rows.Count > 0)
                    throw new Exception("Já  existe uma PCMSO nessas condições. Refaça sua pesquisa.");

            }
            catch (Exception ex)
            {
                result = false;
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

            return result;
        }

        private void button2_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnCliente_Click(object sender, EventArgs e)
        {
            try
            {
                frmClienteSelecionar selecionarCliente = new frmClienteSelecionar(null, null, false, null, null, false, null, null, true);
                selecionarCliente.ShowDialog();

                ClienteFacade clienteFacade = ClienteFacade.getInstance();

                if (selecionarCliente.Cliente != null)
                {
                    cliente = selecionarCliente.Cliente;
                    textCliente.Text = cliente.RazaoSocial;
                    List<VendedorCliente> vendedorCliente = clienteFacade.findClienteVendedorByCliente(cliente);
                    this.vendedorCliente = vendedorCliente.Find(x => string.Equals(x.Situacao, ApplicationConstants.ATIVO));
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
    }
}
