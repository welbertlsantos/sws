﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using SWS.Facade;
using SWS.Entidade;
using SWS.View.Resources;
using SWS.View.ViewHelper;

namespace SWS.View
{
    public partial class frm_ppraAgenteBuscar : BaseFormConsulta
    {
        
        private static String Msg01 = " Selecione um linha";
        private static string Msg02 = " Atenção";

        private Agente agente;
        public Agente getAgente()
        {
            return this.agente;
        }

        private GheFonte gheFonte;

        public frm_ppraAgenteBuscar(GheFonte gheFonte)
        {
            InitializeComponent();
            this.gheFonte = gheFonte;
            validaPermissoes();
        }

        private void validaPermissoes()
        {
            PermissionamentoFacade permissionamentoFacade = PermissionamentoFacade.getInstance();
            this.btn_novo.Enabled = permissionamentoFacade.hasPermission("AGENTE", "INCLUIR");
        }

        private void btn_fechar_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btn_novo_Click(object sender, EventArgs e)
        {
            frmAgentesIncluir formAgenteIncluir = new frmAgentesIncluir();
            formAgenteIncluir.ShowDialog();

            if (formAgenteIncluir.Agente != null)
            {
                text_descricao.Text = formAgenteIncluir.Agente.Descricao;
                btn_buscar.PerformClick();
            }
        }

        private void btn_buscar_Click(object sender, EventArgs e)
        {
            try
            {
                this.Cursor = Cursors.WaitCursor;
                this.grd_agente.Columns.Clear();
                montaDataGrid();
                text_descricao.Focus();
            }
            catch (Exception Ex)
            {
                MessageBox.Show(Ex.Message);
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }

        private void btn_ok_Click(object sender, EventArgs e)
        {
            PpraFacade ppraFacade = PpraFacade.getInstance();

            try
            {

                if (grd_agente.CurrentRow != null)
                {

                    agente = ppraFacade.findAgenteById((Int64)grd_agente.CurrentRow.Cells[0].Value);

                    this.Close();

                }
                else
                {
                    MessageBox.Show(Msg01, Msg02);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
        }

        public void montaDataGrid()
        {
            PpraFacade ppraFacade = PpraFacade.getInstance();

            DataSet ds = ppraFacade.findAgenteAtivoByGheFonteAgenteByFilter(new GheFonteAgente(null, null, null, null, gheFonte, new Agente(null, null, text_descricao.Text.ToUpper().Trim(), String.Empty, String.Empty, String.Empty, ApplicationConstants.ATIVO, false, string.Empty, string.Empty, string.Empty), String.Empty, ApplicationConstants.ATIVO));

            grd_agente.DataSource = ds.Tables[0].DefaultView;
                        
            grd_agente.Columns[0].HeaderText = "id_agente";
            grd_agente.Columns[0].Visible = false;

            grd_agente.Columns[1].HeaderText = "Id Risco";
            grd_agente.Columns[1].Visible = false;

            grd_agente.Columns[2].HeaderText = "Descrição";
            
            grd_agente.Columns[3].HeaderText = "Trajetória";

            grd_agente.Columns[4].HeaderText = "Danos";

            grd_agente.Columns[5].HeaderText = "Limite";

            grd_agente.Columns[6].HeaderText = "Risco";
            grd_agente.Columns[6].DisplayIndex = 3;

            
        }
    }
}

