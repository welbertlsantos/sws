﻿namespace SWS.View
{
    partial class frmContratoDuplicarData
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.flowLayoutPanel1 = new System.Windows.Forms.FlowLayoutPanel();
            this.btnConfirmar = new System.Windows.Forms.Button();
            this.btnFechar = new System.Windows.Forms.Button();
            this.lblDataInicio = new System.Windows.Forms.TextBox();
            this.lblDataTermino = new System.Windows.Forms.TextBox();
            this.dataInicio = new System.Windows.Forms.DateTimePicker();
            this.dataTermino = new System.Windows.Forms.DateTimePicker();
            ((System.ComponentModel.ISupportInitialize)(this.scForm)).BeginInit();
            this.scForm.Panel1.SuspendLayout();
            this.scForm.Panel2.SuspendLayout();
            this.scForm.SuspendLayout();
            this.pnlForm.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.banner)).BeginInit();
            this.flowLayoutPanel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // scForm
            // 
            this.scForm.Location = new System.Drawing.Point(0, 47);
            // 
            // scForm.Panel1
            // 
            this.scForm.Panel1.Controls.Add(this.flowLayoutPanel1);
            this.scForm.Panel1MinSize = 30;
            this.scForm.Panel2MinSize = 40;
            this.scForm.Size = new System.Drawing.Size(400, 215);
            this.scForm.SplitterDistance = 35;
            // 
            // pnlForm
            // 
            this.pnlForm.Controls.Add(this.lblDataInicio);
            this.pnlForm.Controls.Add(this.lblDataTermino);
            this.pnlForm.Controls.Add(this.dataInicio);
            this.pnlForm.Controls.Add(this.dataTermino);
            this.pnlForm.Size = new System.Drawing.Size(397, 163);
            // 
            // banner
            // 
            this.banner.Size = new System.Drawing.Size(400, 41);
            // 
            // flowLayoutPanel1
            // 
            this.flowLayoutPanel1.Controls.Add(this.btnConfirmar);
            this.flowLayoutPanel1.Controls.Add(this.btnFechar);
            this.flowLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.flowLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.flowLayoutPanel1.Name = "flowLayoutPanel1";
            this.flowLayoutPanel1.Size = new System.Drawing.Size(400, 35);
            this.flowLayoutPanel1.TabIndex = 0;
            // 
            // btnConfirmar
            // 
            this.btnConfirmar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnConfirmar.Image = global::SWS.Properties.Resources.fechar_ico;
            this.btnConfirmar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnConfirmar.Location = new System.Drawing.Point(3, 3);
            this.btnConfirmar.Name = "btnConfirmar";
            this.btnConfirmar.Size = new System.Drawing.Size(70, 23);
            this.btnConfirmar.TabIndex = 7;
            this.btnConfirmar.Text = "&Confirmar";
            this.btnConfirmar.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnConfirmar.UseVisualStyleBackColor = true;
            this.btnConfirmar.Click += new System.EventHandler(this.btnConfirmar_Click);
            // 
            // btnFechar
            // 
            this.btnFechar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnFechar.Image = global::SWS.Properties.Resources.close;
            this.btnFechar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnFechar.Location = new System.Drawing.Point(79, 3);
            this.btnFechar.Name = "btnFechar";
            this.btnFechar.Size = new System.Drawing.Size(70, 23);
            this.btnFechar.TabIndex = 17;
            this.btnFechar.TabStop = false;
            this.btnFechar.Text = "&Fechar";
            this.btnFechar.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnFechar.UseVisualStyleBackColor = true;
            this.btnFechar.Click += new System.EventHandler(this.btnFechar_Click);
            // 
            // lblDataInicio
            // 
            this.lblDataInicio.BackColor = System.Drawing.Color.LightGray;
            this.lblDataInicio.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblDataInicio.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDataInicio.Location = new System.Drawing.Point(6, 17);
            this.lblDataInicio.Name = "lblDataInicio";
            this.lblDataInicio.ReadOnly = true;
            this.lblDataInicio.Size = new System.Drawing.Size(129, 21);
            this.lblDataInicio.TabIndex = 8;
            this.lblDataInicio.TabStop = false;
            this.lblDataInicio.Text = "Dt de início Vigência";
            // 
            // lblDataTermino
            // 
            this.lblDataTermino.BackColor = System.Drawing.Color.LightGray;
            this.lblDataTermino.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblDataTermino.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDataTermino.Location = new System.Drawing.Point(6, 37);
            this.lblDataTermino.Name = "lblDataTermino";
            this.lblDataTermino.ReadOnly = true;
            this.lblDataTermino.Size = new System.Drawing.Size(129, 21);
            this.lblDataTermino.TabIndex = 10;
            this.lblDataTermino.TabStop = false;
            this.lblDataTermino.Text = "Dt de término vigência";
            // 
            // dataInicio
            // 
            this.dataInicio.CustomFormat = "dd/MM/yyyy HH:mm:ss";
            this.dataInicio.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dataInicio.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dataInicio.Location = new System.Drawing.Point(134, 17);
            this.dataInicio.Name = "dataInicio";
            this.dataInicio.ShowCheckBox = true;
            this.dataInicio.Size = new System.Drawing.Size(253, 21);
            this.dataInicio.TabIndex = 1;
            // 
            // dataTermino
            // 
            this.dataTermino.Checked = false;
            this.dataTermino.CustomFormat = "dd/MM/yyyy HH:mm:ss";
            this.dataTermino.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dataTermino.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dataTermino.Location = new System.Drawing.Point(134, 37);
            this.dataTermino.Name = "dataTermino";
            this.dataTermino.ShowCheckBox = true;
            this.dataTermino.Size = new System.Drawing.Size(253, 21);
            this.dataTermino.TabIndex = 2;
            // 
            // frmContratoDuplicarData
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(399, 261);
            this.Name = "frmContratoDuplicarData";
            this.Text = "DADOS PARA DUPLICACAO DO CONTRATO";
            this.scForm.Panel1.ResumeLayout(false);
            this.scForm.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.scForm)).EndInit();
            this.scForm.ResumeLayout(false);
            this.pnlForm.ResumeLayout(false);
            this.pnlForm.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.banner)).EndInit();
            this.flowLayoutPanel1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel1;
        private System.Windows.Forms.Button btnConfirmar;
        private System.Windows.Forms.Button btnFechar;
        protected System.Windows.Forms.TextBox lblDataInicio;
        protected System.Windows.Forms.TextBox lblDataTermino;
        protected System.Windows.Forms.DateTimePicker dataInicio;
        protected System.Windows.Forms.DateTimePicker dataTermino;
    }
}