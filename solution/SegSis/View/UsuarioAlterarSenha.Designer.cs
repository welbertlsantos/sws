﻿namespace SWS.View
{
    partial class frm_usuarioSenhaAlterar
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frm_usuarioSenhaAlterar));
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.btn_salvar = new System.Windows.Forms.Button();
            this.btn_cancelar = new System.Windows.Forms.Button();
            this.text_senhaAtual = new System.Windows.Forms.TextBox();
            this.text_novaSenha = new System.Windows.Forms.TextBox();
            this.text_confirmacao = new System.Windows.Forms.TextBox();
            this.lbl_alterarSenha = new System.Windows.Forms.Label();
            this.lbl_senhaAtual = new System.Windows.Forms.Label();
            this.lbl_novaSenha = new System.Windows.Forms.Label();
            this.lbl_confirmacao = new System.Windows.Forms.Label();
            this.UsuarioSenhaAlterarErrorProvider = new System.Windows.Forms.ErrorProvider(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.UsuarioSenhaAlterarErrorProvider)).BeginInit();
            this.SuspendLayout();
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = global::SWS.Properties.Resources.banner1;
            this.pictureBox1.Location = new System.Drawing.Point(0, 1);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(395, 50);
            this.pictureBox1.TabIndex = 0;
            this.pictureBox1.TabStop = false;
            // 
            // pictureBox2
            // 
            this.pictureBox2.Image = global::SWS.Properties.Resources.password1;
            this.pictureBox2.Location = new System.Drawing.Point(70, 44);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(53, 50);
            this.pictureBox2.TabIndex = 1;
            this.pictureBox2.TabStop = false;
            // 
            // btn_salvar
            // 
            this.btn_salvar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_salvar.Location = new System.Drawing.Point(35, 229);
            this.btn_salvar.Name = "btn_salvar";
            this.btn_salvar.Size = new System.Drawing.Size(75, 23);
            this.btn_salvar.TabIndex = 4;
            this.btn_salvar.Text = "&Salvar";
            this.btn_salvar.UseVisualStyleBackColor = true;
            this.btn_salvar.Click += new System.EventHandler(this.btn_salvar_Click);
            // 
            // btn_cancelar
            // 
            this.btn_cancelar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_cancelar.Location = new System.Drawing.Point(116, 229);
            this.btn_cancelar.Name = "btn_cancelar";
            this.btn_cancelar.Size = new System.Drawing.Size(75, 23);
            this.btn_cancelar.TabIndex = 5;
            this.btn_cancelar.Text = "&Cancelar";
            this.btn_cancelar.UseVisualStyleBackColor = true;
            this.btn_cancelar.Click += new System.EventHandler(this.btn_cancelar_Click);
            // 
            // text_senhaAtual
            // 
            this.text_senhaAtual.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.text_senhaAtual.Location = new System.Drawing.Point(116, 110);
            this.text_senhaAtual.MaxLength = 15;
            this.text_senhaAtual.Name = "text_senhaAtual";
            this.text_senhaAtual.Size = new System.Drawing.Size(209, 20);
            this.text_senhaAtual.TabIndex = 1;
            this.text_senhaAtual.UseSystemPasswordChar = true;
            // 
            // text_novaSenha
            // 
            this.text_novaSenha.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.text_novaSenha.Location = new System.Drawing.Point(116, 152);
            this.text_novaSenha.MaxLength = 15;
            this.text_novaSenha.Name = "text_novaSenha";
            this.text_novaSenha.PasswordChar = '*';
            this.text_novaSenha.Size = new System.Drawing.Size(209, 20);
            this.text_novaSenha.TabIndex = 2;
            this.text_novaSenha.UseSystemPasswordChar = true;
            // 
            // text_confirmacao
            // 
            this.text_confirmacao.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.text_confirmacao.Location = new System.Drawing.Point(116, 193);
            this.text_confirmacao.MaxLength = 15;
            this.text_confirmacao.Name = "text_confirmacao";
            this.text_confirmacao.PasswordChar = '*';
            this.text_confirmacao.Size = new System.Drawing.Size(209, 20);
            this.text_confirmacao.TabIndex = 3;
            this.text_confirmacao.UseSystemPasswordChar = true;
            // 
            // lbl_alterarSenha
            // 
            this.lbl_alterarSenha.AutoSize = true;
            this.lbl_alterarSenha.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_alterarSenha.Location = new System.Drawing.Point(129, 57);
            this.lbl_alterarSenha.Name = "lbl_alterarSenha";
            this.lbl_alterarSenha.Size = new System.Drawing.Size(156, 25);
            this.lbl_alterarSenha.TabIndex = 7;
            this.lbl_alterarSenha.Text = "Alterar Senha";
            // 
            // lbl_senhaAtual
            // 
            this.lbl_senhaAtual.AutoSize = true;
            this.lbl_senhaAtual.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_senhaAtual.Location = new System.Drawing.Point(32, 113);
            this.lbl_senhaAtual.Name = "lbl_senhaAtual";
            this.lbl_senhaAtual.Size = new System.Drawing.Size(76, 13);
            this.lbl_senhaAtual.TabIndex = 8;
            this.lbl_senhaAtual.Text = "Senha Atual";
            // 
            // lbl_novaSenha
            // 
            this.lbl_novaSenha.AutoSize = true;
            this.lbl_novaSenha.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_novaSenha.Location = new System.Drawing.Point(34, 154);
            this.lbl_novaSenha.Name = "lbl_novaSenha";
            this.lbl_novaSenha.Size = new System.Drawing.Size(77, 13);
            this.lbl_novaSenha.TabIndex = 9;
            this.lbl_novaSenha.Text = "Nova Senha";
            // 
            // lbl_confirmacao
            // 
            this.lbl_confirmacao.AutoSize = true;
            this.lbl_confirmacao.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_confirmacao.Location = new System.Drawing.Point(34, 195);
            this.lbl_confirmacao.Name = "lbl_confirmacao";
            this.lbl_confirmacao.Size = new System.Drawing.Size(77, 13);
            this.lbl_confirmacao.TabIndex = 10;
            this.lbl_confirmacao.Text = "Confirmacao";
            // 
            // UsuarioSenhaAlterarErrorProvider
            // 
            this.UsuarioSenhaAlterarErrorProvider.ContainerControl = this;
            // 
            // frm_usuarioSenhaAlterar
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(394, 275);
            this.Controls.Add(this.lbl_confirmacao);
            this.Controls.Add(this.lbl_novaSenha);
            this.Controls.Add(this.lbl_senhaAtual);
            this.Controls.Add(this.lbl_alterarSenha);
            this.Controls.Add(this.text_confirmacao);
            this.Controls.Add(this.text_novaSenha);
            this.Controls.Add(this.text_senhaAtual);
            this.Controls.Add(this.btn_cancelar);
            this.Controls.Add(this.btn_salvar);
            this.Controls.Add(this.pictureBox2);
            this.Controls.Add(this.pictureBox1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "frm_usuarioSenhaAlterar";
            this.Text = "ALTERAR SENHA";
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.UsuarioSenhaAlterarErrorProvider)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.Button btn_salvar;
        private System.Windows.Forms.Button btn_cancelar;
        private System.Windows.Forms.TextBox text_senhaAtual;
        private System.Windows.Forms.TextBox text_novaSenha;
        private System.Windows.Forms.TextBox text_confirmacao;
        private System.Windows.Forms.Label lbl_alterarSenha;
        private System.Windows.Forms.Label lbl_senhaAtual;
        private System.Windows.Forms.Label lbl_novaSenha;
        private System.Windows.Forms.Label lbl_confirmacao;
        private System.Windows.Forms.ErrorProvider UsuarioSenhaAlterarErrorProvider;
    }
}