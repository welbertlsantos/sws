﻿using SWS.Entidade;
using SWS.Facade;
using SWS.View.Resources;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SWS.View
{
    public partial class frmFontesSelecionar : frmTemplateConsulta
    {
        List<Fonte> fontes;

        public List<Fonte> Fontes
        {
            get { return fontes; }
            set { fontes = value; }
        }
        
        public frmFontesSelecionar()
        {
            InitializeComponent();
            ActiveControl = textFonte;
        }

        private void btnIncluir_Click(object sender, EventArgs e)
        {
            try
            {
                bool flagSelecao = false;
                EstudoFacade pcmsoFacade = EstudoFacade.getInstance();

                Fontes = new List<Fonte>();

                foreach (DataGridViewRow dvRow in dgvFonte.Rows)
                {
                    if (Convert.ToBoolean(dvRow.Cells[2].Value))
                    {
                        flagSelecao = true;
                        fontes.Add(pcmsoFacade.findFonteById((long)dvRow.Cells[0].Value));
                    }
                }

                if (!flagSelecao)
                    throw new Exception("É necessário selecionar uma fonte para inclusão.");

                this.Close();

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }

        private void btnBuscar_Click(object sender, EventArgs e)
        {
            montaGrid();
        }

        private void btnNovo_Click(object sender, EventArgs e)
        {
            frmFonteIncluir incluirNovaFonte = new frmFonteIncluir();
            incluirNovaFonte.ShowDialog();

            if (incluirNovaFonte.Fonte != null)
            {
                textFonte.Text = incluirNovaFonte.Fonte.Descricao;
                montaGrid();
            }
            
        }

        private void btnFechar_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void montaGrid()
        {
            try
            {
                dgvFonte.ColumnCount = 2;

                EstudoFacade pcmsoFacede = EstudoFacade.getInstance();

                Fonte fonte = new Fonte();
                fonte.Descricao = textFonte.Text.Trim();
                fonte.Situacao = ApplicationConstants.ATIVO;
                fonte.Privado = false;

                this.Cursor = Cursors.WaitCursor;
                DataSet ds = pcmsoFacede.findFonteByFilter(fonte);

                dgvFonte.Columns[0].HeaderText = "idFonte";
                dgvFonte.Columns[0].Visible = false;
                dgvFonte.Columns[1].HeaderText = "Fonte";

                DataGridViewCheckBoxColumn selecionar = new DataGridViewCheckBoxColumn();
                selecionar.Name = "Selec";
                dgvFonte.Columns.Add(selecionar);
                dgvFonte.Columns[2].Width = 50;
                dgvFonte.Columns[2].DisplayIndex = 0;


                foreach (DataRow row in ds.Tables[0].Rows)
                    dgvFonte.Rows.Add(
                        row["ID_FONTE"],
                        row["DESCRICAO"].ToString(),
                        false);

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }

        }
    }
}
