﻿using SWS.Entidade;
using SWS.Facade;
using SWS.View.Resources;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace SWS.View
{
    public partial class frmAtividadeAlterar : frmAtividadeIncluir
    {
        public frmAtividadeAlterar(Atividade atividade) :base()
        {
            InitializeComponent();
            this.Atividade = atividade;
            textAtividade.Text = atividade.Descricao;
        }

        protected override void btnGravar_Click(object sender, EventArgs e)
        {
            try
            {
                if (ValidaDadosTela())
                {
                    EstudoFacade pcmsoFacade = EstudoFacade.getInstance();

                    Atividade = pcmsoFacade.updateAtividade(new Atividade(Atividade.Id, textAtividade.Text, ApplicationConstants.ATIVO));
                    MessageBox.Show("Atividade alterada com sucesso.");
                    this.Close();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        
    }
}
