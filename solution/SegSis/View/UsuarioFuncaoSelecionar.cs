﻿using SWS.Entidade;
using SWS.Facade;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace SWS.View
{
    public partial class frmUsuarioFuncaoSelecionar : frmTemplateConsulta
    {
        private Usuario usuarioTecnico;

        public Usuario UsuarioTecnico
        {
            get { return usuarioTecnico; }
            set { usuarioTecnico = value; }
        }
        
        
        public frmUsuarioFuncaoSelecionar()
        {
            InitializeComponent();
            MontaDataGrid();
        }

        private void btnConfirmar_Click(object sender, EventArgs e)
        {
            try
            {
                if (dgvUsuario.CurrentRow == null)
                    throw new Exception("Selecione uma linha");

                UsuarioFacade usuarioFacade = UsuarioFacade.getInstance();
                UsuarioTecnico = usuarioFacade.findUsuarioById((long)dgvUsuario.CurrentRow.Cells[0].Value);
                this.Close();
                

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }

        private void btnFechar_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        public void MontaDataGrid()
        {
            try
            {
                this.Cursor = Cursors.WaitCursor;

                UsuarioFacade usuarioFacade = UsuarioFacade.getInstance();

                this.dgvUsuario.Columns.Clear();

                this.dgvUsuario.ColumnCount = 3;

                this.dgvUsuario.Columns[0].HeaderText = "idUsuario";
                this.dgvUsuario.Columns[0].Visible = false;
                this.dgvUsuario.Columns[1].HeaderText = "Nome";
                this.dgvUsuario.Columns[2].HeaderText = "Data_de_admissão";

                foreach (Usuario usuario in usuarioFacade.findUsuarioByFuncaoInterna(1, 2))
                    this.dgvUsuario.Rows.Add(usuario.Id, usuario.Nome, usuario.DataAdmissao);

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
                
        }
    }
}
