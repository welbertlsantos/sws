﻿using SWS.Entidade;
using SWS.Facade;
using SWS.View.ViewHelper;
using SWS.ViewHelper;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SWS.View
{
    public partial class frmPcmsoAtividadeIncluir : frmTemplateConsulta
    {
        Estudo pcmso;
        protected Atividade atividade;
        
        public frmPcmsoAtividadeIncluir(Estudo pcmso)
        {
            InitializeComponent();
            this.pcmso = pcmso;
            ComboHelper.comboAno(cbAnoRealizacao);
            ComboHelper.startComboMesSave(cbMesRealizacao);
            ActiveControl = btnAtividadeIncluir;
        }

        public frmPcmsoAtividadeIncluir()
        {
            InitializeComponent();
        }

        protected void frmPcmsoAtividadeIncluir_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter || e.KeyCode == Keys.Return)
                SendKeys.Send("{TAB}");
        }

        protected void btnFechar_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        protected virtual void btnGravar_Click(object sender, EventArgs e)
        {
            try
            {
                if (validaDadosTela())
                {
                    /* criando o conograma */
                    EstudoFacade pcmsoFacade = EstudoFacade.getInstance();

                    Cronograma cronograma = new Cronograma();
                    Cronograma cronogramaFind = pcmsoFacade.findCronogramaByPcmso(pcmso);

                    if (cronogramaFind == null)
                    {
                        cronograma.Descricao = "CRONOGRAMA INICIAL DE ATIVIDADES";
                        cronograma = pcmsoFacade.insertCronograma(cronograma, pcmso);
                    }
                    else
                        cronograma = cronogramaFind;

                    /* verificando se a atividade já não está incluída no pcmso */

                    List<CronogramaAtividade> atividadesInCronograma = new List<CronogramaAtividade>();
                    atividadesInCronograma = pcmsoFacade.findAllAtividadesByCronograma(cronograma).ToList();

                    if (atividadesInCronograma.Exists(x => x.Atividade.Id == atividade.Id))
                        throw new Exception("Atividade já incluída nesse estudo");

                    CronogramaAtividade cronogramaAtividade = new CronogramaAtividade();
                    cronogramaAtividade.Cronograma = cronograma;
                    cronogramaAtividade.Atividade = atividade;
                    cronogramaAtividade.AnoRealizado = ((SelectItem)cbAnoRealizacao.SelectedItem).Valor;
                    cronogramaAtividade.MesRealizado = ((SelectItem)cbMesRealizacao.SelectedItem).Valor;
                    cronogramaAtividade.PublicoAlvo = textPublico.Text.Trim();
                    cronogramaAtividade.Evidencia = textRegistro.Text.Trim();

                    pcmsoFacade.insertCronogramaAtividade(cronogramaAtividade);
                    
                    if (MessageBox.Show("Atividade incluída com sucesso. Deseja incluir mais uma atividade?", "Atenção", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                    {
                        atividade = null;
                        textAtividade.Text = string.Empty;
                        ComboHelper.comboAno(cbAnoRealizacao);
                        ComboHelper.startComboMesSave(cbMesRealizacao);
                        textPublico.Text = string.Empty;
                        textRegistro.Text = string.Empty;
                        ActiveControl = btnAtividadeIncluir;
                    }
                    else
                        this.Close();

                }

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }

        protected bool validaDadosTela()
        {
            bool resultado = true;
            try
            {
                if (atividade == null)
                    throw new Exception("Atividade é obrigatória");

                if (cbMesRealizacao.SelectedItem == null)
                    throw new Exception("O mes da realização da atividade é obrigatória.");

                if (cbAnoRealizacao.SelectedItem == null)
                    throw new Exception("O ano da realização da atividade é obrigatória.");

                if (string.IsNullOrWhiteSpace(textRegistro.Text))
                    throw new Exception("Campo registro é obrigatório.");

                if (string.IsNullOrWhiteSpace(textPublico.Text))
                    throw new Exception("Campo público é obrigatório.");
                    
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
                resultado = false;
            }


            return resultado;

        }

        protected void btnAtividadeIncluir_Click(object sender, EventArgs e)
        {
            frmAtividadeBuscar selecionarAtividade = new frmAtividadeBuscar();
            selecionarAtividade.ShowDialog();

            if (selecionarAtividade.Atividade != null)
            {
                atividade = selecionarAtividade.Atividade;
                textAtividade.Text = atividade.Descricao;
            }

        }

        protected void btnAtividadeExcluir_Click(object sender, EventArgs e)
        {
            try
            {
                if (atividade == null)
                    throw new Exception("Selecione primeiro uma atividade.");

                atividade = null;
                textAtividade.Text = string.Empty;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }
    }
}
