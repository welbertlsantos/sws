﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using SWS.Facade;
using SWS.Entidade;
using SWS.Excecao;
using SWS.View.Resources;

namespace SWS.View
{
    public partial class frm_PcmsoMedicoIncluir : BaseFormConsulta
    {
        private Medico medico = null;
        public Medico getMedico()
        {
            return this.medico;
        }

        private Estudo estudo = null;
        
        private MedicoEstudo medicoEstudo = null;
        public MedicoEstudo getMedicoEstudo()
        {
            return this.medicoEstudo;
        }

        public frm_PcmsoMedicoIncluir(Estudo pcmso)
        {
            InitializeComponent();
            this.estudo = pcmso;
        }

        private void btn_buscar_Click(object sender, EventArgs e)
        {
            try
            {
                MontaDataGrid();
                text_nome.Focus();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        public void MontaDataGrid()
        {
            try
            {
                this.Cursor = Cursors.WaitCursor;

                grd_medico.Columns.Clear();

                PcmsoFacade pcmsoFacade = PcmsoFacade.getInstance();

                Medico medico = new Medico();
                medico.Nome = text_nome.Text;

                medicoEstudo = new MedicoEstudo(estudo, medico, false, ApplicationConstants.ATIVO);
                
                DataSet ds = pcmsoFacade.findAllMedicosAtivosByPcmso(medicoEstudo);

                grd_medico.DataSource = ds.Tables["Medicos"].DefaultView;

                grd_medico.Columns[0].HeaderText = "idMedico";
                grd_medico.Columns[0].Visible = false;

                grd_medico.Columns[1].HeaderText = "Nome";
                grd_medico.Columns[2].HeaderText = "CRM";
                grd_medico.Columns[3].HeaderText = "Situação";
                grd_medico.Columns[3].Visible = false;

                grd_medico.Columns[4].HeaderText = "Telefone1";
                grd_medico.Columns[5].HeaderText = "Telefone2";
                grd_medico.Columns[6].HeaderText = "E-mail    ";
                grd_medico.Columns[7].HeaderText = "Endereço";
                grd_medico.Columns[8].HeaderText = "Número";
                grd_medico.Columns[9].HeaderText = "Complemento";
                grd_medico.Columns[10].HeaderText = "Bairro";
                grd_medico.Columns[11].HeaderText = "CEP";
                grd_medico.Columns[12].HeaderText = "Cidade";
                grd_medico.Columns[13].HeaderText = "UF";

                DataGridViewCheckBoxColumn check = new DataGridViewCheckBoxColumn();
                check.Name = "CheckBox";
                check.HeaderText = "Selecionar";
                grd_medico.Columns.Add(check);
                grd_medico.Columns[14].DisplayIndex = 0;

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }

        private void btn_novo_Click(object sender, EventArgs e)
        {
            frmMedicoIncluir formMedicoIncluir = new frmMedicoIncluir();
            formMedicoIncluir.ShowDialog();

            if (formMedicoIncluir.Medico != null)
            {
                medico = formMedicoIncluir.Medico;
                text_nome.Text = medico.Nome;
                MontaDataGrid();
            }
        }

        private void btn_fechar_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btn_incluir_Click(object sender, EventArgs e)
        {
            try
            {
                PcmsoFacade pcmsoFacade = PcmsoFacade.getInstance();

                foreach (DataGridViewRow dvrom in grd_medico.Rows)
                {
                    if (Convert.ToBoolean(dvrom.Cells[14].Value) == true )
                    {
                        medico = new Medico((Int64)dvrom.Cells[0].Value, (String)dvrom.Cells[1].Value, (String)dvrom.Cells[2].Value, (String)dvrom.Cells[3].Value, dvrom.Cells[4].Value.ToString(),  dvrom.Cells[5].Value.ToString(), dvrom.Cells[6].Value.ToString(), dvrom.Cells[7].Value.ToString(), dvrom.Cells[8].Value.ToString(), dvrom.Cells[9].Value.ToString(), dvrom.Cells[10].Value.ToString(), dvrom.Cells[11].Value.ToString(), dvrom.Cells[13].Value.ToString(), null, string.Empty, string.Empty, string.Empty, null, string.Empty, string.Empty, string.Empty);

                        medicoEstudo = new MedicoEstudo(estudo, medico, false, ApplicationConstants.ATIVO);
                        
                        pcmsoFacade.insertMedicoEstudo(medicoEstudo);

                    }
                }

                this.Close();

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message,"Atenção", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
        }

    }
}
