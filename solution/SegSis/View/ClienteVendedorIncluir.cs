﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using SWS.Facade;
using SWS.Entidade;
using SWS.Excecao;
using SWS.View.ViewHelper;
using System.Text.RegularExpressions;
using SWS.View.Resources;

namespace SWS.View
{
    public partial class frm_clienteVendedorIncluir : BaseFormConsulta
    {
        private static String Msg01 = " Selecione um linha";
        private static string Msg02 = " Atenção";

        VendedorCliente vendedorCliente = null;
        public VendedorCliente getVendedorCliente()
        {
            return this.vendedorCliente;
        }

        public frm_clienteVendedorIncluir()
        {
            InitializeComponent();
            validaPermissoes();
        }

        private void validaPermissoes()
        {
            PermissionamentoFacade permissionamentoFacade = PermissionamentoFacade.getInstance();
            this.btn_novo.Enabled = permissionamentoFacade.hasPermission("VENDEDOR", "INCLUIR");
        }

        private void btn_fechar_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btn_buscar_Click(object sender, EventArgs e)
        {
            try
            {
                this.grd_cliente.Columns.Clear();
                this.Cursor = Cursors.WaitCursor;
                montaDataGrid();
                text_razaoSocial.Focus();
            }
            catch (Exception Ex)
            {
                MessageBox.Show(Ex.Message);
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }

        public void montaDataGrid()
        {
            ClienteFacade clienteFacade = ClienteFacade.getInstance();

            Cliente cliente = new Cliente();

            cliente.RazaoSocial = text_razaoSocial.Text;
            cliente.Fantasia = text_fantasia.Text;
            cliente.Situacao = ApplicationConstants.ATIVO;

            DataSet ds = clienteFacade.findClienteByFilter(cliente, null, null, false, null, null, null, null, false);

            grd_cliente.DataSource = ds.Tables["Clientes"].DefaultView;
            grd_cliente.AutoResizeColumns();
            
            grd_cliente.Columns[0].HeaderText = "Id Cliente";
            grd_cliente.Columns[0].Visible = false;

            grd_cliente.Columns[1].HeaderText = "Razão Social";

            grd_cliente.Columns[2].HeaderText = "Nome Fantasia";
            
            grd_cliente.Columns[3].HeaderText = "CNPJ";

            grd_cliente.Columns[4].HeaderText = "Inscrição Estadual";
            grd_cliente.Columns[4].Visible = false;

            grd_cliente.Columns[5].HeaderText = "Endereço";
            grd_cliente.Columns[5].Visible = false;

            grd_cliente.Columns[6].HeaderText = "Número";
            grd_cliente.Columns[6].Visible = false;

            grd_cliente.Columns[7].HeaderText = "Complemento";
            grd_cliente.Columns[7].Visible = false;

            grd_cliente.Columns[8].HeaderText = "Bairro";
            grd_cliente.Columns[8].Visible = false;

            grd_cliente.Columns[9].HeaderText = "Cidade";
            grd_cliente.Columns[9].Visible = false;

            grd_cliente.Columns[10].HeaderText = "CEP";
            grd_cliente.Columns[10].Visible = false;

            grd_cliente.Columns[11].HeaderText = "UF";
            grd_cliente.Columns[11].Visible = false;

            grd_cliente.Columns[12].HeaderText = "Telefone";
            grd_cliente.Columns[12].Visible = false;

            grd_cliente.Columns[13].HeaderText = "FAX";
            grd_cliente.Columns[13].Visible = false;

            grd_cliente.Columns[14].HeaderText = "Email";
            grd_cliente.Columns[14].Visible = false;

            grd_cliente.Columns[15].HeaderText = "Site";
            grd_cliente.Columns[15].Visible = false;

            grd_cliente.Columns[16].HeaderText = "Data Cadastro";
            grd_cliente.Columns[16].Visible = false;

            grd_cliente.Columns[17].HeaderText = "Responsável";
            grd_cliente.Columns[17].Visible = false;

            grd_cliente.Columns[18].HeaderText = "Cargo";
            grd_cliente.Columns[18].Visible = false;

            grd_cliente.Columns[19].HeaderText = "Documento";
            grd_cliente.Columns[19].Visible = false;

            grd_cliente.Columns[20].HeaderText = "Escopo";
            grd_cliente.Columns[20].Visible = false;

            grd_cliente.Columns[21].HeaderText = "Jornada";
            grd_cliente.Columns[21].Visible = false;

            grd_cliente.Columns[22].HeaderText = "Situação";
            grd_cliente.Columns[22].Visible = false;

            grd_cliente.Columns[23].HeaderText = "Estimativa Masc";
            grd_cliente.Columns[23].Visible = false;

            grd_cliente.Columns[24].HeaderText = "Estimativa Fem";
            grd_cliente.Columns[24].Visible = false;

            grd_cliente.Columns[25].HeaderText = "Endereço Cobrança";
            grd_cliente.Columns[25].Visible = false;

            grd_cliente.Columns[26].HeaderText = "Numero Cobrança";
            grd_cliente.Columns[26].Visible = false;

            grd_cliente.Columns[27].HeaderText = "Complemento Cobrança";
            grd_cliente.Columns[27].Visible = false;

            grd_cliente.Columns[28].HeaderText = "Bairro Cobrança";
            grd_cliente.Columns[28].Visible = false;

            grd_cliente.Columns[29].HeaderText = "Cep Cobrança";
            grd_cliente.Columns[29].Visible = false;

            grd_cliente.Columns[30].HeaderText = "Cidade Cobrança";
            grd_cliente.Columns[30].Visible = false;

            grd_cliente.Columns[31].HeaderText = "UF Cobrança";
            grd_cliente.Columns[31].Visible = false;

            grd_cliente.Columns[32].HeaderText = "Telefone Cobrança";
            grd_cliente.Columns[32].Visible = false;

            grd_cliente.Columns[33].HeaderText = "Fax Cobrança";
            grd_cliente.Columns[33].Visible = false;

            grd_cliente.Columns[34].HeaderText = "Coordenador";
            grd_cliente.Columns[34].Visible = false;

            grd_cliente.Columns[35].HeaderText = "VIP";
            grd_cliente.Columns[35].Visible = false;

            grd_cliente.Columns[36].HeaderText = "Telefone do Responsável";
            grd_cliente.Columns[36].Visible = false;

            grd_cliente.Columns[37].HeaderText = "Usa Contrato";
            grd_cliente.Columns[37].Visible = false;

            grd_cliente.Columns[38].HeaderText = "Particular";
            grd_cliente.Columns[38].Visible = false;

            grd_cliente.Columns[39].HeaderText = "Unidade";
            grd_cliente.Columns[39].Visible = false;

            grd_cliente.Columns[40].HeaderText = "Pode ser Credenciada";
            grd_cliente.Columns[40].Visible = false;

            grd_cliente.Columns[41].HeaderText = "Prestador";
            grd_cliente.Columns[41].Visible = false;

            grd_cliente.Columns[42].HeaderText = "Destaca ISS";
            grd_cliente.Columns[42].Visible = false;

            grd_cliente.Columns[43].HeaderText = "Gera Cobranca Vlr Líquido";
            grd_cliente.Columns[43].Visible = false;

            grd_cliente.Columns[44].HeaderText = "Aliquota ISS";
            grd_cliente.Columns[44].Visible = false;

        }

        private void btn_novo_Click(object sender, EventArgs e)
        {
            frmClientesIncluir formClienteIncluir = new frmClientesIncluir();
            formClienteIncluir.ShowDialog();

            if (formClienteIncluir.Cliente != null)
            {
                text_razaoSocial.Text = formClienteIncluir.Cliente.RazaoSocial.ToUpper();
                text_fantasia.Text = formClienteIncluir.Cliente.Fantasia.ToUpper();
                btn_buscar.PerformClick();
            }
        }

        private void btn_ok_Click(object sender, EventArgs e)
        {
            ClienteFacade clienteFacade = ClienteFacade.getInstance();

            try
            {
                if (grd_cliente.CurrentRow != null)
                {
                    vendedorCliente = clienteFacade.findClienteVendedorByCliente(clienteFacade.findClienteById((Int64)grd_cliente.CurrentRow.Cells[0].Value)).Find(x => String.Equals(x.Situacao, ApplicationConstants.ATIVO));
                    this.Close();

                }
                else
                {
                    MessageBox.Show(Msg01, Msg02);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message,"Atenção", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
        }

    }
}
