﻿using SWS.Entidade;
using SWS.Facade;
using SWS.View.Resources;
using SWS.View.ViewHelper;
using SWS.ViewHelper;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace SWS.View
{
    public partial class frmAtividadePrincipal : frmTemplate
    {

        private Atividade atividade;
        
        public frmAtividadePrincipal()
        {
            InitializeComponent();
            ComboHelper.comboSituacao(cbSituacao);
            ActiveControl = textDescricao;
            validaPermissoes();
        }

        private void btnIncluir_Click(object sender, EventArgs e)
        {
            try
            {
                frmAtividadeIncluir atividadeIncluir = new frmAtividadeIncluir();
                atividadeIncluir.ShowDialog();

                if (atividadeIncluir.Atividade != null)
                {
                    atividade = new Atividade();
                    montaDataGrid();
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btnAlterar_Click(object sender, EventArgs e)
        {
            try
            {
                if (dgvAtividade.CurrentRow == null)
                    throw new Exception("Selecione uma linha.");

                EstudoFacade pcmsoFacade = EstudoFacade.getInstance();

                Atividade atividadeAlterar = pcmsoFacade.findAtividadeById((long)dgvAtividade.CurrentRow.Cells[0].Value);

                if (!string.Equals(atividadeAlterar.Situacao, ApplicationConstants.ATIVO))
                    throw new Exception("Somente atividade ativas podem ser alteradas.");

                frmAtividadeAlterar alterarAtividade = new frmAtividadeAlterar(atividadeAlterar);
                alterarAtividade.ShowDialog();

                atividade = new Atividade();
                montaDataGrid();


            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btnDetalhar_Click(object sender, EventArgs e)
        {
            try
            {
                if (dgvAtividade.CurrentRow == null)
                    throw new Exception("Selecione uma linha.");

                EstudoFacade pcmsoFacade = EstudoFacade.getInstance();

                frmAtividadeDetalhar detalharAtividade = new frmAtividadeDetalhar(pcmsoFacade.findAtividadeById((long)dgvAtividade.CurrentRow.Cells[0].Value));
                detalharAtividade.ShowDialog();

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btnPesquisar_Click(object sender, EventArgs e)
        {
            try
            {
                atividade = new Atividade(null, textDescricao.Text, ((SelectItem)cbSituacao.SelectedItem).Valor);
                montaDataGrid();

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btnReativar_Click(object sender, EventArgs e)
        {
            try
            {
                if (dgvAtividade.CurrentRow == null)
                    throw new Exception("Selecione uma linha.");

                EstudoFacade pcmsoFacade = EstudoFacade.getInstance();

                Atividade atividadeReativar = pcmsoFacade.findAtividadeById((long)dgvAtividade.CurrentRow.Cells[0].Value);

                if (!string.Equals(atividadeReativar.Situacao, ApplicationConstants.DESATIVADO))
                    throw new Exception("Somente atividades desativadas podem ser reativadas.");

                if (MessageBox.Show("Deseja reativar a atividade selecionada?", "Atenção", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                {
                    atividadeReativar.Situacao = ApplicationConstants.ATIVO;
                    pcmsoFacade.updateAtividade(atividadeReativar);

                    MessageBox.Show("Atividade reativada com sucesso.", "Confirmação", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    atividade = new Atividade();
                    montaDataGrid();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btnExcluir_Click(object sender, EventArgs e)
        {
            try
            {
                if (dgvAtividade.CurrentRow == null)
                    throw new Exception("Selecione uma linha.");

                EstudoFacade pcmsoFacade = EstudoFacade.getInstance();

                Atividade atividadeExcluir = pcmsoFacade.findAtividadeById((long)dgvAtividade.CurrentRow.Cells[0].Value);

                if (!string.Equals(atividadeExcluir.Situacao, ApplicationConstants.ATIVO))
                    throw new Exception("Somente atividades ativas podem ser excluídas.");

                if (MessageBox.Show("Deseja excluir a atividade selecionada?", "Atenção", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                {
                    atividadeExcluir.Situacao = ApplicationConstants.DESATIVADO;
                    pcmsoFacade.updateAtividade(atividadeExcluir);

                    MessageBox.Show("Atividade excluída com sucesso.", "Confirmação", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    atividade = new Atividade();
                    montaDataGrid();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btnFechar_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void validaPermissoes()
        {
            PermissionamentoFacade permissionamentoFacade = PermissionamentoFacade.getInstance();

            btnAlterar.Enabled = permissionamentoFacade.hasPermission("ATIVIDADE", "ALTERAR");
            btnIncluir.Enabled = permissionamentoFacade.hasPermission("ATIVIDADE", "INCLUIR");
            btnExcluir.Enabled = permissionamentoFacade.hasPermission("ATIVIDADE", "EXCLUIR");
            btnDetalhar.Enabled = permissionamentoFacade.hasPermission("ATIVIDADE", "DETALHAR");
            btnReativar.Enabled = permissionamentoFacade.hasPermission("ATIVIDADE", "REATIVAR");
        }

        public void montaDataGrid()
        {
            EstudoFacade pcmsoFacade = EstudoFacade.getInstance();

            DataSet ds = pcmsoFacade.findAtividadeByFilter(atividade);

            dgvAtividade.DataSource = ds.Tables["Atividades"].DefaultView;

            dgvAtividade.Columns[0].HeaderText = "ID";
            dgvAtividade.Columns[0].Visible = false;

            dgvAtividade.Columns[1].HeaderText = "Descrição";

            dgvAtividade.Columns[2].HeaderText = "Situação";
        }

        private void dgvAtividade_RowPrePaint(object sender, DataGridViewRowPrePaintEventArgs e)
        {
            DataGridView dgv = sender as DataGridView;

            if (string.Equals(dgv.Rows[e.RowIndex].Cells[2].Value, ApplicationConstants.DESATIVADO))
                dgv.Rows[e.RowIndex].DefaultCellStyle.ForeColor = Color.Red;
        }

        private void dgvAtividade_CellMouseDoubleClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            try
            {
                btnAlterar.PerformClick();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }


    }
}
