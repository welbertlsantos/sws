﻿using SWS.Entidade;
using SWS.Facade;
using SWS.View.Resources;
using SWS.View.ViewHelper;
using SWS.ViewHelper;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace SWS.View
{
    public partial class frmContaIncluir : frmTemplate
    {
        private Conta conta;

        public Conta Conta
        {
            get { return conta; }
            set { conta = value; }
        }

        private Banco bancoSelecionado;

        public Banco BancoSelecionado
        {
            get { return bancoSelecionado; }
            set { bancoSelecionado = value; }
        }
        
        public frmContaIncluir()
        {
            InitializeComponent();
            ActiveControl = textNome;
            ComboHelper.Boolean(cbRegistro, false);
        }

        protected virtual void btnGravar_Click(object sender, EventArgs e)
        {
            try
            {
                if (ValidaCamposObrigatórios())
                {
                    FinanceiroFacade financeiroFacade = FinanceiroFacade.getInstance();
                    conta = financeiroFacade.insertConta(new Conta(null, bancoSelecionado, textNome.Text, textAgencia.Text, textDigitoAgencia.Text, textNumeroConta.Text, textDigitoConta.Text, string.IsNullOrEmpty(textNossoNumero.Text) ? 0 : (long?)Convert.ToInt64(textNossoNumero.Text), textNumeroRemessa.Text, textCarteira.Text, textNumeroConvenio.Text, textVariacaoCarteira.Text, textCodigoCedente.Text, Convert.ToBoolean(((SelectItem)cbRegistro.SelectedItem).Valor), ApplicationConstants.ATIVO, false));

                    MessageBox.Show("Conta Corrente incluída com sucesso", "Confirmação", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    this.Close();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        protected void btnLimpar_Click(object sender, EventArgs e)
        {
            bancoSelecionado = null;
            textBanco.Text = string.Empty;
            textNome.Text = string.Empty;
            ComboHelper.Boolean(cbRegistro, false);
            textAgencia.Text = string.Empty;
            textDigitoAgencia.Text = string.Empty;
            textNumeroConta.Text = string.Empty;
            textDigitoConta.Text = string.Empty;
            textNossoNumero.Text = string.Empty;
            textNumeroRemessa.Text = string.Empty;
            textCarteira.Text = string.Empty;
            textVariacaoCarteira.Text = string.Empty;
            textNumeroConvenio.Text = string.Empty;
            textCodigoCedente.Text = string.Empty;
            textNome.Focus();
        }

        protected void btnFechar_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        protected void frmContaIncluir_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter || e.KeyCode == Keys.Return)
                SendKeys.Send("{TAB}");
        }

        protected void btnBanco_Click(object sender, EventArgs e)
        {
            try
            {
                frmBancoSelecionar selecionarBanco = new frmBancoSelecionar(null, null);
                selecionarBanco.ShowDialog();

                if (selecionarBanco.Banco != null)
                {
                    bancoSelecionado = selecionarBanco.Banco;
                    textBanco.Text = bancoSelecionado.Nome;

                    /* validando informações do banco se for do tipo caixa */
                    if ((bool)bancoSelecionado.Caixa)
                        desabilitandoControles(true);
                    else
                        desabilitandoControles(false);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        protected void textAgencia_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = ValidaCampoHelper.FormataCampoNumerico(sender, e);
        }

        protected void textDigitoAgencia_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = ValidaCampoHelper.FormataCampoNumerico(sender, e);
        }

        protected void textNumeroConta_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = ValidaCampoHelper.FormataCampoNumerico(sender, e);
        }

        protected void textDigitoConta_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = ValidaCampoHelper.FormataCampoNumerico(sender, e);
        }

        protected void textNossoNumero_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = ValidaCampoHelper.FormataNumeroELetras(sender, e);
        }

        protected void textNumeroRemessa_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = ValidaCampoHelper.FormataCampoNumerico(sender, e);
        }

        protected void textCarteira_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = ValidaCampoHelper.FormataCampoNumerico(sender, e);
        }

        protected void textVariacaoCarteira_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = ValidaCampoHelper.FormataCampoNumerico(sender, e);
        }

        protected void textNumeroConvenio_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = ValidaCampoHelper.FormataNumeroELetras(sender, e);
        }

        protected void textCodigoCedente_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = ValidaCampoHelper.FormataCampoNumerico(sender, e);
        }

        protected void desabilitandoControles(bool situacao)
        {
            if (situacao)
            {
                cbRegistro.Enabled = situacao;
                textAgencia.Text = string.Empty;
                textAgencia.ReadOnly = situacao;
                textDigitoAgencia.Text = string.Empty;
                textDigitoAgencia.ReadOnly = situacao;
                textNumeroConta.Text = string.Empty;
                textNumeroConta.ReadOnly = situacao;
                textDigitoConta.Text = string.Empty;
                textDigitoConta.ReadOnly = situacao;
                textNossoNumero.Text = string.Empty;
                textNossoNumero.ReadOnly = situacao;
                textNumeroRemessa.Text = string.Empty;
                textNumeroRemessa.ReadOnly = situacao;
                textCarteira.Text = string.Empty;
                textCarteira.ReadOnly = situacao;
                textVariacaoCarteira.Text = string.Empty;
                textVariacaoCarteira.ReadOnly = situacao;
                textNumeroConvenio.Text = string.Empty;
                textNumeroConvenio.ReadOnly = situacao;
                textCodigoCedente.Text = string.Empty;
                textCodigoCedente.ReadOnly = situacao;
            }
            else
            {
                cbRegistro.Enabled = !situacao;
                textAgencia.ReadOnly = situacao;
                textDigitoAgencia.ReadOnly = situacao;
                textNumeroConta.ReadOnly = situacao;
                textDigitoConta.ReadOnly = situacao;
                textNossoNumero.ReadOnly = situacao;
                textNumeroRemessa.ReadOnly = situacao;
                textCarteira.ReadOnly = situacao;
                textVariacaoCarteira.ReadOnly = situacao;
                textNumeroConvenio.ReadOnly = situacao;
                textCodigoCedente.ReadOnly = situacao;
            }
        }

        protected bool ValidaCamposObrigatórios()
        {
            bool retorno = true;
            
            try
            {
                if (bancoSelecionado == null)
                    throw new Exception("Obrigatório selecionar um banco.");

                if (String.IsNullOrEmpty(textNome.Text))
                    throw new Exception("Nome da conta obrigatório.");

                if (!(bool)bancoSelecionado.Caixa)
                {
                    if (string.IsNullOrEmpty(textAgencia.Text))
                        throw new Exception("Código da agência é obrigatório.");

                    if (string.IsNullOrEmpty(textNumeroConta.Text))
                        throw new Exception("Número da conta corrente é obrigatório.");

                    if (string.IsNullOrEmpty(textNossoNumero.Text))
                        throw new Exception("Nosso número informado pelo banco é obrigatório.");

                    if (string.IsNullOrEmpty(textCarteira.Text))
                        throw new Exception("Número da carteira de cobrança é obrigatório.");

                    if (Convert.ToBoolean(((SelectItem)cbRegistro.SelectedItem).Valor))
                        if (string.IsNullOrEmpty(textNumeroConvenio.Text) && string.IsNullOrEmpty(textCodigoCedente.Text))
                            throw new Exception("Número de convênio ou código de cedente é obrigatório porque a conta cobrança é do tipo registrada.");

                }
            }
            catch (Exception ex)
            {
                retorno = false;
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

            return retorno;

        }
    }
}
