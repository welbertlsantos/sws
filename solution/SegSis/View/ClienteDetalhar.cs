﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using SegSis.Facade;
using SegSis.Entidade;
using SegSis.Excecao;
using SegSis.View.ViewHelper;
using System.Text.RegularExpressions;
using SegSis.View.Resources;
using SegSis.Helper;

namespace SegSis.View
{
    public partial class frm_cliente_detalhar : BaseForm
    {
        frm_cliente formCliente;

        private Cnae cnaePrincipal;

        private Int64 idCliente;
        private Arquivo arquivo = null;
        private ClienteArquivo clienteArquivo = null;
        private Cliente cliente;
        
        public frm_cliente_detalhar(Cliente cliente, frm_cliente formCliente)
        {
            try
            {
                ClienteFacade clienteFacade = ClienteFacade.getInstance();
                
                InitializeComponent();
                this.formCliente = formCliente;
                this.cliente = cliente;

                idCliente = (Int64)cliente.getId();

                ComboHelper.iniciaComboTipoCliente(cb_tipoCliente);
                cb_tipoCliente.SelectedValue = ApplicationConstants.NORMAL;

                //preenchendo os textbox

                text_razaoSocial.Text = cliente.getRazaoSocial();
                text_cnpj.Text = cliente.getCnpj();
                text_fantasia.Text = cliente.getFantasia();
                text_inscricao.Text = cliente.getInscricao();
                foreach (VendedorCliente vendedorCliente in cliente.getVendedorCliente())
                {
                    text_vendedor.Text = vendedorCliente.getVendedor().getNome();
                }

                text_estMasc.Text = Convert.ToString(cliente.getEstMasc());
                text_estFem.Text = Convert.ToString(cliente.getEstFem());
                dt_cadastro.Text = Convert.ToString(cliente.getDataCadastro());
                text_endereco.Text = cliente.getEndereco();
                text_numero.Text = cliente.getNumero();
                text_complemento.Text = cliente.getComplemento();
                text_bairro.Text = cliente.getBairro();
                text_cep.Text = cliente.getCep();

                text_cidade.Text = cliente.getCidadeIbge().getNome();
                
                text_uf.Text = cliente.getUf();
                text_telefone1.Text = cliente.getTelefone1();
                text_telefone2.Text = cliente.getTelefone2();
                text_email.Text = cliente.getEmail();
                text_site.Text = cliente.getSite();
                text_nome.Text = cliente.getResponsavel();
                text_cargo.Text = cliente.getCargo();
                text_documento.Text = cliente.getDocumento();
                text_jornada.Text = cliente.getJornada();
                text_telefoneContato.Text = cliente.getTelefoneContato();
                text_escopo.Text = cliente.getEscopo();
                if (ApplicationConstants.ATIVO.Equals(cliente.getSituacao()))
                {
                    rb_ativo.Checked = true;
                }
                else if (ApplicationConstants.DESATIVADO.Equals(cliente.getSituacao()))
                {
                    rb_desativado.Checked = true;
                }

                // Dados de cobranca

                text_enderecoCob.Text = cliente.getEnderecoCob();
                text_numeroCob.Text = cliente.getNumeroCob();
                text_complementoCob.Text = cliente.getComplementoCob();
                text_bairroCob.Text = cliente.getBairroCob();

                text_cidadeCobranca.Text = cliente.getCidadeIbgeCobranca().getNome();
                text_cepCob.Text = cliente.getCepCob();
                text_telefone1Cob.Text = cliente.getTelefoneCob();
                text_telefone2Cob.Text = cliente.getTelefone2Cob();
                text_ufCob.Text = cliente.getUfCob();


                // preenchendo os dados da matriz
                if (cliente.getMatriz() != null)
                {
                    text_matriz.Text = cliente.getMatriz().getRazaoSocial();
                    gb_credenciada.Enabled = false;
                    text_cnpj.ReadOnly = true;
                    cb_tipoCliente.SelectedValue = ApplicationConstants.UNIDADE;
                }

                // preenchenco os dados da credenciada

                if (cliente.getCredenciada() != null)
                {
                    gb_Matriz.Enabled = false;
                    cb_tipoCliente.SelectedValue = ApplicationConstants.CREDENCIADA;
                    text_credenciada.Text = cliente.getCredenciada().getRazaoSocial();
                }

                grd_cnae.ColumnCount = 3;

                grd_cnae.Columns[0].Name = "ID";
                grd_cnae.Columns[1].Name = "Código Cnae";
                grd_cnae.Columns[2].Name = "Atividade";
                grd_cnae.Columns[0].Visible = false;

                Int32 rowIndex = -1;

                foreach (Cnae cnae in cliente.getCnae())
                {
                    this.grd_cnae.Rows.Insert(++rowIndex, cnae.getId(), cnae.getCodCnae(), cnae.getAtividade());

                }

                buscaCnaePrincipal();

                if (cnaePrincipal != null)
                {
                    text_cnaePrincipal.Text = cnaePrincipal.getCodCnae();
                }

                if (cliente.getCoordenador() != null)
                {
                    text_nomeCoordenador.Text = cliente.getCoordenador().getNome().ToUpper();
                }

                if (cliente.getVip())
                {
                    chk_vip.Checked = true;
                }

                if (cliente.getUsaContrato())
                {
                    chk_usaContrato.Checked = true;
                }

                if (cliente.getParticular())
                {
                    chk_particular.Checked = true;
                }

                if (cliente.getDestacaIss())
                {
                    chk_destacaIss.Checked = true;
                }

                if (cliente.getGeraCobrancaValorLiquido())
                {
                    chk_geraCobrancaValorLiquido.Checked = true;
                }

                text_aliquotaIss.Text = cliente.getAliquotaIss().ToString();
                
                clienteArquivo = clienteFacade.findClienteArquivoByCliente(cliente);
                if (clienteArquivo != null && clienteArquivo.getArquivo() != null)
                {
                    arquivo = clienteFacade.findArquivoById(clienteArquivo.getArquivo().getId());
                    pic_box_logo.Image = FileHelper.byteArrayToImage(arquivo.getConteudo());
                }

                chk_prestador.Checked = cliente.getPrestador() ? true : false;

                montaGridFuncoes();

                montaGridCredenciada();

            }
            catch (Exception Ex)
            {
                MessageBox.Show(Ex.Message);
            }

        }

        private void btn_fechar_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        public void buscaCnaePrincipal()
        {
            try
            {
                ClienteFacade clienteFacade = ClienteFacade.getInstance();
                cnaePrincipal = clienteFacade.findCnaePrincipalCliente(idCliente);

            }
            catch (Exception Ex)
            {
                MessageBox.Show(Ex.Message);
            }

        }


        public void montaGridFuncoes()
        {
            try
            {
                dg_funcao.Columns.Clear();

                if (cliente.getFuncoes().Count > 0)
                {
                    dg_funcao.ColumnCount = 8;

                    dg_funcao.Columns[0].HeaderText = "id_seg_cliente_funcao";
                    dg_funcao.Columns[1].HeaderText = "Id_Função";
                    dg_funcao.Columns[2].HeaderText = "Descrição";
                    dg_funcao.Columns[3].HeaderText = "Cod_CBO";
                    dg_funcao.Columns[4].HeaderText = "Situação";
                    dg_funcao.Columns[5].HeaderText = "Comentário";
                    dg_funcao.Columns[5].Visible = false;
                    dg_funcao.Columns[6].HeaderText = "Situação_Cliente_Função";
                    dg_funcao.Columns[7].HeaderText = "Data_Cadastro";

                    dg_funcao.Columns[0].Visible = false;
                    dg_funcao.Columns[1].Visible = false;
                    dg_funcao.Columns[4].Visible = false;
                    dg_funcao.Columns[6].Visible = false;
                    dg_funcao.Columns[7].Visible = false;

                    foreach (ClienteFuncao clienteFuncao in cliente.getFuncoes())
                    {
                        dg_funcao.Rows.Add(clienteFuncao.getId(), clienteFuncao.getFuncao().getIdFuncao(),
                            clienteFuncao.getFuncao().getDescricao(),
                            clienteFuncao.getFuncao().getCodCbo(),
                            clienteFuncao.getFuncao().getSituacao(),
                            String.Empty,
                            clienteFuncao.getSituacao(),
                            clienteFuncao.getDataCadastro());
                    }
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }


        public void montaGridCredenciada()
        {
            try
            {
                ClienteFacade clienteFacade = ClienteFacade.getInstance();

                dgCredenciada.Columns.Clear();

                /* montando tabela */

                dgCredenciada.ColumnCount = 3;

                dgCredenciada.Columns[0].HeaderText = "Codigo Cliente";
                dgCredenciada.Columns[0].Visible = false;
                dgCredenciada.Columns[1].HeaderText = "Nome da Empresa Credenciada ";
                dgCredenciada.Columns[2].HeaderText = "Nome Fantasia";

                DataSet ds = clienteFacade.findAllCredenciadaByCliente(cliente);


                foreach (DataRow row in ds.Tables[0].Rows)
                {
                    dgCredenciada.Rows.Add(Convert.ToInt64(row["id_cliente"].ToString().PadLeft(6, '0')),
                        row["razao_social"], row["fantasia"]);
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
        }
        
    }
}
