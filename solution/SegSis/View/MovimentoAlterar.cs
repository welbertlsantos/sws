﻿using SWS.Entidade;
using SWS.Facade;
using SWS.View.ViewHelper;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace SWS.View
{
    public partial class frmMovimentoAlterar : frmTemplateConsulta
    {
        private List<Movimento> movimentos;
        private int index;
        private bool alteracao;

        public bool Alteracao
        {
            get { return alteracao; }
            set { alteracao = value; }
        }
    
        public frmMovimentoAlterar(List<Movimento> movimentos, int index)
        {
            InitializeComponent();
            this.movimentos = movimentos;
            this.index = index;
            
            /* buscando na grid o item que será alterado */

            if (movimentos.ElementAt(index).ClienteFuncaoExameAso != null)
            {
                textItem.Text = movimentos.ElementAt(index).ClienteFuncaoExameAso.ClienteFuncaoExame.Exame.Descricao;
                textPrecoAtual.Text = movimentos.ElementAt(index).PrecoUnit.ToString();
            }
            else if (movimentos.ElementAt(index).GheFonteAgenteExameAso != null)
            {
                textItem.Text = movimentos.ElementAt(index).GheFonteAgenteExameAso.GheFonteAgenteExame.Exame.Descricao;
                textPrecoAtual.Text = movimentos.ElementAt(index).PrecoUnit.ToString();
            }
            else
            {
                textItem.Text = movimentos.ElementAt(index).ContratoProduto.Produto.Nome;
                textPrecoAtual.Text = movimentos.ElementAt(index).PrecoUnit.ToString();
            }
            
        }

        private void textPrecoNovo_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = ValidaCampoHelper.FormataDigitacaoNumero(sender, e, textPrecoNovo.Text, 2);
        }

        private void btn_confirma_Click(object sender, EventArgs e)
        {
            try
            {
                FinanceiroFacade financeiroFacade = FinanceiroFacade.getInstance();

                if (MessageBox.Show("Confirma a alteração para o valor R$ " + textPrecoNovo.Text + "?",  "Confirmação", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                {
                    this.Cursor = Cursors.WaitCursor;

                    if (rb_somenteSelecionado.Checked)
                    {
                        Movimento movimentoAlterado = movimentos.ElementAt(index);

                        financeiroFacade.updateupdateValorMovimento(movimentoAlterado, Convert.ToDecimal(textPrecoNovo.Text));
                        alteracao = true;

                        if (MessageBox.Show("Deseja também alterar o contrato do cliente?", "Confirmação", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                        {
                            /* alterar o contrato gerado com os valores informado */
                            
                            if (movimentoAlterado.ContratoProduto != null)
                            {
                                ContratoProduto contratoProduto = movimentoAlterado.ContratoProduto;
                                contratoProduto.PrecoContratado = Convert.ToDecimal(textPrecoNovo.Text);
                                financeiroFacade.updateContratoProduto(contratoProduto);
                            }
                            else if (movimentoAlterado.ContratoExame != null)
                            {
                                ContratoExame contratoExame = financeiroFacade.findContratoExameById((long)movimentoAlterado.ContratoExame.Id);
                                contratoExame.PrecoContratado = Convert.ToDecimal(textPrecoNovo.Text);
                                financeiroFacade.updateContratoExame(contratoExame);
                            }
                            else
                            {
                                MessageBox.Show("Não foi encontrado nenhuma informação do contrato no movimento.", "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                            }
                        }
                    }
                    else
                    {
                        /* usuário selecionou a opção que quer alterar todos os itens que estão filtrados */

                        foreach (Movimento movimento in movimentos)
                        {
                            financeiroFacade.updateupdateValorMovimento(movimento, Convert.ToDecimal(textPrecoNovo.Text));
                            alteracao = true;
                        }

                        if (MessageBox.Show("Deseja também alterar o contrato do cliente?", "Confirmação", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                        {
                            Movimento movimentoProduto = movimentos.Find(x => x.ContratoProduto != null);
                            Movimento movimentoExame = movimentos.Find(x => x.ContratoExame != null);

                            if (movimentoProduto != null)
                            {
                                movimentoProduto.ContratoProduto.PrecoContratado = Convert.ToDecimal(textPrecoNovo.Text);
                                financeiroFacade.updateContratoProduto(movimentoProduto.ContratoProduto);
                            }
                            else if (movimentoExame != null) 
                            {
                                 movimentoExame.ContratoExame = financeiroFacade.findContratoExameById((long)movimentoExame.ContratoExame.Id);
                                 movimentoExame.ContratoExame.PrecoContratado = Convert.ToDecimal(textPrecoNovo.Text);
                                 financeiroFacade.updateContratoExame(movimentoExame.ContratoExame);
                            }
                            else 
                            {
                                MessageBox.Show("Não foi encontrado nenhuma informação do contrato no movimento.", "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                            }
                        }
                    }
                }

                this.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }

        private void btn_fechar_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void textPrecoNovo_Enter(object sender, EventArgs e)
        {
            textPrecoNovo.Tag = textPrecoNovo.Text;
            textPrecoNovo.Text = String.Empty;
        }

        private void textPrecoNovo_Leave(object sender, EventArgs e)
        {
            try
            {
                if (String.IsNullOrEmpty(textPrecoNovo.Text))
                    textPrecoNovo.Text = textPrecoNovo.Tag.ToString();

                textPrecoNovo.Text = ValidaCampoHelper.FormataValorMonetario(textPrecoNovo.Text);

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);

            }

        }
    }
}
