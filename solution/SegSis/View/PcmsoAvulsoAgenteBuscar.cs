﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using SegSis.Entidade;
using SegSis.Facade;
using SegSis.View.Resources;

namespace SegSis.View
{
    public partial class frm_PcmsoAvulsoAgenteBuscar : BaseFormConsulta
    {
        frm_PcmsoAvulso formPcmsoAvulso = null;
        
        Agente agente = null;

        public void setAgente(Agente agente)
        {
            this.agente = agente;
        }

        public frm_PcmsoAvulsoAgenteBuscar(frm_PcmsoAvulso formPcmsoAvulso)
        {
            InitializeComponent();
            this.formPcmsoAvulso = formPcmsoAvulso;
            validaPermissoes();
        }

        private void validaPermissoes()
        {
            PermissionamentoFacade permissionamentoFacade = PermissionamentoFacade.getInstance();

            this.btn_novo.Enabled = permissionamentoFacade.hasPermission("AGENTE", "INCLUIR");
        }

        private void btn_buscar_Click(object sender, EventArgs e)
        {
            try
            {
                this.Cursor = Cursors.WaitCursor;
                this.grd_agente.Columns.Clear();
                montaDataGrid();
                text_descricao.Focus();
            }
            catch (Exception Ex)
            {
                MessageBox.Show(Ex.Message);
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }

        private void btn_ok_Click(object sender, EventArgs e)
        {
            Agente agenteSelecionado = null;
            GheFonte gheFonte = null;
            PcmsoFacade pcmsoFacade = PcmsoFacade.getInstance();

            try
            {
                HashSet<GheFonte> gheFonteSet = pcmsoFacade.findAllGheFonteByGhe(formPcmsoAvulso.ghe);
                
                //Verificando  se já existe um gheFonte padrão cadastrado
                if (gheFonteSet.Count > 0)
                {
                    gheFonte = gheFonteSet.First();
                }
                else
                {
                    gheFonte = pcmsoFacade.incluiGheFonte(new GheFonte(null, new Fonte(1,null,null,false), formPcmsoAvulso.ghe));
                }

                formPcmsoAvulso.gheFonte = gheFonte;

                foreach (DataGridViewRow dvRow in grd_agente.SelectedRows)
                {
                    Int32 cellValue = dvRow.Index; // selecao da grid
                    Int64 id = (Int64)grd_agente.Rows[cellValue].Cells[0].Value; // id do item selecionado.

                    agenteSelecionado = pcmsoFacade.findAgenteById(id);
                    
                    //Salvar GheFonteAgente
                    pcmsoFacade.incluiGheFonteAgente(new GheFonteAgente(null, gheFonte, agenteSelecionado, String.Empty));
                }

                formPcmsoAvulso.montaGridAgente();

                this.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void btn_novo_Click(object sender, EventArgs e)
        {
            frm_agente_incluir formAgente = new frm_agente_incluir();
            formAgente.ShowDialog();

            GheFonte gheFonte = null;
            PcmsoFacade pcmsoFacade = PcmsoFacade.getInstance();

            try
            {
                if (agente != null)
                {

                    HashSet<GheFonte> gheFonteSet = pcmsoFacade.findAllGheFonteByGhe(formPcmsoAvulso.ghe);

                    //Verificando  se já existe um gheFonte padrão cadastrado
                    if (gheFonteSet.Count > 0)
                    {
                        gheFonte = gheFonteSet.First();
                    }
                    else
                    {
                        gheFonte = pcmsoFacade.incluiGheFonte(new GheFonte(null, new Fonte(1, null, null, false), formPcmsoAvulso.ghe));
                    }

                    formPcmsoAvulso.gheFonte = gheFonte;
                    pcmsoFacade.incluiGheFonteAgente(new GheFonteAgente(null, gheFonte, agente, String.Empty));

                    formPcmsoAvulso.montaGridAgente();

                    this.Close();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void btn_fechar_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        public void montaDataGrid()
        {
            PcmsoFacade pcmsoFacade = PcmsoFacade.getInstance();

            Agente agente = new Agente();
            agente.setDescricao(text_descricao.Text);
            agente.setSituacao(ApplicationConstants.ATIVO);
            DataSet ds;
            if (formPcmsoAvulso.gheFonte != null)
            {
                ds = pcmsoFacade.findAllAgentesNotInGheFonte(agente, formPcmsoAvulso.gheFonte);
            }
            else
            {
                ds = pcmsoFacade.findAllAgentesAtivo(agente);
            }

            grd_agente.DataSource = ds.Tables["Agentes"].DefaultView;
            grd_agente.AutoResizeColumns();
            
            grd_agente.Columns[0].HeaderText = "id_agente";
            grd_agente.Columns[1].HeaderText = "id_risco";
            grd_agente.Columns[2].HeaderText = "Descrição";
            grd_agente.Columns[3].HeaderText = "Trajetória";
            grd_agente.Columns[4].HeaderText = "Danos";
            grd_agente.Columns[5].HeaderText = "Limite";
            grd_agente.Columns[6].HeaderText = "Situação";
            grd_agente.Columns[7].HeaderText = "Risco";

            grd_agente.Columns[0].Visible = false;
            grd_agente.Columns[1].Visible = false;
            grd_agente.Columns[6].Visible = false;

            grd_agente.Columns[3].DisplayIndex = 4;
            grd_agente.Columns[4].DisplayIndex = 5;
            grd_agente.Columns[5].DisplayIndex = 6;
            grd_agente.Columns[6].DisplayIndex = 7;
            grd_agente.Columns[7].DisplayIndex = 3;
        }
    }
}