﻿using SWS.Entidade;
using SWS.Excecao;
using SWS.Facade;
using SWS.View.Resources;
using SWS.View.ViewHelper;
using SWS.ViewHelper;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace SWS.View
{
    public partial class frmMedicoPrincipal : frmTemplate
    {
        private Medico medico;
        
        public frmMedicoPrincipal()
        {
            InitializeComponent();
            ComboHelper.comboSituacao(cbSituacao);
            validaPermissoes();
            ActiveControl = text_nome;
        }

        private void bt_incluir_Click(object sender, EventArgs e)
        {
            frmMedicoIncluir frmMedicoIncluir = new frmMedicoIncluir();
            frmMedicoIncluir.ShowDialog();

            if (frmMedicoIncluir.Medico != null)
            {
                medico = frmMedicoIncluir.Medico;
                text_nome.Text = medico.Nome;
                bt_pesquisar.PerformClick();
            }
        }

        private void bt_alterar_Click(object sender, EventArgs e)
        {
            try
            {
                if (grd_medico.CurrentRow == null)
                    throw new Exception("Selecione um médico");
                
                EstudoFacade pcmsoFacade = EstudoFacade.getInstance();

                medico = pcmsoFacade.findMedicoById((Int64)grd_medico.CurrentRow.Cells[0].Value);

                if (pcmsoFacade.findMedicoEstudoByMedico(medico))
                    throw new MedicoException(MedicoException.msg2);

                frmMedicoAlterar formExameAlterar = new frmMedicoAlterar(medico);
                formExameAlterar.ShowDialog();
                
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void bt_excluir_Click(object sender, EventArgs e)
        {
            try
            {
                if (grd_medico.CurrentRow == null)
                    throw new Exception ("Selecione um médico para desativar.");
                
                EstudoFacade pcmsoFacade = EstudoFacade.getInstance();

                if (MessageBox.Show("Gostaria realmente de desativar o médico selecionado?", "Confirmação", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                {

                    medico = pcmsoFacade.findMedicoById((Int64)grd_medico.CurrentRow.Cells[0].Value);

                    if (String.Equals(medico.Situacao, ApplicationConstants.DESATIVADO))
                        throw new Exception("Somente médicos ativos podem ser desativados.");

                    medico.Situacao = ApplicationConstants.DESATIVADO;
                    pcmsoFacade.updateMedico(medico);

                    MessageBox.Show("médico desativado com sucesso.", "Confirmação", MessageBoxButtons.OK, MessageBoxIcon.Information);

                    montaGridMedicos();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btn_reativar_Click(object sender, EventArgs e)
        {
            try
            {
                if (grd_medico.CurrentRow == null)
                    throw new Exception("Selecione um médico");

                EstudoFacade pcmsoFacade = EstudoFacade.getInstance();

                if (MessageBox.Show("Deseja reativar o médico selecionado?", "Confirmação", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                {
                    medico = pcmsoFacade.findMedicoById((Int64)grd_medico.CurrentRow.Cells[0].Value);

                    if (String.Equals(medico.Situacao, ApplicationConstants.ATIVO))
                        throw new Exception("Somente médicos inativos podem ser reativados.");

                    medico.Situacao = ApplicationConstants.ATIVO;
                    pcmsoFacade.updateMedico(medico);
                    MessageBox.Show("Médico reativado com sucesso.", "Confirmação", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    montaGridMedicos();

                }

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void bt_detalhar_Click(object sender, EventArgs e)
        {
            try
            {
                if (grd_medico.CurrentRow == null)
                    throw new Exception("Selecione um médico.");
                
                EstudoFacade pcmsoFacade = EstudoFacade.getInstance();

                frmMedicoDetalhar detalharFuncaoInterna = new frmMedicoDetalhar(pcmsoFacade.findMedicoById((Int64)grd_medico.CurrentRow.Cells[0].Value));
                detalharFuncaoInterna.ShowDialog();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

            finally
            {
                this.Cursor = Cursors.Default;
            }
        }

        private void bt_limpar_Click(object sender, EventArgs e)
        {
            this.text_nome.Text = String.Empty;
            this.text_nome.Focus();
            this.grd_medico.Columns.Clear();
            ComboHelper.comboSituacao(cbSituacao);
        }

        private void bt_pesquisar_Click(object sender, EventArgs e)
        {
            medico = new Medico(null, text_nome.Text, string.Empty, ((SelectItem)cbSituacao.SelectedItem).Valor, string.Empty, string.Empty, string.Empty, string.Empty, string.Empty, string.Empty, string.Empty, string.Empty, string.Empty, null, string.Empty, string.Empty, string.Empty, null, string.Empty, string.Empty, string.Empty);
            montaGridMedicos();
        }

        private void bt_fechar_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void grd_medico_RowPrePaint(object sender, DataGridViewRowPrePaintEventArgs e)
        {
            DataGridView dgv = sender as DataGridView;

            if (String.Equals(dgv.Rows[e.RowIndex].Cells[3].Value, "I"))
                dgv.Rows[e.RowIndex].DefaultCellStyle.ForeColor = Color.Red;
        }

        public void montaGridMedicos()
        {
            try
            {
                this.Cursor = Cursors.WaitCursor;

                EstudoFacade pcmsoFacade = EstudoFacade.getInstance();

                DataSet ds = pcmsoFacade.findMedicoByFilter(medico);
                    
                grd_medico.DataSource = ds.Tables["Medicos"].DefaultView;

                grd_medico.Columns[0].HeaderText = "Id";
                grd_medico.Columns[0].Visible = false;

                grd_medico.Columns[1].HeaderText = "Nome";

                grd_medico.Columns[2].HeaderText = "CRM";
                
                grd_medico.Columns[3].HeaderText = "Situação";
                grd_medico.Columns[3].Visible = false;

                grd_medico.Columns[4].HeaderText = "Telefone";
                
                grd_medico.Columns[5].HeaderText = "Contato";
                
                grd_medico.Columns[6].HeaderText = "Email";
                
                grd_medico.Columns[7].HeaderText = "Endereço";
                grd_medico.Columns[7].Visible = false;

                grd_medico.Columns[8].HeaderText = "Número";
                grd_medico.Columns[8].Visible = false;
                
                grd_medico.Columns[9].HeaderText = "Complemento";
                grd_medico.Columns[9].Visible = false;
                
                grd_medico.Columns[10].HeaderText = "Bairro";
                grd_medico.Columns[10].Visible = false;
                
                grd_medico.Columns[11].HeaderText = "Cep";
                grd_medico.Columns[11].Visible = false;
                
                grd_medico.Columns[12].HeaderText = "UF";
                grd_medico.Columns[12].Visible = false;
                
                grd_medico.Columns[13].HeaderText = "Cidade";
                grd_medico.Columns[13].Visible = false;
                
                grd_medico.Columns[14].HeaderText = "Pis Pasep";
                grd_medico.Columns[14].Visible = false;

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }

        private void validaPermissoes()
        {
            PermissionamentoFacade permissionamentoFacade = PermissionamentoFacade.getInstance();

            bt_alterar.Enabled = permissionamentoFacade.hasPermission("MEDICO", "ALTERAR");
            bt_incluir.Enabled = permissionamentoFacade.hasPermission("MEDICO", "INCLUIR");
            bt_excluir.Enabled = permissionamentoFacade.hasPermission("MEDICO", "EXCLUIR");
            bt_detalhar.Enabled = permissionamentoFacade.hasPermission("MEDICO", "DETALHAR");
            btn_reativar.Enabled = permissionamentoFacade.hasPermission("MEDICO", "REATIVAR");
        }

        private void grd_medico_CellMouseDoubleClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            try
            {
                bt_alterar.PerformClick();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }
    }
}
