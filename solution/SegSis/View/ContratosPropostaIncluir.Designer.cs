﻿namespace SWS.View
{
    partial class frmContratosPropostaIncluir
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmContratosPropostaIncluir));
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            this.flpAcao = new System.Windows.Forms.FlowLayoutPanel();
            this.btnGravar = new System.Windows.Forms.Button();
            this.btnImprimir = new System.Windows.Forms.Button();
            this.btnFechar = new System.Windows.Forms.Button();
            this.grbComentario = new System.Windows.Forms.GroupBox();
            this.textComentario = new System.Windows.Forms.TextBox();
            this.btnExcluirProduto = new System.Windows.Forms.Button();
            this.btnIncluirProduto = new System.Windows.Forms.Button();
            this.grbProdutos = new System.Windows.Forms.GroupBox();
            this.dgvProduto = new System.Windows.Forms.DataGridView();
            this.btnExcluirExame = new System.Windows.Forms.Button();
            this.btnIncluirExame = new System.Windows.Forms.Button();
            this.grbExame = new System.Windows.Forms.GroupBox();
            this.dgvExame = new System.Windows.Forms.DataGridView();
            this.btnPreCliente = new System.Windows.Forms.Button();
            this.textPreCliente = new System.Windows.Forms.TextBox();
            this.lblPreCliente = new System.Windows.Forms.TextBox();
            this.textCodigo = new System.Windows.Forms.MaskedTextBox();
            this.btnCliente = new System.Windows.Forms.Button();
            this.textCliente = new System.Windows.Forms.TextBox();
            this.lblCliente = new System.Windows.Forms.TextBox();
            this.dataVencimento = new System.Windows.Forms.DateTimePicker();
            this.lblDataVencimento = new System.Windows.Forms.TextBox();
            this.dataElaboracao = new System.Windows.Forms.DateTimePicker();
            this.lblDataElaboracao = new System.Windows.Forms.TextBox();
            this.lblCodigo = new System.Windows.Forms.TextBox();
            this.btnImprimirClienteProposta = new System.Windows.Forms.Button();
            this.grbFormaPagamento = new System.Windows.Forms.GroupBox();
            this.textFormaPagamento = new System.Windows.Forms.TextBox();
            this.flpProduto = new System.Windows.Forms.FlowLayoutPanel();
            this.flpExame = new System.Windows.Forms.FlowLayoutPanel();
            ((System.ComponentModel.ISupportInitialize)(this.spForm)).BeginInit();
            this.spForm.Panel1.SuspendLayout();
            this.spForm.Panel2.SuspendLayout();
            this.spForm.SuspendLayout();
            this.pnlForm.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.banner)).BeginInit();
            this.flpAcao.SuspendLayout();
            this.grbComentario.SuspendLayout();
            this.grbProdutos.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvProduto)).BeginInit();
            this.grbExame.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvExame)).BeginInit();
            this.grbFormaPagamento.SuspendLayout();
            this.flpProduto.SuspendLayout();
            this.flpExame.SuspendLayout();
            this.SuspendLayout();
            // 
            // spForm
            // 
            // 
            // spForm.Panel1
            // 
            this.spForm.Panel1.Controls.Add(this.flpAcao);
            // 
            // pnlForm
            // 
            this.pnlForm.Controls.Add(this.flpExame);
            this.pnlForm.Controls.Add(this.flpProduto);
            this.pnlForm.Controls.Add(this.lblDataVencimento);
            this.pnlForm.Controls.Add(this.lblDataElaboracao);
            this.pnlForm.Controls.Add(this.grbFormaPagamento);
            this.pnlForm.Controls.Add(this.btnImprimirClienteProposta);
            this.pnlForm.Controls.Add(this.grbComentario);
            this.pnlForm.Controls.Add(this.grbProdutos);
            this.pnlForm.Controls.Add(this.grbExame);
            this.pnlForm.Controls.Add(this.btnPreCliente);
            this.pnlForm.Controls.Add(this.textPreCliente);
            this.pnlForm.Controls.Add(this.lblPreCliente);
            this.pnlForm.Controls.Add(this.textCodigo);
            this.pnlForm.Controls.Add(this.btnCliente);
            this.pnlForm.Controls.Add(this.textCliente);
            this.pnlForm.Controls.Add(this.lblCliente);
            this.pnlForm.Controls.Add(this.dataVencimento);
            this.pnlForm.Controls.Add(this.dataElaboracao);
            this.pnlForm.Controls.Add(this.lblCodigo);
            this.pnlForm.Size = new System.Drawing.Size(764, 1000);
            // 
            // flpAcao
            // 
            this.flpAcao.Controls.Add(this.btnGravar);
            this.flpAcao.Controls.Add(this.btnImprimir);
            this.flpAcao.Controls.Add(this.btnFechar);
            this.flpAcao.Dock = System.Windows.Forms.DockStyle.Fill;
            this.flpAcao.Location = new System.Drawing.Point(0, 0);
            this.flpAcao.Name = "flpAcao";
            this.flpAcao.Size = new System.Drawing.Size(784, 35);
            this.flpAcao.TabIndex = 0;
            // 
            // btnGravar
            // 
            this.btnGravar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnGravar.Image = global::SWS.Properties.Resources.Gravar;
            this.btnGravar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnGravar.Location = new System.Drawing.Point(3, 3);
            this.btnGravar.Name = "btnGravar";
            this.btnGravar.Size = new System.Drawing.Size(75, 23);
            this.btnGravar.TabIndex = 15;
            this.btnGravar.TabStop = false;
            this.btnGravar.Text = "&Gravar";
            this.btnGravar.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnGravar.UseVisualStyleBackColor = true;
            this.btnGravar.Click += new System.EventHandler(this.btnGravar_Click);
            // 
            // btnImprimir
            // 
            this.btnImprimir.Enabled = false;
            this.btnImprimir.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnImprimir.Image = global::SWS.Properties.Resources.impressora1;
            this.btnImprimir.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnImprimir.Location = new System.Drawing.Point(84, 3);
            this.btnImprimir.Name = "btnImprimir";
            this.btnImprimir.Size = new System.Drawing.Size(75, 23);
            this.btnImprimir.TabIndex = 18;
            this.btnImprimir.Text = "&Imprimir";
            this.btnImprimir.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnImprimir.UseVisualStyleBackColor = true;
            // 
            // btnFechar
            // 
            this.btnFechar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnFechar.Image = global::SWS.Properties.Resources.close;
            this.btnFechar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnFechar.Location = new System.Drawing.Point(165, 3);
            this.btnFechar.Name = "btnFechar";
            this.btnFechar.Size = new System.Drawing.Size(75, 23);
            this.btnFechar.TabIndex = 17;
            this.btnFechar.TabStop = false;
            this.btnFechar.Text = "&Fechar";
            this.btnFechar.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnFechar.UseVisualStyleBackColor = true;
            this.btnFechar.Click += new System.EventHandler(this.btnFechar_Click);
            // 
            // grbComentario
            // 
            this.grbComentario.BackColor = System.Drawing.Color.White;
            this.grbComentario.Controls.Add(this.textComentario);
            this.grbComentario.Location = new System.Drawing.Point(12, 673);
            this.grbComentario.Name = "grbComentario";
            this.grbComentario.Size = new System.Drawing.Size(726, 107);
            this.grbComentario.TabIndex = 48;
            this.grbComentario.TabStop = false;
            this.grbComentario.Text = "Comentário";
            // 
            // textComentario
            // 
            this.textComentario.BackColor = System.Drawing.Color.LightSteelBlue;
            this.textComentario.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.textComentario.Dock = System.Windows.Forms.DockStyle.Fill;
            this.textComentario.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textComentario.Location = new System.Drawing.Point(3, 16);
            this.textComentario.Multiline = true;
            this.textComentario.Name = "textComentario";
            this.textComentario.Size = new System.Drawing.Size(720, 88);
            this.textComentario.TabIndex = 22;
            this.textComentario.Enter += new System.EventHandler(this.textComentario_Enter);
            this.textComentario.Leave += new System.EventHandler(this.textComentario_Leave);
            // 
            // btnExcluirProduto
            // 
            this.btnExcluirProduto.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnExcluirProduto.Image = ((System.Drawing.Image)(resources.GetObject("btnExcluirProduto.Image")));
            this.btnExcluirProduto.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnExcluirProduto.Location = new System.Drawing.Point(84, 3);
            this.btnExcluirProduto.Name = "btnExcluirProduto";
            this.btnExcluirProduto.Size = new System.Drawing.Size(75, 23);
            this.btnExcluirProduto.TabIndex = 47;
            this.btnExcluirProduto.Text = "&Excluir";
            this.btnExcluirProduto.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnExcluirProduto.UseVisualStyleBackColor = true;
            this.btnExcluirProduto.Click += new System.EventHandler(this.btnExcluirProduto_Click);
            // 
            // btnIncluirProduto
            // 
            this.btnIncluirProduto.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnIncluirProduto.Image = ((System.Drawing.Image)(resources.GetObject("btnIncluirProduto.Image")));
            this.btnIncluirProduto.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnIncluirProduto.Location = new System.Drawing.Point(3, 3);
            this.btnIncluirProduto.Name = "btnIncluirProduto";
            this.btnIncluirProduto.Size = new System.Drawing.Size(75, 23);
            this.btnIncluirProduto.TabIndex = 46;
            this.btnIncluirProduto.Text = "&Incluir";
            this.btnIncluirProduto.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnIncluirProduto.UseVisualStyleBackColor = true;
            this.btnIncluirProduto.Click += new System.EventHandler(this.btnIncluirProduto_Click);
            // 
            // grbProdutos
            // 
            this.grbProdutos.Controls.Add(this.dgvProduto);
            this.grbProdutos.Location = new System.Drawing.Point(12, 393);
            this.grbProdutos.Name = "grbProdutos";
            this.grbProdutos.Size = new System.Drawing.Size(726, 237);
            this.grbProdutos.TabIndex = 45;
            this.grbProdutos.TabStop = false;
            this.grbProdutos.Text = "Produtos";
            // 
            // dgvProduto
            // 
            this.dgvProduto.AllowUserToAddRows = false;
            this.dgvProduto.AllowUserToDeleteRows = false;
            this.dgvProduto.AllowUserToOrderColumns = true;
            this.dgvProduto.AllowUserToResizeRows = false;
            this.dgvProduto.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.DisplayedCells;
            this.dgvProduto.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.DisplayedCells;
            this.dgvProduto.BackgroundColor = System.Drawing.Color.White;
            this.dgvProduto.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle5.BackColor = System.Drawing.Color.LightGray;
            dataGridViewCellStyle5.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle5.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle5.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle5.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle5.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvProduto.DefaultCellStyle = dataGridViewCellStyle5;
            this.dgvProduto.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvProduto.Location = new System.Drawing.Point(3, 16);
            this.dgvProduto.MultiSelect = false;
            this.dgvProduto.Name = "dgvProduto";
            this.dgvProduto.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvProduto.Size = new System.Drawing.Size(720, 218);
            this.dgvProduto.TabIndex = 10;
            this.dgvProduto.TabStop = false;
            this.dgvProduto.CellBeginEdit += new System.Windows.Forms.DataGridViewCellCancelEventHandler(this.dgvProduto_CellBeginEdit);
            this.dgvProduto.CellEndEdit += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvProduto_CellEndEdit);
            this.dgvProduto.EditingControlShowing += new System.Windows.Forms.DataGridViewEditingControlShowingEventHandler(this.dgvProduto_EditingControlShowing);
            this.dgvProduto.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.dgvProduto_KeyPress);
            // 
            // btnExcluirExame
            // 
            this.btnExcluirExame.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnExcluirExame.Image = global::SWS.Properties.Resources.lixeira;
            this.btnExcluirExame.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnExcluirExame.Location = new System.Drawing.Point(84, 3);
            this.btnExcluirExame.Name = "btnExcluirExame";
            this.btnExcluirExame.Size = new System.Drawing.Size(75, 23);
            this.btnExcluirExame.TabIndex = 44;
            this.btnExcluirExame.Text = "&Excluir";
            this.btnExcluirExame.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnExcluirExame.UseVisualStyleBackColor = true;
            this.btnExcluirExame.Click += new System.EventHandler(this.btnExcluirExame_Click);
            // 
            // btnIncluirExame
            // 
            this.btnIncluirExame.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnIncluirExame.Image = ((System.Drawing.Image)(resources.GetObject("btnIncluirExame.Image")));
            this.btnIncluirExame.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnIncluirExame.Location = new System.Drawing.Point(3, 3);
            this.btnIncluirExame.Name = "btnIncluirExame";
            this.btnIncluirExame.Size = new System.Drawing.Size(75, 23);
            this.btnIncluirExame.TabIndex = 43;
            this.btnIncluirExame.Text = "&Incluir";
            this.btnIncluirExame.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnIncluirExame.UseVisualStyleBackColor = true;
            this.btnIncluirExame.Click += new System.EventHandler(this.btnIncluirExame_Click);
            // 
            // grbExame
            // 
            this.grbExame.Controls.Add(this.dgvExame);
            this.grbExame.Location = new System.Drawing.Point(12, 113);
            this.grbExame.Name = "grbExame";
            this.grbExame.Size = new System.Drawing.Size(726, 237);
            this.grbExame.TabIndex = 42;
            this.grbExame.TabStop = false;
            this.grbExame.Text = "Exames";
            // 
            // dgvExame
            // 
            this.dgvExame.AllowUserToAddRows = false;
            this.dgvExame.AllowUserToDeleteRows = false;
            this.dgvExame.AllowUserToOrderColumns = true;
            this.dgvExame.AllowUserToResizeRows = false;
            this.dgvExame.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.DisplayedCells;
            this.dgvExame.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.DisplayedCells;
            this.dgvExame.BackgroundColor = System.Drawing.Color.White;
            dataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle6.BackColor = System.Drawing.Color.LightGray;
            dataGridViewCellStyle6.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle6.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle6.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle6.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle6.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvExame.DefaultCellStyle = dataGridViewCellStyle6;
            this.dgvExame.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvExame.Location = new System.Drawing.Point(3, 16);
            this.dgvExame.MultiSelect = false;
            this.dgvExame.Name = "dgvExame";
            this.dgvExame.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvExame.Size = new System.Drawing.Size(720, 218);
            this.dgvExame.TabIndex = 7;
            this.dgvExame.TabStop = false;
            this.dgvExame.CellBeginEdit += new System.Windows.Forms.DataGridViewCellCancelEventHandler(this.dgvExame_CellBeginEdit);
            this.dgvExame.CellEndEdit += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvExame_CellEndEdit);
            this.dgvExame.EditingControlShowing += new System.Windows.Forms.DataGridViewEditingControlShowingEventHandler(this.dgvExame_EditingControlShowing);
            this.dgvExame.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.dgvExame_KeyPress);
            // 
            // btnPreCliente
            // 
            this.btnPreCliente.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnPreCliente.Image = global::SWS.Properties.Resources.busca;
            this.btnPreCliente.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnPreCliente.Location = new System.Drawing.Point(673, 89);
            this.btnPreCliente.Name = "btnPreCliente";
            this.btnPreCliente.Size = new System.Drawing.Size(32, 21);
            this.btnPreCliente.TabIndex = 41;
            this.btnPreCliente.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnPreCliente.UseVisualStyleBackColor = true;
            this.btnPreCliente.Click += new System.EventHandler(this.btnPreCliente_Click);
            // 
            // textPreCliente
            // 
            this.textPreCliente.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.textPreCliente.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textPreCliente.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textPreCliente.Location = new System.Drawing.Point(140, 89);
            this.textPreCliente.Name = "textPreCliente";
            this.textPreCliente.ReadOnly = true;
            this.textPreCliente.Size = new System.Drawing.Size(534, 21);
            this.textPreCliente.TabIndex = 40;
            // 
            // lblPreCliente
            // 
            this.lblPreCliente.BackColor = System.Drawing.Color.LightGray;
            this.lblPreCliente.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblPreCliente.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPreCliente.Location = new System.Drawing.Point(12, 89);
            this.lblPreCliente.Name = "lblPreCliente";
            this.lblPreCliente.ReadOnly = true;
            this.lblPreCliente.Size = new System.Drawing.Size(129, 21);
            this.lblPreCliente.TabIndex = 39;
            this.lblPreCliente.TabStop = false;
            this.lblPreCliente.Text = "Pré Cliente";
            // 
            // textCodigo
            // 
            this.textCodigo.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.textCodigo.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textCodigo.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textCodigo.Location = new System.Drawing.Point(140, 9);
            this.textCodigo.Mask = "9999,99,999999-99";
            this.textCodigo.Name = "textCodigo";
            this.textCodigo.PromptChar = ' ';
            this.textCodigo.ReadOnly = true;
            this.textCodigo.Size = new System.Drawing.Size(596, 21);
            this.textCodigo.TabIndex = 31;
            // 
            // btnCliente
            // 
            this.btnCliente.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnCliente.Image = global::SWS.Properties.Resources.busca;
            this.btnCliente.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnCliente.Location = new System.Drawing.Point(704, 69);
            this.btnCliente.Name = "btnCliente";
            this.btnCliente.Size = new System.Drawing.Size(32, 21);
            this.btnCliente.TabIndex = 38;
            this.btnCliente.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnCliente.UseVisualStyleBackColor = true;
            this.btnCliente.Click += new System.EventHandler(this.btnCliente_Click);
            // 
            // textCliente
            // 
            this.textCliente.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.textCliente.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textCliente.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textCliente.Location = new System.Drawing.Point(140, 69);
            this.textCliente.Name = "textCliente";
            this.textCliente.ReadOnly = true;
            this.textCliente.Size = new System.Drawing.Size(565, 21);
            this.textCliente.TabIndex = 37;
            // 
            // lblCliente
            // 
            this.lblCliente.BackColor = System.Drawing.Color.LightGray;
            this.lblCliente.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblCliente.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCliente.Location = new System.Drawing.Point(12, 69);
            this.lblCliente.Name = "lblCliente";
            this.lblCliente.ReadOnly = true;
            this.lblCliente.Size = new System.Drawing.Size(129, 21);
            this.lblCliente.TabIndex = 36;
            this.lblCliente.TabStop = false;
            this.lblCliente.Text = "Cliente";
            // 
            // dataVencimento
            // 
            this.dataVencimento.Checked = false;
            this.dataVencimento.CustomFormat = "dd/MM/yyyy HH:mm:ss";
            this.dataVencimento.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dataVencimento.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dataVencimento.Location = new System.Drawing.Point(140, 49);
            this.dataVencimento.Name = "dataVencimento";
            this.dataVencimento.ShowCheckBox = true;
            this.dataVencimento.Size = new System.Drawing.Size(596, 21);
            this.dataVencimento.TabIndex = 35;
            // 
            // lblDataVencimento
            // 
            this.lblDataVencimento.BackColor = System.Drawing.Color.LightGray;
            this.lblDataVencimento.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblDataVencimento.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDataVencimento.Location = new System.Drawing.Point(12, 49);
            this.lblDataVencimento.Name = "lblDataVencimento";
            this.lblDataVencimento.ReadOnly = true;
            this.lblDataVencimento.Size = new System.Drawing.Size(129, 21);
            this.lblDataVencimento.TabIndex = 34;
            this.lblDataVencimento.TabStop = false;
            this.lblDataVencimento.Text = "Data de Vencimento";
            // 
            // dataElaboracao
            // 
            this.dataElaboracao.Checked = false;
            this.dataElaboracao.CustomFormat = "dd/MM/yyyy HH:mm:ss";
            this.dataElaboracao.Enabled = false;
            this.dataElaboracao.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dataElaboracao.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dataElaboracao.Location = new System.Drawing.Point(140, 29);
            this.dataElaboracao.Name = "dataElaboracao";
            this.dataElaboracao.ShowCheckBox = true;
            this.dataElaboracao.Size = new System.Drawing.Size(596, 21);
            this.dataElaboracao.TabIndex = 33;
            // 
            // lblDataElaboracao
            // 
            this.lblDataElaboracao.BackColor = System.Drawing.Color.LightGray;
            this.lblDataElaboracao.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblDataElaboracao.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDataElaboracao.Location = new System.Drawing.Point(12, 29);
            this.lblDataElaboracao.Name = "lblDataElaboracao";
            this.lblDataElaboracao.ReadOnly = true;
            this.lblDataElaboracao.Size = new System.Drawing.Size(129, 21);
            this.lblDataElaboracao.TabIndex = 32;
            this.lblDataElaboracao.TabStop = false;
            this.lblDataElaboracao.Text = "Data de elaboração";
            // 
            // lblCodigo
            // 
            this.lblCodigo.BackColor = System.Drawing.Color.LightGray;
            this.lblCodigo.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblCodigo.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCodigo.Location = new System.Drawing.Point(12, 9);
            this.lblCodigo.Name = "lblCodigo";
            this.lblCodigo.ReadOnly = true;
            this.lblCodigo.Size = new System.Drawing.Size(129, 21);
            this.lblCodigo.TabIndex = 30;
            this.lblCodigo.TabStop = false;
            this.lblCodigo.Text = "Código da proposta";
            // 
            // btnImprimirClienteProposta
            // 
            this.btnImprimirClienteProposta.Enabled = false;
            this.btnImprimirClienteProposta.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnImprimirClienteProposta.Image = global::SWS.Properties.Resources.impressora1;
            this.btnImprimirClienteProposta.Location = new System.Drawing.Point(704, 89);
            this.btnImprimirClienteProposta.Name = "btnImprimirClienteProposta";
            this.btnImprimirClienteProposta.Size = new System.Drawing.Size(32, 21);
            this.btnImprimirClienteProposta.TabIndex = 29;
            this.btnImprimirClienteProposta.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnImprimirClienteProposta.UseVisualStyleBackColor = true;
            this.btnImprimirClienteProposta.Click += new System.EventHandler(this.btnImprimirClienteProposta_Click);
            // 
            // grbFormaPagamento
            // 
            this.grbFormaPagamento.BackColor = System.Drawing.Color.White;
            this.grbFormaPagamento.Controls.Add(this.textFormaPagamento);
            this.grbFormaPagamento.Location = new System.Drawing.Point(12, 786);
            this.grbFormaPagamento.Name = "grbFormaPagamento";
            this.grbFormaPagamento.Size = new System.Drawing.Size(726, 160);
            this.grbFormaPagamento.TabIndex = 49;
            this.grbFormaPagamento.TabStop = false;
            this.grbFormaPagamento.Text = "Forma de Pagamento";
            // 
            // textFormaPagamento
            // 
            this.textFormaPagamento.BackColor = System.Drawing.Color.LemonChiffon;
            this.textFormaPagamento.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.textFormaPagamento.Dock = System.Windows.Forms.DockStyle.Fill;
            this.textFormaPagamento.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textFormaPagamento.Location = new System.Drawing.Point(3, 16);
            this.textFormaPagamento.Multiline = true;
            this.textFormaPagamento.Name = "textFormaPagamento";
            this.textFormaPagamento.Size = new System.Drawing.Size(720, 141);
            this.textFormaPagamento.TabIndex = 22;
            this.textFormaPagamento.Enter += new System.EventHandler(this.textFormaPagamento_Enter);
            this.textFormaPagamento.Leave += new System.EventHandler(this.textFormaPagamento_Leave);
            // 
            // flpProduto
            // 
            this.flpProduto.Controls.Add(this.btnIncluirProduto);
            this.flpProduto.Controls.Add(this.btnExcluirProduto);
            this.flpProduto.Location = new System.Drawing.Point(12, 639);
            this.flpProduto.Name = "flpProduto";
            this.flpProduto.Size = new System.Drawing.Size(726, 29);
            this.flpProduto.TabIndex = 50;
            // 
            // flpExame
            // 
            this.flpExame.Controls.Add(this.btnIncluirExame);
            this.flpExame.Controls.Add(this.btnExcluirExame);
            this.flpExame.Location = new System.Drawing.Point(12, 356);
            this.flpExame.Name = "flpExame";
            this.flpExame.Size = new System.Drawing.Size(726, 31);
            this.flpExame.TabIndex = 51;
            // 
            // frmContratosPropostaIncluir
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(784, 561);
            this.Name = "frmContratosPropostaIncluir";
            this.Text = "INCLUIR PROPOSTA ";
            this.Load += new System.EventHandler(this.frmContratosPropostaIncluir_Load);
            this.spForm.Panel1.ResumeLayout(false);
            this.spForm.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.spForm)).EndInit();
            this.spForm.ResumeLayout(false);
            this.pnlForm.ResumeLayout(false);
            this.pnlForm.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.banner)).EndInit();
            this.flpAcao.ResumeLayout(false);
            this.grbComentario.ResumeLayout(false);
            this.grbComentario.PerformLayout();
            this.grbProdutos.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvProduto)).EndInit();
            this.grbExame.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvExame)).EndInit();
            this.grbFormaPagamento.ResumeLayout(false);
            this.grbFormaPagamento.PerformLayout();
            this.flpProduto.ResumeLayout(false);
            this.flpExame.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        protected System.Windows.Forms.Button btnFechar;
        protected System.Windows.Forms.Button btnGravar;
        protected System.Windows.Forms.GroupBox grbComentario;
        protected System.Windows.Forms.TextBox textComentario;
        protected System.Windows.Forms.Button btnExcluirProduto;
        protected System.Windows.Forms.Button btnIncluirProduto;
        protected System.Windows.Forms.GroupBox grbProdutos;
        protected System.Windows.Forms.DataGridView dgvProduto;
        protected System.Windows.Forms.Button btnExcluirExame;
        protected System.Windows.Forms.Button btnIncluirExame;
        protected System.Windows.Forms.GroupBox grbExame;
        protected System.Windows.Forms.DataGridView dgvExame;
        protected System.Windows.Forms.Button btnPreCliente;
        protected System.Windows.Forms.TextBox textPreCliente;
        protected System.Windows.Forms.TextBox lblPreCliente;
        protected System.Windows.Forms.MaskedTextBox textCodigo;
        protected System.Windows.Forms.Button btnCliente;
        protected System.Windows.Forms.TextBox textCliente;
        protected System.Windows.Forms.TextBox lblCliente;
        protected System.Windows.Forms.DateTimePicker dataVencimento;
        protected System.Windows.Forms.TextBox lblDataVencimento;
        protected System.Windows.Forms.DateTimePicker dataElaboracao;
        protected System.Windows.Forms.TextBox lblDataElaboracao;
        protected System.Windows.Forms.TextBox lblCodigo;
        protected System.Windows.Forms.Button btnImprimirClienteProposta;
        protected System.Windows.Forms.GroupBox grbFormaPagamento;
        protected System.Windows.Forms.TextBox textFormaPagamento;
        public System.Windows.Forms.FlowLayoutPanel flpAcao;
        protected System.Windows.Forms.Button btnImprimir;
        private System.Windows.Forms.FlowLayoutPanel flpExame;
        private System.Windows.Forms.FlowLayoutPanel flpProduto;
    }
}