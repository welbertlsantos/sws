﻿namespace SWS.View
{
    partial class frmClienteSelecionar
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            this.flpAcao = new System.Windows.Forms.FlowLayoutPanel();
            this.btn_ok = new System.Windows.Forms.Button();
            this.btn_novo = new System.Windows.Forms.Button();
            this.btn_buscar = new System.Windows.Forms.Button();
            this.btn_fechar = new System.Windows.Forms.Button();
            this.lblSelecaoCombo = new System.Windows.Forms.TextBox();
            this.text_procura = new System.Windows.Forms.MaskedTextBox();
            this.cbTipoPesquisa = new SWS.ComboBoxWithBorder();
            this.lblChave = new System.Windows.Forms.TextBox();
            this.grb_gride = new System.Windows.Forms.GroupBox();
            this.dgvCliente = new System.Windows.Forms.DataGridView();
            ((System.ComponentModel.ISupportInitialize)(this.scForm)).BeginInit();
            this.scForm.Panel1.SuspendLayout();
            this.scForm.Panel2.SuspendLayout();
            this.scForm.SuspendLayout();
            this.pnlForm.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.banner)).BeginInit();
            this.flpAcao.SuspendLayout();
            this.grb_gride.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvCliente)).BeginInit();
            this.SuspendLayout();
            // 
            // scForm
            // 
            // 
            // scForm.Panel1
            // 
            this.scForm.Panel1.Controls.Add(this.flpAcao);
            // 
            // pnlForm
            // 
            this.pnlForm.Controls.Add(this.grb_gride);
            this.pnlForm.Controls.Add(this.lblChave);
            this.pnlForm.Controls.Add(this.cbTipoPesquisa);
            this.pnlForm.Controls.Add(this.text_procura);
            this.pnlForm.Controls.Add(this.lblSelecaoCombo);
            // 
            // flpAcao
            // 
            this.flpAcao.Controls.Add(this.btn_ok);
            this.flpAcao.Controls.Add(this.btn_novo);
            this.flpAcao.Controls.Add(this.btn_buscar);
            this.flpAcao.Controls.Add(this.btn_fechar);
            this.flpAcao.Dock = System.Windows.Forms.DockStyle.Fill;
            this.flpAcao.Location = new System.Drawing.Point(0, 0);
            this.flpAcao.Name = "flpAcao";
            this.flpAcao.Size = new System.Drawing.Size(534, 30);
            this.flpAcao.TabIndex = 0;
            // 
            // btn_ok
            // 
            this.btn_ok.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_ok.Image = global::SWS.Properties.Resources.fechar_ico;
            this.btn_ok.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btn_ok.Location = new System.Drawing.Point(3, 3);
            this.btn_ok.Name = "btn_ok";
            this.btn_ok.Size = new System.Drawing.Size(77, 23);
            this.btn_ok.TabIndex = 8;
            this.btn_ok.TabStop = false;
            this.btn_ok.Text = "&Confirma";
            this.btn_ok.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btn_ok.UseVisualStyleBackColor = true;
            this.btn_ok.Click += new System.EventHandler(this.btn_ok_Click);
            // 
            // btn_novo
            // 
            this.btn_novo.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_novo.Image = global::SWS.Properties.Resources.icone_mais;
            this.btn_novo.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btn_novo.Location = new System.Drawing.Point(86, 3);
            this.btn_novo.Name = "btn_novo";
            this.btn_novo.Size = new System.Drawing.Size(77, 23);
            this.btn_novo.TabIndex = 9;
            this.btn_novo.TabStop = false;
            this.btn_novo.Text = "&Novo";
            this.btn_novo.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btn_novo.UseVisualStyleBackColor = true;
            this.btn_novo.Click += new System.EventHandler(this.btn_novo_Click);
            // 
            // btn_buscar
            // 
            this.btn_buscar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_buscar.Image = global::SWS.Properties.Resources.lupa;
            this.btn_buscar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btn_buscar.Location = new System.Drawing.Point(169, 3);
            this.btn_buscar.Name = "btn_buscar";
            this.btn_buscar.Size = new System.Drawing.Size(77, 23);
            this.btn_buscar.TabIndex = 11;
            this.btn_buscar.TabStop = false;
            this.btn_buscar.Text = "&Buscar";
            this.btn_buscar.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btn_buscar.UseVisualStyleBackColor = true;
            this.btn_buscar.Click += new System.EventHandler(this.btn_buscar_Click);
            // 
            // btn_fechar
            // 
            this.btn_fechar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_fechar.Image = global::SWS.Properties.Resources.close;
            this.btn_fechar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btn_fechar.Location = new System.Drawing.Point(252, 3);
            this.btn_fechar.Name = "btn_fechar";
            this.btn_fechar.Size = new System.Drawing.Size(75, 23);
            this.btn_fechar.TabIndex = 10;
            this.btn_fechar.TabStop = false;
            this.btn_fechar.Text = "&Fechar";
            this.btn_fechar.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btn_fechar.UseVisualStyleBackColor = true;
            this.btn_fechar.Click += new System.EventHandler(this.btn_fechar_Click);
            // 
            // lblSelecaoCombo
            // 
            this.lblSelecaoCombo.BackColor = System.Drawing.Color.LightGray;
            this.lblSelecaoCombo.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblSelecaoCombo.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSelecaoCombo.Location = new System.Drawing.Point(6, 6);
            this.lblSelecaoCombo.Name = "lblSelecaoCombo";
            this.lblSelecaoCombo.ReadOnly = true;
            this.lblSelecaoCombo.Size = new System.Drawing.Size(118, 21);
            this.lblSelecaoCombo.TabIndex = 0;
            this.lblSelecaoCombo.TabStop = false;
            this.lblSelecaoCombo.Text = "Pesquisar por:";
            // 
            // text_procura
            // 
            this.text_procura.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.text_procura.Location = new System.Drawing.Point(123, 26);
            this.text_procura.Name = "text_procura";
            this.text_procura.PromptChar = ' ';
            this.text_procura.Size = new System.Drawing.Size(382, 20);
            this.text_procura.TabIndex = 2;
            this.text_procura.KeyDown += new System.Windows.Forms.KeyEventHandler(this.text_procura_KeyDown);
            this.text_procura.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.text_procura_KeyPress);
            // 
            // cbTipoPesquisa
            // 
            this.cbTipoPesquisa.BackColor = System.Drawing.Color.LightGray;
            this.cbTipoPesquisa.BorderColor = System.Drawing.Color.DimGray;
            this.cbTipoPesquisa.BorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid;
            this.cbTipoPesquisa.FormattingEnabled = true;
            this.cbTipoPesquisa.Location = new System.Drawing.Point(123, 6);
            this.cbTipoPesquisa.Name = "cbTipoPesquisa";
            this.cbTipoPesquisa.Size = new System.Drawing.Size(382, 21);
            this.cbTipoPesquisa.TabIndex = 1;
            this.cbTipoPesquisa.SelectedIndexChanged += new System.EventHandler(this.cbTipoPesquisa_SelectedIndexChanged);
            // 
            // lblChave
            // 
            this.lblChave.BackColor = System.Drawing.Color.LightGray;
            this.lblChave.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblChave.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblChave.Location = new System.Drawing.Point(6, 25);
            this.lblChave.Name = "lblChave";
            this.lblChave.ReadOnly = true;
            this.lblChave.Size = new System.Drawing.Size(118, 21);
            this.lblChave.TabIndex = 4;
            this.lblChave.TabStop = false;
            this.lblChave.Text = "Chave";
            // 
            // grb_gride
            // 
            this.grb_gride.BackColor = System.Drawing.Color.Transparent;
            this.grb_gride.Controls.Add(this.dgvCliente);
            this.grb_gride.Location = new System.Drawing.Point(6, 52);
            this.grb_gride.Name = "grb_gride";
            this.grb_gride.Size = new System.Drawing.Size(505, 222);
            this.grb_gride.TabIndex = 5;
            this.grb_gride.TabStop = false;
            this.grb_gride.Text = "Clientes";
            // 
            // dgvCliente
            // 
            this.dgvCliente.AllowUserToAddRows = false;
            this.dgvCliente.AllowUserToDeleteRows = false;
            this.dgvCliente.AllowUserToOrderColumns = true;
            this.dgvCliente.AllowUserToResizeRows = false;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.Gainsboro;
            this.dgvCliente.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle1;
            this.dgvCliente.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this.dgvCliente.BackgroundColor = System.Drawing.Color.White;
            this.dgvCliente.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.Color.LightGray;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvCliente.DefaultCellStyle = dataGridViewCellStyle2;
            this.dgvCliente.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvCliente.Location = new System.Drawing.Point(3, 16);
            this.dgvCliente.MultiSelect = false;
            this.dgvCliente.Name = "dgvCliente";
            this.dgvCliente.ReadOnly = true;
            this.dgvCliente.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvCliente.Size = new System.Drawing.Size(499, 203);
            this.dgvCliente.TabIndex = 4;
            this.dgvCliente.CellMouseDoubleClick += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.dgvCliente_CellMouseDoubleClick);
            this.dgvCliente.RowPrePaint += new System.Windows.Forms.DataGridViewRowPrePaintEventHandler(this.dgvCliente_RowPrePaint);
            // 
            // frmClienteSelecionar
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(534, 361);
            this.Name = "frmClienteSelecionar";
            this.Text = "SELECIONAR CLIENTE";
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.frmClienteSelecionar_KeyDown);
            this.scForm.Panel1.ResumeLayout(false);
            this.scForm.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.scForm)).EndInit();
            this.scForm.ResumeLayout(false);
            this.pnlForm.ResumeLayout(false);
            this.pnlForm.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.banner)).EndInit();
            this.flpAcao.ResumeLayout(false);
            this.grb_gride.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvCliente)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.FlowLayoutPanel flpAcao;
        private System.Windows.Forms.Button btn_novo;
        private System.Windows.Forms.Button btn_fechar;
        private System.Windows.Forms.Button btn_ok;
        private System.Windows.Forms.Button btn_buscar;
        private System.Windows.Forms.TextBox lblSelecaoCombo;
        private System.Windows.Forms.MaskedTextBox text_procura;
        private System.Windows.Forms.TextBox lblChave;
        private ComboBoxWithBorder cbTipoPesquisa;
        private System.Windows.Forms.GroupBox grb_gride;
        private System.Windows.Forms.DataGridView dgvCliente;
    }
}