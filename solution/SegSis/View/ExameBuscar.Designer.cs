﻿namespace SWS.View
{
    partial class frmExameBuscar
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            this.flpAcao = new System.Windows.Forms.FlowLayoutPanel();
            this.btnIcluir = new System.Windows.Forms.Button();
            this.btnPesquisar = new System.Windows.Forms.Button();
            this.btnNovo = new System.Windows.Forms.Button();
            this.btFechar = new System.Windows.Forms.Button();
            this.textExame = new System.Windows.Forms.TextBox();
            this.lblExame = new System.Windows.Forms.TextBox();
            this.grbExame = new System.Windows.Forms.GroupBox();
            this.dgvExame = new System.Windows.Forms.DataGridView();
            this.lblLaboratorio = new System.Windows.Forms.TextBox();
            this.cbLaboratorio = new SWS.ComboBoxWithBorder();
            this.lblTecla = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.scForm)).BeginInit();
            this.scForm.Panel1.SuspendLayout();
            this.scForm.Panel2.SuspendLayout();
            this.scForm.SuspendLayout();
            this.pnlForm.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.banner)).BeginInit();
            this.flpAcao.SuspendLayout();
            this.grbExame.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvExame)).BeginInit();
            this.SuspendLayout();
            // 
            // scForm
            // 
            // 
            // scForm.Panel1
            // 
            this.scForm.Panel1.Controls.Add(this.flpAcao);
            // 
            // pnlForm
            // 
            this.pnlForm.Controls.Add(this.lblTecla);
            this.pnlForm.Controls.Add(this.cbLaboratorio);
            this.pnlForm.Controls.Add(this.lblLaboratorio);
            this.pnlForm.Controls.Add(this.grbExame);
            this.pnlForm.Controls.Add(this.lblExame);
            this.pnlForm.Controls.Add(this.textExame);
            this.pnlForm.Size = new System.Drawing.Size(514, 296);
            // 
            // flpAcao
            // 
            this.flpAcao.Controls.Add(this.btnIcluir);
            this.flpAcao.Controls.Add(this.btnPesquisar);
            this.flpAcao.Controls.Add(this.btnNovo);
            this.flpAcao.Controls.Add(this.btFechar);
            this.flpAcao.Dock = System.Windows.Forms.DockStyle.Fill;
            this.flpAcao.Location = new System.Drawing.Point(0, 0);
            this.flpAcao.Name = "flpAcao";
            this.flpAcao.Size = new System.Drawing.Size(534, 30);
            this.flpAcao.TabIndex = 0;
            // 
            // btnIcluir
            // 
            this.btnIcluir.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnIcluir.Image = global::SWS.Properties.Resources.fechar_ico;
            this.btnIcluir.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnIcluir.Location = new System.Drawing.Point(3, 3);
            this.btnIcluir.Name = "btnIcluir";
            this.btnIcluir.Size = new System.Drawing.Size(75, 23);
            this.btnIcluir.TabIndex = 3;
            this.btnIcluir.Text = "&Confirma";
            this.btnIcluir.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnIcluir.UseVisualStyleBackColor = true;
            this.btnIcluir.Click += new System.EventHandler(this.btnIcluir_Click);
            // 
            // btnPesquisar
            // 
            this.btnPesquisar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnPesquisar.Image = global::SWS.Properties.Resources.busca;
            this.btnPesquisar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnPesquisar.Location = new System.Drawing.Point(84, 3);
            this.btnPesquisar.Name = "btnPesquisar";
            this.btnPesquisar.Size = new System.Drawing.Size(75, 23);
            this.btnPesquisar.TabIndex = 2;
            this.btnPesquisar.Text = "&Buscar";
            this.btnPesquisar.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnPesquisar.UseVisualStyleBackColor = true;
            this.btnPesquisar.Click += new System.EventHandler(this.btnPesquisar_Click);
            // 
            // btnNovo
            // 
            this.btnNovo.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnNovo.Image = global::SWS.Properties.Resources.icone_mais;
            this.btnNovo.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnNovo.Location = new System.Drawing.Point(165, 3);
            this.btnNovo.Name = "btnNovo";
            this.btnNovo.Size = new System.Drawing.Size(75, 23);
            this.btnNovo.TabIndex = 4;
            this.btnNovo.Text = "&Novo";
            this.btnNovo.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnNovo.UseVisualStyleBackColor = true;
            this.btnNovo.Click += new System.EventHandler(this.btnNovo_Click);
            // 
            // btFechar
            // 
            this.btFechar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btFechar.Image = global::SWS.Properties.Resources.close;
            this.btFechar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btFechar.Location = new System.Drawing.Point(246, 3);
            this.btFechar.Name = "btFechar";
            this.btFechar.Size = new System.Drawing.Size(75, 23);
            this.btFechar.TabIndex = 5;
            this.btFechar.Text = "Fechar";
            this.btFechar.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btFechar.UseVisualStyleBackColor = true;
            this.btFechar.Click += new System.EventHandler(this.btFechar_Click);
            // 
            // textExame
            // 
            this.textExame.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textExame.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.textExame.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textExame.Location = new System.Drawing.Point(99, 16);
            this.textExame.MaxLength = 100;
            this.textExame.Name = "textExame";
            this.textExame.Size = new System.Drawing.Size(407, 21);
            this.textExame.TabIndex = 1;
            // 
            // lblExame
            // 
            this.lblExame.BackColor = System.Drawing.Color.Silver;
            this.lblExame.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblExame.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblExame.Location = new System.Drawing.Point(12, 16);
            this.lblExame.MaxLength = 100;
            this.lblExame.Name = "lblExame";
            this.lblExame.ReadOnly = true;
            this.lblExame.Size = new System.Drawing.Size(88, 21);
            this.lblExame.TabIndex = 3;
            this.lblExame.TabStop = false;
            this.lblExame.Text = "Exame";
            // 
            // grbExame
            // 
            this.grbExame.Controls.Add(this.dgvExame);
            this.grbExame.Location = new System.Drawing.Point(12, 63);
            this.grbExame.Name = "grbExame";
            this.grbExame.Size = new System.Drawing.Size(494, 194);
            this.grbExame.TabIndex = 5;
            this.grbExame.TabStop = false;
            this.grbExame.Text = "Exames";
            // 
            // dgvExame
            // 
            this.dgvExame.AllowUserToAddRows = false;
            this.dgvExame.AllowUserToDeleteRows = false;
            this.dgvExame.AllowUserToOrderColumns = true;
            this.dgvExame.AllowUserToResizeRows = false;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.Gainsboro;
            this.dgvExame.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle1;
            this.dgvExame.BackgroundColor = System.Drawing.Color.White;
            this.dgvExame.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.Color.LightGray;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvExame.DefaultCellStyle = dataGridViewCellStyle2;
            this.dgvExame.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvExame.Location = new System.Drawing.Point(3, 16);
            this.dgvExame.MultiSelect = false;
            this.dgvExame.Name = "dgvExame";
            this.dgvExame.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvExame.Size = new System.Drawing.Size(488, 175);
            this.dgvExame.TabIndex = 3;
            this.dgvExame.CellContentDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvExame_CellContentDoubleClick);
            this.dgvExame.CellDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvExame_CellDoubleClick);
            // 
            // lblLaboratorio
            // 
            this.lblLaboratorio.BackColor = System.Drawing.Color.Silver;
            this.lblLaboratorio.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblLaboratorio.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblLaboratorio.Location = new System.Drawing.Point(12, 36);
            this.lblLaboratorio.MaxLength = 100;
            this.lblLaboratorio.Name = "lblLaboratorio";
            this.lblLaboratorio.ReadOnly = true;
            this.lblLaboratorio.Size = new System.Drawing.Size(88, 21);
            this.lblLaboratorio.TabIndex = 6;
            this.lblLaboratorio.TabStop = false;
            this.lblLaboratorio.Text = "Laboratório";
            // 
            // cbLaboratorio
            // 
            this.cbLaboratorio.BackColor = System.Drawing.Color.LightGray;
            this.cbLaboratorio.BorderColor = System.Drawing.Color.DimGray;
            this.cbLaboratorio.BorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid;
            this.cbLaboratorio.FormattingEnabled = true;
            this.cbLaboratorio.Location = new System.Drawing.Point(99, 36);
            this.cbLaboratorio.Name = "cbLaboratorio";
            this.cbLaboratorio.Size = new System.Drawing.Size(407, 21);
            this.cbLaboratorio.TabIndex = 7;
            // 
            // lblTecla
            // 
            this.lblTecla.AutoSize = true;
            this.lblTecla.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTecla.ForeColor = System.Drawing.Color.Red;
            this.lblTecla.Location = new System.Drawing.Point(12, 264);
            this.lblTecla.Name = "lblTecla";
            this.lblTecla.Size = new System.Drawing.Size(225, 15);
            this.lblTecla.TabIndex = 8;
            this.lblTecla.Text = "Tecle F2 para marcar/descmarcar todos";
            // 
            // frmExameBuscar
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.ClientSize = new System.Drawing.Size(534, 361);
            this.Name = "frmExameBuscar";
            this.Text = "PESQUISAR EXAME";
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.frmExameBuscar_KeyDown);
            this.scForm.Panel1.ResumeLayout(false);
            this.scForm.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.scForm)).EndInit();
            this.scForm.ResumeLayout(false);
            this.pnlForm.ResumeLayout(false);
            this.pnlForm.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.banner)).EndInit();
            this.flpAcao.ResumeLayout(false);
            this.grbExame.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvExame)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.FlowLayoutPanel flpAcao;
        private System.Windows.Forms.Button btnIcluir;
        private System.Windows.Forms.Button btnPesquisar;
        private System.Windows.Forms.Button btnNovo;
        private System.Windows.Forms.Button btFechar;
        private System.Windows.Forms.TextBox lblExame;
        private System.Windows.Forms.TextBox textExame;
        private System.Windows.Forms.GroupBox grbExame;
        private System.Windows.Forms.DataGridView dgvExame;
        private ComboBoxWithBorder cbLaboratorio;
        private System.Windows.Forms.TextBox lblLaboratorio;
        private System.Windows.Forms.Label lblTecla;
    }
}
