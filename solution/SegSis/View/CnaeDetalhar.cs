﻿using SWS.Entidade;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace SWS.View
{
    public partial class frmCnaeDetalhar : frmCnaeIncluir
    {
        public frmCnaeDetalhar(Cnae cnae) :base()
        {
            InitializeComponent();
            this.Cnae = cnae;

            textCodigo.Text = cnae.CodCnae;
            textAtividade.Text = cnae.Atividade;
            cbGrauRisco.SelectedValue = cnae.GrauRisco;
            textGrupo.Text = cnae.Grupo;

            bt_incluir.Enabled = false;
            bt_limpar.Enabled = false;
        }
    }
}
