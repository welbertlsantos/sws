﻿using SWS.Entidade;
using SWS.Facade;
using SWS.View.Resources;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace SWS.View
{
    public partial class frmFonteAlterar : frmFonteIncluir
    {
        public frmFonteAlterar(Fonte fonte) :base()
        {
            InitializeComponent();
            this.Fonte = fonte;
            textDescricao.Text = Fonte.Descricao;
        }

        protected override void btnGravar_Click(object sender, EventArgs e)
        {
            try
            {
                if (ValidaTela())
                {
                    EstudoFacade pcmsoFacade = EstudoFacade.getInstance();

                    Fonte = pcmsoFacade.updateFonte(new Fonte(Fonte.Id, textDescricao.Text, ApplicationConstants.ATIVO, false));
                    MessageBox.Show("Fonte alterada com sucesso.", "Confirmação", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    this.Close();
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }
    }
}
