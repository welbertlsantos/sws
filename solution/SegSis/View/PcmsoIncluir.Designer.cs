﻿namespace SWS.View
{
    partial class frmPcmsoIncluir
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle15 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle14 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle13 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle12 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle10 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle11 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle9 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle8 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle7 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmPcmsoIncluir));
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            this.flpAcao = new System.Windows.Forms.FlowLayoutPanel();
            this.btnGravarNovo = new System.Windows.Forms.Button();
            this.btnObservacao = new System.Windows.Forms.Button();
            this.btnFechar = new System.Windows.Forms.Button();
            this.textCodigoEstudo = new System.Windows.Forms.Label();
            this.lblCliente = new System.Windows.Forms.TextBox();
            this.textCliente = new System.Windows.Forms.TextBox();
            this.btnClienteIncluir = new System.Windows.Forms.Button();
            this.btnClienteExcluir = new System.Windows.Forms.Button();
            this.lblElaborador = new System.Windows.Forms.TextBox();
            this.textElaborador = new System.Windows.Forms.TextBox();
            this.btnElaboradorExcluir = new System.Windows.Forms.Button();
            this.btnElaboradorIncluir = new System.Windows.Forms.Button();
            this.lblVigencia = new System.Windows.Forms.TextBox();
            this.dataCriacao = new System.Windows.Forms.DateTimePicker();
            this.dataValidade = new System.Windows.Forms.DateTimePicker();
            this.lblGrauRisco = new System.Windows.Forms.TextBox();
            this.cbGrauRisco = new SWS.ComboBoxWithBorder();
            this.btnContratanteExcluir = new System.Windows.Forms.Button();
            this.lblContratante = new System.Windows.Forms.TextBox();
            this.textContratante = new System.Windows.Forms.TextBox();
            this.btnContratanteIncluir = new System.Windows.Forms.Button();
            this.lblIncioContrato = new System.Windows.Forms.TextBox();
            this.lblFimContrato = new System.Windows.Forms.TextBox();
            this.textIncioContrato = new System.Windows.Forms.TextBox();
            this.textFimContrato = new System.Windows.Forms.TextBox();
            this.lblNumeroContrato = new System.Windows.Forms.TextBox();
            this.textNumeroContrato = new System.Windows.Forms.TextBox();
            this.lblLocalEstudo = new System.Windows.Forms.TextBox();
            this.textLocalEstudo = new System.Windows.Forms.TextBox();
            this.btnLocalEstudoExcluir = new System.Windows.Forms.Button();
            this.btnLocalEstudoIncluir = new System.Windows.Forms.Button();
            this.btnEstimativaIncluir = new System.Windows.Forms.Button();
            this.btnEstimativaExcluir = new System.Windows.Forms.Button();
            this.grbEstimativa = new System.Windows.Forms.GroupBox();
            this.dgvEstimativa = new System.Windows.Forms.DataGridView();
            this.flpEstimativa = new System.Windows.Forms.FlowLayoutPanel();
            this.flpGhe = new System.Windows.Forms.FlowLayoutPanel();
            this.btnGheIncluir = new System.Windows.Forms.Button();
            this.btnGheAlterar = new System.Windows.Forms.Button();
            this.btnGheExcluir = new System.Windows.Forms.Button();
            this.btnGheDuplicar = new System.Windows.Forms.Button();
            this.grbGhe = new System.Windows.Forms.GroupBox();
            this.dgvGhe = new System.Windows.Forms.DataGridView();
            this.lblExplicacaoFuncao = new System.Windows.Forms.Label();
            this.flpSetor = new System.Windows.Forms.FlowLayoutPanel();
            this.btnSetorIncluir = new System.Windows.Forms.Button();
            this.btnSetorExcluir = new System.Windows.Forms.Button();
            this.btnSetorDuplicar = new System.Windows.Forms.Button();
            this.grbSetor = new System.Windows.Forms.GroupBox();
            this.dgvSetor = new System.Windows.Forms.DataGridView();
            this.flpFuncao = new System.Windows.Forms.FlowLayoutPanel();
            this.btnFuncaoIncluir = new System.Windows.Forms.Button();
            this.btnFuncaoExcluir = new System.Windows.Forms.Button();
            this.grbFuncao = new System.Windows.Forms.GroupBox();
            this.dgvFuncao = new System.Windows.Forms.DataGridView();
            this.lblExplicacaoSegundaParte = new System.Windows.Forms.Label();
            this.flpFonte = new System.Windows.Forms.FlowLayoutPanel();
            this.btnFonteIncluir = new System.Windows.Forms.Button();
            this.btnFonteExcluir = new System.Windows.Forms.Button();
            this.btnFonteDuplicar = new System.Windows.Forms.Button();
            this.grbFonte = new System.Windows.Forms.GroupBox();
            this.dgvFonte = new System.Windows.Forms.DataGridView();
            this.flpAgente = new System.Windows.Forms.FlowLayoutPanel();
            this.btnAgenteIncluir = new System.Windows.Forms.Button();
            this.btnAgenteAlterar = new System.Windows.Forms.Button();
            this.btnAgenteExcluir = new System.Windows.Forms.Button();
            this.btnAgenteDuplicar = new System.Windows.Forms.Button();
            this.grbAgente = new System.Windows.Forms.GroupBox();
            this.dgvAgente = new System.Windows.Forms.DataGridView();
            this.flpExame = new System.Windows.Forms.FlowLayoutPanel();
            this.btnExameIncluir = new System.Windows.Forms.Button();
            this.btnExameAlterar = new System.Windows.Forms.Button();
            this.btnExameExcluir = new System.Windows.Forms.Button();
            this.grbExame = new System.Windows.Forms.GroupBox();
            this.dgvExame = new System.Windows.Forms.DataGridView();
            this.flpPeriodicidade = new System.Windows.Forms.FlowLayoutPanel();
            this.btnPeriodicidadeIncluir = new System.Windows.Forms.Button();
            this.btnPeriodicidadeExcluir = new System.Windows.Forms.Button();
            this.grbPeriodicidade = new System.Windows.Forms.GroupBox();
            this.dgvPeriodicidade = new System.Windows.Forms.DataGridView();
            this.lblExplicacaoTerceiraParte = new System.Windows.Forms.Label();
            this.flpHospital = new System.Windows.Forms.FlowLayoutPanel();
            this.btnHospitalIncluir = new System.Windows.Forms.Button();
            this.btnHospitalExcluir = new System.Windows.Forms.Button();
            this.grbHosptitais = new System.Windows.Forms.GroupBox();
            this.dgvHospitais = new System.Windows.Forms.DataGridView();
            this.flpMedico = new System.Windows.Forms.FlowLayoutPanel();
            this.btnMedicoIncluir = new System.Windows.Forms.Button();
            this.btnMedicoExcluir = new System.Windows.Forms.Button();
            this.grbMedicos = new System.Windows.Forms.GroupBox();
            this.dgvMedico = new System.Windows.Forms.DataGridView();
            this.lblExplicacaoPenultimaParte = new System.Windows.Forms.Label();
            this.flpAtividade = new System.Windows.Forms.FlowLayoutPanel();
            this.btnAtividadeIncluir = new System.Windows.Forms.Button();
            this.btnAtividadeAlterar = new System.Windows.Forms.Button();
            this.btnAtividadeExcluir = new System.Windows.Forms.Button();
            this.grbAtividades = new System.Windows.Forms.GroupBox();
            this.dgvAtividade = new System.Windows.Forms.DataGridView();
            this.lblObrigatorioCliente = new System.Windows.Forms.Label();
            this.lblObrigatorioElaborador = new System.Windows.Forms.Label();
            this.lblObrigatorioVigencia = new System.Windows.Forms.Label();
            this.lblObrigatorioGrauRisco = new System.Windows.Forms.Label();
            this.lblExplicacaoUltimaParte = new System.Windows.Forms.Label();
            this.flpMateriais = new System.Windows.Forms.FlowLayoutPanel();
            this.btnMaterialHospitalarInserir = new System.Windows.Forms.Button();
            this.btnMaterialHospitalarExcluir = new System.Windows.Forms.Button();
            this.tpPcmsoIncluir = new System.Windows.Forms.ToolTip(this.components);
            this.btnDocumentoInserir = new System.Windows.Forms.Button();
            this.btnDocumentoExcluir = new System.Windows.Forms.Button();
            this.grbMaterialHospitalar = new System.Windows.Forms.GroupBox();
            this.dgvMateriais = new System.Windows.Forms.DataGridView();
            this.grbAnexos = new System.Windows.Forms.GroupBox();
            this.dgvDocumentos = new System.Windows.Forms.DataGridView();
            this.flpDocumentos = new System.Windows.Forms.FlowLayoutPanel();
            this.lblCodigo = new System.Windows.Forms.TextBox();
            this.textCodigo = new System.Windows.Forms.MaskedTextBox();
            this.lblCnaeCliente = new System.Windows.Forms.TextBox();
            this.textCnaeCliente = new System.Windows.Forms.TextBox();
            this.lblCnaeContratante = new System.Windows.Forms.TextBox();
            this.textCnaeContratante = new System.Windows.Forms.TextBox();
            ((System.ComponentModel.ISupportInitialize)(this.spForm)).BeginInit();
            this.spForm.Panel1.SuspendLayout();
            this.spForm.Panel2.SuspendLayout();
            this.spForm.SuspendLayout();
            this.pnlForm.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.banner)).BeginInit();
            this.flpAcao.SuspendLayout();
            this.grbEstimativa.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvEstimativa)).BeginInit();
            this.flpEstimativa.SuspendLayout();
            this.flpGhe.SuspendLayout();
            this.grbGhe.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvGhe)).BeginInit();
            this.flpSetor.SuspendLayout();
            this.grbSetor.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvSetor)).BeginInit();
            this.flpFuncao.SuspendLayout();
            this.grbFuncao.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvFuncao)).BeginInit();
            this.flpFonte.SuspendLayout();
            this.grbFonte.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvFonte)).BeginInit();
            this.flpAgente.SuspendLayout();
            this.grbAgente.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvAgente)).BeginInit();
            this.flpExame.SuspendLayout();
            this.grbExame.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvExame)).BeginInit();
            this.flpPeriodicidade.SuspendLayout();
            this.grbPeriodicidade.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvPeriodicidade)).BeginInit();
            this.flpHospital.SuspendLayout();
            this.grbHosptitais.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvHospitais)).BeginInit();
            this.flpMedico.SuspendLayout();
            this.grbMedicos.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvMedico)).BeginInit();
            this.flpAtividade.SuspendLayout();
            this.grbAtividades.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvAtividade)).BeginInit();
            this.flpMateriais.SuspendLayout();
            this.grbMaterialHospitalar.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvMateriais)).BeginInit();
            this.grbAnexos.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvDocumentos)).BeginInit();
            this.flpDocumentos.SuspendLayout();
            this.SuspendLayout();
            // 
            // spForm
            // 
            this.spForm.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            // 
            // spForm.Panel1
            // 
            this.spForm.Panel1.Controls.Add(this.flpAcao);
            // 
            // pnlForm
            // 
            this.pnlForm.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.pnlForm.AutoScroll = true;
            this.pnlForm.Controls.Add(this.textCnaeContratante);
            this.pnlForm.Controls.Add(this.lblCnaeContratante);
            this.pnlForm.Controls.Add(this.textCnaeCliente);
            this.pnlForm.Controls.Add(this.lblCnaeCliente);
            this.pnlForm.Controls.Add(this.textCodigo);
            this.pnlForm.Controls.Add(this.lblCodigo);
            this.pnlForm.Controls.Add(this.btnClienteIncluir);
            this.pnlForm.Controls.Add(this.grbAnexos);
            this.pnlForm.Controls.Add(this.flpDocumentos);
            this.pnlForm.Controls.Add(this.grbMaterialHospitalar);
            this.pnlForm.Controls.Add(this.lblExplicacaoUltimaParte);
            this.pnlForm.Controls.Add(this.lblObrigatorioGrauRisco);
            this.pnlForm.Controls.Add(this.flpMateriais);
            this.pnlForm.Controls.Add(this.lblObrigatorioVigencia);
            this.pnlForm.Controls.Add(this.lblObrigatorioElaborador);
            this.pnlForm.Controls.Add(this.lblObrigatorioCliente);
            this.pnlForm.Controls.Add(this.flpAtividade);
            this.pnlForm.Controls.Add(this.flpMedico);
            this.pnlForm.Controls.Add(this.grbAtividades);
            this.pnlForm.Controls.Add(this.flpHospital);
            this.pnlForm.Controls.Add(this.lblExplicacaoPenultimaParte);
            this.pnlForm.Controls.Add(this.grbMedicos);
            this.pnlForm.Controls.Add(this.lblExplicacaoTerceiraParte);
            this.pnlForm.Controls.Add(this.grbHosptitais);
            this.pnlForm.Controls.Add(this.flpExame);
            this.pnlForm.Controls.Add(this.flpAgente);
            this.pnlForm.Controls.Add(this.flpFonte);
            this.pnlForm.Controls.Add(this.flpPeriodicidade);
            this.pnlForm.Controls.Add(this.grbPeriodicidade);
            this.pnlForm.Controls.Add(this.grbExame);
            this.pnlForm.Controls.Add(this.grbFonte);
            this.pnlForm.Controls.Add(this.grbAgente);
            this.pnlForm.Controls.Add(this.lblExplicacaoSegundaParte);
            this.pnlForm.Controls.Add(this.flpFuncao);
            this.pnlForm.Controls.Add(this.grbFuncao);
            this.pnlForm.Controls.Add(this.flpSetor);
            this.pnlForm.Controls.Add(this.grbSetor);
            this.pnlForm.Controls.Add(this.lblExplicacaoFuncao);
            this.pnlForm.Controls.Add(this.flpGhe);
            this.pnlForm.Controls.Add(this.grbGhe);
            this.pnlForm.Controls.Add(this.flpEstimativa);
            this.pnlForm.Controls.Add(this.grbEstimativa);
            this.pnlForm.Controls.Add(this.btnLocalEstudoIncluir);
            this.pnlForm.Controls.Add(this.btnLocalEstudoExcluir);
            this.pnlForm.Controls.Add(this.lblLocalEstudo);
            this.pnlForm.Controls.Add(this.textLocalEstudo);
            this.pnlForm.Controls.Add(this.textNumeroContrato);
            this.pnlForm.Controls.Add(this.lblNumeroContrato);
            this.pnlForm.Controls.Add(this.textFimContrato);
            this.pnlForm.Controls.Add(this.textIncioContrato);
            this.pnlForm.Controls.Add(this.lblFimContrato);
            this.pnlForm.Controls.Add(this.lblIncioContrato);
            this.pnlForm.Controls.Add(this.btnContratanteExcluir);
            this.pnlForm.Controls.Add(this.lblContratante);
            this.pnlForm.Controls.Add(this.textContratante);
            this.pnlForm.Controls.Add(this.btnContratanteIncluir);
            this.pnlForm.Controls.Add(this.cbGrauRisco);
            this.pnlForm.Controls.Add(this.lblGrauRisco);
            this.pnlForm.Controls.Add(this.dataCriacao);
            this.pnlForm.Controls.Add(this.dataValidade);
            this.pnlForm.Controls.Add(this.lblVigencia);
            this.pnlForm.Controls.Add(this.btnElaboradorExcluir);
            this.pnlForm.Controls.Add(this.btnElaboradorIncluir);
            this.pnlForm.Controls.Add(this.textElaborador);
            this.pnlForm.Controls.Add(this.lblElaborador);
            this.pnlForm.Controls.Add(this.btnClienteExcluir);
            this.pnlForm.Controls.Add(this.lblCliente);
            this.pnlForm.Controls.Add(this.textCliente);
            this.pnlForm.Controls.Add(this.textCodigoEstudo);
            this.pnlForm.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.pnlForm.Size = new System.Drawing.Size(784, 464);
            // 
            // banner
            // 
            this.banner.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            // 
            // flpAcao
            // 
            this.flpAcao.Controls.Add(this.btnGravarNovo);
            this.flpAcao.Controls.Add(this.btnObservacao);
            this.flpAcao.Controls.Add(this.btnFechar);
            this.flpAcao.Dock = System.Windows.Forms.DockStyle.Fill;
            this.flpAcao.Location = new System.Drawing.Point(0, 0);
            this.flpAcao.Name = "flpAcao";
            this.flpAcao.Size = new System.Drawing.Size(784, 35);
            this.flpAcao.TabIndex = 0;
            // 
            // btnGravarNovo
            // 
            this.btnGravarNovo.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnGravarNovo.Image = global::SWS.Properties.Resources.Gravar;
            this.btnGravarNovo.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnGravarNovo.Location = new System.Drawing.Point(3, 3);
            this.btnGravarNovo.Name = "btnGravarNovo";
            this.btnGravarNovo.Size = new System.Drawing.Size(75, 23);
            this.btnGravarNovo.TabIndex = 13;
            this.btnGravarNovo.TabStop = false;
            this.btnGravarNovo.Text = "&Novo";
            this.btnGravarNovo.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnGravarNovo.UseVisualStyleBackColor = true;
            this.btnGravarNovo.Click += new System.EventHandler(this.btGravar_Click);
            // 
            // btnObservacao
            // 
            this.btnObservacao.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnObservacao.Image = global::SWS.Properties.Resources.copiar;
            this.btnObservacao.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnObservacao.Location = new System.Drawing.Point(84, 3);
            this.btnObservacao.Name = "btnObservacao";
            this.btnObservacao.Size = new System.Drawing.Size(75, 23);
            this.btnObservacao.TabIndex = 12;
            this.btnObservacao.TabStop = false;
            this.btnObservacao.Text = "& Inserir Obs";
            this.btnObservacao.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnObservacao.UseVisualStyleBackColor = true;
            this.btnObservacao.Click += new System.EventHandler(this.btObservacao_Click);
            // 
            // btnFechar
            // 
            this.btnFechar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnFechar.Image = global::SWS.Properties.Resources.close;
            this.btnFechar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnFechar.Location = new System.Drawing.Point(165, 3);
            this.btnFechar.Name = "btnFechar";
            this.btnFechar.Size = new System.Drawing.Size(75, 23);
            this.btnFechar.TabIndex = 11;
            this.btnFechar.TabStop = false;
            this.btnFechar.Text = "&Fechar";
            this.btnFechar.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnFechar.UseVisualStyleBackColor = true;
            this.btnFechar.Click += new System.EventHandler(this.btFechar_Click);
            // 
            // textCodigoEstudo
            // 
            this.textCodigoEstudo.AutoSize = true;
            this.textCodigoEstudo.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textCodigoEstudo.ForeColor = System.Drawing.Color.Red;
            this.textCodigoEstudo.Location = new System.Drawing.Point(585, 19);
            this.textCodigoEstudo.Name = "textCodigoEstudo";
            this.textCodigoEstudo.Size = new System.Drawing.Size(0, 24);
            this.textCodigoEstudo.TabIndex = 0;
            this.textCodigoEstudo.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lblCliente
            // 
            this.lblCliente.BackColor = System.Drawing.Color.LightGray;
            this.lblCliente.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblCliente.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCliente.Location = new System.Drawing.Point(7, 26);
            this.lblCliente.Name = "lblCliente";
            this.lblCliente.ReadOnly = true;
            this.lblCliente.Size = new System.Drawing.Size(162, 21);
            this.lblCliente.TabIndex = 39;
            this.lblCliente.TabStop = false;
            this.lblCliente.Text = "Cliente";
            // 
            // textCliente
            // 
            this.textCliente.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.textCliente.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.textCliente.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textCliente.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textCliente.Location = new System.Drawing.Point(168, 26);
            this.textCliente.MaxLength = 100;
            this.textCliente.Name = "textCliente";
            this.textCliente.ReadOnly = true;
            this.textCliente.Size = new System.Drawing.Size(435, 21);
            this.textCliente.TabIndex = 38;
            this.textCliente.TabStop = false;
            // 
            // btnClienteIncluir
            // 
            this.btnClienteIncluir.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnClienteIncluir.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnClienteIncluir.Image = global::SWS.Properties.Resources.busca;
            this.btnClienteIncluir.Location = new System.Drawing.Point(601, 26);
            this.btnClienteIncluir.Name = "btnClienteIncluir";
            this.btnClienteIncluir.Size = new System.Drawing.Size(34, 21);
            this.btnClienteIncluir.TabIndex = 37;
            this.btnClienteIncluir.UseVisualStyleBackColor = true;
            this.btnClienteIncluir.Click += new System.EventHandler(this.btnClienteIncluir_Click);
            // 
            // btnClienteExcluir
            // 
            this.btnClienteExcluir.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnClienteExcluir.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnClienteExcluir.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnClienteExcluir.Image = global::SWS.Properties.Resources.close;
            this.btnClienteExcluir.Location = new System.Drawing.Point(634, 26);
            this.btnClienteExcluir.Name = "btnClienteExcluir";
            this.btnClienteExcluir.Size = new System.Drawing.Size(34, 21);
            this.btnClienteExcluir.TabIndex = 69;
            this.btnClienteExcluir.TabStop = false;
            this.btnClienteExcluir.UseVisualStyleBackColor = true;
            this.btnClienteExcluir.Click += new System.EventHandler(this.btnClienteExcluir_Click);
            // 
            // lblElaborador
            // 
            this.lblElaborador.BackColor = System.Drawing.Color.LightGray;
            this.lblElaborador.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblElaborador.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblElaborador.Location = new System.Drawing.Point(7, 66);
            this.lblElaborador.Name = "lblElaborador";
            this.lblElaborador.ReadOnly = true;
            this.lblElaborador.Size = new System.Drawing.Size(162, 21);
            this.lblElaborador.TabIndex = 70;
            this.lblElaborador.TabStop = false;
            this.lblElaborador.Text = "Elaborador";
            // 
            // textElaborador
            // 
            this.textElaborador.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.textElaborador.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.textElaborador.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textElaborador.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textElaborador.Location = new System.Drawing.Point(168, 66);
            this.textElaborador.MaxLength = 100;
            this.textElaborador.Name = "textElaborador";
            this.textElaborador.ReadOnly = true;
            this.textElaborador.Size = new System.Drawing.Size(435, 21);
            this.textElaborador.TabIndex = 71;
            this.textElaborador.TabStop = false;
            // 
            // btnElaboradorExcluir
            // 
            this.btnElaboradorExcluir.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnElaboradorExcluir.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnElaboradorExcluir.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnElaboradorExcluir.Image = global::SWS.Properties.Resources.close;
            this.btnElaboradorExcluir.Location = new System.Drawing.Point(634, 66);
            this.btnElaboradorExcluir.Name = "btnElaboradorExcluir";
            this.btnElaboradorExcluir.Size = new System.Drawing.Size(34, 21);
            this.btnElaboradorExcluir.TabIndex = 73;
            this.btnElaboradorExcluir.TabStop = false;
            this.btnElaboradorExcluir.UseVisualStyleBackColor = true;
            this.btnElaboradorExcluir.Click += new System.EventHandler(this.btnElaboradorExcluir_Click);
            // 
            // btnElaboradorIncluir
            // 
            this.btnElaboradorIncluir.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnElaboradorIncluir.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnElaboradorIncluir.Image = global::SWS.Properties.Resources.busca;
            this.btnElaboradorIncluir.Location = new System.Drawing.Point(601, 66);
            this.btnElaboradorIncluir.Name = "btnElaboradorIncluir";
            this.btnElaboradorIncluir.Size = new System.Drawing.Size(34, 21);
            this.btnElaboradorIncluir.TabIndex = 72;
            this.btnElaboradorIncluir.UseVisualStyleBackColor = true;
            this.btnElaboradorIncluir.Click += new System.EventHandler(this.btnElaboradorIncluir_Click);
            // 
            // lblVigencia
            // 
            this.lblVigencia.BackColor = System.Drawing.Color.LightGray;
            this.lblVigencia.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblVigencia.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblVigencia.Location = new System.Drawing.Point(7, 86);
            this.lblVigencia.Name = "lblVigencia";
            this.lblVigencia.ReadOnly = true;
            this.lblVigencia.Size = new System.Drawing.Size(162, 21);
            this.lblVigencia.TabIndex = 74;
            this.lblVigencia.TabStop = false;
            this.lblVigencia.Text = "Vigência";
            // 
            // dataCriacao
            // 
            this.dataCriacao.Checked = false;
            this.dataCriacao.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dataCriacao.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dataCriacao.Location = new System.Drawing.Point(168, 86);
            this.dataCriacao.Name = "dataCriacao";
            this.dataCriacao.ShowCheckBox = true;
            this.dataCriacao.Size = new System.Drawing.Size(120, 21);
            this.dataCriacao.TabIndex = 79;
            // 
            // dataValidade
            // 
            this.dataValidade.Checked = false;
            this.dataValidade.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dataValidade.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dataValidade.Location = new System.Drawing.Point(287, 86);
            this.dataValidade.Name = "dataValidade";
            this.dataValidade.ShowCheckBox = true;
            this.dataValidade.Size = new System.Drawing.Size(120, 21);
            this.dataValidade.TabIndex = 80;
            // 
            // lblGrauRisco
            // 
            this.lblGrauRisco.BackColor = System.Drawing.Color.LightGray;
            this.lblGrauRisco.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblGrauRisco.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblGrauRisco.Location = new System.Drawing.Point(7, 106);
            this.lblGrauRisco.Name = "lblGrauRisco";
            this.lblGrauRisco.ReadOnly = true;
            this.lblGrauRisco.Size = new System.Drawing.Size(162, 21);
            this.lblGrauRisco.TabIndex = 81;
            this.lblGrauRisco.TabStop = false;
            this.lblGrauRisco.Text = "Grau de Risco";
            // 
            // cbGrauRisco
            // 
            this.cbGrauRisco.BackColor = System.Drawing.Color.White;
            this.cbGrauRisco.BorderColor = System.Drawing.Color.DimGray;
            this.cbGrauRisco.BorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid;
            this.cbGrauRisco.FormattingEnabled = true;
            this.cbGrauRisco.Location = new System.Drawing.Point(168, 106);
            this.cbGrauRisco.Name = "cbGrauRisco";
            this.cbGrauRisco.Size = new System.Drawing.Size(239, 21);
            this.cbGrauRisco.TabIndex = 82;
            // 
            // btnContratanteExcluir
            // 
            this.btnContratanteExcluir.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnContratanteExcluir.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnContratanteExcluir.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnContratanteExcluir.Image = global::SWS.Properties.Resources.close;
            this.btnContratanteExcluir.Location = new System.Drawing.Point(634, 133);
            this.btnContratanteExcluir.Name = "btnContratanteExcluir";
            this.btnContratanteExcluir.Size = new System.Drawing.Size(34, 21);
            this.btnContratanteExcluir.TabIndex = 86;
            this.btnContratanteExcluir.TabStop = false;
            this.btnContratanteExcluir.UseVisualStyleBackColor = true;
            this.btnContratanteExcluir.Click += new System.EventHandler(this.btnContratanteExcluir_Click);
            // 
            // lblContratante
            // 
            this.lblContratante.BackColor = System.Drawing.Color.LightGray;
            this.lblContratante.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblContratante.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblContratante.Location = new System.Drawing.Point(7, 133);
            this.lblContratante.Name = "lblContratante";
            this.lblContratante.ReadOnly = true;
            this.lblContratante.Size = new System.Drawing.Size(162, 21);
            this.lblContratante.TabIndex = 85;
            this.lblContratante.TabStop = false;
            this.lblContratante.Text = "Contratante";
            // 
            // textContratante
            // 
            this.textContratante.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.textContratante.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.textContratante.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textContratante.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textContratante.Location = new System.Drawing.Point(168, 133);
            this.textContratante.MaxLength = 100;
            this.textContratante.Name = "textContratante";
            this.textContratante.ReadOnly = true;
            this.textContratante.Size = new System.Drawing.Size(435, 21);
            this.textContratante.TabIndex = 84;
            this.textContratante.TabStop = false;
            // 
            // btnContratanteIncluir
            // 
            this.btnContratanteIncluir.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnContratanteIncluir.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnContratanteIncluir.Image = global::SWS.Properties.Resources.busca;
            this.btnContratanteIncluir.Location = new System.Drawing.Point(601, 133);
            this.btnContratanteIncluir.Name = "btnContratanteIncluir";
            this.btnContratanteIncluir.Size = new System.Drawing.Size(34, 21);
            this.btnContratanteIncluir.TabIndex = 83;
            this.btnContratanteIncluir.UseVisualStyleBackColor = true;
            this.btnContratanteIncluir.Click += new System.EventHandler(this.btnContratanteIncluir_Click);
            // 
            // lblIncioContrato
            // 
            this.lblIncioContrato.BackColor = System.Drawing.Color.LightGray;
            this.lblIncioContrato.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblIncioContrato.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblIncioContrato.Location = new System.Drawing.Point(7, 173);
            this.lblIncioContrato.Name = "lblIncioContrato";
            this.lblIncioContrato.ReadOnly = true;
            this.lblIncioContrato.Size = new System.Drawing.Size(162, 21);
            this.lblIncioContrato.TabIndex = 87;
            this.lblIncioContrato.TabStop = false;
            this.lblIncioContrato.Text = "Início Contrato";
            // 
            // lblFimContrato
            // 
            this.lblFimContrato.BackColor = System.Drawing.Color.LightGray;
            this.lblFimContrato.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblFimContrato.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblFimContrato.Location = new System.Drawing.Point(7, 193);
            this.lblFimContrato.Name = "lblFimContrato";
            this.lblFimContrato.ReadOnly = true;
            this.lblFimContrato.Size = new System.Drawing.Size(162, 21);
            this.lblFimContrato.TabIndex = 88;
            this.lblFimContrato.TabStop = false;
            this.lblFimContrato.Text = "Fim Contrato";
            // 
            // textIncioContrato
            // 
            this.textIncioContrato.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.textIncioContrato.BackColor = System.Drawing.Color.White;
            this.textIncioContrato.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textIncioContrato.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.textIncioContrato.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textIncioContrato.ForeColor = System.Drawing.Color.Black;
            this.textIncioContrato.Location = new System.Drawing.Point(168, 173);
            this.textIncioContrato.MaxLength = 15;
            this.textIncioContrato.Name = "textIncioContrato";
            this.textIncioContrato.Size = new System.Drawing.Size(500, 21);
            this.textIncioContrato.TabIndex = 89;
            // 
            // textFimContrato
            // 
            this.textFimContrato.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.textFimContrato.BackColor = System.Drawing.Color.White;
            this.textFimContrato.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textFimContrato.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.textFimContrato.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textFimContrato.ForeColor = System.Drawing.Color.Black;
            this.textFimContrato.Location = new System.Drawing.Point(168, 193);
            this.textFimContrato.MaxLength = 15;
            this.textFimContrato.Name = "textFimContrato";
            this.textFimContrato.Size = new System.Drawing.Size(500, 21);
            this.textFimContrato.TabIndex = 90;
            // 
            // lblNumeroContrato
            // 
            this.lblNumeroContrato.BackColor = System.Drawing.Color.LightGray;
            this.lblNumeroContrato.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblNumeroContrato.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNumeroContrato.Location = new System.Drawing.Point(7, 213);
            this.lblNumeroContrato.Name = "lblNumeroContrato";
            this.lblNumeroContrato.ReadOnly = true;
            this.lblNumeroContrato.Size = new System.Drawing.Size(162, 21);
            this.lblNumeroContrato.TabIndex = 91;
            this.lblNumeroContrato.TabStop = false;
            this.lblNumeroContrato.Text = "Número Contrato";
            // 
            // textNumeroContrato
            // 
            this.textNumeroContrato.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.textNumeroContrato.BackColor = System.Drawing.Color.White;
            this.textNumeroContrato.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textNumeroContrato.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.textNumeroContrato.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textNumeroContrato.ForeColor = System.Drawing.Color.Black;
            this.textNumeroContrato.Location = new System.Drawing.Point(168, 213);
            this.textNumeroContrato.MaxLength = 15;
            this.textNumeroContrato.Name = "textNumeroContrato";
            this.textNumeroContrato.Size = new System.Drawing.Size(500, 21);
            this.textNumeroContrato.TabIndex = 92;
            // 
            // lblLocalEstudo
            // 
            this.lblLocalEstudo.BackColor = System.Drawing.Color.LightGray;
            this.lblLocalEstudo.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblLocalEstudo.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblLocalEstudo.Location = new System.Drawing.Point(7, 233);
            this.lblLocalEstudo.Name = "lblLocalEstudo";
            this.lblLocalEstudo.ReadOnly = true;
            this.lblLocalEstudo.Size = new System.Drawing.Size(162, 21);
            this.lblLocalEstudo.TabIndex = 94;
            this.lblLocalEstudo.TabStop = false;
            this.lblLocalEstudo.Text = "Local / Obra";
            // 
            // textLocalEstudo
            // 
            this.textLocalEstudo.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.textLocalEstudo.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.textLocalEstudo.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textLocalEstudo.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textLocalEstudo.Location = new System.Drawing.Point(168, 233);
            this.textLocalEstudo.MaxLength = 100;
            this.textLocalEstudo.Name = "textLocalEstudo";
            this.textLocalEstudo.ReadOnly = true;
            this.textLocalEstudo.Size = new System.Drawing.Size(486, 21);
            this.textLocalEstudo.TabIndex = 93;
            this.textLocalEstudo.TabStop = false;
            // 
            // btnLocalEstudoExcluir
            // 
            this.btnLocalEstudoExcluir.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnLocalEstudoExcluir.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnLocalEstudoExcluir.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnLocalEstudoExcluir.Image = global::SWS.Properties.Resources.close;
            this.btnLocalEstudoExcluir.Location = new System.Drawing.Point(634, 233);
            this.btnLocalEstudoExcluir.Name = "btnLocalEstudoExcluir";
            this.btnLocalEstudoExcluir.Size = new System.Drawing.Size(34, 21);
            this.btnLocalEstudoExcluir.TabIndex = 96;
            this.btnLocalEstudoExcluir.TabStop = false;
            this.btnLocalEstudoExcluir.UseVisualStyleBackColor = true;
            this.btnLocalEstudoExcluir.Click += new System.EventHandler(this.btnLocalEstudoExcluir_Click);
            // 
            // btnLocalEstudoIncluir
            // 
            this.btnLocalEstudoIncluir.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnLocalEstudoIncluir.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnLocalEstudoIncluir.Image = global::SWS.Properties.Resources.busca;
            this.btnLocalEstudoIncluir.Location = new System.Drawing.Point(601, 233);
            this.btnLocalEstudoIncluir.Name = "btnLocalEstudoIncluir";
            this.btnLocalEstudoIncluir.Size = new System.Drawing.Size(34, 21);
            this.btnLocalEstudoIncluir.TabIndex = 97;
            this.btnLocalEstudoIncluir.UseVisualStyleBackColor = true;
            this.btnLocalEstudoIncluir.Click += new System.EventHandler(this.btnLocalEstudoIncluir_Click);
            // 
            // btnEstimativaIncluir
            // 
            this.btnEstimativaIncluir.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnEstimativaIncluir.Image = ((System.Drawing.Image)(resources.GetObject("btnEstimativaIncluir.Image")));
            this.btnEstimativaIncluir.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnEstimativaIncluir.Location = new System.Drawing.Point(3, 3);
            this.btnEstimativaIncluir.Name = "btnEstimativaIncluir";
            this.btnEstimativaIncluir.Size = new System.Drawing.Size(75, 23);
            this.btnEstimativaIncluir.TabIndex = 98;
            this.btnEstimativaIncluir.Text = "&Incluir";
            this.btnEstimativaIncluir.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnEstimativaIncluir.UseVisualStyleBackColor = true;
            this.btnEstimativaIncluir.Click += new System.EventHandler(this.btnEstimativaIncluir_Click);
            // 
            // btnEstimativaExcluir
            // 
            this.btnEstimativaExcluir.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnEstimativaExcluir.Image = ((System.Drawing.Image)(resources.GetObject("btnEstimativaExcluir.Image")));
            this.btnEstimativaExcluir.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnEstimativaExcluir.Location = new System.Drawing.Point(84, 3);
            this.btnEstimativaExcluir.Name = "btnEstimativaExcluir";
            this.btnEstimativaExcluir.Size = new System.Drawing.Size(75, 23);
            this.btnEstimativaExcluir.TabIndex = 100;
            this.btnEstimativaExcluir.TabStop = false;
            this.btnEstimativaExcluir.Text = "&Excluir";
            this.btnEstimativaExcluir.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnEstimativaExcluir.UseVisualStyleBackColor = true;
            this.btnEstimativaExcluir.Click += new System.EventHandler(this.btnEstimativaExcluir_Click);
            // 
            // grbEstimativa
            // 
            this.grbEstimativa.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.grbEstimativa.Controls.Add(this.dgvEstimativa);
            this.grbEstimativa.Location = new System.Drawing.Point(7, 260);
            this.grbEstimativa.Name = "grbEstimativa";
            this.grbEstimativa.Size = new System.Drawing.Size(334, 167);
            this.grbEstimativa.TabIndex = 99;
            this.grbEstimativa.TabStop = false;
            this.grbEstimativa.Text = "Estimativa";
            // 
            // dgvEstimativa
            // 
            this.dgvEstimativa.AllowUserToAddRows = false;
            this.dgvEstimativa.AllowUserToDeleteRows = false;
            this.dgvEstimativa.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this.dgvEstimativa.BackgroundColor = System.Drawing.Color.White;
            this.dgvEstimativa.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dataGridViewCellStyle15.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle15.BackColor = System.Drawing.Color.LightGray;
            dataGridViewCellStyle15.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle15.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle15.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle15.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle15.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvEstimativa.DefaultCellStyle = dataGridViewCellStyle15;
            this.dgvEstimativa.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvEstimativa.Location = new System.Drawing.Point(3, 16);
            this.dgvEstimativa.MultiSelect = false;
            this.dgvEstimativa.Name = "dgvEstimativa";
            this.dgvEstimativa.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvEstimativa.Size = new System.Drawing.Size(328, 148);
            this.dgvEstimativa.TabIndex = 1;
            this.dgvEstimativa.TabStop = false;
            this.dgvEstimativa.CellEndEdit += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvEstimativa_CellEndEdit);
            this.dgvEstimativa.DataError += new System.Windows.Forms.DataGridViewDataErrorEventHandler(this.dgvEstimativa_DataError);
            this.dgvEstimativa.EditingControlShowing += new System.Windows.Forms.DataGridViewEditingControlShowingEventHandler(this.dgvEstimativa_EditingControlShowing);
            this.dgvEstimativa.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.dgvEstimativa_KeyPress);
            // 
            // flpEstimativa
            // 
            this.flpEstimativa.Controls.Add(this.btnEstimativaIncluir);
            this.flpEstimativa.Controls.Add(this.btnEstimativaExcluir);
            this.flpEstimativa.Location = new System.Drawing.Point(7, 430);
            this.flpEstimativa.Name = "flpEstimativa";
            this.flpEstimativa.Size = new System.Drawing.Size(334, 34);
            this.flpEstimativa.TabIndex = 101;
            // 
            // flpGhe
            // 
            this.flpGhe.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.flpGhe.Controls.Add(this.btnGheIncluir);
            this.flpGhe.Controls.Add(this.btnGheAlterar);
            this.flpGhe.Controls.Add(this.btnGheExcluir);
            this.flpGhe.Controls.Add(this.btnGheDuplicar);
            this.flpGhe.Location = new System.Drawing.Point(7, 673);
            this.flpGhe.Name = "flpGhe";
            this.flpGhe.Size = new System.Drawing.Size(210, 32);
            this.flpGhe.TabIndex = 103;
            // 
            // btnGheIncluir
            // 
            this.btnGheIncluir.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnGheIncluir.Image = ((System.Drawing.Image)(resources.GetObject("btnGheIncluir.Image")));
            this.btnGheIncluir.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnGheIncluir.Location = new System.Drawing.Point(3, 3);
            this.btnGheIncluir.Name = "btnGheIncluir";
            this.btnGheIncluir.Size = new System.Drawing.Size(75, 23);
            this.btnGheIncluir.TabIndex = 8;
            this.btnGheIncluir.Text = "&Incluir";
            this.btnGheIncluir.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnGheIncluir.UseVisualStyleBackColor = true;
            this.btnGheIncluir.Click += new System.EventHandler(this.btnGheIncluir_Click);
            // 
            // btnGheAlterar
            // 
            this.btnGheAlterar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnGheAlterar.Image = global::SWS.Properties.Resources.Alterar;
            this.btnGheAlterar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnGheAlterar.Location = new System.Drawing.Point(84, 3);
            this.btnGheAlterar.Name = "btnGheAlterar";
            this.btnGheAlterar.Size = new System.Drawing.Size(75, 23);
            this.btnGheAlterar.TabIndex = 8;
            this.btnGheAlterar.TabStop = false;
            this.btnGheAlterar.Text = "&Alterar";
            this.btnGheAlterar.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnGheAlterar.UseVisualStyleBackColor = true;
            this.btnGheAlterar.Click += new System.EventHandler(this.btnGheAlterar_Click);
            // 
            // btnGheExcluir
            // 
            this.btnGheExcluir.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnGheExcluir.Image = ((System.Drawing.Image)(resources.GetObject("btnGheExcluir.Image")));
            this.btnGheExcluir.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnGheExcluir.Location = new System.Drawing.Point(3, 32);
            this.btnGheExcluir.Name = "btnGheExcluir";
            this.btnGheExcluir.Size = new System.Drawing.Size(75, 23);
            this.btnGheExcluir.TabIndex = 49;
            this.btnGheExcluir.TabStop = false;
            this.btnGheExcluir.Text = "&Excluir";
            this.btnGheExcluir.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnGheExcluir.UseVisualStyleBackColor = true;
            this.btnGheExcluir.Click += new System.EventHandler(this.btnGheExcluir_Click);
            // 
            // btnGheDuplicar
            // 
            this.btnGheDuplicar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnGheDuplicar.Image = global::SWS.Properties.Resources.copiar;
            this.btnGheDuplicar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnGheDuplicar.Location = new System.Drawing.Point(84, 32);
            this.btnGheDuplicar.Name = "btnGheDuplicar";
            this.btnGheDuplicar.Size = new System.Drawing.Size(75, 23);
            this.btnGheDuplicar.TabIndex = 50;
            this.btnGheDuplicar.TabStop = false;
            this.btnGheDuplicar.Text = "&Duplicar";
            this.btnGheDuplicar.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnGheDuplicar.UseVisualStyleBackColor = true;
            this.btnGheDuplicar.Click += new System.EventHandler(this.btnGheDuplicar_Click);
            // 
            // grbGhe
            // 
            this.grbGhe.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.grbGhe.Controls.Add(this.dgvGhe);
            this.grbGhe.Location = new System.Drawing.Point(7, 470);
            this.grbGhe.Name = "grbGhe";
            this.grbGhe.Size = new System.Drawing.Size(629, 200);
            this.grbGhe.TabIndex = 102;
            this.grbGhe.TabStop = false;
            this.grbGhe.Text = "GHE";
            // 
            // dgvGhe
            // 
            this.dgvGhe.AllowUserToAddRows = false;
            this.dgvGhe.AllowUserToDeleteRows = false;
            this.dgvGhe.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this.dgvGhe.BackgroundColor = System.Drawing.Color.White;
            this.dgvGhe.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dataGridViewCellStyle14.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle14.BackColor = System.Drawing.Color.LightGray;
            dataGridViewCellStyle14.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle14.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle14.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle14.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle14.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvGhe.DefaultCellStyle = dataGridViewCellStyle14;
            this.dgvGhe.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvGhe.Location = new System.Drawing.Point(3, 16);
            this.dgvGhe.MultiSelect = false;
            this.dgvGhe.Name = "dgvGhe";
            this.dgvGhe.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvGhe.Size = new System.Drawing.Size(623, 181);
            this.dgvGhe.TabIndex = 7;
            this.dgvGhe.TabStop = false;
            this.dgvGhe.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvGhe_CellClick);
            this.dgvGhe.CellEndEdit += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvGhe_CellEndEdit);
            this.dgvGhe.DataBindingComplete += new System.Windows.Forms.DataGridViewBindingCompleteEventHandler(this.dgvGhe_DataBindingComplete);
            this.dgvGhe.DataError += new System.Windows.Forms.DataGridViewDataErrorEventHandler(this.dgvGhe_DataError);
            this.dgvGhe.EditingControlShowing += new System.Windows.Forms.DataGridViewEditingControlShowingEventHandler(this.dgvGhe_EditingControlShowing);
            this.dgvGhe.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.dgvGhe_KeyPress);
            // 
            // lblExplicacaoFuncao
            // 
            this.lblExplicacaoFuncao.AutoSize = true;
            this.lblExplicacaoFuncao.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblExplicacaoFuncao.ForeColor = System.Drawing.Color.Blue;
            this.lblExplicacaoFuncao.Location = new System.Drawing.Point(7, 714);
            this.lblExplicacaoFuncao.Name = "lblExplicacaoFuncao";
            this.lblExplicacaoFuncao.Size = new System.Drawing.Size(682, 45);
            this.lblExplicacaoFuncao.TabIndex = 104;
            this.lblExplicacaoFuncao.Text = resources.GetString("lblExplicacaoFuncao.Text");
            // 
            // flpSetor
            // 
            this.flpSetor.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.flpSetor.Controls.Add(this.btnSetorIncluir);
            this.flpSetor.Controls.Add(this.btnSetorExcluir);
            this.flpSetor.Controls.Add(this.btnSetorDuplicar);
            this.flpSetor.Location = new System.Drawing.Point(7, 982);
            this.flpSetor.Name = "flpSetor";
            this.flpSetor.Size = new System.Drawing.Size(210, 32);
            this.flpSetor.TabIndex = 106;
            // 
            // btnSetorIncluir
            // 
            this.btnSetorIncluir.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnSetorIncluir.Image = ((System.Drawing.Image)(resources.GetObject("btnSetorIncluir.Image")));
            this.btnSetorIncluir.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnSetorIncluir.Location = new System.Drawing.Point(3, 3);
            this.btnSetorIncluir.Name = "btnSetorIncluir";
            this.btnSetorIncluir.Size = new System.Drawing.Size(75, 23);
            this.btnSetorIncluir.TabIndex = 10;
            this.btnSetorIncluir.Text = "&Incluir";
            this.btnSetorIncluir.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.tpPcmsoIncluir.SetToolTip(this.btnSetorIncluir, "Incluir setor no ghe.");
            this.btnSetorIncluir.UseVisualStyleBackColor = true;
            this.btnSetorIncluir.Click += new System.EventHandler(this.btnSetorIncluir_Click);
            // 
            // btnSetorExcluir
            // 
            this.btnSetorExcluir.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnSetorExcluir.Image = ((System.Drawing.Image)(resources.GetObject("btnSetorExcluir.Image")));
            this.btnSetorExcluir.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnSetorExcluir.Location = new System.Drawing.Point(84, 3);
            this.btnSetorExcluir.Name = "btnSetorExcluir";
            this.btnSetorExcluir.Size = new System.Drawing.Size(75, 23);
            this.btnSetorExcluir.TabIndex = 52;
            this.btnSetorExcluir.TabStop = false;
            this.btnSetorExcluir.Text = "&Excluir";
            this.btnSetorExcluir.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.tpPcmsoIncluir.SetToolTip(this.btnSetorExcluir, "Exclui o setor e todo o relacionamento\r\nexistente.");
            this.btnSetorExcluir.UseVisualStyleBackColor = true;
            this.btnSetorExcluir.Click += new System.EventHandler(this.btnSetorExcluir_Click);
            // 
            // btnSetorDuplicar
            // 
            this.btnSetorDuplicar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnSetorDuplicar.Image = global::SWS.Properties.Resources.copiar;
            this.btnSetorDuplicar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnSetorDuplicar.Location = new System.Drawing.Point(3, 32);
            this.btnSetorDuplicar.Name = "btnSetorDuplicar";
            this.btnSetorDuplicar.Size = new System.Drawing.Size(75, 23);
            this.btnSetorDuplicar.TabIndex = 53;
            this.btnSetorDuplicar.TabStop = false;
            this.btnSetorDuplicar.Text = "&Duplicar";
            this.btnSetorDuplicar.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.tpPcmsoIncluir.SetToolTip(this.btnSetorDuplicar, "Duplica o Setor para um novo, \r\ncopiado todo o relacionamento existente.");
            this.btnSetorDuplicar.UseVisualStyleBackColor = true;
            this.btnSetorDuplicar.Click += new System.EventHandler(this.btnSetorDuplicar_Click);
            // 
            // grbSetor
            // 
            this.grbSetor.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.grbSetor.Controls.Add(this.dgvSetor);
            this.grbSetor.Location = new System.Drawing.Point(7, 765);
            this.grbSetor.Name = "grbSetor";
            this.grbSetor.Size = new System.Drawing.Size(629, 214);
            this.grbSetor.TabIndex = 105;
            this.grbSetor.TabStop = false;
            this.grbSetor.Text = "Setores";
            // 
            // dgvSetor
            // 
            this.dgvSetor.AllowUserToAddRows = false;
            this.dgvSetor.AllowUserToDeleteRows = false;
            this.dgvSetor.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this.dgvSetor.BackgroundColor = System.Drawing.Color.White;
            this.dgvSetor.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dataGridViewCellStyle13.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle13.BackColor = System.Drawing.Color.LightGray;
            dataGridViewCellStyle13.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle13.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle13.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle13.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle13.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvSetor.DefaultCellStyle = dataGridViewCellStyle13;
            this.dgvSetor.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvSetor.Location = new System.Drawing.Point(3, 16);
            this.dgvSetor.MultiSelect = false;
            this.dgvSetor.Name = "dgvSetor";
            this.dgvSetor.ReadOnly = true;
            this.dgvSetor.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvSetor.Size = new System.Drawing.Size(623, 195);
            this.dgvSetor.TabIndex = 9;
            this.dgvSetor.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvSetor_CellClick);
            this.dgvSetor.DataBindingComplete += new System.Windows.Forms.DataGridViewBindingCompleteEventHandler(this.dgvSetor_DataBindingComplete);
            // 
            // flpFuncao
            // 
            this.flpFuncao.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.flpFuncao.Controls.Add(this.btnFuncaoIncluir);
            this.flpFuncao.Controls.Add(this.btnFuncaoExcluir);
            this.flpFuncao.Location = new System.Drawing.Point(7, 1226);
            this.flpFuncao.Name = "flpFuncao";
            this.flpFuncao.Size = new System.Drawing.Size(210, 30);
            this.flpFuncao.TabIndex = 108;
            // 
            // btnFuncaoIncluir
            // 
            this.btnFuncaoIncluir.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnFuncaoIncluir.Image = ((System.Drawing.Image)(resources.GetObject("btnFuncaoIncluir.Image")));
            this.btnFuncaoIncluir.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnFuncaoIncluir.Location = new System.Drawing.Point(3, 3);
            this.btnFuncaoIncluir.Name = "btnFuncaoIncluir";
            this.btnFuncaoIncluir.Size = new System.Drawing.Size(75, 23);
            this.btnFuncaoIncluir.TabIndex = 12;
            this.btnFuncaoIncluir.Text = "&Incluir";
            this.btnFuncaoIncluir.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnFuncaoIncluir.UseVisualStyleBackColor = true;
            this.btnFuncaoIncluir.Click += new System.EventHandler(this.btnFuncaoIncluir_Click);
            // 
            // btnFuncaoExcluir
            // 
            this.btnFuncaoExcluir.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnFuncaoExcluir.Image = ((System.Drawing.Image)(resources.GetObject("btnFuncaoExcluir.Image")));
            this.btnFuncaoExcluir.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnFuncaoExcluir.Location = new System.Drawing.Point(84, 3);
            this.btnFuncaoExcluir.Name = "btnFuncaoExcluir";
            this.btnFuncaoExcluir.Size = new System.Drawing.Size(75, 23);
            this.btnFuncaoExcluir.TabIndex = 55;
            this.btnFuncaoExcluir.TabStop = false;
            this.btnFuncaoExcluir.Text = "&Excluir";
            this.btnFuncaoExcluir.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnFuncaoExcluir.UseVisualStyleBackColor = true;
            this.btnFuncaoExcluir.Click += new System.EventHandler(this.btnFuncaoExcluir_Click);
            // 
            // grbFuncao
            // 
            this.grbFuncao.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.grbFuncao.Controls.Add(this.dgvFuncao);
            this.grbFuncao.Location = new System.Drawing.Point(7, 1023);
            this.grbFuncao.Name = "grbFuncao";
            this.grbFuncao.Size = new System.Drawing.Size(629, 200);
            this.grbFuncao.TabIndex = 107;
            this.grbFuncao.TabStop = false;
            this.grbFuncao.Text = "Funções";
            // 
            // dgvFuncao
            // 
            this.dgvFuncao.AllowUserToAddRows = false;
            this.dgvFuncao.AllowUserToDeleteRows = false;
            this.dgvFuncao.BackgroundColor = System.Drawing.Color.White;
            this.dgvFuncao.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dataGridViewCellStyle12.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle12.BackColor = System.Drawing.Color.LightGray;
            dataGridViewCellStyle12.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle12.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle12.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle12.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle12.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvFuncao.DefaultCellStyle = dataGridViewCellStyle12;
            this.dgvFuncao.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvFuncao.Location = new System.Drawing.Point(3, 16);
            this.dgvFuncao.MultiSelect = false;
            this.dgvFuncao.Name = "dgvFuncao";
            this.dgvFuncao.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvFuncao.Size = new System.Drawing.Size(623, 181);
            this.dgvFuncao.TabIndex = 11;
            this.dgvFuncao.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvFuncao_CellContentClick);
            this.dgvFuncao.DataBindingComplete += new System.Windows.Forms.DataGridViewBindingCompleteEventHandler(this.dgvFuncao_DataBindingComplete);
            // 
            // lblExplicacaoSegundaParte
            // 
            this.lblExplicacaoSegundaParte.AutoSize = true;
            this.lblExplicacaoSegundaParte.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblExplicacaoSegundaParte.ForeColor = System.Drawing.Color.Blue;
            this.lblExplicacaoSegundaParte.Location = new System.Drawing.Point(7, 1265);
            this.lblExplicacaoSegundaParte.Name = "lblExplicacaoSegundaParte";
            this.lblExplicacaoSegundaParte.Size = new System.Drawing.Size(521, 45);
            this.lblExplicacaoSegundaParte.TabIndex = 109;
            this.lblExplicacaoSegundaParte.Text = "A segunda parte do PCMSO trata-se de selecionar as fontes\r\ncausadoras dos riscos " +
    "e informar quais os riscos e \r\nos exames aplicados para cada um, além de selecio" +
    "nar qual a periodidade para cada exame.";
            // 
            // flpFonte
            // 
            this.flpFonte.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.flpFonte.Controls.Add(this.btnFonteIncluir);
            this.flpFonte.Controls.Add(this.btnFonteExcluir);
            this.flpFonte.Controls.Add(this.btnFonteDuplicar);
            this.flpFonte.Location = new System.Drawing.Point(7, 1520);
            this.flpFonte.Name = "flpFonte";
            this.flpFonte.Size = new System.Drawing.Size(210, 30);
            this.flpFonte.TabIndex = 111;
            // 
            // btnFonteIncluir
            // 
            this.btnFonteIncluir.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnFonteIncluir.Image = ((System.Drawing.Image)(resources.GetObject("btnFonteIncluir.Image")));
            this.btnFonteIncluir.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnFonteIncluir.Location = new System.Drawing.Point(3, 3);
            this.btnFonteIncluir.Name = "btnFonteIncluir";
            this.btnFonteIncluir.Size = new System.Drawing.Size(75, 23);
            this.btnFonteIncluir.TabIndex = 14;
            this.btnFonteIncluir.Text = "&Incluir";
            this.btnFonteIncluir.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnFonteIncluir.UseVisualStyleBackColor = true;
            this.btnFonteIncluir.Click += new System.EventHandler(this.btnFonteIncluir_Click);
            // 
            // btnFonteExcluir
            // 
            this.btnFonteExcluir.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnFonteExcluir.Image = ((System.Drawing.Image)(resources.GetObject("btnFonteExcluir.Image")));
            this.btnFonteExcluir.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnFonteExcluir.Location = new System.Drawing.Point(84, 3);
            this.btnFonteExcluir.Name = "btnFonteExcluir";
            this.btnFonteExcluir.Size = new System.Drawing.Size(75, 23);
            this.btnFonteExcluir.TabIndex = 15;
            this.btnFonteExcluir.Text = "&Excluir";
            this.btnFonteExcluir.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnFonteExcluir.UseVisualStyleBackColor = true;
            this.btnFonteExcluir.Click += new System.EventHandler(this.btnFonteExcluir_Click);
            // 
            // btnFonteDuplicar
            // 
            this.btnFonteDuplicar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnFonteDuplicar.Image = global::SWS.Properties.Resources.copiar;
            this.btnFonteDuplicar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnFonteDuplicar.Location = new System.Drawing.Point(3, 32);
            this.btnFonteDuplicar.Name = "btnFonteDuplicar";
            this.btnFonteDuplicar.Size = new System.Drawing.Size(75, 23);
            this.btnFonteDuplicar.TabIndex = 16;
            this.btnFonteDuplicar.Text = "&Duplicar";
            this.btnFonteDuplicar.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnFonteDuplicar.UseVisualStyleBackColor = true;
            this.btnFonteDuplicar.Click += new System.EventHandler(this.btnFonteDuplicar_Click);
            // 
            // grbFonte
            // 
            this.grbFonte.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.grbFonte.Controls.Add(this.dgvFonte);
            this.grbFonte.Location = new System.Drawing.Point(7, 1318);
            this.grbFonte.Name = "grbFonte";
            this.grbFonte.Size = new System.Drawing.Size(629, 199);
            this.grbFonte.TabIndex = 110;
            this.grbFonte.TabStop = false;
            this.grbFonte.Text = "Fontes";
            // 
            // dgvFonte
            // 
            this.dgvFonte.AllowUserToAddRows = false;
            this.dgvFonte.AllowUserToDeleteRows = false;
            this.dgvFonte.AllowUserToOrderColumns = true;
            this.dgvFonte.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this.dgvFonte.BackgroundColor = System.Drawing.Color.White;
            this.dgvFonte.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dataGridViewCellStyle10.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle10.BackColor = System.Drawing.Color.LightGray;
            dataGridViewCellStyle10.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle10.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle10.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle10.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle10.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvFonte.DefaultCellStyle = dataGridViewCellStyle10;
            this.dgvFonte.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvFonte.Location = new System.Drawing.Point(3, 16);
            this.dgvFonte.MultiSelect = false;
            this.dgvFonte.Name = "dgvFonte";
            this.dgvFonte.ReadOnly = true;
            this.dgvFonte.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvFonte.Size = new System.Drawing.Size(623, 180);
            this.dgvFonte.TabIndex = 13;
            this.dgvFonte.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvFonte_CellClick);
            this.dgvFonte.DataBindingComplete += new System.Windows.Forms.DataGridViewBindingCompleteEventHandler(this.dgvFonte_DataBindingComplete);
            // 
            // flpAgente
            // 
            this.flpAgente.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.flpAgente.Controls.Add(this.btnAgenteIncluir);
            this.flpAgente.Controls.Add(this.btnAgenteAlterar);
            this.flpAgente.Controls.Add(this.btnAgenteExcluir);
            this.flpAgente.Controls.Add(this.btnAgenteDuplicar);
            this.flpAgente.Location = new System.Drawing.Point(7, 1764);
            this.flpAgente.Name = "flpAgente";
            this.flpAgente.Size = new System.Drawing.Size(210, 30);
            this.flpAgente.TabIndex = 113;
            // 
            // btnAgenteIncluir
            // 
            this.btnAgenteIncluir.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnAgenteIncluir.Image = ((System.Drawing.Image)(resources.GetObject("btnAgenteIncluir.Image")));
            this.btnAgenteIncluir.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnAgenteIncluir.Location = new System.Drawing.Point(3, 3);
            this.btnAgenteIncluir.Name = "btnAgenteIncluir";
            this.btnAgenteIncluir.Size = new System.Drawing.Size(75, 23);
            this.btnAgenteIncluir.TabIndex = 14;
            this.btnAgenteIncluir.Text = "&Incluir";
            this.btnAgenteIncluir.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnAgenteIncluir.UseVisualStyleBackColor = true;
            this.btnAgenteIncluir.Click += new System.EventHandler(this.btnAgenteIncluir_Click);
            // 
            // btnAgenteAlterar
            // 
            this.btnAgenteAlterar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnAgenteAlterar.Image = global::SWS.Properties.Resources.Alterar;
            this.btnAgenteAlterar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnAgenteAlterar.Location = new System.Drawing.Point(84, 3);
            this.btnAgenteAlterar.Name = "btnAgenteAlterar";
            this.btnAgenteAlterar.Size = new System.Drawing.Size(75, 23);
            this.btnAgenteAlterar.TabIndex = 15;
            this.btnAgenteAlterar.Text = "&Alterar";
            this.btnAgenteAlterar.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnAgenteAlterar.UseVisualStyleBackColor = true;
            this.btnAgenteAlterar.Click += new System.EventHandler(this.btnAgenteAlterar_Click);
            // 
            // btnAgenteExcluir
            // 
            this.btnAgenteExcluir.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnAgenteExcluir.Image = ((System.Drawing.Image)(resources.GetObject("btnAgenteExcluir.Image")));
            this.btnAgenteExcluir.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnAgenteExcluir.Location = new System.Drawing.Point(3, 32);
            this.btnAgenteExcluir.Name = "btnAgenteExcluir";
            this.btnAgenteExcluir.Size = new System.Drawing.Size(75, 23);
            this.btnAgenteExcluir.TabIndex = 16;
            this.btnAgenteExcluir.Text = "&Excluir";
            this.btnAgenteExcluir.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnAgenteExcluir.UseVisualStyleBackColor = true;
            this.btnAgenteExcluir.Click += new System.EventHandler(this.btnAgenteExcluir_Click);
            // 
            // btnAgenteDuplicar
            // 
            this.btnAgenteDuplicar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnAgenteDuplicar.Image = global::SWS.Properties.Resources.copiar;
            this.btnAgenteDuplicar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnAgenteDuplicar.Location = new System.Drawing.Point(84, 32);
            this.btnAgenteDuplicar.Name = "btnAgenteDuplicar";
            this.btnAgenteDuplicar.Size = new System.Drawing.Size(75, 23);
            this.btnAgenteDuplicar.TabIndex = 17;
            this.btnAgenteDuplicar.Text = "&Duplicar";
            this.btnAgenteDuplicar.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnAgenteDuplicar.UseVisualStyleBackColor = true;
            this.btnAgenteDuplicar.Click += new System.EventHandler(this.btnAgenteDuplicar_Click);
            // 
            // grbAgente
            // 
            this.grbAgente.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.grbAgente.Controls.Add(this.dgvAgente);
            this.grbAgente.Location = new System.Drawing.Point(7, 1562);
            this.grbAgente.Name = "grbAgente";
            this.grbAgente.Size = new System.Drawing.Size(626, 199);
            this.grbAgente.TabIndex = 112;
            this.grbAgente.TabStop = false;
            this.grbAgente.Text = "Agentes";
            // 
            // dgvAgente
            // 
            this.dgvAgente.AllowUserToAddRows = false;
            this.dgvAgente.AllowUserToDeleteRows = false;
            this.dgvAgente.AllowUserToOrderColumns = true;
            this.dgvAgente.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this.dgvAgente.BackgroundColor = System.Drawing.Color.White;
            this.dgvAgente.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dataGridViewCellStyle11.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle11.BackColor = System.Drawing.Color.LightGray;
            dataGridViewCellStyle11.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle11.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle11.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle11.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle11.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvAgente.DefaultCellStyle = dataGridViewCellStyle11;
            this.dgvAgente.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvAgente.Location = new System.Drawing.Point(3, 16);
            this.dgvAgente.MultiSelect = false;
            this.dgvAgente.Name = "dgvAgente";
            this.dgvAgente.ReadOnly = true;
            this.dgvAgente.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvAgente.Size = new System.Drawing.Size(620, 180);
            this.dgvAgente.TabIndex = 13;
            this.dgvAgente.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvAgente_CellClick);
            // 
            // flpExame
            // 
            this.flpExame.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.flpExame.Controls.Add(this.btnExameIncluir);
            this.flpExame.Controls.Add(this.btnExameAlterar);
            this.flpExame.Controls.Add(this.btnExameExcluir);
            this.flpExame.Location = new System.Drawing.Point(7, 2011);
            this.flpExame.Name = "flpExame";
            this.flpExame.Size = new System.Drawing.Size(210, 31);
            this.flpExame.TabIndex = 115;
            // 
            // btnExameIncluir
            // 
            this.btnExameIncluir.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnExameIncluir.Image = ((System.Drawing.Image)(resources.GetObject("btnExameIncluir.Image")));
            this.btnExameIncluir.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnExameIncluir.Location = new System.Drawing.Point(3, 3);
            this.btnExameIncluir.Name = "btnExameIncluir";
            this.btnExameIncluir.Size = new System.Drawing.Size(75, 23);
            this.btnExameIncluir.TabIndex = 17;
            this.btnExameIncluir.Text = "&Incluir";
            this.btnExameIncluir.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnExameIncluir.UseVisualStyleBackColor = true;
            this.btnExameIncluir.Click += new System.EventHandler(this.btnExameIncluir_Click);
            // 
            // btnExameAlterar
            // 
            this.btnExameAlterar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnExameAlterar.Image = global::SWS.Properties.Resources.Alterar;
            this.btnExameAlterar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnExameAlterar.Location = new System.Drawing.Point(84, 3);
            this.btnExameAlterar.Name = "btnExameAlterar";
            this.btnExameAlterar.Size = new System.Drawing.Size(75, 23);
            this.btnExameAlterar.TabIndex = 62;
            this.btnExameAlterar.TabStop = false;
            this.btnExameAlterar.Text = "&Alterar";
            this.btnExameAlterar.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnExameAlterar.UseVisualStyleBackColor = true;
            this.btnExameAlterar.Click += new System.EventHandler(this.btnExameAlterar_Click);
            // 
            // btnExameExcluir
            // 
            this.btnExameExcluir.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnExameExcluir.Image = ((System.Drawing.Image)(resources.GetObject("btnExameExcluir.Image")));
            this.btnExameExcluir.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnExameExcluir.Location = new System.Drawing.Point(3, 32);
            this.btnExameExcluir.Name = "btnExameExcluir";
            this.btnExameExcluir.Size = new System.Drawing.Size(75, 23);
            this.btnExameExcluir.TabIndex = 18;
            this.btnExameExcluir.TabStop = false;
            this.btnExameExcluir.Text = "&Excluir";
            this.btnExameExcluir.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnExameExcluir.UseVisualStyleBackColor = true;
            this.btnExameExcluir.Click += new System.EventHandler(this.btnExameExcluir_Click);
            // 
            // grbExame
            // 
            this.grbExame.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.grbExame.Controls.Add(this.dgvExame);
            this.grbExame.Location = new System.Drawing.Point(7, 1805);
            this.grbExame.Name = "grbExame";
            this.grbExame.Size = new System.Drawing.Size(629, 200);
            this.grbExame.TabIndex = 114;
            this.grbExame.TabStop = false;
            this.grbExame.Text = "Exames";
            // 
            // dgvExame
            // 
            this.dgvExame.AllowUserToAddRows = false;
            this.dgvExame.AllowUserToDeleteRows = false;
            this.dgvExame.BackgroundColor = System.Drawing.Color.White;
            this.dgvExame.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dataGridViewCellStyle9.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle9.BackColor = System.Drawing.Color.Silver;
            dataGridViewCellStyle9.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle9.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle9.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle9.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle9.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvExame.DefaultCellStyle = dataGridViewCellStyle9;
            this.dgvExame.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvExame.Location = new System.Drawing.Point(3, 16);
            this.dgvExame.MultiSelect = false;
            this.dgvExame.Name = "dgvExame";
            this.dgvExame.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvExame.Size = new System.Drawing.Size(623, 181);
            this.dgvExame.TabIndex = 16;
            this.dgvExame.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvExame_CellClick);
            this.dgvExame.CellLeave += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvExame_CellLeave);
            this.dgvExame.DataBindingComplete += new System.Windows.Forms.DataGridViewBindingCompleteEventHandler(this.dgvExame_DataBindingComplete);
            this.dgvExame.DataError += new System.Windows.Forms.DataGridViewDataErrorEventHandler(this.dgvExame_DataError);
            this.dgvExame.EditingControlShowing += new System.Windows.Forms.DataGridViewEditingControlShowingEventHandler(this.dgvExame_EditingControlShowing);
            this.dgvExame.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.dgvExame_KeyPress);
            // 
            // flpPeriodicidade
            // 
            this.flpPeriodicidade.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.flpPeriodicidade.Controls.Add(this.btnPeriodicidadeIncluir);
            this.flpPeriodicidade.Controls.Add(this.btnPeriodicidadeExcluir);
            this.flpPeriodicidade.Location = new System.Drawing.Point(7, 2257);
            this.flpPeriodicidade.Name = "flpPeriodicidade";
            this.flpPeriodicidade.Size = new System.Drawing.Size(215, 30);
            this.flpPeriodicidade.TabIndex = 68;
            // 
            // btnPeriodicidadeIncluir
            // 
            this.btnPeriodicidadeIncluir.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnPeriodicidadeIncluir.Image = ((System.Drawing.Image)(resources.GetObject("btnPeriodicidadeIncluir.Image")));
            this.btnPeriodicidadeIncluir.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnPeriodicidadeIncluir.Location = new System.Drawing.Point(3, 3);
            this.btnPeriodicidadeIncluir.Name = "btnPeriodicidadeIncluir";
            this.btnPeriodicidadeIncluir.Size = new System.Drawing.Size(75, 23);
            this.btnPeriodicidadeIncluir.TabIndex = 20;
            this.btnPeriodicidadeIncluir.Text = "&Incluir";
            this.btnPeriodicidadeIncluir.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnPeriodicidadeIncluir.UseVisualStyleBackColor = true;
            this.btnPeriodicidadeIncluir.Click += new System.EventHandler(this.btnPeriodicidadeIncluir_Click);
            // 
            // btnPeriodicidadeExcluir
            // 
            this.btnPeriodicidadeExcluir.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnPeriodicidadeExcluir.Image = ((System.Drawing.Image)(resources.GetObject("btnPeriodicidadeExcluir.Image")));
            this.btnPeriodicidadeExcluir.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnPeriodicidadeExcluir.Location = new System.Drawing.Point(84, 3);
            this.btnPeriodicidadeExcluir.Name = "btnPeriodicidadeExcluir";
            this.btnPeriodicidadeExcluir.Size = new System.Drawing.Size(75, 23);
            this.btnPeriodicidadeExcluir.TabIndex = 64;
            this.btnPeriodicidadeExcluir.TabStop = false;
            this.btnPeriodicidadeExcluir.Text = "&Excluir";
            this.btnPeriodicidadeExcluir.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnPeriodicidadeExcluir.UseVisualStyleBackColor = true;
            this.btnPeriodicidadeExcluir.Click += new System.EventHandler(this.btnPeriodicidadeExcluir_Click);
            // 
            // grbPeriodicidade
            // 
            this.grbPeriodicidade.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.grbPeriodicidade.Controls.Add(this.dgvPeriodicidade);
            this.grbPeriodicidade.Location = new System.Drawing.Point(7, 2049);
            this.grbPeriodicidade.Name = "grbPeriodicidade";
            this.grbPeriodicidade.Size = new System.Drawing.Size(629, 205);
            this.grbPeriodicidade.TabIndex = 67;
            this.grbPeriodicidade.TabStop = false;
            this.grbPeriodicidade.Text = "Periodicidades";
            // 
            // dgvPeriodicidade
            // 
            this.dgvPeriodicidade.AllowUserToAddRows = false;
            this.dgvPeriodicidade.AllowUserToDeleteRows = false;
            this.dgvPeriodicidade.AllowUserToOrderColumns = true;
            this.dgvPeriodicidade.BackgroundColor = System.Drawing.Color.White;
            this.dgvPeriodicidade.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dataGridViewCellStyle8.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle8.BackColor = System.Drawing.Color.Silver;
            dataGridViewCellStyle8.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle8.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle8.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle8.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle8.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvPeriodicidade.DefaultCellStyle = dataGridViewCellStyle8;
            this.dgvPeriodicidade.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvPeriodicidade.Location = new System.Drawing.Point(3, 16);
            this.dgvPeriodicidade.MultiSelect = false;
            this.dgvPeriodicidade.Name = "dgvPeriodicidade";
            this.dgvPeriodicidade.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvPeriodicidade.Size = new System.Drawing.Size(623, 186);
            this.dgvPeriodicidade.TabIndex = 19;
            // 
            // lblExplicacaoTerceiraParte
            // 
            this.lblExplicacaoTerceiraParte.AutoSize = true;
            this.lblExplicacaoTerceiraParte.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblExplicacaoTerceiraParte.ForeColor = System.Drawing.Color.Blue;
            this.lblExplicacaoTerceiraParte.Location = new System.Drawing.Point(7, 2298);
            this.lblExplicacaoTerceiraParte.Name = "lblExplicacaoTerceiraParte";
            this.lblExplicacaoTerceiraParte.Size = new System.Drawing.Size(474, 30);
            this.lblExplicacaoTerceiraParte.TabIndex = 116;
            this.lblExplicacaoTerceiraParte.Text = "A terceira parte do PCMSO é necessário informar os hospitais, que serão referênci" +
    "as, \r\ne os médicos examinadores que serão relacionados ao estudo.";
            // 
            // flpHospital
            // 
            this.flpHospital.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.flpHospital.Controls.Add(this.btnHospitalIncluir);
            this.flpHospital.Controls.Add(this.btnHospitalExcluir);
            this.flpHospital.Location = new System.Drawing.Point(7, 2545);
            this.flpHospital.Name = "flpHospital";
            this.flpHospital.Size = new System.Drawing.Size(215, 30);
            this.flpHospital.TabIndex = 70;
            // 
            // btnHospitalIncluir
            // 
            this.btnHospitalIncluir.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnHospitalIncluir.Image = ((System.Drawing.Image)(resources.GetObject("btnHospitalIncluir.Image")));
            this.btnHospitalIncluir.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnHospitalIncluir.Location = new System.Drawing.Point(3, 3);
            this.btnHospitalIncluir.Name = "btnHospitalIncluir";
            this.btnHospitalIncluir.Size = new System.Drawing.Size(75, 23);
            this.btnHospitalIncluir.TabIndex = 20;
            this.btnHospitalIncluir.Text = "&Incluir";
            this.btnHospitalIncluir.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnHospitalIncluir.UseVisualStyleBackColor = true;
            this.btnHospitalIncluir.Click += new System.EventHandler(this.btnHospitalIncluir_Click);
            // 
            // btnHospitalExcluir
            // 
            this.btnHospitalExcluir.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnHospitalExcluir.Image = ((System.Drawing.Image)(resources.GetObject("btnHospitalExcluir.Image")));
            this.btnHospitalExcluir.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnHospitalExcluir.Location = new System.Drawing.Point(84, 3);
            this.btnHospitalExcluir.Name = "btnHospitalExcluir";
            this.btnHospitalExcluir.Size = new System.Drawing.Size(75, 23);
            this.btnHospitalExcluir.TabIndex = 64;
            this.btnHospitalExcluir.TabStop = false;
            this.btnHospitalExcluir.Text = "&Excluir";
            this.btnHospitalExcluir.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnHospitalExcluir.UseVisualStyleBackColor = true;
            this.btnHospitalExcluir.Click += new System.EventHandler(this.btnHospitalExcluir_Click);
            // 
            // grbHosptitais
            // 
            this.grbHosptitais.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.grbHosptitais.Controls.Add(this.dgvHospitais);
            this.grbHosptitais.Location = new System.Drawing.Point(7, 2337);
            this.grbHosptitais.Name = "grbHosptitais";
            this.grbHosptitais.Size = new System.Drawing.Size(629, 205);
            this.grbHosptitais.TabIndex = 69;
            this.grbHosptitais.TabStop = false;
            this.grbHosptitais.Text = "Hospitais";
            // 
            // dgvHospitais
            // 
            this.dgvHospitais.AllowUserToAddRows = false;
            this.dgvHospitais.AllowUserToDeleteRows = false;
            this.dgvHospitais.AllowUserToOrderColumns = true;
            dataGridViewCellStyle6.BackColor = System.Drawing.Color.Gainsboro;
            this.dgvHospitais.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle6;
            this.dgvHospitais.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this.dgvHospitais.BackgroundColor = System.Drawing.Color.White;
            this.dgvHospitais.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dataGridViewCellStyle7.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle7.BackColor = System.Drawing.Color.LightGray;
            dataGridViewCellStyle7.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle7.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle7.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle7.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle7.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvHospitais.DefaultCellStyle = dataGridViewCellStyle7;
            this.dgvHospitais.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvHospitais.Location = new System.Drawing.Point(3, 16);
            this.dgvHospitais.MultiSelect = false;
            this.dgvHospitais.Name = "dgvHospitais";
            this.dgvHospitais.ReadOnly = true;
            this.dgvHospitais.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvHospitais.Size = new System.Drawing.Size(623, 186);
            this.dgvHospitais.TabIndex = 19;
            // 
            // flpMedico
            // 
            this.flpMedico.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.flpMedico.Controls.Add(this.btnMedicoIncluir);
            this.flpMedico.Controls.Add(this.btnMedicoExcluir);
            this.flpMedico.Location = new System.Drawing.Point(7, 2796);
            this.flpMedico.Name = "flpMedico";
            this.flpMedico.Size = new System.Drawing.Size(215, 30);
            this.flpMedico.TabIndex = 72;
            // 
            // btnMedicoIncluir
            // 
            this.btnMedicoIncluir.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnMedicoIncluir.Image = ((System.Drawing.Image)(resources.GetObject("btnMedicoIncluir.Image")));
            this.btnMedicoIncluir.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnMedicoIncluir.Location = new System.Drawing.Point(3, 3);
            this.btnMedicoIncluir.Name = "btnMedicoIncluir";
            this.btnMedicoIncluir.Size = new System.Drawing.Size(75, 23);
            this.btnMedicoIncluir.TabIndex = 20;
            this.btnMedicoIncluir.Text = "&Incluir";
            this.btnMedicoIncluir.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnMedicoIncluir.UseVisualStyleBackColor = true;
            this.btnMedicoIncluir.Click += new System.EventHandler(this.btnMedicoIncluir_Click);
            // 
            // btnMedicoExcluir
            // 
            this.btnMedicoExcluir.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnMedicoExcluir.Image = ((System.Drawing.Image)(resources.GetObject("btnMedicoExcluir.Image")));
            this.btnMedicoExcluir.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnMedicoExcluir.Location = new System.Drawing.Point(84, 3);
            this.btnMedicoExcluir.Name = "btnMedicoExcluir";
            this.btnMedicoExcluir.Size = new System.Drawing.Size(75, 23);
            this.btnMedicoExcluir.TabIndex = 64;
            this.btnMedicoExcluir.TabStop = false;
            this.btnMedicoExcluir.Text = "&Excluir";
            this.btnMedicoExcluir.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnMedicoExcluir.UseVisualStyleBackColor = true;
            this.btnMedicoExcluir.Click += new System.EventHandler(this.btnMedicoExcluir_Click);
            // 
            // grbMedicos
            // 
            this.grbMedicos.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.grbMedicos.Controls.Add(this.dgvMedico);
            this.grbMedicos.Location = new System.Drawing.Point(7, 2588);
            this.grbMedicos.Name = "grbMedicos";
            this.grbMedicos.Size = new System.Drawing.Size(629, 205);
            this.grbMedicos.TabIndex = 71;
            this.grbMedicos.TabStop = false;
            this.grbMedicos.Text = "Médicos";
            // 
            // dgvMedico
            // 
            this.dgvMedico.AllowUserToAddRows = false;
            this.dgvMedico.AllowUserToDeleteRows = false;
            this.dgvMedico.AllowUserToOrderColumns = true;
            this.dgvMedico.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this.dgvMedico.BackgroundColor = System.Drawing.Color.White;
            this.dgvMedico.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle5.BackColor = System.Drawing.Color.Silver;
            dataGridViewCellStyle5.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle5.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle5.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle5.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle5.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvMedico.DefaultCellStyle = dataGridViewCellStyle5;
            this.dgvMedico.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvMedico.Location = new System.Drawing.Point(3, 16);
            this.dgvMedico.MultiSelect = false;
            this.dgvMedico.Name = "dgvMedico";
            this.dgvMedico.ReadOnly = true;
            this.dgvMedico.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvMedico.Size = new System.Drawing.Size(623, 186);
            this.dgvMedico.TabIndex = 19;
            // 
            // lblExplicacaoPenultimaParte
            // 
            this.lblExplicacaoPenultimaParte.AutoSize = true;
            this.lblExplicacaoPenultimaParte.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblExplicacaoPenultimaParte.ForeColor = System.Drawing.Color.Blue;
            this.lblExplicacaoPenultimaParte.Location = new System.Drawing.Point(7, 2836);
            this.lblExplicacaoPenultimaParte.Name = "lblExplicacaoPenultimaParte";
            this.lblExplicacaoPenultimaParte.Size = new System.Drawing.Size(431, 30);
            this.lblExplicacaoPenultimaParte.TabIndex = 117;
            this.lblExplicacaoPenultimaParte.Text = "Penúltima parte do PCMSO consiste em informar o cronograma de atividades\r\nque irã" +
    "o compor o estudo.";
            // 
            // flpAtividade
            // 
            this.flpAtividade.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.flpAtividade.Controls.Add(this.btnAtividadeIncluir);
            this.flpAtividade.Controls.Add(this.btnAtividadeAlterar);
            this.flpAtividade.Controls.Add(this.btnAtividadeExcluir);
            this.flpAtividade.Location = new System.Drawing.Point(7, 3084);
            this.flpAtividade.Name = "flpAtividade";
            this.flpAtividade.Size = new System.Drawing.Size(215, 30);
            this.flpAtividade.TabIndex = 74;
            // 
            // btnAtividadeIncluir
            // 
            this.btnAtividadeIncluir.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnAtividadeIncluir.Image = ((System.Drawing.Image)(resources.GetObject("btnAtividadeIncluir.Image")));
            this.btnAtividadeIncluir.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnAtividadeIncluir.Location = new System.Drawing.Point(3, 3);
            this.btnAtividadeIncluir.Name = "btnAtividadeIncluir";
            this.btnAtividadeIncluir.Size = new System.Drawing.Size(75, 23);
            this.btnAtividadeIncluir.TabIndex = 20;
            this.btnAtividadeIncluir.Text = "&Incluir";
            this.btnAtividadeIncluir.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnAtividadeIncluir.UseVisualStyleBackColor = true;
            this.btnAtividadeIncluir.Click += new System.EventHandler(this.btnAtividadeIncluir_Click);
            // 
            // btnAtividadeAlterar
            // 
            this.btnAtividadeAlterar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnAtividadeAlterar.Image = global::SWS.Properties.Resources.Alterar;
            this.btnAtividadeAlterar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnAtividadeAlterar.Location = new System.Drawing.Point(84, 3);
            this.btnAtividadeAlterar.Name = "btnAtividadeAlterar";
            this.btnAtividadeAlterar.Size = new System.Drawing.Size(75, 23);
            this.btnAtividadeAlterar.TabIndex = 65;
            this.btnAtividadeAlterar.TabStop = false;
            this.btnAtividadeAlterar.Text = "&Alterar";
            this.btnAtividadeAlterar.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnAtividadeAlterar.UseVisualStyleBackColor = true;
            this.btnAtividadeAlterar.Click += new System.EventHandler(this.btnAtividadeAlterar_Click);
            // 
            // btnAtividadeExcluir
            // 
            this.btnAtividadeExcluir.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnAtividadeExcluir.Image = ((System.Drawing.Image)(resources.GetObject("btnAtividadeExcluir.Image")));
            this.btnAtividadeExcluir.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnAtividadeExcluir.Location = new System.Drawing.Point(3, 32);
            this.btnAtividadeExcluir.Name = "btnAtividadeExcluir";
            this.btnAtividadeExcluir.Size = new System.Drawing.Size(75, 23);
            this.btnAtividadeExcluir.TabIndex = 64;
            this.btnAtividadeExcluir.TabStop = false;
            this.btnAtividadeExcluir.Text = "&Excluir";
            this.btnAtividadeExcluir.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnAtividadeExcluir.UseVisualStyleBackColor = true;
            this.btnAtividadeExcluir.Click += new System.EventHandler(this.btnAtividadeExcluir_Click);
            // 
            // grbAtividades
            // 
            this.grbAtividades.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.grbAtividades.Controls.Add(this.dgvAtividade);
            this.grbAtividades.Location = new System.Drawing.Point(7, 2876);
            this.grbAtividades.Name = "grbAtividades";
            this.grbAtividades.Size = new System.Drawing.Size(629, 205);
            this.grbAtividades.TabIndex = 73;
            this.grbAtividades.TabStop = false;
            this.grbAtividades.Text = "Atividades";
            // 
            // dgvAtividade
            // 
            this.dgvAtividade.AllowUserToAddRows = false;
            this.dgvAtividade.AllowUserToDeleteRows = false;
            this.dgvAtividade.AllowUserToOrderColumns = true;
            this.dgvAtividade.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this.dgvAtividade.BackgroundColor = System.Drawing.Color.White;
            this.dgvAtividade.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle4.BackColor = System.Drawing.Color.Silver;
            dataGridViewCellStyle4.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle4.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle4.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle4.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle4.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvAtividade.DefaultCellStyle = dataGridViewCellStyle4;
            this.dgvAtividade.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvAtividade.Location = new System.Drawing.Point(3, 16);
            this.dgvAtividade.MultiSelect = false;
            this.dgvAtividade.Name = "dgvAtividade";
            this.dgvAtividade.ReadOnly = true;
            this.dgvAtividade.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvAtividade.Size = new System.Drawing.Size(623, 186);
            this.dgvAtividade.TabIndex = 19;
            // 
            // lblObrigatorioCliente
            // 
            this.lblObrigatorioCliente.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lblObrigatorioCliente.AutoSize = true;
            this.lblObrigatorioCliente.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblObrigatorioCliente.ForeColor = System.Drawing.Color.Red;
            this.lblObrigatorioCliente.Location = new System.Drawing.Point(670, 45);
            this.lblObrigatorioCliente.Name = "lblObrigatorioCliente";
            this.lblObrigatorioCliente.Size = new System.Drawing.Size(15, 20);
            this.lblObrigatorioCliente.TabIndex = 118;
            this.lblObrigatorioCliente.Text = "*";
            // 
            // lblObrigatorioElaborador
            // 
            this.lblObrigatorioElaborador.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lblObrigatorioElaborador.AutoSize = true;
            this.lblObrigatorioElaborador.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblObrigatorioElaborador.ForeColor = System.Drawing.Color.Red;
            this.lblObrigatorioElaborador.Location = new System.Drawing.Point(670, 65);
            this.lblObrigatorioElaborador.Name = "lblObrigatorioElaborador";
            this.lblObrigatorioElaborador.Size = new System.Drawing.Size(15, 20);
            this.lblObrigatorioElaborador.TabIndex = 119;
            this.lblObrigatorioElaborador.Text = "*";
            // 
            // lblObrigatorioVigencia
            // 
            this.lblObrigatorioVigencia.AutoSize = true;
            this.lblObrigatorioVigencia.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblObrigatorioVigencia.ForeColor = System.Drawing.Color.Red;
            this.lblObrigatorioVigencia.Location = new System.Drawing.Point(413, 88);
            this.lblObrigatorioVigencia.Name = "lblObrigatorioVigencia";
            this.lblObrigatorioVigencia.Size = new System.Drawing.Size(15, 20);
            this.lblObrigatorioVigencia.TabIndex = 120;
            this.lblObrigatorioVigencia.Text = "*";
            // 
            // lblObrigatorioGrauRisco
            // 
            this.lblObrigatorioGrauRisco.AutoSize = true;
            this.lblObrigatorioGrauRisco.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblObrigatorioGrauRisco.ForeColor = System.Drawing.Color.Red;
            this.lblObrigatorioGrauRisco.Location = new System.Drawing.Point(413, 107);
            this.lblObrigatorioGrauRisco.Name = "lblObrigatorioGrauRisco";
            this.lblObrigatorioGrauRisco.Size = new System.Drawing.Size(15, 20);
            this.lblObrigatorioGrauRisco.TabIndex = 121;
            this.lblObrigatorioGrauRisco.Text = "*";
            // 
            // lblExplicacaoUltimaParte
            // 
            this.lblExplicacaoUltimaParte.AutoSize = true;
            this.lblExplicacaoUltimaParte.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblExplicacaoUltimaParte.ForeColor = System.Drawing.Color.Blue;
            this.lblExplicacaoUltimaParte.Location = new System.Drawing.Point(4, 3124);
            this.lblExplicacaoUltimaParte.Name = "lblExplicacaoUltimaParte";
            this.lblExplicacaoUltimaParte.Size = new System.Drawing.Size(643, 30);
            this.lblExplicacaoUltimaParte.TabIndex = 123;
            this.lblExplicacaoUltimaParte.Text = "Última parte do PCMSO. Você deve vincular uma lista de matériais de primeiros soc" +
    "orros e ainda informar os anexos\r\nque estarão presentes nesse estudo.";
            // 
            // flpMateriais
            // 
            this.flpMateriais.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.flpMateriais.Controls.Add(this.btnMaterialHospitalarInserir);
            this.flpMateriais.Controls.Add(this.btnMaterialHospitalarExcluir);
            this.flpMateriais.Location = new System.Drawing.Point(7, 3372);
            this.flpMateriais.Name = "flpMateriais";
            this.flpMateriais.Size = new System.Drawing.Size(215, 30);
            this.flpMateriais.TabIndex = 75;
            // 
            // btnMaterialHospitalarInserir
            // 
            this.btnMaterialHospitalarInserir.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnMaterialHospitalarInserir.Image = global::SWS.Properties.Resources.icone_mais;
            this.btnMaterialHospitalarInserir.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnMaterialHospitalarInserir.Location = new System.Drawing.Point(3, 3);
            this.btnMaterialHospitalarInserir.Name = "btnMaterialHospitalarInserir";
            this.btnMaterialHospitalarInserir.Size = new System.Drawing.Size(75, 23);
            this.btnMaterialHospitalarInserir.TabIndex = 20;
            this.btnMaterialHospitalarInserir.Text = "&Inserir";
            this.btnMaterialHospitalarInserir.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.tpPcmsoIncluir.SetToolTip(this.btnMaterialHospitalarInserir, "Incluir ou alterar a caixa de primeiros socorros.");
            this.btnMaterialHospitalarInserir.UseVisualStyleBackColor = true;
            this.btnMaterialHospitalarInserir.Click += new System.EventHandler(this.btnMaterialHospitalarInserir_Click);
            // 
            // btnMaterialHospitalarExcluir
            // 
            this.btnMaterialHospitalarExcluir.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnMaterialHospitalarExcluir.Image = global::SWS.Properties.Resources.lixeira;
            this.btnMaterialHospitalarExcluir.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnMaterialHospitalarExcluir.Location = new System.Drawing.Point(84, 3);
            this.btnMaterialHospitalarExcluir.Name = "btnMaterialHospitalarExcluir";
            this.btnMaterialHospitalarExcluir.Size = new System.Drawing.Size(75, 23);
            this.btnMaterialHospitalarExcluir.TabIndex = 22;
            this.btnMaterialHospitalarExcluir.Text = "&Excluir";
            this.btnMaterialHospitalarExcluir.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.tpPcmsoIncluir.SetToolTip(this.btnMaterialHospitalarExcluir, "Incluir ou alterar a caixa de primeiros socorros.");
            this.btnMaterialHospitalarExcluir.UseVisualStyleBackColor = true;
            this.btnMaterialHospitalarExcluir.Click += new System.EventHandler(this.btnMaterialHospitalarExcluir_Click);
            // 
            // tpPcmsoIncluir
            // 
            this.tpPcmsoIncluir.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.tpPcmsoIncluir.IsBalloon = true;
            this.tpPcmsoIncluir.ToolTipIcon = System.Windows.Forms.ToolTipIcon.Info;
            // 
            // btnDocumentoInserir
            // 
            this.btnDocumentoInserir.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnDocumentoInserir.Image = global::SWS.Properties.Resources.icone_mais;
            this.btnDocumentoInserir.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnDocumentoInserir.Location = new System.Drawing.Point(3, 3);
            this.btnDocumentoInserir.Name = "btnDocumentoInserir";
            this.btnDocumentoInserir.Size = new System.Drawing.Size(75, 23);
            this.btnDocumentoInserir.TabIndex = 20;
            this.btnDocumentoInserir.Text = "&Inserir";
            this.btnDocumentoInserir.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.tpPcmsoIncluir.SetToolTip(this.btnDocumentoInserir, "Incluir ou alterar a caixa de primeiros socorros.");
            this.btnDocumentoInserir.UseVisualStyleBackColor = true;
            this.btnDocumentoInserir.Click += new System.EventHandler(this.btnDocumentoInserir_Click);
            // 
            // btnDocumentoExcluir
            // 
            this.btnDocumentoExcluir.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnDocumentoExcluir.Image = global::SWS.Properties.Resources.lixeira;
            this.btnDocumentoExcluir.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnDocumentoExcluir.Location = new System.Drawing.Point(84, 3);
            this.btnDocumentoExcluir.Name = "btnDocumentoExcluir";
            this.btnDocumentoExcluir.Size = new System.Drawing.Size(75, 23);
            this.btnDocumentoExcluir.TabIndex = 22;
            this.btnDocumentoExcluir.Text = "&Excluir";
            this.btnDocumentoExcluir.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.tpPcmsoIncluir.SetToolTip(this.btnDocumentoExcluir, "Incluir ou alterar a caixa de primeiros socorros.");
            this.btnDocumentoExcluir.UseVisualStyleBackColor = true;
            this.btnDocumentoExcluir.Click += new System.EventHandler(this.btnDocumentoExcluir_Click);
            // 
            // grbMaterialHospitalar
            // 
            this.grbMaterialHospitalar.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.grbMaterialHospitalar.Controls.Add(this.dgvMateriais);
            this.grbMaterialHospitalar.Location = new System.Drawing.Point(7, 3164);
            this.grbMaterialHospitalar.Name = "grbMaterialHospitalar";
            this.grbMaterialHospitalar.Size = new System.Drawing.Size(629, 205);
            this.grbMaterialHospitalar.TabIndex = 124;
            this.grbMaterialHospitalar.TabStop = false;
            this.grbMaterialHospitalar.Text = "Materiais Hospitalar";
            // 
            // dgvMateriais
            // 
            this.dgvMateriais.AllowUserToAddRows = false;
            this.dgvMateriais.AllowUserToDeleteRows = false;
            this.dgvMateriais.AllowUserToOrderColumns = true;
            this.dgvMateriais.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this.dgvMateriais.BackgroundColor = System.Drawing.Color.White;
            this.dgvMateriais.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle3.BackColor = System.Drawing.Color.Silver;
            dataGridViewCellStyle3.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvMateriais.DefaultCellStyle = dataGridViewCellStyle3;
            this.dgvMateriais.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvMateriais.Location = new System.Drawing.Point(3, 16);
            this.dgvMateriais.MultiSelect = false;
            this.dgvMateriais.Name = "dgvMateriais";
            this.dgvMateriais.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvMateriais.Size = new System.Drawing.Size(623, 186);
            this.dgvMateriais.TabIndex = 19;
            this.dgvMateriais.CellEndEdit += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvMateriais_CellEndEdit);
            this.dgvMateriais.DataBindingComplete += new System.Windows.Forms.DataGridViewBindingCompleteEventHandler(this.dgvMateriais_DataBindingComplete);
            this.dgvMateriais.DataError += new System.Windows.Forms.DataGridViewDataErrorEventHandler(this.dgvMateriais_DataError);
            this.dgvMateriais.EditingControlShowing += new System.Windows.Forms.DataGridViewEditingControlShowingEventHandler(this.dgvMateriais_EditingControlShowing);
            this.dgvMateriais.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.dgvMateriais_KeyPress);
            // 
            // grbAnexos
            // 
            this.grbAnexos.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.grbAnexos.Controls.Add(this.dgvDocumentos);
            this.grbAnexos.Location = new System.Drawing.Point(7, 3415);
            this.grbAnexos.Name = "grbAnexos";
            this.grbAnexos.Size = new System.Drawing.Size(629, 205);
            this.grbAnexos.TabIndex = 126;
            this.grbAnexos.TabStop = false;
            this.grbAnexos.Text = "Documentos Anexados";
            // 
            // dgvDocumentos
            // 
            this.dgvDocumentos.AllowUserToAddRows = false;
            this.dgvDocumentos.AllowUserToDeleteRows = false;
            this.dgvDocumentos.AllowUserToOrderColumns = true;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.Gainsboro;
            this.dgvDocumentos.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle1;
            this.dgvDocumentos.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this.dgvDocumentos.BackgroundColor = System.Drawing.Color.White;
            this.dgvDocumentos.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.Color.Silver;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvDocumentos.DefaultCellStyle = dataGridViewCellStyle2;
            this.dgvDocumentos.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvDocumentos.Location = new System.Drawing.Point(3, 16);
            this.dgvDocumentos.MultiSelect = false;
            this.dgvDocumentos.Name = "dgvDocumentos";
            this.dgvDocumentos.ReadOnly = true;
            this.dgvDocumentos.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvDocumentos.Size = new System.Drawing.Size(623, 186);
            this.dgvDocumentos.TabIndex = 19;
            // 
            // flpDocumentos
            // 
            this.flpDocumentos.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.flpDocumentos.Controls.Add(this.btnDocumentoInserir);
            this.flpDocumentos.Controls.Add(this.btnDocumentoExcluir);
            this.flpDocumentos.Location = new System.Drawing.Point(7, 3623);
            this.flpDocumentos.Name = "flpDocumentos";
            this.flpDocumentos.Size = new System.Drawing.Size(215, 30);
            this.flpDocumentos.TabIndex = 125;
            // 
            // lblCodigo
            // 
            this.lblCodigo.BackColor = System.Drawing.Color.LightGray;
            this.lblCodigo.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblCodigo.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCodigo.Location = new System.Drawing.Point(7, 6);
            this.lblCodigo.Name = "lblCodigo";
            this.lblCodigo.ReadOnly = true;
            this.lblCodigo.Size = new System.Drawing.Size(162, 21);
            this.lblCodigo.TabIndex = 127;
            this.lblCodigo.TabStop = false;
            this.lblCodigo.Text = "Cliente";
            // 
            // textCodigo
            // 
            this.textCodigo.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.textCodigo.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.textCodigo.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textCodigo.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textCodigo.Location = new System.Drawing.Point(168, 6);
            this.textCodigo.Mask = "0000,00,000000/00";
            this.textCodigo.Name = "textCodigo";
            this.textCodigo.PromptChar = ' ';
            this.textCodigo.Size = new System.Drawing.Size(500, 21);
            this.textCodigo.TabIndex = 128;
            // 
            // lblCnaeCliente
            // 
            this.lblCnaeCliente.BackColor = System.Drawing.Color.LightGray;
            this.lblCnaeCliente.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblCnaeCliente.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCnaeCliente.Location = new System.Drawing.Point(7, 46);
            this.lblCnaeCliente.Name = "lblCnaeCliente";
            this.lblCnaeCliente.ReadOnly = true;
            this.lblCnaeCliente.Size = new System.Drawing.Size(162, 21);
            this.lblCnaeCliente.TabIndex = 129;
            this.lblCnaeCliente.TabStop = false;
            this.lblCnaeCliente.Text = "CNAE";
            // 
            // textCnaeCliente
            // 
            this.textCnaeCliente.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.textCnaeCliente.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.textCnaeCliente.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textCnaeCliente.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textCnaeCliente.ForeColor = System.Drawing.Color.Blue;
            this.textCnaeCliente.Location = new System.Drawing.Point(168, 46);
            this.textCnaeCliente.MaxLength = 100;
            this.textCnaeCliente.Name = "textCnaeCliente";
            this.textCnaeCliente.ReadOnly = true;
            this.textCnaeCliente.Size = new System.Drawing.Size(500, 21);
            this.textCnaeCliente.TabIndex = 131;
            this.textCnaeCliente.TabStop = false;
            // 
            // lblCnaeContratante
            // 
            this.lblCnaeContratante.BackColor = System.Drawing.Color.LightGray;
            this.lblCnaeContratante.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblCnaeContratante.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCnaeContratante.Location = new System.Drawing.Point(7, 153);
            this.lblCnaeContratante.Name = "lblCnaeContratante";
            this.lblCnaeContratante.ReadOnly = true;
            this.lblCnaeContratante.Size = new System.Drawing.Size(162, 21);
            this.lblCnaeContratante.TabIndex = 132;
            this.lblCnaeContratante.TabStop = false;
            this.lblCnaeContratante.Text = "CNAE Contratante";
            // 
            // textCnaeContratante
            // 
            this.textCnaeContratante.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.textCnaeContratante.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.textCnaeContratante.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textCnaeContratante.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textCnaeContratante.ForeColor = System.Drawing.Color.Blue;
            this.textCnaeContratante.Location = new System.Drawing.Point(168, 153);
            this.textCnaeContratante.MaxLength = 100;
            this.textCnaeContratante.Name = "textCnaeContratante";
            this.textCnaeContratante.ReadOnly = true;
            this.textCnaeContratante.Size = new System.Drawing.Size(500, 21);
            this.textCnaeContratante.TabIndex = 133;
            this.textCnaeContratante.TabStop = false;
            // 
            // frmPcmsoIncluir
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(784, 561);
            this.MaximizeBox = true;
            this.Name = "frmPcmsoIncluir";
            this.Text = "INCLUIR PCMSO";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.spForm.Panel1.ResumeLayout(false);
            this.spForm.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.spForm)).EndInit();
            this.spForm.ResumeLayout(false);
            this.pnlForm.ResumeLayout(false);
            this.pnlForm.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.banner)).EndInit();
            this.flpAcao.ResumeLayout(false);
            this.grbEstimativa.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvEstimativa)).EndInit();
            this.flpEstimativa.ResumeLayout(false);
            this.flpGhe.ResumeLayout(false);
            this.grbGhe.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvGhe)).EndInit();
            this.flpSetor.ResumeLayout(false);
            this.grbSetor.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvSetor)).EndInit();
            this.flpFuncao.ResumeLayout(false);
            this.grbFuncao.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvFuncao)).EndInit();
            this.flpFonte.ResumeLayout(false);
            this.grbFonte.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvFonte)).EndInit();
            this.flpAgente.ResumeLayout(false);
            this.grbAgente.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvAgente)).EndInit();
            this.flpExame.ResumeLayout(false);
            this.grbExame.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvExame)).EndInit();
            this.flpPeriodicidade.ResumeLayout(false);
            this.grbPeriodicidade.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvPeriodicidade)).EndInit();
            this.flpHospital.ResumeLayout(false);
            this.grbHosptitais.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvHospitais)).EndInit();
            this.flpMedico.ResumeLayout(false);
            this.grbMedicos.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvMedico)).EndInit();
            this.flpAtividade.ResumeLayout(false);
            this.grbAtividades.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvAtividade)).EndInit();
            this.flpMateriais.ResumeLayout(false);
            this.grbMaterialHospitalar.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvMateriais)).EndInit();
            this.grbAnexos.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvDocumentos)).EndInit();
            this.flpDocumentos.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        protected System.Windows.Forms.FlowLayoutPanel flpAcao;
        protected System.Windows.Forms.Label textCodigoEstudo;
        protected System.Windows.Forms.TextBox lblCliente;
        protected System.Windows.Forms.TextBox textCliente;
        protected System.Windows.Forms.Button btnClienteIncluir;
        protected System.Windows.Forms.TextBox lblVigencia;
        protected System.Windows.Forms.Button btnElaboradorExcluir;
        protected System.Windows.Forms.Button btnElaboradorIncluir;
        protected System.Windows.Forms.TextBox textElaborador;
        protected System.Windows.Forms.TextBox lblElaborador;
        protected System.Windows.Forms.Button btnClienteExcluir;
        protected System.Windows.Forms.TextBox lblGrauRisco;
        protected System.Windows.Forms.DateTimePicker dataCriacao;
        protected System.Windows.Forms.DateTimePicker dataValidade;
        protected ComboBoxWithBorder cbGrauRisco;
        protected System.Windows.Forms.Button btnContratanteExcluir;
        protected System.Windows.Forms.TextBox lblContratante;
        protected System.Windows.Forms.TextBox textContratante;
        protected System.Windows.Forms.Button btnContratanteIncluir;
        protected System.Windows.Forms.TextBox lblIncioContrato;
        protected System.Windows.Forms.TextBox lblFimContrato;
        protected System.Windows.Forms.TextBox textNumeroContrato;
        protected System.Windows.Forms.TextBox lblNumeroContrato;
        protected System.Windows.Forms.TextBox textFimContrato;
        protected System.Windows.Forms.TextBox textIncioContrato;
        protected System.Windows.Forms.Button btnLocalEstudoIncluir;
        protected System.Windows.Forms.Button btnLocalEstudoExcluir;
        protected System.Windows.Forms.TextBox lblLocalEstudo;
        protected System.Windows.Forms.TextBox textLocalEstudo;
        protected System.Windows.Forms.FlowLayoutPanel flpEstimativa;
        protected System.Windows.Forms.Button btnEstimativaIncluir;
        protected System.Windows.Forms.Button btnEstimativaExcluir;
        protected System.Windows.Forms.GroupBox grbEstimativa;
        protected System.Windows.Forms.DataGridView dgvEstimativa;
        protected System.Windows.Forms.FlowLayoutPanel flpGhe;
        protected System.Windows.Forms.Button btnGheIncluir;
        protected System.Windows.Forms.Button btnGheAlterar;
        protected System.Windows.Forms.Button btnGheExcluir;
        protected System.Windows.Forms.Button btnGheDuplicar;
        protected System.Windows.Forms.GroupBox grbGhe;
        protected System.Windows.Forms.DataGridView dgvGhe;
        protected System.Windows.Forms.Label lblExplicacaoFuncao;
        protected System.Windows.Forms.FlowLayoutPanel flpSetor;
        protected System.Windows.Forms.Button btnSetorIncluir;
        protected System.Windows.Forms.Button btnSetorExcluir;
        protected System.Windows.Forms.GroupBox grbSetor;
        protected System.Windows.Forms.DataGridView dgvSetor;
        protected System.Windows.Forms.FlowLayoutPanel flpFuncao;
        protected System.Windows.Forms.Button btnFuncaoIncluir;
        protected System.Windows.Forms.Button btnFuncaoExcluir;
        protected System.Windows.Forms.GroupBox grbFuncao;
        protected System.Windows.Forms.DataGridView dgvFuncao;
        protected System.Windows.Forms.Label lblExplicacaoSegundaParte;
        protected System.Windows.Forms.FlowLayoutPanel flpFonte;
        protected System.Windows.Forms.Button btnFonteIncluir;
        protected System.Windows.Forms.Button btnFonteExcluir;
        protected System.Windows.Forms.GroupBox grbFonte;
        protected System.Windows.Forms.DataGridView dgvFonte;
        protected System.Windows.Forms.FlowLayoutPanel flpAgente;
        protected System.Windows.Forms.Button btnAgenteIncluir;
        protected System.Windows.Forms.Button btnAgenteAlterar;
        protected System.Windows.Forms.GroupBox grbAgente;
        protected System.Windows.Forms.DataGridView dgvAgente;
        protected System.Windows.Forms.FlowLayoutPanel flpExame;
        protected System.Windows.Forms.Button btnExameIncluir;
        protected System.Windows.Forms.Button btnExameAlterar;
        protected System.Windows.Forms.Button btnExameExcluir;
        protected System.Windows.Forms.GroupBox grbExame;
        protected System.Windows.Forms.DataGridView dgvExame;
        protected System.Windows.Forms.FlowLayoutPanel flpPeriodicidade;
        protected System.Windows.Forms.Button btnPeriodicidadeIncluir;
        protected System.Windows.Forms.Button btnPeriodicidadeExcluir;
        protected System.Windows.Forms.GroupBox grbPeriodicidade;
        protected System.Windows.Forms.DataGridView dgvPeriodicidade;
        protected System.Windows.Forms.Label lblExplicacaoTerceiraParte;
        protected System.Windows.Forms.FlowLayoutPanel flpHospital;
        protected System.Windows.Forms.Button btnHospitalIncluir;
        protected System.Windows.Forms.Button btnHospitalExcluir;
        protected System.Windows.Forms.GroupBox grbHosptitais;
        protected System.Windows.Forms.DataGridView dgvHospitais;
        protected System.Windows.Forms.FlowLayoutPanel flpMedico;
        protected System.Windows.Forms.Button btnMedicoIncluir;
        protected System.Windows.Forms.Button btnMedicoExcluir;
        protected System.Windows.Forms.Label lblExplicacaoPenultimaParte;
        protected System.Windows.Forms.GroupBox grbMedicos;
        protected System.Windows.Forms.DataGridView dgvMedico;
        protected System.Windows.Forms.FlowLayoutPanel flpAtividade;
        protected System.Windows.Forms.Button btnAtividadeIncluir;
        protected System.Windows.Forms.Button btnAtividadeExcluir;
        protected System.Windows.Forms.GroupBox grbAtividades;
        protected System.Windows.Forms.DataGridView dgvAtividade;
        protected System.Windows.Forms.Button btnGravarNovo;
        protected System.Windows.Forms.Button btnObservacao;
        protected System.Windows.Forms.Button btnFechar;
        protected System.Windows.Forms.Label lblObrigatorioGrauRisco;
        protected System.Windows.Forms.Label lblObrigatorioVigencia;
        protected System.Windows.Forms.Label lblObrigatorioElaborador;
        protected System.Windows.Forms.Label lblObrigatorioCliente;
        protected System.Windows.Forms.Button btnAgenteExcluir;
        protected System.Windows.Forms.Button btnAtividadeAlterar;
        protected System.Windows.Forms.Label lblExplicacaoUltimaParte;
        protected System.Windows.Forms.FlowLayoutPanel flpMateriais;
        protected System.Windows.Forms.Button btnMaterialHospitalarInserir;
        protected System.Windows.Forms.ToolTip tpPcmsoIncluir;
        protected System.Windows.Forms.GroupBox grbMaterialHospitalar;
        protected System.Windows.Forms.DataGridView dgvMateriais;
        protected System.Windows.Forms.GroupBox grbAnexos;
        protected System.Windows.Forms.DataGridView dgvDocumentos;
        protected System.Windows.Forms.FlowLayoutPanel flpDocumentos;
        protected System.Windows.Forms.Button btnDocumentoInserir;
        protected System.Windows.Forms.Button btnDocumentoExcluir;
        protected System.Windows.Forms.Button btnMaterialHospitalarExcluir;
        protected System.Windows.Forms.Button btnSetorDuplicar;
        protected System.Windows.Forms.Button btnFonteDuplicar;
        protected System.Windows.Forms.Button btnAgenteDuplicar;
        protected System.Windows.Forms.TextBox lblCodigo;
        protected System.Windows.Forms.MaskedTextBox textCodigo;
        protected System.Windows.Forms.TextBox textCnaeContratante;
        protected System.Windows.Forms.TextBox lblCnaeContratante;
        protected System.Windows.Forms.TextBox textCnaeCliente;
        protected System.Windows.Forms.TextBox lblCnaeCliente;
    }
}