﻿namespace SWS.View
{
    partial class frmUsuarioIncluir
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmUsuarioIncluir));
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            this.flpAcao = new System.Windows.Forms.FlowLayoutPanel();
            this.bt_gravar = new System.Windows.Forms.Button();
            this.btnLimpar = new System.Windows.Forms.Button();
            this.btn_fechar = new System.Windows.Forms.Button();
            this.btnFuncaoInterna = new System.Windows.Forms.Button();
            this.lblFuncao = new System.Windows.Forms.TextBox();
            this.textFuncaoInterna = new System.Windows.Forms.TextBox();
            this.textDataAdmissao = new System.Windows.Forms.TextBox();
            this.lblCoordenador = new System.Windows.Forms.TextBox();
            this.btnMedico = new System.Windows.Forms.Button();
            this.dataAdmissao = new System.Windows.Forms.DateTimePicker();
            this.textCoordenador = new System.Windows.Forms.TextBox();
            this.lblEmpresa = new System.Windows.Forms.TextBox();
            this.btnEmpresa = new System.Windows.Forms.Button();
            this.textEmpresa = new System.Windows.Forms.TextBox();
            this.lblRegistroOrgao = new System.Windows.Forms.TextBox();
            this.lblLogin = new System.Windows.Forms.TextBox();
            this.lblEmail = new System.Windows.Forms.TextBox();
            this.lblRg = new System.Windows.Forms.TextBox();
            this.lblCpf = new System.Windows.Forms.TextBox();
            this.lblNome = new System.Windows.Forms.TextBox();
            this.textNome = new System.Windows.Forms.TextBox();
            this.textCpf = new System.Windows.Forms.MaskedTextBox();
            this.textIdentidade = new System.Windows.Forms.TextBox();
            this.textEmail = new System.Windows.Forms.TextBox();
            this.textLogin = new System.Windows.Forms.TextBox();
            this.textNumeroRegistro = new System.Windows.Forms.TextBox();
            this.lblDocumentacao = new System.Windows.Forms.TextBox();
            this.incluirUsuarioErrorProvider = new System.Windows.Forms.ErrorProvider(this.components);
            this.cbDocumentacao = new SWS.ComboBoxWithBorder();
            this.lblRegistroMte = new System.Windows.Forms.TextBox();
            this.textMte = new System.Windows.Forms.TextBox();
            this.btnAssinatura = new System.Windows.Forms.Button();
            this.lblAssinatura = new System.Windows.Forms.Label();
            this.pbAssinatura = new System.Windows.Forms.PictureBox();
            this.btn_excluir_perfil = new System.Windows.Forms.Button();
            this.grb_perfil = new System.Windows.Forms.GroupBox();
            this.dgvPerfil = new System.Windows.Forms.DataGridView();
            this.btn_incluir_perfil = new System.Windows.Forms.Button();
            this.btExcluirCliente = new System.Windows.Forms.Button();
            this.grbCliente = new System.Windows.Forms.GroupBox();
            this.dgvCliente = new System.Windows.Forms.DataGridView();
            this.btIncluirCliente = new System.Windows.Forms.Button();
            this.lblUsuarioExterno = new System.Windows.Forms.TextBox();
            this.cbExterno = new SWS.ComboBoxWithBorder();
            this.flpAcaoCliente = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel1 = new System.Windows.Forms.FlowLayoutPanel();
            this.textEmissorCertificado = new System.Windows.Forms.TextBox();
            this.cbEmissorCertificado = new SWS.ComboBoxWithBorder();
            this.lblOrgaoEmissor = new System.Windows.Forms.TextBox();
            this.textOrgaoEmissor = new System.Windows.Forms.TextBox();
            this.lblUfEmissor = new System.Windows.Forms.TextBox();
            this.cbUfEmissor = new SWS.ComboBoxWithBorder();
            ((System.ComponentModel.ISupportInitialize)(this.spForm)).BeginInit();
            this.spForm.Panel1.SuspendLayout();
            this.spForm.Panel2.SuspendLayout();
            this.spForm.SuspendLayout();
            this.pnlForm.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.banner)).BeginInit();
            this.flpAcao.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.incluirUsuarioErrorProvider)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbAssinatura)).BeginInit();
            this.grb_perfil.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvPerfil)).BeginInit();
            this.grbCliente.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvCliente)).BeginInit();
            this.flpAcaoCliente.SuspendLayout();
            this.flowLayoutPanel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // spForm
            // 
            // 
            // spForm.Panel1
            // 
            this.spForm.Panel1.Controls.Add(this.flpAcao);
            // 
            // pnlForm
            // 
            this.pnlForm.Controls.Add(this.cbUfEmissor);
            this.pnlForm.Controls.Add(this.lblUfEmissor);
            this.pnlForm.Controls.Add(this.textOrgaoEmissor);
            this.pnlForm.Controls.Add(this.lblOrgaoEmissor);
            this.pnlForm.Controls.Add(this.cbEmissorCertificado);
            this.pnlForm.Controls.Add(this.textEmissorCertificado);
            this.pnlForm.Controls.Add(this.flowLayoutPanel1);
            this.pnlForm.Controls.Add(this.flpAcaoCliente);
            this.pnlForm.Controls.Add(this.cbExterno);
            this.pnlForm.Controls.Add(this.lblUsuarioExterno);
            this.pnlForm.Controls.Add(this.grbCliente);
            this.pnlForm.Controls.Add(this.grb_perfil);
            this.pnlForm.Controls.Add(this.btnAssinatura);
            this.pnlForm.Controls.Add(this.lblAssinatura);
            this.pnlForm.Controls.Add(this.pbAssinatura);
            this.pnlForm.Controls.Add(this.lblRegistroMte);
            this.pnlForm.Controls.Add(this.textMte);
            this.pnlForm.Controls.Add(this.cbDocumentacao);
            this.pnlForm.Controls.Add(this.lblDocumentacao);
            this.pnlForm.Controls.Add(this.btnFuncaoInterna);
            this.pnlForm.Controls.Add(this.lblFuncao);
            this.pnlForm.Controls.Add(this.textFuncaoInterna);
            this.pnlForm.Controls.Add(this.textDataAdmissao);
            this.pnlForm.Controls.Add(this.lblCoordenador);
            this.pnlForm.Controls.Add(this.btnMedico);
            this.pnlForm.Controls.Add(this.dataAdmissao);
            this.pnlForm.Controls.Add(this.textCoordenador);
            this.pnlForm.Controls.Add(this.lblEmpresa);
            this.pnlForm.Controls.Add(this.btnEmpresa);
            this.pnlForm.Controls.Add(this.textEmpresa);
            this.pnlForm.Controls.Add(this.lblRegistroOrgao);
            this.pnlForm.Controls.Add(this.lblLogin);
            this.pnlForm.Controls.Add(this.lblEmail);
            this.pnlForm.Controls.Add(this.lblRg);
            this.pnlForm.Controls.Add(this.lblCpf);
            this.pnlForm.Controls.Add(this.lblNome);
            this.pnlForm.Controls.Add(this.textNome);
            this.pnlForm.Controls.Add(this.textCpf);
            this.pnlForm.Controls.Add(this.textIdentidade);
            this.pnlForm.Controls.Add(this.textEmail);
            this.pnlForm.Controls.Add(this.textLogin);
            this.pnlForm.Controls.Add(this.textNumeroRegistro);
            this.pnlForm.Size = new System.Drawing.Size(764, 1000);
            // 
            // flpAcao
            // 
            this.flpAcao.Controls.Add(this.bt_gravar);
            this.flpAcao.Controls.Add(this.btnLimpar);
            this.flpAcao.Controls.Add(this.btn_fechar);
            this.flpAcao.Dock = System.Windows.Forms.DockStyle.Fill;
            this.flpAcao.Location = new System.Drawing.Point(0, 0);
            this.flpAcao.Name = "flpAcao";
            this.flpAcao.Size = new System.Drawing.Size(784, 35);
            this.flpAcao.TabIndex = 0;
            // 
            // bt_gravar
            // 
            this.bt_gravar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.bt_gravar.Image = global::SWS.Properties.Resources.Gravar;
            this.bt_gravar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.bt_gravar.Location = new System.Drawing.Point(3, 3);
            this.bt_gravar.Name = "bt_gravar";
            this.bt_gravar.Size = new System.Drawing.Size(77, 23);
            this.bt_gravar.TabIndex = 18;
            this.bt_gravar.TabStop = false;
            this.bt_gravar.Text = "Gravar";
            this.bt_gravar.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.bt_gravar.UseVisualStyleBackColor = true;
            this.bt_gravar.Click += new System.EventHandler(this.bt_gravar_Click);
            // 
            // btnLimpar
            // 
            this.btnLimpar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnLimpar.Image = global::SWS.Properties.Resources.vassoura;
            this.btnLimpar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnLimpar.Location = new System.Drawing.Point(86, 3);
            this.btnLimpar.Name = "btnLimpar";
            this.btnLimpar.Size = new System.Drawing.Size(77, 23);
            this.btnLimpar.TabIndex = 20;
            this.btnLimpar.TabStop = false;
            this.btnLimpar.Text = "Limpar";
            this.btnLimpar.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnLimpar.UseVisualStyleBackColor = true;
            this.btnLimpar.Click += new System.EventHandler(this.btnLimpar_Click);
            // 
            // btn_fechar
            // 
            this.btn_fechar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_fechar.Image = global::SWS.Properties.Resources.close;
            this.btn_fechar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btn_fechar.Location = new System.Drawing.Point(169, 3);
            this.btn_fechar.Name = "btn_fechar";
            this.btn_fechar.Size = new System.Drawing.Size(75, 23);
            this.btn_fechar.TabIndex = 19;
            this.btn_fechar.TabStop = false;
            this.btn_fechar.Text = "&Fechar";
            this.btn_fechar.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btn_fechar.UseVisualStyleBackColor = true;
            this.btn_fechar.Click += new System.EventHandler(this.btn_fechar_Click);
            // 
            // btnFuncaoInterna
            // 
            this.btnFuncaoInterna.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnFuncaoInterna.Image = ((System.Drawing.Image)(resources.GetObject("btnFuncaoInterna.Image")));
            this.btnFuncaoInterna.Location = new System.Drawing.Point(686, 133);
            this.btnFuncaoInterna.Name = "btnFuncaoInterna";
            this.btnFuncaoInterna.Size = new System.Drawing.Size(34, 21);
            this.btnFuncaoInterna.TabIndex = 7;
            this.btnFuncaoInterna.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnFuncaoInterna.UseVisualStyleBackColor = true;
            this.btnFuncaoInterna.Click += new System.EventHandler(this.btnFuncaoInterna_Click);
            // 
            // lblFuncao
            // 
            this.lblFuncao.BackColor = System.Drawing.Color.LightGray;
            this.lblFuncao.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblFuncao.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblFuncao.Location = new System.Drawing.Point(12, 133);
            this.lblFuncao.Name = "lblFuncao";
            this.lblFuncao.ReadOnly = true;
            this.lblFuncao.Size = new System.Drawing.Size(209, 21);
            this.lblFuncao.TabIndex = 52;
            this.lblFuncao.TabStop = false;
            this.lblFuncao.Text = "Função Interna";
            // 
            // textFuncaoInterna
            // 
            this.textFuncaoInterna.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.textFuncaoInterna.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textFuncaoInterna.Enabled = false;
            this.textFuncaoInterna.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textFuncaoInterna.Location = new System.Drawing.Point(220, 133);
            this.textFuncaoInterna.MaxLength = 100;
            this.textFuncaoInterna.Name = "textFuncaoInterna";
            this.textFuncaoInterna.ReadOnly = true;
            this.textFuncaoInterna.Size = new System.Drawing.Size(467, 21);
            this.textFuncaoInterna.TabIndex = 38;
            this.textFuncaoInterna.TabStop = false;
            // 
            // textDataAdmissao
            // 
            this.textDataAdmissao.BackColor = System.Drawing.Color.LightGray;
            this.textDataAdmissao.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textDataAdmissao.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textDataAdmissao.Location = new System.Drawing.Point(12, 153);
            this.textDataAdmissao.Name = "textDataAdmissao";
            this.textDataAdmissao.ReadOnly = true;
            this.textDataAdmissao.Size = new System.Drawing.Size(209, 21);
            this.textDataAdmissao.TabIndex = 51;
            this.textDataAdmissao.TabStop = false;
            this.textDataAdmissao.Text = "Data de Admissão";
            // 
            // lblCoordenador
            // 
            this.lblCoordenador.BackColor = System.Drawing.Color.LightGray;
            this.lblCoordenador.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblCoordenador.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCoordenador.Location = new System.Drawing.Point(12, 173);
            this.lblCoordenador.Name = "lblCoordenador";
            this.lblCoordenador.ReadOnly = true;
            this.lblCoordenador.Size = new System.Drawing.Size(209, 21);
            this.lblCoordenador.TabIndex = 50;
            this.lblCoordenador.TabStop = false;
            this.lblCoordenador.Text = "Médico Coordenador";
            // 
            // btnMedico
            // 
            this.btnMedico.BackColor = System.Drawing.Color.White;
            this.btnMedico.Enabled = false;
            this.btnMedico.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnMedico.Image = ((System.Drawing.Image)(resources.GetObject("btnMedico.Image")));
            this.btnMedico.Location = new System.Drawing.Point(686, 173);
            this.btnMedico.Name = "btnMedico";
            this.btnMedico.Size = new System.Drawing.Size(34, 21);
            this.btnMedico.TabIndex = 9;
            this.btnMedico.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnMedico.UseVisualStyleBackColor = false;
            this.btnMedico.Click += new System.EventHandler(this.btnMedico_Click);
            // 
            // dataAdmissao
            // 
            this.dataAdmissao.Checked = false;
            this.dataAdmissao.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dataAdmissao.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dataAdmissao.Location = new System.Drawing.Point(220, 153);
            this.dataAdmissao.Name = "dataAdmissao";
            this.dataAdmissao.ShowCheckBox = true;
            this.dataAdmissao.Size = new System.Drawing.Size(500, 21);
            this.dataAdmissao.TabIndex = 8;
            // 
            // textCoordenador
            // 
            this.textCoordenador.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.textCoordenador.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textCoordenador.Enabled = false;
            this.textCoordenador.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textCoordenador.Location = new System.Drawing.Point(220, 173);
            this.textCoordenador.MaxLength = 100;
            this.textCoordenador.Name = "textCoordenador";
            this.textCoordenador.ReadOnly = true;
            this.textCoordenador.Size = new System.Drawing.Size(467, 21);
            this.textCoordenador.TabIndex = 40;
            this.textCoordenador.TabStop = false;
            // 
            // lblEmpresa
            // 
            this.lblEmpresa.BackColor = System.Drawing.Color.LightGray;
            this.lblEmpresa.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblEmpresa.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblEmpresa.Location = new System.Drawing.Point(12, 113);
            this.lblEmpresa.Name = "lblEmpresa";
            this.lblEmpresa.ReadOnly = true;
            this.lblEmpresa.Size = new System.Drawing.Size(209, 21);
            this.lblEmpresa.TabIndex = 49;
            this.lblEmpresa.TabStop = false;
            this.lblEmpresa.Text = "Empresa";
            // 
            // btnEmpresa
            // 
            this.btnEmpresa.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnEmpresa.Image = ((System.Drawing.Image)(resources.GetObject("btnEmpresa.Image")));
            this.btnEmpresa.Location = new System.Drawing.Point(686, 113);
            this.btnEmpresa.Name = "btnEmpresa";
            this.btnEmpresa.Size = new System.Drawing.Size(34, 21);
            this.btnEmpresa.TabIndex = 6;
            this.btnEmpresa.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnEmpresa.UseVisualStyleBackColor = true;
            this.btnEmpresa.Click += new System.EventHandler(this.btnEmpresa_Click);
            // 
            // textEmpresa
            // 
            this.textEmpresa.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.textEmpresa.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textEmpresa.Enabled = false;
            this.textEmpresa.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textEmpresa.Location = new System.Drawing.Point(220, 113);
            this.textEmpresa.MaxLength = 100;
            this.textEmpresa.Name = "textEmpresa";
            this.textEmpresa.ReadOnly = true;
            this.textEmpresa.Size = new System.Drawing.Size(467, 21);
            this.textEmpresa.TabIndex = 42;
            this.textEmpresa.TabStop = false;
            // 
            // lblRegistroOrgao
            // 
            this.lblRegistroOrgao.BackColor = System.Drawing.Color.LightGray;
            this.lblRegistroOrgao.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblRegistroOrgao.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblRegistroOrgao.Location = new System.Drawing.Point(12, 273);
            this.lblRegistroOrgao.Name = "lblRegistroOrgao";
            this.lblRegistroOrgao.ReadOnly = true;
            this.lblRegistroOrgao.Size = new System.Drawing.Size(209, 21);
            this.lblRegistroOrgao.TabIndex = 48;
            this.lblRegistroOrgao.TabStop = false;
            this.lblRegistroOrgao.Text = "Número de Registro";
            // 
            // lblLogin
            // 
            this.lblLogin.BackColor = System.Drawing.Color.LightGray;
            this.lblLogin.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblLogin.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblLogin.Location = new System.Drawing.Point(12, 93);
            this.lblLogin.Name = "lblLogin";
            this.lblLogin.ReadOnly = true;
            this.lblLogin.Size = new System.Drawing.Size(209, 21);
            this.lblLogin.TabIndex = 47;
            this.lblLogin.TabStop = false;
            this.lblLogin.Text = "Login";
            // 
            // lblEmail
            // 
            this.lblEmail.BackColor = System.Drawing.Color.LightGray;
            this.lblEmail.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblEmail.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblEmail.Location = new System.Drawing.Point(12, 73);
            this.lblEmail.Name = "lblEmail";
            this.lblEmail.ReadOnly = true;
            this.lblEmail.Size = new System.Drawing.Size(209, 21);
            this.lblEmail.TabIndex = 46;
            this.lblEmail.TabStop = false;
            this.lblEmail.Text = "E-mail";
            // 
            // lblRg
            // 
            this.lblRg.BackColor = System.Drawing.Color.LightGray;
            this.lblRg.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblRg.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblRg.Location = new System.Drawing.Point(12, 53);
            this.lblRg.Name = "lblRg";
            this.lblRg.ReadOnly = true;
            this.lblRg.Size = new System.Drawing.Size(209, 21);
            this.lblRg.TabIndex = 45;
            this.lblRg.TabStop = false;
            this.lblRg.Text = "Documento de Identidade";
            // 
            // lblCpf
            // 
            this.lblCpf.BackColor = System.Drawing.Color.LightGray;
            this.lblCpf.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblCpf.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCpf.Location = new System.Drawing.Point(12, 33);
            this.lblCpf.Name = "lblCpf";
            this.lblCpf.ReadOnly = true;
            this.lblCpf.Size = new System.Drawing.Size(209, 21);
            this.lblCpf.TabIndex = 44;
            this.lblCpf.TabStop = false;
            this.lblCpf.Text = "CPF";
            // 
            // lblNome
            // 
            this.lblNome.BackColor = System.Drawing.Color.LightGray;
            this.lblNome.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblNome.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNome.Location = new System.Drawing.Point(12, 13);
            this.lblNome.Name = "lblNome";
            this.lblNome.ReadOnly = true;
            this.lblNome.Size = new System.Drawing.Size(209, 21);
            this.lblNome.TabIndex = 43;
            this.lblNome.TabStop = false;
            this.lblNome.Text = "Nome";
            // 
            // textNome
            // 
            this.textNome.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textNome.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.textNome.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textNome.Location = new System.Drawing.Point(220, 13);
            this.textNome.MaxLength = 100;
            this.textNome.Name = "textNome";
            this.textNome.Size = new System.Drawing.Size(500, 21);
            this.textNome.TabIndex = 1;
            // 
            // textCpf
            // 
            this.textCpf.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textCpf.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textCpf.Location = new System.Drawing.Point(220, 33);
            this.textCpf.Mask = "999,999,999-99";
            this.textCpf.Name = "textCpf";
            this.textCpf.PromptChar = ' ';
            this.textCpf.Size = new System.Drawing.Size(500, 21);
            this.textCpf.TabIndex = 2;
            // 
            // textIdentidade
            // 
            this.textIdentidade.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textIdentidade.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.textIdentidade.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textIdentidade.Location = new System.Drawing.Point(220, 53);
            this.textIdentidade.MaxLength = 15;
            this.textIdentidade.Name = "textIdentidade";
            this.textIdentidade.Size = new System.Drawing.Size(500, 21);
            this.textIdentidade.TabIndex = 3;
            // 
            // textEmail
            // 
            this.textEmail.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textEmail.CharacterCasing = System.Windows.Forms.CharacterCasing.Lower;
            this.textEmail.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textEmail.Location = new System.Drawing.Point(220, 73);
            this.textEmail.MaxLength = 50;
            this.textEmail.Name = "textEmail";
            this.textEmail.Size = new System.Drawing.Size(500, 21);
            this.textEmail.TabIndex = 4;
            // 
            // textLogin
            // 
            this.textLogin.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textLogin.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.textLogin.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textLogin.Location = new System.Drawing.Point(220, 93);
            this.textLogin.MaxLength = 15;
            this.textLogin.Name = "textLogin";
            this.textLogin.Size = new System.Drawing.Size(500, 21);
            this.textLogin.TabIndex = 5;
            // 
            // textNumeroRegistro
            // 
            this.textNumeroRegistro.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textNumeroRegistro.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.textNumeroRegistro.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textNumeroRegistro.Location = new System.Drawing.Point(219, 273);
            this.textNumeroRegistro.MaxLength = 20;
            this.textNumeroRegistro.Name = "textNumeroRegistro";
            this.textNumeroRegistro.Size = new System.Drawing.Size(501, 21);
            this.textNumeroRegistro.TabIndex = 14;
            // 
            // lblDocumentacao
            // 
            this.lblDocumentacao.BackColor = System.Drawing.Color.LightGray;
            this.lblDocumentacao.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblDocumentacao.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDocumentacao.Location = new System.Drawing.Point(12, 193);
            this.lblDocumentacao.Name = "lblDocumentacao";
            this.lblDocumentacao.ReadOnly = true;
            this.lblDocumentacao.Size = new System.Drawing.Size(209, 21);
            this.lblDocumentacao.TabIndex = 53;
            this.lblDocumentacao.TabStop = false;
            this.lblDocumentacao.Text = "Elabora documentação?";
            // 
            // incluirUsuarioErrorProvider
            // 
            this.incluirUsuarioErrorProvider.ContainerControl = this;
            // 
            // cbDocumentacao
            // 
            this.cbDocumentacao.BackColor = System.Drawing.Color.LightGray;
            this.cbDocumentacao.BorderColor = System.Drawing.Color.DimGray;
            this.cbDocumentacao.BorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid;
            this.cbDocumentacao.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbDocumentacao.FormattingEnabled = true;
            this.cbDocumentacao.Location = new System.Drawing.Point(220, 193);
            this.cbDocumentacao.Name = "cbDocumentacao";
            this.cbDocumentacao.Size = new System.Drawing.Size(500, 21);
            this.cbDocumentacao.TabIndex = 10;
            this.cbDocumentacao.SelectedIndexChanged += new System.EventHandler(this.cbDocumentacao_SelectedIndexChanged);
            // 
            // lblRegistroMte
            // 
            this.lblRegistroMte.BackColor = System.Drawing.Color.LightGray;
            this.lblRegistroMte.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblRegistroMte.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblRegistroMte.Location = new System.Drawing.Point(12, 213);
            this.lblRegistroMte.Name = "lblRegistroMte";
            this.lblRegistroMte.ReadOnly = true;
            this.lblRegistroMte.Size = new System.Drawing.Size(209, 21);
            this.lblRegistroMte.TabIndex = 56;
            this.lblRegistroMte.TabStop = false;
            this.lblRegistroMte.Text = "Registro MTE";
            // 
            // textMte
            // 
            this.textMte.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textMte.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.textMte.Enabled = false;
            this.textMte.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textMte.Location = new System.Drawing.Point(219, 213);
            this.textMte.MaxLength = 15;
            this.textMte.Name = "textMte";
            this.textMte.Size = new System.Drawing.Size(501, 21);
            this.textMte.TabIndex = 11;
            // 
            // btnAssinatura
            // 
            this.btnAssinatura.Enabled = false;
            this.btnAssinatura.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnAssinatura.Image = global::SWS.Properties.Resources.Gravar;
            this.btnAssinatura.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnAssinatura.Location = new System.Drawing.Point(12, 508);
            this.btnAssinatura.Name = "btnAssinatura";
            this.btnAssinatura.Size = new System.Drawing.Size(75, 23);
            this.btnAssinatura.TabIndex = 17;
            this.btnAssinatura.Text = "&Abrir";
            this.btnAssinatura.UseVisualStyleBackColor = true;
            this.btnAssinatura.Click += new System.EventHandler(this.btnAssinatura_Click);
            // 
            // lblAssinatura
            // 
            this.lblAssinatura.AutoSize = true;
            this.lblAssinatura.Location = new System.Drawing.Point(9, 364);
            this.lblAssinatura.Name = "lblAssinatura";
            this.lblAssinatura.Size = new System.Drawing.Size(134, 13);
            this.lblAssinatura.TabIndex = 59;
            this.lblAssinatura.Text = "Assinatura de Documentos";
            // 
            // pbAssinatura
            // 
            this.pbAssinatura.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pbAssinatura.Location = new System.Drawing.Point(12, 360);
            this.pbAssinatura.Name = "pbAssinatura";
            this.pbAssinatura.Size = new System.Drawing.Size(333, 142);
            this.pbAssinatura.TabIndex = 58;
            this.pbAssinatura.TabStop = false;
            // 
            // btn_excluir_perfil
            // 
            this.btn_excluir_perfil.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_excluir_perfil.Image = global::SWS.Properties.Resources.lixeira;
            this.btn_excluir_perfil.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btn_excluir_perfil.Location = new System.Drawing.Point(92, 3);
            this.btn_excluir_perfil.Name = "btn_excluir_perfil";
            this.btn_excluir_perfil.Size = new System.Drawing.Size(83, 23);
            this.btn_excluir_perfil.TabIndex = 19;
            this.btn_excluir_perfil.Text = "&Excluir";
            this.btn_excluir_perfil.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btn_excluir_perfil.UseVisualStyleBackColor = true;
            this.btn_excluir_perfil.Click += new System.EventHandler(this.btn_excluir_perfil_Click);
            // 
            // grb_perfil
            // 
            this.grb_perfil.BackColor = System.Drawing.Color.White;
            this.grb_perfil.Controls.Add(this.dgvPerfil);
            this.grb_perfil.Location = new System.Drawing.Point(12, 535);
            this.grb_perfil.Name = "grb_perfil";
            this.grb_perfil.Size = new System.Drawing.Size(711, 158);
            this.grb_perfil.TabIndex = 60;
            this.grb_perfil.TabStop = false;
            this.grb_perfil.Text = "Perfil";
            // 
            // dgvPerfil
            // 
            this.dgvPerfil.AllowUserToAddRows = false;
            this.dgvPerfil.AllowUserToDeleteRows = false;
            this.dgvPerfil.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.DisplayedCells;
            this.dgvPerfil.BackgroundColor = System.Drawing.Color.White;
            this.dgvPerfil.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.Color.LightGray;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvPerfil.DefaultCellStyle = dataGridViewCellStyle2;
            this.dgvPerfil.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvPerfil.Location = new System.Drawing.Point(3, 16);
            this.dgvPerfil.MultiSelect = false;
            this.dgvPerfil.Name = "dgvPerfil";
            this.dgvPerfil.ReadOnly = true;
            this.dgvPerfil.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvPerfil.Size = new System.Drawing.Size(705, 139);
            this.dgvPerfil.TabIndex = 12;
            this.dgvPerfil.TabStop = false;
            // 
            // btn_incluir_perfil
            // 
            this.btn_incluir_perfil.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_incluir_perfil.Image = global::SWS.Properties.Resources.icone_mais;
            this.btn_incluir_perfil.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btn_incluir_perfil.Location = new System.Drawing.Point(3, 3);
            this.btn_incluir_perfil.Name = "btn_incluir_perfil";
            this.btn_incluir_perfil.Size = new System.Drawing.Size(83, 23);
            this.btn_incluir_perfil.TabIndex = 18;
            this.btn_incluir_perfil.Text = "&Incluir";
            this.btn_incluir_perfil.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btn_incluir_perfil.UseVisualStyleBackColor = true;
            this.btn_incluir_perfil.Click += new System.EventHandler(this.btn_incluir_perfil_Click);
            // 
            // btExcluirCliente
            // 
            this.btExcluirCliente.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btExcluirCliente.Image = global::SWS.Properties.Resources.lixeira;
            this.btExcluirCliente.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btExcluirCliente.Location = new System.Drawing.Point(92, 3);
            this.btExcluirCliente.Name = "btExcluirCliente";
            this.btExcluirCliente.Size = new System.Drawing.Size(83, 23);
            this.btExcluirCliente.TabIndex = 21;
            this.btExcluirCliente.Text = "&Excluir";
            this.btExcluirCliente.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btExcluirCliente.UseVisualStyleBackColor = true;
            this.btExcluirCliente.Click += new System.EventHandler(this.btExcluirCliente_Click);
            // 
            // grbCliente
            // 
            this.grbCliente.BackColor = System.Drawing.Color.White;
            this.grbCliente.Controls.Add(this.dgvCliente);
            this.grbCliente.Location = new System.Drawing.Point(12, 732);
            this.grbCliente.Name = "grbCliente";
            this.grbCliente.Size = new System.Drawing.Size(711, 158);
            this.grbCliente.TabIndex = 63;
            this.grbCliente.TabStop = false;
            this.grbCliente.Text = "Clientes";
            // 
            // dgvCliente
            // 
            this.dgvCliente.AllowUserToAddRows = false;
            this.dgvCliente.AllowUserToDeleteRows = false;
            this.dgvCliente.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.DisplayedCells;
            this.dgvCliente.BackgroundColor = System.Drawing.Color.White;
            this.dgvCliente.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.LightGray;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvCliente.DefaultCellStyle = dataGridViewCellStyle1;
            this.dgvCliente.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvCliente.Location = new System.Drawing.Point(3, 16);
            this.dgvCliente.MultiSelect = false;
            this.dgvCliente.Name = "dgvCliente";
            this.dgvCliente.ReadOnly = true;
            this.dgvCliente.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvCliente.Size = new System.Drawing.Size(705, 139);
            this.dgvCliente.TabIndex = 12;
            this.dgvCliente.TabStop = false;
            // 
            // btIncluirCliente
            // 
            this.btIncluirCliente.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btIncluirCliente.Image = global::SWS.Properties.Resources.icone_mais;
            this.btIncluirCliente.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btIncluirCliente.Location = new System.Drawing.Point(3, 3);
            this.btIncluirCliente.Name = "btIncluirCliente";
            this.btIncluirCliente.Size = new System.Drawing.Size(83, 23);
            this.btIncluirCliente.TabIndex = 20;
            this.btIncluirCliente.Text = "&Incluir";
            this.btIncluirCliente.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btIncluirCliente.UseVisualStyleBackColor = true;
            this.btIncluirCliente.Click += new System.EventHandler(this.btIncluirCliente_Click);
            // 
            // lblUsuarioExterno
            // 
            this.lblUsuarioExterno.BackColor = System.Drawing.Color.LightGray;
            this.lblUsuarioExterno.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblUsuarioExterno.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblUsuarioExterno.Location = new System.Drawing.Point(12, 233);
            this.lblUsuarioExterno.Name = "lblUsuarioExterno";
            this.lblUsuarioExterno.ReadOnly = true;
            this.lblUsuarioExterno.Size = new System.Drawing.Size(209, 21);
            this.lblUsuarioExterno.TabIndex = 64;
            this.lblUsuarioExterno.TabStop = false;
            this.lblUsuarioExterno.Text = "Usuário Externo?";
            // 
            // cbExterno
            // 
            this.cbExterno.BackColor = System.Drawing.Color.LightGray;
            this.cbExterno.BorderColor = System.Drawing.Color.DimGray;
            this.cbExterno.BorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid;
            this.cbExterno.FormattingEnabled = true;
            this.cbExterno.Location = new System.Drawing.Point(220, 233);
            this.cbExterno.Name = "cbExterno";
            this.cbExterno.Size = new System.Drawing.Size(500, 21);
            this.cbExterno.TabIndex = 12;
            // 
            // flpAcaoCliente
            // 
            this.flpAcaoCliente.Controls.Add(this.btIncluirCliente);
            this.flpAcaoCliente.Controls.Add(this.btExcluirCliente);
            this.flpAcaoCliente.Location = new System.Drawing.Point(12, 893);
            this.flpAcaoCliente.Name = "flpAcaoCliente";
            this.flpAcaoCliente.Size = new System.Drawing.Size(708, 31);
            this.flpAcaoCliente.TabIndex = 67;
            // 
            // flowLayoutPanel1
            // 
            this.flowLayoutPanel1.Controls.Add(this.btn_incluir_perfil);
            this.flowLayoutPanel1.Controls.Add(this.btn_excluir_perfil);
            this.flowLayoutPanel1.Location = new System.Drawing.Point(12, 696);
            this.flowLayoutPanel1.Name = "flowLayoutPanel1";
            this.flowLayoutPanel1.Size = new System.Drawing.Size(711, 31);
            this.flowLayoutPanel1.TabIndex = 68;
            // 
            // textEmissorCertificado
            // 
            this.textEmissorCertificado.BackColor = System.Drawing.Color.LightGray;
            this.textEmissorCertificado.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textEmissorCertificado.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textEmissorCertificado.Location = new System.Drawing.Point(12, 253);
            this.textEmissorCertificado.Name = "textEmissorCertificado";
            this.textEmissorCertificado.ReadOnly = true;
            this.textEmissorCertificado.Size = new System.Drawing.Size(209, 21);
            this.textEmissorCertificado.TabIndex = 69;
            this.textEmissorCertificado.TabStop = false;
            this.textEmissorCertificado.Text = "Emissor do certificado";
            // 
            // cbEmissorCertificado
            // 
            this.cbEmissorCertificado.BackColor = System.Drawing.Color.LightGray;
            this.cbEmissorCertificado.BorderColor = System.Drawing.Color.DimGray;
            this.cbEmissorCertificado.BorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid;
            this.cbEmissorCertificado.FormattingEnabled = true;
            this.cbEmissorCertificado.Location = new System.Drawing.Point(220, 253);
            this.cbEmissorCertificado.Name = "cbEmissorCertificado";
            this.cbEmissorCertificado.Size = new System.Drawing.Size(500, 21);
            this.cbEmissorCertificado.TabIndex = 13;
            this.cbEmissorCertificado.SelectedIndexChanged += new System.EventHandler(this.cbEmissorCertificado_SelectedIndexChanged);
            // 
            // lblOrgaoEmissor
            // 
            this.lblOrgaoEmissor.BackColor = System.Drawing.Color.LightGray;
            this.lblOrgaoEmissor.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblOrgaoEmissor.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblOrgaoEmissor.Location = new System.Drawing.Point(12, 293);
            this.lblOrgaoEmissor.Name = "lblOrgaoEmissor";
            this.lblOrgaoEmissor.ReadOnly = true;
            this.lblOrgaoEmissor.Size = new System.Drawing.Size(209, 21);
            this.lblOrgaoEmissor.TabIndex = 71;
            this.lblOrgaoEmissor.TabStop = false;
            this.lblOrgaoEmissor.Text = "Orgão emissor";
            // 
            // textOrgaoEmissor
            // 
            this.textOrgaoEmissor.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textOrgaoEmissor.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.textOrgaoEmissor.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textOrgaoEmissor.Location = new System.Drawing.Point(220, 293);
            this.textOrgaoEmissor.MaxLength = 20;
            this.textOrgaoEmissor.Name = "textOrgaoEmissor";
            this.textOrgaoEmissor.Size = new System.Drawing.Size(500, 21);
            this.textOrgaoEmissor.TabIndex = 15;
            // 
            // lblUfEmissor
            // 
            this.lblUfEmissor.BackColor = System.Drawing.Color.LightGray;
            this.lblUfEmissor.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblUfEmissor.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblUfEmissor.Location = new System.Drawing.Point(12, 313);
            this.lblUfEmissor.Name = "lblUfEmissor";
            this.lblUfEmissor.ReadOnly = true;
            this.lblUfEmissor.Size = new System.Drawing.Size(209, 21);
            this.lblUfEmissor.TabIndex = 73;
            this.lblUfEmissor.TabStop = false;
            this.lblUfEmissor.Text = "UF do Emissor";
            // 
            // cbUfEmissor
            // 
            this.cbUfEmissor.BackColor = System.Drawing.Color.LightGray;
            this.cbUfEmissor.BorderColor = System.Drawing.Color.DimGray;
            this.cbUfEmissor.BorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid;
            this.cbUfEmissor.FormattingEnabled = true;
            this.cbUfEmissor.Location = new System.Drawing.Point(220, 313);
            this.cbUfEmissor.Name = "cbUfEmissor";
            this.cbUfEmissor.Size = new System.Drawing.Size(500, 21);
            this.cbUfEmissor.TabIndex = 16;
            // 
            // frmUsuarioIncluir
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(784, 561);
            this.Name = "frmUsuarioIncluir";
            this.Text = "INCLUIR USUÁRIO";
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.frmUsuarioIncluir_KeyDown);
            this.spForm.Panel1.ResumeLayout(false);
            this.spForm.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.spForm)).EndInit();
            this.spForm.ResumeLayout(false);
            this.pnlForm.ResumeLayout(false);
            this.pnlForm.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.banner)).EndInit();
            this.flpAcao.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.incluirUsuarioErrorProvider)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbAssinatura)).EndInit();
            this.grb_perfil.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvPerfil)).EndInit();
            this.grbCliente.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvCliente)).EndInit();
            this.flpAcaoCliente.ResumeLayout(false);
            this.flowLayoutPanel1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        protected System.Windows.Forms.Button btn_fechar;
        protected System.Windows.Forms.Button bt_gravar;
        protected System.Windows.Forms.Button btnLimpar;
        protected System.Windows.Forms.Button btnFuncaoInterna;
        protected System.Windows.Forms.TextBox lblFuncao;
        protected System.Windows.Forms.TextBox textFuncaoInterna;
        protected System.Windows.Forms.TextBox textDataAdmissao;
        protected System.Windows.Forms.TextBox lblCoordenador;
        protected System.Windows.Forms.Button btnMedico;
        protected System.Windows.Forms.DateTimePicker dataAdmissao;
        protected System.Windows.Forms.TextBox textCoordenador;
        protected System.Windows.Forms.TextBox lblEmpresa;
        protected System.Windows.Forms.Button btnEmpresa;
        protected System.Windows.Forms.TextBox textEmpresa;
        protected System.Windows.Forms.TextBox lblRegistroOrgao;
        protected System.Windows.Forms.TextBox lblLogin;
        protected System.Windows.Forms.TextBox lblEmail;
        protected System.Windows.Forms.TextBox lblRg;
        protected System.Windows.Forms.TextBox lblCpf;
        protected System.Windows.Forms.TextBox lblNome;
        protected System.Windows.Forms.TextBox textNome;
        protected System.Windows.Forms.MaskedTextBox textCpf;
        protected System.Windows.Forms.TextBox textIdentidade;
        protected System.Windows.Forms.TextBox textEmail;
        protected System.Windows.Forms.TextBox textLogin;
        protected System.Windows.Forms.TextBox textNumeroRegistro;
        protected System.Windows.Forms.TextBox lblDocumentacao;
        protected System.Windows.Forms.TextBox lblRegistroMte;
        protected System.Windows.Forms.TextBox textMte;
        protected System.Windows.Forms.Button btnAssinatura;
        protected System.Windows.Forms.Label lblAssinatura;
        protected System.Windows.Forms.PictureBox pbAssinatura;
        protected System.Windows.Forms.Button btn_excluir_perfil;
        protected System.Windows.Forms.GroupBox grb_perfil;
        protected System.Windows.Forms.DataGridView dgvPerfil;
        protected System.Windows.Forms.Button btn_incluir_perfil;
        protected System.Windows.Forms.FlowLayoutPanel flpAcao;
        protected System.Windows.Forms.ErrorProvider incluirUsuarioErrorProvider;
        protected ComboBoxWithBorder cbDocumentacao;
        protected System.Windows.Forms.Button btExcluirCliente;
        protected System.Windows.Forms.GroupBox grbCliente;
        protected System.Windows.Forms.DataGridView dgvCliente;
        protected System.Windows.Forms.Button btIncluirCliente;
        protected ComboBoxWithBorder cbExterno;
        protected System.Windows.Forms.TextBox lblUsuarioExterno;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel1;
        private System.Windows.Forms.FlowLayoutPanel flpAcaoCliente;
        protected ComboBoxWithBorder cbEmissorCertificado;
        protected System.Windows.Forms.TextBox textEmissorCertificado;
        protected ComboBoxWithBorder cbUfEmissor;
        protected System.Windows.Forms.TextBox lblUfEmissor;
        protected System.Windows.Forms.TextBox textOrgaoEmissor;
        protected System.Windows.Forms.TextBox lblOrgaoEmissor;
    }
}