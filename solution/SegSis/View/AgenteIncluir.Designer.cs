﻿namespace SegSis.View
{
    partial class frm_agente_incluir
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frm_agente_incluir));
            this.grb_dados = new System.Windows.Forms.GroupBox();
            this.grb_risco = new System.Windows.Forms.GroupBox();
            this.cb_tipo = new System.Windows.Forms.ComboBox();
            this.lbl_limpar = new System.Windows.Forms.Button();
            this.lbl_limite = new System.Windows.Forms.Label();
            this.text_limite = new System.Windows.Forms.TextBox();
            this.lbl_dano = new System.Windows.Forms.Label();
            this.text_dano = new System.Windows.Forms.TextBox();
            this.lbl_trajetoria = new System.Windows.Forms.Label();
            this.text_trajetoria = new System.Windows.Forms.TextBox();
            this.lbl_descricao = new System.Windows.Forms.Label();
            this.text_descricao = new System.Windows.Forms.TextBox();
            this.grb_paginacao = new System.Windows.Forms.GroupBox();
            this.btn_fechar = new System.Windows.Forms.Button();
            this.btn_gravar = new System.Windows.Forms.Button();
            this.incluirAgenteErrorProvider = new System.Windows.Forms.ErrorProvider(this.components);
            this.panel_banner.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox_banner)).BeginInit();
            this.panel.SuspendLayout();
            this.grb_dados.SuspendLayout();
            this.grb_risco.SuspendLayout();
            this.grb_paginacao.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.incluirAgenteErrorProvider)).BeginInit();
            this.SuspendLayout();
            // 
            // panel
            // 
            this.panel.Controls.Add(this.grb_paginacao);
            this.panel.Controls.Add(this.grb_dados);
            // 
            // grb_dados
            // 
            this.grb_dados.BackColor = System.Drawing.Color.White;
            this.grb_dados.Controls.Add(this.grb_risco);
            this.grb_dados.Controls.Add(this.lbl_limpar);
            this.grb_dados.Controls.Add(this.lbl_limite);
            this.grb_dados.Controls.Add(this.text_limite);
            this.grb_dados.Controls.Add(this.lbl_dano);
            this.grb_dados.Controls.Add(this.text_dano);
            this.grb_dados.Controls.Add(this.lbl_trajetoria);
            this.grb_dados.Controls.Add(this.text_trajetoria);
            this.grb_dados.Controls.Add(this.lbl_descricao);
            this.grb_dados.Controls.Add(this.text_descricao);
            this.grb_dados.Location = new System.Drawing.Point(3, 3);
            this.grb_dados.Name = "grb_dados";
            this.grb_dados.Size = new System.Drawing.Size(781, 455);
            this.grb_dados.TabIndex = 0;
            this.grb_dados.TabStop = false;
            this.grb_dados.Text = "Dados";
            // 
            // grb_risco
            // 
            this.grb_risco.Controls.Add(this.cb_tipo);
            this.grb_risco.Location = new System.Drawing.Point(551, 16);
            this.grb_risco.Name = "grb_risco";
            this.grb_risco.Size = new System.Drawing.Size(176, 48);
            this.grb_risco.TabIndex = 4;
            this.grb_risco.TabStop = false;
            this.grb_risco.Text = "Risco";
            // 
            // cb_tipo
            // 
            this.cb_tipo.BackColor = System.Drawing.Color.LightGray;
            this.cb_tipo.Dock = System.Windows.Forms.DockStyle.Fill;
            this.cb_tipo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cb_tipo.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cb_tipo.FormattingEnabled = true;
            this.cb_tipo.Location = new System.Drawing.Point(3, 16);
            this.cb_tipo.Name = "cb_tipo";
            this.cb_tipo.Size = new System.Drawing.Size(170, 21);
            this.cb_tipo.TabIndex = 4;
            // 
            // lbl_limpar
            // 
            this.lbl_limpar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.lbl_limpar.Image = global::SegSis.Properties.Resources.vassoura;
            this.lbl_limpar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.lbl_limpar.Location = new System.Drawing.Point(9, 168);
            this.lbl_limpar.Name = "lbl_limpar";
            this.lbl_limpar.Size = new System.Drawing.Size(64, 23);
            this.lbl_limpar.TabIndex = 6;
            this.lbl_limpar.TabStop = false;
            this.lbl_limpar.Text = "&Limpar";
            this.lbl_limpar.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.lbl_limpar.UseVisualStyleBackColor = true;
            this.lbl_limpar.Click += new System.EventHandler(this.lbl_limpar_Click);
            // 
            // lbl_limite
            // 
            this.lbl_limite.AutoSize = true;
            this.lbl_limite.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_limite.Location = new System.Drawing.Point(548, 67);
            this.lbl_limite.Name = "lbl_limite";
            this.lbl_limite.Size = new System.Drawing.Size(34, 13);
            this.lbl_limite.TabIndex = 15;
            this.lbl_limite.Text = "Limite";
            // 
            // text_limite
            // 
            this.text_limite.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.text_limite.Location = new System.Drawing.Point(551, 83);
            this.text_limite.MaxLength = 100;
            this.text_limite.Name = "text_limite";
            this.text_limite.Size = new System.Drawing.Size(202, 20);
            this.text_limite.TabIndex = 5;
            // 
            // lbl_dano
            // 
            this.lbl_dano.AutoSize = true;
            this.lbl_dano.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_dano.Location = new System.Drawing.Point(6, 110);
            this.lbl_dano.Name = "lbl_dano";
            this.lbl_dano.Size = new System.Drawing.Size(33, 13);
            this.lbl_dano.TabIndex = 13;
            this.lbl_dano.Text = "Dano";
            // 
            // text_dano
            // 
            this.text_dano.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.text_dano.Location = new System.Drawing.Point(9, 126);
            this.text_dano.MaxLength = 255;
            this.text_dano.Multiline = true;
            this.text_dano.Name = "text_dano";
            this.text_dano.Size = new System.Drawing.Size(511, 36);
            this.text_dano.TabIndex = 3;
            // 
            // lbl_trajetoria
            // 
            this.lbl_trajetoria.AutoSize = true;
            this.lbl_trajetoria.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_trajetoria.Location = new System.Drawing.Point(6, 55);
            this.lbl_trajetoria.Name = "lbl_trajetoria";
            this.lbl_trajetoria.Size = new System.Drawing.Size(51, 13);
            this.lbl_trajetoria.TabIndex = 11;
            this.lbl_trajetoria.Text = "Trajetoria";
            // 
            // text_trajetoria
            // 
            this.text_trajetoria.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.text_trajetoria.Location = new System.Drawing.Point(9, 71);
            this.text_trajetoria.MaxLength = 255;
            this.text_trajetoria.Multiline = true;
            this.text_trajetoria.Name = "text_trajetoria";
            this.text_trajetoria.Size = new System.Drawing.Size(511, 36);
            this.text_trajetoria.TabIndex = 2;
            // 
            // lbl_descricao
            // 
            this.lbl_descricao.AutoSize = true;
            this.lbl_descricao.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_descricao.Location = new System.Drawing.Point(6, 16);
            this.lbl_descricao.Name = "lbl_descricao";
            this.lbl_descricao.Size = new System.Drawing.Size(55, 13);
            this.lbl_descricao.TabIndex = 7;
            this.lbl_descricao.Text = "Descrição";
            // 
            // text_descricao
            // 
            this.text_descricao.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.text_descricao.Location = new System.Drawing.Point(9, 32);
            this.text_descricao.MaxLength = 100;
            this.text_descricao.Name = "text_descricao";
            this.text_descricao.Size = new System.Drawing.Size(511, 20);
            this.text_descricao.TabIndex = 1;
            // 
            // grb_paginacao
            // 
            this.grb_paginacao.BackColor = System.Drawing.Color.White;
            this.grb_paginacao.Controls.Add(this.btn_fechar);
            this.grb_paginacao.Controls.Add(this.btn_gravar);
            this.grb_paginacao.Location = new System.Drawing.Point(3, 464);
            this.grb_paginacao.Name = "grb_paginacao";
            this.grb_paginacao.Size = new System.Drawing.Size(781, 51);
            this.grb_paginacao.TabIndex = 28;
            this.grb_paginacao.TabStop = false;
            // 
            // btn_fechar
            // 
            this.btn_fechar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_fechar.Image = global::SegSis.Properties.Resources.close;
            this.btn_fechar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btn_fechar.Location = new System.Drawing.Point(92, 19);
            this.btn_fechar.Name = "btn_fechar";
            this.btn_fechar.Size = new System.Drawing.Size(77, 23);
            this.btn_fechar.TabIndex = 8;
            this.btn_fechar.Text = "&Fechar";
            this.btn_fechar.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btn_fechar.UseVisualStyleBackColor = true;
            this.btn_fechar.Click += new System.EventHandler(this.btn_fechar_Click);
            // 
            // btn_gravar
            // 
            this.btn_gravar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_gravar.Image = global::SegSis.Properties.Resources.fechar_ico;
            this.btn_gravar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btn_gravar.Location = new System.Drawing.Point(9, 19);
            this.btn_gravar.Name = "btn_gravar";
            this.btn_gravar.Size = new System.Drawing.Size(77, 23);
            this.btn_gravar.TabIndex = 7;
            this.btn_gravar.Text = "&Gravar";
            this.btn_gravar.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btn_gravar.UseVisualStyleBackColor = true;
            this.btn_gravar.Click += new System.EventHandler(this.btn_gravar_Click);
            // 
            // incluirAgenteErrorProvider
            // 
            this.incluirAgenteErrorProvider.ContainerControl = this;
            // 
            // frm_agente_incluir
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(794, 568);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.KeyPreview = true;
            this.Name = "frm_agente_incluir";
            this.Text = "INCLUIR AGENTE";
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.frm_agente_incluir_KeyDown);
            this.panel_banner.ResumeLayout(false);
            this.panel_banner.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox_banner)).EndInit();
            this.panel.ResumeLayout(false);
            this.grb_dados.ResumeLayout(false);
            this.grb_dados.PerformLayout();
            this.grb_risco.ResumeLayout(false);
            this.grb_paginacao.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.incluirAgenteErrorProvider)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox grb_dados;
        private System.Windows.Forms.Button lbl_limpar;
        private System.Windows.Forms.Label lbl_descricao;
        private System.Windows.Forms.TextBox text_descricao;
        private System.Windows.Forms.GroupBox grb_paginacao;
        private System.Windows.Forms.Button btn_fechar;
        private System.Windows.Forms.Button btn_gravar;
        private System.Windows.Forms.ErrorProvider incluirAgenteErrorProvider;
        private System.Windows.Forms.Label lbl_limite;
        private System.Windows.Forms.TextBox text_limite;
        private System.Windows.Forms.Label lbl_dano;
        private System.Windows.Forms.TextBox text_dano;
        private System.Windows.Forms.Label lbl_trajetoria;
        private System.Windows.Forms.ComboBox cb_tipo;
        private System.Windows.Forms.TextBox text_trajetoria;
        private System.Windows.Forms.GroupBox grb_risco;
    }
}