﻿using SWS.Entidade;
using SWS.Facade;
using SWS.View.Resources;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace SWS.View
{
    public partial class frmPcmsoAvulsoGheAlterar : frmPcmsoAvulsoGheIncluir
    {
        public frmPcmsoAvulsoGheAlterar(Ghe ghe)
        {
            InitializeComponent();
            this.Ghe = ghe;

            textDescricao.Text = Ghe.Descricao;
            textExposto.Text = Ghe.Nexp.ToString();
            textNumeroPo.Text = Ghe.NumeroPo;
        }

        protected override void btn_salvar_Click(object sender, EventArgs e)
        {
            try
            {
                this.IncluirGheErrorProvider.Clear();
                this.Cursor = Cursors.WaitCursor;

                if (validaCampos())
                {
                    EstudoFacade pcmsoFacade = EstudoFacade.getInstance();

                    Ghe =  pcmsoFacade.updateGhe(new Ghe((long)Ghe.Id, Ghe.Estudo, textDescricao.Text, Convert.ToInt32(textExposto.Text), ApplicationConstants.ATIVO, textNumeroPo.Text));

                    this.Close();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }
    }
}
