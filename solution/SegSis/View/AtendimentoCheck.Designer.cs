﻿namespace SWS.View
{
    partial class frmAtendimentoCheck
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            this.flpAcao = new System.Windows.Forms.FlowLayoutPanel();
            this.btn_exibir_transcritos = new System.Windows.Forms.Button();
            this.btn_cancelado = new System.Windows.Forms.Button();
            this.btn_fechar = new System.Windows.Forms.Button();
            this.dgvExameAtendimento = new System.Windows.Forms.DataGridView();
            this.grbExameAtendimento = new System.Windows.Forms.GroupBox();
            this.btn_excluirRealizado = new System.Windows.Forms.Button();
            this.flpAcaoRealizado = new System.Windows.Forms.FlowLayoutPanel();
            this.dgvExameRealizado = new System.Windows.Forms.DataGridView();
            this.grb_realizados = new System.Windows.Forms.GroupBox();
            this.btn_cancelarEncaminhamento = new System.Windows.Forms.Button();
            this.btn_incluir = new System.Windows.Forms.Button();
            this.btn_cancelar = new System.Windows.Forms.Button();
            this.btn_finalizar = new System.Windows.Forms.Button();
            this.flpPendente = new System.Windows.Forms.FlowLayoutPanel();
            this.dg_examePendente = new System.Windows.Forms.DataGridView();
            this.grb_pendente = new System.Windows.Forms.GroupBox();
            this.chkMarcarTodosPendentes = new System.Windows.Forms.CheckBox();
            this.chkmarcarTodosRealizados = new System.Windows.Forms.CheckBox();
            ((System.ComponentModel.ISupportInitialize)(this.spForm)).BeginInit();
            this.spForm.Panel1.SuspendLayout();
            this.spForm.Panel2.SuspendLayout();
            this.spForm.SuspendLayout();
            this.pnlForm.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.banner)).BeginInit();
            this.flpAcao.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvExameAtendimento)).BeginInit();
            this.grbExameAtendimento.SuspendLayout();
            this.flpAcaoRealizado.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvExameRealizado)).BeginInit();
            this.grb_realizados.SuspendLayout();
            this.flpPendente.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dg_examePendente)).BeginInit();
            this.grb_pendente.SuspendLayout();
            this.SuspendLayout();
            // 
            // spForm
            // 
            this.spForm.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            // 
            // spForm.Panel1
            // 
            this.spForm.Panel1.Controls.Add(this.flpAcao);
            // 
            // pnlForm
            // 
            this.pnlForm.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.pnlForm.AutoScroll = true;
            this.pnlForm.Controls.Add(this.chkmarcarTodosRealizados);
            this.pnlForm.Controls.Add(this.chkMarcarTodosPendentes);
            this.pnlForm.Controls.Add(this.grbExameAtendimento);
            this.pnlForm.Controls.Add(this.flpAcaoRealizado);
            this.pnlForm.Controls.Add(this.grb_realizados);
            this.pnlForm.Controls.Add(this.flpPendente);
            this.pnlForm.Controls.Add(this.grb_pendente);
            this.pnlForm.Size = new System.Drawing.Size(781, 464);
            // 
            // banner
            // 
            this.banner.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            // 
            // flpAcao
            // 
            this.flpAcao.Controls.Add(this.btn_exibir_transcritos);
            this.flpAcao.Controls.Add(this.btn_cancelado);
            this.flpAcao.Controls.Add(this.btn_fechar);
            this.flpAcao.Dock = System.Windows.Forms.DockStyle.Fill;
            this.flpAcao.Location = new System.Drawing.Point(0, 0);
            this.flpAcao.Name = "flpAcao";
            this.flpAcao.Size = new System.Drawing.Size(784, 35);
            this.flpAcao.TabIndex = 0;
            // 
            // btn_exibir_transcritos
            // 
            this.btn_exibir_transcritos.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.btn_exibir_transcritos.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_exibir_transcritos.Location = new System.Drawing.Point(3, 3);
            this.btn_exibir_transcritos.Name = "btn_exibir_transcritos";
            this.btn_exibir_transcritos.Size = new System.Drawing.Size(75, 23);
            this.btn_exibir_transcritos.TabIndex = 6;
            this.btn_exibir_transcritos.Text = "Transcritos";
            this.btn_exibir_transcritos.UseVisualStyleBackColor = false;
            this.btn_exibir_transcritos.Click += new System.EventHandler(this.btn_exibir_transcritos_Click);
            // 
            // btn_cancelado
            // 
            this.btn_cancelado.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.btn_cancelado.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_cancelado.Location = new System.Drawing.Point(84, 3);
            this.btn_cancelado.Name = "btn_cancelado";
            this.btn_cancelado.Size = new System.Drawing.Size(75, 23);
            this.btn_cancelado.TabIndex = 5;
            this.btn_cancelado.Text = "Cancelados";
            this.btn_cancelado.UseVisualStyleBackColor = false;
            this.btn_cancelado.Click += new System.EventHandler(this.btn_cancelado_Click);
            // 
            // btn_fechar
            // 
            this.btn_fechar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_fechar.Image = global::SWS.Properties.Resources.close;
            this.btn_fechar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btn_fechar.Location = new System.Drawing.Point(165, 3);
            this.btn_fechar.Name = "btn_fechar";
            this.btn_fechar.Size = new System.Drawing.Size(75, 23);
            this.btn_fechar.TabIndex = 4;
            this.btn_fechar.Text = "&Fechar";
            this.btn_fechar.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btn_fechar.UseVisualStyleBackColor = true;
            this.btn_fechar.Click += new System.EventHandler(this.btn_fechar_Click);
            // 
            // dgvExameAtendimento
            // 
            this.dgvExameAtendimento.AllowUserToAddRows = false;
            this.dgvExameAtendimento.AllowUserToDeleteRows = false;
            this.dgvExameAtendimento.AllowUserToOrderColumns = true;
            this.dgvExameAtendimento.AllowUserToResizeRows = false;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.Gainsboro;
            this.dgvExameAtendimento.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle1;
            this.dgvExameAtendimento.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this.dgvExameAtendimento.BackgroundColor = System.Drawing.Color.White;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.Color.RoyalBlue;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvExameAtendimento.DefaultCellStyle = dataGridViewCellStyle2;
            this.dgvExameAtendimento.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvExameAtendimento.Location = new System.Drawing.Point(3, 16);
            this.dgvExameAtendimento.MultiSelect = false;
            this.dgvExameAtendimento.Name = "dgvExameAtendimento";
            this.dgvExameAtendimento.ReadOnly = true;
            this.dgvExameAtendimento.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvExameAtendimento.Size = new System.Drawing.Size(632, 169);
            this.dgvExameAtendimento.TabIndex = 0;
            // 
            // grbExameAtendimento
            // 
            this.grbExameAtendimento.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.grbExameAtendimento.Controls.Add(this.dgvExameAtendimento);
            this.grbExameAtendimento.Location = new System.Drawing.Point(12, 507);
            this.grbExameAtendimento.Name = "grbExameAtendimento";
            this.grbExameAtendimento.Size = new System.Drawing.Size(638, 188);
            this.grbExameAtendimento.TabIndex = 10;
            this.grbExameAtendimento.TabStop = false;
            this.grbExameAtendimento.Text = "Exames em Atendimento";
            // 
            // btn_excluirRealizado
            // 
            this.btn_excluirRealizado.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_excluirRealizado.Image = global::SWS.Properties.Resources.close;
            this.btn_excluirRealizado.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btn_excluirRealizado.Location = new System.Drawing.Point(3, 3);
            this.btn_excluirRealizado.Name = "btn_excluirRealizado";
            this.btn_excluirRealizado.Size = new System.Drawing.Size(75, 23);
            this.btn_excluirRealizado.TabIndex = 1;
            this.btn_excluirRealizado.Text = "&Cancelar";
            this.btn_excluirRealizado.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btn_excluirRealizado.UseVisualStyleBackColor = true;
            this.btn_excluirRealizado.Click += new System.EventHandler(this.btn_excluirRealizado_Click);
            // 
            // flpAcaoRealizado
            // 
            this.flpAcaoRealizado.Controls.Add(this.btn_excluirRealizado);
            this.flpAcaoRealizado.Location = new System.Drawing.Point(12, 468);
            this.flpAcaoRealizado.Name = "flpAcaoRealizado";
            this.flpAcaoRealizado.Size = new System.Drawing.Size(723, 33);
            this.flpAcaoRealizado.TabIndex = 9;
            // 
            // dgvExameRealizado
            // 
            this.dgvExameRealizado.AllowUserToAddRows = false;
            this.dgvExameRealizado.AllowUserToDeleteRows = false;
            this.dgvExameRealizado.AllowUserToOrderColumns = true;
            this.dgvExameRealizado.AllowUserToResizeRows = false;
            dataGridViewCellStyle3.BackColor = System.Drawing.Color.Gainsboro;
            this.dgvExameRealizado.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle3;
            this.dgvExameRealizado.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this.dgvExameRealizado.BackgroundColor = System.Drawing.Color.White;
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle4.BackColor = System.Drawing.Color.LightGray;
            dataGridViewCellStyle4.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle4.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle4.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle4.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle4.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvExameRealizado.DefaultCellStyle = dataGridViewCellStyle4;
            this.dgvExameRealizado.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvExameRealizado.Location = new System.Drawing.Point(3, 16);
            this.dgvExameRealizado.MultiSelect = false;
            this.dgvExameRealizado.Name = "dgvExameRealizado";
            this.dgvExameRealizado.RowHeadersVisible = false;
            this.dgvExameRealizado.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvExameRealizado.Size = new System.Drawing.Size(632, 169);
            this.dgvExameRealizado.TabIndex = 0;
            // 
            // grb_realizados
            // 
            this.grb_realizados.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.grb_realizados.Controls.Add(this.dgvExameRealizado);
            this.grb_realizados.Location = new System.Drawing.Point(12, 273);
            this.grb_realizados.Name = "grb_realizados";
            this.grb_realizados.Size = new System.Drawing.Size(638, 188);
            this.grb_realizados.TabIndex = 8;
            this.grb_realizados.TabStop = false;
            this.grb_realizados.Text = "Exames Realizados";
            // 
            // btn_cancelarEncaminhamento
            // 
            this.btn_cancelarEncaminhamento.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.btn_cancelarEncaminhamento.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_cancelarEncaminhamento.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btn_cancelarEncaminhamento.Location = new System.Drawing.Point(246, 3);
            this.btn_cancelarEncaminhamento.Name = "btn_cancelarEncaminhamento";
            this.btn_cancelarEncaminhamento.Size = new System.Drawing.Size(75, 23);
            this.btn_cancelarEncaminhamento.TabIndex = 3;
            this.btn_cancelarEncaminhamento.Text = "&Canc. Enc";
            this.btn_cancelarEncaminhamento.TextAlign = System.Drawing.ContentAlignment.BottomLeft;
            this.btn_cancelarEncaminhamento.UseVisualStyleBackColor = false;
            this.btn_cancelarEncaminhamento.Click += new System.EventHandler(this.btn_cancelarEncaminhamento_Click);
            // 
            // btn_incluir
            // 
            this.btn_incluir.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_incluir.Image = global::SWS.Properties.Resources.icone_mais;
            this.btn_incluir.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btn_incluir.Location = new System.Drawing.Point(165, 3);
            this.btn_incluir.Name = "btn_incluir";
            this.btn_incluir.Size = new System.Drawing.Size(75, 23);
            this.btn_incluir.TabIndex = 1;
            this.btn_incluir.Text = "&Incluir";
            this.btn_incluir.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btn_incluir.UseVisualStyleBackColor = true;
            this.btn_incluir.Click += new System.EventHandler(this.btn_incluir_Click);
            // 
            // btn_cancelar
            // 
            this.btn_cancelar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_cancelar.Image = global::SWS.Properties.Resources.Icone_cancelar;
            this.btn_cancelar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btn_cancelar.Location = new System.Drawing.Point(84, 3);
            this.btn_cancelar.Name = "btn_cancelar";
            this.btn_cancelar.Size = new System.Drawing.Size(75, 23);
            this.btn_cancelar.TabIndex = 2;
            this.btn_cancelar.Text = "&Cancelar";
            this.btn_cancelar.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btn_cancelar.UseVisualStyleBackColor = true;
            this.btn_cancelar.Click += new System.EventHandler(this.btn_cancelar_Click);
            // 
            // btn_finalizar
            // 
            this.btn_finalizar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_finalizar.Image = global::SWS.Properties.Resources.fechar_ico;
            this.btn_finalizar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btn_finalizar.Location = new System.Drawing.Point(3, 3);
            this.btn_finalizar.Name = "btn_finalizar";
            this.btn_finalizar.Size = new System.Drawing.Size(75, 23);
            this.btn_finalizar.TabIndex = 1;
            this.btn_finalizar.Text = "&Finalizar";
            this.btn_finalizar.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btn_finalizar.UseVisualStyleBackColor = true;
            this.btn_finalizar.Click += new System.EventHandler(this.btn_finalizar_Click);
            // 
            // flpPendente
            // 
            this.flpPendente.Controls.Add(this.btn_finalizar);
            this.flpPendente.Controls.Add(this.btn_cancelar);
            this.flpPendente.Controls.Add(this.btn_incluir);
            this.flpPendente.Controls.Add(this.btn_cancelarEncaminhamento);
            this.flpPendente.Location = new System.Drawing.Point(12, 212);
            this.flpPendente.Name = "flpPendente";
            this.flpPendente.Size = new System.Drawing.Size(723, 33);
            this.flpPendente.TabIndex = 7;
            // 
            // dg_examePendente
            // 
            this.dg_examePendente.AllowUserToAddRows = false;
            this.dg_examePendente.AllowUserToDeleteRows = false;
            this.dg_examePendente.AllowUserToOrderColumns = true;
            this.dg_examePendente.AllowUserToResizeColumns = false;
            this.dg_examePendente.AllowUserToResizeRows = false;
            dataGridViewCellStyle5.BackColor = System.Drawing.Color.Gainsboro;
            this.dg_examePendente.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle5;
            this.dg_examePendente.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this.dg_examePendente.BackgroundColor = System.Drawing.Color.White;
            dataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle6.BackColor = System.Drawing.Color.LightGray;
            dataGridViewCellStyle6.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle6.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle6.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle6.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle6.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dg_examePendente.DefaultCellStyle = dataGridViewCellStyle6;
            this.dg_examePendente.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dg_examePendente.Location = new System.Drawing.Point(3, 16);
            this.dg_examePendente.MultiSelect = false;
            this.dg_examePendente.Name = "dg_examePendente";
            this.dg_examePendente.RowHeadersVisible = false;
            this.dg_examePendente.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dg_examePendente.Size = new System.Drawing.Size(632, 163);
            this.dg_examePendente.TabIndex = 0;
            this.dg_examePendente.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dg_examePendente_CellClick);
            // 
            // grb_pendente
            // 
            this.grb_pendente.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.grb_pendente.Controls.Add(this.dg_examePendente);
            this.grb_pendente.Location = new System.Drawing.Point(12, 24);
            this.grb_pendente.Name = "grb_pendente";
            this.grb_pendente.Size = new System.Drawing.Size(638, 182);
            this.grb_pendente.TabIndex = 6;
            this.grb_pendente.TabStop = false;
            this.grb_pendente.Text = "Exames Pendentes";
            // 
            // chkMarcarTodosPendentes
            // 
            this.chkMarcarTodosPendentes.AutoSize = true;
            this.chkMarcarTodosPendentes.Location = new System.Drawing.Point(15, 4);
            this.chkMarcarTodosPendentes.Name = "chkMarcarTodosPendentes";
            this.chkMarcarTodosPendentes.Size = new System.Drawing.Size(248, 17);
            this.chkMarcarTodosPendentes.TabIndex = 11;
            this.chkMarcarTodosPendentes.Text = "Marca/desmarcar todos os exames pendentes.";
            this.chkMarcarTodosPendentes.UseVisualStyleBackColor = true;
            this.chkMarcarTodosPendentes.CheckedChanged += new System.EventHandler(this.chkMarcarTodosPendentes_CheckedChanged);
            // 
            // chkmarcarTodosRealizados
            // 
            this.chkmarcarTodosRealizados.AutoSize = true;
            this.chkmarcarTodosRealizados.Location = new System.Drawing.Point(15, 250);
            this.chkmarcarTodosRealizados.Name = "chkmarcarTodosRealizados";
            this.chkmarcarTodosRealizados.Size = new System.Drawing.Size(251, 17);
            this.chkmarcarTodosRealizados.TabIndex = 12;
            this.chkmarcarTodosRealizados.Text = "Marcar/desmarcar todos os exames realizados?";
            this.chkmarcarTodosRealizados.UseVisualStyleBackColor = true;
            this.chkmarcarTodosRealizados.CheckedChanged += new System.EventHandler(this.chkmarcarTodosRealizados_CheckedChanged);
            // 
            // frmAtendimentoCheck
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(784, 561);
            this.MaximizeBox = true;
            this.Name = "frmAtendimentoCheck";
            this.Text = "ATENDIMENTO CHECK";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.spForm.Panel1.ResumeLayout(false);
            this.spForm.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.spForm)).EndInit();
            this.spForm.ResumeLayout(false);
            this.pnlForm.ResumeLayout(false);
            this.pnlForm.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.banner)).EndInit();
            this.flpAcao.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvExameAtendimento)).EndInit();
            this.grbExameAtendimento.ResumeLayout(false);
            this.flpAcaoRealizado.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvExameRealizado)).EndInit();
            this.grb_realizados.ResumeLayout(false);
            this.flpPendente.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dg_examePendente)).EndInit();
            this.grb_pendente.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.FlowLayoutPanel flpAcao;
        private System.Windows.Forms.Button btn_exibir_transcritos;
        private System.Windows.Forms.Button btn_cancelado;
        private System.Windows.Forms.Button btn_fechar;
        private System.Windows.Forms.CheckBox chkMarcarTodosPendentes;
        private System.Windows.Forms.GroupBox grbExameAtendimento;
        private System.Windows.Forms.DataGridView dgvExameAtendimento;
        private System.Windows.Forms.FlowLayoutPanel flpAcaoRealizado;
        private System.Windows.Forms.Button btn_excluirRealizado;
        private System.Windows.Forms.GroupBox grb_realizados;
        private System.Windows.Forms.DataGridView dgvExameRealizado;
        private System.Windows.Forms.FlowLayoutPanel flpPendente;
        private System.Windows.Forms.Button btn_finalizar;
        private System.Windows.Forms.Button btn_cancelar;
        private System.Windows.Forms.Button btn_incluir;
        private System.Windows.Forms.Button btn_cancelarEncaminhamento;
        private System.Windows.Forms.GroupBox grb_pendente;
        private System.Windows.Forms.DataGridView dg_examePendente;
        private System.Windows.Forms.CheckBox chkmarcarTodosRealizados;
    }
}