﻿namespace SWS.View
{
    partial class frmAgentesIncluir
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.flpAcao = new System.Windows.Forms.FlowLayoutPanel();
            this.btGravar = new System.Windows.Forms.Button();
            this.btLimpar = new System.Windows.Forms.Button();
            this.btFechar = new System.Windows.Forms.Button();
            this.lblDescricao = new System.Windows.Forms.TextBox();
            this.lblDano = new System.Windows.Forms.TextBox();
            this.lblTrajetoria = new System.Windows.Forms.TextBox();
            this.lblLimite = new System.Windows.Forms.TextBox();
            this.textDescricao = new System.Windows.Forms.TextBox();
            this.textTrajetoria = new System.Windows.Forms.TextBox();
            this.textDano = new System.Windows.Forms.TextBox();
            this.cbRisco = new SWS.ComboBoxWithBorder();
            this.lblRisco = new System.Windows.Forms.TextBox();
            this.textLimite = new System.Windows.Forms.TextBox();
            this.errorAgenteIncluir = new System.Windows.Forms.ErrorProvider(this.components);
            this.lblIntensidade = new System.Windows.Forms.TextBox();
            this.lblTecnica = new System.Windows.Forms.TextBox();
            this.textTecnica = new System.Windows.Forms.TextBox();
            this.textIntensidade = new System.Windows.Forms.TextBox();
            this.lblCodigoEsocial = new System.Windows.Forms.TextBox();
            this.textCodigoEsocial = new System.Windows.Forms.TextBox();
            this.btnCodigoEsocial = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.spForm)).BeginInit();
            this.spForm.Panel1.SuspendLayout();
            this.spForm.Panel2.SuspendLayout();
            this.spForm.SuspendLayout();
            this.pnlForm.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.banner)).BeginInit();
            this.flpAcao.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errorAgenteIncluir)).BeginInit();
            this.SuspendLayout();
            // 
            // spForm
            // 
            // 
            // spForm.Panel1
            // 
            this.spForm.Panel1.Controls.Add(this.flpAcao);
            this.spForm.SplitterDistance = 43;
            // 
            // pnlForm
            // 
            this.pnlForm.Controls.Add(this.btnCodigoEsocial);
            this.pnlForm.Controls.Add(this.textCodigoEsocial);
            this.pnlForm.Controls.Add(this.lblCodigoEsocial);
            this.pnlForm.Controls.Add(this.textIntensidade);
            this.pnlForm.Controls.Add(this.textTecnica);
            this.pnlForm.Controls.Add(this.lblTecnica);
            this.pnlForm.Controls.Add(this.lblIntensidade);
            this.pnlForm.Controls.Add(this.textLimite);
            this.pnlForm.Controls.Add(this.lblRisco);
            this.pnlForm.Controls.Add(this.cbRisco);
            this.pnlForm.Controls.Add(this.textDano);
            this.pnlForm.Controls.Add(this.textTrajetoria);
            this.pnlForm.Controls.Add(this.textDescricao);
            this.pnlForm.Controls.Add(this.lblLimite);
            this.pnlForm.Controls.Add(this.lblTrajetoria);
            this.pnlForm.Controls.Add(this.lblDano);
            this.pnlForm.Controls.Add(this.lblDescricao);
            // 
            // flpAcao
            // 
            this.flpAcao.Controls.Add(this.btGravar);
            this.flpAcao.Controls.Add(this.btLimpar);
            this.flpAcao.Controls.Add(this.btFechar);
            this.flpAcao.Dock = System.Windows.Forms.DockStyle.Fill;
            this.flpAcao.Location = new System.Drawing.Point(0, 0);
            this.flpAcao.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.flpAcao.Name = "flpAcao";
            this.flpAcao.Size = new System.Drawing.Size(1045, 43);
            this.flpAcao.TabIndex = 0;
            // 
            // btGravar
            // 
            this.btGravar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btGravar.Image = global::SWS.Properties.Resources.fechar_ico;
            this.btGravar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btGravar.Location = new System.Drawing.Point(4, 4);
            this.btGravar.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.btGravar.Name = "btGravar";
            this.btGravar.Size = new System.Drawing.Size(100, 28);
            this.btGravar.TabIndex = 6;
            this.btGravar.TabStop = false;
            this.btGravar.Text = "&Gravar";
            this.btGravar.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btGravar.UseVisualStyleBackColor = true;
            this.btGravar.Click += new System.EventHandler(this.btGravar_Click);
            // 
            // btLimpar
            // 
            this.btLimpar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btLimpar.Image = global::SWS.Properties.Resources.vassoura;
            this.btLimpar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btLimpar.Location = new System.Drawing.Point(112, 4);
            this.btLimpar.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.btLimpar.Name = "btLimpar";
            this.btLimpar.Size = new System.Drawing.Size(100, 28);
            this.btLimpar.TabIndex = 9;
            this.btLimpar.TabStop = false;
            this.btLimpar.Text = "&Limpar";
            this.btLimpar.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btLimpar.UseVisualStyleBackColor = true;
            this.btLimpar.Click += new System.EventHandler(this.btLimpar_Click);
            // 
            // btFechar
            // 
            this.btFechar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btFechar.Image = global::SWS.Properties.Resources.close;
            this.btFechar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btFechar.Location = new System.Drawing.Point(220, 4);
            this.btFechar.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.btFechar.Name = "btFechar";
            this.btFechar.Size = new System.Drawing.Size(103, 28);
            this.btFechar.TabIndex = 7;
            this.btFechar.TabStop = false;
            this.btFechar.Text = "&Fechar";
            this.btFechar.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btFechar.UseVisualStyleBackColor = true;
            this.btFechar.Click += new System.EventHandler(this.btFechar_Click);
            // 
            // lblDescricao
            // 
            this.lblDescricao.BackColor = System.Drawing.Color.Silver;
            this.lblDescricao.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblDescricao.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDescricao.Location = new System.Drawing.Point(4, 16);
            this.lblDescricao.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.lblDescricao.MaxLength = 100;
            this.lblDescricao.Name = "lblDescricao";
            this.lblDescricao.ReadOnly = true;
            this.lblDescricao.Size = new System.Drawing.Size(195, 24);
            this.lblDescricao.TabIndex = 25;
            this.lblDescricao.TabStop = false;
            this.lblDescricao.Text = "Agente";
            // 
            // lblDano
            // 
            this.lblDano.BackColor = System.Drawing.Color.Silver;
            this.lblDano.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblDano.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDano.Location = new System.Drawing.Point(4, 65);
            this.lblDano.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.lblDano.MaxLength = 100;
            this.lblDano.Name = "lblDano";
            this.lblDano.ReadOnly = true;
            this.lblDano.Size = new System.Drawing.Size(195, 24);
            this.lblDano.TabIndex = 27;
            this.lblDano.TabStop = false;
            this.lblDano.Text = "Dano à saúde";
            // 
            // lblTrajetoria
            // 
            this.lblTrajetoria.BackColor = System.Drawing.Color.Silver;
            this.lblTrajetoria.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblTrajetoria.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTrajetoria.Location = new System.Drawing.Point(4, 41);
            this.lblTrajetoria.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.lblTrajetoria.MaxLength = 100;
            this.lblTrajetoria.Name = "lblTrajetoria";
            this.lblTrajetoria.ReadOnly = true;
            this.lblTrajetoria.Size = new System.Drawing.Size(195, 24);
            this.lblTrajetoria.TabIndex = 26;
            this.lblTrajetoria.TabStop = false;
            this.lblTrajetoria.Text = "Trajetória";
            // 
            // lblLimite
            // 
            this.lblLimite.BackColor = System.Drawing.Color.Silver;
            this.lblLimite.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblLimite.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblLimite.Location = new System.Drawing.Point(4, 90);
            this.lblLimite.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.lblLimite.MaxLength = 100;
            this.lblLimite.Name = "lblLimite";
            this.lblLimite.ReadOnly = true;
            this.lblLimite.Size = new System.Drawing.Size(195, 24);
            this.lblLimite.TabIndex = 28;
            this.lblLimite.TabStop = false;
            this.lblLimite.Text = "Limite de exposição";
            // 
            // textDescricao
            // 
            this.textDescricao.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textDescricao.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.textDescricao.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textDescricao.Location = new System.Drawing.Point(199, 16);
            this.textDescricao.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.textDescricao.MaxLength = 255;
            this.textDescricao.Name = "textDescricao";
            this.textDescricao.Size = new System.Drawing.Size(767, 24);
            this.textDescricao.TabIndex = 1;
            // 
            // textTrajetoria
            // 
            this.textTrajetoria.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textTrajetoria.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.textTrajetoria.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textTrajetoria.Location = new System.Drawing.Point(199, 41);
            this.textTrajetoria.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.textTrajetoria.MaxLength = 255;
            this.textTrajetoria.Name = "textTrajetoria";
            this.textTrajetoria.Size = new System.Drawing.Size(767, 24);
            this.textTrajetoria.TabIndex = 2;
            // 
            // textDano
            // 
            this.textDano.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textDano.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.textDano.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textDano.Location = new System.Drawing.Point(199, 65);
            this.textDano.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.textDano.MaxLength = 255;
            this.textDano.Name = "textDano";
            this.textDano.Size = new System.Drawing.Size(767, 24);
            this.textDano.TabIndex = 3;
            // 
            // cbRisco
            // 
            this.cbRisco.BackColor = System.Drawing.Color.LightGray;
            this.cbRisco.BorderColor = System.Drawing.Color.DimGray;
            this.cbRisco.BorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid;
            this.cbRisco.FormattingEnabled = true;
            this.cbRisco.Location = new System.Drawing.Point(199, 114);
            this.cbRisco.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.cbRisco.Name = "cbRisco";
            this.cbRisco.Size = new System.Drawing.Size(767, 24);
            this.cbRisco.TabIndex = 5;
            // 
            // lblRisco
            // 
            this.lblRisco.BackColor = System.Drawing.Color.Silver;
            this.lblRisco.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblRisco.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblRisco.Location = new System.Drawing.Point(4, 114);
            this.lblRisco.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.lblRisco.MaxLength = 100;
            this.lblRisco.Name = "lblRisco";
            this.lblRisco.ReadOnly = true;
            this.lblRisco.Size = new System.Drawing.Size(195, 24);
            this.lblRisco.TabIndex = 29;
            this.lblRisco.TabStop = false;
            this.lblRisco.Text = "Risco";
            // 
            // textLimite
            // 
            this.textLimite.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textLimite.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.textLimite.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textLimite.Location = new System.Drawing.Point(199, 90);
            this.textLimite.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.textLimite.MaxLength = 100;
            this.textLimite.Name = "textLimite";
            this.textLimite.Size = new System.Drawing.Size(767, 24);
            this.textLimite.TabIndex = 4;
            // 
            // errorAgenteIncluir
            // 
            this.errorAgenteIncluir.ContainerControl = this;
            // 
            // lblIntensidade
            // 
            this.lblIntensidade.BackColor = System.Drawing.Color.Silver;
            this.lblIntensidade.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblIntensidade.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblIntensidade.Location = new System.Drawing.Point(4, 139);
            this.lblIntensidade.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.lblIntensidade.MaxLength = 100;
            this.lblIntensidade.Name = "lblIntensidade";
            this.lblIntensidade.ReadOnly = true;
            this.lblIntensidade.Size = new System.Drawing.Size(195, 24);
            this.lblIntensidade.TabIndex = 30;
            this.lblIntensidade.TabStop = false;
            this.lblIntensidade.Text = "Intensidade";
            // 
            // lblTecnica
            // 
            this.lblTecnica.BackColor = System.Drawing.Color.Silver;
            this.lblTecnica.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblTecnica.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTecnica.Location = new System.Drawing.Point(4, 164);
            this.lblTecnica.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.lblTecnica.MaxLength = 100;
            this.lblTecnica.Name = "lblTecnica";
            this.lblTecnica.ReadOnly = true;
            this.lblTecnica.Size = new System.Drawing.Size(195, 24);
            this.lblTecnica.TabIndex = 31;
            this.lblTecnica.TabStop = false;
            this.lblTecnica.Text = "Técnica";
            // 
            // textTecnica
            // 
            this.textTecnica.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textTecnica.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.textTecnica.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textTecnica.Location = new System.Drawing.Point(199, 164);
            this.textTecnica.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.textTecnica.MaxLength = 100;
            this.textTecnica.Name = "textTecnica";
            this.textTecnica.Size = new System.Drawing.Size(767, 24);
            this.textTecnica.TabIndex = 7;
            // 
            // textIntensidade
            // 
            this.textIntensidade.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textIntensidade.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.textIntensidade.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textIntensidade.Location = new System.Drawing.Point(199, 139);
            this.textIntensidade.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.textIntensidade.MaxLength = 100;
            this.textIntensidade.Name = "textIntensidade";
            this.textIntensidade.Size = new System.Drawing.Size(767, 24);
            this.textIntensidade.TabIndex = 6;
            // 
            // lblCodigoEsocial
            // 
            this.lblCodigoEsocial.BackColor = System.Drawing.Color.Silver;
            this.lblCodigoEsocial.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblCodigoEsocial.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCodigoEsocial.Location = new System.Drawing.Point(4, 188);
            this.lblCodigoEsocial.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.lblCodigoEsocial.MaxLength = 100;
            this.lblCodigoEsocial.Name = "lblCodigoEsocial";
            this.lblCodigoEsocial.ReadOnly = true;
            this.lblCodigoEsocial.Size = new System.Drawing.Size(195, 24);
            this.lblCodigoEsocial.TabIndex = 32;
            this.lblCodigoEsocial.TabStop = false;
            this.lblCodigoEsocial.Text = "Código E-Social";
            // 
            // textCodigoEsocial
            // 
            this.textCodigoEsocial.BackColor = System.Drawing.Color.LightGray;
            this.textCodigoEsocial.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textCodigoEsocial.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.textCodigoEsocial.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textCodigoEsocial.Location = new System.Drawing.Point(199, 188);
            this.textCodigoEsocial.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.textCodigoEsocial.MaxLength = 100;
            this.textCodigoEsocial.Name = "textCodigoEsocial";
            this.textCodigoEsocial.Size = new System.Drawing.Size(730, 24);
            this.textCodigoEsocial.TabIndex = 33;
            // 
            // btnCodigoEsocial
            // 
            this.btnCodigoEsocial.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnCodigoEsocial.Image = global::SWS.Properties.Resources.busca;
            this.btnCodigoEsocial.Location = new System.Drawing.Point(927, 188);
            this.btnCodigoEsocial.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.btnCodigoEsocial.Name = "btnCodigoEsocial";
            this.btnCodigoEsocial.Size = new System.Drawing.Size(40, 26);
            this.btnCodigoEsocial.TabIndex = 34;
            this.btnCodigoEsocial.UseVisualStyleBackColor = true;
            this.btnCodigoEsocial.Click += new System.EventHandler(this.btnCodigoEsocial_Click);
            // 
            // frmAgentesIncluir
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.ClientSize = new System.Drawing.Size(1045, 690);
            this.Margin = new System.Windows.Forms.Padding(5, 5, 5, 5);
            this.Name = "frmAgentesIncluir";
            this.Text = "INCLUIR AGENTE";
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.frmAgentesIncluir_KeyDown);
            this.spForm.Panel1.ResumeLayout(false);
            this.spForm.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.spForm)).EndInit();
            this.spForm.ResumeLayout(false);
            this.pnlForm.ResumeLayout(false);
            this.pnlForm.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.banner)).EndInit();
            this.flpAcao.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.errorAgenteIncluir)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        protected System.Windows.Forms.FlowLayoutPanel flpAcao;
        protected System.Windows.Forms.Button btGravar;
        protected System.Windows.Forms.Button btLimpar;
        protected System.Windows.Forms.Button btFechar;
        protected System.Windows.Forms.TextBox textLimite;
        protected System.Windows.Forms.TextBox lblRisco;
        protected ComboBoxWithBorder cbRisco;
        protected System.Windows.Forms.TextBox textDano;
        protected System.Windows.Forms.TextBox textTrajetoria;
        protected System.Windows.Forms.TextBox textDescricao;
        protected System.Windows.Forms.TextBox lblLimite;
        protected System.Windows.Forms.TextBox lblTrajetoria;
        protected System.Windows.Forms.TextBox lblDano;
        protected System.Windows.Forms.TextBox lblDescricao;
        protected System.Windows.Forms.ErrorProvider errorAgenteIncluir;
        protected System.Windows.Forms.TextBox textIntensidade;
        protected System.Windows.Forms.TextBox textTecnica;
        protected System.Windows.Forms.TextBox lblTecnica;
        protected System.Windows.Forms.TextBox lblIntensidade;
        protected System.Windows.Forms.TextBox textCodigoEsocial;
        protected System.Windows.Forms.TextBox lblCodigoEsocial;
        protected System.Windows.Forms.Button btnCodigoEsocial;

    }
}
