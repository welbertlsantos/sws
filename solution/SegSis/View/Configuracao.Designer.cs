﻿namespace SWS.View
{
    partial class frmConfiguracao
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmConfiguracao));
            this.flowLayoutPanel1 = new System.Windows.Forms.FlowLayoutPanel();
            this.btnFechar = new System.Windows.Forms.Button();
            this.lblDiretorio = new System.Windows.Forms.TextBox();
            this.textDiretorio = new System.Windows.Forms.TextBox();
            this.lblUltimaNFe = new System.Windows.Forms.TextBox();
            this.lblJurosMensalProrrogacaoCRCPercentual = new System.Windows.Forms.TextBox();
            this.lblQtdeViasImpressoraTermica = new System.Windows.Forms.TextBox();
            this.lblNomeEmpresaTermica = new System.Windows.Forms.TextBox();
            this.lblServidorTermica = new System.Windows.Forms.TextBox();
            this.lblNomeTermica = new System.Windows.Forms.TextBox();
            this.lblSerialCliente = new System.Windows.Forms.TextBox();
            this.lblFlagMovimentoFinalizacaoExame = new System.Windows.Forms.TextBox();
            this.lblProdutoAutomatico = new System.Windows.Forms.TextBox();
            this.lblAso2Via = new System.Windows.Forms.TextBox();
            this.lblAsoTranscrito = new System.Windows.Forms.TextBox();
            this.lblAsoInclusaoExames = new System.Windows.Forms.TextBox();
            this.lblCidadeEmpresa = new System.Windows.Forms.TextBox();
            this.lblNomeEmpresa = new System.Windows.Forms.TextBox();
            this.lblEnderecoEmpresa = new System.Windows.Forms.TextBox();
            this.lblNumeroEmpresa = new System.Windows.Forms.TextBox();
            this.lblComplementoEmpresa = new System.Windows.Forms.TextBox();
            this.lblBairroEmpresa = new System.Windows.Forms.TextBox();
            this.lblUFEmpresa = new System.Windows.Forms.TextBox();
            this.lblCEPEmpresa = new System.Windows.Forms.TextBox();
            this.lblCNPJEmpresa = new System.Windows.Forms.TextBox();
            this.lblInscricaoEmpresa = new System.Windows.Forms.TextBox();
            this.lblEmailEmpresa = new System.Windows.Forms.TextBox();
            this.lblServidorSMTP = new System.Windows.Forms.TextBox();
            this.lblNumeroPorta = new System.Windows.Forms.TextBox();
            this.lblSSL = new System.Windows.Forms.TextBox();
            this.lblEmailEnvio = new System.Windows.Forms.TextBox();
            this.lblSenhaEmail = new System.Windows.Forms.TextBox();
            this.textNumeroInicialNF = new System.Windows.Forms.TextBox();
            this.textJurosProrrogacao = new System.Windows.Forms.TextBox();
            this.textQtdeTermica = new System.Windows.Forms.TextBox();
            this.textNomeEmpresaTermica = new System.Windows.Forms.TextBox();
            this.textNomeImpressoraTermica = new System.Windows.Forms.TextBox();
            this.textSerialCliente = new System.Windows.Forms.TextBox();
            this.cbGeraMovimentoExame = new SWS.ComboBoxWithBorder();
            this.cbGravaProdutoAutomatico = new SWS.ComboBoxWithBorder();
            this.textAso2Via = new System.Windows.Forms.TextBox();
            this.btnAso2Via = new System.Windows.Forms.Button();
            this.btnAsoTranscrito = new System.Windows.Forms.Button();
            this.textAsoTranscrito = new System.Windows.Forms.TextBox();
            this.btnAsoInclusaoExame = new System.Windows.Forms.Button();
            this.textAsoInclusaoExame = new System.Windows.Forms.TextBox();
            this.textCidade = new System.Windows.Forms.TextBox();
            this.textNomeEmpresa = new System.Windows.Forms.TextBox();
            this.textEndereco = new System.Windows.Forms.TextBox();
            this.textNumero = new System.Windows.Forms.TextBox();
            this.textComplemento = new System.Windows.Forms.TextBox();
            this.textBairro = new System.Windows.Forms.TextBox();
            this.textUF = new System.Windows.Forms.TextBox();
            this.textCEP = new System.Windows.Forms.MaskedTextBox();
            this.textCNPJ = new System.Windows.Forms.MaskedTextBox();
            this.textInscricao = new System.Windows.Forms.TextBox();
            this.textEmail = new System.Windows.Forms.TextBox();
            this.textServidorSMTP = new System.Windows.Forms.TextBox();
            this.textNumeroPorta = new System.Windows.Forms.TextBox();
            this.cbSSL = new SWS.ComboBoxWithBorder();
            this.textEmailEnvio = new System.Windows.Forms.TextBox();
            this.textSenhaEmail = new System.Windows.Forms.MaskedTextBox();
            this.textIPServidorTermica = new System.Windows.Forms.TextBox();
            this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
            this.textDiasPesquisa = new System.Windows.Forms.TextBox();
            this.textVersaoSistema = new System.Windows.Forms.TextBox();
            this.lblObrigatorioGravarAtendimentoComMedicoExaminador = new System.Windows.Forms.TextBox();
            this.lbdNumeroLicencas = new System.Windows.Forms.TextBox();
            this.textNumeroLicencas = new System.Windows.Forms.TextBox();
            this.lblCampoProcessoRealizadoInteiro = new System.Windows.Forms.TextBox();
            this.lblCampoGracaoIntegracao = new System.Windows.Forms.TextBox();
            this.textLocalntegracao = new System.Windows.Forms.TextBox();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.cbUtilizaFilial = new SWS.ComboBoxWithBorder();
            this.lblTempoPesquisaHistorico = new System.Windows.Forms.TextBox();
            this.lblImprimeSomenteSala = new System.Windows.Forms.TextBox();
            this.cbImprimeSala = new SWS.ComboBoxWithBorder();
            this.cbPcmsoVencido = new SWS.ComboBoxWithBorder();
            this.lblImprimirDadosAtendimentoPcmsoVencido = new System.Windows.Forms.TextBox();
            this.lblOrdenarAtendimentoPorSenha = new System.Windows.Forms.TextBox();
            this.cbOrdenacaoAtendimento = new SWS.ComboBoxWithBorder();
            this.lblVersaoSistema = new System.Windows.Forms.TextBox();
            this.lblCancelaAposFinalizado = new System.Windows.Forms.TextBox();
            this.cbCancelaExameAtendimentoFinalizado = new SWS.ComboBoxWithBorder();
            this.lblGravaAtendimentoBloqueado = new System.Windows.Forms.TextBox();
            this.cbAtendimentoBloqueado = new SWS.ComboBoxWithBorder();
            this.cbMedicoExaminador = new SWS.ComboBoxWithBorder();
            this.cbConverterEsocial2220 = new SWS.ComboBoxWithBorder();
            this.lblCodigoExameAnaliseRisco = new System.Windows.Forms.TextBox();
            this.btnAnaliseRisco = new System.Windows.Forms.Button();
            this.textAnaliseRisco = new System.Windows.Forms.TextBox();
            ((System.ComponentModel.ISupportInitialize)(this.spForm)).BeginInit();
            this.spForm.Panel1.SuspendLayout();
            this.spForm.Panel2.SuspendLayout();
            this.spForm.SuspendLayout();
            this.pnlForm.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.banner)).BeginInit();
            this.flowLayoutPanel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // spForm
            // 
            // 
            // spForm.Panel1
            // 
            this.spForm.Panel1.Controls.Add(this.flowLayoutPanel1);
            this.spForm.SplitterDistance = 43;
            // 
            // pnlForm
            // 
            this.pnlForm.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnlForm.Controls.Add(this.btnAnaliseRisco);
            this.pnlForm.Controls.Add(this.textAnaliseRisco);
            this.pnlForm.Controls.Add(this.lblCodigoExameAnaliseRisco);
            this.pnlForm.Controls.Add(this.textLocalntegracao);
            this.pnlForm.Controls.Add(this.lblCampoGracaoIntegracao);
            this.pnlForm.Controls.Add(this.cbConverterEsocial2220);
            this.pnlForm.Controls.Add(this.lblCampoProcessoRealizadoInteiro);
            this.pnlForm.Controls.Add(this.textNumeroLicencas);
            this.pnlForm.Controls.Add(this.lbdNumeroLicencas);
            this.pnlForm.Controls.Add(this.cbMedicoExaminador);
            this.pnlForm.Controls.Add(this.lblObrigatorioGravarAtendimentoComMedicoExaminador);
            this.pnlForm.Controls.Add(this.cbAtendimentoBloqueado);
            this.pnlForm.Controls.Add(this.lblGravaAtendimentoBloqueado);
            this.pnlForm.Controls.Add(this.cbCancelaExameAtendimentoFinalizado);
            this.pnlForm.Controls.Add(this.lblCancelaAposFinalizado);
            this.pnlForm.Controls.Add(this.textVersaoSistema);
            this.pnlForm.Controls.Add(this.lblVersaoSistema);
            this.pnlForm.Controls.Add(this.cbOrdenacaoAtendimento);
            this.pnlForm.Controls.Add(this.lblOrdenarAtendimentoPorSenha);
            this.pnlForm.Controls.Add(this.cbPcmsoVencido);
            this.pnlForm.Controls.Add(this.lblImprimirDadosAtendimentoPcmsoVencido);
            this.pnlForm.Controls.Add(this.cbImprimeSala);
            this.pnlForm.Controls.Add(this.lblImprimeSomenteSala);
            this.pnlForm.Controls.Add(this.textDiasPesquisa);
            this.pnlForm.Controls.Add(this.lblTempoPesquisaHistorico);
            this.pnlForm.Controls.Add(this.cbUtilizaFilial);
            this.pnlForm.Controls.Add(this.textBox1);
            this.pnlForm.Controls.Add(this.textIPServidorTermica);
            this.pnlForm.Controls.Add(this.textSenhaEmail);
            this.pnlForm.Controls.Add(this.textEmailEnvio);
            this.pnlForm.Controls.Add(this.cbSSL);
            this.pnlForm.Controls.Add(this.textNumeroPorta);
            this.pnlForm.Controls.Add(this.textServidorSMTP);
            this.pnlForm.Controls.Add(this.textEmail);
            this.pnlForm.Controls.Add(this.textInscricao);
            this.pnlForm.Controls.Add(this.textCNPJ);
            this.pnlForm.Controls.Add(this.textCEP);
            this.pnlForm.Controls.Add(this.textUF);
            this.pnlForm.Controls.Add(this.textBairro);
            this.pnlForm.Controls.Add(this.textComplemento);
            this.pnlForm.Controls.Add(this.textNumero);
            this.pnlForm.Controls.Add(this.textEndereco);
            this.pnlForm.Controls.Add(this.textNomeEmpresa);
            this.pnlForm.Controls.Add(this.textCidade);
            this.pnlForm.Controls.Add(this.btnAsoInclusaoExame);
            this.pnlForm.Controls.Add(this.textAsoInclusaoExame);
            this.pnlForm.Controls.Add(this.btnAsoTranscrito);
            this.pnlForm.Controls.Add(this.textAsoTranscrito);
            this.pnlForm.Controls.Add(this.btnAso2Via);
            this.pnlForm.Controls.Add(this.textAso2Via);
            this.pnlForm.Controls.Add(this.cbGravaProdutoAutomatico);
            this.pnlForm.Controls.Add(this.cbGeraMovimentoExame);
            this.pnlForm.Controls.Add(this.textSerialCliente);
            this.pnlForm.Controls.Add(this.textNomeImpressoraTermica);
            this.pnlForm.Controls.Add(this.textNomeEmpresaTermica);
            this.pnlForm.Controls.Add(this.textQtdeTermica);
            this.pnlForm.Controls.Add(this.textJurosProrrogacao);
            this.pnlForm.Controls.Add(this.textNumeroInicialNF);
            this.pnlForm.Controls.Add(this.lblSenhaEmail);
            this.pnlForm.Controls.Add(this.lblEmailEnvio);
            this.pnlForm.Controls.Add(this.lblSSL);
            this.pnlForm.Controls.Add(this.lblNumeroPorta);
            this.pnlForm.Controls.Add(this.lblServidorSMTP);
            this.pnlForm.Controls.Add(this.lblEmailEmpresa);
            this.pnlForm.Controls.Add(this.lblInscricaoEmpresa);
            this.pnlForm.Controls.Add(this.lblCNPJEmpresa);
            this.pnlForm.Controls.Add(this.lblCEPEmpresa);
            this.pnlForm.Controls.Add(this.lblUFEmpresa);
            this.pnlForm.Controls.Add(this.lblBairroEmpresa);
            this.pnlForm.Controls.Add(this.lblComplementoEmpresa);
            this.pnlForm.Controls.Add(this.lblNumeroEmpresa);
            this.pnlForm.Controls.Add(this.lblEnderecoEmpresa);
            this.pnlForm.Controls.Add(this.lblNomeEmpresa);
            this.pnlForm.Controls.Add(this.lblCidadeEmpresa);
            this.pnlForm.Controls.Add(this.lblAsoInclusaoExames);
            this.pnlForm.Controls.Add(this.lblAsoTranscrito);
            this.pnlForm.Controls.Add(this.lblAso2Via);
            this.pnlForm.Controls.Add(this.lblProdutoAutomatico);
            this.pnlForm.Controls.Add(this.lblFlagMovimentoFinalizacaoExame);
            this.pnlForm.Controls.Add(this.lblSerialCliente);
            this.pnlForm.Controls.Add(this.lblNomeTermica);
            this.pnlForm.Controls.Add(this.lblServidorTermica);
            this.pnlForm.Controls.Add(this.lblNomeEmpresaTermica);
            this.pnlForm.Controls.Add(this.lblQtdeViasImpressoraTermica);
            this.pnlForm.Controls.Add(this.textDiretorio);
            this.pnlForm.Controls.Add(this.lblDiretorio);
            this.pnlForm.Controls.Add(this.lblJurosMensalProrrogacaoCRCPercentual);
            this.pnlForm.Controls.Add(this.lblUltimaNFe);
            this.pnlForm.Location = new System.Drawing.Point(0, 2);
            this.pnlForm.Margin = new System.Windows.Forms.Padding(5, 5, 5, 5);
            this.pnlForm.Size = new System.Drawing.Size(1018, 1100);
            this.toolTip1.SetToolTip(this.pnlForm, "Informa se o servidor SMTP utiliza certificado SSL para envio de emails.");
            // 
            // flowLayoutPanel1
            // 
            this.flowLayoutPanel1.Controls.Add(this.btnFechar);
            this.flowLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.flowLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.flowLayoutPanel1.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.flowLayoutPanel1.Name = "flowLayoutPanel1";
            this.flowLayoutPanel1.Size = new System.Drawing.Size(1045, 43);
            this.flowLayoutPanel1.TabIndex = 0;
            // 
            // btnFechar
            // 
            this.btnFechar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnFechar.Image = global::SWS.Properties.Resources.close;
            this.btnFechar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnFechar.Location = new System.Drawing.Point(4, 4);
            this.btnFechar.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.btnFechar.Name = "btnFechar";
            this.btnFechar.Size = new System.Drawing.Size(100, 28);
            this.btnFechar.TabIndex = 0;
            this.btnFechar.TabStop = false;
            this.btnFechar.Text = "&Fechar";
            this.btnFechar.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnFechar.UseVisualStyleBackColor = true;
            this.btnFechar.Click += new System.EventHandler(this.btnFechar_Click);
            // 
            // lblDiretorio
            // 
            this.lblDiretorio.BackColor = System.Drawing.Color.LightGray;
            this.lblDiretorio.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblDiretorio.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDiretorio.Location = new System.Drawing.Point(16, 18);
            this.lblDiretorio.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.lblDiretorio.Name = "lblDiretorio";
            this.lblDiretorio.Size = new System.Drawing.Size(375, 24);
            this.lblDiretorio.TabIndex = 1;
            this.lblDiretorio.TabStop = false;
            this.lblDiretorio.Text = "01 - Diretório da aplicação";
            // 
            // textDiretorio
            // 
            this.textDiretorio.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textDiretorio.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textDiretorio.Location = new System.Drawing.Point(391, 18);
            this.textDiretorio.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.textDiretorio.MaxLength = 200;
            this.textDiretorio.Name = "textDiretorio";
            this.textDiretorio.Size = new System.Drawing.Size(605, 24);
            this.textDiretorio.TabIndex = 1;
            this.toolTip1.SetToolTip(this.textDiretorio, resources.GetString("textDiretorio.ToolTip"));
            this.textDiretorio.Enter += new System.EventHandler(this.textDiretorio_Enter);
            this.textDiretorio.Leave += new System.EventHandler(this.textDiretorio_Leave);
            // 
            // lblUltimaNFe
            // 
            this.lblUltimaNFe.BackColor = System.Drawing.Color.LightGray;
            this.lblUltimaNFe.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblUltimaNFe.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblUltimaNFe.Location = new System.Drawing.Point(16, 43);
            this.lblUltimaNFe.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.lblUltimaNFe.Name = "lblUltimaNFe";
            this.lblUltimaNFe.Size = new System.Drawing.Size(375, 24);
            this.lblUltimaNFe.TabIndex = 13;
            this.lblUltimaNFe.TabStop = false;
            this.lblUltimaNFe.Text = "09 - Próxima NFE";
            // 
            // lblJurosMensalProrrogacaoCRCPercentual
            // 
            this.lblJurosMensalProrrogacaoCRCPercentual.BackColor = System.Drawing.Color.LightGray;
            this.lblJurosMensalProrrogacaoCRCPercentual.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblJurosMensalProrrogacaoCRCPercentual.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblJurosMensalProrrogacaoCRCPercentual.Location = new System.Drawing.Point(16, 68);
            this.lblJurosMensalProrrogacaoCRCPercentual.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.lblJurosMensalProrrogacaoCRCPercentual.Name = "lblJurosMensalProrrogacaoCRCPercentual";
            this.lblJurosMensalProrrogacaoCRCPercentual.Size = new System.Drawing.Size(375, 24);
            this.lblJurosMensalProrrogacaoCRCPercentual.TabIndex = 14;
            this.lblJurosMensalProrrogacaoCRCPercentual.TabStop = false;
            this.lblJurosMensalProrrogacaoCRCPercentual.Text = "10 - Juros mensal na prorrogação de cobrança %";
            // 
            // lblQtdeViasImpressoraTermica
            // 
            this.lblQtdeViasImpressoraTermica.BackColor = System.Drawing.Color.LightGray;
            this.lblQtdeViasImpressoraTermica.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblQtdeViasImpressoraTermica.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblQtdeViasImpressoraTermica.Location = new System.Drawing.Point(16, 92);
            this.lblQtdeViasImpressoraTermica.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.lblQtdeViasImpressoraTermica.Name = "lblQtdeViasImpressoraTermica";
            this.lblQtdeViasImpressoraTermica.Size = new System.Drawing.Size(375, 24);
            this.lblQtdeViasImpressoraTermica.TabIndex = 15;
            this.lblQtdeViasImpressoraTermica.TabStop = false;
            this.lblQtdeViasImpressoraTermica.Text = "11 - Quantidade padrão de vias térmica";
            // 
            // lblNomeEmpresaTermica
            // 
            this.lblNomeEmpresaTermica.BackColor = System.Drawing.Color.LightGray;
            this.lblNomeEmpresaTermica.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblNomeEmpresaTermica.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNomeEmpresaTermica.Location = new System.Drawing.Point(16, 117);
            this.lblNomeEmpresaTermica.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.lblNomeEmpresaTermica.Name = "lblNomeEmpresaTermica";
            this.lblNomeEmpresaTermica.Size = new System.Drawing.Size(375, 24);
            this.lblNomeEmpresaTermica.TabIndex = 16;
            this.lblNomeEmpresaTermica.TabStop = false;
            this.lblNomeEmpresaTermica.Text = "12 - Nome da empresa térmica";
            // 
            // lblServidorTermica
            // 
            this.lblServidorTermica.BackColor = System.Drawing.Color.LightGray;
            this.lblServidorTermica.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblServidorTermica.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblServidorTermica.Location = new System.Drawing.Point(16, 142);
            this.lblServidorTermica.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.lblServidorTermica.Name = "lblServidorTermica";
            this.lblServidorTermica.Size = new System.Drawing.Size(375, 24);
            this.lblServidorTermica.TabIndex = 17;
            this.lblServidorTermica.TabStop = false;
            this.lblServidorTermica.Text = "13 - IP do servidor térmica";
            // 
            // lblNomeTermica
            // 
            this.lblNomeTermica.BackColor = System.Drawing.Color.LightGray;
            this.lblNomeTermica.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblNomeTermica.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNomeTermica.Location = new System.Drawing.Point(16, 166);
            this.lblNomeTermica.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.lblNomeTermica.Name = "lblNomeTermica";
            this.lblNomeTermica.Size = new System.Drawing.Size(375, 24);
            this.lblNomeTermica.TabIndex = 18;
            this.lblNomeTermica.TabStop = false;
            this.lblNomeTermica.Text = "14 - Nome da térmica";
            // 
            // lblSerialCliente
            // 
            this.lblSerialCliente.BackColor = System.Drawing.Color.LightGray;
            this.lblSerialCliente.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblSerialCliente.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSerialCliente.Location = new System.Drawing.Point(16, 191);
            this.lblSerialCliente.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.lblSerialCliente.Name = "lblSerialCliente";
            this.lblSerialCliente.Size = new System.Drawing.Size(375, 24);
            this.lblSerialCliente.TabIndex = 19;
            this.lblSerialCliente.TabStop = false;
            this.lblSerialCliente.Text = "15 - Serial do cliente";
            // 
            // lblFlagMovimentoFinalizacaoExame
            // 
            this.lblFlagMovimentoFinalizacaoExame.BackColor = System.Drawing.Color.LightGray;
            this.lblFlagMovimentoFinalizacaoExame.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblFlagMovimentoFinalizacaoExame.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblFlagMovimentoFinalizacaoExame.Location = new System.Drawing.Point(16, 215);
            this.lblFlagMovimentoFinalizacaoExame.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.lblFlagMovimentoFinalizacaoExame.Name = "lblFlagMovimentoFinalizacaoExame";
            this.lblFlagMovimentoFinalizacaoExame.Size = new System.Drawing.Size(375, 24);
            this.lblFlagMovimentoFinalizacaoExame.TabIndex = 20;
            this.lblFlagMovimentoFinalizacaoExame.TabStop = false;
            this.lblFlagMovimentoFinalizacaoExame.Text = "16 - Gera movimento finalização exame";
            // 
            // lblProdutoAutomatico
            // 
            this.lblProdutoAutomatico.BackColor = System.Drawing.Color.LightGray;
            this.lblProdutoAutomatico.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblProdutoAutomatico.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblProdutoAutomatico.Location = new System.Drawing.Point(16, 240);
            this.lblProdutoAutomatico.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.lblProdutoAutomatico.Name = "lblProdutoAutomatico";
            this.lblProdutoAutomatico.Size = new System.Drawing.Size(375, 24);
            this.lblProdutoAutomatico.TabIndex = 23;
            this.lblProdutoAutomatico.TabStop = false;
            this.lblProdutoAutomatico.Text = "19 - Grava produto automático";
            // 
            // lblAso2Via
            // 
            this.lblAso2Via.BackColor = System.Drawing.Color.LightGray;
            this.lblAso2Via.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblAso2Via.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblAso2Via.Location = new System.Drawing.Point(16, 265);
            this.lblAso2Via.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.lblAso2Via.Name = "lblAso2Via";
            this.lblAso2Via.Size = new System.Drawing.Size(375, 24);
            this.lblAso2Via.TabIndex = 24;
            this.lblAso2Via.TabStop = false;
            this.lblAso2Via.Text = "20 - Aso segunda via";
            // 
            // lblAsoTranscrito
            // 
            this.lblAsoTranscrito.BackColor = System.Drawing.Color.LightGray;
            this.lblAsoTranscrito.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblAsoTranscrito.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblAsoTranscrito.Location = new System.Drawing.Point(16, 289);
            this.lblAsoTranscrito.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.lblAsoTranscrito.Name = "lblAsoTranscrito";
            this.lblAsoTranscrito.Size = new System.Drawing.Size(375, 24);
            this.lblAsoTranscrito.TabIndex = 25;
            this.lblAsoTranscrito.TabStop = false;
            this.lblAsoTranscrito.Text = "21 - Aso transcrito";
            // 
            // lblAsoInclusaoExames
            // 
            this.lblAsoInclusaoExames.BackColor = System.Drawing.Color.LightGray;
            this.lblAsoInclusaoExames.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblAsoInclusaoExames.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblAsoInclusaoExames.Location = new System.Drawing.Point(16, 314);
            this.lblAsoInclusaoExames.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.lblAsoInclusaoExames.Name = "lblAsoInclusaoExames";
            this.lblAsoInclusaoExames.Size = new System.Drawing.Size(375, 24);
            this.lblAsoInclusaoExames.TabIndex = 26;
            this.lblAsoInclusaoExames.TabStop = false;
            this.lblAsoInclusaoExames.Text = "22 - Aso com inclusão exames";
            // 
            // lblCidadeEmpresa
            // 
            this.lblCidadeEmpresa.BackColor = System.Drawing.Color.LightGray;
            this.lblCidadeEmpresa.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblCidadeEmpresa.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCidadeEmpresa.Location = new System.Drawing.Point(16, 338);
            this.lblCidadeEmpresa.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.lblCidadeEmpresa.Name = "lblCidadeEmpresa";
            this.lblCidadeEmpresa.Size = new System.Drawing.Size(375, 24);
            this.lblCidadeEmpresa.TabIndex = 27;
            this.lblCidadeEmpresa.TabStop = false;
            this.lblCidadeEmpresa.Text = "23 - Cidade";
            // 
            // lblNomeEmpresa
            // 
            this.lblNomeEmpresa.BackColor = System.Drawing.Color.LightGray;
            this.lblNomeEmpresa.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblNomeEmpresa.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNomeEmpresa.Location = new System.Drawing.Point(16, 363);
            this.lblNomeEmpresa.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.lblNomeEmpresa.Name = "lblNomeEmpresa";
            this.lblNomeEmpresa.Size = new System.Drawing.Size(375, 24);
            this.lblNomeEmpresa.TabIndex = 28;
            this.lblNomeEmpresa.TabStop = false;
            this.lblNomeEmpresa.Text = "24 - Nome da empresa";
            // 
            // lblEnderecoEmpresa
            // 
            this.lblEnderecoEmpresa.BackColor = System.Drawing.Color.LightGray;
            this.lblEnderecoEmpresa.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblEnderecoEmpresa.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblEnderecoEmpresa.Location = new System.Drawing.Point(16, 388);
            this.lblEnderecoEmpresa.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.lblEnderecoEmpresa.Name = "lblEnderecoEmpresa";
            this.lblEnderecoEmpresa.Size = new System.Drawing.Size(375, 24);
            this.lblEnderecoEmpresa.TabIndex = 29;
            this.lblEnderecoEmpresa.TabStop = false;
            this.lblEnderecoEmpresa.Text = "25 - Endereço";
            // 
            // lblNumeroEmpresa
            // 
            this.lblNumeroEmpresa.BackColor = System.Drawing.Color.LightGray;
            this.lblNumeroEmpresa.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblNumeroEmpresa.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNumeroEmpresa.Location = new System.Drawing.Point(16, 412);
            this.lblNumeroEmpresa.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.lblNumeroEmpresa.Name = "lblNumeroEmpresa";
            this.lblNumeroEmpresa.Size = new System.Drawing.Size(375, 24);
            this.lblNumeroEmpresa.TabIndex = 30;
            this.lblNumeroEmpresa.TabStop = false;
            this.lblNumeroEmpresa.Text = "26 - Número";
            // 
            // lblComplementoEmpresa
            // 
            this.lblComplementoEmpresa.BackColor = System.Drawing.Color.LightGray;
            this.lblComplementoEmpresa.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblComplementoEmpresa.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblComplementoEmpresa.Location = new System.Drawing.Point(16, 437);
            this.lblComplementoEmpresa.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.lblComplementoEmpresa.Name = "lblComplementoEmpresa";
            this.lblComplementoEmpresa.Size = new System.Drawing.Size(375, 24);
            this.lblComplementoEmpresa.TabIndex = 31;
            this.lblComplementoEmpresa.TabStop = false;
            this.lblComplementoEmpresa.Text = "27 - Complemento";
            // 
            // lblBairroEmpresa
            // 
            this.lblBairroEmpresa.BackColor = System.Drawing.Color.LightGray;
            this.lblBairroEmpresa.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblBairroEmpresa.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblBairroEmpresa.Location = new System.Drawing.Point(16, 462);
            this.lblBairroEmpresa.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.lblBairroEmpresa.Name = "lblBairroEmpresa";
            this.lblBairroEmpresa.Size = new System.Drawing.Size(375, 24);
            this.lblBairroEmpresa.TabIndex = 32;
            this.lblBairroEmpresa.TabStop = false;
            this.lblBairroEmpresa.Text = "28 - Bairro";
            // 
            // lblUFEmpresa
            // 
            this.lblUFEmpresa.BackColor = System.Drawing.Color.LightGray;
            this.lblUFEmpresa.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblUFEmpresa.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblUFEmpresa.Location = new System.Drawing.Point(16, 486);
            this.lblUFEmpresa.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.lblUFEmpresa.Name = "lblUFEmpresa";
            this.lblUFEmpresa.Size = new System.Drawing.Size(375, 24);
            this.lblUFEmpresa.TabIndex = 33;
            this.lblUFEmpresa.TabStop = false;
            this.lblUFEmpresa.Text = "29 - UF";
            // 
            // lblCEPEmpresa
            // 
            this.lblCEPEmpresa.BackColor = System.Drawing.Color.LightGray;
            this.lblCEPEmpresa.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblCEPEmpresa.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCEPEmpresa.Location = new System.Drawing.Point(16, 511);
            this.lblCEPEmpresa.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.lblCEPEmpresa.Name = "lblCEPEmpresa";
            this.lblCEPEmpresa.Size = new System.Drawing.Size(375, 24);
            this.lblCEPEmpresa.TabIndex = 34;
            this.lblCEPEmpresa.TabStop = false;
            this.lblCEPEmpresa.Text = "30 - CEP";
            // 
            // lblCNPJEmpresa
            // 
            this.lblCNPJEmpresa.BackColor = System.Drawing.Color.LightGray;
            this.lblCNPJEmpresa.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblCNPJEmpresa.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCNPJEmpresa.Location = new System.Drawing.Point(16, 535);
            this.lblCNPJEmpresa.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.lblCNPJEmpresa.Name = "lblCNPJEmpresa";
            this.lblCNPJEmpresa.Size = new System.Drawing.Size(375, 24);
            this.lblCNPJEmpresa.TabIndex = 35;
            this.lblCNPJEmpresa.TabStop = false;
            this.lblCNPJEmpresa.Text = "31 - CNPJ";
            // 
            // lblInscricaoEmpresa
            // 
            this.lblInscricaoEmpresa.BackColor = System.Drawing.Color.LightGray;
            this.lblInscricaoEmpresa.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblInscricaoEmpresa.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblInscricaoEmpresa.Location = new System.Drawing.Point(16, 560);
            this.lblInscricaoEmpresa.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.lblInscricaoEmpresa.Name = "lblInscricaoEmpresa";
            this.lblInscricaoEmpresa.Size = new System.Drawing.Size(375, 24);
            this.lblInscricaoEmpresa.TabIndex = 36;
            this.lblInscricaoEmpresa.TabStop = false;
            this.lblInscricaoEmpresa.Text = "32 - Inscrição Estadual / Municipal";
            // 
            // lblEmailEmpresa
            // 
            this.lblEmailEmpresa.BackColor = System.Drawing.Color.LightGray;
            this.lblEmailEmpresa.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblEmailEmpresa.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblEmailEmpresa.Location = new System.Drawing.Point(16, 585);
            this.lblEmailEmpresa.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.lblEmailEmpresa.Name = "lblEmailEmpresa";
            this.lblEmailEmpresa.Size = new System.Drawing.Size(375, 24);
            this.lblEmailEmpresa.TabIndex = 37;
            this.lblEmailEmpresa.TabStop = false;
            this.lblEmailEmpresa.Text = "33 - E-mail";
            // 
            // lblServidorSMTP
            // 
            this.lblServidorSMTP.BackColor = System.Drawing.Color.LightGray;
            this.lblServidorSMTP.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblServidorSMTP.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblServidorSMTP.Location = new System.Drawing.Point(16, 609);
            this.lblServidorSMTP.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.lblServidorSMTP.Name = "lblServidorSMTP";
            this.lblServidorSMTP.Size = new System.Drawing.Size(375, 24);
            this.lblServidorSMTP.TabIndex = 38;
            this.lblServidorSMTP.TabStop = false;
            this.lblServidorSMTP.Text = "34 - Servidor SMTP";
            // 
            // lblNumeroPorta
            // 
            this.lblNumeroPorta.BackColor = System.Drawing.Color.LightGray;
            this.lblNumeroPorta.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblNumeroPorta.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNumeroPorta.Location = new System.Drawing.Point(16, 634);
            this.lblNumeroPorta.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.lblNumeroPorta.Name = "lblNumeroPorta";
            this.lblNumeroPorta.Size = new System.Drawing.Size(375, 24);
            this.lblNumeroPorta.TabIndex = 39;
            this.lblNumeroPorta.TabStop = false;
            this.lblNumeroPorta.Text = "35 - Número da porta";
            // 
            // lblSSL
            // 
            this.lblSSL.BackColor = System.Drawing.Color.LightGray;
            this.lblSSL.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblSSL.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSSL.Location = new System.Drawing.Point(16, 658);
            this.lblSSL.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.lblSSL.Name = "lblSSL";
            this.lblSSL.Size = new System.Drawing.Size(375, 24);
            this.lblSSL.TabIndex = 40;
            this.lblSSL.TabStop = false;
            this.lblSSL.Text = "36 - Usa SSL";
            // 
            // lblEmailEnvio
            // 
            this.lblEmailEnvio.BackColor = System.Drawing.Color.LightGray;
            this.lblEmailEnvio.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblEmailEnvio.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblEmailEnvio.Location = new System.Drawing.Point(16, 683);
            this.lblEmailEnvio.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.lblEmailEnvio.Name = "lblEmailEnvio";
            this.lblEmailEnvio.Size = new System.Drawing.Size(375, 24);
            this.lblEmailEnvio.TabIndex = 41;
            this.lblEmailEnvio.TabStop = false;
            this.lblEmailEnvio.Text = "37 - Email para envio";
            // 
            // lblSenhaEmail
            // 
            this.lblSenhaEmail.BackColor = System.Drawing.Color.LightGray;
            this.lblSenhaEmail.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblSenhaEmail.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSenhaEmail.Location = new System.Drawing.Point(16, 708);
            this.lblSenhaEmail.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.lblSenhaEmail.Name = "lblSenhaEmail";
            this.lblSenhaEmail.Size = new System.Drawing.Size(375, 24);
            this.lblSenhaEmail.TabIndex = 42;
            this.lblSenhaEmail.TabStop = false;
            this.lblSenhaEmail.Text = "38 - Senha para envio";
            // 
            // textNumeroInicialNF
            // 
            this.textNumeroInicialNF.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textNumeroInicialNF.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textNumeroInicialNF.Location = new System.Drawing.Point(391, 43);
            this.textNumeroInicialNF.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.textNumeroInicialNF.MaxLength = 10;
            this.textNumeroInicialNF.Name = "textNumeroInicialNF";
            this.textNumeroInicialNF.ReadOnly = true;
            this.textNumeroInicialNF.Size = new System.Drawing.Size(605, 24);
            this.textNumeroInicialNF.TabIndex = 9;
            this.toolTip1.SetToolTip(this.textNumeroInicialNF, "Número inicial da nota fiscal para utilização do sistema.");
            this.textNumeroInicialNF.Enter += new System.EventHandler(this.textNumeroInicialNF_Enter);
            this.textNumeroInicialNF.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.textNumeroInicialNF_KeyPress);
            this.textNumeroInicialNF.Leave += new System.EventHandler(this.textNumeroInicialNF_Leave);
            // 
            // textJurosProrrogacao
            // 
            this.textJurosProrrogacao.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textJurosProrrogacao.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textJurosProrrogacao.Location = new System.Drawing.Point(391, 68);
            this.textJurosProrrogacao.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.textJurosProrrogacao.MaxLength = 15;
            this.textJurosProrrogacao.Name = "textJurosProrrogacao";
            this.textJurosProrrogacao.Size = new System.Drawing.Size(605, 24);
            this.textJurosProrrogacao.TabIndex = 10;
            this.toolTip1.SetToolTip(this.textJurosProrrogacao, "% de juros cobrado nas prorrogações de cobranças.");
            this.textJurosProrrogacao.Enter += new System.EventHandler(this.textJurosProrrogacao_Enter);
            this.textJurosProrrogacao.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.textJurosProrrogacao_KeyPress);
            this.textJurosProrrogacao.Leave += new System.EventHandler(this.textJurosProrrogacao_Leave);
            // 
            // textQtdeTermica
            // 
            this.textQtdeTermica.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textQtdeTermica.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textQtdeTermica.Location = new System.Drawing.Point(391, 92);
            this.textQtdeTermica.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.textQtdeTermica.MaxLength = 2;
            this.textQtdeTermica.Name = "textQtdeTermica";
            this.textQtdeTermica.Size = new System.Drawing.Size(605, 24);
            this.textQtdeTermica.TabIndex = 11;
            this.toolTip1.SetToolTip(this.textQtdeTermica, "Quantidade de vias utilizada para impressão térmica.");
            this.textQtdeTermica.Enter += new System.EventHandler(this.textQtdeTermica_Enter);
            this.textQtdeTermica.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.textQtdeTermica_KeyPress);
            this.textQtdeTermica.Leave += new System.EventHandler(this.textQtdeTermica_Leave);
            // 
            // textNomeEmpresaTermica
            // 
            this.textNomeEmpresaTermica.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textNomeEmpresaTermica.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.textNomeEmpresaTermica.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textNomeEmpresaTermica.Location = new System.Drawing.Point(391, 117);
            this.textNomeEmpresaTermica.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.textNomeEmpresaTermica.MaxLength = 200;
            this.textNomeEmpresaTermica.Name = "textNomeEmpresaTermica";
            this.textNomeEmpresaTermica.Size = new System.Drawing.Size(605, 24);
            this.textNomeEmpresaTermica.TabIndex = 12;
            this.toolTip1.SetToolTip(this.textNomeEmpresaTermica, "Nome da empresa utilizado para impressão da encaminhamento e exames na impressora" +
        " térmica.");
            this.textNomeEmpresaTermica.Enter += new System.EventHandler(this.textNomeEmpresaTermica_Enter);
            this.textNomeEmpresaTermica.Leave += new System.EventHandler(this.textNomeEmpresaTermica_Leave);
            // 
            // textNomeImpressoraTermica
            // 
            this.textNomeImpressoraTermica.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textNomeImpressoraTermica.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.textNomeImpressoraTermica.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textNomeImpressoraTermica.Location = new System.Drawing.Point(391, 166);
            this.textNomeImpressoraTermica.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.textNomeImpressoraTermica.MaxLength = 100;
            this.textNomeImpressoraTermica.Name = "textNomeImpressoraTermica";
            this.textNomeImpressoraTermica.Size = new System.Drawing.Size(605, 24);
            this.textNomeImpressoraTermica.TabIndex = 14;
            this.toolTip1.SetToolTip(this.textNomeImpressoraTermica, "Nome da fila de compartilhamento da impressora térmica.");
            this.textNomeImpressoraTermica.Enter += new System.EventHandler(this.textNomeImpressoraTermica_Enter);
            this.textNomeImpressoraTermica.Leave += new System.EventHandler(this.textNomeImpressoraTermica_Leave);
            // 
            // textSerialCliente
            // 
            this.textSerialCliente.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.textSerialCliente.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textSerialCliente.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textSerialCliente.Location = new System.Drawing.Point(391, 191);
            this.textSerialCliente.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.textSerialCliente.MaxLength = 15;
            this.textSerialCliente.Name = "textSerialCliente";
            this.textSerialCliente.ReadOnly = true;
            this.textSerialCliente.Size = new System.Drawing.Size(605, 24);
            this.textSerialCliente.TabIndex = 15;
            this.textSerialCliente.TabStop = false;
            this.toolTip1.SetToolTip(this.textSerialCliente, "Seria de tempo de utilização do sistema pelo cliente. Essa informação não pode se" +
        "r alterada.");
            // 
            // cbGeraMovimentoExame
            // 
            this.cbGeraMovimentoExame.BackColor = System.Drawing.Color.LightGray;
            this.cbGeraMovimentoExame.BorderColor = System.Drawing.Color.DimGray;
            this.cbGeraMovimentoExame.BorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid;
            this.cbGeraMovimentoExame.FormattingEnabled = true;
            this.cbGeraMovimentoExame.Location = new System.Drawing.Point(391, 215);
            this.cbGeraMovimentoExame.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.cbGeraMovimentoExame.Name = "cbGeraMovimentoExame";
            this.cbGeraMovimentoExame.Size = new System.Drawing.Size(604, 24);
            this.cbGeraMovimentoExame.TabIndex = 16;
            this.toolTip1.SetToolTip(this.cbGeraMovimentoExame, resources.GetString("cbGeraMovimentoExame.ToolTip"));
            this.cbGeraMovimentoExame.SelectedIndexChanged += new System.EventHandler(this.cbGeraMovimentoExame_SelectedIndexChanged);
            // 
            // cbGravaProdutoAutomatico
            // 
            this.cbGravaProdutoAutomatico.BackColor = System.Drawing.Color.LightGray;
            this.cbGravaProdutoAutomatico.BorderColor = System.Drawing.Color.DimGray;
            this.cbGravaProdutoAutomatico.BorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid;
            this.cbGravaProdutoAutomatico.FormattingEnabled = true;
            this.cbGravaProdutoAutomatico.Location = new System.Drawing.Point(391, 240);
            this.cbGravaProdutoAutomatico.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.cbGravaProdutoAutomatico.Name = "cbGravaProdutoAutomatico";
            this.cbGravaProdutoAutomatico.Size = new System.Drawing.Size(604, 24);
            this.cbGravaProdutoAutomatico.TabIndex = 19;
            this.toolTip1.SetToolTip(this.cbGravaProdutoAutomatico, resources.GetString("cbGravaProdutoAutomatico.ToolTip"));
            this.cbGravaProdutoAutomatico.SelectedIndexChanged += new System.EventHandler(this.cbGravaProdutoAutomatico_SelectedIndexChanged);
            // 
            // textAso2Via
            // 
            this.textAso2Via.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.textAso2Via.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textAso2Via.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textAso2Via.Location = new System.Drawing.Point(391, 265);
            this.textAso2Via.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.textAso2Via.MaxLength = 3;
            this.textAso2Via.Name = "textAso2Via";
            this.textAso2Via.ReadOnly = true;
            this.textAso2Via.Size = new System.Drawing.Size(566, 24);
            this.textAso2Via.TabIndex = 59;
            this.textAso2Via.TabStop = false;
            // 
            // btnAso2Via
            // 
            this.btnAso2Via.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnAso2Via.Image = global::SWS.Properties.Resources.busca;
            this.btnAso2Via.Location = new System.Drawing.Point(953, 265);
            this.btnAso2Via.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.btnAso2Via.Name = "btnAso2Via";
            this.btnAso2Via.Size = new System.Drawing.Size(43, 26);
            this.btnAso2Via.TabIndex = 20;
            this.toolTip1.SetToolTip(this.btnAso2Via, "Código do produto que será gravado no movimento caso haja ocorrência de impressão" +
        " de segunda de impressão do atendimento.");
            this.btnAso2Via.UseVisualStyleBackColor = true;
            this.btnAso2Via.Click += new System.EventHandler(this.btnAso2Via_Click);
            // 
            // btnAsoTranscrito
            // 
            this.btnAsoTranscrito.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnAsoTranscrito.Image = global::SWS.Properties.Resources.busca;
            this.btnAsoTranscrito.Location = new System.Drawing.Point(953, 289);
            this.btnAsoTranscrito.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.btnAsoTranscrito.Name = "btnAsoTranscrito";
            this.btnAsoTranscrito.Size = new System.Drawing.Size(43, 26);
            this.btnAsoTranscrito.TabIndex = 21;
            this.toolTip1.SetToolTip(this.btnAsoTranscrito, "Código do produto que será gravado no movimento casa haja ocorrência de exames tr" +
        "anscritos no atendimento.");
            this.btnAsoTranscrito.UseVisualStyleBackColor = true;
            this.btnAsoTranscrito.Click += new System.EventHandler(this.btnAsoTranscrito_Click);
            // 
            // textAsoTranscrito
            // 
            this.textAsoTranscrito.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.textAsoTranscrito.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textAsoTranscrito.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textAsoTranscrito.Location = new System.Drawing.Point(391, 289);
            this.textAsoTranscrito.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.textAsoTranscrito.MaxLength = 3;
            this.textAsoTranscrito.Name = "textAsoTranscrito";
            this.textAsoTranscrito.ReadOnly = true;
            this.textAsoTranscrito.Size = new System.Drawing.Size(566, 24);
            this.textAsoTranscrito.TabIndex = 61;
            this.textAsoTranscrito.TabStop = false;
            // 
            // btnAsoInclusaoExame
            // 
            this.btnAsoInclusaoExame.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnAsoInclusaoExame.Image = global::SWS.Properties.Resources.busca;
            this.btnAsoInclusaoExame.Location = new System.Drawing.Point(953, 314);
            this.btnAsoInclusaoExame.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.btnAsoInclusaoExame.Name = "btnAsoInclusaoExame";
            this.btnAsoInclusaoExame.Size = new System.Drawing.Size(43, 26);
            this.btnAsoInclusaoExame.TabIndex = 22;
            this.toolTip1.SetToolTip(this.btnAsoInclusaoExame, "Código do produto que será gravado no movimento caso haja ocorrência de inclusão " +
        "de exames no atendimento.");
            this.btnAsoInclusaoExame.UseVisualStyleBackColor = true;
            this.btnAsoInclusaoExame.Click += new System.EventHandler(this.btnAsoInclusaoExame_Click);
            // 
            // textAsoInclusaoExame
            // 
            this.textAsoInclusaoExame.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.textAsoInclusaoExame.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textAsoInclusaoExame.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textAsoInclusaoExame.Location = new System.Drawing.Point(391, 314);
            this.textAsoInclusaoExame.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.textAsoInclusaoExame.MaxLength = 3;
            this.textAsoInclusaoExame.Name = "textAsoInclusaoExame";
            this.textAsoInclusaoExame.ReadOnly = true;
            this.textAsoInclusaoExame.Size = new System.Drawing.Size(566, 24);
            this.textAsoInclusaoExame.TabIndex = 63;
            this.textAsoInclusaoExame.TabStop = false;
            // 
            // textCidade
            // 
            this.textCidade.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textCidade.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.textCidade.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textCidade.Location = new System.Drawing.Point(391, 338);
            this.textCidade.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.textCidade.MaxLength = 200;
            this.textCidade.Name = "textCidade";
            this.textCidade.Size = new System.Drawing.Size(605, 24);
            this.textCidade.TabIndex = 23;
            this.toolTip1.SetToolTip(this.textCidade, "Nome da cidade que aparecerá impresso no rodapé de alguns relatórios.");
            this.textCidade.Enter += new System.EventHandler(this.textCidade_Enter);
            this.textCidade.Leave += new System.EventHandler(this.textCidade_Leave);
            // 
            // textNomeEmpresa
            // 
            this.textNomeEmpresa.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textNomeEmpresa.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.textNomeEmpresa.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textNomeEmpresa.Location = new System.Drawing.Point(391, 363);
            this.textNomeEmpresa.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.textNomeEmpresa.MaxLength = 200;
            this.textNomeEmpresa.Name = "textNomeEmpresa";
            this.textNomeEmpresa.Size = new System.Drawing.Size(605, 24);
            this.textNomeEmpresa.TabIndex = 24;
            this.toolTip1.SetToolTip(this.textNomeEmpresa, "Nome da empresa que aparecerá impresso em alguns relatórios.");
            this.textNomeEmpresa.Enter += new System.EventHandler(this.textNomeEmpresa_Enter);
            this.textNomeEmpresa.Leave += new System.EventHandler(this.textNomeEmpresa_Leave);
            // 
            // textEndereco
            // 
            this.textEndereco.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textEndereco.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.textEndereco.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textEndereco.Location = new System.Drawing.Point(391, 388);
            this.textEndereco.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.textEndereco.MaxLength = 255;
            this.textEndereco.Name = "textEndereco";
            this.textEndereco.Size = new System.Drawing.Size(605, 24);
            this.textEndereco.TabIndex = 25;
            this.toolTip1.SetToolTip(this.textEndereco, "Endereço da empresa que aparecerá impresso em alguns relatórios.");
            this.textEndereco.Enter += new System.EventHandler(this.textEndereco_Enter);
            this.textEndereco.Leave += new System.EventHandler(this.textEndereco_Leave);
            // 
            // textNumero
            // 
            this.textNumero.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textNumero.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.textNumero.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textNumero.Location = new System.Drawing.Point(391, 412);
            this.textNumero.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.textNumero.MaxLength = 30;
            this.textNumero.Name = "textNumero";
            this.textNumero.Size = new System.Drawing.Size(605, 24);
            this.textNumero.TabIndex = 26;
            this.toolTip1.SetToolTip(this.textNumero, "Número do endereço da empresa que aparecerá impresso em alguns relatórios.");
            this.textNumero.Enter += new System.EventHandler(this.textNumero_Enter);
            this.textNumero.Leave += new System.EventHandler(this.textNumero_Leave);
            // 
            // textComplemento
            // 
            this.textComplemento.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textComplemento.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.textComplemento.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textComplemento.Location = new System.Drawing.Point(391, 437);
            this.textComplemento.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.textComplemento.MaxLength = 255;
            this.textComplemento.Name = "textComplemento";
            this.textComplemento.Size = new System.Drawing.Size(605, 24);
            this.textComplemento.TabIndex = 27;
            this.toolTip1.SetToolTip(this.textComplemento, "Complemento do endereço da empresa que aparecerá impresso em alguns relatórios.\r\n" +
        "\r\n");
            this.textComplemento.Enter += new System.EventHandler(this.textComplemento_Enter);
            this.textComplemento.Leave += new System.EventHandler(this.textComplemento_Leave);
            // 
            // textBairro
            // 
            this.textBairro.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBairro.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.textBairro.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBairro.Location = new System.Drawing.Point(391, 462);
            this.textBairro.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.textBairro.MaxLength = 255;
            this.textBairro.Name = "textBairro";
            this.textBairro.Size = new System.Drawing.Size(605, 24);
            this.textBairro.TabIndex = 28;
            this.toolTip1.SetToolTip(this.textBairro, "Bairro da empresa que aparecerá impresso em alguns relatórios.");
            this.textBairro.Enter += new System.EventHandler(this.textBairro_Enter);
            this.textBairro.Leave += new System.EventHandler(this.textBairro_Leave);
            // 
            // textUF
            // 
            this.textUF.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textUF.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.textUF.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textUF.Location = new System.Drawing.Point(391, 486);
            this.textUF.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.textUF.MaxLength = 2;
            this.textUF.Name = "textUF";
            this.textUF.Size = new System.Drawing.Size(605, 24);
            this.textUF.TabIndex = 29;
            this.toolTip1.SetToolTip(this.textUF, "UF da empresa que aparecerá impresso em alguns relatórios.\r\n");
            this.textUF.Enter += new System.EventHandler(this.textUF_Enter);
            this.textUF.Leave += new System.EventHandler(this.textUF_Leave);
            // 
            // textCEP
            // 
            this.textCEP.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textCEP.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textCEP.Location = new System.Drawing.Point(391, 511);
            this.textCEP.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.textCEP.Mask = "99,999-999";
            this.textCEP.Name = "textCEP";
            this.textCEP.PromptChar = ' ';
            this.textCEP.Size = new System.Drawing.Size(605, 24);
            this.textCEP.TabIndex = 30;
            this.toolTip1.SetToolTip(this.textCEP, "CEP da empresa que aparecerá impresso em alguns relatórios.");
            this.textCEP.Enter += new System.EventHandler(this.textCEP_Enter);
            this.textCEP.Leave += new System.EventHandler(this.textCEP_Leave);
            // 
            // textCNPJ
            // 
            this.textCNPJ.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textCNPJ.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textCNPJ.Location = new System.Drawing.Point(391, 535);
            this.textCNPJ.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.textCNPJ.Mask = "99,999,999/9999-99";
            this.textCNPJ.Name = "textCNPJ";
            this.textCNPJ.PromptChar = ' ';
            this.textCNPJ.Size = new System.Drawing.Size(605, 24);
            this.textCNPJ.TabIndex = 31;
            this.toolTip1.SetToolTip(this.textCNPJ, "CNPJ da empresa que aparecerá impresso em alguns relatórios.");
            this.textCNPJ.Enter += new System.EventHandler(this.textCNPJ_Enter);
            this.textCNPJ.Leave += new System.EventHandler(this.textCNPJ_Leave);
            // 
            // textInscricao
            // 
            this.textInscricao.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textInscricao.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.textInscricao.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textInscricao.Location = new System.Drawing.Point(391, 560);
            this.textInscricao.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.textInscricao.MaxLength = 30;
            this.textInscricao.Name = "textInscricao";
            this.textInscricao.Size = new System.Drawing.Size(605, 24);
            this.textInscricao.TabIndex = 32;
            this.toolTip1.SetToolTip(this.textInscricao, "Inscrição estadual ou municipal da empresa que aparecerá impresso em alguns relat" +
        "órios.");
            this.textInscricao.Enter += new System.EventHandler(this.textInscricao_Enter);
            this.textInscricao.Leave += new System.EventHandler(this.textInscricao_Leave);
            // 
            // textEmail
            // 
            this.textEmail.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textEmail.CharacterCasing = System.Windows.Forms.CharacterCasing.Lower;
            this.textEmail.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textEmail.Location = new System.Drawing.Point(391, 585);
            this.textEmail.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.textEmail.MaxLength = 255;
            this.textEmail.Name = "textEmail";
            this.textEmail.Size = new System.Drawing.Size(605, 24);
            this.textEmail.TabIndex = 33;
            this.toolTip1.SetToolTip(this.textEmail, "E-Mail da empresa que aparecerá impresso em alguns relatórios.");
            this.textEmail.Enter += new System.EventHandler(this.textEmail_Enter);
            this.textEmail.Leave += new System.EventHandler(this.textEmail_Leave);
            // 
            // textServidorSMTP
            // 
            this.textServidorSMTP.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textServidorSMTP.CharacterCasing = System.Windows.Forms.CharacterCasing.Lower;
            this.textServidorSMTP.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textServidorSMTP.Location = new System.Drawing.Point(391, 609);
            this.textServidorSMTP.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.textServidorSMTP.MaxLength = 255;
            this.textServidorSMTP.Name = "textServidorSMTP";
            this.textServidorSMTP.Size = new System.Drawing.Size(605, 24);
            this.textServidorSMTP.TabIndex = 34;
            this.toolTip1.SetToolTip(this.textServidorSMTP, "nome do servidor SMTP para envio de emails pelo sistema.");
            this.textServidorSMTP.Enter += new System.EventHandler(this.textServidorSMTP_Enter);
            this.textServidorSMTP.Leave += new System.EventHandler(this.textServidorSMTP_Leave);
            // 
            // textNumeroPorta
            // 
            this.textNumeroPorta.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textNumeroPorta.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textNumeroPorta.Location = new System.Drawing.Point(391, 634);
            this.textNumeroPorta.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.textNumeroPorta.MaxLength = 4;
            this.textNumeroPorta.Name = "textNumeroPorta";
            this.textNumeroPorta.Size = new System.Drawing.Size(605, 24);
            this.textNumeroPorta.TabIndex = 35;
            this.toolTip1.SetToolTip(this.textNumeroPorta, "Número da porta usada pelo servidor de emails SMTP para envio de emails pelo site" +
        "ma.");
            this.textNumeroPorta.Enter += new System.EventHandler(this.textNumeroPorta_Enter);
            this.textNumeroPorta.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.textNumeroPorta_KeyPress);
            this.textNumeroPorta.Leave += new System.EventHandler(this.textNumeroPorta_Leave);
            // 
            // cbSSL
            // 
            this.cbSSL.BackColor = System.Drawing.Color.LightGray;
            this.cbSSL.BorderColor = System.Drawing.Color.DimGray;
            this.cbSSL.BorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid;
            this.cbSSL.FormattingEnabled = true;
            this.cbSSL.Location = new System.Drawing.Point(391, 658);
            this.cbSSL.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.cbSSL.Name = "cbSSL";
            this.cbSSL.Size = new System.Drawing.Size(604, 24);
            this.cbSSL.TabIndex = 36;
            this.cbSSL.SelectedIndexChanged += new System.EventHandler(this.cbSSL_SelectedIndexChanged);
            // 
            // textEmailEnvio
            // 
            this.textEmailEnvio.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textEmailEnvio.CharacterCasing = System.Windows.Forms.CharacterCasing.Lower;
            this.textEmailEnvio.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textEmailEnvio.Location = new System.Drawing.Point(391, 683);
            this.textEmailEnvio.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.textEmailEnvio.MaxLength = 255;
            this.textEmailEnvio.Name = "textEmailEnvio";
            this.textEmailEnvio.Size = new System.Drawing.Size(605, 24);
            this.textEmailEnvio.TabIndex = 37;
            this.toolTip1.SetToolTip(this.textEmailEnvio, "E-mail que enviará os emails pelo sistema.");
            this.textEmailEnvio.Enter += new System.EventHandler(this.textEmailEnvio_Enter);
            this.textEmailEnvio.Leave += new System.EventHandler(this.textEmailEnvio_Leave);
            // 
            // textSenhaEmail
            // 
            this.textSenhaEmail.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textSenhaEmail.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textSenhaEmail.Location = new System.Drawing.Point(391, 708);
            this.textSenhaEmail.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.textSenhaEmail.Name = "textSenhaEmail";
            this.textSenhaEmail.PasswordChar = '*';
            this.textSenhaEmail.PromptChar = ' ';
            this.textSenhaEmail.Size = new System.Drawing.Size(605, 24);
            this.textSenhaEmail.TabIndex = 38;
            this.toolTip1.SetToolTip(this.textSenhaEmail, "Senha da conta para envio de emails pelo sistema.");
            this.textSenhaEmail.Enter += new System.EventHandler(this.textSenhaEmail_Enter);
            this.textSenhaEmail.Leave += new System.EventHandler(this.textSenhaEmail_Leave);
            // 
            // textIPServidorTermica
            // 
            this.textIPServidorTermica.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textIPServidorTermica.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.textIPServidorTermica.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textIPServidorTermica.Location = new System.Drawing.Point(391, 142);
            this.textIPServidorTermica.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.textIPServidorTermica.MaxLength = 15;
            this.textIPServidorTermica.Name = "textIPServidorTermica";
            this.textIPServidorTermica.Size = new System.Drawing.Size(605, 24);
            this.textIPServidorTermica.TabIndex = 13;
            this.toolTip1.SetToolTip(this.textIPServidorTermica, "IP do servidor onde está localizada a impressora térmica. O formado do IP deverá " +
        "ser 255.255.255.255");
            this.textIPServidorTermica.Enter += new System.EventHandler(this.textIPServidorTermica_Enter_1);
            this.textIPServidorTermica.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.textIPServidorTermica_KeyPress);
            this.textIPServidorTermica.Leave += new System.EventHandler(this.textIPServidorTermica_Leave_1);
            // 
            // textDiasPesquisa
            // 
            this.textDiasPesquisa.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textDiasPesquisa.CharacterCasing = System.Windows.Forms.CharacterCasing.Lower;
            this.textDiasPesquisa.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textDiasPesquisa.Location = new System.Drawing.Point(391, 757);
            this.textDiasPesquisa.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.textDiasPesquisa.MaxLength = 3;
            this.textDiasPesquisa.Name = "textDiasPesquisa";
            this.textDiasPesquisa.Size = new System.Drawing.Size(605, 24);
            this.textDiasPesquisa.TabIndex = 40;
            this.toolTip1.SetToolTip(this.textDiasPesquisa, "E-mail que enviará os emails pelo sistema.");
            this.textDiasPesquisa.Enter += new System.EventHandler(this.textDiasPesquisa_Enter);
            this.textDiasPesquisa.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.textDiasPesquisa_KeyPress);
            this.textDiasPesquisa.Leave += new System.EventHandler(this.textDiasPesquisa_Leave);
            // 
            // textVersaoSistema
            // 
            this.textVersaoSistema.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textVersaoSistema.CharacterCasing = System.Windows.Forms.CharacterCasing.Lower;
            this.textVersaoSistema.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textVersaoSistema.Location = new System.Drawing.Point(391, 854);
            this.textVersaoSistema.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.textVersaoSistema.MaxLength = 3;
            this.textVersaoSistema.Name = "textVersaoSistema";
            this.textVersaoSistema.ReadOnly = true;
            this.textVersaoSistema.Size = new System.Drawing.Size(605, 24);
            this.textVersaoSistema.TabIndex = 72;
            this.textVersaoSistema.TabStop = false;
            this.toolTip1.SetToolTip(this.textVersaoSistema, "Versão atual do sistema SWS");
            // 
            // lblObrigatorioGravarAtendimentoComMedicoExaminador
            // 
            this.lblObrigatorioGravarAtendimentoComMedicoExaminador.BackColor = System.Drawing.Color.LightGray;
            this.lblObrigatorioGravarAtendimentoComMedicoExaminador.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblObrigatorioGravarAtendimentoComMedicoExaminador.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblObrigatorioGravarAtendimentoComMedicoExaminador.Location = new System.Drawing.Point(16, 926);
            this.lblObrigatorioGravarAtendimentoComMedicoExaminador.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.lblObrigatorioGravarAtendimentoComMedicoExaminador.Name = "lblObrigatorioGravarAtendimentoComMedicoExaminador";
            this.lblObrigatorioGravarAtendimentoComMedicoExaminador.Size = new System.Drawing.Size(375, 24);
            this.lblObrigatorioGravarAtendimentoComMedicoExaminador.TabIndex = 77;
            this.lblObrigatorioGravarAtendimentoComMedicoExaminador.TabStop = false;
            this.lblObrigatorioGravarAtendimentoComMedicoExaminador.Text = "47 - Obrigatório Médico Examinador";
            this.toolTip1.SetToolTip(this.lblObrigatorioGravarAtendimentoComMedicoExaminador, "Obrigatório gravar o atendimento com o médico examinador");
            // 
            // lbdNumeroLicencas
            // 
            this.lbdNumeroLicencas.BackColor = System.Drawing.Color.LightGray;
            this.lbdNumeroLicencas.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lbdNumeroLicencas.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbdNumeroLicencas.Location = new System.Drawing.Point(16, 949);
            this.lbdNumeroLicencas.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.lbdNumeroLicencas.Name = "lbdNumeroLicencas";
            this.lbdNumeroLicencas.Size = new System.Drawing.Size(375, 24);
            this.lbdNumeroLicencas.TabIndex = 79;
            this.lbdNumeroLicencas.TabStop = false;
            this.lbdNumeroLicencas.Text = "48 - Número de Licenças";
            this.toolTip1.SetToolTip(this.lbdNumeroLicencas, "Número de Licenças");
            // 
            // textNumeroLicencas
            // 
            this.textNumeroLicencas.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textNumeroLicencas.CharacterCasing = System.Windows.Forms.CharacterCasing.Lower;
            this.textNumeroLicencas.Enabled = false;
            this.textNumeroLicencas.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textNumeroLicencas.Location = new System.Drawing.Point(391, 949);
            this.textNumeroLicencas.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.textNumeroLicencas.MaxLength = 3;
            this.textNumeroLicencas.Name = "textNumeroLicencas";
            this.textNumeroLicencas.Size = new System.Drawing.Size(605, 24);
            this.textNumeroLicencas.TabIndex = 80;
            this.toolTip1.SetToolTip(this.textNumeroLicencas, "Número de licenças permitidas no sistema");
            // 
            // lblCampoProcessoRealizadoInteiro
            // 
            this.lblCampoProcessoRealizadoInteiro.BackColor = System.Drawing.Color.LightGray;
            this.lblCampoProcessoRealizadoInteiro.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblCampoProcessoRealizadoInteiro.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCampoProcessoRealizadoInteiro.Location = new System.Drawing.Point(16, 974);
            this.lblCampoProcessoRealizadoInteiro.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.lblCampoProcessoRealizadoInteiro.Name = "lblCampoProcessoRealizadoInteiro";
            this.lblCampoProcessoRealizadoInteiro.Size = new System.Drawing.Size(375, 24);
            this.lblCampoProcessoRealizadoInteiro.TabIndex = 81;
            this.lblCampoProcessoRealizadoInteiro.TabStop = false;
            this.lblCampoProcessoRealizadoInteiro.Text = "49 - Converter E-social 2220 nome Processo";
            this.toolTip1.SetToolTip(this.lblCampoProcessoRealizadoInteiro, "Número de Licenças");
            // 
            // lblCampoGracaoIntegracao
            // 
            this.lblCampoGracaoIntegracao.BackColor = System.Drawing.Color.LightGray;
            this.lblCampoGracaoIntegracao.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblCampoGracaoIntegracao.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCampoGracaoIntegracao.Location = new System.Drawing.Point(16, 998);
            this.lblCampoGracaoIntegracao.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.lblCampoGracaoIntegracao.Name = "lblCampoGracaoIntegracao";
            this.lblCampoGracaoIntegracao.Size = new System.Drawing.Size(375, 24);
            this.lblCampoGracaoIntegracao.TabIndex = 83;
            this.lblCampoGracaoIntegracao.TabStop = false;
            this.lblCampoGracaoIntegracao.Text = "50 - local de arquivo da integração";
            this.toolTip1.SetToolTip(this.lblCampoGracaoIntegracao, "Número de Licenças");
            // 
            // textLocalntegracao
            // 
            this.textLocalntegracao.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textLocalntegracao.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.textLocalntegracao.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textLocalntegracao.Location = new System.Drawing.Point(391, 998);
            this.textLocalntegracao.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.textLocalntegracao.MaxLength = 255;
            this.textLocalntegracao.Name = "textLocalntegracao";
            this.textLocalntegracao.Size = new System.Drawing.Size(605, 24);
            this.textLocalntegracao.TabIndex = 84;
            this.toolTip1.SetToolTip(this.textLocalntegracao, "local onde vai ser gravado o XML da integração");
            this.textLocalntegracao.Enter += new System.EventHandler(this.textLocalntegracao_Enter);
            this.textLocalntegracao.Leave += new System.EventHandler(this.textLocalntegracao_Leave);
            // 
            // textBox1
            // 
            this.textBox1.BackColor = System.Drawing.Color.LightGray;
            this.textBox1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox1.Location = new System.Drawing.Point(16, 732);
            this.textBox1.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(375, 24);
            this.textBox1.TabIndex = 64;
            this.textBox1.TabStop = false;
            this.textBox1.Text = "39 - Utiliza Filial?";
            // 
            // cbUtilizaFilial
            // 
            this.cbUtilizaFilial.BackColor = System.Drawing.Color.LightGray;
            this.cbUtilizaFilial.BorderColor = System.Drawing.Color.DimGray;
            this.cbUtilizaFilial.BorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid;
            this.cbUtilizaFilial.FormattingEnabled = true;
            this.cbUtilizaFilial.Location = new System.Drawing.Point(391, 732);
            this.cbUtilizaFilial.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.cbUtilizaFilial.Name = "cbUtilizaFilial";
            this.cbUtilizaFilial.Size = new System.Drawing.Size(604, 24);
            this.cbUtilizaFilial.TabIndex = 39;
            this.cbUtilizaFilial.SelectedIndexChanged += new System.EventHandler(this.cbUtilizaFilial_SelectedIndexChanged);
            // 
            // lblTempoPesquisaHistorico
            // 
            this.lblTempoPesquisaHistorico.BackColor = System.Drawing.Color.LightGray;
            this.lblTempoPesquisaHistorico.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblTempoPesquisaHistorico.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTempoPesquisaHistorico.Location = new System.Drawing.Point(16, 757);
            this.lblTempoPesquisaHistorico.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.lblTempoPesquisaHistorico.Name = "lblTempoPesquisaHistorico";
            this.lblTempoPesquisaHistorico.Size = new System.Drawing.Size(375, 24);
            this.lblTempoPesquisaHistorico.TabIndex = 65;
            this.lblTempoPesquisaHistorico.TabStop = false;
            this.lblTempoPesquisaHistorico.Text = "40 - Dias para pesquisar histórico";
            // 
            // lblImprimeSomenteSala
            // 
            this.lblImprimeSomenteSala.BackColor = System.Drawing.Color.LightGray;
            this.lblImprimeSomenteSala.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblImprimeSomenteSala.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblImprimeSomenteSala.Location = new System.Drawing.Point(16, 782);
            this.lblImprimeSomenteSala.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.lblImprimeSomenteSala.Name = "lblImprimeSomenteSala";
            this.lblImprimeSomenteSala.Size = new System.Drawing.Size(375, 24);
            this.lblImprimeSomenteSala.TabIndex = 66;
            this.lblImprimeSomenteSala.TabStop = false;
            this.lblImprimeSomenteSala.Text = "41 - Imprimir somente sala no encaminhamento?";
            // 
            // cbImprimeSala
            // 
            this.cbImprimeSala.BackColor = System.Drawing.Color.LightGray;
            this.cbImprimeSala.BorderColor = System.Drawing.Color.DimGray;
            this.cbImprimeSala.BorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid;
            this.cbImprimeSala.FormattingEnabled = true;
            this.cbImprimeSala.Location = new System.Drawing.Point(391, 782);
            this.cbImprimeSala.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.cbImprimeSala.Name = "cbImprimeSala";
            this.cbImprimeSala.Size = new System.Drawing.Size(604, 24);
            this.cbImprimeSala.TabIndex = 67;
            this.cbImprimeSala.SelectedIndexChanged += new System.EventHandler(this.cbImprimeSala_SelectedIndexChanged);
            // 
            // cbPcmsoVencido
            // 
            this.cbPcmsoVencido.BackColor = System.Drawing.Color.LightGray;
            this.cbPcmsoVencido.BorderColor = System.Drawing.Color.DimGray;
            this.cbPcmsoVencido.BorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid;
            this.cbPcmsoVencido.FormattingEnabled = true;
            this.cbPcmsoVencido.Location = new System.Drawing.Point(391, 806);
            this.cbPcmsoVencido.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.cbPcmsoVencido.Name = "cbPcmsoVencido";
            this.cbPcmsoVencido.Size = new System.Drawing.Size(604, 24);
            this.cbPcmsoVencido.TabIndex = 69;
            this.cbPcmsoVencido.SelectedIndexChanged += new System.EventHandler(this.cbPcmsoVencido_SelectedIndexChanged);
            // 
            // lblImprimirDadosAtendimentoPcmsoVencido
            // 
            this.lblImprimirDadosAtendimentoPcmsoVencido.BackColor = System.Drawing.Color.LightGray;
            this.lblImprimirDadosAtendimentoPcmsoVencido.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblImprimirDadosAtendimentoPcmsoVencido.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblImprimirDadosAtendimentoPcmsoVencido.Location = new System.Drawing.Point(16, 806);
            this.lblImprimirDadosAtendimentoPcmsoVencido.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.lblImprimirDadosAtendimentoPcmsoVencido.Name = "lblImprimirDadosAtendimentoPcmsoVencido";
            this.lblImprimirDadosAtendimentoPcmsoVencido.Size = new System.Drawing.Size(375, 24);
            this.lblImprimirDadosAtendimentoPcmsoVencido.TabIndex = 68;
            this.lblImprimirDadosAtendimentoPcmsoVencido.TabStop = false;
            this.lblImprimirDadosAtendimentoPcmsoVencido.Text = "42 - Imprimir no aso com PCMSO vencido?";
            // 
            // lblOrdenarAtendimentoPorSenha
            // 
            this.lblOrdenarAtendimentoPorSenha.BackColor = System.Drawing.Color.LightGray;
            this.lblOrdenarAtendimentoPorSenha.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblOrdenarAtendimentoPorSenha.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblOrdenarAtendimentoPorSenha.Location = new System.Drawing.Point(16, 830);
            this.lblOrdenarAtendimentoPorSenha.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.lblOrdenarAtendimentoPorSenha.Name = "lblOrdenarAtendimentoPorSenha";
            this.lblOrdenarAtendimentoPorSenha.Size = new System.Drawing.Size(375, 24);
            this.lblOrdenarAtendimentoPorSenha.TabIndex = 70;
            this.lblOrdenarAtendimentoPorSenha.TabStop = false;
            this.lblOrdenarAtendimentoPorSenha.Text = "43 - Ordernar atendimentos por senha?";
            // 
            // cbOrdenacaoAtendimento
            // 
            this.cbOrdenacaoAtendimento.BackColor = System.Drawing.Color.LightGray;
            this.cbOrdenacaoAtendimento.BorderColor = System.Drawing.Color.DimGray;
            this.cbOrdenacaoAtendimento.BorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid;
            this.cbOrdenacaoAtendimento.FormattingEnabled = true;
            this.cbOrdenacaoAtendimento.Location = new System.Drawing.Point(391, 831);
            this.cbOrdenacaoAtendimento.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.cbOrdenacaoAtendimento.Name = "cbOrdenacaoAtendimento";
            this.cbOrdenacaoAtendimento.Size = new System.Drawing.Size(604, 24);
            this.cbOrdenacaoAtendimento.TabIndex = 71;
            this.cbOrdenacaoAtendimento.SelectedIndexChanged += new System.EventHandler(this.cbOrdenacaoAtendimento_SelectedIndexChanged);
            // 
            // lblVersaoSistema
            // 
            this.lblVersaoSistema.BackColor = System.Drawing.Color.LightGray;
            this.lblVersaoSistema.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblVersaoSistema.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblVersaoSistema.Location = new System.Drawing.Point(16, 854);
            this.lblVersaoSistema.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.lblVersaoSistema.Name = "lblVersaoSistema";
            this.lblVersaoSistema.Size = new System.Drawing.Size(375, 24);
            this.lblVersaoSistema.TabIndex = 72;
            this.lblVersaoSistema.TabStop = false;
            this.lblVersaoSistema.Text = "44 - Versão atual do sistema";
            // 
            // lblCancelaAposFinalizado
            // 
            this.lblCancelaAposFinalizado.BackColor = System.Drawing.Color.LightGray;
            this.lblCancelaAposFinalizado.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblCancelaAposFinalizado.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCancelaAposFinalizado.Location = new System.Drawing.Point(16, 879);
            this.lblCancelaAposFinalizado.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.lblCancelaAposFinalizado.Name = "lblCancelaAposFinalizado";
            this.lblCancelaAposFinalizado.Size = new System.Drawing.Size(375, 24);
            this.lblCancelaAposFinalizado.TabIndex = 74;
            this.lblCancelaAposFinalizado.TabStop = false;
            this.lblCancelaAposFinalizado.Text = "45 - Cancelar exame apos atendimento finalizado?";
            // 
            // cbCancelaExameAtendimentoFinalizado
            // 
            this.cbCancelaExameAtendimentoFinalizado.BackColor = System.Drawing.Color.LightGray;
            this.cbCancelaExameAtendimentoFinalizado.BorderColor = System.Drawing.Color.DimGray;
            this.cbCancelaExameAtendimentoFinalizado.BorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid;
            this.cbCancelaExameAtendimentoFinalizado.FormattingEnabled = true;
            this.cbCancelaExameAtendimentoFinalizado.Location = new System.Drawing.Point(391, 879);
            this.cbCancelaExameAtendimentoFinalizado.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.cbCancelaExameAtendimentoFinalizado.Name = "cbCancelaExameAtendimentoFinalizado";
            this.cbCancelaExameAtendimentoFinalizado.Size = new System.Drawing.Size(604, 24);
            this.cbCancelaExameAtendimentoFinalizado.TabIndex = 73;
            this.cbCancelaExameAtendimentoFinalizado.SelectedIndexChanged += new System.EventHandler(this.cbCancelaExameAtendimentoFinalizado_SelectedIndexChanged);
            // 
            // lblGravaAtendimentoBloqueado
            // 
            this.lblGravaAtendimentoBloqueado.BackColor = System.Drawing.Color.LightGray;
            this.lblGravaAtendimentoBloqueado.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblGravaAtendimentoBloqueado.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblGravaAtendimentoBloqueado.Location = new System.Drawing.Point(16, 901);
            this.lblGravaAtendimentoBloqueado.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.lblGravaAtendimentoBloqueado.Name = "lblGravaAtendimentoBloqueado";
            this.lblGravaAtendimentoBloqueado.Size = new System.Drawing.Size(375, 24);
            this.lblGravaAtendimentoBloqueado.TabIndex = 75;
            this.lblGravaAtendimentoBloqueado.TabStop = false;
            this.lblGravaAtendimentoBloqueado.Text = "46 - Gravar atendimento bloqueado";
            // 
            // cbAtendimentoBloqueado
            // 
            this.cbAtendimentoBloqueado.BackColor = System.Drawing.Color.LightGray;
            this.cbAtendimentoBloqueado.BorderColor = System.Drawing.Color.DimGray;
            this.cbAtendimentoBloqueado.BorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid;
            this.cbAtendimentoBloqueado.FormattingEnabled = true;
            this.cbAtendimentoBloqueado.Location = new System.Drawing.Point(391, 901);
            this.cbAtendimentoBloqueado.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.cbAtendimentoBloqueado.Name = "cbAtendimentoBloqueado";
            this.cbAtendimentoBloqueado.Size = new System.Drawing.Size(604, 24);
            this.cbAtendimentoBloqueado.TabIndex = 76;
            this.cbAtendimentoBloqueado.SelectedIndexChanged += new System.EventHandler(this.cbAtendimentoBloqueado_SelectedIndexChanged);
            // 
            // cbMedicoExaminador
            // 
            this.cbMedicoExaminador.BackColor = System.Drawing.Color.LightGray;
            this.cbMedicoExaminador.BorderColor = System.Drawing.Color.DimGray;
            this.cbMedicoExaminador.BorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid;
            this.cbMedicoExaminador.FormattingEnabled = true;
            this.cbMedicoExaminador.Location = new System.Drawing.Point(391, 926);
            this.cbMedicoExaminador.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.cbMedicoExaminador.Name = "cbMedicoExaminador";
            this.cbMedicoExaminador.Size = new System.Drawing.Size(604, 24);
            this.cbMedicoExaminador.TabIndex = 78;
            this.cbMedicoExaminador.SelectedIndexChanged += new System.EventHandler(this.cbMedicoExaminador_SelectedIndexChanged);
            // 
            // cbConverterEsocial2220
            // 
            this.cbConverterEsocial2220.BackColor = System.Drawing.Color.LightGray;
            this.cbConverterEsocial2220.BorderColor = System.Drawing.Color.DimGray;
            this.cbConverterEsocial2220.BorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid;
            this.cbConverterEsocial2220.FormattingEnabled = true;
            this.cbConverterEsocial2220.Location = new System.Drawing.Point(391, 974);
            this.cbConverterEsocial2220.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.cbConverterEsocial2220.Name = "cbConverterEsocial2220";
            this.cbConverterEsocial2220.Size = new System.Drawing.Size(604, 24);
            this.cbConverterEsocial2220.TabIndex = 82;
            this.cbConverterEsocial2220.SelectedIndexChanged += new System.EventHandler(this.cbConverterEsocial2220_SelectedIndexChanged);
            // 
            // lblCodigoExameAnaliseRisco
            // 
            this.lblCodigoExameAnaliseRisco.BackColor = System.Drawing.Color.LightGray;
            this.lblCodigoExameAnaliseRisco.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblCodigoExameAnaliseRisco.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCodigoExameAnaliseRisco.Location = new System.Drawing.Point(16, 1022);
            this.lblCodigoExameAnaliseRisco.Margin = new System.Windows.Forms.Padding(4);
            this.lblCodigoExameAnaliseRisco.Name = "lblCodigoExameAnaliseRisco";
            this.lblCodigoExameAnaliseRisco.Size = new System.Drawing.Size(375, 24);
            this.lblCodigoExameAnaliseRisco.TabIndex = 85;
            this.lblCodigoExameAnaliseRisco.TabStop = false;
            this.lblCodigoExameAnaliseRisco.Text = "51 - Código Análise Risco";
            this.toolTip1.SetToolTip(this.lblCodigoExameAnaliseRisco, "Código do exame para análise de risco");
            // 
            // btnAnaliseRisco
            // 
            this.btnAnaliseRisco.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnAnaliseRisco.Image = global::SWS.Properties.Resources.busca;
            this.btnAnaliseRisco.Location = new System.Drawing.Point(953, 1022);
            this.btnAnaliseRisco.Margin = new System.Windows.Forms.Padding(4);
            this.btnAnaliseRisco.Name = "btnAnaliseRisco";
            this.btnAnaliseRisco.Size = new System.Drawing.Size(43, 26);
            this.btnAnaliseRisco.TabIndex = 86;
            this.toolTip1.SetToolTip(this.btnAnaliseRisco, "Código do produto que será gravado no movimento caso haja ocorrência de impressão" +
        " de segunda de impressão do atendimento.");
            this.btnAnaliseRisco.UseVisualStyleBackColor = true;
            this.btnAnaliseRisco.Click += new System.EventHandler(this.btnAnaliseRisco_Click);
            // 
            // textAnaliseRisco
            // 
            this.textAnaliseRisco.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.textAnaliseRisco.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textAnaliseRisco.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textAnaliseRisco.Location = new System.Drawing.Point(391, 1022);
            this.textAnaliseRisco.Margin = new System.Windows.Forms.Padding(4);
            this.textAnaliseRisco.MaxLength = 3;
            this.textAnaliseRisco.Name = "textAnaliseRisco";
            this.textAnaliseRisco.ReadOnly = true;
            this.textAnaliseRisco.Size = new System.Drawing.Size(566, 24);
            this.textAnaliseRisco.TabIndex = 87;
            this.textAnaliseRisco.TabStop = false;
            // 
            // frmConfiguracao
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1045, 690);
            this.Margin = new System.Windows.Forms.Padding(5, 5, 5, 5);
            this.Name = "frmConfiguracao";
            this.Text = "CONFIGURAÇÃO SISTEMA";
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.frmConfiguracao_KeyDown);
            this.spForm.Panel1.ResumeLayout(false);
            this.spForm.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.spForm)).EndInit();
            this.spForm.ResumeLayout(false);
            this.pnlForm.ResumeLayout(false);
            this.pnlForm.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.banner)).EndInit();
            this.flowLayoutPanel1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel1;
        private System.Windows.Forms.Button btnFechar;
        private System.Windows.Forms.TextBox textDiretorio;
        private System.Windows.Forms.TextBox lblDiretorio;
        private System.Windows.Forms.TextBox lblSenhaEmail;
        private System.Windows.Forms.TextBox lblEmailEnvio;
        private System.Windows.Forms.TextBox lblSSL;
        private System.Windows.Forms.TextBox lblNumeroPorta;
        private System.Windows.Forms.TextBox lblServidorSMTP;
        private System.Windows.Forms.TextBox lblEmailEmpresa;
        private System.Windows.Forms.TextBox lblInscricaoEmpresa;
        private System.Windows.Forms.TextBox lblCNPJEmpresa;
        private System.Windows.Forms.TextBox lblCEPEmpresa;
        private System.Windows.Forms.TextBox lblUFEmpresa;
        private System.Windows.Forms.TextBox lblBairroEmpresa;
        private System.Windows.Forms.TextBox lblComplementoEmpresa;
        private System.Windows.Forms.TextBox lblNumeroEmpresa;
        private System.Windows.Forms.TextBox lblEnderecoEmpresa;
        private System.Windows.Forms.TextBox lblNomeEmpresa;
        private System.Windows.Forms.TextBox lblCidadeEmpresa;
        private System.Windows.Forms.TextBox lblAsoInclusaoExames;
        private System.Windows.Forms.TextBox lblAsoTranscrito;
        private System.Windows.Forms.TextBox lblAso2Via;
        private System.Windows.Forms.TextBox lblProdutoAutomatico;
        private System.Windows.Forms.TextBox lblFlagMovimentoFinalizacaoExame;
        private System.Windows.Forms.TextBox lblSerialCliente;
        private System.Windows.Forms.TextBox lblNomeTermica;
        private System.Windows.Forms.TextBox lblServidorTermica;
        private System.Windows.Forms.TextBox lblNomeEmpresaTermica;
        private System.Windows.Forms.TextBox lblQtdeViasImpressoraTermica;
        private System.Windows.Forms.TextBox lblJurosMensalProrrogacaoCRCPercentual;
        private System.Windows.Forms.TextBox lblUltimaNFe;
        private System.Windows.Forms.TextBox textNomeEmpresaTermica;
        private System.Windows.Forms.TextBox textQtdeTermica;
        private System.Windows.Forms.TextBox textJurosProrrogacao;
        private System.Windows.Forms.TextBox textNumeroInicialNF;
        private System.Windows.Forms.TextBox textNomeImpressoraTermica;
        private System.Windows.Forms.MaskedTextBox textSenhaEmail;
        private System.Windows.Forms.TextBox textEmailEnvio;
        private ComboBoxWithBorder cbSSL;
        private System.Windows.Forms.TextBox textNumeroPorta;
        private System.Windows.Forms.TextBox textServidorSMTP;
        private System.Windows.Forms.TextBox textEmail;
        private System.Windows.Forms.TextBox textInscricao;
        private System.Windows.Forms.MaskedTextBox textCNPJ;
        private System.Windows.Forms.MaskedTextBox textCEP;
        private System.Windows.Forms.TextBox textUF;
        private System.Windows.Forms.TextBox textBairro;
        private System.Windows.Forms.TextBox textComplemento;
        private System.Windows.Forms.TextBox textNumero;
        private System.Windows.Forms.TextBox textEndereco;
        private System.Windows.Forms.TextBox textNomeEmpresa;
        private System.Windows.Forms.TextBox textCidade;
        private System.Windows.Forms.Button btnAsoInclusaoExame;
        private System.Windows.Forms.TextBox textAsoInclusaoExame;
        private System.Windows.Forms.Button btnAsoTranscrito;
        private System.Windows.Forms.TextBox textAsoTranscrito;
        private System.Windows.Forms.Button btnAso2Via;
        private System.Windows.Forms.TextBox textAso2Via;
        private ComboBoxWithBorder cbGravaProdutoAutomatico;
        private ComboBoxWithBorder cbGeraMovimentoExame;
        private System.Windows.Forms.TextBox textSerialCliente;
        private System.Windows.Forms.TextBox textIPServidorTermica;
        private System.Windows.Forms.ToolTip toolTip1;
        private ComboBoxWithBorder cbUtilizaFilial;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.TextBox textDiasPesquisa;
        private System.Windows.Forms.TextBox lblTempoPesquisaHistorico;
        private ComboBoxWithBorder cbImprimeSala;
        private System.Windows.Forms.TextBox lblImprimeSomenteSala;
        private ComboBoxWithBorder cbPcmsoVencido;
        private System.Windows.Forms.TextBox lblImprimirDadosAtendimentoPcmsoVencido;
        private ComboBoxWithBorder cbOrdenacaoAtendimento;
        private System.Windows.Forms.TextBox lblOrdenarAtendimentoPorSenha;
        private System.Windows.Forms.TextBox textVersaoSistema;
        private System.Windows.Forms.TextBox lblVersaoSistema;
        private ComboBoxWithBorder cbCancelaExameAtendimentoFinalizado;
        private System.Windows.Forms.TextBox lblCancelaAposFinalizado;
        private ComboBoxWithBorder cbAtendimentoBloqueado;
        private System.Windows.Forms.TextBox lblGravaAtendimentoBloqueado;
        private ComboBoxWithBorder cbMedicoExaminador;
        private System.Windows.Forms.TextBox lblObrigatorioGravarAtendimentoComMedicoExaminador;
        private System.Windows.Forms.TextBox lbdNumeroLicencas;
        private System.Windows.Forms.TextBox textNumeroLicencas;
        private ComboBoxWithBorder cbConverterEsocial2220;
        private System.Windows.Forms.TextBox lblCampoProcessoRealizadoInteiro;
        private System.Windows.Forms.TextBox textLocalntegracao;
        private System.Windows.Forms.TextBox lblCampoGracaoIntegracao;
        private System.Windows.Forms.Button btnAnaliseRisco;
        private System.Windows.Forms.TextBox textAnaliseRisco;
        private System.Windows.Forms.TextBox lblCodigoExameAnaliseRisco;
    }
}