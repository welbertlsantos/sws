﻿using SWS.Entidade;
using SWS.View.ViewHelper;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace SWS.View
{
    public partial class frmProdutoDetalhar : frmProdutoIncluir
    {
        public frmProdutoDetalhar(Produto produto) :base()
        {
            InitializeComponent();
            textNome.Text = produto.Nome;
            textPreco.Text = ValidaCampoHelper.FormataValorMonetario(produto.Preco.ToString());
            textCusto.Text = ValidaCampoHelper.FormataValorMonetario(produto.Custo.ToString());
            btnIncluir.Enabled = false;
            cbPadraoContrato.SelectedValue = produto.PadraoContrato == true ? "true" : "false";
            cbPadraoContrato.Enabled = false;
            textDescricao.Text = produto.Descricao;
            textDescricao.Enabled = false;
            textDescricao.ReadOnly = true;

        }
    }
}
