﻿namespace SWS.View
{
    partial class frmRelLaudoAtendimento
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmRelLaudoAtendimento));
            this.grbCliente = new System.Windows.Forms.GroupBox();
            this.btnPesquisaCliente = new System.Windows.Forms.Button();
            this.textCliente = new System.Windows.Forms.TextBox();
            this.grbPeriodo = new System.Windows.Forms.GroupBox();
            this.dataFinal = new System.Windows.Forms.DateTimePicker();
            this.lblPeriodo = new System.Windows.Forms.Label();
            this.dataInicial = new System.Windows.Forms.DateTimePicker();
            this.grbAcao = new System.Windows.Forms.GroupBox();
            this.btnFechar = new System.Windows.Forms.Button();
            this.btnImprime = new System.Windows.Forms.Button();
            this.grbSituacao = new System.Windows.Forms.GroupBox();
            this.rbTodos = new System.Windows.Forms.RadioButton();
            this.rbLaudados = new System.Windows.Forms.RadioButton();
            this.rbNaoLaudado = new System.Windows.Forms.RadioButton();
            this.grbOrdenacao = new System.Windows.Forms.GroupBox();
            this.rbAtendimentoExame = new System.Windows.Forms.RadioButton();
            this.rbAtendimento = new System.Windows.Forms.RadioButton();
            this.grbTipoRelatorio = new System.Windows.Forms.GroupBox();
            this.rbSintetico = new System.Windows.Forms.RadioButton();
            this.rbAnalitico = new System.Windows.Forms.RadioButton();
            this.grbCliente.SuspendLayout();
            this.grbPeriodo.SuspendLayout();
            this.grbAcao.SuspendLayout();
            this.grbSituacao.SuspendLayout();
            this.grbOrdenacao.SuspendLayout();
            this.grbTipoRelatorio.SuspendLayout();
            this.SuspendLayout();
            // 
            // grbCliente
            // 
            this.grbCliente.Controls.Add(this.btnPesquisaCliente);
            this.grbCliente.Controls.Add(this.textCliente);
            this.grbCliente.Location = new System.Drawing.Point(13, 13);
            this.grbCliente.Name = "grbCliente";
            this.grbCliente.Size = new System.Drawing.Size(575, 52);
            this.grbCliente.TabIndex = 0;
            this.grbCliente.TabStop = false;
            this.grbCliente.Text = "Cliente";
            // 
            // btnPesquisaCliente
            // 
            this.btnPesquisaCliente.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnPesquisaCliente.Image = global::SWS.Properties.Resources.lupa;
            this.btnPesquisaCliente.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnPesquisaCliente.Location = new System.Drawing.Point(484, 16);
            this.btnPesquisaCliente.Name = "btnPesquisaCliente";
            this.btnPesquisaCliente.Size = new System.Drawing.Size(75, 23);
            this.btnPesquisaCliente.TabIndex = 1;
            this.btnPesquisaCliente.Text = "&Buscar";
            this.btnPesquisaCliente.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnPesquisaCliente.UseVisualStyleBackColor = true;
            this.btnPesquisaCliente.Click += new System.EventHandler(this.btnPesquisaCliente_Click);
            // 
            // textCliente
            // 
            this.textCliente.BackColor = System.Drawing.Color.LightGray;
            this.textCliente.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textCliente.Location = new System.Drawing.Point(6, 19);
            this.textCliente.MaxLength = 100;
            this.textCliente.Name = "textCliente";
            this.textCliente.Size = new System.Drawing.Size(472, 20);
            this.textCliente.TabIndex = 0;
            this.textCliente.TabStop = false;
            // 
            // grbPeriodo
            // 
            this.grbPeriodo.Controls.Add(this.dataFinal);
            this.grbPeriodo.Controls.Add(this.lblPeriodo);
            this.grbPeriodo.Controls.Add(this.dataInicial);
            this.grbPeriodo.Location = new System.Drawing.Point(13, 72);
            this.grbPeriodo.Name = "grbPeriodo";
            this.grbPeriodo.Size = new System.Drawing.Size(255, 52);
            this.grbPeriodo.TabIndex = 1;
            this.grbPeriodo.TabStop = false;
            this.grbPeriodo.Text = "Período";
            // 
            // dataFinal
            // 
            this.dataFinal.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dataFinal.Location = new System.Drawing.Point(143, 19);
            this.dataFinal.Name = "dataFinal";
            this.dataFinal.Size = new System.Drawing.Size(102, 20);
            this.dataFinal.TabIndex = 2;
            // 
            // lblPeriodo
            // 
            this.lblPeriodo.AutoSize = true;
            this.lblPeriodo.Location = new System.Drawing.Point(114, 25);
            this.lblPeriodo.Name = "lblPeriodo";
            this.lblPeriodo.Size = new System.Drawing.Size(23, 13);
            this.lblPeriodo.TabIndex = 1;
            this.lblPeriodo.Text = "Até";
            // 
            // dataInicial
            // 
            this.dataInicial.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dataInicial.Location = new System.Drawing.Point(6, 19);
            this.dataInicial.Name = "dataInicial";
            this.dataInicial.Size = new System.Drawing.Size(102, 20);
            this.dataInicial.TabIndex = 0;
            // 
            // grbAcao
            // 
            this.grbAcao.Controls.Add(this.btnFechar);
            this.grbAcao.Controls.Add(this.btnImprime);
            this.grbAcao.Location = new System.Drawing.Point(13, 341);
            this.grbAcao.Name = "grbAcao";
            this.grbAcao.Size = new System.Drawing.Size(575, 52);
            this.grbAcao.TabIndex = 2;
            this.grbAcao.TabStop = false;
            this.grbAcao.Text = "Ações";
            // 
            // btnFechar
            // 
            this.btnFechar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnFechar.Image = global::SWS.Properties.Resources.close;
            this.btnFechar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnFechar.Location = new System.Drawing.Point(87, 19);
            this.btnFechar.Name = "btnFechar";
            this.btnFechar.Size = new System.Drawing.Size(75, 23);
            this.btnFechar.TabIndex = 1;
            this.btnFechar.Text = "&Fechar";
            this.btnFechar.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnFechar.UseVisualStyleBackColor = true;
            this.btnFechar.Click += new System.EventHandler(this.btnFechar_Click);
            // 
            // btnImprime
            // 
            this.btnImprime.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnImprime.Image = global::SWS.Properties.Resources.impressora1;
            this.btnImprime.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnImprime.Location = new System.Drawing.Point(6, 19);
            this.btnImprime.Name = "btnImprime";
            this.btnImprime.Size = new System.Drawing.Size(75, 23);
            this.btnImprime.TabIndex = 0;
            this.btnImprime.Text = "&Imprimir";
            this.btnImprime.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnImprime.UseVisualStyleBackColor = true;
            this.btnImprime.Click += new System.EventHandler(this.btnImprime_Click);
            // 
            // grbSituacao
            // 
            this.grbSituacao.Controls.Add(this.rbTodos);
            this.grbSituacao.Controls.Add(this.rbLaudados);
            this.grbSituacao.Controls.Add(this.rbNaoLaudado);
            this.grbSituacao.Location = new System.Drawing.Point(275, 72);
            this.grbSituacao.Name = "grbSituacao";
            this.grbSituacao.Size = new System.Drawing.Size(216, 96);
            this.grbSituacao.TabIndex = 3;
            this.grbSituacao.TabStop = false;
            this.grbSituacao.Text = "Situação";
            // 
            // rbTodos
            // 
            this.rbTodos.AutoSize = true;
            this.rbTodos.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.rbTodos.Location = new System.Drawing.Point(7, 65);
            this.rbTodos.Name = "rbTodos";
            this.rbTodos.Size = new System.Drawing.Size(182, 17);
            this.rbTodos.TabIndex = 2;
            this.rbTodos.Text = "Laudados + não laudados (todos)";
            this.rbTodos.UseVisualStyleBackColor = true;
            // 
            // rbLaudados
            // 
            this.rbLaudados.AutoSize = true;
            this.rbLaudados.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.rbLaudados.Location = new System.Drawing.Point(7, 42);
            this.rbLaudados.Name = "rbLaudados";
            this.rbLaudados.Size = new System.Drawing.Size(71, 17);
            this.rbLaudados.TabIndex = 1;
            this.rbLaudados.Text = "Laudados";
            this.rbLaudados.UseVisualStyleBackColor = true;
            // 
            // rbNaoLaudado
            // 
            this.rbNaoLaudado.AutoSize = true;
            this.rbNaoLaudado.Checked = true;
            this.rbNaoLaudado.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.rbNaoLaudado.Location = new System.Drawing.Point(7, 19);
            this.rbNaoLaudado.Name = "rbNaoLaudado";
            this.rbNaoLaudado.Size = new System.Drawing.Size(94, 17);
            this.rbNaoLaudado.TabIndex = 0;
            this.rbNaoLaudado.TabStop = true;
            this.rbNaoLaudado.Text = "Não Laudados";
            this.rbNaoLaudado.UseVisualStyleBackColor = true;
            // 
            // grbOrdenacao
            // 
            this.grbOrdenacao.Controls.Add(this.rbAtendimentoExame);
            this.grbOrdenacao.Controls.Add(this.rbAtendimento);
            this.grbOrdenacao.Location = new System.Drawing.Point(13, 214);
            this.grbOrdenacao.Name = "grbOrdenacao";
            this.grbOrdenacao.Size = new System.Drawing.Size(255, 67);
            this.grbOrdenacao.TabIndex = 4;
            this.grbOrdenacao.TabStop = false;
            this.grbOrdenacao.Text = "Ordenação";
            // 
            // rbAtendimentoExame
            // 
            this.rbAtendimentoExame.AutoSize = true;
            this.rbAtendimentoExame.Enabled = false;
            this.rbAtendimentoExame.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.rbAtendimentoExame.Location = new System.Drawing.Point(6, 19);
            this.rbAtendimentoExame.Name = "rbAtendimentoExame";
            this.rbAtendimentoExame.Size = new System.Drawing.Size(132, 17);
            this.rbAtendimentoExame.TabIndex = 1;
            this.rbAtendimentoExame.Text = "Atendimento + Exames";
            this.rbAtendimentoExame.UseVisualStyleBackColor = true;
            // 
            // rbAtendimento
            // 
            this.rbAtendimento.AutoSize = true;
            this.rbAtendimento.Checked = true;
            this.rbAtendimento.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.rbAtendimento.Location = new System.Drawing.Point(6, 42);
            this.rbAtendimento.Name = "rbAtendimento";
            this.rbAtendimento.Size = new System.Drawing.Size(83, 17);
            this.rbAtendimento.TabIndex = 0;
            this.rbAtendimento.TabStop = true;
            this.rbAtendimento.Text = "Atendimento";
            this.rbAtendimento.UseVisualStyleBackColor = true;
            // 
            // grbTipoRelatorio
            // 
            this.grbTipoRelatorio.Controls.Add(this.rbSintetico);
            this.grbTipoRelatorio.Controls.Add(this.rbAnalitico);
            this.grbTipoRelatorio.Location = new System.Drawing.Point(12, 130);
            this.grbTipoRelatorio.Name = "grbTipoRelatorio";
            this.grbTipoRelatorio.Size = new System.Drawing.Size(255, 78);
            this.grbTipoRelatorio.TabIndex = 5;
            this.grbTipoRelatorio.TabStop = false;
            this.grbTipoRelatorio.Text = "Tipo de relatório";
            // 
            // rbSintetico
            // 
            this.rbSintetico.AutoSize = true;
            this.rbSintetico.Checked = true;
            this.rbSintetico.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.rbSintetico.Location = new System.Drawing.Point(6, 19);
            this.rbSintetico.Name = "rbSintetico";
            this.rbSintetico.Size = new System.Drawing.Size(65, 17);
            this.rbSintetico.TabIndex = 1;
            this.rbSintetico.TabStop = true;
            this.rbSintetico.Text = "Sintético";
            this.rbSintetico.UseVisualStyleBackColor = true;
            this.rbSintetico.CheckedChanged += new System.EventHandler(this.rbSintetico_CheckedChanged);
            // 
            // rbAnalitico
            // 
            this.rbAnalitico.AutoSize = true;
            this.rbAnalitico.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.rbAnalitico.Location = new System.Drawing.Point(6, 42);
            this.rbAnalitico.Name = "rbAnalitico";
            this.rbAnalitico.Size = new System.Drawing.Size(66, 17);
            this.rbAnalitico.TabIndex = 0;
            this.rbAnalitico.Text = "Analítico";
            this.rbAnalitico.UseVisualStyleBackColor = true;
            // 
            // frmRelLaudoAtendimento
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(600, 400);
            this.ControlBox = false;
            this.Controls.Add(this.grbTipoRelatorio);
            this.Controls.Add(this.grbOrdenacao);
            this.Controls.Add(this.grbSituacao);
            this.Controls.Add(this.grbAcao);
            this.Controls.Add(this.grbPeriodo);
            this.Controls.Add(this.grbCliente);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "frmRelLaudoAtendimento";
            this.Text = "LAUDADOS POR ATENDIMENTO";
            this.grbCliente.ResumeLayout(false);
            this.grbCliente.PerformLayout();
            this.grbPeriodo.ResumeLayout(false);
            this.grbPeriodo.PerformLayout();
            this.grbAcao.ResumeLayout(false);
            this.grbSituacao.ResumeLayout(false);
            this.grbSituacao.PerformLayout();
            this.grbOrdenacao.ResumeLayout(false);
            this.grbOrdenacao.PerformLayout();
            this.grbTipoRelatorio.ResumeLayout(false);
            this.grbTipoRelatorio.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox grbCliente;
        private System.Windows.Forms.TextBox textCliente;
        private System.Windows.Forms.Button btnPesquisaCliente;
        private System.Windows.Forms.GroupBox grbPeriodo;
        private System.Windows.Forms.DateTimePicker dataFinal;
        private System.Windows.Forms.Label lblPeriodo;
        private System.Windows.Forms.DateTimePicker dataInicial;
        private System.Windows.Forms.GroupBox grbAcao;
        private System.Windows.Forms.Button btnFechar;
        private System.Windows.Forms.Button btnImprime;
        private System.Windows.Forms.GroupBox grbSituacao;
        private System.Windows.Forms.RadioButton rbTodos;
        private System.Windows.Forms.RadioButton rbLaudados;
        private System.Windows.Forms.RadioButton rbNaoLaudado;
        private System.Windows.Forms.GroupBox grbOrdenacao;
        private System.Windows.Forms.RadioButton rbAtendimentoExame;
        private System.Windows.Forms.RadioButton rbAtendimento;
        private System.Windows.Forms.GroupBox grbTipoRelatorio;
        private System.Windows.Forms.RadioButton rbSintetico;
        private System.Windows.Forms.RadioButton rbAnalitico;
    }
}