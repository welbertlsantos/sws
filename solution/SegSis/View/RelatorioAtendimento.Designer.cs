﻿namespace SWS.View
{
    partial class frmRelatorioAtendimento
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.flpAcao = new System.Windows.Forms.FlowLayoutPanel();
            this.btnImprimir = new System.Windows.Forms.Button();
            this.btnFechar = new System.Windows.Forms.Button();
            this.btnCliente = new System.Windows.Forms.Button();
            this.lblCliente = new System.Windows.Forms.TextBox();
            this.textCliente = new System.Windows.Forms.TextBox();
            this.lblPcmso = new System.Windows.Forms.TextBox();
            this.textPcmso = new System.Windows.Forms.TextBox();
            this.btnPcmso = new System.Windows.Forms.Button();
            this.lblExame = new System.Windows.Forms.TextBox();
            this.textExame = new System.Windows.Forms.TextBox();
            this.btnExame = new System.Windows.Forms.Button();
            this.lblPeriodo = new System.Windows.Forms.TextBox();
            this.dataInicial = new System.Windows.Forms.DateTimePicker();
            this.dataFinal = new System.Windows.Forms.DateTimePicker();
            this.lblSituacaoAtendimento = new System.Windows.Forms.TextBox();
            this.cbSituacaoAtendimento = new SWS.ComboBoxWithBorder();
            this.lblSituacaoExame = new System.Windows.Forms.TextBox();
            this.cbSituacaoExame = new SWS.ComboBoxWithBorder();
            this.lblTipoAtendimento = new System.Windows.Forms.TextBox();
            this.cbTipoAtendimento = new SWS.ComboBoxWithBorder();
            this.lblTipoRelatorio = new System.Windows.Forms.TextBox();
            this.cbTipoRelatorio = new SWS.ComboBoxWithBorder();
            this.btnExcluirCliente = new System.Windows.Forms.Button();
            this.btnExcluirPcmso = new System.Windows.Forms.Button();
            this.btnExcluirExame = new System.Windows.Forms.Button();
            this.lblCentroCusto = new System.Windows.Forms.TextBox();
            this.cbCentroCusto = new SWS.ComboBoxWithBorder();
            ((System.ComponentModel.ISupportInitialize)(this.scForm)).BeginInit();
            this.scForm.Panel1.SuspendLayout();
            this.scForm.Panel2.SuspendLayout();
            this.scForm.SuspendLayout();
            this.pnlForm.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.banner)).BeginInit();
            this.flpAcao.SuspendLayout();
            this.SuspendLayout();
            // 
            // scForm
            // 
            // 
            // scForm.Panel1
            // 
            this.scForm.Panel1.Controls.Add(this.flpAcao);
            this.scForm.SplitterDistance = 36;
            // 
            // pnlForm
            // 
            this.pnlForm.Controls.Add(this.cbCentroCusto);
            this.pnlForm.Controls.Add(this.lblCentroCusto);
            this.pnlForm.Controls.Add(this.btnExcluirExame);
            this.pnlForm.Controls.Add(this.btnExcluirPcmso);
            this.pnlForm.Controls.Add(this.btnExcluirCliente);
            this.pnlForm.Controls.Add(this.cbTipoRelatorio);
            this.pnlForm.Controls.Add(this.lblTipoRelatorio);
            this.pnlForm.Controls.Add(this.cbTipoAtendimento);
            this.pnlForm.Controls.Add(this.lblTipoAtendimento);
            this.pnlForm.Controls.Add(this.cbSituacaoExame);
            this.pnlForm.Controls.Add(this.lblSituacaoExame);
            this.pnlForm.Controls.Add(this.cbSituacaoAtendimento);
            this.pnlForm.Controls.Add(this.lblSituacaoAtendimento);
            this.pnlForm.Controls.Add(this.lblPeriodo);
            this.pnlForm.Controls.Add(this.dataInicial);
            this.pnlForm.Controls.Add(this.dataFinal);
            this.pnlForm.Controls.Add(this.btnExame);
            this.pnlForm.Controls.Add(this.textExame);
            this.pnlForm.Controls.Add(this.lblExame);
            this.pnlForm.Controls.Add(this.btnPcmso);
            this.pnlForm.Controls.Add(this.textPcmso);
            this.pnlForm.Controls.Add(this.lblPcmso);
            this.pnlForm.Controls.Add(this.textCliente);
            this.pnlForm.Controls.Add(this.btnCliente);
            this.pnlForm.Controls.Add(this.lblCliente);
            // 
            // flpAcao
            // 
            this.flpAcao.Controls.Add(this.btnImprimir);
            this.flpAcao.Controls.Add(this.btnFechar);
            this.flpAcao.Dock = System.Windows.Forms.DockStyle.Fill;
            this.flpAcao.Location = new System.Drawing.Point(0, 0);
            this.flpAcao.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.flpAcao.Name = "flpAcao";
            this.flpAcao.Size = new System.Drawing.Size(712, 36);
            this.flpAcao.TabIndex = 0;
            // 
            // btnImprimir
            // 
            this.btnImprimir.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnImprimir.Image = global::SWS.Properties.Resources.impressora2;
            this.btnImprimir.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnImprimir.Location = new System.Drawing.Point(4, 4);
            this.btnImprimir.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.btnImprimir.Name = "btnImprimir";
            this.btnImprimir.Size = new System.Drawing.Size(100, 28);
            this.btnImprimir.TabIndex = 0;
            this.btnImprimir.Text = "&Imprimir";
            this.btnImprimir.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnImprimir.UseVisualStyleBackColor = true;
            this.btnImprimir.Click += new System.EventHandler(this.btnImprimir_Click);
            // 
            // btnFechar
            // 
            this.btnFechar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnFechar.Image = global::SWS.Properties.Resources.close;
            this.btnFechar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnFechar.Location = new System.Drawing.Point(112, 4);
            this.btnFechar.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.btnFechar.Name = "btnFechar";
            this.btnFechar.Size = new System.Drawing.Size(100, 28);
            this.btnFechar.TabIndex = 1;
            this.btnFechar.Text = "&Fechar";
            this.btnFechar.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnFechar.UseVisualStyleBackColor = true;
            this.btnFechar.Click += new System.EventHandler(this.btnFechar_Click);
            // 
            // btnCliente
            // 
            this.btnCliente.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnCliente.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnCliente.Image = global::SWS.Properties.Resources.busca;
            this.btnCliente.Location = new System.Drawing.Point(575, 18);
            this.btnCliente.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.btnCliente.Name = "btnCliente";
            this.btnCliente.Size = new System.Drawing.Size(45, 26);
            this.btnCliente.TabIndex = 91;
            this.btnCliente.UseVisualStyleBackColor = true;
            this.btnCliente.Click += new System.EventHandler(this.btnCliente_Click);
            // 
            // lblCliente
            // 
            this.lblCliente.BackColor = System.Drawing.Color.LightGray;
            this.lblCliente.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblCliente.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCliente.Location = new System.Drawing.Point(16, 18);
            this.lblCliente.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.lblCliente.Name = "lblCliente";
            this.lblCliente.ReadOnly = true;
            this.lblCliente.Size = new System.Drawing.Size(123, 24);
            this.lblCliente.TabIndex = 92;
            this.lblCliente.TabStop = false;
            this.lblCliente.Text = "Cliente";
            // 
            // textCliente
            // 
            this.textCliente.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.textCliente.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textCliente.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textCliente.Location = new System.Drawing.Point(139, 18);
            this.textCliente.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.textCliente.Name = "textCliente";
            this.textCliente.ReadOnly = true;
            this.textCliente.Size = new System.Drawing.Size(437, 24);
            this.textCliente.TabIndex = 93;
            this.textCliente.TabStop = false;
            // 
            // lblPcmso
            // 
            this.lblPcmso.BackColor = System.Drawing.Color.LightGray;
            this.lblPcmso.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblPcmso.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPcmso.Location = new System.Drawing.Point(16, 68);
            this.lblPcmso.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.lblPcmso.Name = "lblPcmso";
            this.lblPcmso.ReadOnly = true;
            this.lblPcmso.Size = new System.Drawing.Size(123, 24);
            this.lblPcmso.TabIndex = 94;
            this.lblPcmso.TabStop = false;
            this.lblPcmso.Text = "PCMSO";
            // 
            // textPcmso
            // 
            this.textPcmso.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.textPcmso.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textPcmso.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textPcmso.Location = new System.Drawing.Point(139, 68);
            this.textPcmso.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.textPcmso.Name = "textPcmso";
            this.textPcmso.ReadOnly = true;
            this.textPcmso.Size = new System.Drawing.Size(437, 24);
            this.textPcmso.TabIndex = 95;
            this.textPcmso.TabStop = false;
            // 
            // btnPcmso
            // 
            this.btnPcmso.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnPcmso.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnPcmso.Image = global::SWS.Properties.Resources.busca;
            this.btnPcmso.Location = new System.Drawing.Point(575, 68);
            this.btnPcmso.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.btnPcmso.Name = "btnPcmso";
            this.btnPcmso.Size = new System.Drawing.Size(45, 26);
            this.btnPcmso.TabIndex = 96;
            this.btnPcmso.UseVisualStyleBackColor = true;
            this.btnPcmso.Click += new System.EventHandler(this.btnPcmso_Click);
            // 
            // lblExame
            // 
            this.lblExame.BackColor = System.Drawing.Color.LightGray;
            this.lblExame.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblExame.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblExame.Location = new System.Drawing.Point(16, 92);
            this.lblExame.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.lblExame.Name = "lblExame";
            this.lblExame.ReadOnly = true;
            this.lblExame.Size = new System.Drawing.Size(123, 24);
            this.lblExame.TabIndex = 97;
            this.lblExame.TabStop = false;
            this.lblExame.Text = "Exame";
            // 
            // textExame
            // 
            this.textExame.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.textExame.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textExame.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textExame.Location = new System.Drawing.Point(139, 92);
            this.textExame.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.textExame.Name = "textExame";
            this.textExame.ReadOnly = true;
            this.textExame.Size = new System.Drawing.Size(437, 24);
            this.textExame.TabIndex = 98;
            this.textExame.TabStop = false;
            // 
            // btnExame
            // 
            this.btnExame.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnExame.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnExame.Image = global::SWS.Properties.Resources.busca;
            this.btnExame.Location = new System.Drawing.Point(575, 92);
            this.btnExame.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.btnExame.Name = "btnExame";
            this.btnExame.Size = new System.Drawing.Size(45, 26);
            this.btnExame.TabIndex = 99;
            this.btnExame.UseVisualStyleBackColor = true;
            this.btnExame.Click += new System.EventHandler(this.btnExame_Click);
            // 
            // lblPeriodo
            // 
            this.lblPeriodo.BackColor = System.Drawing.Color.LightGray;
            this.lblPeriodo.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblPeriodo.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPeriodo.Location = new System.Drawing.Point(16, 117);
            this.lblPeriodo.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.lblPeriodo.Name = "lblPeriodo";
            this.lblPeriodo.ReadOnly = true;
            this.lblPeriodo.Size = new System.Drawing.Size(123, 24);
            this.lblPeriodo.TabIndex = 100;
            this.lblPeriodo.TabStop = false;
            this.lblPeriodo.Text = "Período";
            // 
            // dataInicial
            // 
            this.dataInicial.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dataInicial.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dataInicial.Location = new System.Drawing.Point(139, 117);
            this.dataInicial.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.dataInicial.Name = "dataInicial";
            this.dataInicial.ShowCheckBox = true;
            this.dataInicial.Size = new System.Drawing.Size(141, 24);
            this.dataInicial.TabIndex = 101;
            // 
            // dataFinal
            // 
            this.dataFinal.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dataFinal.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dataFinal.Location = new System.Drawing.Point(277, 117);
            this.dataFinal.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.dataFinal.Name = "dataFinal";
            this.dataFinal.ShowCheckBox = true;
            this.dataFinal.Size = new System.Drawing.Size(141, 24);
            this.dataFinal.TabIndex = 102;
            // 
            // lblSituacaoAtendimento
            // 
            this.lblSituacaoAtendimento.BackColor = System.Drawing.Color.LightGray;
            this.lblSituacaoAtendimento.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblSituacaoAtendimento.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSituacaoAtendimento.Location = new System.Drawing.Point(16, 142);
            this.lblSituacaoAtendimento.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.lblSituacaoAtendimento.Name = "lblSituacaoAtendimento";
            this.lblSituacaoAtendimento.ReadOnly = true;
            this.lblSituacaoAtendimento.Size = new System.Drawing.Size(123, 24);
            this.lblSituacaoAtendimento.TabIndex = 103;
            this.lblSituacaoAtendimento.TabStop = false;
            this.lblSituacaoAtendimento.Text = "Sit. Atend";
            // 
            // cbSituacaoAtendimento
            // 
            this.cbSituacaoAtendimento.BackColor = System.Drawing.Color.LightGray;
            this.cbSituacaoAtendimento.BorderColor = System.Drawing.Color.DimGray;
            this.cbSituacaoAtendimento.BorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid;
            this.cbSituacaoAtendimento.FormattingEnabled = true;
            this.cbSituacaoAtendimento.Location = new System.Drawing.Point(139, 142);
            this.cbSituacaoAtendimento.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.cbSituacaoAtendimento.Name = "cbSituacaoAtendimento";
            this.cbSituacaoAtendimento.Size = new System.Drawing.Size(520, 24);
            this.cbSituacaoAtendimento.TabIndex = 104;
            // 
            // lblSituacaoExame
            // 
            this.lblSituacaoExame.BackColor = System.Drawing.Color.LightGray;
            this.lblSituacaoExame.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblSituacaoExame.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSituacaoExame.Location = new System.Drawing.Point(16, 166);
            this.lblSituacaoExame.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.lblSituacaoExame.Name = "lblSituacaoExame";
            this.lblSituacaoExame.ReadOnly = true;
            this.lblSituacaoExame.Size = new System.Drawing.Size(123, 24);
            this.lblSituacaoExame.TabIndex = 105;
            this.lblSituacaoExame.TabStop = false;
            this.lblSituacaoExame.Text = "Situação Exame";
            // 
            // cbSituacaoExame
            // 
            this.cbSituacaoExame.BackColor = System.Drawing.Color.LightGray;
            this.cbSituacaoExame.BorderColor = System.Drawing.Color.DimGray;
            this.cbSituacaoExame.BorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid;
            this.cbSituacaoExame.FormattingEnabled = true;
            this.cbSituacaoExame.Location = new System.Drawing.Point(139, 166);
            this.cbSituacaoExame.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.cbSituacaoExame.Name = "cbSituacaoExame";
            this.cbSituacaoExame.Size = new System.Drawing.Size(520, 24);
            this.cbSituacaoExame.TabIndex = 106;
            // 
            // lblTipoAtendimento
            // 
            this.lblTipoAtendimento.BackColor = System.Drawing.Color.LightGray;
            this.lblTipoAtendimento.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblTipoAtendimento.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTipoAtendimento.Location = new System.Drawing.Point(16, 191);
            this.lblTipoAtendimento.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.lblTipoAtendimento.Name = "lblTipoAtendimento";
            this.lblTipoAtendimento.ReadOnly = true;
            this.lblTipoAtendimento.Size = new System.Drawing.Size(123, 24);
            this.lblTipoAtendimento.TabIndex = 107;
            this.lblTipoAtendimento.TabStop = false;
            this.lblTipoAtendimento.Text = "Tipo Atend.";
            // 
            // cbTipoAtendimento
            // 
            this.cbTipoAtendimento.BackColor = System.Drawing.Color.LightGray;
            this.cbTipoAtendimento.BorderColor = System.Drawing.Color.DimGray;
            this.cbTipoAtendimento.BorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid;
            this.cbTipoAtendimento.FormattingEnabled = true;
            this.cbTipoAtendimento.Location = new System.Drawing.Point(139, 191);
            this.cbTipoAtendimento.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.cbTipoAtendimento.Name = "cbTipoAtendimento";
            this.cbTipoAtendimento.Size = new System.Drawing.Size(520, 24);
            this.cbTipoAtendimento.TabIndex = 108;
            // 
            // lblTipoRelatorio
            // 
            this.lblTipoRelatorio.BackColor = System.Drawing.Color.LightGray;
            this.lblTipoRelatorio.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblTipoRelatorio.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTipoRelatorio.Location = new System.Drawing.Point(16, 215);
            this.lblTipoRelatorio.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.lblTipoRelatorio.Name = "lblTipoRelatorio";
            this.lblTipoRelatorio.ReadOnly = true;
            this.lblTipoRelatorio.Size = new System.Drawing.Size(123, 24);
            this.lblTipoRelatorio.TabIndex = 109;
            this.lblTipoRelatorio.TabStop = false;
            this.lblTipoRelatorio.Text = "Tipo Rel.";
            // 
            // cbTipoRelatorio
            // 
            this.cbTipoRelatorio.BackColor = System.Drawing.Color.LightGray;
            this.cbTipoRelatorio.BorderColor = System.Drawing.Color.DimGray;
            this.cbTipoRelatorio.BorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid;
            this.cbTipoRelatorio.FormattingEnabled = true;
            this.cbTipoRelatorio.Location = new System.Drawing.Point(139, 215);
            this.cbTipoRelatorio.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.cbTipoRelatorio.Name = "cbTipoRelatorio";
            this.cbTipoRelatorio.Size = new System.Drawing.Size(520, 24);
            this.cbTipoRelatorio.TabIndex = 110;
            this.cbTipoRelatorio.SelectedIndexChanged += new System.EventHandler(this.cbTipoRelatorio_SelectedIndexChanged);
            // 
            // btnExcluirCliente
            // 
            this.btnExcluirCliente.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnExcluirCliente.Image = global::SWS.Properties.Resources.close;
            this.btnExcluirCliente.Location = new System.Drawing.Point(619, 18);
            this.btnExcluirCliente.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.btnExcluirCliente.Name = "btnExcluirCliente";
            this.btnExcluirCliente.Size = new System.Drawing.Size(40, 26);
            this.btnExcluirCliente.TabIndex = 111;
            this.btnExcluirCliente.UseVisualStyleBackColor = true;
            this.btnExcluirCliente.Click += new System.EventHandler(this.btnExcluirCliente_Click);
            // 
            // btnExcluirPcmso
            // 
            this.btnExcluirPcmso.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnExcluirPcmso.Image = global::SWS.Properties.Resources.close;
            this.btnExcluirPcmso.Location = new System.Drawing.Point(619, 68);
            this.btnExcluirPcmso.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.btnExcluirPcmso.Name = "btnExcluirPcmso";
            this.btnExcluirPcmso.Size = new System.Drawing.Size(40, 26);
            this.btnExcluirPcmso.TabIndex = 112;
            this.btnExcluirPcmso.UseVisualStyleBackColor = true;
            this.btnExcluirPcmso.Click += new System.EventHandler(this.btnExcluirPcmso_Click);
            // 
            // btnExcluirExame
            // 
            this.btnExcluirExame.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnExcluirExame.Image = global::SWS.Properties.Resources.close;
            this.btnExcluirExame.Location = new System.Drawing.Point(619, 92);
            this.btnExcluirExame.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.btnExcluirExame.Name = "btnExcluirExame";
            this.btnExcluirExame.Size = new System.Drawing.Size(40, 26);
            this.btnExcluirExame.TabIndex = 113;
            this.btnExcluirExame.UseVisualStyleBackColor = true;
            this.btnExcluirExame.Click += new System.EventHandler(this.btnExcluirExame_Click);
            // 
            // lblCentroCusto
            // 
            this.lblCentroCusto.BackColor = System.Drawing.Color.LightGray;
            this.lblCentroCusto.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblCentroCusto.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCentroCusto.Location = new System.Drawing.Point(16, 43);
            this.lblCentroCusto.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.lblCentroCusto.Name = "lblCentroCusto";
            this.lblCentroCusto.ReadOnly = true;
            this.lblCentroCusto.Size = new System.Drawing.Size(123, 24);
            this.lblCentroCusto.TabIndex = 114;
            this.lblCentroCusto.TabStop = false;
            this.lblCentroCusto.Text = "Centro de Custo";
            // 
            // cbCentroCusto
            // 
            this.cbCentroCusto.BackColor = System.Drawing.Color.LightGray;
            this.cbCentroCusto.BorderColor = System.Drawing.Color.DimGray;
            this.cbCentroCusto.BorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid;
            this.cbCentroCusto.Enabled = false;
            this.cbCentroCusto.FormattingEnabled = true;
            this.cbCentroCusto.Location = new System.Drawing.Point(139, 43);
            this.cbCentroCusto.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.cbCentroCusto.Name = "cbCentroCusto";
            this.cbCentroCusto.Size = new System.Drawing.Size(519, 24);
            this.cbCentroCusto.TabIndex = 115;
            // 
            // frmRelatorioAtendimento
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(712, 444);
            this.Margin = new System.Windows.Forms.Padding(5, 5, 5, 5);
            this.Name = "frmRelatorioAtendimento";
            this.Text = "RELATÓRIO DE ATENDIMENTO";
            this.scForm.Panel1.ResumeLayout(false);
            this.scForm.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.scForm)).EndInit();
            this.scForm.ResumeLayout(false);
            this.pnlForm.ResumeLayout(false);
            this.pnlForm.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.banner)).EndInit();
            this.flpAcao.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.FlowLayoutPanel flpAcao;
        private System.Windows.Forms.Button btnImprimir;
        private System.Windows.Forms.Button btnFechar;
        protected System.Windows.Forms.TextBox lblCliente;
        private System.Windows.Forms.TextBox textCliente;
        private System.Windows.Forms.TextBox textPcmso;
        protected System.Windows.Forms.TextBox lblPcmso;
        protected System.Windows.Forms.TextBox lblExame;
        private System.Windows.Forms.TextBox textExame;
        private System.Windows.Forms.TextBox lblPeriodo;
        private System.Windows.Forms.DateTimePicker dataInicial;
        private System.Windows.Forms.DateTimePicker dataFinal;
        private System.Windows.Forms.TextBox lblSituacaoAtendimento;
        private System.Windows.Forms.TextBox lblSituacaoExame;
        private ComboBoxWithBorder cbSituacaoAtendimento;
        private ComboBoxWithBorder cbSituacaoExame;
        private System.Windows.Forms.TextBox lblTipoAtendimento;
        private ComboBoxWithBorder cbTipoRelatorio;
        private System.Windows.Forms.TextBox lblTipoRelatorio;
        private ComboBoxWithBorder cbTipoAtendimento;
        private System.Windows.Forms.Button btnExcluirExame;
        private System.Windows.Forms.Button btnExcluirPcmso;
        private System.Windows.Forms.Button btnExcluirCliente;
        private System.Windows.Forms.Button btnCliente;
        private System.Windows.Forms.Button btnPcmso;
        private System.Windows.Forms.Button btnExame;
        private ComboBoxWithBorder cbCentroCusto;
        protected System.Windows.Forms.TextBox lblCentroCusto;
    }
}