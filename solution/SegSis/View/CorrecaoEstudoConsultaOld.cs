﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using SWS.Entidade;
using SWS.Facade;

namespace SWS.View
{
    public partial class frm_CorrecaoEstudoConsultaOld : BaseFormConsulta
    {
        private Estudo estudo = null;
        
        public frm_CorrecaoEstudoConsultaOld(Estudo estudo)
        {
            InitializeComponent();
            this.estudo = estudo;
            montaGrid();
        }

        public void montaGrid()
        {
            try
            {
                this.Cursor = Cursors.WaitCursor;

                LogFacade logFacade = LogFacade.getInstance();

                dgCorrecao.Columns.Clear();

                DataSet ds = logFacade.findAllCorrecaoEstudoByEstudo(estudo);

                dgCorrecao.DataSource = ds.Tables[0].DefaultView;

                dgCorrecao.Columns[0].HeaderText = "Id";
                dgCorrecao.Columns[0].Visible = false;

                dgCorrecao.Columns[1].HeaderText = "Id Usuario";
                dgCorrecao.Columns[1].Visible = false;

                dgCorrecao.Columns[2].HeaderText = "Usuario";
                dgCorrecao.Columns[2].DefaultCellStyle.ForeColor = Color.Red;

                dgCorrecao.Columns[3].HeaderText = "Id Estudo";
                dgCorrecao.Columns[3].Visible = false;

                dgCorrecao.Columns[4].HeaderText = "Histórico";

                dgCorrecao.Columns[5].HeaderText = "Data Gravação";


            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }

        }

        private void btnFechar_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
