﻿namespace SWS.View
{
    partial class frm_CorrecaoEstudo
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frm_CorrecaoEstudo));
            this.grbMotivo = new System.Windows.Forms.GroupBox();
            this.textHistorico = new System.Windows.Forms.TextBox();
            this.tpCorrecao = new System.Windows.Forms.ToolTip(this.components);
            this.grbAcao = new System.Windows.Forms.GroupBox();
            this.btnGravar = new System.Windows.Forms.Button();
            this.btnFechar = new System.Windows.Forms.Button();
            this.grbMotivo.SuspendLayout();
            this.grbAcao.SuspendLayout();
            this.SuspendLayout();
            // 
            // grbMotivo
            // 
            this.grbMotivo.Controls.Add(this.textHistorico);
            this.grbMotivo.Location = new System.Drawing.Point(13, 13);
            this.grbMotivo.Name = "grbMotivo";
            this.grbMotivo.Size = new System.Drawing.Size(661, 178);
            this.grbMotivo.TabIndex = 0;
            this.grbMotivo.TabStop = false;
            this.grbMotivo.Text = "Justificativa de Correção";
            // 
            // textHistorico
            // 
            this.textHistorico.BackColor = System.Drawing.Color.LightSteelBlue;
            this.textHistorico.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textHistorico.Dock = System.Windows.Forms.DockStyle.Fill;
            this.textHistorico.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textHistorico.Location = new System.Drawing.Point(3, 16);
            this.textHistorico.Multiline = true;
            this.textHistorico.Name = "textHistorico";
            this.textHistorico.Size = new System.Drawing.Size(655, 159);
            this.textHistorico.TabIndex = 0;
            this.tpCorrecao.SetToolTip(this.textHistorico, "O motivo da correção deverá ser informado. Use esse campo\r\ninformando o que está " +
                    "sendo corrigido no estudo.");
            // 
            // tpCorrecao
            // 
            this.tpCorrecao.ToolTipIcon = System.Windows.Forms.ToolTipIcon.Info;
            // 
            // grbAcao
            // 
            this.grbAcao.Controls.Add(this.btnFechar);
            this.grbAcao.Controls.Add(this.btnGravar);
            this.grbAcao.Location = new System.Drawing.Point(12, 197);
            this.grbAcao.Name = "grbAcao";
            this.grbAcao.Size = new System.Drawing.Size(662, 53);
            this.grbAcao.TabIndex = 1;
            this.grbAcao.TabStop = false;
            this.grbAcao.Text = "Ação";
            // 
            // btnGravar
            // 
            this.btnGravar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnGravar.Image = global::SWS.Properties.Resources.Gravar;
            this.btnGravar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnGravar.Location = new System.Drawing.Point(6, 19);
            this.btnGravar.Name = "btnGravar";
            this.btnGravar.Size = new System.Drawing.Size(75, 23);
            this.btnGravar.TabIndex = 0;
            this.btnGravar.Text = "&Gravar";
            this.btnGravar.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnGravar.UseVisualStyleBackColor = true;
            this.btnGravar.Click += new System.EventHandler(this.btnGravar_Click);
            // 
            // btnFechar
            // 
            this.btnFechar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnFechar.Image = global::SWS.Properties.Resources.close;
            this.btnFechar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnFechar.Location = new System.Drawing.Point(87, 19);
            this.btnFechar.Name = "btnFechar";
            this.btnFechar.Size = new System.Drawing.Size(75, 23);
            this.btnFechar.TabIndex = 1;
            this.btnFechar.Text = "&Fechar";
            this.btnFechar.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnFechar.UseVisualStyleBackColor = true;
            this.btnFechar.Click += new System.EventHandler(this.btnFechar_Click);
            // 
            // frm_CorrecaoEstudo
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(686, 262);
            this.ControlBox = false;
            this.Controls.Add(this.grbAcao);
            this.Controls.Add(this.grbMotivo);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.KeyPreview = true;
            this.Name = "frm_CorrecaoEstudo";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "MOTIVO DE CORREÇÃO DO ESTUDO";
            this.grbMotivo.ResumeLayout(false);
            this.grbMotivo.PerformLayout();
            this.grbAcao.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox grbMotivo;
        private System.Windows.Forms.TextBox textHistorico;
        private System.Windows.Forms.ToolTip tpCorrecao;
        private System.Windows.Forms.GroupBox grbAcao;
        private System.Windows.Forms.Button btnFechar;
        private System.Windows.Forms.Button btnGravar;
    }
}