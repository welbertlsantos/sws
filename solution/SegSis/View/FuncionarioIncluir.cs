﻿using SWS.Entidade;
using SWS.Facade;
using SWS.View.Resources;
using SWS.View.ViewHelper;
using SWS.ViewHelper;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace SWS.View
{
    public partial class frmFuncionarioIncluir : frmTemplate
    {

        protected Funcionario funcionario;

        public Funcionario Funcionario
        {
            get { return funcionario; }
            set { funcionario = value; }
        }

        protected CidadeIbge cidade = null;

        protected List<ClienteFuncaoFuncionario> clienteFuncaoFuncionarioList = new List<ClienteFuncaoFuncionario>();

        public List<ClienteFuncaoFuncionario> ClienteFuncaoFuncionario
        {
            get { return clienteFuncaoFuncionarioList; }
            set { clienteFuncaoFuncionarioList = value; }
        }
        
        
        public frmFuncionarioIncluir()
        {
            InitializeComponent();
            /* carregando combos */
            ComboHelper.startSexoIncluir(cbSexo);
            ComboHelper.TipoSague(cbTipoSanguineo);
            ComboHelper.FatorRH(cbFatorRh);
            ComboHelper.unidadeFederativa(cbUF);
            ActiveControl = textNome;
            ComboHelper.Boolean(cbPcd, false);
            ComboHelper.unidadeFederativa(cbUfEmissor);
            ComboHelper.unidadeFederativa(cbUfEmissorCtps);
        }

        protected virtual void btGravar_Click(object sender, EventArgs e)
        {
            try
            {
                if (!validaDadosTela())
                    throw new Exception("Verificar dados digitados.");

                /* preparando os dados do funcionário */
                funcionario = new Funcionario(null, textNome.Text.Trim(), textCpf.Text, textRg.Text, textLogradouro.Text.Trim(), textNumero.Text.Trim(), textComplemento.Text.Trim(), textBairro.Text.Trim(), cidade, (((SelectItem)cbUF.SelectedItem).Valor).ToString(), textTelefone.Text.Trim(), textCelular.Text.Trim(), textEmail.Text, (((SelectItem)cbTipoSanguineo.SelectedItem).Valor).ToString(), (((SelectItem)cbFatorRh.SelectedItem).Valor).ToString(), Convert.ToDateTime(dataNascimento.Text), String.IsNullOrEmpty(textPeso.Text) ? 0 : Convert.ToInt32(textPeso.Text), String.IsNullOrEmpty(textAltura.Text) ? 0 : Convert.ToDecimal(textAltura.Text), ApplicationConstants.ATIVO, textCEP.Text, (((SelectItem)cbSexo.SelectedItem).Valor).ToString(), textCtps.Text.Trim(), textPis.Text.Trim(), Convert.ToBoolean(((SelectItem)cbPcd.SelectedItem).Valor), textOrgaoEmissor.Text, ((SelectItem)cbUfEmissor.SelectedItem).Valor, textSerieCtps.Text.Trim(), ((SelectItem)cbUfEmissorCtps.SelectedItem).Valor);

                /* verificando se o clienteFuncionario informado é estagiário. Nesse caso 
                 * a obrigação do cadastro do pis não e necessário */

                int quantidadeEstagiario = 0;

                clienteFuncaoFuncionarioList.ForEach(delegate(ClienteFuncaoFuncionario clienteFuncaoFuncionarioProcura)
                {
                    if (clienteFuncaoFuncionarioProcura.Estagiario == true)
                    {
                        quantidadeEstagiario++;
                    }
                });

                if ((quantidadeEstagiario != clienteFuncaoFuncionarioList.Count) && string.IsNullOrEmpty(funcionario.PisPasep))
                    throw new Exception("O campo Pis é obrigatório porque o funcionário está relacionado a um cliente");

                funcionario.ClienteFuncaoFuncionario = clienteFuncaoFuncionarioList;

                FuncionarioFacade funcionarioFacade = FuncionarioFacade.getInstance();

                funcionario = funcionarioFacade.incluirFuncionario(funcionario);

                MessageBox.Show("Funcionário incluído com sucesso.", "Confirmação", MessageBoxButtons.OK, MessageBoxIcon.Information);

                this.Close();

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        protected void btImprimir_Click(object sender, EventArgs e)
        {
            try
            {
                throw new Exception("Em construção!");
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }

        protected void btLimpar_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Deseja apagar todos os dados da tela?", "Atenção", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
            {
                textNome.Text = String.Empty;
                textCpf.Text = String.Empty;
                textRg.Text = String.Empty;
                dataNascimento.Checked = false;
                ComboHelper.startSexoIncluir(cbSexo);
                textEmail.Text = String.Empty;
                ComboHelper.TipoSague(cbTipoSanguineo);
                ComboHelper.FatorRH(cbFatorRh);
                textPeso.Text = String.Empty;
                textAltura.Text = String.Empty;
                textCtps.Text = String.Empty;
                textPis.Text = String.Empty;

                textLogradouro.Text = String.Empty;
                textNumero.Text = String.Empty;
                textComplemento.Text = String.Empty;
                textBairro.Text = String.Empty;
                textCEP.Text = String.Empty;
                ComboHelper.unidadeFederativa(cbUF);
                cidade = null;
                textCidade.Text = String.Empty;
                textTelefone.Text = String.Empty;
                textCelular.Text = String.Empty;

                dgvCliente.Rows.Clear();
                clienteFuncaoFuncionarioList.Clear();

                dgvFuncao.Rows.Clear();

                textOrgaoEmissor.Text = string.Empty;
                ComboHelper.unidadeFederativa(cbUfEmissor);

                textNome.Focus();
            }
        }

        protected void btFechar_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        protected void btCidadeComercial_Click(object sender, EventArgs e)
        {
            try
            {
                if (cbUF.SelectedIndex == 0)
                    throw new Exception("Você deve primeiro selecionar a unidade federativa.");

                frmCidadePesquisa cidadeSeleciona = new frmCidadePesquisa(((SelectItem)cbUF.SelectedItem).Valor.ToString());
                cidadeSeleciona.ShowDialog();

                if (cidadeSeleciona.Cidade != null)
                {
                    cidade = cidadeSeleciona.Cidade;
                    textCidade.Text = cidade.Nome;
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        protected virtual void btIncluirCliente_Click(object sender, EventArgs e)
        {
            try
            {
                Cliente cliente = null;
                ClienteFuncao clienteFuncao = null;

                frmClienteSelecionar selecionarCliente = new frmClienteSelecionar(null, null, false, null, null, null, null, null, true);
                selecionarCliente.ShowDialog();

                if (selecionarCliente.Cliente != null)
                {
                    cliente = selecionarCliente.Cliente;

                    /* buscando o cliente Função */

                    frmClienteFuncaoSeleciona selecionarClienteFuncao = new frmClienteFuncaoSeleciona(cliente);
                    selecionarClienteFuncao.ShowDialog();

                    if (selecionarClienteFuncao.ClienteFuncao != null)
                    {
                        clienteFuncao = selecionarClienteFuncao.ClienteFuncao;

                        /* solicitando informação da função e do cliente função funcionário*/

                        frmClienteFuncaoFuncionarioIncluir selecionarClienteFuncaoFuncionario = new frmClienteFuncaoFuncionarioIncluir(clienteFuncao);
                        selecionarClienteFuncaoFuncionario.ShowDialog();

                        if (selecionarClienteFuncaoFuncionario.ClienteFuncaoFuncionario != null)
                        {
                            clienteFuncaoFuncionarioList.Add(selecionarClienteFuncaoFuncionario.ClienteFuncaoFuncionario);
                            
                            montaGridCliente();
                            montaGridFuncao();

                        }
                    }
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        protected virtual void btAlterarCliente_Click(object sender, EventArgs e)
        {
            try
            {
                if (dgvCliente.CurrentRow == null)
                    throw new Exception("Selecione uma linha.");

                frmClienteFuncaoFuncionarioAlterar alterarFuncionarioCliente = new frmClienteFuncaoFuncionarioAlterar(clienteFuncaoFuncionarioList.Find(x => x.ClienteFuncao.Cliente.Id == (long)dgvCliente.CurrentRow.Cells[1].Value));
                alterarFuncionarioCliente.ShowDialog();

                montaGridCliente();

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        protected virtual void btExcluirCliente_Click(object sender, EventArgs e)
        {
            try
            {
                if (dgvCliente.CurrentRow == null)
                    throw new Exception("Você deve selecionar o cliente primeiro");

                if (MessageBox.Show("Você deseja remover o cliente selecionado", "Confirmação", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                {
                    Int64 idCliente = (Int64)dgvCliente.CurrentRow.Cells[1].Value;

                    /* excluindo a função do cliente */

                    int index = clienteFuncaoFuncionarioList.FindIndex(x => x.ClienteFuncao.Cliente.Id == idCliente);
                    clienteFuncaoFuncionarioList.RemoveAt(index);
                    montaGridCliente();
                    montaGridFuncao();

                }

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        protected Boolean validaDadosTela()
        {
            Boolean valida = true;
            epFuncionarioIncluir.Clear();

            /* validando campos obrigatórios */

            if (String.IsNullOrEmpty(textNome.Text.Trim()))
            {
                epFuncionarioIncluir.SetError(textNome, "Campo nome obrigatório.");
                valida = false;
            }

            if (String.IsNullOrEmpty(textRg.Text.Trim()))
            {
                epFuncionarioIncluir.SetError(textRg, "Campo Documento de Identidade obrigatório.");
                valida = false;
            }

            if (String.IsNullOrEmpty(textCpf.Text.Replace(".", "").Replace(",", "").Replace("-", "").Trim()))
            {
                epFuncionarioIncluir.SetError(textCpf, "O campo CPF é obrigatório.");
                valida = false;
            }

            if (String.IsNullOrEmpty(textOrgaoEmissor.Text.Trim()))
            {
                epFuncionarioIncluir.SetError(textOrgaoEmissor, "O Emissor do documento de identidade é obrigatório.");
                valida = false;
            }

            if (cbUfEmissor.SelectedIndex == -1)
            {
                epFuncionarioIncluir.SetError(cbUfEmissor, "A unidade federativa do emissor do documento de identificação é obrigatória.");
                valida = false;
            }

            if (cbSexo.SelectedIndex == -1)
            {
                epFuncionarioIncluir.SetError(cbSexo, "Sexo do funcionário é obrigatório.");
                valida = false;
            }

            if (!dataNascimento.Checked)
            {
                epFuncionarioIncluir.SetError(dataNascimento, "Data de Nascimento obrigatória.");
                valida = false;
            }

            if (clienteFuncaoFuncionarioList.Count == 0)
            {
                epFuncionarioIncluir.SetError(grbCliente, "O funcionário deve ter pelo menos um cliente relacionado.");
                valida = false;
            }

            return valida;

        }

        protected void FuncionarioIncluir_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter || e.KeyCode == Keys.Return)
                SendKeys.Send("{TAB}");

        }

        protected void textCpf_Leave(object sender, EventArgs e)
        {
            try
            {
                if (!String.IsNullOrEmpty(textCpf.Text.Replace(".", "").Replace("-", "").Trim()))
                {
                    if (!ValidaCampoHelper.ValidaCPF(textCpf.Text))
                    {
                        throw new Exception("CPF digitado é inválido.");
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
                textCpf.Focus();
                textCpf.Select(0, 0);
            }
        }

        protected void textPeso_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = ValidaCampoHelper.FormataCampoNumerico(sender, e);
        }

        protected void textAltura_Enter(object sender, EventArgs e)
        {
            textAltura.Tag = textAltura.Text;
            textAltura.Text = String.Empty;
        }

        protected void textAltura_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = ValidaCampoHelper.FormataDigitacaoNumero(sender, e, textAltura.Text, 2);
        }

        protected void textAltura_Leave(object sender, EventArgs e)
        {
            if (String.IsNullOrEmpty(textAltura.Text))
                textAltura.Text = ValidaCampoHelper.FormataValorMonetario(textAltura.Tag.ToString());
            else
                textAltura.Text = ValidaCampoHelper.FormataValorMonetario(textAltura.Text);
        }

        protected void textEmail_Leave(object sender, EventArgs e)
        {
            try
            {
                if (!String.IsNullOrEmpty(textEmail.Text.Trim()))
                {
                    if (!ValidaCampoHelper.ValidaEnderecoEmail(textEmail.Text))
                        throw new Exception("O e-mail digitado é inválido");
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
                textEmail.Focus();
                textEmail.Select(0, 0);
            }
        }

        protected void montaGridCliente()
        {
            try
            {
                this.Cursor = Cursors.WaitCursor;

                dgvCliente.Columns.Clear();
                dgvCliente.ColumnCount = 5;

                dgvCliente.Columns[0].HeaderText = "id_cliente_funcao_funcionario";
                dgvCliente.Columns[0].Visible = false;
                dgvCliente.Columns[1].HeaderText = "id_cliente";
                dgvCliente.Columns[1].Visible = false;
                dgvCliente.Columns[2].HeaderText = "Razão Social";
                dgvCliente.Columns[3].HeaderText = "Nome de Fantasia";
                dgvCliente.Columns[4].HeaderText = "CNPJ";
                
                
                List<ClienteFuncaoFuncionario> clienteFuncaoFuncionarioAgrupado = new List<SWS.Entidade.ClienteFuncaoFuncionario>();

                /* separando elemento que está repetido em relação ao cliente */

                foreach (ClienteFuncaoFuncionario cf in clienteFuncaoFuncionarioList)
                {

                    ClienteFuncaoFuncionario cfFind = clienteFuncaoFuncionarioList.Find(cliente => cliente.ClienteFuncao.Cliente.Id == cf.ClienteFuncao.Cliente.Id);
                    if (clienteFuncaoFuncionarioAgrupado.Count == 0)
                    {
                        clienteFuncaoFuncionarioAgrupado.Add(cf);
                    }
                    else if (!clienteFuncaoFuncionarioAgrupado.Exists(cliente => cliente.ClienteFuncao.Cliente.Id == cfFind.ClienteFuncao.Cliente.Id))
                    {
                        clienteFuncaoFuncionarioAgrupado.Add(cf);
                    }
                   
                }

                foreach (ClienteFuncaoFuncionario cf in clienteFuncaoFuncionarioAgrupado)
                {
                      dgvCliente.Rows.Add(cf.Id, cf.ClienteFuncao.Cliente.Id, cf.ClienteFuncao.Cliente.RazaoSocial, cf.ClienteFuncao.Cliente.Fantasia,
                            cf.ClienteFuncao.Cliente.Fisica == true ? String.Format(@"{0:000\.000\.000\-00}", Convert.ToInt64(cf.ClienteFuncao.Cliente.Cnpj)) : String.Format(@"{0:00\.000\.000\/0000\-00}", Convert.ToInt64(cf.ClienteFuncao.Cliente.Cnpj)));
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }

        protected void montaGridFuncao()
        {
            try
            {
                this.Cursor = Cursors.WaitCursor;

                dgvFuncao.Columns.Clear();
                dgvFuncao.ColumnCount = 8;

                dgvFuncao.Columns[0].HeaderText = "id_cliente_funcao_funcionario";
                dgvFuncao.Columns[0].Visible = false;

                dgvFuncao.Columns[1].HeaderText = "Código Função";
                dgvFuncao.Columns[1].ReadOnly = true;
                dgvFuncao.Columns[1].Width = 50;
                                
                dgvFuncao.Columns[2].HeaderText = "Descrição";
                dgvFuncao.Columns[2].ReadOnly = true;
                dgvFuncao.Columns[2].Width = 350;

                dgvFuncao.Columns[3].HeaderText = "Código CBO";
                dgvFuncao.Columns[3].ReadOnly = true;
                
                dgvFuncao.Columns[4].HeaderText = "Data de Cadastro";
                dgvFuncao.Columns[4].ReadOnly = true;
                
                dgvFuncao.Columns[5].HeaderText = "Data de Desligamento";
                dgvFuncao.Columns[5].ReadOnly = true;

                dgvFuncao.Columns[6].HeaderText = "Matricula";
                dgvFuncao.Columns[6].ReadOnly = true;

                dgvFuncao.Columns[7].HeaderText = "idClienteFuncao";
                dgvFuncao.Columns[7].Visible = false;

                DataGridViewCheckBoxColumn vip = new DataGridViewCheckBoxColumn();
                vip.Name = "VIP";
                dgvFuncao.Columns.Add(vip);

                foreach (ClienteFuncaoFuncionario cff in clienteFuncaoFuncionarioList)
                {
                    if ((Int64)cff.ClienteFuncao.Cliente.Id == (Int64)dgvCliente.CurrentRow.Cells[1].Value)
                    {
                            dgvFuncao.Rows.Add(
                                cff.Id, 
                                cff.ClienteFuncao.Funcao.Id,
                                cff.ClienteFuncao.Funcao.Descricao,
                                cff.ClienteFuncao.Funcao.CodCbo,
                                String.Format("{0:dd/MM/yyyy}", cff.DataCadastro),
                                cff.DataDesligamento != null ? String.Format("{0:dd/MM/yyyy}", cff.DataDesligamento) : null, 
                                cff.Matricula, 
                                cff.ClienteFuncao.Id,
                                cff.Vip);
                    }
                }

                desabilitaSort(dgvFuncao);

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }

        protected void dgvCliente_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                if (dgvCliente.CurrentRow == null)
                    throw new Exception("Você deve selecionar o cliente primeiro.");

                montaGridFuncao();

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            
        }

        protected void dataNascimento_ValueChanged(object sender, EventArgs e)
        {
            textIdade.Text = String.Empty;
            textIdade.Text = ValidaCampoHelper.CalculaIdade(Convert.ToDateTime(dataNascimento.Value)).ToString() + " anos";
        }

        protected void dgvCliente_CellFormatting(object sender, DataGridViewCellFormattingEventArgs e)
        {
            if (e.RowIndex >= 0)
            {
                DataGridViewCell cell = dgvCliente.Rows[e.RowIndex].Cells[e.ColumnIndex];
                cell.ToolTipText = "Clique na linha para mostrar a função do funcionário no cliente." + System.Environment.NewLine +
                    "Duplo clique para alterar os dados do funcionário no cliente.";
            }
        }

        protected void dgvCliente_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            btAlterarCliente.PerformClick();
        }

        protected void dgvFuncao_RowPrePaint(object sender, DataGridViewRowPrePaintEventArgs e)
        {
            DataGridView dgv = sender as DataGridView;

            if (dgv.Rows[e.RowIndex].Cells[5].Value != null)
            {
                dgv.Rows[e.RowIndex].DefaultCellStyle.ForeColor = Color.Red;
                dgv.Rows[e.RowIndex].DefaultCellStyle.Font = new System.Drawing.Font(this.Font, FontStyle.Strikeout);
            }
        }

        protected virtual void dgvFuncao_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                if (e.ColumnIndex == 8)
                {
                    /* alterando o cliente funcao funcionario */
                    clienteFuncaoFuncionarioList.Find(x => x.ClienteFuncao.Funcao.Id == (long)dgvFuncao.CurrentRow.Cells[1].Value).Vip = (bool)dgvFuncao.Rows[e.RowIndex].Cells[8].EditedFormattedValue;
                    montaGridFuncao();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }

        private void desabilitaSort(DataGridView dataGridView)
        {
            foreach (DataGridViewColumn column in dataGridView.Columns)
                column.SortMode = DataGridViewColumnSortMode.NotSortable;
        }

    }
}
