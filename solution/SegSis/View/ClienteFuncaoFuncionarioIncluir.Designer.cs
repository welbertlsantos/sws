﻿namespace SWS.View
{
    partial class frmClienteFuncaoFuncionarioIncluir
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.flpAcao = new System.Windows.Forms.FlowLayoutPanel();
            this.btConfirmar = new System.Windows.Forms.Button();
            this.btFechar = new System.Windows.Forms.Button();
            this.cbBrPdh = new SWS.ComboBoxWithBorder();
            this.lblBrPdh = new System.Windows.Forms.TextBox();
            this.textRegime = new System.Windows.Forms.TextBox();
            this.lblRegime = new System.Windows.Forms.TextBox();
            this.dataAdmissao = new System.Windows.Forms.DateTimePicker();
            this.lblFuncionario = new System.Windows.Forms.Label();
            this.lblDataAdmissao = new System.Windows.Forms.TextBox();
            this.textFantasia = new System.Windows.Forms.TextBox();
            this.lblCnpj = new System.Windows.Forms.TextBox();
            this.textCnpj = new System.Windows.Forms.MaskedTextBox();
            this.lblFantasia = new System.Windows.Forms.TextBox();
            this.lblCliente = new System.Windows.Forms.Label();
            this.textRazaoSocial = new System.Windows.Forms.TextBox();
            this.lblRazaoSocial = new System.Windows.Forms.TextBox();
            this.lblMatricula = new System.Windows.Forms.TextBox();
            this.lblVinculo = new System.Windows.Forms.TextBox();
            this.lblEstagiario = new System.Windows.Forms.TextBox();
            this.textMatricula = new System.Windows.Forms.TextBox();
            this.cbVinculo = new SWS.ComboBoxWithBorder();
            this.cbEstagiario = new SWS.ComboBoxWithBorder();
            this.tPClienteFuncionário = new System.Windows.Forms.ToolTip(this.components);
            this.lblClienteVIP = new System.Windows.Forms.TextBox();
            this.cbVip = new SWS.ComboBoxWithBorder();
            ((System.ComponentModel.ISupportInitialize)(this.scForm)).BeginInit();
            this.scForm.Panel1.SuspendLayout();
            this.scForm.Panel2.SuspendLayout();
            this.scForm.SuspendLayout();
            this.pnlForm.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.banner)).BeginInit();
            this.flpAcao.SuspendLayout();
            this.SuspendLayout();
            // 
            // scForm
            // 
            // 
            // scForm.Panel1
            // 
            this.scForm.Panel1.Controls.Add(this.flpAcao);
            // 
            // pnlForm
            // 
            this.pnlForm.Controls.Add(this.cbVip);
            this.pnlForm.Controls.Add(this.lblClienteVIP);
            this.pnlForm.Controls.Add(this.cbEstagiario);
            this.pnlForm.Controls.Add(this.cbVinculo);
            this.pnlForm.Controls.Add(this.textMatricula);
            this.pnlForm.Controls.Add(this.lblEstagiario);
            this.pnlForm.Controls.Add(this.lblVinculo);
            this.pnlForm.Controls.Add(this.lblMatricula);
            this.pnlForm.Controls.Add(this.cbBrPdh);
            this.pnlForm.Controls.Add(this.lblBrPdh);
            this.pnlForm.Controls.Add(this.textRegime);
            this.pnlForm.Controls.Add(this.lblRegime);
            this.pnlForm.Controls.Add(this.dataAdmissao);
            this.pnlForm.Controls.Add(this.lblFuncionario);
            this.pnlForm.Controls.Add(this.lblDataAdmissao);
            this.pnlForm.Controls.Add(this.textFantasia);
            this.pnlForm.Controls.Add(this.lblCnpj);
            this.pnlForm.Controls.Add(this.textCnpj);
            this.pnlForm.Controls.Add(this.lblFantasia);
            this.pnlForm.Controls.Add(this.lblCliente);
            this.pnlForm.Controls.Add(this.textRazaoSocial);
            this.pnlForm.Controls.Add(this.lblRazaoSocial);
            // 
            // flpAcao
            // 
            this.flpAcao.Controls.Add(this.btConfirmar);
            this.flpAcao.Controls.Add(this.btFechar);
            this.flpAcao.Dock = System.Windows.Forms.DockStyle.Fill;
            this.flpAcao.Location = new System.Drawing.Point(0, 0);
            this.flpAcao.Name = "flpAcao";
            this.flpAcao.Size = new System.Drawing.Size(534, 30);
            this.flpAcao.TabIndex = 0;
            // 
            // btConfirmar
            // 
            this.btConfirmar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btConfirmar.Image = global::SWS.Properties.Resources.Gravar;
            this.btConfirmar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btConfirmar.Location = new System.Drawing.Point(3, 3);
            this.btConfirmar.Name = "btConfirmar";
            this.btConfirmar.Size = new System.Drawing.Size(75, 23);
            this.btConfirmar.TabIndex = 6;
            this.btConfirmar.Text = "Gravar";
            this.btConfirmar.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btConfirmar.UseVisualStyleBackColor = true;
            this.btConfirmar.Click += new System.EventHandler(this.btConfirmar_Click);
            // 
            // btFechar
            // 
            this.btFechar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btFechar.Image = global::SWS.Properties.Resources.close;
            this.btFechar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btFechar.Location = new System.Drawing.Point(84, 3);
            this.btFechar.Name = "btFechar";
            this.btFechar.Size = new System.Drawing.Size(75, 23);
            this.btFechar.TabIndex = 7;
            this.btFechar.Text = "Fechar";
            this.btFechar.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btFechar.UseVisualStyleBackColor = true;
            this.btFechar.Click += new System.EventHandler(this.btFechar_Click);
            // 
            // cbBrPdh
            // 
            this.cbBrPdh.BackColor = System.Drawing.Color.LightGray;
            this.cbBrPdh.BorderColor = System.Drawing.Color.Gray;
            this.cbBrPdh.BorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid;
            this.cbBrPdh.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cbBrPdh.FormattingEnabled = true;
            this.cbBrPdh.Location = new System.Drawing.Point(211, 166);
            this.cbBrPdh.Name = "cbBrPdh";
            this.cbBrPdh.Size = new System.Drawing.Size(288, 21);
            this.cbBrPdh.TabIndex = 3;
            // 
            // lblBrPdh
            // 
            this.lblBrPdh.BackColor = System.Drawing.Color.LightGray;
            this.lblBrPdh.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblBrPdh.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblBrPdh.Location = new System.Drawing.Point(12, 166);
            this.lblBrPdh.Name = "lblBrPdh";
            this.lblBrPdh.ReadOnly = true;
            this.lblBrPdh.Size = new System.Drawing.Size(200, 21);
            this.lblBrPdh.TabIndex = 57;
            this.lblBrPdh.TabStop = false;
            this.lblBrPdh.Text = "Informação extra PPP";
            // 
            // textRegime
            // 
            this.textRegime.BackColor = System.Drawing.Color.White;
            this.textRegime.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textRegime.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.textRegime.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textRegime.Location = new System.Drawing.Point(211, 146);
            this.textRegime.MaxLength = 15;
            this.textRegime.Name = "textRegime";
            this.textRegime.Size = new System.Drawing.Size(288, 21);
            this.textRegime.TabIndex = 2;
            // 
            // lblRegime
            // 
            this.lblRegime.BackColor = System.Drawing.Color.LightGray;
            this.lblRegime.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblRegime.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblRegime.Location = new System.Drawing.Point(12, 146);
            this.lblRegime.Name = "lblRegime";
            this.lblRegime.ReadOnly = true;
            this.lblRegime.Size = new System.Drawing.Size(200, 21);
            this.lblRegime.TabIndex = 56;
            this.lblRegime.TabStop = false;
            this.lblRegime.Text = "Regime de revezamento";
            // 
            // dataAdmissao
            // 
            this.dataAdmissao.CalendarFont = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dataAdmissao.Checked = false;
            this.dataAdmissao.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dataAdmissao.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dataAdmissao.Location = new System.Drawing.Point(211, 126);
            this.dataAdmissao.Name = "dataAdmissao";
            this.dataAdmissao.ShowCheckBox = true;
            this.dataAdmissao.Size = new System.Drawing.Size(288, 21);
            this.dataAdmissao.TabIndex = 1;
            // 
            // lblFuncionario
            // 
            this.lblFuncionario.AutoSize = true;
            this.lblFuncionario.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblFuncionario.ForeColor = System.Drawing.Color.Red;
            this.lblFuncionario.Location = new System.Drawing.Point(9, 108);
            this.lblFuncionario.Name = "lblFuncionario";
            this.lblFuncionario.Size = new System.Drawing.Size(72, 15);
            this.lblFuncionario.TabIndex = 55;
            this.lblFuncionario.Text = "Funcionário";
            // 
            // lblDataAdmissao
            // 
            this.lblDataAdmissao.BackColor = System.Drawing.Color.LightGray;
            this.lblDataAdmissao.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblDataAdmissao.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDataAdmissao.Location = new System.Drawing.Point(12, 126);
            this.lblDataAdmissao.Name = "lblDataAdmissao";
            this.lblDataAdmissao.ReadOnly = true;
            this.lblDataAdmissao.Size = new System.Drawing.Size(200, 21);
            this.lblDataAdmissao.TabIndex = 54;
            this.lblDataAdmissao.TabStop = false;
            this.lblDataAdmissao.Text = "Data de Admissão";
            // 
            // textFantasia
            // 
            this.textFantasia.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.textFantasia.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textFantasia.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.textFantasia.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textFantasia.ForeColor = System.Drawing.Color.Blue;
            this.textFantasia.Location = new System.Drawing.Point(211, 50);
            this.textFantasia.Name = "textFantasia";
            this.textFantasia.ReadOnly = true;
            this.textFantasia.Size = new System.Drawing.Size(288, 21);
            this.textFantasia.TabIndex = 50;
            this.textFantasia.TabStop = false;
            // 
            // lblCnpj
            // 
            this.lblCnpj.BackColor = System.Drawing.Color.LightGray;
            this.lblCnpj.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblCnpj.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCnpj.Location = new System.Drawing.Point(12, 70);
            this.lblCnpj.Name = "lblCnpj";
            this.lblCnpj.ReadOnly = true;
            this.lblCnpj.Size = new System.Drawing.Size(200, 21);
            this.lblCnpj.TabIndex = 52;
            this.lblCnpj.TabStop = false;
            this.lblCnpj.Text = "CNPJ";
            // 
            // textCnpj
            // 
            this.textCnpj.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.textCnpj.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textCnpj.ForeColor = System.Drawing.Color.Blue;
            this.textCnpj.Location = new System.Drawing.Point(211, 70);
            this.textCnpj.Mask = "99,999,999/9999-99";
            this.textCnpj.Name = "textCnpj";
            this.textCnpj.PromptChar = ' ';
            this.textCnpj.ReadOnly = true;
            this.textCnpj.Size = new System.Drawing.Size(288, 21);
            this.textCnpj.TabIndex = 53;
            this.textCnpj.TabStop = false;
            // 
            // lblFantasia
            // 
            this.lblFantasia.BackColor = System.Drawing.Color.LightGray;
            this.lblFantasia.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblFantasia.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblFantasia.Location = new System.Drawing.Point(12, 50);
            this.lblFantasia.Name = "lblFantasia";
            this.lblFantasia.ReadOnly = true;
            this.lblFantasia.Size = new System.Drawing.Size(200, 21);
            this.lblFantasia.TabIndex = 51;
            this.lblFantasia.TabStop = false;
            this.lblFantasia.Text = "Nome de Fantasia";
            // 
            // lblCliente
            // 
            this.lblCliente.AutoSize = true;
            this.lblCliente.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCliente.ForeColor = System.Drawing.Color.Red;
            this.lblCliente.Location = new System.Drawing.Point(9, 12);
            this.lblCliente.Name = "lblCliente";
            this.lblCliente.Size = new System.Drawing.Size(45, 15);
            this.lblCliente.TabIndex = 49;
            this.lblCliente.Text = "Cliente";
            // 
            // textRazaoSocial
            // 
            this.textRazaoSocial.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.textRazaoSocial.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textRazaoSocial.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.textRazaoSocial.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textRazaoSocial.ForeColor = System.Drawing.Color.Blue;
            this.textRazaoSocial.Location = new System.Drawing.Point(211, 30);
            this.textRazaoSocial.Name = "textRazaoSocial";
            this.textRazaoSocial.ReadOnly = true;
            this.textRazaoSocial.Size = new System.Drawing.Size(288, 21);
            this.textRazaoSocial.TabIndex = 47;
            this.textRazaoSocial.TabStop = false;
            // 
            // lblRazaoSocial
            // 
            this.lblRazaoSocial.BackColor = System.Drawing.Color.LightGray;
            this.lblRazaoSocial.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblRazaoSocial.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblRazaoSocial.Location = new System.Drawing.Point(12, 30);
            this.lblRazaoSocial.Name = "lblRazaoSocial";
            this.lblRazaoSocial.ReadOnly = true;
            this.lblRazaoSocial.Size = new System.Drawing.Size(200, 21);
            this.lblRazaoSocial.TabIndex = 48;
            this.lblRazaoSocial.TabStop = false;
            this.lblRazaoSocial.Text = "Razão Social";
            // 
            // lblMatricula
            // 
            this.lblMatricula.BackColor = System.Drawing.Color.LightGray;
            this.lblMatricula.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblMatricula.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblMatricula.Location = new System.Drawing.Point(12, 186);
            this.lblMatricula.Name = "lblMatricula";
            this.lblMatricula.ReadOnly = true;
            this.lblMatricula.Size = new System.Drawing.Size(200, 21);
            this.lblMatricula.TabIndex = 58;
            this.lblMatricula.TabStop = false;
            this.lblMatricula.Text = "Matrícula";
            // 
            // lblVinculo
            // 
            this.lblVinculo.BackColor = System.Drawing.Color.LightGray;
            this.lblVinculo.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblVinculo.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblVinculo.Location = new System.Drawing.Point(12, 206);
            this.lblVinculo.Name = "lblVinculo";
            this.lblVinculo.ReadOnly = true;
            this.lblVinculo.Size = new System.Drawing.Size(200, 21);
            this.lblVinculo.TabIndex = 59;
            this.lblVinculo.TabStop = false;
            this.lblVinculo.Text = "Possui vínculo?";
            // 
            // lblEstagiario
            // 
            this.lblEstagiario.BackColor = System.Drawing.Color.LightGray;
            this.lblEstagiario.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblEstagiario.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblEstagiario.Location = new System.Drawing.Point(12, 226);
            this.lblEstagiario.Name = "lblEstagiario";
            this.lblEstagiario.ReadOnly = true;
            this.lblEstagiario.Size = new System.Drawing.Size(200, 21);
            this.lblEstagiario.TabIndex = 60;
            this.lblEstagiario.TabStop = false;
            this.lblEstagiario.Text = "É estagiário?";
            // 
            // textMatricula
            // 
            this.textMatricula.BackColor = System.Drawing.Color.White;
            this.textMatricula.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textMatricula.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.textMatricula.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textMatricula.Location = new System.Drawing.Point(211, 186);
            this.textMatricula.MaxLength = 30;
            this.textMatricula.Name = "textMatricula";
            this.textMatricula.Size = new System.Drawing.Size(288, 21);
            this.textMatricula.TabIndex = 4;
            // 
            // cbVinculo
            // 
            this.cbVinculo.BackColor = System.Drawing.Color.LightGray;
            this.cbVinculo.BorderColor = System.Drawing.Color.Gray;
            this.cbVinculo.BorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid;
            this.cbVinculo.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cbVinculo.FormattingEnabled = true;
            this.cbVinculo.Location = new System.Drawing.Point(211, 206);
            this.cbVinculo.Name = "cbVinculo";
            this.cbVinculo.Size = new System.Drawing.Size(288, 21);
            this.cbVinculo.TabIndex = 5;
            // 
            // cbEstagiario
            // 
            this.cbEstagiario.BackColor = System.Drawing.Color.LightGray;
            this.cbEstagiario.BorderColor = System.Drawing.Color.Gray;
            this.cbEstagiario.BorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid;
            this.cbEstagiario.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cbEstagiario.FormattingEnabled = true;
            this.cbEstagiario.Location = new System.Drawing.Point(211, 226);
            this.cbEstagiario.Name = "cbEstagiario";
            this.cbEstagiario.Size = new System.Drawing.Size(288, 21);
            this.cbEstagiario.TabIndex = 6;
            // 
            // lblClienteVIP
            // 
            this.lblClienteVIP.BackColor = System.Drawing.Color.LightGray;
            this.lblClienteVIP.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblClienteVIP.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblClienteVIP.Location = new System.Drawing.Point(12, 246);
            this.lblClienteVIP.Name = "lblClienteVIP";
            this.lblClienteVIP.ReadOnly = true;
            this.lblClienteVIP.Size = new System.Drawing.Size(200, 21);
            this.lblClienteVIP.TabIndex = 61;
            this.lblClienteVIP.TabStop = false;
            this.lblClienteVIP.Text = "É VIP?";
            // 
            // cbVip
            // 
            this.cbVip.BackColor = System.Drawing.Color.LightGray;
            this.cbVip.BorderColor = System.Drawing.Color.Gray;
            this.cbVip.BorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid;
            this.cbVip.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cbVip.FormattingEnabled = true;
            this.cbVip.Location = new System.Drawing.Point(211, 246);
            this.cbVip.Name = "cbVip";
            this.cbVip.Size = new System.Drawing.Size(288, 21);
            this.cbVip.TabIndex = 62;
            this.cbVip.SelectedIndexChanged += new System.EventHandler(this.cbVip_SelectedIndexChanged);
            // 
            // frmClienteFuncaoFuncionarioIncluir
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(534, 361);
            this.Name = "frmClienteFuncaoFuncionarioIncluir";
            this.Text = "INCLUIR FUNCIONÁRIO";
            this.scForm.Panel1.ResumeLayout(false);
            this.scForm.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.scForm)).EndInit();
            this.scForm.ResumeLayout(false);
            this.pnlForm.ResumeLayout(false);
            this.pnlForm.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.banner)).EndInit();
            this.flpAcao.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        protected System.Windows.Forms.Button btFechar;
        protected System.Windows.Forms.Button btConfirmar;
        protected ComboBoxWithBorder cbBrPdh;
        protected System.Windows.Forms.TextBox lblBrPdh;
        protected System.Windows.Forms.TextBox textRegime;
        protected System.Windows.Forms.TextBox lblRegime;
        protected System.Windows.Forms.DateTimePicker dataAdmissao;
        protected System.Windows.Forms.Label lblFuncionario;
        protected System.Windows.Forms.TextBox lblDataAdmissao;
        protected System.Windows.Forms.TextBox textFantasia;
        protected System.Windows.Forms.TextBox lblCnpj;
        protected System.Windows.Forms.MaskedTextBox textCnpj;
        protected System.Windows.Forms.TextBox lblFantasia;
        protected System.Windows.Forms.Label lblCliente;
        protected System.Windows.Forms.TextBox textRazaoSocial;
        protected System.Windows.Forms.TextBox lblRazaoSocial;
        protected ComboBoxWithBorder cbEstagiario;
        protected ComboBoxWithBorder cbVinculo;
        protected System.Windows.Forms.TextBox textMatricula;
        protected System.Windows.Forms.TextBox lblEstagiario;
        protected System.Windows.Forms.TextBox lblVinculo;
        protected System.Windows.Forms.TextBox lblMatricula;
        protected System.Windows.Forms.ToolTip tPClienteFuncionário;
        protected System.Windows.Forms.FlowLayoutPanel flpAcao;
        protected ComboBoxWithBorder cbVip;
        protected System.Windows.Forms.TextBox lblClienteVIP;
    }
}