﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using SWS.Facade;
using SWS.Entidade;
using SWS.Excecao;
using SWS.View.ViewHelper;
using System.Text.RegularExpressions;
using SWS.View.Resources;

namespace SWS.View
{
    public partial class frmCronogramaAtividadeAlterar : BaseFormConsulta
    {
        private CronogramaAtividade cronogramaAtividade;

        public frmCronogramaAtividadeAlterar(CronogramaAtividade cronogramaAtividade)
        {
            InitializeComponent();
            this.cronogramaAtividade = cronogramaAtividade;
            ComboHelper.startComboMesSave(cb_mes);
            cb_mes.SelectedValue = (Convert.ToString(cronogramaAtividade.MesRealizado));

            // preenchendos os dados em tela.
            text_atividade.Text = Convert.ToString(cronogramaAtividade.Atividade.Descricao);
            text_ano.Text = Convert.ToString(cronogramaAtividade.AnoRealizado);
            text_evidencia.Text = Convert.ToString(cronogramaAtividade.Evidencia);
            text_publico.Text = Convert.ToString(cronogramaAtividade.PublicoAlvo);
        }

        private void btn_fechar_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btn_limpar_Click(object sender, EventArgs e)
        {
            ComboHelper.startComboMesSave(cb_mes);
            text_ano.Text = String.Empty;
            text_evidencia.Text = String.Empty;
            text_publico.Text = String.Empty;

        }

        private void btn_alterar_Click(object sender, EventArgs e)
        {
            try
            {
                if (!String.IsNullOrEmpty(this.text_ano.Text) && text_ano.Text.Length != 4)
                    throw new AtividadeException(AtividadeException.msg1);
                
                cronogramaAtividade.AnoRealizado = text_ano.Text;
                cronogramaAtividade.Evidencia = text_evidencia.Text.ToUpper();
                cronogramaAtividade.MesRealizado = Convert.ToString(cb_mes.SelectedValue);
                cronogramaAtividade.PublicoAlvo = text_publico.Text.ToUpper();
                
                this.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }

        private void text_ano_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = ValidaCampoHelper.FormataDigitacaoNumero(sender, e, text_ano.Text);
        }
    }
}