﻿using SWS.Entidade;
using SWS.Facade;
using SWS.View.Resources;
using SWS.View.ViewHelper;
using SWS.ViewHelper;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace SWS.View
{
    public partial class frmFontePrincipal : frmTemplate
    {
        private Fonte fonte;
        
        public frmFontePrincipal()
        {
            InitializeComponent();
            ComboHelper.comboSituacao(cbSituacao);
            ActiveControl = textDescricao;
            validaPermissoes();
        }

        private void btnIncluir_Click(object sender, EventArgs e)
        {
            try
            {
                frmFonteIncluir incluirFonte = new frmFonteIncluir();
                incluirFonte.ShowDialog();

                if (incluirFonte.Fonte != null)
                {
                    fonte = new Fonte();
                    montaDataGrid();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }

        private void btnAlterar_Click(object sender, EventArgs e)
        {
            try
            {
                if (dgvFonte.CurrentRow == null)
                    throw new Exception("Selecione uma linha.");

                EstudoFacade pcmsoFacade = EstudoFacade.getInstance();

                Fonte fonteAlterar = pcmsoFacade.findFonteById((long)dgvFonte.CurrentRow.Cells[0].Value);

                if (!string.Equals(fonteAlterar.Situacao, ApplicationConstants.ATIVO))
                    throw new Exception("Somente fontes ativas podem ser alteradas.");

                frmFonteAlterar alterarFonte = new frmFonteAlterar(fonteAlterar);
                alterarFonte.ShowDialog();

                fonte = new Fonte();
                montaDataGrid();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }


        }

        private void btnPesquisar_Click(object sender, EventArgs e)
        {
            try
            {
                fonte = new Fonte(null, textDescricao.Text, ((SelectItem)cbSituacao.SelectedItem).Valor, false);
                montaDataGrid();
                textDescricao.Focus();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }

        private void btnReativar_Click(object sender, EventArgs e)
        {
            try
            {
                if (dgvFonte.CurrentRow == null)
                    throw new Exception("Selecione uma linha.");

                EstudoFacade pcmsoFacade = EstudoFacade.getInstance();
                
                Fonte fonteReativar = pcmsoFacade.findFonteById((long)dgvFonte.CurrentRow.Cells[0].Value);

                if (!string.Equals(fonteReativar.Situacao, ApplicationConstants.DESATIVADO))
                    throw new Exception("Somente fontes inativas podem ser reativadas.");

                if (MessageBox.Show("Deseja reativar a fonte selecionada?", "Atenção", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                {
                    fonteReativar.Situacao = ApplicationConstants.ATIVO;
                    pcmsoFacade.updateFonte(fonteReativar);
                    MessageBox.Show("Fonte reativada com sucesso.", "Confirmação", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    fonte = new Fonte();
                    montaDataGrid();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }

        private void btnDetalhar_Click(object sender, EventArgs e)
        {
            try
            {
                if (dgvFonte.CurrentRow == null)
                    throw new Exception("Selecione uma linha.");

                EstudoFacade pcmsoFacade = EstudoFacade.getInstance();

                frmFonteDetalhar detalharFonte = new frmFonteDetalhar(pcmsoFacade.findFonteById((long)dgvFonte.CurrentRow.Cells[0].Value));
                detalharFonte.ShowDialog();

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }

        private void btnExcluir_Click(object sender, EventArgs e)
        {
            try
            {
                if (dgvFonte.CurrentRow == null)
                    throw new Exception("Selecione uma linha.");

                EstudoFacade pcmsoFacade = EstudoFacade.getInstance();

                Fonte fonteExcluir = pcmsoFacade.findFonteById((long)dgvFonte.CurrentRow.Cells[0].Value);

                if (!string.Equals(fonteExcluir.Situacao, ApplicationConstants.ATIVO))
                    throw new Exception("Somente fontes ativas podem ser excluídas.");

                if (MessageBox.Show("Deseja excluir a fonte selecionada?", "Atenção", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                {
                    fonteExcluir.Situacao = ApplicationConstants.DESATIVADO;
                    pcmsoFacade.updateFonte(fonteExcluir);
                    MessageBox.Show("Fonte excluída com sucesso.", "Confirmação", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    fonte = new Fonte();
                    montaDataGrid();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }

        private void btnFechar_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void validaPermissoes()
        {
            PermissionamentoFacade permissionamentoFacade = PermissionamentoFacade.getInstance();

            btnAlterar.Enabled = permissionamentoFacade.hasPermission("FONTE", "ALTERAR");
            btnIncluir.Enabled = permissionamentoFacade.hasPermission("FONTE", "INCLUIR");
            btnExcluir.Enabled = permissionamentoFacade.hasPermission("FONTE", "EXCLUIR");
            btnDetalhar.Enabled = permissionamentoFacade.hasPermission("FONTE", "DETALHAR");
            btnReativar.Enabled = permissionamentoFacade.hasPermission("FONTE", "REATIVAR");
        }

        public void montaDataGrid()
        {

            try
            {
                this.Cursor = Cursors.WaitCursor;

                EstudoFacade pmcsoFacade = EstudoFacade.getInstance();

                DataSet ds = pmcsoFacade.findFonteByFilter(fonte);

                dgvFonte.DataSource = ds.Tables["Fontes"].DefaultView;

                dgvFonte.Columns[0].HeaderText = "ID";
                dgvFonte.Columns[0].Visible = false;

                dgvFonte.Columns[1].HeaderText = "Descrição";

                dgvFonte.Columns[2].HeaderText = "Situação";

                dgvFonte.Columns[3].HeaderText = "Privado";
                dgvFonte.Columns[3].Visible = false;

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }

        }

        private void dgvFonte_RowPrePaint(object sender, DataGridViewRowPrePaintEventArgs e)
        {
            DataGridView dgv = sender as DataGridView;

            if (string.Equals(dgv.Rows[e.RowIndex].Cells[2].Value, ApplicationConstants.DESATIVADO))
                dgv.Rows[e.RowIndex].DefaultCellStyle.ForeColor = Color.Red;
        }

        private void dgvFonte_CellMouseDoubleClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            try
            {
                btnAlterar.PerformClick();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }
    }
}
