﻿namespace SWS.View
{
    partial class frmClientePrincipal
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            this.flpAcao = new System.Windows.Forms.FlowLayoutPanel();
            this.bt_incluir = new System.Windows.Forms.Button();
            this.bt_alterar = new System.Windows.Forms.Button();
            this.bt_excluir = new System.Windows.Forms.Button();
            this.btn_reativar = new System.Windows.Forms.Button();
            this.bt_pesquisar = new System.Windows.Forms.Button();
            this.bt_detalhar = new System.Windows.Forms.Button();
            this.bt_limpar = new System.Windows.Forms.Button();
            this.bt_fechar = new System.Windows.Forms.Button();
            this.dataInicial = new System.Windows.Forms.DateTimePicker();
            this.dataFinal = new System.Windows.Forms.DateTimePicker();
            this.textCnpj = new System.Windows.Forms.MaskedTextBox();
            this.textFantasia = new System.Windows.Forms.TextBox();
            this.textCliente = new System.Windows.Forms.TextBox();
            this.lblCliente = new System.Windows.Forms.TextBox();
            this.lblFantasia = new System.Windows.Forms.TextBox();
            this.lblCnpj = new System.Windows.Forms.TextBox();
            this.lblCidade = new System.Windows.Forms.TextBox();
            this.lblUf = new System.Windows.Forms.TextBox();
            this.cbCidade = new SWS.ComboBoxWithBorder();
            this.cbUf = new SWS.ComboBoxWithBorder();
            this.lblTipoCliente = new System.Windows.Forms.TextBox();
            this.cbTipoCliente = new SWS.ComboBoxWithBorder();
            this.lblSituacao = new System.Windows.Forms.TextBox();
            this.cbSituacao = new SWS.ComboBoxWithBorder();
            this.lblDataCadastro = new System.Windows.Forms.TextBox();
            this.grbCliente = new System.Windows.Forms.GroupBox();
            this.dgvCliente = new System.Windows.Forms.DataGridView();
            this.lblMarcInativo = new System.Windows.Forms.Label();
            this.lblInativo = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.spForm)).BeginInit();
            this.spForm.Panel1.SuspendLayout();
            this.spForm.Panel2.SuspendLayout();
            this.spForm.SuspendLayout();
            this.pnlForm.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.banner)).BeginInit();
            this.flpAcao.SuspendLayout();
            this.grbCliente.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvCliente)).BeginInit();
            this.SuspendLayout();
            // 
            // spForm
            // 
            // 
            // spForm.Panel1
            // 
            this.spForm.Panel1.Controls.Add(this.flpAcao);
            // 
            // spForm.Panel2
            // 
            this.spForm.Panel2.AutoScroll = false;
            // 
            // pnlForm
            // 
            this.pnlForm.Controls.Add(this.lblInativo);
            this.pnlForm.Controls.Add(this.lblMarcInativo);
            this.pnlForm.Controls.Add(this.grbCliente);
            this.pnlForm.Controls.Add(this.cbSituacao);
            this.pnlForm.Controls.Add(this.lblDataCadastro);
            this.pnlForm.Controls.Add(this.lblSituacao);
            this.pnlForm.Controls.Add(this.cbTipoCliente);
            this.pnlForm.Controls.Add(this.lblTipoCliente);
            this.pnlForm.Controls.Add(this.cbUf);
            this.pnlForm.Controls.Add(this.cbCidade);
            this.pnlForm.Controls.Add(this.lblUf);
            this.pnlForm.Controls.Add(this.lblCidade);
            this.pnlForm.Controls.Add(this.lblCnpj);
            this.pnlForm.Controls.Add(this.lblFantasia);
            this.pnlForm.Controls.Add(this.lblCliente);
            this.pnlForm.Controls.Add(this.textCnpj);
            this.pnlForm.Controls.Add(this.textFantasia);
            this.pnlForm.Controls.Add(this.textCliente);
            this.pnlForm.Controls.Add(this.dataInicial);
            this.pnlForm.Controls.Add(this.dataFinal);
            this.pnlForm.Size = new System.Drawing.Size(781, 490);
            // 
            // banner
            // 
            this.banner.Size = new System.Drawing.Size(800, 39);
            // 
            // flpAcao
            // 
            this.flpAcao.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.flpAcao.Controls.Add(this.bt_incluir);
            this.flpAcao.Controls.Add(this.bt_alterar);
            this.flpAcao.Controls.Add(this.bt_excluir);
            this.flpAcao.Controls.Add(this.btn_reativar);
            this.flpAcao.Controls.Add(this.bt_pesquisar);
            this.flpAcao.Controls.Add(this.bt_detalhar);
            this.flpAcao.Controls.Add(this.bt_limpar);
            this.flpAcao.Controls.Add(this.bt_fechar);
            this.flpAcao.Location = new System.Drawing.Point(0, 0);
            this.flpAcao.Name = "flpAcao";
            this.flpAcao.Size = new System.Drawing.Size(784, 35);
            this.flpAcao.TabIndex = 0;
            // 
            // bt_incluir
            // 
            this.bt_incluir.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.bt_incluir.Image = global::SWS.Properties.Resources.icone_mais;
            this.bt_incluir.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.bt_incluir.Location = new System.Drawing.Point(3, 3);
            this.bt_incluir.Name = "bt_incluir";
            this.bt_incluir.Size = new System.Drawing.Size(71, 22);
            this.bt_incluir.TabIndex = 23;
            this.bt_incluir.Text = "&Incluir";
            this.bt_incluir.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.bt_incluir.UseVisualStyleBackColor = true;
            this.bt_incluir.Click += new System.EventHandler(this.bt_incluir_Click);
            // 
            // bt_alterar
            // 
            this.bt_alterar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.bt_alterar.Image = global::SWS.Properties.Resources.Alterar;
            this.bt_alterar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.bt_alterar.Location = new System.Drawing.Point(80, 3);
            this.bt_alterar.Name = "bt_alterar";
            this.bt_alterar.Size = new System.Drawing.Size(71, 22);
            this.bt_alterar.TabIndex = 24;
            this.bt_alterar.Text = "&Alterar";
            this.bt_alterar.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.bt_alterar.UseVisualStyleBackColor = true;
            this.bt_alterar.Click += new System.EventHandler(this.bt_alterar_Click);
            // 
            // bt_excluir
            // 
            this.bt_excluir.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.bt_excluir.Image = global::SWS.Properties.Resources.lixeira;
            this.bt_excluir.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.bt_excluir.Location = new System.Drawing.Point(157, 3);
            this.bt_excluir.Name = "bt_excluir";
            this.bt_excluir.Size = new System.Drawing.Size(71, 22);
            this.bt_excluir.TabIndex = 25;
            this.bt_excluir.Text = "&Excluir";
            this.bt_excluir.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.bt_excluir.UseVisualStyleBackColor = true;
            this.bt_excluir.Click += new System.EventHandler(this.bt_excluir_Click);
            // 
            // btn_reativar
            // 
            this.btn_reativar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_reativar.Image = global::SWS.Properties.Resources.devolucao;
            this.btn_reativar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btn_reativar.Location = new System.Drawing.Point(234, 3);
            this.btn_reativar.Name = "btn_reativar";
            this.btn_reativar.Size = new System.Drawing.Size(71, 22);
            this.btn_reativar.TabIndex = 28;
            this.btn_reativar.Text = "&Reativar";
            this.btn_reativar.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btn_reativar.UseVisualStyleBackColor = true;
            this.btn_reativar.Click += new System.EventHandler(this.btn_reativar_Click);
            // 
            // bt_pesquisar
            // 
            this.bt_pesquisar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.bt_pesquisar.Image = global::SWS.Properties.Resources.lupa;
            this.bt_pesquisar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.bt_pesquisar.Location = new System.Drawing.Point(311, 3);
            this.bt_pesquisar.Name = "bt_pesquisar";
            this.bt_pesquisar.Size = new System.Drawing.Size(71, 22);
            this.bt_pesquisar.TabIndex = 21;
            this.bt_pesquisar.Text = "Pesquisar";
            this.bt_pesquisar.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.bt_pesquisar.UseVisualStyleBackColor = true;
            this.bt_pesquisar.Click += new System.EventHandler(this.bt_pesquisar_Click);
            // 
            // bt_detalhar
            // 
            this.bt_detalhar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.bt_detalhar.Image = global::SWS.Properties.Resources.lupa;
            this.bt_detalhar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.bt_detalhar.Location = new System.Drawing.Point(388, 3);
            this.bt_detalhar.Name = "bt_detalhar";
            this.bt_detalhar.Size = new System.Drawing.Size(71, 22);
            this.bt_detalhar.TabIndex = 26;
            this.bt_detalhar.Text = "&Detalhar";
            this.bt_detalhar.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.bt_detalhar.UseVisualStyleBackColor = true;
            this.bt_detalhar.Click += new System.EventHandler(this.bt_detalhar_Click);
            // 
            // bt_limpar
            // 
            this.bt_limpar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.bt_limpar.Image = global::SWS.Properties.Resources.vassoura;
            this.bt_limpar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.bt_limpar.Location = new System.Drawing.Point(465, 3);
            this.bt_limpar.Name = "bt_limpar";
            this.bt_limpar.Size = new System.Drawing.Size(71, 22);
            this.bt_limpar.TabIndex = 22;
            this.bt_limpar.Text = "Limpar";
            this.bt_limpar.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.bt_limpar.UseVisualStyleBackColor = true;
            this.bt_limpar.Click += new System.EventHandler(this.bt_limpar_Click);
            // 
            // bt_fechar
            // 
            this.bt_fechar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.bt_fechar.Image = global::SWS.Properties.Resources.close;
            this.bt_fechar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.bt_fechar.Location = new System.Drawing.Point(542, 3);
            this.bt_fechar.Name = "bt_fechar";
            this.bt_fechar.Size = new System.Drawing.Size(71, 22);
            this.bt_fechar.TabIndex = 27;
            this.bt_fechar.Text = "&Fechar";
            this.bt_fechar.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.bt_fechar.UseVisualStyleBackColor = true;
            this.bt_fechar.Click += new System.EventHandler(this.bt_fechar_Click);
            // 
            // dataInicial
            // 
            this.dataInicial.Checked = false;
            this.dataInicial.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dataInicial.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dataInicial.Location = new System.Drawing.Point(150, 154);
            this.dataInicial.Name = "dataInicial";
            this.dataInicial.ShowCheckBox = true;
            this.dataInicial.Size = new System.Drawing.Size(115, 21);
            this.dataInicial.TabIndex = 11;
            // 
            // dataFinal
            // 
            this.dataFinal.Checked = false;
            this.dataFinal.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dataFinal.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dataFinal.Location = new System.Drawing.Point(265, 154);
            this.dataFinal.Name = "dataFinal";
            this.dataFinal.ShowCheckBox = true;
            this.dataFinal.Size = new System.Drawing.Size(115, 21);
            this.dataFinal.TabIndex = 10;
            this.dataFinal.Value = new System.DateTime(2011, 10, 27, 21, 57, 46, 0);
            // 
            // textCnpj
            // 
            this.textCnpj.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.textCnpj.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textCnpj.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textCnpj.Location = new System.Drawing.Point(150, 54);
            this.textCnpj.Name = "textCnpj";
            this.textCnpj.PromptChar = ' ';
            this.textCnpj.Size = new System.Drawing.Size(619, 21);
            this.textCnpj.TabIndex = 23;
            this.textCnpj.Enter += new System.EventHandler(this.textCnpj_Enter);
            this.textCnpj.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.textCnpj_KeyPress);
            this.textCnpj.Leave += new System.EventHandler(this.textCnpj_Leave);
            // 
            // textFantasia
            // 
            this.textFantasia.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.textFantasia.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textFantasia.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textFantasia.Location = new System.Drawing.Point(150, 34);
            this.textFantasia.MaxLength = 100;
            this.textFantasia.Name = "textFantasia";
            this.textFantasia.Size = new System.Drawing.Size(619, 21);
            this.textFantasia.TabIndex = 24;
            // 
            // textCliente
            // 
            this.textCliente.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.textCliente.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textCliente.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.textCliente.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textCliente.Location = new System.Drawing.Point(150, 14);
            this.textCliente.MaxLength = 100;
            this.textCliente.Name = "textCliente";
            this.textCliente.Size = new System.Drawing.Size(619, 21);
            this.textCliente.TabIndex = 22;
            // 
            // lblCliente
            // 
            this.lblCliente.BackColor = System.Drawing.Color.LightGray;
            this.lblCliente.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblCliente.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCliente.Location = new System.Drawing.Point(3, 14);
            this.lblCliente.MaxLength = 100;
            this.lblCliente.Name = "lblCliente";
            this.lblCliente.ReadOnly = true;
            this.lblCliente.Size = new System.Drawing.Size(148, 21);
            this.lblCliente.TabIndex = 33;
            this.lblCliente.TabStop = false;
            this.lblCliente.Text = "Cliente";
            // 
            // lblFantasia
            // 
            this.lblFantasia.BackColor = System.Drawing.Color.LightGray;
            this.lblFantasia.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblFantasia.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblFantasia.Location = new System.Drawing.Point(3, 34);
            this.lblFantasia.MaxLength = 100;
            this.lblFantasia.Name = "lblFantasia";
            this.lblFantasia.ReadOnly = true;
            this.lblFantasia.Size = new System.Drawing.Size(148, 21);
            this.lblFantasia.TabIndex = 34;
            this.lblFantasia.TabStop = false;
            this.lblFantasia.Text = "Nome de fantasia";
            // 
            // lblCnpj
            // 
            this.lblCnpj.BackColor = System.Drawing.Color.LightGray;
            this.lblCnpj.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblCnpj.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCnpj.Location = new System.Drawing.Point(3, 54);
            this.lblCnpj.MaxLength = 100;
            this.lblCnpj.Name = "lblCnpj";
            this.lblCnpj.ReadOnly = true;
            this.lblCnpj.Size = new System.Drawing.Size(148, 21);
            this.lblCnpj.TabIndex = 35;
            this.lblCnpj.TabStop = false;
            this.lblCnpj.Text = "CNPJ";
            // 
            // lblCidade
            // 
            this.lblCidade.BackColor = System.Drawing.Color.LightGray;
            this.lblCidade.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblCidade.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCidade.Location = new System.Drawing.Point(3, 94);
            this.lblCidade.MaxLength = 100;
            this.lblCidade.Name = "lblCidade";
            this.lblCidade.ReadOnly = true;
            this.lblCidade.Size = new System.Drawing.Size(148, 21);
            this.lblCidade.TabIndex = 36;
            this.lblCidade.TabStop = false;
            this.lblCidade.Text = "Cidade";
            // 
            // lblUf
            // 
            this.lblUf.BackColor = System.Drawing.Color.LightGray;
            this.lblUf.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblUf.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblUf.Location = new System.Drawing.Point(3, 74);
            this.lblUf.MaxLength = 100;
            this.lblUf.Name = "lblUf";
            this.lblUf.ReadOnly = true;
            this.lblUf.Size = new System.Drawing.Size(148, 21);
            this.lblUf.TabIndex = 37;
            this.lblUf.TabStop = false;
            this.lblUf.Text = "Unidade federativa";
            // 
            // cbCidade
            // 
            this.cbCidade.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.cbCidade.BorderColor = System.Drawing.Color.DimGray;
            this.cbCidade.BorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid;
            this.cbCidade.FormattingEnabled = true;
            this.cbCidade.Location = new System.Drawing.Point(150, 94);
            this.cbCidade.Name = "cbCidade";
            this.cbCidade.Size = new System.Drawing.Size(619, 21);
            this.cbCidade.TabIndex = 38;
            this.cbCidade.Click += new System.EventHandler(this.cbCidade_Click);
            // 
            // cbUf
            // 
            this.cbUf.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.cbUf.BorderColor = System.Drawing.Color.DimGray;
            this.cbUf.BorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid;
            this.cbUf.FormattingEnabled = true;
            this.cbUf.Location = new System.Drawing.Point(150, 74);
            this.cbUf.Name = "cbUf";
            this.cbUf.Size = new System.Drawing.Size(619, 21);
            this.cbUf.TabIndex = 39;
            this.cbUf.SelectedIndexChanged += new System.EventHandler(this.cbUf_SelectedIndexChanged);
            // 
            // lblTipoCliente
            // 
            this.lblTipoCliente.BackColor = System.Drawing.Color.LightGray;
            this.lblTipoCliente.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblTipoCliente.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTipoCliente.Location = new System.Drawing.Point(3, 114);
            this.lblTipoCliente.MaxLength = 100;
            this.lblTipoCliente.Name = "lblTipoCliente";
            this.lblTipoCliente.ReadOnly = true;
            this.lblTipoCliente.Size = new System.Drawing.Size(148, 21);
            this.lblTipoCliente.TabIndex = 40;
            this.lblTipoCliente.TabStop = false;
            this.lblTipoCliente.Text = "Tipo de cliente";
            // 
            // cbTipoCliente
            // 
            this.cbTipoCliente.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.cbTipoCliente.BorderColor = System.Drawing.Color.DimGray;
            this.cbTipoCliente.BorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid;
            this.cbTipoCliente.FormattingEnabled = true;
            this.cbTipoCliente.Location = new System.Drawing.Point(150, 114);
            this.cbTipoCliente.Name = "cbTipoCliente";
            this.cbTipoCliente.Size = new System.Drawing.Size(619, 21);
            this.cbTipoCliente.TabIndex = 41;
            this.cbTipoCliente.SelectedIndexChanged += new System.EventHandler(this.cbTipoCliente_SelectedIndexChanged);
            // 
            // lblSituacao
            // 
            this.lblSituacao.BackColor = System.Drawing.Color.LightGray;
            this.lblSituacao.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblSituacao.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSituacao.Location = new System.Drawing.Point(3, 134);
            this.lblSituacao.MaxLength = 100;
            this.lblSituacao.Name = "lblSituacao";
            this.lblSituacao.ReadOnly = true;
            this.lblSituacao.Size = new System.Drawing.Size(148, 21);
            this.lblSituacao.TabIndex = 42;
            this.lblSituacao.TabStop = false;
            this.lblSituacao.Text = "Situação";
            // 
            // cbSituacao
            // 
            this.cbSituacao.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.cbSituacao.BorderColor = System.Drawing.Color.DimGray;
            this.cbSituacao.BorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid;
            this.cbSituacao.FormattingEnabled = true;
            this.cbSituacao.Location = new System.Drawing.Point(150, 134);
            this.cbSituacao.Name = "cbSituacao";
            this.cbSituacao.Size = new System.Drawing.Size(619, 21);
            this.cbSituacao.TabIndex = 43;
            // 
            // lblDataCadastro
            // 
            this.lblDataCadastro.BackColor = System.Drawing.Color.LightGray;
            this.lblDataCadastro.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblDataCadastro.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDataCadastro.Location = new System.Drawing.Point(3, 154);
            this.lblDataCadastro.MaxLength = 100;
            this.lblDataCadastro.Name = "lblDataCadastro";
            this.lblDataCadastro.ReadOnly = true;
            this.lblDataCadastro.Size = new System.Drawing.Size(148, 21);
            this.lblDataCadastro.TabIndex = 44;
            this.lblDataCadastro.TabStop = false;
            this.lblDataCadastro.Text = "Período de cadastro";
            // 
            // grbCliente
            // 
            this.grbCliente.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.grbCliente.BackColor = System.Drawing.Color.White;
            this.grbCliente.Controls.Add(this.dgvCliente);
            this.grbCliente.Location = new System.Drawing.Point(3, 181);
            this.grbCliente.Name = "grbCliente";
            this.grbCliente.Size = new System.Drawing.Size(768, 253);
            this.grbCliente.TabIndex = 45;
            this.grbCliente.TabStop = false;
            this.grbCliente.Text = "Clientes";
            // 
            // dgvCliente
            // 
            this.dgvCliente.AllowUserToAddRows = false;
            this.dgvCliente.AllowUserToDeleteRows = false;
            this.dgvCliente.AllowUserToOrderColumns = true;
            this.dgvCliente.AllowUserToResizeRows = false;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.Gainsboro;
            this.dgvCliente.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle1;
            this.dgvCliente.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this.dgvCliente.BackgroundColor = System.Drawing.Color.White;
            this.dgvCliente.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.Color.LightGray;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvCliente.DefaultCellStyle = dataGridViewCellStyle2;
            this.dgvCliente.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvCliente.Location = new System.Drawing.Point(3, 16);
            this.dgvCliente.MultiSelect = false;
            this.dgvCliente.Name = "dgvCliente";
            this.dgvCliente.ReadOnly = true;
            this.dgvCliente.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvCliente.Size = new System.Drawing.Size(762, 234);
            this.dgvCliente.TabIndex = 14;
            this.dgvCliente.CellMouseDoubleClick += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.dgvCliente_CellMouseDoubleClick);
            this.dgvCliente.RowPrePaint += new System.Windows.Forms.DataGridViewRowPrePaintEventHandler(this.dgvCliente_RowPrePaint);
            // 
            // lblMarcInativo
            // 
            this.lblMarcInativo.AutoSize = true;
            this.lblMarcInativo.BackColor = System.Drawing.Color.Red;
            this.lblMarcInativo.Location = new System.Drawing.Point(9, 442);
            this.lblMarcInativo.Name = "lblMarcInativo";
            this.lblMarcInativo.Size = new System.Drawing.Size(13, 13);
            this.lblMarcInativo.TabIndex = 46;
            this.lblMarcInativo.Text = "  ";
            // 
            // lblInativo
            // 
            this.lblInativo.AutoSize = true;
            this.lblInativo.Location = new System.Drawing.Point(28, 442);
            this.lblInativo.Name = "lblInativo";
            this.lblInativo.Size = new System.Drawing.Size(86, 13);
            this.lblInativo.TabIndex = 47;
            this.lblInativo.Text = "Clientes inativos.";
            // 
            // frmClientePrincipal
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.ClientSize = new System.Drawing.Size(784, 561);
            this.Name = "frmClientePrincipal";
            this.Text = "GERENCIAR CLIENTES";
            this.spForm.Panel1.ResumeLayout(false);
            this.spForm.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.spForm)).EndInit();
            this.spForm.ResumeLayout(false);
            this.pnlForm.ResumeLayout(false);
            this.pnlForm.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.banner)).EndInit();
            this.flpAcao.ResumeLayout(false);
            this.grbCliente.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvCliente)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.FlowLayoutPanel flpAcao;
        private System.Windows.Forms.Button bt_incluir;
        private System.Windows.Forms.Button bt_alterar;
        private System.Windows.Forms.Button bt_excluir;
        private System.Windows.Forms.Button btn_reativar;
        private System.Windows.Forms.Button bt_pesquisar;
        private System.Windows.Forms.Button bt_detalhar;
        private System.Windows.Forms.Button bt_limpar;
        private System.Windows.Forms.Button bt_fechar;
        private System.Windows.Forms.TextBox lblUf;
        private System.Windows.Forms.TextBox lblCidade;
        private System.Windows.Forms.TextBox lblCnpj;
        private System.Windows.Forms.TextBox lblFantasia;
        private System.Windows.Forms.TextBox lblCliente;
        private System.Windows.Forms.DateTimePicker dataInicial;
        private System.Windows.Forms.DateTimePicker dataFinal;
        private System.Windows.Forms.MaskedTextBox textCnpj;
        private System.Windows.Forms.TextBox textFantasia;
        private System.Windows.Forms.TextBox textCliente;
        private ComboBoxWithBorder cbSituacao;
        private System.Windows.Forms.TextBox lblDataCadastro;
        private System.Windows.Forms.TextBox lblSituacao;
        private ComboBoxWithBorder cbTipoCliente;
        private System.Windows.Forms.TextBox lblTipoCliente;
        private ComboBoxWithBorder cbUf;
        private ComboBoxWithBorder cbCidade;
        private System.Windows.Forms.GroupBox grbCliente;
        private System.Windows.Forms.DataGridView dgvCliente;
        private System.Windows.Forms.Label lblInativo;
        private System.Windows.Forms.Label lblMarcInativo;
    }
}
