﻿using SWS.Entidade;
using SWS.Facade;
using SWS.View.Resources;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SWS.View
{
    public partial class frmPerfilSelecionar : frmTemplateConsulta
    {
        HashSet<Perfil> perfil = new HashSet<Perfil>();

        public HashSet<Perfil> Perfil
        {
            get { return perfil; }
            set { perfil = value; }
        }

        private void validaPermissoes()
        {
            PermissionamentoFacade permissionamentoFacade = PermissionamentoFacade.getInstance();
            this.btnIncluir.Enabled = permissionamentoFacade.hasPermission("PERFIL", "INCLUIR");
        }
        
        
        public frmPerfilSelecionar()
        {
            InitializeComponent();
            validaPermissoes();
            montaGrid();
            
        }

        private void montaGrid()
        {
            try
            {
                Perfil perfil = new Perfil();
                perfil.Situacao = ApplicationConstants.ATIVO;

                dgvPerfil.Columns.Clear();

                dgvPerfil.ColumnCount = 2;

                PermissionamentoFacade permissionamentoFacade = PermissionamentoFacade.getInstance();
                this.Cursor = Cursors.WaitCursor;
                DataSet ds = permissionamentoFacade.findPerfilByFilter(perfil);

                dgvPerfil.Columns[0].HeaderText = "idPerfil";
                dgvPerfil.Columns[0].Visible = false;
                dgvPerfil.Columns[1].HeaderText = "Perfil";

                foreach (DataRow row in ds.Tables[0].Rows)
                {
                    dgvPerfil.Rows.Add(
                        row["ID_PERFIL"],
                        row["DESCRICAO"].ToString()
                        );
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Atenção", ex.Message, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }

        private void btnFechar_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnConfirmar_Click(object sender, EventArgs e)
        {
            try
            {
                if (dgvPerfil.CurrentRow == null)
                    throw new Exception("Selecione um ou mais de um perfil.");

                foreach (DataGridViewRow row in dgvPerfil.SelectedRows)
                {
                    Perfil perfil = new Perfil();
                    perfil.Id = (long)dgvPerfil.Rows[row.Index].Cells[0].Value;
                    perfil.Descricao = dgvPerfil.Rows[row.Index].Cells[1].Value.ToString();
                    perfil.Situacao = ApplicationConstants.ATIVO;

                    Perfil.Add(perfil);
                }
                this.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Atenção", ex.Message, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            
        }

        private void btnIncluir_Click(object sender, EventArgs e)
        {
            frmPerfilIncluir incluirPerfil = new frmPerfilIncluir();
            incluirPerfil.ShowDialog();

            if (incluirPerfil.Perfil != null)
            {
                this.Perfil.Add(incluirPerfil.Perfil);
                this.Close();
            }
        }
    }
}
