﻿namespace SWS.View
{
    partial class frmEsqueciSenha
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.flpAcao = new System.Windows.Forms.FlowLayoutPanel();
            this.btRecuperar = new System.Windows.Forms.Button();
            this.btSair = new System.Windows.Forms.Button();
            this.lblNome = new System.Windows.Forms.TextBox();
            this.lblLogin = new System.Windows.Forms.TextBox();
            this.lblCPF = new System.Windows.Forms.TextBox();
            this.textNome = new System.Windows.Forms.TextBox();
            this.textLogin = new System.Windows.Forms.TextBox();
            this.textCPF = new System.Windows.Forms.MaskedTextBox();
            ((System.ComponentModel.ISupportInitialize)(this.scForm)).BeginInit();
            this.scForm.Panel1.SuspendLayout();
            this.scForm.Panel2.SuspendLayout();
            this.scForm.SuspendLayout();
            this.pnlForm.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.banner)).BeginInit();
            this.flpAcao.SuspendLayout();
            this.SuspendLayout();
            // 
            // scForm
            // 
            // 
            // scForm.Panel1
            // 
            this.scForm.Panel1.Controls.Add(this.flpAcao);
            // 
            // pnlForm
            // 
            this.pnlForm.Controls.Add(this.textCPF);
            this.pnlForm.Controls.Add(this.textLogin);
            this.pnlForm.Controls.Add(this.textNome);
            this.pnlForm.Controls.Add(this.lblCPF);
            this.pnlForm.Controls.Add(this.lblLogin);
            this.pnlForm.Controls.Add(this.lblNome);
            // 
            // flpAcao
            // 
            this.flpAcao.Controls.Add(this.btRecuperar);
            this.flpAcao.Controls.Add(this.btSair);
            this.flpAcao.Dock = System.Windows.Forms.DockStyle.Fill;
            this.flpAcao.Location = new System.Drawing.Point(0, 0);
            this.flpAcao.Name = "flpAcao";
            this.flpAcao.Size = new System.Drawing.Size(534, 30);
            this.flpAcao.TabIndex = 0;
            // 
            // btRecuperar
            // 
            this.btRecuperar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btRecuperar.Image = global::SWS.Properties.Resources.devolucao;
            this.btRecuperar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btRecuperar.Location = new System.Drawing.Point(3, 3);
            this.btRecuperar.Name = "btRecuperar";
            this.btRecuperar.Size = new System.Drawing.Size(75, 23);
            this.btRecuperar.TabIndex = 0;
            this.btRecuperar.TabStop = false;
            this.btRecuperar.Text = "Recuperar";
            this.btRecuperar.UseVisualStyleBackColor = true;
            this.btRecuperar.Click += new System.EventHandler(this.btRecuperar_Click);
            // 
            // btSair
            // 
            this.btSair.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btSair.Image = global::SWS.Properties.Resources.close;
            this.btSair.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btSair.Location = new System.Drawing.Point(84, 3);
            this.btSair.Name = "btSair";
            this.btSair.Size = new System.Drawing.Size(75, 23);
            this.btSair.TabIndex = 1;
            this.btSair.TabStop = false;
            this.btSair.Text = "Sair";
            this.btSair.UseVisualStyleBackColor = true;
            this.btSair.Click += new System.EventHandler(this.btSair_Click);
            // 
            // lblNome
            // 
            this.lblNome.BackColor = System.Drawing.Color.LightGray;
            this.lblNome.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblNome.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNome.Location = new System.Drawing.Point(12, 12);
            this.lblNome.Name = "lblNome";
            this.lblNome.ReadOnly = true;
            this.lblNome.Size = new System.Drawing.Size(100, 21);
            this.lblNome.TabIndex = 0;
            this.lblNome.TabStop = false;
            this.lblNome.Text = "Nome";
            // 
            // lblLogin
            // 
            this.lblLogin.BackColor = System.Drawing.Color.LightGray;
            this.lblLogin.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblLogin.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblLogin.Location = new System.Drawing.Point(12, 32);
            this.lblLogin.Name = "lblLogin";
            this.lblLogin.ReadOnly = true;
            this.lblLogin.Size = new System.Drawing.Size(100, 21);
            this.lblLogin.TabIndex = 1;
            this.lblLogin.TabStop = false;
            this.lblLogin.Text = "Login";
            // 
            // lblCPF
            // 
            this.lblCPF.BackColor = System.Drawing.Color.LightGray;
            this.lblCPF.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblCPF.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCPF.Location = new System.Drawing.Point(12, 52);
            this.lblCPF.Name = "lblCPF";
            this.lblCPF.ReadOnly = true;
            this.lblCPF.Size = new System.Drawing.Size(100, 21);
            this.lblCPF.TabIndex = 2;
            this.lblCPF.TabStop = false;
            this.lblCPF.Text = "CPF";
            // 
            // textNome
            // 
            this.textNome.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textNome.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.textNome.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textNome.Location = new System.Drawing.Point(111, 12);
            this.textNome.MaxLength = 100;
            this.textNome.Name = "textNome";
            this.textNome.Size = new System.Drawing.Size(371, 21);
            this.textNome.TabIndex = 1;
            // 
            // textLogin
            // 
            this.textLogin.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textLogin.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.textLogin.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textLogin.Location = new System.Drawing.Point(111, 32);
            this.textLogin.MaxLength = 15;
            this.textLogin.Name = "textLogin";
            this.textLogin.Size = new System.Drawing.Size(371, 21);
            this.textLogin.TabIndex = 2;
            // 
            // textCPF
            // 
            this.textCPF.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textCPF.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textCPF.Location = new System.Drawing.Point(111, 52);
            this.textCPF.Mask = "999,999,999-99";
            this.textCPF.Name = "textCPF";
            this.textCPF.PromptChar = ' ';
            this.textCPF.Size = new System.Drawing.Size(371, 21);
            this.textCPF.TabIndex = 3;
            // 
            // frmEsqueciSenha
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(534, 361);
            this.Name = "frmEsqueciSenha";
            this.Text = "RESETAR MINHA SENHA";
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.frmEsqueciSenha_KeyDown);
            this.scForm.Panel1.ResumeLayout(false);
            this.scForm.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.scForm)).EndInit();
            this.scForm.ResumeLayout(false);
            this.pnlForm.ResumeLayout(false);
            this.pnlForm.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.banner)).EndInit();
            this.flpAcao.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.FlowLayoutPanel flpAcao;
        private System.Windows.Forms.MaskedTextBox textCPF;
        private System.Windows.Forms.TextBox textLogin;
        private System.Windows.Forms.TextBox textNome;
        private System.Windows.Forms.TextBox lblCPF;
        private System.Windows.Forms.TextBox lblLogin;
        private System.Windows.Forms.TextBox lblNome;
        private System.Windows.Forms.Button btRecuperar;
        private System.Windows.Forms.Button btSair;
    }
}