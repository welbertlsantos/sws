﻿namespace SegSis.View
{
    partial class frm_FuncionarioClienteAtivoIncluir
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frm_FuncionarioClienteAtivoIncluir));
            this.grb_paginacao = new System.Windows.Forms.GroupBox();
            this.btn_novo = new System.Windows.Forms.Button();
            this.btn_fechar = new System.Windows.Forms.Button();
            this.btn_ok = new System.Windows.Forms.Button();
            this.grb_gride = new System.Windows.Forms.GroupBox();
            this.grd_cliente = new System.Windows.Forms.DataGridView();
            this.grb_dados = new System.Windows.Forms.GroupBox();
            this.lbl_Fantasia = new System.Windows.Forms.Label();
            this.text_fantasia = new System.Windows.Forms.TextBox();
            this.btn_buscar = new System.Windows.Forms.Button();
            this.lbl_razaoSocial = new System.Windows.Forms.Label();
            this.text_razaoSocial = new System.Windows.Forms.TextBox();
            this.grb_paginacao.SuspendLayout();
            this.grb_gride.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grd_cliente)).BeginInit();
            this.grb_dados.SuspendLayout();
            this.SuspendLayout();
            // 
            // grb_paginacao
            // 
            this.grb_paginacao.Controls.Add(this.btn_novo);
            this.grb_paginacao.Controls.Add(this.btn_fechar);
            this.grb_paginacao.Controls.Add(this.btn_ok);
            this.grb_paginacao.Location = new System.Drawing.Point(4, 351);
            this.grb_paginacao.Name = "grb_paginacao";
            this.grb_paginacao.Size = new System.Drawing.Size(593, 47);
            this.grb_paginacao.TabIndex = 46;
            this.grb_paginacao.TabStop = false;
            // 
            // btn_novo
            // 
            this.btn_novo.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_novo.Image = global::SegSis.Properties.Resources.icone_mais;
            this.btn_novo.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btn_novo.Location = new System.Drawing.Point(89, 18);
            this.btn_novo.Name = "btn_novo";
            this.btn_novo.Size = new System.Drawing.Size(77, 23);
            this.btn_novo.TabIndex = 6;
            this.btn_novo.Text = "&Novo";
            this.btn_novo.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btn_novo.UseVisualStyleBackColor = true;
            this.btn_novo.Click += new System.EventHandler(this.btn_novo_Click);
            // 
            // btn_fechar
            // 
            this.btn_fechar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_fechar.Image = global::SegSis.Properties.Resources.close;
            this.btn_fechar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btn_fechar.Location = new System.Drawing.Point(172, 18);
            this.btn_fechar.Name = "btn_fechar";
            this.btn_fechar.Size = new System.Drawing.Size(75, 23);
            this.btn_fechar.TabIndex = 7;
            this.btn_fechar.Text = "&Fechar";
            this.btn_fechar.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btn_fechar.UseVisualStyleBackColor = true;
            this.btn_fechar.Click += new System.EventHandler(this.btn_fechar_Click);
            // 
            // btn_ok
            // 
            this.btn_ok.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_ok.Image = global::SegSis.Properties.Resources.fechar_ico;
            this.btn_ok.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btn_ok.Location = new System.Drawing.Point(6, 18);
            this.btn_ok.Name = "btn_ok";
            this.btn_ok.Size = new System.Drawing.Size(77, 23);
            this.btn_ok.TabIndex = 5;
            this.btn_ok.Text = "&Ok";
            this.btn_ok.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btn_ok.UseVisualStyleBackColor = true;
            this.btn_ok.Click += new System.EventHandler(this.btn_ok_Click);
            // 
            // grb_gride
            // 
            this.grb_gride.BackColor = System.Drawing.Color.Transparent;
            this.grb_gride.Controls.Add(this.grd_cliente);
            this.grb_gride.Location = new System.Drawing.Point(4, 139);
            this.grb_gride.Name = "grb_gride";
            this.grb_gride.Size = new System.Drawing.Size(593, 206);
            this.grb_gride.TabIndex = 45;
            this.grb_gride.TabStop = false;
            this.grb_gride.Text = "Clientes";
            // 
            // grd_cliente
            // 
            this.grd_cliente.AllowUserToAddRows = false;
            this.grd_cliente.AllowUserToDeleteRows = false;
            this.grd_cliente.AllowUserToOrderColumns = true;
            this.grd_cliente.AllowUserToResizeRows = false;
            this.grd_cliente.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this.grd_cliente.BackgroundColor = System.Drawing.Color.White;
            this.grd_cliente.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.grd_cliente.Location = new System.Drawing.Point(6, 19);
            this.grd_cliente.MultiSelect = false;
            this.grd_cliente.Name = "grd_cliente";
            this.grd_cliente.ReadOnly = true;
            this.grd_cliente.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.grd_cliente.Size = new System.Drawing.Size(581, 181);
            this.grd_cliente.TabIndex = 4;
            // 
            // grb_dados
            // 
            this.grb_dados.Controls.Add(this.lbl_Fantasia);
            this.grb_dados.Controls.Add(this.text_fantasia);
            this.grb_dados.Controls.Add(this.btn_buscar);
            this.grb_dados.Controls.Add(this.lbl_razaoSocial);
            this.grb_dados.Controls.Add(this.text_razaoSocial);
            this.grb_dados.Location = new System.Drawing.Point(4, 3);
            this.grb_dados.Name = "grb_dados";
            this.grb_dados.Size = new System.Drawing.Size(593, 130);
            this.grb_dados.TabIndex = 44;
            this.grb_dados.TabStop = false;
            this.grb_dados.Text = "Dados";
            // 
            // lbl_Fantasia
            // 
            this.lbl_Fantasia.AutoSize = true;
            this.lbl_Fantasia.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_Fantasia.Location = new System.Drawing.Point(3, 55);
            this.lbl_Fantasia.Name = "lbl_Fantasia";
            this.lbl_Fantasia.Size = new System.Drawing.Size(93, 13);
            this.lbl_Fantasia.TabIndex = 21;
            this.lbl_Fantasia.Text = "Nome de Fantasia";
            // 
            // text_fantasia
            // 
            this.text_fantasia.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.text_fantasia.Location = new System.Drawing.Point(6, 71);
            this.text_fantasia.MaxLength = 100;
            this.text_fantasia.Name = "text_fantasia";
            this.text_fantasia.Size = new System.Drawing.Size(577, 20);
            this.text_fantasia.TabIndex = 2;
            // 
            // btn_buscar
            // 
            this.btn_buscar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_buscar.Image = global::SegSis.Properties.Resources.lupa;
            this.btn_buscar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btn_buscar.Location = new System.Drawing.Point(6, 97);
            this.btn_buscar.Name = "btn_buscar";
            this.btn_buscar.Size = new System.Drawing.Size(77, 23);
            this.btn_buscar.TabIndex = 3;
            this.btn_buscar.Text = "&Buscar";
            this.btn_buscar.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btn_buscar.UseVisualStyleBackColor = true;
            this.btn_buscar.Click += new System.EventHandler(this.btn_buscar_Click);
            // 
            // lbl_razaoSocial
            // 
            this.lbl_razaoSocial.AutoSize = true;
            this.lbl_razaoSocial.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_razaoSocial.Location = new System.Drawing.Point(3, 16);
            this.lbl_razaoSocial.Name = "lbl_razaoSocial";
            this.lbl_razaoSocial.Size = new System.Drawing.Size(70, 13);
            this.lbl_razaoSocial.TabIndex = 7;
            this.lbl_razaoSocial.Text = "Razão Social";
            // 
            // text_razaoSocial
            // 
            this.text_razaoSocial.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.text_razaoSocial.Location = new System.Drawing.Point(6, 32);
            this.text_razaoSocial.MaxLength = 100;
            this.text_razaoSocial.Name = "text_razaoSocial";
            this.text_razaoSocial.Size = new System.Drawing.Size(577, 20);
            this.text_razaoSocial.TabIndex = 1;
            // 
            // frm_FuncionarioClienteAtivoIncluir
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(600, 400);
            this.ControlBox = false;
            this.Controls.Add(this.grb_paginacao);
            this.Controls.Add(this.grb_gride);
            this.Controls.Add(this.grb_dados);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "frm_FuncionarioClienteAtivoIncluir";
            this.Text = "PESQUISAR CLIENTES";
            this.grb_paginacao.ResumeLayout(false);
            this.grb_gride.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.grd_cliente)).EndInit();
            this.grb_dados.ResumeLayout(false);
            this.grb_dados.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox grb_paginacao;
        private System.Windows.Forms.Button btn_fechar;
        private System.Windows.Forms.Button btn_ok;
        private System.Windows.Forms.GroupBox grb_gride;
        private System.Windows.Forms.DataGridView grd_cliente;
        private System.Windows.Forms.GroupBox grb_dados;
        private System.Windows.Forms.Label lbl_Fantasia;
        private System.Windows.Forms.TextBox text_fantasia;
        private System.Windows.Forms.Button btn_buscar;
        private System.Windows.Forms.Label lbl_razaoSocial;
        private System.Windows.Forms.TextBox text_razaoSocial;
        private System.Windows.Forms.Button btn_novo;
    }
}