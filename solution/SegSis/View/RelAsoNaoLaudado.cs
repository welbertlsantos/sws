﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using SWS.Entidade;
using System.Diagnostics;
using SWS.View.Resources;

namespace SWS.View
{
    public partial class frmRelLaudoAtendimento : BaseFormConsulta
    {
        
        Cliente cliente;

        static String msg1 = "A data final não pode ser menor do que a data inicial.";
        
        public frmRelLaudoAtendimento()
        {
            InitializeComponent();
        }

        private void btnPesquisaCliente_Click(object sender, EventArgs e)
        {
            try
            {
                frmClienteSelecionar formCliente = new frmClienteSelecionar(null, null, false, null, null, null, null, null, null);
                formCliente.ShowDialog();

                if (formCliente.Cliente != null)
                {
                    cliente = formCliente.Cliente;
                    textCliente.Text = cliente.RazaoSocial;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }

        }

        public Boolean validaDados()
        {
            Boolean retorno = false;
            try
            {

                if (Convert.ToDateTime(dataInicial.Text) > Convert.ToDateTime(dataFinal.Text))
                {
                    throw new Exception(msg1);
                }

            }
            catch (Exception ex)
            {
                retorno = true;
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }

            return retorno;
        }

        private void btnImprime_Click(object sender, EventArgs e)
        {
            try
            {
                if (!validaDados())
                {
                    this.Cursor = Cursors.WaitCursor;

                    String nomeRelatorio = "ATENDIMENTO_LAUDO.JASPER";
                    String situacao = rbLaudados.Checked ? ApplicationConstants.LAUDADO : rbNaoLaudado.Checked ? ApplicationConstants.NAO_LAUDADO : ApplicationConstants.TODOS;
                    String ordenacao = rbAtendimento.Checked ? ApplicationConstants.ATENDIMENTO_RELATORIO : ApplicationConstants.ATENDIMENTO_EXAME_RELATORIO ;
                    String tipo = rbSintetico.Checked ? ApplicationConstants.SINTETICO : ApplicationConstants.ANALITICO;
                    Int64? id_cliente = cliente == null ? (Int64?)0 : cliente.Id;

                    var process = new Process();

                    process.StartInfo.FileName = "Relatorio\\jre7\\bin\\java";
                    process.StartInfo.Arguments = " -jar Relatorio\\MiddlewareJasper.jar " + "Relatorio\\Report\\" + nomeRelatorio + " ID_CLIENTE:" + id_cliente + ":Integer" + " SITUACAO:" + situacao + ":String" + " ORDENACAO:" + ordenacao + ":String" + " DT_INICIAL:" + Convert.ToDateTime(dataInicial.Text).ToShortDateString() + ":String" + " DT_FINAL:" + Convert.ToDateTime(dataFinal.Text).ToShortDateString() + ":String" + " TIPO:" + tipo + ":String" ;
                    process.StartInfo.UseShellExecute = false;
                    process.StartInfo.RedirectStandardError = true;
                    process.StartInfo.RedirectStandardInput = true;
                    process.StartInfo.RedirectStandardOutput = true;
                    process.StartInfo.CreateNoWindow = true;

                    process.Start();
                    process.StandardOutput.ReadToEnd();
                    process.WaitForExit();

                }

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }

        private void btnFechar_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void rbSintetico_CheckedChanged(object sender, EventArgs e)
        {
            if (rbSintetico.Checked)
            {
                rbAtendimentoExame.Checked = false;
                rbAtendimentoExame.Enabled = false;
            }
            else
            {
                rbAtendimentoExame.Enabled = true;
            }
        }
    }
}
