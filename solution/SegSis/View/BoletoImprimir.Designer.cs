﻿namespace SWS.View
{
    partial class frmBoletoImprimir
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.flpAcao = new System.Windows.Forms.FlowLayoutPanel();
            this.btnFechar = new System.Windows.Forms.Button();
            this.btnGerar = new System.Windows.Forms.Button();
            this.webImprimirBoleto = new System.Windows.Forms.WebBrowser();
            ((System.ComponentModel.ISupportInitialize)(this.spForm)).BeginInit();
            this.spForm.Panel1.SuspendLayout();
            this.spForm.Panel2.SuspendLayout();
            this.spForm.SuspendLayout();
            this.pnlForm.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.banner)).BeginInit();
            this.flpAcao.SuspendLayout();
            this.SuspendLayout();
            // 
            // spForm
            // 
            // 
            // spForm.Panel1
            // 
            this.spForm.Panel1.Controls.Add(this.flpAcao);
            // 
            // pnlForm
            // 
            this.pnlForm.Controls.Add(this.webImprimirBoleto);
            // 
            // flpAcao
            // 
            this.flpAcao.Controls.Add(this.btnGerar);
            this.flpAcao.Controls.Add(this.btnFechar);
            this.flpAcao.Dock = System.Windows.Forms.DockStyle.Fill;
            this.flpAcao.Location = new System.Drawing.Point(0, 0);
            this.flpAcao.Name = "flpAcao";
            this.flpAcao.Size = new System.Drawing.Size(784, 35);
            this.flpAcao.TabIndex = 0;
            // 
            // btnFechar
            // 
            this.btnFechar.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnFechar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnFechar.Image = global::SWS.Properties.Resources.close;
            this.btnFechar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnFechar.Location = new System.Drawing.Point(84, 3);
            this.btnFechar.Name = "btnFechar";
            this.btnFechar.Size = new System.Drawing.Size(75, 23);
            this.btnFechar.TabIndex = 2;
            this.btnFechar.Text = "&Fechar";
            this.btnFechar.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnFechar.UseVisualStyleBackColor = true;
            this.btnFechar.Click += new System.EventHandler(this.btnFechar_Click);
            // 
            // btnGerar
            // 
            this.btnGerar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnGerar.Image = global::SWS.Properties.Resources.Gravar;
            this.btnGerar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnGerar.Location = new System.Drawing.Point(3, 3);
            this.btnGerar.Name = "btnGerar";
            this.btnGerar.Size = new System.Drawing.Size(75, 23);
            this.btnGerar.TabIndex = 1;
            this.btnGerar.Text = "Gerar PDF";
            this.btnGerar.UseVisualStyleBackColor = true;
            this.btnGerar.Click += new System.EventHandler(this.btnGerar_Click);
            // 
            // webImprimirBoleto
            // 
            this.webImprimirBoleto.Dock = System.Windows.Forms.DockStyle.Fill;
            this.webImprimirBoleto.Location = new System.Drawing.Point(0, 0);
            this.webImprimirBoleto.MinimumSize = new System.Drawing.Size(20, 20);
            this.webImprimirBoleto.Name = "webImprimirBoleto";
            this.webImprimirBoleto.Size = new System.Drawing.Size(764, 480);
            this.webImprimirBoleto.TabIndex = 0;
            this.webImprimirBoleto.TabStop = false;
            // 
            // frmBoletoImprimir
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(784, 561);
            this.Name = "frmBoletoImprimir";
            this.Text = "IMPRESSÃO DE BOLETO";
            this.spForm.Panel1.ResumeLayout(false);
            this.spForm.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.spForm)).EndInit();
            this.spForm.ResumeLayout(false);
            this.pnlForm.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.banner)).EndInit();
            this.flpAcao.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.FlowLayoutPanel flpAcao;
        private System.Windows.Forms.Button btnGerar;
        private System.Windows.Forms.Button btnFechar;
        private System.Windows.Forms.WebBrowser webImprimirBoleto;
    }
}