﻿using SWS.Entidade;
using SWS.View.Resources;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace SWS.View
{
    public partial class frmFuncaoComentarioIncluir : SWS.View.frmTemplateConsulta
    {

        private ComentarioFuncao comentarioFuncao;

        public ComentarioFuncao ComentarioFuncao
        {
            get { return comentarioFuncao; }
            set { comentarioFuncao = value; }
        }
        
        public frmFuncaoComentarioIncluir()
        {
            InitializeComponent();
            ActiveControl = textAtividade;
        }

        protected void button2_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        protected virtual void btnGravar_Click(object sender, EventArgs e)
        {
            try
            {
                if (String.IsNullOrWhiteSpace(textAtividade.Text))
                    throw new Exception("A atividade não pode ser nula");

                ComentarioFuncao = new ComentarioFuncao(null, null, textAtividade.Text, ApplicationConstants.ATIVO);
                this.Close();

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
    }
}
