﻿using SWS.Entidade;
using SWS.Facade;
using SWS.Helper;
using SWS.View.Resources;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace SWS.View
{
    public partial class frmAtendimentoImprimir : frmTemplateConsulta
    {
        Aso atendimento;
        public static String msg1 = "";
        public static String msg2 = "Atenção";
        
        private Boolean? segundaVia;

        public frmAtendimentoImprimir(Aso atendimento, Boolean? segundaVia)
        {
            InitializeComponent();
            this.atendimento = atendimento;
            this.segundaVia = segundaVia;
            montaGridRelatorio();
        }

        public void montaGridRelatorio()
        {
            try
            {
                this.grd_relatorioASo.Columns.Clear();

                RelatorioFacade relatorioFacade = RelatorioFacade.getInstance();

                DataSet ds = relatorioFacade.findAllRelatoriosBySituacao(ApplicationConstants.ASO);

                grd_relatorioASo.DataSource = ds.Tables["Relatorios"].DefaultView;

                grd_relatorioASo.Columns[0].HeaderText = "id";
                grd_relatorioASo.Columns[0].Visible = false;
                
                grd_relatorioASo.Columns[1].HeaderText = "Nome do Relatório";
                grd_relatorioASo.Columns[1].Visible = false;

                grd_relatorioASo.Columns[2].HeaderText = "Tipo";
                grd_relatorioASo.Columns[2].Visible = false;

                grd_relatorioASo.Columns[3].HeaderText = "Data de Criação";
                
                grd_relatorioASo.Columns[4].HeaderText = "Situação";
                grd_relatorioASo.Columns[4].Visible = false;

                grd_relatorioASo.Columns[5].HeaderText = "Relatório";
                grd_relatorioASo.Columns[5].DisplayIndex = 0;

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btn_imprimir_Click(object sender, EventArgs e)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método btn_imprimir_Click");

            try
            {
                /* enviando o aso para a impressão, selecionando o nome do relatório. */

                if (grd_relatorioASo.CurrentRow == null)
                    throw new Exception("Você deve selecionar o relatório.");
                
                this.Cursor = Cursors.WaitCursor;

                String nomeRelatorio = (string)grd_relatorioASo.CurrentRow.Cells[1].Value;

                //System.Diagnostics.Process.Start("Relatorio\\jre7\\bin\\java", "-jar Relatorio\\MiddlewareJasper.jar " + "Relatorio\\Report\\" + nomeRelatorio + " ID_ASO:" + aso.getId() + ":Integer");

                var process = new Process();

                /* tempo para impressão do aso */

                //System.Threading.Thread.Sleep(60000);

                process.StartInfo.FileName = "Relatorio\\jre7\\bin\\java";
                process.StartInfo.Arguments = " -jar Relatorio\\MiddlewareJasper.jar " + "Relatorio\\Report\\" + nomeRelatorio + " ID_ASO:" + atendimento.Id + ":Integer" + " SEGUNDA_VIA:" + (segundaVia == true ? "TRUE" : "FALSE") + ":STRING";
                process.StartInfo.UseShellExecute = false;
                process.StartInfo.RedirectStandardError = true;
                process.StartInfo.RedirectStandardInput = true;
                process.StartInfo.RedirectStandardOutput = true;
                process.StartInfo.CreateNoWindow = true;

                process.Start();  
                process.StandardOutput.ReadToEnd();

                //process.WaitForExit();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK , MessageBoxIcon.Error);
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }

        private void btn_cancelar_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void grd_relatorioASo_CellMouseDoubleClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            try
            {
                btn_imprimir.PerformClick();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }
    }
}
