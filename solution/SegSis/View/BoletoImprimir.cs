﻿using BoletoNet;
using BoletoNetGerandoPDF;
using iTextSharp.text;
using iTextSharp.text.pdf;
using SWS.Entidade;
using SWS.Facade;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace SWS.View
{
    public partial class frmBoletoImprimir : frmTemplate
    {
        private Cobranca cobranca;
        
        public frmBoletoImprimir(Cobranca cobranca)
        {
            InitializeComponent();
            ActiveControl = btnGerar;
            this.cobranca = cobranca;
            GeraBoleto();
        }

        private void btnGerar_Click(object sender, EventArgs e)
        {
            string imagePath = GerarImagem();

            string pdfPath = Path.Combine(Environment.CurrentDirectory, "boleto.pdf");

            Document doc = new Document(PageSize.A4, 60, 20, 20, 20);
            try
            {
                PdfWriter.GetInstance(doc, new FileStream(pdfPath, FileMode.Create));
                doc.Open();
                iTextSharp.text.Image gif = iTextSharp.text.Image.GetInstance(imagePath);

                gif.ScaleAbsolute(484f, 626f);

                doc.Add(gif);
            }
            catch (DocumentException dex)
            {
                MessageBox.Show(dex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
                this.Close();
            }
            catch (IOException ioex)
            {
                MessageBox.Show(ioex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
                this.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
                this.Close();
            }
            finally
            {
                doc.Close();
            }

            System.Diagnostics.Process.Start(pdfPath);

        }

        private void btnFechar_Click(object sender, EventArgs e)
        {
            this.Close();
            this.Dispose();
        }

        private void GeraBoleto()
        {
            try
            {
                FinanceiroFacade financeiroFacade = FinanceiroFacade.getInstance();

                cobranca.Nota = financeiroFacade.findNotaFiscalById((long)cobranca.Nota.Id);
                cobranca.Conta = financeiroFacade.findContaById((long)cobranca.Conta.Id);

                // Cria o boleto, e passa os parâmetros usuais
                BoletoBancario boletoBanco = new BoletoBancario();
                boletoBanco.CodigoBanco = Convert.ToInt16(cobranca.Conta.Banco.CodigoBanco);

                DateTime vencimento = cobranca.DataVencimento;

                Cedente cedente = new Cedente(cobranca.Nota.Empresa.Cnpj, cobranca.Nota.Empresa.RazaoSocial, Convert.ToInt32(cobranca.Conta.AgenciaNumero).ToString(), Convert.ToInt32(cobranca.Conta.ContaNumero).ToString());

                Boleto boleto = new Boleto(vencimento, (Decimal)cobranca.ValorAjustado, cobranca.Conta.Carteira, Convert.ToString(cobranca.NossoNumero), cedente);

                cedente.Convenio = String.IsNullOrEmpty(cobranca.Conta.Convenio) ? cedente.Convenio = 0 : cedente.Convenio = Convert.ToInt32(cobranca.Conta.Convenio);

                /* variáveis auxiliares na emissão do boleto */

                String digitoAgencia = String.IsNullOrEmpty(cobranca.Conta.AgenciaDigito) ? String.Empty : cobranca.Conta.AgenciaDigito;

                String digitoConta = String.IsNullOrEmpty(cobranca.Conta.ContaDigito) ? String.Empty : cobranca.Conta.ContaDigito;

                cedente.ContaBancaria = new ContaBancaria( cobranca.Conta.AgenciaNumero, digitoAgencia, cobranca.Conta.ContaNumero, digitoConta, null);

                /* verificar o numero do documento se poderá ser
                 * concatenado para numero_documento + parcela */

                boleto.NumeroDocumento = cobranca.NumeroDocumento;
                boleto.NumeroParcela = cobranca.NumeroParcela;

                if (boletoBanco.CodigoBanco == 1)
                {
                    boleto.EspecieDocumento = new EspecieDocumento_BancoBrasil(((int)EnumEspecieDocumento_BancoBrasil.DuplicataServico).ToString());
                    boletoBanco.MostrarComprovanteEntrega = true;

                    //Na carteira 198 o código do Cedente é a conta bancária
                    cedente.Codigo = cobranca.Conta.CodigoCedenteBanco;

                    Instrucao instrucao = new Instrucao(399);
                    instrucao.Descricao = "COBRAR JUROS DEFINIDO PELO BANCO (FACP) PROCEDA OS AJUSTES DE VALORES PERTINENTES.";

                    boleto.Instrucoes.Add(instrucao);
                }
                else if (boletoBanco.CodigoBanco == 399)
                {
                    boleto.Especie = "9 - REAL";
                    boleto.Aceite = "NÃO";
                    boletoBanco.MostrarComprovanteEntrega = true;

                    cedente.Codigo = cobranca.Conta.AgenciaNumero + cobranca.Conta.ContaNumero;

                    Instrucao instrucao = new Instrucao(399);
                    instrucao.Descricao = "MULTA DE 2% APÓS O VENCIMENTO.";

                    boleto.Instrucoes.Add(instrucao);
                }

                boleto.Sacado = new Sacado(cobranca.Nota.Cliente.Cnpj, cobranca.Nota.Cliente.RazaoSocial);
                boleto.Sacado.Endereco.End = cobranca.Nota.Cliente.EnderecoCob + " " + cobranca.Nota.Cliente.NumeroCob + " " + cobranca.Nota.Cliente.ComplementoCob;
                boleto.Sacado.Endereco.Bairro = cobranca.Nota.Cliente.BairroCob;
                boleto.Sacado.Endereco.Cidade = cobranca.Nota.Cliente.CidadeIbgeCobranca.Nome;
                boleto.Sacado.Endereco.CEP = cobranca.Nota.Cliente.CepCob;
                boleto.Sacado.Endereco.UF = cobranca.Nota.Cliente.UfCob;
                boleto.Sacado.Endereco.Numero = cobranca.Nota.Cliente.NumeroCob;
                boleto.Sacado.Endereco.Complemento = cobranca.Nota.Cliente.ComplementoCob;

                boletoBanco.Boleto = boleto;

                boletoBanco.Boleto.Valida();

                StringBuilder html = new StringBuilder();

                html.Append(boletoBanco.MontaHtml());
                html.Append("</br></br></br></br></br></br></br></br></br></br>");

                string boletoPath = System.IO.Path.GetTempFileName();

                using (FileStream f = new FileStream(boletoPath, FileMode.Create))
                {
                    StreamWriter w = new StreamWriter(f, System.Text.Encoding.Default);
                    w.Write(html.ToString());
                    w.Close();
                    f.Close();
                }

                this.webImprimirBoleto.Url = new Uri(boletoPath);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
                this.Close();
            }
        }

        private string GerarImagem()
        {
            string file = string.Empty;

            try
            {
                string address = this.webImprimirBoleto.Url.ToString();
                int width = 680;
                int height = 1096;

                int webBrowserWidth = 680;
                int webBrowserHeight = 1096;

                System.Drawing.Bitmap bmp = WebsiteThumbnailImageGenerator.GetWebSiteThumbnail(address, webBrowserWidth, webBrowserHeight, width, height);

                file = Path.Combine(Environment.CurrentDirectory, "boleto.bmp");

                bmp.Save(file);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
                this.Close();
            }

            return file;
        }
    }
}
