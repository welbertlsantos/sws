﻿using SWS.Entidade;
using SWS.Facade;
using SWS.View.Resources;
using SWS.View.ViewHelper;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace SWS.View
{
    public partial class frmContratosPropostaIncluir : frmTemplate
    {
        protected Contrato proposta;
        protected Cliente cliente;
        protected ClienteProposta clienteProposta;

        public Contrato Proposta
        {
            get { return proposta; }
            set { proposta = value; }
        }

        public frmContratosPropostaIncluir()
        {
            InitializeComponent();
            
        }

        protected virtual void btnGravar_Click(object sender, EventArgs e)
        {
            try
            {
                /* inicinado método para gravar a contrato e validando informações. */

                if (validaDados())
                {
                    FinanceiroFacade financeiroFacade = FinanceiroFacade.getInstance();

                    proposta = new Contrato(null, string.Empty, cliente, PermissionamentoFacade.usuarioAutenticado, Convert.ToDateTime(dataElaboracao.Value), dataVencimento.Checked ? Convert.ToDateTime(dataVencimento.Text) : (DateTime?)null , Convert.ToDateTime(DateTime.Now), ApplicationConstants.CONTRATO_GRAVADO, textComentario.Text, null, null, string.Empty, null, false, null, null, false, false, false, null, false, Convert.ToDecimal(0), Convert.ToDecimal(0), null,  null, clienteProposta, true, textFormaPagamento.Text, null, null, string.Empty, PermissionamentoFacade.usuarioAutenticado.Empresa);

                    /* incluindo as coleções de exames e produtos */
                    
                    List<ContratoExame> exames = new List<ContratoExame>();
                    foreach (DataGridViewRow dvRow in dgvExame.Rows)
                        exames.Add(new ContratoExame(null, new Exame((Int64)dvRow.Cells[0].Value, dvRow.Cells[1].Value.ToString(), dvRow.Cells[2].Value.ToString(), (Boolean?)dvRow.Cells[3].Value, Convert.ToDecimal(dvRow.Cells[4].Value),  (Int32)dvRow.Cells[5].Value, Convert.ToDecimal(dvRow.Cells[6].Value), (Boolean)dvRow.Cells[7].Value, (Boolean)dvRow.Cells[8].Value, string.Empty, false, null, false), proposta, Convert.ToDecimal(dvRow.Cells[4].Value), PermissionamentoFacade.usuarioAutenticado, (Decimal)dvRow.Cells[6].Value, DateTime.Now, ApplicationConstants.ATIVO, true));

                    List<ContratoProduto> produtos = new List<ContratoProduto>();
                    foreach (DataGridViewRow dvRow in dgvProduto.Rows)
                        produtos.Add(new ContratoProduto(null, new Produto((Int64)dvRow.Cells[0].Value, dvRow.Cells[1].Value.ToString(), dvRow.Cells[2].Value.ToString(), Convert.ToDecimal(dvRow.Cells[3].Value), dvRow.Cells[4].Value.ToString(), Convert.ToDecimal(dvRow.Cells[5].Value), (bool)dvRow.Cells[6].Value, string.Empty), proposta, Convert.ToDecimal(dvRow.Cells[3].Value), PermissionamentoFacade.usuarioAutenticado, Convert.ToDecimal(dvRow.Cells[5].Value), DateTime.Now, ApplicationConstants.ATIVO, true)); 
                        
                    proposta = financeiroFacade.insertContrato(proposta, exames, produtos);

                    MessageBox.Show("Proposta criada com sucesso. Anote o número de identicação da proposta. " + System.Environment.NewLine + ValidaCampoHelper.RetornaCodigoEstudoFormatado(proposta.CodigoContrato), "Confirmação", MessageBoxButtons.OK, MessageBoxIcon.Information);

                    this.Close();
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }

        protected void btnFechar_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        protected void btnCliente_Click(object sender, EventArgs e)
        {
            try
            {
                if (clienteProposta != null)
                {
                    if (MessageBox.Show("A proposta já tem um cliente proposta relacionado. Deseja excluir essa informação?", "Atenção", MessageBoxButtons.YesNo, MessageBoxIcon.Question ) == DialogResult.Yes)
                    {
                        clienteProposta = null;
                        textPreCliente.Text = string.Empty;
                    }
                    else
                        throw new Exception ("A proposta não pode ter um cliente e um clienteProposta ao mesmo tempo.");
                }

                frmClienteSelecionar formCliente = new frmClienteSelecionar(null, null, false, null, true, null, null, null, true);
                formCliente.ShowDialog();

                if (formCliente.Cliente != null)
                {
                    cliente = formCliente.Cliente;
                    textCliente.Text = cliente.RazaoSocial;
                    clienteProposta = null;
                    textPreCliente.Text = string.Empty;
                    
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        protected void btnPreCliente_Click(object sender, EventArgs e)
        {
            try
            {

                if (cliente != null)
                {
                    if (MessageBox.Show("A proposta tem um cliente proposta. Deseja excluir essa informação?", "Atenção", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                    {
                        clienteProposta = null;
                        textPreCliente.Text = string.Empty;
                    }
                    else
                        throw new Exception("A proposta não pode ter um cliente e um clienteProposta ao mesmo tempo.");
                       
                }

                frmPreClienteIncluir formclienteProposta = new frmPreClienteIncluir();
                formclienteProposta.ShowDialog();

                if (formclienteProposta.ClienteProposta != null)
                {
                    clienteProposta = formclienteProposta.ClienteProposta;
                    textPreCliente.Text = clienteProposta.RazaoSocial;
                    btnImprimirClienteProposta.Enabled = true;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        protected void btnImprimirClienteProposta_Click(object sender, EventArgs e)
        {
            try
            {
                this.Cursor = Cursors.WaitCursor;

                if (clienteProposta == null)
                    throw new Exception("Não existe cliente pré selecionado.");

                var process = new System.Diagnostics.Process();

                Int64 id = (Int64)clienteProposta.Id;

                process.StartInfo.FileName = "Relatorio\\jre7\\bin\\java";
                process.StartInfo.Arguments = " -jar Relatorio\\MiddlewareJasper.jar " + "Relatorio\\Report\\" + "CLIENTE_PROPOSTA.JASPER" + " ID_CLIENTE_PROPOSTA:" + id + ":Integer";
                process.StartInfo.UseShellExecute = false;
                process.StartInfo.RedirectStandardError = true;
                process.StartInfo.RedirectStandardInput = true;
                process.StartInfo.RedirectStandardOutput = true;
                process.StartInfo.CreateNoWindow = true;
                process.Start();
                process.StandardOutput.ReadToEnd();
                process.WaitForExit();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }

        protected virtual void btnIncluirExame_Click(object sender, EventArgs e)
        {
            try
            {
                frmExameBuscar exameBuscar = new frmExameBuscar();
                exameBuscar.ShowDialog();

                if (exameBuscar.Exame != null)
                {
                    /* verificando se o exame não está presente na grid */

                    List<Exame> examesInGrid = montaListaExamesInGrid();

                    if (examesInGrid.Contains(exameBuscar.Exame))
                        throw new Exception("Exame já incluído.");

                    /* incluindo a informação do exame na grid de exame */

                    dgvExame.Rows.Add(exameBuscar.Exame.Id, exameBuscar.Exame.Descricao, exameBuscar.Exame.Situacao, exameBuscar.Exame.Laboratorio, exameBuscar.Exame.Preco, exameBuscar.Exame.Prioridade, exameBuscar.Exame.Custo, exameBuscar.Exame.Externo, exameBuscar.Exame.LiberaDocumento);

                    /* posicionando a seleção da ultima linha incluida */
                    dgvExame.CurrentCell = dgvExame[0, dgvExame.RowCount - 1];

                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        protected virtual void btnExcluirExame_Click(object sender, EventArgs e)
        {
            try
            {

                if (dgvExame.CurrentRow == null)
                    throw new Exception("Selecione um exame.");

                dgvExame.Rows.RemoveAt(dgvExame.CurrentRow.Index);

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        protected virtual void btnIncluirProduto_Click(object sender, EventArgs e)
        {
            try
            {

                frmProdutoBusca produtoBusca = new frmProdutoBusca();
                produtoBusca.ShowDialog();

                if (produtoBusca.Produto != null)
                {
                    /* verificando se o produto selecionado não está incluído. */

                    List<Produto> produtosInGrid = montaListaProdutosInGrid();

                    if (produtosInGrid.Contains(produtoBusca.Produto))
                        throw new Exception("Produto já incluído.");

                    /* incluíndo a informação na grid de produto. */

                    dgvProduto.Rows.Add(produtoBusca.Produto.Id, produtoBusca.Produto.Nome, produtoBusca.Produto.Situacao, produtoBusca.Produto.Preco, produtoBusca.Produto.Tipo, produtoBusca.Produto.Custo, produtoBusca.Produto.PadraoContrato);

                    /*posicionando a seleção na última linha */
                    dgvProduto.CurrentCell = dgvProduto[0, dgvProduto.RowCount - 1];

                }

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        protected virtual void btnExcluirProduto_Click(object sender, EventArgs e)
        {
            try
            {
                if (dgvProduto.CurrentRow == null)
                    throw new Exception("Selecione um produto.");

                dgvProduto.Rows.RemoveAt(dgvProduto.CurrentRow.Index);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        protected void textComentario_Enter(object sender, EventArgs e)
        {
            textComentario.BackColor = Color.White;
        }

        protected void textComentario_Leave(object sender, EventArgs e)
        {
            textComentario.BackColor = Color.LightSteelBlue;
        }

        protected void textFormaPagamento_Enter(object sender, EventArgs e)
        {
            textFormaPagamento.BackColor = Color.White;
        }

        protected void textFormaPagamento_Leave(object sender, EventArgs e)
        {
            textFormaPagamento.BackColor = Color.LemonChiffon;
        }

        protected virtual List<Exame> montaListaExamesInGrid()
        {
            List<Exame> examesInGrid = new List<Exame>();

            try
            {
                foreach (DataGridViewRow dvRow in dgvExame.Rows)
                    examesInGrid.Add(new Exame((long)dvRow.Cells[0].Value, dvRow.Cells[1].Value.ToString(), dvRow.Cells[2].Value.ToString(), Convert.ToBoolean(dvRow.Cells[3].Value), Convert.ToDecimal(dvRow.Cells[4].Value), Convert.ToInt32(dvRow.Cells[5].Value), Convert.ToDecimal(dvRow.Cells[6].Value), Convert.ToBoolean(dvRow.Cells[7].Value), Convert.ToBoolean(dvRow.Cells[8].Value), string.Empty, false, null, false));

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

            return examesInGrid;
        }

        protected virtual List<Produto> montaListaProdutosInGrid()
        {

            List<Produto> produtosInGrid = new List<Produto>();
            try
            {
                foreach (DataGridViewRow dvRow in dgvProduto.Rows)
                    produtosInGrid.Add(new Produto((long)dvRow.Cells[0].Value, dvRow.Cells[1].Value.ToString(), dvRow.Cells[3].Value.ToString(), Convert.ToDecimal(dvRow.Cells[3].Value), dvRow.Cells[4].Value.ToString(), Convert.ToDecimal(dvRow.Cells[5].Value), (bool)dvRow.Cells[6].Value, string.Empty));

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

            return produtosInGrid;
        }

        protected virtual void MontaDataGridExame()
        {
            try
            {
                dgvExame.Columns.Clear();
                dgvExame.ColumnCount = 9;

                dgvExame.Columns[0].HeaderText = "Código";
                dgvExame.Columns[0].ReadOnly = true;

                dgvExame.Columns[1].HeaderText = "Exame";
                dgvExame.Columns[1].ReadOnly = true;
                
                dgvExame.Columns[2].HeaderText = "Situação";
                dgvExame.Columns[2].Visible = false;
                
                dgvExame.Columns[3].HeaderText = "Exame de Laboratório";
                dgvExame.Columns[3].Visible = false;
                
                dgvExame.Columns[4].HeaderText = "Preço";
                dgvExame.Columns[4].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
                dgvExame.Columns[4].HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleRight;
                dgvExame.Columns[4].DefaultCellStyle.BackColor = Color.LemonChiffon;
                
                dgvExame.Columns[5].HeaderText = "Prioridade";
                dgvExame.Columns[5].Visible = false;
                
                dgvExame.Columns[6].HeaderText = "Custo";
                dgvExame.Columns[6].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
                dgvExame.Columns[6].HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleRight;
                dgvExame.Columns[6].DefaultCellStyle.BackColor = Color.LemonChiffon;
                
                dgvExame.Columns[7].HeaderText = "Externo";
                dgvExame.Columns[7].Visible = false;
                
                dgvExame.Columns[8].HeaderText = "Libera Documentação";
                dgvExame.Columns[8].Visible = false;

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }

        private void frmContratosPropostaIncluir_Load(object sender, EventArgs e)
        {
            MontaDataGridExame();
            MontaDataGridProduto();
        }

        protected virtual void MontaDataGridProduto()
        {
            try
            {

                dgvProduto.Columns.Clear();
                dgvProduto.ColumnCount = 7;

                dgvProduto.Columns[0].HeaderText = "Código";
                dgvProduto.Columns[0].ReadOnly = true;
                    
                dgvProduto.Columns[1].HeaderText = "Produto";
                dgvProduto.Columns[1].ReadOnly = true;
                
                dgvProduto.Columns[2].HeaderText = "Situação";
                dgvProduto.Columns[2].Visible = false;
                
                dgvProduto.Columns[3].HeaderText = "Preço R$";
                dgvProduto.Columns[3].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
                dgvProduto.Columns[3].HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleRight;
                dgvProduto.Columns[3].DefaultCellStyle.BackColor = Color.LemonChiffon;

                dgvProduto.Columns[4].HeaderText = "Tipo_Estudo";
                dgvProduto.Columns[4].Visible = false;
                
                dgvProduto.Columns[5].HeaderText = "Custo R$";
                dgvProduto.Columns[5].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
                dgvProduto.Columns[5].HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleRight;
                dgvProduto.Columns[5].DefaultCellStyle.BackColor = Color.LemonChiffon;

                dgvProduto.Columns[6].HeaderText = "Padrao Contrato";
                dgvProduto.Columns[6].Visible = false;
                

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }

        protected void dgvExame_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = ValidaCampoHelper.FormataDigitacaoNumero(sender, e, dgvExame.CurrentCell.EditedFormattedValue.ToString(), 2);
        }

        protected void dgvExame_EditingControlShowing(object sender, DataGridViewEditingControlShowingEventArgs e)
        {
            if (e.Control is DataGridViewTextBoxEditingControl)
                e.Control.KeyPress += new KeyPressEventHandler(dgvExame_KeyPress);
        }

        protected void dgvExame_CellBeginEdit(object sender, DataGridViewCellCancelEventArgs e)
        {
            dgvExame.CurrentCell.Tag = dgvExame.CurrentCell.Value;
            dgvExame.CurrentCell.Value = String.Empty;
        }

        protected virtual void dgvExame_CellEndEdit(object sender, DataGridViewCellEventArgs e)
        {
            if (String.IsNullOrEmpty(dgvExame.CurrentCell.EditedFormattedValue.ToString()))
                dgvExame.CurrentCell.Value = dgvExame.CurrentCell.Tag.ToString();

            dgvExame.CurrentCell.Value = ValidaCampoHelper.FormataValorMonetario(dgvExame.CurrentCell.Value.ToString());
        }

        protected void dgvProduto_CellBeginEdit(object sender, DataGridViewCellCancelEventArgs e)
        {
            dgvProduto.CurrentCell.Tag = dgvProduto.CurrentCell.Value;
            dgvProduto.CurrentCell.Value = String.Empty;
        }

        protected virtual void dgvProduto_CellEndEdit(object sender, DataGridViewCellEventArgs e)
        {
            if (String.IsNullOrEmpty(dgvProduto.CurrentCell.EditedFormattedValue.ToString()))
                dgvProduto.CurrentCell.Value = dgvProduto.CurrentCell.Tag.ToString();

            dgvProduto.CurrentCell.Value = ValidaCampoHelper.FormataValorMonetario(dgvProduto.CurrentCell.Value.ToString());
        }

        protected void dgvProduto_EditingControlShowing(object sender, DataGridViewEditingControlShowingEventArgs e)
        {
            if (e.Control is DataGridViewTextBoxEditingControl)
                e.Control.KeyPress += new KeyPressEventHandler(dgvProduto_KeyPress);
        }

        protected void dgvProduto_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = ValidaCampoHelper.FormataDigitacaoNumero(sender, e, dgvProduto.CurrentCell.EditedFormattedValue.ToString(), 2);
        }

        protected bool validaDados()
        {
            bool retorno = true;

            try
            {
                /* validando dados em relação ao cliente */

                /* o usuário irá gravar uma proposta. Nesse caso é obrigatório 
                 * existir um cliente ou um clienteProposta */

                if (cliente == null && clienteProposta == null)
                    throw new Exception("O cliente é obrigatório.");

                /* validando em relação a exames e ou produtos */

                if (dgvExame.Rows.Count == 0 && dgvProduto.Rows.Count == 0)
                    throw new Exception("A proposta deverá pelo menos ter um exame ou um produto cadastrado.");
            }
            catch (Exception ex)
            {
                retorno = false;
                MessageBox.Show(ex.Message, "Atençao", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }

            return retorno;
        }

    }
}
