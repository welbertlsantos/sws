﻿using SWS.Facade;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using SWS.Entidade;
using SWS.View.Resources;

namespace SWS.View
{
    public partial class frmSetorAltera : frmSetorIncluir
    {
        private bool flagAltera;

        public bool FlagAltera
        {
            get { return flagAltera; }
            set { flagAltera = value; }
        }
        public frmSetorAltera(Setor setor) :base()
        {
            InitializeComponent();
            this.Setor = setor;
            textDescricao.Text = Setor.Descricao;
        }

        protected override void btn_gravar_Click(object sender, EventArgs e)
        {
            try
            {
                /* verificando se não foi preenchido a descrição */
                if (string.IsNullOrEmpty(textDescricao.Text.Trim()))
                {
                    textDescricao.Focus();
                    throw new Exception("O campo descrição é obrigatório");
                }

                /* verificando se a descrição não existe no banco de dados */
                EstudoFacade pcmsoFacade = EstudoFacade.getInstance();
                Setor setorAltera = pcmsoFacade.findSetorByDescricao(textDescricao.Text);
                if (setorAltera != null && setorAltera.Id != Setor.Id)
                {
                    textDescricao.Focus();
                    throw new Exception("Não é possivel incluir o setor. Descrição já cadastrada.");

                }

                Setor = pcmsoFacade.updateSetor(new Setor(Setor.Id, textDescricao.Text, ApplicationConstants.ATIVO));
                MessageBox.Show("Setor alterado com sucesso.", "Confirmação", MessageBoxButtons.OK, MessageBoxIcon.Information);
                flagAltera = true;
                this.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }
    }
}
