﻿using SWS.Entidade;
using SWS.Facade;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace SWS.View
{
    public partial class frmProdutoMovimentoBuscar : frmTemplateConsulta
    {
        private Movimento movimento;

        public Movimento Movimento
        {
            get { return movimento; }
            set { movimento = value; }
        }

        private LinkedList<Movimento> movimentos = new LinkedList<Movimento>();
        private Cliente cliente;
        private Produto produto;
        
        public frmProdutoMovimentoBuscar(Cliente cliente)
        {
            InitializeComponent();
            this.cliente = cliente;
            if (cliente != null)
            {
                btnCliente.Enabled = false;
                btnExcluirCliente.Enabled = false;
                textCliente.Text = cliente.RazaoSocial;
            }
            
        }

        private void btCliente_Click(object sender, EventArgs e)
        {
            frmClienteSelecionar formCliente = new frmClienteSelecionar(null, null, null, null, null, null, null, null, null);
            formCliente.ShowDialog();

            if (formCliente.Cliente != null)
            {
                cliente = formCliente.Cliente;
                textCliente.Text = cliente.RazaoSocial;
            }

        }

        private void btnExcluirCliente_Click(object sender, EventArgs e)
        {
            try
            {
                if (cliente == null)
                    throw new Exception("Você deve primeiro selecionar o cliente.");

                cliente = null;
                textCliente.Text = string.Empty;

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }

        private void btnFechar_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnConfirmar_Click(object sender, EventArgs e)
        {
            try
            {
                if (dgvProduto.CurrentRow == null)
                    throw new Exception("Você deve selecionar uma linha.");

                FinanceiroFacade financeiroFacade = FinanceiroFacade.getInstance();
                movimento = financeiroFacade.findMovimentoById((long)dgvProduto.CurrentRow.Cells[0].Value);
                this.Close();

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }


        }

        private void btnBuscar_Click(object sender, EventArgs e)
        {
            try
            {
                if (!validaDadosTela())
                {
                    montaDataGrid();
                }
                
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            
        }

        private void montaDataGrid()
        {
            try
            {
                dgvProduto.Columns.Clear();
                FinanceiroFacade financeiroFacade = FinanceiroFacade.getInstance();
                movimentos = financeiroFacade.findByProdutoInDataMovimento(produto, Convert.ToDateTime(dataLancamentoInicial.Text + " 00:00:00"), cliente, true, Convert.ToDateTime(dataLancamentoFinal.Text + " 23:59:59"));

                this.dgvProduto.ColumnCount = 6;

                this.dgvProduto.Columns[0].HeaderText = "id";
                this.dgvProduto.Columns[0].Visible = false;

                this.dgvProduto.Columns[1].HeaderText = "Produto";
                this.dgvProduto.Columns[1].ReadOnly = true;

                this.dgvProduto.Columns[2].HeaderText = "Data";
                this.dgvProduto.Columns[2].ReadOnly = true;
                this.dgvProduto.Columns[2].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
                this.dgvProduto.Columns[2].HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleCenter;

                this.dgvProduto.Columns[3].HeaderText = "Cliente";
                this.dgvProduto.Columns[3].ReadOnly = true;

                this.dgvProduto.Columns[4].HeaderText = "Contratante / Unidade";
                this.dgvProduto.Columns[4].ReadOnly = true;

                this.dgvProduto.Columns[5].HeaderText = "Tipo";
                this.dgvProduto.Columns[5].Visible = false;

                /* preenchendo a grid com as informações dos PRODUTOS */

                foreach (Movimento movimento in movimentos)
                {
                    dgvProduto.Rows.Add(
                        movimento.Id,
                        movimento.ContratoProduto.Produto.Nome,
                        String.Format("{0:dd/MM/yyyy} ", movimento.DataInclusao),
                        movimento.Cliente.RazaoSocial,
                        movimento.Unidade != null ? movimento.Unidade.RazaoSocial : String.Empty,
                        SWS.View.Resources.ApplicationConstants.PRODUTO);

                }

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }

        private bool validaDadosTela()
        {
            bool retorno = false;
            try
            {
                if (dataLancamentoInicial.Checked && !dataLancamentoFinal.Checked)
                    throw new Exception("Você deve marcar a data final para pesquisa.");
                
                if (dataLancamentoFinal.Checked && !dataLancamentoInicial.Checked)
                    throw new Exception("Você deve marcar a data inicial para pesquisa.");

                if (dataLancamentoFinal.Checked && dataLancamentoInicial.Checked)
                    if (Convert.ToDateTime(dataLancamentoInicial.Text) > Convert.ToDateTime(dataLancamentoFinal.Text))
                        throw new Exception("A data de pesquisa inicial não pode ser maior que a final. Refaça sua pesquisa.");

                if (produto == null)
                    throw new Exception("Você deve selecionar o produto.");

            }
            catch (Exception ex)
            {
                retorno = true;
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            return retorno;
        }

        private void btnProduto_Click(object sender, EventArgs e)
        {
            frmProdutoBusca buscarProduto = new frmProdutoBusca();
            buscarProduto.ShowDialog();

            if (buscarProduto.Produto != null)
            {
                produto = buscarProduto.Produto;
                textProduto.Text = produto.Nome;
            }

        }

        private void btnExcluirProduto_Click(object sender, EventArgs e)
        {
            try
            {
                if (produto == null)
                    throw new Exception("Você deve selecionar primeiro o produto.");
                
                produto = null;
                textProduto.Text = string.Empty;
                    
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }
    }
}
