﻿namespace SWS.View
{
    partial class frm_GheIncluir
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frm_GheIncluir));
            this.grb_dados = new System.Windows.Forms.GroupBox();
            this.lbl_nexp = new System.Windows.Forms.Label();
            this.text_nexp = new System.Windows.Forms.TextBox();
            this.lbl_gheDescricao = new System.Windows.Forms.Label();
            this.text_descricaoGhe = new System.Windows.Forms.TextBox();
            this.grb_paginacao = new System.Windows.Forms.GroupBox();
            this.btn_fechar = new System.Windows.Forms.Button();
            this.btn_salvar = new System.Windows.Forms.Button();
            this.grb_norma = new System.Windows.Forms.GroupBox();
            this.grd_norma = new System.Windows.Forms.DataGridView();
            this.btn_excluir = new System.Windows.Forms.Button();
            this.btn_inserir = new System.Windows.Forms.Button();
            this.lblNumeroPO = new System.Windows.Forms.Label();
            this.textNumeroPO = new System.Windows.Forms.TextBox();
            this.grb_dados.SuspendLayout();
            this.grb_paginacao.SuspendLayout();
            this.grb_norma.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grd_norma)).BeginInit();
            this.SuspendLayout();
            // 
            // grb_dados
            // 
            this.grb_dados.Controls.Add(this.lblNumeroPO);
            this.grb_dados.Controls.Add(this.textNumeroPO);
            this.grb_dados.Controls.Add(this.lbl_nexp);
            this.grb_dados.Controls.Add(this.text_nexp);
            this.grb_dados.Controls.Add(this.lbl_gheDescricao);
            this.grb_dados.Controls.Add(this.text_descricaoGhe);
            this.grb_dados.Location = new System.Drawing.Point(12, 12);
            this.grb_dados.Name = "grb_dados";
            this.grb_dados.Size = new System.Drawing.Size(576, 103);
            this.grb_dados.TabIndex = 0;
            this.grb_dados.TabStop = false;
            this.grb_dados.Text = "Dados";
            // 
            // lbl_nexp
            // 
            this.lbl_nexp.AutoSize = true;
            this.lbl_nexp.Location = new System.Drawing.Point(6, 55);
            this.lbl_nexp.Name = "lbl_nexp";
            this.lbl_nexp.Size = new System.Drawing.Size(80, 13);
            this.lbl_nexp.TabIndex = 8;
            this.lbl_nexp.Text = "No. de Exposto";
            // 
            // text_nexp
            // 
            this.text_nexp.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.text_nexp.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.text_nexp.ForeColor = System.Drawing.Color.Red;
            this.text_nexp.Location = new System.Drawing.Point(9, 71);
            this.text_nexp.MaxLength = 4;
            this.text_nexp.Name = "text_nexp";
            this.text_nexp.Size = new System.Drawing.Size(88, 20);
            this.text_nexp.TabIndex = 2;
            this.text_nexp.Text = "0";
            this.text_nexp.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.text_nexp_KeyPress);
            // 
            // lbl_gheDescricao
            // 
            this.lbl_gheDescricao.AutoSize = true;
            this.lbl_gheDescricao.Location = new System.Drawing.Point(6, 16);
            this.lbl_gheDescricao.Name = "lbl_gheDescricao";
            this.lbl_gheDescricao.Size = new System.Drawing.Size(55, 13);
            this.lbl_gheDescricao.TabIndex = 4;
            this.lbl_gheDescricao.Text = "Descrição";
            // 
            // text_descricaoGhe
            // 
            this.text_descricaoGhe.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.text_descricaoGhe.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.text_descricaoGhe.Location = new System.Drawing.Point(9, 32);
            this.text_descricaoGhe.MaxLength = 100;
            this.text_descricaoGhe.Name = "text_descricaoGhe";
            this.text_descricaoGhe.Size = new System.Drawing.Size(561, 20);
            this.text_descricaoGhe.TabIndex = 1;
            // 
            // grb_paginacao
            // 
            this.grb_paginacao.Controls.Add(this.btn_fechar);
            this.grb_paginacao.Controls.Add(this.btn_salvar);
            this.grb_paginacao.Location = new System.Drawing.Point(12, 340);
            this.grb_paginacao.Name = "grb_paginacao";
            this.grb_paginacao.Size = new System.Drawing.Size(576, 56);
            this.grb_paginacao.TabIndex = 1;
            this.grb_paginacao.TabStop = false;
            // 
            // btn_fechar
            // 
            this.btn_fechar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_fechar.Image = global::SWS.Properties.Resources.close;
            this.btn_fechar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btn_fechar.Location = new System.Drawing.Point(90, 19);
            this.btn_fechar.Name = "btn_fechar";
            this.btn_fechar.Size = new System.Drawing.Size(75, 23);
            this.btn_fechar.TabIndex = 8;
            this.btn_fechar.Text = "&Fechar";
            this.btn_fechar.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btn_fechar.UseVisualStyleBackColor = true;
            this.btn_fechar.Click += new System.EventHandler(this.btn_fechar_Click);
            // 
            // btn_salvar
            // 
            this.btn_salvar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_salvar.Image = global::SWS.Properties.Resources.Gravar;
            this.btn_salvar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btn_salvar.Location = new System.Drawing.Point(9, 19);
            this.btn_salvar.Name = "btn_salvar";
            this.btn_salvar.Size = new System.Drawing.Size(75, 23);
            this.btn_salvar.TabIndex = 7;
            this.btn_salvar.Text = "&Salvar";
            this.btn_salvar.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btn_salvar.UseVisualStyleBackColor = true;
            this.btn_salvar.Click += new System.EventHandler(this.btn_salvar_Click);
            // 
            // grb_norma
            // 
            this.grb_norma.Controls.Add(this.grd_norma);
            this.grb_norma.Location = new System.Drawing.Point(12, 121);
            this.grb_norma.Name = "grb_norma";
            this.grb_norma.Size = new System.Drawing.Size(576, 184);
            this.grb_norma.TabIndex = 2;
            this.grb_norma.TabStop = false;
            this.grb_norma.Text = "Notas GHE";
            // 
            // grd_norma
            // 
            this.grd_norma.AllowUserToAddRows = false;
            this.grd_norma.AllowUserToDeleteRows = false;
            this.grd_norma.AllowUserToOrderColumns = true;
            this.grd_norma.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this.grd_norma.BackgroundColor = System.Drawing.Color.White;
            this.grd_norma.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.LightGray;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.grd_norma.DefaultCellStyle = dataGridViewCellStyle1;
            this.grd_norma.Dock = System.Windows.Forms.DockStyle.Fill;
            this.grd_norma.Location = new System.Drawing.Point(3, 16);
            this.grd_norma.MultiSelect = false;
            this.grd_norma.Name = "grd_norma";
            this.grd_norma.ReadOnly = true;
            this.grd_norma.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.grd_norma.Size = new System.Drawing.Size(570, 165);
            this.grd_norma.TabIndex = 4;
            // 
            // btn_excluir
            // 
            this.btn_excluir.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_excluir.Image = global::SWS.Properties.Resources.lixeira;
            this.btn_excluir.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btn_excluir.Location = new System.Drawing.Point(93, 311);
            this.btn_excluir.Name = "btn_excluir";
            this.btn_excluir.Size = new System.Drawing.Size(75, 23);
            this.btn_excluir.TabIndex = 6;
            this.btn_excluir.Text = "&Excluir";
            this.btn_excluir.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btn_excluir.UseVisualStyleBackColor = true;
            this.btn_excluir.Click += new System.EventHandler(this.btn_excluir_Click);
            // 
            // btn_inserir
            // 
            this.btn_inserir.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_inserir.Image = global::SWS.Properties.Resources.icone_mais;
            this.btn_inserir.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btn_inserir.Location = new System.Drawing.Point(12, 311);
            this.btn_inserir.Name = "btn_inserir";
            this.btn_inserir.Size = new System.Drawing.Size(75, 23);
            this.btn_inserir.TabIndex = 5;
            this.btn_inserir.Text = "&Inserir";
            this.btn_inserir.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btn_inserir.UseVisualStyleBackColor = true;
            this.btn_inserir.Click += new System.EventHandler(this.btn_inserir_Click);
            // 
            // lblNumeroPO
            // 
            this.lblNumeroPO.AutoSize = true;
            this.lblNumeroPO.Location = new System.Drawing.Point(100, 55);
            this.lblNumeroPO.Name = "lblNumeroPO";
            this.lblNumeroPO.Size = new System.Drawing.Size(60, 13);
            this.lblNumeroPO.TabIndex = 10;
            this.lblNumeroPO.Text = "Número Po";
            // 
            // textNumeroPO
            // 
            this.textNumeroPO.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textNumeroPO.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textNumeroPO.ForeColor = System.Drawing.Color.Black;
            this.textNumeroPO.Location = new System.Drawing.Point(103, 71);
            this.textNumeroPO.MaxLength = 15;
            this.textNumeroPO.Name = "textNumeroPO";
            this.textNumeroPO.Size = new System.Drawing.Size(176, 20);
            this.textNumeroPO.TabIndex = 9;
            // 
            // frm_GheIncluir
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(600, 400);
            this.Controls.Add(this.btn_excluir);
            this.Controls.Add(this.grb_norma);
            this.Controls.Add(this.grb_paginacao);
            this.Controls.Add(this.btn_inserir);
            this.Controls.Add(this.grb_dados);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "frm_GheIncluir";
            this.Text = "INCLUIR GHE";
            this.grb_dados.ResumeLayout(false);
            this.grb_dados.PerformLayout();
            this.grb_paginacao.ResumeLayout(false);
            this.grb_norma.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.grd_norma)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox grb_dados;
        private System.Windows.Forms.Label lbl_gheDescricao;
        private System.Windows.Forms.TextBox text_descricaoGhe;
        private System.Windows.Forms.Label lbl_nexp;
        private System.Windows.Forms.TextBox text_nexp;
        private System.Windows.Forms.GroupBox grb_paginacao;
        private System.Windows.Forms.GroupBox grb_norma;
        private System.Windows.Forms.DataGridView grd_norma;
        private System.Windows.Forms.Button btn_fechar;
        private System.Windows.Forms.Button btn_salvar;
        private System.Windows.Forms.Button btn_excluir;
        private System.Windows.Forms.Button btn_inserir;
        private System.Windows.Forms.Label lblNumeroPO;
        private System.Windows.Forms.TextBox textNumeroPO;
    }
}