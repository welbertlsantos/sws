﻿using SWS.Entidade;
using SWS.Facade;
using SWS.View.Resources;
using SWS.View.ViewHelper;
using SWS.ViewHelper;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace SWS.View
{
    public partial class frmCnaePrincipal : frmTemplate
    {

        private Cnae cnae;

        public Cnae Cnae
        {
            get { return cnae; }
            set { cnae = value; }
        }
        
        public frmCnaePrincipal()
        {
            InitializeComponent();
            validaPermissoes();
            ComboHelper.comboSituacao(cbSituacao);
        }

        private void btnIncluir_Click(object sender, EventArgs e)
        {
            try
            {
                frmCnaeIncluir frmCnaeIncluir = new frmCnaeIncluir();
                frmCnaeIncluir.ShowDialog();

                if (frmCnaeIncluir.Cnae != null)
                {
                    cnae = frmCnaeIncluir.Cnae;
                    textAtividade.Text = cnae.Atividade;
                    textCodigo.Text = cnae.CodCnae;
                    cbSituacao.SelectedValue = cnae.Situacao;
                    montaDataGrid();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }

        private void btnAlterar_Click(object sender, EventArgs e)
        {
            try
            {
                
                if (dgvCnae.CurrentRow == null)
                    throw new Exception("Selecione uma linha.");
                
                if (!String.Equals(dgvCnae.CurrentRow.Cells[5].Value.ToString(), ApplicationConstants.ATIVO))
                    throw new Exception ("Somente é possível alterar um cnae ativo.");
                    
                ClienteFacade clienteFacade = ClienteFacade.getInstance();
                Cnae cnaeAlterar = clienteFacade.findCnaeById((long)dgvCnae.CurrentRow.Cells[0].Value);

                frmCnaeAlterar alteraCnae = new frmCnaeAlterar(cnaeAlterar);
                alteraCnae.ShowDialog();

                if (alteraCnae.Cnae != null)
                {
                    cnae = alteraCnae.Cnae;
                    textAtividade.Text = cnae.Atividade;
                    textCodigo.Text = cnae.CodCnae;
                    cbSituacao.SelectedValue = cnae.Situacao;
                    montaDataGrid();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btnReativar_Click(object sender, EventArgs e)
        {
            try
            {
                if (dgvCnae.CurrentRow == null)
                    throw new Exception("Selecione uma linha");

                if (!String.Equals(dgvCnae.CurrentRow.Cells[5].Value.ToString(), ApplicationConstants.DESATIVADO))
                    throw new Exception("Somente é possível reativar um CNAE que esteja inativo.");
                
                if (MessageBox.Show("Deseja reativar o CNAE selecionado?", "Confirmação", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                {

                    ClienteFacade clienteFacade = ClienteFacade.getInstance();
                    Cnae cnae = clienteFacade.findCnaeById((long)dgvCnae.CurrentRow.Cells[0].Value);
                    cnae.Situacao = ApplicationConstants.ATIVO;

                    cnae = clienteFacade.alteraCnae(cnae);
                    MessageBox.Show("CNAE reativado com sucesso.", "Confirmação", MessageBoxButtons.OK, MessageBoxIcon.Information);

                    montaDataGrid();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btnDetalhar_Click(object sender, EventArgs e)
        {
            try
            {
                if (dgvCnae.CurrentRow == null)
                    throw new Exception ("Selecione uma linha.");
                
                ClienteFacade clienteFacade = ClienteFacade.getInstance();

                Cnae cnae = clienteFacade.findCnaeById((long)dgvCnae.CurrentRow.Cells[0].Value);

                frmCnaeDetalhar cnaeDetalhar = new frmCnaeDetalhar(cnae);
                cnaeDetalhar.ShowDialog();
                
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btnLimpar_Click(object sender, EventArgs e)
        {
            textCodigo.Text = string.Empty;
            textAtividade.Text = string.Empty;
            dgvCnae.Columns.Clear();
            textCodigo.Focus();
            cnae = null;
            ComboHelper.comboSituacao(cbSituacao);
        }

        private void btnPesquisar_Click(object sender, EventArgs e)
        {
            try
            {
                montaDataGrid();
                textCodigo.Focus();
            }
            catch (Exception Ex)
            {
                MessageBox.Show(Ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btnExcluir_Click(object sender, EventArgs e)
        {
            try
            {
                if (dgvCnae.CurrentRow == null)
                    throw new Exception ("Selecione uma linha.");
                
                ClienteFacade clienteFacade = ClienteFacade.getInstance();

                if (!String.Equals(dgvCnae.CurrentRow.Cells[5].Value.ToString(), ApplicationConstants.ATIVO))
                    throw new Exception("Somente é possível excluir um cnae ativo");

                if (MessageBox.Show("Gostaria realmente de excluir o CNAE?", "Confirmaçao", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                {
                    cnae = clienteFacade.findCnaeById((long)dgvCnae.CurrentRow.Cells[0].Value);
                    cnae.Situacao = ApplicationConstants.DESATIVADO;

                    clienteFacade.alteraCnae(cnae);
                    MessageBox.Show("CNAE excluído com sucesso!", "Confirmação", MessageBoxButtons.OK, MessageBoxIcon.Information);

                    montaDataGrid();
                    
                }
                
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btnFechar_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void validaPermissoes()
        {
            PermissionamentoFacade permissionamentoFacade = PermissionamentoFacade.getInstance();

            btnAlterar.Enabled = permissionamentoFacade.hasPermission("CNAE", "ALTERAR");
            btnIncluir.Enabled = permissionamentoFacade.hasPermission("CNAE", "INCLUIR");
            btnExcluir.Enabled = permissionamentoFacade.hasPermission("CNAE", "EXCLUIR");
            btnDetalhar.Enabled = permissionamentoFacade.hasPermission("CNAE", "DETALHAR");
            btnReativar.Enabled = permissionamentoFacade.hasPermission("CNAE", "REATIVAR");
        }

        public void montaDataGrid()
        {
            try
            {
                this.Cursor = Cursors.WaitCursor;

                ClienteFacade clienteFacade = ClienteFacade.getInstance();

                cnae = new Cnae(null, ValidaCampoHelper.RetornaCnaSemFormato(textCodigo.Text).Trim(), textAtividade.Text.Trim(), string.Empty, string.Empty, (((SelectItem)cbSituacao.SelectedItem).Valor));

                DataSet ds = clienteFacade.findCnaeByFilter(cnae);

                dgvCnae.DataSource = ds.Tables["Cnaes"].DefaultView;

                dgvCnae.Columns[0].HeaderText = "ID";
                dgvCnae.Columns[0].Visible = false;

                dgvCnae.Columns[1].HeaderText = "Código";
                dgvCnae.Columns[2].HeaderText = "Atividade";
                dgvCnae.Columns[3].HeaderText = "GrauRisco";
                dgvCnae.Columns[4].HeaderText = "Grupo";
                dgvCnae.Columns[5].HeaderText = "Situação";

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }

        }

        private void dgvCnae_RowPrePaint(object sender, DataGridViewRowPrePaintEventArgs e)
        {
            DataGridView dgv = sender as DataGridView;

            if (String.Equals(dgv.Rows[e.RowIndex].Cells[5].Value, ApplicationConstants.DESATIVADO))
            {
                dgv.Rows[e.RowIndex].DefaultCellStyle.ForeColor = Color.Red;
                dgv.Rows[e.RowIndex].DefaultCellStyle.Font = new System.Drawing.Font(this.Font, FontStyle.Strikeout);
            }
        }

        private void dgvCnae_CellMouseDoubleClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            try
            {
                btnAlterar.PerformClick();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }


    }
}
