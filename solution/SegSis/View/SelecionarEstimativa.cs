﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using SegSis.Entidade;
using SegSis.Facade;

namespace SegSis.View
{
    public partial class frm_SelecionarEstimativa : BaseFormConsulta
    {
        private Estudo estudo;

        private static String msg1 = "É necessário selecionar pelo menos um tipo de estimativa";
                
        public frm_SelecionarEstimativa(Estudo estudo)
        {
            InitializeComponent();
            this.estudo = estudo;
        }

        private void btnBusar_Click(object sender, EventArgs e)
        {
            try
            {
                PpraFacade ppraFacade = PpraFacade.getInstance();

                DataSet ds = ppraFacade.findAllEstimativaNotInEstudo(estudo, new Estimativa(0, textEstimativa.Text, String.Empty));

                montaDataGrid(ds);

                textEstimativa.Focus();
                
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }


        private void montaDataGrid(DataSet ds)
        {
            try
            {
                this.dgEstimativa.Columns.Clear();

                dgEstimativa.DataSource = ds.Tables[0].DefaultView;

                dgEstimativa.Columns[0].HeaderText = "Selec";
                dgEstimativa.Columns[0].Width = 30;
                
                dgEstimativa.Columns[1].HeaderText = "Id_estimativa";
                dgEstimativa.Columns[1].Visible = false;

                dgEstimativa.Columns[2].HeaderText = "Tipo de Estimativa";
                dgEstimativa.Columns[2].ReadOnly = true;
                dgEstimativa.Columns[2].Width = 100;
                
                dgEstimativa.Columns[3].HeaderText = "Situação";
                dgEstimativa.Columns[3].Visible = false;
                
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }

        }

        private void btnConfirma_Click(object sender, EventArgs e)
        {
            Boolean gravou = false;

            try
            {
                PpraFacade ppraFacade = PpraFacade.getInstance();
                
                foreach (DataGridViewRow dvRow in dgEstimativa.Rows)
                {
                    if ((Boolean)dvRow.Cells[0].Value == true)
                    {
                        gravou = true;
                        /* incluindo a estimativa no estudo */

                        ppraFacade.insertEstimativaEstudo(new EstimativaEstudo(null, estudo,
                            new Estimativa(Convert.ToInt64(dvRow.Cells[1].Value), (String)dvRow.Cells[2].Value,
                                (String)dvRow.Cells[3].Value), (Int32)0));
                    }
                }

                if (!gravou)
                {
                    throw new Exception(msg1);
                }
                else
                {
                    this.Close();
                }


            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
        }

        private void btnFechar_Click(object sender, EventArgs e)
        {
            this.Close();
        }

    }
}
