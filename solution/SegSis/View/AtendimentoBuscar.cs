﻿using SWS.Entidade;
using SWS.Facade;
using SWS.View.ViewHelper;
using SWS.ViewHelper;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace SWS.View
{
    public partial class frmAtendimentoBuscar : frmTemplateConsulta
    {

        private Aso atendimento;

        public Aso Atendimento
        {
            get { return atendimento; }
            set { atendimento = value; }
        }

        private Cliente cliente;
        
        
        public frmAtendimentoBuscar(Cliente cliente, SelectItem situacaoAtendimento)
        {
            InitializeComponent();
            this.cliente = cliente;
            ComboHelper.situacaoAso(cbSituacao);
            if (cliente != null)
            {
                btnCliente.Enabled = false;
                textCliente.Text = cliente.RazaoSocial;
                btnExcluirCliente.Enabled = false;
            }

            if (situacaoAtendimento != null)
            {
                cbSituacao.SelectedItem = situacaoAtendimento;
                cbSituacao.SelectedValue = situacaoAtendimento.Valor;
                cbSituacao.SelectedText = situacaoAtendimento.Nome;
                cbSituacao.Enabled = false;
            }
        }

        private void btnPesquisa_Click(object sender, EventArgs e)
        {
            try
            {
                if (!validaDadosTela())
                {
                    montaGridAtendimento();
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btnConfirmar_Click(object sender, EventArgs e)
        {
            try
            {
                if (dgvAtendimentos.CurrentRow == null)
                    throw new Exception("Você não selecionou nenhum atendimento.");

                AsoFacade asoFacade = AsoFacade.getInstance();
                
                this.Atendimento = asoFacade.findAtendimentoById((long)dgvAtendimentos.CurrentRow.Cells[0].Value);
                this.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }

        private void btnFechar_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnCliente_Click(object sender, EventArgs e)
        {
            try
            {
                frmClienteSelecionar buscarCliente = new frmClienteSelecionar(null, null, false, null, null, null, null, null, null);
                buscarCliente.ShowDialog();

                if (buscarCliente.Cliente != null)
                {
                    cliente = buscarCliente.Cliente;
                    textCliente.Text = cliente.RazaoSocial;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }

        private void btnExcluirCliente_Click(object sender, EventArgs e)
        {
            try
            {
                if (cliente == null)
                    throw new Exception("Você deve selecionar primeiro um cliente antes de excluí-lo.");

                cliente = null;
                textCliente.Text = string.Empty;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }

        private void dgvAtendimentos_CellContentDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            btnConfirmar.PerformClick();
        }

        private void montaGridAtendimento()
        {
            try
            {
                this.Cursor = Cursors.WaitCursor;

                /* montando objeto para buscar os atendimentos */
                AsoFacade asoFacade = AsoFacade.getInstance();
                this.dgvAtendimentos.Columns.Clear();

                DataSet ds = asoFacade.findAsoByFilter(new Aso(null, null, null, textCodigo.Text, this.dataEmissaoInicial.Checked ? Convert.ToDateTime(this.dataEmissaoInicial.Text + " 00:00:00") : (DateTime?)null, null, cliente, null, (((SelectItem)cbSituacao.SelectedItem).Valor), null, null, string.Empty, null, null, null, null, null, this.dataGravacaoInicial.Checked ? Convert.ToDateTime(this.dataGravacaoInicial.Text + " 00:00:00") : (DateTime?)null, string.Empty, string.Empty, string.Empty, null, PermissionamentoFacade.usuarioAutenticado.Empresa, string.Empty, null, null, null, string.Empty, false, string.Empty, string.Empty, string.Empty), null, this.dataEmissaoFinal.Checked ? Convert.ToDateTime(this.dataEmissaoFinal.Text + " 23:59:59") : (DateTime?)null, this.dataGravacaoFinal.Checked ? Convert.ToDateTime(this.dataGravacaoFinal.Text + " 23:59:59") : (DateTime?)null);

                dgvAtendimentos.DataSource = ds.Tables["Asos"].DefaultView;

                // montando ids das colunas

                dgvAtendimentos.Columns[0].HeaderText = "id_aso";
                dgvAtendimentos.Columns[0].Visible = false;

                dgvAtendimentos.Columns[1].HeaderText = "id_cliente_funcao_funcionario";
                dgvAtendimentos.Columns[1].Visible = false;

                dgvAtendimentos.Columns[2].HeaderText = "Código Atendimento";
                dgvAtendimentos.Columns[3].HeaderText = "Data";
                dgvAtendimentos.Columns[4].HeaderText = "Nome Funcionário";
                dgvAtendimentos.Columns[5].HeaderText = "CPF";

                dgvAtendimentos.Columns[6].HeaderText = "Matrícula";
                dgvAtendimentos.Columns[6].Visible = false;

                dgvAtendimentos.Columns[7].HeaderText = "Razão Social";
                dgvAtendimentos.Columns[8].HeaderText = "CNPJ";

                dgvAtendimentos.Columns[9].HeaderText = "id_periodicidade";
                dgvAtendimentos.Columns[9].Visible = false;

                dgvAtendimentos.Columns[10].HeaderText = "Periodicidade";
                dgvAtendimentos.Columns[11].HeaderText = "Situação";

                dgvAtendimentos.Columns[12].HeaderText = "Criado Por";
                dgvAtendimentos.Columns[12].Visible = false;

                dgvAtendimentos.Columns[13].HeaderText = "Data Finalização";
                dgvAtendimentos.Columns[13].Visible = false;

                dgvAtendimentos.Columns[14].HeaderText = "Laudo";
                dgvAtendimentos.Columns[14].Visible = false;

                dgvAtendimentos.Columns[15].HeaderText = "Finalizado Por";
                dgvAtendimentos.Columns[15].Visible = false;

                dgvAtendimentos.Columns[16].HeaderText = "Cancelado Por";
                dgvAtendimentos.Columns[16].Visible = false;

                dgvAtendimentos.Columns[17].HeaderText = "Prioridade";
                dgvAtendimentos.Columns[17].Visible = false;

                dgvAtendimentos.Columns[18].HeaderText = "PIS";
                dgvAtendimentos.Columns[18].DisplayIndex = 6;
                dgvAtendimentos.Columns[19].HeaderText = "CTPS";
                dgvAtendimentos.Columns[19].DisplayIndex = 7;
                dgvAtendimentos.Columns[20].HeaderText = "RG";
                dgvAtendimentos.Columns[20].DisplayIndex = 8;
                dgvAtendimentos.Columns[21].HeaderText = "UF_RG";
                dgvAtendimentos.Columns[21].DisplayIndex = 9;

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }


        }

        private bool validaDadosTela()
        {
            bool retorno = false;
            try
            {
                /* validando a data de emissão */
                if (this.dataEmissaoInicial.Checked && !this.dataEmissaoFinal.Checked)
                    throw new Exception("Você deve selecionar a data de emissão final.");

                if (this.dataEmissaoFinal.Checked && !this.dataEmissaoInicial.Checked)
                    throw new Exception("Você deve selecionar a data de emissão inicial.");

                if (this.dataEmissaoInicial.Checked && this.dataEmissaoFinal.Checked)
                    if (Convert.ToDateTime(this.dataEmissaoInicial.Text) > Convert.ToDateTime(this.dataEmissaoFinal.Text))
                        throw new Exception("A data de emissão inicial não pode ser maior que a data de final. Refaça sua pesquisa.");
                
                /* validando a data de gravação */
                if (this.dataGravacaoInicial.Checked && !this.dataGravacaoFinal.Checked)
                    throw new Exception("Você deve selecionar a data de gravação final. ");

                if (this.dataGravacaoFinal.Checked && !this.dataGravacaoInicial.Checked)
                    throw new Exception("Você deve selecionar a data de gravação inicial.");

                if (this.dataGravacaoInicial.Checked && this.dataGravacaoFinal.Checked)
                    if (Convert.ToDateTime(this.dataGravacaoInicial.Text) > Convert.ToDateTime(this.dataGravacaoFinal.Text))
                        throw new Exception("A data de gravação inicial não pode ser maior que a data de final. Refaça sua pesquisa.");
                
            }
            catch (Exception ex)
            {
                retorno = true;
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            return retorno;
        }

    }
}
