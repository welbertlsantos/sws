﻿namespace SWS.View
{
    partial class frmEmailVisualizar
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.flpAcao = new System.Windows.Forms.FlowLayoutPanel();
            this.btnFechar = new System.Windows.Forms.Button();
            this.grbMensagem = new System.Windows.Forms.GroupBox();
            this.textMensagem = new System.Windows.Forms.TextBox();
            this.textAssunto = new System.Windows.Forms.TextBox();
            this.textEmail = new System.Windows.Forms.TextBox();
            this.lblPrioridade = new System.Windows.Forms.TextBox();
            this.lblAssunto = new System.Windows.Forms.TextBox();
            this.lblPara = new System.Windows.Forms.TextBox();
            this.textPrioridade = new System.Windows.Forms.TextBox();
            this.grbAnexo = new System.Windows.Forms.GroupBox();
            this.textAnexo = new System.Windows.Forms.TextBox();
            ((System.ComponentModel.ISupportInitialize)(this.scForm)).BeginInit();
            this.scForm.Panel1.SuspendLayout();
            this.scForm.Panel2.SuspendLayout();
            this.scForm.SuspendLayout();
            this.pnlForm.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.banner)).BeginInit();
            this.flpAcao.SuspendLayout();
            this.grbMensagem.SuspendLayout();
            this.grbAnexo.SuspendLayout();
            this.SuspendLayout();
            // 
            // scForm
            // 
            // 
            // scForm.Panel1
            // 
            this.scForm.Panel1.Controls.Add(this.flpAcao);
            // 
            // pnlForm
            // 
            this.pnlForm.Controls.Add(this.grbAnexo);
            this.pnlForm.Controls.Add(this.textPrioridade);
            this.pnlForm.Controls.Add(this.grbMensagem);
            this.pnlForm.Controls.Add(this.textAssunto);
            this.pnlForm.Controls.Add(this.textEmail);
            this.pnlForm.Controls.Add(this.lblPrioridade);
            this.pnlForm.Controls.Add(this.lblAssunto);
            this.pnlForm.Controls.Add(this.lblPara);
            // 
            // flpAcao
            // 
            this.flpAcao.Controls.Add(this.btnFechar);
            this.flpAcao.Dock = System.Windows.Forms.DockStyle.Fill;
            this.flpAcao.Location = new System.Drawing.Point(0, 0);
            this.flpAcao.Name = "flpAcao";
            this.flpAcao.Size = new System.Drawing.Size(534, 30);
            this.flpAcao.TabIndex = 0;
            // 
            // btnFechar
            // 
            this.btnFechar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnFechar.Image = global::SWS.Properties.Resources.close;
            this.btnFechar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnFechar.Location = new System.Drawing.Point(3, 3);
            this.btnFechar.Name = "btnFechar";
            this.btnFechar.Size = new System.Drawing.Size(75, 23);
            this.btnFechar.TabIndex = 7;
            this.btnFechar.Text = "&Fechar";
            this.btnFechar.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnFechar.UseVisualStyleBackColor = true;
            this.btnFechar.Click += new System.EventHandler(this.btnFechar_Click);
            // 
            // grbMensagem
            // 
            this.grbMensagem.Controls.Add(this.textMensagem);
            this.grbMensagem.Location = new System.Drawing.Point(5, 74);
            this.grbMensagem.Name = "grbMensagem";
            this.grbMensagem.Size = new System.Drawing.Size(504, 149);
            this.grbMensagem.TabIndex = 15;
            this.grbMensagem.TabStop = false;
            this.grbMensagem.Text = "Mensagem";
            // 
            // textMensagem
            // 
            this.textMensagem.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.textMensagem.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textMensagem.Dock = System.Windows.Forms.DockStyle.Fill;
            this.textMensagem.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textMensagem.Location = new System.Drawing.Point(3, 16);
            this.textMensagem.Multiline = true;
            this.textMensagem.Name = "textMensagem";
            this.textMensagem.ReadOnly = true;
            this.textMensagem.Size = new System.Drawing.Size(498, 130);
            this.textMensagem.TabIndex = 4;
            // 
            // textAssunto
            // 
            this.textAssunto.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.textAssunto.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textAssunto.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textAssunto.Location = new System.Drawing.Point(104, 27);
            this.textAssunto.Name = "textAssunto";
            this.textAssunto.ReadOnly = true;
            this.textAssunto.Size = new System.Drawing.Size(400, 21);
            this.textAssunto.TabIndex = 12;
            // 
            // textEmail
            // 
            this.textEmail.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.textEmail.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textEmail.CharacterCasing = System.Windows.Forms.CharacterCasing.Lower;
            this.textEmail.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textEmail.ForeColor = System.Drawing.Color.Blue;
            this.textEmail.Location = new System.Drawing.Point(104, 7);
            this.textEmail.MaxLength = 100;
            this.textEmail.Name = "textEmail";
            this.textEmail.ReadOnly = true;
            this.textEmail.Size = new System.Drawing.Size(400, 21);
            this.textEmail.TabIndex = 10;
            // 
            // lblPrioridade
            // 
            this.lblPrioridade.BackColor = System.Drawing.Color.LightGray;
            this.lblPrioridade.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblPrioridade.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPrioridade.Location = new System.Drawing.Point(5, 47);
            this.lblPrioridade.Name = "lblPrioridade";
            this.lblPrioridade.ReadOnly = true;
            this.lblPrioridade.Size = new System.Drawing.Size(100, 21);
            this.lblPrioridade.TabIndex = 13;
            this.lblPrioridade.TabStop = false;
            this.lblPrioridade.Text = "Prioridade";
            // 
            // lblAssunto
            // 
            this.lblAssunto.BackColor = System.Drawing.Color.LightGray;
            this.lblAssunto.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblAssunto.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblAssunto.Location = new System.Drawing.Point(5, 27);
            this.lblAssunto.Name = "lblAssunto";
            this.lblAssunto.ReadOnly = true;
            this.lblAssunto.Size = new System.Drawing.Size(100, 21);
            this.lblAssunto.TabIndex = 11;
            this.lblAssunto.TabStop = false;
            this.lblAssunto.Text = "Assunto";
            // 
            // lblPara
            // 
            this.lblPara.BackColor = System.Drawing.Color.LightGray;
            this.lblPara.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblPara.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPara.Location = new System.Drawing.Point(5, 7);
            this.lblPara.Name = "lblPara";
            this.lblPara.ReadOnly = true;
            this.lblPara.Size = new System.Drawing.Size(100, 21);
            this.lblPara.TabIndex = 9;
            this.lblPara.TabStop = false;
            this.lblPara.Text = "Para";
            // 
            // textPrioridade
            // 
            this.textPrioridade.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.textPrioridade.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textPrioridade.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textPrioridade.Location = new System.Drawing.Point(104, 47);
            this.textPrioridade.Name = "textPrioridade";
            this.textPrioridade.ReadOnly = true;
            this.textPrioridade.Size = new System.Drawing.Size(400, 21);
            this.textPrioridade.TabIndex = 16;
            // 
            // grbAnexo
            // 
            this.grbAnexo.Controls.Add(this.textAnexo);
            this.grbAnexo.Location = new System.Drawing.Point(8, 226);
            this.grbAnexo.Name = "grbAnexo";
            this.grbAnexo.Size = new System.Drawing.Size(501, 48);
            this.grbAnexo.TabIndex = 17;
            this.grbAnexo.TabStop = false;
            this.grbAnexo.Text = "Anexo";
            // 
            // textAnexo
            // 
            this.textAnexo.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.textAnexo.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textAnexo.CharacterCasing = System.Windows.Forms.CharacterCasing.Lower;
            this.textAnexo.Dock = System.Windows.Forms.DockStyle.Fill;
            this.textAnexo.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textAnexo.ForeColor = System.Drawing.Color.Blue;
            this.textAnexo.Location = new System.Drawing.Point(3, 16);
            this.textAnexo.Multiline = true;
            this.textAnexo.Name = "textAnexo";
            this.textAnexo.ReadOnly = true;
            this.textAnexo.Size = new System.Drawing.Size(495, 29);
            this.textAnexo.TabIndex = 6;
            // 
            // frmEmailVisualizar
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(534, 361);
            this.Name = "frmEmailVisualizar";
            this.Text = "VISUALIZAR E-MAIL";
            this.scForm.Panel1.ResumeLayout(false);
            this.scForm.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.scForm)).EndInit();
            this.scForm.ResumeLayout(false);
            this.pnlForm.ResumeLayout(false);
            this.pnlForm.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.banner)).EndInit();
            this.flpAcao.ResumeLayout(false);
            this.grbMensagem.ResumeLayout(false);
            this.grbMensagem.PerformLayout();
            this.grbAnexo.ResumeLayout(false);
            this.grbAnexo.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.FlowLayoutPanel flpAcao;
        private System.Windows.Forms.Button btnFechar;
        private System.Windows.Forms.TextBox textPrioridade;
        private System.Windows.Forms.GroupBox grbMensagem;
        private System.Windows.Forms.TextBox textMensagem;
        private System.Windows.Forms.TextBox textAssunto;
        private System.Windows.Forms.TextBox textEmail;
        private System.Windows.Forms.TextBox lblPrioridade;
        private System.Windows.Forms.TextBox lblAssunto;
        private System.Windows.Forms.TextBox lblPara;
        private System.Windows.Forms.GroupBox grbAnexo;
        private System.Windows.Forms.TextBox textAnexo;
    }
}