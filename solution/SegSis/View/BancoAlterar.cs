﻿using SWS.Entidade;
using SWS.Facade;
using SWS.View.Resources;
using SWS.View.ViewHelper;
using SWS.ViewHelper;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace SWS.View
{
    public partial class frmBancoAlterar : frmBancoIncluir
    {
        public frmBancoAlterar(Banco banco) :base()
        {
            InitializeComponent();
            this.Banco = banco;

            /* preenchendo a tela */

            textNome.Text = Banco.Nome;
            textCodigo.Text = Banco.CodigoBanco;
            ComboHelper.Boolean(cbCaixa, false);
            ComboHelper.Boolean(cbBoleto, false);
            cbCaixa.SelectedValue = Banco.Caixa == true ? "true" : "false";
            cbBoleto.SelectedValue = Banco.Boleto == true ? "true" : "false";

        }

        protected override void btnGravar_Click(object sender, EventArgs e)
        {
            try
            {
                if (ValidaCampos())
                {
                    FinanceiroFacade financeiroFacade = FinanceiroFacade.getInstance();

                    Banco = financeiroFacade.alteraBanco(new Banco(Banco.Id, this.textNome.Text, textCodigo.Text, (bool?)Convert.ToBoolean(((SelectItem)cbCaixa.SelectedItem).Valor), ApplicationConstants.ATIVO, false, (bool?)Convert.ToBoolean(((SelectItem)cbBoleto.SelectedItem).Valor)));

                    MessageBox.Show("Banco alterado com sucesso", "Confirmação", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    this.Close();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }
    }
}
