﻿using SWS.Entidade;
using SWS.Facade;
using SWS.View.Resources;
using SWS.View.ViewHelper;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace SWS.View
{
    public partial class frmMovimentoIncluir : frmTemplateConsulta
    {

        private Cliente cliente;

        public Cliente Cliente
        {
            get { return cliente; }
            set { cliente = value; }
        }
        private Cliente unidade;
        private Cliente clienteCredenciada = null;
        private ContratoProduto contratoProduto;
        private bool inclusao;
        private DateTime? dataInclusaoMovimento;

        public DateTime? DataInclusaoMovimento
        {
            get { return dataInclusaoMovimento; }
            set { dataInclusaoMovimento = value; }
        }

        public bool Inclusao
        {
            get { return inclusao; }
            set { inclusao = value; }
        }

        private CentroCusto centroCusto;
        
        public frmMovimentoIncluir()
        {
            InitializeComponent();
            dataInclusao.Text = Convert.ToString(DateTime.Now);
            ActiveControl = btnCliente;
        }

        private void btn_confirmar_Click(object sender, EventArgs e)
        {
            try
            {
                /* verificando se tem algum item na grid para ser incluído */
                if (dgvMovimento.Rows.Count == 0)
                    throw new Exception("Você não inseriu nenhum produto.");
                
                FinanceiroFacade financeiroFacade = FinanceiroFacade.getInstance();
                    
                /* gravando cada linha do dataGrid na tabela de movimento */
                foreach (DataGridViewRow dvRow in dgvMovimento.Rows)
                {
                    financeiroFacade.insertMovimento(new Movimento(null, null, null, cliente, new ContratoProduto((Int64)dvRow.Cells[0].Value, null, null, null, null, null,null, String.Empty, false ), null, Convert.ToDateTime(dataInclusao.Text + " " + Convert.ToString(DateTime.Now.ToLongTimeString())), null, ApplicationConstants.PENDENTE, (Decimal)dvRow.Cells[5].Value, null, null, null, (Int32)dvRow.Cells[4].Value, PermissionamentoFacade.usuarioAutenticado, PermissionamentoFacade.usuarioAutenticado.Empresa, (Decimal)dvRow.Cells[7].Value, unidade, null, DateTime.Now, centroCusto, null, this.clienteCredenciada));
                    inclusao = true;
                }

                MessageBox.Show("Inclusão no movimento realizada com sucesso. Atualize os itens de pesquisa.", "Confirmação", MessageBoxButtons.OK, MessageBoxIcon.Information);
                this.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btn_fechar_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btn_unidade_Click(object sender, EventArgs e)
        {
            try
            {
                if (cliente == null)
                    throw new Exception("É necessário primeiro selecionar o cliente.");

                frmUnidadeBuscar formUnidade = new frmUnidadeBuscar(cliente);
                formUnidade.ShowDialog();

                if (formUnidade.Unidade != null)
                {
                    unidade = formUnidade.Unidade;
                    textUnidade.Text = unidade.RazaoSocial;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btn_incluirProduto_Click(object sender, EventArgs e)
        {
            try
            {
                if (cliente == null)
                    throw new Exception("Você deve selecionar primeiro o cliente");
                
                frmMovimentoIncluirProduto formMovimentoProduto = new frmMovimentoIncluirProduto(cliente);
                formMovimentoProduto.ShowDialog();

                if (formMovimentoProduto.ContratoProduto != null)
                {
                    contratoProduto = formMovimentoProduto.ContratoProduto;
                    textPreco.Text = Convert.ToString(contratoProduto.PrecoContratado);
                    textProduto.Text = contratoProduto.Produto.Nome;
                    textQuantidade.Focus();
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }

        private void btn_OK_Click(object sender, EventArgs e)
        {
            try
            {
                /* validando informações do produto, a quantidade não pode ser vazia ou igual a zero. */

                if (String.IsNullOrEmpty(textQuantidade.Text) || Convert.ToInt32(textQuantidade.Text) <= 0 )
                    throw new Exception("A campo quantidade não poder ser igual a zero ou estar vazio");

                dgvMovimento.ColumnCount = 10;

                dgvMovimento.Columns[0].HeaderText = "id_contrato_produto";
                dgvMovimento.Columns[0].Visible = false;

                dgvMovimento.Columns[1].HeaderText = "Código Produto";
                dgvMovimento.Columns[1].Width = 50;
                dgvMovimento.Columns[1].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
                dgvMovimento.Columns[1].HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleCenter;

                dgvMovimento.Columns[2].HeaderText = "Item";
                dgvMovimento.Columns[2].Width = 30;
                dgvMovimento.Columns[2].DisplayIndex = 0;
                dgvMovimento.Columns[2].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
                dgvMovimento.Columns[2].HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleCenter;
                
                dgvMovimento.Columns[3].HeaderText = "Produto";
                dgvMovimento.Columns[3].Width = 200;

                dgvMovimento.Columns[4].HeaderText = "Quantidade";
                dgvMovimento.Columns[4].Width = 70;
                dgvMovimento.Columns[4].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
                dgvMovimento.Columns[4].HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleRight;
                    
                dgvMovimento.Columns[5].HeaderText = "Preço R$";
                dgvMovimento.Columns[5].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
                dgvMovimento.Columns[5].HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleRight;
                dgvMovimento.Columns[5].Width = 70;

                dgvMovimento.Columns[6].HeaderText = "Preco Total";
                dgvMovimento.Columns[6].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
                dgvMovimento.Columns[6].HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleRight;
                dgvMovimento.Columns[6].Width = 80;

                dgvMovimento.Columns[7].HeaderText = "Custo Unitário";
                dgvMovimento.Columns[7].Visible = false;

                dgvMovimento.Columns[8].HeaderText = "IdCentroCusto";
                dgvMovimento.Columns[8].Visible = false;

                dgvMovimento.Columns[9].HeaderText = "Centro de custo";
                dgvMovimento.Columns[9].Visible = false;


                dgvMovimento.Rows.Add(contratoProduto.Id, contratoProduto.Produto.Id, dgvMovimento.Rows.Count + 1, contratoProduto.Produto.Nome, Convert.ToInt32(textQuantidade.Text), contratoProduto.PrecoContratado, Convert.ToDecimal(contratoProduto.PrecoContratado * Convert.ToInt32(textQuantidade.Text)), Convert.ToDecimal(contratoProduto.CustoContrato), centroCusto != null ? centroCusto.Id : null, centroCusto != null ? centroCusto.Descricao : string.Empty); 
                somaTotalGrid();

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
                textQuantidade.Text = String.Empty;
                textQuantidade.Focus();
            }
        }

        private void btn_cliente_Click(object sender, EventArgs e)
        {
            try
            {
                frmClienteSelecionar formCliente = new frmClienteSelecionar(null, null, false, null, null, null, null, null, null);
                formCliente.ShowDialog();

                if (formCliente.Cliente != null)
                {
                    cliente = formCliente.Cliente;
                    textCliente.Text = cliente.RazaoSocial;
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void text_quantidade_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = ValidaCampoHelper.FormataCampoNumerico(sender, e);
        }

        private void frmMovimentoIncluir_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter || e.KeyCode == Keys.Return)
                SendKeys.Send("{TAB}");
        }

        private void dgv_movimento_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.KeyCode == Keys.Delete)
                {
                    if (dgvMovimento.CurrentRow == null)
                        throw new Exception("Selecione uma linha");

                    if (MessageBox.Show("Deseja excluir o item?", "Confirmação", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                    {
                        dgvMovimento.Rows.RemoveAt(dgvMovimento.CurrentRow.Index);
                        somaTotalGrid();
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }

        private void somaTotalGrid()
        {
            try
            {
                textQuantidadeItens.Text = Convert.ToString(dgvMovimento.Rows.Count);

                /* varrendo a grid para apurar o total */
                Decimal valorTotal = 0;

                foreach (DataGridViewRow dvRow in dgvMovimento.Rows)
                {
                    valorTotal += Convert.ToDecimal(Convert.ToDecimal(dvRow.Cells[5].Value)
                        * Convert.ToInt32(dvRow.Cells[4].Value));
                }

                textTotalItens.Text = valorTotal.ToString();

                /* zerando campos já incluídos na grid. */
                contratoProduto = null;
                textProduto.Text = String.Empty;
                textQuantidade.Text = String.Empty;
                textPreco.Text = String.Empty;
                btnIncluirProduto.Focus();

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }

        private void btnCentroCusto_Click(object sender, EventArgs e)
        {
            try
            {
                if (cliente == null)
                    throw new Exception("Selecione um cliente.");

                if (!cliente.UsaCentroCusto)
                    throw new Exception("Cliente não utiliza centro de custo.");

                frmClienteCentroCustoSelecionar selecionarCentroCusto = new frmClienteCentroCustoSelecionar(cliente);
                selecionarCentroCusto.ShowDialog();

                if (selecionarCentroCusto.CentroCustoSelecionado != null)
                {
                    centroCusto = selecionarCentroCusto.CentroCustoSelecionado;
                    textCentroCusto.Text = centroCusto.Descricao;
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }

        private void btnCredenciada_Click(object sender, EventArgs e)
        {
            try
            {
                if (this.cliente != null && this.cliente.Credenciadora == false)
                {
                    throw new Exception("Cliente selecionado não é credenciadora.");
                }

                frmClienteCrendiadaSelecionar selecionarCredenciada = new frmClienteCrendiadaSelecionar(this.cliente);
                selecionarCredenciada.ShowDialog();

                if (selecionarCredenciada.Credenciada != null)
                {
                    this.clienteCredenciada = selecionarCredenciada.Credenciada;
                    textCredenciada.Text = this.clienteCredenciada.RazaoSocial;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }

    }
}
