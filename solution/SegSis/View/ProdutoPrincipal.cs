﻿using SWS.Entidade;
using SWS.Facade;
using SWS.View.Resources;
using SWS.View.ViewHelper;
using SWS.ViewHelper;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace SWS.View
{
    public partial class frmProdutoPrincipal : frmTemplate
    {
        private Produto produto;
        
        public frmProdutoPrincipal()
        {
            InitializeComponent();
            ComboHelper.comboSituacao(cbSituacao);
            ActiveControl = textDescricao;
        }

        private void btnIncluir_Click(object sender, EventArgs e)
        {
            try
            {
                frmProdutoIncluir incluirProduto = new frmProdutoIncluir();
                incluirProduto.ShowDialog();

                if (incluirProduto.Produto != null)
                {
                    produto = new Produto(null, string.Empty, string.Empty, null, string.Empty, null, false, string.Empty);
                    montaDataGrid();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }

        private void btnAlterar_Click(object sender, EventArgs e)
        {
            try
            {
                if (dgvProduto.CurrentRow == null)
                    throw new Exception("Selecione uma linha.");

                FinanceiroFacade financeiroFacade = FinanceiroFacade.getInstance();

                Produto produtoAlterar = financeiroFacade.findProdutoById((long)dgvProduto.CurrentRow.Cells[0].Value);

                if (!string.Equals(produtoAlterar.Situacao, ApplicationConstants.ATIVO))
                    throw new Exception("Somente produtos ativos podem ser alterados.");

                frmProdutoAlterar alteraProduto = new frmProdutoAlterar(produtoAlterar);
                alteraProduto.ShowDialog();

                produto = new Produto(null, string.Empty, string.Empty, null, string.Empty, null, false, string.Empty);
                montaDataGrid();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btnReativar_Click(object sender, EventArgs e)
        {
            try
            {

                if (dgvProduto.CurrentRow == null)
                    throw new Exception("Selecione uma linha.");

                FinanceiroFacade financeiroFacade = FinanceiroFacade.getInstance();

                Produto produtoReativar = financeiroFacade.findProdutoById((long)dgvProduto.CurrentRow.Cells[0].Value);

                if (!string.Equals(produtoReativar.Situacao, ApplicationConstants.DESATIVADO))
                    throw new Exception("Somente produtos desativados podem ser reativados.");

                if (MessageBox.Show("Deseja reativar o produto selecionado?", "Atenção", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                {
                    produtoReativar.Situacao = ApplicationConstants.ATIVO;
                    financeiroFacade.updateProduto(produtoReativar);
                    MessageBox.Show("Produto reativado com sucesso.", "Confirmação", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    produto = new Produto(null, string.Empty, string.Empty, null, string.Empty, null, false, string.Empty);
                    montaDataGrid();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btnPesquisar_Click(object sender, EventArgs e)
        {
            try
            {
                produto = new Produto(null, textDescricao.Text, ((SelectItem)cbSituacao.SelectedItem).Valor, null, string.Empty, null, false, string.Empty);
                montaDataGrid();
                textDescricao.Focus();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btnDetalhar_Click(object sender, EventArgs e)
        {
            try
            {

                if (dgvProduto.CurrentRow == null)
                    throw new Exception("Selecione uma linha.");
                
                FinanceiroFacade finaceiroFacade = FinanceiroFacade.getInstance();
                
                frmProdutoDetalhar detalharProduto = new frmProdutoDetalhar(finaceiroFacade.findProdutoById((long)dgvProduto.CurrentRow.Cells[0].Value));
                detalharProduto.ShowDialog();

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btnExcluir_Click(object sender, EventArgs e)
        {
            try
            {
                if (dgvProduto.CurrentRow == null)
                    throw new Exception("Selecione uma linha.");

                FinanceiroFacade financeiroFacade = FinanceiroFacade.getInstance();

                Produto produtoExcluir = financeiroFacade.findProdutoById((long)dgvProduto.CurrentRow.Cells[0].Value);

                if (!string.Equals(produtoExcluir.Situacao, ApplicationConstants.ATIVO))
                    throw new Exception("Somente produtos ativos podem ser excluídos.");

                if (string.Equals(produtoExcluir.Tipo, ApplicationConstants.CONSTRUCAO))
                    throw new Exception("Produto exclusivo do sistema. Não pode ser excluído.");

                if (MessageBox.Show("Deseja excluir o produto selecionado?", "Atenção", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                {
                    produtoExcluir.Situacao = ApplicationConstants.DESATIVADO;
                    financeiroFacade.updateProduto(produtoExcluir);
                    MessageBox.Show("Produto excluído com sucesso.", "Confirmação", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    produto = new Produto(null, string.Empty, string.Empty, null, string.Empty, null, false, string.Empty);
                    montaDataGrid();
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btnFechar_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void dgvProduto_RowPrePaint(object sender, DataGridViewRowPrePaintEventArgs e)
        {
            DataGridView dgv = sender as DataGridView;
            
            if (String.Equals(dgv.Rows[e.RowIndex].Cells[2].Value.ToString(), ApplicationConstants.DESATIVADO))
                dgv.Rows[e.RowIndex].DefaultCellStyle.ForeColor = Color.Red;
        }

        private void validaPermissoes()
        {
            PermissionamentoFacade permissionamentoFacade = PermissionamentoFacade.getInstance();

            btnAlterar.Enabled = permissionamentoFacade.hasPermission("PRODUTO", "ALTERAR");
            btnIncluir.Enabled = permissionamentoFacade.hasPermission("PRODUTO", "INCLUIR");
            btnExcluir.Enabled = permissionamentoFacade.hasPermission("PRODUTO", "EXCLUIR");
            btnDetalhar.Enabled = permissionamentoFacade.hasPermission("PRODUTO", "DETALHAR");
            btnReativar.Enabled = permissionamentoFacade.hasPermission("PRODUTO", "REATIVAR");
        }

        public void montaDataGrid()
        {
            try
            {
                this.dgvProduto.Columns.Clear();

                this.Cursor = Cursors.WaitCursor;

                FinanceiroFacade financeiroFacade = FinanceiroFacade.getInstance();

                DataSet ds = financeiroFacade.findProdutoByFilter(produto);

                this.dgvProduto.DataSource = ds.Tables["Produtos"].DefaultView;

                this.dgvProduto.Columns[0].HeaderText = "Id_Produto";
                this.dgvProduto.Columns[0].Visible = false;

                this.dgvProduto.Columns[1].HeaderText = "Descrição";

                this.dgvProduto.Columns[2].HeaderText = "Situação";
                this.dgvProduto.Columns[2].Visible = false;

                this.dgvProduto.Columns[3].HeaderText = "Preço(R$)";
                this.dgvProduto.Columns[3].DefaultCellStyle.Format = "N2";
                this.dgvProduto.Columns[3].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
                this.dgvProduto.Columns[3].HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleRight;
                this.dgvProduto.Columns[3].MinimumWidth = 50;

                this.dgvProduto.Columns[4].HeaderText = "Tipo";
                this.dgvProduto.Columns[4].Visible = false;

                this.dgvProduto.Columns[5].HeaderText = "Custo (R$)";
                this.dgvProduto.Columns[5].DefaultCellStyle.Format = "N2";
                this.dgvProduto.Columns[5].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
                this.dgvProduto.Columns[5].HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleRight;
                this.dgvProduto.Columns[5].MinimumWidth = 50;

                this.dgvProduto.Columns[6].HeaderText = "Padrão Contrato";
                this.dgvProduto.Columns[6].Visible = false;

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }

        }

        private void dgvProduto_CellMouseDoubleClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            try
            {
                btnAlterar.PerformClick();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }
    }
}
