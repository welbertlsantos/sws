﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using SegSis.Entidade;
using SegSis.Facade;

namespace SegSis.View
{
    public partial class frm_FuncionarioAlterarClienteFuncaoFuncionario : Form
    {
        public ClienteFuncaoFuncionario clienteFuncaoFuncionario = null;

        public ClienteFuncaoFuncionario getClienteFuncaoFuncionario()
        {
            return this.clienteFuncaoFuncionario;
        }
        
        public frm_FuncionarioAlterarClienteFuncaoFuncionario(ClienteFuncaoFuncionario clienteFuncaoFuncionario)
        {
            InitializeComponent();
            this.clienteFuncaoFuncionario = clienteFuncaoFuncionario;
        }

        private void btn_fechar_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btn_gravar_Click(object sender, EventArgs e)
        {
            try
            {
                this.Cursor = Cursors.WaitCursor;

                Boolean vip = chk_vip.Checked ? true : false;

                clienteFuncaoFuncionario.setMatricula(text_matricula.Text);
                clienteFuncaoFuncionario.setVip(vip);

                //Incluindo no banco de Dados.

                FuncionarioFacade funcionarioFacade = FuncionarioFacade.getInstance();

                funcionarioFacade.incluirClienteFuncaoFuncionario(clienteFuncaoFuncionario);

                this.Close();

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }
    }
}
