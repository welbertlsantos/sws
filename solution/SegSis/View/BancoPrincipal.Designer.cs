﻿namespace SWS.View
{
    partial class frmBancoPrincipal
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            this.flpAcao = new System.Windows.Forms.FlowLayoutPanel();
            this.btnIncluir = new System.Windows.Forms.Button();
            this.btnAlterar = new System.Windows.Forms.Button();
            this.btnPesquisar = new System.Windows.Forms.Button();
            this.btnReativar = new System.Windows.Forms.Button();
            this.btnDetalhar = new System.Windows.Forms.Button();
            this.btnExcluir = new System.Windows.Forms.Button();
            this.btnFechar = new System.Windows.Forms.Button();
            this.textBanco = new System.Windows.Forms.TextBox();
            this.lblBanco = new System.Windows.Forms.TextBox();
            this.grbBanco = new System.Windows.Forms.GroupBox();
            this.dgvBanco = new System.Windows.Forms.DataGridView();
            this.lblCaixa = new System.Windows.Forms.TextBox();
            this.lblBoleto = new System.Windows.Forms.TextBox();
            this.cbCaixa = new SWS.ComboBoxWithBorder();
            this.cbBoleto = new SWS.ComboBoxWithBorder();
            ((System.ComponentModel.ISupportInitialize)(this.spForm)).BeginInit();
            this.spForm.Panel1.SuspendLayout();
            this.spForm.Panel2.SuspendLayout();
            this.spForm.SuspendLayout();
            this.pnlForm.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.banner)).BeginInit();
            this.flpAcao.SuspendLayout();
            this.grbBanco.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvBanco)).BeginInit();
            this.SuspendLayout();
            // 
            // spForm
            // 
            // 
            // spForm.Panel1
            // 
            this.spForm.Panel1.Controls.Add(this.flpAcao);
            // 
            // pnlForm
            // 
            this.pnlForm.Controls.Add(this.cbBoleto);
            this.pnlForm.Controls.Add(this.cbCaixa);
            this.pnlForm.Controls.Add(this.lblBoleto);
            this.pnlForm.Controls.Add(this.lblCaixa);
            this.pnlForm.Controls.Add(this.grbBanco);
            this.pnlForm.Controls.Add(this.lblBanco);
            this.pnlForm.Controls.Add(this.textBanco);
            // 
            // flpAcao
            // 
            this.flpAcao.Controls.Add(this.btnIncluir);
            this.flpAcao.Controls.Add(this.btnAlterar);
            this.flpAcao.Controls.Add(this.btnPesquisar);
            this.flpAcao.Controls.Add(this.btnReativar);
            this.flpAcao.Controls.Add(this.btnDetalhar);
            this.flpAcao.Controls.Add(this.btnExcluir);
            this.flpAcao.Controls.Add(this.btnFechar);
            this.flpAcao.Dock = System.Windows.Forms.DockStyle.Fill;
            this.flpAcao.Location = new System.Drawing.Point(0, 0);
            this.flpAcao.Name = "flpAcao";
            this.flpAcao.Size = new System.Drawing.Size(784, 35);
            this.flpAcao.TabIndex = 0;
            // 
            // btnIncluir
            // 
            this.btnIncluir.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnIncluir.Image = global::SWS.Properties.Resources.icone_mais;
            this.btnIncluir.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnIncluir.Location = new System.Drawing.Point(3, 3);
            this.btnIncluir.Name = "btnIncluir";
            this.btnIncluir.Size = new System.Drawing.Size(75, 23);
            this.btnIncluir.TabIndex = 4;
            this.btnIncluir.Text = "&Incluir";
            this.btnIncluir.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnIncluir.UseVisualStyleBackColor = true;
            this.btnIncluir.Click += new System.EventHandler(this.btnIncluir_Click);
            // 
            // btnAlterar
            // 
            this.btnAlterar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnAlterar.Image = global::SWS.Properties.Resources.Alterar;
            this.btnAlterar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnAlterar.Location = new System.Drawing.Point(84, 3);
            this.btnAlterar.Name = "btnAlterar";
            this.btnAlterar.Size = new System.Drawing.Size(75, 23);
            this.btnAlterar.TabIndex = 5;
            this.btnAlterar.Text = "&Alterar";
            this.btnAlterar.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnAlterar.UseVisualStyleBackColor = true;
            this.btnAlterar.Click += new System.EventHandler(this.btnAlterar_Click);
            // 
            // btnPesquisar
            // 
            this.btnPesquisar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnPesquisar.Image = global::SWS.Properties.Resources.lupa;
            this.btnPesquisar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnPesquisar.Location = new System.Drawing.Point(165, 3);
            this.btnPesquisar.Name = "btnPesquisar";
            this.btnPesquisar.Size = new System.Drawing.Size(75, 23);
            this.btnPesquisar.TabIndex = 6;
            this.btnPesquisar.Text = "Pesquisar";
            this.btnPesquisar.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnPesquisar.UseVisualStyleBackColor = true;
            this.btnPesquisar.Click += new System.EventHandler(this.btnPesquisar_Click);
            // 
            // btnReativar
            // 
            this.btnReativar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnReativar.Image = global::SWS.Properties.Resources.devolucao;
            this.btnReativar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnReativar.Location = new System.Drawing.Point(246, 3);
            this.btnReativar.Name = "btnReativar";
            this.btnReativar.Size = new System.Drawing.Size(75, 23);
            this.btnReativar.TabIndex = 7;
            this.btnReativar.Text = "&Reativar";
            this.btnReativar.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnReativar.UseVisualStyleBackColor = true;
            this.btnReativar.Click += new System.EventHandler(this.btnReativar_Click);
            // 
            // btnDetalhar
            // 
            this.btnDetalhar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnDetalhar.Image = global::SWS.Properties.Resources.lupa;
            this.btnDetalhar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnDetalhar.Location = new System.Drawing.Point(327, 3);
            this.btnDetalhar.Name = "btnDetalhar";
            this.btnDetalhar.Size = new System.Drawing.Size(75, 23);
            this.btnDetalhar.TabIndex = 8;
            this.btnDetalhar.Text = "&Detalhar";
            this.btnDetalhar.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnDetalhar.UseVisualStyleBackColor = true;
            this.btnDetalhar.Click += new System.EventHandler(this.btnDetalhar_Click);
            // 
            // btnExcluir
            // 
            this.btnExcluir.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnExcluir.Image = global::SWS.Properties.Resources.lixeira;
            this.btnExcluir.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnExcluir.Location = new System.Drawing.Point(408, 3);
            this.btnExcluir.Name = "btnExcluir";
            this.btnExcluir.Size = new System.Drawing.Size(75, 23);
            this.btnExcluir.TabIndex = 9;
            this.btnExcluir.Text = "&Excluir";
            this.btnExcluir.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnExcluir.UseVisualStyleBackColor = true;
            this.btnExcluir.Click += new System.EventHandler(this.btnExcluir_Click);
            // 
            // btnFechar
            // 
            this.btnFechar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnFechar.Image = global::SWS.Properties.Resources.close;
            this.btnFechar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnFechar.Location = new System.Drawing.Point(489, 3);
            this.btnFechar.Name = "btnFechar";
            this.btnFechar.Size = new System.Drawing.Size(75, 23);
            this.btnFechar.TabIndex = 10;
            this.btnFechar.Text = "&Fechar";
            this.btnFechar.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnFechar.UseVisualStyleBackColor = true;
            this.btnFechar.Click += new System.EventHandler(this.btnFechar_Click);
            // 
            // textBanco
            // 
            this.textBanco.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBanco.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.textBanco.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBanco.Location = new System.Drawing.Point(132, 14);
            this.textBanco.MaxLength = 100;
            this.textBanco.Name = "textBanco";
            this.textBanco.Size = new System.Drawing.Size(618, 21);
            this.textBanco.TabIndex = 1;
            // 
            // lblBanco
            // 
            this.lblBanco.BackColor = System.Drawing.Color.LightGray;
            this.lblBanco.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblBanco.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblBanco.Location = new System.Drawing.Point(12, 14);
            this.lblBanco.MaxLength = 100;
            this.lblBanco.Name = "lblBanco";
            this.lblBanco.ReadOnly = true;
            this.lblBanco.Size = new System.Drawing.Size(121, 21);
            this.lblBanco.TabIndex = 3;
            this.lblBanco.Text = "Banco";
            // 
            // grbBanco
            // 
            this.grbBanco.Controls.Add(this.dgvBanco);
            this.grbBanco.Location = new System.Drawing.Point(12, 81);
            this.grbBanco.Name = "grbBanco";
            this.grbBanco.Size = new System.Drawing.Size(738, 371);
            this.grbBanco.TabIndex = 3;
            this.grbBanco.TabStop = false;
            this.grbBanco.Text = "Bancos";
            // 
            // dgvBanco
            // 
            this.dgvBanco.AllowUserToAddRows = false;
            this.dgvBanco.AllowUserToDeleteRows = false;
            this.dgvBanco.AllowUserToOrderColumns = true;
            this.dgvBanco.AllowUserToResizeRows = false;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.Gainsboro;
            this.dgvBanco.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle1;
            this.dgvBanco.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.DisplayedCells;
            this.dgvBanco.BackgroundColor = System.Drawing.Color.White;
            this.dgvBanco.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.Color.LightGray;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvBanco.DefaultCellStyle = dataGridViewCellStyle2;
            this.dgvBanco.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvBanco.Location = new System.Drawing.Point(3, 16);
            this.dgvBanco.MultiSelect = false;
            this.dgvBanco.Name = "dgvBanco";
            this.dgvBanco.ReadOnly = true;
            this.dgvBanco.RowHeadersWidth = 30;
            dataGridViewCellStyle3.Font = new System.Drawing.Font("Arial", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dgvBanco.RowsDefaultCellStyle = dataGridViewCellStyle3;
            this.dgvBanco.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvBanco.Size = new System.Drawing.Size(732, 352);
            this.dgvBanco.TabIndex = 3;
            this.dgvBanco.CellMouseDoubleClick += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.dgvBanco_CellMouseDoubleClick);
            this.dgvBanco.RowPrePaint += new System.Windows.Forms.DataGridViewRowPrePaintEventHandler(this.dgvBanco_RowPrePaint);
            // 
            // lblCaixa
            // 
            this.lblCaixa.BackColor = System.Drawing.Color.LightGray;
            this.lblCaixa.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblCaixa.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCaixa.Location = new System.Drawing.Point(12, 34);
            this.lblCaixa.MaxLength = 100;
            this.lblCaixa.Name = "lblCaixa";
            this.lblCaixa.ReadOnly = true;
            this.lblCaixa.Size = new System.Drawing.Size(121, 21);
            this.lblCaixa.TabIndex = 4;
            this.lblCaixa.Text = "Caixa";
            // 
            // lblBoleto
            // 
            this.lblBoleto.BackColor = System.Drawing.Color.LightGray;
            this.lblBoleto.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblBoleto.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblBoleto.Location = new System.Drawing.Point(12, 54);
            this.lblBoleto.MaxLength = 100;
            this.lblBoleto.Name = "lblBoleto";
            this.lblBoleto.ReadOnly = true;
            this.lblBoleto.Size = new System.Drawing.Size(121, 21);
            this.lblBoleto.TabIndex = 5;
            this.lblBoleto.Text = "Boleto";
            // 
            // cbCaixa
            // 
            this.cbCaixa.BackColor = System.Drawing.Color.LightGray;
            this.cbCaixa.BorderColor = System.Drawing.Color.DimGray;
            this.cbCaixa.BorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid;
            this.cbCaixa.FormattingEnabled = true;
            this.cbCaixa.Location = new System.Drawing.Point(132, 34);
            this.cbCaixa.Name = "cbCaixa";
            this.cbCaixa.Size = new System.Drawing.Size(618, 21);
            this.cbCaixa.TabIndex = 6;
            // 
            // cbBoleto
            // 
            this.cbBoleto.BackColor = System.Drawing.Color.LightGray;
            this.cbBoleto.BorderColor = System.Drawing.Color.DimGray;
            this.cbBoleto.BorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid;
            this.cbBoleto.FormattingEnabled = true;
            this.cbBoleto.Location = new System.Drawing.Point(132, 54);
            this.cbBoleto.Name = "cbBoleto";
            this.cbBoleto.Size = new System.Drawing.Size(618, 21);
            this.cbBoleto.TabIndex = 7;
            // 
            // frmBancoPrincipal
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(784, 561);
            this.Name = "frmBancoPrincipal";
            this.Text = "GERENCIAR BANCOS";
            this.spForm.Panel1.ResumeLayout(false);
            this.spForm.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.spForm)).EndInit();
            this.spForm.ResumeLayout(false);
            this.pnlForm.ResumeLayout(false);
            this.pnlForm.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.banner)).EndInit();
            this.flpAcao.ResumeLayout(false);
            this.grbBanco.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvBanco)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.FlowLayoutPanel flpAcao;
        private System.Windows.Forms.Button btnIncluir;
        private System.Windows.Forms.Button btnAlterar;
        private System.Windows.Forms.Button btnPesquisar;
        private System.Windows.Forms.Button btnReativar;
        private System.Windows.Forms.Button btnDetalhar;
        private System.Windows.Forms.Button btnExcluir;
        private System.Windows.Forms.Button btnFechar;
        private System.Windows.Forms.TextBox lblBanco;
        private System.Windows.Forms.TextBox textBanco;
        private System.Windows.Forms.GroupBox grbBanco;
        private System.Windows.Forms.DataGridView dgvBanco;
        private ComboBoxWithBorder cbBoleto;
        private ComboBoxWithBorder cbCaixa;
        private System.Windows.Forms.TextBox lblBoleto;
        private System.Windows.Forms.TextBox lblCaixa;
    }
}