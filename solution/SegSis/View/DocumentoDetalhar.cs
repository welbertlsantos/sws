﻿using SWS.Entidade;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace SWS.View
{
    public partial class frmDocumentoDetalhar : frmDocumentoIncluir
    {
        public frmDocumentoDetalhar(Documento documento) :base()
        {
            InitializeComponent();
            this.textDescricao.Text = documento.Descricao;
            btnGravar.Enabled = false;
        }
    }
}
