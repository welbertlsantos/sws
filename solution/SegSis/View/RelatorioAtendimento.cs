﻿using SWS.Entidade;
using SWS.Facade;
using SWS.View.Resources;
using SWS.View.ViewHelper;
using SWS.ViewHelper;
using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace SWS.View
{
    public partial class frmRelatorioAtendimento : frmTemplateConsulta
    {
        private Cliente cliente;
        private Estudo pcmso;
        private Exame exame;
        private CentroCusto centroCusto;
        
        public frmRelatorioAtendimento()
        {
            InitializeComponent();
            /* inicializando combos */
            ComboHelper.situacaoAtendimentoRelatorioAtendimento(cbSituacaoAtendimento);
            ComboHelper.situacaoExameRelatorioAtendimento(cbSituacaoExame);
            ComboHelper.tipoAtendimentoRelatorioAtendimento(cbTipoAtendimento);
            ComboHelper.tipoRelatorioRelatorioAtendimento(cbTipoRelatorio);

        }

        private void btnFechar_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnCliente_Click(object sender, EventArgs e)
        {
            try
            {
                frmClienteSelecionar selecionarCliente = new frmClienteSelecionar(null, null, false, null, null, null, null, null, null);
                selecionarCliente.ShowDialog();

                if (selecionarCliente.Cliente != null)
                {
                    cliente = selecionarCliente.Cliente;
                    textCliente.Text = cliente.RazaoSocial;

                    /* preenchendo a seleção de contro de custo */
                    if (cliente.UsaCentroCusto == true)
                    {
                        cbCentroCusto.Enabled = true;
                        carregaCentroCusto(cliente.CentroCusto, cbCentroCusto);
                    }
                        

                }

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
        }

        private void btnPcmso_Click(object sender, EventArgs e)
        {
            try
            {
                if (cliente == null) throw new Exception("Selecione primeiro o cliente");
                frmPcmsoBuscar selecionarPcmso = new frmPcmsoBuscar(cliente);
                selecionarPcmso.ShowDialog();

                if (selecionarPcmso.Estudo != null)
                {
                    pcmso = selecionarPcmso.Estudo;
                    textPcmso.Text = ViewHelper.ValidaCampoHelper.RetornaCodigoEstudoFormatado(pcmso.CodigoEstudo);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }

        }

        private void btnExame_Click(object sender, EventArgs e)
        {
            try
            {
                frmExameBuscar selecionarExame = new frmExameBuscar(false);
                selecionarExame.ShowDialog();

                if (selecionarExame.Exame != null)
                {
                    exame = selecionarExame.Exame;
                    textExame.Text = exame.Descricao;
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }

        }

        private void btnExcluirCliente_Click(object sender, EventArgs e)
        {
            try
            {
                if (cliente == null) throw new Exception("Selecione primeiro o cliente.");
                cliente = null;
                textCliente.Text = string.Empty;

                /* excluindo informação de centro de custo */
                this.centroCusto = null;
                this.cbCentroCusto.DataSource = null;
                this.cbCentroCusto.Enabled = false;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }

        }

        private void btnExcluirPcmso_Click(object sender, EventArgs e)
        {
            try
            {
                if (pcmso == null) throw new Exception("Selecione primeiro o PCMSO.");
                pcmso = null;
                textPcmso.Text = string.Empty;

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
        }

        private void btnExcluirExame_Click(object sender, EventArgs e)
        {
            try
            {
                if (exame == null) throw new Exception("Selecione primeiro o Exame.");
                exame = null;
                textExame.Text = string.Empty;

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }

        }

        private void btnImprimir_Click(object sender, EventArgs e)
        {
            try
            {
                this.Cursor = Cursors.WaitCursor;
                if (!validaDados())
                {
                    string nomeRelatorio = ((SelectItem)cbTipoRelatorio.SelectedItem).Valor;
                    long idCliente = cliente != null ? (Int64)cliente.Id : 0;
                    long idExame = exame != null ? (Int64)exame.Id : 0;

                    string dataInicial = Convert.ToDateTime(this.dataInicial.Text).ToShortDateString();
                    string dataFinal = Convert.ToDateTime(this.dataFinal.Text).ToShortDateString();
                    string situacaoExame = ((SelectItem)cbSituacaoExame.SelectedItem) == null ? string.Empty : ((SelectItem)cbSituacaoExame.SelectedItem).Valor;
                    string tipoAtendimento = ((SelectItem)cbTipoAtendimento.SelectedItem) == null ? string.Empty : ((SelectItem)cbTipoAtendimento.SelectedItem).Valor;
                    string situacaoAtendimento = ((SelectItem)cbSituacaoAtendimento.SelectedItem) == null ? string.Empty : ((SelectItem)cbSituacaoAtendimento.SelectedItem).Valor;
                    string razaoSocial = cliente != null ? cliente.RazaoSocial : string.Empty;
                    string nomeExame = exame != null ? exame.Descricao : string.Empty;
                    long idPcmso = pcmso != null ? (long)pcmso.Id : 0;
                    string codigoEstudo = pcmso != null ? ViewHelper.ValidaCampoHelper.RetornaCodigoEstudoFormatado(pcmso.CodigoEstudo) : string.Empty;
                    string centroCustoLabel = cbCentroCusto.SelectedIndex > 0 ? ((SelectItem)cbCentroCusto.SelectedItem).Nome : string.Empty;
                    long idCentroCusto = cbCentroCusto.SelectedIndex > 0 ? Convert.ToInt64(((SelectItem)cbCentroCusto.SelectedItem).Valor) : 0;


                    var process = new Process();

                    process.StartInfo.FileName = "Relatorio\\jre7\\bin\\java";
                        process.StartInfo.Arguments = " -jar Relatorio\\MiddlewareJasper.jar " + "Relatorio\\Report\\" + nomeRelatorio + " ID_CLIENTE:" + idCliente + ":Integer" + " ID_EXAME:" + idExame + ":Integer" + " SITUACAO_EXAME:" + situacaoExame + ":String" + " TIPO_ATENDIMENTO:" + tipoAtendimento + ":String" + " DATA_INICIAL:" + dataInicial + ":String" + " DATA_FINAL:" + dataFinal + ":String" + " RAZAO_SOCIAL:" + '"' + razaoSocial + '"' + ":String" + " NOME_EXAME:" + '"' + nomeExame + '"' + ":String" + " SITUACAO_ATENDIMENTO:" + situacaoAtendimento + ":String" + " ID_PCMSO:" + idPcmso + ":INTEGER" + " CODIGO_ESTUDO:" + codigoEstudo + ":STRING" + " ID_EMPRESA:" + PermissionamentoFacade.usuarioAutenticado.Empresa.Id + ":INTEGER" + " CENTRO_CUSTO:" + '"' + centroCustoLabel + '"' + ":STRING" + " ID_CENTROCUSTO:" + idCentroCusto + ":INTEGER" ;
                    process.StartInfo.UseShellExecute = false;
                    process.StartInfo.RedirectStandardError = true;
                    process.StartInfo.RedirectStandardInput = true;
                    process.StartInfo.RedirectStandardOutput = true;
                    process.StartInfo.CreateNoWindow = true;

                    process.Start();
                    process.StandardOutput.ReadToEnd();
                    process.WaitForExit();

                }

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }

        public Boolean validaDados()
        {
            bool retorno = false;
            try
            {
                /* verificando datas */
                if (Convert.ToDateTime(dataInicial.Text) > Convert.ToDateTime(dataFinal.Text)) 
                    throw new Exception("A data final não pode ser menor do que a data inicial.");

                /* verificando se o relatório selecionado é por matriz e unidade.
                 * Nesse caso, o cliente selecionado precisa ser uma matriz */

                if (cliente == null && ((SelectItem)cbTipoRelatorio.SelectedItem).Valor == "RELATORIO_ATENDIMENTO_UNIDADE.JASPER")
                    throw new Exception("Obrigatório selecionar um cliente.");

                if (cliente == null && ((SelectItem)cbTipoRelatorio.SelectedItem).Valor == "RELATORIO_ATENDIMENTO_SETOR.jasper")
                    throw new Exception("Obrigatório selecionar um cliente.");

                if (!dataInicial.Checked && !dataFinal.Checked)
                    throw new Exception("Você deve selecionar o período");

                if (dataInicial.Checked && !dataFinal.Checked)
                    throw new Exception("Você deve selecionar a datata final");

                if (!dataInicial.Checked && dataFinal.Checked)
                    throw new Exception("Você deve selecionar a datata inicial.");

                ClienteFacade clienteFacade = ClienteFacade.getInstance();

                if (cliente != null && !clienteFacade.findClienteHaveUnidade(cliente) && ((SelectItem)cbTipoRelatorio.SelectedItem).Valor == "RELATORIO_ATENDIMENTO_UNIDADE.JASPER")
                    throw new Exception("O cliente precisa ser um cliente matriz.");

            }
            catch (Exception ex)
            {
                retorno = true;
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }

            return retorno;
        }

        public void carregaCentroCusto(List<CentroCusto> centroCustos, ComboBox cb)
        {
            try
            {
                ArrayList arr = new ArrayList();
                /* incluindo a primeira informação do combo */

                arr.Add(new SelectItem("0", "Todos os centros de custos"));

                centroCustos.OrderBy(item => item.Descricao).ToList().ForEach(delegate(CentroCusto cc)
                {
                    arr.Add(new SelectItem(cc.Id.ToString(), cc.Descricao));
                });

                cb.DataSource = arr;

                cb.DisplayMember = "Nome";
                cb.ValueMember = "Valor";

                cb.DropDownStyle = ComboBoxStyle.DropDownList;

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void cbTipoRelatorio_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                /* verificando se foi selecionado o relatório de setor então não pode haver
                 * seleção de exame e nem seleção de tipo de atendimento */

                if (((SelectItem)cbTipoRelatorio.SelectedItem).Valor == "RELATORIO_ATENDIMENTO_SETOR.jasper")
                {
                    /* zerando informações de exames e tipo de atendimento */
                    exame = null;
                    textExame.Text = string.Empty;
                    cbTipoAtendimento.SelectedIndex = -1;
                }


            }
            catch (Exception ex)
            {
                MessageBox.Show("Atenção", ex.Message, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
            
    }
}
