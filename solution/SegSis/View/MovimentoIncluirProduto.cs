﻿using Org.BouncyCastle.Utilities.Collections;
using SWS.Entidade;
using SWS.Facade;
using SWS.View.Resources;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace SWS.View
{
    public partial class frmMovimentoIncluirProduto : frmTemplateConsulta
    {
        private Cliente cliente;
        private ContratoProduto contratoProduto;
        private Contrato contratoVigente;

        public ContratoProduto ContratoProduto
        {
            get { return contratoProduto; }
            set { contratoProduto = value; }
        }
        
        public frmMovimentoIncluirProduto(Cliente cliente)
        {
            InitializeComponent();
            this.cliente = cliente;
            ActiveControl = text_descricao;
        }

        private void btn_fechar_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void montaGridProdutos()
        {
            try
            {
                this.Cursor = Cursors.WaitCursor;

                this.dgv_produto.Columns.Clear();

                FinanceiroFacade financeiroFacade = FinanceiroFacade.getInstance();

                /* verificando se o cliente está marcado para usar o contrato.
                 * Caso não esteja então será permitido incluir qualquer produto no movimento avulso.
                 * sendo necessário apenas atualizar o contrato systema com o novo produto. Caso tenha contrato
                 * então será permitido apenas a inclusão dos produtos que estão no contrato.
                 */

                if (cliente.UsaContrato)
                {
                    /* verificando o contrato em vigência do cliente */

                    contratoVigente = financeiroFacade.findContratoVigenteByData(DateTime.Now, cliente, null);

                    /* não será permitido incluir um produto que não esteja no contrato do cliente .*/

                    if (contratoVigente == null)
                        throw new Exception("O cliente utiliza contrato porém não tem nenhum contrato vigente nessa data.");
                    
                    HashSet<ContratoProduto> produtos = financeiroFacade.findAllProdutosByClienteInContrato(contratoVigente, String.IsNullOrWhiteSpace(text_descricao.Text) ? null : new Produto(null, text_descricao.Text.ToUpper(), String.Empty, 0 , String.Empty, 0, false, string.Empty));

                    dgv_produto.ColumnCount = 8;

                    this.dgv_produto.Columns[0].HeaderText = "Id_ContratoProduto";
                    this.dgv_produto.Columns[0].Visible = false;

                    this.dgv_produto.Columns[1].HeaderText = "Id_Produto";
                    this.dgv_produto.Columns[1].Visible = false;

                    this.dgv_produto.Columns[2].HeaderText = "Descrição";

                    this.dgv_produto.Columns[3].HeaderText = "Situação";
                    this.dgv_produto.Columns[3].Visible = false;

                    this.dgv_produto.Columns[4].HeaderText = "Preço do Produto";
                    this.dgv_produto.Columns[4].HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleRight;
                    this.dgv_produto.Columns[4].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;

                    this.dgv_produto.Columns[5].HeaderText = "Tipo";
                    this.dgv_produto.Columns[5].Visible = false;

                    this.dgv_produto.Columns[6].HeaderText = "Preço de Custo";
                    this.dgv_produto.Columns[6].Visible = false;

                    this.dgv_produto.Columns[7].HeaderText = "Padrão Contrato";
                    this.dgv_produto.Columns[7].Visible = false;

                    foreach (ContratoProduto cp in produtos)
                        dgv_produto.Rows.Add(cp.Id, cp.Produto.Id, cp.Produto.Nome, cp.Produto.Situacao, cp.PrecoContratado, cp.Produto.Tipo, cp.CustoContrato, cp.Produto.PadraoContrato);
                }
                else
                {
                    DataSet ds = financeiroFacade.findProdutoByFilter(new Produto(null, text_descricao.Text, ApplicationConstants.ATIVO, 0, string.Empty,0, false, string.Empty));

                    dgv_produto.DataSource = ds.Tables["produtos"].DefaultView;

                    dgv_produto.Columns[0].HeaderText = "Id Produto ";
                    dgv_produto.Columns[0].Visible = false;

                    dgv_produto.Columns[1].HeaderText = "Produto ";

                    dgv_produto.Columns[2].HeaderText = "Situação ";
                    dgv_produto.Columns[2].Visible = false;

                    dgv_produto.Columns[3].HeaderText = "Preço de Venda";
                    dgv_produto.Columns[3].HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleRight;
                    dgv_produto.Columns[3].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;

                    dgv_produto.Columns[4].HeaderText = "Tipo ";
                    dgv_produto.Columns[4].Visible = false;

                    dgv_produto.Columns[5].HeaderText = "Custo";
                    dgv_produto.Columns[5].Visible = true;

                    dgv_produto.Columns[6].HeaderText = "Padrão Contrato";
                    dgv_produto.Columns[6].Visible = false;

                }

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }

        }

        private void btn_pesquisar_Click(object sender, EventArgs e)
        {
            montaGridProdutos();
        }

        private void btn_incluir_Click(object sender, EventArgs e)
        {
            try
            {
                
                if (dgv_produto.CurrentRow == null)
                    throw new Exception ("Selecione um produto");
                
                this.Cursor = Cursors.WaitCursor;

                FinanceiroFacade financeiroFacade = FinanceiroFacade.getInstance();

                /* verificando se o cliente usa o contrato */
                if (cliente.UsaContrato)
                /* selecionando o produto */
                    contratoProduto = new ContratoProduto((Int64)dgv_produto.CurrentRow.Cells[0].Value, new Produto((Int64)dgv_produto.CurrentRow.Cells[1].Value, (String)dgv_produto.CurrentRow.Cells[2].Value, (String)dgv_produto.CurrentRow.Cells[3].Value, (Decimal)dgv_produto.CurrentRow.Cells[4].Value, (String)dgv_produto.CurrentRow.Cells[5].Value, (Decimal)dgv_produto.CurrentRow.Cells[6].Value, (bool)dgv_produto.CurrentRow.Cells[7].Value, string.Empty), contratoVigente, (Decimal)dgv_produto.CurrentRow.Cells[4].Value, null, (Decimal)dgv_produto.CurrentRow.Cells[6].Value, null, String.Empty, true);
                else
                {
                    /* verificando se o cliente não tem contrato systema. */
                    contratoVigente = financeiroFacade.findContratoSistemaByCliente(cliente);
                    
                    if (contratoVigente != null)
                    {
                        /* verificando se existe o produto já cadastrado no contrato */
                        contratoProduto = financeiroFacade.findContratoProdutoByProdutoInContrato(contratoVigente, new Produto((Int64)dgv_produto.CurrentRow.Cells[0].Value, (String)dgv_produto.CurrentRow.Cells[1].Value, (String)dgv_produto.CurrentRow.Cells[2].Value, (Decimal)dgv_produto.CurrentRow.Cells[3].Value, (String)dgv_produto.CurrentRow.Cells[4].Value, (Decimal)dgv_produto.CurrentRow.Cells[5].Value, (bool)dgv_produto.CurrentRow.Cells[6].Value, string.Empty));

                        if (contratoProduto == null)
                        {
                            /* incluindo o produto selecionado no contrato.*/
                            financeiroFacade.insertContratoProduto(new ContratoProduto(null, new Produto((Int64)dgv_produto.CurrentRow.Cells[0].Value, (String)dgv_produto.CurrentRow.Cells[1].Value, (String)dgv_produto.CurrentRow.Cells[2].Value, (Decimal)dgv_produto.CurrentRow.Cells[3].Value, (String)dgv_produto.CurrentRow.Cells[4].Value, (Decimal)dgv_produto.CurrentRow.Cells[5].Value, (bool)dgv_produto.CurrentRow.Cells[6].Value, string.Empty), contratoVigente, (Decimal)dgv_produto.CurrentRow.Cells[3].Value, PermissionamentoFacade.usuarioAutenticado, (Decimal?)dgv_produto.CurrentRow.Cells[5].Value, DateTime.Now, ApplicationConstants.ATIVO, true));

                            /* buscando o contrato que foi criado.*/
                            contratoProduto = financeiroFacade.findContratoProdutoByProdutoInContrato(contratoVigente, new Produto((Int64)dgv_produto.CurrentRow.Cells[0].Value, (String)dgv_produto.CurrentRow.Cells[1].Value, (String)dgv_produto.CurrentRow.Cells[2].Value, (Decimal)dgv_produto.CurrentRow.Cells[3].Value, (String)dgv_produto.CurrentRow.Cells[4].Value, (Decimal)dgv_produto.CurrentRow.Cells[5].Value, (bool)dgv_produto.CurrentRow.Cells[6].Value, string.Empty));

                        }
                        else
                        {
                            /* alterando o valor do produto no contrato. */
                            contratoProduto.PrecoContratado = (Decimal?)dgv_produto.CurrentRow.Cells[3].Value;
                            contratoProduto.CustoContrato = (Decimal?)dgv_produto.CurrentRow.Cells[5].Value;
                            
                            contratoProduto = financeiroFacade.updateContratoProduto(contratoProduto);
                        }
                        
                    }
                    else
                    {
                            /* incluindo um contrato sistema para o cliente. */
                            contratoVigente = financeiroFacade.insertContrato(new Contrato(null, String.Empty, cliente, PermissionamentoFacade.usuarioAutenticado, null, null, null, ApplicationConstants.CONTRATO_GRAVADO, String.Empty, null, null, String.Empty, null, false, null, null, false, false, false, null, false, null, null, null, null, null, false, null, null, null, String.Empty, PermissionamentoFacade.usuarioAutenticado.Empresa), null, null);

                            /* incluindo o produto selecionado no contrato. */
                            financeiroFacade.insertContratoProduto(new ContratoProduto(null, new Produto((Int64)dgv_produto.CurrentRow.Cells[0].Value, (String)dgv_produto.CurrentRow.Cells[1].Value, (String)dgv_produto.CurrentRow.Cells[2].Value, (Decimal)dgv_produto.CurrentRow.Cells[3].Value, (String)dgv_produto.CurrentRow.Cells[4].Value, (Decimal)dgv_produto.CurrentRow.Cells[5].Value, (bool)dgv_produto.CurrentRow.Cells[6].Value, string.Empty), contratoVigente, (Decimal?)dgv_produto.CurrentRow.Cells[3].Value, PermissionamentoFacade.usuarioAutenticado, (Decimal?)dgv_produto.CurrentRow.Cells[5].Value, DateTime.Now, ApplicationConstants.ATIVO, true));

                            contratoProduto = financeiroFacade.findContratoProdutoByProdutoInContrato(contratoVigente, new Produto((Int64)dgv_produto.CurrentRow.Cells[0].Value, (String)dgv_produto.CurrentRow.Cells[1].Value, (String)dgv_produto.CurrentRow.Cells[2].Value, (Decimal)dgv_produto.CurrentRow.Cells[3].Value, (String)dgv_produto.CurrentRow.Cells[4].Value, (Decimal)dgv_produto.CurrentRow.Cells[5].Value, (bool)dgv_produto.CurrentRow.Cells[6].Value, string.Empty));

                        }

                    }
                    this.Close();

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }

        private void dgv_produto_CellMouseDoubleClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            try
            {
                btn_incluir.PerformClick();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }
    }
}
