﻿namespace SWS.View
{
    partial class frmProtocoloIncluirItens
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            this.flpAcao = new System.Windows.Forms.FlowLayoutPanel();
            this.btnGravar = new System.Windows.Forms.Button();
            this.btnFechar = new System.Windows.Forms.Button();
            this.flowLayoutPanel1 = new System.Windows.Forms.FlowLayoutPanel();
            this.btnAtendimento = new System.Windows.Forms.Button();
            this.btnDocumento = new System.Windows.Forms.Button();
            this.btnProduto = new System.Windows.Forms.Button();
            this.dgvItensProtocolo = new System.Windows.Forms.DataGridView();
            this.listBoxInformacao = new System.Windows.Forms.ListBox();
            this.marcarDesmarcarTodos = new System.Windows.Forms.CheckBox();
            ((System.ComponentModel.ISupportInitialize)(this.spForm)).BeginInit();
            this.spForm.Panel1.SuspendLayout();
            this.spForm.Panel2.SuspendLayout();
            this.spForm.SuspendLayout();
            this.pnlForm.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.banner)).BeginInit();
            this.flpAcao.SuspendLayout();
            this.flowLayoutPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvItensProtocolo)).BeginInit();
            this.SuspendLayout();
            // 
            // spForm
            // 
            // 
            // spForm.Panel1
            // 
            this.spForm.Panel1.Controls.Add(this.flpAcao);
            // 
            // pnlForm
            // 
            this.pnlForm.Controls.Add(this.marcarDesmarcarTodos);
            this.pnlForm.Controls.Add(this.listBoxInformacao);
            this.pnlForm.Controls.Add(this.dgvItensProtocolo);
            this.pnlForm.Controls.Add(this.flowLayoutPanel1);
            // 
            // flpAcao
            // 
            this.flpAcao.Controls.Add(this.btnGravar);
            this.flpAcao.Controls.Add(this.btnFechar);
            this.flpAcao.Dock = System.Windows.Forms.DockStyle.Fill;
            this.flpAcao.Location = new System.Drawing.Point(0, 0);
            this.flpAcao.Name = "flpAcao";
            this.flpAcao.Size = new System.Drawing.Size(784, 35);
            this.flpAcao.TabIndex = 0;
            // 
            // btnGravar
            // 
            this.btnGravar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnGravar.Image = global::SWS.Properties.Resources.Gravar;
            this.btnGravar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnGravar.Location = new System.Drawing.Point(3, 3);
            this.btnGravar.Name = "btnGravar";
            this.btnGravar.Size = new System.Drawing.Size(75, 23);
            this.btnGravar.TabIndex = 7;
            this.btnGravar.Text = "&Confirmar";
            this.btnGravar.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnGravar.UseVisualStyleBackColor = true;
            this.btnGravar.Click += new System.EventHandler(this.btnGravar_Click);
            // 
            // btnFechar
            // 
            this.btnFechar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnFechar.Image = global::SWS.Properties.Resources.close;
            this.btnFechar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnFechar.Location = new System.Drawing.Point(84, 3);
            this.btnFechar.Name = "btnFechar";
            this.btnFechar.Size = new System.Drawing.Size(75, 23);
            this.btnFechar.TabIndex = 8;
            this.btnFechar.Text = "&Cancelar";
            this.btnFechar.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnFechar.UseVisualStyleBackColor = true;
            this.btnFechar.Click += new System.EventHandler(this.btnFechar_Click);
            // 
            // flowLayoutPanel1
            // 
            this.flowLayoutPanel1.Controls.Add(this.btnAtendimento);
            this.flowLayoutPanel1.Controls.Add(this.btnDocumento);
            this.flowLayoutPanel1.Controls.Add(this.btnProduto);
            this.flowLayoutPanel1.Location = new System.Drawing.Point(3, 3);
            this.flowLayoutPanel1.Name = "flowLayoutPanel1";
            this.flowLayoutPanel1.Size = new System.Drawing.Size(758, 60);
            this.flowLayoutPanel1.TabIndex = 0;
            // 
            // btnAtendimento
            // 
            this.btnAtendimento.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnAtendimento.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAtendimento.Image = global::SWS.Properties.Resources.proposta;
            this.btnAtendimento.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnAtendimento.Location = new System.Drawing.Point(3, 3);
            this.btnAtendimento.Name = "btnAtendimento";
            this.btnAtendimento.Size = new System.Drawing.Size(100, 50);
            this.btnAtendimento.TabIndex = 0;
            this.btnAtendimento.Text = "Atendimento";
            this.btnAtendimento.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnAtendimento.UseVisualStyleBackColor = true;
            this.btnAtendimento.Click += new System.EventHandler(this.btnAtendimento_Click);
            // 
            // btnDocumento
            // 
            this.btnDocumento.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnDocumento.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnDocumento.ForeColor = System.Drawing.Color.Black;
            this.btnDocumento.Image = global::SWS.Properties.Resources.contrato;
            this.btnDocumento.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnDocumento.Location = new System.Drawing.Point(109, 3);
            this.btnDocumento.Name = "btnDocumento";
            this.btnDocumento.Size = new System.Drawing.Size(100, 50);
            this.btnDocumento.TabIndex = 1;
            this.btnDocumento.Text = "Documento";
            this.btnDocumento.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnDocumento.UseVisualStyleBackColor = true;
            this.btnDocumento.Click += new System.EventHandler(this.btnDocumento_Click);
            // 
            // btnProduto
            // 
            this.btnProduto.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnProduto.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnProduto.Image = global::SWS.Properties.Resources.aditivo;
            this.btnProduto.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnProduto.Location = new System.Drawing.Point(215, 3);
            this.btnProduto.Name = "btnProduto";
            this.btnProduto.Size = new System.Drawing.Size(100, 50);
            this.btnProduto.TabIndex = 2;
            this.btnProduto.Text = "Produto";
            this.btnProduto.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnProduto.UseVisualStyleBackColor = true;
            this.btnProduto.Click += new System.EventHandler(this.btnProduto_Click);
            // 
            // dgvItensProtocolo
            // 
            this.dgvItensProtocolo.AllowUserToAddRows = false;
            this.dgvItensProtocolo.AllowUserToDeleteRows = false;
            this.dgvItensProtocolo.AllowUserToOrderColumns = true;
            this.dgvItensProtocolo.AllowUserToResizeRows = false;
            dataGridViewCellStyle3.BackColor = System.Drawing.Color.Gainsboro;
            this.dgvItensProtocolo.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle3;
            this.dgvItensProtocolo.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.DisplayedCells;
            this.dgvItensProtocolo.BackgroundColor = System.Drawing.Color.White;
            this.dgvItensProtocolo.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle4.BackColor = System.Drawing.Color.LightGray;
            dataGridViewCellStyle4.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle4.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle4.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle4.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle4.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvItensProtocolo.DefaultCellStyle = dataGridViewCellStyle4;
            this.dgvItensProtocolo.Location = new System.Drawing.Point(3, 173);
            this.dgvItensProtocolo.MultiSelect = false;
            this.dgvItensProtocolo.Name = "dgvItensProtocolo";
            this.dgvItensProtocolo.RowHeadersVisible = false;
            this.dgvItensProtocolo.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.CellSelect;
            this.dgvItensProtocolo.Size = new System.Drawing.Size(758, 279);
            this.dgvItensProtocolo.TabIndex = 1;
            // 
            // listBoxInformacao
            // 
            this.listBoxInformacao.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.listBoxInformacao.FormattingEnabled = true;
            this.listBoxInformacao.Location = new System.Drawing.Point(3, 70);
            this.listBoxInformacao.Name = "listBoxInformacao";
            this.listBoxInformacao.Size = new System.Drawing.Size(758, 69);
            this.listBoxInformacao.TabIndex = 2;
            // 
            // marcarDesmarcarTodos
            // 
            this.marcarDesmarcarTodos.AutoSize = true;
            this.marcarDesmarcarTodos.Location = new System.Drawing.Point(4, 146);
            this.marcarDesmarcarTodos.Name = "marcarDesmarcarTodos";
            this.marcarDesmarcarTodos.Size = new System.Drawing.Size(148, 17);
            this.marcarDesmarcarTodos.TabIndex = 3;
            this.marcarDesmarcarTodos.Text = "Marcar/Desmarcar Todos";
            this.marcarDesmarcarTodos.UseVisualStyleBackColor = true;
            this.marcarDesmarcarTodos.CheckedChanged += new System.EventHandler(this.marcarDesmarcarTodos_CheckedChanged);
            // 
            // frmProtocoloIncluirItens
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(784, 561);
            this.Name = "frmProtocoloIncluirItens";
            this.Text = "INCLUIR ITENS NO PROTOCOLO";
            this.spForm.Panel1.ResumeLayout(false);
            this.spForm.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.spForm)).EndInit();
            this.spForm.ResumeLayout(false);
            this.pnlForm.ResumeLayout(false);
            this.pnlForm.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.banner)).EndInit();
            this.flpAcao.ResumeLayout(false);
            this.flowLayoutPanel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvItensProtocolo)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.FlowLayoutPanel flpAcao;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel1;
        private System.Windows.Forms.Button btnAtendimento;
        private System.Windows.Forms.Button btnDocumento;
        private System.Windows.Forms.Button btnProduto;
        private System.Windows.Forms.DataGridView dgvItensProtocolo;
        private System.Windows.Forms.ListBox listBoxInformacao;
        private System.Windows.Forms.Button btnGravar;
        private System.Windows.Forms.Button btnFechar;
        private System.Windows.Forms.CheckBox marcarDesmarcarTodos;
    }
}