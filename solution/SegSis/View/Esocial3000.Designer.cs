﻿namespace SWS.View
{
    partial class frmEsocial3000
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            this.flpAcao = new System.Windows.Forms.FlowLayoutPanel();
            this.btIncluir = new System.Windows.Forms.Button();
            this.btPesquisar = new System.Windows.Forms.Button();
            this.btnFechar = new System.Windows.Forms.Button();
            this.periodoInicial = new System.Windows.Forms.DateTimePicker();
            this.periodoFinal = new System.Windows.Forms.DateTimePicker();
            this.cbTipoEvento = new SWS.ComboBoxWithBorder();
            this.lblTipoEsocial = new System.Windows.Forms.TextBox();
            this.lblDataEmissao = new System.Windows.Forms.TextBox();
            this.btnExcluirCliente = new System.Windows.Forms.Button();
            this.btnCliente = new System.Windows.Forms.Button();
            this.textCliente = new System.Windows.Forms.TextBox();
            this.lblCliente = new System.Windows.Forms.TextBox();
            this.lblCodigoLote = new System.Windows.Forms.TextBox();
            this.lblFuncionario = new System.Windows.Forms.TextBox();
            this.btnExcluirFuncionario = new System.Windows.Forms.Button();
            this.btnFuncionario = new System.Windows.Forms.Button();
            this.textFuncionario = new System.Windows.Forms.TextBox();
            this.textCodigo = new System.Windows.Forms.MaskedTextBox();
            this.grbEventos = new System.Windows.Forms.GroupBox();
            this.dgvLoteCancelado = new System.Windows.Forms.DataGridView();
            ((System.ComponentModel.ISupportInitialize)(this.spForm)).BeginInit();
            this.spForm.Panel1.SuspendLayout();
            this.spForm.Panel2.SuspendLayout();
            this.spForm.SuspendLayout();
            this.pnlForm.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.banner)).BeginInit();
            this.flpAcao.SuspendLayout();
            this.grbEventos.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvLoteCancelado)).BeginInit();
            this.SuspendLayout();
            // 
            // spForm
            // 
            // 
            // spForm.Panel1
            // 
            this.spForm.Panel1.Controls.Add(this.flpAcao);
            // 
            // pnlForm
            // 
            this.pnlForm.Controls.Add(this.grbEventos);
            this.pnlForm.Controls.Add(this.textCodigo);
            this.pnlForm.Controls.Add(this.btnExcluirFuncionario);
            this.pnlForm.Controls.Add(this.btnFuncionario);
            this.pnlForm.Controls.Add(this.textFuncionario);
            this.pnlForm.Controls.Add(this.lblFuncionario);
            this.pnlForm.Controls.Add(this.lblCodigoLote);
            this.pnlForm.Controls.Add(this.periodoInicial);
            this.pnlForm.Controls.Add(this.periodoFinal);
            this.pnlForm.Controls.Add(this.cbTipoEvento);
            this.pnlForm.Controls.Add(this.lblTipoEsocial);
            this.pnlForm.Controls.Add(this.lblDataEmissao);
            this.pnlForm.Controls.Add(this.btnExcluirCliente);
            this.pnlForm.Controls.Add(this.btnCliente);
            this.pnlForm.Controls.Add(this.textCliente);
            this.pnlForm.Controls.Add(this.lblCliente);
            // 
            // flpAcao
            // 
            this.flpAcao.Controls.Add(this.btIncluir);
            this.flpAcao.Controls.Add(this.btPesquisar);
            this.flpAcao.Controls.Add(this.btnFechar);
            this.flpAcao.Dock = System.Windows.Forms.DockStyle.Fill;
            this.flpAcao.Location = new System.Drawing.Point(0, 0);
            this.flpAcao.Name = "flpAcao";
            this.flpAcao.Size = new System.Drawing.Size(784, 35);
            this.flpAcao.TabIndex = 0;
            // 
            // btIncluir
            // 
            this.btIncluir.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btIncluir.Image = global::SWS.Properties.Resources.Gravar;
            this.btIncluir.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btIncluir.Location = new System.Drawing.Point(3, 3);
            this.btIncluir.Name = "btIncluir";
            this.btIncluir.Size = new System.Drawing.Size(71, 22);
            this.btIncluir.TabIndex = 26;
            this.btIncluir.TabStop = false;
            this.btIncluir.Text = "&Gravar";
            this.btIncluir.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btIncluir.UseVisualStyleBackColor = true;
            this.btIncluir.Click += new System.EventHandler(this.btIncluir_Click);
            // 
            // btPesquisar
            // 
            this.btPesquisar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btPesquisar.Image = global::SWS.Properties.Resources.lupa;
            this.btPesquisar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btPesquisar.Location = new System.Drawing.Point(80, 3);
            this.btPesquisar.Name = "btPesquisar";
            this.btPesquisar.Size = new System.Drawing.Size(71, 22);
            this.btPesquisar.TabIndex = 27;
            this.btPesquisar.TabStop = false;
            this.btPesquisar.Text = "&Pesquisar";
            this.btPesquisar.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btPesquisar.UseVisualStyleBackColor = true;
            this.btPesquisar.Click += new System.EventHandler(this.btPesquisar_Click);
            // 
            // btnFechar
            // 
            this.btnFechar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnFechar.Image = global::SWS.Properties.Resources.close;
            this.btnFechar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnFechar.Location = new System.Drawing.Point(157, 3);
            this.btnFechar.Name = "btnFechar";
            this.btnFechar.Size = new System.Drawing.Size(71, 22);
            this.btnFechar.TabIndex = 28;
            this.btnFechar.TabStop = false;
            this.btnFechar.Text = "&Fechar";
            this.btnFechar.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnFechar.UseVisualStyleBackColor = true;
            this.btnFechar.Click += new System.EventHandler(this.btnFechar_Click);
            // 
            // periodoInicial
            // 
            this.periodoInicial.Checked = false;
            this.periodoInicial.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.periodoInicial.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.periodoInicial.Location = new System.Drawing.Point(140, 83);
            this.periodoInicial.Name = "periodoInicial";
            this.periodoInicial.ShowCheckBox = true;
            this.periodoInicial.Size = new System.Drawing.Size(107, 21);
            this.periodoInicial.TabIndex = 5;
            // 
            // periodoFinal
            // 
            this.periodoFinal.Checked = false;
            this.periodoFinal.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.periodoFinal.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.periodoFinal.Location = new System.Drawing.Point(246, 83);
            this.periodoFinal.Name = "periodoFinal";
            this.periodoFinal.ShowCheckBox = true;
            this.periodoFinal.Size = new System.Drawing.Size(107, 21);
            this.periodoFinal.TabIndex = 6;
            // 
            // cbTipoEvento
            // 
            this.cbTipoEvento.BackColor = System.Drawing.Color.LightGray;
            this.cbTipoEvento.BorderColor = System.Drawing.Color.DimGray;
            this.cbTipoEvento.BorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid;
            this.cbTipoEvento.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbTipoEvento.FormattingEnabled = true;
            this.cbTipoEvento.Location = new System.Drawing.Point(140, 63);
            this.cbTipoEvento.Name = "cbTipoEvento";
            this.cbTipoEvento.Size = new System.Drawing.Size(107, 21);
            this.cbTipoEvento.TabIndex = 4;
            // 
            // lblTipoEsocial
            // 
            this.lblTipoEsocial.BackColor = System.Drawing.Color.LightGray;
            this.lblTipoEsocial.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblTipoEsocial.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTipoEsocial.Location = new System.Drawing.Point(3, 63);
            this.lblTipoEsocial.Name = "lblTipoEsocial";
            this.lblTipoEsocial.ReadOnly = true;
            this.lblTipoEsocial.Size = new System.Drawing.Size(138, 21);
            this.lblTipoEsocial.TabIndex = 103;
            this.lblTipoEsocial.TabStop = false;
            this.lblTipoEsocial.Text = "Tipo";
            // 
            // lblDataEmissao
            // 
            this.lblDataEmissao.BackColor = System.Drawing.Color.LightGray;
            this.lblDataEmissao.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblDataEmissao.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDataEmissao.Location = new System.Drawing.Point(3, 83);
            this.lblDataEmissao.Name = "lblDataEmissao";
            this.lblDataEmissao.ReadOnly = true;
            this.lblDataEmissao.Size = new System.Drawing.Size(138, 21);
            this.lblDataEmissao.TabIndex = 102;
            this.lblDataEmissao.TabStop = false;
            this.lblDataEmissao.Text = "Período de geração";
            // 
            // btnExcluirCliente
            // 
            this.btnExcluirCliente.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnExcluirCliente.Image = global::SWS.Properties.Resources.close;
            this.btnExcluirCliente.Location = new System.Drawing.Point(731, 3);
            this.btnExcluirCliente.Name = "btnExcluirCliente";
            this.btnExcluirCliente.Size = new System.Drawing.Size(30, 21);
            this.btnExcluirCliente.TabIndex = 101;
            this.btnExcluirCliente.TabStop = false;
            this.btnExcluirCliente.UseVisualStyleBackColor = true;
            this.btnExcluirCliente.Click += new System.EventHandler(this.btnExcluirCliente_Click);
            // 
            // btnCliente
            // 
            this.btnCliente.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnCliente.Image = global::SWS.Properties.Resources.busca;
            this.btnCliente.Location = new System.Drawing.Point(702, 3);
            this.btnCliente.Name = "btnCliente";
            this.btnCliente.Size = new System.Drawing.Size(30, 21);
            this.btnCliente.TabIndex = 1;
            this.btnCliente.UseVisualStyleBackColor = true;
            this.btnCliente.Click += new System.EventHandler(this.btnCliente_Click);
            // 
            // textCliente
            // 
            this.textCliente.BackColor = System.Drawing.SystemColors.Control;
            this.textCliente.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textCliente.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textCliente.Location = new System.Drawing.Point(140, 3);
            this.textCliente.MaxLength = 100;
            this.textCliente.Name = "textCliente";
            this.textCliente.ReadOnly = true;
            this.textCliente.Size = new System.Drawing.Size(563, 21);
            this.textCliente.TabIndex = 99;
            this.textCliente.TabStop = false;
            // 
            // lblCliente
            // 
            this.lblCliente.BackColor = System.Drawing.Color.LightGray;
            this.lblCliente.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblCliente.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCliente.Location = new System.Drawing.Point(3, 3);
            this.lblCliente.Name = "lblCliente";
            this.lblCliente.ReadOnly = true;
            this.lblCliente.Size = new System.Drawing.Size(138, 21);
            this.lblCliente.TabIndex = 98;
            this.lblCliente.TabStop = false;
            this.lblCliente.Text = "Cliente";
            // 
            // lblCodigoLote
            // 
            this.lblCodigoLote.BackColor = System.Drawing.Color.LightGray;
            this.lblCodigoLote.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblCodigoLote.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCodigoLote.Location = new System.Drawing.Point(3, 23);
            this.lblCodigoLote.Name = "lblCodigoLote";
            this.lblCodigoLote.ReadOnly = true;
            this.lblCodigoLote.Size = new System.Drawing.Size(138, 21);
            this.lblCodigoLote.TabIndex = 106;
            this.lblCodigoLote.TabStop = false;
            this.lblCodigoLote.Text = "Código Lote";
            // 
            // lblFuncionario
            // 
            this.lblFuncionario.BackColor = System.Drawing.Color.LightGray;
            this.lblFuncionario.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblFuncionario.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblFuncionario.Location = new System.Drawing.Point(3, 43);
            this.lblFuncionario.Name = "lblFuncionario";
            this.lblFuncionario.ReadOnly = true;
            this.lblFuncionario.Size = new System.Drawing.Size(138, 21);
            this.lblFuncionario.TabIndex = 107;
            this.lblFuncionario.TabStop = false;
            this.lblFuncionario.Text = "Funcionário";
            // 
            // btnExcluirFuncionario
            // 
            this.btnExcluirFuncionario.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnExcluirFuncionario.Image = global::SWS.Properties.Resources.close;
            this.btnExcluirFuncionario.Location = new System.Drawing.Point(731, 43);
            this.btnExcluirFuncionario.Name = "btnExcluirFuncionario";
            this.btnExcluirFuncionario.Size = new System.Drawing.Size(30, 21);
            this.btnExcluirFuncionario.TabIndex = 110;
            this.btnExcluirFuncionario.TabStop = false;
            this.btnExcluirFuncionario.UseVisualStyleBackColor = true;
            this.btnExcluirFuncionario.Click += new System.EventHandler(this.btnExcluirFuncionario_Click);
            // 
            // btnFuncionario
            // 
            this.btnFuncionario.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnFuncionario.Image = global::SWS.Properties.Resources.busca;
            this.btnFuncionario.Location = new System.Drawing.Point(702, 43);
            this.btnFuncionario.Name = "btnFuncionario";
            this.btnFuncionario.Size = new System.Drawing.Size(30, 21);
            this.btnFuncionario.TabIndex = 3;
            this.btnFuncionario.UseVisualStyleBackColor = true;
            this.btnFuncionario.Click += new System.EventHandler(this.btnFuncionario_Click);
            // 
            // textFuncionario
            // 
            this.textFuncionario.BackColor = System.Drawing.SystemColors.Control;
            this.textFuncionario.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textFuncionario.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textFuncionario.Location = new System.Drawing.Point(140, 43);
            this.textFuncionario.MaxLength = 100;
            this.textFuncionario.Name = "textFuncionario";
            this.textFuncionario.ReadOnly = true;
            this.textFuncionario.Size = new System.Drawing.Size(563, 21);
            this.textFuncionario.TabIndex = 108;
            this.textFuncionario.TabStop = false;
            // 
            // textCodigo
            // 
            this.textCodigo.BackColor = System.Drawing.Color.White;
            this.textCodigo.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textCodigo.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textCodigo.Location = new System.Drawing.Point(140, 23);
            this.textCodigo.Mask = "9999,99,99,999999";
            this.textCodigo.Name = "textCodigo";
            this.textCodigo.PromptChar = ' ';
            this.textCodigo.Size = new System.Drawing.Size(621, 21);
            this.textCodigo.TabIndex = 2;
            // 
            // grbEventos
            // 
            this.grbEventos.Controls.Add(this.dgvLoteCancelado);
            this.grbEventos.Location = new System.Drawing.Point(3, 110);
            this.grbEventos.Name = "grbEventos";
            this.grbEventos.Size = new System.Drawing.Size(758, 342);
            this.grbEventos.TabIndex = 112;
            this.grbEventos.TabStop = false;
            this.grbEventos.Text = "Eventos";
            // 
            // dgvLoteCancelado
            // 
            this.dgvLoteCancelado.AllowUserToAddRows = false;
            this.dgvLoteCancelado.AllowUserToDeleteRows = false;
            this.dgvLoteCancelado.AllowUserToOrderColumns = true;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.Gainsboro;
            this.dgvLoteCancelado.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle1;
            this.dgvLoteCancelado.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this.dgvLoteCancelado.BackgroundColor = System.Drawing.Color.White;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Arial", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvLoteCancelado.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle2;
            this.dgvLoteCancelado.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle3.BackColor = System.Drawing.Color.LightGray;
            dataGridViewCellStyle3.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvLoteCancelado.DefaultCellStyle = dataGridViewCellStyle3;
            this.dgvLoteCancelado.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvLoteCancelado.Location = new System.Drawing.Point(3, 16);
            this.dgvLoteCancelado.MultiSelect = false;
            this.dgvLoteCancelado.Name = "dgvLoteCancelado";
            this.dgvLoteCancelado.ReadOnly = true;
            this.dgvLoteCancelado.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvLoteCancelado.Size = new System.Drawing.Size(752, 323);
            this.dgvLoteCancelado.TabIndex = 7;
            // 
            // frmEsocial3000
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(784, 561);
            this.Name = "frmEsocial3000";
            this.Text = "INCLUIR EVENTO 3000";
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.frmEsocial3000_KeyDown);
            this.spForm.Panel1.ResumeLayout(false);
            this.spForm.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.spForm)).EndInit();
            this.spForm.ResumeLayout(false);
            this.pnlForm.ResumeLayout(false);
            this.pnlForm.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.banner)).EndInit();
            this.flpAcao.ResumeLayout(false);
            this.grbEventos.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvLoteCancelado)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.FlowLayoutPanel flpAcao;
        private System.Windows.Forms.Button btIncluir;
        private System.Windows.Forms.Button btPesquisar;
        private System.Windows.Forms.Button btnFechar;
        private System.Windows.Forms.DateTimePicker periodoInicial;
        private System.Windows.Forms.DateTimePicker periodoFinal;
        private ComboBoxWithBorder cbTipoEvento;
        private System.Windows.Forms.TextBox lblTipoEsocial;
        private System.Windows.Forms.TextBox lblDataEmissao;
        private System.Windows.Forms.Button btnExcluirCliente;
        private System.Windows.Forms.Button btnCliente;
        public System.Windows.Forms.TextBox textCliente;
        private System.Windows.Forms.TextBox lblCliente;
        private System.Windows.Forms.TextBox lblCodigoLote;
        private System.Windows.Forms.Button btnExcluirFuncionario;
        private System.Windows.Forms.Button btnFuncionario;
        public System.Windows.Forms.TextBox textFuncionario;
        private System.Windows.Forms.TextBox lblFuncionario;
        public System.Windows.Forms.MaskedTextBox textCodigo;
        private System.Windows.Forms.GroupBox grbEventos;
        public System.Windows.Forms.DataGridView dgvLoteCancelado;
    }
}